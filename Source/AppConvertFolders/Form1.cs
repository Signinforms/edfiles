﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using Shinetech.DAL;
using DataQuicker2.Framework;
using System.IO;
using Shinetech.Engines.Adapters;
using System.Web;
using System.Threading;
using Amib.Threading;

namespace AppConvertFolders
{
    public partial class Form1 : Form
    {
        SmartThreadPool smartThreadPool;

        private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;

        static string VolumePath_ROOT = @"C:\inetpub\ftproot\Volume";

        string VolumePath = @"C:\inetpub\ftproot\Volume";
        static string VolumePath_Site = @"C:\inetpub\EFileFolderWeb\Volume";

        Dictionary<int, string> colors = new Dictionary<int, string>();

        DataTable dataSource;
        Hashtable officeSource;
        Hashtable templateSource;
        Hashtable templateColorSource;

        string EffID;
        string EffDividerID;

       // IList removeFolders;

        int fileNumb = 0;
        int workedfileNum = 0;
        public Form1()
        {
            STPStartInfo stpStartInfo = new STPStartInfo();
            stpStartInfo.AreThreadsBackground = false;
          
            smartThreadPool = new SmartThreadPool(stpStartInfo);

            smartThreadPool.Start();

            colors.Add(0, "#FF0000");
            colors.Add(1, "#FF9933");
            colors.Add(2, "#CC0066");
            colors.Add(3, "#CC9999");
            colors.Add(4, "#9900CC");
            colors.Add(5, "#9999FF");
            colors.Add(6, "#663300");
            colors.Add(7, "#66CC33");
            colors.Add(8, "#333366");
            colors.Add(9, "#33CC99");
            colors.Add(10, "#0033CC");
            colors.Add(11, "#00CCFF");

            InitializeComponent();
        }

        public void CreateLog(Exception ex, string backStr)
        {
            object obj;
            if (ex != null)
                obj = GetExceptionMsg(ex, backStr);
            else
                obj = backStr;
            if (!Directory.Exists(Application.StartupPath + "\\log"))
            {
                Directory.CreateDirectory(Application.StartupPath + "\\log");
            }
            string logPath = Application.StartupPath + "\\log\\" + DateTime.Now.ToString("yyyyMMdd") + ".log";
            if (File.Exists(logPath))
            {
                using (StreamWriter sw = File.AppendText(logPath))
                {
                    sw.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " " + obj.ToString());
                    sw.Close();
                }
            }
            else
            {
                FileStream fs = new FileStream(logPath, FileMode.Create, FileAccess.Write);
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(DateTime.Now.ToString("HH:mm:ss") + " " + obj.ToString());
                    sw.Close();
                    fs.Close();
                }
            }
        }
        public string GetExceptionMsg(Exception ex, string backStr)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("****************************异常文本****************************");
            sb.AppendLine("【出现时间】：" + DateTime.Now.ToString());
            sb.AppendLine("【出现地点】：" + backStr);
            if (ex != null)
            {
                sb.AppendLine("【异常类型】：" + ex.GetType().Name);
                sb.AppendLine("【异常信息】：" + ex.Message);
                sb.AppendLine("【堆栈调用】：" + ex.StackTrace);
            }
            else
            {
                sb.AppendLine("【未处理异常】：" + backStr);
            }
            sb.AppendLine("***************************************************************");
            return sb.ToString();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            DataTable offices = GetOfficeUsers();
            dataGridView1.AutoGenerateColumns = false;

            IList lists = new ArrayList();
            Hashtable hashUsers = new Hashtable();
            foreach (DataRow row in offices.Select("", "Name asc"))
            {
                lists.Add(row["Name"]);
                hashUsers.Add(row["Name"], row["UID"]);
            }

            lists.Insert(0,"");
            hashUsers.Add("", "");

            this.dataSource = this.MakeDataTable();
            this.officeSource = hashUsers;

            comboBoxUsers.DataSource = lists;

           
        }

        public static DataTable GetOfficeUsers()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "eFileFolder_GetOfficeUsers";
                command.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;

                DataTable table = new DataTable();
                
                dataAdapter.Fill(table);

                return table;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }

        private DataTable MakeDataTable()
        {
            // Create new DataTable.
            DataTable table = new DataTable();

            // Declare DataColumn and DataRow variables.
            DataColumn column;

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "ItemNo";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "FileName";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "FirstName_File";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "LastName_File";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "FileNo";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "FileYear";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "FirstName_Folder";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "LastName_Folder";
            column.DefaultValue = false;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "FolderName";
            column.DefaultValue = "";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "Folder_ID";
            column.DefaultValue = -1;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "DividerNo";
            column.DefaultValue = "";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "Divider_ID";
            column.DefaultValue = -1;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.Int32");
            column.ColumnName = "File_ID";
            column.DefaultValue = -1;
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "Status";
            column.DefaultValue = "NA";
            table.Columns.Add(column);

            column = new DataColumn();
            column.DataType = Type.GetType("System.String");
            column.ColumnName = "File_Path";
            table.Columns.Add(column);


            return table;
        }

        protected void comboBoxUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBoxMessage.Clear();

            this.EffDividerID = "-1";
            if (comboBoxUsers.SelectedIndex == 0) return;
            if (string.IsNullOrEmpty(comboBoxUsers.SelectedValue.ToString())) return;

            // 获取该officeid 的所有folders

            this.EffID = this.officeSource[comboBoxUsers.SelectedValue.ToString()].ToString();
       
            this.dataSource.Clear();


            IList templates = GetTemplates(this.EffID);
            IList templateNames = new ArrayList();
            templateSource = new Hashtable();
            foreach (TemplateEntity row in templates)
            {
                templateNames.Add(comboBoxUsers.SelectedValue.ToString()+"-"+row.Name);
                if (!templateSource.ContainsKey(comboBoxUsers.SelectedValue.ToString() + "-" + row.Name))
                    templateSource.Add(comboBoxUsers.SelectedValue.ToString() + "-" + row.Name, row);

            }

            var template = new TemplateEntity("");
            template.TemplateID = -1;
            template.OfficeID = this.EffID;
            template.Content = "";
            template.Name = "";


            templateSource.Add(comboBoxUsers.SelectedValue.ToString() + "-" , template);
            templateNames.Insert(0, template.Name);

            comboBoxTemplates.DataSource = templateNames;      

            comboBoxTemplates.SelectedIndex = 0;

            //initFiles();
        }

        public static DataTable GetUserTemplates(string uid)
        {
            FolderTemplate template = new FolderTemplate();

            ObjectQuery query = template.CreateQuery();
            query.SetCriteria(template.UID, uid);

            DataTable table = new DataTable();
            try
            {
                query.Fill(table);
            }
            catch { }

            return table;
        }

        public DataTable GetOfficeUserIDBySubUserID(string strSubUserID)
        {
            Account user = new Account();
            ObjectQuery query = user.CreateQuery();

            query.SetSelectFields(new IColumn[] { user.OfficeUID });
            query.SetCriteria(user.UID, strSubUserID);

            try
            {
                DataTable dt = new DataTable();
                query.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        protected ArrayList GetTemplates(string uid)
        {

            DataTable table = GetUserTemplates(uid);

            ArrayList temples = new ArrayList();
            TemplateEntity template;

            foreach (DataRow item in table.Rows)
            {
                template = new TemplateEntity(item["Name"] as string);
                template.TemplateID = (int)item["TemplateID"];
                template.OfficeID = uid;
                template.Content = item["Content"] as string;

                temples.Add(template);
            }

            DataTable dtOfficeUser = GetOfficeUserIDBySubUserID(uid);
            var officeID1 = dtOfficeUser.Rows[0]["OfficeUID"].ToString();

            DataTable otable = GetUserTemplates(officeID1);

            foreach (DataRow item in otable.Rows)
            {
                template = new TemplateEntity(item["Name"] as string);
                template.TemplateID = (int)item["TemplateID"];
                template.OfficeID = officeID1;
                template.Content = item["Content"] as string;

                temples.Add(template);
            }

            template = new TemplateEntity("-- Options --");
            template.TemplateID = -1;
            template.OfficeID = uid;
            template.Content = "";

            temples.Insert(0, template);

            return temples;

        }

        protected void imageField_Click(object sender, EventArgs e)
        {         
            if (MessageBox.Show("Are you sure to start converting now?", "Question", MessageBoxButtons.YesNo) == DialogResult.No) return;


            textBoxMessage.Clear();
            
            labelST.Text = DateTime.Now.ToLongTimeString();

            #region delete duplicated folders

            /*
            FileFolder folderQ = new FileFolder();
            ObjectQuery query = folderQ.CreateQuery();
            query.SetCriteria(folderQ.OfficeID, "CC76D852-0518-437B-A72C-BC260C8D275B");
            query.SetCriteria(folderQ.DOB, Operator.MoreThan, new DateTime(2014, 9, 22));
            query.SetOrderBy(folderQ.FolderName);

            DataTable dt = new DataTable();
            query.Fill(dt);

            linkLabelLeft.Text = dt.Rows.Count.ToString();
            int count = dt.Rows.Count;
           
            for (int i = 1; i < count;i++)
            {
                DataRow row1 = dt.Rows[i-1];
                DataRow row2 = dt.Rows[i];

                var foldername1 = row1["FolderName"].ToString();
                var foldername2 = row2["FolderName"].ToString();

                if (foldername1.ToLower().Trim().Equals(foldername2.ToLower().Trim()))
                {
                    var folderId = Convert.ToInt32(row1["FolderID"]);
                    FileFolder folderD = new FileFolder(folderId);

                    if (folderD.IsExist)
                    {
                        try
                        {
                            folderD.Delete();
                            textBoxMessage.AppendText(row1["FolderName"].ToString() + "\r\n");
                        }
                        catch (Exception)
                        {
                            
                            
                        }
                        
                    }
                }

                
                
            }

            //清楚重复的folders

            return;

             * */
            #endregion

            buttonStart.Enabled = false;

           
            string uid = this.EffID;

            textBoxMessage.AppendText("\r\nStart Converting Folders");

            CreateLog(null, "\r\nStart Converting Folders");

            textBoxMessage.AppendText("\r\nConverting Folders");

            initFolders();

            linkLabelLeft.Text = (this.dataSource.Rows.Count - workedfileNum).ToString();
            linkLabelFinish.Text = workedfileNum.ToString();

            int rowid = 0;
            //转换文件
            foreach (DataRow row in this.dataSource.Rows)
            {
                rowid++;

                if (Convert.ToInt32(row["File_ID"]) > 0) //该文件已经存在
                {
                    string path = row["File_Path"] as string;

                    if (File.Exists(path))
                    {
                        try
                        {
                            File.Delete(path);
                        }
                        catch (Exception)
                        { }
                    }

                    row["Status"] = string.Format("File-{0} Exist", Path.GetFileNameWithoutExtension(path));

                    string message = string.Format("\r\n{0}: {1} Pass - {2}", row["ItemNo"], row["FileName"], DateTime.Now.ToLongTimeString());
                    textBoxMessage.AppendText(message);

                    CreateLog(null, message);

                    workedfileNum++;
                    linkLabelLeft.Text = (this.dataSource.Rows.Count - workedfileNum).ToString();
                    linkLabelFinish.Text = workedfileNum.ToString();
                    continue;
                }

                int fid = Convert.ToInt32(row["Folder_ID"]);

                var folder = new FileFolder(fid);

                int oldertid = Convert.ToInt32(row["Divider_ID"]);

                if (oldertid <= 0) //该Divider不存在            
                {
                    #region 创建新的divider
                    Divider divider = new Divider();

                    divider.EffID.Value = folder.FolderID.Value;
                    divider.DOB.Value = DateTime.Now;
                    divider.Locked.Value = false;
                    divider.OfficeID.Value = folder.OfficeID.Value;
                    divider.GUID.Value = Guid.NewGuid().ToString("N");

                    divider.Name.Value = Path.GetFileNameWithoutExtension(row["FileName"].ToString()); //带pdf扩展名
                    
                    if(templateColorSource.ContainsKey(divider.Name.Value.ToLower()))
                    {
                        divider.Color.Value = templateColorSource[divider.Name.Value.ToLower()].ToString();
                    }
                    else{
                         var r = new Random();
                         divider.Color.Value = colors[Convert.ToInt32(r.NextDouble() * 11)];
                    }
                   
                    //创建divider
                    divider.Create();

                    folder.OtherInfo.Value++;

                    folder.Update();

                    int tid = FileFolderManagement.GetDividerId(divider.GUID.Value, divider.OfficeID.Value);
                    if (tid == -1)
                    {
                        tid = divider.DividerID.Value;
                    }

                    row["Divider_ID"] = tid;
                    oldertid = tid;

                    #endregion
                }

                string vpath = @"C:\inetpub\EFileFolderWeb\Volume";
                string pathname = string.Format("{0}\\{1}\\{2}", vpath, fid, oldertid);

                string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, fid, Path.DirectorySeparatorChar, oldertid); //转换目标路径
                if (!Directory.Exists(dstpath))
                {
                    Directory.CreateDirectory(dstpath);
                }

                string filePath = row["File_Path"] as string;

                //该文件不在数据库但是物理文件也不存在，则只是创建divider等
                if (string.IsNullOrEmpty(filePath))
                {
                    row["Status"] = string.Format("{0} No Exist in Folder - {1}", Path.GetFileNameWithoutExtension(filePath), row["FolderName"]);

                    string message = string.Format("\r\n{0}: {1} No Exist in Disk - {2}", row["ItemNo"], row["FileName"], DateTime.Now.ToLongTimeString());
                    textBoxMessage.AppendText(message);

                    CreateLog(null, message);

                    workedfileNum++;
                    linkLabelLeft.Text = (this.dataSource.Rows.Count - workedfileNum).ToString();
                    linkLabelFinish.Text = workedfileNum.ToString();

                    continue;
                }

                string filename = Path.GetFileName(filePath);
                string encodeName = HttpUtility.UrlPathEncode(filename);
                encodeName = encodeName.Replace("%", "$");
                dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);

                //拷贝文件并转换格式
                try
                {
                    File.Copy(filePath, dstpath, true);
                    string[] state = new string[] { dstpath, fid.ToString(), oldertid.ToString(), uid, pathname, filePath, rowid.ToString() };

                    smartThreadPool.QueueWorkItem(new WorkItemCallback(ImageAdapter.ProcessPostFile), state, new PostExecuteWorkItemCallback(this.PostExecuteWorkItem));
                    //Thread t = new Thread(new ThreadStart(delegate() {
                    //    ImageAdapter.ProcessPostFile(dstpath, fid.ToString(), oldertid.ToString(), uid, pathname, filePath);
                    //}));                    

                    //t.Start();

                    //t.Join();

                    
                }
                catch (Exception exp)
                {
                    row["Status"] = exp.Message;

                    string message = string.Format("\r\n{0}: {1} failed - {2}", row["ItemNo"].ToString(), row["FileName"].ToString(), DateTime.Now.ToLongTimeString());
                    textBoxMessage.AppendText(message);
                    CreateLog(null, message);

                    workedfileNum++;
                    linkLabelLeft.Text = (this.dataSource.Rows.Count - workedfileNum).ToString();
                    linkLabelFinish.Text = workedfileNum.ToString();
                }
            }


            //最后进行一个清理操作
            //string fullPath = VolumePath;

            //string[] folders = Directory.GetDirectories(fullPath);

            //foreach (var subfolder in folders)
            //{
            //    try
            //     {
            //        string[] files = Directory.GetFiles(subfolder);
            //        if (files.Length == 0)
            //        {
            //            Directory.Delete(subfolder);
            //            textBoxMessage.AppendText(string.Format("\r\nFinish Folder:{0}", subfolder));
            //        }
            //    }
            //    catch (Exception)
            //    {
            //    }
            //}

            //textBoxMessage.AppendText("\r\nClear Folders");

            //textBoxMessage.AppendText("\r\nFinish Converting Folders");

            //labelET.Text = DateTime.Now.ToLongTimeString();

            //buttonStart.Enabled = true;

            //this.BindGrid();

        }

        protected void PostExecuteWorkItem(IWorkItemResult result)
        {
            if (this.InvokeRequired)
            {
                MethodInvoker del = delegate { PostExecuteWorkItem(result); };
                this.Invoke(del);

                return;
            }

            string[] state = result.GetResult() as string[];
            var row = this.dataSource.Rows[Convert.ToInt32(state[6]) - 1];

            if(this.dataSource.Rows.Count==Convert.ToInt32(state[6])){
                buttonStart.Enabled = true; // 最后一条记录
            }

            row["Status"] = "Success";

            
            string message = string.Format("\r\n{0}: {1} finished - {2}", row["ItemNo"], row["FileName"], DateTime.Now.ToLongTimeString());
            textBoxMessage.AppendText(message);
            CreateLog(null, message);

            workedfileNum++;
            linkLabelLeft.Text = (this.dataSource.Rows.Count - workedfileNum).ToString();
            linkLabelFinish.Text = workedfileNum.ToString();

            try
            {
                File.Delete(state[5]); //添加后删除原文
            }
            catch (Exception) //
            {
            }

            //检查该目录是否为空
            string[] files = Directory.GetFiles(Path.GetDirectoryName(state[5]));
            if (files.Length == 0)
            {
                try
                {
                    Directory.Delete(Path.GetDirectoryName(state[5]));
                    message = string.Format("\r\nFinish Folder:{0}", row["FolderName"].ToString());
                    textBoxMessage.AppendText(message);
                    CreateLog(null, message);
                }
                catch (Exception)
                {
                }
            }
        }

        //创建没有的folder
        protected void initFolders()
        {
            //var template = new FolderTemplate(Convert.ToInt32(DropDownList3.SelectedValue));
            //string[] dividers = template.Content.Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);


            Account act = new Account(this.EffID);
            string officeUID1 = this.EffID;
            if (act.IsSubUser.Value == "1")
            {
                officeUID1 = act.OfficeUID.Value;
            }


            // Convert to efilefolders
            foreach (DataRow row in this.dataSource.Rows)
            {
                var foldername = row["FolderName"].ToString();
                int folderId = -1;
                //检查该Folder是否已经有了
                folderId = FileFolderManagement.GetFileFolderWithItsName(foldername, this.EffID);

                //没哟则创建
                if (folderId == -1)
                {
                    FileFolder folder = null;

                    folder = new FileFolder();
                    folder.FolderName.Value = row["FolderName"] as string;
                    folder.FirstName.Value = row["FirstName_Folder"] as string;
                    folder.Lastname.Value = row["LastName_Folder"] as string;

                    folder.DOB.Value = DateTime.Now;
                    folder.OfficeID.Value = this.EffID;
                    folder.OfficeID1.Value = officeUID1;
                    folder.OtherInfo.Value = 0;
                    folder.SecurityLevel.Value = 1;

                    Dictionary<string, Divider> list = new Dictionary<string, Divider>();

                    //创建Folder
                    folderId = FileFolderManagement.CreatNewFileFolder(folder, list);

                    string message = string.Format("\r\n{0}: Created - {1}", row["ItemNo"].ToString(), row["FolderName"].ToString());
                    textBoxMessage.AppendText(message);

                    CreateLog(null, message);
                }
                else
                {
                    string message = string.Format("\r\n{0}: Exists - {1}", row["ItemNo"].ToString(), row["FolderName"].ToString());
                    textBoxMessage.AppendText(message);
                    CreateLog(null, message);
                }

                //为该folder添加divider，根据filename
                row["Folder_ID"] = folderId;

            }
        }

        private void comboBoxTemplates_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxTemplates.SelectedIndex == 0) return;

            if (string.IsNullOrEmpty(comboBoxTemplates.SelectedValue.ToString())) return;

            TemplateEntity ett = this.templateSource[comboBoxTemplates.SelectedValue.ToString()] as TemplateEntity;
            this.EffDividerID = ett.TemplateID.ToString();

            this.dataSource.Rows.Clear();

            if (Directory.Exists(textBoxFolder.Text))
            {
                initFiles();
            }
            
        }

        public static int GetFileWithName(string dividerName, int dividerId, int fodlerId)
        {
            Document doc = new Document();
            ObjectQuery query = doc.CreateQuery();

            query.SetCriteria(doc.FileFolderID, fodlerId);
            query.SetCriteria(doc.DividerID, dividerId);
            query.SetCriteria(doc.Name, dividerName);
            query.SetSelectFields(doc.DocumentID);

            DataTable table = new DataTable();
            try
            {
                query.Fill(table);
                if (table.Rows.Count <= 0)
                {
                    return -1;
                }

                return Convert.ToInt32(table.Rows[0][0]);
            }
            catch (Exception ex)
            {
                return -1;
            }
        }

        protected void initFiles()
        {
           
            this.dataSource.Rows.Clear();

            string fullPath = VolumePath;

            //Get All Sub folders 
            string[] folders = Directory.GetDirectories(fullPath,"*",SearchOption.TopDirectoryOnly);
            var i = 0;

            var dividerNames = new ArrayList();
            var dividerNamesWithCase = new Hashtable();

            if (!this.EffDividerID.Equals("-1")) // 选择了模板
            {
                templateColorSource = new Hashtable();

                var template = new FolderTemplate(Convert.ToInt32(this.EffDividerID));
                string[] dividers = template.Content.Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                var dname = "";
                foreach (var divider in dividers)
                {
                    string[] templateItems = divider.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                    dname = templateItems[0];
                    dividerNamesWithCase.Add(dname.ToLower(), dname);
                    dividerNames.Add(dname.ToLower());

                    templateColorSource.Add(dname.ToLower(), templateItems[1]); // templateItems[0] 是颜色
                }

            }


            //获取volume目录下的所有folder文件夹
            foreach (var subfolder in folders)
            {
                if (!subfolder.Contains(",")) continue; //目录都是以,号为间隔

                //排出系统目录
                var d = new DirectoryInfo(subfolder);
                
                if (d.Attributes == FileAttributes.Hidden || d.Attributes == FileAttributes.System)
                {
                    continue;
                }

                if ("System Volume Information".ToLower() == d.Name.ToLower() || "$RECYCLE.BIN".ToLower() == d.Name.ToLower())
                    continue;

                #region 获取folder的信息和是否已经创建并存在该folder

                var parentPath = Path.GetDirectoryName(subfolder);
                var folderName = subfolder.Substring(parentPath.Length + 1);
                string[] folderNames = folderName.Split(new string[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries); //folderName.Split(',',' '); 
                int fodlerId = -1;
                //判断该文件夹是否已经存在
                fodlerId = FileFolderManagement.GetFileFolderWithItsName(folderName, this.EffID);

                string subFirstName, subLastName;
                if (folderNames.Length > 1)
                {
                    subFirstName = folderNames[1]; // foldername的格式是lastname,firstname
                    subLastName = folderNames[0];
                }
                else
                {
                    subFirstName = folderName;
                    subLastName = "";
                }

                #endregion

                string[] files ; 

                try
                {
                    files =  Directory.GetFiles(subfolder);
                }
                catch (Exception exp)
                {
                    textBoxMessage.AppendText(exp.Message);
                    continue;
                }
                
                if (this.EffDividerID.Equals("-1"))
                {
                    // subfolder获取其下的文件
                    foreach (var pdf in files)
                    {
                        if (!Path.GetExtension(pdf).ToLower().Equals(".pdf"))
                        {
                            #region 删除非pdf文件
                            try
                            {
                                File.Delete(pdf); // 要删除否则后面无法删除目录
                            }
                            catch (Exception)
                            {


                            }
                            continue;

                            #endregion
                        }

                        #region 不选择任何模板时,以文件为准创建tab并导入pdf文件

                        i++;
                        var newRow = this.dataSource.NewRow();

                        newRow["ItemNo"] = i.ToString();
                        newRow["FileName"] = Path.GetFileName(pdf);
                        newRow["File_Path"] = pdf;

                        string[] fileNames = Path.GetFileNameWithoutExtension(pdf).Split(new string[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);
                        if (fileNames.Length > 2)
                        {
                            newRow["FirstName_File"] = fileNames[0];                            
                            newRow["LastName_File"] = fileNames[1] + " " + fileNames[2];
                        }
                        else if (fileNames.Length == 2)
                        {
                            newRow["FirstName_File"] = fileNames[0];
                            newRow["LastName_File"] = fileNames[1];
                        }
                        else
                        {
                            newRow["FirstName_File"] = Path.GetFileNameWithoutExtension(pdf);
                            newRow["LastName_File"] = "";
                        }

                        newRow["FolderName"] = folderName;
                        newRow["FirstName_Folder"] = subFirstName;
                        newRow["LastName_Folder"] = subLastName;

                        newRow["Folder_ID"] = fodlerId;

                        if (fodlerId != -1) //该folder已经存在的话，判断divider和文件
                        {
                            newRow["Status"] = string.Format("Folder:{0} exists", folderName);

                            //判断该文件是否已经存在
                            int dividerId = FileFolderManagement.GetDividersWithName(Path.GetFileNameWithoutExtension(pdf), fodlerId);

                            if (dividerId > 0)
                            {
                                newRow["Divider_ID"] = dividerId;
                                newRow["Status"] = string.Format("Tab:{0} exists, Will No Convert", Path.GetFileNameWithoutExtension(pdf));

                                string divider = Path.GetFileNameWithoutExtension(pdf);//必然物理文件一定存在
                                int fileId = FileFolderManagement.GetFileWithName(divider, dividerId, fodlerId);

                                string fullfile = pdf; // 一定存在该divider

                                if (fileId > 0) // 物理文件和数据库文件已经存在
                                {
                                    newRow["File_ID"] = fileId;// 后面检查该值不为-1则认为没有存在file在数据库中
                                    newRow["Status"] = string.Format("File {0} exists, Will No Add this file again", divider + ".pdf");

                                    //检查该物理文件是否存在，提示删除

                                    if (!fullfile.Equals("")) //没有与tab匹配文件
                                    {
                                        newRow["File_Path"] = fullfile;
                                        newRow["Status"] = string.Format("File {0} exists in volume and database, please delete it from disk", divider + ".pdf");
                                    }
                                }
                                else
                                {
                                    newRow["File_Path"] = fullfile;
                                    newRow["Status"] = string.Format("File {0} exists, will add to this tab:{1}", divider + ".pdf", divider);
                                }
                            }
                        }

                        this.dataSource.Rows.Add(newRow);

                        #endregion
                    }
                }
                else
                {
                    #region 遍历模板并检查该tab是否已经存在
                    foreach (string divider in dividerNames)
                    {
                        i++;

                        #region
                        var newRow = this.dataSource.NewRow();

                        newRow["ItemNo"] = i.ToString();
                        newRow["FileName"] = dividerNamesWithCase[divider].ToString() + ".pdf"; //大小写敏感

                        string[] fileNames = dividerNamesWithCase[divider].ToString().Split(new string[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);
                        if (fileNames.Length > 2)
                        {
                            newRow["FirstName_File"] = fileNames[0];                           
                            newRow["LastName_File"] = fileNames[1] + " " + fileNames[2];
                        }
                        else if (fileNames.Length == 2) // 有中间名字的情况
                        {
                            newRow["FirstName_File"] = fileNames[0];
                            newRow["LastName_File"] = fileNames[1];
                        }
                        else
                        {
                            newRow["FirstName_File"] = dividerNamesWithCase[divider].ToString();
                            newRow["LastName_File"] = "";
                        }

                        newRow["FolderName"] = folderName;
                        newRow["FirstName_Folder"] = subFirstName;
                        newRow["LastName_Folder"] = subLastName;

                        newRow["Folder_ID"] = fodlerId;

                        newRow["Divider_ID"] = -1;

                        //检查该tab是否已经存在
                        int dividerId = FileFolderManagement.GetDividersWithName(divider, fodlerId);

                        if (dividerId > 0)
                        {
                            newRow["Divider_ID"] = dividerId;

                            newRow["Status"] = string.Format("Template Tab:{0} exists, Will No Add this tab", divider);

                            //判断该文件是否已经存在(divider为tab的名称)
                            int fileId = FileFolderManagement.GetFileWithName(divider, dividerId, fodlerId);

                            if (fileId > 0)
                            {
                                newRow["File_ID"] = fileId;// 后面检查该值不为-1则认为没有存在file在数据库中
                                newRow["Status"] = string.Format("File {0} exists, Will No Add this file again", divider + ".pdf");

                                CreateLog(null, newRow["Status"].ToString());

                                //检查该物理文件是否存在，提示删除
                                string fullfile = findMatchingFile(divider, files);
                                if (!fullfile.Equals("")) //没有与tab匹配文件
                                {
                                    newRow["File_Path"] = fullfile;
                                    newRow["Status"] = string.Format("File {0} exists in volume and database, please delete it from disk", divider + ".pdf");
                                    CreateLog(null, newRow["Status"].ToString());
                                }
                            }
                            else
                            {
                                //只要divider开头匹配filename即可，返回的是物理文件的路径
                                string fullfile = findMatchingFile(divider, files);
                                if (fullfile.Equals("")) //没有与tab匹配文件
                                {
                                    newRow["Status"] = string.Format("File {0} not exists", divider + ".pdf");
                                    CreateLog(null, newRow["Status"].ToString());
                                }
                                else
                                {
                                    newRow["File_Path"] = fullfile;
                                    newRow["Status"] = string.Format("File {0} exists, will add to this tab:{1}", Path.GetFileName(fullfile), divider);
                                    CreateLog(null, newRow["Status"].ToString());
                                }
                            }

                        }
                        else
                        {
                            newRow["Status"] = string.Format("Create Tab:{0} and add matching file ", divider);
                            CreateLog(null, newRow["Status"].ToString());
                        }


                        this.dataSource.Rows.Add(newRow);

                        #endregion
                    }

                    #endregion
                }

            }

            this.fileNumb = this.dataSource.Rows.Count;
            linkLabelTotal.Text = this.fileNumb.ToString();

            dataGridView1.DataSource = this.dataSource;
          
           // this.BindGrid();

        }

        protected string findMatchingFile(string divider, string[] files)
        {
            foreach (string pdf in files)
            {
                string filename = Path.GetFileNameWithoutExtension(pdf).ToLower();

                //filename匹配divider的开头
                if (divider.Equals(filename, StringComparison.OrdinalIgnoreCase) || divider.StartsWith(filename, StringComparison.OrdinalIgnoreCase)) return pdf;
            }

            return "";
        }

        private void buttonInvalidePages_Click(object sender, EventArgs e)
        {
            return;

            if (MessageBox.Show("Are you sure to start pages now?", "Question", MessageBoxButtons.YesNo) == DialogResult.No) return;


            DataTable table = FileFolderManagement.GetInvlaidPathPages();
            string sql = string.Empty;
            int k=0, count = 0;
            string result;

            foreach(DataRow row in table.Rows){
                count++;
                sql = string.Format("UPDATE Page set ImagePath = 'Volume\\{0}\\{1}\\{2}' WHERE PageID = {3}", row["FileFolderID"], row["DividerID"], row["ImageName"],row["PageID"]);
                
                result =  FileFolderManagement.UpdateInvalidPagePath(sql);

                if (result.Length > 0)
                {
                    k++;
                    textBoxMessage.AppendText("\r\n" + result);

                    textBoxMessage.AppendText("\r\n" + sql);
                }
            }

            textBoxMessage.AppendText("\r\n Wrong pagesP: " + k.ToString());

            textBoxMessage.AppendText("\r\ntotal pages: " + count.ToString());



            MessageBox.Show("End pages Now");
        }

        private void buttonOpen_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = VolumePath_ROOT;

            if (folderBrowserDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.VolumePath = folderBrowserDialog1.SelectedPath;
                textBoxFolder.Text = this.VolumePath;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            smartThreadPool.Shutdown();
        }

        private void buttonExport_Click(object sender, EventArgs e)
        {

        }

       
    }

    [Serializable]
    public class TemplateEntity
    {
        public TemplateEntity(string name)
        {
            _TemplateID = -1;
            _Name = name;
        }

        private int _TemplateID;
        [Bindable(true)]
        public int TemplateID
        {
            get { return _TemplateID; }

            set { _TemplateID = value; }
        }

        private string _Name;
        [Bindable(true)]
        public string Name
        {
            get { return _Name; }

            set { _Name = value; }
        }

        private string _OfficeID;
        [Bindable(true)]
        public string OfficeID
        {
            get { return _OfficeID; }

            set { _OfficeID = value; }
        }

        private string _content;
        [Bindable(true)]
        public string Content
        {
            get { return _content; }

            set
            {
                _dividers.Clear();
                _content = value;

                string[] items = _content.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (string divider in items)
                {
                    string[] dcs = divider.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                    DividerEntity entity = new DividerEntity(dcs[0]);
                    entity.Color = dcs[1];
                    _dividers.Add(entity);
                }
            }
        }

        List<DividerEntity> _dividers = new List<DividerEntity>();

        public List<DividerEntity> Dividers
        {
            get
            {
                return _dividers;

            }
        }

    }

    [Serializable]
    public class DividerEntity
    {
        public DividerEntity(string name)
        {
            _DividerID = -1;
            _Name = name;
            _DOB = DateTime.Now;
            _Color = "#e78302";

        }

        public DividerEntity(string name, string color, DateTime date)
        {
            _DividerID = -1;
            _Name = name;
            _DOB = date;
            _Color = color;

        }

        private int _DividerID;
        [Bindable(true)]
        public int DividerID
        {
            get { return _DividerID; }

            set { _DividerID = value; }
        }

        private string _Name;
        [Bindable(true)]
        public string Name
        {
            get { return _Name; }

            set { _Name = value; }
        }

        private DateTime _DOB;
        [Bindable(true)]
        public DateTime DOB
        {
            get { return _DOB; }

            set { _DOB = value; }
        }

        private string _Color;
        [Bindable(true)]
        public string Color
        {
            get { return _Color; }

            set { _Color = value; }
        }

        private string _OfficeID;
        [Bindable(true)]
        public string OfficeID
        {
            get { return _OfficeID; }

            set { _OfficeID = value; }
        }
    }

}
