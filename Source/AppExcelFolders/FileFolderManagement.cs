using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;

using System.Globalization;

using DataQuicker2.Framework;
using Shinetech.DAL;
using AppConvertFolders;

/// <summary>
/// Summary description for FileFolderManagement
/// </summary>
public class FileFolderManagement
{
    public static string ConnectionName = "Default";
    private static readonly string ConnectionString =
        ConfigurationManager.ConnectionStrings["Default"].ConnectionString;

    public FileFolderManagement()
    {
        
    }

    public static bool ExistBox(string name, string uid)
    {
        try
        {
            Box box = new Box();
            ObjectQuery query = box.CreateQuery();
            query.SetCriteria(box.UserId, uid);
            query.SetCriteria(box.BoxName, name);

            DataTable dt = new DataTable();
            query.Fill(dt);

            return dt.Rows.Count > 0;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return true;
    }

    public static void CreatNewDivider(Divider tab)
    {
        
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            tab.Create(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }
    }

    public static int CreatNewFileFolder(FileFolder folder)
    {

        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            folder.Create(transaction);
            transaction.Commit();

            return folder.FolderID.Value;
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }
    }

    public static int CreatNewFileFolder(FileFolder folder, Dictionary<string, Divider> list)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            folder.OtherInfo.Value = list.Count;
            folder.Create(transaction);

            foreach (Divider item in list.Values)
            {
                item.OfficeID.Value = folder.OfficeID.Value;
                item.EffID.Value = folder.FolderID.Value;
                item.Create(transaction);
            }

            transaction.Commit();
            return folder.FolderID.Value;   //todo:binguo
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            
            throw new ApplicationException(ex.Message);
        }
    }

    public static int CreatNewFileFolder(FileFolder folder, Divider divider)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            folder.OtherInfo.Value = 1;
            folder.Create(transaction);

            divider.OfficeID.Value = folder.OfficeID.Value;
            divider.EffID.Value = folder.FolderID.Value;
            divider.Create(transaction);

            transaction.Commit();            
            return folder.FolderID.Value;   
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }
    }

    
    public static bool IsExistDivider(string name,string officeid)
    {
        try
        {
            Divider tab = new Divider();
            ObjectQuery query = tab.CreateQuery();
            query.SetCriteria(tab.OfficeID, officeid);
            query.SetCriteria(tab.Name, name);

            DataTable dt = new DataTable();
            query.Fill(dt);

            return dt.Rows.Count > 0;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return true;
    }

    public static int GetDividerId(string guid, string officeid)
    {
        string sqlstr = "SELECT DividerID FROM dbo.Divider " +
            "WHERE OfficeID='" + officeid + "' AND " + "GUID='" + guid + "'";
           
        SqlHelper sql = new SqlHelper("Default");
      
        try
        {
            object t =  sql.ExecuteScalar(sqlstr);
            return Convert.ToInt32(t);
        }
        catch (Exception ex)
        {
            return -1;
        }
    }

    public static bool IsExistFileFolder(string strFoldeName)
    {
        try
        {
            FileFolder folder = new FileFolder();
          
            ObjectQuery query = folder.CreateQuery();
            query.SetCriteria(folder.FolderName, strFoldeName);

            DataTable dt = new DataTable();
            query.Fill(dt);

            return dt.Rows.Count > 0;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return true;
    }


    public static DataTable GetFileFolders(string folderName,string firstname, string lastname, string date,string officeid)
    {
        FileFolder folder = new FileFolder();
        ObjectQuery query = folder.CreateQuery();

        query.SetCriteria(folder.OfficeID1, officeid); //change OfficeID to OfficeID1 by johnlu655 in 2013-11-23

        if (!string.IsNullOrEmpty(folderName))
            query.SetCriteria(folder.FolderName, Operator.BothSidesLike, folderName);

        if (!string.IsNullOrEmpty(firstname))
            query.SetCriteria(folder.FirstName,Operator.BothSidesLike, firstname);

        if (!string.IsNullOrEmpty(lastname))
            query.SetCriteria(folder.Lastname, Operator.BothSidesLike, lastname);

        if (!string.IsNullOrEmpty(date))
            query.SetCriteria(folder.FileNumber, Operator.BothSidesLike, date);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch(Exception ex)
        {
            return null;
        }
    }

    public static DataTable GetFileFolders(string keyname,string officeid)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SelectFileFolders";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@Key", keyname);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@OfficeID", officeid);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch(Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static Int32 GetFileFolderWithItsName(string keyname, string officeid)
    {
        FileFolder folder = new FileFolder();
        ObjectQuery query = folder.CreateQuery();

        query.SetCriteria(folder.OfficeID1, officeid);
        query.SetCriteria(folder.FolderName, keyname);
        //string[] folderNames = keyname.Split(new string[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);

        //if (folderNames.Length > 1)
        //{
        //    query.SetCriteria(folder.FirstName, folderNames[0]);
        //    query.SetCriteria(folder.Lastname, folderNames[1]);
        //}
        //else
        //{
        //    query.SetCriteria(folder.FolderName, keyname);
        //}

       
        query.SetSelectFields(folder.FolderID);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            if (table.Rows.Count <= 0)
            {
                return -1;
            }

            return Convert.ToInt32(table.Rows[0][0]);
        }
        catch (Exception ex)
        {
            return -1;
        }

    }

    public static DataTable GetFileFoldersWithPrefx(string keyname, string officeid)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SelectFileFoldersWithPrefx";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@Key", keyname);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@OfficeID", officeid);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetRequestFiles(string uid)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SearchRequestFiles";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@UID", uid);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetOfficeFileFolders(string folderName, string firstname, string lastname, string date, string officeid)
    {
        FileFolder folder = new FileFolder();
        ObjectQuery query = folder.CreateQuery();

        query.SetCriteria(folder.OfficeID1, officeid);

        if (!string.IsNullOrEmpty(folderName))
            query.SetCriteria(folder.FolderName, Operator.BothSidesLike, folderName);

        if (!string.IsNullOrEmpty(firstname))
            query.SetCriteria(folder.FirstName, Operator.BothSidesLike, firstname);

        if (!string.IsNullOrEmpty(lastname))
            query.SetCriteria(folder.Lastname, Operator.BothSidesLike, lastname);

        if (!string.IsNullOrEmpty(date))
            query.SetCriteria(folder.FileNumber, Operator.BothSidesLike, date);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static DataTable GetOfficeFileFolders(string keyname, string officeid)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SelectOfficeFileFolders";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@Key", keyname);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@OfficeID", officeid);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetOfficeFileFoldersWithPrefx(string keyname, string officeid)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SelectOfficeFileFoldersWithPrefx";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@Key", keyname);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@OfficeID", officeid);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetFileFoldersByUserID(string uid)
    {
        FileFolder folder = new FileFolder();
        DataTable table = new DataTable();
        try
        {
            ObjectQuery query = folder.CreateQuery();
            query.SetCriteria(folder.OfficeID1, uid);
            query.SetSelectFields(folder.FolderID, folder.FolderName);
            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetItsFileFoldersByUserID(string uid)
    {
        FileFolder folder = new FileFolder();
        DataTable table = new DataTable();
        try
        {
            ObjectQuery query = folder.CreateQuery();
            query.SetCriteria(folder.OfficeID, uid);
            query.SetSelectFields(folder.FolderID, folder.FolderName);
            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetRestoredeFolders()
    {
        FileFolder folder = new FileFolder();
        Account user = new Account();
        DataTable table = new DataTable();
        try
        {
            ObjectQuery query = folder.CreateQuery();
            query.SetAssociation(new IColumn[] { folder.SavedID }, new IColumn[] { user.UID }, AssociationType.LeftJoin);
            query.SetCriteria(folder.OfficeID,Operator.Equal, '0');
            query.SetCriteria(folder.OfficeID1, Operator.Equal, '0');
            query.SetSelectFields(folder.FolderID, folder.FolderName, folder.OfficeID, folder.SavedID, folder.DeleteDate, folder.FileNumber,
                user.Name, user.Firstname, user.Lastname);
            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetFileFoldersWithDisplayName(string userid,string officeid)
    {
        string sqlstr = "SELECT FolderID,DisplayName=(FolderName+'   ('+convert(varchar(10),DOB,101)+')') FROM dbo.FileFolder "+
            "WHERE OfficeID='" + userid + "' OR" +
            "(OfficeID1='" + officeid + "' AND SecurityLevel!='0'" + ") ORDER BY DOB DESC";
        SqlHelper sql = new SqlHelper("Default");
        DataTable table = new DataTable();
        try
        {
            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static DataTable GetMyFileFoldersWithDisplayName(string userid)
    {
        string sqlstr =
            "SELECT FolderID,DisplayName=(FolderName+'('+convert(varchar(10),DOB,101)+')') FROM dbo.FileFolder " +
            "WHERE OfficeID='" + userid + "'ORDER BY DOB DESC";
        SqlHelper sql = new SqlHelper("Default");
        DataTable table = new DataTable();
        try
        {
            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static DataTable GetFileFoldersInfor(string officeid1)
    {
        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchFileFoldersInfor] '" + officeid1 + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

        //FileFolder folder = new FileFolder();
        //Account user = new Account();
        //Reminder item = new Reminder();
        //DataTable dt = new DataTable("Folder");
        //try
        //{
        //    ObjectQuery query = folder.CreateQuery();
        //    query.SetCriteria(folder.OfficeID1, officeid1);
        //    //query.SetAssociation(new IColumn[] { folder.OfficeID }, new IColumn[] { user.UID }, AssociationType.LeftJoin);
        //    query.SetAssociation(new IColumn[] { folder.FolderID }, new IColumn[] { item.EffID }, AssociationType.LeftJoin);
        //    query.SetAssociation(new IColumn[] { item.UID }, new IColumn[] { user.UID }, AssociationType.LeftJoin);

        //    query.SetSelectFields(folder.FolderID, folder.FolderName, folder.FirstName, folder.Lastname,folder.FileNumber,
        //        folder.Alert1, folder.Alert2, folder.Alert3, folder.Alert4, folder.Comments, item.DOB, folder.OtherInfo, folder.OtherInfo2,
        //        folder.OfficeID, folder.OfficeID1, folder.SecurityLevel, folder.FolderCode,folder.LastDate, user.Name);

        //    query.SetOrderBy(folder.LastDate,false);
        //    query.Fill(dt);
        //}
        //catch (Exception ex)
        //{
        //    new ApplicationException(ex.Message);
        //}

        //return dt;
    }

    

    public static DataTable GetFileForms()
    {
        FileForm form = new FileForm();
        Account user = new Account();

        DataTable dt = new DataTable("FileForm");
        try
        {
            ObjectQuery query = form.CreateQuery();
            query.SetAssociation(new IColumn[] { form.UserID }, new IColumn[] { user.UID }, AssociationType.LeftJoin);
            query.SetSelectFields(form.FormId, form.FileQuantity, form.OfficeName, form.PhoneNumber, form.UserID, 
                                form.RequestedBy, form.RequestDate, form.ConvertDate,form.Comment,form.Status,
                                user.Name, user.Firstname, user.Lastname);

            query.SetOrderBy(form.CreateDate, false);
            query.Fill(dt);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return dt;
    }

    public static DataTable GetFileFormsWithStatus()
    {
        FormFile file = new FormFile();
        FileForm form = new FileForm();
        Account user = new Account();

        DataTable dt = new DataTable("FileForm");
        try
        {
            ObjectQuery query = file.CreateQuery();
            query.SetAssociation(new IColumn[] { file.UserID }, new IColumn[] { user.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { file.FormId }, new IColumn[] { form.FormId }, AssociationType.LeftJoin);

            query.SetSelectFields(file.FileID,file.FileName,file.FileNumber,file.FirstName,file.LastName,file.YearDate,
                file.UploadDate,file.ConvertDate,file.CreateDate,file.Flag,file.Status,
                form.FormId, form.FileQuantity, form.OfficeName, form.PhoneNumber, form.UserID,
                                form.RequestedBy, form.RequestDate, form.Comment, 
                                user.Name);
            query.TopN = 1000;
            query.SetCriteria(file.FirstName, Operator.NotEqual, "");
            query.SetOrderBy(form.CreateDate, false);
            query.Fill(dt);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return dt;
    }

    public static DataTable GetFileFormsByUID(string uid)
    {
        FileForm form = new FileForm();
        Account user = new Account();

        DataTable dt = new DataTable("FileForm");
        try
        {
            ObjectQuery query = form.CreateQuery();
            query.SetAssociation(new IColumn[] { form.UserID }, new IColumn[] { user.UID }, AssociationType.LeftJoin);
            query.SetSelectFields(form.FormId, form.FileQuantity, form.OfficeName, form.PhoneNumber, form.UserID,
                                form.RequestedBy, form.RequestDate, form.ConvertDate, form.Comment, form.Status,
                                user.Name, user.Firstname, user.Lastname);

            query.SetOrderBy(form.CreateDate, false);
            query.SetCriteria(form.UserID, uid);
            query.Fill(dt);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return dt;
    }

    public static DataTable GetFileFormsWithStatus(string uid)
    {
        FormFile file = new FormFile();
        FileForm form = new FileForm();
        Account user = new Account();

        DataTable dt = new DataTable("FileForm");
        try
        {
            ObjectQuery query = file.CreateQuery();
            query.SetAssociation(new IColumn[] { file.UserID }, new IColumn[] { user.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { file.FormId }, new IColumn[] { form.FormId }, AssociationType.LeftJoin);

            query.SetSelectFields(file.FileID, file.FileName, file.FileNumber, file.FirstName, file.LastName, file.YearDate,
                file.UploadDate, file.ConvertDate, file.CreateDate, file.Flag, file.Status,
                form.FormId, form.FileQuantity, form.OfficeName, form.PhoneNumber, form.UserID,
                                form.RequestedBy, form.RequestDate, form.Comment, user.Name);

            query.SetCriteria(file.FirstName,Operator.NotEqual, "");
            query.SetCriteria(file.UserID, uid);
            query.SetOrderBy(form.CreateDate, false);
            query.Fill(dt);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return dt;
    }

    
    public static DataTable GetFileFormsByUID(string uid, string key)
    {
        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchFileFormsByKey] '%" + key + "%' ," + "'" + uid + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetFileFormsWithStatusByUID(string uid, string key)
    {
        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchFileFormsWithStatusByKey] '%" + key + "%' ," + "'" + uid + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }


    public static DataTable GetAllFileFormsByKey(string key,string date)
    {
        DateTime v = DateTime.Parse(date, new CultureInfo("en-us", false));

        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllFileFormsByKeyDate] '%" + key + "%','" + v.ToShortDateString() + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetAllFileFormsWithStatusByKey(string key, string date)
    {
        DateTime v = DateTime.Parse(date, new CultureInfo("en-us", false));

        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllFileFormsWithStatusByKeyDate] '%" + key + "%','" + v.ToShortDateString() + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetAllFileFormsByKey(string key, string date,string date1)
    {
        DateTime v = DateTime.Parse(date, new CultureInfo("en-us", false));
        DateTime v1 = DateTime.Parse(date1, new CultureInfo("en-us", false));

        if (v > v1)
        {
            var temp = v1;
            v1 = v;
            v = temp;
        }

        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllFileFormsByKeyDateSE] '%" + key + "%','" + v.ToShortDateString() + "','" + v1.ToShortDateString() + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetAllFileFormsWithStatusByKey(string key, string date, string date1)
    {
        DateTime v = DateTime.Parse(date, new CultureInfo("en-us", false));
        DateTime v1 = DateTime.Parse(date1, new CultureInfo("en-us", false));

        if (v > v1)
        {
            var temp = v1;
            v1 = v;
            v = temp;
        }

        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllFileFormsWithStatusByKeyDateSE] '%" + key + "%','" + v.ToShortDateString() + "','" + v1.ToShortDateString() + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetAllFileFormsByKey(string key)
    {
        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllFileFormsByKey] '%" + key + "%'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetAllFileFormsWithStatusByKey(string key)
    {
        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllFileFormsWithStatusByKey] '%" + key + "%'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }


    public static DataTable GetAllPaperFilesByKey(string key, string date)
    {
        DateTime v = DateTime.Parse(date, new CultureInfo("en-us", false));

        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllPaperFilesByKeyDate] '%" + key + "%','" + v.ToShortDateString() + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetAllPaperFilesByKey(string key, string date, string date1)
    {
        DateTime v = DateTime.Parse(date, new CultureInfo("en-us", false));
        DateTime v1 = DateTime.Parse(date1, new CultureInfo("en-us", false));

        if (v > v1)
        {
            var temp = v1;
            v1 = v;
            v = temp;
        }

        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllPaperFilesByKeyDateSE] '%" + key + "%','" + v.ToShortDateString() + "','" + v1.ToShortDateString() + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetAllPaperFilesByKey(string key)
    {
        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllPaperFilesByKey] '%" + key + "%'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetUploadedFiles()
    {
        UploadFile uploadFile = new UploadFile();
        Account user = new Account();

         DataTable dt = new DataTable("UploadFile");
        try
        {
            ObjectQuery query = uploadFile.CreateQuery();
            query.SetAssociation(new IColumn[] { uploadFile.UserID }, new IColumn[] { user.UID }, AssociationType.LeftJoin);

            query.SetSelectFields(uploadFile.FileID, uploadFile.ConvertedDate, uploadFile.DividerId, uploadFile.FolderId, uploadFile.FolderName,
                    uploadFile.FirstName, uploadFile.LastName, uploadFile.Flag, uploadFile.FormFileID, uploadFile.UploadDate, uploadFile.UserID,
                    user.Name);

            query.SetOrderBy(uploadFile.UploadDate, false);
            query.Fill(dt);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return dt;
    }

    public static DataTable GetFormFiles(String key)
    {
        FormFile ffile = new FormFile();
        DataTable table = new DataTable("FormFile");

        try
        {
            ObjectQuery query = ffile.CreateQuery();
            query.SetCriteria(ffile.FormId, key);
       
            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
        
    }

    public static DataTable GetFormFilesById(string id)
    {
        FormFile ffile = new FormFile();
        DataTable table = new DataTable("FormFile");

        try
        {
            ObjectQuery query = ffile.CreateQuery();
            query.SetCriteria(ffile.FormId, id);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetPaperFiles(string userId, string boxkey)
    {
        DataTable table = new DataTable("PagerFile");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchPagerFilesByKey] '%" + boxkey + "%' ," + "'" + userId + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {            
        }

        return table;

    }



    public static DataTable GetPaperFiles(String userId, int boxId)
    {
        PaperFile pfile = new PaperFile();
        DataTable table = new DataTable("PagerFile");
        Account account = new Account();
        Box box = new Box();
        try
        {
            ObjectQuery query = pfile.CreateQuery();
            query.SetCriteria(pfile.FirstName, Operator.NotEqual, "");
            query.SetCriteria(pfile.UserID, userId);
            query.SetCriteria(pfile.Flag, false);
            query.SetCriteria(box.BoxId, boxId);

            query.SetAssociation(new IColumn[] { pfile.UserID }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { pfile.BoxId }, new IColumn[] { box.BoxId }, AssociationType.LeftJoin);

            query.SetSelectFields(pfile.FileID, pfile.FileName, pfile.FileNumber, pfile.FirstName, pfile.LastName,
                pfile.CreateDate, pfile.UserID, pfile.BoxId, pfile.RequestDate, pfile.Flag, pfile.Status,box.BoxName, box.BoxNumber,
                box.Warehouse, box.Section, box.Row, box.Space, box.Other, box.DestroyDate, account.Name);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetPaperFilesByBoxId(string boxId)
    {
        PaperFile pfile = new PaperFile();
        DataTable table = new DataTable("PagerFile");

        try
        {
            ObjectQuery query = pfile.CreateQuery();
            query.SetCriteria(pfile.BoxId, boxId);
            query.SetOrderBy(pfile.FileIndex,true);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetPaperFiles(String userId)
    {
        PaperFile pfile = new PaperFile();
        Account account = new Account();
        Box box = new Box();

        DataTable table = new DataTable("PagerFile");
        
        try
        {
            ObjectQuery query = pfile.CreateQuery();
            query.SetCriteria(pfile.FirstName, Operator.NotEqual, "");
            query.SetCriteria(pfile.UserID, userId);
            query.SetCriteria(pfile.Flag,false);

            query.SetAssociation(new IColumn[] { pfile.UserID }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { pfile.BoxId }, new IColumn[] { box.BoxId }, AssociationType.LeftJoin);

            query.SetSelectFields(pfile.FileID, pfile.FileName, pfile.FileNumber, pfile.FirstName, pfile.LastName,
                pfile.CreateDate, pfile.UserID, pfile.BoxId, pfile.RequestDate, pfile.Flag,pfile.Status, box.BoxName, box.BoxNumber,
                box.Warehouse, box.Section, box.Row, box.Space, box.Other,box.DestroyDate, account.Name);                

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetBoxPaperFiles(String userId)
    {
        Account account = new Account();
        Box box = new Box();

        DataTable table = new DataTable("PagerFile");

        try
        {
            ObjectQuery query = box.CreateQuery();
           // query.SetCriteria(pfile.FirstName, Operator.NotEqual, "");
            query.SetCriteria(box.UserId, userId);
            //query.SetCriteria(box.Flag, false);

            //query.SetAssociation(new IColumn[] { pfile.UserID }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
           // query.SetAssociation(new IColumn[] { pfile.BoxId }, new IColumn[] { box.BoxId }, AssociationType.LeftJoin);

            //query.SetSelectFields(pfile.FileID, pfile.FileName, pfile.FileNumber, pfile.FirstName, pfile.LastName,
            //    pfile.CreateDate, pfile.UserID, pfile.BoxId, pfile.RequestDate, pfile.Flag, box.BoxName, box.BoxNumber,
            //    box.Warehouse, box.Section, box.Row, box.Space, box.Other, box.DestroyDate, account.Name);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetPaperFiles(String userId, string firstName, string lastName, string boxName, 
        string boxNumber, string fileName,string fileNumber)
    {
        PaperFile pfile = new PaperFile();
        Account account = new Account();
        Box box = new Box();

        DataTable table = new DataTable("PagerFile");

        try
        {
            ObjectQuery query = pfile.CreateQuery();

            query.TopN = 200;

            if (!string.IsNullOrEmpty(firstName))
            {
                query.SetCriteria(pfile.FirstName, Operator.BothSidesLike, firstName);
            }
            else
            {
                query.SetCriteria(pfile.FirstName, Operator.NotEqual, "");
            }

            if (!string.IsNullOrEmpty(lastName))
            {
                query.SetCriteria(pfile.LastName, Operator.BothSidesLike, lastName);
            }
            else
            {
                query.SetCriteria(pfile.LastName, Operator.NotEqual, "");
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                query.SetCriteria(pfile.FileName, Operator.BothSidesLike, fileName);
            }

            if (!string.IsNullOrEmpty(fileNumber))
            {
                query.SetCriteria(pfile.FileNumber, Operator.BothSidesLike, fileNumber);
            }

            if (!string.IsNullOrEmpty(boxName))
            {
                query.SetCriteria(box.BoxName, Operator.BothSidesLike, boxName);
            }

            if (!string.IsNullOrEmpty(boxNumber))
            {
                query.SetCriteria(box.BoxNumber, Operator.BothSidesLike, boxNumber);
            }

            query.SetOrderBy(pfile.RequestDate,false);
           
            query.SetAssociation(new IColumn[] { pfile.UserID }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { pfile.BoxId }, new IColumn[] { box.BoxId }, AssociationType.LeftJoin);

            query.SetSelectFields(pfile.FileID, pfile.FileName, pfile.FileNumber, pfile.FirstName, pfile.LastName,
                pfile.CreateDate, pfile.UserID, pfile.BoxId, pfile.RequestDate, pfile.Flag, box.BoxName, box.BoxNumber,
                box.Warehouse, box.Section, box.Row, box.Space, box.Other,box.DestroyDate, account.Name);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }


    public static DataTable GetBoxPaperFiles(String userId, string firstName, string lastName, string boxName,
        string fileName)
    {
        PaperFile pfile = new PaperFile();
        Account account = new Account();
        Box box = new Box();

        DataTable table = new DataTable("PagerFile");

        try
        {
            ObjectQuery query = box.CreateQuery();
           
            query.TopN = 200;

            if (!string.IsNullOrEmpty(firstName))
            {
                query.SetCriteria(pfile.FirstName, Operator.BothSidesLike, firstName);
            }
            else
            {
                query.SetCriteria(pfile.FirstName, Operator.NotEqual, "");
            }

            if (!string.IsNullOrEmpty(lastName))
            {
                query.SetCriteria(pfile.LastName, Operator.BothSidesLike, lastName);
            }
            else
            {
                query.SetCriteria(pfile.LastName, Operator.NotEqual, "");
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                query.SetCriteria(pfile.FileName, Operator.BothSidesLike, fileName);
            }


            if (!string.IsNullOrEmpty(boxName))
            {
                query.SetCriteria(box.BoxName, Operator.BothSidesLike, boxName);
            }

            query.SetCriteria(box.UserId, userId);
            query.SetOrderBy(pfile.RequestDate, false);
            

            query.SetAssociation(new IColumn[] { box.UserId }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { box.BoxId }, new IColumn[] { pfile.BoxId }, AssociationType.LeftJoin);

            query.SetSelectFields(box.BoxId, box.BoxName, box.BoxNumber, box.Warehouse, box.Section, box.Row, box.Space, box.Other, box.DestroyDate, account.Name);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetPaperFiles(String userId, string firstName, string lastName, string boxName,
        string fileName)
    {
        PaperFile pfile = new PaperFile();
        Account account = new Account();
        Box box = new Box();

        DataTable table = new DataTable("PagerFile");

        try
        {
            ObjectQuery query = pfile.CreateQuery();

            if (!string.IsNullOrEmpty(firstName))
            {
                query.SetCriteria(pfile.FirstName, Operator.BothSidesLike, firstName);
            }
            else
            {
                query.SetCriteria(pfile.FirstName, Operator.NotEqual, "");
            }

            if (!string.IsNullOrEmpty(lastName))
            {
                query.SetCriteria(pfile.LastName, Operator.BothSidesLike, lastName);
            }
            else
            {
                query.SetCriteria(pfile.LastName, Operator.NotEqual, "");
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                query.SetCriteria(pfile.FileName, Operator.BothSidesLike, fileName);
            }

            if (!string.IsNullOrEmpty(boxName))
            {
                query.SetCriteria(box.BoxName, Operator.BothSidesLike, boxName);
            }

            query.SetCriteria(pfile.UserID, userId);
            query.SetOrderBy(pfile.RequestDate, false);

            query.SetAssociation(new IColumn[] { pfile.UserID }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { pfile.BoxId }, new IColumn[] { box.BoxId }, AssociationType.LeftJoin);

            query.SetSelectFields(pfile.FileID, pfile.FileName, pfile.FileNumber, pfile.FirstName, pfile.LastName,
                pfile.CreateDate, pfile.UserID, pfile.BoxId, pfile.RequestDate, pfile.Flag, box.BoxName, box.BoxNumber,
                box.Warehouse, box.Section, box.Row, box.Space, box.Other,box.DestroyDate, account.Name);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetPaperFiles(String userId, string firstName, string lastName, string boxName,
        string fileName,int boxId)
    {
        PaperFile pfile = new PaperFile();
        Account account = new Account();
        Box box = new Box();

        DataTable table = new DataTable("PagerFile");

        try
        {
            ObjectQuery query = pfile.CreateQuery();

            query.SetCriteria(box.BoxId, boxId);

            if (!string.IsNullOrEmpty(firstName))
            {
                query.SetCriteria(pfile.FirstName, Operator.BothSidesLike, firstName);
            }
            else
            {
                query.SetCriteria(pfile.FirstName, Operator.NotEqual, "");
            }

            if (!string.IsNullOrEmpty(lastName))
            {
                query.SetCriteria(pfile.LastName, Operator.BothSidesLike, lastName);
            }
            else
            {
                query.SetCriteria(pfile.LastName, Operator.NotEqual, "");
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                query.SetCriteria(pfile.FileName, Operator.BothSidesLike, fileName);
            }

            if (!string.IsNullOrEmpty(boxName))
            {
                query.SetCriteria(box.BoxName, Operator.BothSidesLike, boxName);
            }

            query.SetCriteria(pfile.UserID, userId);

            query.SetOrderBy(pfile.RequestDate, false);

            query.SetAssociation(new IColumn[] { pfile.UserID }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { pfile.BoxId }, new IColumn[] { box.BoxId }, AssociationType.LeftJoin);

            query.SetSelectFields(pfile.FileID, pfile.FileName, pfile.FileNumber, pfile.FirstName, pfile.LastName,
                pfile.CreateDate, pfile.UserID, pfile.BoxId, pfile.RequestDate, pfile.Flag, box.BoxName, box.BoxNumber,
                box.Warehouse, box.Section, box.Row, box.Space, box.Other, box.DestroyDate, account.Name);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetPaperFiles()
    {
        PaperFile pfile = new PaperFile();
        Account account = new Account();
        Box box = new Box();

        DataTable table = new DataTable("PagerFile");

        try
        {
            ObjectQuery query = pfile.CreateQuery();
            query.SetCriteria(pfile.FirstName,Operator.NotEqual,"");
          
            query.SetAssociation(new IColumn[] { pfile.UserID }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { pfile.BoxId }, new IColumn[] { box.BoxId }, AssociationType.LeftJoin);
            query.SetOrderBy(pfile.RequestDate,false);

            query.SetSelectFields(pfile.FileID, pfile.FileName, pfile.FileNumber, pfile.FirstName, pfile.LastName,
                pfile.CreateDate, pfile.UserID, pfile.BoxId, pfile.RequestDate, pfile.Flag, pfile.Status,box.BoxName, box.BoxNumber,
                box.Warehouse, box.Section, box.Row, box.Space, box.Other, account.Name);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetRequestPaperFiles()
    {
        PaperFile pfile = new PaperFile();
        Account account = new Account();
        Box box = new Box();

        DataTable table = new DataTable("PagerFile");

        try
        {
            ObjectQuery query = pfile.CreateQuery();
            query.SetCriteria(pfile.FirstName, Operator.NotEqual, "");
            query.SetCriteria(pfile.Flag, true);

            query.SetAssociation(new IColumn[] { pfile.UserID }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { pfile.BoxId }, new IColumn[] { box.BoxId }, AssociationType.LeftJoin);
            query.SetOrderBy(pfile.RequestDate, false);

            query.SetSelectFields(pfile.FileID, pfile.FileName, pfile.FileNumber, pfile.FirstName, pfile.LastName,
                pfile.CreateDate, pfile.UserID, pfile.BoxId, pfile.RequestDate, pfile.Flag, box.BoxName, box.BoxNumber,
                box.Warehouse, box.Section, box.Row, box.Space, box.Other, account.Name);

            query.SetOrderBy(pfile.RequestDate,false);
            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }


    public static DataTable GetUploadFile(string firstName,string lastName,string uid)
    {
        UploadFile ufile = new UploadFile();
        DataTable table = new DataTable("UploadFile");

        try
        {
            ObjectQuery query = ufile.CreateQuery();
            query.SetCriteria(ufile.UserID, uid);
            query.SetCriteria(ufile.FirstName, firstName);
            query.SetCriteria(ufile.LastName, lastName);

            query.SetOrderBy(ufile.FileID,false);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetFileForms(String key)
    {
        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchUploadFiles] '%" + key + "%'";
           
            SqlHelper sql = new SqlHelper("Default");
            
            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }


    public static bool UpdateFileFolder(FileFolder folder)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            folder.Update(transaction);
            transaction.Commit();

            return true;
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }

    }
  

    public static DataTable GetReminders(string uid)
    {
        //Reminder reminder = new Reminder();
        FileFolder folder = new FileFolder();

        ObjectQuery query = folder.CreateQuery();

        query.SetCriteria(folder.OfficeID1, uid);
        query.SetOrderBy(folder.FolderID, false);
       // query.SetAssociation(new IColumn[] { reminder.EffID }, new IColumn[] { folder.FolderID }, AssociationType.LeftJoin);
        query.TopN = 10;
        query.SetSelectFields(folder.FolderID, folder.FolderName, folder.FirstName, folder.Lastname, folder.DOB, folder.OfficeID);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static int GetNumEFF(string uid)
    { 
        int numEFF ;
        string SQL = "select count(1)  from FileFolder where FileFolder.OfficeID1 = '";
        SQL += uid;
        SQL += "'";
        SqlHelper sqlH = new SqlHelper(ConnectionName);
        numEFF = Convert.ToInt16(sqlH.ExecuteScalar(SQL));
        return numEFF;
    }

    public static int GetUsedEFFNum(string uid)
    {
        int usedEFFNum;
        string SQL = "select count(1)  from FileFolder where FileFolder.OfficeID1 = '";
        SQL += uid;
        SQL += "'";
        //SQL += "' and OtherInfo2 > 0";
        SqlHelper sqlH = new SqlHelper(ConnectionName);
        usedEFFNum = Convert.ToInt16(sqlH.ExecuteScalar(SQL));
        return usedEFFNum;
    }

    public static int GetAllUsedEFFNum(string uid)
    {
        int allUsedEFFNum;
        string SQL = "select count(1)  from FileFolder where FileFolder.OfficeID1 = '";
        SQL += uid;
        SQL += "'";
        //SQL += "' and OtherInfo2 > 0";
        SqlHelper sqlH = new SqlHelper(ConnectionName);
        allUsedEFFNum = Convert.ToInt16(sqlH.ExecuteScalar(SQL));
        return allUsedEFFNum;
    }

    public static int GetUnUsedEFFNum(string uid)
    {
        int unUsedEFFNum;
        string SQL = "select count(1)  from FileFolder where FileFolder.OfficeID1 = '";
        SQL += uid;
        SQL += "' and OtherInfo2 <= 0";
        SqlHelper sqlH = new SqlHelper(ConnectionName);
        unUsedEFFNum = Convert.ToInt16(sqlH.ExecuteScalar(SQL));
        return unUsedEFFNum;
    }

    public static double GetUsedEFFSizes(string uid)
    {
        double sizes = 0.0d;
        string SQL = "SELECT SUM(OtherInfo2)  FROM FileFolder WHERE FileFolder.OfficeID1 = '" + uid + "'";

        try
        {
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sizes = Convert.ToDouble(sqlH.ExecuteScalar(SQL));
            return sizes;
        }
        catch (Exception ex)
        {
            return sizes;
        }
    }

    public static void UpdateReminder(Reminder item)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            item.Update(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }

    }

    public static void CreatReminder(string folderID,string uid,DateTime date)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();

        Reminder item = new Reminder();
        item.UID.Value = uid;
        item.EffID.Value = Convert.ToInt32(folderID);
        item.DOB.Value = date;

        ObjectQuery query = item.CreateQuery();
        query.SetCriteria(item.EffID, folderID);
        query.SetCriteria(item.UID, uid);
        query.SetSelectFields(item.ReminderID);

        DataTable table = new DataTable();
        int count=0;
        try
        {
            query.Fill(table);
            
            count = table.Rows.Count;
            if(count>0)
            {
                item.ReminderID.Value = (int)table.Rows[0][0];
                item.Load(connection);
                item.DOB.Value = date;
            }
        }
        finally
        {
            table.Dispose();
            table = null;
        }
        
        
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            if (count > 0)
            {
                item.Update(transaction);
            }
            else
            {
                item.Create(transaction);
            }
            
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
        }
    }

    public static bool DeleteDivider(int dividerID)
    {
        Divider objDivider = new Divider(dividerID);
        if (objDivider.IsExist)
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            IDbTransaction transaction = connection.BeginTransaction();
            try
            {
                int effid = objDivider.EffID.Value;
                objDivider.Delete(transaction);
                FileFolder eff = new FileFolder(effid);
                eff.OtherInfo.Value--;
                eff.Update(transaction);
                transaction.Commit();

                return true;
            }
            catch
            {
                transaction.Rollback();
            }
        }

        return false;
    }

    public static int InsertDivider(Divider item)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();

        try
        {
            item.Create(transaction);
            FileFolder eff = new FileFolder(item.EffID.Value);
            eff.OtherInfo.Value++;
            eff.Update(transaction);

            transaction.Commit();

            return item.DividerID.Value;
        }
        catch(Exception ex)
        {
            //Console.Write(ex.Message);
            transaction.Rollback();

            return -1;
        }
    }

    public static bool UpdateDivider(Divider item)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
      
        try
        {
            item.Update(connection);
          
            return true;
        }
        catch (Exception ex)
        {
            
            return false;
        }
    }

    public static DataTable GetDividers(int effid)
    {
        Divider divider = new Divider();
        ObjectQuery query = divider.CreateQuery();

        query.SetCriteria(divider.EffID, effid);
      
        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static int GetDividersWithName(string dividerName, int effid)
    {
        Divider divider = new Divider();
        ObjectQuery query = divider.CreateQuery();

        query.SetCriteria(divider.EffID, effid);
        query.SetCriteria(divider.Name, dividerName);
        query.SetSelectFields(divider.DividerID);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            if (table.Rows.Count <= 0)
            {
                return -1;
            }

            return Convert.ToInt32(table.Rows[0][0]);
        }
        catch (Exception ex)
        {
            return -1;
        }
    }

    public static DataTable GetDividerIDByFolderID(string strFolderID)
    {
        Divider divider = new Divider();
        DataTable dt = new DataTable();
        try
        {
            ObjectQuery query = divider.CreateQuery();
            query.SetCriteria(divider.EffID, strFolderID);
            query.Fill(dt);
        }
        catch (Exception ex)
        {

            new ApplicationException(ex.Message);
        }
        return dt;
    }

    public static void DelQuickNoteByDivierID(string DivierID)
    {
        try
        {
            string SQL = "Delete from QuickNote where DividerID = '";
            SQL += DivierID;
            SQL += "'";
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);
        }catch{}
        
    }

    public static void DelNewNoteByDivierID(string DivierID)
    {
        try
        {
            string SQL = "Delete from NewNote where DividerID = '";
            SQL += DivierID;
            SQL += "'";
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);
        }catch{}
        
    }

    public static bool DelDocumentByDocumentID(string DocumentID)
    {
        try
        {
            string SQL = "Delete from Document where DocumentID = '";
            SQL += DocumentID;
            SQL += "'";
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);

            return true;
        }catch{}
        {
            return false;
        }
        
    }

    public static int GetFileIdByGUID(string guid)
    {
        try
        {
            string SQL = "SELECT DocumentID from Document where GUID = '";
            SQL += guid;
            SQL += "'";
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            return (int)sqlH.ExecuteScalar(SQL);
        }
        catch
        {
            return -1;
        }
    }

    public static DataTable GetPageInforByDividerID(string strDividerID)
    {
        Shinetech.DAL.Page page = new Shinetech.DAL.Page();
        DataTable dt = new DataTable();
        try
        {
            ObjectQuery query = page.CreateQuery();
            query.SetCriteria(page.DividerID, strDividerID);
            query.Fill(dt);
        }
        catch (Exception ex)
        {

            new ApplicationException(ex.Message);
        }
        return dt;
    }

    public static void DelPathByDividerID(string strDividerID)
    {
        try
        {
            string SQL = "Delete from Page where DividerID = '";
            SQL += strDividerID;
            SQL += "'";
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);
        }
        catch
        {}

    }

    public static bool DelFileFolderByFolderID(string strFolderID)
    {
        try
        {
            string SQL = "Delete from FileFolder where FolderID = '";
            SQL += strFolderID;
            SQL += "'";
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);

            return true;
        }catch
        {
            return false;
        }
        
    }

    public static bool RestoreFolderByID(string strFolderID)
    {
        try
        {
            string SQL = "UPDATE FileFolder SET OfficeID = SavedID,  OfficeID1=SavedID1, RestoreDate='" + DateTime.Now.ToString() + "' where FolderID = ";
            SQL += strFolderID;
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }

    }

    public static bool RecycleFolderByID(string strFolderID)
    {
        try
        {
            string SQL = "UPDATE FileFolder SET SavedID=OfficeID,  SavedID1=OfficeID1,  OfficeID='00000000-0000-0000-0000-000000000000', OfficeID1='00000000-0000-0000-0000-000000000000', DeleteDate='" + DateTime.Now.ToString() + "' where FolderID = ";
            SQL += strFolderID;           
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);

            return true;
        }
        catch(Exception ex)
        {
            return false;
        }

    }


    public static DataTable GetSharedEFFs(string userId)
    {
        EFFolder efolder = new EFFolder();
        FileFolder folder = new FileFolder();


        DataTable dt = new DataTable();
        try
        {
            ObjectQuery query = efolder.CreateQuery();
            query.SetCriteria(efolder.UserID, userId);

            query.SetOrderBy(efolder.CreateDate, false);
            query.SetAssociation(new IColumn[] { efolder.FolderID }, new IColumn[] { folder.FolderID }, AssociationType.LeftJoin);
            query.SetSelectFields(efolder.EFFID, efolder.FolderID,folder.FolderName, folder.FirstName, folder.Lastname, folder.DOB,
                folder.OfficeID, folder.OtherInfo, folder.OtherInfo2, folder.SecurityLevel);


            query.Fill(dt);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return dt;
    }

    public static void RemoveEFFolder(int Id)
    {
        EFFolder efolder = new EFFolder(Id);
        if (efolder.IsExist)
        {
            efolder.Delete();
        }
    }

    public static DataTable GetFolderInforByEFFID(string strFolderID)
    {
        FileFolder folder = new FileFolder();
        DataTable dtFolder = new DataTable();

        try
        {
            ObjectQuery query = folder.CreateQuery();
            query.SetCriteria(folder.FolderID, strFolderID);
            query.Fill(dtFolder);
        }
        catch (Exception ex)
        {

            new ApplicationException(ex.Message);
        }
        return dtFolder;
    }

    public static int GetUsedFoldersNumber(string uid)
    {
        string SQL = "SELECT COUNT(FolderID) AS CNT FROM FileFolder WHERE OfficeID1 = '";
        SQL += uid;
        SQL += "'";

        try
        {
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            int count = (int)sqlH.ExecuteScalar(SQL) ;

            return count;
        }
        catch(Exception ex)
        {
            throw new ApplicationException(ex.Message);
        }

    }

    public static DataTable GetDocumentByUID(string uid)
    {
        Document document = new Document();
        FileFolder folder = new FileFolder();
        Divider divider = new Divider();

        ObjectQuery query = document.CreateQuery();

        query.SetCriteria(document.UID,uid);
        query.SetCriteria(document.DocumentID, Operator.MoreThan, 0); //用于过滤删除的Document
        query.SetOrderBy(document.DOB, false);
        query.SetAssociation(new IColumn[] { document.FileFolderID}, new IColumn[] { folder.FolderID }, AssociationType.LeftJoin);
        query.SetAssociation(new IColumn[] { document.DividerID }, new IColumn[] { divider.DividerID}, AssociationType.LeftJoin);
        query.SetSelectFields(document.DocumentID);
        query.SetSelectFields(document.Name,"DocumentName");
        query.SetSelectFields(folder.FolderName);
        query.SetSelectFields(document.DOB, "CreateDate");
        query.SetSelectFields(divider.Name,"TabName");
        query.SetSelectFields(document.PathName);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static DataTable GetDocumentByFolderId(string folderId)
    {
        Document document = new Document();       

        ObjectQuery query = document.CreateQuery();

        query.SetCriteria(document.FileFolderID, folderId);
        query.SetOrderBy(document.DOB, false);        

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static DataTable GetDocumentByUID(string uid,string fileName,string foldeName)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
       
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_FindDocumentsByFields"; 
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@UID", uid);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@DocumentName", "%" + fileName + "%");
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlParameter param3 = command.Parameters.AddWithValue("@FolderName", "%" + foldeName + "%");
            param3.Direction = ParameterDirection.Input;
            param3.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetSubDocumentByUID(string uid, string fileName, string foldeName)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_FindSubDocumentsByFields";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@UID", uid);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@DocumentName", "%" + fileName + "%");
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlParameter param3 = command.Parameters.AddWithValue("@FolderName", "%" + foldeName + "%");
            param3.Direction = ParameterDirection.Input;
            param3.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetDocumentByUID(string uid, string folderId)
    {
        Document document = new Document();
        FileFolder folder = new FileFolder();
        Divider divider = new Divider();

        ObjectQuery query = document.CreateQuery();

        query.SetCriteria(document.UID, uid);
        query.SetCriteria(document.FileFolderID, folderId);
        query.SetCriteria(document.DocumentID,Operator.MoreThan,0); //用于过滤删除的Document
        query.SetOrderBy(document.DOB, false);
        query.SetAssociation(new IColumn[] { document.FileFolderID }, new IColumn[] { folder.FolderID }, AssociationType.LeftJoin);
        query.SetAssociation(new IColumn[] { document.DividerID }, new IColumn[] { divider.DividerID }, AssociationType.LeftJoin);
        query.SetSelectFields(document.DocumentID);
        query.SetSelectFields(document.Name, "DocumentName");
        query.SetSelectFields(folder.FolderName);
        query.SetSelectFields(document.DOB, "CreateDate");
        query.SetSelectFields(divider.Name, "TabName");
        query.SetSelectFields(document.PathName);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }


    public static void DelPageByDocumentID(string DocumentID)
    {
        try
        {
            string SQL = "Delete from Page where DocumentID = '";
            SQL += DocumentID;
            SQL += "'";

            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);
        }catch{}
        
    }

    public static DataTable GetDocumentByDocumentID(string DID)
    {
        Shinetech.DAL.Page page = new Shinetech.DAL.Page();

        ObjectQuery query = page.CreateQuery();

        query.SetCriteria(page.DocumentID, DID);
        query.SetSelectFields(page.PageID,page.ImagePath,page.ImagePath2,page.ImagePath3,page.ImagePath4);


        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static void DelQucikNoteByPageID(string strPageID)
    {
        try
        {
            string SQL = "Delete from quicknote where pageID = '";
            SQL += strPageID;
            SQL += "'";
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);
        }catch{}
        
    }

    public static DataTable GetPathNameByFolderID(string FolderID)
    {
        Document document = new Document();

        ObjectQuery query = document.CreateQuery();

        query.SetCriteria(document.FileFolderID, FolderID);
        query.SetSelectFields(document.PathName);


        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }


    public static DataTable GetDocumentByKey(string keyname, string UID)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SelectDocument";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter param1 = command.Parameters.AddWithValue("@Key", keyname);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@UID", UID);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetDocInforByDocumentID(string DID)
    {
        Document document = new Document();

        ObjectQuery query = document.CreateQuery();


        query.SetCriteria(document.DocumentID, DID);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static DataTable GetFileFolderSizeByDocumentID(string DID)
    {
        Document document = new Document();
        FileFolder folder = new FileFolder();

        ObjectQuery query = folder.CreateQuery();


        query.SetCriteria(document.DocumentID, DID);

        query.SetAssociation(new IColumn[] { folder.FolderID }, new IColumn[] { document.FileFolderID }, AssociationType.LeftJoin);
        query.SetSelectFields(folder.OtherInfo2,folder.FolderID);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static void UpdateFolderSize(string folderID, Double size)
    {
        string SQL = "UPDATE filefolder SET OtherInfo2 = '";
        SQL += size;
        SQL += "' ";
        SQL += "WHERE FolderID = '";
        SQL += folderID;
        SQL += "'";

        SqlHelper sqlH = new SqlHelper(ConnectionName);
        sqlH.ExecuteNonQuery(SQL);
        
       
    }

    public static DataTable GetAnalysticTrackers(DateTime date)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_AnalystVisitors";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter param1 = command.Parameters.AddWithValue("@date", date);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.DateTime;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetGiftNumbers(DateTime sdate, DateTime edate)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_GiftNumbers";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter param1 = command.Parameters.AddWithValue("@sdate", sdate);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.DateTime;

            SqlParameter param2 = command.Parameters.AddWithValue("@edate", edate);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.DateTime;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetTemplates(string uid)
    {
        FolderTemplate template = new FolderTemplate();

        ObjectQuery query = template.CreateQuery();
        query.SetCriteria(template.UID, uid);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
        }catch{}

        return table;
    }

    public static int SaveFolderTemplate(TemplateEntity template)
    {
        FolderTemplate ft = new FolderTemplate();
        ft.Name.Value = template.Name;
        ft.Content.Value = template.Content;
        ft.UID.Value = template.OfficeID;
      
        try
        {
            ft.Create();

            return ft.TemplateID.Value;
        }
        catch { }

        return -1;
    }

    public static void UpdateFolderTemplate(int tid, string content)
    {
        FolderTemplate ft = new FolderTemplate(tid);

        if (ft.IsExist)
        {
            try
            {
                ft.Content.Value = content;
                ft.Update();
            }
            catch { }
        }
        
    }

    public static DataTable GetOfficeSignatures(string name, string sdate, string edate, string officeid)
    {
        string whereSqlS = string.Empty, whereSqlE = string.Empty;
        if (!string.IsNullOrEmpty(sdate))
            whereSqlS += "a.SignInDate>='" + sdate + "' ";

        if (!string.IsNullOrEmpty(edate)) 
            whereSqlE += "a.SignInDate<='" + edate + "' ";
        
        string sqlstr =
               "SELECT TOP 100 a.*,DATEDIFF(minute,a.SignInDate,GetDate()) as WaitTime FROM dbo.Signature a " +
               "WHERE a.DoctorID='" + officeid + "'" +
               " AND a.PrintName LIKE '%"+name+"%' " ;

        if (!string.IsNullOrEmpty(whereSqlS))
        {
            sqlstr += " AND " + whereSqlS;
        }

        if (!string.IsNullOrEmpty(whereSqlE))
        {
            sqlstr += " AND " + whereSqlE;
        }

        sqlstr += "ORDER BY a.CreateDate DESC";

        SqlHelper sql = new SqlHelper("Default");
        DataTable table = new DataTable();
        try
        {
            sql.Fill(table, sqlstr);

        }
        catch (Exception ex)
        {

        }

        return table;

        /*Signature signin = new Signature();
        ObjectQuery query = signin.CreateQuery();

        query.SetCriteria(signin.DoctorID, officeid);
        query.SetOrderBy(signin.CreateDate, false);

        if (!string.IsNullOrEmpty(name))
            query.SetCriteria(signin.PrintName, Operator.BothSidesLike, name);

        if (!string.IsNullOrEmpty(sdate))
            query.SetCriteria(signin.SignInDate, Operator.MoreEqualThan, sdate);

        if (!string.IsNullOrEmpty(edate))

            query.SetCriteria(signin.SignInDate, Operator.LessEqualThan, edate);
        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }*/
    }

    public static DataTable Get50OfficeSignatures(string officeId)
    {
        string sqlstr =
               "SELECT TOP 100 a.*,DATEDIFF(second,a.SignInDate,a.CheckInDate) as WaitTime FROM dbo.Signature a " +
               "WHERE a.DoctorID='" + officeId + "'ORDER BY a.CreateDate DESC";
        SqlHelper sql = new SqlHelper("Default");
        DataTable table = new DataTable();
        try
        {
            sql.Fill(table, sqlstr);
            
        }
        catch (Exception ex)
        {
           
        }

        return table;

        /*Signature signin = new Signature();
        ObjectQuery query = signin.CreateQuery();

        query.SetCriteria(signin.DoctorID, officeId);
        query.SetOrderBy(signin.CreateDate,false);
        
        query.TopN = 50;

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }*/
    }

    public static DataTable GetRestoredPaperFiles()
    {
        Document document = new Document();
        FileFolder folder = new FileFolder();
        Account user = new Account();
        DataTable table = new DataTable();
        try
        {
            ObjectQuery query = document.CreateQuery();
            query.SetAssociation(new IColumn[] { document.UID1 }, new IColumn[] { user.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { document.SavedID1 }, new IColumn[] { folder.FolderID}, AssociationType.LeftJoin);
            query.SetCriteria(document.DividerID, Operator.Equal,0);
            query.SetCriteria(document.FileFolderID, Operator.Equal, 0);

            query.SetSelectFields(document.DocumentID, document.Name, folder.FolderName, document.PathName, folder.FolderID, document.Size, document.DeleteDate,
                user.Firstname, user.Lastname);
            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static bool RestoreDocumentByID(string documentId)
    {
        try
        {
            string SQL = "UPDATE Document SET DividerID = SavedID, UID=UID1,  FileFolderID=SavedID1, RestoreDate='" + DateTime.Now.ToString() + "' where DocumentID = ";
            SQL += documentId;
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
        
    }

    public static void ClearFolderValidCode(int p)
    {
        try
        {
            string SQL = "DELETE ValidateCode WHERE EFFID =" + p.ToString();
            
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);
        }
        catch (Exception ex)
        {
        
        }
    }

    public static int GetFileWithName(string dividerName, int dividerId, int fodlerId)
    {
        Document doc = new Document();
        ObjectQuery query = doc.CreateQuery();

        query.SetCriteria(doc.FileFolderID, fodlerId);
        query.SetCriteria(doc.DividerID, dividerId);
        string[] dividers = dividerName.Split(new string[] { ";", " " }, StringSplitOptions.RemoveEmptyEntries);
        if (dividers.Length > 1)
        {
            dividerName = dividers[0];
        }

        query.SetCriteria(doc.Name,Operator.SuffixLike, dividerName);
        query.SetSelectFields(doc.DocumentID);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            if (table.Rows.Count <= 0)
            {
                return -1;
            }

            return Convert.ToInt32(table.Rows[0][0]);
        }
        catch (Exception ex)
        {
            return -1;
        }
    }
}