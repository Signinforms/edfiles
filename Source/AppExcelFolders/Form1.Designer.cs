﻿namespace AppConvertFolders
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxUsers = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxTemplates = new System.Windows.Forms.ComboBox();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.linkLabelFinish = new System.Windows.Forms.LinkLabel();
            this.linkLabelLeft = new System.Windows.Forms.LinkLabel();
            this.linkLabelTotal = new System.Windows.Forms.LinkLabel();
            this.buttonStart = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.labelST = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelET = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxFilePath = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.buttonPath = new System.Windows.Forms.Button();
            this.checkBoxIsFirstColume = new System.Windows.Forms.CheckBox();
            this.labelDividers = new System.Windows.Forms.Label();
            this.buttonCreateNow = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(15, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "User Name:";
            // 
            // comboBoxUsers
            // 
            this.comboBoxUsers.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxUsers.FormattingEnabled = true;
            this.comboBoxUsers.Location = new System.Drawing.Point(122, 30);
            this.comboBoxUsers.Name = "comboBoxUsers";
            this.comboBoxUsers.Size = new System.Drawing.Size(209, 20);
            this.comboBoxUsers.TabIndex = 1;
            this.comboBoxUsers.SelectedIndexChanged += new System.EventHandler(this.comboBoxUsers_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(15, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Templates:";
            // 
            // comboBoxTemplates
            // 
            this.comboBoxTemplates.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxTemplates.FormattingEnabled = true;
            this.comboBoxTemplates.Location = new System.Drawing.Point(122, 74);
            this.comboBoxTemplates.Name = "comboBoxTemplates";
            this.comboBoxTemplates.Size = new System.Drawing.Size(209, 20);
            this.comboBoxTemplates.TabIndex = 3;
            this.comboBoxTemplates.SelectedIndexChanged += new System.EventHandler(this.comboBoxTemplates_SelectedIndexChanged);
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.Location = new System.Drawing.Point(18, 196);
            this.textBoxMessage.Multiline = true;
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.Size = new System.Drawing.Size(651, 234);
            this.textBoxMessage.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(15, 161);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "Total:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(201, 161);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Finish:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(390, 161);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 16);
            this.label5.TabIndex = 7;
            this.label5.Text = "Left:";
            // 
            // linkLabelFinish
            // 
            this.linkLabelFinish.AutoSize = true;
            this.linkLabelFinish.Location = new System.Drawing.Point(304, 165);
            this.linkLabelFinish.Name = "linkLabelFinish";
            this.linkLabelFinish.Size = new System.Drawing.Size(11, 12);
            this.linkLabelFinish.TabIndex = 8;
            this.linkLabelFinish.TabStop = true;
            this.linkLabelFinish.Text = "0";
            this.linkLabelFinish.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelFinish_LinkClicked);
            // 
            // linkLabelLeft
            // 
            this.linkLabelLeft.AutoSize = true;
            this.linkLabelLeft.Location = new System.Drawing.Point(466, 165);
            this.linkLabelLeft.Name = "linkLabelLeft";
            this.linkLabelLeft.Size = new System.Drawing.Size(11, 12);
            this.linkLabelLeft.TabIndex = 9;
            this.linkLabelLeft.TabStop = true;
            this.linkLabelLeft.Text = "0";
            // 
            // linkLabelTotal
            // 
            this.linkLabelTotal.AutoSize = true;
            this.linkLabelTotal.Location = new System.Drawing.Point(92, 165);
            this.linkLabelTotal.Name = "linkLabelTotal";
            this.linkLabelTotal.Size = new System.Drawing.Size(11, 12);
            this.linkLabelTotal.TabIndex = 10;
            this.linkLabelTotal.TabStop = true;
            this.linkLabelTotal.Text = "0";
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(366, 9);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(99, 40);
            this.buttonStart.TabIndex = 11;
            this.buttonStart.Tag = "0";
            this.buttonStart.Text = "Convert Now";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.imageField_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label6.Location = new System.Drawing.Point(560, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(109, 19);
            this.label6.TabIndex = 12;
            this.label6.Text = "StartTime:";
            // 
            // labelST
            // 
            this.labelST.AutoSize = true;
            this.labelST.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelST.Location = new System.Drawing.Point(523, 39);
            this.labelST.Name = "labelST";
            this.labelST.Size = new System.Drawing.Size(49, 19);
            this.labelST.TabIndex = 13;
            this.labelST.Text = "    ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label7.Location = new System.Drawing.Point(560, 71);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(89, 19);
            this.label7.TabIndex = 14;
            this.label7.Text = "EndTime:";
            // 
            // labelET
            // 
            this.labelET.AutoSize = true;
            this.labelET.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labelET.Location = new System.Drawing.Point(515, 108);
            this.labelET.Name = "labelET";
            this.labelET.Size = new System.Drawing.Size(39, 19);
            this.labelET.TabIndex = 15;
            this.labelET.Text = "   ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label8.Location = new System.Drawing.Point(15, 124);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 16);
            this.label8.TabIndex = 16;
            this.label8.Text = "Excel Sheet:";
            // 
            // textBoxFilePath
            // 
            this.textBoxFilePath.Location = new System.Drawing.Point(122, 119);
            this.textBoxFilePath.Name = "textBoxFilePath";
            this.textBoxFilePath.Size = new System.Drawing.Size(245, 21);
            this.textBoxFilePath.TabIndex = 17;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "Excel Sheet(*.xlsx)|*.xlsx";
            // 
            // buttonPath
            // 
            this.buttonPath.Location = new System.Drawing.Point(403, 117);
            this.buttonPath.Name = "buttonPath";
            this.buttonPath.Size = new System.Drawing.Size(62, 23);
            this.buttonPath.TabIndex = 18;
            this.buttonPath.Text = "Open..";
            this.buttonPath.UseVisualStyleBackColor = true;
            this.buttonPath.Click += new System.EventHandler(this.buttonPath_Click);
            // 
            // checkBoxIsFirstColume
            // 
            this.checkBoxIsFirstColume.AutoSize = true;
            this.checkBoxIsFirstColume.Checked = true;
            this.checkBoxIsFirstColume.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxIsFirstColume.Location = new System.Drawing.Point(483, 121);
            this.checkBoxIsFirstColume.Name = "checkBoxIsFirstColume";
            this.checkBoxIsFirstColume.Size = new System.Drawing.Size(186, 16);
            this.checkBoxIsFirstColume.TabIndex = 19;
            this.checkBoxIsFirstColume.Text = "Is First Row As ColumnNames";
            this.checkBoxIsFirstColume.UseVisualStyleBackColor = true;
            // 
            // labelDividers
            // 
            this.labelDividers.Location = new System.Drawing.Point(353, 71);
            this.labelDividers.Name = "labelDividers";
            this.labelDividers.Size = new System.Drawing.Size(201, 37);
            this.labelDividers.TabIndex = 20;
            this.labelDividers.Text = "label9";
            // 
            // buttonCreateNow
            // 
            this.buttonCreateNow.Location = new System.Drawing.Point(496, 143);
            this.buttonCreateNow.Name = "buttonCreateNow";
            this.buttonCreateNow.Size = new System.Drawing.Size(99, 40);
            this.buttonCreateNow.TabIndex = 21;
            this.buttonCreateNow.Tag = "0";
            this.buttonCreateNow.Text = "Create Now";
            this.buttonCreateNow.UseVisualStyleBackColor = true;
            this.buttonCreateNow.Click += new System.EventHandler(this.buttonCreateNow_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(603, 143);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 40);
            this.button1.TabIndex = 22;
            this.button1.Tag = "0";
            this.button1.Text = "Create Divider";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 467);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.buttonCreateNow);
            this.Controls.Add(this.labelDividers);
            this.Controls.Add(this.checkBoxIsFirstColume);
            this.Controls.Add(this.buttonPath);
            this.Controls.Add(this.textBoxFilePath);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.labelET);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.labelST);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.linkLabelTotal);
            this.Controls.Add(this.linkLabelLeft);
            this.Controls.Add(this.linkLabelFinish);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxMessage);
            this.Controls.Add(this.comboBoxTemplates);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxUsers);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxUsers;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxTemplates;
        private System.Windows.Forms.TextBox textBoxMessage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.LinkLabel linkLabelFinish;
        private System.Windows.Forms.LinkLabel linkLabelLeft;
        private System.Windows.Forms.LinkLabel linkLabelTotal;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelST;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label labelET;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox textBoxFilePath;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button buttonPath;
        private System.Windows.Forms.CheckBox checkBoxIsFirstColume;
        private System.Windows.Forms.Label labelDividers;
        private System.Windows.Forms.Button buttonCreateNow;
        private System.Windows.Forms.Button button1;
    }
}

