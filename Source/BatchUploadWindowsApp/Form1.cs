﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BatchUploadWindowsApp.ServiceReference1;
using System.IO;
using System.Web;
using Newtonsoft.Json;
using System.Configuration;

namespace BatchUploadWindowsApp
{
    public partial class Form1 : Form
    {

        public static string RootPath
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["RootPath"]);
            }
        }

        public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
        public static string BasePath = System.Configuration.ConfigurationManager.AppSettings["BasePath"];
        public string newFileName = string.Empty;

        public enum Action
        {
            FolderCreated,
            TabCreated,
            FileUploaded,
            UploadError,
            FolderProcessFinished,
            FileDeleted,
            TabDeleted,
            FolderDeleted,
            TabDeleteError,
            FolderDeleteError,
            FileDeleteError,
            UnknownException
        }

        public enum FileAction
        {
            Create = 1,
            Delete = 2,
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressBar1.Minimum = 1;
            progressBar1.Value = 1;
            progressBar1.Step = 1;
            Start();
            System.Windows.Forms.Application.Exit();
        }

        private void FindOrCreateTab(string[] tabFolders, List<FolderStructure> tabsDetails, string folderName, string uid, ServiceReference1.EFileFolderJSONWSSoapClient obj, int folderId)
        {
             int tabId = 0;
                string color = null;
            foreach (string tab in tabFolders)
            {
                if (progressBar1.Value >= 65536)
                    progressBar1.Value = 0;
                bool isTabFound = false;
                foreach (FolderStructure tabRow in tabsDetails)
                {
                    folderId = Convert.ToInt32(tabRow.FolderID);
                    //Check if tab exists in folder in database
                    if (string.Compare(tab.Substring(tab.LastIndexOf('\\') + 1).Split('#')[0].Trim(), tabRow.Name.ToString(), true) == 0)
                    {
                        isTabFound = true;
                        tabId = Convert.ToInt32(tabRow.DividerID);
                        break;
                    }
                }
                //Upload files if tab exists
                if (isTabFound && folderId > 0)
                {
                    //Upload files
                    string[] files = Directory.GetFiles(tab);
                    CopyFile(files, tabId, folderId, folderName.Substring(folderName.LastIndexOf('\\') + 1), uid);
                    if (Directory.GetFiles(tab).Length <= 0)
                    {
                        try
                        {
                            Directory.Delete(tab);
                            WriteWindowServiceLog(folderId, tabId, uid, Action.TabDeleted.ToString(), 0, tab);
                        }
                        catch (Exception ex)
                        {
                            WriteWindowServiceLog(folderId, tabId, uid, Action.TabDeleteError.ToString(), 0, tab, ex.Message);
                        }
                    }
                }
                else
                {
                    //Create tab
                    if (tab.Substring(tab.LastIndexOf('\\') + 1).Split('#').Length > 1)
                    {
                        color = '#' + tab.Substring(tab.LastIndexOf('\\') + 1).Split('#')[1].ToUpper();
                    }
                    else
                    {
                        color = "#FFCCFF";
                    }
                    if (folderId > 0)
                    {
                        tabId = Convert.ToInt32(obj.CreateDivider(tab.Substring(tab.LastIndexOf('\\') + 1).Split('#')[0].Trim(), uid, folderId, color));
                        WriteWindowServiceLog(folderId, tabId, uid, Action.TabCreated.ToString());
                        //Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + CurrentVolume + "/" + folderId + "/" + tabId));
                        progressBar1.Value += 3;
                        //Get files from folder
                        string[] files = Directory.GetFiles(tab);
                        //Upload files                                         
                        CopyFile(files, tabId, folderId, folderName.Substring(folderName.LastIndexOf('\\') + 1), uid);
                        if (Directory.GetFiles(tab).Length <= 0)
                        {
                            try
                            {
                                Directory.Delete(tab);
                                WriteWindowServiceLog(folderId, tabId, uid, Action.TabDeleted.ToString(), 0, tab);
                            }
                            catch (Exception ex)
                            {
                                WriteWindowServiceLog(folderId, tabId, uid, Action.TabDeleteError.ToString(), 0, tab, ex.Message);
                            }
                        }
                    }
                }
            }
        }

        private void Start()
        {
            try
            {
                progressBar1.Value += 3;
                //Create Sync folder if does not exist
                if (!Directory.Exists(RootPath))
                {
                    Directory.CreateDirectory(RootPath);
                }

                //Check if Sync folder exists
                if (Directory.Exists(RootPath))
                {
                    progressBar1.Value += 3;
                    //Get all UserName folders from Sync folder
                    string[] userFolders = Directory.GetDirectories(RootPath);
                    ServiceReference1.EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();

                    foreach (string userName in userFolders)
                    {
                        if (progressBar1.Value >= 65536)
                            progressBar1.Value = 0;
                        progressBar1.Value += 3;
                        List<UserStructure> userDetails = JsonConvert.DeserializeObject<List<UserStructure>>(obj.GetUserIdFromUserName(userName.Substring(userName.LastIndexOf('\\') + 1)));
                        if (userDetails != null && userDetails.Count > 0)
                        {
                            //Get UID from UserName
                            string uid = userDetails[0].UID;
                            //Get all folders inside UserName folder.
                            string[] folders = Directory.GetDirectories(userName);
                            foreach (string folderName in folders)
                            {
                                if (progressBar1.Value >= 65536)
                                    progressBar1.Value = 0;
                                progressBar1.Value += 3;
                                //Check if folder exists and if exists get all tabs
                                //List<FolderStructure> tabsDetails = JsonConvert.DeserializeObject<List<FolderStructure>>(obj.CheckFolderIdExists(folderName.Substring(folderName.LastIndexOf('\\') + 1), uid));
                                List<FolderStructure> tabsDetails = JsonConvert.DeserializeObject<List<FolderStructure>>(obj.CheckFolderIdExists(folderName.Substring(folderName.LastIndexOf('\\') + 1).Split('-')[0].Trim(), uid));

                                //Get sub folders from folder (tabs)
                                string[] tabFolders = Directory.GetDirectories(folderName);
                                int tabId = 0, folderId = 0;
                                string color = null;
                                if (tabFolders.Length > 0 && tabsDetails != null && tabsDetails.Count > 0)
                                {

                                    FindOrCreateTab(tabFolders, tabsDetails, folderName, uid, obj, folderId);
                                    
                                }
                                else if (tabFolders.Length > 0)
                                {
                                    if (progressBar1.Value >= 65536)
                                        progressBar1.Value = 0;
                                    //Create Folder
                                    string folder = folderName.Substring(folderName.LastIndexOf('\\') + 1);
                                    string[] splitFolderName = folder.Split(',');
                                    string firstName = string.Empty, lastName = string.Empty, fileNumber = null, alert = null;

                                    if (splitFolderName.Length > 1)
                                    {
                                        lastName = splitFolderName[0].Trim();
                                        if (splitFolderName[1].Trim().IndexOf('-') > 0)
                                        {
                                            string[] info = splitFolderName[1].Trim().Split('-');
                                            firstName = info[0].Trim();
                                            if (!string.IsNullOrEmpty(info[1].Trim()))
                                                fileNumber = info[1].Trim();
                                            if (info.Length > 2 && !string.IsNullOrEmpty(info[2].Trim()))
                                            {
                                                if(info[2].LastIndexOf('(') >= 0)
                                                    alert = info[2].Substring(0, info[2].LastIndexOf('(')).Trim();
                                                else
                                                    alert = info[2].Trim();
                                            }
                                        }
                                        else
                                        {
                                            if (!string.IsNullOrEmpty(splitFolderName[1].Trim()))
                                                firstName = splitFolderName[1].Trim();
                                            else
                                                firstName = userName.Substring(userName.LastIndexOf('\\') + 1);
                                        }
                                    }
                                    else
                                    {
                                        firstName = lastName = userName.Substring(userName.LastIndexOf('\\') + 1);
                                    }
                                    string folderNameAsName = folderName.Split('-')[0].Substring(folderName.Split('-')[0].LastIndexOf('\\')).Split('\\')[1].Trim();
                                    string templateName = string.Empty;
                                    if (folderName.IndexOf('(') > 0)
                                    {
                                        string temp = folderName.Split('(')[1].Trim();
                                        if (temp.IndexOf(')') > 0)
                                        {
                                            templateName = temp.Substring(0, temp.LastIndexOf(')'));
                                            if(folderNameAsName.LastIndexOf('(') > 0 &&  !string.IsNullOrEmpty(folderNameAsName.Substring(0, folderNameAsName.LastIndexOf('('))))
                                                folderNameAsName = folderNameAsName.Substring(0, folderNameAsName.LastIndexOf('('));
                                        }
                                    }

                                    //folderId = Convert.ToInt32(obj.CreateFolder(folderName.Substring(folderName.LastIndexOf('\\') + 1), uid, firstName, lastName, tabFolders.Length, fileNumber, alert));
                                    DataSet dt = obj.CreateFolder(false,folderNameAsName, uid, firstName, lastName, tabFolders.Length, fileNumber, alert, templateName, "", "", "", "", "", "", "", "");
                                    folderId = Convert.ToInt32(dt.Tables[0].Rows[0]["Column1"]);

                                    WriteWindowServiceLog(folderId, tabId, uid, Action.FolderCreated.ToString());
                                    if(dt.Tables[1].Rows.Count > 0)
                                        tabsDetails = JsonConvert.DeserializeObject<List<FolderStructure>>(JsonConvert.SerializeObject(dt.Tables[1]));
                                    //Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + CurrentVolume + Path.DirectorySeparatorChar + folderId));
                                    progressBar1.Value += 3;
                                    FindOrCreateTab(tabFolders, tabsDetails, folderName, uid, obj, folderId);
                                }
                                WriteWindowServiceLog(folderId, tabId, uid, Action.FolderProcessFinished.ToString());
                                if (Directory.GetDirectories(folderName).Length <= 0 && Directory.GetFiles(folderName).Length <= 0)
                                {
                                    try
                                    {
                                        Directory.Delete(folderName);
                                        WriteWindowServiceLog(folderId, tabId, uid, Action.FolderDeleted.ToString(), 0, folderName);
                                    }
                                    catch (Exception ex)
                                    {
                                        WriteWindowServiceLog(folderId, tabId, uid, Action.FolderDeleteError.ToString(), 0, folderName, ex.Message);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteWindowServiceLog(0, 0, "", Action.UnknownException.ToString(), 0, "", ex.Message);
                System.Windows.Forms.Application.Exit();
            }
        }

        private void CopyFile(string[] files, int tabId, int folderId, string folderName, string uid)
        {
            try
            {
                ServiceReference1.EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();
                if (files.Length > 0)
                {
                    foreach (string file in files)
                    {
                        string documentID = string.Empty;
                        documentID = ProcessFile(tabId, folderId, folderName.Substring(folderName.LastIndexOf('\\') + 1), file.Substring(file.LastIndexOf('\\') + 1), file.Substring(0, file.LastIndexOf('\\')), uid);
                        if (!string.IsNullOrEmpty(documentID) && Convert.ToInt32(documentID) > 0)
                        {
                            try
                            {
                                obj.AuditLogByDocId(Convert.ToInt32(documentID), uid, FileAction.Create.GetHashCode());
                                WriteWindowServiceLog(folderId, tabId, uid, Action.FileUploaded.ToString(), Convert.ToInt32(documentID), file.Substring(file.LastIndexOf('\\') + 1));
                                try
                                {
                                    File.Delete(file);
                                    WriteWindowServiceLog(folderId, tabId, uid, Action.FileDeleted.ToString(), 0, file.Substring(file.LastIndexOf('\\') + 1));
                                }
                                catch (Exception ex)
                                {
                                    WriteWindowServiceLog(folderId, tabId, uid, Action.FileDeleteError.ToString(), 0, file.Substring(file.LastIndexOf('\\') + 1), ex.Message);
                                }
                                progressBar1.Value += 3;
                            }
                            catch (Exception ex)
                            {
                                WriteWindowServiceLog(folderId, tabId, uid, Action.UploadError.ToString(), 0, file.Substring(file.LastIndexOf('\\') + 1), ex.Message);
                            }
                        }
                        else
                        {
                            WriteWindowServiceLog(folderId, tabId, uid, Action.UploadError.ToString(), 0, file.Substring(file.LastIndexOf('\\') + 1), "Document insert error.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteWindowServiceLog(folderId, tabId, uid, Action.UploadError.ToString(),0,string.Empty, ex.Message);
            }
        }

        public string ProcessFile(int dividerIdForFile, int folderId, string folderName, string filename, string sourcePath, string uid)
        {
            try
            {
                if (!String.IsNullOrEmpty(folderName))
                {
                    if (Path.GetExtension(filename).ToLower() == ".pdf" || Path.GetExtension(filename).ToLower() == ".jpg" || Path.GetExtension(filename).ToLower() == ".jpeg")
                    {

                        string folderID = folderId.ToString();
                        string dividerID = Convert.ToString(dividerIdForFile);
                        string path = BasePath + CurrentVolume + Path.DirectorySeparatorChar +
                                    folderID + Path.DirectorySeparatorChar + dividerIdForFile;


                        string encodeName = HttpUtility.UrlPathEncode(filename);
                        encodeName = encodeName.Replace("%", "$");

                        string fileName = String.Concat(
                                path,
                                Path.DirectorySeparatorChar,
                                encodeName
                            );

                        fileName = String.Concat(path, Path.DirectorySeparatorChar, ChangeExtension(fileName));

                        fileName = GetRenameFileName(fileName);

                        newFileName = Path.GetFileName(fileName);

                        encodeName = newFileName;

                        newFileName = HttpUtility.UrlDecode(newFileName.Replace('$', '%').ToString());

                        int Offset = 0; // starting offset.
                        //define the chunk size
                        int ChunkSize = 5242880; // 10 mb
                        byte[] Buffer = new byte[ChunkSize];
                        long FileSize = new FileInfo(string.Concat(sourcePath, Path.DirectorySeparatorChar, filename)).Length;
                        string documentId = string.Empty;
                        FileStream objfilestream = new FileStream(string.Concat(sourcePath, Path.DirectorySeparatorChar, filename), FileMode.Open, FileAccess.Read);
                        try
                        {
                            bool ChunkAppened = false;
                            ServiceReference1.EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();

                            objfilestream.Position = Offset;
                            int BytesRead = 0;
                            while (Offset != FileSize) // continue uploading the file chunks until offset = file size.
                            {
                                BytesRead = objfilestream.Read(Buffer, 0, ChunkSize); // read the next chunk 
                                if (BytesRead != Buffer.Length)
                                {
                                    ChunkSize = BytesRead;
                                    byte[] TrimmedBuffer = new byte[BytesRead];
                                    Array.Copy(Buffer, TrimmedBuffer, BytesRead);
                                    Buffer = TrimmedBuffer; // the trimmed buffer should become the new 'buffer'
                                }
                                String base64String = Convert.ToBase64String(Buffer);
                                ChunkAppened = obj.ProcessFile(folderId, dividerIdForFile, folderName, uid, BasePath, base64String, newFileName, Offset);
                                if (!ChunkAppened)
                                {
                                    WriteWindowServiceLog(folderId, dividerIdForFile, uid, Action.UploadError.ToString(), 0, newFileName, "Failed to process file");
                                    break;
                                }
                                Offset += BytesRead;
                            }
                            if (ChunkAppened)
                                documentId = obj.InsertDocument(dividerIdForFile, folderId, newFileName, encodeName, uid);
                        }
                        catch (Exception ex)
                        {
                            WriteWindowServiceLog(folderId, dividerIdForFile, uid, Action.UploadError.ToString(), 0, newFileName, ex.Message);
                        }
                        finally
                        {
                            objfilestream.Close();
                        }
                        return documentId.Split('#')[0].Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                WriteWindowServiceLog(folderId, dividerIdForFile, uid, Action.UploadError.ToString(), 0, newFileName, ex.Message);
                return null;
            }
            return null;
        }

        private string ChangeExtension(string path)
        {
            string renameFileName = string.Empty;
            renameFileName = Path.GetFileNameWithoutExtension(path) + Path.GetExtension(path).ToLower().Replace(".jpeg", ".jpg");
            return renameFileName;
        }

        private string GetRenameFileName(string path)
        {
            if (File.Exists(path))
            {
                string fileName = Path.GetFileNameWithoutExtension(path);
                string fileExtension = Path.GetExtension(path);
                string fileDirectory = Path.GetDirectoryName(path);
                int i = 1;
                while (true)
                {
                    string renameFileName = fileDirectory + @"\" + fileName + "_v" + i + fileExtension;
                    if (File.Exists(renameFileName))
                        i++;
                    else
                    {
                        path = renameFileName;
                        break;
                    }
                }
            }
            return path;
        }

        public static void WriteWindowServiceLog(int folderId, int dividerId, string uid, string action, int documentId = 0, string fileName = "", string errorMsg = "")
        {
            try
            {
                if (!Directory.Exists("C:\\BatchUploadLog"))
                    Directory.CreateDirectory("C:\\BatchUploadLog");

                using (StreamWriter swData2 = new StreamWriter("C:\\BatchUploadLog\\BatchUploadLog.txt", true))
                {
                    swData2.WriteLine("===== Log " + DateTime.Now);
                    swData2.WriteLine("FolderID = " + folderId);
                    swData2.WriteLine("DividerId = " + dividerId);
                    swData2.WriteLine("fileName = " + fileName);
                    swData2.WriteLine("documentId = " + documentId);
                    swData2.WriteLine("uid = " + uid);
                    swData2.WriteLine("action = " + action);
                    swData2.WriteLine("error = " + errorMsg);
                    swData2.WriteLine("==============================================================");
                    swData2.Close();
                    swData2.Dispose();
                }
            }
            catch (Exception ex1)
            {
                string errorMessage = ex1.Message;
            }
        }
    }

    public class FolderStructure
    {
        public string FolderName { get; set; }
        public int FolderID { get; set; }
        public int DividerID { get; set; }
        public string Name { get; set; }
        public string OfficeID { get; set; }
        public int EFFID { get; set; }

    }

    public class UserStructure
    {
        public string UID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
