﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using CheckWorkareaFiles.ServiceReference1;
using System.IO;
using Newtonsoft.Json;

namespace CheckWorkareaFiles
{
    public partial class Form1 : Form
    {
        public static string demoUser = ConfigurationSettings.AppSettings["DemoUser"];
        public static ServiceReference1.EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                List<UserStructure> dtUsers = JsonConvert.DeserializeObject<List<UserStructure>>(obj.GetOfficeUsers());
                if (dtUsers != null && dtUsers.Count > 0)
                {
                    for (int i = 328; i < dtUsers.Count; i++)
                    {
                        try
                        {
                            if (Convert.ToString(dtUsers[i].UID) != demoUser)
                            {
                                CheckWorkareaExist(dtUsers[i].UID);
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteErrorLog(string.Empty, ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog(string.Empty, ex);
            }
        }

        private void CheckWorkareaExist(string uid)
        {
            try
            {
                List<WorkAreaStructure> dtWorkArea = JsonConvert.DeserializeObject<List<WorkAreaStructure>>(obj.GetWorkAreaDocumentsByUID(uid));
                if (dtWorkArea != null && dtWorkArea.Count > 0)
                {
                    try { progressBar1.Maximum = dtWorkArea.Count; }
                    catch (Exception) { }
                    var index = 0;
                    foreach (var workArea in dtWorkArea)
                    {
                        try
                        {
                            obj.CheckWorkAreaExists(workArea.UID, workArea.FileName, workArea.PathName, workArea.Size, workArea.WorkAreaId);
                            try
                            { progressBar1.Value = index; }
                            catch (Exception) { }
                            index++;
                        }
                        catch (Exception ex)
                        {
                            WriteErrorLog(string.Empty, ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteErrorLog(string.Empty, ex);
            }
        }

        public static void WriteErrorLog(string messgae, Exception ex)
        {
            try
            {
                if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/eff")))
                    Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/eff"));

                StreamWriter swData2 = new StreamWriter(System.Web.Hosting.HostingEnvironment.MapPath("~/eff") + "\\ExistLog.txt", true);
                swData2.WriteLine("=====Unhandled Exception occured at  : " + DateTime.Now);
                swData2.WriteLine("Message = " + messgae);
                swData2.WriteLine("Error = " + ex.Message);
                swData2.WriteLine(ex.StackTrace);
                swData2.WriteLine(ex.Source);
                swData2.WriteLine(ex.InnerException);
                swData2.WriteLine("====");
                swData2.Close();
                swData2.Dispose();
            }
            catch (Exception ex1)
            {
                string errorMessage = ex1.Message;
            }
        }

        public class WorkAreaStructure
        {
            public string UID { get; set; }
            public string FileName { get; set; }
            public string PathName { get; set; }
            public decimal Size { get; set; }
            public int WorkAreaId { get; set; }
        }

        public class UserStructure
        {
            public string UID { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
        }

    }
}
