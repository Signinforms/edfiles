﻿using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace ClassMultiUpload
{
    [RunInstaller(true)]
    public partial class Installer1 : System.Configuration.Install.Installer
    {
        public Installer1()
        {
            InitializeComponent();
        }

        public override void Commit(System.Collections.IDictionary savedState)
        {
            //base.Commit(savedState);
            //System.Diagnostics.Process.Start(Context.Parameters["TARGETDIR"].ToString() + "application.exe");
            //// Very important! Removes all those nasty temp files.
            //base.Dispose();

            System.Diagnostics.EventLog.WriteEntry("InstallerClass", "Installerclass Invoked By Kavit");
            try
            {
                if (Environment.Is64BitOperatingSystem)//IntPtr.Size == 4)
                {
                    // 64-bit
                    System.Diagnostics.EventLog.WriteEntry("InstallerClass", "Installerclass Invoked By 64 bit");
                    RegistryKey key = Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Wow6432Node\PFU\ScanSnap Extension\MultiUpload");
                    key.SetValue("", @"C:\Program Files (x86)\eFileFolders\MultiUpload\MultiUpload.exe");
                    key.SetValue("Path", @"C:\Program Files (x86)\eFileFolders\MultiUpload");
                    key.SetValue("Config", @"C:\Program Files (x86)\eFileFolders\MultiUpload\MultiUpload.ini");
                    //key.Close();

                }
                else
                {
                    // 32-bit 
                    System.Diagnostics.EventLog.WriteEntry("InstallerClass", "Installerclass Invoked By 32 bit" + IntPtr.Size.ToString());
                    RegistryKey key = Registry.CurrentUser.CreateSubKey(@"\SOFTWARE\PFU\ScanSnap Extension\MultiUpload");
                    key.SetValue("", @"C:\Program Files\eFileFolders\MultiUpload\MultiUpload.exe");
                    key.SetValue("Path", @"C:\Program Files\eFileFolders\MultiUpload");
                    key.SetValue("Config", @"C:\Program Files\eFileFolders\MultiUpload\MultiUpload.ini");
                    //key.Close();
                }


            }
            catch (Exception exc)
            {
                System.Diagnostics.EventLog.WriteEntry("InstallerClassError", exc.Message);
            }
        }
    }
}
