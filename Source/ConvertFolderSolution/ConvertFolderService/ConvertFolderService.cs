﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Web;
using DataQuicker2.Framework;
using Schedule;
using Shinetech.DAL;
using Shinetech.Engines.Adapters;
//using Shinetech.Engines.Adapters;

namespace AJiwani.eFileFolders.Service
{
    public partial class ConvertFolderService : ServiceBase
    {
        protected string hourMinute;
        protected string localRoot; // the server upload path
        protected string serverVolume;// the server website volume path

        protected ScheduleTimer tickTimer;

        public ConvertFolderService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                hourMinute = ConfigurationManager.AppSettings["Timer"];
                localRoot = ConfigurationManager.AppSettings["UploadVolume"];
                serverVolume = ConfigurationManager.AppSettings["ServerVolume"];
                ThreadPool.SetMaxThreads(4, 8);
            }
            catch (Exception exp)
            {
                return;
            }

            tickTimer = new ScheduleTimer();
            tickTimer.Elapsed += TickTimer_Elapsed;
            tickTimer.AddEvent(new ScheduledTime(EventTimeBase.Daily, TimeSpan.Parse(hourMinute)));

            tickTimer.Start();
        }

        protected override void OnStop()
        {
            try
            {
                tickTimer.Stop();
                tickTimer.Dispose();
                tickTimer = null;
            }
            catch
            {
            }
        }

        public void Start()
        {
            OnStart(null);
        }

        public void StopE()
        {
            OnStop();
        }

        private void TickTimer_Elapsed(object sender, ScheduledEventArgs e)
        {
            UploadFile uFile = new UploadFile();
            EventLog log = new EventLog("eFileFolders");
            if (!EventLog.SourceExists("Conversion Service"))
                EventLog.CreateEventSource("Conversion Service", "eFileFolders");

            DataTable dt = new DataTable("FolderFiles");
            try
            {
                ObjectQuery query = uFile.CreateQuery();
                query.SetCriteria(uFile.Flag, Operator.Equal,"n");

                query.SetSelectFields(uFile.FileID,uFile.FolderId,uFile.DividerId,uFile.FormFileID, uFile.FirstName, uFile.LastName,
                    uFile.UserID, uFile.FileName, uFile.Message, uFile.Flag);

                query.SetOrderBy(uFile.UploadDate, false);
                query.Fill(dt);

                string fullPath, serverPath,fileName, destFilePath;
                foreach(DataRow item in dt.Rows)
                {
                    fileName = Path.GetFileName((string) item["FileName"]);
                    fullPath = Path.Combine(localRoot, (string) item["FileName"]);
                    
                    string folderId = item["FolderID"].ToString();
                    string dividerId = item["DividerID"].ToString();
                    string uid = item["UserID"].ToString();

                    serverPath = Path.Combine(serverVolume, folderId.ToString());
                    serverPath = Path.Combine(serverPath, dividerId.ToString());
                    
                    if (!Directory.Exists(serverPath))
                    {
                        Directory.CreateDirectory(serverPath);
                    }

                    string encodeName = HttpUtility.UrlPathEncode(fileName);
                    encodeName = encodeName.Replace("%", "$");

                    serverPath = Path.Combine(serverPath, encodeName);

                    File.Copy(fullPath, serverPath, true);
                    //fullPath//C:\Upload\OfficeUser\efilefolders\Tortoise_Merge.pdf
                    //serverPath//C:\Upload\217\514\Tortoise_Merge.pdf

                    try
                    {
                        ImageAdapter.ProcessPostFile(fullPath, folderId, dividerId, uid, Path.GetDirectoryName(serverPath),
                        Path.GetFileName(fullPath), (int)item["FormFileID"], (int)item["FileID"]);
                    }
                    catch (Exception exp)
                    {
                        log.Source = "Conversion Service";
                        log.WriteEntry(exp.Message, EventLogEntryType.Warning);
                    }
                    
                }
            }
            catch (Exception ex)
            {}
        }
    }
}
