using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace AJiwani.eFileFolders.Service
{
    public static class EventLogHelper
    {
        const string EVENT_NAME = "Application";
        const string EVENT_SOURCE = "ConvertFolderService";
        static EventLog eventLog;

        static EventLogHelper()
        {
            eventLog = new EventLog();
            eventLog.Log = EVENT_NAME;
            eventLog.Source = EVENT_SOURCE;
        }

        public static void WriteInformation(string message)
        {
            eventLog.WriteEntry(message, EventLogEntryType.Information);
        }

        public static void WriteError(string errorMessage)
        {
            eventLog.WriteEntry(errorMessage, EventLogEntryType.Error);
        }

        public static void WriteError(List<string> errorMessages)
        {
            string temp = string.Empty;

            errorMessages.ForEach(delegate(string message)
            {
                temp += message + Environment.NewLine;
            });

            try
            {
                eventLog.WriteEntry(temp);
            }
            catch (Exception ex)
            {
                Console.WriteLine("EventLogHelper: " + ex.Message);
            }

        }
    }
}
