﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;

namespace AJiwani.eFileFolders.Service
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            //Process currentProcess = Process.GetCurrentProcess();
            //currentProcess.PriorityClass = ProcessPriorityClass.Normal;

            if (args == null || args.Length == 0)
            {
                ConvertFolderService service = new ConvertFolderService();

                if (Environment.UserInteractive)
                {
                    service.Start();
                    Console.WriteLine("Servie Started. Press any key to exit.");
                    Console.ReadLine();

                    service.StopE();

                    Thread.Sleep(1000);

                    Environment.Exit(0);
                }
                else
                {
                    ServiceBase[] ServicesToRun;
                    ServicesToRun = new ServiceBase[] { service };

                    ServiceBase.Run(ServicesToRun);
                }
            }
            else
            {
                if (args.Length == 1 && args[0].Length > 1)
                {
                    if (args[0].StartsWith("-") || args[0].StartsWith("/"))
                    {
                        switch (args[0].Substring(1).ToLower())
                        {
                            case "i":
                            case "install":
                                ServiceSelfInstaller.Install();
                                break;
                            case "u":
                            case "uninstall":
                                ServiceSelfInstaller.Uninstall();
                                break;
                        }
                    }
                }
            }
        }
    }
}
