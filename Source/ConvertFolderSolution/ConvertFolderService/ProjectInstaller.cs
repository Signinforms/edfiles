using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;

namespace AJiwani.eFileFolders.Service
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        const string SERVICE_NAME = "ConvertFolderService";

        public ProjectInstaller()
        {
            InitializeComponent();
        }

        protected override void OnAfterInstall(IDictionary savedState)
        {
            base.OnAfterInstall(savedState);

            try
            {
                ServiceController serviceController = new ServiceController();

                serviceController.MachineName = Environment.MachineName;
                serviceController.ServiceName = SERVICE_NAME;
                serviceController.Start();

                serviceController.Close();
            }
            catch { }
        }
    }
}