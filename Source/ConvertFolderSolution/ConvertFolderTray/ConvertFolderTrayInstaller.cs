using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;

namespace AJiwani.eFileFolder.Tray
{
    [RunInstaller(true)]
    public partial class ConvertFolderTrayInstaller : Installer
    {
        public ConvertFolderTrayInstaller()
        {
            InitializeComponent();
        }
    }
}