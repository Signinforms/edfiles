﻿namespace AJiwani.eFileFolder.Tray
{
    partial class SettingsForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.browserServerButton = new System.Windows.Forms.Button();
            this.textBoxServerLocation = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.browserButton = new System.Windows.Forms.Button();
            this.textBoxUploadLocation = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.timePicker1 = new MyTimePicker.TimePicker();
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(517, 271);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 21);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(509, 246);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.browserServerButton);
            this.groupBox2.Controls.Add(this.textBoxServerLocation);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.browserButton);
            this.groupBox2.Controls.Add(this.textBoxUploadLocation);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(18, 105);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(471, 135);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Volume";
            // 
            // browserServerButton
            // 
            this.browserServerButton.Location = new System.Drawing.Point(387, 80);
            this.browserServerButton.Name = "browserServerButton";
            this.browserServerButton.Size = new System.Drawing.Size(78, 26);
            this.browserServerButton.TabIndex = 5;
            this.browserServerButton.Text = "Browse...";
            this.browserServerButton.UseVisualStyleBackColor = true;
            this.browserServerButton.Click += new System.EventHandler(this.browserServerButton_Click);
            // 
            // textBoxServerLocation
            // 
            this.textBoxServerLocation.Location = new System.Drawing.Point(135, 83);
            this.textBoxServerLocation.Name = "textBoxServerLocation";
            this.textBoxServerLocation.Size = new System.Drawing.Size(248, 21);
            this.textBoxServerLocation.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "Server Location";
            // 
            // browserButton
            // 
            this.browserButton.Location = new System.Drawing.Point(387, 37);
            this.browserButton.Name = "browserButton";
            this.browserButton.Size = new System.Drawing.Size(78, 26);
            this.browserButton.TabIndex = 3;
            this.browserButton.Text = "Browse...";
            this.browserButton.UseVisualStyleBackColor = true;
            this.browserButton.Click += new System.EventHandler(this.browserButton_Click);
            // 
            // textBoxUploadLocation
            // 
            this.textBoxUploadLocation.Location = new System.Drawing.Point(135, 40);
            this.textBoxUploadLocation.Name = "textBoxUploadLocation";
            this.textBoxUploadLocation.Size = new System.Drawing.Size(248, 21);
            this.textBoxUploadLocation.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "Upload Location";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.timePicker1);
            this.groupBox1.Location = new System.Drawing.Point(18, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(471, 88);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Timer";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "Hours && Minutes :";
            // 
            // timePicker1
            // 
            this.timePicker1.ButtonColor = System.Drawing.SystemColors.Control;
            this.timePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.timePicker1.Format = "hh:MM";
            this.timePicker1.KeyToHidePicker = System.Windows.Forms.Keys.None;
            this.timePicker1.Location = new System.Drawing.Point(135, 32);
            this.timePicker1.Name = "timePicker1";
            this.timePicker1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.timePicker1.SelectedColor = System.Drawing.Color.White;
            this.timePicker1.SeparatorColor = System.Drawing.Color.RosyBrown;
            this.timePicker1.Size = new System.Drawing.Size(141, 26);
            this.timePicker1.TabIndex = 0;
            this.timePicker1.Value = System.TimeSpan.Parse("23:49:00");
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(447, 300);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(78, 26);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(351, 300);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(78, 26);
            this.okButton.TabIndex = 2;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(541, 345);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "ConvertFolder Service Settings";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Label label1;
        private MyTimePicker.TimePicker timePicker1;
        private System.Windows.Forms.Button browserButton;
        private System.Windows.Forms.TextBox textBoxUploadLocation;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Button browserServerButton;
        private System.Windows.Forms.TextBox textBoxServerLocation;
        private System.Windows.Forms.Label label3;
    }
}