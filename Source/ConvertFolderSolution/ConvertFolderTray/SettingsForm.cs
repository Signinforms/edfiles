using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace AJiwani.eFileFolder.Tray
{
    public partial class SettingsForm : Form
    {
        Configuration config = null;

        public SettingsForm()
        {
            InitializeComponent();

            ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap();
            fileMap.ExeConfigFilename = ConfigurationManager.AppSettings["ServiceConfigPath"];
            config = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);

            ShowConfigSettings();
        }

        private void ShowConfigSettings()
        {
            KeyValueConfigurationElement keyValue = null;
            keyValue = config.AppSettings.Settings["UploadVolume"];
            this.textBoxUploadLocation.Text = keyValue.Value;

            keyValue = config.AppSettings.Settings["ServerVolume"];
            this.textBoxServerLocation.Text = keyValue.Value;

            keyValue = config.AppSettings.Settings["timer"];
            this.timePicker1.Value = TimeSpan.Parse(keyValue.Value);
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;

            config.AppSettings.Settings["UploadVolume"].Value = this.textBoxUploadLocation.Text;
            config.AppSettings.Settings["ServerVolume"].Value = this.textBoxServerLocation.Text;
            config.AppSettings.Settings["timer"].Value = this.timePicker1.Value.ToString();

            config.Save();

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void browserButton_Click(object sender, EventArgs e)
        {
            if(folderBrowserDialog.ShowDialog(this)==DialogResult.OK)
            {
                textBoxUploadLocation.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void browserServerButton_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialog.ShowDialog(this) == DialogResult.OK)
            {
                textBoxServerLocation.Text = folderBrowserDialog.SelectedPath;
            }
        }
    }
}