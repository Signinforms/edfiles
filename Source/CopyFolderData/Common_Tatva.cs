﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.Data.SqlClient;
using iTextSharp.text.pdf;

/// <summary>
/// Summary description for Common_Tatva
/// </summary>
public static class Common_Tatva
{
    #region Properties

    public static readonly string Region = ConfigurationManager.AppSettings["RackSpaceRegion"];

    public static string RackSpaceDownloadDirectory = ConfigurationManager.AppSettings["RackSpaceServerFolder"];
    public static string RackSpaceThumbnailDownload = ConfigurationManager.AppSettings["RackSpaceServerThumbnailFolder"];
    public static string RackSpaceWorkareaDownload = ConfigurationManager.AppSettings["RackSpaceServerWorkareaFolder"];
    public static string RackSpaceNewSplitWorkareaDownload = ConfigurationManager.AppSettings["RackSpaceServerNewSplitWorkareaFolder"];
    public static string NewSplitEdFilesDownload = ConfigurationManager.AppSettings["ServerNewEdFilesFolder"];
    public static string RackSpaceArchiveDownload = ConfigurationManager.AppSettings["RackSpaceServerArchiveFolder"];
    public static string RackspaceDeletedDirectory = ConfigurationManager.AppSettings["deletedDirectory"];
    public static string UploadFolder = ConfigurationManager.AppSettings["UploadFolder"];

    public static string UserLogoFolder = ConfigurationManager.AppSettings["UserLogoFolder"];

    //public  Common_Tatva()
    //{
    //    //
    //    // TODO: Add constructor logic here
    //    //

    //}

    private static string UrlSuffix
    {
        get
        {
            if (HttpContext.Current.Request.ApplicationPath == "/")
            {
                if (HttpContext.Current.Request.Url.Port == 80)
                    return HttpContext.Current.Request.Url.Host + HttpContext.Current.Request.ApplicationPath;
                else
                    return HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port.ToString() + HttpContext.Current.Request.ApplicationPath;
            }
            else
            {
                if (HttpContext.Current.Request.Url.Port == 80)
                    return HttpContext.Current.Request.Url.Host + HttpContext.Current.Request.ApplicationPath + "/";
                else
                    return HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port.ToString() + HttpContext.Current.Request.ApplicationPath + "/";
            }
        }

    }

    public static string UrlBase
    {
        get
        {
            return "http://" + UrlSuffix;
        }
    }

    public static string CurrentVolume
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
        }
    }

    public static string Inbox
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["Inbox"];
        }
    }

    public static string eFileFlow
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["eFileFlow"];
        }
    }

    public static string ScanInBox
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["ScanInBox"];
        }
    }

    public static string WorkAreaPath
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["WorkareaPath"];
        }
    }

    public static string eFileFlowPath
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["eFileFlowPath"];
        }
    }

    public static string InboxPath
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["InboxPath"];
        }
    }

    public static string ExpirationTime
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["ValidShareIdHours"];
        }
    }
    #endregion

    #region General Methods

    /// <summary>
    /// Get WorkAreaId by Path name
    /// </summary>
    /// <param name="workareaPath"></param>
    /// <returns></returns>
    public static int GetWorkAreaId(string workareaPath)
    {
        DataTable dtWorkArea = (DataTable)System.Web.HttpContext.Current.Session["WorkArea_DataSource"];
        if (dtWorkArea.Rows.Count > 0)
        {
            for (int i = 0; i < dtWorkArea.Rows.Count; i++)
            {
                if (dtWorkArea.Rows[i]["PathName"].ToString() == workareaPath.Replace("WorkArea", "workArea"))
                {
                    return Convert.ToInt32(dtWorkArea.Rows[i]["WorkAreaId"].ToString());
                }
            }
        }
        return 0;
    }

    /// <summary>
    /// After moving document to target folder, delete all pages like(SWF file) from source folder
    /// </summary>
    /// <param name="page"></param>
    /// <returns></returns>
    public static bool DeletePagesFromFolder(DataSet page)
    {
        try
        {
            string file = string.Empty;
            for (int i = 0; i < page.Tables[0].Rows.Count; i++)
            {
                file = HttpContext.Current.Server.MapPath(Convert.ToString(page.Tables[0].Rows[i]["ImagePath"]));
                if ((File.Exists(file)))
                    File.Delete(file);
            }
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    /// <summary>
    /// Get folder id form path
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static string GetFolderIdFromPath(string path)
    {
        string folderId = string.Empty;
        if (path.Contains("\\"))
        {
            folderId = path.Split('\\')[1];
        }
        else
        {
            folderId = path.Split('/')[1];
        }
        //folderId = path.Split(new string[] { CurrentVolume + "/" }, StringSplitOptions.None)[1].ToString().Split('/')[0];

        return folderId;
    }

    public static string GetNewPathOnly(string oldPath, string newDividerId, string renameFile)
    {
        string newPath = string.Empty;

        if (!string.IsNullOrEmpty(renameFile) && renameFile != "")
            newPath = CurrentVolume + "/" + GetFolderIdFromPath(oldPath) + "/" + newDividerId + "/" + renameFile + Path.GetExtension(HttpContext.Current.Server.MapPath(oldPath));
        else
            newPath = CurrentVolume + "/" + GetFolderIdFromPath(oldPath) + "/" + newDividerId + "/" + Path.GetFileName(HttpContext.Current.Server.MapPath(oldPath));
        return newPath;
    }

    /// <summary>
    /// Get new path from oldpath and target id(new dividerId)
    /// </summary>
    /// <param name="oldPath"></param>
    /// <param name="newDividerId"></param>
    /// <returns></returns>
    public static string GetNewPath(string oldPath, string newDividerId, string renameFile)
    {
        string newPath = string.Empty;

        if (!string.IsNullOrEmpty(renameFile) && renameFile != "")
            newPath = CurrentVolume + "/" + GetFolderIdFromPath(oldPath) + "/" + newDividerId + "/" + renameFile + Path.GetExtension(HttpContext.Current.Server.MapPath(oldPath));
        else
            newPath = CurrentVolume + "/" + GetFolderIdFromPath(oldPath) + "/" + newDividerId + "/" + Path.GetFileName(HttpContext.Current.Server.MapPath(oldPath));

        string destinationFile = HttpContext.Current.Server.MapPath(newPath);

        if (!Directory.Exists(Path.GetDirectoryName(destinationFile)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(destinationFile));
        }
        if (File.Exists(destinationFile))
        {
            destinationFile = GetNewFileName(destinationFile);
        }

        string encodeName = HttpUtility.UrlPathEncode(Path.GetFileName(destinationFile));
        //encodeName = encodeName.Replace("%", "$");

        newPath = CurrentVolume + "/" + GetFolderIdFromPath(oldPath) + "/" + newDividerId + "/" + Path.GetFileName(destinationFile);
        return newPath;
    }

    /// <summary>
    /// Generates new file name if file already exists
    /// </summary>
    /// <param name="destinationFile">Name of file to be renamed</param>
    /// <returns>New file name</returns>
    public static string GetNewFileName(string destinationFile)
    {
        string fileName = Path.GetFileNameWithoutExtension(destinationFile);
        string fileExtension = Path.GetExtension(destinationFile);
        string fileDirectory = Path.GetDirectoryName(destinationFile);
        int i = 1;
        while (true)
        {
            string renameFileName = fileDirectory + @"\" + fileName + "_v" + i + fileExtension;
            if (File.Exists(renameFileName))
                i++;
            else
            {
                destinationFile = renameFileName;
                break;
            }
        }
        return destinationFile;
    }

    public static string ChangeExtension(string path)
    {
        string renameFileName = string.Empty;
        renameFileName = Path.GetFileNameWithoutExtension(path) + Path.GetExtension(path).ToLower().Replace(".jpeg", ".jpg");
        return renameFileName;
    }

    public static string GetRenameFileName(string path)
    {
        WriteLogs("Step 0: Entered in GetRenameFilename & Path : " + path);
        if (File.Exists(path))
        {
            WriteLogs("Step 1: Path Exists & Path : " + path);
            string fileName = Path.GetFileNameWithoutExtension(path);
            WriteLogs("Step 2: Path Exists & Filename : " + fileName);
            string fileExtension = Path.GetExtension(path);
            string fileDirectory = Path.GetDirectoryName(path);
            int i = 1;
            while (true)
            {
                string renameFileName = fileDirectory + @"\" + fileName + "_v" + i + fileExtension;
                WriteLogs("Step 3: Path Exists & Filename : " + fileName + " & RenameFileName : " + renameFileName);
                if (File.Exists(renameFileName))
                    i++;
                else
                {
                    path = renameFileName;
                    WriteLogs("Step 4: Path Exists & Filename : " + fileName + " & Final Renamed File : " + renameFileName);
                    break;
                }
            }
            WriteLogs("Step 5: Path Exists & Filename : " + fileName + " & Final Path : " + path);
        }
        return path;
    }

    public static void WriteErrorLog(string folderId, string dividerId, string fileName, Exception ex)
    {
        try
        {
            if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/eff")))
                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/eff"));

            StreamWriter swData2 = new StreamWriter(System.Web.Hosting.HostingEnvironment.MapPath("~/eff") + "\\ErrorLog.txt", true);
            swData2.WriteLine("=====Unhandled Exception occured at  : " + DateTime.Now);
            swData2.WriteLine("Folder = " + folderId);
            swData2.WriteLine("Divider = " + dividerId);
            swData2.WriteLine("fileName = " + fileName);
            swData2.WriteLine("Error = " + ex.Message);
            swData2.WriteLine(ex.StackTrace);
            swData2.WriteLine(ex.Source);
            swData2.WriteLine(ex.InnerException);
            swData2.WriteLine("====");
            swData2.Close();
            swData2.Dispose();
        }
        catch (Exception ex1)
        {
            string errorMessage = ex1.Message;
        }
    }

    //Remove this once log form works properly.
    public static void WriteLogFomLog(string logformId, string functionName, string message)
    {
        try
        {
            if (!Directory.Exists("D:\\Website\\EFileFolderWeb\\LogFormLogs"))
                Directory.CreateDirectory("D:\\Website\\EFileFolderWeb\\LogFormLogs");

            StreamWriter swData2 = new StreamWriter("D:\\Website\\EFileFolderWeb\\LogFormLogs\\ErrorLog.txt", true);
            swData2.WriteLine("=====Unhandled Exception occured at  : " + DateTime.Now);
            swData2.WriteLine("logformId = " + logformId);
            swData2.WriteLine("functionName = " + functionName);
            swData2.WriteLine("message = " + message);
            swData2.WriteLine("====");
            swData2.Close();
            swData2.Dispose();
        }
        catch (Exception ex1)
        {
            string errorMessage = ex1.Message;
        }
    }

    public static void WriteLogs(string message)
    {
        try
        {
            if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/eff")))
                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/eff"));

            StreamWriter swData2 = new StreamWriter(System.Web.Hosting.HostingEnvironment.MapPath("~/eff") + "\\log.txt", true);
            swData2.WriteLine("=====Log : " + DateTime.Now);
            swData2.WriteLine(message);
            swData2.WriteLine("====");
            swData2.Close();
            swData2.Dispose();
        }
        catch (Exception ex1)
        {
            string errorMessage = ex1.Message;
        }
    }
    #endregion

    #region SubStringMethod
    /// <summary>
    /// Method to remove last space element from filename
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public static string SubStringFilename(string fileName)
    {
        if (fileName.LastIndexOf('.') <= 0)
            fileName = fileName.Trim();
        else
            fileName = Path.GetFileNameWithoutExtension(fileName).Trim() + Path.GetExtension(fileName);
        return fileName;
    }
    #endregion
}