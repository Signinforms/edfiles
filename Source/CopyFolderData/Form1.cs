﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Web;
using Newtonsoft.Json;
using System.Configuration;
using CopyFolderData.ServiceReference1;
using net.openstack.Core.Domain;
using net.openstack.Providers.Rackspace;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using Tesseract;
using Ghostscript.NET.Rasterizer;
using System.Diagnostics;



namespace CopyFolderData
{
    public partial class Form1 : Form
    {
        public static string RootPath
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["RootPath"]);
            }
        }

        public static string DeletedPath = ConfigurationManager.AppSettings["DeletedArchive"];

        public static int index = 1;

        public enum Action
        {
            FolderCreated,
            TabCreated,
            FileUploaded,
            UploadError,
            FolderProcessFinished,
            FileDeleted,
            TabDeleted,
            FolderDeleted,
            TabDeleteError,
            FolderDeleteError,
            FileDeleteError,
            UnknownException
        }

        public enum FileAction
        {
            Create = 1,
            Delete = 2,
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressBar2.Value = 1;
            progressBar2.Step = 1;
            WriteWindowProcessLog("Step 1 : Start", "", "", "Start");
            Start();
            try
            {
                progressBar2.Value = progressBar2.Maximum;
            }
            catch (Exception) { }
            label2.Text = "";
            label2.Refresh();
            label1.Text = "All files have been uploaded";
            label1.Refresh();
            //System.Windows.Forms.Application.Exit();
        }

        private void FindOrCreateArchive(string[] subFolders, string folderName, string uid, ServiceReference1.EFileFolderJSONWSSoapClient obj, string folderpath, int level)
        {
            try
            {
                foreach (string folder in subFolders)
                {
                    int newLevel = level;
                    int folderID = obj.InsertFolderArchiveTree(folder.Substring(folder.LastIndexOf('\\') + 1).Split('#')[0].Trim(), Convert.ToInt32(folderpath.Substring(folderpath.LastIndexOf('/') + 1)), uid, newLevel, folderpath);
                    newLevel++;
                    folderpath = folderpath + "/" + folderID.ToString();

                    if (folderID > 0)
                    {
                        //Upload files
                        string[] files = Directory.GetFiles(folder);
                        if (files.Length > 0)
                        {
                            try
                            {
                                textBox1.AppendText("Folder : " + folder.Split(new string[] { RootPath }, StringSplitOptions.None)[1]);
                            }
                            catch (Exception) { }
                            textBox1.AppendText(Environment.NewLine);
                            textBox1.AppendText(Environment.NewLine);
                            textBox1.Refresh();
                            int i = 1;
                            try
                            {
                                progressBar2.Value = 1;
                            }
                            catch (Exception) { }
                            label2.Text = "Files uploaded : 0/" + files.Count();
                            label2.Refresh();
                            try
                            {
                                foreach (string file in files)
                                {
                                    progressBar2.Maximum = files.Count();
                                    //Dictionary<string, float> newPath = UploadArchiveToCloud(file, folderpath, uid);
                                    string fileName = System.IO.Path.GetFileName(file);
                                    string base64String = string.Empty;
                                    try
                                    {
                                        base64String = Convert.ToBase64String(File.ReadAllBytes(file));
                                        progressBar2.Value = i;
                                    }
                                    catch (Exception) { }
                                    label1.Text = "File " + fileName + " is being uploaded....";
                                    label1.Refresh();
                                    int archiveId = obj.InsertDocumentArchive(fileName, folderpath, uid, null, base64String, true);
                                    if (archiveId > 0)
                                    {
                                        try
                                        {
                                            if (File.Exists(file))
                                            {
                                                string[] splitData = file.Split(new string[] { RootPath }, StringSplitOptions.None); ;
                                                if (splitData.Length > 1)
                                                {
                                                    string dest = RootPath + DeletedPath + splitData[1];
                                                    if (!Directory.Exists(System.IO.Path.GetDirectoryName(dest)))
                                                        Directory.CreateDirectory(System.IO.Path.GetDirectoryName(dest));
                                                    File.Move(file, dest);
                                                }
                                            }
                                        }
                                        catch (Exception)
                                        {
                                        }

                                        textBox1.AppendText("(" + i + ")  " + fileName);
                                        textBox1.AppendText(Environment.NewLine);
                                        textBox1.Refresh();
                                        label1.Text = "File " + fileName + " has been uploaded successfully !!!";
                                        label1.Refresh();
                                        label2.Text = "Files Uploaded : " + i + "/" + files.Count();
                                        label2.Refresh();
                                        i++;
                                    }
                                    else
                                    {
                                        WriteWindowServiceLog(0, 0, uid, Action.UploadError.ToString(), 0, file.Substring(file.LastIndexOf('\\') + 1), "Document insert error.");
                                    }
                                    try
                                    {
                                        progressBar2.Value = i;
                                    }
                                    catch (Exception) { }
                                    //index++;
                                }
                            }
                            catch (Exception ex)
                            { }
                        }
                        if (Directory.GetDirectories(folder).Length > 0)
                        {
                            try
                            {
                                string[] folders = Directory.GetDirectories(folder);
                                textBox1.AppendText(Environment.NewLine);
                                textBox1.AppendText(Environment.NewLine);
                                textBox1.Refresh();
                                FindOrCreateArchive(folders, folder, uid, obj, folderpath, newLevel);
                                if (folderpath.LastIndexOf('/') > 0)
                                {
                                    folderpath = folderpath.Substring(0, folderpath.LastIndexOf('/'));
                                    newLevel--;
                                }
                            }
                            catch (Exception ex)
                            {
                                WriteWindowProcessLog(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, ex.Message);
                            }
                        }
                        else
                        {
                            try
                            {
                                if (folderpath.LastIndexOf('/') > 0)
                                {
                                    folderpath = folderpath.Substring(0, folderpath.LastIndexOf('/'));
                                    newLevel--;
                                }
                            }
                            catch (Exception) { }
                        }
                    }
                    textBox1.AppendText(Environment.NewLine);
                    textBox1.AppendText(Environment.NewLine);
                    textBox1.Refresh();
                }
            }
            catch (Exception ex)
            {
                WriteWindowProcessLog(string.Empty, string.Empty, uid, string.Empty, string.Empty, ex.Message);
            }
        }

        private void Start()
        {
            try
            {
                //progressBar2.Value += 3;
                //Create Sync folder if does not exist
                if (!Directory.Exists(RootPath))
                {
                    Directory.CreateDirectory(RootPath);
                }

                //Check if Sync folder exists
                if (Directory.Exists(RootPath))
                {
                    //WriteWindowProcessLog("Step 2: Rootpath", RootPath, "", "");
                    //progressBar2.Value += 3;
                    //Get all UserName folders from Sync folder
                    string[] userFolders = Directory.GetDirectories(RootPath);
                    ServiceReference1.EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();

                    foreach (string user in userFolders)
                    {
                        //WriteWindowProcessLog("Step 9: userdetails", RootPath, "", "");
                        if (!user.Contains(DeletedPath.Split('\\')[0]))
                        {
                            List<UserStructure> userDetails = JsonConvert.DeserializeObject<List<UserStructure>>(obj.GetUserIdFromUserName(user.Substring(user.LastIndexOf('\\') + 1)));
                            //WriteWindowProcessLog("Step 10: fetched uid from userdetails", RootPath, "", "");
                            if (userDetails != null && userDetails.Count > 0)
                            {
                                //Get UID from UserName
                                string uid = string.Empty;
                                if (Convert.ToBoolean(Convert.ToInt32(userDetails[0].IsSubUser)))
                                    uid = userDetails[0].OfficeUID;
                                else
                                    uid = userDetails[0].UID;
                                //WriteWindowProcessLog("Step 3: Fetch uid", user, uid, "");
                                //Get all folders inside UserName folder.
                                string[] foldersList = Directory.GetDirectories(user);
                                foreach (string folder in foldersList)
                                {
                                    //progressBar2.Value += 3;
                                    try
                                    {
                                        int level = 1;
                                        int newparentid = obj.InsertFolderArchiveTree(folder.Substring(folder.LastIndexOf('\\') + 1), 0, uid, level, string.Empty);
                                        level++;
                                        WriteWindowProcessLog("Step 4: Fetch Folderid", newparentid.ToString(), uid, "");

                                        //Check if folder exists and if exists get all tabs
                                        //Get sub folders from folder (tabs)
                                        int tabId = 0;
                                        string[] filesList = Directory.GetFiles(folder);
                                        if (filesList.Length > 0)
                                        {
                                            try
                                            {
                                                textBox1.AppendText("Folder : " + folder.Split(new string[] { RootPath }, StringSplitOptions.None)[1]);
                                            }
                                            catch (Exception) { }
                                            textBox1.AppendText(Environment.NewLine);
                                            textBox1.AppendText(Environment.NewLine);
                                            textBox1.Refresh();
                                            int i = 1;
                                            progressBar2.Value = 1;
                                            label2.Text = "Files uploaded : 0/" + filesList.Count();
                                            label2.Refresh();
                                            foreach (string file in filesList)
                                            {
                                                try
                                                {
                                                    progressBar2.Maximum = filesList.Count();
                                                    int archiveId = 0;
                                                    WriteWindowProcessLog("Step 5: upload file", newparentid.ToString(), uid, "", file);
                                                    //Dictionary<string, float> newPath = UploadArchiveToCloud(file, newparentid.ToString(), uid);
                                                    string fileName = System.IO.Path.GetFileName(file);
                                                    label1.Text = "File " + fileName + " is being uploaded....";
                                                    label1.Refresh();
                                                    string base64String = string.Empty;
                                                    try
                                                    {
                                                        base64String = Convert.ToBase64String(File.ReadAllBytes(file));
                                                        progressBar2.Value = i;
                                                    }
                                                    catch (Exception)
                                                    {
                                                    }
                                                    archiveId = obj.InsertDocumentArchive(fileName, newparentid.ToString(), uid, null, base64String, true);

                                                    if (archiveId > 0)
                                                    {
                                                        try
                                                        {
                                                            if (File.Exists(file))
                                                            {
                                                                string[] splitData = file.Split(new string[] { RootPath }, StringSplitOptions.None); ;
                                                                if (splitData.Length > 1)
                                                                {
                                                                    string dest = RootPath + DeletedPath + splitData[1];
                                                                    if (!Directory.Exists(System.IO.Path.GetDirectoryName(dest)))
                                                                        Directory.CreateDirectory(System.IO.Path.GetDirectoryName(dest));
                                                                    File.Move(file, dest);
                                                                }
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            WriteWindowServiceLog(newparentid, tabId, uid, Action.UploadError.ToString(), 0, file.Substring(file.LastIndexOf('\\') + 1), ex.Message);
                                                        }

                                                        textBox1.AppendText("(" + i + ")  " + fileName);
                                                        textBox1.AppendText(Environment.NewLine);
                                                        textBox1.Refresh();
                                                        label1.Text = "File " + fileName + " has been uploaded successfully !!!";
                                                        label1.Refresh();
                                                        label2.Text = "Files Uploaded : " + i + "/" + filesList.Count();
                                                        label2.Refresh();
                                                        i++;
                                                    }
                                                    else
                                                    {
                                                        WriteWindowServiceLog(newparentid, tabId, uid, Action.UploadError.ToString(), 0, file.Substring(file.LastIndexOf('\\') + 1), "Document insert error.");
                                                    }
                                                    try
                                                    {
                                                        //int progress = progressBar2.Value + (progressBar2.Maximum / filesList.Count() / 2);
                                                        //if (progress > progressBar2.Maximum)
                                                        //    progressBar2.Value = progressBar2.Maximum;
                                                        //else
                                                        progressBar2.Value = i;
                                                    }
                                                    catch (Exception) { }
                                                    //index++;
                                                }
                                                catch (Exception ex)
                                                {
                                                    WriteWindowServiceLog(newparentid, tabId, uid, Action.UploadError.ToString(), 0, file.Substring(file.LastIndexOf('\\') + 1), ex.Message);
                                                    continue;
                                                }
                                            }
                                        }


                                        string[] subFolders = Directory.GetDirectories(folder);

                                        if (newparentid > 0 && subFolders.Length != 0)
                                        {
                                            textBox1.AppendText(Environment.NewLine);
                                            textBox1.AppendText(Environment.NewLine);
                                            textBox1.Refresh();
                                            FindOrCreateArchive(subFolders, folder, uid, obj, newparentid.ToString(), level);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        WriteWindowServiceLog(0, 0, uid, Action.UploadError.ToString(), 0, string.Empty, ex.Message);
                                        continue;
                                    }
                                    textBox1.AppendText(Environment.NewLine);
                                    textBox1.AppendText(Environment.NewLine);
                                    textBox1.Refresh();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteWindowServiceLog(0, 0, "", Action.UnknownException.ToString(), 0, "", ex.Message);
                System.Windows.Forms.Application.Exit();
            }
        }

        private string ChangeExtension(string path)
        {
            string renameFileName = string.Empty;
            renameFileName = System.IO.Path.GetFileNameWithoutExtension(path) + System.IO.Path.GetExtension(path).ToLower().Replace(".jpeg", ".jpg");
            return renameFileName;
        }

        private string GetRenameFileName(string path)
        {
            if (File.Exists(path))
            {
                string fileName = System.IO.Path.GetFileNameWithoutExtension(path);
                string fileExtension = System.IO.Path.GetExtension(path);
                string fileDirectory = System.IO.Path.GetDirectoryName(path);
                int i = 1;
                while (true)
                {
                    string renameFileName = fileDirectory + @"\" + fileName + "(" + i + ")" + fileExtension;
                    if (File.Exists(renameFileName))
                        i++;
                    else
                    {
                        path = renameFileName;
                        break;
                    }
                }
            }
            return path;
        }

        public static void WriteWindowServiceLog(int folderId, int dividerId, string uid, string action, int documentId = 0, string fileName = "", string errorMsg = "")
        {
            try
            {
                if (!Directory.Exists("C:\\CopyFolderLog"))
                    Directory.CreateDirectory("C:\\CopyFolderLog");

                using (StreamWriter swData2 = new StreamWriter("C:\\CopyFolderLog\\CopyFolderLog.txt", true))
                {
                    swData2.WriteLine("===== Log " + DateTime.Now);
                    swData2.WriteLine("FolderID = " + folderId);
                    swData2.WriteLine("DividerId = " + dividerId);
                    swData2.WriteLine("fileName = " + fileName);
                    swData2.WriteLine("documentId = " + documentId);
                    swData2.WriteLine("uid = " + uid);
                    swData2.WriteLine("action = " + action);
                    swData2.WriteLine("error = " + errorMsg);
                    swData2.WriteLine("==============================================================");
                    swData2.Close();
                    swData2.Dispose();
                }
            }
            catch (Exception ex1)
            {
                string errorMessage = ex1.Message;
            }
        }

        public static void WriteWindowProcessLog(string process, string folderId = "", string uid = "", string action = "", string fileName = "", string errorMsg = "")
        {
            try
            {
                if (!Directory.Exists("C:\\FolderProcessLog"))
                    Directory.CreateDirectory("C:\\FolderProcessLog");

                using (StreamWriter swData2 = new StreamWriter("C:\\FolderProcessLog\\FolderProcessLog.txt", true))
                {
                    swData2.WriteLine("===== Log " + DateTime.Now);
                    swData2.WriteLine("Step = " + process);
                    swData2.WriteLine("FolderID = " + folderId);
                    swData2.WriteLine("fileName = " + fileName);
                    swData2.WriteLine("uid = " + uid);
                    swData2.WriteLine("error = " + errorMsg);
                    swData2.WriteLine("==============================================================");
                    swData2.Close();
                    swData2.Dispose();
                }
            }
            catch (Exception ex1)
            {
                string errorMessage = ex1.Message;
            }
        }

        private void progressBar2_Click(object sender, EventArgs e)
        {

        }
    }

    public class UserStructure
    {
        public string UID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OfficeUID { get; set; }
        public string IsSubUser { get; set; }
    }
}
