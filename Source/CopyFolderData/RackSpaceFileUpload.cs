﻿using net.openstack.Core.Domain;
using net.openstack.Providers.Rackspace;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;

#region RackSpaceFileUpload
/// <summary>
/// Summary description for RackSpaceFileUpload
/// </summary>
public class RackSpaceFileUpload
{
    #region Properties
    public static CloudIdentity user = new CloudIdentity
    {
        Username = ConfigurationManager.AppSettings["RackSpaceUserName"],
        APIKey = ConfigurationManager.AppSettings["RackSpaceAPIKey"]
    };
    public static CloudFilesProvider cloudfilesProvider = new CloudFilesProvider(user);
    #endregion

    public RackSpaceFileUpload()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public RackSpaceFileUpload(string sessionID)
    {
        //if (!Common_Tatva.IsUserSwitched())
        //    Sessions.RackSpaceUserID = sessionID;
        //else
        //    Sessions.SwitchedRackspaceId = sessionID;
    }

    /// <summary>
    /// Creates container named as user identity if it does not already exists
    /// </summary>
    /// <param name="containerName">Name of container to be created</param>
    /// <returns>Ture if container created successfully, false otheriwse</returns>
    public bool CreateContainer(string containerName, ref string errorMessage)
    {
        try
        {
            cloudfilesProvider.CreateContainer(containerName, null, Common_Tatva.Region);
            return true;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(containerName, string.Empty, string.Empty, ex);
            errorMessage = ex.Message;
            return false;
        }
    }

    /// <summary>
    /// Uploads object to eFileShare folder
    /// </summary>
    /// <param name="fileName">Name of file to be uploaded</param>
    /// <param name="folder">Name of folder into which file is to be uploaded</param>
    /// <param name="fileContent">Content of file to be uploaded</param>
    /// <param name="folderID">ID of share folder</param>
    /// <param name="errorMsg">Error Message</param>
    /// <returns>True if object uploaded successfully, false otherwise</returns>
    public string UploadObject(ref string errorMsg, string fileName, Enum_Tatva.Folders folder, Stream fileContent, string folderID = null, string uid = null)
    {
        MemoryStream memoryStream = new MemoryStream();
        fileName = fileName.Replace("$20", " ");
        fileName = Common_Tatva.SubStringFilename(fileName);

        try
        {
            fileName = fileName.Replace("$20", " ");
            fileName = Common_Tatva.SubStringFilename(fileName);
            string objectToUploaded = string.Empty;
            if (CreateContainer(uid, ref errorMsg))
            {
                if (fileName.IndexOf('/') > 0)
                {
                    int fileIndex = fileName.LastIndexOf('/') + 1;
                    folderID = fileName.Substring(0, fileIndex - 1);
                    fileName = fileName.Substring(fileIndex);
                }
                objectToUploaded = GeneratePath(fileName.ToLower(), folder, folderID);


                //Check if object is already there with same name
                try
                {
                    cloudfilesProvider.GetObject(uid, objectToUploaded, memoryStream);
                }
                catch (Exception ex)
                {
                    Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(objectToUploaded) ? string.Empty : objectToUploaded, string.Empty, fileName, ex);
                }
                if (memoryStream.Length > 0)
                {
                    fileName = GetNewFileName(fileName.ToLower(), folder, folderID, uid);
                    fileName = fileName.Substring(fileName.LastIndexOf('\\') + 1);
                    objectToUploaded = GeneratePath(fileName, folder, folderID);
                }
                cloudfilesProvider.CreateObject(uid, fileContent, objectToUploaded);
            }
            return objectToUploaded;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(folderID) ? string.Empty : folderID, string.Empty, fileName, ex);
            errorMsg = ex.Message;
            return fileName;
        }
        finally
        {
            memoryStream.Close();
            memoryStream.Dispose();
        }

    }

    /// <summary>
    /// Generates path according to folder type for uploading object
    /// </summary>
    /// <param name="fileName">Name of file to be uploaded</param>
    /// <param name="folder">Name of folder into which file is to be uploaded</param>
    /// <param name="folderID">ID of share folder</param>
    /// <returns>Path to which file is to be uploaded</returns>
    public string GeneratePath(string fileName, Enum_Tatva.Folders folder, string folderID = null)
    {
        string filePath = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(folderID))
            {
                filePath = Enum_Tatva.GetEnumDescription(folder) + "/" + folderID + "/" + fileName;
            }
            else
            {
                filePath = Enum_Tatva.GetEnumDescription(folder) + "/" + fileName; ;
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(folderID) ? string.Empty : folderID, string.Empty, fileName, ex);
        }
        return filePath;
    }

    /// <summary>
    /// Returns list of object from specified container
    /// </summary>
    /// <param name="containerName">Name of container to list objects</param>
    /// <param name="markerStart">Name of folder from which objects to be retrived</param>
    /// <returns>Returns list of object from specified container</returns>
    public IEnumerable<ContainerObject> GetObjectList(string containerName, string markerStart = null)
    {
        return cloudfilesProvider.ListObjects(containerName, null, markerStart);
    }

    /// <summary>
    /// Generates new file name if file already exists
    /// </summary>
    /// <param name="destinationFile">Name of file to be renamed</param>
    /// <param name="folder">Folder where file resides</param>
    /// <returns>New file name</returns>
    public string GetNewFileName(string destinationFile, Enum_Tatva.Folders folder, string folderID = null, string uid = null)
    {
        try
        {
            string fileName = Path.GetFileNameWithoutExtension(destinationFile);
            string fileExtension = Path.GetExtension(destinationFile);
            int i = 1;
            IEnumerable<ContainerObject> objectList = GetObjectList(uid, Enum_Tatva.GetEnumDescription(folder) + (string.IsNullOrEmpty(folderID) ? string.Empty : "/" + folderID));

            ContainerObject[] containerObject = objectList.ToArray<ContainerObject>();
            for (int index = 0; index < containerObject.Length; index++)
            {
                string renameFileName = fileName + "_v" + i + fileExtension;
                string fileNameToCompare = string.Empty;
                if (!string.IsNullOrEmpty(folderID))
                {
                    if (containerObject[index].Name.Contains(folderID) && string.Equals(renameFileName, containerObject[index].Name.Substring(containerObject[index].Name.LastIndexOf('/') + 1)))
                    {
                        i++;
                        index = -1;
                        continue;
                    }
                }
                else
                {
                    string comparePath = GeneratePath(renameFileName.ToLower(), folder);
                    if (string.Equals(comparePath, containerObject[index].Name))
                    {
                        i++;
                        index = -1;
                        continue;
                    }
                }
                destinationFile = renameFileName;
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(destinationFile) ? string.Empty : destinationFile, string.Empty, destinationFile, ex);
        }
        return destinationFile;
    }

    /// <summary>
    /// Overwrites object in share folder 
    /// </summary>
    /// <param name="objectPath">File path to be overwritten</param>
    /// <param name="fileContent">Content of file</param>
    /// <param name="errorMsg">Error Message</param>
    public void UploadObjectForViewer(ref string errorMsg, string objectPath, Stream fileContent, string uid)
    {
        try
        {
            cloudfilesProvider.CreateObject(uid, fileContent, objectPath);
        }
        catch (Exception ex)
        {
            errorMsg = ex.Message;
        }
    }
}
#endregion