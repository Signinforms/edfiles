﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DataQuicker2.Framework;
using Shinetech.DAL;
using System.IO;
using System.Web;
using System.Net;
using Ionic.Zip;
using System.IO.Compression;
using System.Data.SqlClient;
using System.Configuration;
using System.Reflection;
using System.Windows.Forms;
using System.Web;

namespace CopyFoldersApplication
{
    public partial class Form1 : Form
    {
        private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
        private static string uid = ConfigurationManager.AppSettings["UserId"];
        private static string archiveFolder = ConfigurationManager.AppSettings["ArchiveFolder"];

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressBar1.Minimum = 1;
            progressBar1.Value = 1;
            progressBar1.Step = 1;
            Start();
            System.Windows.Forms.Application.Exit();
        }

        private void Start()
        {
            DataTable dtFolders = new DataTable("FileFolder");
            try
            {
                string sqlstr = "EXEC [dbo].[eFileFolder_SearchFileFoldersInfor] '" + uid + "'";
                WriteLogs("Stored procedure query" + sqlstr, new Exception());
                SqlHelper sql = new SqlHelper("Default");
                WriteLogs("Sql initialized", new Exception());
                sql.Fill(dtFolders, sqlstr);
                WriteLogs("Total folders are : " + dtFolders.Rows.Count, new Exception());
                int count = 0;
                foreach (DataRow dr in dtFolders.Rows)
                {
                    WriteLogs("Folder number : " + count + " & FolderID :" + Convert.ToInt32(Convert.ToString(dr["FolderID"])), new Exception());
                    CopyFolders(Convert.ToInt32(Convert.ToString(dr["FolderID"])));
                    count++;
                }
            }
            catch (Exception ex)
            {
                WriteErrorLogs(string.Empty, ex);
                new ApplicationException(ex.Message);
            }
        }

        private void CopyFolders(int folderId)
        {
            //文件夹打包
            try
            {
                FileFolder folder = new FileFolder(folderId);
                MemoryStream memoryStream = new MemoryStream();
                if (folder.IsExist)
                {
                    string path = "D:\\EdFiles_Fullertonsd_Latest\\ArchiveBox\\";
                    string sourcePath = "D:\\Website\\EFileFolderWeb\\";
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }

                    string folderPath = Path.Combine(path, folder.FolderName.Value + ".zip");
                    WriteLogs("Folderpath : " + folderPath, new Exception());
                    int i = 1;
                    while (File.Exists(folderPath)) //删除以前的folder及数据
                    {
                        folderPath = Path.Combine(path, folder.FolderName.Value + " (" + i + ").zip");
                        i++;
                    }

                    //Directory.CreateDirectory(folderPath); //创建目标文件夹
                    StringBuilder folderComment = new StringBuilder();
                    folderComment.AppendFormat("Folder Name : {0}\r\n", folder.FolderName.Value);
                    folderComment.AppendFormat("File Number : {0}\r\n", folder.FileNumber.Value);
                    folderComment.AppendFormat("Email  : {0}   ", folder.Email.Value);
                    folderComment.AppendFormat("Tel.   : {0}\r\n", folder.Tel.Value);
                    folderComment.AppendFormat("Alert 1: {0}\r\n", folder.Alert1.Value);
                    folderComment.AppendFormat("Alert 2: {0}\r\n", folder.Alert2.Value);
                    folderComment.AppendFormat("Comments: {0}\r\n", folder.Comments.Value);


                    //ZipFile zipfile = new ZipFile();
                    ZipStorer zip = ZipStorer.Create(folderPath, folderComment.ToString());

                    try
                    {

                        zip.EncodeUTF8 = true;

                        MemoryStream readme = new MemoryStream(
                            System.Text.Encoding.UTF8.GetBytes(folderComment.ToString()));

                        // Stores a new file directly from the stream                     
                        zip.AddStream(ZipStorer.Compression.Store, "readme.txt", readme, DateTime.Now, "Please read");

                        readme.Close();


                        DataTable dividers = GetDividerByEffid(Convert.ToInt32(folderId));
                        DataTable documents = GetDocumentByFolderId(folderId.ToString());

                        if (dividers != null && documents != null)
                        {
                            foreach (DataRow item in dividers.Rows)
                            {
                                #region 数据文件拷贝
                                string dividerText = (string)item["Name"];

                                //获取该divider下的所有文档并从原路径中拷贝过来
                                //在archives目录下创建该用户登录名的folder
                                foreach (DataRow docRow in documents.Rows)
                                {
                                    if (docRow["DividerID"].Equals(item["DividerID"]))
                                    {

                                        string docName = (string)docRow["Name"];
                                        string docPath = (string)docRow["PathName"];

                                        string dcoFullPath = sourcePath + docPath;

                                        //文档编码后的名称,即保存在磁盘上的文档名称
                                        string encodeName1 = HttpUtility.UrlPathEncode(docName);
                                        encodeName1 = encodeName1.Replace("%", "$");
                                        string docEncodeFullName = Path.Combine(dcoFullPath, encodeName1 + ".pdf"); // 其他的格式现在不管
                                        WriteLogs("Document path : " + docEncodeFullName, new Exception());
                                        if (File.Exists(docEncodeFullName))
                                        {
                                            //dividerText为zip中的子目录名称
                                            string targetFileNameInZip = Path.Combine(dividerText, docName + Path.GetExtension(docEncodeFullName));
                                            //添加该文件到压缩文件中
                                            zip.AddFile(ZipStorer.Compression.Store, docEncodeFullName, targetFileNameInZip, "");
                                        }
                                        else
                                        {
                                            WriteErrorLogs("Document name with full path : " + docEncodeFullName + " & DocumentId : " + Convert.ToString(docRow["DocumentID"]), new Exception());
                                        }
                                    }
                                }
                                #endregion 数据文件拷贝
                            }
                        }


                        zip.Close();
                        // zip.Dispose();
                        zip = null;
                    }
                    catch (Exception ex)
                    {
                        WriteErrorLogs("For folderid : " + folderId + " & folderPath : " + folderPath, ex);
                    }
                    finally
                    {
                        if (zip != null)
                        {
                            zip.Close();
                            zip = null;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                WriteErrorLogs("For folderid : " + folderId, ex);
            }
        }

        public static DataTable GetDividerByEffid(int EFFID)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            try
            {
                // keyname = string.Join(",", keyname.Split(new char[0], StringSplitOptions.RemoveEmptyEntries));
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "GetDividersByEffid";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter param1 = command.Parameters.AddWithValue("@Effid", EFFID);
                param1.Direction = ParameterDirection.Input;
                param1.DbType = DbType.Int32;

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;

                DataTable table = new DataTable();

                dataAdapter.Fill(table);

                return table;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }

        public static DataTable GetDocumentByFolderId(string folderId)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "GetDocumentsFolderWise";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter param1 = command.Parameters.AddWithValue("@FolderID", folderId);
                param1.Direction = ParameterDirection.Input;
                param1.DbType = DbType.String;

                SqlParameter param2 = command.Parameters.AddWithValue("@UID", uid);
                param2.Direction = ParameterDirection.Input;
                param2.DbType = DbType.String;

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                DataTable table = new DataTable();
                dataAdapter.Fill(table);
                return table;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void WriteLogs(string message, Exception ex = null)
        {
            try
            {
                if (!Directory.Exists("D:\\EdFiles_Fullertonsd\\Logs"))
                    Directory.CreateDirectory("D:\\EdFiles_Fullertonsd\\Logs");

                StreamWriter swData2 = new StreamWriter("D:\\EdFiles_Fullertonsd\\Logs\\Log.txt", true);
                swData2.WriteLine("=====Unhandled Exception occured at  : " + DateTime.Now);
                swData2.WriteLine("message = " + message);
                swData2.WriteLine("Error = " + ex.Message);
                swData2.WriteLine(ex.StackTrace);
                swData2.WriteLine(ex.Source);
                swData2.WriteLine(ex.InnerException);
                swData2.WriteLine("====");
                swData2.Close();
                swData2.Dispose();
            }
            catch (Exception ex1)
            {
                string errorMessage = ex1.Message;
            }
        }

        private void WriteErrorLogs(string message, Exception ex = null)
        {
            try
            {
                if (!Directory.Exists("D:\\EdFiles_Fullertonsd\\Logs"))
                    Directory.CreateDirectory("D:\\EdFiles_Fullertonsd\\Logs");

                StreamWriter swData2 = new StreamWriter("D:\\EdFiles_Fullertonsd\\Logs\\ErrorLog.txt", true);
                swData2.WriteLine("=====Unhandled Exception occured at  : " + DateTime.Now);
                swData2.WriteLine("message = " + message);
                swData2.WriteLine("Error = " + ex.Message);
                swData2.WriteLine(ex.StackTrace);
                swData2.WriteLine(ex.Source);
                swData2.WriteLine(ex.InnerException);
                swData2.WriteLine("====");
                swData2.Close();
                swData2.Dispose();
            }
            catch (Exception ex1)
            {
                string errorMessage = ex1.Message;
            }
        }
    }
}
