﻿// The source code generated by DataQuicker2 Code Generator.
// Code Generator Assembly Version: 1.2.0.21138
// Create time: 2008-10-16 11:48:50

using System;
using System.Data;
using System.Text;
using System.Collections.Generic;
using DataQuicker2.Framework;

namespace Shinetech.DAL
{
	/// <summary>
	/// 实体对象Divider。<br />
	/// Entity class Divider.
	/// </summary>
	public class Divider: Table
	{
		#region Properties And Fields
		
		private Column<int> dividerID = new Column<int>();
		/// <summary>
		/// 设置/取得列对象DividerID。<br />
		/// Set/Get column instance DividerID.
		/// </summary>
		public Column<int> DividerID
		{
			get
			{
				return this.dividerID;
			}
			set
			{
				this.dividerID = value;
			}
		}
		
		private Column<string> name = new Column<string>();
		/// <summary>
		/// 设置/取得列对象Name。<br />
		/// Set/Get column instance Name.
		/// </summary>
		public Column<string> Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}
		
		private Column<DateTime> dOB = new Column<DateTime>();
		/// <summary>
		/// 设置/取得列对象DOB。<br />
		/// Set/Get column instance DOB.
		/// </summary>
		public Column<DateTime> DOB
		{
			get
			{
				return this.dOB;
			}
			set
			{
				this.dOB = value;
			}
		}

        private Column<string> color = new Column<string>();
		/// <summary>
		/// 设置/取得列对象Color。<br />
		/// Set/Get column instance Color.
		/// </summary>
        public Column<string> Color
		{
			get
			{
				return this.color;
			}
			set
			{
				this.color = value;
			}
		}

        private Column<bool> locked = new Column<bool>();
        /// <summary>
        /// 设置/取得列对象Locked。<br />
        /// Set/Get column instance Locked.
        /// </summary>
        public Column<bool> Locked
        {
            get
            {
                return this.locked;
            }
            set
            {
                this.locked = value;
            }
        }

        private Column<string> officeID = new Column<string>();
		/// <summary>
        /// 设置/取得列对象OfficeID。<br />
        /// Set/Get column instance OfficeID.
		/// </summary>
        public Column<string> OfficeID
		{
			get
			{
                return this.officeID;
			}
			set
			{
                this.officeID = value;
			}
		}

        private Column<int> effID = new Column<int>();
        /// <summary>
        /// 设置/取得列对象DividerID。<br />
        /// Set/Get column instance DividerID.
        /// </summary>
        public Column<int> EffID
        {
            get
            {
                return this.effID;
            }
            set
            {
                this.effID = value;
            }
        }

        private Column<string> metaXML = new Column<string>();
        /// <summary>
        /// 设置/取得列对象metaXML。<br />
        /// Set/Get column instance metaXML.
        /// </summary>
        public Column<string> MetaXML
        {
            get
            {
                return this.metaXML;
            }
            set
            {
                this.metaXML = value;
            }
        }

        private Column<string> guid = new Column<string>();
        /// <summary>
        /// 设置/取得列对象GUID。<br />
        /// Set/Get column instance GUID.
        /// </summary>
        public Column<string> GUID
        {
            get
            {
                return this.guid;
            }
            set
            {
                this.guid = value;
            }
        }
		
		#endregion
	
		/// <summary>
		/// 构造实体对象Divider。<br />
		/// Construct entity instance Divider.
		/// </summary>
		public Divider() : base()
		{
		}
		
		/// <summary>
		/// 构造实体对象Divider。<br />
		/// Construct entity instance Divider.
		/// </summary>
		/// <param name="dividerID" />
		public Divider(int dividerID) : base(dividerID)
		{
		}
		
		/// <summary>
		/// 构造实体对象Divider。<br />
		/// Construct entity instance Divider.
		/// </summary>
		/// <param name="dividerID" />
		/// <param name="connection">已打开的数据库连接对象。</param>
		public Divider(int dividerID, IDbConnection connection) : base(dividerID, connection)
		{
		}
		
		/// <summary>
		/// 构造实体对象Divider。<br />
		/// Construct entity instance Divider.
		/// </summary>
		/// <param name="dividerID" />
		/// <param name="transaction">已开启的事务对象。</param>
		public Divider(int dividerID, IDbTransaction transaction) : base(dividerID, transaction)
		{
		}
		
		/// <summary>
		/// 绑定实体对象与数据库对象。<br />
		/// Bind entity instance to database.
		/// </summary>
		protected override void Bind()
		{

            this.BindPrimaryKey(this.dividerID, ColumnValueType.DbAutoIncrease);
				
			this.BindTableName("Divider");
			
			this.BindColumn(this.dividerID, "DividerID");
					
			this.BindColumn(this.name, "Name", 50);
					
			this.BindColumn(this.dOB, "DOB");
					
			this.BindColumn(this.color, "Color",10);

            this.BindColumn(this.locked, "Locked");

            this.BindColumn(this.officeID, "OfficeID");

            this.BindColumn(this.effID, "EFFID");

            this.BindColumn(this.metaXML, "MetaXML");

            this.BindColumn(this.guid, "GUID", 50);
					
		}
	}
}
