﻿// The source code generated by DataQuicker2 Code Generator.
// Code Generator Assembly Version: 1.2.0.21138
// Create time: 2008-10-16 11:48:50

using System;
using System.Data;
using System.Text;
using System.Collections.Generic;
using DataQuicker2.Framework;

namespace Shinetech.DAL
{
	/// <summary>
    /// 实体对象EFFolder。<br />
    /// Entity class EFFolder.
	/// </summary>
	public class EFFolder: Table
	{
		#region Properties And Fields

        private Column<int> eFFID = new Column<int>();
        /// <summary>
        /// 设置/取得列对象eFFID。<br />
        /// Set/Get column instance eFFID.
        /// </summary>
        public Column<int> EFFID
        {
            get
            {
                return this.eFFID;
            }
            set
            {
                this.eFFID = value;
            }
        }
		private Column<int> folderID = new Column<int>();
		/// <summary>
		/// 设置/取得列对象FolderID。<br />
		/// Set/Get column instance FolderID.
		/// </summary>
		public Column<int> FolderID
		{
			get
			{
				return this.folderID;
			}
			set
			{
				this.folderID = value;
			}
		}

        private Column<DateTime> createDate = new Column<DateTime>();
		/// <summary>
		/// 设置/取得列对象DOB。<br />
		/// Set/Get column instance DOB.
		/// </summary>
        public Column<DateTime> CreateDate
		{
			get
			{
                return this.createDate;
			}
			set
			{
                this.createDate = value;
			}
		}


        private Column<string> userID = new Column<string>();
        /// <summary>
        /// 设置/取得列对象OfficeID。<br />
        /// Set/Get column instance OfficeID.
        /// </summary>
        public Column<string> UserID
        {
            get
            {
                return this.userID;
            }
            set
            {
                this.userID = value;
            }
        }
		
		#endregion
	
		/// <summary>
		/// 构造实体对象FileFolder。<br />
		/// Construct entity instance FileFolder.
		/// </summary>
		public EFFolder() : base()
		{
		}
		
		/// <summary>
		/// 构造实体对象FileFolder。<br />
		/// Construct entity instance FileFolder.
		/// </summary>
		/// <param name="folderID" />
        public EFFolder(int eFFID): base(eFFID)
		{
		}
		
		/// <summary>
		/// 构造实体对象FileFolder。<br />
		/// Construct entity instance FileFolder.
		/// </summary>
		/// <param name="folderID" />
		/// <param name="connection">已打开的数据库连接对象。</param>
        public EFFolder(int eFFID, IDbConnection connection): base(eFFID, connection)
		{
		}
		
		/// <summary>
		/// 构造实体对象FileFolder。<br />
		/// Construct entity instance FileFolder.
		/// </summary>
		/// <param name="folderID" />
		/// <param name="transaction">已开启的事务对象。</param>
        public EFFolder(int eFFID, IDbTransaction transaction)
            : base(eFFID, transaction)
		{
		}
		
		/// <summary>
		/// 绑定实体对象与数据库对象。<br />
		/// Bind entity instance to database.
		/// </summary>
		protected override void Bind()
		{

            this.BindPrimaryKey(this.eFFID, ColumnValueType.DbAutoIncrease);

            this.BindTableName("EFFolder");

            this.BindColumn(this.eFFID, "EFFID");

			this.BindColumn(this.folderID, "FolderID");

            this.BindColumn(this.userID, "UserID");

            this.BindColumn(this.createDate, "CreateDate");
			
		}
	}
}
