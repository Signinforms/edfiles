﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Globalization;
using System.ComponentModel;

namespace Shinetech.DAL
{
    public static class General_Class
    {
        private static readonly string ConnectionString =
         ConfigurationManager.ConnectionStrings["Default"].ConnectionString;

        #region AuditLog
        /// <summary>
        /// Insert AuditLog By DocumentId
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="modifyBy"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static string AuditLogByDocId(int documentId, string modifyBy, int action)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertAuditLogByDocId";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[4];
                    parameters[0] = new SqlParameter("@DocumentID", SqlDbType.Int);
                    parameters[0].Value = documentId;
                    parameters[1] = new SqlParameter("@ModifiedBy", SqlDbType.NVarChar, 50);
                    parameters[1].Value = modifyBy;
                    parameters[2] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[2].Value = System.DateTime.Now;
                    parameters[3] = new SqlParameter("@Action", SqlDbType.TinyInt);
                    parameters[3].Value = action;

                    command.Parameters.AddRange(parameters);
                    command.ExecuteNonQuery();
                    return parameters[3].Value.ToString();
                }
                catch (Exception ex)
                {
                    return string.Empty;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Insert AuditLog By DividerId
        /// </summary>
        /// <param name="dividerId"></param>
        /// <param name="modifyBy"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static string AuditLogByDividerId(int dividerId, string modifyBy, int action)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertAuditLogByDividerId";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[4];
                    parameters[0] = new SqlParameter("@DividerId", SqlDbType.Int);
                    parameters[0].Value = dividerId;
                    parameters[1] = new SqlParameter("@ModifiedBy", SqlDbType.NVarChar, 50);
                    parameters[1].Value = modifyBy;
                    parameters[2] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[2].Value = System.DateTime.Now;
                    parameters[3] = new SqlParameter("@Action", SqlDbType.TinyInt);
                    parameters[3].Value = action;

                    command.Parameters.AddRange(parameters);
                    command.ExecuteNonQuery();
                    return parameters[3].Value.ToString();
                    //return int.Parse(parameters[8].Value.ToString());
                }
                catch (Exception ex)
                {
                    return string.Empty;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Insert AuditLog By FolderId
        /// </summary>
        /// <param name="folderId"></param>
        /// <param name="modifyBy"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static string AuditLogByFolderId(int folderId, string modifyBy, int action)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertAuditLogByFolderId";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[4];
                    parameters[0] = new SqlParameter("@FolderID", SqlDbType.Int);
                    parameters[0].Value = folderId;
                    parameters[1] = new SqlParameter("@ModifiedBy", SqlDbType.NVarChar, 50);
                    parameters[1].Value = modifyBy;
                    parameters[2] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[2].Value = System.DateTime.Now;
                    parameters[3] = new SqlParameter("@Action", SqlDbType.TinyInt);
                    parameters[3].Value = action;

                    command.Parameters.AddRange(parameters);
                    command.ExecuteNonQuery();
                    return parameters[3].Value.ToString();
                }
                catch (Exception ex)
                {
                    return string.Empty;
                }
                finally
                {
                    connection.Close();
                }
        }
        #endregion

        #region CheckList
        /// <summary>
        /// Get data CheckList QuestionsWithAnswers
        /// </summary>
        /// <param name="folderid"></param>
        /// <returns></returns>
        public static DataTable GetCheckListQuestionsWithAnswers(string folderid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetCheckListQuestionsWithAnswers_NEW";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@folderId", folderid);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetDefaultFolderLogQuestion(string folderid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetDefaultFolderLogQuestion";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@folderId", folderid);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataSet GetQns(int createdon, string name, string folderId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetQNS";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@createdOn", createdon);
                    SqlParameter param2 = command.Parameters.AddWithValue("@name", name);
                    SqlParameter param3 = command.Parameters.AddWithValue("@folderId", folderId);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataSet table = new DataSet();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Gets CheckListAnswers
        /// </summary>
        /// <param name="ans"></param>
        /// <param name="qnsId"></param>
        /// <returns></returns>
        public static string UpdateQns(string ans, string qnsId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateQns";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@ansId", SqlDbType.VarChar, 50);
                    parameters[0].Value = qnsId;
                    parameters[1] = new SqlParameter("@ans", SqlDbType.NVarChar, 2000);
                    parameters[1].Value = ans;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string InsertQns(string ans, string question, int folderId, string userid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertQns";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[4];
                    parameters[0] = new SqlParameter("@ans", SqlDbType.VarChar, 200);
                    parameters[0].Value = ans;
                    parameters[1] = new SqlParameter("@question", SqlDbType.NVarChar, 200);
                    parameters[1].Value = question;
                    parameters[2] = new SqlParameter("@folderId", SqlDbType.Int, 200);
                    parameters[2].Value = folderId;
                    parameters[3] = new SqlParameter("@userid", SqlDbType.NVarChar, 200);
                    parameters[3].Value = userid;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string InsertFolderlog(string folderID, string logName)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertFolderlog";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@folderId", SqlDbType.Int);
                    parameters[0].Value = folderID;
                    parameters[1] = new SqlParameter("@logName", SqlDbType.NVarChar, 100);
                    parameters[1].Value = logName;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataSet GetFolderlog(string folderID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetFolderLogId";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@folderId", SqlDbType.Int);
                    parameters[0].Value = folderID;

                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataSet dataset = new DataSet();

                    dataAdapter.Fill(dataset);

                    return dataset;

                    //var value = command.ExecuteScalar();
                    //return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return new DataSet();
                }
                finally
                {
                    connection.Close();
                }
        }




        public static string InsertQuestion(DataTable dt, string folderId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertQuestion";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@tblChecklistQuestion", SqlDbType.Structured);
                    parameters[0].Value = dt;
                    parameters[1] = new SqlParameter("@FolderId", SqlDbType.Int);
                    parameters[1].Value = folderId;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);

                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Update CheckListAnswers
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string CheckListAnswersUpdate(string answer, int id)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateCheckListAnswers";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@ID", SqlDbType.Int);
                    parameters[0].Value = id;
                    parameters[1] = new SqlParameter("@Answer", SqlDbType.NVarChar, 2000);
                    parameters[1].Value = answer;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }


        public static DataSet QnsInsetandUpdate(DataTable dt, string isCreate, DateTime Createdon, Guid userid, string folderId, int folderLogId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "Insertqns_1";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[6];
                    parameters[0] = new SqlParameter("@tblQns", SqlDbType.Structured);
                    parameters[0].Value = dt;
                    parameters[1] = new SqlParameter("@IsCreate", SqlDbType.Int);
                    parameters[1].Value = Convert.ToInt32(isCreate);
                    parameters[2] = new SqlParameter("@createDateTime", SqlDbType.DateTime);
                    parameters[2].Value = Createdon;
                    parameters[3] = new SqlParameter("@UserId", SqlDbType.UniqueIdentifier);
                    parameters[3].Value = userid;
                    parameters[4] = new SqlParameter("@folderId", SqlDbType.VarChar);
                    parameters[4].Value = folderId;
                    parameters[5] = new SqlParameter("@folderLogID", SqlDbType.Int);
                    parameters[5].Value = folderLogId;

                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataSet dataset = new DataSet();

                    dataAdapter.Fill(dataset);

                    return dataset;

                }
                catch (Exception ex)
                {
                    return new DataSet();
                }
                finally
                {
                    connection.Close();
                }
        }



        public static DataSet GetFolderLogDetailsByFolderId(string folderID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetFolderLogDetailsByFolderId";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@folderId", SqlDbType.Int);
                    parameters[0].Value = folderID;
                    command.Parameters.AddRange(parameters);
                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;
                    DataSet dataset = new DataSet();
                    dataAdapter.Fill(dataset);
                    return dataset;
                }
                catch (Exception ex)
                {
                    return new DataSet();
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataSet GetFolderlogDetailsById(string folderID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetFolderLogDetailsById";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@folderId", SqlDbType.Int);
                    parameters[0].Value = folderID;
                    command.Parameters.AddRange(parameters);
                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;
                    DataSet dataset = new DataSet();
                    dataAdapter.Fill(dataset);

                    return dataset;
                }
                catch (Exception ex)
                {
                    return new DataSet();
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetFolderIdByOfficeID(string officeId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetFolderIdByOfficeID";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@OfficeID1", SqlDbType.NVarChar, 50);
                    parameters[0].Value = officeId;

                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable dt = new DataTable();

                    dataAdapter.Fill(dt);

                    return dt;
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                    connection.Close();
                }
        }


        public static DataTable GetQuestionByFolderLogID(int folderLogId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetQuestionDetailsByFolderLogId";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@folderLogId", SqlDbType.Int);
                    parameters[0].Value = folderLogId;
                    command.Parameters.AddRange(parameters);
                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;
                    DataTable dt = new DataTable();
                    dataAdapter.Fill(dt);
                    return dt;
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                    connection.Close();
                }
        }


        public static DataSet SearchCheckListQuestionsWithAnswers(string folderid, string userName, DateTime? fromDate, DateTime? toDate, int folderLogId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    //command.CommandText = "GetCheckListQuestionsWithAnswers_NEW";
                    command.CommandText = "SearchCheckListQuestionsWithAnswers";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[5];
                    parameters[0] = new SqlParameter("@folderId", SqlDbType.VarChar);
                    parameters[0].Value = folderid;
                    parameters[1] = new SqlParameter("@userName", SqlDbType.NVarChar);
                    parameters[1].Value = userName;
                    parameters[2] = new SqlParameter("@fromDate", SqlDbType.DateTime);
                    parameters[2].Value = fromDate;
                    parameters[3] = new SqlParameter("@toDate", SqlDbType.DateTime);
                    parameters[3].Value = toDate;
                    parameters[4] = new SqlParameter("@folderLogID", SqlDbType.Int);
                    parameters[4].Value = folderLogId;

                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataSet dataset = new DataSet();

                    dataAdapter.Fill(dataset);

                    return dataset;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }


        public static DataSet GetFolderLogDetails(int folderLogId, int folderId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetFolderLogDetails";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@folderId", SqlDbType.Int);
                    parameters[0].Value = folderId;
                    parameters[1] = new SqlParameter("@folderLogID", SqlDbType.Int);
                    parameters[1].Value = folderLogId;
                    command.Parameters.AddRange(parameters);
                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;
                    DataSet dataset = new DataSet();
                    dataAdapter.Fill(dataset);

                    return dataset;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }


        public static string UpdateQuestion(int questionId, string folderId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateCheckListAnswers";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@ID", SqlDbType.Int);
                    parameters[0].Value = questionId;
                    parameters[1] = new SqlParameter("@folderId", SqlDbType.NVarChar);
                    parameters[1].Value = folderId;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }



        public static string deleteAnswer(DataTable dt)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DeleteCheckListAnswers";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@tblAns", SqlDbType.Structured);
                    parameters[0].Value = dt;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }
        public static string UpdateFolderLog(DataTable dt, DateTime Createdon, string folderId, int folderLogID, string logName)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateFolderLog";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[5];
                    parameters[0] = new SqlParameter("@updateQuestion", SqlDbType.Structured);
                    parameters[0].Value = dt;
                    parameters[1] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    parameters[1].Value = Createdon;
                    parameters[2] = new SqlParameter("@folderId", SqlDbType.VarChar);
                    parameters[2].Value = folderId;
                    parameters[3] = new SqlParameter("@folderLogID", SqlDbType.Int);
                    parameters[3].Value = folderLogID;
                    parameters[4] = new SqlParameter("@LogName", SqlDbType.NVarChar, 100);
                    parameters[4].Value = logName;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }
        #endregion

        #region DocumentClass
        /// <summary>
        /// Update Document Class column
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="className"></param>
        /// <returns></returns>
        public static string DocumentClassUpdate(string documentId, string className)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DocumentClassUpdate";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@DocumentId", SqlDbType.Int);
                    parameters[0].Value = documentId;
                    parameters[1] = new SqlParameter("@Class", SqlDbType.NVarChar, 50);
                    parameters[1].Value = className;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        //public static int DocumentInsertforArchive(string FileName, string path, int folderId, float size)
        //{
        //    using (SqlConnection connection = new SqlConnection(ConnectionString))
        //        try
        //        {
        //            connection.Open();
        //            SqlCommand command = new SqlCommand();
        //            command.Connection = connection;
        //            command.CommandText = "DocumentInsertForArchive";
        //            command.CommandType = CommandType.StoredProcedure;

        //            SqlParameter[] parameters = new SqlParameter[5];
        //            parameters[0] = new SqlParameter("@FileName", SqlDbType.NVarChar, 250);
        //            parameters[0].Value = FileName;
        //            parameters[1] = new SqlParameter("@PathName", SqlDbType.NVarChar, 350);
        //            parameters[1].Value = path;
        //            parameters[2] = new SqlParameter("@FolderId", SqlDbType.Int);
        //            parameters[2].Value = folderId;
        //            parameters[3] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
        //            parameters[3].Value = System.DateTime.Now;
        //            parameters[4] = new SqlParameter("@Size", SqlDbType.NChar, 10);
        //            parameters[4].Value = size;

        //            command.Parameters.AddRange(parameters);
        //            var value = command.ExecuteScalar();
        //            return Convert.ToInt32(value);
        //        }
        //        catch (Exception ex)
        //        {
        //            return 0;
        //        }
        //        finally
        //        {
        //            connection.Close();
        //        }
        //}

        /// <summary>
        /// Insert Document
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="FolderID"></param>
        /// <param name="UID"></param>
        /// <param name="path"></param>
        /// <param name="DividerID"></param>
        /// <param name="pageCount"></param>
        /// <param name="size"></param>
        /// <param name="fileExtention"></param>
        /// <param name="documentOrder"></param>
        /// <param name="className"></param>
        /// <returns></returns>
        public static string DocumentsInsert(string FileName, int FolderID, string UID, string path, int DividerID, int pageCount, float size, string displayName, int fileExtention, int documentOrder, string className, string fullFileName = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DocumentsInsert";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[14];
                    parameters[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 100);
                    parameters[0].Value = FileName;
                    parameters[1] = new SqlParameter("@FileFolderID", SqlDbType.Int);
                    parameters[1].Value = FolderID;
                    parameters[2] = new SqlParameter("@DOB", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;
                    parameters[3] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                    parameters[3].Value = UID;
                    parameters[4] = new SqlParameter("@PathName", SqlDbType.NVarChar, 100);
                    parameters[4].Value = path;
                    parameters[5] = new SqlParameter("@PagesCount", SqlDbType.Int);
                    parameters[5].Value = pageCount;
                    parameters[6] = new SqlParameter("@Size", SqlDbType.Float);
                    parameters[6].Value = size;
                    parameters[7] = new SqlParameter("@DividerID", SqlDbType.Int);
                    parameters[7].Value = DividerID;
                    parameters[8] = new SqlParameter("@FileExtention", SqlDbType.Int);
                    parameters[8].Value = fileExtention;
                    parameters[9] = new SqlParameter("@GUID", SqlDbType.NVarChar, 50);
                    parameters[9].Value = "";
                    parameters[10] = new SqlParameter("@DisplayName", SqlDbType.NVarChar, 100);
                    parameters[10].Value = displayName;
                    parameters[11] = new SqlParameter("@DocumentOrder", SqlDbType.Int);
                    parameters[11].Value = documentOrder;
                    parameters[12] = new SqlParameter("@Class", SqlDbType.NVarChar, 50);
                    parameters[12].Value = className;
                    parameters[13] = new SqlParameter("@FullFileName", SqlDbType.NVarChar, 100);
                    parameters[13].Value = fullFileName;


                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Get documents by Filter
        /// </summary>
        /// <param name="param"></param>
        /// <param name="proc"></param>
        /// <returns></returns>
        public static DataSet GetFilteredDocs(Dictionary<KeyValuePairSearchFileFolderBox, SqlDbType> param, string proc)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    DataSet ds = new DataSet();
                    connection.Open();
                    SqlCommand command = new SqlCommand();

                    command.Connection = connection;
                    command.CommandText = proc;
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter ad = new SqlDataAdapter(command);


                    foreach (KeyValuePair<KeyValuePairSearchFileFolderBox, SqlDbType> entry in param)
                    {
                        command.Parameters.Add("@" + entry.Key.key, entry.Value);
                        command.Parameters["@" + entry.Key.key].Value = entry.Key.value;
                    }

                    ad.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    return new DataSet();
                }
                finally
                {
                    connection.Close();
                }
        }

        public class KeyValuePairSearchFileFolderBox
        {
            public string key { get; set; }
            public string value { get; set; }
        }
        #endregion

        #region OpenAuth

        /// <summary>
        /// To check whther the Email exists or not for Social Login
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static DataSet IsEmailExists(string email)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    bool count;
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "CheckIfEmailExists";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@Email", SqlDbType.NVarChar, 256);
                    parameters[0].Value = email;

                    command.Parameters.AddRange(parameters);
                    DataSet dataSet = new DataSet();
                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;
                    dataAdapter.Fill(dataSet);
                    return dataSet;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        #endregion

        #region Update FileFolder
        /// <summary>
        /// Update FileFolder For Alert (for use eScanner)
        /// </summary>
        /// <param name="folderId"></param>
        /// <param name="alert1"></param>
        /// <param name="alert2"></param>
        /// <returns></returns>
        public static string UpdateFileFolder(string folderId, string alert1, string alert2)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateFileFolderForAlert";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@FolderId", SqlDbType.Int);
                    parameters[0].Value = folderId;
                    parameters[1] = new SqlParameter("@Alert1", SqlDbType.NVarChar, 50);
                    parameters[1].Value = alert1;
                    parameters[2] = new SqlParameter("@Alert2", SqlDbType.NVarChar, 50);
                    parameters[2].Value = alert2;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Retrives row from document table 
        /// </summary>
        /// <param name="ids">Unique ID of document to be retrived</param>
        /// <returns></returns>
        public static DataTable GetDocumentFromDocumentID(int id)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetDocumentFromDocumentID";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@DocumentID", id);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.Int32;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }
        #endregion

        #region Index
        public static string InsertIndexValues(DataTable dt, string deletedIndexData)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "IndexInsert";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@TempIndexPairs", SqlDbType.Structured);
                    parameters[0].Value = dt;
                    parameters[1] = new SqlParameter("@DeletedIndex", DbType.String);
                    parameters[1].Value = deletedIndexData;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetIndexValues(int documentId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetIndexValues";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@DocumentID", documentId);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.Int32;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }
        #endregion

        #region WorkAreaIndex
        public static DataTable GetWorkAreaIndexKeys(string UID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetWorkAreaIndexKeys";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                    parameters[0].Value = UID;

                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable SearchWorkAreaDocs(string docName, string indexVal, string selectedPath, string UID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "SearchWorkAreaDocs";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[4];
                    parameters[0] = new SqlParameter("@DocName", SqlDbType.NVarChar, 50);
                    parameters[0].Value = docName;

                    parameters[1] = new SqlParameter("@IndexVal", SqlDbType.NVarChar, 50);
                    parameters[1].Value = indexVal;

                    parameters[2] = new SqlParameter("@SelectedPath", SqlDbType.NVarChar, 50);
                    parameters[2].Value = selectedPath;

                    parameters[3] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                    parameters[3].Value = UID;

                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string InsertWorkAreaIndexValues(DataTable dt, string deletedIndexData, int workAreaId, string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "IndexInsertWorkArea";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[4];
                    parameters[0] = new SqlParameter("@TempIndexPairsWorkArea", SqlDbType.Structured);
                    parameters[0].Value = dt;
                    parameters[1] = new SqlParameter("@DeletedIndex", DbType.String);
                    parameters[1].Value = deletedIndexData;
                    parameters[2] = new SqlParameter("@WorkAreaId", DbType.Int32);
                    parameters[2].Value = workAreaId;
                    parameters[3] = new SqlParameter("@UID", DbType.String);
                    parameters[3].Value = uid;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string DeleteWorkAreaIndexValues(int workareaId, string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DeleteIndexWorkArea";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@WorkAreaId", SqlDbType.Int);
                    parameters[0].Value = workareaId;
                    parameters[1] = new SqlParameter("@UID", DbType.String);
                    parameters[1].Value = uid;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetWorkAreaIndexValues(int workareaId, string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetWorkAreaIndexValues";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@WorkAreaId", workareaId);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlParameter param2 = command.Parameters.AddWithValue("@UID", uid);
                    param2.Direction = ParameterDirection.Input;
                    param2.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }
        #endregion

        #region FolderTemplate
        public static DataTable GetFoldersByTemplateId(int templateId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetFoldersByTemplateId";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@TemplateId", templateId);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.Int32;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static void AddDividers(DataTable dividers)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "AddTabsAfterUpdatingTemplate";
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@tblDividers", dividers);
                    command.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetFolderTemplateByTemplateID(int templateId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetFolderTemplateByTemplateID";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@TemplateID", SqlDbType.Int);
                    parameters[0].Value = templateId;
                    command.Parameters.AddRange(parameters);
                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;
                    DataTable dt = new DataTable();
                    dataAdapter.Fill(dt);
                    return dt;
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string UpdateLockForSelectedTemplates(DataTable lockedFolderTemplates)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "LockFolderTemplate";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@tblLockTemplate", SqlDbType.Structured);
                    parameters[0].Value = lockedFolderTemplates;
                    parameters[1] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[1].Value = DateTime.Now;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }
        #endregion

        #region CheckFolderLogExists
        /// <summary>
        /// Check if the FolderLog Exists according to Folder ID
        /// </summary>
        /// <param name="folderId"></param>
        /// <returns></returns>
        public static bool CheckFolderLogExists(int folderId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    bool count;
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "CheckIffolderLogExists";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@FolderId", SqlDbType.Int);
                    parameters[0].Value = folderId;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    if (value != null)
                    {
                        count = true;
                        return count;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
                finally
                {
                    connection.Close();
                }
        }
        #endregion

        #region Account
        /// <summary>
        /// Get UId through User Name from account table
        /// </summary>
        /// <param name="folderid"></param>
        /// <returns></returns>
        public static DataTable GetUserIdFromUserName(string name)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetUserIdFromUserName";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@Name", name);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static bool ChangeUserMode(string uid, bool isDisabled)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "ChangeUserMode";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                    parameters[0].Value = uid;
                    parameters[1] = new SqlParameter("@IsDisabled", SqlDbType.Bit);
                    parameters[1].Value = isDisabled;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    if (Convert.ToInt32(value) > 0)
                        return true;
                    else
                        return false;
                }
                catch (Exception ex)
                {
                    return false;
                }
                finally
                {
                    connection.Close();
                }
        }

        #endregion

        #region eFileFlow
        /// <summary>
        /// UpdateFileFlow
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string UpdateEdForms(string name, int eFileFlowid, int isDeleted, int IsRename)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateEdForms";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[5];
                    parameters[0] = new SqlParameter("@eFileFlowId", SqlDbType.Int);
                    parameters[0].Value = eFileFlowid;
                    parameters[1] = new SqlParameter("@Name", SqlDbType.NVarChar, 100);
                    parameters[1].Value = name;
                    parameters[2] = new SqlParameter("@IsDeleted", SqlDbType.Bit);
                    parameters[2].Value = isDeleted;
                    parameters[3] = new SqlParameter("@IsRename", SqlDbType.Bit);
                    parameters[3].Value = IsRename;
                    parameters[4] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[4].Value = DateTime.Now;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetEdFormsById(int id)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetEdFormsById";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@EdFormsId", id);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.Int32;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// DocumentsInsert in eFileFlow Table
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="UID"></param>
        /// <param name="size"></param>
        /// <param name="isDeleted"></param>
        /// <returns></returns>
        public static string DocumentsInsertForEdForms(string FileName, Guid UID, decimal size, int isDeleted)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DocumentsInsertForEdForms";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[5];
                    parameters[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 100);
                    parameters[0].Value = FileName;
                    parameters[1] = new SqlParameter("@UID", SqlDbType.UniqueIdentifier);
                    parameters[1].Value = UID;
                    parameters[2] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;
                    parameters[3] = new SqlParameter("@Size", SqlDbType.Decimal);
                    parameters[3].Value = size;
                    parameters[4] = new SqlParameter("@IsDeleted", SqlDbType.Bit);
                    parameters[4].Value = isDeleted;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        ///  Get eFileFlowDocuments
        /// </summary>
        /// <param name="folderid"></param>
        /// <returns></returns>
        public static DataTable GetEdForms(Guid UID, string searchKey = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GeteFileFlowDocuments";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@UID", UID);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.Guid;
                    SqlParameter param2 = command.Parameters.AddWithValue("@Search", searchKey);
                    param2.Direction = ParameterDirection.Input;
                    param2.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetEdFormsUserById(int id)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetEdFormsUserById";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@EdFormsUserId", id);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.Int32;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        #endregion

        #region eFileFlowShare
        /// <summary>
        /// eFileFlow ShareMail
        /// </summary>
        /// <param name="eFileFlowId"></param>
        /// <param name="fileName"></param>
        /// <param name="strToMail"></param>
        /// <param name="MailTitle"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static void eFileFlowShareMail(Guid eFileFlowShareId, int eFileFlowId, string fileName, string strToMail, string MailTitle, int status, int isDeleted, decimal size)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    // DateTime? someDate = null;
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InserteFileFlow_ShareMail";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[9];
                    parameters[0] = new SqlParameter("@eFileFlowShareId", SqlDbType.UniqueIdentifier);
                    parameters[0].Value = eFileFlowShareId;
                    parameters[1] = new SqlParameter("@eFileFlowId", SqlDbType.Int);
                    parameters[1].Value = eFileFlowId;
                    parameters[2] = new SqlParameter("@FileName", SqlDbType.NVarChar, 100);
                    parameters[2].Value = fileName;
                    parameters[3] = new SqlParameter("@ToMail", SqlDbType.NVarChar, 500);
                    parameters[3].Value = strToMail;
                    parameters[4] = new SqlParameter("@MailTitle", SqlDbType.NVarChar, 50);
                    parameters[4].Value = MailTitle;
                    parameters[5] = new SqlParameter("@Date", SqlDbType.DateTime);
                    parameters[5].Value = System.DateTime.Now;
                    parameters[6] = new SqlParameter("@Status", SqlDbType.TinyInt);
                    parameters[6].Value = status;
                    parameters[7] = new SqlParameter("@IsDeleted", SqlDbType.Bit);
                    parameters[7].Value = isDeleted;
                    parameters[8] = new SqlParameter("@Size", SqlDbType.Decimal);
                    parameters[8].Value = size;

                    command.Parameters.AddRange(parameters);
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Get Pending Files From shareIDs 
        /// </summary>
        /// <param name="ids">Share ID</param>
        /// <returns></returns>
        public static DataTable GetPendingFilesFromeShareIds(string ids)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetPendingFilesFromeShareIds";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@shareIds", ids);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string UpdateShare(string filename, Guid shareId, int documentid, int workareaid, int isDeleted, int isRename, int isMove, int status = 0)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateFileFlowShare";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[9];
                    parameters[0] = new SqlParameter("@ShareId", SqlDbType.UniqueIdentifier);
                    parameters[0].Value = shareId;
                    parameters[1] = new SqlParameter("@FileName", SqlDbType.NVarChar, 100);
                    parameters[1].Value = filename;
                    parameters[2] = new SqlParameter("@IsDeleted", SqlDbType.Bit);
                    parameters[2].Value = isDeleted;
                    parameters[3] = new SqlParameter("@IsRename", SqlDbType.Bit);
                    parameters[3].Value = isRename;

                    parameters[4] = new SqlParameter("@DocumentId", SqlDbType.Int);
                    if (documentid == 0)
                        parameters[4].Value = DBNull.Value;
                    else
                        parameters[4].Value = documentid;

                    parameters[5] = new SqlParameter("@IsMove", SqlDbType.Bit);
                    parameters[5].Value = isMove;
                    parameters[6] = new SqlParameter("@Modifydate", SqlDbType.DateTime);
                    parameters[6].Value = DateTime.Now;

                    parameters[7] = new SqlParameter("@WorkAreaId", SqlDbType.Int);
                    if (workareaid == 0)
                        parameters[7].Value = DBNull.Value;
                    else
                        parameters[7].Value = workareaid;

                    parameters[8] = new SqlParameter("@Status", SqlDbType.TinyInt);
                    parameters[8].Value = status;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Documents Insert For WorkArea
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="UID"></param>
        /// <param name="size"></param>
        /// <param name="isDeleted"></param>
        /// <returns></returns>
        public static string DocumentsInsertForWorkArea(string FileName, string UID, string UploadedBy, decimal size, int isDeleted, string pathName, int type = 1, int workAreaTreeId = 0)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DocumentsInsertForWorkArea";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[9];
                    parameters[0] = new SqlParameter("@FileName", SqlDbType.NVarChar, 250);
                    parameters[0].Value = FileName;
                    parameters[1] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                    parameters[1].Value = UID;
                    parameters[2] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;
                    parameters[3] = new SqlParameter("@Size", SqlDbType.Decimal);
                    parameters[3].Value = size;
                    parameters[4] = new SqlParameter("@IsDeleted", SqlDbType.Bit);
                    parameters[4].Value = isDeleted;
                    parameters[5] = new SqlParameter("@PathName", SqlDbType.NVarChar, 250);
                    parameters[5].Value = pathName;
                    parameters[6] = new SqlParameter("@Type", SqlDbType.Int);
                    parameters[6].Value = type;
                    parameters[7] = new SqlParameter("@UploadedBy", SqlDbType.NVarChar, 50);
                    parameters[7].Value = UploadedBy;
                    parameters[8] = new SqlParameter("@WorkAreaTreeId", SqlDbType.Int);
                    parameters[8].Value = workAreaTreeId;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Documents Insert For WorkArea
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="UID"></param>
        /// <param name="size"></param>
        /// <param name="isDeleted"></param>
        /// <returns></returns>
        public static string DocumentsInsertForLogForm(string FileName, string UID, DateTime CreatedDate, int FolderID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DocumentsInsertForLogForm";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[4];
                    parameters[0] = new SqlParameter("@FileName", SqlDbType.NVarChar, 250);
                    parameters[0].Value = FileName;
                    parameters[1] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                    parameters[1].Value = UID;
                    parameters[2] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;
                    parameters[3] = new SqlParameter("@FolderID", SqlDbType.Int);
                    parameters[3].Value = FolderID;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string UpdateLogForm(DateTime ModifyDate, int id)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateLogFormDetails";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@id", SqlDbType.Int);
                    parameters[0].Value = id;
                    parameters[1] = new SqlParameter("@ModifyDate", SqlDbType.DateTime);
                    parameters[1].Value = DateTime.Now;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string InsertLogForLogForm(DateTime modifyDate, int logFormId, string savedValues, string documentPath, string UID, bool isError, string errorMsg = "")
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertLogFormLogs";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[7];
                    parameters[0] = new SqlParameter("@logFormId", SqlDbType.Int);
                    parameters[0].Value = logFormId;
                    parameters[1] = new SqlParameter("@savedValues", SqlDbType.NVarChar);
                    parameters[1].Value = savedValues;
                    parameters[2] = new SqlParameter("@documentPath", SqlDbType.NVarChar, 500);
                    parameters[2].Value = documentPath;
                    parameters[3] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                    parameters[3].Value = UID;
                    parameters[4] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[4].Value = modifyDate;
                    parameters[5] = new SqlParameter("@IsError", SqlDbType.Bit);
                    parameters[5].Value = isError;
                    parameters[6] = new SqlParameter("@ErrorMsg", SqlDbType.NVarChar);
                    parameters[6].Value = errorMsg;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetLogFormLogs(int logformId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetLogFormLogs";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@logFormId", logformId);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.Int32;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Insert EDFormsUserShare Logs
        /// </summary>
        /// <param name="modifyDate"></param>
        /// <param name="edFormsUserShareId"></param>
        /// <param name="savedValues"></param>
        /// <param name="isError"></param>
        /// <param name="errorMsg"></param>
        /// <returns></returns>
        public static string InsertEdFormsUserShareLogs(DateTime modifyDate, string edFormsUserShareId, string savedValues, bool isError, string errorMsg = "")
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertEdFormsUserShareLogs";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[5];
                    parameters[0] = new SqlParameter("@EdFormsUserShareId", SqlDbType.NVarChar);
                    parameters[0].Value = edFormsUserShareId;
                    parameters[1] = new SqlParameter("@savedValues", SqlDbType.NVarChar);
                    parameters[1].Value = savedValues;
                    parameters[2] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[2].Value = modifyDate;
                    parameters[3] = new SqlParameter("@IsError", SqlDbType.Bit);
                    parameters[3].Value = isError;
                    parameters[4] = new SqlParameter("@ErrorMsg", SqlDbType.NVarChar);
                    parameters[4].Value = errorMsg;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Get EDFormsUserShare Logs
        /// </summary>
        /// <param name="edFormsUserShareId"></param>
        /// <returns></returns>
        public static DataTable GetEdFormsUserShareLogs(string edFormsUserShareId, string modifiedDate = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetEdFormsUserShareLogs";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@EdFormsUserShareId", edFormsUserShareId);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlParameter param2 = command.Parameters.AddWithValue("@ModifiedDate", modifiedDate);
                    param2.Direction = ParameterDirection.Input;
                    param2.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetDepartmentbyEdFormUserId(int? edFormsUserId, int? edFormsId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetDepartmentbyEdFormUserId";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@EdFormsUserId", edFormsUserId);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.Int32;

                    SqlParameter param2 = command.Parameters.AddWithValue("@EdFormsId", edFormsId);
                    param2.Direction = ParameterDirection.Input;
                    param2.DbType = DbType.Int32;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        ///  Get eFileFlowDocuments
        /// </summary>
        /// <param name="folderid"></param>
        /// <returns></returns>
        public static DataTable GetLogFormDocs(string UID, int folderID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetLogFormDocs";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@UID", UID);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlParameter param2 = command.Parameters.AddWithValue("@FolderID", folderID);
                    param2.Direction = ParameterDirection.Input;
                    param2.DbType = DbType.Int32;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetLogFormDocsAdmin()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetLogFormDocsAdmin";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetFilteredLogForms(string uid, string fileName, string folderName)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetFilteredLogForms";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@UID", uid);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlParameter param2 = command.Parameters.AddWithValue("@Filename", fileName);
                    param2.Direction = ParameterDirection.Input;
                    param2.DbType = DbType.String;

                    SqlParameter param3 = command.Parameters.AddWithValue("@FolderName", folderName);
                    param3.Direction = ParameterDirection.Input;
                    param3.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string DeleteLogForm(int id, string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DeleteLogForm";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@ID", DbType.Int32);
                    parameters[0].Value = id;
                    parameters[1] = new SqlParameter("@UID", DbType.String);
                    parameters[1].Value = uid;
                    parameters[2] = new SqlParameter("@ModifyDate", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetLogFormDetails(int logFormID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetLogFormDetails";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@id", logFormID);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.Int32;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Get WorkArea Documents
        /// </summary>
        /// <param name="UID"></param>
        /// <returns></returns>
        public static DataTable GetWorkAreaDocuments(string UID = null, string keyName = "", string objectName = "")
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    keyName = string.Join(",", keyName.Split(new char[0], StringSplitOptions.RemoveEmptyEntries));
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetWorkAreaDocuments";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@UID", UID);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlParameter param2 = command.Parameters.AddWithValue("@Key", keyName);
                    param2.Direction = ParameterDirection.Input;
                    param2.DbType = DbType.String;

                    SqlParameter param3 = command.Parameters.AddWithValue("@ObjectName", objectName);
                    param3.Direction = ParameterDirection.Input;
                    param3.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Get workarea folders list by UID
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static DataTable GetWorkAreaTreeList(string uid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "GetWorkAreaTreeByUID";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                parameters[0].Value = uid;

                command.Parameters.AddRange(parameters);

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;

                DataTable dt = new DataTable();

                dataAdapter.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Creates a new folder for workarea
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="pathName"></param>
        /// <param name="uid"></param>
        public static int CreateFolderForWorkArea(string folderName, string pathName, string uid, int level = 0, int parentId = 0, int workAreaTreeId = 0)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "CreateFolderForWorkArea";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[6];
                    parameters[0] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                    parameters[0].Value = uid;
                    parameters[1] = new SqlParameter("@FolderName", SqlDbType.NVarChar, 150);
                    parameters[1].Value = folderName;
                    parameters[2] = new SqlParameter("@PathName", SqlDbType.NVarChar, 250);
                    parameters[2].Value = pathName;
                    parameters[3] = new SqlParameter("@WorkAreaTreeId", SqlDbType.Int);
                    parameters[3].Value = workAreaTreeId;
                    parameters[4] = new SqlParameter("@Level", SqlDbType.Int);
                    parameters[4].Value = level;
                    parameters[5] = new SqlParameter("@ParentId", SqlDbType.Int);
                    parameters[5].Value = parentId;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToInt32(value);
                }
                catch (Exception ex)
                {
                    return 0;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Get WorkArea Documents
        /// </summary>
        /// <param name="UID"></param>
        /// <returns></returns>
        public static DataTable GetWorkAreaDocumentsTest(string UID, string keyName = "", string objectName = "")
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    keyName = string.Join(",", keyName.Split(new char[0], StringSplitOptions.RemoveEmptyEntries));
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetWorkAreaDocuments_Back";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@UID", UID);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlParameter param2 = command.Parameters.AddWithValue("@Key", keyName);
                    param2.Direction = ParameterDirection.Input;
                    param2.DbType = DbType.String;

                    SqlParameter param3 = command.Parameters.AddWithValue("@ObjectName", objectName);
                    param3.Direction = ParameterDirection.Input;
                    param3.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }


        /// <summary>
        /// Get WorkArea Document for rename and delete operation
        /// </summary>
        /// <param name="UID">Unique idenitifer of user</param>
        /// <returns></returns>
        public static DataTable GetWorkAreaDocumentForCRUD(string UID, string fileName, string filePath)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {

                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetWorkAreaDocumentForCRUD";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@UID", UID);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlParameter param2 = command.Parameters.AddWithValue("@FileName", fileName);
                    param2.Direction = ParameterDirection.Input;
                    param2.DbType = DbType.String;

                    SqlParameter param3 = command.Parameters.AddWithValue("@Path", filePath);
                    param3.Direction = ParameterDirection.Input;
                    param3.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Update WorkArea
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="WorkAreaId"></param>
        /// <param name="documentid"></param>
        /// <param name="isDeleted"></param>
        /// <param name="isRename"></param>
        /// <param name="isMove"></param>
        /// <returns></returns>
        public static string UpdateWorkArea(string filename, int WorkAreaId, int documentid, int isDeleted, int isRename, int isMove, string newFilePath = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateWorkArea";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[8];
                    parameters[0] = new SqlParameter("@WorkAreaId", SqlDbType.Int);
                    parameters[0].Value = WorkAreaId;
                    parameters[1] = new SqlParameter("@FileName", SqlDbType.NVarChar, 250);
                    parameters[1].Value = filename;
                    parameters[2] = new SqlParameter("@IsDeleted", SqlDbType.Bit);
                    parameters[2].Value = isDeleted;
                    parameters[3] = new SqlParameter("@IsRename", SqlDbType.Bit);
                    parameters[3].Value = isRename;
                    parameters[4] = new SqlParameter("@DocumentId", SqlDbType.Int);
                    parameters[4].Value = documentid;
                    parameters[5] = new SqlParameter("@IsMove", SqlDbType.Bit);
                    parameters[5].Value = isMove;
                    parameters[6] = new SqlParameter("@Modifydate", SqlDbType.DateTime);
                    parameters[6].Value = DateTime.Now;
                    parameters[7] = new SqlParameter("@NewFilePath", SqlDbType.NVarChar, 250);
                    if (string.IsNullOrEmpty(newFilePath))
                        parameters[7].Value = DBNull.Value;
                    else
                        parameters[7].Value = newFilePath;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Documents Insert For Inbox
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="UID"></param>
        /// <param name="size"></param>
        /// <param name="isDeleted"></param>
        /// <param name="iPAddress"></param>
        /// <returns></returns>
        public static string DocumentsInsertForInbox(string FileName, string UID, decimal size, int isDeleted, string iPAddress)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DocumentsInsertForInbox";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[6];
                    parameters[0] = new SqlParameter("@FileName", SqlDbType.NVarChar, 100);
                    parameters[0].Value = FileName;
                    parameters[1] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                    parameters[1].Value = UID;
                    parameters[2] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;
                    parameters[3] = new SqlParameter("@Size", SqlDbType.Decimal);
                    parameters[3].Value = size;
                    parameters[4] = new SqlParameter("@IsDeleted", SqlDbType.Bit);
                    parameters[4].Value = isDeleted;
                    parameters[5] = new SqlParameter("@IPAddress", SqlDbType.NVarChar, 20);
                    parameters[5].Value = iPAddress;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable eFileFlowShareGetData(string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    DataTable ds = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter();
                    connection.Open();
                    SqlCommand sqlComm = new SqlCommand("WorkArea_PendingSelectData", connection);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@UID", uid);
                    da.SelectCommand = sqlComm;
                    da.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                    connection.Close();
                }
        }

        public static void eFileFlowShareUpdate(Guid eFileFlowShareId, string FileName, int documentId, int workAreaId, int isDeleted, int isMove, int isRename)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand sqlComm = new SqlCommand("WorkArea_PendingUpdateData", connection);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.Parameters.AddWithValue("@eFileFlowShareId", eFileFlowShareId);
                    sqlComm.Parameters.AddWithValue("@FileName", FileName);
                    sqlComm.Parameters.AddWithValue("@DocumentId", documentId);
                    sqlComm.Parameters.AddWithValue("@WorkAreaId", workAreaId);
                    sqlComm.Parameters.AddWithValue("@IsDeleted", isDeleted);
                    sqlComm.Parameters.AddWithValue("@Modifydate", DateTime.Now);
                    sqlComm.Parameters.AddWithValue("@IsMove", isMove);
                    sqlComm.Parameters.AddWithValue("@IsRename", isRename);

                    sqlComm.ExecuteNonQuery();
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    connection.Close();
                }
        }

        //public static void eFileFlowShareDelete(int eFileFlowId)
        //{
        //    SqlConnection connection = new SqlConnection(ConnectionString);
        //    try
        //    {
        //        connection.Open();
        //        SqlCommand sqlComm = new SqlCommand("WorkArea_PendindDeleteData", connection);
        //        sqlComm.CommandType = CommandType.StoredProcedure;
        //        sqlComm.Parameters.AddWithValue("@eFileFlowId", eFileFlowId);
        //        sqlComm.ExecuteNonQuery();
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        connection.Close();
        //    }
        //}

        /// <summary>
        /// Checks if file exists in share table
        /// </summary>
        /// <param name="shareID">Share ID</param>
        /// <returns></returns>
        public static DataTable CheckSharedIdExists(Guid shareID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    DataTable ds = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter();
                    connection.Open();
                    SqlCommand sqlComm = new SqlCommand("CheckIfShareIdExists", connection);
                    sqlComm.CommandType = CommandType.StoredProcedure;

                    SqlParameter parameter;
                    parameter = sqlComm.Parameters.AddWithValue("@ShareID", shareID);

                    da.SelectCommand = sqlComm;
                    da.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                    connection.Close();
                }
        }


        /// <summary>
        /// Get Inbox Documents
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static DataTable GetInboxDocuments(string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetInboxDocuments";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@UID", uid);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }
        #endregion

        #region Inbox
        /// <summary>
        /// Get InboxFiles From inboxIds 
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public static DataTable GetInboxFilesFrominboxIds(string ids)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetInboxFilesFrominboxIds";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@inboxIds", ids);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }


        /// <summary>
        /// Update Inbox
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="inBoxId"></param>
        /// <param name="documentid"></param>
        /// <param name="isDeleted"></param>
        /// <param name="isRename"></param>
        /// <param name="isMove"></param>
        /// <returns></returns>
        public static string UpdateInbox(string filename, int inBoxId, int documentid, int workareaid, int isDeleted, int isRename, int isMove)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateInbox";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[8];
                    parameters[0] = new SqlParameter("@InboxId", SqlDbType.Int);
                    parameters[0].Value = inBoxId;
                    parameters[1] = new SqlParameter("@FileName", SqlDbType.NVarChar, 100);
                    parameters[1].Value = filename;
                    parameters[2] = new SqlParameter("@IsDeleted", SqlDbType.Bit);
                    parameters[2].Value = isDeleted;
                    parameters[3] = new SqlParameter("@IsRename", SqlDbType.Bit);
                    parameters[3].Value = isRename;

                    parameters[4] = new SqlParameter("@DocumentId", SqlDbType.Int);
                    if (documentid == 0)
                        parameters[4].Value = DBNull.Value;
                    else
                        parameters[4].Value = documentid;

                    parameters[5] = new SqlParameter("@IsMove", SqlDbType.Bit);
                    parameters[5].Value = isMove;
                    parameters[6] = new SqlParameter("@Modifydate", SqlDbType.DateTime);
                    parameters[6].Value = DateTime.Now;

                    parameters[7] = new SqlParameter("@WorkAreaId", SqlDbType.Int);
                    if (workareaid == 0)
                        parameters[7].Value = DBNull.Value;
                    else
                        parameters[7].Value = workareaid;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }
        #endregion

        #region LoginRestriction
        /// <summary>
        /// Inserts login restriction for sub user
        /// </summary>
        /// <param name="uid">unique idenitifer of sub user</param>
        /// <param name="isRestricted">true if IP restriction is enables, false otherwise</param>
        /// <param name="ipAddress">IP Addresses to be validated</param>
        /// <returns></returns>
        public static string InsertLoginRestriction(string uid, bool isRestricted, string ipAddress)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertLoginRestriction";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@uid", SqlDbType.NVarChar, 100);
                    parameters[0].Value = uid;
                    parameters[1] = new SqlParameter("@IsIPRestricted", SqlDbType.Bit);
                    parameters[1].Value = isRestricted;
                    parameters[2] = new SqlParameter("@IPAddresses", SqlDbType.NVarChar, 500);
                    parameters[2].Value = ipAddress;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string InsertPublicHolidayRestriction(string uid, bool isRestricted, string holidayIds)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertPublicHolidayRestriction";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@uid", SqlDbType.NVarChar, 100);
                    parameters[0].Value = uid;
                    parameters[1] = new SqlParameter("@IsTimeRestricted", SqlDbType.Bit);
                    parameters[1].Value = isRestricted;
                    parameters[2] = new SqlParameter("@HolidaIDs", SqlDbType.NVarChar, 50);
                    parameters[2].Value = holidayIds;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Validates if sub user allowed to access system from requested IP
        /// </summary>
        /// <param name="uid">Unique identifier of sub user</param>
        /// <param name="ipAddress">IP Address of http request from where sub user accesses the system</param>
        /// <returns></returns>
        public static bool ValidateIPLoginRestriction(string uid, string ipAddress)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "ValidateIPLoginRestriction";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@uid", SqlDbType.NVarChar, 100);
                    parameters[0].Value = uid;
                    parameters[1] = new SqlParameter("@IPAddress", SqlDbType.NVarChar, 50);
                    parameters[1].Value = ipAddress;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    if (value.ToString() == "False")
                        return false;
                    else
                        return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetIPAddrForSubUser(string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    DataTable ds = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter();
                    connection.Open();
                    SqlCommand sqlComm = new SqlCommand("GetIPAddrForSubUser", connection);
                    sqlComm.CommandType = CommandType.StoredProcedure;

                    SqlParameter parameter;
                    parameter = sqlComm.Parameters.AddWithValue("@uid", uid);

                    da.SelectCommand = sqlComm;
                    da.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetTimeRestForSubUser(string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    DataTable ds = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter();
                    connection.Open();
                    SqlCommand sqlComm = new SqlCommand("GetTimeRestForSubUser", connection);
                    sqlComm.CommandType = CommandType.StoredProcedure;

                    SqlParameter parameter;
                    parameter = sqlComm.Parameters.AddWithValue("@uid", uid);

                    da.SelectCommand = sqlComm;
                    da.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetPublicHolidayRestForSubUser(string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    DataTable ds = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter();
                    connection.Open();
                    SqlCommand sqlComm = new SqlCommand("GetPublicHolidayRestForSubUser", connection);
                    sqlComm.CommandType = CommandType.StoredProcedure;

                    SqlParameter parameter;
                    parameter = sqlComm.Parameters.AddWithValue("@uid", uid);

                    da.SelectCommand = sqlComm;
                    da.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string UpdateTimeRestForSubUser(DataTable dt, string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateTimeLoginRestriction";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@TempTimeRest", SqlDbType.Structured);
                    parameters[0].Value = dt;
                    parameters[1] = new SqlParameter("@uid", DbType.String);
                    parameters[1].Value = uid;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string InsertTimeLoginRest(DataTable dt, string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertTimeLoginRestriction";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@TempTimeRest", SqlDbType.Structured);
                    parameters[0].Value = dt;
                    parameters[1] = new SqlParameter("@uid", DbType.String);
                    parameters[1].Value = uid;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static bool ValidateTimeLoginRestriction(string uid, TimeSpan loginTime, int day)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "ValidateTimeLoginRestriction";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@uid", SqlDbType.NVarChar, 100);
                    parameters[0].Value = uid;
                    parameters[1] = new SqlParameter("@loginTime", SqlDbType.Time, 7);
                    parameters[1].Value = loginTime;
                    parameters[2] = new SqlParameter("@loginDay", SqlDbType.TinyInt);
                    parameters[2].Value = day;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    if (value.ToString() == "False")
                        return false;
                    else
                        return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static void DeleteLoginRestriction(string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DeleteLoginRestriction";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@uid", SqlDbType.NVarChar, 100);
                    parameters[0].Value = uid;

                    command.Parameters.AddRange(parameters);
                    command.ExecuteScalar();

                }
                catch (Exception ex)
                {
                }
                finally
                {
                    connection.Close();
                }
        }
        #endregion

        #region PublicHolidays
        public static DataTable GetPublicHolidays()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetPublicHolidays";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetPublicHolidayDates(int year)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    DataTable ds = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter();
                    connection.Open();
                    SqlCommand sqlComm = new SqlCommand("GetPublicHolidayDates", connection);
                    sqlComm.CommandType = CommandType.StoredProcedure;

                    SqlParameter parameter;
                    parameter = sqlComm.Parameters.AddWithValue("@year", year);

                    da.SelectCommand = sqlComm;
                    da.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string InsertPublicHoliday(string holidayName)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertPublicHoliday";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@HolidayName", SqlDbType.NVarChar, 100);
                    parameters[0].Value = holidayName;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string UpdatePublicHoliday(string newName, int holidayID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdatePublicHoliday";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@HolidayID", DbType.Int16);
                    parameters[0].Value = holidayID;
                    parameters[1] = new SqlParameter("@HolidayName", SqlDbType.NVarChar, 100);
                    parameters[1].Value = newName;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string UpdatePublicHolidayDates(int holidayID, int yearOfHoliday, DateTime holdidayDate)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdatePublicHolidayDates";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@HolidayID", DbType.Int16);
                    parameters[0].Value = holidayID;
                    parameters[1] = new SqlParameter("@HolidayDate", SqlDbType.Date);
                    parameters[1].Value = holdidayDate;
                    parameters[2] = new SqlParameter("@HolidayYear", SqlDbType.Int);
                    parameters[2].Value = yearOfHoliday;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string DeletePublicHoliday(int holidayID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DeletePublicHoliday";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@HolidayID", DbType.Int16);
                    parameters[0].Value = holidayID;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static bool ValidatePublicHolidayRestriction(string uid, string loginTime)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "ValidatePublicHolidayRestriction";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@uid", SqlDbType.NVarChar, 100);
                    parameters[0].Value = uid;
                    parameters[1] = new SqlParameter("@loginDate", SqlDbType.NVarChar, 50);
                    parameters[1].Value = loginTime;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    if (value.ToString() == "False")
                        return false;
                    else
                        return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
                finally
                {
                    connection.Close();
                }
        }

        #endregion

        #region Documents&FolderLogs

        public static string CreateDocumentsAndFolderLogs(Guid uid, int? folderId, int? folderLogId, int? docId, int? dividerId, int? logFormId, int? eFileFlowId, int? workAreaId, string eFileFlowShareId, int? inboxId, int? action, int? subAction, int? type, int? archiveId, int? edFormsId, int? edFormsUserId, string ip = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertDocumentsAndFolderLogs";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[18];
                    parameters[0] = new SqlParameter("@UID", SqlDbType.UniqueIdentifier);
                    parameters[0].Value = uid;
                    parameters[1] = new SqlParameter("@FolderId", SqlDbType.Int);
                    parameters[1].Value = folderId != 0 ? folderId : null;
                    parameters[2] = new SqlParameter("@DividerId", SqlDbType.Int);
                    parameters[2].Value = dividerId != 0 ? dividerId : null;
                    parameters[3] = new SqlParameter("@FolderLogId", SqlDbType.Int);
                    parameters[3].Value = folderLogId != 0 ? folderLogId : null;
                    parameters[4] = new SqlParameter("@LogFormId", SqlDbType.Int);
                    parameters[4].Value = logFormId != 0 ? logFormId : null;
                    parameters[5] = new SqlParameter("@DocumentID", SqlDbType.Int);
                    parameters[5].Value = docId != 0 ? docId : null;
                    parameters[6] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    parameters[6].Value = System.DateTime.Now;
                    parameters[7] = new SqlParameter("@Action", SqlDbType.TinyInt);
                    parameters[7].Value = action;
                    parameters[8] = new SqlParameter("@SubAction", SqlDbType.Int);
                    parameters[8].Value = subAction;
                    parameters[9] = new SqlParameter("@Type", SqlDbType.TinyInt);
                    parameters[9].Value = type;
                    parameters[10] = new SqlParameter("@EFileFlowId", SqlDbType.Int);
                    parameters[10].Value = eFileFlowId != 0 ? eFileFlowId : null;
                    parameters[11] = new SqlParameter("@WorkAreaId", SqlDbType.Int);
                    parameters[11].Value = workAreaId != 0 ? workAreaId : null;
                    parameters[12] = new SqlParameter("@EFileFlowShareId", SqlDbType.NVarChar, 50);
                    parameters[12].Value = !string.IsNullOrEmpty(eFileFlowShareId) ? eFileFlowShareId : null;
                    parameters[13] = new SqlParameter("@InboxId", SqlDbType.Int);
                    parameters[13].Value = inboxId != 0 ? inboxId : null;
                    parameters[14] = new SqlParameter("@ArchiveId", SqlDbType.Int);
                    parameters[14].Value = archiveId != 0 ? archiveId : null;
                    parameters[15] = new SqlParameter("@EdFormsId", SqlDbType.Int);
                    parameters[15].Value = edFormsId != 0 ? edFormsId : null;
                    parameters[16] = new SqlParameter("@EdFormsUserId", SqlDbType.Int);
                    parameters[16].Value = edFormsUserId != 0 ? edFormsUserId : null;
                    parameters[17] = new SqlParameter("@IP", SqlDbType.NVarChar, 50);
                    parameters[17].Value = ip;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
        }

        public static int IsSubUserExist(Guid uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "IsSubUserExist";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@uid", SqlDbType.UniqueIdentifier);
                    parameters[0].Value = uid;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToInt32(value);
                }
                catch (Exception ex)
                {
                    return 5;
                }
        }

        public static DataSet GetAllDocumentAndFolderLogs(string uid, string subUserUID, int? folderId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter();
                    connection.Open();
                    SqlCommand sqlComm = new SqlCommand("GetDocumentsAndFoldersLogs", connection);
                    sqlComm.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = sqlComm.Parameters.AddWithValue("@uid", uid);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlParameter param4 = sqlComm.Parameters.AddWithValue("@subUserID", subUserUID);
                    param4.Direction = ParameterDirection.Input;
                    param4.DbType = DbType.String;

                    SqlParameter param2 = sqlComm.Parameters.AddWithValue("@FolderId", folderId);
                    param2.Direction = ParameterDirection.Input;
                    param2.DbType = DbType.Int32;

                    da.SelectCommand = sqlComm;
                    da.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataSet GetAllShareLinkLogs(string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter();
                    connection.Open();
                    SqlCommand sqlComm = new SqlCommand("GetLinkShareLogs", connection);
                    sqlComm.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = sqlComm.Parameters.AddWithValue("@UID", uid);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    da.SelectCommand = sqlComm;
                    da.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataSet GetAllDocAndFolderLogsForExcel(string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    DataSet ds = new DataSet();
                    SqlDataAdapter da = new SqlDataAdapter();
                    connection.Open();
                    SqlCommand sqlComm = new SqlCommand("GetAllDocumentsAndFolderLogs", connection);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@uid", SqlDbType.NVarChar);
                    parameters[0].Value = uid;
                    sqlComm.Parameters.AddRange(parameters);
                    da.SelectCommand = sqlComm;
                    da.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string GetFileFolderByID(int FolderId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    DataTable ds = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter();
                    connection.Open();
                    SqlCommand sqlComm = new SqlCommand("GetFolderNameOnly", connection);
                    sqlComm.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = sqlComm.Parameters.AddWithValue("@FolderId", FolderId);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.Int32;
                    da.SelectCommand = sqlComm;
                    da.Fill(ds);
                    return ds.Rows[0].ItemArray[0].ToString();
                }
                catch { return null; }
                finally { connection.Close(); }
        }

        public static DataTable GetAllUsers()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetUsersOnly";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetAllSubUsers(string officeUID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetAllSubUserByOfficeUID";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@OfficeUID", SqlDbType.NVarChar);
                    parameters[0].Value = officeUID;
                    command.Parameters.AddRange(parameters);
                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;
                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string CreateUserLogs(Guid modifiedBy, string accountId, int action)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertUserLogs";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[4];
                    parameters[0] = new SqlParameter("@ModifiedBy", SqlDbType.UniqueIdentifier);
                    parameters[0].Value = modifiedBy;
                    parameters[1] = new SqlParameter("@AccountId", SqlDbType.UniqueIdentifier);
                    parameters[1].Value = new Guid(accountId);
                    parameters[2] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    parameters[2].Value = System.DateTime.Now;
                    parameters[3] = new SqlParameter("@Action", SqlDbType.TinyInt);
                    parameters[3].Value = action;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
        }

        #endregion

        #region BatchUpload

        public static DataTable CheckFolderIdExists(string folderName, string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    DataTable ds = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter();
                    connection.Open();
                    SqlCommand sqlComm = new SqlCommand("CheckFolderIdExists", connection);
                    sqlComm.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = sqlComm.Parameters.AddWithValue("@FolderName", folderName);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlParameter param2 = sqlComm.Parameters.AddWithValue("@UID", uid);
                    param2.Direction = ParameterDirection.Input;
                    param2.DbType = DbType.String;

                    da.SelectCommand = sqlComm;
                    da.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string CreateDivider(string dividerName, string officeId, int effId, string color)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "CreateDivider";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[4];
                    parameters[0] = new SqlParameter("@DividerName", SqlDbType.NVarChar, 50);
                    parameters[0].Value = dividerName;
                    parameters[1] = new SqlParameter("@OfficeID", SqlDbType.NVarChar, 50);
                    parameters[1].Value = officeId;
                    parameters[2] = new SqlParameter("@EFFID", SqlDbType.Int);
                    parameters[2].Value = effId;
                    parameters[3] = new SqlParameter("@Color", SqlDbType.NVarChar, 10);
                    parameters[3].Value = color;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isCheck">Do not check for existing folder if folder creation performs from excel sheet so value 1, otherwise 0</param>
        /// <param name="folderName"></param>
        /// <param name="officeId"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="otherInfo"></param>
        /// <param name="fileNumber"></param>
        /// <param name="alert"></param>
        /// <param name="templateName"></param>
        /// <param name="address"></param>
        /// <param name="city"></param>
        /// <param name="postal"></param>
        /// <param name="tel"></param>
        /// <param name="ssn"></param>
        /// <param name="email"></param>
        /// <param name="alert2"></param>
        /// <param name="comments"></param>
        /// <returns></returns>
        public static DataSet CreateFileFolder(bool isCheck, bool isBatch, string folderName, string officeId, string officeId1, string firstName, string lastName, int otherInfo, string fileNumber, string alert, string templateName,
            string address = null, string city = null, string postal = null, string tel = null, string ssn = null, string email = null, string alert2 = null, string comments = null, string dob = null, string site1 = null, string site2 = null)
        {
            DataSet table = new DataSet();
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "CreateFileFolder";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[22];

                    parameters[0] = new SqlParameter("@FolderName", SqlDbType.NVarChar, 50);
                    parameters[0].Value = folderName;

                    parameters[1] = new SqlParameter("@OfficeID", SqlDbType.NVarChar, 50);
                    parameters[1].Value = officeId;

                    parameters[2] = new SqlParameter("@FirstName", SqlDbType.VarChar, 50);
                    parameters[2].Value = firstName;

                    parameters[3] = new SqlParameter("@LastName", SqlDbType.VarChar, 50);
                    parameters[3].Value = lastName;

                    parameters[4] = new SqlParameter("@Dob", SqlDbType.NVarChar, 50);
                    parameters[4].Value = dob;

                    parameters[5] = new SqlParameter("@OtherInfo", SqlDbType.Int);
                    parameters[5].Value = otherInfo;

                    parameters[6] = new SqlParameter("@FileNumber", SqlDbType.VarChar, 20);
                    parameters[6].Value = fileNumber;

                    parameters[7] = new SqlParameter("@Alert1", SqlDbType.NVarChar, 50);
                    parameters[7].Value = alert;

                    parameters[8] = new SqlParameter("@TemplateName", SqlDbType.VarChar, 50);
                    parameters[8].Value = templateName;

                    parameters[9] = new SqlParameter("@Address", SqlDbType.NVarChar, 250);
                    parameters[9].Value = address;

                    parameters[10] = new SqlParameter("@City", SqlDbType.NVarChar, 50);
                    parameters[10].Value = city;

                    parameters[11] = new SqlParameter("@Postal", SqlDbType.NChar, 10);
                    parameters[11].Value = postal;

                    parameters[12] = new SqlParameter("@Tel", SqlDbType.NVarChar, 50);
                    parameters[12].Value = tel;

                    parameters[13] = new SqlParameter("@SSN", SqlDbType.NVarChar, 50);
                    parameters[13].Value = ssn;

                    parameters[14] = new SqlParameter("@Email", SqlDbType.NVarChar, 50);
                    parameters[14].Value = email;

                    parameters[15] = new SqlParameter("@Alert2", SqlDbType.NVarChar, 50);
                    parameters[15].Value = alert2;

                    parameters[16] = new SqlParameter("@Comments", SqlDbType.NVarChar, 50);
                    parameters[16].Value = comments;

                    parameters[17] = new SqlParameter("@IsCheck", SqlDbType.Bit);
                    parameters[17].Value = isCheck;

                    parameters[18] = new SqlParameter("@OfficeID1", SqlDbType.NVarChar, 50);
                    parameters[18].Value = officeId1;

                    parameters[19] = new SqlParameter("@Site1", SqlDbType.NVarChar, 50);
                    parameters[19].Value = site1;

                    parameters[20] = new SqlParameter("@Site2", SqlDbType.NVarChar, 50);
                    parameters[20].Value = site2;

                    parameters[21] = new SqlParameter("@IsBatch", SqlDbType.Bit);
                    parameters[21].Value = isBatch;

                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;
                    dataAdapter.Fill(table);

                }
                catch (Exception ex)
                {
                    var test = "";
                    return table;
                }
                finally
                {
                    connection.Close();
                }
            return table;

        }

        #endregion

        #region DeletedItems
        public static DataTable GetDeletedDocuments(string officeUID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetDeletedDocuments";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@OfficeUID", officeUID);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetDeletedFolders(string officeUID, string subUserId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetDeletedFolders";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@OfficeUID", officeUID);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlParameter param2 = command.Parameters.AddWithValue("@SubUserID", subUserId);
                    param2.Direction = ParameterDirection.Input;
                    param2.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }
        #endregion

        #region EmailGroups
        public static DataTable GetEmailGroups(string officeId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetEmailGroups";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@OfficeID", SqlDbType.NVarChar, 50);
                    parameters[0].Value = officeId;

                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable dt = new DataTable();

                    dataAdapter.Fill(dt);

                    return dt;
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                    connection.Close();
                }
        }

        public static bool DeleteEmailGroup(string officeId, int groupId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DeleteEmailGroup";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@OfficeID", SqlDbType.NVarChar, 50);
                    parameters[0].Value = officeId;
                    parameters[1] = new SqlParameter("@GroupId", SqlDbType.Int);
                    parameters[1].Value = groupId;
                    parameters[2] = new SqlParameter("@ModifyDate", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;
                    command.Parameters.AddRange(parameters);
                    command.ExecuteScalar();

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetEmailsFromGroup(int groupID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetEmailsFromGroup";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@GroupId", SqlDbType.Int);
                    parameters[0].Value = groupID;

                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable dt = new DataTable();

                    dataAdapter.Fill(dt);

                    return dt;
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GeteFileFlowDocs(string userId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GeteFileFlowDocs";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@UserID", userId);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string InsertEmails(DataTable dt, string deletedIndexData, string groupName, string groupID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertEmails";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[4];
                    parameters[0] = new SqlParameter("@TempEmailPairs", SqlDbType.Structured);
                    parameters[0].Value = dt;
                    parameters[1] = new SqlParameter("@DeletedIndex", DbType.String);
                    parameters[1].Value = deletedIndexData;
                    parameters[2] = new SqlParameter("@GroupName", SqlDbType.NVarChar, 50);
                    parameters[2].Value = groupName;
                    parameters[3] = new SqlParameter("@GroupID", SqlDbType.Int);
                    if (!string.IsNullOrEmpty(groupID))
                        parameters[3].Value = int.Parse(groupID);
                    else
                        parameters[3].Value = 0;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string InsertEmailGroupAndEmails(DataTable dt, string groupName, string createdBy)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertEmailGroupAndEmails";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@TempEmailPairs", SqlDbType.Structured);
                    parameters[0].Value = dt;
                    parameters[1] = new SqlParameter("@GroupName", SqlDbType.NVarChar, 50);
                    parameters[1].Value = groupName;
                    parameters[2] = new SqlParameter("@CreatedBy", SqlDbType.NVarChar, 50);
                    parameters[2].Value = createdBy;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }
        #endregion

        #region TabAccessRestriction
        public static string InsertTabAccessRestriction(string uid, bool isRestricted)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertTabAccessRestriction";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@uid", SqlDbType.NVarChar, 100);
                    parameters[0].Value = uid;
                    parameters[1] = new SqlParameter("@IsTabAccessRestricted", SqlDbType.Bit);
                    parameters[1].Value = isRestricted;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string UpdateTabAccessRestriction(string uid, bool isRestricted)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateTabAccessRestriction";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@uid", SqlDbType.NVarChar, 100);
                    parameters[0].Value = uid;
                    parameters[1] = new SqlParameter("@IsTabAccessRestricted", SqlDbType.Bit);
                    parameters[1].Value = isRestricted;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string UpdateTabAccessRestrictionSelected(DataTable dtHideLockedTabs)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateTabAccessRestrictionSelected";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@tblHideLockedTabs", SqlDbType.Structured);
                    parameters[0].Value = dtHideLockedTabs;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        #region LockMultipleTabs

        //by jenish
        //Get Tabs and Folders list to lock tabs for selected
        public static DataTable GetTabsByOfficeId(string officeId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    DataTable ds = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter();
                    connection.Open();
                    SqlCommand sqlComm = new SqlCommand("GetDividersByOfficeId", connection);
                    sqlComm.CommandType = CommandType.StoredProcedure;

                    SqlParameter parameter;
                    parameter = sqlComm.Parameters.AddWithValue("@UID", officeId);

                    da.SelectCommand = sqlComm;
                    da.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string UpdateLocksForSelectedTabs(DataTable dtLockedTabs, string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "LockSelectedTabs";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@tblLockTabs", SqlDbType.Structured);
                    parameters[0].Value = dtLockedTabs;
                    parameters[1] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                    parameters[1].Value = uid;
                    parameters[2] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        //Lock searched tabs
        public static void UpdateLockSearchTab(string name, string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "LockSearchTab";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
                    parameters[0].Value = name;
                    parameters[1] = new SqlParameter("@OfficeID", SqlDbType.NVarChar, 50);
                    parameters[1].Value = uid;

                    command.Parameters.AddRange(parameters);
                    command.ExecuteScalar();
                }
                catch (Exception ex)
                {

                }
                finally
                {
                    connection.Close();
                }
        }

        public static string DeleteAuditLog(DataTable dtLogs)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DeleteAuditLog";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@tblLog", SqlDbType.Structured);
                    parameters[0].Value = dtLogs;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        #endregion

        public static DataTable GetTabRestForSubUser(string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    DataTable ds = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter();
                    connection.Open();
                    SqlCommand sqlComm = new SqlCommand("GetTabRestForSubUser", connection);
                    sqlComm.CommandType = CommandType.StoredProcedure;

                    SqlParameter parameter;
                    parameter = sqlComm.Parameters.AddWithValue("@uid", uid);

                    da.SelectCommand = sqlComm;
                    da.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                    connection.Close();
                }
        }
        #endregion

        #region Casbo
        public static string DocumentsInsertForCasbo(string FileName, string UID, DateTime CreatedDate)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DocumentsInsertForCasbo";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@FileName", SqlDbType.NVarChar, 250);
                    parameters[0].Value = FileName;
                    parameters[1] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                    parameters[1].Value = UID;
                    parameters[2] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GetCasboPdfsForAdmin(string fileName = "")
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetCasboPdfsForAdmin";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@FileName", fileName);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;


                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string DocumentsUpdateForCasbo(string FileName, string fileId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DocumentsUpdateForCasbo";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@FileName", SqlDbType.NVarChar, 250);
                    parameters[0].Value = FileName;
                    parameters[1] = new SqlParameter("@FileId", SqlDbType.Int);
                    parameters[1].Value = int.Parse(fileId);
                    parameters[2] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string DocumentsDeleteForCasbo(int fileId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DocumentsDeleteForCasbo";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@FileId", SqlDbType.Int);
                    parameters[0].Value = fileId;
                    parameters[1] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[1].Value = DateTime.Now;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string InsertCasboLogs(string custId, int fileId, int action, string firstName, string lastName)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertCasboLogs";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[6];
                    parameters[0] = new SqlParameter("@custId", SqlDbType.NVarChar, 50);
                    parameters[0].Value = custId;
                    parameters[1] = new SqlParameter("@FileID", SqlDbType.Int);
                    parameters[1].Value = fileId;
                    parameters[2] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    parameters[2].Value = System.DateTime.Now;
                    parameters[3] = new SqlParameter("@Action", SqlDbType.TinyInt);
                    parameters[3].Value = action;
                    parameters[4] = new SqlParameter("@firstName", SqlDbType.NVarChar, 50);
                    parameters[4].Value = firstName;
                    parameters[5] = new SqlParameter("@lastName", SqlDbType.NVarChar, 50);
                    parameters[5].Value = lastName;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
        }

        public static DataTable GetCasboLogs()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    SqlDataAdapter da = new SqlDataAdapter();
                    connection.Open();
                    SqlCommand sqlComm = new SqlCommand("GetCasboLogs", connection);
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand = sqlComm;
                    DataTable table = new DataTable();
                    da.Fill(table);

                    return table;

                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }
        #endregion

        #region eFileSplit
        /// <summary>
        /// DocumentsInsert in eFileSplit Table
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="UID"></param>
        /// <param name="size"></param>
        /// <param name="isDeleted"></param>
        /// <returns></returns>
        public static string DocumentsInsertForeFileSplit(string FileName, Guid UID, decimal size, int isDeleted, string modifiedBy)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DocumentsInsertForeFileSplit";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[6];
                    parameters[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 100);
                    parameters[0].Value = FileName;
                    parameters[1] = new SqlParameter("@UID", SqlDbType.UniqueIdentifier);
                    parameters[1].Value = UID;
                    parameters[2] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;
                    parameters[3] = new SqlParameter("@Size", SqlDbType.Decimal);
                    parameters[3].Value = size;
                    parameters[4] = new SqlParameter("@IsDeleted", SqlDbType.Bit);
                    parameters[4].Value = isDeleted;
                    parameters[5] = new SqlParameter("@ModifiedBy", SqlDbType.NVarChar, 50);
                    parameters[5].Value = modifiedBy;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataTable GeteFileSplitDocuments(Guid UID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GeteFileSplitDocuments";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@UID", UID);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.Guid;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Gets the file flow file from file flow identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public static DataTable GetFileSplitById(int id)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetFileSplitById";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@ID", id);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.Int32;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// UpdateFileFlow
        /// </summary>
        /// <param name="answer"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string UpdateFileSplit(string name, int eFileFlowid, int isDeleted, int isRename, int isMoved, string modifiedBy)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateFileSplit";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[7];
                    parameters[0] = new SqlParameter("@ID", SqlDbType.Int);
                    parameters[0].Value = eFileFlowid;
                    parameters[1] = new SqlParameter("@Name", SqlDbType.NVarChar, 100);
                    parameters[1].Value = name;
                    parameters[2] = new SqlParameter("@IsDeleted", SqlDbType.Bit);
                    parameters[2].Value = isDeleted;
                    parameters[3] = new SqlParameter("@IsRename", SqlDbType.Bit);
                    parameters[3].Value = isRename;
                    parameters[4] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[4].Value = DateTime.Now;
                    parameters[5] = new SqlParameter("@IsMoved", SqlDbType.Bit);
                    parameters[5].Value = isMoved;
                    parameters[6] = new SqlParameter("@ModifiedBy", SqlDbType.NVarChar, 50);
                    parameters[6].Value = modifiedBy;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string UpdateFileSplitSize(int eFileSplitid, long size, string modifiedBy)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateFileSplitSize";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[4];
                    parameters[0] = new SqlParameter("@ID", SqlDbType.Int);
                    parameters[0].Value = eFileSplitid;
                    parameters[1] = new SqlParameter("@Size", SqlDbType.Decimal);
                    parameters[1].Value = size;
                    parameters[2] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;
                    parameters[3] = new SqlParameter("@ModifiedBy", SqlDbType.NVarChar, 50);
                    parameters[3].Value = modifiedBy;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }
        #endregion

        //for deleting divider & related documents, also inserting in auditLog
        public static void DeleteDividerAndDocuments(string DeletedBy, int DividerId, int Action)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "DeleteDividerAndDocuments";
                command.CommandType = CommandType.StoredProcedure;


                SqlParameter[] parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@DeletedBy", SqlDbType.NVarChar, 50);
                parameters[0].Value = DeletedBy;
                parameters[1] = new SqlParameter("@DividerId", SqlDbType.NVarChar, 50);
                parameters[1].Value = DividerId;
                parameters[2] = new SqlParameter("@Action", SqlDbType.NVarChar, 100);
                parameters[2].Value = Action;

                command.Parameters.AddRange(parameters);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                connection.Close();
            }
        }

        #region ChangeDefaultPassword
        public static int ChangeDefaultPassword(string name, string password)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "ChangeAdminPassword";
                command.CommandType = CommandType.StoredProcedure;


                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
                parameters[0].Value = name;
                parameters[1] = new SqlParameter("@Password", SqlDbType.NVarChar, 50);
                parameters[1].Value = password;

                command.Parameters.AddRange(parameters);
                int value = command.ExecuteNonQuery();
                return value;
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                connection.Close();
            }
        }
        #endregion

        #region SMTP MailBox

        ///<summary>
        ///For creating or updating MailBox
        ///</summary>
        ///<param name="Email"></param>
        ///<param name="MailBoxId"></param>
        ///<param name="OfficeId"></param>
        ///<param name="Password"></param>
        ///<param name="UID"></param>
        ///<returns></returns>
        public static int UpdateOrCreateMailBox(string Email, string Password, string UID, int MailBoxId = 0)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "UpdateOrCreateMailBox";
                command.CommandType = CommandType.StoredProcedure;


                SqlParameter[] parameters = new SqlParameter[5];
                parameters[0] = new SqlParameter("@MailBoxId", SqlDbType.Int);
                parameters[0].Value = MailBoxId;
                parameters[1] = new SqlParameter("@Email", SqlDbType.NVarChar, 100);
                parameters[1].Value = Email;
                parameters[2] = new SqlParameter("@Password", SqlDbType.NVarChar, 50);
                parameters[2].Value = Password;
                parameters[3] = new SqlParameter("@UID", SqlDbType.UniqueIdentifier);
                parameters[3].Value = new Guid(UID);
                parameters[4] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                parameters[4].Value = System.DateTime.Now;

                command.Parameters.AddRange(parameters);
                int count = command.ExecuteNonQuery();
                return count;
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                connection.Close();
            }
        }

        ///<summary>
        ///Get MailBox list or specific one mailbox
        ///</summary>
        ///<param name="email"></param>
        ///<param name="MailBoxId"></param>
        ///<returns></returns>
        public static DataTable GetMailBox(string email = null, int MailBoxId = 0)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "GetMailBoxAndPostOffice";
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@MailBoxId", SqlDbType.Int);
                parameters[0].Value = MailBoxId;
                parameters[1] = new SqlParameter("@Email", SqlDbType.NVarChar, 100);
                parameters[1].Value = email;
                command.Parameters.AddRange(parameters);
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;
                DataTable table = new DataTable();

                dataAdapter.Fill(table);

                return table;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
            finally
            {
                connection.Close();
            }
        }

        ///<summary>
        ///mailbox exists or not for specific user
        ///</summary>
        ///<param name="boxId"></param>
        ///<param name="UID"></param>
        ///<returns></returns>
        public static bool isMailBoxExist(string UID, int boxId = 0)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "IsMailBoxExist";
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@UID", SqlDbType.UniqueIdentifier);
                parameters[0].Value = new Guid(UID);
                parameters[1] = new SqlParameter("@BoxId", SqlDbType.Int);
                parameters[1].Value = boxId;
                command.Parameters.AddRange(parameters);
                var value = command.ExecuteScalar();
                return Convert.ToBoolean(value);
            }
            catch (Exception ex)
            {
                return true;
            }
            finally
            {
                connection.Close();
            }
        }

        ///<summary>
        ///Delete mailBox by mailBoxId
        ///</summary>
        ///<param name="mailBoxId"></param>
        ///<returns></returns>
        public static bool DeleteMailBox(int mailBoxId)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "DeleteMailBoxById";
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@MailBoxId", SqlDbType.Int);
                parameters[0].Value = mailBoxId;
                command.Parameters.AddRange(parameters);
                var value = command.ExecuteScalar();
                return Convert.ToBoolean(value);
            }
            catch (Exception ex)
            {
                return true;
            }
            finally
            {
                connection.Close();
            }
        }

        ///<summary>
        ///Get Or Update Mail Count
        ///</summary>
        ///<param name="mailIndex"></param>
        ///<returns></returns>
        public static int GetOrUpdateMailCount(int mailBoxId, int mailIndex = 0)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "GetOrUpdateMailCount";
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@MailIndex", SqlDbType.Int);
                parameters[0].Value = mailIndex;
                parameters[1] = new SqlParameter("@MailBoxId", SqlDbType.Int);
                parameters[1].Value = mailBoxId;
                command.Parameters.AddRange(parameters);
                var value = command.ExecuteScalar();
                return Convert.ToInt32(value);
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                connection.Close();
            }
        }

        #endregion

        #region Share Popup

        /// <summary>
        ///for saving shareLink logs.
        /// </summary>
        /// <param name="strName"></param>
        /// <param name="strEmail"></param>
        /// <param name="strMailInfor"></param>
        /// <param name="strMailInfor"></param>
        /// <param name="strMailTitle"></param>
        /// <returns></returns>
        public static int SaveShareLinkLog(string strName, string strEmail, string strMailInfor, int shareId, string IP)
        {
            //string ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "InsertShareLinkLogs";
                command.CommandType = CommandType.StoredProcedure;


                SqlParameter[] parameters = new SqlParameter[7];
                parameters[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 50);
                parameters[0].Value = strName;
                parameters[1] = new SqlParameter("@Email", SqlDbType.NVarChar, 50);
                parameters[1].Value = strEmail;
                parameters[2] = new SqlParameter("@Message", SqlDbType.NVarChar, 100);
                parameters[2].Value = strMailInfor;
                parameters[3] = new SqlParameter("@DOB", SqlDbType.DateTime);
                parameters[3].Value = System.DateTime.Now;
                parameters[4] = new SqlParameter("@IP", SqlDbType.NVarChar, 50);
                parameters[4].Value = IP;
                parameters[5] = new SqlParameter("@LinkId", SqlDbType.Int);
                parameters[5].Value = shareId;
                parameters[6] = new SqlParameter("@LogId", SqlDbType.Int);
                parameters[6].Direction = ParameterDirection.Output;

                command.Parameters.AddRange(parameters);
                command.ExecuteNonQuery();

                return int.Parse(parameters[6].Value.ToString());
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        ///for saving shareLink.
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="docType"></param>
        /// <param name="docId"></param>
        /// <param name="folderId"></param>
        /// <param name="workareaId"></param>
        /// <param name="efileflowId"></param>
        /// <returns></returns>
        public static int SaveShareLink(Guid uid, int docType, int? docId, int? folderId, int? workareaId, int? efileflowId, int? archiveId)
        {
            //string uid = Membership.GetUser().ProviderUserKey.ToString();
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "InsertShareLink";
                command.CommandType = CommandType.StoredProcedure;


                SqlParameter[] parameters = new SqlParameter[9];
                parameters[0] = new SqlParameter("@UID", SqlDbType.UniqueIdentifier);
                parameters[0].Value = uid;
                parameters[1] = new SqlParameter("@DocumentType", SqlDbType.Int);
                parameters[1].Value = docType;
                parameters[2] = new SqlParameter("@DocumentId", SqlDbType.Int);
                parameters[2].Value = docId;
                parameters[3] = new SqlParameter("@FolderId", SqlDbType.Int);
                parameters[3].Value = folderId;
                parameters[4] = new SqlParameter("@WorkAreaId", SqlDbType.Int);
                parameters[4].Value = workareaId;
                parameters[5] = new SqlParameter("@eFileFlowId", SqlDbType.Int);
                parameters[5].Value = efileflowId;
                parameters[6] = new SqlParameter("@ArchiveId", SqlDbType.Int);
                parameters[6].Value = archiveId;
                parameters[7] = new SqlParameter("@SharedTime", SqlDbType.DateTime);
                parameters[7].Value = System.DateTime.Now;
                parameters[8] = new SqlParameter("@ShareId", SqlDbType.Int);
                parameters[8].Direction = ParameterDirection.Output;

                command.Parameters.AddRange(parameters);
                command.ExecuteNonQuery();

                return int.Parse(parameters[8].Value.ToString());
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                connection.Close();
            }
        }

        public static int GetExpirationTime(Guid uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetExpirationTime";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@uid", SqlDbType.UniqueIdentifier);
                    parameters[0].Value = uid;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToInt32(value);
                }
                catch (Exception ex)
                {
                    return 0;
                }
        }

        public static int UpdateExpirationTime(Guid uid, int expirationTime)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateExpirationTime";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@uid", SqlDbType.UniqueIdentifier);
                    parameters[0].Value = uid;
                    parameters[1] = new SqlParameter("@ExpirationTime", SqlDbType.Int);
                    parameters[1].Value = expirationTime;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToInt32(value);
                }
                catch (Exception ex)
                {
                    return 0;
                }
        }

        /// <summary>
        /// Check valid EMail by Mailid
        /// </summary>
        /// <param name="mailId"></param>
        /// <param name="emailid"></param>
        /// <returns></returns>
        public static int CheckValidEMailByMailId(int mailId, string emailid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "CheckValidEMailByMailId";
                    command.Parameters.Add("@MailId", SqlDbType.Int).Value = mailId;
                    command.Parameters.Add("@EmailId", SqlDbType.NVarChar).Value = emailid;
                    var value = command.ExecuteScalar();
                    return Convert.ToInt32(value);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
        }
        #endregion

        #region CopyFoldersFromOneUserToAnother

        public static DataSet GetSelectedFolderDetails(string folderIds, string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))

                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetFolderDetails";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = command.Parameters.AddWithValue("@FolderId", folderIds);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlParameter param2 = command.Parameters.AddWithValue("@UID", uid);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataSet table = new DataSet();

                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string InsertCopyFolderLogs(string fromUser, string toUser, int oldFolderID, int newFolderID, int action)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertCopyFolderLogs";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[6];
                    parameters[0] = new SqlParameter("@FromUser", SqlDbType.NVarChar, 50);
                    parameters[0].Value = fromUser;
                    parameters[1] = new SqlParameter("@ToUser", SqlDbType.NVarChar, 50);
                    parameters[1].Value = toUser;
                    parameters[2] = new SqlParameter("@OldFolderID", SqlDbType.Int);
                    parameters[2].Value = oldFolderID;
                    parameters[3] = new SqlParameter("@NewFolderID", SqlDbType.Int);
                    parameters[3].Value = newFolderID;
                    parameters[4] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    parameters[4].Value = System.DateTime.Now;
                    parameters[5] = new SqlParameter("@Action", SqlDbType.Bit);
                    parameters[5].Value = action;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
        }

        public static string DeleteFileFolder(int folderID)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DeleteFileFolder";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];

                    parameters[0] = new SqlParameter("@FolderID", SqlDbType.Int);
                    parameters[0].Value = folderID;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
        }

        public static DataTable GetCopyFolderLogs(string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    DataTable ds = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter();
                    connection.Open();
                    SqlCommand sqlComm = new SqlCommand("GetCopyFolderLogs", connection);
                    sqlComm.CommandType = CommandType.StoredProcedure;

                    SqlParameter param1 = sqlComm.Parameters.AddWithValue("@UID", uid);
                    param1.Direction = ParameterDirection.Input;
                    param1.DbType = DbType.String;

                    da.SelectCommand = sqlComm;
                    da.Fill(ds);
                    return ds;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        #endregion

        #region MoveFileFromOneFolderToAnother
        /// <summary>
        /// Updates the document.
        /// </summary>
        /// <param name="documentID">The document identifier.</param>
        /// <param name="movedBy">The moved by.</param>
        /// <returns></returns>
        public static string UpdateDocument(int documentID, string movedBy)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateDocument";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@DocumentID", SqlDbType.Int);
                    parameters[0].Value = documentID;
                    parameters[1] = new SqlParameter("@MovedBy", SqlDbType.NVarChar, 50);
                    parameters[1].Value = movedBy;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }
        #endregion

        #region LatestChanges
        public static bool IsExistDividerForFolder(int dividerId, int folderId, string name, int type = 2)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            try
            {
                using (SqlCommand cmd = new SqlCommand("IsSameDividerExists", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@DividerId", dividerId);
                    cmd.Parameters.AddWithValue("@FolderId", folderId);
                    cmd.Parameters.AddWithValue("@DividerName", name);
                    cmd.Parameters.AddWithValue("@Type", type);
                    cmd.Parameters.Add("@IsExist", SqlDbType.Int);
                    cmd.Parameters["@IsExist"].Direction = ParameterDirection.Output;
                    connection.Open();
                    cmd.ExecuteNonQuery();
                    bool isExist = Convert.ToBoolean(cmd.Parameters["@IsExist"].Value);
                    return isExist;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                connection.Close();
            }
        }
        #endregion

        #region SwitchUser

        public static string AssignUser(string assignedTo, string assignedBy, DataTable dtUsers)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "AssignOffice";
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter[] parameters = new SqlParameter[5];
                parameters[0] = new SqlParameter("@AssignedTo", SqlDbType.NVarChar, 50);
                parameters[0].Value = assignedTo;
                parameters[1] = new SqlParameter("@AssignedBy", SqlDbType.NVarChar, 50);
                parameters[1].Value = assignedBy;
                parameters[2] = new SqlParameter("@AssignedDate", SqlDbType.DateTime);
                parameters[2].Value = DateTime.Now;
                parameters[3] = new SqlParameter("@tblAssigneUID", SqlDbType.Structured);
                parameters[3].Value = dtUsers;
                parameters[4] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                parameters[4].Value = DateTime.Now;
                command.Parameters.AddRange(parameters);
                var value = command.ExecuteScalar();
                return Convert.ToString(value);
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
            finally
            {
                connection.Close();
            }
        }

        public static DataTable GetAssignedUserByOfficeId(string officeId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetAssignedOffices";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@OfficeId", SqlDbType.NVarChar, 50);
                    parameters[0].Value = officeId;

                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable dt = new DataTable();

                    dataAdapter.Fill(dt);

                    return dt;
                }
                catch (Exception ex)
                {
                    return new DataTable();
                }
                finally
                {
                    connection.Close();
                }
        }


        public static DataSet GetAssignedUserForSubUser(string officeId, string subUserId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetAssignedOfficeForSubusers";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@OfficeId", SqlDbType.NVarChar, 50);
                    parameters[0].Value = officeId;
                    parameters[1] = new SqlParameter("@SubUserId", SqlDbType.NVarChar, 50);
                    parameters[1].Value = subUserId;

                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataSet ds = new DataSet();

                    dataAdapter.Fill(ds);

                    return ds;
                }
                catch (Exception ex)
                {
                    return new DataSet();
                }
                finally
                {
                    connection.Close();
                }
        }

        public static DataSet GetAssignedUserForOffice(string officeId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetAssignedOfficeForOffice";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@OfficeId", SqlDbType.NVarChar, 50);
                    parameters[0].Value = officeId;

                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataSet ds = new DataSet();

                    dataAdapter.Fill(ds);

                    return ds;
                }
                catch (Exception ex)
                {
                    return new DataSet();
                }
                finally
                {
                    connection.Close();
                }
        }

        public static string UnAssignOffice(string assignedUid, string assignedTo)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UnAssignOffice";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@AssignedUID", SqlDbType.NVarChar, 50);
                    parameters[0].Value = assignedUid;
                    parameters[1] = new SqlParameter("@AssignedTo", SqlDbType.NVarChar, 50);
                    parameters[1].Value = assignedTo;
                    parameters[2] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return string.Empty;
                }
                finally
                {
                    connection.Close();
                }
        }

        #endregion

        #region ElasticSearch

        /// <summary>
        /// Create Archive Documents
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="pathName"></param>
        /// <param name="folderId"></param>
        /// <param name="size"></param>
        /// <param name="isBatchUpload"></param>
        /// <returns></returns>
        public static int DocumentInsertForArchive(string fileName, string pathName, int folderId, float size, bool isBatchUpload = false)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);

            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "DocumentInsertForArchive";
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter[] parameters = new SqlParameter[6];
                parameters[0] = new SqlParameter("@FileName", SqlDbType.NVarChar, 150);
                parameters[0].Value = fileName;
                parameters[1] = new SqlParameter("@PathName", SqlDbType.NVarChar, 250);
                parameters[1].Value = pathName;
                parameters[2] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                parameters[2].Value = DateTime.Now;
                parameters[3] = new SqlParameter("@Size", SqlDbType.Float);
                parameters[3].Value = size;
                parameters[4] = new SqlParameter("@FolderId", SqlDbType.Int);
                parameters[4].Value = folderId;
                parameters[5] = new SqlParameter("@IsBatchUpload", SqlDbType.Bit);
                parameters[5].Value = isBatchUpload;
                command.Parameters.AddRange(parameters);
                var value = command.ExecuteScalar();
                return Convert.ToInt32(Convert.ToString(value));
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Create archive folders
        /// </summary>
        /// <param name="FolderName"></param>
        /// <param name="parentId"></param>
        /// <param name="uid"></param>
        /// <param name="level"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static int DocumentInsertforArchiveTree(string FolderName, int? parentId, string uid, int level, string path)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DocumentInsertforArchiveTree";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[6];
                    parameters[0] = new SqlParameter("@FolderName", SqlDbType.NVarChar, 50);
                    parameters[0].Value = FolderName;
                    parameters[1] = new SqlParameter("@ParentId", SqlDbType.Int);
                    parameters[1].Value = parentId;
                    parameters[2] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                    parameters[2].Value = uid;
                    parameters[3] = new SqlParameter("@Level", SqlDbType.Int);
                    parameters[3].Value = level;
                    parameters[4] = new SqlParameter("@Path", SqlDbType.NVarChar, 100);
                    parameters[4].Value = path;
                    parameters[5] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    parameters[5].Value = DateTime.Now;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToInt32(value);
                }
                catch (Exception ex)
                {
                    return 0;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Get folders list by UID
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static DataTable GetArchiveTreeList(string uid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "GetArchiveTreeByUID";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                parameters[0].Value = uid;

                command.Parameters.AddRange(parameters);

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;

                DataTable dt = new DataTable();

                dataAdapter.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Get Archive Docs for User Login by pagination
        /// </summary>
        /// <param name="FolderId"></param>
        /// <param name="uid"></param>
        /// <param name="sortBy"></param>
        /// <param name="sortOrder"></param>
        /// <param name="pageNo"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static DataTable GetArchiveForUser(int? FolderId = null, string uid = null, string sortBy = null, string sortOrder = null, int pageNo = 0, int pageSize = 10)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "GetArchiveForUser";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[6];
                parameters[0] = new SqlParameter("@FolderId", SqlDbType.Int);
                parameters[0].Value = FolderId;
                parameters[1] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                parameters[1].Value = uid;
                parameters[2] = new SqlParameter("@SortBy", SqlDbType.NVarChar, 50);
                parameters[2].Value = sortBy;
                parameters[3] = new SqlParameter("@SortOrder", SqlDbType.NVarChar, 4);
                parameters[3].Value = sortOrder;
                parameters[4] = new SqlParameter("@PageNo", SqlDbType.Int);
                parameters[4].Value = pageNo;
                parameters[5] = new SqlParameter("@PageSize", SqlDbType.Int);
                parameters[5].Value = pageSize;

                command.Parameters.AddRange(parameters);

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;

                DataTable dt = new DataTable();

                dataAdapter.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Get Archive Docs for Admin
        /// </summary>
        /// <param name="FolderId"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static DataTable GetArchiveForAdmin(int? FolderId = null, string uid = null)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "GetArchiveForAdmin";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@FolderId", SqlDbType.Int);
                parameters[0].Value = FolderId;
                parameters[1] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                parameters[1].Value = uid;

                command.Parameters.AddRange(parameters);

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;

                DataTable dt = new DataTable();

                dataAdapter.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }


        /// <summary>
        /// Delete archive document by Id
        /// </summary>
        /// <param name="archiveId"></param>
        /// <returns></returns>
        public static int DeleteArchive(int archiveId)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "DeleteArchive";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@ArchiveId", SqlDbType.Int);
                parameters[0].Value = archiveId;

                command.Parameters.AddRange(parameters);
                var value = command.ExecuteScalar();
                return Convert.ToInt32(value);
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Delete folder by id from ArchiveTree
        /// </summary>
        /// <param name="archiveTreeId"></param>
        /// <returns></returns>
        public static int DeleteArchiveTree(int archiveTreeId)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "DeleteArchiveTreeById";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@ArchiveTreeId", SqlDbType.Int);
                parameters[0].Value = archiveTreeId;

                command.Parameters.AddRange(parameters);
                var value = command.ExecuteScalar();
                return Convert.ToInt32(value);
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Edit Archive Informations
        /// </summary>
        /// <param name="archiveId"></param>
        /// <param name="Filename"></param>
        /// <param name="PathName"></param>
        /// <returns></returns>
        public static int UpdateArchive(int archiveId, string Filename, string PathName)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "UpdateArchive";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@ArchiveId", SqlDbType.Int);
                parameters[0].Value = archiveId;
                parameters[1] = new SqlParameter("@FileName", SqlDbType.NVarChar, 250);
                parameters[1].Value = Filename;
                parameters[2] = new SqlParameter("@PathName", SqlDbType.NVarChar, 350);
                parameters[2].Value = PathName;

                command.Parameters.AddRange(parameters);
                var value = command.ExecuteScalar();
                return Convert.ToInt32(value);
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Edit ArchiveTree Informations
        /// </summary>
        /// <param name="archiveId"></param>
        /// <param name="Filename"></param>
        /// <param name="PathName"></param>
        /// <returns></returns>
        public static int UpdateArchiveTree(int archiveTreeId, string FolderName)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "UpdateArchiveTree";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@ArchiveTreeId", SqlDbType.Int);
                parameters[0].Value = archiveTreeId;
                parameters[1] = new SqlParameter("@FolderName", SqlDbType.NVarChar, 50);
                parameters[1].Value = FolderName;

                command.Parameters.AddRange(parameters);
                var value = command.ExecuteScalar();
                return Convert.ToInt32(value);
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Get all the archive documents by UID for batch Upload
        /// </summary>
        /// <param name="uid">UID</param>
        /// <returns></returns>
        public static DataTable GetArchiveByUID(string uid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "GetArchiveByUID";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                parameters[0].Value = uid;

                command.Parameters.AddRange(parameters);
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;

                DataTable dt = new DataTable();

                dataAdapter.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Create Logs of result when uploading to elastic
        /// </summary>
        /// <param name="archiveId">ArchiveId</param>
        /// <param name="isSuccess">IsSuccess</param>
        public static void InsertIntoArchiveLog(int archiveId, bool isSuccess)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "InsertIntoArchiveLog";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@ArchiveId", SqlDbType.Int);
                parameters[0].Value = archiveId;
                parameters[1] = new SqlParameter("@IsSuccess", SqlDbType.Bit);
                parameters[1].Value = isSuccess;
                parameters[2] = new SqlParameter("@LogDate", SqlDbType.DateTime);
                parameters[2].Value = DateTime.Now;

                command.Parameters.AddRange(parameters);
                command.ExecuteScalar();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                connection.Close();
            }
        }

        #endregion

        #region WorkAreaRecords

        public static void InsertIntoWorkAreaRecords(int workAreaId, string fileName, string pathName, string UID, decimal size)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "InsertIntoWorkareaRecords";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[5];
                parameters[0] = new SqlParameter("@WorkAreaId", SqlDbType.Int);
                parameters[0].Value = workAreaId;
                parameters[1] = new SqlParameter("@FileName", SqlDbType.NVarChar, 250);
                parameters[1].Value = fileName;
                parameters[2] = new SqlParameter("@PathName", SqlDbType.NVarChar, 350);
                parameters[2].Value = pathName;
                parameters[3] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                parameters[3].Value = UID;
                parameters[4] = new SqlParameter("@Size", SqlDbType.Decimal);
                parameters[4].Value = size;

                command.Parameters.AddRange(parameters);
                var value = command.ExecuteScalar();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Get all the archive documents by Id and UID
        /// </summary>
        /// <param name="FolderId"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static DataTable GetWorkAreaByWorkAreaTreeId(int FolderId, string uid = null)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "GetWorkAreaByWorkAreaTreeId";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@WorkAreaTreeId", SqlDbType.Int);
                parameters[0].Value = FolderId;
                parameters[1] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                parameters[1].Value = uid;

                command.Parameters.AddRange(parameters);

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;

                DataTable dt = new DataTable();

                dataAdapter.Fill(dt);

                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Delete folder by id from WorkAreaTree
        /// </summary>
        /// <param name="archiveTreeId"></param>
        /// <returns></returns>
        public static int UpdateWorkAreaTree(int WorkAreaTreeId, int isDeleted, int isRename, string newFolderPath = null, string folderName = null)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "UpdateWorkAreaTree";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[5];
                parameters[0] = new SqlParameter("@WorkAreaTreeId", SqlDbType.Int);
                parameters[0].Value = WorkAreaTreeId;
                parameters[1] = new SqlParameter("@IsDeleted", SqlDbType.Bit);
                parameters[1].Value = isDeleted;
                parameters[2] = new SqlParameter("@FolderName", SqlDbType.NVarChar, 250);
                if (string.IsNullOrEmpty(folderName))
                    parameters[2].Value = DBNull.Value;
                else
                    parameters[2].Value = folderName;
                parameters[3] = new SqlParameter("@IsRename", SqlDbType.Bit);
                parameters[3].Value = isRename;
                parameters[4] = new SqlParameter("@NewFolderPath", SqlDbType.NVarChar, 250);
                if (string.IsNullOrEmpty(newFolderPath))
                    parameters[4].Value = DBNull.Value;
                else
                    parameters[4].Value = newFolderPath;

                command.Parameters.AddRange(parameters);
                var value = command.ExecuteScalar();
                return Convert.ToInt32(value);

            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Get all alert,status,email from filefolder
        /// </summary>
        /// <param name="archiveTreeId"></param>
        /// <returns></returns>
        public static DataTable GetEmailGroupEfileflow(string uid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "GetEfileflowEmailGroup";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@Uid", SqlDbType.NVarChar, 50);
                parameters[0].Value = uid;

                command.Parameters.AddRange(parameters);

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;

                DataTable dt = new DataTable();

                dataAdapter.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }

        #endregion

        #region EdFileShare

        /// <summary>
        /// Insert into EdFileShare when sharing from Splitting Window
        /// </summary>
        /// <param name="edFileShareId">Unique EdFileShareId</param>
        /// <param name="edFileId">Document Id</param>
        /// <param name="fileName">Filename</param>
        /// <param name="toEmail">To Name</param>
        /// <param name="title">Mail Title</param>
        /// <param name="size">Size of File</param>
        public static void InsertIntoEdFileShare(Guid edFileShareId, int edFileId, string fileName, string toEmail, string title, decimal size)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "InsertIntoEdFileShare";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[7];
                parameters[0] = new SqlParameter("@EdFileShareId", SqlDbType.UniqueIdentifier);
                parameters[0].Value = edFileShareId;
                parameters[1] = new SqlParameter("@EdFileId", SqlDbType.Int);
                parameters[1].Value = edFileId;
                parameters[2] = new SqlParameter("@FileName", SqlDbType.NVarChar, 100);
                parameters[2].Value = fileName;
                parameters[3] = new SqlParameter("@ToEmail", SqlDbType.NVarChar, 100);
                parameters[3].Value = toEmail;
                parameters[4] = new SqlParameter("@Title", SqlDbType.NVarChar, 50);
                parameters[4].Value = title;
                parameters[5] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                parameters[5].Value = DateTime.Now;
                parameters[6] = new SqlParameter("@Size", SqlDbType.Decimal);
                parameters[6].Value = size;

                command.Parameters.AddRange(parameters);
                var value = command.ExecuteScalar();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                connection.Close();
            }
        }
        #endregion

        #region New EdForms

        /// <summary>
        /// Create new departments
        /// </summary>
        /// <param name="departmentName">DepartmentName</param>
        /// <returns></returns>
        public static int CreateDepartment(string departmentName)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "CreateDepartment";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@DepartmentName", SqlDbType.NVarChar, 150);
                    parameters[0].Value = departmentName;
                    parameters[1] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    parameters[1].Value = DateTime.Now;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToInt32(Convert.ToString(value));
                }
                catch (Exception ex)
                {
                    return 0;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Get all the departments for Admin
        /// </summary>
        /// <returns></returns>
        public static DataTable GetDepartmentsForAdmin()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetDepartmentsForAdmin";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);

                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Update the department by departmentId
        /// </summary>
        /// <param name="departmentId">DepartmentId</param>
        /// <param name="newName">If renaming then new name otherwise old name</param>
        /// <param name="isDeleted">If renaming then isDeleted will be null</param>
        /// <returns></returns>
        public static int UpdateDepartment(int departmentId, string newName, bool? isDeleted = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateDepartment";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[4];
                    parameters[0] = new SqlParameter("@DepartmentId", SqlDbType.Int);
                    parameters[0].Value = departmentId;
                    parameters[1] = new SqlParameter("@DepartmentName", SqlDbType.NVarChar, 150);
                    parameters[1].Value = newName;
                    parameters[2] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;
                    parameters[3] = new SqlParameter("@IsDeleted", SqlDbType.Bit);
                    parameters[3].Value = isDeleted;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToInt32(Convert.ToString(value));
                }
                catch (Exception ex)
                {
                    return 0;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Get All the Edforms for Admin
        /// </summary>
        /// <param name="uid">The uid.</param>
        /// <param name="departmentId">The department identifier.</param>
        /// <param name="isUser">The is user.</param>
        /// <returns></returns>
        public static DataTable GetEdFormsForAdmin(string uid, int? departmentId, bool? isUser = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetEdFormsForAdmin";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@DepartmentId", SqlDbType.Int);
                    parameters[0].Value = departmentId;
                    parameters[1] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                    parameters[1].Value = uid;
                    parameters[2] = new SqlParameter("@IsUser", SqlDbType.Bit);
                    parameters[2].Value = isUser;
                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);
                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Insert EdForms for Admin
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="departmentId"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static int DocumentsInsertForEdFormsAdmin(string fileName, string officeId, int? departmentId, decimal size)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DocumentsInsertForEdFormsAdmin";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[5];
                    parameters[0] = new SqlParameter("@Name", SqlDbType.NVarChar, 150);
                    parameters[0].Value = fileName;
                    parameters[1] = new SqlParameter("@DepartmentId", SqlDbType.Int);
                    parameters[1].Value = departmentId;
                    parameters[2] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;
                    parameters[3] = new SqlParameter("@Size", SqlDbType.Decimal);
                    parameters[3].Value = size;
                    parameters[4] = new SqlParameter("@OfficeId", SqlDbType.NVarChar);
                    parameters[4].Value = officeId;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToInt32(Convert.ToString(value));
                }
                catch (Exception ex)
                {
                    return 0;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Update EdForms for Admin
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="edFormsId">EdFormsId</param>
        /// <param name="IsRename">IsRename</param>
        /// <param name="isDeleted">IsDeleted</param>
        /// <returns></returns>
        public static string UpdateEdFormsForAdmin(string name, int edFormsId, bool IsRename, bool? isDeleted = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateEdFormsForAdmin";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[5];
                    parameters[0] = new SqlParameter("@EdFormsId", SqlDbType.Int);
                    parameters[0].Value = edFormsId;
                    parameters[1] = new SqlParameter("@Name", SqlDbType.NVarChar, 100);
                    parameters[1].Value = name;
                    parameters[2] = new SqlParameter("@IsDeleted", SqlDbType.Bit);
                    parameters[2].Value = isDeleted;
                    parameters[3] = new SqlParameter("@IsRename", SqlDbType.Bit);
                    parameters[3].Value = IsRename;
                    parameters[4] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[4].Value = DateTime.Now;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Add EdForms for Users
        /// </summary>
        /// <param name="uid">UID</param>
        /// <param name="dtEdFormsId">EdFormsId Table</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="size">The size.</param>
        /// <param name="departmentId">The department identifier.</param>
        /// <returns></returns>
        public static DataTable DocumentsInsertForEdFormsUser(Guid uid, DataTable dtEdFormsId = null, string fileName = null, decimal? size = null, int? departmentId = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DocumentsInsertForEdFormsUser";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[6];
                    parameters[0] = new SqlParameter("@tblEdFormsId", SqlDbType.Structured);
                    parameters[0].Value = dtEdFormsId;
                    parameters[1] = new SqlParameter("@UID", SqlDbType.UniqueIdentifier);
                    parameters[1].Value = uid;
                    parameters[2] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;
                    parameters[3] = new SqlParameter("@FileName", SqlDbType.NVarChar, 150);
                    parameters[3].Value = fileName;
                    parameters[4] = new SqlParameter("@Size", SqlDbType.Decimal);
                    parameters[4].Value = size;
                    parameters[5] = new SqlParameter("@DepartmentId", SqlDbType.Int);
                    parameters[5].Value = departmentId;
                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);
                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Get EdForms for User by departmentId
        /// </summary>
        /// <param name="departmentId"></param>
        /// <returns></returns>
        public static DataTable GetEdFormsForUser(int departmentId, string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetEdFormsForUser";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@DepartmentId", SqlDbType.Int);
                    parameters[0].Value = departmentId;
                    parameters[1] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                    parameters[1].Value = uid;
                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);
                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Update EdForms for User
        /// </summary>
        /// <param name="name">Name</param>
        /// <param name="edFormsUserId">EdFormsUserId</param>
        /// <param name="IsRename">IsRename</param>
        /// <param name="isDeleted">IsDeleted</param>
        /// <returns></returns>
        public static string UpdateEdFormsForUser(string name, int edFormsUserId, bool IsRename, bool? isDeleted = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateEdFormsForUser";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[5];
                    parameters[0] = new SqlParameter("@EdFormsUserId", SqlDbType.Int);
                    parameters[0].Value = edFormsUserId;
                    parameters[1] = new SqlParameter("@Name", SqlDbType.NVarChar, 100);
                    parameters[1].Value = name;
                    parameters[2] = new SqlParameter("@IsDeleted", SqlDbType.Bit);
                    parameters[2].Value = isDeleted;
                    parameters[3] = new SqlParameter("@IsRename", SqlDbType.Bit);
                    parameters[3].Value = IsRename;
                    parameters[4] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[4].Value = DateTime.Now;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Insert into EdFormsFolder when Sharing the EdForms
        /// </summary>
        /// <param name="dtEdFormsUserId"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static void DocumentsInsertForEdFormsUserFolder(DataTable dtEdFormsId, Guid uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "DocumentsInsertForEdFormsUserFolder";
                    command.CommandType = CommandType.StoredProcedure;


                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@tblEdFormsUserId", SqlDbType.Structured);
                    parameters[0].Value = dtEdFormsId;
                    parameters[1] = new SqlParameter("@UID", SqlDbType.UniqueIdentifier);
                    parameters[1].Value = uid;
                    parameters[2] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;
                    command.Parameters.AddRange(parameters);
                    command.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    connection.Close();
                    var a = 1;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Get Folder Details by Alert, Email & Status
        /// </summary>
        /// <param name="uid">UID</param>
        /// <param name="alert">Alert</param>
        /// <param name="status">Status</param>
        /// <param name="email">Email</param>
        /// <param name="isAll">All Folder Selected</param>
        /// <returns></returns>
        public static DataTable GetEdFormsEmail(string uid, string alert = null, string status = null, string email = null, bool? isAll = null)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "GetEdFormsEmail";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[5];
                parameters[0] = new SqlParameter("@Uid", SqlDbType.NVarChar, 50);
                parameters[0].Value = uid;
                parameters[1] = new SqlParameter("@Alert", SqlDbType.NVarChar, 50);
                if (string.IsNullOrEmpty(alert))
                    parameters[1].Value = DBNull.Value;
                else
                    parameters[1].Value = alert;
                parameters[2] = new SqlParameter("@Status", SqlDbType.NVarChar, 50);
                if (string.IsNullOrEmpty(status))
                    parameters[2].Value = DBNull.Value;
                else
                    parameters[2].Value = status;
                parameters[3] = new SqlParameter("@IsAll", SqlDbType.Bit);
                parameters[3].Value = isAll.HasValue ? isAll.Value : Convert.ToBoolean(null);
                parameters[4] = new SqlParameter("@Email", SqlDbType.NVarChar, 150);
                if (string.IsNullOrEmpty(email))
                    parameters[4].Value = DBNull.Value;
                else
                    parameters[4].Value = email;

                command.Parameters.AddRange(parameters);

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;

                DataTable dt = new DataTable();

                dataAdapter.Fill(dt);

                return dt;

            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Insert for every edforms share
        /// </summary>
        /// <param name="edFormsUserShareId">EdFormsShare Unique Id</param>
        /// <param name="edFormsUserId">EdFormsUserId</param>
        /// <param name="toEmail">ToEmail</param>
        /// <param name="title">Title</param>
        /// <param name="status">Status</param>
        /// <returns></returns>
        public static int DocumentInsertForEdFormsShare(string edFormsUserShareId, int edFormsUserId, string toEmail, string title, int status, string fileName, string uid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "DocumentsInsertForEdFormsUserShare";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[8];
                parameters[0] = new SqlParameter("@EdFormsUserShareId", SqlDbType.NVarChar, 50);
                parameters[0].Value = edFormsUserShareId;
                parameters[1] = new SqlParameter("@EdFormsUserId", SqlDbType.Int);
                parameters[1].Value = edFormsUserId;
                parameters[2] = new SqlParameter("@ToEmail", SqlDbType.NVarChar, 150);
                parameters[2].Value = toEmail;
                parameters[3] = new SqlParameter("@Title", SqlDbType.NVarChar, 50);
                parameters[3].Value = title;
                parameters[4] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                parameters[4].Value = DateTime.Now;
                parameters[5] = new SqlParameter("@Status", SqlDbType.TinyInt);
                parameters[5].Value = status;
                parameters[6] = new SqlParameter("@FileName", SqlDbType.NVarChar, 150);
                parameters[6].Value = fileName;
                parameters[7] = new SqlParameter("@UID", SqlDbType.UniqueIdentifier);
                parameters[7].Value = new Guid(uid);

                command.Parameters.AddRange(parameters);
                var value = command.ExecuteScalar();
                return Convert.ToInt32(value);
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Create logs of every EdForms while sharing and making any activity
        /// </summary>
        /// <param name="edFormsUserShareId">EdFormsUserShareId</param>
        /// <param name="status">Status</param>
        /// <param name="ip">IP</param>
        /// <returns></returns>
        public static int InsertIntoEdFormsUserLogs(string edFormsUserShareId, byte status, string ip)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "InsertIntoEdFormsUserLogs";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[4];
                parameters[0] = new SqlParameter("@EdFormsUserShareId", SqlDbType.NVarChar, 50);
                parameters[0].Value = edFormsUserShareId;
                parameters[1] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                parameters[1].Value = DateTime.Now;
                parameters[2] = new SqlParameter("@Status", SqlDbType.TinyInt);
                parameters[2].Value = status;
                parameters[3] = new SqlParameter("@IP", SqlDbType.NVarChar, 50);
                parameters[3].Value = ip;

                command.Parameters.AddRange(parameters);
                var value = command.ExecuteScalar();
                return Convert.ToInt32(value);
            }
            catch (Exception ex)
            {
                return 0;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Assign Departments to particular user
        /// </summary>
        /// <param name="departmentsList">Contains all the selected Departments</param>
        /// <param name="uid">OfficeId</param>
        public static void InsertIntoUserDepartments(DataTable departmentsList, Guid uid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "InsertIntoUserDepartments";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@tblDepartment", SqlDbType.Structured);
                parameters[0].Value = departmentsList;
                parameters[1] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                parameters[1].Value = DateTime.Now;
                parameters[2] = new SqlParameter("@UID", SqlDbType.UniqueIdentifier);
                parameters[2].Value = uid;

                command.Parameters.AddRange(parameters);
                command.ExecuteScalar();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Get Assigned Departments for User
        /// </summary>
        /// <param name="uid">OfficeId</param>
        /// <returns></returns>
        public static DataSet GetAssignedDepartment(Guid uid)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "GetAssignedDepartments";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@Uid", SqlDbType.UniqueIdentifier);
                parameters[0].Value = uid;

                command.Parameters.AddRange(parameters);

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;

                DataSet ds = new DataSet();

                dataAdapter.Fill(ds);

                return ds;

            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Unassign Department by departmentid and uid
        /// </summary>
        /// <param name="departmentId">DepartmentId</param>
        /// <param name="uid">UID</param>
        /// <returns></returns>
        public static string UnAssignDepartment(int departmentId, string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UnAssignDepartmentForOffice";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@UID", SqlDbType.UniqueIdentifier, 50);
                    parameters[0].Value = new Guid(uid);
                    parameters[1] = new SqlParameter("@DepartmentId", SqlDbType.Int);
                    parameters[1].Value = departmentId;
                    parameters[2] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[2].Value = DateTime.Now;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return string.Empty;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Get EdFormsUser Logs
        /// </summary>
        /// <param name="edFormsUserId"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        public static DataTable GetEdFormsUserLogs(int edFormsUserId, string uid, int? status = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetEdFormsUserLogs";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@EdFormsUserId", SqlDbType.Int);
                    parameters[0].Value = edFormsUserId;
                    parameters[1] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                    parameters[1].Value = uid;
                    parameters[2] = new SqlParameter("@Status", SqlDbType.Int);
                    parameters[2].Value = status;
                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);
                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Inserts reminder for users when submitting EdForms
        /// </summary>
        /// <param name="usersList"></param>
        /// <param name="edFormsUserShareId"></param>
        public static void InsertIntoEdFormsUserReminder(DataTable usersList, string edFormsUserShareId)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "InsertIntoEdFormsUserReminder";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@tblUsers", SqlDbType.Structured);
                parameters[0].Value = usersList;
                parameters[1] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                parameters[1].Value = DateTime.Now;
                parameters[2] = new SqlParameter("@EdFormsUserShareId", SqlDbType.NVarChar, 50);
                parameters[2].Value = edFormsUserShareId;

                command.Parameters.AddRange(parameters);
                command.ExecuteScalar();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Get Submitted EdForms Details by FolderId
        /// </summary>
        /// <param name="status">Status</param>
        /// <param name="folderId">FolderId</param>
        /// <returns></returns>
        public static DataTable GetEdFormsForUserFolder(int status, int logStatus, string uid, int? folderId = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetEdFormsForUserFolder";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[4];
                    parameters[0] = new SqlParameter("@FolderId", SqlDbType.Int);
                    parameters[0].Value = folderId;
                    parameters[1] = new SqlParameter("@Status", SqlDbType.Int);
                    parameters[1].Value = status;
                    parameters[2] = new SqlParameter("@UID", SqlDbType.UniqueIdentifier);
                    parameters[2].Value = new Guid(uid);
                    parameters[3] = new SqlParameter("@LogStatus", SqlDbType.Int);
                    parameters[3].Value = logStatus;
                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);
                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Get Shared EdForms details by specific Ids
        /// </summary>
        /// <param name="edFormsUserShareId">EdFormUserShareId</param>
        /// <returns></returns>
        public static DataTable GetEdFormsUserShareById(string edFormsUserShareId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetEdFormsUserShareById";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@EdFormsUserShareIds", SqlDbType.NVarChar, -1);
                    parameters[0].Value = edFormsUserShareId;
                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);
                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Update EdFormsShare when Moving to tab
        /// </summary>
        /// <param name="edFormsUserShareId">EdFormUserShareId</param>
        /// <param name="folderId">FolderId</param>
        /// <param name="dividerId">DividerId</param>
        /// <param name="uid">UID</param>
        /// <returns></returns>
        public static int MoveEdFormsUserShare(string edFormsUserShareId, int folderId, int dividerId, string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "MoveEdFormsUserShare";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[5];
                    parameters[0] = new SqlParameter("@EdFormsUserShareId", SqlDbType.NVarChar, 50);
                    parameters[0].Value = edFormsUserShareId;
                    parameters[1] = new SqlParameter("@MovedBy", SqlDbType.UniqueIdentifier, 50);
                    parameters[1].Value = new Guid(uid);
                    parameters[2] = new SqlParameter("@FolderId", SqlDbType.Int);
                    parameters[2].Value = folderId;
                    parameters[3] = new SqlParameter("@DividerId", SqlDbType.Int);
                    parameters[3].Value = dividerId;
                    parameters[4] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[4].Value = DateTime.Now;
                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToInt32(Convert.ToString(value));
                }
                catch (Exception ex)
                {
                    return 0;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Update EdFormsShare While Renaming or Deleting
        /// </summary>
        /// <param name="fileName">FileName</param>
        /// <param name="edFormsUserShareId">EdFormsUserShareId</param>
        /// <param name="IsRename">IsRename</param>
        /// <param name="isDeleted">IsDeleted</param>
        /// <returns></returns>
        public static string UpdateEdFormsForUserShare(string fileName, string edFormsUserShareId, bool IsRename, bool? isDeleted = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateEdFormsForUserShare";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[5];
                    parameters[0] = new SqlParameter("@EdFormsUserShareId", SqlDbType.NVarChar, 50);
                    parameters[0].Value = edFormsUserShareId;
                    parameters[1] = new SqlParameter("@FileName", SqlDbType.NVarChar, 100);
                    parameters[1].Value = fileName;
                    parameters[2] = new SqlParameter("@IsDeleted", SqlDbType.Bit);
                    parameters[2].Value = isDeleted;
                    parameters[3] = new SqlParameter("@IsRename", SqlDbType.Bit);
                    parameters[3].Value = IsRename;
                    parameters[4] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                    parameters[4].Value = DateTime.Now;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Change Status for EdFormsShare
        /// </summary>
        /// <param name="edFormsUserShareId">EdFormsUserShareId</param>
        /// <param name="status">Status</param>
        public static void ChangeEdFormsUserShareStatus(string edFormsUserShareId, int status)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "ChangeEdFormsUserShareStatus";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@Status", SqlDbType.Int);
                parameters[0].Value = status;
                parameters[1] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                parameters[1].Value = DateTime.Now;
                parameters[2] = new SqlParameter("@EdFormsUserShareId", SqlDbType.NVarChar, 50);
                parameters[2].Value = edFormsUserShareId;

                command.Parameters.AddRange(parameters);
                command.ExecuteScalar();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Check If Any Submitted EdForms exist in Pending for Specific Folder
        /// </summary>
        /// <param name="folderId">FolderId</param>
        /// <param name="status">Status</param>
        /// <returns></returns>
        public static bool CheckIfEdFormsExists(int folderId, int status)
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "CheckIfEdFormsExists";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter[] parameters = new SqlParameter[2];
                parameters[0] = new SqlParameter("@Status", SqlDbType.Int);
                parameters[0].Value = status;
                parameters[1] = new SqlParameter("@FolderId", SqlDbType.Int);
                parameters[1].Value = folderId;

                command.Parameters.AddRange(parameters);
                var value = command.ExecuteScalar();
                return Convert.ToBoolean(value);
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// Get Users list for sending Reminser while submitting EdForms
        /// </summary>
        /// <param name="edFormsUserShareId"></param>
        /// <returns></returns>
        public static DataTable GetEdFormsUserReminder(string edFormsUserShareId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetEdFormsUserReminder";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@EdFormsUserShareId", SqlDbType.NVarChar, 50);
                    parameters[0].Value = edFormsUserShareId;
                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);
                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Updates the ed forms user folder.
        /// </summary>
        /// <param name="edFormsUserShareId">The ed forms user share identifier.</param>
        /// <param name="folderId">The folder identifier.</param>
        /// <returns></returns>
        public static string UpdateEdFormsUserFolder(string edFormsUserShareId, int folderId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateEdFormsUserFolder";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@EdFormsUserShareId", SqlDbType.NVarChar, 50);
                    parameters[0].Value = edFormsUserShareId;
                    parameters[1] = new SqlParameter("@FolderId", SqlDbType.Int);
                    parameters[1].Value = folderId;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Insert or Update Account login Detail
        /// </summary>
        public static void InsertAccountLoginDetail(string uid, bool IsActive, DateTime ActiveDate, DateTime LoginDate, DateTime? LogoutDate)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertAccountLoginDetail";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[6];
                    parameters[0] = new SqlParameter("@uid", SqlDbType.NVarChar, 50);
                    parameters[0].Value = uid;
                    parameters[1] = new SqlParameter("@IsActive", SqlDbType.Bit);
                    parameters[1].Value = IsActive;
                    parameters[2] = new SqlParameter("@ActiveDate", SqlDbType.DateTime);
                    parameters[2].Value = ActiveDate;
                    parameters[3] = new SqlParameter("@LoginDate", SqlDbType.DateTime);
                    parameters[3].Value = LoginDate;
                    parameters[4] = new SqlParameter("@LogoutDate", SqlDbType.DateTime);
                    parameters[4].Value = LogoutDate;
                    parameters[5] = new SqlParameter("@CreateDate", SqlDbType.DateTime);
                    parameters[5].Value = DateTime.Now;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    //return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Insert or Update Account login Detail
        /// </summary>
        public static void UpdateActiveDate_AccountLoginDetail(string uid, DateTime ActiveDate)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateActiveDate_AccountLoginDetail";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@uid", SqlDbType.NVarChar, 50);
                    parameters[0].Value = uid;
                    parameters[1] = new SqlParameter("@ActiveDate", SqlDbType.DateTime);
                    parameters[1].Value = ActiveDate;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    //return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Insert or Update Account login Detail
        /// </summary>
        public static void UpdateLogoutDate_AccountLoginDetail(string uid, DateTime LogoutDate)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateLogoutDate_AccountLoginDetail";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@uid", SqlDbType.NVarChar, 50);
                    parameters[0].Value = uid;
                    parameters[1] = new SqlParameter("@LogoutDate", SqlDbType.DateTime);
                    parameters[1].Value = LogoutDate;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    //return Convert.ToString(value);
                }
                catch (Exception ex)
                {
                    //return "";
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Get online users count for admin
        /// </summary>
        /// <returns></returns>
        public static int GetOnlineUserCount()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetOnlineUserCount";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@CurrentDate", SqlDbType.DateTime);
                    parameters[0].Value = DateTime.Now;

                    command.Parameters.AddRange(parameters);
                    var value = command.ExecuteScalar();
                    return Convert.ToInt32(value);
                }
                catch (Exception ex)
                {
                    return 0;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Get online users details for admin
        /// </summary>
        /// <returns></returns>
        public static DataTable GetOnlineUserDetails()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetOnlineUserDetails";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@CurrentDate", SqlDbType.DateTime);
                    parameters[0].Value = DateTime.Now;

                    command.Parameters.AddRange(parameters);
                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);
                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        #endregion

        #region FolderReminder

        public static DataTable GetFolderRemindersForAlert()
        {
            SqlConnection connection = new SqlConnection(ConnectionString);
            try
            {
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter();
                connection.Open();
                SqlCommand sqlComm = new SqlCommand("eFileFolder_GetFolderRemindersForAlert", connection);
                sqlComm.CommandType = CommandType.StoredProcedure;
                da.SelectCommand = sqlComm;
                da.Fill(dt);
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
        }

        #endregion

        #region Comprehensive Search

        /// <summary>
        /// Gets the coprehensive search for folder.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="officeId">The office identifier.</param>
        /// <returns></returns>
        public static DataSet GetCoprehensiveSearchForFolder(string key, string officeId, string uid)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "eFileFolders_GetCoprehensivSearchFolder";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@Key", SqlDbType.NVarChar);
                    parameters[0].Value = key;
                    parameters[1] = new SqlParameter("@OfficeID", SqlDbType.NVarChar);
                    parameters[1].Value = officeId;
                    parameters[2] = new SqlParameter("@UID", SqlDbType.NVarChar);
                    parameters[2].Value = uid;

                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataSet dataset = new DataSet();

                    dataAdapter.Fill(dataset);

                    return dataset;
                }
                catch (Exception ex)
                {
                    return new DataSet();
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Gets the coprehensive search for docs.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="officeId">The office identifier.</param>
        /// <returns></returns>
        public static DataSet GetCoprehensiveSearchForDocs(string key, string officeId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "eFileFolders_GetCoprehensivSearchDocs";
                    command.CommandType = CommandType.StoredProcedure;
                    SqlParameter[] parameters = new SqlParameter[2];
                    parameters[0] = new SqlParameter("@Key", SqlDbType.NVarChar);
                    parameters[0].Value = key;
                    parameters[1] = new SqlParameter("@OfficeID", SqlDbType.NVarChar);
                    parameters[1].Value = officeId;

                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataSet dataset = new DataSet();

                    dataAdapter.Fill(dataset);

                    return dataset;
                }
                catch (Exception ex)
                {
                    return new DataSet();
                }
                finally
                {
                    connection.Close();
                }
        }

        #endregion

        #region AutoUpload

        /// <summary>
        /// Gets the folder and divider details.
        /// </summary>
        /// <param name="folderId">The folder identifier.</param>
        /// <param name="dividerId">The divider identifier.</param>
        /// <param name="officeId">The office identifier.</param>
        /// <returns></returns>
        public static DataTable GetFolderAndDividerDetails(int folderId, int dividerId, string officeId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetFolderAndDividerDetails";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@FolderId", SqlDbType.Int);
                    parameters[0].Value = folderId;
                    parameters[1] = new SqlParameter("@DividerId", SqlDbType.Int);
                    parameters[1].Value = dividerId;
                    parameters[2] = new SqlParameter("@OfficeId", SqlDbType.UniqueIdentifier);
                    parameters[2].Value = new Guid(officeId);
                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);
                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        #endregion

        #region EstimatorPricing

        /// <summary>
        /// Updates the delete estimator price.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="price">The price.</param>
        /// <param name="isDeleted">The is deleted.</param>
        /// <returns>
        /// Updated Rows Count
        /// </returns>
        public static int UpdateDeleteEstimatorPrice(int id, decimal? price, bool? isDeleted = null)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateDeleteEstimatorPrice";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[3];
                    parameters[0] = new SqlParameter("@Id", SqlDbType.Int);
                    parameters[0].Value = id;
                    parameters[1] = new SqlParameter("@Price", SqlDbType.Decimal);
                    parameters[1].Value = price;
                    parameters[2] = new SqlParameter("@IsDeleted", SqlDbType.Bit);
                    parameters[2].Value = isDeleted;
                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    var value = command.ExecuteScalar();
                    return Convert.ToInt32(value);
                }
                catch (Exception ex)
                {
                    return 0;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Gets the estimator pricing.
        /// </summary>
        /// <param name="recordTypeId">The record type identifier.</param>
        /// <returns>
        /// Pricing Details
        /// </returns>
        public static DataTable GetEstimatorPricing(int recordTypeId)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetEstimatorPricing";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@RecordTypeId", SqlDbType.Int);
                    parameters[0].Value = recordTypeId;
                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);
                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Gets the record types.
        /// </summary>
        /// <returns>
        /// Record type
        /// </returns>
        public static DataTable GetRecordTypes()
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetRecordTypes";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);
                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        #endregion

        #region DriveSizeLogs

        /// <summary>
        /// Creates the drive size logs.
        /// </summary>
        /// <param name="size">The size.</param>
        public static void CreateDriveSizeLogs(string size)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "InsertDriveSizeLog";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@Size", SqlDbType.NVarChar);
                    parameters[0].Value = size;
                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Gets the drive size logs.
        /// </summary>
        /// <returns></returns>
        public static DataTable GetDriveSizeLogs()
        {

            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetDriveSizeLog";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);
                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        #endregion

        #region Ocr

        /// <summary>
        /// Updates the documents for ocr.
        /// </summary>
        /// <param name="documents">The documents.</param>
        public static void UpdateOcrStatus(DataTable documents)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "UpdateOcrStatus";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@Documents", SqlDbType.Structured);
                    parameters[0].Value = documents;

                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    connection.Close();
                }
        }

        /// <summary>
        /// Gets the requested ocr docs.
        /// </summary>
        /// <param name="ocrStatus">The ocr status.</param>
        /// <returns></returns>
        public static DataTable GetRequestedOcrDocs(int ocrStatus)
        {
            using (SqlConnection connection = new SqlConnection(ConnectionString))
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand();
                    command.Connection = connection;
                    command.CommandText = "GetRequestedOcrDocs";
                    command.CommandType = CommandType.StoredProcedure;

                    SqlParameter[] parameters = new SqlParameter[1];
                    parameters[0] = new SqlParameter("@OcrStatus", SqlDbType.Int);
                    parameters[0].Value = ocrStatus;

                    command.Parameters.AddRange(parameters);

                    SqlDataAdapter dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = command;

                    DataTable table = new DataTable();
                    dataAdapter.Fill(table);
                    return table;
                }
                catch (Exception ex)
                {
                    return null;
                }
                finally
                {
                    connection.Close();
                }
        }

        #endregion
    }

    public class LoginTimeRestriction
    {
        public string UID { get; set; }
        [DefaultValue(0)]
        public int Day { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public bool IsTimeRestrictied { get; set; }
    }

    public class IndexKeyValue
    {
        public string IndexKey { get; set; }
        public string IndexValue { get; set; }
        public int IndexId { get; set; }
    }

    public class LockedTabUsers
    {
        public string UID { get; set; }
        public bool IsHideLockedTabs { get; set; }
    }

    public class LockedTabs
    {
        public string DividerId { get; set; }
        public bool Locked { get; set; }
    }

    public class LockedFolderTemplate
    {
        public string TemplateID { get; set; }
        public bool? Locked { get; set; }
    }

    public class EdFormsTemplateForAdmin
    {
        public int EdFormsId { get; set; }
    }

    public class EdFormsTemplateForUser
    {
        public int EdFormsUserId { get; set; }
        public int FileFolderId { get; set; }
    }

    public class AuditLog
    {
        public string LogId { get; set; }
        public bool Checked { get; set; }
    }

    public class AssignedUser
    {
        public string UID { get; set; }
    }

    public class AssignedDepartment
    {
        public int DepartmentId { get; set; }
    }

    public class LogKeyValuePairNew
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class FolderTabDetails
    {
        public int FolderId { get; set; }
        public int TabId { get; set; }
    }
}
