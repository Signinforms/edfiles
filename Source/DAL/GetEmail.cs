﻿using System;
using System.Collections.Generic;
using System.Text;
using FormBot.Email;

namespace Shinetech.DAL
{
    public class GetEmail
    {
        /// <summary>
        /// Checks the email.
        /// </summary>
        public void CheckEmail()
        {
            CheckMail checkMail = new CheckMail();
            checkMail.AutoCheckMailForAccount();
        }
    }
}
