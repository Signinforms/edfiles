﻿// The source code generated by DataQuicker2 Code Generator.
// Code Generator Assembly Version: 1.2.0.21138
// Create time: 2008-10-28 13:26:19

using System;
using System.Data;
using System.Text;
using System.Collections.Generic;
using DataQuicker2.Framework;

namespace Shinetech.DAL
{
	/// <summary>
	/// 实体对象Aspnet_Membership。<br />
	/// Entity class Aspnet_Membership.
	/// </summary>
	public class Aspnet_Membership: Table
	{
		#region Properties And Fields
		
		private Column<string> applicationId = new Column<string>();
		/// <summary>
		/// 设置/取得列对象ApplicationId。<br />
		/// Set/Get column instance ApplicationId.
		/// </summary>
		public Column<string> ApplicationId
		{
			get
			{
				return this.applicationId;
			}
			set
			{
				this.applicationId = value;
			}
		}
		
		private Column<string> userId = new Column<string>();
		/// <summary>
		/// 设置/取得列对象UserId。<br />
		/// Set/Get column instance UserId.
		/// </summary>
		public Column<string> UserId
		{
			get
			{
				return this.userId;
			}
			set
			{
				this.userId = value;
			}
		}
		
		private Column<string> password = new Column<string>();
		/// <summary>
		/// 设置/取得列对象Password。<br />
		/// Set/Get column instance Password.
		/// </summary>
		public Column<string> Password
		{
			get
			{
				return this.password;
			}
			set
			{
				this.password = value;
			}
		}
		
		private Column<int> passwordFormat = new Column<int>();
		/// <summary>
		/// 设置/取得列对象PasswordFormat。<br />
		/// Set/Get column instance PasswordFormat.
		/// </summary>
		public Column<int> PasswordFormat
		{
			get
			{
				return this.passwordFormat;
			}
			set
			{
				this.passwordFormat = value;
			}
		}
		
		private Column<string> passwordSalt = new Column<string>();
		/// <summary>
		/// 设置/取得列对象PasswordSalt。<br />
		/// Set/Get column instance PasswordSalt.
		/// </summary>
		public Column<string> PasswordSalt
		{
			get
			{
				return this.passwordSalt;
			}
			set
			{
				this.passwordSalt = value;
			}
		}
		
		private Column<string> mobilePIN = new Column<string>();
		/// <summary>
		/// 设置/取得列对象MobilePIN。<br />
		/// Set/Get column instance MobilePIN.
		/// </summary>
		public Column<string> MobilePIN
		{
			get
			{
				return this.mobilePIN;
			}
			set
			{
				this.mobilePIN = value;
			}
		}
		
		private Column<string> email = new Column<string>();
		/// <summary>
		/// 设置/取得列对象Email。<br />
		/// Set/Get column instance Email.
		/// </summary>
		public Column<string> Email
		{
			get
			{
				return this.email;
			}
			set
			{
				this.email = value;
			}
		}
		
		private Column<string> loweredEmail = new Column<string>();
		/// <summary>
		/// 设置/取得列对象LoweredEmail。<br />
		/// Set/Get column instance LoweredEmail.
		/// </summary>
		public Column<string> LoweredEmail
		{
			get
			{
				return this.loweredEmail;
			}
			set
			{
				this.loweredEmail = value;
			}
		}
		
		private Column<string> passwordQuestion = new Column<string>();
		/// <summary>
		/// 设置/取得列对象PasswordQuestion。<br />
		/// Set/Get column instance PasswordQuestion.
		/// </summary>
		public Column<string> PasswordQuestion
		{
			get
			{
				return this.passwordQuestion;
			}
			set
			{
				this.passwordQuestion = value;
			}
		}
		
		private Column<string> passwordAnswer = new Column<string>();
		/// <summary>
		/// 设置/取得列对象PasswordAnswer。<br />
		/// Set/Get column instance PasswordAnswer.
		/// </summary>
		public Column<string> PasswordAnswer
		{
			get
			{
				return this.passwordAnswer;
			}
			set
			{
				this.passwordAnswer = value;
			}
		}
		
		private Column<bool> isApproved = new Column<bool>();
		/// <summary>
		/// 设置/取得列对象IsApproved。<br />
		/// Set/Get column instance IsApproved.
		/// </summary>
		public Column<bool> IsApproved
		{
			get
			{
				return this.isApproved;
			}
			set
			{
				this.isApproved = value;
			}
		}
		
		private Column<bool> isLockedOut = new Column<bool>();
		/// <summary>
		/// 设置/取得列对象IsLockedOut。<br />
		/// Set/Get column instance IsLockedOut.
		/// </summary>
		public Column<bool> IsLockedOut
		{
			get
			{
				return this.isLockedOut;
			}
			set
			{
				this.isLockedOut = value;
			}
		}
		
		private Column<DateTime> createDate = new Column<DateTime>();
		/// <summary>
		/// 设置/取得列对象CreateDate。<br />
		/// Set/Get column instance CreateDate.
		/// </summary>
		public Column<DateTime> CreateDate
		{
			get
			{
				return this.createDate;
			}
			set
			{
				this.createDate = value;
			}
		}
		
		private Column<DateTime> lastLoginDate = new Column<DateTime>();
		/// <summary>
		/// 设置/取得列对象LastLoginDate。<br />
		/// Set/Get column instance LastLoginDate.
		/// </summary>
		public Column<DateTime> LastLoginDate
		{
			get
			{
				return this.lastLoginDate;
			}
			set
			{
				this.lastLoginDate = value;
			}
		}
		
		private Column<DateTime> lastPasswordChangedDate = new Column<DateTime>();
		/// <summary>
		/// 设置/取得列对象LastPasswordChangedDate。<br />
		/// Set/Get column instance LastPasswordChangedDate.
		/// </summary>
		public Column<DateTime> LastPasswordChangedDate
		{
			get
			{
				return this.lastPasswordChangedDate;
			}
			set
			{
				this.lastPasswordChangedDate = value;
			}
		}
		
		private Column<DateTime> lastLockoutDate = new Column<DateTime>();
		/// <summary>
		/// 设置/取得列对象LastLockoutDate。<br />
		/// Set/Get column instance LastLockoutDate.
		/// </summary>
		public Column<DateTime> LastLockoutDate
		{
			get
			{
				return this.lastLockoutDate;
			}
			set
			{
				this.lastLockoutDate = value;
			}
		}
		
		private Column<int> failedPasswordAttemptCount = new Column<int>();
		/// <summary>
		/// 设置/取得列对象FailedPasswordAttemptCount。<br />
		/// Set/Get column instance FailedPasswordAttemptCount.
		/// </summary>
		public Column<int> FailedPasswordAttemptCount
		{
			get
			{
				return this.failedPasswordAttemptCount;
			}
			set
			{
				this.failedPasswordAttemptCount = value;
			}
		}
		
		private Column<DateTime> failedPasswordAttemptWindowStart = new Column<DateTime>();
		/// <summary>
		/// 设置/取得列对象FailedPasswordAttemptWindowStart。<br />
		/// Set/Get column instance FailedPasswordAttemptWindowStart.
		/// </summary>
		public Column<DateTime> FailedPasswordAttemptWindowStart
		{
			get
			{
				return this.failedPasswordAttemptWindowStart;
			}
			set
			{
				this.failedPasswordAttemptWindowStart = value;
			}
		}
		
		private Column<int> failedPasswordAnswerAttemptCount = new Column<int>();
		/// <summary>
		/// 设置/取得列对象FailedPasswordAnswerAttemptCount。<br />
		/// Set/Get column instance FailedPasswordAnswerAttemptCount.
		/// </summary>
		public Column<int> FailedPasswordAnswerAttemptCount
		{
			get
			{
				return this.failedPasswordAnswerAttemptCount;
			}
			set
			{
				this.failedPasswordAnswerAttemptCount = value;
			}
		}
		
		private Column<DateTime> failedPasswordAnswerAttemptWindowStart = new Column<DateTime>();
		/// <summary>
		/// 设置/取得列对象FailedPasswordAnswerAttemptWindowStart。<br />
		/// Set/Get column instance FailedPasswordAnswerAttemptWindowStart.
		/// </summary>
		public Column<DateTime> FailedPasswordAnswerAttemptWindowStart
		{
			get
			{
				return this.failedPasswordAnswerAttemptWindowStart;
			}
			set
			{
				this.failedPasswordAnswerAttemptWindowStart = value;
			}
		}
		
		private Column<string> comment = new Column<string>();
		/// <summary>
		/// 设置/取得列对象Comment。<br />
		/// Set/Get column instance Comment.
		/// </summary>
		public Column<string> Comment
		{
			get
			{
				return this.comment;
			}
			set
			{
				this.comment = value;
			}
		}
		
		#endregion
	
		/// <summary>
		/// 构造实体对象Aspnet_Membership。<br />
		/// Construct entity instance Aspnet_Membership.
		/// </summary>
		public Aspnet_Membership() : base()
		{
		}
		
		/// <summary>
		/// 构造实体对象Aspnet_Membership。<br />
		/// Construct entity instance Aspnet_Membership.
		/// </summary>
		/// <param name="userId" />
		public Aspnet_Membership(string userId) : base(userId)
		{
		}
		
		/// <summary>
		/// 构造实体对象Aspnet_Membership。<br />
		/// Construct entity instance Aspnet_Membership.
		/// </summary>
		/// <param name="userId" />
		/// <param name="connection">已打开的数据库连接对象。</param>
		public Aspnet_Membership(string userId, IDbConnection connection) : base(userId, connection)
		{
		}
		
		/// <summary>
		/// 构造实体对象Aspnet_Membership。<br />
		/// Construct entity instance Aspnet_Membership.
		/// </summary>
		/// <param name="userId" />
		/// <param name="transaction">已开启的事务对象。</param>
		public Aspnet_Membership(string userId, IDbTransaction transaction) : base(userId, transaction)
		{
		}
		
		/// <summary>
		/// 绑定实体对象与数据库对象。<br />
		/// Bind entity instance to database.
		/// </summary>
		protected override void Bind()
		{
			
			this.BindPrimaryKey(this.userId, ColumnValueType.IntManaged);
				
			this.BindTableName("aspnet_Membership");
			
			this.BindColumn(this.applicationId, "ApplicationId");
					
			this.BindColumn(this.userId, "UserId");
					
			this.BindColumn(this.password, "Password", 128);
					
			this.BindColumn(this.passwordFormat, "PasswordFormat");
					
			this.BindColumn(this.passwordSalt, "PasswordSalt", 128);
					
			this.BindColumn(this.mobilePIN, "MobilePIN", 16);
					
			this.BindColumn(this.email, "Email", 256);
					
			this.BindColumn(this.loweredEmail, "LoweredEmail", 256);
					
			this.BindColumn(this.passwordQuestion, "PasswordQuestion", 256);
					
			this.BindColumn(this.passwordAnswer, "PasswordAnswer", 128);
					
			this.BindColumn(this.isApproved, "IsApproved");
					
			this.BindColumn(this.isLockedOut, "IsLockedOut");
					
			this.BindColumn(this.createDate, "CreateDate");
					
			this.BindColumn(this.lastLoginDate, "LastLoginDate");
					
			this.BindColumn(this.lastPasswordChangedDate, "LastPasswordChangedDate");
					
			this.BindColumn(this.lastLockoutDate, "LastLockoutDate");
					
			this.BindColumn(this.failedPasswordAttemptCount, "FailedPasswordAttemptCount");
					
			this.BindColumn(this.failedPasswordAttemptWindowStart, "FailedPasswordAttemptWindowStart");
					
			this.BindColumn(this.failedPasswordAnswerAttemptCount, "FailedPasswordAnswerAttemptCount");
					
			this.BindColumn(this.failedPasswordAnswerAttemptWindowStart, "FailedPasswordAnswerAttemptWindowStart");
					
			this.BindColumn(this.comment, "Comment", 1073741823);
					
		}
	}
}
