// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 列对象类（数据表中的列）。<br/>
	/// Column class, stands for data column.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class Column<T> : IColumn
	{
		private IEntity parentEntity;
		private string columnName;
		private object value;
		private int maxLength;
		private bool isChanged;
		private ColumnValueType columValueType = ColumnValueType.Unmanaged;

		/// <summary>
		/// 构造列对象<br/>Construct column instance.
		/// </summary>
		public Column()
		{
		}

		/// <summary>
		/// 设置该列的值为null。<br/>
		/// Set the column value to null.
		/// </summary>
		public void SetToNull()
		{
			this.value = null;
			this.IsChanged = true;
		}

		#region IColumn Members
		/// <summary>
		/// 取得当前列所属的表对象。<br/>
		/// Get the parent <see cref="Table"/> instance of current instance.
		/// </summary>
		public IEntity ParentEntity
		{
			get
			{
				return this.parentEntity;
			}
			protected internal set
			{
				this.parentEntity = value;
			}
		}

		IEntity IColumn.ParentEntity
		{
			get
			{
				return this.ParentEntity;
			}
			set
			{
				this.ParentEntity = value;
			}
		}

		/// <summary>
		/// 取得当前字段映射到数据库中的列名。<br/>
		/// Get the current instance mapped data column name.
		/// </summary>
		public string ColumnName
		{
			get
			{
				return this.columnName;
			}
			protected internal set
			{
				this.columnName = value;
			}
		}

		string IColumn.ColumnName
		{
			get
			{
				return this.ColumnName;
			}
			set
			{
				this.ColumnName = value;
			}
		}

		/// <summary>
		/// 设置/取得当前字段的值，如果当前字段是字符串型，并且设置了MaxLength，那么超出该最大长度的字符将被自动截断。
		/// 如果当前列值为空，则返回当前列类型的默认值。<br/>
		/// Set/Get the value of current column. If current column is string type and set the MaxLength, the exceeded characters will be truncated automatically.
		/// If current column value is null, return default value of current column type.
		/// </summary>
		public T Value
		{
			get
			{
				if (this.value == null) return default(T);

				if (this.maxLength > 0 && this.value is string)
				{
					string strValue = this.value.ToString().Trim();
					object returnValue;
					if (this.maxLength < strValue.Length)
						returnValue = strValue.Substring(0, this.maxLength);
					else
						returnValue = strValue;
					return (T)returnValue;
				}

				return this.ToGenericValue();
			}
			set
			{
				if (this.ParentEntity != null)
				{
					Table table = this.ParentEntity as Table;
					if (table != null && table.PrimaryKey == this as IColumn)
						this.ParentEntity.IsExist = false;
				}

				if (value != null && this.maxLength > 0 && this.GenericType == typeof(string))
				{
					string strValue = value.ToString().Trim();
					if (this.maxLength < strValue.Length)
						this.value = strValue.Substring(0, this.maxLength);
					else
						this.value = strValue;
				}
				else
					this.value = value;

				this.IsChanged = true;
			}
		}

		private T ToGenericValue()
		{
			if (this.value is uint
				|| this.value is short
				|| this.value is ushort
				|| this.value is long)
			{
				return (T)Convert.ChangeType(this.value, typeof(int));
			}

			Type valueType = this.value.GetType();
			Type genericType = typeof(T);
			if (genericType == valueType || (genericType.IsSubclassOf(typeof(Enum)) && valueType.IsValueType))
				return (T)this.value;
			else
				return (T)Convert.ChangeType(this.value, typeof(T));
		}

		/// <summary>
		/// 取得泛型的实现类型。<br/>
		/// Get generic type.
		/// </summary>
		public Type GenericType
		{
			get
			{
				return typeof(T);
			}
		}

		object IColumn.Value
		{
			get
			{
				if (this.maxLength > 0 && this.GenericType == typeof(string))
				{
					string strValue = this.value.ToString().Trim();
					if (this.maxLength < strValue.Length)
						return strValue.Substring(0, this.maxLength);
				}
				return this.value;
			}
			set
			{
				if (value != null && this.maxLength > 0 && this.GenericType == typeof(string))
				{
					string strValue = value.ToString().Trim();
					if (strValue.Length <= this.maxLength)
						this.value = strValue;
					else
						this.value = strValue.Substring(0, this.maxLength);
				}
				else
					this.value = value;
			}
		}

		string IColumn.Null2EmptyStringValue
		{
			get
			{
				return this.Null2EmptyStringValue;
			}
		}

		/// <summary>
		/// 当列值为Null时，返回空字符串，否则将当前列值转换成字符串形式返回。<br/>
		/// Return empty string when column value is null. Otherwise convert current column value to string and return.
		/// </summary>
		public string Null2EmptyStringValue
		{
			get
			{
				if (this.value == null) return "";
				return this.Value.ToString();
			}
		}

		/// <summary>
		/// 取得当前列值的类型。<br/>
		/// Get column value type.
		/// </summary>
		public ColumnValueType ColumnValueType
		{
			get
			{
				return this.columValueType;
			}
			protected internal set
			{
				this.columValueType = value;
			}
		}

		ColumnValueType IColumn.ColumnValueType
		{
			get
			{
				return this.ColumnValueType;
			}
			set
			{
				this.ColumnValueType = value;
			}
		}

		/// <summary>
		/// 取得当前列值是否为空。<br/>
		/// Get true when current column value is empty.
		/// </summary>
		public bool IsNull
		{
			get
			{
				return this.value == null;
			}
		}

		/// <summary>
		/// 返回转换为String类型的当前列值。<br/>
		/// Convert current instance to string.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			if (this.value == null) return null;
			return this.value.ToString();
		}

		/// <summary>
		/// 取得当前列值的最大长度，仅对于字符串类型字段有效，将阶段超出MaxLength部分的字符。<br/>
		/// Get the maximum length of current column value. It's only useful for string type column. It will truncate exceeded characters automatically.
		/// </summary>
		public int MaxLength
		{
			get
			{
				return this.maxLength;
			}
			protected internal set
			{
				this.maxLength = value;
			}
		}

		int IColumn.MaxLength
		{
			get
			{
				return this.MaxLength;
			}
			set
			{
				this.MaxLength = value;
			}
		}

		#endregion

		#region IDbObject Members

		string IDbObject.DbObjectName
		{
			get
			{
				return this.ColumnName;
			}
		}

		/// <summary>
		/// 取得当前列的值是否已改变。<br/>
		/// Get true when current instance value has been changed.
		/// </summary>
		public bool IsChanged
		{
			get
			{
				return this.isChanged;
			}
			protected internal set
			{
				this.isChanged = value;
				this.ParentEntity.IsChanged = true;
			}
		}

		bool IDbObject.IsChanged
		{
			get
			{
				return this.IsChanged;
			}
			set
			{
				this.IsChanged = value;
			}
		}

		DbObjectType IDbObject.DbObjectType
		{
			get
			{
				return DbObjectType.Column;
			}
		}

		#endregion
	}
}
