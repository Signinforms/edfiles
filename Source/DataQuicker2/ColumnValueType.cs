// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 主键的类型。<br/>
	/// The primary key type.
	/// </summary>
	public enum ColumnValueType
	{
		/// <summary>
		/// 非DataQuicker2托管的。<br/>
		/// Unmanaged by DataQuicker2.
		/// </summary>
		Unmanaged = 0,
		/// <summary>
		/// 数据库自增长字段。<br/>
		/// Database auto increase column.
		/// </summary>
		DbAutoIncrease = 1, 
		/// <summary>
		/// DataQuicker2托管的INT值填充，字段类型一般应为INT类型，也可以为字符串类型，但是性能损失很大，并且不利于索引。<br/>
		/// Managed to be filled in integer value. Normally the column is INT. The data column also can be string, but the performance will cut down obviously, and it's not convenient to database index.
		/// </summary>
		IntManaged = 2,
		/// <summary>
		/// DataQuicker2托管的GUID值填充，字段类型一般应为Varchar(32)。<br/>
		/// Managed to be filled in GUID value. Normally the column is Varchar(32).
		/// </summary>
		GuidManaged = 4
	}
}
