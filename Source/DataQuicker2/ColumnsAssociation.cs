// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
    internal class ColumnsAssociation
    {
        private List<IColumn> mainColumns;
        private List<IColumn> secondColumns;
        private AssociationType associationType;

        public ColumnsAssociation(IColumn[] mainColumns, IColumn[] secondColumns)
        {
            this.mainColumns = new List<IColumn>();
            this.secondColumns = new List<IColumn>();

            for (int i = 0; i < mainColumns.Length; i++)
            {
                if (!mainColumns[i].GenericType.IsSubclassOf(typeof(Table))
                    && !mainColumns[i].GenericType.IsSubclassOf(typeof(View))
                    && !secondColumns[i].GenericType.IsSubclassOf(typeof(Table))
                    && !secondColumns[i].GenericType.IsSubclassOf(typeof(View)))
                {
                    if (mainColumns[i].GenericType != secondColumns[i].GenericType)
                        throw new InvalidOperationException("请保证关联的字段类型完全一致。");
                }

                this.mainColumns.Add(mainColumns[i]);
                this.secondColumns.Add(secondColumns[i]);
            }
        }

        public ColumnsAssociation(IColumn[] mainColumns, IColumn[] secondColumns, AssociationType associationType)
            : this(mainColumns, secondColumns)
        {
            this.associationType = associationType;
        }

        public List<IColumn> MainColumns
        {
            get
            {
                return this.mainColumns;
            }
        }

        public List<IColumn> SecondColumns
        {
            get
            {
                return this.secondColumns;
            }
        }

        public AssociationType AssociationType
        {
            get
            {
                return this.associationType;
            }
        }
    }
}
