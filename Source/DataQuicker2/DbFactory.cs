// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.IO;
using System.Xml;
using System.Data;
using System.Reflection;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using DataQuicker2.Framework.Common;

namespace DataQuicker2.Framework
{
    /// <summary>
    /// IDbConnection数据库链接对象工厂<br/>
    /// IDbConnection instance factory.
    /// </summary>
    public static class DbFactory
    {
        private static object syncObject = new object();
        private static List<string> executedConnectionNames = new List<string>();

        /// <summary>
        /// 创建默认的IDbConnection数据库连接对象，返回的连接对象并未打开。<br/>
        /// Create the default database connection without opening it from the function.
        /// </summary>
        /// <returns></returns>
        public static IDbConnection CreateConnection()
        {
            return CreateConnection("Default");
        }

        /// <summary>
        /// 创建特定的数据库连接对象，返回的连接对象并未打开。<br/>
        /// Create the special database connection without opening it from the function.
        /// </summary>
        /// <param name="connectionName"></param>
        /// <returns></returns>
        public static IDbConnection CreateConnection(string connectionName)
        {
            string strProviderName = GetConnectionStringSettings(connectionName).ProviderName;
            string strConnectionString = ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
            CheckScriptRegister(connectionName, strProviderName, strConnectionString);
            return CreateDbConnection(strProviderName, strConnectionString);
        }

        /// <summary>
        /// 创建默认的IDbConnection数据库连接对象，返回的连接对象已经打开。<br/>
        /// Create the default database connection without opening it from the function.
        /// </summary>
        /// <returns></returns>
        public static IDbConnection CreateOpenConnection()
        {
            IDbConnection conn = CreateConnection("Default");
            conn.Open();
            return conn;
        }

        /// <summary>
        /// 创建默认的IDbConnection数据库连接对象，返回的连接对象已经打开。<br/>
        /// Create the default database connection without opening it from the function.
        /// </summary>
        /// <param name="connectionName"></param>
        /// <returns></returns>
        public static IDbConnection CreateOpenConnection(string connectionName)
        {
            IDbConnection conn = CreateConnection(connectionName);
            conn.Open();
            return conn;
        }

        private static IDbConnection CreateDbConnection(string providerName, string connectionString)
        {
            IDbConnection connection = null;
            switch (providerName)
            {
                case "System.Data.SqlClient":
                    connection = SqlClientFactory.Instance.CreateConnection();
                    break;
                case "System.Data.OleDb":
                    connection = System.Data.OleDb.OleDbFactory.Instance.CreateConnection();
                    break;
                case "System.Data.OracleClient":
                    connection = System.Data.OracleClient.OracleClientFactory.Instance.CreateConnection();
                    break;
                case "System.Data.Odbc":
                    connection = System.Data.Odbc.OdbcFactory.Instance.CreateConnection();
                    break;
            }
            if (connection == null)
                throw new NotSupportedException("数据库连接字符串中的providerName [" + providerName + "] 不被支持。");

            connection.ConnectionString = connectionString;
            return connection;
        }

        private static string GetManifestFile(string providerName)
        {
            switch (providerName)
            {
                case "System.Data.SqlClient":
                    return "DataQuicker2.Framework.SqlScript.sql";
            }

            return null;
        }

        private static void CheckScriptRegister(string connectionName, string providerName, string connectionString)
        {
            if (!executedConnectionNames.Contains(connectionName))
            {
                lock (syncObject)
                {
                    if (!executedConnectionNames.Contains(connectionName))
                    {
                        string strManifestFileName = GetManifestFile(providerName);
                        if (Kit.IsEmpty(strManifestFileName)) return;

                        Stream fsManifest = typeof(DbFactory).Assembly.GetManifestResourceStream(strManifestFileName);
                        if (fsManifest == null) return;

                        string strSQL = "";
                        StreamReader sr = new StreamReader(fsManifest);
                        try
                        {
                            strSQL = sr.ReadToEnd();
                        }
                        finally
                        {
                            sr.Close();
                            fsManifest.Close();
                        }

                        executedConnectionNames.Add(connectionName);

                        if (Kit.IsEmpty(strSQL)) return;

                        IDbConnection connection = CreateDbConnection(providerName, connectionString);
                        connection.Open();
                        try
                        {
                            string[] strSQLArray = SplitSQL(strSQL);
                            foreach (string sql in strSQLArray)
                            {
                                using (IDbCommand command = connection.CreateCommand())
                                {
                                    command.CommandType = CommandType.Text;
                                    command.CommandText = sql;
                                    command.ExecuteNonQuery();
                                }
                            }
                        }
                        finally
                        {
                            connection.Close();
                        }
                    }
                }
            }
        }

        private static string[] SplitSQL(string sql)
        {
            int index = 0;
            int num = 0;
            string str = sql.Trim(new char[] { ' ', '\r', '\n' });
            str = str + "\r\n\r\n";
            while ((index = str.IndexOf("GO\r\n\r\n", index + 6)) >= 0)
            {
                num++;
            }

            string[] strArr = new string[num];
            string tmp;
            int i = 0;
            while (str.Length > 0)
            {
                index = str.IndexOf("GO\r\n\r\n");
                if (index < 0)
                {
                    tmp = str.Trim(new char[] { ' ', '\r', '\n' });
                    tmp = tmp.Replace("GO\r\n", "\r\n");
                    strArr[i++] = tmp;
                    break;
                }
                tmp = str.Substring(0, index).Trim(new char[] { ' ', '\r', '\n' });
                tmp = tmp.Replace("GO\r\n", "\r\n");
                strArr[i++] = tmp;
                str = str.Substring(index + 6);
            }
            return strArr;
        }

        private static ConnectionStringSettings GetConnectionStringSettings(string connectionName)
        {
            if (ConfigurationManager.ConnectionStrings == null || ConfigurationManager.ConnectionStrings[connectionName] == null)
            {
                string message = "在应用程序配置文件的connectionStrings节点中没有找到<add name=\"{0}\" />数据库连接名称配置。";
                message = string.Format(message, connectionName);
                throw new NotFoundConnectionStringInConfigurationException(message);
            }
            return ConfigurationManager.ConnectionStrings[connectionName];
        }

        /// <summary>
        /// 创建一个与数据库连接对象关联的持久层解析对象。<br/>
        /// Create persist analyzer instance associated with IDbConnection instance.
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public static IProvider CreateProvider(IDbConnection connection)
        {
            return CreateProviderByName(connection.GetType().Namespace);
        }

        /// <summary>
        /// 创建一个与数据库连接名称关联的持久层解析对象。<br/>
        /// Create persist analyzer instance associated with database connection name.
        /// </summary>
        /// <param name="connectionName"></param>
        /// <returns></returns>
        public static IProvider CreateProvider(string connectionName)
        {
            return CreateProviderByName(GetConnectionStringSettings(connectionName).ProviderName);
        }

        /// <summary>
        /// 创建一个与数据库默认连接对象关联的持久对象。<br/>
        /// Create provider instance associated with default IDbConnection instance.
        /// </summary>
        /// <returns></returns>
        public static IProvider CreateProvider()
        {
            return CreateProviderByName(GetConnectionStringSettings("Default").ProviderName);
        }

        private static IProvider CreateProviderByName(string providerName)
        {
            IProvider provider = null;
            switch (providerName)
            {
                case "System.Data.SqlClient":
                    provider = new SqlProvider();
                    break;
            }
            if (provider == null)
                throw new NotSupportedException("数据库连接字符串中的providerName [" + providerName + "] 不被支持。");

            return provider;
        }

        /// <summary>
        /// 获取默认的数据库操作类。<br/>
        /// Get default database operate class.
        /// </summary>
        /// <returns></returns>
        public static IDbHelper CreateDbHelper()
        {
            return CreateDbHelper("Default");
        }

        /// <summary>
        /// 获取数据库操作类。<br/>
        /// Get database operate class.
        /// </summary>
        /// <param name="connectionName"></param>
        /// <returns></returns>
        public static IDbHelper CreateDbHelper(string connectionName)
        {
            string strProviderName = GetConnectionStringSettings(connectionName).ProviderName;
            switch (strProviderName)
            {
                case "System.Data.SqlClient":
                    return new SqlHelper(connectionName);
            }

            throw new NotFoundConnectionStringInConfigurationException("默认数据库连接配置的ProviderName无法映射到IDbHelper对象。");
        }
    }
}