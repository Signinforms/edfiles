// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 数据库对象的类型。<br/>
	/// Database object type.
	/// </summary>
	public enum DbObjectType
	{
		/// <summary>
		/// 数据表。<br/>Table
		/// </summary>
		Table = 1,
		/// <summary>
		/// 数据列。<br/>Column
		/// </summary>
		Column = 2,
		/// <summary>
		/// 数据视图。<br/>View
		/// </summary>
		View = 4,
		/// <summary>
		/// 存储过程。<br/>Store Procedure.
		/// </summary>
		StoreProcedure = 8,
		/// <summary>
		/// 存储过程参数。<br/>Store procedure parameter.
		/// </summary>
		SpParameter = 16
	}
}
