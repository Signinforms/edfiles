// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework.DqlLexicalAnalysis
{
	/// <summary>
	/// 数据库条件的关系符。<br/>
	/// The connector of database condition.
	/// </summary>
	public enum DqlConnectorType
	{
		/// <summary>
		/// 无关系符号。<br/>
		/// None
		/// </summary>
		None = 0,
		/// <summary>
		/// 并。<br/>
		/// And
		/// </summary>
		And = 1, 
		/// <summary>
		/// 或。<br/>
		/// Or
		/// </summary>
		Or = 2
	}
}
