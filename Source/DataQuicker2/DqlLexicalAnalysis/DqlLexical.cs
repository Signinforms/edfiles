// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using DataQuicker2.Framework.Common;

namespace DataQuicker2.Framework.DqlLexicalAnalysis
{
	/// <summary>
	/// DQL词法解析器，将配置的.dql.xml文件解析为DataQuicker2框架所需要的中间XML语言。<br/>
	/// DQL lexical analyzer, it's used to parse .dql.xml file to intermedia xml language of DataQuicker2 framework.
	/// </summary>
	public class DqlLexical
	{
		private XmlDocument sqlDoc;
		private XmlNode rootNode;
		private string dql;
		private bool isInvalid;

		private IndexPosition select = new IndexPosition();
		private IndexPosition from = new IndexPosition();
		private IndexPosition where = new IndexPosition();
		private IndexPosition groupby = new IndexPosition();
		private IndexPosition orderby = new IndexPosition();

		/// <summary>
		/// 构造DQL词法分析器。<br/>
		/// Construct DQL lexical analyzer.
		/// </summary>
		/// <param name="dql"></param>
		public DqlLexical(string dql)
		{
			this.sqlDoc = new XmlDocument();
			this.rootNode = sqlDoc.CreateElement("select");
			this.dql = dql.Trim();
		}

		/// <summary>
		/// 取得当前DQL语句。<br/>
		/// Get current analyzed querying DQL.
		/// </summary>
		public string Value
		{
			get
			{
				if (!this.isInvalid)
					return this.rootNode.OuterXml;
				else
					return this.dql;
			}
		}

		/// <summary>
		/// 当返回true时说明当前DQL语句不能转换为SQL的查询语句。<br/>
		/// Get true when current DQL cannot parse to SELECT clause of SQL.
		/// </summary>
		public bool IsInvalid
		{
			get
			{
				return this.isInvalid;
			}
		}

		/// <summary>
		/// 分析DQL。<br/>
		/// Analyze DQL.
		/// </summary>
		public void Analyze()
		{
			try
			{
				this.AnalyzeIndexPosition();
			}
			catch (NotFoundSelectDqlException)
			{
				this.isInvalid = true;
				return;
			}

			try
			{
				this.GetFields();
				this.GetTables();
				this.GetFilters();
				this.GetGroup();
				this.GetOrder();
			}
			catch (Exception exp)
			{
				Log.Instance.Error("DQL的词法解析错误，请检查DQL语法。", exp);
				throw new WrongSyntaxDqlException("DQL的词法解析错误，请检查DQL语法。", exp);
			}
		}

		private void GetFields()
		{
			string strFields = this.dql.Substring(this.select.right + 1, this.from.left - this.select.right - 1);
			XmlElement fieldsNode = this.sqlDoc.CreateElement("fields");
			fieldsNode.InnerText = strFields.Trim();
			this.rootNode.AppendChild(fieldsNode);
		}

		private void GetTables()
		{
			XmlElement tablesNode = this.sqlDoc.CreateElement("tables");
			this.rootNode.AppendChild(tablesNode);

			bool quot = false;
			int startedIndex = this.from.right + 1;
			Stack<int> bracketStack = new Stack<int>();
			int maxIndex = this.dql.Length;

			if (this.where.exist)
				maxIndex = this.where.left;
			else
			{
				if (this.groupby.exist)
				{
					maxIndex = this.groupby.left;
				}
				else
				{
					if (this.orderby.exist)
						maxIndex = this.orderby.left;
				}
			}

			for (int i = this.from.right + 1; i < maxIndex; i++)
			{
				if (this.dql[i] == '\'') quot = !quot;
				if (quot) continue;

				if (this.dql[i] == '(') bracketStack.Push(i);

				if (this.dql[i] == ')')
				{
					int startedBracketIndex = bracketStack.Pop();
					if (bracketStack.Count == 0)
					{
						string innerBracketString = this.dql.Substring(startedBracketIndex + 1, i - startedBracketIndex -1);
						DqlLexical lexical = new DqlLexical(innerBracketString);
						lexical.Analyze();

						string strBeforeBracketString = this.dql.Substring(startedIndex, startedBracketIndex - startedIndex + 1);
						startedIndex = i;

						XmlText textNode = this.sqlDoc.CreateTextNode(strBeforeBracketString);
						tablesNode.AppendChild(textNode);

						if (!lexical.IsInvalid)
						{
							XmlElement innerBracketSelectNode = this.sqlDoc.CreateElement("select");
							innerBracketSelectNode.InnerXml = lexical.rootNode.InnerXml;
							tablesNode.AppendChild(innerBracketSelectNode);
						}
						else
						{
							textNode = this.sqlDoc.CreateTextNode(lexical.Value);
							tablesNode.AppendChild(textNode);
						}
					}
				}
			}

			string strFields = this.dql.Substring(startedIndex, maxIndex - startedIndex).Trim();
			XmlText text = this.sqlDoc.CreateTextNode(strFields);
			tablesNode.AppendChild(text);
			return;
		}

		private void GetFilters()
		{
			if (this.where.exist)
			{
				int maxIndex = this.dql.Length;
				if (this.groupby.exist)
					maxIndex = this.groupby.left - 1;
				else
				{
					if (this.orderby.exist)
						maxIndex = this.orderby.left - 1;
				}

				XmlElement conditionsNode = this.sqlDoc.CreateElement("filters");
				this.rootNode.AppendChild(conditionsNode);
				this.GetConditionItem(this.where.right + 1, maxIndex, conditionsNode);
			}
		}

		private void GetConditionItem(int startedIndex, int maxIndex, XmlNode parentNode)
		{
			bool quot = false;
			XmlNode conditionNode;

			bool preExpressionFixed = false;
			int lastOperatorLeft = startedIndex;
			int lastConnectorRight = startedIndex;
			DqlConnectorType connector = DqlConnectorType.None;
			Stack<int> bracketsStack = new Stack<int>();
			int variableIndexStart;
			for (int i = startedIndex; i < maxIndex; i++)
			{
				
				if (this.dql[i] == '\'') quot = !quot;

				if (quot) continue;

				if (this.dql[i] == '(') bracketsStack.Push(i);

				if (this.dql[i] == ')')
				{
					if (bracketsStack.Count == 0)
					{
						Log.Instance.Error("DQL中括号(和)不匹配。");
						throw new WrongSyntaxDqlException("DQL中括号(和)不匹配。");
					}

					int preBracketIndex = bracketsStack.Pop();
					if (bracketsStack.Count > 0) continue;

					//get the sentence between ( and ) to analyze
					string sentence = this.dql.Substring(preBracketIndex + 1, i - preBracketIndex - 1);
					DqlLexical lexical = new DqlLexical(sentence);
					lexical.Analyze();
					if (lexical.isInvalid)	//Check the sentence is SELECT clause or not
					{
						//If it's not expression, maybe it's a function likes Contains() or IN (...)
						if (!IsExpression(sentence)) 
						{
							int latterConnectorLeftIndex = this.GetLatterConnectorLeftIndex(i + 1, maxIndex);
							string conditionItem = this.dql.Substring(lastConnectorRight, latterConnectorLeftIndex - lastConnectorRight + 1).Trim();
							conditionNode = this.CreateConditionNode();
							parentNode.AppendChild(conditionNode);
							conditionNode.Attributes["type"].Value = "expression";
							conditionNode.Attributes["connector"].Value = ToString(connector);
							variableIndexStart = conditionItem.IndexOf("{%");
							if (variableIndexStart != -1)
							{
								int variableIndexEnd = conditionItem.IndexOf("%}", variableIndexStart + 2);
								string strVariableName = conditionItem.Substring(variableIndexStart + 2, variableIndexEnd - variableIndexStart - 2);
								conditionNode.Attributes["param"].Value = strVariableName.Trim();
								conditionItem = conditionItem.Substring(0, variableIndexStart) + "{0}" + conditionItem.Substring(variableIndexEnd + 2, conditionItem.Length - variableIndexEnd - 2);
							}
							conditionNode.Attributes["expression"].Value = conditionItem;
						}
						else //It's express, likes (A=a AND B!=b) and so on.
						{
							conditionNode = this.CreateConditionNode();
							parentNode.AppendChild(conditionNode);
							conditionNode.Attributes["type"].Value = "container";
							conditionNode.Attributes["connector"].Value = ToString(connector);
							this.GetConditionItem(preBracketIndex + 1, i - 1, conditionNode);
						}
					}
					else //It's a sub-query
					{
						string conditionItem = this.dql.Substring(lastConnectorRight, i - lastConnectorRight - 1).Trim();
						conditionItem = this.dql.Substring(lastConnectorRight, preBracketIndex - lastConnectorRight + 1) + "{0})";
						conditionNode = this.CreateConditionNode();
						conditionNode.Attributes["type"].Value = "expression";
						conditionNode.Attributes["expression"].Value = conditionItem;
						conditionNode.Attributes["connector"].Value = ToString(connector);
						conditionNode.InnerXml = lexical.Value;
						parentNode.AppendChild(conditionNode);
					}

					preExpressionFixed = true;
					continue;
				}

				if (bracketsStack.Count > 0) continue;

				int left = maxIndex - i;
				int length = 0;
				
				if (this.dql[i] == ' ')
				{
					DqlConnectorType tempConnectorType = OperatorCheck.GetConnector(dql.Substring(i, (left > 5) ? 5 : left), out length);
					if (tempConnectorType != DqlConnectorType.None)
					{
						if (!preExpressionFixed)
						{
							string expression = this.dql.Substring(lastConnectorRight, i - lastConnectorRight);
							conditionNode = this.CreateExpressionConditionNode(expression, connector);
							parentNode.AppendChild(conditionNode);
						}
						else
							preExpressionFixed = false;

						i += length;
						connector = tempConnectorType;
						lastConnectorRight = i;
						continue;
					}
				}

				length = OperatorCheck.GetOperaterLength(dql.Substring(i, (left > 11) ? 11 : left));
				if (length > 0)
				{
					lastOperatorLeft = i;
					i += length - 1;
					continue;
				}
			}

			if (!preExpressionFixed)
			{
				int maxLength = maxIndex + 1 > this.dql.Length ? maxIndex : maxIndex + 1;
				string lastExpression = this.dql.Substring(lastConnectorRight, maxLength - lastConnectorRight);
				conditionNode = this.CreateExpressionConditionNode(lastExpression, connector);
				parentNode.AppendChild(conditionNode);
			}
		}

		private int GetLatterConnectorLeftIndex(int startedIndex, int maxIndex)
		{
			bool quot = false;
			Stack<int> bracketsStack = new Stack<int>();
			int i = startedIndex;
			for (; i < maxIndex; i++)
			{
				if (this.dql[i] == '\'') quot = !quot;
				if (quot) continue;

				if (this.dql[i] == '(') bracketsStack.Push(i);

				if (this.dql[i] == ')')
				{
					bracketsStack.Pop();
					continue;
				}

				if (bracketsStack.Count > 0) continue;

				if (this.dql[i] == ' ')
				{
					int left = maxIndex - i;
					DqlConnectorType tempConnectorType = OperatorCheck.GetConnector(dql.Substring(i, (left > 5) ? 5 : left));
					if (tempConnectorType != DqlConnectorType.None)
						return i;
				}
			}
			return i;
		}

		private void GetGroup()
		{
			if (!this.groupby.exist) return;

			XmlNode groupbyNode = this.sqlDoc.CreateElement("groups");
			this.rootNode.AppendChild(groupbyNode);

			int startedIndex = this.groupby.right + 1;
			int maxIndex = this.dql.Length;
			if (this.orderby.exist)
				maxIndex = this.orderby.left;

			string strGroupBy = this.dql.Substring(startedIndex, maxIndex - startedIndex);

			string[] groupBys = strGroupBy.Split(',');
			foreach (string groupBy in groupBys)
			{
				XmlNode fieldNode = this.sqlDoc.CreateElement("group");
				fieldNode.InnerText = groupBy;
				groupbyNode.AppendChild(fieldNode);
			}
		}

		private void GetOrder()
		{
			if (!this.orderby.exist) return;

			XmlNode orderbyNodes = this.sqlDoc.CreateElement("orders");
			this.rootNode.AppendChild(orderbyNodes);

			int startedIndex = this.orderby.right + 1;
			int maxIndex = this.dql.Length;

			string strOrderBy = this.dql.Substring(startedIndex, maxIndex - startedIndex);

			string[] orderBys = strOrderBy.Split(',');
			foreach (string orderBy in orderBys)
			{
				XmlNode orderbyNode = this.sqlDoc.CreateElement("order");
				orderbyNode.InnerText = orderBy;
				orderbyNodes.AppendChild(orderbyNode);
			}
		}

		private XmlNode CreateConditionNode()
		{
			XmlNode conditionNode = this.sqlDoc.CreateElement("condition");
			XmlAttribute attribute = this.sqlDoc.CreateAttribute("type");
			conditionNode.Attributes.Append(attribute);

			attribute = this.sqlDoc.CreateAttribute("connector");
			conditionNode.Attributes.Append(attribute);

			attribute = this.sqlDoc.CreateAttribute("expression");
			conditionNode.Attributes.Append(attribute);

			attribute = this.sqlDoc.CreateAttribute("param");
			conditionNode.Attributes.Append(attribute);

			return conditionNode;
		}

		private XmlNode CreateExpressionConditionNode(string expression, DqlConnectorType connector)
		{
			XmlNode conditionNode = CreateConditionNode();
			conditionNode.Attributes["type"].Value = "expression";
			conditionNode.Attributes["connector"].Value = ToString(connector);
			int variableIndexStart = expression.IndexOf("{%");
			if (variableIndexStart != -1)
			{
				int variableIndexEnd = expression.IndexOf("%}", variableIndexStart + 2);
				string strVariableName = expression.Substring(variableIndexStart + 2, variableIndexEnd - variableIndexStart - 2);
				conditionNode.Attributes["param"].Value = strVariableName.Trim();
				expression = expression.Substring(0, variableIndexStart) + "{0}" + expression.Substring(variableIndexEnd + 2, expression.Length - variableIndexEnd - 2);
			}
			conditionNode.Attributes["expression"].Value = expression;
			return conditionNode;
		}

		/// <summary>
		/// 检查当前DQL语句是否是表达式。<br/>
		/// Check whether the current DQL is expression or not.
		/// </summary>
		/// <param name="dql"></param>
		/// <returns></returns>
		private bool IsExpression(string dql)
		{
			bool quot = false;
			Stack<int> bracketsStack = new Stack<int>();

			for (int i = 0; i < dql.Length; i++)
			{
				if (this.dql[i] == '\'') quot = !quot;

				//在单引号内的为字符串常量，不进行处理
				if (quot) continue;

				int left = dql.Length - i;
				if (OperatorCheck.GetConnector(dql.Substring(i, (left > 5) ? 5 : left)) != DqlConnectorType.None) return true;
				if (OperatorCheck.GetOperaterLength(dql.Substring(i, (left > 11) ? 11 : left)) > 0) return true;

				if (this.dql[i] == '(')
				{
					bracketsStack.Push(i);
				}

				if (this.dql[i] == ')')
				{
					if (bracketsStack.Count == 0)
					{
						Log.Instance.Error("DQL中括号(和)不匹配。");
						throw new WrongSyntaxDqlException("DQL中括号(和)不匹配。");
					}
					int preBracketIndex = bracketsStack.Pop();
					//get the sentence between ( and ) to analyze
					string sentence = this.dql.Substring(preBracketIndex + 1, i - preBracketIndex - 1);
					if (IsExpression(sentence)) return true;
				}
			}

			return false;
		}

		private static string ToString(DqlConnectorType connector)
		{
			if (connector == DqlConnectorType.And) return "and";
			else if (connector == DqlConnectorType.Or) return "or";
			return "";
		}

		/// <summary>
		/// 分析出当前DQL中的SELECT, FROM, WHERE和GROUP BY的定位。<br/>
		/// Analyze to get the position of SELECT, FROM, WHERE and GROUP BY.
		/// </summary>
		private void AnalyzeIndexPosition()
		{
			if (this.dql.IndexOf("select") != 0) throw new NotFoundSelectDqlException();

			this.select.left = 0;
			this.select.right = 5;
			this.select.exist = true;

			bool quot = false;
			Stack<int> bracketsStack = new Stack<int>();
			StringBuilder sbStack = new StringBuilder();

			for (int i = 0; i < this.dql.Length; i++)
			{
				if (this.dql[i] == '\'')
				{
					quot = !quot;
				}

				if (quot) continue;

				if (this.dql[i] == '(')
				{
					bracketsStack.Push(i);
				}

				if (this.dql[i] == ')')
				{
					if (bracketsStack.Count == 0)
					{
						Log.Instance.Error("DQL中括号(和)不匹配。");
						throw new WrongSyntaxDqlException("DQL中括号(和)不匹配。");
					}
					bracketsStack.Pop();
				}

				if (bracketsStack.Count > 0) continue;

				if (this.dql[i] == ' ')
				{
					int leftDqlLength = this.dql.Length - i - 1;
					if (leftDqlLength > 5)
					{
						if (string.Compare(dql.Substring(i + 1, 5), "from ", true) == 0)
						{
							if (this.from.exist)
							{
								Log.Instance.Error("DQL语法错误，出现了多次from关键字。");
								throw new WrongSyntaxDqlException("DQL语法错误，出现了多次from关键字。");
							}
							this.from.left = i + 1;
							this.from.right = i + 4;
							this.from.exist = true;
						}
					}

					if (leftDqlLength > 6)
					{
						if (string.Compare(dql.Substring(i + 1, 6), "where ", true) == 0)
						{
							if (this.where.exist)
							{
								Log.Instance.Error("DQL语法错误，出现了多次where关键字。");
								throw new WrongSyntaxDqlException("DQL语法错误，出现了多次where关键字。");
							}

							this.where.left = i + 1;
							this.where.right = i + 5;
							this.where.exist = true;
						}
					}

					if (leftDqlLength > 9)
					{
						if (string.Compare(dql.Substring(i + 1, 9), "group by ", true) == 0)
						{
							if (this.groupby.exist)
							{
								Log.Instance.Error("DQL语法错误，出现了多次group by关键字。");
								throw new WrongSyntaxDqlException("DQL语法错误，出现了多次group by关键字。");
							}

							this.groupby.left = i + 1;
							this.groupby.right = i + 8;
							this.groupby.exist = true;
						}
					}

					if (leftDqlLength > 9)
					{
						if (string.Compare(dql.Substring(i + 1, 9), "order by ", true) == 0)
						{
							if (this.orderby.exist)
							{
								Log.Instance.Error("DQL语法错误，出现了多次order by关键字。");
								throw new WrongSyntaxDqlException("DQL语法错误，出现了多次order by关键字。");
							}

							this.orderby.left = i + 1;
							this.orderby.right = i + 8;
							this.orderby.exist = true;
						}
					}
				}
			}
		}

		private struct IndexPosition
		{
			public int left;
			public int right;
			public bool exist;

			#if DEBUG
			/// <summary>
			/// 将未知信息打印在Console上。<br/>
			/// Print out the position information on console.
			/// </summary>
			public void Printf()
			{
				Console.WriteLine("Exist: {0}", this.exist);
				Console.WriteLine("Left: {0}", this.left);
				Console.WriteLine("Right: {0}", this.right);
			}
			#endif
		}
	}
}
