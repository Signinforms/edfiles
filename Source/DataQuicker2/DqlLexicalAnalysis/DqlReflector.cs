// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using DataQuicker2.Framework.Common;

namespace DataQuicker2.Framework.DqlLexicalAnalysis
{
	/// <summary>
	/// 将DqlQuery对象反转为SQL语句的类。<br/>
	/// The class resposible for reflecting DqlQuery object to SQL.
	/// </summary>
	public class DqlReflector
	{
		private DqlQuery query;
		private bool requireWhereClause;
		private Dictionary<string, string> parameters;
		private SqlQueryReflector reflector;

		/// <summary>
		/// 构造函数。<br/>
		/// Constructor.
		/// </summary>
		/// <param name="query"></param>
		public DqlReflector(DqlQuery query)
		{
			Kit.VerifyNotNull(query);
			this.query = query;
			this.parameters = new Dictionary<string, string>();
			this.InitParams();
			this.requireWhereClause = query.RequireWhereClause;
			this.Reflect();
		}

		/// <summary>
		/// 取得生成的SQL语句中的SELECT部分。<br/>
		/// Get the SELECT section of generated SQL.
		/// </summary>
		public string SelectSection
		{
			get
			{
				return this.reflector.SelectSection;
			}
		}

		/// <summary>
		/// 取得生成的SQL语句中的FROM部分。<br/>
		/// Get the FROM section of generated SQL.
		/// </summary>
		public string FromSection
		{
			get
			{
				return this.reflector.FromSection;
			}
		}

		/// <summary>
		/// 取得生成的SQL语句中的WHERE部分。<br/>
		/// Get the WHERE section of generated SQL.
		/// </summary>
		public string WhereSection
		{
			get
			{
				return this.reflector.WhereSection;
			}
		}

		/// <summary>
		/// 取得生成的SQL语句中的GROUP BY部分。<br/>
		/// Get the GROUP by section of generated SQL.
		/// </summary>
		public string GroupbySection
		{
			get
			{
				return this.reflector.GroupbySection;
			}
		}

		/// <summary>
		/// 取得生成的SQL语句中的ORDER BY部分。<br/>
		/// Get the order by section of generated SQL.
		/// </summary>
		public string OrderbySection
		{
			get
			{
				return this.reflector.OrderbySection;
			}
		}

		/// <summary>
		/// 取得生产的SQL语句。<br/>
		/// Get generated SQL.
		/// </summary>
		public string Sql
		{
			get
			{
				return this.reflector.Sql;
			}
		}

		/// <summary>
		/// 取得生产的查询记录数量的SQL语句。<br/>
		/// Get generated SQL for query records count.
		/// </summary>
		public string Sql4Count
		{
			get
			{
				return this.reflector.Sql4Count;
			}
		}

		/// <summary>
		/// 将基于DqlQuery对象的当前对象反转为SQL语句。<br/>
		/// Reflect current object based on DqlQuery instance to SQL.
		/// </summary>
		/// <returns></returns>
		private void Reflect()
		{
			XmlNode node = this.query.DqlDocument.SelectSingleNode("sql/select");
			this.reflector = new SqlQueryReflector(node, this.query.Guid, this.parameters, this.requireWhereClause);
			reflector.Analyze();
		}

		private XmlDocument DqlDoc
		{
			get
			{
				return this.query.DqlDocument;
			}
		}

		private void InitParams()
		{
			XmlNodeList paramNodes = this.query.DqlDocument.SelectNodes("sql/param");
			foreach (XmlNode paramNode in paramNodes)
			{
				string strParamName = paramNode.Attributes["name"].Value;
				string strParamValue;
				if (paramNode.InnerText == this.query.Guid)
					strParamValue = null;
				else
					strParamValue = paramNode.InnerText;

				this.parameters.Add(strParamName, strParamValue);
			}
		}
	}
}
