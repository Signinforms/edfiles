// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using DataQuicker2.Framework.Common;

namespace DataQuicker2.Framework.DqlLexicalAnalysis
{
	/// <summary>
	/// 对DQL进行词法分析，并产生DQL XML语句。<br/>
	/// Lexical analyzing on DQL, and generate DQL XML.
	/// </summary>
	public static class Lexical
	{
		/// <summary>
		/// 对DQL进行词法分析，并生成SQL XML语句。<br/>
		/// Lexical analyzing on DQL, and return SQL XML.
		/// </summary>
		/// <param name="dqlNode"></param>
		/// <returns></returns>
		public static string GenerateDqlXml(XmlNode dqlNode)
		{
			CheckParameterType(dqlNode);

			string strDql = dqlNode.SelectSingleNode("text").InnerText;
			strDql = strDql.Replace("\r\n", " ").Replace('\t', ' ');
			strDql = Kit.RemoveMultiChars(strDql, ' ').ToLower();
			DqlLexical lexical = new DqlLexical(strDql);
			lexical.Analyze();
			if (lexical.IsInvalid)
			{
				string message = "配置的DQL无法解析为DQL中间指令，错误的DQL语句为: \r\n" + strDql;
				Log.Instance.ErrorFormat(message);
				throw new WrongSyntaxDqlException(message);
			}

			List<string> paramsList = new List<string>();
			XmlDocument selectDoc = new XmlDocument();
			selectDoc.LoadXml(lexical.Value);
			XmlNodeList paramConditionNodes = selectDoc.SelectNodes("//condition");
			foreach (XmlNode paramConditionNode in paramConditionNodes)
			{
				if (paramConditionNode.Attributes["param"] != null && !Kit.IsEmpty(paramConditionNode.Attributes["param"].Value))
				{
					if (!paramsList.Contains(paramConditionNode.Attributes["param"].Value))
					{
						paramsList.Add(paramConditionNode.Attributes["param"].Value);
					}
				}
			}

			XmlDocument doc = new XmlDocument();
			XmlNode sqlNode = doc.CreateElement("sql");

			XmlNode requiredWhereClauseNode = dqlNode.SelectSingleNode("./@requiredWhereClause");
			string strRequiredWhereClause = requiredWhereClauseNode.Value.ToLower();
			if (string.Compare(strRequiredWhereClause, "true") != 0 && string.Compare(strRequiredWhereClause, "false") != 0)
			{
				string message = "配置的DQL节点的requiredWhereClause属性设置不正确，该属性为必选项，其值应为true或者false。";
				Log.Instance.ErrorFormat(message);
				throw new WrongSyntaxDqlException(message);
			}

			strRequiredWhereClause = "<requiredWhereClause>" + strRequiredWhereClause + "</requiredWhereClause>";

			StringBuilder sbParamsXML = new StringBuilder();
			XmlNodeList paramNodes = dqlNode.SelectNodes("param");

			if (paramsList.Count != paramNodes.Count)
			{
				string message = "DQL[{0}]中的参数定义列表中的参数个数与下文中Text中的DQL语句中出现的参数数量不一致。";
				message = string.Format(message, dqlNode.Attributes["name"].Value);
				Log.Instance.Error(message);
				throw new DqlParamsConfiguraitonException(message);
			}


			foreach (XmlNode paramNode in paramNodes)
			{
				string strName = paramNode.Attributes["name"].Value.ToLower().Trim();
				string strType = "";
				if (!paramsList.Contains(strName))
				{
					string message = "DQL[{0}]中的参数定义列表中的参数在下文的Text中未使用。";
					message = string.Format(message, dqlNode.Attributes["name"].Value);
					Log.Instance.Error(message);
					throw new DqlParamsConfiguraitonException(message);
				}
				else
					paramsList.Remove(strName);

				if (paramNode.Attributes["type"] != null)
					strType = paramNode.Attributes["type"].Value;

				string strValue = paramNode.InnerText.Trim();
				sbParamsXML.AppendFormat("<param name=\"{0}\" type=\"{1}\">{2}</param>", strName, strType, strValue);
			}

			if (paramsList.Count != 0)
			{
				StringBuilder sbIncorrectParams = new StringBuilder();
				foreach (string param in paramsList)
					sbIncorrectParams.AppendFormat("{0};", param);
				string message = "DQL[{0}]参数配置不正确，在DQL.Text中出现的参数{1}在参数定义列表中不存在。";
				message = string.Format(message, dqlNode.Attributes["name"].Value, sbIncorrectParams);
				Log.Instance.Error(message);
				throw new DqlParamsConfiguraitonException(message);
			}

			sqlNode.InnerXml = strRequiredWhereClause + sbParamsXML.ToString() + lexical.Value;

			return sqlNode.OuterXml;
		}

		private static void CheckParameterType(XmlNode dqlNode)
		{
			XmlNodeList paramNodes = dqlNode.SelectNodes("param");
			foreach (XmlNode paramNode in paramNodes)
			{
				if (paramNode.Attributes["type"] != null)
				{
					string strTypeName = paramNode.Attributes["type"].Value;
					if (Type.GetType(strTypeName, false) == null)
					{
						string message = "DQL语句{0}中的参数{1}类型{2}无法在当前引用域中找到。";
						message = string.Format(message, dqlNode.Attributes["name"].Value, paramNode.Attributes["name"].Value, strTypeName);
						Log.Instance.Error(message);
						throw new InvalidParameterTypeException(message);
					}
				}
			}
		}
	}
}
