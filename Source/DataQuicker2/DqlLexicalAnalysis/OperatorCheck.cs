// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Text;
using DataQuicker2.Framework.Common;

namespace DataQuicker2.Framework.DqlLexicalAnalysis
{
	/// <summary>
	/// 负责分析检查SQL的运算符和关系连接符。<br/>
	/// Be reposible for checking SQL operator and connector.
	/// </summary>
	public static class OperatorCheck
	{
		/// <summary>
		/// 检查字符是否时SQL的操作符。<br/>
		/// Get true when the char is MSSQL operator.
		/// </summary>
		/// <param name="c"></param>
		/// <returns></returns>
		public static bool IsOperator(char c)
		{
			if (c == '<' || c=='>' || c=='=')
				return true;
			return false;
		}

		private static readonly string[] Operators = { " not exist ", " not exist(", " not like ", " not like'", " not in ", " not in(",  " exist ", " exist(", " like ", " like'", " in ", " in(", "<=", ">=", "<>", "<", ">", "=" };
		/// <summary>
		/// 检查是否为比较符，并返回比较符的长度，如果不为操作符，返回0。<br/>
		/// Check whether it's comparable operator, and return the length of it. If not, return 0;
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		public static int GetOperaterLength(string s)
		{
			string[] ops = Operators;
			string str = Kit.RemoveMultiChars(s, ' ');
			foreach (string op in ops)
			{
				if (str.Length < op.Length) continue;
				if (string.Compare(op, str.Substring(0, op.Length), true) == 0)
					return op.Length;
			}
			return 0;
		}

		/// <summary>
		/// 检测字符串是否包含SQL关系运算符。<br/>
		/// Return SQL connector contained in the targeted string.
		/// </summary>
		/// <param name="s"></param>
		/// <returns></returns>
		public static DqlConnectorType GetConnector(string s)
		{
			int length;
			return GetConnector(s, out length);
		}

		/// <summary>
		/// 检测字符串是否包含SQL关系运算符。<br/>
		/// Return SQL connector contained in the targeted string.
		/// </summary>
		/// <param name="s"></param>
		/// <param name="length">
		/// 如果包括关系运算符，则返回运算符占的字符数。<br/>
		/// If contains SQL connector, output the length of it.
		/// </param>
		/// <returns></returns>
		public static DqlConnectorType GetConnector(string s, out int length)
		{
			StringBuilder sbConnector = new StringBuilder();
			length = 0;
			foreach (char c in s)
			{
				if (c == ' ' && sbConnector.Length > 0) break;
				sbConnector.Append(c);
				length++;
			}

			string strConnector = sbConnector.ToString().Trim();
			if (string.Compare(strConnector, "and", true) == 0)
				return DqlConnectorType.And;
			else if (string.Compare(strConnector, "or", true) == 0)
				return DqlConnectorType.Or;
			else
				return DqlConnectorType.None;
		}
	}
}
