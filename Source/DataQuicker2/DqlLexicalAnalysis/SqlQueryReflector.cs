// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using DataQuicker2.Framework.Common;

namespace DataQuicker2.Framework.DqlLexicalAnalysis
{
	internal class SqlQueryReflector
	{
		private XmlNode selectNode;
		private string guid;
		private Dictionary<string, string> parameters;
		private bool requireWhereClause;
		private string selectSection;
		private string fromSection;
		private string whereSection;
		private string groupbySection;
		private string orderbySection;

		public SqlQueryReflector(XmlNode selectNode, string guid, Dictionary<string, string> parameters, bool requireWhereClause)
		{
			this.selectNode = selectNode;
			this.guid = guid;
			this.parameters = parameters;
			this.requireWhereClause = requireWhereClause;
		}

		/// <summary>
		/// 取得解析到的SQL语句。<br/>
		/// Get analyzed SQL.
		/// </summary>
		public string Sql
		{
			get
			{
				if (Kit.IsEmpty(this.whereSection) && this.requireWhereClause) return "";

				StringBuilder sbSQL = new StringBuilder();
				sbSQL.Append("select ");
				sbSQL.Append(this.selectSection);

				sbSQL.Append(" from ");
				sbSQL.Append(this.fromSection);

				if (!Kit.IsEmpty(this.whereSection))
				{
					sbSQL.Append(" where ");
					sbSQL.Append(this.whereSection);
				}

				if (!Kit.IsEmpty(this.groupbySection))
				{
					sbSQL.Append(" group by ");
					sbSQL.Append(this.groupbySection);
				}

				if (!Kit.IsEmpty(this.orderbySection))
				{
					sbSQL.Append(" order by ");
					sbSQL.Append(this.orderbySection);
				}

				return sbSQL.ToString();
			}
		}

		public string Sql4Count
		{
			get
			{
				if (Kit.IsEmpty(this.whereSection) && this.requireWhereClause) return "";

				StringBuilder sbSQL = new StringBuilder();
				sbSQL.Append("select ");
				sbSQL.Append(this.selectSection);

				sbSQL.Append(" from ");
				sbSQL.Append(this.fromSection);

				if (!Kit.IsEmpty(this.whereSection))
				{
					sbSQL.Append(" where ");
					sbSQL.Append(this.whereSection);
				}

				if (!Kit.IsEmpty(this.groupbySection))
				{
					sbSQL.Append(" group by ");
					sbSQL.Append(this.groupbySection);
				}

				return string.Format("select count(*) from ({0}) as [dataquicker2_temporary_view]", sbSQL);
			}
		}

		/// <summary>
		/// 取得SELECT部分的SQL语句。<br/>
		/// Get the SELECT section of querying sql.
		/// </summary>
		public string SelectSection
		{
			get
			{
				return this.selectSection;
			}
		}

		/// <summary>
		/// 取得FROM部分的SQL语句。<br/>
		/// Get the FROM section of querying sql.
		/// </summary>
		public string FromSection
		{
			get
			{
				return this.fromSection;
			}
		}

		/// <summary>
		/// 取得WHERE部分的SQL语句。<br/>
		/// Get the WHERE section of querying sql.
		/// </summary>
		public string WhereSection
		{
			get
			{
				return this.whereSection;
			}
		}

		/// <summary>
		/// 取得GROUP BY部分的SQL语句。<br/>
		/// Get the GROUP BY section of querying sql.
		/// </summary>
		public string GroupbySection
		{
			get
			{
				return this.groupbySection;
			}
		}

		/// <summary>
		/// 取得ORDER BY部分的SQL语句。<br/>
		/// Get the ORDER BY section of querying sql.
		/// </summary>
		public string OrderbySection
		{
			get
			{
				return this.orderbySection;
			}
		}

		/// <summary>
		/// 开始解析SQL。<br/>
		/// Start to analyze.
		/// </summary>
		/// <returns></returns>
		public void Analyze()
		{			
			this.selectSection = this.GetSelectSection();
			this.fromSection = this.GetFromSection();
			this.whereSection = this.GetWhereSection();
			this.groupbySection = this.GetGroupSection();
			this.orderbySection = this.GetOrderSection();
		}

		private string GetSelectSection()
		{
			return this.selectNode.SelectSingleNode("fields").InnerText;
		}

		private string GetFromSection()
		{
			StringBuilder sbSQL = new StringBuilder();
			XmlNode tablesNode = this.selectNode.SelectSingleNode("tables");
			XmlNodeList childNodes = tablesNode.ChildNodes;
			foreach (XmlNode childNode in childNodes)
			{
				if (childNode.NodeType == XmlNodeType.Text)
					sbSQL.Append(childNode.InnerText);
				if (childNode.NodeType == XmlNodeType.Element)
				{
					if (string.Compare(childNode.Name, "select", true) == 0)
					{
						SqlQueryReflector subQuery = new SqlQueryReflector(childNode, this.guid, this.parameters, false);
						subQuery.Analyze();
						sbSQL.Append(subQuery.Sql);
					}
				}
			}

			return sbSQL.ToString();
		}

		private string GetWhereSection()
		{
			XmlNode filtersNode = this.selectNode.SelectSingleNode("filters");
			if (filtersNode == null) return "";

			StringBuilder sbSQL = new StringBuilder();
			foreach (XmlNode conditionNode in filtersNode)
			{
				string strConditionType = conditionNode.Attributes["type"].Value;
				string strConditionItem = "";
				if (string.Compare(strConditionType, "container", true) == 0)
					strConditionItem = this.GetConditionContainer(conditionNode);
				else if (string.Compare(strConditionType, "expression", true) == 0)
					strConditionItem = this.GetConditionExpression(conditionNode);

				if (Kit.IsEmpty(strConditionItem)) continue;

				if (sbSQL.Length > 0)
				{
					string strConnector = conditionNode.Attributes["connector"].Value;
					sbSQL.AppendFormat(" {0} ", strConnector);
				}
				sbSQL.Append(strConditionItem);
			}

			return sbSQL.ToString();
		}

		private string GetConditionContainer(XmlNode conditionContainerNode)
		{
			StringBuilder sbSQL = new StringBuilder();

			foreach (XmlNode conditionNode in conditionContainerNode.ChildNodes)
			{
				string strConditionType = conditionNode.Attributes["type"].Value;
				string strConditionItem = "";
				if (string.Compare(strConditionType, "container", true) == 0)
					strConditionItem = this.GetConditionContainer(conditionNode);
				else if (string.Compare(strConditionType, "expression", true) == 0)
					strConditionItem = this.GetConditionExpression(conditionNode);

				if (Kit.IsEmpty(strConditionItem)) continue;

				if (sbSQL.Length > 0)
				{
					string strConnector = conditionNode.Attributes["connector"].Value;
					sbSQL.AppendFormat(" {0} ", strConnector);
				}
				sbSQL.Append(strConditionItem);
			}

			if (sbSQL.Length > 0)
				return "(" + sbSQL.ToString() + ")";
			else
				return "";
		}

		private string GetConditionExpression(XmlNode conditionExpressionNode)
		{
			string strExpression = conditionExpressionNode.Attributes["expression"].Value;
			string strParamName = conditionExpressionNode.Attributes["param"].Value;

			if (!Kit.IsEmpty(strParamName))
			{
				if (!this.parameters.ContainsKey(strParamName))
				{
					string message = "DQL.Text中的参数{0}在DQL参数声明列表中未定义。";
					message = string.Format(message, strParamName);
					Log.Instance.Error(message);
					throw new NotDefineParameterException(message);
				}

				string strParameterValue = this.parameters[strParamName];
				if (strParameterValue == null) return "";

				strExpression = string.Format(strExpression, strParameterValue);
			}
			else if (conditionExpressionNode.ChildNodes.Count == 1)
			{
				if (string.Compare(conditionExpressionNode.ChildNodes[0].Name, "select", true) != 0)
				{
					string message = "条件语句中的子查询相关错误，在DQL的语法中，在WHERE部分中的子条件项中出现子查询的时，只能包括SELECT语句得到的视图";
					Log.Instance.Error(message);
					throw new WrongSyntaxDqlException(message);
				}

				SqlQueryReflector subQuery = new SqlQueryReflector(conditionExpressionNode.ChildNodes[0], this.guid, this.parameters, this.requireWhereClause);
				subQuery.Analyze();
				if (Kit.IsEmpty(subQuery.Sql)) return "";

				strExpression = string.Format(strExpression, subQuery.Sql);
			}

			return strExpression;
		}

		private string GetGroupSection()
		{
			XmlNode groupsNode = this.selectNode.SelectSingleNode("groups");
			if (groupsNode == null || groupsNode.ChildNodes == null || groupsNode.ChildNodes.Count == 0) return "";

			StringBuilder sbSQL = new StringBuilder();
			foreach (XmlNode groupNode in groupsNode.ChildNodes)
			{
				if (sbSQL.Length > 0) sbSQL.Append(", ");
				sbSQL.Append(groupNode.InnerText);
			}
			return sbSQL.ToString();
		}

		private string GetOrderSection()
		{
			XmlNode ordersNode = this.selectNode.SelectSingleNode("orders");
			if (ordersNode == null || ordersNode.ChildNodes == null || ordersNode.ChildNodes.Count == 0) return "";

			StringBuilder sbSQL = new StringBuilder();
			foreach (XmlNode orderNode in ordersNode.ChildNodes)
			{
				if (sbSQL.Length > 0) sbSQL.Append(", ");
				sbSQL.Append(orderNode.InnerText);
			}
			return sbSQL.ToString();
		}
	}
}