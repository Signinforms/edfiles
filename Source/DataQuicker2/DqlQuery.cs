// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Modified: Alex
// Email: AlexHe.cs@Gmail.com
// Date: 2006-04-23
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Xml;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using DataQuicker2.Framework.Common;
using DataQuicker2.Framework.DqlLexicalAnalysis;

namespace DataQuicker2.Framework
{
    /// <summary>
    /// 数据查询类。<br/>Query database class.
    /// </summary>
    public class DqlQuery
    {
        private int pageIndex;
        private int pageSize = 10;
        private PagingType pagingType = PagingType.None;
        private string dqlName;
        private string guid;
        private string lastExecutedSql;
        private IEntity parentEntity;
        private bool requireWhereClause;
        private XmlDocument dqlXmlDocument;

        /// <summary>
        /// 构造一个SQL数据库查询对象。<br/>
        /// Construct a SQL database query instance.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="dqlName">
        /// 在查询DQL配置文件中的名称。<br/>
        /// The unique sql name in DQL configuration files.
        /// </param>
        /// <param name="dqlAnalyzedXml">
        /// 分析DQL后的中间SQL XML字符串。<br/>
        /// The intermedial SQL XML string after analyzed DQL.
        /// </param>
        public DqlQuery(IEntity entity, string dqlName, string dqlAnalyzedXml)
        {
            this.parentEntity = entity;
            this.dqlName = dqlName;
            this.guid = System.Guid.NewGuid().ToString("N");
            this.dqlXmlDocument = new XmlDocument();
            this.dqlXmlDocument.LoadXml(dqlAnalyzedXml);
            this.InitParams();
            this.requireWhereClause = Kit.ToBool(this.dqlXmlDocument.SelectSingleNode("sql/requiredWhereClause").InnerText);
        }

        /// <summary>
        /// 取得当前DQL映射的XML Document对象。<br/>
        /// Get the xml document which is the current DQL mapping to.
        /// </summary>
        protected internal XmlDocument DqlDocument
        {
            get
            {
                return this.dqlXmlDocument;
            }
        }

        /// <summary>
        /// 取得当前查询对象的生产实体。<br/>
        /// Get the producer of current query instance.
        /// </summary>
        public IEntity ParentEntity
        {
            get
            {
                return this.parentEntity;
            }
        }

        /// <summary>
        /// 是否当前DQL必须包含Where条件语句，如果该值为true，在解析SQL时，凡是没有Where条件的查询与子查询都会被忽略，但对于在FROM部分中的视图查询的SELECT语句无效。<br/>
        /// Get true when require where condition clause. If the value is true, the analyzed query and sub-query will be ignored. But no useful to the SELECT clause as view in FROM section.
        /// </summary>
        public bool RequireWhereClause
        {
            get
            {
                return this.requireWhereClause;
            }
        }

        private void InitParams()
        {
            string xpath = "sql/param";
            XmlNodeList paramNodes = this.dqlXmlDocument.SelectNodes(xpath);
            if (paramNodes == null) return;
            foreach (XmlNode paramNode in paramNodes)
            {
                if (Kit.IsEmpty(paramNode.InnerText))
                    paramNode.InnerText = this.guid;
            }
        }

        /// <summary>
        /// 重置所有的排序字段，无论这些排序字段是在DQL配置文件中还是在此前手动调用AppendOrderByColumn()添加的。<br/>
        /// Reset all order by columns, however they are in DQL configuration or append manually before.
        /// </summary>
        public void ResetOrderByColumns()
        {
            XmlNode ordersNode = this.dqlXmlDocument.SelectSingleNode("sql/select/orders");
            ordersNode.ParentNode.RemoveChild(ordersNode);
        }

        /// <summary>
        /// 增加Order By排序字段，默认排序方式为ASC升序。<br/>
        /// Add Order By sort column, the default sort way is ASC.
        /// </summary>
        /// <param name="column"></param>
        public void AppendOrderByColumn(IColumn column)
        {
            this.AppendOrderByColumn(column, true);
        }

        /// <summary>
        /// 增加Order By排序字段。<br/>
        /// Add Order By sort column.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="ascending"></param>
        public void AppendOrderByColumn(IColumn column, bool ascending)
        {
            XmlNode ordersNode = this.dqlXmlDocument.SelectSingleNode("sql/select/orders");
            if (ordersNode == null)
            {
                ordersNode = this.dqlXmlDocument.CreateElement("orders");
                this.dqlXmlDocument.SelectSingleNode("sql/select").AppendChild(ordersNode);
            }

            string strColumnName = ReservedWords.Protect(column.ColumnName);
            XmlNode orderNode = this.dqlXmlDocument.CreateElement("order");
            orderNode.InnerXml = strColumnName + (ascending ? " asc" : " desc");
            ordersNode.AppendChild(orderNode);
        }

        /// <summary>
        /// 设置/取得参数的值，在设定Parameter值时，值的类型必须与在DQL参数配置中的类型相同。<br/>
        /// Set/Get parameter value. When set parameter value, the type of value has to equal to configured parameter type of DQL parameter.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public object this[string name]
        {
            get
            {
                XmlNode paramNode = this.GetParamNode(name);
                if (paramNode == null)
                    throw new IndexOutOfRangeException("指定的参数名" + name + "在DQL参数声明中不存在。");
                return paramNode.InnerText == this.guid ? null : paramNode.InnerText;
            }
            set
            {
                Kit.VerifyNotNull(name);
                if (Kit.IsEmpty(value)) return;

                XmlNode paramNode = this.GetParamNode(name);
                if (paramNode == null)
                {
                    string message = "索引名称name在DQL声明中不存在。";
                    Log.Instance.Warn(message);
                    throw new InvalidOperationException(message);
                }

                Type valueType = value.GetType();
                if (paramNode.Attributes["type"] != null && !Kit.IsEmpty(paramNode.Attributes["type"].Value))
                {
                    string strType = paramNode.Attributes["type"].Value;
                    Type configType = Type.GetType(strType);

                    if (configType != valueType && valueType.GetInterface(configType.FullName) == null)
                    {
                        string message = "在DQL[{0}]中指定的参数{1}的类型{2}与设定值的类型{3}不匹配。";
                        message = string.Format(message, this.dqlName, name, strType, value.GetType().FullName);
                        Log.Instance.Warn(message);
                        throw new InvalidOperationException(message);
                    }

                    if (valueType.GetInterface("System.Collections.IList") != null)
                    {
                        IList list = value as IList;
                        if (list.Count == 0) return;

                        StringBuilder sbItemList = new StringBuilder();
                        IEnumerator iterator = list.GetEnumerator();
                        while (iterator.MoveNext())
                            sbItemList.AppendFormat("'{0}', ", Kit.ConvertSqlValue(iterator.Current));

                        sbItemList.Remove(sbItemList.Length - 2, 2);
                        paramNode.InnerText = sbItemList.ToString();
                        return;
                    }
                }

                if (valueType.IsSubclassOf(typeof(System.Enum)))
                    paramNode.InnerText = ((int)value).ToString();
                else
                    paramNode.InnerText = value.ToString();
            }
        }

        /// <summary>
        /// 设定查询参数的日期类型值，如果参数value不能转换成日期类型，则本次赋值无效。<br/>
        /// Set DateTime type value to targeted parameter. If argument value cannot convert into DateTime type, the set is non-useful.
        /// </summary>
        /// <param name="paramName"></param>
        /// <param name="valueString"></param>
        public void SetDateTime(string paramName, string valueString)
        {
            try
            {
                this[paramName] = DateTime.Parse(valueString);
            }
            catch { }
        }

        /// <summary>
        /// 设定查询参数的日期类型值，如果参数value不能转换成日期类型，则使用默认的时间参数。<br/>
        /// Set DateTime type value to targeted parameter. If argument value cannot convert into DateTime type, the method uses default argument.
        /// </summary>
        /// <param name="paramName"></param>
        /// <param name="valueString"></param>
        /// <param name="defaultValue"></param>
        public void SetDateTime(string paramName, string valueString, DateTime defaultValue)
        {
            try
            {
                this[paramName] = DateTime.Parse(valueString);
            }
            catch
            {
                this[paramName] = defaultValue.ToString();
            }
        }

        /// <summary>
        /// 设定查询参数的整数类型值，如果参数value不能转换成整数类型，则本次赋值无效。<br/>
        /// Set int type value to targeted parameter. If argument value cannot convert into int type, the set is non-useful.
        /// </summary>
        /// <param name="paramName"></param>
        /// <param name="valueString"></param>
        public void SetInt(string paramName, string valueString)
        {
            try
            {
                this[paramName] = int.Parse(valueString);
            }
            catch { }
        }

        /// <summary>
        /// 设定查询参数的整数类型值，如果参数value不能转换成整数类型，则使用默认的整型参数。<br/>
        /// Set int type value to targeted parameter. If argument value cannot convert into int type, the method uses default integer argument.
        /// </summary>
        /// <param name="paramName"></param>
        /// <param name="valueString"></param>
        /// <param name="defaultValue"></param>
        public void SetInt(string paramName, string valueString, int defaultValue)
        {
            try
            {
                this[paramName] = int.Parse(valueString);
            }
            catch
            {
                this[paramName] = defaultValue.ToString();
            }
        }

        /// <summary>
        /// 设定查询参数的浮点类型值，如果参数value不能转换成浮点类型，则本次赋值无效。<br/>
        /// Set float type value to targeted parameter. If argument value cannot convert into float type, the set is non-useful.
        /// </summary>
        /// <param name="paramName"></param>
        /// <param name="valueString"></param>
        public void SetFloat(string paramName, string valueString)
        {
            try
            {
                this[paramName] = float.Parse(valueString);
            }
            catch { }
        }

        /// <summary>
        /// 设定查询参数的浮点类型值，如果参数value不能转换成浮点类型，则使用默认浮点参数。<br/>
        /// Set float type value to targeted parameter. If argument value cannot convert into float type, the method uses float argument.
        /// </summary>
        /// <param name="paramName"></param>
        /// <param name="valueString"></param>
        /// <param name="defaultValue"></param>
        public void SetFloat(string paramName, string valueString, float defaultValue)
        {
            try
            {
                this[paramName] = float.Parse(valueString);
            }
            catch
            {
                this[paramName] = defaultValue.ToString();
            }
        }

        /// <summary>
        /// 设定查询参数的双浮点类型值，如果参数value不能转换成双浮点类型，则本次赋值无效。<br/>
        /// Set double type value to targeted parameter. If argument value cannot convert into double type, the set is non-useful.
        /// </summary>
        /// <param name="paramName"></param>
        /// <param name="valueString"></param>
        public void SetDouble(string paramName, string valueString)
        {
            try
            {
                this[paramName] = double.Parse(valueString);
            }
            catch { }
        }

        /// <summary>
        /// 设定查询参数的浮点类型值，如果参数value不能转换成浮点类型，则使用默认双浮点参数。<br/>
        /// Set float type value to targeted parameter. If argument value cannot convert into float type, the method uses double argument.
        /// </summary>
        /// <param name="paramName"></param>
        /// <param name="valueString"></param>
        /// <param name="defaultValue"></param>
        public void SetDouble(string paramName, string valueString, double defaultValue)
        {
            try
            {
                this[paramName] = float.Parse(valueString);
            }
            catch
            {
                this[paramName] = defaultValue.ToString();
            }
        }

        private XmlNode GetParamNode(string name)
        {
            string strName = name.ToLower().Trim();
            string xpath = "sql/param[@name='{0}']";
            xpath = string.Format(xpath, strName);
            return this.dqlXmlDocument.SelectSingleNode(xpath);
        }

        /// <summary>
        /// 设置/取得需要的数据页码，DataQuicker2支持数据翻页。<br/>
        /// Set/Get the current page index. DataQuicker2 support automatical paging.
        /// </summary>
        public int PageIndex
        {
            get
            {
                return this.pageIndex;
            }
            set
            {
                this.pageIndex = value;
            }
        }

        /// <summary>
        /// 设置/取得每数据页的记录数，默认值为10。<br/>
        /// Set/Get the records count in one data page. The default value is 10.
        /// </summary>
        public int PageSize
        {
            get
            {
                return this.pageSize;
            }
            set
            {
                this.pageSize = value;
            }
        }

        /// <summary>
        /// 设置/取得数据分页的方式。<br/>
        /// Set/Get the records paging type.
        /// </summary>
        public PagingType PagingType
        {
            get
            {
                return this.pagingType;
            }
            set
            {
                this.pagingType = value;
            }
        }

        /// <summary>
        /// 取得查询DQL的名称。<br/>
        /// Get the name of querying dql. That's configured in DQL configuration files.
        /// </summary>
        public string DqlName
        {
            get
            {
                return this.dqlName;
            }
        }

        /// <summary>
        /// 取得当前查询的GUID。<br/>
        /// Get the guid No. of current query.
        /// </summary>
        public string Guid
        {
            get
            {
                return this.guid;
            }
        }

        /// <summary>
        /// 取得当前对象最后执行时的SQL语句。<br/>
        /// Get last executed SQL of current instance.
        /// </summary>
        public string LastExecutedSql
        {
            get
            {
                return this.lastExecutedSql;
            }
            protected internal set
            {
                this.lastExecutedSql = value;
            }
        }

        #region GetDataReader Functions

        /// <summary>
        /// 填充阅读器对象并返回。
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public IDataReader GetDataReader(IDbConnection connection)
        {
            Kit.VerifyNotNull(connection);
            IProvider provider = DbFactory.CreateProvider(connection);
            return provider.GetDataReader(this, connection);
        }


        /// <summary>
        /// 填充阅读器对象并返回。
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public IDataReader GetDataReader(IDbTransaction transaction)
        {
            Kit.VerifyNotNull(transaction);
            Kit.VerifyNotNull(transaction.Connection);
            IProvider provider = DbFactory.CreateProvider(transaction.Connection);
            return provider.GetDataReader(this, transaction);
        }

        #endregion

        #region GetRecordsCount
        /// <summary>
        /// 通过在当前对象列上的赋值获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        public int GetRecordsCount()
        {
            int count = 0;
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                count = this.GetRecordsCount(connection);
            }
            finally
            {
                connection.Close();
            }
            return count;
        }

        /// <summary>
        /// 通过在当前对象列上的赋值获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        public int GetRecordsCount(IDbConnection connection)
        {
            Kit.VerifyNotNull(connection);
            IProvider provider = DbFactory.CreateProvider(connection);
            return provider.GetRecordsCount(this, connection);
        }

        /// <summary>
        /// 通过在当前对象列上的赋值获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="trans">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        public int GetRecordsCount(IDbTransaction trans)
        {
            Kit.VerifyNotNull(trans);
            IProvider provider = DbFactory.CreateProvider(trans.Connection);
            return provider.GetRecordsCount(this, trans);
        }
        #endregion

        #region Fill Functions

        /// <summary>
		/// 填充数据集对象。<br/>
		/// Fill the data collection instance.
		/// </summary>
		/// <param name="dt"></param>
		public void Fill(DataTable dt)
        {
            Kit.VerifyNotNull(dt);
            IProvider provider = DbFactory.CreateProvider();
            provider.Fill(this, dt);
        }

        /// <summary>
        /// 填充数据集对象。<br/>
        /// Fill the data collection instance.
        /// </summary>
        /// <param name="dst"></param>
        public void Fill(DataSet dst)
        {
            Kit.VerifyNotNull(dst);
            IProvider provider = DbFactory.CreateProvider();
            provider.Fill(this, dst);
        }

        /// <summary>
        /// 填充数据集对象。<br/>
        /// Fill the data collection instance.
        /// </summary>
        /// <param name="dst"></param>
        /// <param name="tableName"></param>
        public void Fill(DataSet dst, string tableName)
        {
            Kit.VerifyNotNull(dst);
            Kit.VerifyNotNull(tableName);
            IProvider provider = DbFactory.CreateProvider();
            provider.Fill(this, dst, tableName);
        }

        /// <summary>
        /// 填充数据集对象。<br/>
        /// Fill the data collection instance.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="connection"></param>
        public void Fill(DataTable dt, IDbConnection connection)
        {
            Kit.VerifyNotNull(dt);
            Kit.VerifyNotNull(connection);
            IProvider provider = DbFactory.CreateProvider(connection);
            provider.Fill(this, dt, connection);
        }

        /// <summary>
        /// 填充数据集对象。<br/>
        /// Fill the data collection instance.
        /// </summary>
        /// <param name="dst"></param>
        /// <param name="connection"></param>
        public void Fill(DataSet dst, IDbConnection connection)
        {
            Kit.VerifyNotNull(dst);
            Kit.VerifyNotNull(connection);
            IProvider provider = DbFactory.CreateProvider(connection);
            provider.Fill(this, dst, connection);
        }

        /// <summary>
        /// 填充数据集对象。<br/>
        /// Fill the data collection instance.
        /// </summary>
        /// <param name="dst"></param>
        /// <param name="tableName"></param>
        /// <param name="connection"></param>
        public void Fill(DataSet dst, string tableName, IDbConnection connection)
        {
            Kit.VerifyNotNull(dst);
            Kit.VerifyNotNull(tableName);
            Kit.VerifyNotNull(connection);
            IProvider provider = DbFactory.CreateProvider(connection);
            provider.Fill(this, dst, tableName, connection);
        }

        /// <summary>
        /// 填充数据集对象。<br/>
        /// Fill the data collection instance.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="transaction"></param>
        public void Fill(DataTable dt, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(dt);
            Kit.VerifyNotNull(transaction);
            Kit.VerifyNotNull(transaction.Connection);
            IProvider provider = DbFactory.CreateProvider(transaction.Connection);
            provider.Fill(this, dt, transaction);
        }

        /// <summary>
        /// 填充数据集对象。<br/>
        /// Fill the data collection instance.
        /// </summary>
        /// <param name="dst"></param>
        /// <param name="transaction"></param>
        public void Fill(DataSet dst, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(dst);
            Kit.VerifyNotNull(transaction);
            Kit.VerifyNotNull(transaction.Connection);
            IProvider provider = DbFactory.CreateProvider(transaction.Connection);
            provider.Fill(this, dst, transaction);
        }

        /// <summary>
        /// 填充数据集对象。<br/>
        /// Fill the data collection instance.
        /// </summary>
        /// <param name="dst"></param>
        /// <param name="tableName"></param>
        /// <param name="transaction"></param>
        public void Fill(DataSet dst, string tableName, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(dst);
            Kit.VerifyNotNull(tableName);
            Kit.VerifyNotNull(transaction);
            Kit.VerifyNotNull(transaction.Connection);
            IProvider provider = DbFactory.CreateProvider(transaction.Connection);
            provider.Fill(this, dst, tableName, transaction);
        }
        #endregion
    }
}
