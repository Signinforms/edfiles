// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using DataQuicker2.Framework.Common;
using DataQuicker2.Framework.DqlLexicalAnalysis;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// SQL查询对象工厂，负责生产SQL查询对象。<br/>
	/// SQL query instance factory, be resposible for producing SQL Query instance.
	/// </summary>
	public static class DqlQueryFactory
	{
		private static object syncObject = new object();
		private static Hashtable htDqlFilesCache = new Hashtable();
		private static Hashtable htDqlNamesCache = new Hashtable();
		private static XmlSchema dqlSchema;

		private static XmlSchema DqlSchema
		{
			get
			{
				if (dqlSchema == null)
				{
					lock (syncObject)
					{
						if (dqlSchema == null)
						{
							Assembly assembly = Assembly.GetExecutingAssembly();
							Stream fsManifest = assembly.GetManifestResourceStream("DataQuicker2.Framework.DqlConfig.xsd");
							try
							{
								dqlSchema = XmlSchema.Read(fsManifest, null);
							}
							finally
							{
								fsManifest.Close();
								fsManifest.Dispose();
							}
						}
					}
				}
				return dqlSchema;
			}
		}

		/// <summary>
		/// 构造<see cref="DqlQuery"/>对象。<br/>
		/// Construct <see cref="DqlQuery"/> instance.
		/// </summary>
		/// <param name="entity"></param>
		/// <param name="dqlName"></param>
		/// <returns></returns>
		public static DqlQuery CreateQuery(IEntity entity, string dqlName)
		{
			Type entityType = entity.GetType();
			string strDqlName = entityType.FullName + "." + dqlName;
			if (!htDqlNamesCache.Contains(strDqlName))
			{
				lock (syncObject)
				{
					if (!htDqlNamesCache.Contains(strDqlName))
					{
						string strDqlText = GetDqlFileText(entityType);
						XmlDocument doc = new XmlDocument();
						doc.LoadXml(strDqlText);
						string strXpath = "dqls/dql[@name='{0}']";
						strXpath = string.Format(strXpath, dqlName.Replace("'", ""));
						XmlNode dqlNode = doc.SelectSingleNode(strXpath);
						if (dqlNode == null)
						{
							Type type = entityType.GetType();
							string strManifestFileName = type.FullName + ".dql.xml";
							string message = "在实体{0}配置文件{1}中未找到DQL语句对象{2}。";
							message = string.Format(message, entityType.Name, strManifestFileName, dqlName);
							throw new NotFoundDqlNameException(message);
						}
						htDqlNamesCache.Add(strDqlName, Lexical.GenerateDqlXml(dqlNode));
					}
				}
			}

			string strXML = htDqlNamesCache[strDqlName].ToString();
			return new DqlQuery(entity, dqlName, strXML);
		}

		/// <summary>
		/// 取得.dql.xml文件的文本内容。<br/>
		/// Get the text of entity.dql.xml.
		/// </summary>
		/// <param name="entityType"></param>
		/// <returns></returns>
		private static string GetDqlFileText(Type entityType)
		{
			if (!htDqlFilesCache.Contains(entityType))
			{
				lock (syncObject)
				{
					if (!htDqlFilesCache.Contains(entityType))
					{
						Type type = entityType;
						string strManifestFileName = type.FullName + ".dql.xml";
						Assembly assembly = type.Assembly;
						Stream fsManifest = assembly.GetManifestResourceStream(strManifestFileName);
						if (fsManifest == null)
							throw new InvalidProgramException("在实体DLL中未发现嵌入资源" + strManifestFileName + "文件。");

						StreamReader sr = new StreamReader(fsManifest);
						try
						{
							string strDql = sr.ReadToEnd();
							XmlDocument doc = new XmlDocument();
							doc.LoadXml(strDql);
							if (!Kit.ValidateXml(DqlSchema, doc))
								throw new DqlConfigSchemaException(strManifestFileName + " XML Schema验证错误。");

							htDqlFilesCache.Add(entityType, strDql);
						}
						finally
						{
							sr.Close();
							fsManifest.Close();
							fsManifest.Dispose();
						}
					}
				}
			}

			return htDqlFilesCache[entityType].ToString();
		}
	}
}
