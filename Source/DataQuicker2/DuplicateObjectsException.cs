// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 实例化得到的对象数量不唯一。<br/>
	/// The duplicate records when initializing object.
	/// </summary>
	public class DuplicateObjectsException : Exception
	{
		/// <summary>
		/// 构造函数<br/>Constructor
		/// </summary>
		public DuplicateObjectsException() : base() { }

		/// <summary>
		/// 
		/// 构造函数<br/>Constructor
		/// </summary>
		/// <param name="message"></param>
		public DuplicateObjectsException(string message) : base(message) { }

		/// <summary>
		/// 构造函数<br/>Constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public DuplicateObjectsException(string message, Exception innerException) : base(message, innerException) { }

		/// <summary>
		/// 构造函数<br/>Constructor
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected DuplicateObjectsException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}
