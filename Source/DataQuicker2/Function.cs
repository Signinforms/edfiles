// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
    /// <summary>
    /// �ۺϺ�����<Br/>
    /// Aggregate functions.
    /// </summary>
    public enum Function
    {
        /// <summary>
        /// No function
        /// </summary>
        None = 0,
        /// <summary>
        /// Count()
        /// </summary>
        Count = 1,
        /// <summary>
        /// Max()
        /// </summary>
        Max = 2,
        /// <summary>
        /// Min()
        /// </summary>
        Min = 4,
        /// <summary>
        /// Average()
        /// </summary>
        Average = 8,
        /// <summary>
        /// Sum()
        /// </summary>
        Sum = 16
    }
}
