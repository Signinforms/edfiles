// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 列对象接口。<br/>
	/// The column interface.
	/// </summary>
	public interface IColumn : IDbObject
	{
		/// <summary>
		/// 取得泛型的实现类型。<br/>
		/// Get the generic type of implemented column class.
		/// </summary>
		Type GenericType{get;}

		/// <summary>
		/// 设置/取得列值。<br/>
		/// Set/Get the column value.
		/// </summary>
		object Value{get;set;}

		/// <summary>
		/// 当列值为Null时，返回空字符串。<br/>
		/// Return empty string when column value is null.
		/// </summary>
		string Null2EmptyStringValue { get;}

		/// <summary>
		/// 返回转换为String类型的当前列值。<br/>
		/// Convert to string.
		/// </summary>
		/// <returns></returns>
		string ToString();

		/// <summary>
		/// 设置/取得当前列对象映射的列名。<br/>
		/// Set/Get the mapping data column name of current column object.
		/// </summary>
		string ColumnName { get;set;}

		/// <summary>
		/// 设置/取得当前对象的父表对象。<br/>
		/// Set/Get the parent Table instance of current instance.
		/// </summary>
		IEntity ParentEntity { get;set;}

		/// <summary>
		/// 设置/取得当前列值的类型。<br/>
		/// Set/Get column value type.
		/// </summary>
		ColumnValueType ColumnValueType { get;set;}

		/// <summary>
		/// 取得当前值是否为空。<br/>
		/// Get true when current column value is null.
		/// </summary>
		bool IsNull{get;}

		/// <summary>
		/// 设置/取得当前列值的长度。<br/>
		/// Set/Get the length of current column value.
		/// </summary>
		int MaxLength { get;set;}
	}
}
