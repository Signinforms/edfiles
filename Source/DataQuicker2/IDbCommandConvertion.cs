// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// IDbCommand对象转换类。<br/>
	/// IDbCommand instance conversation class.
	/// </summary>
	public static class IDbCommandConvertion
	{
		/// <summary>
		/// 将IDbCommand对象转换用string字符串以便于查看其内容。<br/>
		/// Convert IDbCommand instance to string for convenient viewing.
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		public static string ToString(IDbCommand command)
		{
			if (command == null) return "";
			StringBuilder sbText = new StringBuilder();
			sbText.AppendFormat("[{0}]\r\n", command.GetType().FullName);
			sbText.AppendFormat("CommandType={0} \r\n", command.CommandType);
			sbText.AppendFormat("CommandText={0} \r\n", command.CommandText);
			sbText.Append("Parameters: \r\n");
			foreach (IDbDataParameter param in command.Parameters)
				sbText.AppendFormat("Parameter: Name={0}; Value={1} \r\n", param.ParameterName, param.Value);

			return sbText.ToString();
		}
	}
}
