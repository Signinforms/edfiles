// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
//
// Modified: Alex
// Email: AlexHe.cs@Gmail.com
// Date: 2006-04-05
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 数据库SQL语句操作接口。<br/>
	/// The interface to operate SQL against database.
	/// </summary>
	public interface IDbHelper
	{
        /// <summary>
        /// 取得默认的数据库连接名称
        /// </summary>
        string DefaultConnectionName { get;}
		/// <summary>
		/// 执行SQL语句。<br/>Execute SQL.
		/// </summary>
		/// <param name="sql"></param>
		/// <returns></returns>
		int ExecuteNonQuery(string sql);
		/// <summary>
		/// 执行SQL语句。<br/>Execute SQL.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		int ExecuteNonQuery(string sql, IDataParameterCollection parameters);
		/// <summary>
		/// 执行SQL语句。<br/>Execute SQL.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="connection">
		/// 已打开的数据库连接对象。<br/>Opened database connection instance.
		/// </param>
		/// <returns></returns>
		int ExecuteNonQuery(string sql, IDbConnection connection);
		/// <summary>
		/// 执行SQL语句。<br/>Execute SQL.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <param name="connection">
		/// 已打开的数据库连接对象。<br/>Opened database connection instance.
		/// </param>
		/// <returns></returns>
		int ExecuteNonQuery(string sql, IDataParameterCollection parameters, IDbConnection connection);
		/// <summary>
		/// 执行SQL语句。<br/>Execute SQL.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="transaction">
		/// 已打开的数据库事务对象。<br/>Opened database transaction instance.
		/// </param>
		/// <returns></returns>
		int ExecuteNonQuery(string sql, IDbTransaction transaction);
		/// <summary>
		/// 执行SQL语句。<br/>Execute SQL.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <param name="transaction">
		/// 已打开的数据库事务对象。<br/>Opened database transaction instance.
		/// </param>
		/// <returns></returns>
		int ExecuteNonQuery(string sql, IDataParameterCollection parameters, IDbTransaction transaction);

		/// <summary>
		/// 执行SQL语句，并返回第一个单元格的值。<br/>Execute SQL, return first cell of result table.
		/// </summary>
		/// <param name="sql"></param>
		/// <returns></returns>
		object ExecuteScalar(string sql);
		/// <summary>
		/// 执行SQL语句，并返回第一个单元格的值。<br/>Execute SQL, return first cell of result table.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		object ExecuteScalar(string sql, IDataParameterCollection parameters);
		/// <summary>
		/// 执行SQL语句，并返回第一个单元格的值。<br/>Execute SQL, return first cell of result table.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="connection">
		/// 已打开的数据库连接对象。<br/>Opened database connection instance.
		/// </param>
		/// <returns></returns>
		object ExecuteScalar(string sql, IDbConnection connection);
		/// <summary>
		/// 执行SQL语句，并返回第一个单元格的值。<br/>Execute SQL, return first cell of result table.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <param name="connection">
		/// 已打开的数据库连接对象。<br/>Opened database connection instance.
		/// </param>
		/// <returns></returns>
		object ExecuteScalar(string sql, IDataParameterCollection parameters, IDbConnection connection);
		/// <summary>
		/// 执行SQL语句，并返回第一个单元格的值。<br/>Execute SQL, return first cell of result table.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="transaction">
		/// 已打开的数据库事务对象。<br/>Opened database transaction instance.
		/// </param>
		/// <returns></returns>
		object ExecuteScalar(string sql, IDbTransaction transaction);
		/// <summary>
		/// 执行SQL语句，并返回第一个单元格的值。<br/>Execute SQL, return first cell of result table.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <param name="transaction">
		/// 已打开的数据库事务对象。<br/>Opened database transaction instance.
		/// </param>
		/// <returns></returns>
		object ExecuteScalar(string sql, IDataParameterCollection parameters, IDbTransaction transaction);

        #region GetDataReader

        /// <summary>
        /// 填充阅读器对象并返回。<br/>Fill and return DateReader instance.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        IDataReader GetDataReader(string sql, IDbConnection connection);

        /// <summary>
        /// 填充阅读器对象并返回。<br/>Fill and return DateReader instance.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="connection">
        /// 已打开的数据库连接对象。<br/>Opened database connection instance.
        /// </param>
        /// <returns></returns>
        IDataReader GetDataReader(string sql, IDataParameterCollection parameters, IDbConnection connection);
        /// <summary>
        /// 填充阅读器对象并返回。<br/>Fill and return DateReader instance.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="transaction">
        /// 已打开的数据库事务对象。<br/>Opened database transaction instance.
        /// </param>
        /// <returns></returns>
        IDataReader GetDataReader(string sql, IDbTransaction transaction);
        /// <summary>
        /// 填充阅读器对象并返回。<br/>Fill and return DateReader instance.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="transaction">
        /// 已打开的数据库事务对象。<br/>Opened database transaction instance.
        /// </param>
        /// <returns></returns>
        IDataReader GetDataReader(string sql, IDataParameterCollection parameters, IDbTransaction transaction);

        #endregion

        #region GetDataTable
        /// <summary>
		/// 填充数据集对象并返回。<br/>Fill and return data table instance.
		/// </summary>
		/// <param name="sql"></param>
		/// <returns></returns>
		DataTable GetDataTable(string sql);
		/// <summary>
		/// 填充数据集对象并返回。<br/>Fill and return data table instance.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		DataTable GetDataTable(string sql, IDataParameterCollection parameters);
		/// <summary>
		/// 填充数据集对象并返回。<br/>Fill and return data table instance.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="connection">
		/// 已打开的数据库连接对象。<br/>Opened database connection instance.
		/// </param>
		/// <returns></returns>
		DataTable GetDataTable(string sql, IDbConnection connection);
		/// <summary>
		/// 填充数据集对象并返回。<br/>Fill and return data table instance.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <param name="connection">
		/// 已打开的数据库连接对象。<br/>Opened database connection instance.
		/// </param>
		/// <returns></returns>
		DataTable GetDataTable(string sql, IDataParameterCollection parameters, IDbConnection connection);
		/// <summary>
		/// 填充数据集对象并返回。<br/>Fill and return data table instance.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="transaction">
		/// 已打开的数据库事务对象。<br/>Opened database transaction instance.
		/// </param>
		/// <returns></returns>
		DataTable GetDataTable(string sql, IDbTransaction transaction);
		/// <summary>
		/// 填充数据集对象并返回。<br/>Fill and return data table instance.
		/// </summary>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <param name="transaction">
		/// 已打开的数据库事务对象。<br/>Opened database transaction instance.
		/// </param>
		/// <returns></returns>
		DataTable GetDataTable(string sql, IDataParameterCollection parameters, IDbTransaction transaction);

        #endregion

        /// <summary>
		/// 填充数据集对象并返回。<br/>Fill and return data table instance.
		/// </summary>
		/// <param name="dt"></param>
		/// <param name="sql"></param>
		/// <returns></returns>
		void Fill(DataTable dt, string sql);
		/// <summary>
		/// 填充数据集对象并返回。<br/>Fill and return data table instance.
		/// </summary>
		/// <param name="dt"></param>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		void Fill(DataTable dt, string sql, IDataParameterCollection parameters);
		/// <summary>
		/// 填充数据集对象并返回。<br/>Fill and return data table instance.
		/// </summary>
		/// <param name="dt"></param>
		/// <param name="sql"></param>
		/// <param name="connection">
		/// 已打开的数据库连接对象。<br/>Opened database connection instance.
		/// </param>
		/// <returns></returns>
		void Fill(DataTable dt, string sql, IDbConnection connection);
		/// <summary>
		/// 填充数据集对象并返回。<br/>Fill and return data table instance.
		/// </summary>
		/// <param name="dt"></param>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <param name="connection">
		/// 已打开的数据库连接对象。<br/>Opened database connection instance.
		/// </param>
		/// <returns></returns>
		void Fill(DataTable dt, string sql, IDataParameterCollection parameters, IDbConnection connection);
		/// <summary>
		/// 填充数据集对象并返回。<br/>Fill and return data table instance.
		/// </summary>
		/// <param name="dt"></param>
		/// <param name="sql"></param>
		/// <param name="transaction">
		/// 已打开的数据库事务对象。<br/>Opened database transaction instance.
		/// </param>
		/// <returns></returns>
		void Fill(DataTable dt, string sql, IDbTransaction transaction);
		/// <summary>
		/// 填充数据集对象并返回。<br/>Fill and return data table instance.
		/// </summary>
		/// <param name="dt"></param>
		/// <param name="sql"></param>
		/// <param name="parameters"></param>
		/// <param name="transaction">
		/// 已打开的数据库事务对象。<br/>Opened database transaction instance.
		/// </param>
		/// <returns></returns>
		void Fill(DataTable dt, string sql, IDataParameterCollection parameters, IDbTransaction transaction);
	}
}
