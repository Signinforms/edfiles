// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 数据库对象接口，所有的数据库对象均需要实现本接口。<br/>
	/// The database object interface. All database objects have to implement this interface.
	/// </summary>
	public interface IDbObject
	{
		/// <summary>
		/// 取得数据库对象的名称。<br/>
		/// Get the database object name.
		/// </summary>
		string DbObjectName
		{
			get;
		}

		/// <summary>
		/// 取得数据库对象的类型。<br/>
		/// Get the database object type.
		/// </summary>
		DbObjectType DbObjectType
		{
			get;
		}

		/// <summary>
		/// 设置/获取当前数据库对象的值是否已变更。<br/>
		/// Set/Get true when the value of current database object has been changed.
		/// </summary>
		bool IsChanged
		{
			get;
			set;
		}
	}
}
