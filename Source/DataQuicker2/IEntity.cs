// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 实体层接口。<br/>
	/// The entity interface.
	/// </summary>
	public interface IEntity : IDbObject
	{
		/// <summary>
		/// 取得当前实体包含的所有列。<br/>
		/// Get all columns contains in current entity.
		/// </summary>
		List<IColumn> Columns{get;}
		/// <summary>
		/// 设置/取得当前实体是否存在。<br/>
		/// Set/Get true when current instance exists.
		/// </summary>
		bool IsExist { get;set;}
		/// <summary>
		/// 通过在当前对象列上的赋值初始化当前对象。<br/>
		/// Init current instance by the set value on columns of current instance.
		/// </summary>
		void InitObject();
		/// <summary>
		/// 通过在当前对象列上的赋值初始化当前对象。<br/>
		/// Init current instance by the set value on columns of current instance.
		/// </summary>
		/// <param name="connection"></param>
		void InitObject(IDbConnection connection);
		/// <summary>
		/// 通过在当前对象列上的赋值初始化当前对象。<br/>
		/// Init current instance by the set value on columns of current instance.
		/// </summary>
		/// <param name="transaction"></param>
		void InitObject(IDbTransaction transaction);
		/// <summary>
		/// 在当前实体对象上创建DqlQuery对象，执行查询。<br/>
		/// Create DqlQuery instance on current entity, to execute querying.
		/// </summary>
		/// <param name="dqlName"></param>
		/// <returns></returns>
		DqlQuery CreateQuery(string dqlName);
		/// <summary>
		/// 重置当前实体。<br/>
		/// Reset current instance.
		/// </summary>
		void Reset();
		/// <summary>
		/// 在当前实体对象上创建ObjectQuery对象，执行查询。<br/>
		/// Create ObjectQuery instance on current entity, to execute querying.
		/// </summary>
		/// <returns></returns>
		ObjectQuery CreateQuery();
	}
}