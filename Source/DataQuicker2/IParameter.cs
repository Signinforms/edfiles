// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 参数接口。<br/>
	/// Parameter interface.
	/// </summary>
	public interface IParameter : IDbObject
	{
		/// <summary>
		/// 设置/取得当前参数对象的值。<br/>
		/// Set/Get value of current parameter instance.
		/// </summary>
		object Value { get;set;}

		/// <summary>
		/// 取得当前参数值是否为空。<br/>
		/// Get true when current parameter value is null.
		/// </summary>
		bool IsNull { get;}

		/// <summary>
		/// 设置/取得存储过程的参数名。<br/>
		/// Set/Get store parameter name.
		/// </summary>
		string ParameterName { get;set;}

		/// <summary>
		/// 设置/取得存储过程参数的字符串最大长度，对非字符类型参数无效。<br/>
		/// Set/Get max length of string-type store parameter.
		/// </summary>
		int MaxLength { get;set;}
	}
}
