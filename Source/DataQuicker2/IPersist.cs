// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 持久操作的接口。<br/>
	/// The persist interface.
	/// </summary>
	public interface IPersist
	{
		/// <summary>
		/// 将新增的对象持久到数据库中。<br/>
		/// Persist new instance to database.
		/// </summary>
		void Create();

		/// <summary>
		/// 将新增的对象持久到数据库中。<br/>
		/// Persist new instance to database.
		/// </summary>
		/// <param name="trans"></param>
		void Create(IDbTransaction trans);

		/// <summary>
		/// 将新增的对象持久到数据库中。<br/>
		/// Persist new instance to database.
		/// </summary>
		/// <param name="connection"></param>
		void Create(IDbConnection connection);

		/// <summary>
		/// 将更新的对象持久到数据库中。<br/>
		/// Persist modified instance to database.
		/// </summary>
		void Update();

		/// <summary>
		/// 将更新的对象持久到数据库中。<br/>
		/// Persist modified instance to database.
		/// </summary>
		/// <param name="trans"></param>
		void Update(IDbTransaction trans);

		/// <summary>
		/// 将更新的对象持久到数据库中。<br/>
		/// Persist modified instance to database.
		/// </summary>
		/// <param name="connection"></param>
		void Update(IDbConnection connection);

		/// <summary>
		/// 将删除的对象持久到数据库中。<br/>
		/// Persist deleted instance to database.
		/// </summary>
		void Delete();

		/// <summary>
		/// 将删除的对象持久到数据库中。<br/>
		/// Persist deleted instance to database.
		/// </summary>
		/// <param name="trans"></param>
		void Delete(IDbTransaction trans);

		/// <summary>
		/// 将删除的对象持久到数据库中。<br/>
		/// Persist deleted instance to database.
		/// </summary>
		/// <param name="connection"></param>
		void Delete(IDbConnection connection);
	}
}