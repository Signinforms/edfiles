// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
//
// Modified: Alex
// Email: AlexHe.cs@Gmail.com
// Date: 2006-04-23
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 持久层的数据操作分析接口。<br/>
	/// The analyzing interface of persist classes.
	/// </summary>
	public interface IProvider
	{
		/// <summary>
		/// 对实体对象进行更新。<br/>
		/// Update table instance.
		/// </summary>
		/// <param name="table"></param>
		void Update(Table table);

		/// <summary>
		/// 对实体对象进行更新。<br/>
		/// Update table instance.
		/// </summary>
		/// <param name="table"></param>
		/// <param name="connection">
		/// IDbConnection对象必须已打开。<br/>
		/// IDbConnection instance must be opened.
		/// </param>
		void Update(Table table, IDbConnection connection);

		/// <summary>
		/// 对实体对象进行更新。<br/>
		/// Update table instance.
		/// </summary>
		/// <param name="table"></param>
		/// <param name="transaction">
		/// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
		/// Transaction instance, it associated IDbConnection instance must be opened.
		/// </param>
		void Update(Table table, IDbTransaction transaction);

		/// <summary>
		/// 从数据库中删除实体对象。<br/>
		/// Delete current instance from database.
		/// </summary>
		/// <param name="table"></param>
		void Delete(Table table);

		/// <summary>
		/// 从数据库中删除实体对象。<br/>
		/// Delete current instance from database.
		/// </summary>
		/// <param name="table"></param>
		/// <param name="connection">
		/// IDbConnection对象必须已打开。<br/>
		/// IDbConnection instance must be opened.
		/// </param>
		void Delete(Table table, IDbConnection connection);

		/// <summary>
		/// 从数据库中删除实体对象。<br/>
		/// Delete current instance from database.
		/// </summary>
		/// <param name="table"></param>
		/// <param name="transaction">
		/// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
		/// Transaction instance, it associated IDbConnection instance must be opened.
		/// </param>
		void Delete(Table table, IDbTransaction transaction);

		/// <summary>
		/// 向数据库中添加实体对象。<br/>
		/// Create <see cref="Table"/> instance to database.
		/// </summary>
		/// <param name="table"></param>
		void Create(Table table);

		/// <summary>
		/// 向数据库中添加实体对象。<br/>
		/// Create <see cref="Table"/> instance to database.
		/// </summary>
		/// <param name="table"></param>
		/// <param name="connection">
		/// IDbConnection对象必须已打开。<br/>
		/// IDbConnection instance must be opened.
		/// </param>
		void Create(Table table, IDbConnection connection);

		/// <summary>
		/// 向数据库中添加实体对象。<br/>
		/// Create <see cref="Table"/> instance to database.
		/// </summary>
		/// <param name="table"></param>
		/// <param name="transaction">
		/// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
		/// Transaction instance, it associated IDbConnection instance must be opened.
		/// </param>
		void Create(Table table, IDbTransaction transaction);

		/// <summary>
		/// 通过实体对象的主键值来实例化当前对象。<br/>
		/// Initialize current instance by primary key of table instance.
		/// </summary>
		/// <param name="table"></param>
		void Load(Table table);

		/// <summary>
		/// 通过实体对象的主键值来实例化当前对象。<br/>
		/// Initialize current instance by primary key of table instance.
		/// </summary>
		/// <param name="table"></param>
		/// <param name="connection">
		/// IDbConnection对象必须已打开。<br/>
		/// IDbConnection instance must be opened.
		/// </param>
		void Load(Table table, IDbConnection connection);

		/// <summary>
		/// 通过实体对象的主键值来实例化当前对象。<br/>
		/// Initialize current instance by primary key of table instance.
		/// </summary>
		/// <param name="table"></param>
		/// <param name="transaction">
		/// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
		/// Transaction instance, it associated IDbConnection instance must be opened.
		/// </param>
		void Load(Table table, IDbTransaction transaction);

        #region InitObject
        /// <summary>
		/// 通过实体对象所有列属性上设定的值来实例化当前对象。<br/>
		/// Initialize current instance by all columns value on current entity.
		/// </summary>
		/// <param name="entity"></param>
		void InitObject(IEntity entity);

		/// <summary>
		/// 通过实体对象所有列属性上设定的值来实例化当前对象。<br/>
		/// Initialize current instance by all columns value on current entity.
		/// </summary>
		/// <param name="entity"></param>
		/// <param name="connection">
		/// IDbConnection对象必须已打开。<br/>
		/// IDbConnection instance must be opened.
		/// </param>
		void InitObject(IEntity entity, IDbConnection connection);

		/// <summary>
		/// 通过实体对象所有列属性上设定的值来实例化当前对象。<br/>
		/// Initialize current instance by all columns value on current entity.
		/// </summary>
		/// <param name="entity"></param>
		/// <param name="transaction">
		/// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
		/// Transaction instance, it associated IDbConnection instance must be opened.
		/// </param>
		void InitObject(IEntity entity, IDbTransaction transaction);
        #endregion

        #region GetRecordsCount
        /// <summary>
        /// 通过实体对象所有列属性上设定的值来获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns ></returns>
        int GetRecordsCount(IEntity entity);

        /// <summary>
        /// 通过实体对象所有列属性上设定的值来实例化当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        int GetRecordsCount(IEntity entity, IDbConnection connection);

        /// <summary>
        /// 通过实体对象所有列属性上设定的值来实例化当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="transaction">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        int GetRecordsCount(IEntity entity, IDbTransaction transaction);

        /// <summary>
        /// 通过DqlQuery来获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="query"></param>
        /// <returns ></returns>
        int GetRecordsCount(DqlQuery query);

        /// <summary>
        /// 通过DqlQuery来获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        int GetRecordsCount(DqlQuery query, IDbConnection connection);

        /// <summary>
        /// 通过DqlQuery来获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="transaction">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        int GetRecordsCount(DqlQuery query, IDbTransaction transaction);

        /// <summary>
        /// 通过ObjectQuery来获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="query"></param>
        /// <returns ></returns>
        int GetRecordsCount(ObjectQuery query);

        /// <summary>
        /// 通过ObjectQuery来获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        int GetRecordsCount(ObjectQuery query, IDbConnection connection);

        /// <summary>
        /// 通过ObjectQuery来获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="transaction">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        int GetRecordsCount(ObjectQuery query, IDbTransaction transaction);
        #endregion

        /// <summary>
		/// 使用DqlQuery来填充DataTable对象。<br/>
		/// Fill the DataTable instance from DqlQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dt"></param>
		/// <returns></returns>
		void Fill(DqlQuery query, DataTable dt);

		/// <summary>
		/// 使用DqlQuery来填充DataSet对象。<br/>
		/// Fill the DataSet instance from DqlQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dst"></param>
		/// <returns></returns>
		void Fill(DqlQuery query, DataSet dst);

		/// <summary>
		/// 使用DqlQuery来填充DataSet中的某个表对象。<br/>
		/// Fill one DataTable instance of DataSet instance from DqlQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dst"></param>
		/// <param name="tableName"></param>
		/// <returns></returns>
		void Fill(DqlQuery query, DataSet dst, string tableName);

		/// <summary>
		/// 使用DqlQuery来填充DataTable对象。<br/>
		/// Fill the DataTable instance from DqlQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dt"></param>
		/// <param name="connection"></param>
		/// <returns></returns>
		void Fill(DqlQuery query, DataTable dt, IDbConnection connection);

		/// <summary>
		/// 使用DqlQuery来填充DataSet对象。<br/>
		/// Fill the DataSet instance from DqlQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dst"></param>
		/// <param name="connection"></param>
		/// <returns></returns>
		void Fill(DqlQuery query, DataSet dst, IDbConnection connection);

		/// <summary>
		/// 使用DqlQuery来填充DataSet中的某个表对象。<br/>
		/// Fill one DataTable instance of DataSet instance from DqlQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dst"></param>
		/// <param name="tableName"></param>
		/// <param name="connection"></param>
		/// <returns></returns>
		void Fill(DqlQuery query, DataSet dst, string tableName, IDbConnection connection);

		/// <summary>
		/// 使用DqlQuery来填充DataTable对象。<br/>
		/// Fill the DataTable instance from DqlQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dt"></param>
		/// <param name="transaction"></param>
		/// <returns></returns>
		void Fill(DqlQuery query, DataTable dt, IDbTransaction transaction);

		/// <summary>
		/// 使用DqlQuery来填充DataSet对象。<br/>
		/// Fill the DataSet instance from DqlQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dst"></param>
		/// <param name="transaction"></param>
		/// <returns></returns>
		void Fill(DqlQuery query, DataSet dst, IDbTransaction transaction);

		/// <summary>
		/// 使用DqlQuery来填充DataSet中的某个表对象。<br/>
		/// Fill one DataTable instance of DataSet instance from DqlQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dst"></param>
		/// <param name="tableName"></param>
		/// <param name="transaction"></param>
		/// <returns></returns>
		void Fill(DqlQuery query, DataSet dst, string tableName, IDbTransaction transaction);

		/// <summary>
		/// 执行SQL语句。<br/>Execue SQL.
		/// </summary>
		/// <param name="storeProcedure"></param>
		/// <returns></returns>
		int ExecuteNonQuery(StoreProcedure storeProcedure);

		/// <summary>
		/// 执行SQL语句。<br/>Execue SQL.
		/// </summary>
		/// <param name="storeProcedure"></param>
		/// <param name="connection"></param>
		/// <returns></returns>
		int ExecuteNonQuery(StoreProcedure storeProcedure, IDbConnection connection);

		/// <summary>
		/// 执行SQL语句。<br/>Execue SQL.
		/// </summary>
		/// <param name="storeProcedure"></param>
		/// <param name="transaction"></param>
		/// <returns></returns>
		int ExecuteNonQuery(StoreProcedure storeProcedure, IDbTransaction transaction);

		/// <summary>
		/// 执行查询，返回结果的第一行第一列单元格的结果。<br/>
		/// Execute query, return the first cell in first row.
		/// </summary>
		/// <returns></returns>
		object ExecuteScalar(StoreProcedure storeProcedure);

		/// <summary>
		/// 执行查询，返回结果的第一行第一列单元格的结果。<br/>
		/// Execute query, return the first cell in first row.
		/// </summary>
		/// <param name="storeProcedure"></param>
		/// <param name="connection"></param>
		/// <returns></returns>
		object ExecuteScalar(StoreProcedure storeProcedure, IDbConnection connection);

		/// <summary>
		/// 执行查询，返回结果的第一行第一列单元格的结果。<br/>
		/// Execute query, return the first cell in first row.
		/// </summary>
		/// <param name="storeProcedure"></param>
		/// <param name="transaction"></param>
		/// <returns></returns>
		object ExecuteScalar(StoreProcedure storeProcedure, IDbTransaction transaction);

		/// <summary>
		/// 执行查询返回数据集。<br/>
		/// Execute querying and return data table.
		/// </summary>
		/// <param name="storeProcedure"></param>
		/// <param name="dt"></param>
		/// <returns></returns>
		void Fill(StoreProcedure storeProcedure, DataTable dt);

		/// <summary>
		/// 执行查询返回数据集。<br/>
		/// Execute querying and return data table.
		/// </summary>
		/// <param name="storeProcedure"></param>
		/// <param name="dt"></param>
		/// <param name="connection"></param>
		/// <returns></returns>
		void Fill(StoreProcedure storeProcedure, DataTable dt, IDbConnection connection);

		/// <summary>
		/// 执行查询返回数据集。<br/>
		/// Execute querying and return data table.
		/// </summary>
		/// <param name="storeProcedure"></param>
		/// <param name="dt"></param>
		/// <param name="transaction"></param>
		/// <returns></returns>
		void Fill(StoreProcedure storeProcedure, DataTable dt, IDbTransaction transaction);

        #region GetDataReader

        /// <summary>
        /// 获得并返回数据阅读器对象
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        IDataReader GetDataReader(ObjectQuery query, IDbConnection connection);

        /// <summary>
        /// 获得并返回数据阅读器对象
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        IDataReader GetDataReader(ObjectQuery query, IDbTransaction transaction);

        /// <summary>
        /// 获得并返回数据阅读器对象
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        IDataReader GetDataReader(DqlQuery query, IDbConnection connection);

        /// <summary>
        /// 获得并返回数据阅读器对象
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        IDataReader GetDataReader(DqlQuery query, IDbTransaction transaction);

        /// <summary>
        /// 获得并返回数据阅读器对象
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="storeProcedure"></param>
        /// <returns></returns>
        IDataReader GetDataReader(StoreProcedure storeProcedure, IDbConnection connection);

        /// <summary>
        /// 获得并返回数据阅读器对象
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="storeProcedure"></param>
        /// <returns></returns>
        IDataReader GetDataReader(StoreProcedure storeProcedure, IDbTransaction transaction);

        #endregion

        #region ObjectQuery Methods
        /// <summary>
		/// 使用ObjectQuery来填充DataTable对象。<br/>
		/// Fill the DataTable instance from ObjectQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dt"></param>
		/// <returns></returns>
		void Fill(ObjectQuery query, DataTable dt);

		/// <summary>
		/// 使用ObjectQuery来填充DataSet对象。<br/>
		/// Fill the DataSet instance from ObjectQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dst"></param>
		/// <returns></returns>
		void Fill(ObjectQuery query, DataSet dst);

		/// <summary>
		/// 使用ObjectQuery来填充DataSet中的某个表对象。<br/>
		/// Fill one DataTable instance of DataSet instance from ObjectQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dst"></param>
		/// <param name="tableName"></param>
		/// <returns></returns>
		void Fill(ObjectQuery query, DataSet dst, string tableName);

		/// <summary>
		/// 使用ObjectQuery来填充DataTable对象。<br/>
		/// Fill the DataTable instance from ObjectQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dt"></param>
		/// <param name="connection"></param>
		/// <returns></returns>
		void Fill(ObjectQuery query, DataTable dt, IDbConnection connection);

		/// <summary>
		/// 使用ObjectQuery来填充DataSet对象。<br/>
		/// Fill the DataSet instance from ObjectQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dst"></param>
		/// <param name="connection"></param>
		/// <returns></returns>
		void Fill(ObjectQuery query, DataSet dst, IDbConnection connection);

		/// <summary>
		/// 使用ObjectQuery来填充DataSet中的某个表对象。<br/>
		/// Fill one DataTable instance of DataSet instance from ObjectQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dst"></param>
		/// <param name="tableName"></param>
		/// <param name="connection"></param>
		/// <returns></returns>
		void Fill(ObjectQuery query, DataSet dst, string tableName, IDbConnection connection);

		/// <summary>
		/// 使用ObjectQuery来填充DataTable对象。<br/>
		/// Fill the DataTable instance from ObjectQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dt"></param>
		/// <param name="transaction"></param>
		/// <returns></returns>
		void Fill(ObjectQuery query, DataTable dt, IDbTransaction transaction);

		/// <summary>
		/// 使用ObjectQuery来填充DataSet对象。<br/>
		/// Fill the DataSet instance from ObjectQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dst"></param>
		/// <param name="transaction"></param>
		/// <returns></returns>
		void Fill(ObjectQuery query, DataSet dst, IDbTransaction transaction);

		/// <summary>
		/// 使用ObjectQuery来填充DataSet中的某个表对象。<br/>
		/// Fill one DataTable instance of DataSet instance from ObjectQuery instance.
		/// </summary>
		/// <param name="query"></param>
		/// <param name="dst"></param>
		/// <param name="tableName"></param>
		/// <param name="transaction"></param>
		/// <returns></returns>
		void Fill(ObjectQuery query, DataSet dst, string tableName, IDbTransaction transaction);
		#endregion
	}
}
