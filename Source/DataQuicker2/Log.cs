// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// DataQuicker2日志的封装类。<br/>
	/// The wrapper class of DataQuicker2.
	/// </summary>
	public static class Log
	{
		private static log4net.ILog instance = log4net.LogManager.GetLogger("DataQuicker2");

		/// <summary>
		/// 取得DataQuicker2的Log4Net日志接口。<br/>
		/// Get Log4Net instance of DataQuicker2.
		/// </summary>
		public static log4net.ILog Instance
		{
			get
			{
				return instance;
			}
		}
	}
}
