// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 一个表实体中包括多个主键错误。<br/>
	/// The exception of multi primary key in one table instance.
	/// </summary>
	public class MultiPrimaryKeyException:Exception
	{
		/// <summary>
		/// 构造函数<br/>Constructor
		/// </summary>
		public MultiPrimaryKeyException():base() { }

		/// <summary>
		/// 
		/// 构造函数<br/>Constructor
		/// </summary>
		/// <param name="message"></param>
		public MultiPrimaryKeyException(string message) : base(message) { }

		/// <summary>
		/// 构造函数<br/>Constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public MultiPrimaryKeyException(string message, Exception innerException) : base(message, innerException) { }

		/// <summary>
		/// 构造函数<br/>Constructor
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected MultiPrimaryKeyException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}
