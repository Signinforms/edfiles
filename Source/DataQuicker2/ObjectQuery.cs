// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Modified: Alex
// Email: AlexHe.cs@Gmail.com
// Date: 2006-04-23
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using DataQuicker2.Framework.Common;

namespace DataQuicker2.Framework
{
    /// <summary>
    /// 由实体对象来构造查询，所有设定的查询条件之间的关系都是AND关系，不支持OR。<br/>
    /// Construct querying based on entities. The relationship of all condition items is AND, it don't support OR.
    /// </summary>
    public class ObjectQuery
    {
        private Dictionary<IColumn, bool> orderByFields;
        private List<SelectColumn> selectFields;
        private List<IColumn> groupByFields;
        private List<ColumnsAssociation> associations;
        private List<CriteriaItem> criteriaItems;
        private IEntity mainEntity;
        private Dictionary<IEntity, string> entities = new Dictionary<IEntity, string>();
        private bool selectDistinct;
        private int topN;
        private string lastExecutedSql;
        private int pageIndex;
        private int pageSize = 10;
        private PagingType pagingType = PagingType.None;
        private static log4net.ILog logger = log4net.LogManager.GetLogger("DateQuicker2");

        #region Internal Properties
        /// <summary>
        /// 取得构造当前查询的实体对象。<br/>
        /// Get entity instance which construct current query object.
        /// </summary>
        public IEntity MainEntity
        {
            get
            {
                return this.mainEntity;
            }
        }

        internal Dictionary<IColumn, bool> OrderByFields
        {
            get
            {
                return this.orderByFields;
            }
        }

        internal List<SelectColumn> SelectFields
        {
            get
            {
                return this.selectFields;
            }
        }

        internal List<IColumn> GroupByFields
        {
            get
            {
                return this.groupByFields;
            }
        }

        internal List<ColumnsAssociation> Associations
        {
            get
            {
                return this.associations;
            }
        }

        internal List<CriteriaItem> CriteriaItems
        {
            get
            {
                return this.criteriaItems;
            }
        }

        internal Dictionary<IEntity, string> Entities
        {
            get
            {
                return this.entities;
            }
        }
        #endregion

        /// <summary>
        /// 构造对象查询。<br/>
        /// Construct object query instance.
        /// </summary>
        /// <param name="entity"></param>
        public ObjectQuery(IEntity entity)
        {
            Kit.VerifyNotNull(entity);
            this.mainEntity = entity;
            this.entities.Add(entity, entity.DbObjectName);
        }

        /// <summary>
        /// 设定实体间的关联，参数currentColumns和targetedColumns关联列的顺序必须一致，默认的表关联是INNER JOIN。<br/>
        /// Create association between entities.The order of two arguments should be the same. The default association between entities is INNER JOIN.
        /// </summary>
        /// <param name="currentColumns"></param>
        /// <param name="targetedColumns"></param>
        public void SetAssociation(IColumn[] currentColumns, IColumn[] targetedColumns)
        {
            this.SetAssociation(currentColumns, targetedColumns, AssociationType.InnerJoin);
        }

        /// <summary>
        /// 设定实体间的关联，参数currentColumns和targetedColumns关联列的顺序必须一致。<br/>
        /// Create association between entities.The order of two arguments should be the same. 
        /// </summary>
        /// <param name="currentColumns"></param>
        /// <param name="targetedColumns"></param>
        /// <param name="associationType"></param>
        public void SetAssociation(IColumn[] currentColumns, IColumn[] targetedColumns, AssociationType associationType)
        {
            this.SetAssociation(currentColumns, targetedColumns, null, associationType);
        }

        /// <summary>
        /// 设定实体间的关联，参数currentColumns和targetedColumns关联列的顺序必须一致。<br/>
        /// Create association between entities.The order of two arguments should be the same. 
        /// </summary>
        /// <param name="currentColumns"></param>
        /// <param name="targetedColumns"></param>
        /// <param name="targetedEntityAliasName"></param>
        /// <param name="associationType"></param>
        public void SetAssociation(IColumn[] currentColumns, IColumn[] targetedColumns, string targetedEntityAliasName, AssociationType associationType)
        {
            Kit.VerifyNotNull(currentColumns);
            Kit.VerifyNotNull(targetedColumns);

            if (currentColumns.Length != targetedColumns.Length || currentColumns.Length == 0)
                throw new InvalidOperationException("目标实体的关联字段与本查询对象中的关联字段数量不一致，或无关联字段存在。");

            IEntity entity = currentColumns[0].ParentEntity;
            if (!this.entities.ContainsKey(entity))
                throw new InvalidOperationException("currentColumns所属的Entity对象在当前查询对象中不存在。");

            foreach (IColumn column in currentColumns)
                if (entity != column.ParentEntity)
                    throw new InvalidOperationException("参数currentColumns总的所有列应该属于同一个Entity对象。");

            entity = targetedColumns[0].ParentEntity;
            foreach (IColumn column in targetedColumns)
                if (entity != column.ParentEntity)
                    throw new InvalidOperationException("参数targetedColumns总的所有列应该属于同一个Entity对象。");

            string newEntityAliasName = Kit.IsEmpty(targetedEntityAliasName) ? entity.DbObjectName : targetedEntityAliasName;
            if (this.entities.ContainsValue(newEntityAliasName))
                throw new InvalidOperationException("新增加的关联实体的表名或指定的别名在当前查询集合中已存在。");

            if (this.associations == null)
                this.associations = new List<ColumnsAssociation>();

            this.entities.Add(entity, newEntityAliasName);
            this.associations.Add(new ColumnsAssociation(currentColumns, targetedColumns, associationType));
        }

        /// <summary>
        /// 添加查询的字段，如果不手动加入字段的话，默认为SELECT *。
        /// 如果在级联实体存在相同列名的话，SELECT *在查询中会报错。
        /// 如果字段不属于当前实体，则应在添加实体关联后再添加关联实体字段。<br/>
        /// Add select fields, if no manually adding, DataQuicker2 gets SELECT * as default querying. 
        /// If there are some column names in associated entities, it throws exception when querying by SELECT *.
        /// If the column doesn't belong to current entity, it should be added after creating association.
        /// </summary>
        /// <param name="columns"></param>
        public void SetSelectFields(params IColumn[] columns)
        {
            Kit.VerifyNotNull(columns);

            foreach (IColumn column in columns)
            {
                Kit.VerifyNotNull(column);
                this.SetSelectFields(column, column.DbObjectName);
            }
        }

        /// <summary>
        /// 添加查询的列，并为该列设定列名。如果不手动加入字段的话，默认为SELECT *。
        /// 当column参数为null时，默认为SELECT '' as AliasName。
        /// 如果在级联实体存在相同列名的话，SELECT *在查询中会报错。
        /// 如果在关联实体查询中，多个实体中存在重名列，并未通过本方法来设置别名时，DataQuicker2在查询中会报错。
        /// Add querying column, and set its alias name. 
        /// If there are repeated column names in associated entity collection and no manually set the alias name by this function, 
        /// it will throw exception when executing querying.
        /// If argument column is null, the detault case is SELECT '' AS AliasName
        /// </summary>
        /// <param name="column"></param>
        /// <param name="aliasName"></param>
        public void SetSelectFields(IColumn column, string aliasName)
        {
            this.SetSelectFields(column, Function.None, aliasName);
        }

        /// <summary>
        /// 添加查询的列，并为该列设定列名。如果在关联实体查询中，多个实体中存在重名列，并未通过本方法来设置别名时，DataQuicker2在查询中将会自动更改列名，可能造成潜在问题。参数column为空时，代表*。
        /// Add querying column, and set its alias name. If there are repeated column names in associated querying, and no manually setting alias name, there will be potential problems when querying because DataQuicker2 will set alias name automatically. The null argument column value stands for *.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="function"></param>
        /// <param name="aliasName"></param>
        public void SetSelectFields(IColumn column, Function function, string aliasName)
        {
            if (Kit.IsEmpty(aliasName) && function != Function.None)
                throw new InvalidOperationException("使用聚集函数时必须指定别名。");

            if (this.selectFields == null)
                this.selectFields = new List<SelectColumn>();

            this.selectFields.Add(new SelectColumn(column, function, aliasName));
        }

        /// <summary>
        /// 设定查询条件，默认运算符是等于（＝）。<br/>
        /// Set query condition, the default operator is Equal (=).
        /// </summary>
        /// <param name="column"></param>
        /// <param name="value"></param>
        public void SetCriteria(IColumn column, object value)
        {
            this.SetCriteria(column, Operator.Equal, new object[] { value });
        }

        /// <summary>
        /// 设定查询条件。<br/>
        /// Set query condition criteria.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="op"></param>
        /// <param name="values"></param>
        public void SetCriteria(IColumn column, Operator op, params object[] values)
        {
            Kit.VerifyNotNull(column);
            Kit.VerifyNotNull(values);
            if (values.Length == 0)
                throw new InvalidOperationException("查询条件项设定的值数量为0。");

            if (op != Operator.NotIn && op != Operator.In && values.Length > 1)
                throw new InvalidOperationException("在非IN和NOT IN运算操作时，设定值的数量应该唯一。");

            if (this.criteriaItems == null)
                this.criteriaItems = new List<CriteriaItem>();

            this.criteriaItems.Add(new CriteriaItem(column, op, values));
        }

        /// <summary>
        /// 设定Group By字段。<br/>
        /// Set Group By field.
        /// </summary>
        /// <param name="column"></param>
        public void SetGroupBy(IColumn column)
        {
            Kit.VerifyNotNull(column);
            if (this.groupByFields == null)
                this.groupByFields = new List<IColumn>();

            if (this.groupByFields.Contains(column))
                throw new InvalidOperationException("不能重复添加Group By字段。");

            this.groupByFields.Add(column);
        }

        /// <summary>
        /// 设定Order By字段，默认为升序排列。<Br/>
        /// Set order by fields, the default sort is ascending.
        /// </summary>
        /// <param name="column"></param>
        public void SetOrderBy(IColumn column)
        {
            this.SetOrderBy(column, true);
        }

        /// <summary>
        /// 设定Order By字段。<Br/>
        /// Set order by fields.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="isAscending"></param>
        public void SetOrderBy(IColumn column, bool isAscending)
        {
            Kit.VerifyNotNull(column);
            if (this.orderByFields == null)
                this.orderByFields = new Dictionary<IColumn, bool>();

            if (this.orderByFields.ContainsKey(column))
                throw new InvalidOperationException("不能重复添加Order By字段。");

            this.orderByFields.Add(column, isAscending);
        }

        /// <summary>
        /// 当前ObjectQuery最后执行时产生的SQL语句。<br/>
        /// The last executed SQL of current ObjectQuery instance.
        /// </summary>
        public string LastExecutedSql
        {
            get
            {
                return this.lastExecutedSql;
            }
            protected internal set
            {
                this.lastExecutedSql = value;
            }
        }

        /// <summary>
        /// 设置/取得需要的数据页码，DataQuicker2支持数据翻页。<br/>
        /// Set/Get the current page index. DataQuicker2 support automatical paging.
        /// </summary>
        public int PageIndex
        {
            get
            {
                return this.pageIndex;
            }
            set
            {
                if (value <= 0)
                    throw new OverflowException("分页的编码最小从1开始。");
                this.pageIndex = value;
            }
        }

        /// <summary>
        /// 设置/取得每数据页的记录数，默认值为10。<br/>
        /// Set/Get the records count in one data page. The default value is 10.
        /// </summary>
        public int PageSize
        {
            get
            {
                return this.pageSize;
            }
            set
            {
                if (value <= 0)
                    throw new OverflowException("每页记录数最小为1。");
                this.pageSize = value;
            }
        }

        /// <summary>
        /// 设置/取得查询是选用SELECT ALL还是SELECT DISTINCT
        /// </summary>
        public bool SelectDistinct
        {
            get
            {
                return this.selectDistinct;
            }
            set
            {
                this.selectDistinct = value;
            }
        }

        /// <summary>
        /// 设置/取得查询的TOP N数量
        /// </summary>
        public int TopN
        {
            get
            {
                return this.topN;
            }
            set
            {
                this.topN = value;
            }
        }

        /// <summary>
        /// 设置/取得数据分页的方式，默认不分页。<br/>
        /// Set/Get the records paging type, it's no paging as default.
        /// </summary>
        public PagingType PagingType
        {
            get
            {
                return this.pagingType;
            }
            set
            {
                this.pagingType = value;
            }
        }

        #region GetDataReader Functions

        /// <summary>
        /// 填充阅读器对象并返回。
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public IDataReader GetDataReader(IDbConnection connection)
        {
            Kit.VerifyNotNull(connection);
            IProvider provider = DbFactory.CreateProvider(connection);
            return provider.GetDataReader(this, connection);
        }


        /// <summary>
        /// 填充阅读器对象并返回。
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public IDataReader GetDataReader(IDbTransaction transaction)
        {
            Kit.VerifyNotNull(transaction);
            Kit.VerifyNotNull(transaction.Connection);
            IProvider provider = DbFactory.CreateProvider(transaction.Connection);
            return provider.GetDataReader(this, transaction);
        }

        #endregion
        #region GetRecordsCount
        /// <summary>
        /// 通过在当前对象列上的赋值获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        public int GetRecordsCount()
        {
            int count = 0;
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                count = this.GetRecordsCount(connection);
            }
            finally
            {
                connection.Close();
            }
            return count;
        }

        /// <summary>
        /// 通过在当前对象列上的赋值获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        public int GetRecordsCount(IDbConnection connection)
        {
            Kit.VerifyNotNull(connection);
            IProvider provider = DbFactory.CreateProvider(connection);
            return provider.GetRecordsCount(this, connection);
        }

        /// <summary>
        /// 通过在当前对象列上的赋值获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="trans">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        public int GetRecordsCount(IDbTransaction trans)
        {
            Kit.VerifyNotNull(trans);
            IProvider provider = DbFactory.CreateProvider(trans.Connection);
            return provider.GetRecordsCount(this, trans);
        }
        #endregion
        #region Fill Functions

        /// <summary>
        /// 填充数据集对象。<br/>
        /// Fill the data collection instance.
        /// </summary>
        /// <param name="dt"></param>
        public void Fill(DataTable dt)
        {
            Kit.VerifyNotNull(dt);
            IProvider provider = DbFactory.CreateProvider();
            provider.Fill(this, dt);
        }

        /// <summary>
        /// 填充数据集对象。<br/>
        /// Fill the data collection instance.
        /// </summary>
        /// <param name="dst"></param>
        public void Fill(DataSet dst)
        {
            Kit.VerifyNotNull(dst);
            IProvider provider = DbFactory.CreateProvider();
            provider.Fill(this, dst);
        }

        /// <summary>
        /// 填充数据集对象。<br/>
        /// Fill the data collection instance.
        /// </summary>
        /// <param name="dst"></param>
        /// <param name="tableName"></param>
        public void Fill(DataSet dst, string tableName)
        {
            Kit.VerifyNotNull(dst);
            Kit.VerifyNotNull(tableName);
            IProvider provider = DbFactory.CreateProvider();
            provider.Fill(this, dst, tableName);
        }

        /// <summary>
        /// 填充数据集对象。<br/>
        /// Fill the data collection instance.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="connection"></param>
        public void Fill(DataTable dt, IDbConnection connection)
        {
            Kit.VerifyNotNull(dt);
            Kit.VerifyNotNull(connection);
            try
            {
                IProvider provider = DbFactory.CreateProvider(connection);
                provider.Fill(this, dt, connection);
            }
            catch (Exception exp)
            {
                logger.Error("", exp);
            }
        }

        /// <summary>
        /// 填充数据集对象。<br/>
        /// Fill the data collection instance.
        /// </summary>
        /// <param name="dst"></param>
        /// <param name="connection"></param>
        public void Fill(DataSet dst, IDbConnection connection)
        {
            Kit.VerifyNotNull(dst);
            Kit.VerifyNotNull(connection);
            try
            {
                IProvider provider = DbFactory.CreateProvider(connection);
                provider.Fill(this, dst, connection);
            }
            catch (Exception exp)
            {
                logger.Error("", exp);
            }
        }

        /// <summary>
        /// 填充数据集对象。<br/>
        /// Fill the data collection instance.
        /// </summary>
        /// <param name="dst"></param>
        /// <param name="tableName"></param>
        /// <param name="connection"></param>
        public void Fill(DataSet dst, string tableName, IDbConnection connection)
        {
            Kit.VerifyNotNull(dst);
            Kit.VerifyNotNull(tableName);
            Kit.VerifyNotNull(connection);
            try
            {
                IProvider provider = DbFactory.CreateProvider(connection);
                provider.Fill(this, dst, tableName, connection);
            }
            catch (Exception exp)
            {
                logger.Error("", exp);
            }
        }

        /// <summary>
        /// 填充数据集对象。<br/>
        /// Fill the data collection instance.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="transaction"></param>
        public void Fill(DataTable dt, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(dt);
            Kit.VerifyNotNull(transaction);
            Kit.VerifyNotNull(transaction.Connection);
            try
            {
                IProvider provider = DbFactory.CreateProvider(transaction.Connection);
                provider.Fill(this, dt, transaction);
            }
            catch (Exception exp)
            {
                logger.Error("", exp);
            }
        }

        /// <summary>
        /// 填充数据集对象。<br/>
        /// Fill the data collection instance.
        /// </summary>
        /// <param name="dst"></param>
        /// <param name="transaction"></param>
        public void Fill(DataSet dst, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(dst);
            Kit.VerifyNotNull(transaction);
            Kit.VerifyNotNull(transaction.Connection);
            try
            {
                IProvider provider = DbFactory.CreateProvider(transaction.Connection);
                provider.Fill(this, dst, transaction);
            }
            catch (Exception exp)
            {
                logger.Error("", exp);
            }
        }

        /// <summary>
        /// 填充数据集对象。<br/>
        /// Fill the data collection instance.
        /// </summary>
        /// <param name="dst"></param>
        /// <param name="tableName"></param>
        /// <param name="transaction"></param>
        public void Fill(DataSet dst, string tableName, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(dst);
            Kit.VerifyNotNull(tableName);
            Kit.VerifyNotNull(transaction);
            Kit.VerifyNotNull(transaction.Connection);
            try
            {
                IProvider provider = DbFactory.CreateProvider(transaction.Connection);
                provider.Fill(this, dst, tableName, transaction);
            }
            catch (Exception exp)
            {
                logger.Error("", exp);
            }
        }
        #endregion
    }
}
