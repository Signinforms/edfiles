// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 操作运算符。<br/>
	/// Operator.
	/// </summary>
	public enum Operator
	{
		/// <summary>
		/// 小于&lt;
		/// </summary>
		LessThan,
		/// <summary>
		/// 大于&gt;
		/// </summary>
		MoreThan,
		/// <summary>
		/// 等于=
		/// </summary>
		Equal,
		/// <summary>
		/// 不等于!=
		/// </summary>
		NotEqual,
		/// <summary>
		/// 小于等于&lt;=
		/// </summary>
		LessEqualThan,
		/// <summary>
		/// 大于等于&gt;=
		/// </summary>
		MoreEqualThan,
		/// <summary>
		/// IN
		/// </summary>
		In,
		/// <summary>
		/// NOT IN
		/// </summary>
		NotIn,
		/// <summary>
		/// LIKE '[Value]'
		/// </summary>
		Like,
		/// <summary>
		/// LIKE '%[Value]'
		/// </summary>
		PrefixLike,
		/// <summary>
		/// LIKE '[Value]%'
		/// </summary>
		SuffixLike,
		/// <summary>
		/// LIKE '%[Value]%'
		/// </summary>
		BothSidesLike,
		/// <summary>
		/// NOT LIKE '[Value]'
		/// </summary>
		NotLike,
		/// <summary>
		/// NOT LIKE %'[Value]'
		/// </summary>
		PrefixNotLike,
		/// <summary>
		/// NOT LIKE '[Value]%'
		/// </summary>
		SuffixNotLike,
		/// <summary>
		/// NOT LIKE '%[Value]%'
		/// </summary>
		BothSidesNotLike
	}
}
