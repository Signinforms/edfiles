// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 数据查询的分页方式。<br/>
	/// The records paging type.
	/// </summary>
	public enum PagingType
	{
		/// <summary>
		/// 不分页。<br/>
		/// Non-paging.
		/// </summary>
		None = 0,
		/// <summary>
		/// 使用DataQuicker2框架管理数据分页。<br/>
		/// Use managed paging of DataQuicker2 framework.
		/// </summary>
		ManagedPaging = 1,
	}
}
