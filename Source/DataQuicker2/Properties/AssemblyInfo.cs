﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DataQuicker2.NET")]
[assembly: AssemblyDescription("DataQuicker2数据层框架由5维空间工作室开发和维护，实现了由数据库到系统逻辑的跳跃式开发，节省了开发人员在数据库操作上的编码和维护消耗。")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("http://dev.5vnet.com")]
[assembly: AssemblyProduct("DataQuicker2.NET")]
[assembly: AssemblyCopyright("Copyright © 5VNET.COM 2006")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ca36625a-5b36-4503-84ae-33d4a03fc960")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("0.9.9.0")]
[assembly: AssemblyFileVersion("0.9.9.0")]
