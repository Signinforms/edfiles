using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 在查询中存在相同的列名，且并未指定别名。<br/>
	/// The duplicate column names in querying, and no alias name specified.
	/// </summary>
	public class RepeatedColumnNamesException: Exception
	{
		/// <summary>
		/// 构造函数<br/>Constructor
		/// </summary>
		public RepeatedColumnNamesException() : base() { }

		/// <summary>
		/// 
		/// 构造函数<br/>Constructor
		/// </summary>
		/// <param name="message"></param>
		public RepeatedColumnNamesException(string message) : base(message) { }

		/// <summary>
		/// 构造函数<br/>Constructor
		/// </summary>
		/// <param name="message"></param>
		/// <param name="innerException"></param>
		public RepeatedColumnNamesException(string message, Exception innerException) : base(message, innerException) { }

		/// <summary>
		/// 构造函数<br/>Constructor
		/// </summary>
		/// <param name="info"></param>
		/// <param name="context"></param>
		protected RepeatedColumnNamesException(SerializationInfo info, StreamingContext context) : base(info, context) { }
	}
}
