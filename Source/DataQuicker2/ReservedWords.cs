// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 检查SQL中的保留关键字。<br/>
	/// Check the SQL reserved words.
	/// </summary>
	public class ReservedWords
	{
		/// <summary>
		/// 保护当前字符串不为SQL保留关键字。<br/>
		/// Protect current token cannot be the SQL reserved keyword.
		/// </summary>
		/// <param name="token"></param>
		/// <returns></returns>
		public static string Protect(string token)
		{
			if (token.IndexOf("[") == -1 || token.IndexOf("]") == -1)
				return "[" + token + "]";
			return token;
		}
	}
}
