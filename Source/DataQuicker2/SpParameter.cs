// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Collections.Generic;
using System.Text;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 存储过程参数类。<br/>Store parameter class.
	/// </summary>
	public class SpParameter<T> : IParameter
	{
		private string parameterName;
		private bool isChanged;
		private object value;
		private int maxLength = -1;

		#region IDbObject Members

		/// <summary>
		/// 取得参数的名称。<br/>
		/// Get Parameter Name.
		/// </summary>
		public string ParameterName
		{
			get
			{
				return this.parameterName;
			}
			protected internal set
			{
				this.parameterName = value;
			}
		}

		string IDbObject.DbObjectName
		{
			get
			{
				return this.ParameterName;
			}
		}

		DbObjectType IDbObject.DbObjectType
		{
			get
			{
				return DbObjectType.SpParameter;
			}
		}

		/// <summary>
		/// 取得当前参数值是否已改变。<br/>
		/// Get true when current parameter value has been changed.
		/// </summary>
		public bool IsChanged
		{
			get
			{
				return this.isChanged;
			}
			protected internal set
			{
				this.isChanged = value;
			}
		}

		bool IDbObject.IsChanged
		{
			get
			{
				return this.IsChanged;
			}
			set
			{
				this.IsChanged = value;
			}
		}

		#endregion

		#region IParameter Members

		/// <summary>
		/// 设置/取得当前对象的值。<br/>
		/// Set/Get value of current parameter instance.
		/// </summary>
		public T Value
		{
			get
			{
				if (this.value == null) return default(T);

				if (this.maxLength > 0 && this.value is string)
				{
					string strValue = this.value.ToString().Trim();
					object returnValue;
					if (this.maxLength < strValue.Length)
						returnValue = strValue.Substring(0, this.maxLength);
					else
						returnValue = strValue;
					return (T)returnValue;
				}

				return this.ToGenericValue();
			}
			set
			{
				if (value == null) this.value = null;

				string strValue = value.ToString().Trim();
				if (this.maxLength > 0 && strValue.Length > this.maxLength)
					this.value = strValue.Substring(0, this.maxLength);
				else
					this.value = value;

				this.isChanged = true;
			}
		}

		object IParameter.Value
		{
			set
			{
				if(value == null) this.value = null;
				this.value = value;
			}
			get
			{
				return this.value;
			}
		}

		/// <summary>
		/// 取得当前参数值是否为空。<br/>
		/// Get true when current parameter value is null.
		/// </summary>
		public bool IsNull
		{
			get
			{
				return this.value == null;
			}
		}

		string IParameter.ParameterName
		{
			get
			{
				return this.ParameterName;
			}
			set
			{
				this.ParameterName = value;
			}
		}

		int IParameter.MaxLength
		{
			get
			{
				return this.maxLength;
			}
			set
			{
				this.maxLength = value;
			}
		}

		/// <summary>
		/// 取得本参数的最大长度，如果为非字符类型的参数，则返回-1
		/// </summary>
		public int MaxLength
		{
			get
			{
				return this.maxLength;
			}
		}
		#endregion

		private T ToGenericValue()
		{
			if (this.value is uint
				|| this.value is short
				|| this.value is ushort
				|| this.value is long)
			{
				return (T)Convert.ChangeType(this.value, typeof(int));
			}

			Type valueType = this.value.GetType();
			Type genericType = typeof(T);
			if (genericType == valueType || (genericType.IsSubclassOf(typeof(Enum)) && valueType.IsValueType))
				return (T)this.value;
			else
				return (T)Convert.ChangeType(this.value, typeof(T));
		}
	}
}
