// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
//
// Modified: Alex
// Email: AlexHe.cs@Gmail.com
// Date: 2006-04-05
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using DataQuicker2.Framework.Common;

namespace DataQuicker2.Framework
{
    /// <summary>
    /// MSSQL数据库SQL语句操作对象。<br/>
    /// The class to operate MSSQL database by SQL.
    /// </summary>
    public class SqlHelper : IDbHelper
    {
        /// <summary>
        /// SqlHelper的构造函数
        /// </summary>
        /// <param name="defaultConnectionName">默认的数据库连接名称</param>
        public SqlHelper(string defaultConnectionName)
        {
            this.defaultConnectionName = defaultConnectionName;
        }

        private string defaultConnectionName;

        /// <summary>
        /// 取得默认的数据库连接名称
        /// </summary>
        public string DefaultConnectionName
        {
            get
            {
                return this.defaultConnectionName;
            }
        }

        #region IHelper Members

        /// <summary>
        /// 执行SQL语句。<br/>Execute SQL.
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string sql)
        {
            IDbConnection connection = DbFactory.CreateOpenConnection(this.defaultConnectionName);
            try
            {
                return ExecuteNonQuery(sql, null, connection, null);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 执行SQL语句。<br/>Execute SQL.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string sql, IDataParameterCollection parameters)
        {
            IDbConnection connection = DbFactory.CreateOpenConnection(this.defaultConnectionName);
            try
            {
                return ExecuteNonQuery(sql, parameters, connection, null);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 执行SQL语句。<br/>Execute SQL.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="connection">
        /// 已打开的数据库连接对象。<br/>Opened database connection instance.
        /// </param>
        /// <returns></returns>
        public int ExecuteNonQuery(string sql, IDbConnection connection)
        {
            return ExecuteNonQuery(sql, null, connection, null);
        }

        /// <summary>
        /// 执行SQL语句。<br/>Execute SQL.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="connection">
        /// 已打开的数据库连接对象。<br/>Opened database connection instance.
        /// </param>
        /// <returns></returns>
        public int ExecuteNonQuery(string sql, IDataParameterCollection parameters, IDbConnection connection)
        {
            return ExecuteNonQuery(sql, parameters, connection, null);
        }

        /// <summary>
        /// 执行SQL语句。<br/>Execute SQL.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="transaction">
        /// 已打开的数据库事务对象。<br/>Opened database transaction instance.
        /// </param>
        /// <returns></returns>
        public int ExecuteNonQuery(string sql, IDbTransaction transaction)
        {
            return ExecuteNonQuery(sql, null, transaction.Connection, transaction);
        }

        /// <summary>
        /// 执行SQL语句。<br/>Execute SQL.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="transaction">
        /// 已打开的数据库事务对象。<br/>Opened database transaction instance.
        /// </param>
        /// <returns></returns>
        public int ExecuteNonQuery(string sql, IDataParameterCollection parameters, IDbTransaction transaction)
        {
            return ExecuteNonQuery(sql, parameters, transaction.Connection, transaction);
        }

        /// <summary>
        /// 执行SQL语句，并返回第一个单元格的值。<br/>Execute SQL, return first cell of result table.
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public object ExecuteScalar(string sql)
        {
            IDbConnection connection = DbFactory.CreateOpenConnection(this.defaultConnectionName);
            try
            {
                return ExecuteScalar(sql, null, connection, null);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 执行SQL语句，并返回第一个单元格的值。<br/>Execute SQL, return first cell of result table.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public object ExecuteScalar(string sql, IDataParameterCollection parameters)
        {
            IDbConnection connection = DbFactory.CreateOpenConnection(this.defaultConnectionName);
            try
            {
                return ExecuteScalar(sql, parameters, connection, null);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 执行SQL语句，并返回第一个单元格的值。<br/>Execute SQL, return first cell of result table.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="connection">
        /// 已打开的数据库连接对象。<br/>Opened database connection instance.
        /// </param>
        /// <returns></returns>
        public object ExecuteScalar(string sql, IDbConnection connection)
        {
            return ExecuteScalar(sql, null, connection, null);
        }

        /// <summary>
        /// 执行SQL语句，并返回第一个单元格的值。<br/>Execute SQL, return first cell of result table.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="connection">
        /// 已打开的数据库连接对象。<br/>Opened database connection instance.
        /// </param>
        /// <returns></returns>
        public object ExecuteScalar(string sql, IDataParameterCollection parameters, IDbConnection connection)
        {
            return ExecuteScalar(sql, parameters, connection, null);
        }

        /// <summary>
        /// 执行SQL语句，并返回第一个单元格的值。<br/>Execute SQL, return first cell of result table.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="transaction">
        /// 已打开的数据库事务对象。<br/>Opened database transaction instance.
        /// </param>
        /// <returns></returns>
        public object ExecuteScalar(string sql, IDbTransaction transaction)
        {
            return ExecuteScalar(sql, null, transaction.Connection, transaction);
        }

        /// <summary>
        /// 执行SQL语句，并返回第一个单元格的值。<br/>Execute SQL, return first cell of result table.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="transaction">
        /// 已打开的数据库事务对象。<br/>Opened database transaction instance.
        /// </param>
        /// <returns></returns>
        public object ExecuteScalar(string sql, IDataParameterCollection parameters, IDbTransaction transaction)
        {
            return ExecuteScalar(sql, parameters, transaction.Connection, transaction);
        }

        #region GetDataReader

        /// <summary>
        /// 填充阅读器对象并返回。<br/>Fill and return DataReader instance.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public IDataReader GetDataReader(string sql, IDbConnection connection)
        {
            SqlCommand command = GetDbCommand(sql, null, connection, null) as SqlCommand;
            return command.ExecuteReader();
        }

        /// <summary>
        /// 填充阅读器对象并返回。<br/>Fill and return DataReader instance.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="connection">
        /// 已打开的数据库连接对象。<br/>Opened database connection instance.
        /// </param>
        /// <returns></returns>
        public IDataReader GetDataReader(string sql, IDataParameterCollection parameters, IDbConnection connection)
        {
            SqlCommand command = GetDbCommand(sql, parameters, connection, null) as SqlCommand;
            return command.ExecuteReader();
        }

        /// <summary>
        /// 填充阅读器对象并返回。<br/>Fill and return DataReader instance.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="transaction">
        /// 已打开的数据库事务对象。<br/>Opened database transaction instance.
        /// </param>
        /// <returns></returns>
        public IDataReader GetDataReader(string sql, IDbTransaction transaction)
        {
            SqlCommand command = GetDbCommand(sql, null, transaction.Connection, transaction) as SqlCommand;
            return command.ExecuteReader();
        }

        /// <summary>
        /// 填充阅读器对象并返回。<br/>Fill and return DataReader instance.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="transaction">
        /// 已打开的数据库事务对象。<br/>Opened database transaction instance.
        /// </param>
        /// <returns></returns>
        public IDataReader GetDataReader(string sql, IDataParameterCollection parameters, IDbTransaction transaction)
        {
            SqlCommand command = GetDbCommand(sql, parameters, transaction.Connection, transaction) as SqlCommand;
            return command.ExecuteReader();
        }

        #endregion


        #region GetDataTable

        /// <summary>
        /// 填充数据集对象并返回。<br/>Fill and return data table instance.
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public DataTable GetDataTable(string sql)
        {
            IDbConnection connection = DbFactory.CreateOpenConnection(this.defaultConnectionName);
            try
            {
                SqlCommand command = GetDbCommand(sql, null, connection, null) as SqlCommand;
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                return dt;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 填充数据集对象并返回。<br/>Fill and return data table instance.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable GetDataTable(string sql, IDataParameterCollection parameters)
        {
            IDbConnection connection = DbFactory.CreateOpenConnection(this.defaultConnectionName);
            try
            {
                SqlCommand command = GetDbCommand(sql, parameters, connection, null) as SqlCommand;
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dt = new DataTable();
                adapter.Fill(dt);
                return dt;
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 填充数据集对象并返回。<br/>Fill and return data table instance.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="connection">
        /// 已打开的数据库连接对象。<br/>Opened database connection instance.
        /// </param>
        /// <returns></returns>
        public DataTable GetDataTable(string sql, IDbConnection connection)
        {
            SqlCommand command = GetDbCommand(sql, null, connection, null) as SqlCommand;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            return dt;
        }

        /// <summary>
        /// 填充数据集对象并返回。<br/>Fill and return data table instance.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="connection">
        /// 已打开的数据库连接对象。<br/>Opened database connection instance.
        /// </param>
        /// <returns></returns>
        public DataTable GetDataTable(string sql, IDataParameterCollection parameters, IDbConnection connection)
        {
            SqlCommand command = GetDbCommand(sql, parameters, connection, null) as SqlCommand;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            return dt;
        }

        /// <summary>
        /// 填充数据集对象并返回。<br/>Fill and return data table instance.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="transaction">
        /// 已打开的数据库事务对象。<br/>Opened database transaction instance.
        /// </param>
        /// <returns></returns>
        public DataTable GetDataTable(string sql, IDbTransaction transaction)
        {
            SqlCommand command = GetDbCommand(sql, null, transaction.Connection, transaction) as SqlCommand;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            return dt;
        }

        /// <summary>
        /// 填充数据集对象并返回。<br/>Fill and return data table instance.
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="transaction">
        /// 已打开的数据库事务对象。<br/>Opened database transaction instance.
        /// </param>
        /// <returns></returns>
        public DataTable GetDataTable(string sql, IDataParameterCollection parameters, IDbTransaction transaction)
        {
            SqlCommand command = GetDbCommand(sql, parameters, transaction.Connection, transaction) as SqlCommand;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable dt = new DataTable();
            adapter.Fill(dt);
            return dt;
        }

        #endregion

        /// <summary>
        /// 填充数据集对象并返回。<br/>Fill and return data table instance.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <returns></returns>
        public void Fill(DataTable dt, string sql)
        {
            Kit.VerifyNotNull(dt);
            Kit.VerifyNotNull(sql);
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                SqlCommand command = GetDbCommand(sql, null, connection, null) as SqlCommand;
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
            }
            finally
            {
                connection.Close();
            }
        }
        /// <summary>
        /// 填充数据集对象并返回。<br/>Fill and return data table instance.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public void Fill(DataTable dt, string sql, IDataParameterCollection parameters)
        {
            Kit.VerifyNotNull(dt);
            Kit.VerifyNotNull(sql);
            IDbConnection connection = DbFactory.CreateOpenConnection(this.defaultConnectionName);
            try
            {
                SqlCommand command = GetDbCommand(sql, parameters, connection, null) as SqlCommand;
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(dt);
            }
            finally
            {
                connection.Close();
            }
        }
        /// <summary>
        /// 填充数据集对象并返回。<br/>Fill and return data table instance.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="connection">
        /// 已打开的数据库连接对象。<br/>Opened database connection instance.
        /// </param>
        /// <returns></returns>
        public void Fill(DataTable dt, string sql, IDbConnection connection)
        {
            Kit.VerifyNotNull(dt);
            Kit.VerifyNotNull(sql);
            Kit.VerifyNotNull(connection);
            SqlCommand command = GetDbCommand(sql, null, connection, null) as SqlCommand;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(dt);
        }
        /// <summary>
        /// 填充数据集对象并返回。<br/>Fill and return data table instance.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="connection">
        /// 已打开的数据库连接对象。<br/>Opened database connection instance.
        /// </param>
        /// <returns></returns>
        public void Fill(DataTable dt, string sql, IDataParameterCollection parameters, IDbConnection connection)
        {
            Kit.VerifyNotNull(dt);
            Kit.VerifyNotNull(sql);
            Kit.VerifyNotNull(connection);
            SqlCommand command = GetDbCommand(sql, parameters, connection, null) as SqlCommand;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(dt);
        }
        /// <summary>
        /// 填充数据集对象并返回。<br/>Fill and return data table instance.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="transaction">
        /// 已打开的数据库事务对象。<br/>Opened database transaction instance.
        /// </param>
        /// <returns></returns>
        public void Fill(DataTable dt, string sql, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(dt);
            Kit.VerifyNotNull(sql);
            Kit.VerifyNotNull(transaction);
            SqlCommand command = GetDbCommand(sql, null, transaction.Connection, transaction) as SqlCommand;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(dt);
        }
        /// <summary>
        /// 填充数据集对象并返回。<br/>Fill and return data table instance.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="sql"></param>
        /// <param name="parameters"></param>
        /// <param name="transaction">
        /// 已打开的数据库事务对象。<br/>Opened database transaction instance.
        /// </param>
        /// <returns></returns>
        public void Fill(DataTable dt, string sql, IDataParameterCollection parameters, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(dt);
            Kit.VerifyNotNull(sql);
            Kit.VerifyNotNull(transaction);
            SqlCommand command = GetDbCommand(sql, parameters, transaction.Connection, transaction) as SqlCommand;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            adapter.Fill(dt);
        }

        #endregion

        private static int ExecuteNonQuery(string sql, IDataParameterCollection parameters, IDbConnection connection, IDbTransaction transaction)
        {
            IDbCommand command = GetDbCommand(sql, parameters, connection, transaction);
            return command.ExecuteNonQuery();
        }

        private static object ExecuteScalar(string sql, IDataParameterCollection parameters, IDbConnection connection, IDbTransaction transaction)
        {
            IDbCommand command = GetDbCommand(sql, parameters, connection, transaction);
            return command.ExecuteScalar();
        }

        private static IDbCommand GetDbCommand(string sql, IDataParameterCollection parameters, IDbConnection connection, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(sql);
            Kit.VerifyNotNull(connection);
            if (connection.State != ConnectionState.Open)
                throw new InvalidOperationException("数据库对象未打开。");

            IDbCommand command = connection.CreateCommand();
            command.CommandText = sql;
            command.CommandType = CommandType.Text;
            command.Connection = connection;
            if (transaction != null)
                command.Transaction = transaction;
            if (parameters != null)
            {
                foreach (IDataParameter parameter in parameters)
                    command.Parameters.Add(parameter);
            }

            return command;
        }
    }
}
