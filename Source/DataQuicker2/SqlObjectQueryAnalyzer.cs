// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using DataQuicker2.Framework.Common;

namespace DataQuicker2.Framework
{
	internal static class SqlObjectQueryAnalyzer
	{
		public static SqlCommand CreateCommand4Count(ObjectQuery query)
		{
			string strSQL = "select count(*) from ({0}) as [dataquicker2_temporary_view]";
			strSQL = string.Format(strSQL, CreateNonPagingSQL(query, false));
			return new SqlCommand(strSQL);
		}

		public static SqlCommand CreateCommand(ObjectQuery query)
		{
			if (query.PagingType == PagingType.None)
				return CreateNonPagingCommand(query);
			else
				return CreatePagingCommand(query);
		}

		public static string CreateNonPagingSQL(ObjectQuery query, bool includeOrderBy)
		{
			Kit.VerifyNotNull(query);
			StringBuilder sbSQL = new StringBuilder();
			string strDistinct = query.SelectDistinct ? "DISTINCT" : "ALL";
			string strTopN = query.TopN > 0 ? "TOP " + query.TopN.ToString() : "";
			sbSQL.AppendFormat("SELECT {0} {1} {2} ", strDistinct, strTopN, ParseSelectString(query));
			sbSQL.AppendFormat("FROM {0} ", ParseFromString(query));

			string strWhere = ParseWhereString(query);
			if (!Kit.IsEmpty(strWhere))
				sbSQL.AppendFormat("WHERE {0} ", strWhere);

			string strGroupBy = ParseGroupByString(query);
			if (!Kit.IsEmpty(strGroupBy))
				sbSQL.AppendFormat("GROUP BY {0} ", strGroupBy);

			if (includeOrderBy)
			{
				string strOrderBy = ParseOrderByString(query);
				if (!Kit.IsEmpty(strOrderBy))
					sbSQL.AppendFormat("ORDER BY {0} ", strOrderBy);
			}

			return sbSQL.ToString();
		}

		public static SqlCommand CreateNonPagingCommand(ObjectQuery query)
		{
			return new SqlCommand(CreateNonPagingSQL(query, true));
		}

		public static SqlCommand CreatePagingCommand(ObjectQuery query)
		{
			Kit.VerifyNotNull(query);
			if (!(query.MainEntity is Table))
				throw new ViewNotSupportedPagingException("基于视图对象构造的查询不支持DataQuicker2托管分页查询。");

			Table table = query.MainEntity as Table;
			string strPrimaryKey = table.PrimaryKey.ColumnName.Replace("[", "").Replace("]", "");
			strPrimaryKey = table.TableName.Replace("[", "").Replace("]", "") + "." + strPrimaryKey;

			SqlCommand command = new SqlCommand();
			command.CommandType = CommandType.StoredProcedure;
			command.CommandText = "DataQuicker2_Framework_PagingCursor";

			SqlParameterCollection collection = command.Parameters;
			collection.Add(new SqlParameter("@Tables", ParseFromString(query)));
			collection.Add(new SqlParameter("@PK", strPrimaryKey));
			collection.Add(new SqlParameter("@Sort", ParseOrderByString(query)));
			collection.Add(new SqlParameter("@PageNumber", query.PageIndex));
			collection.Add(new SqlParameter("@PageSize", query.PageSize));
			collection.Add(new SqlParameter("@Fields", ParseSelectString(query)));
			collection.Add(new SqlParameter("@Filter", ParseWhereString(query)));
			collection.Add(new SqlParameter("@Group", ParseGroupByString(query)));

			return command;
		}

		private static string ParseSelectString(ObjectQuery query)
		{
			if (query.SelectFields == null || query.SelectFields.Count == 0) return "*";

			List<string> existedColumnNames = new List<string>();

			StringBuilder sbSQL = new StringBuilder();
			foreach (SelectColumn selectColumn in query.SelectFields)
			{
				string columnName;
				string strAliasName = ReservedWords.Protect(selectColumn.AliasName);
				if (existedColumnNames.Contains(strAliasName))
					throw new RepeatedColumnNamesException("查询字段中存在相同列名。");
				else
					existedColumnNames.Add(strAliasName);

				if (selectColumn.Column == null)
				{
					if (selectColumn.Function != Function.None)
					{
						columnName = "*";
						string strFunction = ParseFunction(selectColumn.Function);
						strFunction = string.Format(strFunction, columnName);
						sbSQL.AppendFormat("{0} as {1}, ", strFunction, strAliasName);
					}
					else
					{
						sbSQL.AppendFormat("'' as {0}, ", strAliasName);
					}
				}
				else
				{
					columnName = ReservedWords.Protect(query.Entities[selectColumn.Column.ParentEntity]) + "." + ReservedWords.Protect(selectColumn.Column.ColumnName);
					if (selectColumn.Function == Function.None)
						sbSQL.AppendFormat("{0} as {1}, ", columnName, strAliasName);
					else
					{
						string strFunction = ParseFunction(selectColumn.Function);
						strFunction = string.Format(strFunction, columnName);
						sbSQL.AppendFormat("{0} as {1}, ", strFunction, strAliasName);
					}
				}
			}

			if (sbSQL.Length > 2) sbSQL.Remove(sbSQL.Length - 2, 2);

			return sbSQL.ToString();
		}

		private static string ParseFunction(Function funcion)
		{
			switch (funcion)
			{
				case Function.Average:
					return "Average({0})";
				case Function.Count:
					return "Count({0})";
				case Function.Max:
					return "Max({0})";
				case Function.Min:
					return "Min({0})";
				case Function.Sum:
					return "Sum({0})";
			}
			return "{0}";
		}

		private static string ParseFromString(ObjectQuery query)
		{
			if (query.Entities.Count == 1)
			{
				Dictionary<IEntity, string>.Enumerator entity0 = query.Entities.GetEnumerator();
				entity0.MoveNext();
				return ReservedWords.Protect(entity0.Current.Value);
			}

			StringBuilder sbSQL = new StringBuilder();
			Dictionary<IEntity, string>.Enumerator iterator = query.Entities.GetEnumerator();
			while (iterator.MoveNext())
			{
				if (sbSQL.Length == 0)
					sbSQL.AppendFormat("{0} {1}", ReservedWords.Protect(iterator.Current.Key.DbObjectName), ReservedWords.Protect(iterator.Current.Value));
				else
				{
					ColumnsAssociation columnsAssociation = GetColumnsAssociation(query, iterator.Current.Key);
					string strAssociationType = ParseAssociation(columnsAssociation.AssociationType);
					string strSecondEntityName = ReservedWords.Protect(iterator.Current.Key.DbObjectName);
					string strSecondEntityAliasName = ReservedWords.Protect(iterator.Current.Value);
					string strAssociationColumns = ParseAssociationColumns(query, iterator.Current.Key);
					sbSQL.AppendFormat(" {0} {1} {2} ON {3}", strAssociationType, strSecondEntityName, strSecondEntityAliasName, strAssociationColumns);
				}
			}
			return sbSQL.ToString();
		}

		private static string ParseAssociation(AssociationType associationType)
		{
			switch (associationType)
			{
				case AssociationType.InnerJoin:
					return "INNER JOIN";
				case AssociationType.LeftJoin:
					return "LEFT JOIN";
				default:
					return "RIGHT JOIN";
			}
		}

		private static string ParseAssociationColumns(ObjectQuery query, IEntity entity)
		{
			foreach (ColumnsAssociation columnAssociation in query.Associations)
			{
				if (columnAssociation.SecondColumns[0].ParentEntity == entity)
				{
					StringBuilder sbSQL = new StringBuilder();

					for (int i = 0; i < columnAssociation.MainColumns.Count; i++)
					{
						string strMainEntityName = ReservedWords.Protect(query.Entities[columnAssociation.MainColumns[i].ParentEntity]);
						string strMainColumn = ReservedWords.Protect(columnAssociation.MainColumns[i].ColumnName);
						string strSecondEntityName = ReservedWords.Protect(query.Entities[columnAssociation.SecondColumns[i].ParentEntity]);
						string strSecondColumn = ReservedWords.Protect(columnAssociation.SecondColumns[i].ColumnName);
						sbSQL.AppendFormat("{0}.{1}={2}.{3} AND ", strMainEntityName, strMainColumn, strSecondEntityName, strSecondColumn);
					}
					if (sbSQL.Length > 0)
						sbSQL.Remove(sbSQL.Length - 5, 5);

					return sbSQL.ToString();
				}
			}
			return "";
		}

		private static ColumnsAssociation GetColumnsAssociation(ObjectQuery query, IEntity secondEntity)
		{
			foreach (ColumnsAssociation columnAssociation in query.Associations)
			{
				if (columnAssociation.SecondColumns[0].ParentEntity == secondEntity)
					return columnAssociation;
			}
			return null;
		}

		private static string ParseWhereString(ObjectQuery query)
		{
			if (query.CriteriaItems == null || query.CriteriaItems.Count == 0) return "";
			StringBuilder sbSQL = new StringBuilder();
			foreach (CriteriaItem criteriaItem in query.CriteriaItems)
			{
				string strCriteriaItem = ParseOperator(query, criteriaItem.Column, criteriaItem.Op, criteriaItem.Values);
				if (sbSQL.Length == 0)
					sbSQL.Append(strCriteriaItem);
				else
					sbSQL.AppendFormat(" AND {0}", strCriteriaItem);
			}
			return sbSQL.ToString();
		}

		private static string ParseOperator(ObjectQuery query, IColumn column, Operator op, params object[] values)
		{
			bool isStringType = column.GenericType == typeof(string) || column.GenericType == typeof(DateTime);
			string strSQL = null;
			switch (op)
			{
				case Operator.BothSidesLike:
					strSQL = "LIKE '%{0}%'";
					break;
				case Operator.BothSidesNotLike:
					strSQL = "NOT LIKE '%{0}%'";
					break;
				case Operator.Equal:
					strSQL = "= {0}";
					break;
				case Operator.NotEqual:
					strSQL = "<> {0}";
					break;
				case Operator.In:
					strSQL = "IN ({0})";
					break;
				case Operator.LessEqualThan:
					strSQL = "<= {0}";
					break;
				case Operator.LessThan:
					strSQL = "< {0}";
					break;
				case Operator.Like:
					strSQL = "LIKE '{0}'";
					break;
				case Operator.MoreEqualThan:
					strSQL = ">= {0}";
					break;
				case Operator.MoreThan:
					strSQL = "> {0}";
					break;
				case Operator.NotIn:
					strSQL = "NOT IN ({0})";
					break;
				case Operator.NotLike:
					strSQL = "NOT LIKE '{0}'";
					break;
				case Operator.PrefixLike:
					strSQL = "LIKE '%{0}'";
					break;
				case Operator.PrefixNotLike:
					strSQL = "NOT LIKE '%{0}'";
					break;
				case Operator.SuffixLike:
					strSQL = "LIKE '{0}%'";
					break;
				case Operator.SuffixNotLike:
					strSQL = "NOT LIKE '{0}%'";
					break;
			}

			if (strSQL.IndexOf("IN") > -1)
			{
				StringBuilder sbValues = new StringBuilder();
				foreach (object value in values)
						sbValues.AppendFormat("'{0}', ", Kit.ConvertSqlValue(value));

				if (sbValues.Length > 0) sbValues.Remove(sbValues.Length - 2, 2);
				strSQL = string.Format(strSQL, sbValues.ToString());
			}
			else if (strSQL.IndexOf("LIKE") > -1)
				strSQL = string.Format(strSQL, Kit.ConvertSqlValue(values[0]));
			else
			{
                if (values[0] != null && values[0].GetType().GetInterface("DataQuicker2.Framework.IColumn") != null)
                {
                    IColumn argColumn = values[0] as IColumn;
                    string strColumnName = ReservedWords.Protect(argColumn.ParentEntity.DbObjectName);
                    strColumnName += "." + ReservedWords.Protect(argColumn.ColumnName);
                    strSQL = string.Format(strSQL, strColumnName);
                }
                else if (isStringType || column.GenericType.IsSubclassOf(typeof(Table)) || column.GenericType.IsSubclassOf(typeof(View)))
                {
                    strSQL = string.Format(strSQL, "'" + Kit.ConvertSqlValue(values[0]) + "'");
                }
                else
                {
                    string strValue = Kit.ConvertSqlValue(values[0]);
                    strSQL = string.Format(strSQL, (strValue.Length == 0) ? "''" : strValue);
                }
			}

			return ReservedWords.Protect(query.Entities[column.ParentEntity]) + "." + ReservedWords.Protect(column.ColumnName) + " " + strSQL;
		}

		private static string ParseGroupByString(ObjectQuery query)
		{
			if (query.GroupByFields == null || query.GroupByFields.Count == 0) return "";
			StringBuilder sbSQL = new StringBuilder();
			foreach (IColumn column in query.GroupByFields)
			{
				if (sbSQL.Length == 0)
					sbSQL.Append(ReservedWords.Protect(query.Entities[column.ParentEntity]) + "." + ReservedWords.Protect(column.ColumnName));
				else
					sbSQL.AppendFormat(", {0}.{1}", ReservedWords.Protect(query.Entities[column.ParentEntity]), ReservedWords.Protect(column.ColumnName));
			}
			return sbSQL.ToString();
		}

		private static string ParseOrderByString(ObjectQuery query)
		{
			if (query.OrderByFields == null || query.OrderByFields.Count == 0) return "";
			StringBuilder sbSQL = new StringBuilder();
			Dictionary<IColumn, bool>.Enumerator iterator = query.OrderByFields.GetEnumerator();
			while (iterator.MoveNext())
			{
				string strColumnName = ReservedWords.Protect(query.Entities[iterator.Current.Key.ParentEntity]) + "." + ReservedWords.Protect(iterator.Current.Key.ColumnName);
				string strOrderBy = iterator.Current.Value?"ASC":"DESC";
				if (sbSQL.Length == 0)
					sbSQL.AppendFormat("{0} {1}", strColumnName, strOrderBy);
			}
			return sbSQL.ToString();
		}
	}
}
