// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
//
// Modified: Alex
// Email: AlexHe.cs@Gmail.com
// Date: 2006-04-23
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using DataQuicker2.Framework.Common;
using DataQuicker2.Framework.DqlLexicalAnalysis;

namespace DataQuicker2.Framework
{
    /// <summary>
    /// 数据库操作类。<br/>
    /// Database operation class.
    /// </summary>
    public class SqlProvider : IProvider
    {
        #region IPersistProvider Members

        #region Update
        /// <summary>
        /// 对实体对象进行更新。<br/>
        /// Update table instance.
        /// </summary>
        /// <param name="table"></param>
        public void Update(Table table)
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                this.Update(table, connection);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 对实体对象进行更新。<br/>
        /// Update table instance.
        /// </summary>
        /// <param name="table"></param>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        public void Update(Table table, IDbConnection connection)
        {
            IDbCommand command = this.GetUpdateCommand(table);
            if (command == null) return;
            command.Connection = connection;
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception exp)
            {
                string strSQL = IDbCommandConvertion.ToString(command);
				Log.Instance.Error("在执行更新操作时发生错误，执行的SQL语句为：\r\n" + strSQL, exp);
                throw exp;
            }
        }

        /// <summary>
        /// 对实体对象进行更新。<br/>
        /// Update table instance.
        /// </summary>
        /// <param name="table"></param>
        /// <param name="transaction">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        public void Update(Table table, IDbTransaction transaction)
        {
            IDbCommand command = this.GetUpdateCommand(table);
            if (command == null) return;
            command.Connection = transaction.Connection;
            command.Transaction = transaction;
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception exp)
            {
                string strSQL = IDbCommandConvertion.ToString(command);
				Log.Instance.Error("在执行更新操作时发生错误，执行的SQL语句为：\r\n" + strSQL, exp);
                throw exp;
            }
        }

        /// <summary>
        /// 分析更新操作。<br/>
        /// Analyze update operation.
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        private SqlCommand GetUpdateCommand(Table table)
        {
            if (!table.IsExist)
                throw new InvalidOperationException("在对象实例化之前或不存在时不能进行更新。");

            if (table.PrimaryKey.IsNull)
                throw new InvalidOperationException("更新时表的主键值不能为空。");

            SqlCommand command = new SqlCommand();

            int index = 0;
            string strVariableName;
            StringBuilder sbSQL = new StringBuilder();
            List<IColumn>.Enumerator iterator = table.Columns.GetEnumerator();
            while (iterator.MoveNext())
            {
                IColumn column = iterator.Current;
                if (column.IsChanged && (table.PrimaryKey as IColumn != column))
                {
                    object value = null;
                    if (column.GenericType.IsSubclassOf(typeof(Table)))
                    {
                        Table refTable = column.Value as Table;
                        value = refTable.PrimaryKey.Value;
                    }
                    else
                    {
                        value = column.Value;
                    }

                    if (value == null) value = DBNull.Value;

                    strVariableName = "@Column" + (index++).ToString();
                    if (sbSQL.Length != 0) sbSQL.Append(", ");
                    sbSQL.Append(ReservedWords.Protect(column.ColumnName));
                    sbSQL.Append("=");
                    sbSQL.Append(strVariableName);
                    command.Parameters.Add(new SqlParameter(strVariableName, value));
                }
            }

            if (sbSQL.Length == 0) return null;

            strVariableName = "@PrimaryKeyValue";
            string strPrimaryKey = ReservedWords.Protect(table.PrimaryKey.ColumnName) + "=" + strVariableName;
            command.Parameters.Add(new SqlParameter(strVariableName, table.PrimaryKey.Value));

            string strSQL = "UPDATE {0} SET {1} WHERE {2}";
            strSQL = string.Format(strSQL, ReservedWords.Protect(table.TableName), sbSQL.ToString(), strPrimaryKey);
            command.CommandType = CommandType.Text;
            command.CommandText = strSQL;
            return command;
        }

        #endregion

        #region Delete

        /// <summary>
        /// 从数据库中删除实体对象。<br/>
        /// Delete current instance from database.
        /// </summary>
        /// <param name="table"></param>
        public void Delete(Table table)
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                this.Delete(table, connection);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 从数据库中删除实体对象。<br/>
        /// Delete current instance from database.
        /// </summary>
        /// <param name="table"></param>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        public void Delete(Table table, IDbConnection connection)
        {
            IDbCommand command = this.GetDeleteCommand(table);
            command.Connection = connection;
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception exp)
            {
                string strSQL = IDbCommandConvertion.ToString(command);
				Log.Instance.Error("在执行删除操作时发生错误，执行的SQL语句为：\r\n" + strSQL, exp);
                throw exp;
            }
            table.IsExist = false;
        }

        /// <summary>
        /// 从数据库中删除实体对象。<br/>
        /// Delete current instance from database.
        /// </summary>
        /// <param name="table"></param>
        /// <param name="transaction">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        public void Delete(Table table, IDbTransaction transaction)
        {
            IDbCommand command = this.GetDeleteCommand(table);
            command.Connection = transaction.Connection;
            command.Transaction = transaction;
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception exp)
            {
                string strSQL = IDbCommandConvertion.ToString(command);
				Log.Instance.Error("在执行删除操作时发生错误，执行的SQL语句为：\r\n" + strSQL, exp);
                throw exp;
            }
            table.IsExist = false;
        }

        /// <summary>
        /// 分析删除操作。<br/>
        /// Analyze delete operation.
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        private IDbCommand GetDeleteCommand(Table table)
        {
            if (!table.IsExist)
                throw new InvalidOperationException("在对象实例化之前或不存在时不能进行删除。");

            if (table.PrimaryKey.IsNull)
                throw new InvalidOperationException("删除时表的主键值不能为空。");

            SqlCommand command = new SqlCommand();
            string strSQL = "UPDATE {0} SET isDeleted = '1' WHERE {1}";

            string strVariableName = "@PrimaryKeyValue";
            string strPrimaryKey = ReservedWords.Protect(table.PrimaryKey.ColumnName) + "=" + strVariableName;
            command.Parameters.Add(new SqlParameter(strVariableName, table.PrimaryKey.Value));

            strSQL = string.Format(strSQL, ReservedWords.Protect(table.TableName), strPrimaryKey);
            command.CommandType = CommandType.Text;
            command.CommandText = strSQL;

            return command;
        }

        #endregion

        #region Create

        /// <summary>
        /// 向数据库中添加实体对象。<br/>
        /// Create <see cref="Table"/> instance to database.
        /// </summary>
        /// <param name="table"></param>
        public void Create(Table table)
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                this.Create(table, connection);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 向数据库中添加实体对象。<br/>
        /// Create <see cref="Table"/> instance to database.
        /// </summary>
        /// <param name="table"></param>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        public void Create(Table table, IDbConnection connection)
        {
            IDbCommand command = GetCreateCommand(table);
            command.Connection = connection;
            try
            {
                object result = command.ExecuteScalar();

                if (result != null && result != DBNull.Value)
                    table.PrimaryKey.Value = result;

                table.IsExist = true;
            }
            catch (Exception exp)
            {
                string strSQL = IDbCommandConvertion.ToString(command);
				Log.Instance.Error("在执行新增操作时发生错误，执行的SQL语句为：\r\n" + strSQL, exp);
                throw exp;
            }
        }

        /// <summary>
        /// 向数据库中添加实体对象。<br/>
        /// Create <see cref="Table"/> instance to database.
        /// </summary>
        /// <param name="table"></param>
        /// <param name="transaction">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        public void Create(Table table, IDbTransaction transaction)
        {
            IDbCommand command = GetCreateCommand(table);
            command.Connection = transaction.Connection;
            command.Transaction = transaction;
            try
            {
                object result = command.ExecuteScalar();

                if (result != null && result != DBNull.Value)
                    table.PrimaryKey.Value = result;

                table.IsExist = true;
            }
            catch (Exception exp)
            {
                string strSQL = IDbCommandConvertion.ToString(command);
				Log.Instance.Error("在执行新增操作时发生错误，执行的SQL语句为：\r\n" + strSQL, exp);
                throw exp;
            }
        }

        private static SqlCommand GetCreateCommand(Table table)
        {
            SqlCommand command = new SqlCommand();
            string strSQL = GetInsertSQL(table, command);

            IColumn primaryKey = table.PrimaryKey;

            if (primaryKey.ColumnValueType == ColumnValueType.DbAutoIncrease)
                strSQL += "SELECT @@identity FROM " + ReservedWords.Protect(table.TableName) + ";";
            else if (primaryKey.ColumnValueType != ColumnValueType.Unmanaged && !table.CancelManageAutoValue)
            {
                if (primaryKey.ColumnValueType == ColumnValueType.GuidManaged)
                {
                    strSQL = GetManageGuid(table.PrimaryKey) + strSQL + "SELECT @PrimaryKeyValue";
                }
                else if (primaryKey.ColumnValueType == ColumnValueType.IntManaged && primaryKey.GenericType == typeof(int))
                    strSQL = GetIntManageIntIncrease(table.PrimaryKey) + strSQL + "SELECT @PrimaryKeyValue";
                else if (primaryKey.ColumnValueType == ColumnValueType.IntManaged && primaryKey.GenericType == typeof(string))
                    strSQL = GetStringManageIntIncrease(table.PrimaryKey) + strSQL + "SELECT @PrimaryKeyValue";
            }

            command.CommandType = CommandType.Text;
            command.CommandText = strSQL;

            return command;
        }

        private static string GetInsertSQL(Table table, SqlCommand command)
        {
            StringBuilder sbFieldList = new StringBuilder();
            StringBuilder sbFieldValueList = new StringBuilder();

            int index = 0;
            string strVariableName;

            foreach (IColumn column in table.Columns)
            {
                if (!column.IsNull && (column.ColumnValueType == ColumnValueType.Unmanaged || table.CancelManageAutoValue))
                {
                    object value = null;
                    if (column.GenericType.IsSubclassOf(typeof(Table)))
                    {
                        Table refTable = column.Value as Table;
                        if (refTable.PrimaryKey.IsNull) continue;
                        value = refTable.PrimaryKey.Value;
                    }
                    else
                        value = column.Value;

                    strVariableName = "@ColumnValue" + index++.ToString();
                    sbFieldList.Append(ReservedWords.Protect(column.ColumnName));
                    sbFieldList.Append(", ");
                    sbFieldValueList.Append(strVariableName);
                    sbFieldValueList.Append(", ");
                    command.Parameters.Add(new SqlParameter(strVariableName, value));
                }
                else if (column.ColumnValueType == ColumnValueType.GuidManaged || column.ColumnValueType == ColumnValueType.IntManaged)
                {
                    strVariableName = "@PrimaryKeyValue";
                    sbFieldList.Append(ReservedWords.Protect(column.ColumnName));
                    sbFieldList.Append(", ");
                    sbFieldValueList.Append(strVariableName);
                    sbFieldValueList.Append(", ");
                }
            }

            if (sbFieldList.Length == 0) return "";

            sbFieldList.Remove(sbFieldList.Length - 2, 2);
            sbFieldValueList.Remove(sbFieldValueList.Length - 2, 2);

            string strSQL = "INSERT INTO {0}({1}) VALUES ({2}); ";
            strSQL = string.Format(strSQL, ReservedWords.Protect(table.TableName), sbFieldList, sbFieldValueList);

            return strSQL;
        }

        private static string GetStringManageIntIncrease(IColumn primaryKey)
        {
            string strSQL = "DECLARE @PrimaryKeyValue AS INT; SET @PrimaryKeyValue=ISNULL((SELECT MAX(CASE ISNUMERIC({0})WHEN 1 THEN CAST({0} AS int)WHEN 0 THEN 0 END) FROM {1}),0)+1; ";
            strSQL = string.Format(strSQL, ReservedWords.Protect(primaryKey.ColumnName), ReservedWords.Protect(primaryKey.ParentEntity.DbObjectName));
            return strSQL;
        }

        private static string GetIntManageIntIncrease(IColumn primaryKey)
        {
            string strSQL = "DECLARE @PrimaryKeyValue AS INT; SET @PrimaryKeyValue=ISNULL((SELECT MAX({0}) FROM {1}),0)+1; ";
            strSQL = string.Format(strSQL, ReservedWords.Protect(primaryKey.ColumnName), ReservedWords.Protect(primaryKey.ParentEntity.DbObjectName));
            return strSQL;
        }

        private static string GetManageGuid(IColumn primaryKey)
        {
            string strSQL = "DECLARE @PrimaryKeyValue AS uniqueidentifier; SET @PrimaryKeyValue='{0}'; ";
            if (primaryKey.IsNull)
            {
                strSQL = string.Format(strSQL, System.Guid.NewGuid().ToString());
            }
            else
            {
                strSQL = string.Format(strSQL, primaryKey.Value.ToString());
            }
            return strSQL;
        }

        #endregion

        #region Load

        /// <summary>
        /// 通过实体对象的主键值来实例化当前对象。<br/>
        /// Initialize current instance by primary key of table instance.
        /// </summary>
        /// <param name="table"></param>
        public void Load(Table table)
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                this.Load(table, connection);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 通过实体对象的主键值来实例化当前对象。<br/>
        /// Initialize current instance by primary key of table instance.
        /// </summary>
        /// <param name="table"></param>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        public void Load(Table table, IDbConnection connection)
        {
            Load(table, connection as SqlConnection, null);
        }

        /// <summary>
        /// 通过实体对象的主键值来实例化当前对象。<br/>
        /// Initialize current instance by primary key of table instance.
        /// </summary>
        /// <param name="table"></param>
        /// <param name="transaction">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        public void Load(Table table, IDbTransaction transaction)
        {
            Load(table, transaction.Connection as SqlConnection, transaction as SqlTransaction);
        }

        private static void Load(Table table, SqlConnection connection, SqlTransaction transaction)
        {
            if (table.PrimaryKey.IsNull)
                throw new InvalidOperationException("主键值为空，不能进行实例化。");

            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append("SELECT ");
         
            foreach(IColumn column in table.Columns)
            {
                sbSQL.Append(ReservedWords.Protect(column.ColumnName));
                sbSQL.Append(", ");
            }
           
            sbSQL.Remove(sbSQL.Length - 2, 2);
            sbSQL.Append(" FROM ");
            sbSQL.Append(ReservedWords.Protect(table.TableName));
            sbSQL.AppendFormat(" WHERE {0}=@PrimaryKeyValue", ReservedWords.Protect(table.PrimaryKey.ColumnName));

            SqlCommand command = new SqlCommand();
            command.Parameters.Add(new SqlParameter("@PrimaryKeyValue", table.PrimaryKey.Value));
            command.CommandType = CommandType.Text;
            command.CommandText = sbSQL.ToString();
            command.Connection = connection;
            if (transaction != null)
                command.Transaction = transaction;

            SqlDataReader reader = null;
            try
            {
                reader = command.ExecuteReader();
            }
            catch (Exception exp)
            {
                string strSQL = IDbCommandConvertion.ToString(command);
				Log.Instance.Error("在填充数据集时发生错误，执行的SQL语句为：\r\n" + strSQL, exp);
                throw exp;
            }

            if (!reader.HasRows)
            {
                table.IsExist = false;
                reader.Close();
                reader.Dispose();
                return;
            }

            short count = 0;
            Dictionary<string, object> readerValues = new Dictionary<string, object>();
            try
            {
                while (reader.Read())
                {
                    if (count > 0)
                        throw new DuplicateObjectsException("实例化得到的对象数量不唯一。");
                    else
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                            readerValues.Add(reader.GetName(i), reader[i]);
                        count++;
                    }
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
            }

            table.IsExist = true;
            Dictionary<string, object>.Enumerator iterator = readerValues.GetEnumerator();
            while (iterator.MoveNext())
            {
                string strColumnName = iterator.Current.Key;
                SetColumnValue(table, strColumnName, iterator.Current.Value, connection, transaction);
            }
        }

        #endregion

        #region InitObject

        /// <summary>
        /// 通过实体对象所有列属性上设定的值来实例化当前对象。<br/>
        /// Initialize current instance by all columns value on current entity.
        /// </summary>
        /// <param name="entity"></param>
        public void InitObject(IEntity entity)
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                this.InitObject(entity, connection);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 通过实体对象所有列属性上设定的值来实例化当前对象。<br/>
        /// Initialize current instance by all columns value on current entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        public void InitObject(IEntity entity, IDbConnection connection)
        {
            InitObject(entity, connection as SqlConnection, null);
        }

        /// <summary>
        /// 通过实体对象所有列属性上设定的值来实例化当前对象。<br/>
        /// Initialize current instance by all columns value on current entity.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="transaction">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        public void InitObject(IEntity entity, IDbTransaction transaction)
        {
            InitObject(entity, transaction.Connection as SqlConnection, transaction as SqlTransaction);
        }

        /// <summary>
        /// 分析实例化操作。<br/>
        /// Analyze initialization operation.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        private static void InitObject(IEntity entity, SqlConnection connection, SqlTransaction transaction)
        {
            StringBuilder sbSelectFieldList = new StringBuilder();
            StringBuilder sbWhereFieldList = new StringBuilder();
            SqlCommand command = new SqlCommand();

            for (int i = 0; i < entity.Columns.Count; i++)
            {
                IColumn column = entity.Columns[i];
                sbSelectFieldList.Append(ReservedWords.Protect(column.ColumnName));
                sbSelectFieldList.Append(", ");

                if (!column.IsNull)
                {
                    string strVariableName = "@ColumnValue" + i.ToString();
                    if (column.GenericType.IsSubclassOf(typeof(Table)))
                    {
                        Table refTable = column.Value as Table;
                        if (!refTable.PrimaryKey.IsNull)
                            command.Parameters.Add(new SqlParameter(strVariableName, refTable.PrimaryKey.Value));
                        else
                            continue;
                    }
                    else
                        command.Parameters.Add(new SqlParameter(strVariableName, column.Value));

                    sbWhereFieldList.AppendFormat("{0}={1}", ReservedWords.Protect(column.ColumnName), strVariableName);
                    sbWhereFieldList.Append(" AND ");
                }
            }

            if (sbWhereFieldList.Length == 0)
                throw new InvalidProgramException("实体上所有的字段值都为空，不能进行实例化。");

            sbSelectFieldList.Remove(sbSelectFieldList.Length - 2, 2);
            sbWhereFieldList.Remove(sbWhereFieldList.Length - 5, 5);

            string strSQL = "SELECT {0} FROM {1} WHERE {2}";
            strSQL = string.Format(strSQL, sbSelectFieldList, ReservedWords.Protect(entity.DbObjectName), sbWhereFieldList);
            command.CommandType = CommandType.Text;
            command.CommandText = strSQL;
            command.Connection = connection;
            if (transaction != null)
                command.Transaction = transaction;

            SqlDataReader reader = null;
            try
            {
                reader = command.ExecuteReader();
            }
            catch (Exception exp)
            {
                strSQL = IDbCommandConvertion.ToString(command);
				Log.Instance.Error("在填充数据集时发生错误，执行的SQL语句为：\r\n" + strSQL, exp);
                throw exp;
            }

            if (!reader.HasRows)
            {
                entity.IsExist = false;
                reader.Close();
                reader.Dispose();
                return;
            }

            short count = 0;
            Dictionary<string, object> readerValues = new Dictionary<string, object>();
            try
            {
                while (reader.Read())
                {
                    if (count > 0)
                        throw new DuplicateObjectsException("实例化得到的对象数量不唯一。");
                    else
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                            readerValues.Add(reader.GetName(i), reader[i]);

                        count++;
                    }
                }
            }
            finally
            {
                reader.Close();
                reader.Dispose();
            }

            entity.IsExist = true;
            Dictionary<string, object>.Enumerator iterator = readerValues.GetEnumerator();
            while (iterator.MoveNext())
            {
                string strColumnName = iterator.Current.Key;
                SetColumnValue(entity, strColumnName, iterator.Current.Value, connection, transaction);
            }
        }

        #endregion

        #region GetRecordsCount

        /// <summary>
        /// 通过实体对象所有列属性上设定的值来获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="entity"></param>
        public int GetRecordsCount(IEntity entity)
        {
            int intResult = 0;
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                intResult = this.GetRecordsCount(entity, connection);
            }
            finally
            {
                connection.Close();
            }
            return intResult;
        }

        /// <summary>
        /// 通过实体对象所有列属性上设定的值来获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        public int GetRecordsCount(IEntity entity, IDbConnection connection)
        {
            return GetRecordsCount(entity, connection as SqlConnection, null);
        }

        /// <summary>
        /// 通过实体对象所有列属性上设定的值来获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="transaction">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        public int GetRecordsCount(IEntity entity, IDbTransaction transaction)
        {
            return GetRecordsCount(entity, transaction.Connection as SqlConnection, transaction as SqlTransaction);
        }

        /// <summary>
        /// 获得当前记录数。<br/>
        /// Get current RecordsCount.
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <returns>记录数</returns>
        private static int GetRecordsCount(IEntity entity, SqlConnection connection, SqlTransaction transaction)
        {
            StringBuilder sbWhereFieldList = new StringBuilder();
            SqlCommand command = new SqlCommand();

            for (int i = 0; i < entity.Columns.Count; i++)
            {
                IColumn column = entity.Columns[i];
                if (!column.IsNull)
                {
                    string strVariableName = "@ColumnValue" + i.ToString();
                    if (column.GenericType.IsSubclassOf(typeof(Table)))
                    {
                        Table refTable = column.Value as Table;
                        if (!refTable.PrimaryKey.IsNull)
                            command.Parameters.Add(new SqlParameter(strVariableName, refTable.PrimaryKey.Value));
                        else
                            continue;
                    }
                    else
                        command.Parameters.Add(new SqlParameter(strVariableName, column.Value));

                    sbWhereFieldList.AppendFormat("{0}={1}", ReservedWords.Protect(column.ColumnName), strVariableName);
                    sbWhereFieldList.Append(" AND ");
                }
            }

            string strSQL = string.Empty;
            if (sbWhereFieldList.Length == 0)
            {
                strSQL = "SELECT COUNT(*) FROM {0}";
                strSQL = string.Format(strSQL, ReservedWords.Protect(entity.DbObjectName));
            }
            else
            {
                sbWhereFieldList.Remove(sbWhereFieldList.Length - 5, 5);

                strSQL = "SELECT COUNT(*) FROM {0} WHERE {1}";
                strSQL = string.Format(strSQL, ReservedWords.Protect(entity.DbObjectName), sbWhereFieldList);
            }

            command.CommandType = CommandType.Text;
            command.CommandText = strSQL;
            command.Connection = connection;
            if (transaction != null)
                command.Transaction = transaction;

            int count = 0;

            try
            {
                count = (int)command.ExecuteScalar();
            }
            catch (Exception exp)
            {
                strSQL = IDbCommandConvertion.ToString(command);
				Log.Instance.Error("在填充数据集时发生错误，执行的SQL语句为：\r\n" + strSQL, exp);
                throw exp;
            }

            return count;
        }

        /// <summary>
        /// 通过DqlQuery来获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="query"></param>
        /// <returns ></returns>
        public int GetRecordsCount(DqlQuery query)
        {
			IDbConnection connection = DbFactory.CreateOpenConnection();
            try
            {
				return this.GetRecordsCount(query, connection);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 通过DqlQuery来获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        public int GetRecordsCount(DqlQuery query, IDbConnection connection)
        {
            return GetRecordsCount(query, connection as SqlConnection, null);
        }

        /// <summary>
        /// 通过DqlQuery来获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="transaction">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        public int GetRecordsCount(DqlQuery query, IDbTransaction transaction)
        {
			Kit.VerifyNotNull(transaction);
            return GetRecordsCount(query, transaction.Connection as SqlConnection, transaction as SqlTransaction);
        }

        /// <summary>
		/// 通过DqlQuery来获得当前记录数。<br/>
        /// Get current RecordsCount.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <returns>记录数</returns>
        private static int GetRecordsCount(DqlQuery query, SqlConnection connection, SqlTransaction transaction)
        {
			Kit.VerifyNotNull(query);
			Kit.VerifyNotNull(connection);
			if (connection.State != ConnectionState.Open)
				throw new InvalidOperationException("Connection对象未打开。");

			DqlReflector reflector = new DqlReflector(query);
			string strSQL = reflector.Sql4Count;

			SqlCommand command = new SqlCommand(strSQL);
			command.Connection = connection;
            if (transaction != null) command.Transaction = transaction;

			int rowsCount = (int)command.ExecuteScalar();
            query.LastExecutedSql = IDbCommandConvertion.ToString(command);
			return rowsCount;
        }

        /// <summary>
        /// 通过ObjectQuery来获得当前记录数。<br/>
        /// Get current RecordsCount by DqlQuery.
        /// </summary>
        /// <param name="query"></param>
        /// <returns ></returns>
        public int GetRecordsCount(ObjectQuery query)
        {
            int intResult = 0;
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                intResult = this.GetRecordsCount(query, connection);
            }
            finally
            {
                connection.Close();
            }
            return intResult;
        }

        /// <summary>
        /// 通过ObjectQuery来获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        public int GetRecordsCount(ObjectQuery query, IDbConnection connection)
        {
            return GetRecordsCount(query, connection as SqlConnection, null);
        }

        /// <summary>
        /// 通过ObjectQuery来获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="transaction">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        public int GetRecordsCount(ObjectQuery query, IDbTransaction transaction)
        {
			Kit.VerifyNotNull(transaction);
            return GetRecordsCount(query, transaction.Connection as SqlConnection, transaction as SqlTransaction);
        }

        /// <summary>
        /// 通过ObjectQuery来获得当前记录数。<br/>
        /// Get current RecordsCount.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        /// <returns>记录数</returns>
        private static int GetRecordsCount(ObjectQuery query, SqlConnection connection, SqlTransaction transaction)
        {
            Kit.VerifyNotNull(query);
			Kit.VerifyNotNull(connection);
            SqlCommand command = SqlObjectQueryAnalyzer.CreateCommand4Count(query);
            command.Connection = connection;
            if(transaction != null)
                command.Transaction = transaction as SqlTransaction;

			try
			{
				int count = (int)command.ExecuteScalar();
				query.LastExecutedSql = IDbCommandConvertion.ToString(command);
				return count;
			}
			catch(Exception exp)
			{
				Log.Instance.Error(exp);
			}

			return 0;
        }

        #endregion

        private static void SetColumnValue(IEntity entity, string columnName, object value, SqlConnection connection, SqlTransaction transaction)
        {
            foreach (IColumn column in entity.Columns)
            {
                if (string.Compare(column.ColumnName, columnName, true) == 0)
                {
                    if (value == null || value == DBNull.Value)
                    {
                        column.Value = null;
                        return;
                    }

                    if (column.GenericType.IsSubclassOf(typeof(Table)))
                    {
                        Type entityType = column.GenericType;
                        Table refTable = Activator.CreateInstance(entityType) as Table;
                        refTable.PrimaryKey.Value = value;
                        if (transaction != null)
                            refTable.Load(transaction);
                        else
                            refTable.Load(connection);

                        column.Value = refTable;
                    }
                    else if (column.GenericType == typeof(string) || column.GenericType == typeof(DateTime))
                    {
                        column.Value = value.ToString().Trim();
                    }
                    else
                        column.Value = value;

                    return;
                }
            }
        }

        #region Fill
        private static SqlCommand GetPagingCommand(DqlQuery query, DqlReflector reflector, string sqlServerVersion)
        {
            Kit.VerifyNotNull(reflector);
            if (!(query.ParentEntity is Table))
                throw new ViewNotSupportedPagingException("基于视图对象构造的查询不支持DataQuicker2托管分页查询。");

            string strPrimaryKey = (query.ParentEntity as Table).PrimaryKey.ColumnName.Replace("[", "").Replace("]", "");
            strPrimaryKey = query.ParentEntity.DbObjectName.Replace("[", "").Replace("]", "") + "." + strPrimaryKey;

            SqlCommand command = new SqlCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "DataQuicker2_Framework_PagingCursor";

            SqlParameterCollection collection = command.Parameters;
            collection.Add(new SqlParameter("@Tables", reflector.FromSection));
            collection.Add(new SqlParameter("@PK", strPrimaryKey));
            collection.Add(new SqlParameter("@Sort", reflector.OrderbySection));
            collection.Add(new SqlParameter("@PageNumber", query.PageIndex));
            collection.Add(new SqlParameter("@PageSize", query.PageSize));
            collection.Add(new SqlParameter("@Fields", reflector.SelectSection));
            collection.Add(new SqlParameter("@Filter", reflector.WhereSection));
            collection.Add(new SqlParameter("@Group", reflector.GroupbySection));

            return command;
        }

        private static DqlReflector GetDqlReflector(DqlQuery query, out string sql)
        {
            DqlReflector reflector = new DqlReflector(query);
            sql = reflector.Sql;
            if (Kit.IsEmpty(sql))
                throw new NoSqlBeReflectedException("解析得到的SQL查询语句为空字符串，可能因为在DQL中设置了requiredWhereClause=true，并且在DqlQuery查询中没有给参数赋值。");

            return reflector;
        }

        /// <summary>
        /// 使用DqlQuery来填充DataTable对象。<br/>
        /// Fill the DataTable instance from DqlQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public void Fill(DqlQuery query, DataTable dt)
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                this.Fill(query, dt, connection);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 使用DqlQuery来填充DataSet对象。<br/>
        /// Fill the DataSet instance from DqlQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dst"></param>
        /// <returns></returns>
        public void Fill(DqlQuery query, DataSet dst)
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                this.Fill(query, dst, connection);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 使用DqlQuery来填充DataSet中的某个表对象。<br/>
        /// Fill one DataTable instance of DataSet instance from DqlQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dst"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public void Fill(DqlQuery query, DataSet dst, string tableName)
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                this.Fill(query, dst, tableName, connection);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 使用DqlQuery来填充DataTable对象。<br/>
        /// Fill the DataTable instance from DqlQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dt"></param>
        /// <param name="connection">
        /// 打开的数据库链接。<br/>Opened database connection.
        /// </param>
        /// <returns></returns>
        public void Fill(DqlQuery query, DataTable dt, IDbConnection connection)
        {
            Kit.VerifyNotNull(dt);
            string strSQL;
            DqlReflector reflector = GetDqlReflector(query, out strSQL);

            SqlConnection sqlConnection = connection as SqlConnection;
            if (connection.State != ConnectionState.Open)
                throw new InvalidOperationException("Connection对象未打开。");

            SqlDataAdapter adapter;
            if (query.PagingType == PagingType.None)
            {
                adapter = new SqlDataAdapter(strSQL, sqlConnection);
                query.LastExecutedSql = strSQL;
                try
                {
                    adapter.Fill(dt);
                }
                catch (Exception exp)
                {
					Log.Instance.Error("在填充数据集时发生错误，执行的SQL语句为：\r\n" + strSQL, exp);
                    throw exp;
                }
            }
            else
            {
                SqlCommand command = GetPagingCommand(query, reflector, sqlConnection.ServerVersion);
                command.Connection = sqlConnection;
                adapter = new SqlDataAdapter(command);
                query.LastExecutedSql = IDbCommandConvertion.ToString(command);
                try
                {
                    adapter.Fill(dt);
                }
                catch (Exception exp)
                {
                    strSQL = IDbCommandConvertion.ToString(command);
					Log.Instance.Error("在填充数据集时发生错误，执行的IDbCommand对象为：\r\n" + strSQL, exp);
                    throw exp;
                }
            }
        }

        /// <summary>
        /// 使用DqlQuery来填充DataSet对象。<br/>
        /// Fill the DataSet instance from DqlQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dst"></param>
        /// <param name="connection">
        /// 打开的数据库链接。<br/>Opened database connection.
        /// </param>
        /// <returns></returns>
        public void Fill(DqlQuery query, DataSet dst, IDbConnection connection)
        {
            Kit.VerifyNotNull(dst);
            string strSQL;
            DqlReflector reflector = GetDqlReflector(query, out strSQL);

            SqlConnection sqlConnection = connection as SqlConnection;
            if (connection.State != ConnectionState.Open)
                throw new InvalidOperationException("Connection对象未打开。");

            SqlDataAdapter adapter;
            if (query.PagingType == PagingType.None)
            {
                adapter = new SqlDataAdapter(strSQL, sqlConnection);
                query.LastExecutedSql = strSQL;
                try
                {
                    adapter.Fill(dst);
                }
                catch (Exception exp)
                {
					Log.Instance.Error("在填充数据集时发生错误，执行的SQL语句为：\r\n" + strSQL, exp);
                    throw exp;
                }
            }
            else
            {
                SqlCommand command = GetPagingCommand(query, reflector, sqlConnection.ServerVersion);
                command.Connection = sqlConnection;
                adapter = new SqlDataAdapter(command);
                query.LastExecutedSql = IDbCommandConvertion.ToString(command);
                try
                {
                    adapter.Fill(dst);
                }
                catch (Exception exp)
                {
                    strSQL = IDbCommandConvertion.ToString(command);
					Log.Instance.Error("在填充数据集时发生错误，执行的IDbCommand对象为：\r\n" + strSQL, exp);
                    throw exp;
                }
            }
        }

        /// <summary>
        /// 使用DqlQuery来填充DataSet中的某个表对象。<br/>
        /// Fill one DataTable instance of DataSet instance from DqlQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dst"></param>
        /// <param name="tableName"></param>
        /// <param name="connection">
        /// 打开的数据库链接。<br/>Opened database connection.
        /// </param>
        /// <returns></returns>
        public void Fill(DqlQuery query, DataSet dst, string tableName, IDbConnection connection)
        {
            Kit.VerifyNotNull(dst);
            Kit.VerifyNotNull(tableName);
            string strSQL;
            DqlReflector reflector = GetDqlReflector(query, out strSQL);

            SqlConnection sqlConnection = connection as SqlConnection;
            if (connection.State != ConnectionState.Open)
                throw new InvalidOperationException("Connection对象未打开。");

            SqlDataAdapter adapter;
            if (query.PagingType == PagingType.None)
            {
                adapter = new SqlDataAdapter(strSQL, sqlConnection);
                query.LastExecutedSql = strSQL;
                try
                {
                    adapter.Fill(dst, tableName);
                }
                catch (Exception exp)
                {
					Log.Instance.Error("在填充数据集时发生错误，执行的SQL语句为：\r\n" + strSQL, exp);
                    throw exp;
                }
            }
            else
            {
                SqlCommand command = GetPagingCommand(query, reflector, sqlConnection.ServerVersion);
                command.Connection = sqlConnection;
                adapter = new SqlDataAdapter(command);
                query.LastExecutedSql = IDbCommandConvertion.ToString(command);
                try
                {
                    adapter.Fill(dst, tableName);
                }
                catch (Exception exp)
                {
                    strSQL = IDbCommandConvertion.ToString(command);
					Log.Instance.Error("在填充数据集时发生错误，执行的IDbCommand对象为：\r\n" + strSQL, exp);
                    throw exp;
                }
            }
        }

        /// <summary>
        /// 使用DqlQuery来填充DataTable对象。<br/>
        /// Fill the DataTable instance from DqlQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dt"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public void Fill(DqlQuery query, DataTable dt, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(dt);
            string strSQL;
            DqlReflector reflector = GetDqlReflector(query, out strSQL);

            SqlTransaction sqlTransaction = transaction as SqlTransaction;
            SqlConnection sqlConnection = sqlTransaction.Connection;
            if (sqlConnection.State != ConnectionState.Open)
                throw new InvalidOperationException("Connection对象未打开。");

            SqlDataAdapter adapter;
            SqlCommand command;
            if (query.PagingType == PagingType.None)
            {
                command = new SqlCommand(strSQL, sqlConnection);
                command.Transaction = sqlTransaction;
                adapter = new SqlDataAdapter(command);
            }
            else
            {
                command = GetPagingCommand(query, reflector, sqlConnection.ServerVersion);
                command.Connection = sqlConnection;
                command.Transaction = sqlTransaction;
                adapter = new SqlDataAdapter(command);
            }

            query.LastExecutedSql = IDbCommandConvertion.ToString(command);

            try
            {
                adapter.Fill(dt);
            }
            catch (Exception exp)
            {
                strSQL = IDbCommandConvertion.ToString(command);
				Log.Instance.Error("在填充数据集时发生错误，执行的IDbCommand对象为：\r\n" + strSQL, exp);
                throw exp;
            }
        }

        /// <summary>
        /// 使用DqlQuery来填充DataSet对象。<br/>
        /// Fill the DataSet instance from DqlQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dst"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public void Fill(DqlQuery query, DataSet dst, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(dst);
            string strSQL;
            DqlReflector reflector = GetDqlReflector(query, out strSQL);

            SqlTransaction sqlTransaction = transaction as SqlTransaction;
            SqlConnection sqlConnection = sqlTransaction.Connection;
            if (sqlConnection.State != ConnectionState.Open)
                throw new InvalidOperationException("Connection对象未打开。");

            SqlDataAdapter adapter;
            SqlCommand command;
            if (query.PagingType == PagingType.None)
            {
                command = new SqlCommand(strSQL, sqlConnection);
                command.Transaction = sqlTransaction;
                adapter = new SqlDataAdapter(command);
            }
            else
            {
                command = GetPagingCommand(query, reflector, sqlConnection.ServerVersion);
                command.Connection = sqlConnection;
                command.Transaction = sqlTransaction;
                adapter = new SqlDataAdapter(command);
            }

            query.LastExecutedSql = IDbCommandConvertion.ToString(command);

            try
            {
                adapter.Fill(dst);
            }
            catch (Exception exp)
            {
                strSQL = IDbCommandConvertion.ToString(command);
				Log.Instance.Error("在填充数据集时发生错误，执行的IDbCommand对象为：\r\n" + strSQL, exp);
                throw exp;
            }
        }

        /// <summary>
        /// 使用DqlQuery来填充DataSet中的某个表对象。<br/>
        /// Fill one DataTable instance of DataSet instance from DqlQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dst"></param>
        /// <param name="tableName"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public void Fill(DqlQuery query, DataSet dst, string tableName, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(dst);
            Kit.VerifyNotNull(tableName);
            string strSQL;
            DqlReflector reflector = GetDqlReflector(query, out strSQL);

            SqlTransaction sqlTransaction = transaction as SqlTransaction;
            SqlConnection sqlConnection = sqlTransaction.Connection;
            if (sqlConnection.State != ConnectionState.Open)
                throw new InvalidOperationException("Connection对象未打开。");

            SqlDataAdapter adapter;
            SqlCommand command;
            if (query.PagingType == PagingType.None)
            {
                command = new SqlCommand(strSQL, sqlConnection);
                command.Transaction = sqlTransaction;
                adapter = new SqlDataAdapter(command);
            }
            else
            {
                command = GetPagingCommand(query, reflector, sqlConnection.ServerVersion);
                command.Connection = sqlConnection;
                command.Transaction = sqlTransaction;
                adapter = new SqlDataAdapter(command);
            }

            query.LastExecutedSql = IDbCommandConvertion.ToString(command);

            try
            {
                adapter.Fill(dst, tableName);
            }
            catch (Exception exp)
            {
                strSQL = IDbCommandConvertion.ToString(command);
				Log.Instance.Error("在填充数据集时发生错误，执行的IDbCommand对象为：\r\n" + strSQL, exp);
                throw exp;
            }
        }

        #endregion

        #region Store Procedure

        private static void CheckSpParameters(StoreProcedure storeProcedure)
        {
            foreach (IParameter parameter in storeProcedure.Parameters)
            {
                if (!parameter.IsChanged)
                {
                    string message = "存储过程{0}中的参数{1}未赋值。";
                    message = string.Format(message, storeProcedure.StoreProcedureName, parameter.ParameterName);
                    throw new SpParametersNullException(message);
                }
            }
        }

        private static IDbCommand GetDbCommand(StoreProcedure storeProcedure, IDbConnection connection, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(connection);

            IDbCommand command = connection.CreateCommand();
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = storeProcedure.StoreProcedureName;
            command.Connection = connection;
            if (transaction != null)
                command.Transaction = transaction;

            foreach (IParameter parameter in storeProcedure.Parameters)
            {
                string parameterName = parameter.ParameterName;
                if (parameterName[0] == '@')
                    parameterName = '@' + parameterName;
                command.Parameters.Add(new SqlParameter(parameterName, parameter.Value));
            }

            return command;
        }

        /// <summary>
        /// 执行SQL语句。<br/>Execue SQL.
        /// </summary>
        /// <param name="storeProcedure"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(StoreProcedure storeProcedure)
        {
            CheckSpParameters(storeProcedure);
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                return ExecuteNonQuery(storeProcedure, connection, null);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 执行SQL语句。<br/>Execue SQL.
        /// </summary>
        /// <param name="storeProcedure"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(StoreProcedure storeProcedure, IDbConnection connection)
        {
            CheckSpParameters(storeProcedure);
            return ExecuteNonQuery(storeProcedure, connection, null);
        }

        /// <summary>
        /// 执行SQL语句。<br/>Execue SQL.
        /// </summary>
        /// <param name="storeProcedure"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(StoreProcedure storeProcedure, IDbTransaction transaction)
        {
            CheckSpParameters(storeProcedure);
            return ExecuteNonQuery(storeProcedure, transaction.Connection, transaction);
        }

        private static int ExecuteNonQuery(StoreProcedure storeProcedure, IDbConnection connection, IDbTransaction transaction)
        {
            IDbCommand command = GetDbCommand(storeProcedure, connection, transaction);
            return command.ExecuteNonQuery();
        }

        /// <summary>
        /// 执行查询，返回结果的第一行第一列单元格的结果。<br/>
        /// Execute query, return the first cell in first row.
        /// </summary>
        /// <returns></returns>
        public object ExecuteScalar(StoreProcedure storeProcedure)
        {
            CheckSpParameters(storeProcedure);
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                return ExecuteScalar(storeProcedure, connection, null);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 执行查询，返回结果的第一行第一列单元格的结果。<br/>
        /// Execute query, return the first cell in first row.
        /// </summary>
        /// <param name="storeProcedure"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public object ExecuteScalar(StoreProcedure storeProcedure, IDbConnection connection)
        {
            CheckSpParameters(storeProcedure);
            return ExecuteScalar(storeProcedure, connection, null);
        }

        /// <summary>
        /// 执行查询，返回结果的第一行第一列单元格的结果。<br/>
        /// Execute query, return the first cell in first row.
        /// </summary>
        /// <param name="storeProcedure"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public object ExecuteScalar(StoreProcedure storeProcedure, IDbTransaction transaction)
        {
            CheckSpParameters(storeProcedure);
            return ExecuteScalar(storeProcedure, transaction.Connection, transaction);
        }

        private static object ExecuteScalar(StoreProcedure storeProcedure, IDbConnection connection, IDbTransaction transaction)
        {
            IDbCommand command = GetDbCommand(storeProcedure, connection, transaction);
            return command.ExecuteScalar();
        }

        /// <summary>
        /// 执行查询返回数据集。<br/>
        /// Execute querying and return data table.
        /// </summary>
        /// <param name="storeProcedure"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public void Fill(StoreProcedure storeProcedure, DataTable dt)
        {
            CheckSpParameters(storeProcedure);
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                Fill(storeProcedure, dt, connection, null);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 执行查询返回数据集。<br/>
        /// Execute querying and return data table.
        /// </summary>
        /// <param name="storeProcedure"></param>
        /// <param name="dt"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public void Fill(StoreProcedure storeProcedure, DataTable dt, IDbConnection connection)
        {
            CheckSpParameters(storeProcedure);
            Fill(storeProcedure, dt, connection, null);
        }

        /// <summary>
        /// 执行查询返回数据集。<br/>
        /// Execute querying and return data table.
        /// </summary>
        /// <param name="storeProcedure"></param>
        /// <param name="dt"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public void Fill(StoreProcedure storeProcedure, DataTable dt, IDbTransaction transaction)
        {
            CheckSpParameters(storeProcedure);
            Fill(storeProcedure, dt, transaction.Connection, transaction);
        }

        private static void Fill(StoreProcedure storeProcedure, DataTable dt, IDbConnection connection, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(dt);
            IDbCommand command = GetDbCommand(storeProcedure, connection, transaction);
            SqlDataAdapter adapter = new SqlDataAdapter(command as SqlCommand);
            adapter.Fill(dt);
        }

        #endregion

        #region ObjectQuery Fill Methods

        /// <summary>
        /// 使用ObjectQuery来填充DataTable对象。<br/>
        /// Fill the DataTable instance from ObjectQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public void Fill(ObjectQuery query, DataTable dt)
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                this.Fill(query, dt, connection);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 使用ObjectQuery来填充DataSet对象。<br/>
        /// Fill the DataSet instance from ObjectQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dst"></param>
        /// <returns></returns>
        public void Fill(ObjectQuery query, DataSet dst)
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                this.Fill(query, dst, connection);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 使用ObjectQuery来填充DataSet中的某个表对象。<br/>
        /// Fill one DataTable instance of DataSet instance from ObjectQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dst"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public void Fill(ObjectQuery query, DataSet dst, string tableName)
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                this.Fill(query, dst, tableName, connection);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 使用ObjectQuery来填充DataTable对象。<br/>
        /// Fill the DataTable instance from ObjectQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dt"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public void Fill(ObjectQuery query, DataTable dt, IDbConnection connection)
        {
            Kit.VerifyNotNull(query);
            Kit.VerifyNotNull(dt);
            Kit.VerifyNotNull(connection);
            SqlCommand command = SqlObjectQueryAnalyzer.CreateCommand(query);
            command.Connection = connection as SqlConnection;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            query.LastExecutedSql = IDbCommandConvertion.ToString(command);
            adapter.Fill(dt);
        }

        /// <summary>
        /// 使用ObjectQuery来填充DataSet对象。<br/>
        /// Fill the DataSet instance from ObjectQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dst"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public void Fill(ObjectQuery query, DataSet dst, IDbConnection connection)
        {
            Kit.VerifyNotNull(query);
            Kit.VerifyNotNull(dst);
            Kit.VerifyNotNull(connection);
            SqlCommand command = SqlObjectQueryAnalyzer.CreateCommand(query);
            command.Connection = connection as SqlConnection;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            query.LastExecutedSql = IDbCommandConvertion.ToString(command);
            adapter.Fill(dst);
        }

        /// <summary>
        /// 使用ObjectQuery来填充DataSet中的某个表对象。<br/>
        /// Fill one DataTable instance of DataSet instance from ObjectQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dst"></param>
        /// <param name="tableName"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public void Fill(ObjectQuery query, DataSet dst, string tableName, IDbConnection connection)
        {
            Kit.VerifyNotNull(query);
            Kit.VerifyNotNull(dst);
            Kit.VerifyNotNull(tableName);
            Kit.VerifyNotNull(connection);
            SqlCommand command = SqlObjectQueryAnalyzer.CreateCommand(query);
            command.Connection = connection as SqlConnection;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            query.LastExecutedSql = IDbCommandConvertion.ToString(command);
            adapter.Fill(dst, tableName);
        }

        /// <summary>
        /// 使用ObjectQuery来填充DataTable对象。<br/>
        /// Fill the DataTable instance from ObjectQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dt"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public void Fill(ObjectQuery query, DataTable dt, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(query);
            Kit.VerifyNotNull(dt);
            Kit.VerifyNotNull(transaction);
            SqlCommand command = SqlObjectQueryAnalyzer.CreateCommand(query);
            command.Connection = transaction.Connection as SqlConnection;
            command.Transaction = transaction as SqlTransaction;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            query.LastExecutedSql = IDbCommandConvertion.ToString(command);
            adapter.Fill(dt);
        }

        /// <summary>
        /// 使用ObjectQuery来填充DataSet对象。<br/>
        /// Fill the DataSet instance from ObjectQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dst"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public void Fill(ObjectQuery query, DataSet dst, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(query);
            Kit.VerifyNotNull(dst);
            Kit.VerifyNotNull(transaction);
            SqlCommand command = SqlObjectQueryAnalyzer.CreateCommand(query);
            command.Connection = transaction.Connection as SqlConnection;
            command.Transaction = transaction as SqlTransaction;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            query.LastExecutedSql = IDbCommandConvertion.ToString(command);
            adapter.Fill(dst);
        }

        /// <summary>
        /// 使用ObjectQuery来填充DataSet中的某个表对象。<br/>
        /// Fill one DataTable instance of DataSet instance from ObjectQuery instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="dst"></param>
        /// <param name="tableName"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public void Fill(ObjectQuery query, DataSet dst, string tableName, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(query);
            Kit.VerifyNotNull(dst);
            Kit.VerifyNotNull(tableName);
            Kit.VerifyNotNull(transaction);
            SqlCommand command = SqlObjectQueryAnalyzer.CreateCommand(query);
            command.Connection = transaction.Connection as SqlConnection;
            command.Transaction = transaction as SqlTransaction;
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            query.LastExecutedSql = IDbCommandConvertion.ToString(command);
            adapter.Fill(dst, tableName);
        }
        #endregion

        #region GetDataReader

        /// <summary>
        /// 获得并返回数据阅读器对象
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public IDataReader GetDataReader(DqlQuery query, IDbConnection connection)
        {
            string strSQL;
            DqlReflector reflector = GetDqlReflector(query, out strSQL);

            SqlConnection sqlConnection = connection as SqlConnection;
            if (connection.State != ConnectionState.Open)
                throw new InvalidOperationException("Connection对象未打开。");

            SqlCommand command;
            if (query.PagingType == PagingType.None)
            {
                command = new SqlCommand(strSQL, sqlConnection);
            }
            else
            {
                command = GetPagingCommand(query, reflector, sqlConnection.ServerVersion);
                command.Connection = sqlConnection;
            }

            query.LastExecutedSql = IDbCommandConvertion.ToString(command);

            return command.ExecuteReader();
        }

        /// <summary>
        /// 获得并返回数据阅读器对象
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public IDataReader GetDataReader(DqlQuery query, IDbTransaction transaction)
        {
            string strSQL;
            DqlReflector reflector = GetDqlReflector(query, out strSQL);

            SqlTransaction sqlTransaction = transaction as SqlTransaction;
            SqlConnection sqlConnection = sqlTransaction.Connection;
            if (sqlConnection.State != ConnectionState.Open)
                throw new InvalidOperationException("Connection对象未打开。");

            SqlCommand command;
            if (query.PagingType == PagingType.None)
            {
                command = new SqlCommand(strSQL, sqlConnection);
                command.Transaction = sqlTransaction;
            }
            else
            {
                command = GetPagingCommand(query, reflector, sqlConnection.ServerVersion);
                command.Connection = sqlConnection;
                command.Transaction = sqlTransaction;
            }

            query.LastExecutedSql = IDbCommandConvertion.ToString(command);
            return command.ExecuteReader();
        }

        /// <summary>
        /// 获得并返回数据阅读器对象
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="connection"></param>
        /// <returns></returns>
        public IDataReader GetDataReader(ObjectQuery query, IDbConnection connection)
        {
            Kit.VerifyNotNull(query);
            Kit.VerifyNotNull(connection);
            SqlCommand command = SqlObjectQueryAnalyzer.CreateCommand(query);
            command.Connection = connection as SqlConnection;
            return command.ExecuteReader();
        }

        /// <summary>
        /// 获得并返回数据阅读器对象
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="query"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public IDataReader GetDataReader(ObjectQuery query, IDbTransaction transaction)
        {
            Kit.VerifyNotNull(query);
            Kit.VerifyNotNull(transaction);
            SqlCommand command = SqlObjectQueryAnalyzer.CreateCommand(query);
            command.Connection = transaction.Connection as SqlConnection;
            command.Transaction = transaction as SqlTransaction;
            return command.ExecuteReader();
        }

        /// <summary>
        /// 获得并返回数据阅读器对象
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="storeProcedure"></param>
        /// <returns></returns>
        public IDataReader GetDataReader(StoreProcedure storeProcedure, IDbConnection connection)
        {
            CheckSpParameters(storeProcedure);
            return GetDataReader(storeProcedure, connection, null);
        }

        /// <summary>
        /// 获得并返回数据阅读器对象
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="transaction"></param>
        /// <param name="storeProcedure"></param>
        /// <returns></returns>
        public IDataReader GetDataReader(StoreProcedure storeProcedure, IDbTransaction transaction)
        {
            CheckSpParameters(storeProcedure);
            return GetDataReader(storeProcedure, transaction.Connection, transaction);
        }

        private static IDataReader GetDataReader(StoreProcedure storeProcedure, IDbConnection connection, IDbTransaction transaction)
        {
            IDbCommand command = GetDbCommand(storeProcedure, connection, transaction);
            return command.ExecuteReader();
        }

        #endregion

        #endregion
    }
}
