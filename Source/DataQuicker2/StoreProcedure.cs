// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
//
// Modified: Alex
// Email: AlexHe.cs@Gmail.com
// Date: 2006-04-08
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Data;
using System.Collections.Generic;
using System.Text;
using DataQuicker2.Framework.Common;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 存储过程的实体抽象基类。<br/>
	/// The abstract class of store procedure.
	/// </summary>
	public abstract class StoreProcedure : IDbObject
	{
		private string storeProcedureName;
		private List<IParameter> parameters = new List<IParameter>();

		/// <summary>
		/// 构造存储过程对象。<br/>Construct store procedure instance.
		/// </summary>
		public StoreProcedure()
		{
			this.Bind();
		}

		/// <summary>
		/// 取得当前存储过程的参数集。<br/>
		/// Get parameter collection of current store procedure.
		/// </summary>
		public List<IParameter> Parameters
		{
			get
			{
				return this.parameters;
			}
		}

		#region IDbObject Members

		/// <summary>
		/// 取得存储过程名称。<br/>
		/// Get store procedure name.
		/// </summary>
		public string StoreProcedureName
		{
			get
			{
				return this.storeProcedureName;
			}
			protected internal set
			{
				this.storeProcedureName = value;
			}
		}

		string IDbObject.DbObjectName
		{
			get 
			{
				return this.StoreProcedureName;
			}
		}

		DbObjectType IDbObject.DbObjectType
		{
			get 
			{
				return DbObjectType.StoreProcedure;
			}
		}

		bool IDbObject.IsChanged
		{
			get
			{
				return true;
			}
			set
			{
				
			}
		}

		#endregion

		/// <summary>
		/// 执行SQL语句。<br/>Execue SQL.
		/// </summary>
		/// <returns></returns>
		public int ExecuteNonQuery()
		{
			IDbConnection connection = DbFactory.CreateConnection();
			IProvider provider = DbFactory.CreateProvider();
			connection.Open();
			try
			{
				return provider.ExecuteNonQuery(this, connection);
			}
			finally
			{
				connection.Close();
			}
		}

		/// <summary>
		/// 执行SQL语句。<br/>Execue SQL.
		/// </summary>
		/// <param name="connection"></param>
		/// <returns></returns>
		public int ExecuteNonQuery(IDbConnection connection)
		{
			Kit.VerifyNotNull(connection);
			IProvider provider = DbFactory.CreateProvider(connection);
			return provider.ExecuteNonQuery(this, connection);
		}

		/// <summary>
		/// 执行SQL语句。<br/>Execue SQL.
		/// </summary>
		/// <param name="transaction"></param>
		/// <returns></returns>
		public int ExecuteNonQuery(IDbTransaction transaction)
		{
			Kit.VerifyNotNull(transaction);
			Kit.VerifyNotNull(transaction.Connection);
			IProvider provider = DbFactory.CreateProvider(transaction.Connection);
			return provider.ExecuteNonQuery(this, transaction);
		}

		/// <summary>
		/// 执行查询，返回结果的第一行第一列单元格的结果。<br/>
		/// Execute query, return the first cell in first row.
		/// </summary>
		/// <returns></returns>
		public object ExecuteScalar()
		{
			IDbConnection connection = DbFactory.CreateConnection();
			IProvider provider = DbFactory.CreateProvider();
			connection.Open();
			try
			{
				return provider.ExecuteScalar(this);
			}
			finally
			{
				connection.Close();
			}
		}

		/// <summary>
		/// 执行查询，返回结果的第一行第一列单元格的结果。<br/>
		/// Execute query, return the first cell in first row.
		/// </summary>
		/// <param name="connection"></param>
		/// <returns></returns>
		public object ExecuteScalar(IDbConnection connection)
		{
			Kit.VerifyNotNull(connection);
			IProvider provider = DbFactory.CreateProvider(connection);
			return provider.ExecuteScalar(this, connection);
		}

		/// <summary>
		/// 执行查询，返回结果的第一行第一列单元格的结果。<br/>
		/// Execute query, return the first cell in first row.
		/// </summary>
		/// <param name="transaction"></param>
		/// <returns></returns>
		public object ExecuteScalar(IDbTransaction transaction)
		{
			Kit.VerifyNotNull(transaction);
			Kit.VerifyNotNull(transaction.Connection);
			IProvider provider = DbFactory.CreateProvider(transaction.Connection);
			return provider.ExecuteScalar(this, transaction);
		}

		/// <summary>
		/// 执行查询返回数据集。<br/>
		/// Execute querying and return data table.
		/// </summary>
		/// <param name="dt"></param>
		/// <returns></returns>
		public void Fill(DataTable dt)
		{
			Kit.VerifyNotNull(dt);
			IDbConnection connection = DbFactory.CreateConnection();
			IProvider provider = DbFactory.CreateProvider();
			connection.Open();
			try
			{
				provider.Fill(this, dt, connection);
			}
			finally
			{
				connection.Close();
			}
		}

		/// <summary>
		/// 执行查询返回数据集。<br/>
		/// Execute querying and return data table.
		/// </summary>
		/// <param name="dt"></param>
		/// <param name="connection"></param>
		/// <returns></returns>
		public void Fill(DataTable dt, IDbConnection connection)
		{
			Kit.VerifyNotNull(dt);
			Kit.VerifyNotNull(connection);
			IProvider provider = DbFactory.CreateProvider(connection);
			provider.Fill(this, dt, connection);
		}

		/// <summary>
		/// 执行查询返回数据集。<br/>
		/// Execute querying and return data table.
		/// </summary>
		/// <param name="dt"></param>
		/// <param name="transaction"></param>
		/// <returns></returns>
		public void Fill(DataTable dt, IDbTransaction transaction)
		{
			Kit.VerifyNotNull(dt);
			Kit.VerifyNotNull(transaction);
			Kit.VerifyNotNull(transaction.Connection);
			IProvider provider = DbFactory.CreateProvider(transaction.Connection);
			provider.Fill(this, dt, transaction);
		}

        /// <summary>
        /// 填充阅读器对象并返回。
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        public IDataReader GetDataReader(IDbConnection connection)
        {
            Kit.VerifyNotNull(connection);
            IProvider provider = DbFactory.CreateProvider(connection);
            return provider.GetDataReader(this, connection);
        }


        /// <summary>
        /// 填充阅读器对象并返回。
        /// Fill and return DataReader instance.
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public IDataReader GetDataReader(IDbTransaction transaction)
        {
            Kit.VerifyNotNull(transaction);
            Kit.VerifyNotNull(transaction.Connection);
            IProvider provider = DbFactory.CreateProvider(transaction.Connection);
            return provider.GetDataReader(this, transaction);
        }

		/// <summary>
		/// 负责绑定存储过程参数和存储过程名。<br/>
		/// Bind store procedure and parameters.
		/// </summary>
		protected abstract void Bind();

		/// <summary>
		/// 绑定存储过程。<br/>
		/// Bind store procedure.
		/// </summary>
		/// <param name="storeProcedureName"></param>
		protected void BindStoreParameterName(string storeProcedureName)
		{
			this.storeProcedureName = storeProcedureName;
		}

		/// <summary>
		/// 绑定存储过程参数对象与参数名。<br/>
		/// Bind store parameter to parameter name.
		/// </summary>
		/// <param name="parameter"></param>
		/// <param name="parameterName"></param>
		protected void BindParameter(IParameter parameter, string parameterName)
		{
			parameter.ParameterName = parameterName;
			this.parameters.Add(parameter);
		}

		/// <summary>
		/// 绑定存储过程参数对象与参数名。<br/>
		/// Bind store parameter to parameter name.
		/// </summary>
		/// <param name="parameter"></param>
		/// <param name="parameterName"></param>
		/// <param name="maxLength"></param>
		protected void BindParameter(IParameter parameter, string parameterName, int maxLength)
		{
			parameter.ParameterName = parameterName;
			parameter.MaxLength = maxLength;
			this.parameters.Add(parameter);
		}
	}
}
