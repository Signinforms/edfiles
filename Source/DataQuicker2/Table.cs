// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
//
// Modified: Alex
// Email: AlexHe.cs@Gmail.com
// Date: 2006-04-04
//  
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using DataQuicker2.Framework.Common;

namespace DataQuicker2.Framework
{
    /// <summary>
    /// 数据表抽象类（数据库中的数据表），实现本类的实体类可以进行实例化、更新、删除、新增数据等操作。<br/>
    /// Table abstract class, stands for the data table. The classes inherited from current class can do initialization, update, delete and insert.
    /// </summary>
    public abstract class Table : IDbObject, IEntity, IPersist
    {
        private string tableName;
        private bool isChanged;
        private bool isExist;
        private List<IColumn> columns;
        private bool cancelManageAutoValue;
        private IColumn primaryKey;

        #region IDbObject Members

        string IDbObject.DbObjectName
        {
            get
            {
                return this.TableName;
            }
        }

        /// <summary>
        /// 取得映射的数据表名。<br/>
        /// Get data table name.
        /// </summary>
        public string TableName
        {
            get
            {
                return this.tableName;
            }
            protected internal set
            {
                this.tableName = value;
            }
        }

        /// <summary>
        /// 取得当前表对象中的列值是否已改变。<br/>
        /// Check whether the value of current column has been changed.
        /// </summary>
        public bool IsChanged
        {
            get
            {
                return this.isChanged;
            }
            protected internal set
            {
                this.isChanged = value;
            }
        }

        bool IDbObject.IsChanged
        {
            get
            {
                return this.IsChanged;
            }
            set
            {
                this.IsChanged = value;
            }
        }

        DbObjectType IDbObject.DbObjectType
        {
            get
            {
                return DbObjectType.Table;
            }
        }

        #endregion

        /// <summary>
        /// 取得当前表的主键。<br/>
        /// Get the primary key of current instance. 
        /// </summary>
        public IColumn PrimaryKey
        {
            get
            {
                return this.primaryKey;
            }
            protected internal set
            {
                this.primaryKey = value;
            }
        }

        /// <summary>
        /// 构造<see cref="Table"/>对象。<br/>
        /// Construct <see cref="Table"/> instance.
        /// </summary>
        protected Table()
        {
            this.columns = new List<IColumn>();
            this.Bind();
        }

        /// <summary>
        /// 构造<see cref="Table"/>对象，并根据主键值进行实例化。<br/>
        /// Construct Table instance, initialize instance based on primary key value.
        /// </summary>
        /// <param name="primaryKey"></param>
        protected Table(object primaryKey)
            : this()
        {
            this.primaryKey.Value = primaryKey;
            this.Load();
        }

        /// <summary>
        /// 构造<see cref="Table"/>对象，并根据主键值进行实例化。<br/>
        /// Construct Table instance, initialize instance based on primary key value.
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <param name="connection"></param>
        protected Table(object primaryKey, IDbConnection connection)
            : this()
        {
            this.primaryKey.Value = primaryKey;
            this.Load(connection);
        }

        /// <summary>
        /// 构造<see cref="Table"/>对象，并根据主键值进行实例化。<br/>
        /// Construct Table instance, initialize instance based on primary key value.
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <param name="transaction"></param>
        protected Table(object primaryKey, IDbTransaction transaction)
            : this()
        {
            this.primaryKey.Value = primaryKey;
            this.Load(transaction);
        }

        /// <summary>
        /// 实现对象与数据表之间的关联，绑定的实现由实体类进行。<br/>
        /// Bind the instance to database table. It should be implemented by child class.
        /// </summary>
        protected abstract void Bind();

        /// <summary>
        /// 将当前表对象映射到数据表上。<br/>
        /// Map current instance to data table name.
        /// </summary>
        /// <param name="tableName"></param>
        protected void BindTableName(string tableName)
        {
            this.tableName = tableName;
        }

        /// <summary>
        /// 设置当前表的主键列对象，默认值类型为非DataQuicker2托管。<br/>
        /// Set the primary key column instance. The default value type is unmanaged of DataQuicker2.
        /// </summary>
        /// <param name="primaryKey"></param>
        protected void BindPrimaryKey(IColumn primaryKey)
        {
            this.BindPrimaryKey(primaryKey, ColumnValueType.Unmanaged);
        }

        /// <summary>
        /// 设置当前表的主键列对象。<br/>
        /// Set the primary key column instance.
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <param name="valueType">
        /// 列值类型。<br/>
        /// Column value type.
        /// </param>
        protected void BindPrimaryKey(IColumn primaryKey, ColumnValueType valueType)
        {
            if (this.primaryKey != null)
            {
                string message = "DataQuicker2框架不支持单表中包括多个主键。实体设计错误：{0}";
                message = string.Format(message, this.GetType().FullName);
                throw new MultiPrimaryKeyException(message);
            }

            this.primaryKey = primaryKey;
            this.primaryKey.ColumnValueType = valueType;
        }

        /// <summary>
        /// 将字段对象映射到数据表中实际的列上。<br/>
        /// Mapping column instance to the physical column of data table.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="columnName"></param>
        protected void BindColumn(IColumn column, string columnName)
        {
            this.BindColumn(column, columnName, 0);
        }

        /// <summary>
        /// 将字段对象映射到数据视图实际的列上。<br/>
        /// Mapping column instance to the physical column of data view.
        /// </summary>
        /// <param name="column"></param>
        /// <param name="columnName"></param>
        /// <param name="maxLength">
        /// 列值的最大长度，对于字符串型的列才有效。<br/>
        /// The maximum length of column value, it's useful for string type column.
        /// </param>
        protected void BindColumn(IColumn column, string columnName, int maxLength)
        {
            column.ColumnName = columnName;
            column.ParentEntity = this;
            column.MaxLength = maxLength;
            this.columns.Add(column);
        }

        /// <summary>
        /// 在当前实体对象上创建SqlQuery对象，执行查询。<br/>
        /// Create SqlQuery instance on current entity, to execute querying.
        /// </summary>
        /// <param name="dqlName"></param>
        /// <returns></returns>
        public DqlQuery CreateQuery(string dqlName)
        {
            Kit.VerifyNotNull(dqlName);
            return DqlQueryFactory.CreateQuery(this, dqlName);
        }

        /// <summary>
        /// 在当前实体对象上创建ObjectQuery对象，执行查询。<br/>
        /// Create ObjectQuery instance on current entity, to execute querying.
        /// </summary>
        /// <returns></returns>
        public ObjectQuery CreateQuery()
        {
            return new ObjectQuery(this);
        }

        #region IEntity Members

        /// <summary>
        /// 当前对象是否实例化成功。<br/>
        /// Get true when current instance exists.
        /// </summary>
        public bool IsExist
        {
            get
            {
                return this.isExist;
            }
            protected internal set
            {
                this.isExist = value;
            }
        }

        bool IEntity.IsExist
        {
            get
            {
                return this.IsExist;
            }
            set
            {
                this.IsExist = value;
            }
        }

        #region InitObject
        /// <summary>
        /// 通过在当前对象列上的赋值初始化当前对象。<br/>
        /// Init current instance by the set value on columns of current instance.
        /// </summary>
        public void InitObject()
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                this.InitObject(connection);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 通过在当前对象列上的赋值初始化当前对象。<br/>
        /// Init current instance by the set value on columns of current instance.
        /// </summary>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        public void InitObject(IDbConnection connection)
        {
            Kit.VerifyNotNull(connection);
            IProvider provider = DbFactory.CreateProvider(connection);
            provider.InitObject(this, connection);
        }

        /// <summary>
        /// 通过在当前对象列上的赋值初始化当前对象。<br/>
        /// Init current instance by the set value on columns of current instance.
        /// </summary>
        /// <param name="trans">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        public void InitObject(IDbTransaction trans)
        {
            Kit.VerifyNotNull(trans);
            IProvider provider = DbFactory.CreateProvider(trans.Connection);
            provider.InitObject(this, trans);
        }

        #endregion

        #region GetRecordsCount
        /// <summary>
        /// 通过在当前对象列上的赋值获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        public int GetRecordsCount()
        {
            int count = 0;
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                count = this.GetRecordsCount(connection);
            }
            finally
            {
                connection.Close();
            }
            return count;
        }

        /// <summary>
        /// 通过在当前对象列上的赋值获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        public int GetRecordsCount(IDbConnection connection)
        {
            Kit.VerifyNotNull(connection);
            IProvider provider = DbFactory.CreateProvider(connection);
            return provider.GetRecordsCount(this, connection);
        }

        /// <summary>
        /// 通过在当前对象列上的赋值获得当前记录数。<br/>
        /// Get current RecordsCount by the set value on columns of current instance.
        /// </summary>
        /// <param name="trans">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        public int GetRecordsCount(IDbTransaction trans)
        {
            Kit.VerifyNotNull(trans);
            IProvider provider = DbFactory.CreateProvider(trans.Connection);
            return provider.GetRecordsCount(this, trans);
        }

        #endregion

        /// <summary>
        /// 取得当前实体包含的所有列对象。<br/>
        /// Get all contained column instances.
        /// </summary>
        public List<IColumn> Columns
        {
            get
            {
                return columns;
            }
        }

        /// <summary>
        /// 重置当前实体。<br/>
        /// Reset current instance.
        /// </summary>
        public void Reset()
        {
            this.isExist = false;
            this.isChanged = false;
            foreach (IColumn column in this.columns)
            {
                column.Value = null;
                column.IsChanged = false;
            }
        }

        #endregion

        #region IPersist Members

        /// <summary>
        /// 将当前对象实例化到数据库中。<br/>
        /// Persist the current instance to database.
        /// </summary>
        public void Create()
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                this.Create(connection);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 将当前对象实例化到数据库中。<br/>
        /// Persist the current instance to database.
        /// </summary>
        /// <param name="trans">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        public void Create(IDbTransaction trans)
        {
            Kit.VerifyNotNull(trans);
            IProvider provider = DbFactory.CreateProvider(trans.Connection);
            provider.Create(this, trans);
        }

        /// <summary>
        /// 将当前对象实例化到数据库中。<br/>
        /// Persist the current instance to database.
        /// </summary>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        public void Create(IDbConnection connection)
        {
            Kit.VerifyNotNull(connection);
            IProvider provider = DbFactory.CreateProvider(connection);
            provider.Create(this, connection);
        }

        /// <summary>
        /// 将当前对象更新到数据库中。<br/>
        /// Update the current instance to database.
        /// </summary>
        public void Update()
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                this.Update(connection);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 将当前对象更新到数据库中。<br/>
        /// Update the current instance to database.
        /// </summary>
        /// <param name="trans">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        public void Update(IDbTransaction trans)
        {
            Kit.VerifyNotNull(trans);
            IProvider provider = DbFactory.CreateProvider(trans.Connection);
            provider.Update(this, trans);
        }

        /// <summary>
        /// 将当前对象更新到数据库中。<br/>
        /// Update the current instance to database.
        /// </summary>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        public void Update(IDbConnection connection)
        {
            Kit.VerifyNotNull(connection);
            IProvider provider = DbFactory.CreateProvider(connection);
            provider.Update(this, connection);
        }

        /// <summary>
        /// 从数据库中删除当前对象。<br/>
        /// Delete current instance from database.
        /// </summary>
        public void Delete()
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                this.Delete(connection);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 从数据库中删除当前对象。<br/>
        /// Delete current instance from database.
        /// </summary>
        /// <param name="trans">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        public void Delete(IDbTransaction trans)
        {
            Kit.VerifyNotNull(trans);
            IProvider provider = DbFactory.CreateProvider(trans.Connection);
            provider.Delete(this, trans);
        }

        /// <summary>
        /// 从数据库中删除当前对象。<br/>
        /// Delete current instance from database.
        /// </summary>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        public void Delete(IDbConnection connection)
        {
            Kit.VerifyNotNull(connection);
            IProvider provider = DbFactory.CreateProvider(connection);
            provider.Delete(this, connection);
        }

        #endregion

        /// <summary>
        /// 根据实体的主键值来实例化当前对象。<br/>
        /// Load current instance based on primary key value.
        /// </summary>
        public void Load()
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            try
            {
                this.Load(connection);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>
        /// 根据实体的主键值来实例化当前对象。<br/>
        /// Load current instance based on primary key value.
        /// </summary>
        /// <param name="connection">
        /// IDbConnection对象必须已打开。<br/>
        /// IDbConnection instance must be opened.
        /// </param>
        public void Load(IDbConnection connection)
        {
            Kit.VerifyNotNull(connection);
            IProvider provider = DbFactory.CreateProvider(connection);
            provider.Load(this, connection);
        }

        /// <summary>
        /// 根据实体的主键值来实例化当前对象。<br/>
        /// Load current instance based on primary key value.
        /// </summary>
        /// <param name="trans">
        /// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
        /// Transaction instance, it associated IDbConnection instance must be opened.
        /// </param>
        public void Load(IDbTransaction trans)
        {
            Kit.VerifyNotNull(trans);
            IProvider provider = DbFactory.CreateProvider(trans.Connection);
            provider.Load(this, trans);
        }

        /// <summary>
        /// 设置/取得是否取消DataQuicker2托管的自增长字段。
        /// </summary>
        public bool CancelManageAutoValue
        {
            get
            {
                return this.cancelManageAutoValue;
            }
            set
            {
                this.cancelManageAutoValue = value;
            }
        }
    }
}