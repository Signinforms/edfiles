// Project Name: DataQuicker2 Framework (O/R Mapping)
// Author: Eunge Liu
// Licensor: 5VNET.COM Studio
// Company: http://dev.5vnet.com
// Email: eunge.liu@gmail.com
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using System;
using System.Data;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using DataQuicker2.Framework.Common;

namespace DataQuicker2.Framework
{
	/// <summary>
	/// 视图实体层的抽象类，所有的视图实体都继承于本类。<br/>
	/// The abstract class of all view entities. 
	/// </summary>
	public abstract class View : IDbObject, IEntity
	{
		private string viewName;
		private bool isExist;
		private List<IColumn> columns;

		/// <summary>
		/// 构造<see cref="View"/>对象。<br/>
		/// Construct <see cref="View"/> instance.
		/// </summary>
		public View()
		{
			this.columns = new List<IColumn>();
			this.Bind();
		}

		#region IDbObject Members

		string IDbObject.DbObjectName
		{
			get 
			{
				return this.ViewName;
			}
		}

		/// <summary>
		/// 取得当前视图对象映射的视图名。<br/>
		/// Get the data view name.
		/// </summary>
		public string ViewName
		{
			get
			{
				return this.viewName;
			}
			protected internal set
			{
				this.viewName = value;
			}
		}

		DbObjectType IDbObject.DbObjectType
		{
			get
			{
				return DbObjectType.View;
			}
		}

		bool IDbObject.IsChanged
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		#endregion

		/// <summary>
		/// 将字段对象映射到数据视图实际的列上。<br/>
		/// Mapping column instance to the physical column of data view.
		/// </summary>
		/// <param name="column"></param>
		/// <param name="columnName"></param>
		protected void BindColumn(IColumn column, string columnName)
		{
			this.BindColumn(column, columnName, 0);
		}

		/// <summary>
		/// 将字段对象映射到数据视图实际的列上。<br/>
		/// Mapping column instance to the physical column of data view.
		/// </summary>
		/// <param name="column"></param>
		/// <param name="columnName"></param>
		/// <param name="maxLength">
		/// 列值的最大长度，对于字符串型的列才有效。<br/>
		/// The maximum length of column value, it's useful for string type column.
		/// </param>
		protected void BindColumn(IColumn column, string columnName, int maxLength)
		{
			column.ColumnName = columnName;
			column.MaxLength = maxLength;
			this.columns.Add(column);
			column.ParentEntity = this;
		}

		/// <summary>
		/// 绑定字段映射和视图关联，绑定的实现由视图实体类进行。<br/>
		/// Bind the instance to database view. It should be implemented by child class.
		/// </summary>
		protected abstract void Bind();

		/// <summary>
		/// 将当前视图对象映射到数据库视图上。<br/>
		/// Map current instance to data view name.
		/// </summary>
		/// <param name="viewName"></param>
		protected void BindViewName(string viewName)
		{
			this.viewName = viewName;
		}

		/// <summary>
		/// 在当前实体对象上创建SqlQuery对象，执行查询。<br/>
		/// Create SqlQuery instance on current entity, to execute querying.
		/// </summary>
		/// <param name="dqlName"></param>
		/// <returns></returns>
		public DqlQuery CreateQuery(string dqlName)
		{
			Kit.VerifyNotNull(dqlName);
			return DqlQueryFactory.CreateQuery(this, dqlName);
		}

		/// <summary>
		/// 在当前实体对象上创建ObjectQuery对象，执行查询。<br/>
		/// Create ObjectQuery instance on current entity, to execute querying.
		/// </summary>
		/// <returns></returns>
		public ObjectQuery CreateQuery()
		{
			return new ObjectQuery(this);
		}

		#region IEntity Members

		/// <summary>
		/// 当前对象是否实例化成功。<br/>
		/// Get true when current instance exists.
		/// </summary>
		public bool IsExist
		{
			get
			{
				return this.isExist;
			}
			protected internal set
			{
				this.isExist = value;
			}
		}

		bool IEntity.IsExist
		{
			get
			{
				return this.IsExist;
			}
			set
			{
				this.IsExist = value;
			}
		}

		/// <summary>
		/// 通过在当前对象列上的赋值初始化当前对象。<br/>
		/// Init current instance by the set value on columns of current instance.
		/// </summary>
		public void InitObject()
		{
			IDbConnection connection = DbFactory.CreateConnection();
			connection.Open();
			try
			{
				this.InitObject(connection);
			}
			finally
			{
				connection.Close();
			}
		}

		/// <summary>
		/// 通过在当前对象列上的赋值初始化当前对象。<br/>
		/// Init current instance by the set value on columns of current instance.
		/// </summary>
		/// <param name="connection">
		/// IDbConnection对象必须已打开。<br/>
		/// IDbConnection instance must be opened.
		/// </param>
		public void InitObject(IDbConnection connection)
		{
			Kit.VerifyNotNull(connection);
			IProvider provider = DbFactory.CreateProvider(connection);
			provider.InitObject(this, connection);
		}

		/// <summary>
		/// 通过在当前对象列上的赋值初始化当前对象。<br/>
		/// Init current instance by the set value on columns of current instance.
		/// </summary>
		/// <param name="trans">
		/// 事务对象，其关联的IDbConnection对象必须已打开。<br/>
		/// Transaction instance, it associated IDbConnection instance must be opened.
		/// </param>
		public void InitObject(IDbTransaction trans)
		{
			Kit.VerifyNotNull(trans);
			IProvider provider = DbFactory.CreateProvider(trans.Connection);
			provider.InitObject(this, trans);
		}

		List<IColumn> IEntity.Columns
		{
			get
			{
				return columns;
			}
		}

		/// <summary>
		/// 重置当前实体。<br/>
		/// Reset current instance.
		/// </summary>
		public void Reset()
		{
			this.isExist = false;
			foreach (IColumn column in this.columns)
			{
				column.Value = null;
				column.IsChanged = false;
			}
		}

		#endregion
	}
}