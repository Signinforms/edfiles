﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.SqlClient;
using Shinetech.DAL;
using System.Reflection;
using System.Globalization;
using System.Web;
using Shinetech.Utility;
using Elasticsearch.Net;
using Nest;
using EdWindowService;

/// <summary>
/// Summary description for Common_Tatva
/// </summary>
public static class Common_Tatva
{
    #region Properties

    public static readonly string Region = ConfigurationSettings.AppSettings["RackSpaceRegion"];

    public static string RackSpaceDownloadDirectory = ConfigurationSettings.AppSettings["RackSpaceServerFolder"];

    public static string UploadFolder = ConfigurationSettings.AppSettings["UploadFolder"];

    public static string UserLogoFolder = ConfigurationSettings.AppSettings["UserLogoFolder"];
    public static readonly string WEBSITE = ConfigurationSettings.AppSettings["WEBSITE"];

    #endregion


    #region Elastic Properties

    public static string hostString = ConfigurationSettings.AppSettings.Get("ElasticHostString");
    public static string elasticUserName = ConfigurationSettings.AppSettings.Get("ElasticUserName");
    public static string elasticUserSecret = ConfigurationSettings.AppSettings.Get("ElasticUserSecret");
    public static string elasticIndexName = ConfigurationSettings.AppSettings.Get("ElasticDocIndices");
    public static Uri[] hosts = hostString.Split(',').Select(x => new Uri(x)).ToArray();
    public static StaticConnectionPool connectionPool = new StaticConnectionPool(hosts);
    public static ConnectionSettings settings = new ConnectionSettings(connectionPool)
                                                                         .DefaultMappingFor<DocumentIndex>(i => i
                                                                         .IndexName(elasticIndexName))
                                                                         .BasicAuthentication(elasticUserName, elasticUserSecret).DisableDirectStreaming();
    public static ElasticClient highClient = new ElasticClient(settings);

    #endregion

    #region General Methods

    public static void WriteErrorLog(string officeId, string Uid, string fileName, Exception ex)
    {
        try
        {
            string path = Assembly.GetExecutingAssembly().Location;
            string curPath = path.Substring(0, path.LastIndexOf("\\")) + "\\Logs"; ;
            if (!Directory.Exists(curPath))
                Directory.CreateDirectory(curPath);

            StreamWriter swData2 = new StreamWriter(curPath + "\\ServiceLog.txt", true);
            swData2.WriteLine("=====Unhandled Exception occured at  : " + DateTime.Now);
            swData2.WriteLine("OfficeId = " + officeId);
            swData2.WriteLine("UID = " + Uid);
            swData2.WriteLine("fileName = " + fileName);
            swData2.WriteLine("Error = " + ex.Message);
            swData2.WriteLine(ex.StackTrace);
            swData2.WriteLine(ex.Source);
            swData2.WriteLine(ex.InnerException);
            swData2.WriteLine("====");
            swData2.Close();
            swData2.Dispose();
        }
        catch (Exception ex1)
        {
            string errorMessage = ex1.Message;
        }
    }

    #endregion

    #region Send Mail

    public static void SendMailForFolderReminder(string email, string userName, string folderName, string reminderName, DateTime reminderDate, int folderId)
    {
        string strWebLink = "<a href = ' " + WEBSITE + "'";
        strWebLink += " target='_blank'>www.edfiles.com</a>";

        string strMailTemplet = GetMailTempletReminders();
        strMailTemplet = strMailTemplet.Replace("[[ToName]]", userName);
        strMailTemplet = strMailTemplet.Replace("[[WebLink]]", strWebLink);
        strMailTemplet = strMailTemplet.Replace("[[Logo]]", GetLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[CasboLogo]]", GetCasboLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[ReminderName]]", reminderName);

        string strELink = "<a href = ' " + WEBSITE + "/Office/FileFolderBox.aspx?id=" + folderId + "'";
        strELink += " target='_blank'>" + folderName + "</a>";

        strMailTemplet = strMailTemplet.Replace("[[EdFiles]]", strELink);

        string strEmailSubject = "An Alert For Folder Reminders - EdFiles";

        Email.SendFromEFileFolder(email, strEmailSubject, strMailTemplet);
    }

    private static string GetMailTempletReminders()
    {

        string strMailTemplet = string.Empty;
        string mailTempletUrl = Assembly.GetExecutingAssembly().Location;
        mailTempletUrl = mailTempletUrl.Substring(0, mailTempletUrl.LastIndexOf("\\")) + "\\Mail\\MailFolderReminder.html";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    private static string GetLogoPath()
    {
        try
        {
            string filePath = Assembly.GetExecutingAssembly().Location;
            filePath = filePath.Substring(0, filePath.LastIndexOf("\\")) + "\\New\\images\\edFile_logo.png";
            return filePath;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
    }

    private static string GetCasboLogoPath()
    {
        try
        {
            string filePath = Assembly.GetExecutingAssembly().Location;
            filePath = filePath.Substring(0, filePath.LastIndexOf("\\")) + "\\New\\images\\casbo-logo.png";
            return filePath;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
    }

    /// <summary>
    /// Creates the document.
    /// </summary>
    /// <param name="document">The document.</param>
    /// <returns></returns>
    public static bool CreateDocument(DocumentIndex document)
    {
        try
        {
            return highClient.Index(document, idx => idx.Index(elasticIndexName).Id(document.DocumentId)).IsValid;
        }
        catch (Exception ex) { return false; }
    }

    #endregion


}