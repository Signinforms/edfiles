﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EdWindowService
{
    public class DocumentIndex
    {
        public int DocumentId { get; set; }
        public int FolderId { get; set; }
        public int DividerId { get; set; }
        public float Size { get; set; }
        public object JsonData { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public bool IsDeleted { get; set; }
        public string DividerName { get; set; }
        public string DisplayName { get; set; }
    }
}
