﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;

/// <summary>
/// Summary description for Enum_Tatva
/// </summary>
public class Enum_Tatva
{
    public Enum_Tatva()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public enum FileMove
    {
        FileExist = 0,
        FileMoveSuccess = 1,
        FileMoveError = 2
    }

    public enum WorkAreaType
    {
        Upload = 1,
        SMTPUpload = 2,
        RenameUpload = 3,
        SplitUpload = 4,
        UtilityUpload = 5
    }

    public enum Action
    {
        Create = 1,
        Delete = 2,
        Restore = 3,
        Move = 4
    }

    public enum Class
    {
        Class1 = 1,
        Class2 = 2,
        Class3 = 3
    }

    public enum IsDelete
    {
        No = 0,
        Yes = 1
    }

    /// <summary>
    /// To use eFileFlowShare
    /// </summary>
    public enum Status
    {
        Send = 1,
        Saved = 2,
        Submitted = 3,
        Moved = 4,
        Rejected = 5
    }

    public enum Folders
    {
        [Description("eFileFlow")]
        EFileFlow = 1,
        [Description("eFileShare")]
        EFileShare = 2,
        [Description("inbox")]
        inbox = 3,
        [Description("workArea")]
        WorkArea = 4,
        [Description("fileFolder")]
        FileFolder = 5,
        [Description("viewer")]
        ViewerDownload = 6,
        [Description("eFileSplit")]
        EFileSplit = 7
    }

    public static string GetEnumDescription(Enum value)
    {
        FieldInfo fi = value.GetType().GetField(value.ToString());

        DescriptionAttribute[] attributes =
            (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

        if (attributes != null && attributes.Length > 0)
            return attributes[0].Description;
        else
            return value.ToString();
    }

    public enum WeekDays
    {
        Sunday,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        AllDays
    }

    public enum DocumentsAndFoldersLogs
    {
        Share = 1,
        Edit = 2,
        View = 3,
        Preview = 4,
        Delete = 5,
        Rename = 6,
        DocIndex = 7,
        Download = 8,
        BookView = 9,
        Create = 10,
        EditReminder = 11,
        Upload = 12,
        Move = 13
    }

    public enum DocumentsAndFoldersLogsSubAction
    {
        FileNotFound = 1
    }

    public enum TypeOfTable
    {
        OtherTable = 1
    }

    public enum CopyFoldersFromOneUserToAnother
    {
        Move = 1
    }

    public enum OcrStatus
    {
        NoOcr = 0,
        OcrRequested = 1,
        OcrDone = 2
    }
}