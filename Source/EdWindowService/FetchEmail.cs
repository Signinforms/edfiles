﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Configuration;
using OpenPop.Mime;
using OpenPop.Pop3;
using Shinetech.DAL;
using System.IO;
using System.Threading;
using System.Collections.Concurrent;
using System.Data;
using Shinetech.Utility;
using Account = Shinetech.DAL.Account;
using System.Web;

namespace EdWindowService
{
    public partial class FetchEmail : ServiceBase
    {
        #region Properties
        /// <summary>
        /// The _email service
        /// </summary>
        Timer autoFetchEmailTimer;
        Timer autoReminderTimer;
        Timer autoOcrTimer;
        protected bool errorOccured = false;
        protected string hostName = Email.GetMailServer();
        protected int port = Convert.ToInt32(ConfigurationSettings.AppSettings["https_port"]);
        protected bool useSsl = Convert.ToBoolean(ConfigurationSettings.AppSettings["https_useSsl"]);
        protected string scheduledTime = ConfigurationSettings.AppSettings["ScheduledTime"].ToString();
        protected string driveName = ConfigurationSettings.AppSettings["DriveName"].ToString();
        protected string volumePath = ConfigurationSettings.AppSettings["VolumePath"].ToString();
        SimpleScheduler scheduler = new SimpleScheduler();
        #endregion

        #region ctor
        public FetchEmail()
        {
            InitializeComponent();
            eventLog1 = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("MySource"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "MySource", "MyNewLog");
            }
            eventLog1.Source = "MySource";
            eventLog1.Log = "MyNewLog";
        }
        #endregion

        protected override void OnStart(string[] args)
        {
            eventLog1.WriteEntry("In OnStart");
            Start();
        }

        protected override void OnStop()
        {
            eventLog1.WriteEntry("In onStop.");
        }

        /// <summary>
        /// Start Function
        /// </summary>
        public void Start()
        {
            //GetMailBox();
            //GetFolderReminders();
            //UpdateDriveSizeLogs();
            //ConvertPdfToSearchable();
            Common_Tatva.WriteErrorLog("Entered In Start() function of FetchEmail.cs", " at -----" + DateTime.Now.ToString(), "", new Exception());
            if (Directory.Exists(driveName))
            {
                autoFetchEmailTimer = new Timer(AutoFetchEmailTimer_Elapsed, null, 0, 300000);
                autoReminderTimer = new Timer(AutoReminderTimer_Elapsed, null, 0, 60000);
                autoOcrTimer = new Timer(AutoOcrTimer_Elapsed, null, 0, 30000);
            }
            else
                Common_Tatva.WriteErrorLog("S: drive not found", " at -----" + DateTime.Now.ToString(), "", new Exception());
            Common_Tatva.WriteErrorLog("Ended In Start() function of FetchEmail.cs", " at -----" + DateTime.Now.ToString(), "", new Exception());
        }

        #region Timers

        private void AutoFetchEmailTimer_Elapsed(object state)
        {

            Common_Tatva.WriteErrorLog("Entered In AutoFetchEmailTimer_Elapsed() function of FetchEmail.cs", " at -----" + DateTime.Now.ToString(), "", new Exception());
            GetMailBox();
        }

        private void AutoReminderTimer_Elapsed(object state)
        {

            Common_Tatva.WriteErrorLog("Entered In AutoReminderTimer_Elapsed() function of FetchEmail.cs", " at -----" + DateTime.Now.ToString(), "", new Exception());
            string currentTime = String.Format("{0:t}", DateTime.Now);
            Common_Tatva.WriteErrorLog("Schedule Time : " + currentTime, " at -----" + DateTime.Now.ToString(), "", new Exception());
            if (currentTime == scheduledTime)
            {
                GetFolderReminders();
                UpdateDriveSizeLogs();
            }
        }

        private void AutoOcrTimer_Elapsed(object state)
        {

            Common_Tatva.WriteErrorLog("Entered In AutoOcrTimer_Elapsed() function of FetchEmail.cs", " at -----" + DateTime.Now.ToString(), "", new Exception());
            ConvertPdfToSearchable();
        }

        #endregion

        #region Methods

        #region MailBox
        /// <summary>
        /// Get all working mailboxes from database.
        /// </summary>
        public void GetMailBox()
        {
            Common_Tatva.WriteErrorLog("Entered In GetMailBox() function of FetchEmail.cs", " at -----" + DateTime.Now.ToString(), "", new Exception());
            var mailBoxList = General_Class.GetMailBox();
            //Common_Tatva.WriteErrorLog("Before entering in for loop", " at -----" + DateTime.Now.ToString(), " Total rows are " + mailBoxList.Rows.Count, new Exception());
            for (int i = 0; i < mailBoxList.Rows.Count; i++)
            {
                try
                {
                    string username = Convert.ToString(mailBoxList.Rows[i]["Email"]);
                    string password = Convert.ToString(mailBoxList.Rows[i]["Password"]);
                    int mailboxid = Convert.ToInt32(mailBoxList.Rows[i]["MailBoxId"]);
                    string uid = Convert.ToString(mailBoxList.Rows[i]["UID"]);
                    Task taskS3 = new Task(() =>
                    { CheckNewMail(username, password, mailboxid, uid); });

                    taskS3.Start(scheduler);

                    //Thread thread = new Thread(new ParameterizedThreadStart(CheckNewMail));
                    //thread.Start(new { userName = mailBoxList.Rows[i]["Email"].ToString(), passWord = mailBoxList.Rows[i]["Password"].ToString(), mailBoxId = Convert.ToInt32(mailBoxList.Rows[i]["MailBoxId"].ToString()), uid = mailBoxList.Rows[i]["UID"].ToString() });
                }
                catch (Exception ex)
                {
                    Common_Tatva.WriteErrorLog(string.Empty, " at -----" + DateTime.Now.ToString(), " Total rows are " + mailBoxList.Rows.Count, ex);
                }
            }
            //Common_Tatva.WriteErrorLog("After ending in for loop", " at -----" + DateTime.Now.ToString(), " Total rows are " + mailBoxList.Rows.Count, new Exception());
        }

        /// <summary>
        /// Checks new mails
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="mailBoxId"></param>
        /// <param name="uid"></param>
        public void CheckNewMail(string username, string password, int mailBoxId, string uid)
        {
            Pop3Client pop3Client = new Pop3Client();
            try
            {
                //Common_Tatva.WriteErrorLog("Entered in CheckNewMail()", " at -----" + DateTime.Now.ToString(), " ", new Exception());
                pop3Client.Connect(hostName, port, useSsl);
                Common_Tatva.WriteErrorLog("Before Authenticating", " at -----" + DateTime.Now.ToString(), " For username : " + username + " Password : " + password, new Exception());
                pop3Client.Authenticate(username, password);
                int count = pop3Client.GetMessageCount();
                int startingIndex = General_Class.GetOrUpdateMailCount(mailBoxId);
                startingIndex = startingIndex == 0 ? 1 : startingIndex;
                int endingIndex = startingIndex;
                General_Class.GetOrUpdateMailCount(mailBoxId, count + 1);
                //Common_Tatva.WriteErrorLog("After Authenticating", " at -----" + DateTime.Now.ToString(), " For username : " + username + " Password : " + password, new Exception());
                for (int i = startingIndex; i < count + 1; i++)
                {
                    try
                    {
                        OpenPop.Mime.Message message = pop3Client.GetMessage(i);
                        List<MessagePart> attachments = message.FindAllAttachments();
                        //Common_Tatva.WriteErrorLog("Before checking attachment count", " at -----" + DateTime.Now.ToString(), " For email : " + message.Headers.From.Address + " Total attachments are " + attachments.Count, new Exception());
                        if (attachments.Count > 0)
                            SaveAttachments(attachments, message.Headers.From.Address, uid);
                    }
                    catch (Exception ex)
                    {
                        endingIndex++;
                        Common_Tatva.WriteErrorLog("Error occured in CheckNewEmail function for i = " + i, string.Empty, string.Empty, ex);
                    }
                }

            }
            catch (Exception ex)
            {
                Common_Tatva.WriteErrorLog("Error occured in CheckNewEmail function ", string.Empty, string.Empty, ex);
            }
            finally
            {
                pop3Client.Dispose();
            }
        }

        /// <summary>
        /// Save attachments to Rackspace and database according to fromEmail
        /// </summary>
        /// <param name="attachments"></param>
        /// <param name="fromEmail"></param>
        public void SaveAttachments(List<MessagePart> attachments, string fromEmail, string uid)
        {
            Common_Tatva.WriteErrorLog("Entered In SaveAttachments() function of FetchEmail.cs", " at -----" + DateTime.Now.ToString(), " For email : " + fromEmail + " Total attachments are " + attachments.Count + "and for UID : " + uid, new Exception());

            foreach (MessagePart attachment in attachments)
            {
                try
                {
                    //if (attachment.ContentType.MediaType.Split('/')[1] == "pdf" || attachment.ContentType.MediaType.Split('/')[1] == "png"
                    //    || attachment.ContentType.MediaType.Split('/')[1] == "jpg" || attachment.ContentType.MediaType.Split('/')[1] == "jpeg")
                    //{
                        try
                        {
                            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                            //Common_Tatva.WriteErrorLog("rackSpaceFileUpload initialized for Office : " + uid + " at ----", DateTime.Now.ToString(), string.Empty, new Exception());
                            string errorMsg = string.Empty;
                            string fileName = attachment.FileName.Replace(@"#", @"");
                            Stream stream = new MemoryStream(attachment.Body);
                            decimal size = 0;
                            try
                            {
                                size = Math.Round((decimal)(stream.Length / 1024) / 1024, 2);
                            }
                            catch (Exception) { }
                            string newFileNamePath = rackSpaceFileUpload.UploadObject(ref errorMsg, fileName, Enum_Tatva.Folders.WorkArea, stream, null, uid);
                            //Common_Tatva.WriteErrorLog("Right After UploadObject : " + uid + " at ----", DateTime.Now.ToString(), errorMsg, new Exception());
                            if (string.IsNullOrEmpty(errorMsg))
                            {
                                try
                                {
                                    string workAreaId = General_Class.DocumentsInsertForWorkArea(newFileNamePath.Substring(newFileNamePath.LastIndexOf('/') + 1), uid, uid, size, 0, newFileNamePath, Enum_Tatva.WorkAreaType.SMTPUpload.GetHashCode());
                                    if (Convert.ToInt32(workAreaId) > 0)
                                        General_Class.CreateDocumentsAndFolderLogs(new Guid(uid), 0, 0, 0, 0, 0, 0, Convert.ToInt32(workAreaId), null, 0, (int)Enum_Tatva.DocumentsAndFoldersLogs.Upload, 0, 0, 0, 0, 0);
                                    //Common_Tatva.WriteErrorLog("Updating database for WorkArea by Email for OfficeId == " + uid, workAreaId, fileName, new Exception());
                                }
                                catch (Exception ex)
                                {
                                    Common_Tatva.WriteErrorLog(uid, uid, fileName, ex);
                                }
                            }
                            else
                                Common_Tatva.WriteErrorLog(uid + "=====" + errorMsg, uid, fileName, new Exception());
                        }
                        catch (Exception ex) { }
                    //}
                }
                catch (Exception ex)
                {
                    Common_Tatva.WriteErrorLog(uid, uid, fromEmail, ex);
                }
            }

        }

        #endregion

        #region ReminderShare

        /// <summary>
        /// Sends the mail for folder reminder.
        /// </summary>
        public void GetFolderReminders()
        {
            try
            {
                DataTable dtReminders = General_Class.GetFolderRemindersForAlert();
                if (dtReminders != null && dtReminders.Rows.Count > 0)
                {
                    foreach (DataRow drReminder in dtReminders.Rows)
                    {
                        string email = Convert.ToString(drReminder["Email"]);
                        string folderName = Convert.ToString(drReminder["FolderName"]);
                        string reminderName = Convert.ToString(drReminder["Name"]);
                        DateTime reminderDate = Convert.ToDateTime(drReminder["ReminderDate"]);
                        int folderId = Convert.ToInt32(drReminder["FolderId"]);
                        if (!string.IsNullOrEmpty(email))
                        {
                            Common_Tatva.SendMailForFolderReminder(email, Convert.ToString(drReminder["UserName"]), folderName, reminderName, reminderDate, folderId);
                        }

                        if (Convert.ToBoolean(Convert.ToInt32(drReminder["IsSubUser"])))
                        {
                            Account account = new Account(Convert.ToString(drReminder["OfficeID1"]));
                            if (account != null && !string.IsNullOrEmpty(account.Email.ToString()))
                            {
                                Common_Tatva.SendMailForFolderReminder(account.Email.ToString(), account.Name.ToString(), folderName, reminderName, reminderDate, folderId);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteErrorLog(null, null, null, ex);
            }
        }

        #endregion

        #region OCR

        /// <summary>
        /// Updates the drive size logs.
        /// </summary>
        public void UpdateDriveSizeLogs()
        {
            try
            {
                float dblSByte = 0;
                foreach (DriveInfo drive in DriveInfo.GetDrives())
                {
                    if (drive.IsReady && drive.Name == driveName)
                    {
                        var driveSize = drive.TotalSize;
                        var freeSize = drive.AvailableFreeSpace;

                        float currentSize = driveSize - freeSize;
                        string[] Suffix = { "B", "KB", "MB", "GB", "TB" };
                        int i;
                        dblSByte = currentSize;
                        for (i = 0; i < Suffix.Length && currentSize >= 1024; i++, currentSize /= 1024)
                        {
                            dblSByte = currentSize / 1024;
                        }
                    }
                }
                General_Class.CreateDriveSizeLogs(dblSByte.ToString("0.##"));
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteErrorLog(null, null, null, ex);
            }
        }

        /// <summary>
        /// Converts the PDF to searchable.
        /// </summary>
        public void ConvertPdfToSearchable()
        {
            try
            {
                DataTable dtDocs = General_Class.GetRequestedOcrDocs(Enum_Tatva.OcrStatus.OcrRequested.GetHashCode());
                if (dtDocs != null && dtDocs.Rows.Count > 0)
                {
                    DataTable dtDocsToUpdate = new DataTable();
                    dtDocsToUpdate.Columns.Add("Index");
                    dtDocsToUpdate.Columns.Add("DocumentID");
                    dtDocsToUpdate.Columns.Add("OcrStatus");
                    for (int i = 0; i < dtDocs.Rows.Count; i++)
                    {
                        DataRow dr = dtDocs.Rows[i];
                        int documentId = Convert.ToInt32(dr["DocumentID"]);
                        string name = Convert.ToString(dr["Name"]);
                        string path = Convert.ToString(dr["PathName"]);
                        name = HttpUtility.UrlPathEncode(name).Trim();
                        name = name.Replace("%", "$");
                        string fullPath = volumePath + path + Path.DirectorySeparatorChar + name + ".pdf";
                        if (File.Exists(fullPath))
                        {
                            byte[] dataBytes = Convert.FromBase64String(Convert.ToBase64String(File.ReadAllBytes(fullPath)));
                            Stream stream = new MemoryStream(dataBytes);
                            string pdfText = PDFParser.ExtractTextFromPdfImages(name, stream);
                            var document = new DocumentIndex()
                            {
                                DocumentId = documentId,
                                FolderId = Convert.ToInt32(dr["FileFolderId"]),
                                DividerId = Convert.ToInt32(dr["DividerID"]),
                                Name = Convert.ToString(dr["Name"]),
                                Path = Convert.ToString(dr["PathName"]),
                                Size = (float)Convert.ToDouble(dr["Size"]),
                                JsonData = pdfText,
                                IsDeleted = false,
                                DisplayName = Convert.ToString(dr["DisplayName"])
                            };
                            bool isValid = Common_Tatva.CreateDocument(document);
                            Common_Tatva.WriteErrorLog("IsValid : " + isValid, "DocId : " + documentId, string.Empty, new Exception());
                            if (isValid)
                                dtDocsToUpdate.Rows.Add(i + 1, documentId, Enum_Tatva.OcrStatus.OcrDone.GetHashCode());
                        }
                    }
                    General_Class.UpdateOcrStatus(dtDocsToUpdate);
                }
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteErrorLog("Entered In ConvertPdfToSearchable() function of FetchEmail.cs", " at -----" + DateTime.Now.ToString(), string.Empty, ex);
            }
        }

        #endregion

        #endregion
    }

    /// <summary>
    /// Scheduler class to make thread-safe event
    /// reference - http://www.codeguru.com/csharp/article.php/c18931/Understanding-the-NET-Task-Parallel-Library-TaskScheduler.htm
    /// </summary>
    public sealed class SimpleScheduler : TaskScheduler, IDisposable
    {
        private BlockingCollection<Task> _tasks = new BlockingCollection<Task>();
        private Thread _main = null;

        public SimpleScheduler()
        {
            _main = new Thread(new ThreadStart(this.Main));
        }

        private void Main()
        {
            //Console.WriteLine("Starting Thread " + Thread.CurrentThread.ManagedThreadId.ToString());

            foreach (var t in _tasks.GetConsumingEnumerable())
            {
                TryExecuteTask(t);
            }
        }

        /// <summary>
        /// Used by the Debugger
        /// </summary>
        /// <returns></returns>
        protected override IEnumerable<Task> GetScheduledTasks()
        {
            return _tasks.ToArray<Task>();
        }


        protected override void QueueTask(Task task)
        {
            _tasks.Add(task);

            if (!_main.IsAlive) { _main.Start(); }//Start thread if not done so already
        }


        protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
        {
            return false;
        }


        #region IDisposable Members

        public void Dispose()
        {
            _tasks.CompleteAdding(); //Drops you out of the thread
        }

        #endregion
    }
}
