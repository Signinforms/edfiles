﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using PdfSharp.Drawing;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Drawing;
using System.Drawing.Imaging;
using System.Reflection;

namespace EdWindowService
{
    /// <summary>
    /// Summary description for PDFParser
    /// </summary>
    public class PDFParser
    {
        /// <summary>
        /// Fetch texts from pdf file
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static string ExtractTextFromPdfImages(string filename, Stream stream)
        {
            //List<String> pages = new List<String>();
            var ocrtext = string.Empty;
            var assemblyPath = Assembly.GetExecutingAssembly().Location;
            var tesseractPath = assemblyPath.Substring(0, assemblyPath.LastIndexOf("\\") + 1) + "tesseract-master.1153";
            try
            {
                string prevPage = "";
                //var sb = new StringBuilder();
                var images = new Dictionary<string, System.Drawing.Image>();
                byte[] bytes = ReadToEnd(stream);
                using (var reader = new PdfReader(bytes))
                {
                    var parser = new PdfReaderContentParser(reader);
                    ImageRenderListener listener = null;
                    for (var i = 1; i <= reader.NumberOfPages; i++)
                    {
                        var sb = new StringBuilder();
                        parser.ProcessContent(i, (listener = new ImageRenderListener()));
                        var index = 1;
                        if (listener.Images.Count > 0)
                        {
                            try
                            {
                                foreach (KeyValuePair<System.Drawing.Image, string> pair in listener.Images)
                                {
                                    images.Add(string.Format("{0}_Page_{1}_Image_{2}{3}",
                                       System.IO.Path.GetFileNameWithoutExtension(filename), i.ToString("D4"), index.ToString("D4"), pair.Value), pair.Key);
                                    Bitmap imgsource = new Bitmap(pair.Key);
                                    ImageConverter converter = new ImageConverter();
                                    var imgFile = (byte[])converter.ConvertTo(imgsource, typeof(byte[]));
                                    ocrtext += ParseText(tesseractPath, imgFile, "eng", "fra");
                                    index++;
                                }
                                ITextExtractionStrategy its = new SimpleTextExtractionStrategy();
                                var s = PdfTextExtractor.GetTextFromPage(reader, i, its);
                                if (prevPage != s) sb.Append(s);
                                prevPage = s;
                                ocrtext += sb.ToString();
                            }
                            catch (Exception ex)
                            {
                                continue;
                            }
                        }
                        else
                        {
                            ITextExtractionStrategy its = new SimpleTextExtractionStrategy();
                            var s = PdfTextExtractor.GetTextFromPage(reader, i, its);
                            if (prevPage != s) sb.Append(s);
                            prevPage = s;
                            ocrtext += sb.ToString();
                        }
                    }
                    return ocrtext;
                }
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteErrorLog(ex.Message, "Exception in ExtractTextFromPdfImages - " + filename, null, ex);
                //return pages.ToArray();
                return ocrtext;
            }
        }

        internal class ImageRenderListener : IRenderListener
        {
            Dictionary<System.Drawing.Image, string> images = new Dictionary<System.Drawing.Image, string>();
            public Dictionary<System.Drawing.Image, string> Images
            {
                get { return images; }
            }
            public void BeginTextBlock() { }
            public void EndTextBlock() { }

            public void RenderImage(ImageRenderInfo renderInfo)
            {
                try
                {
                    //var image1 = renderInfo.GetImageCTM();
                    PdfImageObject image = renderInfo.GetImage();
                    PdfName filter = (PdfName)image.Get(PdfName.FILTER);

                    if (filter != null)
                    {
                        System.Drawing.Image drawingImage = image.GetDrawingImage();
                        string extension = ".";
                        if (filter == PdfName.DCTDECODE)
                        {
                            extension += PdfImageObject.ImageBytesType.JPG.FileExtension;
                        }
                        else if (filter == PdfName.JPXDECODE)
                        {
                            extension += PdfImageObject.ImageBytesType.JP2.FileExtension;
                        }
                        else if (filter == PdfName.FLATEDECODE)
                        {
                            extension += PdfImageObject.ImageBytesType.PNG.FileExtension;
                        }
                        else if (filter == PdfName.LZWDECODE || filter == PdfName.CCITTFAXDECODE)
                        {
                            extension += PdfImageObject.ImageBytesType.CCITT.FileExtension;
                        }
                        this.Images.Add(drawingImage, extension);
                    }
                }
                catch (Exception ex)
                {
                    Common_Tatva.WriteErrorLog(string.Empty, string.Empty, "Not FileName - RenderImage()", ex);
                }
            }
            public void RenderText(TextRenderInfo renderInfo) { }
        }

        /// <summary>
        /// Convert stream to bytes array
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static byte[] ReadToEnd(System.IO.Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }

        private static string ParseText(string tesseractPath, byte[] imageFile, params string[] lang)
        {
            string output = string.Empty;
            var tempOutputFile = System.IO.Path.GetTempPath() + Guid.NewGuid();
            var tempImageFile = System.IO.Path.GetTempFileName();

            try
            {
                File.WriteAllBytes(tempImageFile, imageFile);

                ProcessStartInfo info = new ProcessStartInfo();
                info.WorkingDirectory = tesseractPath;
                info.WindowStyle = ProcessWindowStyle.Hidden;
                info.UseShellExecute = false;
                info.FileName = "cmd.exe";
                info.Arguments =
                    "/c tesseract.exe " +
                    // Image file.
                    tempImageFile + " " +
                    // Output file (tesseract add '.txt' at the end)
                    tempOutputFile +
                    // Languages.
                    " -l " + string.Join("+", lang);

                // Start tesseract.
                Process process = Process.Start(info);
                process.WaitForExit();
                if (process.ExitCode == 0)
                {
                    // Exit code: success.
                    output = File.ReadAllText(tempOutputFile + ".txt");
                }
                else
                {
                    throw new Exception("Error. Tesseract stopped with an error code = " + process.ExitCode);
                }
            }
            finally
            {
                File.Delete(tempImageFile);
                File.Delete(tempOutputFile + ".txt");
            }
            return output;
        }

    }
}
