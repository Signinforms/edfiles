﻿using net.openstack.Core.Domain;
using net.openstack.Providers.Rackspace;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Reflection;
using System.Net;
using System.Net.Http;

#region RackSpaceFileUpload
/// <summary>
/// Summary description for RackSpaceFileUpload
/// </summary>
public class RackSpaceFileUpload
{
    #region Properties
    public static CloudIdentity user = new CloudIdentity
    {
        Username = ConfigurationSettings.AppSettings["RackSpaceUserName"],
        APIKey = ConfigurationSettings.AppSettings["RackSpaceAPIKey"]
    };
    public static CloudFilesProvider cloudfilesProvider = new CloudFilesProvider(user);
    public static string Sessions_RackSpaceUserID = "";
    #endregion

    public RackSpaceFileUpload()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public RackSpaceFileUpload(string sessionID)
    {
        Sessions_RackSpaceUserID = sessionID;
    }

    /// <summary>
    /// Creates container named as user identity if it does not already exists
    /// </summary>
    /// <param name="containerName">Name of container to be created</param>
    /// <returns>Ture if container created successfully, false otheriwse</returns>
    public bool CreateContainer(string containerName, ref string errorMessage)
    {
        try
        {
            Common_Tatva.WriteErrorLog("Entered in CreateContainer: " + containerName + " at ----", DateTime.Now.ToString(), user.Username + "========" + cloudfilesProvider.ToString() + "=======" + user.APIKey + "=======" + errorMessage, new Exception());
            cloudfilesProvider.CreateContainer(containerName, null, Common_Tatva.Region);
            return true;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog("Entered in CreateContainer Catch: " + containerName + "false  at ----", DateTime.Now.ToString(), errorMessage, new Exception());
            Common_Tatva.WriteErrorLog(containerName, string.Empty, string.Empty, ex);
            errorMessage = ex.Message;
            return false;
        }
    }

    /// <summary>
    /// Uploads object to eFileShare folder
    /// </summary>
    /// <param name="fileName">Name of file to be uploaded</param>
    /// <param name="folder">Name of folder into which file is to be uploaded</param>
    /// <param name="fileContent">Content of file to be uploaded</param>
    /// <param name="folderID">ID of share folder</param>
    /// <param name="errorMsg">Error Message</param>
    /// <returns>True if object uploaded successfully, false otherwise</returns>
    public string UploadObject(ref string errorMsg, string fileName, Enum_Tatva.Folders folder, Stream fileContent, string folderID = null, string uid = null)
    {
        Common_Tatva.WriteErrorLog("Entered in UploadObject: " + uid + " at ----", DateTime.Now.ToString(), errorMsg, new Exception());
        MemoryStream memoryStream = new MemoryStream();
        try
        {
            Sessions_RackSpaceUserID = uid;
            string objectToUploaded = string.Empty;
            //Common_Tatva.WriteErrorLog("Just Before creating container: " + uid + " at ----", DateTime.Now.ToString(), errorMsg, new Exception());
            if (CreateContainer(uid, ref errorMsg))
            {
                //Common_Tatva.WriteErrorLog("Container created  CreateContainer: " + uid + " at ----", DateTime.Now.ToString(), errorMsg, new Exception());
                if (fileName.IndexOf('/') > 0)
                {
                    int fileIndex = fileName.LastIndexOf('/') + 1;
                    folderID = fileName.Substring(0, fileIndex - 1);
                    fileName = fileName.Substring(fileIndex);
                }
                objectToUploaded = GeneratePath(fileName.ToLower(), folder, folderID);


                //Check if object is already there with same name
                try
                {
                    cloudfilesProvider.GetObject(uid, objectToUploaded, memoryStream);
                    //Common_Tatva.WriteErrorLog("Object is already there with same name: " + uid + " at ----", DateTime.Now.ToString(), errorMsg, new Exception());
                }
                catch (Exception ex)
                {
                    Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(objectToUploaded) ? string.Empty : objectToUploaded, "Object is not there", fileName, ex);
                }
                if (memoryStream.Length > 0)
                {
                    //Common_Tatva.WriteErrorLog("Object is already there with same name: " + uid + " at ----", DateTime.Now.ToString(), "memorystream length > 0 =====" + memoryStream.Length.ToString(), new Exception());
                    fileName = GetNewFileName(fileName.ToLower(), folder, folderID, uid);
                    fileName = fileName.Substring(fileName.LastIndexOf('\\') + 1);
                    objectToUploaded = GeneratePath(fileName, folder, folderID);
                    //Common_Tatva.WriteErrorLog("Object is already there with same name: " + uid + " at ----", DateTime.Now.ToString(), "Generated path =====" + objectToUploaded, new Exception());
                }

                if (folder.Equals(Enum_Tatva.Folders.EFileShare) && folderID != null)
                {
                    string sourceObjectName = GeneratePath(fileName.ToLower(), Enum_Tatva.Folders.EFileFlow);
                    cloudfilesProvider.CopyObject(uid, sourceObjectName, uid, objectToUploaded);
                }
                else
                {
                    //Common_Tatva.WriteErrorLog("Just before: cloudfilesProvider.CreateObject" + uid + " at ----", DateTime.Now.ToString() + "======" + fileContent.ToString(), "Generated path =====" + objectToUploaded, new Exception());
                    cloudfilesProvider.CreateObject(uid, fileContent, objectToUploaded);
                    //Common_Tatva.WriteErrorLog("Right after: cloudfilesProvider.CreateObject" + uid + " at ----", DateTime.Now.ToString() + "======" + fileContent.ToString(), "Generated path =====" + objectToUploaded, new Exception());
                }
            }
            return objectToUploaded;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(folderID) ? string.Empty : folderID, "Catch", fileName, ex);
            errorMsg = ex.Message;
            return fileName;
        }
        finally
        {
            memoryStream.Close();
            memoryStream.Dispose();
        }

    }

    /// <summary>
    /// Generates path according to folder type for uploading object
    /// </summary>
    /// <param name="fileName">Name of file to be uploaded</param>
    /// <param name="folder">Name of folder into which file is to be uploaded</param>
    /// <param name="folderID">ID of share folder</param>
    /// <returns>Path to which file is to be uploaded</returns>
    public string GeneratePath(string fileName, Enum_Tatva.Folders folder, string folderID = null)
    {
        string filePath = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(folderID))
            {
                filePath = Enum_Tatva.GetEnumDescription(folder) + "/" + folderID + "/" + fileName;
            }
            else
            {
                filePath = Enum_Tatva.GetEnumDescription(folder) + "/" + fileName; ;
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(folderID) ? string.Empty : folderID, string.Empty, fileName, ex);
        }
        return filePath;
    }

    /// <summary>
    /// Returns list of object from specified container
    /// </summary>
    /// <param name="containerName">Name of container to list objects</param>
    /// <param name="markerStart">Name of folder from which objects to be retrived</param>
    /// <returns>Returns list of object from specified container</returns>
    public IEnumerable<ContainerObject> GetObjectList(string containerName, string markerStart = null)
    {
        return cloudfilesProvider.ListObjects(containerName, null, markerStart);
    }

    /// <summary>
    /// Renames object with new name
    /// </summary>
    /// <param name="oldObjectPath">Name of object to be renamed</param>
    /// <param name="newObjectName">New name of object</param>
    /// <param name="errorMsg">Error Message</param>
    public string RenameObject(ref string errorMsg, string oldObjectPath, string newObjectName, Enum_Tatva.Folders folder, string userid = null)
    {
        try
        {
            string newPath = oldObjectPath.Substring(0, oldObjectPath.LastIndexOf('/'));
            string extension = oldObjectPath.Substring(oldObjectPath.LastIndexOf('.'));
            string newName = string.Empty;
            if (newObjectName.LastIndexOf('.') > 0)
                newName = newPath + "/" + newObjectName.ToLower();
            else
                newName = newPath + "/" + newObjectName.ToLower() + extension;

            newName = MoveObject(ref errorMsg, oldObjectPath, newName, folder, userid);
            return newName;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(oldObjectPath) ? string.Empty : oldObjectPath, string.Empty, oldObjectPath, ex);
            errorMsg = ex.Message;
            return string.Empty;
        }
    }

    /// <summary>
    /// Delets object from container
    /// </summary>
    /// <param name="objectPath">Path of object from where object to be deleted</param>
    /// <param name="errorMsg">Error Message</param>
    public List<string> DeleteObject(ref string errorMsg, string objectPath)
    {
        List<string> objectsToDelete = new List<string>();
        try
        {
            IEnumerable<ContainerObject> objectList = cloudfilesProvider.ListObjects(Sessions_RackSpaceUserID, null, objectPath);
            if (objectList.ToArray<ContainerObject>().Length <= 0)
            {
                objectList = cloudfilesProvider.ListObjects(Sessions_RackSpaceUserID, null);
            }

            ContainerObject[] objList = objectList.ToArray<ContainerObject>();

            for (int i = objList.Length - 1; i >= 0; i--)
            {
                if (objList[i].Name.Contains(objectPath))
                {
                    cloudfilesProvider.DeleteObject(Sessions_RackSpaceUserID, objList[i].Name);
                    objectsToDelete.Add(objList[i].Name);
                }
            }
            cloudfilesProvider.DeleteObject(Sessions_RackSpaceUserID, objectPath);
            objectsToDelete.Add(objectPath);
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(objectPath) ? string.Empty : objectPath, string.Empty, objectPath, ex);
            errorMsg = ex.Message;
        }
        return objectsToDelete;
    }

    /// <summary>
    /// Copies file from cloud to local system
    /// </summary>
    /// <param name="objectPath">Path of file to be copied</param>
    /// <returns>New filename as filename + current timestamp</returns>
    /// <param name="errorMsg">Error Message</param>
    public string GetObject(ref string errorMsg, string objectPath, string userID = null)
    {
        MemoryStream memoryStream = new MemoryStream();
        FileStream file = null;
        try
        {
            string fileName = objectPath.Substring(objectPath.LastIndexOf('/') + 1, objectPath.LastIndexOf('.') - objectPath.LastIndexOf('/') - 1) +
                (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds + objectPath.Substring(objectPath.LastIndexOf('.'));

            cloudfilesProvider.GetObject(string.IsNullOrEmpty(userID) ? Sessions_RackSpaceUserID : userID, objectPath, memoryStream);

            //file = new FileStream(HttpContext.Current.Server.MapPath("~/" + Common_Tatva.RackSpaceDownloadDirectory) + "\\" + fileName, FileMode.Create, FileAccess.Write);
            file = new FileStream(Assembly.GetExecutingAssembly().Location + "\\" + fileName, FileMode.Create, FileAccess.Write);

            memoryStream.WriteTo(file);
            file.Close();
            file.Dispose();
            memoryStream.Close();
            return fileName;

        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(objectPath) ? string.Empty : objectPath, string.Empty, objectPath, ex);
            errorMsg = ex.Message;
            return string.Empty;
        }
        finally
        {
            memoryStream.Close();
            memoryStream.Dispose();
            if (file != null)
            {
                file.Close();
                file.Dispose();
            }
        }
    }

    /// <summary>
    /// Creates folder inside specified path
    /// </summary>
    /// <param name="path">Path in which folder is to be created</param>
    /// <param name="folderName">Name of folder to be created</param>
    /// <param name="errorMsg">Error Message</param>
    public void CreateFolder(ref string errorMsg, string path, string folderName)
    {
        MemoryStream memoryStream = new MemoryStream();
        try
        {
            cloudfilesProvider.CreateObject(Sessions_RackSpaceUserID, memoryStream, path + "/" + folderName + "/" + Sessions_RackSpaceUserID + ".txt");
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(path) ? string.Empty : path, string.Empty, path, ex);
            errorMsg = ex.Message;
        }
        finally
        {
            memoryStream.Close();
            memoryStream.Dispose();
        }
    }

    /// <summary>
    /// Moves object from old path to new path
    /// </summary>
    /// <param name="oldObjectPath">Path where object is located</param>
    /// <param name="newObjectPath">Path where object is to be moved</param>
    /// <param name="errorMsg">Error Message</param>
    public string MoveObject(ref string errorMsg, string oldObjectPath, string newObjectPath, Enum_Tatva.Folders folder, string userid = null)
    {
        MemoryStream memoryStream = new MemoryStream();
        try
        {
            string uid = string.Empty;
            if (string.IsNullOrEmpty(userid))
                uid = Sessions_RackSpaceUserID;
            else
                uid = userid;
            try
            {
                cloudfilesProvider.GetObject(uid, newObjectPath, memoryStream);
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(newObjectPath) ? string.Empty : newObjectPath, string.Empty, newObjectPath, ex);
            }
            if (memoryStream.Length > 0)
            {
                string folderPath = string.Empty;
                int length = (newObjectPath.LastIndexOf('/') - (Enum_Tatva.GetEnumDescription(folder).Length + 1));
                if (length > 0)
                    folderPath = newObjectPath.Substring((Enum_Tatva.GetEnumDescription(folder).Length + 1), length);

                string fileName = GetNewFileName(newObjectPath.Substring(newObjectPath.LastIndexOf('/') + 1).ToLower(), folder, folderPath);
                fileName = fileName.Substring(fileName.LastIndexOf('\\') + 1);
                newObjectPath = GeneratePath(fileName, folder, folderPath);
            }
            cloudfilesProvider.MoveObject(uid, oldObjectPath, uid, newObjectPath);
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(oldObjectPath) ? string.Empty : oldObjectPath, string.Empty, oldObjectPath, ex);
            errorMsg = ex.Message;
        }
        finally
        {
            memoryStream.Close();
            memoryStream.Dispose();
        }
        return newObjectPath;
    }

    /// <summary>
    /// Returns size of file in bytes
    /// </summary>
    /// <param name="errorMsg">Error Messgae</param>
    /// <param name="path">Path of file</param>
    /// <returns></returns>
    public long GetSizeOfFile(ref string errorMsg, string path, string uid = null)
    {
        long size = 0;
        try
        {
            if (string.IsNullOrEmpty(uid))
                uid = Sessions_RackSpaceUserID;
            Dictionary<string, string> properties = cloudfilesProvider.GetObjectHeaders(uid, path);
            if (properties.ContainsKey("Content-Length"))
            {
                size = Convert.ToInt64(properties["Content-Length"]);
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(path) ? string.Empty : path, string.Empty, path, ex);
            errorMsg = ex.Message;
        }
        return size;
    }

    /// <summary>
    /// Generates new file name if file already exists
    /// </summary>
    /// <param name="destinationFile">Name of file to be renamed</param>
    /// <param name="folder">Folder where file resides</param>
    /// <returns>New file name</returns>
    public string GetNewFileName(string destinationFile, Enum_Tatva.Folders folder, string folderID = null, string uid = null)
    {
        try
        {
            if (string.IsNullOrEmpty(uid))
                uid = Sessions_RackSpaceUserID;
            string fileName = Path.GetFileNameWithoutExtension(destinationFile);
            string fileExtension = Path.GetExtension(destinationFile);
            int i = 1;
            IEnumerable<ContainerObject> objectList = GetObjectList(uid, Enum_Tatva.GetEnumDescription(folder) + (string.IsNullOrEmpty(folderID) ? string.Empty : "/" + folderID));

            ContainerObject[] containerObject = objectList.ToArray<ContainerObject>();
            for (int index = 0; index < containerObject.Length; index++)
            {
                string renameFileName = fileName + "_v" + i + fileExtension;
                string fileNameToCompare = string.Empty;
                if (!string.IsNullOrEmpty(folderID))
                {
                    if (containerObject[index].Name.Contains(folderID) && string.Equals(renameFileName, containerObject[index].Name.Substring(containerObject[index].Name.LastIndexOf('/') + 1)))
                    {
                        i++;
                        index = -1;
                        continue;
                    }
                }
                else
                {
                    string comparePath = GeneratePath(renameFileName.ToLower(), folder);
                    if (string.Equals(comparePath, containerObject[index].Name))
                    {
                        i++;
                        index = -1;
                        continue;
                    }
                }
                destinationFile = renameFileName;
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(destinationFile) ? string.Empty : destinationFile, string.Empty, destinationFile, ex);
        }
        return destinationFile;
    }

    /// <summary>
    /// Overwrites object in share folder 
    /// </summary>
    /// <param name="objectPath">File path to be overwritten</param>
    /// <param name="fileContent">Content of file</param>
    /// <param name="errorMsg">Error Message</param>
    public void UploadObjectForViewer(ref string errorMsg, string objectPath, Stream fileContent, string uid)
    {
        try
        {
            cloudfilesProvider.CreateObject(uid, fileContent, objectPath);
        }
        catch (Exception ex)
        {
            errorMsg = ex.Message;
        }
    }

    public string GetPath(string fileName, Enum_Tatva.Folders folder, string sessionID, ref long fileSize, string folderID = null)
    {
        try
        {
            string filePath = string.Empty;
            string errorMsg = string.Empty;
            if (folder == Enum_Tatva.Folders.WorkArea)
                filePath = fileName;
            else
                filePath = GeneratePath(fileName.ToLower(), folder, folderID);
            string newFileName = GetObject(ref errorMsg, filePath);
            if (string.IsNullOrEmpty(errorMsg))
            {
                fileSize = GetSizeOfFile(ref errorMsg, filePath);
                return Common_Tatva.RackSpaceDownloadDirectory + "/" + newFileName;
            }
            else
            {
                return string.Empty;
            }
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
    }

    /// <summary>
    /// Gets sub user's file content to be uploaded into parent's account
    /// </summary>
    /// <param name="uid">Sub user's uid</param>
    /// <param name="objectPath">Path of object from which content to be retrived</param>
    /// <returns></returns>
    public MemoryStream GetObjectForTestRackspacePage(string uid, string objectPath)
    {
        MemoryStream memoryStream = new MemoryStream();
        try
        {
            cloudfilesProvider.GetObject(uid, objectPath, memoryStream);
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(objectPath) ? string.Empty : objectPath, string.Empty, objectPath, ex);
        }
        finally
        {
            memoryStream.Close();
            memoryStream.Dispose();
        }
        return memoryStream;
    }

    /// <summary>
    /// Uploads sub user's files to parent user
    /// </summary>
    /// <param name="errorMsg"></param>
    /// <param name="fileName">filename to be uploaded</param>
    /// <param name="folder">folder in which file to be uploaded</param>
    /// <param name="fileContent">content of file</param>
    /// <param name="folderID"></param>
    /// <param name="uid">user id where file is to be uploaded</param>
    /// <param name="fileSize">size of file</param>
    /// <returns>path of uploaded file</returns>
    public string UploadObjectForTestRackspacePage(ref string errorMsg, string fileName, Enum_Tatva.Folders folder, Stream fileContent, string folderID = null, string uid = null, long fileSize = 0)
    {
        MemoryStream memoryStream = new MemoryStream();
        try
        {
            string objectToUploaded = string.Empty;
            if (string.IsNullOrEmpty(uid))
                uid = Sessions_RackSpaceUserID;
            if (CreateContainer(uid, ref errorMsg))
            {
                if (fileName.IndexOf('/') > 0)
                {
                    int fileIndex = fileName.LastIndexOf('/') + 1;
                    folderID = fileName.Substring(0, fileIndex - 1);
                    fileName = fileName.Substring(fileIndex);
                }
                objectToUploaded = GeneratePath(fileName.ToLower(), folder, folderID);

                //Check if object is already there with same name
                try
                {
                    cloudfilesProvider.GetObject(uid, objectToUploaded, memoryStream);
                }
                catch (Exception ex)
                {
                    Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(objectToUploaded) ? string.Empty : objectToUploaded, string.Empty, fileName, ex);
                }
                if (memoryStream.Length > 0)
                {
                    if (memoryStream.Length == fileSize)
                        return string.Empty;
                    fileName = GetNewFileName(fileName.ToLower(), folder, folderID, uid);
                    fileName = fileName.Substring(fileName.LastIndexOf('\\') + 1);
                    objectToUploaded = GeneratePath(fileName, folder, folderID);
                }

                if (folder.Equals(Enum_Tatva.Folders.EFileShare) && folderID != null)
                {
                    string sourceObjectName = GeneratePath(fileName.ToLower(), Enum_Tatva.Folders.EFileFlow);
                    cloudfilesProvider.CopyObject(uid, sourceObjectName, Sessions_RackSpaceUserID, objectToUploaded);
                }
                else
                {
                    cloudfilesProvider.CreateObject(uid, fileContent, objectToUploaded);
                }
            }
            return objectToUploaded;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(folderID) ? string.Empty : folderID, string.Empty, fileName, ex);
            errorMsg = ex.Message;
            return fileName;
        }
        finally
        {
            memoryStream.Close();
            memoryStream.Dispose();
        }

    }


    /// <summary>
    /// Copies file from cloud to local system for test rackspace
    /// </summary>
    /// <param name="objectPath">Path of file to be copied</param>
    /// <returns>New filename as filename + current timestamp</returns>
    /// <param name="errorMsg">Error Message</param>
    public string GetObjectForTestRackSpace(ref string errorMsg, string fileName, MemoryStream memoryStream, string folderName, string parentUserID, string uid)
    {
        FileStream file = null;
        try
        {
            //string fileName = objectPath.Substring(objectPath.LastIndexOf('/') + 1, objectPath.LastIndexOf('.') - objectPath.LastIndexOf('/') - 1) +
            //    (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds + objectPath.Substring(objectPath.LastIndexOf('.'));
            //MemoryStream memoryStream = new MemoryStream();

            //cloudfilesProvider.GetObject(userID, objectPath, memoryStream);
            var parent = Assembly.GetExecutingAssembly().Location;
            //var parent = HttpContext.Current.Server.MapPath("~/Upload/RackSpaceUploads/" + parentUserID);
            if (!Directory.Exists(parent))
            {
                Directory.CreateDirectory(parent);
            }
            if (!Directory.Exists(parent + "\\" + uid))
            {
                Directory.CreateDirectory(parent + "\\" + uid);
            }
            if (!Directory.Exists(parent + "\\" + uid + "\\" + folderName))
            {
                Directory.CreateDirectory(parent + "\\" + uid + "\\" + folderName);
            }
            file = new FileStream(Assembly.GetExecutingAssembly().Location + "\\" + fileName, FileMode.Create, FileAccess.Write);
            //file = new FileStream(HttpContext.Current.Server.MapPath("~/Upload/RackSpaceUploads/" + parentUserID + "\\" + uid + "\\" + folderName) + "\\" + fileName, FileMode.Create, FileAccess.Write);

            memoryStream.WriteTo(file);
            file.Close();
            memoryStream.Close();
            return fileName;

        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.IsNullOrEmpty(fileName) ? string.Empty : fileName, string.Empty, fileName, ex);
            errorMsg = ex.Message;
            return string.Empty;
        }
        finally
        {
            if (file != null)
            {
                file.Close();
                file.Dispose();
            }
        }
    }
}
#endregion