﻿<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:template match="/" xmlns:ms="urn:schemas-microsoft-com:xslt"
      xmlns:dt="urn:schemas-microsoft-com:datatypes">
		<HTML>
			<HEAD>
				<STYLE>
					.stdPVTblLCell {
					background-color: #00a7e7;
					color: white;
					font-weight: bold;
					text-align: left;
					padding-left: 4px;
					padding-top: 4px;
					padding-bottom: 4px;
					width: 100%;
					font-size: 12pt;
					}
					.stdPageHdr {
					color: DarkBlue;
					font-weight: bold;
					font-style:italic;
					font-family:Verdana;
					text-align: left;
					padding-left: 4px;
					padding-top: 4px;
					padding-bottom: 4px;
					width: 100%;
					font-size: 20pt;
					}
					.gridHeader {
					background-color: #C0C0C0;
					color: DarkBlue;
					font-size: 9pt;
					font-weight: bold;
					font-family:Verdana;
					vertical-align:middle;
					text-align:center;
					border: solid thin Black;
					}
					.totalHeader {
					background-color: #CCFFCC;
					color: DarkBlue;
					font-size: 9pt;
					font-weight: bold;
					font-family:Verdana;
					vertical-align:middle;
					text-align:right;
					}
					.SearchHeader {
					color: DarkBlue;
					font-size: 9pt;
					font-weight: bold;
					font-family:Verdana;
					}
					.SearchKey {
					color: DarkBlue;
					font-size: 9pt;
					vertical-align:middle;
					text-align:right;
					font-family:Verdana;
					}
					.SearchValue {
					color: Black;
					font-size: 9pt;
					font-weight: bold;
					vertical-align:middle;
					text-align:left;
					font-family:Verdana;
					}
					.SearchResultHeader {
					background-color: #CCFFCC;
					color: DarkBlue;
					font-size: 9pt;
					font-weight: bold;
					font-family:Verdana;
					}
					.SearchResultItem {
					background-color: #CCFFFF;
					color: Black;
					font-size: 8pt;
					font-family:Verdana;
					border: solid thin Black;
					}
					.SearchResultAltItem {
					background-color: #99CCFF;
					color: Black;
					font-size: 8pt;
					font-family:Verdana;
					border: solid thin Black;
					}
				</STYLE>
			</HEAD>
			<BODY>
				<TABLE>
					<TR>
						<TD></TD>
				  </TR>
					<TR>
						<TD  class="stdPageHdr" >Customer Track Details</TD>
                      
                      
					</TR>
					<TR  class="stdPVTblLCell">
						<TD >
						   Partner Details						</TD>
                           
				  </TR><TR><TD><TABLE><TR>
						<TD colspan="4"></TD>
					</TR>
					<TR>
						<TD class="SearchKey">Contact Name</TD>
				  <TD class="SearchValue" colspan="3">
							<xsl:value-of select="NewDataSet/PartnerDetails/ContactName"/>						</TD>
					</TR>
					<TR>
						<TD class="SearchKey">Company Name</TD>
						<TD colspan="3" class="SearchValue">
							<xsl:value-of select="NewDataSet/PartnerDetails/CompanyName"/>						</TD>
					</TR>
					
					<TR>
						<TD colspan="4"></TD>
					</TR>
					<TR>
						<TD colspan="4"></TD>
					</TR></TABLE></TD></TR>
					
					<TR class="SearchResultHeader">
						<TD >Customer Details</TD>
				  </TR>
					<TR>
						<TD colspan="4"></TD>
					</TR>
                    <TR>
						<TD colspan="4"><TABLE><TR>
						
						<TD  width="10%" class="gridHeader">
							User Name						</TD>
				  <TD width="7%" class="gridHeader">
							Email						</TD>
                  <TD width="12%" class="gridHeader">
							Contact Name				  </TD>
			      <TD width="7%" class="gridHeader">
							Tel. No.						</TD>
				  <TD width="7%" class="gridHeader">
							IP Address						</TD>
				  <TD width="7%" class="gridHeader">Date
                  											</TD>
				  <TD width="7%" class="gridHeader">
							Start Time						</TD>
                  <TD width="7%" class="gridHeader">
							End Time						</TD>
                  <TD width="7%" class="gridHeader">
							Visitor						</TD>
                  <TD width="7%" class="gridHeader">
							Trial						</TD>
                  <TD width="7%" class="gridHeader">
							Purchase						</TD>
                  <TD width="7%" class="gridHeader">
							Purchase Amount						</TD>
				  </TR>
					<xsl:for-each select="NewDataSet/Table">
						<xsl:choose>
							<xsl:when test="position() mod 2 = 1">
								<TR>
									
									<TD class="SearchResultItem">
										<xsl:value-of select="UserName"/>									</TD>
									<TD class="SearchResultItem">
										<xsl:value-of select="EmailAddr"/>									</TD>
									<TD class="SearchResultItem">
										<xsl:value-of select="ContactName"/>									</TD>
									<TD class="SearchResultItem">
										<xsl:value-of select="Tel"/>									</TD>
									<TD class="SearchResultItem">
										<xsl:value-of select="IPAddress"/>									</TD>
									<TD class="SearchResultItem">
										<xsl:value-of select="ms:format-date(InDate, 'yyyy-MM-dd')"/>	</TD>
                                    <TD class="SearchResultItem">
										<xsl:value-of select="ms:format-time(InDate, 'HH:mm')"/>								</TD>
                                    <TD class="SearchResultItem">
										<xsl:value-of select="ms:format-time(OutDate, 'HH:mm')"/>										</TD>
                                    <TD class="SearchResultItem">
										<xsl:value-of select="VisitorFlag"/>									</TD>
                                    <TD class="SearchResultItem">
										<xsl:value-of select="TrialFlag"/>									</TD>
                                    <TD class="SearchResultItem">
										<xsl:value-of select="PurchaseFlag"/>									</TD>
                                     <TD class="SearchResultItem">
										<xsl:value-of select="PurchaseCharge"/>									</TD>
								</TR>
							</xsl:when>
							<xsl:otherwise>
								<TR>
									
									<TD class="SearchResultAltItem">
										<xsl:value-of select="UserName"/>									</TD>
									<TD class="SearchResultAltItem">
										<xsl:value-of select="EmailAddr"/>									</TD>
									<TD class="SearchResultAltItem">
										<xsl:value-of select="ContactName"/>									</TD>
									<TD class="SearchResultAltItem">
										<xsl:value-of select="Tel"/>									</TD>
									<TD class="SearchResultAltItem">
										<xsl:value-of select="IPAddress"/>									</TD>
									<TD class="SearchResultAltItem">
										<xsl:value-of select="ms:format-date(InDate, 'yyyy-MM-dd')"/>		</TD>
                                    <TD class="SearchResultAltItem">
										<xsl:value-of select="ms:format-time(InDate, 'HH:mm')"/>	</TD>
									<TD class="SearchResultAltItem">
										<xsl:value-of select="ms:format-time(OutDate, 'HH:mm')"/>									</TD>
									<TD class="SearchResultAltItem">
										<xsl:value-of select="VisitorFlag"/>									</TD>
									<TD class="SearchResultAltItem">
										<xsl:value-of select="TrialFlag"/>									</TD>
                                    <TD class="SearchResultAltItem">
										<xsl:value-of select="PurchaseFlag"/>									</TD>
                                    <TD class="SearchResultAltItem">
										<xsl:value-of select="PurchaseCharge"/>									</TD>
								</TR>
							</xsl:otherwise>
						</xsl:choose>
					</xsl:for-each> <TR><TD ></TD></TR>
                    <TR><TD class="totalHeader">Total</TD><TD colspan="10" class="totalHeader"></TD><TD class="totalHeader"><xsl:value-of select="sum(NewDataSet/Table/PurchaseCharge)"/></TD></TR></TABLE> </TD>
					</TR>
					
                  
				</TABLE>
		</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet>