using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using System.Xml;
using DataQuicker2.Framework;
using ExportToExcel;
using Schedule;
using Shinetech.DAL;
using Shinetech.Utility;

namespace Shinetech.Services
{
    public partial class eFileReportService : ServiceBase
    {
        private static readonly string exePath = Assembly.GetExecutingAssembly().Location;
        protected ScheduleTimer  _TickTimer = new ScheduleTimer();
        protected string _DailyTime;
        protected float _StorageFee;
        protected string errorFile = string.Empty;
        protected const string strSubject = "partner report from efilefolder";
        const string mailTempletUrl = @"\Template\MailPartner.htm";

        public static string ConnectionName = "Default";
        private static string ConnectionString;
        private static ExportToExcel.ExcelExport objExport; 

        public eFileReportService()
        {
            InitializeComponent();

        }

        protected override void OnStart(string[] args)
        {
            Start();
        }

        private void TickTimer_Elapsed(object sender, ScheduledEventArgs e)
        {
            DoTask();
        }

        public void DoTask()
        {
            string path = Path.GetDirectoryName(exePath);
            Partner partner = new Partner();

            ObjectQuery query = partner.CreateQuery();

            DataTable table = new DataTable();
            XmlNode b;
            XmlNode a;
            XmlDataDocument XMLDoc;

            try
            {
                query.Fill(table);

                foreach (DataRow item in table.Rows)
                {
                    try
                    {
                        int pid = Convert.ToInt32(item["PartnerID"]);
                        string to = item["ContactEmail"].ToString();
                        string contact = item["ContactName"].ToString();
                        string companyName = item["CompanyName"].ToString();
                        DataSet ds = this.GetVisitorDataSet(pid);

                        XMLDoc = new XmlDataDocument(ds);
                        a = XMLDoc.CreateElement("PartnerDetails");
                        a.InnerXml = string.Format("<ContactName>{0}</ContactName><CompanyName>{1}</CompanyName>",  contact, companyName);
                      
                        XMLDoc.DataSet.EnforceConstraints = false;
                        b = XMLDoc.DocumentElement.FirstChild;
                        XMLDoc.DocumentElement.InsertBefore(a, b);
                        
                        string strExcelFile = objExport.TransformXMLDocumentToExcel(XMLDoc, "PartnerExcel.xsl", pid.ToString());
                        string strMailTemplet = getMailTempletStr();
                        strMailTemplet = strMailTemplet.Replace("[username]", contact);
                        strMailTemplet = strMailTemplet.Replace("[datetime]", DateTime.Now.ToShortDateString());

                        Email.SendPartnerFromExcelReportService(to, strSubject, strMailTemplet, strExcelFile);

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }


                }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp);
            }
        }


        private DataSet GetVisitorDataSet(int partnerId)
        {
            if(string.IsNullOrEmpty(ConnectionString))
            {
                ConnectionString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            }

            SqlConnection connection = new SqlConnection(ConnectionString);

            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "eFileFolder_QueryVisitors";
                command.CommandType = CommandType.StoredProcedure;

                SqlParameter param1 = command.Parameters.AddWithValue("@date", DateTime.Now.AddDays(-1));
                param1.Direction = ParameterDirection.Input;
                param1.DbType = DbType.DateTime;

                SqlParameter param2 = command.Parameters.AddWithValue("@id", partnerId);
                param2.Direction = ParameterDirection.Input;
                param2.DbType = DbType.Int32;

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;

                DataSet ds = new DataSet();

                dataAdapter.Fill(ds);

                return ds;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
           
        }


        bool IsValidEmail(string strIn)
        {
            // Return true if strIn is in valid e-mail format. 
            return Regex.IsMatch(strIn, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }

        protected override void OnStop()
        {
            _TickTimer.Stop();
          
        }

        public void init()
        {
            try
            {
                objExport = new ExcelExport();
                objExport.TempFolder = @"\Excel\Temp\";
                objExport.TemplateFolder = @"\Excel\Template\";
                objExport.XSLStyleSheetFolder = @"\Excel\XSLStyleSheet\";

                _DailyTime = ConfigurationManager.AppSettings["ReportTime"];
                errorFile = ConfigurationManager.AppSettings["LogPath"];
                ConnectionString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            }
            catch (Exception exp)
            {
                WriteErrorMessage(exp.Message);
               
            }
        }

        public void Start()
        {
            try
            {
                objExport = new ExcelExport();
                objExport.TempFolder = @"\Excel\Temp\";
                objExport.TemplateFolder = @"\Excel\Template\";
                objExport.XSLStyleSheetFolder = @"\Excel\XSLStyleSheet\";

                _DailyTime = ConfigurationManager.AppSettings["ReportTime"];
                errorFile = ConfigurationManager.AppSettings["LogPath"];
                ConnectionString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
            }catch(Exception exp)
            {
                WriteErrorMessage(exp.Message);
                return;
            }

           
            _TickTimer.Elapsed += new ScheduledEventHandler(TickTimer_Elapsed);
            if(ConfigurationManager.AppSettings["test_mode"].ToLower()=="test")
            {
                _TickTimer.AddEvent(new Schedule.ScheduledTime("Hourly", "30,0"));
            }
            else
            {
                _TickTimer.AddEvent(new Schedule.ScheduledTime("Weekly", _DailyTime));
            }
            
            
            
            _TickTimer.Start();
        }

        public void StopE()
        {
            OnStop();
        }

        public void WriteErrorMessage(string errorMsg)
        {
            string applicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            if (!applicationBase.EndsWith(@"\"))
            {
                applicationBase += @"\";
            }

            FileStream fs = null;
            try 
            {
                fs = new FileStream(applicationBase + errorFile, FileMode.Append);
                StreamWriter sw1 = new StreamWriter(fs);
                sw1.WriteLine(errorMsg+" "+DateTime.Now.ToString());
                sw1.Flush();
                sw1.Close();
            }
            finally
            {
               if(fs!=null)
                   fs.Close();
            }
        }

      

        public string getMailTempletStr()
        {
            string strMailTemplet = string.Empty;
            string applicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            try
            {
                StreamReader sr = new StreamReader(applicationBase+mailTempletUrl);
                string sLine = "";
                while (sLine != null)
                {
                    sLine = sr.ReadLine();
                    if (sLine != null)
                        strMailTemplet += sLine;
                }
                sr.Close();
            }
            catch(Exception exp)
            {
                throw new ApplicationException(exp.Message);
            }
            
            return strMailTemplet;
        }


       

       
    }


}
