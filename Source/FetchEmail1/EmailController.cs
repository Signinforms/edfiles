﻿using System.Collections.Generic;
using System.Web.Mvc;
using FormBot.Email;
using System;
using System.Xml;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Configuration;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Text;
using System.Web;
using System.Data;
using Ionic.Zip;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System.Transactions;
using System.Web.Security;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.AspNet.Identity.Owin;
using System.Net;

namespace FormBot.Main.Areas.Email.Controllers
{
    /// <summary>
    /// Email Controller
    /// </summary>
    public class EmailController
    {
        /// <summary>
        /// Checks the email.
        /// </summary>
        public void CheckEmail()
        {
            CheckMail checkMail = new CheckMail();
            checkMail.AutoCheckMailForAccount();
        }
    }
}