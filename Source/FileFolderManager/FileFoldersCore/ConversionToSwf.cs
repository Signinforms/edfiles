using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace FileFoldersCore
{
    public class ConversionToSwf
    {
        const string PROCESS_NAME = "pdf2swf.exe";

        public static string Generate(string sourcePath, string destinationPath)
        {
            Process swfprocess = new Process();
            //swfprocess.EnableRaisingEvents = true;
            swfprocess.StartInfo.UseShellExecute = true;
            swfprocess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            //swfprocess.Exited += new EventHandler(swfprocess_Exited);
            //swfprocess.ErrorDataReceived += new DataReceivedEventHandler(swfprocess_ErrorDataReceived);

            swfprocess.StartInfo.FileName = PROCESS_NAME;
            swfprocess.StartInfo.WorkingDirectory = Application.StartupPath;
            //use relatvie path
            string args = string.Format("-o {0} {1} -t -T 9 -s internallinkfunction -s externallinkfunction -f", destinationPath, sourcePath);
            swfprocess.StartInfo.Arguments = args;

            try
            {
                swfprocess.Start();
                swfprocess.WaitForExit();
                swfprocess.Close();

                return "true";
            }
            catch (Exception exp)
            {
                return "false";
            }
            finally
            {
                
            }
        }
    }
}
