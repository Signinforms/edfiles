using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Xml.Serialization;

namespace FileFoldersCore
{
    [Serializable]
    public class Divider
    {
        [XmlAttribute]
        private System.Guid _guid;
        [XmlAttribute]
        private int _rid = -1;

        [XmlAttribute]
        private string _name = "";
        [XmlAttribute]
        private Color _color = Color.White;
        [XmlAttribute]
        private int _filesCount = 0;

        [XmlAttribute(DataType = "date")]
        private DateTime _mdate;
        [XmlAttribute(DataType = "date")]
        private DateTime _cdate;

        [XmlArray("eFiles"), XmlArrayItem("eFile", typeof(eFile))]
        private System.Collections.ArrayList _efiles = new System.Collections.ArrayList();
        [XmlAttribute]
        private FileStatus _status = FileStatus.Normal;

        public Guid GUId
        {
            get
            {
                return _guid;
            }
            set
            {
                _guid = value;
            }
        }

        public int RId
        {
            get
            {
                return _rid;
            }
            set
            {
                _rid = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public int FilesCount
        {
            get
            {
                return _filesCount;
            }
            set
            {
                _filesCount = value;
            }
        }

        public DateTime LastModifiedDate
        {
            get
            {
                return _mdate;
            }
            set
            {
                _mdate = value;
            }
        }

        public DateTime CreatedDate
        {
            get
            {
                return _cdate;
            }
            set
            {
                _cdate = value;
            }
        }

        public Color DividerColor
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
            }
        }


        public System.Collections.ArrayList Files
        {
            get
            {
                return _efiles;
            }
            set
            {
                _efiles = value;
            }
        }

        public FileStatus Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

    }
}
