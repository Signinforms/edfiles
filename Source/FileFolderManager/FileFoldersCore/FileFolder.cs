using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace FileFoldersCore
{
    [Serializable]
    public class FileFolder
    {
        [XmlAttribute]
        private System.Guid _guid;
        [XmlAttribute]
        private int _rid = -1;
        [XmlAttribute]
        private string _name = "";
        [XmlAttribute]
        private int _tabcound = 0;
        [XmlAttribute]
        private string _firstName = "";
        [XmlAttribute]
        private string _lastName = "";
        [XmlAttribute]
        private string _fileNumber = "";
        [XmlAttribute]
        private int _security = 0;

        [XmlAttribute(DataType = "date")]
        private DateTime _mdate;
        [XmlAttribute(DataType = "date")]
        private DateTime _cdate;

        [XmlAttribute]
        private FileStatus _status = FileStatus.Normal;

        [XmlArray("Dividers"), XmlArrayItem("Divider", typeof(Divider))]
        private System.Collections.ArrayList _dividers = new System.Collections.ArrayList();

        public FileFolder()
        {

        }

        public Guid GUId
        {
            get
            {
                return _guid;
            }
            set
            {
                _guid = value;
            }
        }

        public int RId
        {
            get
            {
                return _rid;
            }
            set
            {
                _rid = value;
            }
        }


        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public int DividersCount
        {
            get
            {
                return _tabcound;
            }
            set
            {
                _tabcound = value;
            }
        }

        public DateTime LastModifiedDate
        {
            get
            {
                return _mdate;
            }
            set
            {
                _mdate = value;
            }
        }

        public DateTime CreatedDate
        {
            get
            {
                return _cdate;
            }
            set
            {
                _cdate = value;
            }
        }

        public System.Collections.ArrayList Dividers
        {
            get
            {
                return _dividers;
            }
            set
            {
                _dividers = value;
            }
        }

        public FileStatus Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
            }
        }

        public string FileNumber
        {
            get
            {
                return _fileNumber;
            }
            set
            {
                _fileNumber = value;
            }
        }

        public int Security
        {
            get
            {
                return _security;
            }
            set
            {
                _security = value;
            }
        }
    }
}
