using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Configuration;
using DataQuicker2.Framework;
using FFMangerDAL;

namespace FileFoldersCore
{
    public sealed class FileFolderSettings 
    {
        static public DataTable GetAllFileFolders()
        {
            DataTable table = new DataTable();

            try
            {
                FileFolderTable eff = new FileFolderTable();
                ObjectQuery query = eff.CreateQuery();
                query.Fill(table);

            }
            catch { }

            return table;

        }

        static public FileFolderTable GetFileFolder(string fid)
        {
            FileFolderTable table = new FileFolderTable(fid);
            
            if(table.IsExist)
            {
                return table;
            }
            else
            {
                return null;
            }
        }

        static public DividerTable GetDivider(string did)
        {
            DividerTable table = new DividerTable(did);
            
            if(table.IsExist)
            {
                return table;
            }
            else
            {
                return null;
            }
        }

        static public FileTable GetFileTable(string eid)
        {
            FileTable table = new FileTable(eid);
            
            if(table.IsExist)
            {
                return table;
            }
            else
            {
                return null;
            }
        }

        static public DataTable GetFileTableByDID(string did)
        {
            DataTable table = new DataTable();

            try
            {
                FileTable eff = new FileTable();
                ObjectQuery query = eff.CreateQuery();
                query.SetCriteria(eff.DividerID,did);

                query.Fill(table);

            }
            catch { }

            return table;
        }

        static public DataTable GetDividerTableByFID(string fid)
        {
            DataTable table = new DataTable();

            try
            {
                DividerTable eff = new DividerTable();
                ObjectQuery query = eff.CreateQuery();
                query.SetCriteria(eff.FolderID, fid);

                query.Fill(table);

            }
            catch { }

            return table;
        }

        static public DataRow GetDividerDataRow(string did)
        {
            DataTable table = new DataTable();

            try
            {
                DividerTable eff = new DividerTable();
                ObjectQuery query = eff.CreateQuery();
                query.SetCriteria(eff.ID, did);

                query.Fill(table);

            }
            catch { }

            return table.Rows[0];
        }

        static public DataRow GetFolderDataRow(string fid)
        {
            DataTable table = new DataTable();

            try
            {
                FileFolderTable eff = new FileFolderTable();
                ObjectQuery query = eff.CreateQuery();
                query.SetCriteria(eff.ID, fid);

                query.Fill(table);

            }
            catch { }

            return table.Rows[0];
        }

        static public DataRow GetFileDataRow(string fid)
        {
            DataTable table = new DataTable();

            try
            {
                FileTable eff = new FileTable();
                ObjectQuery query = eff.CreateQuery();
                query.SetCriteria(eff.ID, fid);

                query.Fill(table);

            }
            catch { }

            return table.Rows[0];
        }
    }
}
