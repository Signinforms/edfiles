using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace FileFoldersCore
{
    public enum FileStatus
    {
        [Description("Normal")]
        Normal=0,
        [Description("New")]
        New,
        [Description("Updated")]
        Updated,
        [Description("Deleted")]
        Deleted
    }
}
