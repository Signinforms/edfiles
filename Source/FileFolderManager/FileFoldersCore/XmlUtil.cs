using System;
using System.Drawing;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Data;
using System.Windows.Forms;
using System.Web;
using DSL.Builder;
using FFMangerDAL;

namespace FileFoldersCore
{
    public class XmlUtil
    {
        static char[] hexDigits = {
         '0', '1', '2', '3', '4', '5', '6', '7',
         '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};


        static public void GenerateXML(DataRow folder)
        {
            try
            {

                string baseRoot = Path.Combine(Application.StartupPath, "Volume");
                baseRoot = Path.Combine(baseRoot, folder["ID"].ToString());

                XMLBuilder folderBuilder = XMLBuilder.Start("FolderData");
                folderBuilder.AddXmlDeclaration("1.0", "UTF-8", "yes");

                folderBuilder.E("Folder")
                   .E("Id").T(folder["ID"].ToString()).Up()
                   .E("Name").Text(folder["FolderName"].ToString()).Up()
                   .E("FirstName").T(folder["FirstName"].ToString()).Up()
                   .E("Lastname").T(folder["Lastname"].ToString()).Up()
                   .E("Status").T(folder["FileStatus"].ToString()).Up()
                   .E("SecurityLevel").T(folder["SecurityLevel"].ToString()).Up()
                   .Up();


                DataTable table = FileFolderSettings.GetDividerTableByFID(folder["ID"].ToString());
                foreach (DataRow divider in table.Rows)
                {
                    folderBuilder = folderBuilder
                        .E("Tab")
                        .E("ID").T(divider["ID"].ToString()).Up()
                        .E("Name").T(divider["DividerName"].ToString()).Up()
                        .E("Status").T(divider["FileStatus"].ToString()).Up()
                        .E("Color").T(divider["DividerColor"].ToString()).Up().Up();//ColorToHexString
                }

                SaveXMLFile(folder["ID"].ToString(), folderBuilder, baseRoot);
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
            }

        }

        public static string ColorToHexString(Color color)
        {
            byte[] bytes = new byte[3];
            bytes[0] = color.R;
            bytes[1] = color.G;
            bytes[2] = color.B;
            char[] chars = new char[bytes.Length * 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                int b = bytes[i];
                chars[i * 2] = hexDigits[b >> 4];
                chars[i * 2 + 1] = hexDigits[b & 0xF];
            }
            return "#"+new string(chars);
        }

        static public string GenerateEFF(DataRow folder)
        {
            string baseRoot = Path.Combine(Application.StartupPath, "Volume");
            baseRoot = Path.Combine(baseRoot, folder["ID"].ToString());

            XMLBuilder folderBuilder = XMLBuilder.Start("eff");
            folderBuilder.AddXmlDeclaration("1.0", "UTF-8", "yes");
         
            folderBuilder.E("head")
                    .E("version").T("1.0").Up()
                    .E("date").Text(DateTime.Now.ToString()).Up()
                    .E("username").T("").Up().Up()
               .E("folders")
                    .E("folder")
                        .E("id").T(folder["ID"].ToString()).Up()
                        .E("Status").T(folder["FileStatus"].ToString()).Up()
                        .E("path").T(baseRoot)
                        .Up()
                    .Up();

            return SaveEFF(folder["ID"].ToString(), folderBuilder, baseRoot);
        }




        static public void GenerateXML(DataRow divider, DataRow folder)
        {
            try
            {

                string baseRoot = Path.Combine(Application.StartupPath, "Volume");
                baseRoot = Path.Combine(baseRoot, folder["ID"].ToString());
                baseRoot = Path.Combine(baseRoot, divider["ID"].ToString());

                string relateivePath = Path.Combine("Volume", folder["ID"].ToString());
                relateivePath = Path.Combine(relateivePath, divider["ID"].ToString());

                XMLBuilder folderBuilder = XMLBuilder.Start("FolderData");
                folderBuilder.AddXmlDeclaration("1.0", "UTF-8", "yes");
                folderBuilder.E("Tab")
                   .E("Id").T(divider["ID"].ToString()).Up()
                   .E("Status").T(divider["FileStatus"].ToString()).Up()
                   .E("Name").Text(divider["DividerName"].ToString()).Up()
                   .Up();


                int total = 0;

                DataTable table = FileFolderSettings.GetFileTableByDID(divider["ID"].ToString());
                foreach (DataRow efile in table.Rows)
                {
                    folderBuilder = folderBuilder
                        .E("File")
                            .E("Id").T(efile["ID"].ToString()).Up()
                            .E("Status").T(efile["FileStatus"].ToString()).Up()
                            .E("Name").T(efile["FileName"].ToString()).Up().Up();


                    string pageName = "", encodeName;

                    for (int i = 1; i <= (int)efile["PageNumber"]; i++)
                    {
                        total++;
                        encodeName = HttpUtility.UrlPathEncode(efile["FileName"].ToString());
                        encodeName = encodeName.Replace("%", "$");
                        pageName = string.Concat(relateivePath, Path.DirectorySeparatorChar, encodeName, "_", i, ".swf");
                        folderBuilder = folderBuilder
                        .E("Page")
                            .E("Id").T(total.ToString()).Up()
                            .E("FileId").T(efile["ID"].ToString()).Up()
                            .E("Url_1").T(pageName).Up().Up();
                    }
                }

                SaveXMLFile(divider["ID"].ToString(), folderBuilder, baseRoot);
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp.Message);
            }

        }


        static public void SaveXMLFile(string name, XMLBuilder builder, string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filename = Path.Combine(path, name+".xml");
            XmlTextWriter writer = new XmlTextWriter(filename, Encoding.UTF8);

            builder.ToWriter(writer);
            writer.Flush();
            writer.Close();
        }

        static public string SaveEFF(string name, XMLBuilder builder, string path)
        {

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string filename = Path.Combine(path, name + ".eff");
            XmlTextWriter writer = new XmlTextWriter(filename,Encoding.UTF8);
            builder.ToWriter(writer);
            writer.Close();

            return filename;


        }
    }
}
