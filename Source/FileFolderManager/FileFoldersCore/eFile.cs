using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace FileFoldersCore
{
    [Serializable]
    public class eFile
    {
        [XmlAttribute]
        private System.Guid _guid;
        [XmlAttribute]
        private int _rid = -1;
        [XmlAttribute]
        private float _size = 0;
        [XmlAttribute]
        private string _name = "";
        [XmlAttribute]
        private string _fullname = "";
        [XmlAttribute]
        private bool _isDirty = false;

        [XmlAttribute(DataType = "date")]
        private DateTime _mdate;
        [XmlAttribute(DataType = "date")]
        private DateTime _cdate;

        [XmlAttribute]
        private int _pageCount = 0;

        [XmlAttribute]
        private FileStatus _status = FileStatus.Normal;

        public eFile()
		{
		}

        public Guid GUId
        {
            get
            {
                return _guid;
            }
            set
            {
                _guid = value;
            }
        }

        public int RId
        {
            get
            {
                return _rid;
            }
            set
            {
                _rid = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public string FullName
        {
            get
            {
                return _fullname;
            }
            set
            {
                _fullname = value;
            }
        }


        public FileStatus Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public DateTime LastModifiedDate
        {
            get
            {
                return _mdate;
            }
            set
            {
                _mdate = value;
            }
        }

        public DateTime CreatedDate
        {
            get
            {
                return _cdate;
            }
            set
            {
                _cdate = value;
            }
        }

        public int PageCount
        {
            get
            {
                return _pageCount;
            }
            set
            {
                _pageCount = value;
            }
        }

        public float FileSize
        {
            get
            {
                return _size;
            }
            set
            {
                _size = value;
            }
        }

    }
}
