using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Drawing;
using FileFoldersCore;
using CommonTools;
using FFMangerDAL;

namespace SyncFileFolderManager
{
    public class EFFTree : CommonTools.TreeListView
	{
		ContextMenu m_contextMenu = null;
        ContextMenu m_contextMenutab = null;
        ContextMenu m_contextMenuFile = null;
        ContextMenu m_contextMenuFolder = null;

        public event FolderEventHandler FolderEvent;
        private DataTable _effSetting = null;
        private UserSettings _settings = null;

        public Node _selectNode;

        public delegate void FolderEventHandler(object sender, EventArgs e);

        public EFFTree()
		{
			m_contextMenu = new ContextMenu();
            m_contextMenu.MenuItems.Add(new MenuItem("Add eFileFolder", new EventHandler(OnAddeFileFolder)));         
            m_contextMenu.MenuItems.Add("-");
            m_contextMenu.MenuItems.Add(new MenuItem("Collapse All Children", new EventHandler(OnCollapseAllChildren)));
            m_contextMenu.MenuItems.Add(new MenuItem("Expand All Children", new EventHandler(OnExpandAllChildren)));

            m_contextMenuFolder = new ContextMenu();
            m_contextMenuFolder.MenuItems.Add(new MenuItem("Preview eFileFolder", new EventHandler(OnPreviewFolder)));
            m_contextMenuFolder.MenuItems.Add("-");

            m_contextMenuFolder.MenuItems.Add(new MenuItem("Update eFileFolder", new EventHandler(OnUpdateFileFolder)));
            m_contextMenuFolder.MenuItems.Add(new MenuItem("Delete eFileFolder", new EventHandler(OnDeleteFileFolder)));
            m_contextMenuFolder.MenuItems.Add("-");
            m_contextMenuFolder.MenuItems.Add(new MenuItem("Add Tab", new EventHandler(OnAddDivider)));
            m_contextMenuFolder.MenuItems.Add("-");

            m_contextMenuFolder.MenuItems.Add(new MenuItem("Collapse All Children", new EventHandler(OnCollapseAllChildren)));
            m_contextMenuFolder.MenuItems.Add(new MenuItem("Expand All Children", new EventHandler(OnExpandAllChildren)));

            m_contextMenutab = new ContextMenu();

            m_contextMenutab.MenuItems.Add(new MenuItem("Update Tab", new EventHandler(OnUpdateDivider)));
            m_contextMenutab.MenuItems.Add(new MenuItem("Delete Tab", new EventHandler(OnDeleteDivider)));
            m_contextMenutab.MenuItems.Add("-");

            m_contextMenutab.MenuItems.Add(new MenuItem("Collapse All Children", new EventHandler(OnCollapseAllChildren)));
            m_contextMenutab.MenuItems.Add(new MenuItem("Expand All Children", new EventHandler(OnExpandAllChildren)));

            m_contextMenuFile = new ContextMenu();
            m_contextMenuFile.MenuItems.Add(new MenuItem("Delete eFile", new EventHandler(OnDeleteFile)));
            m_contextMenuFile.MenuItems.Add("-");

            m_contextMenuFile.MenuItems.Add(new MenuItem("Collapse All Children", new EventHandler(OnCollapseAllChildren)));
            m_contextMenuFile.MenuItems.Add(new MenuItem("Expand All Children", new EventHandler(OnExpandAllChildren)));
		}

        public void OnPreviewFolder(object sender, EventArgs e)
        {
           //generate eff xml file

            if (_selectNode!=null)
            {
                DataRow folder = _selectNode.Tag as DataRow;
                string path = XmlUtil.GenerateEFF(folder);

                //preview
                Process p = new Process();
                p.StartInfo.FileName = path;
                p.StartInfo.Arguments = _settings.UID + "|" + _settings.Password;
                p.StartInfo.UseShellExecute = true;

                try
                {

                    p.Start();
                }
                catch
                {
                    MessageBox.Show("Please check whether you have installed the offline viewer.","Warning",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                }
            }
            
        }

        public void OnDeleteFileFolder(object sender, EventArgs e)
        {
            DataRow row = _selectNode.Tag as DataRow;
            if (row == null || row.Table.Columns.Contains("FolderName") == false) return;

            if (MessageBox.Show(this, "Are you sure to delete this eFileFolder,Yes/No?", "Exclamation", MessageBoxButtons.YesNo,
                MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                DataRow folder = _selectNode.Tag as DataRow;
                if (folder == null) return;
                folder["FileStatus"] = FileStatus.Deleted;
                this.BeginUpdate();
                _selectNode.SetData(new object[4] { folder["FolderName"].ToString(), null, null, null });

                FileFolderTable table = new FileFolderTable(folder["ID"].ToString());
                table.FileStatus.Value = (int)FileStatus.Deleted;
                table.Update();
                folder.AcceptChanges();
                this.Nodes.Remove(_selectNode);
                this.EndUpdate();

            }
        }

        public void OnDeleteDivider(object sender, EventArgs e)
        {
            DataRow row = _selectNode.Tag as DataRow;
            if (row == null || row.Table.Columns.Contains("DividerName") == false) return;

            if (MessageBox.Show(this, "Are you sure to delete this tab,Yes/No?", "Exclamation", MessageBoxButtons.YesNo,
                MessageBoxIcon.Exclamation) == DialogResult.Yes)
            {
                Node folderNode = _selectNode.Parent;
                if (folderNode.Nodes.Count == 1)
                {
                    MessageBox.Show(this, "You can't remove the last one tab of this efilefolder", "Exclamation",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                    return;
                }
                DataRow divider = _selectNode.Tag as DataRow;
                divider["FileStatus"] = FileStatus.Deleted;
                DataRow folder = _selectNode.Parent.Tag as DataRow;
                
                this.BeginUpdate();
                _selectNode.SetData(new object[4] { divider["DividerName"].ToString(), null, null, null });
                DividerTable table = new DividerTable(divider["ID"].ToString());
                table.FileStatus.Value =(int) FileStatus.Deleted;
                table.Update();
                divider.AcceptChanges();
                this.EndUpdate();

                
                folderNode.Nodes.Remove(_selectNode);

                XmlUtil.GenerateXML(folder);

            }
        }

        public void OnDeleteFile(object sender, EventArgs e)
        {
            DataRow row = _selectNode.Tag as DataRow;
            if (row == null || row.Table.Columns.Contains("FileName") == false) return;

            if(MessageBox.Show(this,"Are you sure to delete this file,Yes/No?","Exclamation",MessageBoxButtons.YesNo,
                MessageBoxIcon.Exclamation)==DialogResult.Yes)
            {
                DataRow efile = _selectNode.Tag as DataRow;
                DataRow divider = _selectNode.Parent.Tag as DataRow;
                DataRow folder = _selectNode.Parent.Parent.Tag as DataRow;
                Node dividerNode = _selectNode.Parent;
                efile["FileStatus"] = FileStatus.Deleted;

                this.BeginUpdate();
                _selectNode.SetData(new object[4] { efile["FileName"].ToString(), null, null, null });
                FileTable table = new FileTable(efile["ID"].ToString());
                table.FileStatus.Value = (int)FileStatus.Deleted;
                table.Update();

                efile.AcceptChanges();
                dividerNode.Nodes.Remove(_selectNode);
                
                this.EndUpdate();

                XmlUtil.GenerateXML(divider,folder);
                
            }
        }

        public DataTable Settings
        {
            set
            {
                _effSetting = value;
            }
        }

        public UserSettings userSettings
        {
            set
            {
                this._settings = value;
            }
        }

        public void OnUpdateFileFolder(object sender, EventArgs e)
        {
            DataRow row = _selectNode.Tag as DataRow;
            if (row == null || row.Table.Columns.Contains("FolderName") == false) return;

            FormUpdateFolder dlg = new FormUpdateFolder(_selectNode.Tag as DataRow);
            if (dlg.ShowDialog(this) == DialogResult.OK)
            {
                DataRow folder = _selectNode.Tag as DataRow;

                if (folder == null) return;
                string oldKey = folder["FolderName"].ToString();

                folder["FolderName"] = dlg.FolderName;
                folder["FirstName"] = dlg.FirstName;
                folder["LastName"] = dlg.LastName;
                folder["FileNumber"] = dlg.FileNumber;
                folder["SecurityLevel"] = dlg.Security;
                folder["FolderCode"] = dlg.FolderCode;

                int status = (int)folder["FileStatus"];
                status = status == (int)FileStatus.New ? (int)FileStatus.New : (int)FileStatus.Updated;

                folder["FileStatus"] = status;

                this.BeginUpdate();
                folder.AcceptChanges();
                _selectNode.SetData(new object[4] { folder["FolderName"].ToString(), null, null, null });

                //save into database
                FileFolderTable table = new FileFolderTable(folder["ID"].ToString());
                table.FolderName.Value = folder["FolderName"].ToString();
                table.Lastname.Value = folder["FirstName"].ToString();
                table.FileNumber.Value = folder["FileNumber"].ToString();
                table.SecurityLevel.Value = (int)folder["SecurityLevel"];
                table.FileStatus.Value = (int)folder["FileStatus"];
                table.FolderCode.Value = folder["FolderCode"].ToString();
                table.Update();

                this.EndUpdate();

                XmlUtil.GenerateXML(folder);

            }
        }

        public void OnUpdateDivider(object sender, EventArgs e)
        {
            DataRow row = _selectNode.Tag as DataRow;
            if (row == null || row.Table.Columns.Contains("DividerName") == false) return;

            FormUpdateDivider dlg = new FormUpdateDivider(_selectNode.Tag as DataRow);
            if (dlg.ShowDialog(this) == DialogResult.OK)
            {
                DataRow divider = _selectNode.Tag as DataRow;
                DataRow folder = _selectNode.Parent.Tag as DataRow;
                if (divider == null || folder == null) return;
                divider["DividerColor"] = XmlUtil.ColorToHexString(dlg.DividerColor);
                divider["DividerName"] = dlg.DividerName;
                int status = (int)divider["FileStatus"];
                status = status == (int)FileStatus.New ? (int)FileStatus.New : (int)FileStatus.Updated;
                divider["FileStatus"] = status;

                this.BeginUpdate();
                divider.AcceptChanges();
                _selectNode.SetData(new object[4] { divider["DividerName"].ToString(), null, null, null });

                DividerTable table = new DividerTable(divider["ID"].ToString());
                table.DividerName.Value = dlg.DividerName;
                table.DividerColor.Value = XmlUtil.ColorToHexString(dlg.DividerColor);
                table.FileStatus.Value = (int)divider["FileStatus"];
                table.Update();

                this.EndUpdate();

                XmlUtil.GenerateXML(folder);

            }
        }

        public void OnAddDivider(object sender, EventArgs e)
        {
            DataRow row = _selectNode.Tag as DataRow;
            if (row == null || row.Table.Columns.Contains("FolderName") == false) return;

            FormUpdateDivider dlg = new FormUpdateDivider(_selectNode.Tag as DataRow,true);
            if (dlg.ShowDialog(this) == DialogResult.OK)
            {
                DataRow folder = _selectNode.Tag as DataRow;

                DividerTable dvd = new DividerTable();
                dvd.ID.Value = Guid.NewGuid().ToString();
                dvd.DividerName.Value = dlg.DividerName;
                dvd.DividerColor.Value = XmlUtil.ColorToHexString(dlg.DividerColor);
                dvd.FileStatus.Value = (int)FileStatus.New;
                dvd.FolderID.Value = folder["ID"].ToString();

                dvd.Create();

                Node tabNode = new CommonTools.Node(new object[4] { dvd.DividerName, null, null, null });
                tabNode.Tag = FileFolderSettings.GetDividerDataRow(dvd.ID.Value); ;
                
                this.BeginUpdate();
                _selectNode.Nodes.Add(tabNode);
           
                this.EndUpdate();

                XmlUtil.GenerateXML(folder);
            }
        }


	    public void OnAddeFileFolder(object sender, EventArgs e)
	    {

	        FormNewFileFolder dlg = new FormNewFileFolder(false);
	        if(dlg.ShowDialog(this)==DialogResult.OK)
	        {
                try
                {
                    CommonTools.Node node, tabNode;

                    FileFolderTable folder = new FileFolderTable();
                    folder.ID.Value = Guid.NewGuid().ToString();
                    folder.FolderName.Value = dlg.FolderName;
                    folder.FirstName.Value = dlg.FirstName;
                    folder.Lastname.Value = dlg.LastName;
                    folder.FileNumber.Value = dlg.FileNumber;
                    folder.SecurityLevel.Value = dlg.Security;
                    folder.FolderCode.Value = dlg.FolderCode;
                    
                    folder.DividersCount.Value = dlg.DividerCount;
                    folder.FileStatus.Value = (int)FileStatus.New;

                    folder.Create();

                    node = new CommonTools.Node(new object[4] { dlg.FolderName, null, null, null });

                    DataRow oldRow = FileFolderSettings.GetFolderDataRow(folder.ID.Value);

                    DataRow newRow = _effSetting.NewRow();
                    CreateNewDataRow(newRow, oldRow);
                    _effSetting.Rows.Add(newRow);
                    node.Tag = newRow;

                    this.BeginUpdate();
                    foreach (DataGridViewRow item in dlg.Dividers)
                    {
                        DividerTable dvd = new DividerTable();
                        dvd.ID.Value = Guid.NewGuid().ToString();
                        dvd.DividerName.Value = item.Cells[0].Value as string;
                        dvd.DividerColor.Value = XmlUtil.ColorToHexString((Color)item.Cells[1].Value);
                        dvd.FileStatus.Value = (int)FileStatus.New;
                        dvd.FolderID.Value = folder.ID.Value;
                        dvd.Create();

                        tabNode = new CommonTools.Node(new object[4] { item.Cells[0].Value.ToString(), null, null, null });

                        tabNode.Tag = FileFolderSettings.GetDividerDataRow(dvd.ID.Value);
                        node.Nodes.Add(tabNode);
                    }

                    this.Nodes.Add(node);

                    this.EndUpdate();

                    XmlUtil.GenerateXML(newRow);

                    node.Expand();
                }
                catch (Exception exp)
                {
                   // MessageBox.Show(exp.ToString());
                }
	        }
	    }

        private void CreateNewDataRow(DataRow newRow, DataRow oldRow)
        {
            foreach (DataColumn col in oldRow.Table.Columns)
            {
                newRow[col.ColumnName] = oldRow[col.ColumnName];
            }

        }

        public void OnCollapseAllChildren(object sender, EventArgs e)
		{
			BeginUpdate();
			if (MultiSelect && NodesSelection.Count > 0)
			{
				foreach (CommonTools.Node selnode in NodesSelection)
				{
					foreach (CommonTools.Node node in selnode.Nodes)
						node.Collapse();
				}
				NodesSelection.Clear();
			}
			if (FocusedNode != null && FocusedNode.HasChildren)
			{
				foreach (CommonTools.Node node in FocusedNode.Nodes)
					node.Collapse();
			}
			EndUpdate();
		}
        public void OnExpandAllChildren(object sender, EventArgs e)
		{
            
			BeginUpdate();
			if (MultiSelect && NodesSelection.Count > 0)
			{
				foreach (CommonTools.Node selnode in NodesSelection)
					selnode.ExpandAll();
				NodesSelection.Clear();
			}
			if (FocusedNode != null)
				FocusedNode.ExpandAll();
			EndUpdate();
		}
        public void OnDeleteSelectedNode(object sender, EventArgs e)
		{
			BeginUpdate();
			CommonTools.Node node = FocusedNode;
			if (node != null && node.Owner != null)
			{
				node.Collapse();
				CommonTools.Node nextnode = CommonTools.NodeCollection.GetNextNode(node, 1);
				if (nextnode == null)
					nextnode = CommonTools.NodeCollection.GetNextNode(node, -1);
				node.Owner.Remove(node);
				FocusedNode = nextnode;
			}
			EndUpdate();
		}
		protected override void BeforeShowContextMenu()
		{
            ContextMenu = m_contextMenu;
            Node node = GetHitNode();
		    _selectNode = node;

            if (node == null)
            {
                ContextMenu = m_contextMenu;
            }
            else
            {
                if (node.Tag != null && node.Tag is DataRow)
                {
                    DataRow row = node.Tag as DataRow;

                    if (row.Table.Columns.Contains("FolderName"))
                    {
                        ContextMenu = m_contextMenuFolder;
                    }

                    if (row.Table.Columns.Contains("DividerName"))
                    {
                        ContextMenu = m_contextMenutab;
                    }
                    else if (row.Table.Columns.Contains("FileName"))
                    {
                        ContextMenu = m_contextMenuFile;
                    }
                }
                
            }
		}
		protected override object GetData(CommonTools.Node node, CommonTools.TreeListColumn column)
		{
			object data = base.GetData(node, column);
			if (data != null)
				return data;

            if (column.Fieldname == "tabcount")
			{
				if (node.HasChildren)
					return node.Nodes.Count;
				return "";
			}

            if (column.Fieldname == "status")
            {
                FileStatus status = FileStatus.Normal;
                if (node.Tag is DataRow)
                {
                    DataRow item = node.Tag as DataRow;
                    status = (FileStatus)item["FileStatus"];
                }
                
                if(status ==FileStatus.Normal)
                {
                    return string.Empty;
                }
                else
                {
                    return status;
                }

            }
            
			return string.Empty;
		}



        protected override CommonTools.TreeList.TextFormatting GetFormatting(CommonTools.Node node, CommonTools.TreeListColumn column)
        {
            CommonTools.TreeList.TextFormatting format = new CommonTools.TreeList.TextFormatting(column.CellFormat);

            if (column.Fieldname == "tabcolor")
            {
                if(node.Tag is DataRow)
                {
                    DataRow dvd = node.Tag as DataRow;
                    if (dvd.Table.Columns.Contains("DividerColor"))
                    {
                        format.BackColor = ColorTranslator.FromHtml(dvd["DividerColor"].ToString());
                    }
                    
                }
                
            }

            return format;
        }

        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // EFFTree
            // 
            this.AllowDrop = true;
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        public static string GetDescription(object enumValue, string defDesc)
        {
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

            if (null != fi)
            {
                object[] attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }

            return defDesc;
        }
  
	}
}
