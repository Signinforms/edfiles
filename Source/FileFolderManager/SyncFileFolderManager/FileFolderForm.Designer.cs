namespace SyncFileFolderManager
{
    partial class FileFolderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            CommonTools.TreeListColumn treeListColumn1 = new CommonTools.TreeListColumn("name", "Name");
            CommonTools.TreeListColumn treeListColumn2 = new CommonTools.TreeListColumn("tabcount", "TabCount");
            CommonTools.TreeListColumn treeListColumn3 = new CommonTools.TreeListColumn("tabcolor", "Color");
            CommonTools.TreeListColumn treeListColumn4 = new CommonTools.TreeListColumn("status", "Status");
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("3.5 软盘 (A:)", 21, 22, new System.Windows.Forms.TreeNode[] {
            treeNode1});
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("LU6 (C:)", 23, 24, new System.Windows.Forms.TreeNode[] {
            treeNode3});
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("LU7 (D:)", 23, 24, new System.Windows.Forms.TreeNode[] {
            treeNode5});
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("LU8 (E:)", 23, 24, new System.Windows.Forms.TreeNode[] {
            treeNode7});
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("LU9 (F:)", 23, 24, new System.Windows.Forms.TreeNode[] {
            treeNode9});
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("新建 (G:)", 25, 26, new System.Windows.Forms.TreeNode[] {
            treeNode11});
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("DVD 驱动器 (H:)", 27, 28, new System.Windows.Forms.TreeNode[] {
            treeNode13});
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("控制面板", 19, 20, new System.Windows.Forms.TreeNode[] {
            treeNode15});
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("Administrator 的文档", 16, 18, new System.Windows.Forms.TreeNode[] {
            treeNode17});
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("共享文档", 16, 18, new System.Windows.Forms.TreeNode[] {
            treeNode19});
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("我的电脑", 8, 9, new System.Windows.Forms.TreeNode[] {
            treeNode2,
            treeNode4,
            treeNode6,
            treeNode8,
            treeNode10,
            treeNode12,
            treeNode14,
            treeNode16,
            treeNode18,
            treeNode20});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileFolderForm));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logOffToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripSeparator();
            this.addTabToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateTabToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteTabToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripSeparator();
            this.removeFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.synchronizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripSeparator();
            this.optionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helperToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeListView2 = new SyncFileFolderManager.EFFTree();
            this.browser1 = new FileBrowser.Browser();
            this.browserPluginWrapper1 = new FileBrowser.BrowserPluginWrapper();
            this.browserPluginWrapper2 = new FileBrowser.BrowserPluginWrapper();
            this.previewEFileFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem6 = new System.Windows.Forms.ToolStripSeparator();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.treeListView2)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 548);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(808, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolToolStripMenuItem,
            this.helperToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(808, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logpToolStripMenuItem,
            this.logOffToolStripMenuItem,
            this.toolStripMenuItem1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // logpToolStripMenuItem
            // 
            this.logpToolStripMenuItem.Name = "logpToolStripMenuItem";
            this.logpToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
            this.logpToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.logpToolStripMenuItem.Text = "Log On...";
            this.logpToolStripMenuItem.Click += new System.EventHandler(this.logpToolStripMenuItem_Click);
            // 
            // logOffToolStripMenuItem
            // 
            this.logOffToolStripMenuItem.Enabled = false;
            this.logOffToolStripMenuItem.Name = "logOffToolStripMenuItem";
            this.logOffToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.logOffToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.logOffToolStripMenuItem.Text = "Log Off";
            this.logOffToolStripMenuItem.Click += new System.EventHandler(this.logOffToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(162, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(165, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolToolStripMenuItem
            // 
            this.toolToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.previewEFileFolderToolStripMenuItem,
            this.toolStripMenuItem6,
            this.createFolderToolStripMenuItem,
            this.updateFolderToolStripMenuItem,
            this.deleteFolderToolStripMenuItem,
            this.toolStripMenuItem4,
            this.addTabToolStripMenuItem,
            this.updateTabToolStripMenuItem,
            this.deleteTabToolStripMenuItem,
            this.toolStripMenuItem5,
            this.removeFileToolStripMenuItem,
            this.toolStripMenuItem2,
            this.synchronizeToolStripMenuItem,
            this.toolStripMenuItem3,
            this.optionToolStripMenuItem});
            this.toolToolStripMenuItem.Name = "toolToolStripMenuItem";
            this.toolToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolToolStripMenuItem.Text = "&Tools";
            this.toolToolStripMenuItem.DropDownOpening += new System.EventHandler(this.toolToolStripMenuItem_DropDownOpening);
            // 
            // createFolderToolStripMenuItem
            // 
            this.createFolderToolStripMenuItem.Name = "createFolderToolStripMenuItem";
            this.createFolderToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
            this.createFolderToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.createFolderToolStripMenuItem.Text = "Create eFileFolder";
            this.createFolderToolStripMenuItem.Click += new System.EventHandler(this.createFolderToolStripMenuItem_Click);
            // 
            // updateFolderToolStripMenuItem
            // 
            this.updateFolderToolStripMenuItem.Name = "updateFolderToolStripMenuItem";
            this.updateFolderToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.E)));
            this.updateFolderToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.updateFolderToolStripMenuItem.Text = "Update eFileFolder";
            this.updateFolderToolStripMenuItem.Click += new System.EventHandler(this.updateFolderToolStripMenuItem_Click);
            // 
            // deleteFolderToolStripMenuItem
            // 
            this.deleteFolderToolStripMenuItem.Name = "deleteFolderToolStripMenuItem";
            this.deleteFolderToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
                        | System.Windows.Forms.Keys.E)));
            this.deleteFolderToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.deleteFolderToolStripMenuItem.Text = "Delete eFileFolder";
            this.deleteFolderToolStripMenuItem.Click += new System.EventHandler(this.deleteFolderToolStripMenuItem_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(252, 6);
            // 
            // addTabToolStripMenuItem
            // 
            this.addTabToolStripMenuItem.Name = "addTabToolStripMenuItem";
            this.addTabToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.addTabToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.addTabToolStripMenuItem.Text = "Add Tab";
            this.addTabToolStripMenuItem.Click += new System.EventHandler(this.addTabToolStripMenuItem_Click);
            // 
            // updateTabToolStripMenuItem
            // 
            this.updateTabToolStripMenuItem.Name = "updateTabToolStripMenuItem";
            this.updateTabToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.T)));
            this.updateTabToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.updateTabToolStripMenuItem.Text = "Update Tab";
            this.updateTabToolStripMenuItem.Click += new System.EventHandler(this.updateTabToolStripMenuItem_Click);
            // 
            // deleteTabToolStripMenuItem
            // 
            this.deleteTabToolStripMenuItem.Name = "deleteTabToolStripMenuItem";
            this.deleteTabToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
                        | System.Windows.Forms.Keys.T)));
            this.deleteTabToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.deleteTabToolStripMenuItem.Text = "Delete Tab";
            this.deleteTabToolStripMenuItem.Click += new System.EventHandler(this.deleteTabToolStripMenuItem_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(252, 6);
            // 
            // removeFileToolStripMenuItem
            // 
            this.removeFileToolStripMenuItem.Name = "removeFileToolStripMenuItem";
            this.removeFileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
                        | System.Windows.Forms.Keys.F)));
            this.removeFileToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.removeFileToolStripMenuItem.Text = "Delete File";
            this.removeFileToolStripMenuItem.Click += new System.EventHandler(this.removeFileToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(252, 6);
            // 
            // synchronizeToolStripMenuItem
            // 
            this.synchronizeToolStripMenuItem.Enabled = false;
            this.synchronizeToolStripMenuItem.Name = "synchronizeToolStripMenuItem";
            this.synchronizeToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.S)));
            this.synchronizeToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.synchronizeToolStripMenuItem.Text = "Synchronize";
            this.synchronizeToolStripMenuItem.Click += new System.EventHandler(this.synchronizeToolStripMenuItem_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(252, 6);
            // 
            // optionToolStripMenuItem
            // 
            this.optionToolStripMenuItem.Enabled = false;
            this.optionToolStripMenuItem.Name = "optionToolStripMenuItem";
            this.optionToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.O)));
            this.optionToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.optionToolStripMenuItem.Text = "Option";
            // 
            // helperToolStripMenuItem
            // 
            this.helperToolStripMenuItem.Name = "helperToolStripMenuItem";
            this.helperToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.helperToolStripMenuItem.Text = "&Help";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(207)))), ((int)(((byte)(214)))), ((int)(((byte)(228)))));
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(70, 50);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton3,
            this.toolStripSeparator1,
            this.toolStripButton4,
            this.toolStripButton5});
            this.toolStrip1.Location = new System.Drawing.Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(808, 57);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::SyncFileFolderManager.Properties.Resources.Signin_1;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(74, 54);
            this.toolStripButton1.Text = "Connect";
            this.toolStripButton1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.toolStripButton1_MouseUp);
            this.toolStripButton1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.toolStripButton1_MouseDown);
            this.toolStripButton1.MouseLeave += new System.EventHandler(this.toolStripButton1_MouseLeave);
            this.toolStripButton1.MouseEnter += new System.EventHandler(this.toolStripButton1_MouseEnter);
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::SyncFileFolderManager.Properties.Resources.synchronize_4;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(74, 54);
            this.toolStripButton3.Text = "Synchronize";
            this.toolStripButton3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.toolStripButton3_MouseUp);
            this.toolStripButton3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.toolStripButton3_MouseDown);
            this.toolStripButton3.MouseLeave += new System.EventHandler(this.toolStripButton3_MouseLeave);
            this.toolStripButton3.MouseEnter += new System.EventHandler(this.toolStripButton3_MouseEnter);
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 57);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = global::SyncFileFolderManager.Properties.Resources.option_1;
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(74, 54);
            this.toolStripButton4.Text = "Option";
            this.toolStripButton4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.toolStripButton4_MouseUp);
            this.toolStripButton4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.toolStripButton4_MouseDown);
            this.toolStripButton4.MouseLeave += new System.EventHandler(this.toolStripButton4_MouseLeave);
            this.toolStripButton4.MouseEnter += new System.EventHandler(this.toolStripButton4_MouseEnter);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = global::SyncFileFolderManager.Properties.Resources.help_1;
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(74, 54);
            this.toolStripButton5.Text = "Help";
            this.toolStripButton5.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.toolStripButton5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.toolStripButton5_MouseUp);
            this.toolStripButton5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.toolStripButton5_MouseDown);
            this.toolStripButton5.MouseLeave += new System.EventHandler(this.toolStripButton5_MouseLeave);
            this.toolStripButton5.MouseEnter += new System.EventHandler(this.toolStripButton5_MouseEnter);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 81);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeListView2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.browser1);
            this.splitContainer1.Size = new System.Drawing.Size(808, 467);
            this.splitContainer1.SplitterDistance = 343;
            this.splitContainer1.TabIndex = 3;
            // 
            // treeListView2
            // 
            this.treeListView2.AllowDrop = true;
            treeListColumn1.AutoSizeMinSize = 0;
            treeListColumn1.Width = 120;
            treeListColumn2.AutoSizeMinSize = 0;
            treeListColumn2.Width = 60;
            treeListColumn3.AutoSizeMinSize = 0;
            treeListColumn3.Width = 80;
            treeListColumn4.AutoSizeMinSize = 0;
            treeListColumn4.Width = 60;
            this.treeListView2.Columns.AddRange(new CommonTools.TreeListColumn[] {
            treeListColumn1,
            treeListColumn2,
            treeListColumn3,
            treeListColumn4});
            this.treeListView2.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.treeListView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeListView2.Enabled = true;
            this.treeListView2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeListView2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.treeListView2.Images = null;
            this.treeListView2.Location = new System.Drawing.Point(0, 0);
            this.treeListView2.MultiSelect = false;
            this.treeListView2.Name = "treeListView2";
            this.treeListView2.Size = new System.Drawing.Size(343, 467);
            this.treeListView2.TabIndex = 1;
            
            this.treeListView2.ViewOptions.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeListView2.ViewOptions.ShowLine = false;
            this.treeListView2.DragDrop += new System.Windows.Forms.DragEventHandler(this.treeListView2_DragDrop);
            this.treeListView2.DragEnter += new System.Windows.Forms.DragEventHandler(this.treeListView2_DragEnter);
            // 
            // browser1
            // 
            this.browser1.AllowDrop = true;
            this.browser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.browser1.ListViewMode = System.Windows.Forms.View.List;
            this.browser1.Location = new System.Drawing.Point(0, 0);
            this.browser1.Name = "browser1";
            treeNode1.Name = "";
            treeNode1.Text = "";
            treeNode2.ImageIndex = 21;
            treeNode2.Name = "3.5 软盘 (A:)";
            treeNode2.SelectedImageIndex = 22;
            treeNode2.Text = "3.5 软盘 (A:)";
            treeNode3.Name = "";
            treeNode3.Text = "";
            treeNode4.ImageIndex = 23;
            treeNode4.Name = "LU6 (C:)";
            treeNode4.SelectedImageIndex = 24;
            treeNode4.Text = "LU6 (C:)";
            treeNode5.Name = "";
            treeNode5.Text = "";
            treeNode6.ImageIndex = 23;
            treeNode6.Name = "LU7 (D:)";
            treeNode6.SelectedImageIndex = 24;
            treeNode6.Text = "LU7 (D:)";
            treeNode7.Name = "";
            treeNode7.Text = "";
            treeNode8.ImageIndex = 23;
            treeNode8.Name = "LU8 (E:)";
            treeNode8.SelectedImageIndex = 24;
            treeNode8.Text = "LU8 (E:)";
            treeNode9.Name = "";
            treeNode9.Text = "";
            treeNode10.ImageIndex = 23;
            treeNode10.Name = "LU9 (F:)";
            treeNode10.SelectedImageIndex = 24;
            treeNode10.Text = "LU9 (F:)";
            treeNode11.Name = "";
            treeNode11.Text = "";
            treeNode12.ImageIndex = 25;
            treeNode12.Name = "新建 (G:)";
            treeNode12.SelectedImageIndex = 26;
            treeNode12.Text = "新建 (G:)";
            treeNode13.Name = "";
            treeNode13.Text = "";
            treeNode14.ImageIndex = 27;
            treeNode14.Name = "DVD 驱动器 (H:)";
            treeNode14.SelectedImageIndex = 28;
            treeNode14.Text = "DVD 驱动器 (H:)";
            treeNode15.Name = "";
            treeNode15.Text = "";
            treeNode16.ImageIndex = 19;
            treeNode16.Name = "控制面板";
            treeNode16.SelectedImageIndex = 20;
            treeNode16.Text = "控制面板";
            treeNode17.Name = "";
            treeNode17.Text = "";
            treeNode18.ImageIndex = 16;
            treeNode18.Name = "Administrator 的文档";
            treeNode18.SelectedImageIndex = 18;
            treeNode18.Text = "Administrator 的文档";
            treeNode19.Name = "";
            treeNode19.Text = "";
            treeNode20.ImageIndex = 16;
            treeNode20.Name = "共享文档";
            treeNode20.SelectedImageIndex = 18;
            treeNode20.Text = "共享文档";
            treeNode21.ImageIndex = 8;
            treeNode21.Name = "我的电脑";
            treeNode21.SelectedImageIndex = 9;
            treeNode21.Text = "我的电脑";
            this.browser1.SelectedNode = treeNode21;
            this.browser1.Size = new System.Drawing.Size(461, 467);
            this.browser1.SplitterDistance = 162;
            this.browser1.TabIndex = 0;
            // 
            // previewEFileFolderToolStripMenuItem
            // 
            this.previewEFileFolderToolStripMenuItem.Name = "previewEFileFolderToolStripMenuItem";
            this.previewEFileFolderToolStripMenuItem.Size = new System.Drawing.Size(255, 22);
            this.previewEFileFolderToolStripMenuItem.Text = "Preview eFileFolder";
            // 
            // toolStripMenuItem6
            // 
            this.toolStripMenuItem6.Name = "toolStripMenuItem6";
            this.toolStripMenuItem6.Size = new System.Drawing.Size(252, 6);
            // 
            // FileFolderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 570);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FileFolderForm";
            this.Text = "eFileFolder Manager";
            this.Load += new System.EventHandler(this.FileFolderForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FileFolderForm_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.treeListView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helperToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private CommonTools.TreeListView treeListView1;
        private EFFTree treeListView2;
        private System.Windows.Forms.ToolStripMenuItem logpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logOffToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripMenuItem synchronizeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionToolStripMenuItem;
        private FileBrowser.BrowserPluginWrapper browserPluginWrapper1;
        private FileBrowser.BrowserPluginWrapper browserPluginWrapper2;
        private FileBrowser.Browser browser1;
        private System.Windows.Forms.ToolStripMenuItem createFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addTabToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateTabToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteTabToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem3;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem4;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem previewEFileFolderToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem6;
    }
}

