using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Data;
using System.Reflection;
using System.Windows.Forms;
using CommonTools;
using FileFoldersCore;
using FFMangerDAL;

using SyncFileFolderManager.EFileFoldersWSProxy;

namespace SyncFileFolderManager
{
    public partial class FileFolderForm : Form
    {
        private bool _signIn= false;
        private string _uid = "";
        private string _pwd = "";

        private EFileFolderWebService _ws = null;
        private UserSettings _settings = null;
        private DataTable _effSetting = null;

        public int availableCount = -1;

        public FileFolderForm()
        {
            _ws = new EFileFolderWebService();
     
            _settings = new UserSettings();
            _settings.Reload();

            InitializeComponent();
            
        }

        #region Event Methods

        private void toolStripButton1_MouseEnter(object sender, EventArgs e)
        {

            if (this._signIn)
            {
                toolStripButton1.Image = global::SyncFileFolderManager.Properties.Resources.Signout_2;
            }
            else
            {
                toolStripButton1.Image = global::SyncFileFolderManager.Properties.Resources.Signin_2;
            }
            
        }

        private void toolStripButton1_MouseLeave(object sender, EventArgs e)
        {
            if (this._signIn)
            {
                toolStripButton1.Image = global::SyncFileFolderManager.Properties.Resources.Signout_1;
            }
            else
            {
                toolStripButton1.Image = global::SyncFileFolderManager.Properties.Resources.Signin_1;
            }
            
        }

        private void toolStripButton1_MouseDown(object sender, MouseEventArgs e)
        {
            if(this._signIn)
            {
                toolStripButton1.Image = global::SyncFileFolderManager.Properties.Resources.Signout_3;
            }
            else
            {
                toolStripButton1.Image = global::SyncFileFolderManager.Properties.Resources.Signin_3;
            }
        }

        private void toolStripButton1_MouseUp(object sender, MouseEventArgs e)
        {
            if (this._signIn)
            {
                toolStripButton1.Image = global::SyncFileFolderManager.Properties.Resources.Signout_1;
            }
            else
            {
                toolStripButton1.Image = global::SyncFileFolderManager.Properties.Resources.Signin_1;
            }
            
        }

        private void toolStripButton3_MouseDown(object sender, MouseEventArgs e)
        {
            if (!_signIn) return;
            toolStripButton3.Image = global::SyncFileFolderManager.Properties.Resources.synchronize_3;
        }

        private void toolStripButton3_MouseEnter(object sender, EventArgs e)
        {
            if (!_signIn) return;
            toolStripButton3.Image = global::SyncFileFolderManager.Properties.Resources.synchronize_2;
        }

        private void toolStripButton3_MouseLeave(object sender, EventArgs e)
        {
            if (!_signIn) return;
            toolStripButton3.Image = global::SyncFileFolderManager.Properties.Resources.synchronize_1;
        }

        private void toolStripButton3_MouseUp(object sender, MouseEventArgs e)
        {
            if (!_signIn) return;
            toolStripButton3.Image = global::SyncFileFolderManager.Properties.Resources.synchronize_1;
        }

        private void toolStripButton4_MouseUp(object sender, MouseEventArgs e)
        {
           // if (!_signIn) return;
            toolStripButton4.Image = global::SyncFileFolderManager.Properties.Resources.option_1;
        }

        private void toolStripButton4_MouseLeave(object sender, EventArgs e)
        {
            //if (!_signIn) return;
            toolStripButton4.Image = global::SyncFileFolderManager.Properties.Resources.option_1;
        }

        private void toolStripButton4_MouseEnter(object sender, EventArgs e)
        {
            // if (!_signIn) return;
            toolStripButton4.Image = global::SyncFileFolderManager.Properties.Resources.option_2;
        }

        private void toolStripButton4_MouseDown(object sender, MouseEventArgs e)
        {
            //if (!_signIn) return;
            toolStripButton4.Image = global::SyncFileFolderManager.Properties.Resources.option_3;
        }

        private void toolStripButton5_MouseDown(object sender, MouseEventArgs e)
        {
            toolStripButton5.Image = global::SyncFileFolderManager.Properties.Resources.help_3;
        }

        private void toolStripButton5_MouseEnter(object sender, EventArgs e)
        {
            toolStripButton5.Image = global::SyncFileFolderManager.Properties.Resources.help_2;
        }

        private void toolStripButton5_MouseLeave(object sender, EventArgs e)
        {
            toolStripButton5.Image = global::SyncFileFolderManager.Properties.Resources.help_1;
        }

        private void toolStripButton5_MouseUp(object sender, MouseEventArgs e)
        {
            toolStripButton5.Image = global::SyncFileFolderManager.Properties.Resources.help_1;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if(this._signIn) //signout
            {
                _uid = "";
                _pwd = "";
                this._signIn = false;
                this.EnableUI(false);

                this._effSetting = null;

                treeListView2.Nodes.Clear();

            }
            else //signin
            {
                this.LoginUI();
            }
            
        }

        #endregion

        private void LoginUI()
        {
            FormLogin login = new FormLogin(this._ws,this._settings);
            try
            {
                if (login.ShowDialog(this) == DialogResult.OK)
                {
                    _uid = login.UID;
                    _pwd = login.PWD;
                    this._signIn = true;
                    this.EnableUI(true);
                    treeListView2.SelectedNode = null;
                    treeListView2._selectNode = null;
                    //LoadXML();

                    //get the availableeff number
                    GetAvailableEFF();

                    //try
                    //{
                    //    string version = _ws.GetCurrentEFFVersion();
                    //    if (version.ToLower() != Application.ProductVersion)
                    //    {
                    //        FormUpdater updater = new FormUpdater();
                    //        updater.ShowDialog(this);
                    //    }
                    //}
                    //catch { }

                }
            }catch{}

        }

        public void GetAvailableEFF()
        {
            _ws.GetAvailableFoldersCompleted+=new GetAvailableFoldersCompletedEventHandler(ws_GetAvailableFoldersCompleted);
            try
            {
               _ws.GetAvailableFoldersAsync();
            }
            catch
            {
                availableCount = -1;
            }
           
        }

        private void ws_GetAvailableFoldersCompleted(object sender, GetAvailableFoldersCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                availableCount = e.Result;
                if (availableCount <= 0 && availableCount != -1)
                {
                    FormNotification message = new FormNotification();
                    message.ShowDialog(this);
                }
            }
        }

        private void LoadXML()
        {

            _effSetting = FileFolderSettings.GetAllFileFolders();

            treeListView2.Settings = _effSetting;
            
            treeListView2.BeginUpdate();

            CommonTools.Node node,tabNode,fileNode;

            foreach (DataRow item in _effSetting.Rows)
            {
                if ((int)item["FileStatus"]==(int)(FileStatus.Deleted)) continue;

                node = new CommonTools.Node(new object[4] { item["FolderName"], null, null, null });
                node.Tag = item;
                DataTable dividerTable = FileFolderSettings.GetDividerTableByFID(item["ID"].ToString());
                foreach (DataRow tab in dividerTable.Rows)
                {
                    if ((int)tab["FileStatus"]==(int)(FileStatus.Deleted)) continue;

                    tabNode = new CommonTools.Node(new object[4] { tab["DividerName"].ToString(), null, null, null });
                    tabNode.Tag = tab;
                    node.Nodes.Add(tabNode);
                    DataTable fileTable = FileFolderSettings.GetFileTableByDID(tab["ID"].ToString());
                    foreach (DataRow file in fileTable.Rows)
                    {
                        if ((int)file["FileStatus"]==(int)(FileStatus.Deleted)) continue;

                        fileNode = new CommonTools.Node(new object[4] { file["FileName"].ToString(), null, null, null });
                        fileNode.Tag = file;
                        tabNode.Nodes.Add(fileNode);
                    }
                }

                treeListView2.Nodes.Add(node);
                
            }

            
            treeListView2.EndUpdate();
            treeListView2.userSettings = this._settings;
            
                     
        }

   
        private void EnableUI(bool b)
        {
            if (b)
            {
                Bitmap bitmap;
                if(_signIn)
                {
                    bitmap = global::SyncFileFolderManager.Properties.Resources.Signout_1;
                }
                else
                {
                    bitmap = global::SyncFileFolderManager.Properties.Resources.Signin_3;
                }
                toolStripButton1.Image = bitmap;
                toolStripButton3.Image = global::SyncFileFolderManager.Properties.Resources.synchronize_1;
               //toolStripButton4.Image = global::SyncFileFolderManager.Properties.Resources.option_1;
            }
            else
            {
                toolStripButton1.Image = global::SyncFileFolderManager.Properties.Resources.Signin_1;
                toolStripButton3.Image = global::SyncFileFolderManager.Properties.Resources.synchronize_4;
                //toolStripButton4.Image = global::SyncFileFolderManager.Properties.Resources.option_4;
            }

            logpToolStripMenuItem.Enabled = !b;
            logOffToolStripMenuItem.Enabled = b;
            synchronizeToolStripMenuItem.Enabled = b;
            //optionToolStripMenuItem.Enabled = b;

            //treeListView2.Enabled = b;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void logpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LoginUI();
        }

        
        private void FileFolderForm_Load(object sender, EventArgs e)
        {
            treeListView2.SelectedNode = null;
            treeListView2._selectNode = null;

            LoadXML();

            treeListView2.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.EFFTree_AfterSelect);

            try
            {
                string version = _ws.GetCurrentEFFVersion();
                if (version.ToLower() != Application.ProductVersion)
                {
                    FormUpdater updater = new FormUpdater();
                    updater.ShowDialog(this);
                }
            }
            catch { }
        }

        private void ws_GetCurrentEFFVersionCompleted(object sender, GetCurrentEFFVersionCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                MessageBox.Show(e.Result);
            }
        }

        private void EFFTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            treeListView2._selectNode = this.treeListView2.SelectedNode;
        }

        private void logOffToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this._signIn = false;
            EnableUI(false);

            this.treeListView2.BeginUpdate();
            this.treeListView2.Nodes.Clear();
            this.treeListView2.EndUpdate();
        }

        private void treeListView2_DragDrop(object sender, DragEventArgs e)
        {
            Node node = treeListView2.GetHitNode();

            IDataObject dob = e.Data;
            string[] contents = dob.GetFormats();
            object obj = dob.GetData("FileNameW");
            if (obj is string[])
            {
                string[] strs = obj as string[];
                if (node==null) 
                {
                    DropFolder(strs, e);// new efilefolder
                }
                else if (node.Tag is DataRow)
                {
                    DataRow dvd = node.Tag as DataRow;
                    if (dvd.Table.Columns.Contains("DividerName"))
                    {
                        Node pNode = node.Parent;
                        DataRow folder = pNode.Tag as DataRow;
                        DropFiles(folder, dvd, node, strs, e);
                    }                    
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                }
                
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }

            
        }

        public void DropFiles(DataRow folder, DataRow dvd, Node node, string[] files, DragEventArgs e)
        {
            ArrayList items = new ArrayList();
            foreach (string filename in files)
            {
                if(Path.GetExtension(filename).ToLower()==".pdf")
                {
                    items.Add(filename);
                }
            }

            if(items.Count>0)
            {
                FormConversion form = new FormConversion(folder, dvd, items, node, treeListView2);
                form.ShowDialog(this);

                XmlUtil.GenerateXML(dvd, folder);
            }
            else
            {
                MessageBox.Show("The drogged files don't contain pdf files.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Effect = DragDropEffects.None;
            }
        }

        public void DropFolder(string[] files, DragEventArgs e)
        {
            if(files.Length>1)
            {
                MessageBox.Show("You just only can drog one folder at a time.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Effect = DragDropEffects.None;
                return;
            }

            if(Path.HasExtension(files[0]))
            {
                MessageBox.Show("You need drog a folder into this node.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Effect = DragDropEffects.None;
                return;
            }

            //go through the files with pdf extension
            string[] pdffiles = Directory.GetFiles(files[0],"*.pdf");
            if(pdffiles.Length==0)
            {
                MessageBox.Show("There are no pdf files under this folder.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Effect = DragDropEffects.None;
                return;
            }

            if (pdffiles.Length > 12)
            {
                MessageBox.Show("The number of the drogged files can't be more than 12", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Effect = DragDropEffects.None;
                return;
            }
                   

            ArrayList items = new ArrayList();
            items.AddRange(pdffiles);


            FileFolderTable folder = new FileFolderTable();
            folder.ID.Value = Guid.NewGuid().ToString();
            DirectoryInfo info = new DirectoryInfo(files[0]);
            folder.FolderName.Value = info.Name;
            folder.FirstName.Value = "NA";
            folder.Lastname.Value = "NA";
            folder.FileNumber.Value = "";
            folder.SecurityLevel.Value = 0;
            folder.DividersCount.Value = 1;
            folder.FileStatus.Value =(int) FileStatus.New;

            folder.Create();

            Node node = new CommonTools.Node(new object[4] { folder.FolderName.Value, null, null, null });

            DataRow oldRow = FileFolderSettings.GetFolderDataRow(folder.ID.Value);

            DataRow newRow = _effSetting.NewRow();
            CreateNewDataRow(newRow,oldRow);
            _effSetting.Rows.Add(newRow);
            node.Tag = newRow;

            //need to do this task by johnlu655 2010-07-03 0135
            //string shortFileName;
            //foreach(string filePath in pdffiles)
            //{
            //    shortFileName = Path.GetFileNameWithoutExtension(filePath);

            //    DividerTable dvd = new DividerTable();
            //    dvd.ID.Value = Guid.NewGuid().ToString();
            //    dvd.DividerName.Value = shortFileName.Substring(0,20);
            //    dvd.DividerColor.Value = XmlUtil.ColorToHexString(Color.Red);
            //    dvd.FileStatus.Value = (int)FileStatus.New;
            //    dvd.FolderID.Value = folder.ID.Value;

            //    dvd.Create();

            //    Node tabNode = new CommonTools.Node(new object[4] { dvd.DividerName.Value, null, null, null });
            //    tabNode.Tag = FileFolderSettings.GetDividerDataRow(dvd.ID.Value);
            //    node.Nodes.Add(tabNode);
            //}
           

            treeListView2.SuspendLayout();

            treeListView2.Nodes.Add(node);
            node.Expand();

            treeListView2.ResumeLayout();

           

           FormConversion form = new FormConversion(node.Tag as DataRow, items, node,treeListView2);
           form.ShowDialog(this);

           XmlUtil.GenerateXML(node.Tag as DataRow);
        }

        private void CreateNewDataRow(DataRow newRow, DataRow oldRow)
        {
            foreach (DataColumn col in oldRow.Table.Columns)
            {
                newRow[col.ColumnName] = oldRow[col.ColumnName];
            }

        }

        private void treeListView2_DragEnter(object sender, DragEventArgs e)
        {
            IDataObject dob = e.Data;
            if (dob.GetDataPresent("FileNameW"))
            {
                e.Effect = DragDropEffects.Link;
            }
        }


        //sync files
        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if(this._signIn)
            {
                if (availableCount == -1)
                {
                    MessageBox.Show("Can't get your unused efilefolders information", "Warning",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    return;
                }
                else if (availableCount <= 0)
                {
                    FormNotification notify = new FormNotification();
                    notify.ShowDialog();
                    return;
                }

                try
                {
                    FormSynchronize form = new FormSynchronize(_effSetting, _ws, treeListView2);
                    form.ShowDialog(this);
                }
                finally
                {
                    this.GetAvailableEFF();
                }
                
            }
        }

        private void FileFolderForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            if (_effSetting != null)
            {
                //Check the status, and alert
                foreach (DataRow folder in _effSetting.Rows)
                {
                    if ((int)folder["FileStatus"] != (int)FileStatus.Normal)
                    {
                        if (MessageBox.Show(this, string.Format("The folder: {0} is Updated, Do you want to synchronize now, Y/N", folder["FolderName"].ToString()),
                            "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {

                            if (availableCount == -1)
                            {
                                MessageBox.Show("Can't get your unused efilefolders information", "Warning",
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                                return;
                            }
                            else if (availableCount <= 0)
                            {
                                FormNotification notify = new FormNotification();
                                notify.ShowDialog();
                                return;
                            }

                            try
                            {
                                FormSynchronize form = new FormSynchronize(_effSetting, _ws, treeListView2);
                                form.ShowDialog(this);
                            }
                            finally
                            {
                                this.GetAvailableEFF();
                            }
                            
                            e.Cancel = true;
                            return;
                        }
                        else
                        {
                            return;
                        }
                    }
                    
                }
            }
                
        }

        private void synchronizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this._signIn)
            {
                if (availableCount == -1)
                {
                    MessageBox.Show("Can't get your unused efilefolders information", "Warning",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    return;
                }
                else if (availableCount <= 0)
                {
                    FormNotification notify = new FormNotification();
                    notify.ShowDialog();
                    return;
                }

                try
                {
                    FormSynchronize form = new FormSynchronize(_effSetting, _ws, treeListView2);
                    form.ShowDialog(this);
                }finally
                {
                    this.GetAvailableEFF();
                }
                
            }
        }

        private void toolToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {

            if (treeListView2._selectNode != null)
            {
                DataRow item = treeListView2._selectNode.Tag as DataRow;
                if (item != null)
                {
                    if (item.Table.Columns.Contains("FolderName"))
                    {
                        previewEFileFolderToolStripMenuItem.Enabled = true;

                        createFolderToolStripMenuItem.Enabled = true;
                        updateFolderToolStripMenuItem.Enabled = true;
                        deleteFolderToolStripMenuItem.Enabled = true;
                        addTabToolStripMenuItem.Enabled = true;

                        updateTabToolStripMenuItem.Enabled = false;
                        deleteTabToolStripMenuItem.Enabled = false;

                        removeFileToolStripMenuItem.Enabled = false;
                    }
                    else if (item.Table.Columns.Contains("DividerName"))
                    {
                        createFolderToolStripMenuItem.Enabled = true;
                        updateFolderToolStripMenuItem.Enabled = false;
                        deleteFolderToolStripMenuItem.Enabled = false;
                        addTabToolStripMenuItem.Enabled = false;
                        updateTabToolStripMenuItem.Enabled = true;
                        deleteTabToolStripMenuItem.Enabled = true;

                        removeFileToolStripMenuItem.Enabled = false;

                        previewEFileFolderToolStripMenuItem.Enabled = false;
                    }
                    else if (item.Table.Columns.Contains("FileName"))
                    {
                        createFolderToolStripMenuItem.Enabled = true;
                        updateFolderToolStripMenuItem.Enabled = false;
                        deleteFolderToolStripMenuItem.Enabled = false;
                        addTabToolStripMenuItem.Enabled = false;

                        updateTabToolStripMenuItem.Enabled = false;

                        deleteTabToolStripMenuItem.Enabled = false;
                        removeFileToolStripMenuItem.Enabled = true;

                        previewEFileFolderToolStripMenuItem.Enabled = false;
                    }

                }
                else
                {
                    createFolderToolStripMenuItem.Enabled = true;
                    updateFolderToolStripMenuItem.Enabled = false;
                    deleteFolderToolStripMenuItem.Enabled = false;
                    addTabToolStripMenuItem.Enabled = false;

                    updateTabToolStripMenuItem.Enabled = false;

                    deleteTabToolStripMenuItem.Enabled = false;
                    removeFileToolStripMenuItem.Enabled = false;

                    previewEFileFolderToolStripMenuItem.Enabled = false;
                }
            }
            else
            {
               
                createFolderToolStripMenuItem.Enabled = true;
                
                updateFolderToolStripMenuItem.Enabled = false;
                deleteFolderToolStripMenuItem.Enabled = false;
                addTabToolStripMenuItem.Enabled = false;

                updateTabToolStripMenuItem.Enabled = false;

                deleteTabToolStripMenuItem.Enabled = false;
                removeFileToolStripMenuItem.Enabled = false;

                previewEFileFolderToolStripMenuItem.Enabled = false;
            }
        }

        private void deleteFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.treeListView2.OnDeleteFileFolder(treeListView2, null);
        }

        private void updateFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.treeListView2.OnUpdateFileFolder(treeListView2, null);
        }

        private void removeFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.treeListView2.OnDeleteFile(treeListView2, null);
        }

        private void deleteTabToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.treeListView2.OnDeleteDivider(treeListView2, null);
        }

        private void updateTabToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.treeListView2.OnUpdateDivider(treeListView2, null);
        }

        private void addTabToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.treeListView2.OnAddDivider(treeListView2, null);
            
        }

        private void createFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.treeListView2.OnAddeFileFolder(treeListView2, null);
            
           
        }
    }
}