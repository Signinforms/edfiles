using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading;
using System.Web;
using System.Windows.Forms;
using CommonTools;
using FileFoldersCore;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using FFMangerDAL;

namespace SyncFileFolderManager
{
    public partial class FormConversion : Form
    {
        private ArrayList _items;
        private Node _tabNode;
        private EFFTree _tree;
        private string _destpath;
        private DataRow _folder;
        private DataRow _divider;
        private Node _folderNode;

        private string _folderPath;

        private const string TEXT_PROCESSING = "Processing : {0}";

        public FormConversion(DataRow folder, DataRow dvd, ArrayList items, Node node, EFFTree tree)
        {
            _items = items;
            _tabNode = node;
            _tree = tree;
            _folder = folder;
            _divider = dvd;
            _destpath = Path.Combine("Volume",folder["ID"].ToString());
            _destpath = Path.Combine(_destpath, dvd["ID"].ToString());

            InitializeComponent();
        }

        public FormConversion(DataRow folder, ArrayList items, Node folerNode, EFFTree tree)
        {
            _items = items;
            _folderNode = folerNode;
            _tree = tree;
            _folder = folder;

            _folderPath = Path.Combine("Volume", folder["ID"].ToString());

            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

        }

        private void FormConversion_Load(object sender, EventArgs e)
        {
            loadingCircle1.Active = true;

            Thread t = new Thread(new ThreadStart(Excute));
            t.IsBackground = true;
            t.Start();
        }

        public  void Excute()
        {
            string destpath, destfile,destfileCopy, filewithex,filewithouext;
           
 
            ShowStatusDelegate showStatus = ShowStatus;
            ShowButtonDelegate buttonStatus = ShowButtonStatus;
            AddFileDelegate newNode = AddFileNode;

            string shortFileName;
            int length = 20;

            foreach (string filename in _items) //with path
            {
                shortFileName = Path.GetFileNameWithoutExtension(filename);

                DividerTable dvd = new DividerTable();
                dvd.ID.Value = Guid.NewGuid().ToString();
                length = Math.Min(shortFileName.Length, 20);
                dvd.DividerName.Value = shortFileName.Substring(0, length);
                dvd.DividerColor.Value = XmlUtil.ColorToHexString(Color.Red);
                dvd.FileStatus.Value = (int)FileStatus.New;
                dvd.FolderID.Value = (string)_folder["ID"];

                dvd.Create();

                _destpath = Path.Combine(_folderPath, dvd.ID.Value);

                //_destpath = Path.Combine(_folderPath, dvd.DividerName.Value);

                destpath = _destpath; // just only use relative path. Path.Combine(Application.StartupPath, _destpath);

                if (!Directory.Exists(destpath))
                {
                    Directory.CreateDirectory(destpath);
                }

                Node tabNode = new CommonTools.Node(new object[4] { dvd.DividerName.Value, null, null, null });
                tabNode.Tag = FileFolderSettings.GetDividerDataRow(dvd.ID.Value);
                _folderNode.Nodes.Add(tabNode);

                _divider = (DataRow) tabNode.Tag;

                FileInfo info = new FileInfo(filename);
                float size = (float)(info.Length / (1024f * 1024f));
                filewithouext = Path.GetFileNameWithoutExtension(filename);
                filewithex = Path.GetFileName(filename);// need to urlencoding for blank space
                label1.Invoke(showStatus, new object[] { filewithex });

                string encodeName = HttpUtility.UrlPathEncode(filewithouext);
                encodeName = encodeName.Replace("%", "$");


                destfile = Path.Combine(destpath, encodeName + "_%.swf");

                encodeName = HttpUtility.UrlPathEncode(filewithex);
                encodeName = encodeName.Replace("%", "$");

                destfileCopy = Path.Combine(destpath, encodeName);
                try
                {
                    File.Copy(filename, destfileCopy, true);

                    PdfDocument reader = PdfReader.Open(filename);
                    int count = reader.PageCount;
                    reader.Close();

                    if (ConversionToSwf.Generate(destfileCopy, destfile) == "true")
                    {
                        //add file into the tabnode
                        FileTable efile = new FileTable();
                        efile.ID.Value = Guid.NewGuid().ToString();
                        efile.FileName.Value = filewithouext;// without urlencoding for showing 
                        efile.FullName.Value = destfileCopy;// with urlencoding for reading  
                        efile.PageNumber.Value = count;
                        efile.FileStatus.Value = (int)FileStatus.New;
                        efile.FileSize.Value = size;
                        efile.DividerID.Value = _divider["ID"].ToString();
                        efile.FolderID.Value = _folder["ID"].ToString();


                        efile.Create();

                        int i = (int)_divider["FilesCount"];
                        _divider["FilesCount"] = i +1;

                        DividerTable dtable = new DividerTable(_divider["ID"].ToString());
                        dtable.FilesCount.Value = dtable.FilesCount.Value + 1;

                        dtable.Update();

                        _divider.AcceptChanges();

                        _tree.Invoke(newNode, new object[] { FileFolderSettings.GetFileDataRow(efile.ID.Value), tabNode});

                        XmlUtil.GenerateXML(_divider, _folder);
                    }
                }
                catch(Exception exp)
                {
                    Console.WriteLine(exp.Message);
                }

                

            }


            button1.Invoke(buttonStatus, new Object[] { true });

        }

        private void GenerateXMLFiles(FileFolder _folder, Divider _divider)
        {
            throw new NotImplementedException();
        }

        public void AddFileNode(DataRow efile, Node tabNode)
        {
            Node fileNode = new CommonTools.Node(new object[4] { efile["FileName"].ToString(), null, null, null });
            fileNode.Tag = efile;

            _tree.SuspendLayout();
            tabNode.Nodes.Add(fileNode);
            tabNode.Expand();
            _tree.ResumeLayout();
        }

        public void ShowStatus(string status)
        {
            if (label1.InvokeRequired == false)
            {
                label1.Text = string.Format(TEXT_PROCESSING, status); ;
            }
            else
            {
                ShowStatusDelegate showStatus = new ShowStatusDelegate(ShowStatus);
                label1.Invoke(showStatus, new object[] { status });
            }
        }

        public void ShowButtonStatus(bool finished)
        {
            if (label1.InvokeRequired == false)
            {
                this.button1.Enabled = finished;
                this.loadingCircle1.Active = false;
            }
            else
            {
                this.loadingCircle1.Active = false;
                ShowButtonDelegate showStatus = new ShowButtonDelegate(ShowButtonStatus);
                button1.Invoke(showStatus, new object[] { finished });
            }
        }

        public void UpdateButtonEnable(bool finished)
        {
            this.button1.Enabled = finished;
        }

        private delegate void AddFileDelegate(DataRow efile, Node tabNode);
        private delegate void ShowStatusDelegate(String filename);
        private delegate void ShowButtonDelegate(bool finished);
    }
}