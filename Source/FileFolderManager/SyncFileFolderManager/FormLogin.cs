using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DataQuicker2.Framework;
using SyncFileFolderManager.EFileFoldersWSProxy;
using FFMangerDAL;

namespace SyncFileFolderManager
{

    public partial class FormLogin : Form
    {
        private UserSettings _settings = null;
        private EFileFolderWebService _ws = null;
        private string _uid = "";
        private string _pwd= "";

        public FormLogin(EFileFolderWebService ws, UserSettings settings)
        {
            this._ws = ws;
            this._settings = settings;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.label1.Text = "";

            if (!VerifyUI()) return;

            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            _uid = this.textBoxUID.Text.Trim();
            _pwd = this.textBoxPwd.Text.Trim();
            try
            {
                AuthenticationInfo header = new AuthenticationInfo();
                header.username = _uid;
                header.password = _pwd;
                _ws.AuthenticationInfoValue = header;
                bool rs = _ws.LoginUser(_uid, _pwd);
                if (rs)
                {
                    this.DialogResult = DialogResult.OK;
                    if (checkBox1.Checked)
                    {
                        _settings.UID = _uid;
                        _settings.Password = _pwd;
                    }
                }
                else
                {
                    this.label1.Text = "Failed to logon";
                }

                _settings.RememberMe = checkBox1.Checked;
                _settings.Save();
                

                //save as xml
            }catch(Exception exp){
                this.label1.Text = exp.Message;
            }

            this.Cursor = System.Windows.Forms.Cursors.Default;
        }

        private bool VerifyUI()
        {
            return true;
        }

        public string UID
        {
            get
            {
                return _uid;
            }
        }

        public string PWD
        {
            get
            {
                return _pwd;
            }
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {
            this.linkLabel1.Links[0].LinkData = "http://www.efilefolders.com/";

            if (_settings.RememberMe)
            {
                _uid = _settings.UID;
                textBoxUID.Text = _uid;
                _pwd = _settings.Password;
                textBoxPwd.Text = _pwd;
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
           string target =  e.Link.LinkData as string;
           System.Diagnostics.Process.Start(target);

           this.DialogResult = DialogResult.Abort;
        }


    }
}