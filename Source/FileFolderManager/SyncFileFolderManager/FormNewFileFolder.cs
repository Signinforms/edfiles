using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SyncFileFolderManager
{
    public partial class FormNewFileFolder : Form
    {
        DataSet data;
        private bool _editMode = false;
        const string FileName = "ColorData.xml";
        Dictionary<int, Color> _colors = new Dictionary<int, Color>();

        public FormNewFileFolder(bool editMode)
        {
            this._editMode = editMode;
            InitializeComponent();

            _colors.Add(0, Color.FromArgb(0xFF,0,0));
            _colors.Add(1, Color.FromArgb(0xFF,0x99,0x33));
            _colors.Add(2,  Color.FromArgb(0xCC,0,0x66));
            _colors.Add(3, Color.FromArgb(0xCC,0x99,0x99));
            _colors.Add(4, Color.FromArgb(0x99,0,0xCC));
            _colors.Add(5, Color.FromArgb(0x99,0x99,0xFF));
            _colors.Add(6, Color.FromArgb(0x66,0x33,0));
            _colors.Add(7, Color.FromArgb(0x66,0xCC,0x33));
            _colors.Add(8, Color.FromArgb(0x33,0x33,0x66));
            _colors.Add(9, Color.FromArgb(0x33,0xCC,0x99));
            _colors.Add(10, Color.FromArgb(0x00,0x33,0xCC));
            _colors.Add(11, Color.FromArgb(0x00,0xCC,0xFF));

            LoadDataColumns();

        }

        private void LoadDataColumns()
        {
            ColorPickerColumn colColorPick = new ColorPickerColumn();
            colColorPick.Name = "Color";
            colColorPick.HeaderText = "Color";

            DataGridViewTextBoxColumn TextColumn = new DataGridViewTextBoxColumn();
            TextColumn.Name = "Name";
            TextColumn.HeaderText = "Name";
            dataGridView1.SuspendLayout();

            dataGridView1.Columns.Add(TextColumn);
            dataGridView1.Columns.Add(colColorPick);

            dataGridView1.ResumeLayout();
        }

        private void FormNewFileFolder_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 2;
        }

        /// <summary>
        /// Add count dividers into this gridview from the start index,
        /// so we can keep the older dividers and colors
        /// </summary>
        /// <param name="dv"></param>
        /// <param name="data"></param>
        /// <param name="startindex"></param>
        /// <param name="count"></param>
        private void LoadGridValuesFromTable(ref DataGridView dv, Dictionary<int, Color> data, int startindex, int count)
        {
            Color lclColor;
            int numb = 0;
            dv.SuspendLayout();
            for(int i =startindex;i<data.Count;i++)
            {
                numb = i+1;
                lclColor = data[i];
                string name = "Tab_Name" + numb;
                int rownumber = dv.Rows.Add();

                dv.Rows[rownumber].Cells[0].Value = name;
                dv.Rows[rownumber].Cells[1].Value = lclColor;
               
                if (--count == 0)
                {
                    break;
                }
            }
            dv.ResumeLayout();
        }
       
        //Get a string containg a predefined system color name or a hex value and returns
        //an integer representing the RGB value of this color.
        public int ConvertColorNameToValue(string ColorName)
        {

            Color TempColor;
            int result;
            //Check if the string is a system color name or a Hex number
            if (int.TryParse(ColorName, System.Globalization.NumberStyles.HexNumber, null, out result))
                TempColor = Color.FromArgb(result);
            else
                //The string is a system color name
                TempColor = Color.FromName(ColorName);
            return TempColor.ToArgb();
        }

        

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (VerifyUI())
            {
                this.DialogResult = DialogResult.OK;
            }
            
        }

        private bool VerifyUI()
        {
            if (this.textBoxFName.Text.Trim() == "")
            {
                errorProvider1.SetError(textBoxFName, "This field can't be empty. ");

                return false;
            }
            else
            {
                errorProvider1.SetError(textBoxFName, "");
            }

            if (this.textBoxFTName.Text.Trim() == "")
            {
                errorProvider1.SetError(textBoxFTName, "This field can't be empty. ");
                return false;
            }
            else
            {
                errorProvider1.SetError(textBoxFTName, "");
            }

            if (this.textBoxLTName.Text.Trim() == "")
            {
                errorProvider1.SetError(textBoxLTName, "This field can't be empty. ");
                return false;
            }
            else
            {
                errorProvider1.SetError(textBoxLTName, "");
            }

            return true; 
        }

        public string FolderName
        {
            get
            {
                return this.textBoxFName.Text.Trim();
            }
        }

        public string FirstName
        {
            get
            {
                return this.textBoxFTName.Text.Trim();
            }
        }

        public string LastName
        {
            get
            {
                return this.textBoxLTName.Text.Trim();
            } 
        }

        public string FileNumber
        {
            get
            {
                return this.textBoxFN.Text.Trim();
            }
        }

        public int Security
        {
            get
            {
                return this.comboBox2.SelectedIndex;
            }
        }

        public string FolderCode
        {
            get
            {
                return this.textBoxFolderCode.Text.Trim();
            }
        }

        public int DividerCount
        {
            get
            {
                return this.comboBox1.SelectedIndex;
            }
        }

        public DataGridViewRowCollection Dividers
        {
            get
            {
                return this.dataGridView1.Rows;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int count = comboBox1.SelectedIndex+1;
            int exist = dataGridView1.Rows.Count;
            int increase = 0;
            if(count>exist)
            {
                increase = count-exist;
                LoadGridValuesFromTable(ref dataGridView1, _colors, exist, increase);
            }
            else
            {
              
                dataGridView1.SuspendLayout();
                for(int i=exist-1;i>=count;i--)
                {
                    dataGridView1.Rows.RemoveAt(i);
                }

                dataGridView1.ResumeLayout();
            }

            
        }
    }
}