using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SyncFileFolderManager
{
    public partial class FormNotification : Form
    {
        public FormNotification()
        {
            InitializeComponent();
        }

        private void FormNotification_Load(object sender, EventArgs e)
        {
            this.linkLabel2.Links[0].LinkData = "http://www.efilefolders.com/";
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string target = e.Link.LinkData as string;
            System.Diagnostics.Process.Start(target);

            this.DialogResult = DialogResult.Abort;
        }
    }
}