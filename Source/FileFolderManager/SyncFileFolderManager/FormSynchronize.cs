using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading;
using System.Web;
using System.Windows.Forms;
using CommonTools;
using FFMangerDAL;
using FileFoldersCore;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using SyncFileFolderManager.EFileFoldersWSProxy;

namespace SyncFileFolderManager
{
    public partial class FormSynchronize : Form
    {
        private EFileFolderWebService _ws = null;
        public int _chunkSize = 128 * 1024;	
        private EFFTree _tree;
        private DataTable _settings;

        private const string TEXT_PROCESSING = "Processing : {0}";

        public FormSynchronize(DataTable settings,EFileFolderWebService ws, EFFTree tree)
        {
            _settings = settings;
            _ws = ws;
            _tree = tree;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;

        }

        private void FormConversion_Load(object sender, EventArgs e)
        {
            loadingCircle1.Active = true;

            Thread t = new Thread(new ThreadStart(Excute));
            t.Start();
        }

        public  void Excute()
        {
            int id = -1;
            bool ret = false;
            SaveSettingDelegate saveSetting = SaveSetting;
            ShowStatusDelegate showStatus = ShowStatus;
            ArrayList listRemoved = new ArrayList();

            try
            {
                foreach (DataRow folder in _settings.Rows)
                {
                    label1.Invoke(showStatus, new object[] { "Folder -" + folder["FolderName"].ToString() });

                    switch ((FileStatus)folder["FileStatus"])
                    {
                        case FileStatus.New:

                            id = CreateFileFolder(folder);
                            if (id == -1)
                            {
                                continue; //next folder
                            }
                            folder["FileStatus"] = (int)FileStatus.Normal;
                            folder["FolderID"] = id;
                            folder.AcceptChanges();
                            //save into database
                            SaveFolderInDb(folder);
                            break;
                        case FileStatus.Updated:
                            ret = ModifyFileFolder(folder);
                            if (!ret)
                            {
                                continue; //next folder
                            }

                            folder["FileStatus"] = (int)FileStatus.Normal;
                            folder.AcceptChanges();
                            // to do save in database
                            SaveFolderInDb(folder);
                            break;
                        case FileStatus.Deleted: // todo remove from ui,and database
                           // ret = RemoveFileFolder(folder);
                            if (true)
                            {
                                FileFolderTable folderTable = new FileFolderTable(folder["ID"].ToString());
                                if (folderTable.IsExist)
                                {
                                    folderTable.Delete();
                                }

                                folder["FileStatus"] = (int)FileStatus.Normal;
                                folder.AcceptChanges();
                            }
                            continue;
                        default:
                            break;
                    }

                    this.GoThroughDividers(folder);
                }
            }
            catch (Exception exp)
            {
               
            }
            finally
            {
                RefreshSettingDelegate refreshSetting = RefreshSetting;
                _tree.Invoke(refreshSetting);
            }

            ShowButtonDelegate buttonStatus = ShowButtonStatus;
         
            button1.Invoke(buttonStatus, new Object[] { true });

        }

        private void SaveFolderInDb(DataRow folder)
        {
            FileFolderTable item = new FileFolderTable(folder["ID"].ToString());

            item.DividersCount.Value = (int)folder["DividersCount"];
            item.FileNumber.Value = folder["FileNumber"].ToString();
            item.FileStatus.Value = (int)folder["FileStatus"];
            item.FirstName.Value = folder["FirstName"].ToString();
            item.FolderID.Value = (int)folder["FolderID"];
            item.FolderName.Value = folder["FolderName"].ToString();
            item.Lastname.Value = folder["Lastname"].ToString();
            item.ModifyDate.Value = (DateTime)folder["ModifyDate"];
            item.SecurityLevel.Value = (int)folder["SecurityLevel"];
            item.FolderCode.Value = folder["FolderCode"].ToString();
            if(item.IsExist)
            {
                item.Update();
            }
            else
            {
                item.Create();
            }
        }

     
        private int CreateFileFolder(DataRow folder)
        {
            try
            {
                return _ws.CreateFileFolder3(folder["FolderName"].ToString(), folder["FirstName"].ToString(), folder["Lastname"].ToString(),
                                        DateTime.Now, folder["SecurityLevel"].ToString(), folder["FileNumber"].ToString(),
                                        folder["ID"].ToString(), folder["FolderCode"].ToString());
            }
            catch
            {
                return -1;
            }
        }

        private bool ModifyFileFolder(DataRow folder)
        {
            try
            {
                return _ws.UpdateFileFolder1((int)folder["FolderID"], folder["FolderName"].ToString(), folder["FirstName"].ToString(),
                                folder["Lastname"].ToString(), folder["SecurityLevel"].ToString(),
                                folder["FileNumber"].ToString(), folder["ID"].ToString(),folder["FolderCode"].ToString());
            }
            catch
            {
                return false;
            }
        }

        private bool RemoveFileFolder(DataRow folder)
        {
            try
            {
                return _ws.RemoveFileFolder((int)folder["FolderID"]);
            }
            catch
            {
                return false;
            }
        }


        private Node GetNodeByDataRow(DataRow folder)
        {

            foreach (Node item in this._tree.Nodes)
            {
                if (item.Tag == folder)
                {
                    return item;
                }
            }

            return null;
        }

        private void GoThroughDividers(DataRow folder)
        {
            int id = -1;
            bool ret = false;
            SaveSettingDelegate saveSetting = SaveSetting;
            ShowStatusDelegate showStatus = ShowStatus;

            ArrayList listRemoved = new ArrayList();

            try
            {
                Node folderNode = GetNodeByDataRow(folder);
                //DataTable dividers = FileFolderSettings.GetDividerTableByFID(folder["ID"].ToString());
                foreach (Node dividerNode in folderNode.Nodes)
                {
                    DataRow divider = dividerNode.Tag as DataRow;

                    label1.Invoke(showStatus, new object[] {"Tab -" + divider["DividerName"].ToString()});

                    switch ((FileStatus)divider["FileStatus"])
                    {
                        case FileStatus.New:
                            id = CreateDivider(divider, folder);
                            if (id == -1)
                            {
                                continue; //next divider
                            }
                            else
                            {
                                divider["FileStatus"] = (int)FileStatus.Normal;
                                divider["DividerID"] = id;
                                divider.AcceptChanges();
                                SaveDividerInDb(divider);
                            }
                            break;
                        case FileStatus.Updated:
                            ret = ModifyDivider(divider, folder);
                            if (!ret)
                            {
                                continue; //next divider
                            }
                            divider["FileStatus"] = (int)FileStatus.Normal;
                            divider.AcceptChanges();
                            SaveDividerInDb(divider);
                            break;
                        case FileStatus.Deleted:
                            //ret = this.RemoveDivider(divider, folder);
                            if (true)
                            {
                                DividerTable dividerTable = new DividerTable(divider["ID"].ToString());
                                if (dividerTable.IsExist)
                                {
                                    dividerTable.Delete();
                                }
                                divider["FileStatus"] = (int)FileStatus.Normal;
                                divider.AcceptChanges();
                            }
                           
                            continue;
                        default:
                            break;

                    }
                    this.GoThroughFiles(divider, folder, dividerNode);
                }
            }
            finally
            {
                
            }
        }

        private void SaveDividerInDb(DataRow divider)
        {
            DividerTable item = new DividerTable(divider["ID"].ToString());

            item.DividerColor.Value = divider["DividerColor"].ToString();
            item.DividerID.Value = (int)divider["DividerID"];
            item.DividerName.Value = divider["DividerName"].ToString();
            item.FilesCount.Value = (int)divider["FilesCount"];
            item.FileStatus.Value = (int)divider["FileStatus"];
            item.FolderID.Value = divider["FolderID"].ToString();
            item.ModifyDate.Value = (DateTime)divider["ModifyDate"];

            if (item.IsExist)
            {
                item.Update();
            }
            else
            {
                item.Create();
            }
        }

        private int CreateDivider(DataRow divider, DataRow folder)
        {
            try
            {
                return _ws.InsertTab(divider["DividerName"].ToString(), divider["DividerColor"].ToString(), 
                    (int)folder["FolderID"], divider["ID"].ToString());
            }
            catch
            {
                return -1;
            }

        }

        private string GetStringColorFormat(Color color)
        {
      
           return  String.Format("#{0:x2}{1:x2}{2:x2}", color.R, color.G, color.B).ToUpper();
        }

        private bool ModifyDivider(DataRow divider, DataRow folder)
        {
            try
            {
                return _ws.UpdateTab((int)divider["DividerID"], divider["DividerName"].ToString(),
                            divider["DividerColor"].ToString(), (int)folder["FolderID"], divider["ID"].ToString());
            }
            catch
            {
                return false;
            }
        }

        private bool RemoveDivider(DataRow divider, DataRow folder)
        {
            try
            {
                return _ws.RemoveTab((int)divider["DividerID"], divider["ID"].ToString());
            }
            catch
            {
                return false;
            }
        }

        private Node GetNodeByDataRow(DataRow file,Node dividerNode)
        {

            foreach (Node item in dividerNode.Nodes)
            {
                if (item.Tag == file)
                {
                    return item;
                }
            }

            return null;
        }

        private void GoThroughFiles(DataRow divider, DataRow folder,Node dividerNode)
        {
            int id = -1;
            bool ret = false;
            SaveSettingDelegate saveSetting = SaveSetting;
            ShowStatusDelegate showStatus = ShowStatus;

            ArrayList listRemoved = new ArrayList();

            try
            {

                //DataTable files = FileFolderSettings.GetFileTableByDID(divider["ID"].ToString());
                foreach (Node fileNode in dividerNode.Nodes)
                {
                    DataRow file = fileNode.Tag as DataRow;

                    label1.Invoke(showStatus, new object[] { "File -" + file["FileName"] });

                    switch ((FileStatus)file["FileStatus"])
                    {
                        case FileStatus.New:
                            id = CreateFile(file, divider, folder);
                            if (id == -1)
                            {
                                continue; //next folder
                            }
                            else
                            {
                                file["FileStatus"] = FileStatus.Normal;
                                file["FileID"] = id;
                                file.AcceptChanges();
                                SaveFileInDb(file);
                                GoThoughPages(file, divider, folder);
                            }

                            Invoke(saveSetting, new object[] { false });
                            break;
                        case FileStatus.Deleted:
                           // ret = RemoveFile(file, divider);
                            if (true)
                            {
                                FileTable fileTable = new FileTable(file["ID"].ToString());
                                if(fileTable.IsExist)
                                {
                                    fileTable.Delete();
                                }

                                file["FileStatus"] = FileStatus.Normal;
                                file.AcceptChanges();
                            }
                            break;
                        default:
                            break;

                    }

                }
            }
            finally
            {
             
            }

        }

        private void SaveFileInDb(DataRow file)
        {
            FileTable item = new FileTable(file["ID"].ToString());

            item.DividerID.Value = file["DividerID"].ToString();
            item.FileID.Value = (int)file["FileID"];
            item.FileName.Value = file["FileName"].ToString();
            double var = (double)file["FileSize"];
            float size = (float)var;
            item.FileSize.Value = size;
            item.FileStatus.Value = (int)file["FileStatus"];
            item.FolderID.Value = file["FolderID"].ToString();
            item.FullName.Value = file["FullName"].ToString();
            item.PageNumber.Value = (int)file["PageNumber"];
            item.ModifyDate.Value = (DateTime)file["ModifyDate"];

            if (item.IsExist)
            {
                item.Update();
            }
            else
            {
                item.Create();
            }
        }


        private void GoThoughPages(DataRow file, DataRow divider, DataRow folder)
        {
            string fullname = file["FullName"].ToString();
            string pdfname = Path.GetFileName(fullname);
            string filePath = Path.GetDirectoryName(fullname);
            string swfname = Path.GetFileNameWithoutExtension(fullname);

            string retStr = SendLargeFile(pdfname,(int)divider["DividerID"], (int)folder["FolderID"], fullname);

            string localPageName, remotePageName;
            int count = (int)file["PageNumber"];
            for (int i = 1; i <= count; i++)
            {
                localPageName = string.Format("{0}_{1}.swf", swfname, i);
                remotePageName = string.Format("{0}_{1}_{2}_{3}.swf", swfname, folder["FolderID"], divider["DividerID"], i);
                retStr = SendFile(remotePageName,(int)divider["DividerID"],(int)folder["FolderID"], filePath + Path.DirectorySeparatorChar + localPageName);
            }
        }

        private string SendLargeFile(string remoteName, int dividerId, int folderId, string fullname)
        {
            FileInfo fInfo = new FileInfo(fullname);
            if (fInfo.Exists)
            {
                try
                {
                    using (FileStream fs = new FileStream(fullname,
                    FileMode.Open, FileAccess.Read))
                    {
                        byte[] Buffer = new byte[_chunkSize];
                        long SentBytes = 0;
                        int BytesRead = fs.Read(Buffer, 0, _chunkSize);
                        while (BytesRead > 0)
                        {
                            _ws.AppendChunk(dividerId, folderId, remoteName, Buffer, SentBytes);
                            SentBytes += BytesRead;
                            BytesRead = fs.Read(Buffer, 0, _chunkSize);
                        }
                    }
                    return "OK";
                }
                catch (Exception exp)
                {
                    return exp.Message;
                }
            }
            else
            {
                return "File Not Exist";
            }
        }

        private string SendFile(string remoteName,int dividerId,int folderId,string fullname)
        {
            
                FileInfo fInfo = new FileInfo(fullname); 
                if (fInfo.Exists)
                {
                    try
                    {

                        long numBytes = fInfo.Length;

                        FileStream fStream = new FileStream(fullname, FileMode.Open, FileAccess.Read);
                        BinaryReader br = new BinaryReader(fStream);

                        byte[] data = br.ReadBytes((int)numBytes);
                        br.Close();

                        _ws.UploadFile(data, dividerId, folderId, remoteName);
                     
                        fStream.Close();
                        fStream.Dispose();

                        return "OK";
                        
                    }
                    catch(Exception exp)
                    {
                        return exp.Message;
                    }

                }
                else
                {
                    return "Page Not Exist";
                }

        }

        private int CreateFile(DataRow file, DataRow divider, DataRow folder)
        {
            try
            {
                string path = string.Format("Volume\\{0}\\{1}", folder["FolderID"], divider["DividerID"]);
                double var = (double)file["FileSize"];
                float size = (float)var;
                return _ws.AppendFile(file["FileName"].ToString(), (int)file["PageNumber"], path, size,
                    1, (int)folder["FolderID"], file["ID"].ToString(), (int)divider["DividerID"]);
            }
            catch
            {
                return -1;
            }
        }

        

        protected static StreamType getFileExtention(string ext)
        {
            StreamType streamType = StreamType.PDF;

            switch (ext.ToLower())
            {
                case ".pdf":
                    streamType = StreamType.PDF;
                    break;
                case ".docx":
                case ".doc":
                    streamType = StreamType.Word;
                    break;
                case ".xls":
                    streamType = StreamType.Excel;
                    break;
                case ".ppt":
                    streamType = StreamType.PPT;
                    break;
                case ".rtf":
                    streamType = StreamType.RTF;
                    break;
                case ".odt":
                    streamType = StreamType.ODT;
                    break;
                case ".ods":
                    streamType = StreamType.ODS;
                    break;
                case ".odp":
                    streamType = StreamType.ODP;
                    break;
                case ".jpeg":
                case ".jpg":
                    streamType = StreamType.JPG;
                    break;
                case ".png":
                    streamType = StreamType.PNG;
                    break;
                default:
                    break;
            }

            return streamType;
        }

        private bool RemoveFile(DataRow file, DataRow divider)
        {
            try
            {
                return _ws.RemoveFile((int)file["FileID"], file["ID"].ToString());
            }
            catch
            {
                return false;
            }
        }



        public void ShowStatus(string status)
        {
            if (label1.InvokeRequired == false)
            {
                label1.Text = string.Format(TEXT_PROCESSING, status); ;
            }
            else
            {
                ShowStatusDelegate showStatus = new ShowStatusDelegate(ShowStatus);
                label1.Invoke(showStatus, new object[] { status });
            }
        }

        public void ShowButtonStatus(bool finished)
        {
            if (label1.InvokeRequired == false)
            {
                this.button1.Enabled = finished;
                this.loadingCircle1.Active = false;
            }
            else
            {
                this.loadingCircle1.Active = false;
                ShowButtonDelegate showStatus = new ShowButtonDelegate(ShowButtonStatus);
                button1.Invoke(showStatus, new object[] { finished });
            }
        }

        public void UpdateButtonEnable(bool finished)
        {
            this.button1.Enabled = finished;
        }

        public void SaveSetting(bool refresh)
        {
            
        }

        public void RefreshSetting()
        {
           this._tree.Refresh();
        }


        private delegate void ShowStatusDelegate(String filename);
        private delegate void ShowButtonDelegate(bool finished);
        private delegate void SaveSettingDelegate(bool refresh);
        private delegate void RefreshSettingDelegate();
    }

    public enum StreamType
    {
        PDF = 1,
        Word,
        Excel,
        PPT,
        Html,
        XPS,
        Text,
        RTF,
        ODT,
        ODS,
        ODP,
        JPG,
        PNG
    }
}