using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using FileFoldersCore;

namespace SyncFileFolderManager
{
    public partial class FormUpdateDivider : Form
    {
        private DataRow _divider;
        private DataRow _folder;

        public FormUpdateDivider(DataRow divider)
        {
            _divider = divider;
            InitializeComponent();
        }

        public FormUpdateDivider(DataRow folder,bool isfolder)
        {
            _folder = folder;
            InitializeComponent();
        }


        private void FormUpdateDivider_Load(object sender, EventArgs e)
        {
            if (_divider !=null)
            {
                textBox1.Text = _divider["DividerName"].ToString();
   
                comboBoxColorPicker1.Color = ColorTranslator.FromHtml(_divider["DividerColor"].ToString());
            }
            
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (VerifyUI())
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private bool VerifyUI()
        {
            if (this.textBox1.Text.Trim() == "")
            {
                errorProvider1.SetError(textBox1, "This field can't be empty. ");

                return false;
            }

            return true;
        }

        public Color DividerColor
        {
            get
            {
                return this.comboBoxColorPicker1.Color;
            }
        }

        public string DividerName
        {
            get
            {
                return this.textBox1.Text.Trim();
            }
        }
    }
}