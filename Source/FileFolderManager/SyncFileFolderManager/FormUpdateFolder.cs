using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using FileFoldersCore;

namespace SyncFileFolderManager
{
    public partial class FormUpdateFolder : Form
    {
        private DataRow _folder;

        public FormUpdateFolder(DataRow folder)
        {
            _folder = folder;

            InitializeComponent();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            if (VerifyUI())
            {
                this.DialogResult = DialogResult.OK;
            }
        }

        private bool VerifyUI()
        {
            if (this.textBoxFName.Text.Trim() == "")
            {
                errorProvider1.SetError(textBoxFName, "This field can't be empty. ");

                return false;
            }
            else
            {
                errorProvider1.SetError(textBoxFName, "");
            }

            if (this.textBoxFTName.Text.Trim() == "")
            {
                errorProvider1.SetError(textBoxFTName, "This field can't be empty. ");
                return false;
            }
            else
            {
                errorProvider1.SetError(textBoxFTName, "");
            }

            if (this.textBoxLTName.Text.Trim() == "")
            {
                errorProvider1.SetError(textBoxLTName, "This field can't be empty. ");
                return false;
            }
            else
            {
                errorProvider1.SetError(textBoxLTName, "");
            }

            return true;
        }

        private void FormUpdateFolder_Load(object sender, EventArgs e)
        {
            this.textBoxFName.Text = _folder["FolderName"].ToString();
            this.textBoxFTName.Text = _folder["FirstName"].ToString();
            this.textBoxLTName.Text = _folder["Lastname"].ToString();
            this.textBoxFN.Text = _folder["FileNumber"].ToString();
            this.comboBox2.SelectedIndex = (int)_folder["SecurityLevel"];

            this.textBoxFolderCode.Text = _folder["FolderCode"] == DBNull.Value ? "" : _folder["FolderCode"].ToString();
        }

        public string FolderName
        {
            get
            {
                return this.textBoxFName.Text.Trim();
            }
        }

        public string FirstName
        {
            get
            {
                return this.textBoxFTName.Text.Trim();
            }
        }

        public string LastName
        {
            get
            {
                return this.textBoxLTName.Text.Trim();
            }
        }

        public string FileNumber
        {
            get
            {
                return this.textBoxFN.Text.Trim();
            }
        }

        public int Security
        {
            get
            {
                return this.comboBox2.SelectedIndex;
            }
        }

        public string FolderCode
        {
            get
            {
                return this.textBoxFolderCode.Text.Trim();
            }
        }
    }
}