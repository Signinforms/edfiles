using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;

namespace SyncFileFolderManager
{
    public partial class FormUpdater : Form
    {
        public FormUpdater()
        {
            InitializeComponent();
        }

        private void FormNotification_Load(object sender, EventArgs e)
        {
            this.linkLabel2.Links[0].LinkData = ConfigurationManager.AppSettings["effurl"]; ;
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string target = e.Link.LinkData as string;
            System.Diagnostics.Process.Start(target);

            this.DialogResult = DialogResult.Abort;
        }
    }
}