using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace SyncFileFolderManager
{
    public sealed class UserSettings : System.Configuration.ApplicationSettingsBase 
    {
        [UserScopedSetting()]
        [SettingsSerializeAs(System.Configuration.SettingsSerializeAs.String)]
        [DefaultSettingValue("")]
        public string UID
        {
            get
            {
                return (string)this["UID"];
            }
            set
            {
                this["UID"] = value;
            }
        }

        [UserScopedSetting()]
        [SettingsSerializeAs(System.Configuration.SettingsSerializeAs.String)]
        [DefaultSettingValue("")]
        public string Password
        {
            get
            {
                return (string)this["Password"];
            }
            set
            {
                this["Password"] = value;
            }
        }

        [UserScopedSetting()]
        [SettingsSerializeAs(System.Configuration.SettingsSerializeAs.String)]
        [DefaultSettingValue("")]
        public bool RememberMe
        {
            get
            {
                return (bool)this["RememberMe"];
            }
            set
            {
                this["RememberMe"] = value;
            }
        }
    }

}
