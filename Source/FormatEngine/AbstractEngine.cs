
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web.Configuration;
//using Amib.Threading;
using DataQuicker2.Framework;
using Shinetech.DAL;
//using WebSupergoo.ABCpdf7;
//using WebSupergoo.ABCpdf7.Objects;
//using DocV7 = WebSupergoo.ABCpdf7.Doc;
using DocPage = Shinetech.DAL.Page;
using WebPage = System.Web.UI.Page;

namespace Shinetech.Engines
{
	public abstract class AbstractEngine : IConvert {

        protected const string _licese = "719-253-05";
        protected static double _fixedWidth = Convert.ToDouble(WebConfigurationManager.AppSettings["FixedWidth"]);
        protected static double _fixedHeight = Convert.ToDouble(WebConfigurationManager.AppSettings["FixedHeight"]);
        protected static double _fixedRate = Convert.ToDouble(WebConfigurationManager.AppSettings["FixedRate"]);

        protected WorkItem _workItem;
		public virtual void Dispose(){}

        public virtual double GetDots4FixPixels(double pdfWidth, double pdfHeight, double fixedWidth, double fixedHeight, bool isLarge)
        {
            double factor = isLarge ? 2 : 1;
            bool fixedFactor = (pdfWidth / pdfHeight) > (fixedWidth / fixedHeight);
            double basePixels = fixedFactor ? fixedWidth : fixedHeight;

            return (basePixels / (fixedFactor ? pdfWidth : pdfHeight)) * factor;
        }

        public virtual int callbackExecute(PdfDesc result)
        {
            int DocumentId = 0;
            Document doc = new Document();
            doc.Name.Value = result.Name;
            doc.PagesCount.Value = result.Count;
            doc.PathName.Value = result.FilePath;
            doc.Size.Value = result.Size;
            doc.FileFolderID.Value = result.EFFID;
            doc.DividerID.Value = result.DividerID;
            doc.DOB.Value = DateTime.Now;
            doc.UID.Value = result.UID;
            doc.FileExtention.Value = (int)result.FileFormat;//modified by johnlu in 2009-04-27 for multiple file format


            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();

            IDbTransaction transaction = connection.BeginTransaction();
            try
            {
                doc.Create(transaction);

                foreach (PageDesc item in result.Pages.Values)
                {
                    DocumentId = doc.DocumentID.Value;
                    DocPage page = new DocPage();
                    page.PageNumber.Value = item._PageNumber;
                    page.DocumentID.Value = doc.DocumentID.Value;
                    page.ImageName.Value = item._Name;
                    page.ImagePath.Value = result.FilePath + Path.DirectorySeparatorChar + item._Name;
                    page.ImagePath2.Value = result.FilePath + Path.DirectorySeparatorChar + item._Name1;
                    page.DividerID.Value = doc.DividerID.Value;
                    page.Size.Value = 0;
                    page.Create(transaction);
                }

                FileFolder eff = new FileFolder(result.EFFID);
                eff.OtherInfo2.Value += doc.Size.Value;
                eff.Update(transaction);

                transaction.Commit();
                return DocumentId;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw new ApplicationException(ex.Message);
            }
 
        }

        /// convert pdf into jpeg
        /// </summary>
        /// <returns></returns>
        public virtual PdfDesc Execute()
        {
            /*int nk = 0;
            DocV7 theDoc = new DocV7();
            nk = 1;
            //XSettings.License = _licese;
            nk = 2;
            PdfDesc pdfDoc = new PdfDesc(_workItem.FileName);
            pdfDoc.EFFID = _workItem.FolderID;
            pdfDoc.DividerID = _workItem.DividerID;
            pdfDoc.UID = _workItem.UID;
            pdfDoc.FilePath = _workItem.Path;
            pdfDoc.FileFormat = _workItem.ContentType;

            try
            {
                theDoc.Units = "inches";
                XReadOptions options = this.GetXReadOptions();
                
                theDoc.Read(_workItem.Content, options);

                double width = theDoc.MediaBox.Width;
                double height = theDoc.MediaBox.Height;

                pdfDoc.TotalPages = theDoc.PageCount;
                //pdfDoc.Size = _workItem.Content.Length / (float)(1024 * 1024);//MB
                int n = theDoc.PageCount;
                double smallDots = GetDots4FixPixels(width, height, _fixedWidth, _fixedHeight, false);
                double largeDots = smallDots * _fixedRate;
                string tempV = string.Empty;
                long totalSize = 0;//

                for (int i = 1; i <= n; i++)
                {
                    tempV = string.Format("{0}_{1}_{2}_{3}", _workItem.FileName, _workItem.FolderID,
                        _workItem.DividerID, i);
                    PageDesc pageDesc = new PageDesc(i, tempV);
                    theDoc.PageNumber = i;
                    theDoc.Rendering.SaveAppend = false;
                    theDoc.Rendering.SaveQuality = 75;
                    theDoc.Rendering.DotsPerInchX = smallDots;
                    theDoc.Rendering.DotsPerInchY = smallDots;
                    theDoc.Rendering.AntiAliasImages = true;
                    theDoc.Rendering.AntiAliasText = true;

                    theDoc.Rect.String = theDoc.MediaBox.String;

                    using (MemoryStream ms0 = new MemoryStream())
                    {
                        theDoc.Rendering.Save(_workItem.MachinePath + Path.DirectorySeparatorChar + pageDesc._Name,
                        ms0);
                        totalSize += ms0.Length;
                        using (Image img0 = Image.FromStream(ms0))
                        {
                            img0.Save(_workItem.MachinePath + Path.DirectorySeparatorChar + pageDesc._Name);
                        }
                    }

                    theDoc.Rendering.SaveQuality = 85;
                    theDoc.Rendering.DotsPerInchX = 120;
                    theDoc.Rendering.DotsPerInchY = 120;
                    theDoc.Rendering.AntiAliasImages = true;
                    theDoc.Rendering.AntiAliasText = true;
                    using (MemoryStream ms1 = new MemoryStream())
                    {
                        theDoc.Rendering.Save(_workItem.MachinePath + Path.DirectorySeparatorChar + pageDesc._Name1,
                        ms1);
                        totalSize += ms1.Length;
                        using (Image img1 = Image.FromStream(ms1))
                        {
                            img1.Save(_workItem.MachinePath + Path.DirectorySeparatorChar + pageDesc._Name1);
                        }
                    }

                    pdfDoc.AddPage(pageDesc);
                }

                pdfDoc.Size = totalSize / (float)(1024 * 1024);//MB

            }
            catch (Exception exp)
            {
                throw new ApplicationException(exp.ToString() + ":n=" + nk);
            }
            finally
            {
                theDoc.Clear();
                _workItem.Content.Close();
            }
            
            return pdfDoc;*/

            return null;
        }


        /*public XReadOptions GetXReadOptions()
        {
            XReadOptions options = new XReadOptions();

            switch (_workItem.ContentType)
            {
                case StreamType.PDF:
                    options.ReadModule = ReadModuleType.Pdf;
                    options.FileExtension = ".pdf";
                    break;
                case StreamType.Word:
                    options.ReadModule = ReadModuleType.OpenOffice;
                    options.FileExtension = ".doc";
                    break;
                case StreamType.Excel:
                    options.ReadModule = ReadModuleType.OpenOffice;
                    options.OpenOfficeParameters["ReduceImageResolution"] = true;
                    options.OpenOfficeParameters["MaxImageResolution"] = 120;
                    options.FileExtension = ".xls";
                    break;
                case StreamType.PPT:
                    options.ReadModule = ReadModuleType.OpenOffice;
                    options.FileExtension = ".ppt";
                    break;
                case StreamType.RTF:
                    options.ReadModule = ReadModuleType.OpenOffice;
                    options.FileExtension = ".rtf";
                    break;

                case StreamType.ODT:
                    options.ReadModule = ReadModuleType.OpenOffice;
                    options.FileExtension = ".odt";
                    break;
                case StreamType.ODS:
                    options.ReadModule = ReadModuleType.OpenOffice;
                    options.FileExtension = ".ods";
                    break;
                case StreamType.ODP:
                    options.ReadModule = ReadModuleType.OpenOffice;
                    options.FileExtension = ".odp";
                    break;
                default:
                    break;
            }

            return options;
        }*/


        public virtual PdfDesc SmartExecute()
        {
            throw new NotImplementedException();
        }
    }

}