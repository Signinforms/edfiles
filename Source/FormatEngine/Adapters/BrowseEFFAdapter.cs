using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Xml;
using System.Xml.Serialization;
using com.flajaxian;
using DataQuicker2.Framework;
using Shinetech.DAL;
using Shinetech.Framework;
using Page=System.Web.UI.Page;

namespace Shinetech.Engines.Adapters
{
    public class BrowseEFFAdapter : FileUploaderAdapter 
    {
        public override void ProcessFile(HttpPostedFile file)
        {
            byte[] rawdata = new byte[file.InputStream.Length];
            file.InputStream.Read(rawdata, 0, (int) file.InputStream.Length);
            MemoryStream ms = new MemoryStream(rawdata);

            try
            {
                XmlSerializer xmls = new XmlSerializer(typeof (XmlEFFolder));
                XmlEFFolder eff = xmls.Deserialize(ms) as XmlEFFolder;
                int id = eff.folders[0].id;

                FileFolder fFolder = new FileFolder(id);
                if(fFolder.IsExist)
                {
                    
                }

            }catch(Exception exp)
            {
                Console.Write(exp.Message);
            }

        }
    }
}
