using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Xml;
using System.Xml.Serialization;
using com.flajaxian;
using DataQuicker2.Framework;
using Shinetech.DAL;
using Shinetech.Framework;
using Page = System.Web.UI.Page;

namespace Shinetech.Engines.Adapters
{
    public class EFFAdapter : FileUploaderAdapter
    {
        public override void ProcessFile(HttpPostedFile file)
        {
            string uid = this.Parent.State["UID"];
            //验证EFF文件
            //添加EFF到EFF表中

            byte[] rawdata = new byte[file.InputStream.Length];
            file.InputStream.Read(rawdata, 0, (int)file.InputStream.Length);
            MemoryStream ms = new MemoryStream(rawdata);


            try
            {
                XmlSerializer xmls = new XmlSerializer(typeof(XmlEFFolder));
                XmlEFFolder eff = xmls.Deserialize(ms) as XmlEFFolder;

                FileFolder folder = new FileFolder(eff.folders[0].id);
                if (folder.IsExist)
                {
                    EFFolder eFFolder = new EFFolder();

                    ObjectQuery query = eFFolder.CreateQuery();

                    query.SetCriteria(eFFolder.UserID, uid);
                    query.SetCriteria(eFFolder.FolderID, eff.folders[0].id);

                    IDbConnection connection = DbFactory.CreateConnection();
                    connection.Open();
                    DataTable table = new DataTable();
                    query.Fill(table);

                    if (table.Rows.Count == 0)
                    {
                        EFFolder objEFF = new EFFolder();
                        objEFF.FolderID.Value = folder.FolderID.Value;
                        objEFF.UserID.Value = uid;
                        objEFF.CreateDate.Value = DateTime.Now;

                        objEFF.Create(connection);
                    }

                }

            }
            catch (Exception exp)
            {
                Console.Write(exp.Message);
            }

        }
    }
}
