using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using com.flajaxian;
using System.IO;

namespace Shinetech.Engines.Adapters
{
    public class FileSaverAdapterEx : FileSaverAdapter
    {
        public override void ProcessFile(HttpPostedFile file)
        {
            if (!String.IsNullOrEmpty(this.FolderName))
            {
                this.Parent.Page.Session["FileName"] = file.FileName;
                string basePath = this.Parent.Page.Server.MapPath(this.FolderName);

                string folderID = this.Parent.State["EffID"];
                string dividerID = Parent.State["DividerID"];
                string path = basePath + Path.DirectorySeparatorChar +
                            folderID + Path.DirectorySeparatorChar + dividerID;

                string encodeName = HttpUtility.UrlPathEncode(file.FileName);
                encodeName = encodeName.Replace("%", "$");

                string fileName = String.Concat(
                        path,
                        Path.DirectorySeparatorChar,
                        encodeName
                    );
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

               

                file.SaveAs(fileName);
            }
        }
    }
}
