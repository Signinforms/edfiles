using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using com.flajaxian;

namespace Shinetech.Engines.Adapters
{
    public class ImageAdapter : FileUploaderAdapter 
    {
        private string _folderName;
        private string _imagePath;
        public event EventHandler<FileSaveCompletedEventArgs> FileSaveCompleted;

        //卢远宗 2015-11-02
        public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];

        public override void ProcessFile(HttpPostedFile file)
        {
            string fileName = String.Concat(
                        this.Parent.Page.Server.MapPath(this.FolderName),
                        Path.DirectorySeparatorChar,
                        file.FileName
                    );


            WorkItem item = new WorkItem();
            item.FolderID = Convert.ToInt32(Parent.State["EffID"]);
            item.DividerID = Convert.ToInt32(Parent.State["DividerID"]);
            item.Path = this.PathUrl + Path.DirectorySeparatorChar +
                        item.FolderID + Path.DirectorySeparatorChar + item.DividerID;

             item.MachinePath = string.Concat(this.Parent.Page.Server.MapPath(this.FolderName), Path.DirectorySeparatorChar ,item.FolderID,
             Path.DirectorySeparatorChar, item.DividerID);

            if (!Directory.Exists(item.MachinePath))
            {
                Directory.CreateDirectory(item.MachinePath);
            }
            
            item.FileName = Path.GetFileNameWithoutExtension(file.FileName);
            item.FullFileName = file.FileName;
            item.UID = this.Parent.State["UID"];
            item.contentType = getFileExtention(Path.GetExtension(file.FileName));
            item.Content = file.InputStream;
            try
            {
                IConvert task = EngineProvider.GetEngine(item);
                PdfDesc result = task.Execute();
                if (result!=null)
                {
                    task.callbackExecute(result);
                }
            }
            catch (Exception exp)
            {
                using (StreamWriter sw = new StreamWriter(fileName + ".txt"))
                {
                    sw.WriteLine(exp.ToString());
                    sw.WriteLine(DateTime.Now);
                }
            }
        }

        public static void ProcessPostFile(HttpPostedFile file,string folderId,string tabId,string uid,string path,string fileName)
        {
            WorkItem item = new WorkItem();
            item.FolderID = Convert.ToInt32(folderId);
            item.DividerID = Convert.ToInt32(tabId);
            item.Path = CurrentVolume + Path.DirectorySeparatorChar +
                        item.FolderID + Path.DirectorySeparatorChar + item.DividerID;

            item.MachinePath = path;
   
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            item.FileName = Path.GetFileNameWithoutExtension(file.FileName);
            item.FullFileName = file.FileName;
            item.UID = uid;
            item.contentType = getFileExtention(Path.GetExtension(file.FileName));
            item.Content = file.InputStream;
            try
            {
                IConvert task = EngineProvider.GetEngine(item);
                PdfDesc result = task.Execute();
                if (result != null)
                {
                    task.callbackExecute(result);
                }

            }
            catch (Exception exp)
            {
                using (StreamWriter sw = new StreamWriter(fileName + ".txt"))
                {
                    sw.WriteLine(exp.ToString());
                    sw.WriteLine(DateTime.Now);
                }
            }
        }

        /// <summary>
        /// Convert file to eff
        /// </summary>
        /// <param name="file">the file physical path with file name, which can use by filestream directly
        /// FileStream(file, FileMode.Open, FileAccess.Read))</param>
        /// <param name="folderId">folder Id</param>
        /// <param name="tabId">divider id</param>
        /// <param name="uid">uid</param>
        /// <param name="path">the machine path of this file under webserver diretory with file name</param>
        /// <param name="fileName">fileName with extension</param>
        public static void ProcessPostFile(string file, string folderId, string tabId, string uid, string path, string fileName)
        {
            using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                WorkItem item = new WorkItem();
                item.FolderID = Convert.ToInt32(folderId);
                item.DividerID = Convert.ToInt32(tabId);
                item.Path = CurrentVolume + Path.DirectorySeparatorChar +
                            item.FolderID + Path.DirectorySeparatorChar + item.DividerID;

                item.MachinePath = path; //

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                item.FileName = Path.GetFileNameWithoutExtension(fileName);
                item.FullFileName = Path.GetFileName(fileName);//with extension
                item.UID = uid;
                item.contentType = getFileExtention(Path.GetExtension(file));
                item.Content = stream;
                try
                {
                    IConvert task = EngineProvider.GetEngine(item);
                    PdfDesc result = task.Execute();
                    if (result != null)
                    {
                        task.callbackExecute(result);
                    }

                }
                catch (Exception exp)
                {
                    //using (StreamWriter sw = new StreamWriter(fileName + ".txt"))
                    //{
                    //    sw.WriteLine(exp.ToString());
                    //    sw.WriteLine(DateTime.Now);
                    //}
                }
            }
        }

        public static void ProcessPostFile(string file, string folderId, string tabId, string uid, string path, string fileName,string dispName,int docOrder)
        {
            using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                WorkItem item = new WorkItem();
                item.FolderID = Convert.ToInt32(folderId);
                item.DividerID = Convert.ToInt32(tabId);
                item.Path = CurrentVolume + Path.DirectorySeparatorChar +
                            item.FolderID + Path.DirectorySeparatorChar + item.DividerID;

                item.MachinePath = path; //

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                item.FileName = Path.GetFileNameWithoutExtension(fileName);
                item.FullFileName = Path.GetFileName(fileName);//with extension
                item.UID = uid;
                item.contentType = getFileExtention(Path.GetExtension(file));
                item.Content = stream;
                
                try
                {
                    IConvert task = EngineProvider.GetEngine(item);
                    PdfDesc result = task.Execute();
                    result.DocumentOrder = docOrder;
                    
                    if (result != null)
                    {
                        task.callbackExecute(result);
                    }

                }
                catch (Exception exp)
                {
                    //using (StreamWriter sw = new StreamWriter(fileName + ".txt"))
                    //{
                    //    sw.WriteLine(exp.ToString());
                    //    sw.WriteLine(DateTime.Now);
                    //}
                }
            }
        }

        /// <summary>
        /// Convert file to eff
        /// </summary>
        /// <param name="file">the file physical path with file name, which can use by filestream directly
        /// FileStream(file, FileMode.Open, FileAccess.Read))</param>
        /// <param name="folderId">folder Id</param>
        /// <param name="tabId">divider id</param>
        /// <param name="uid">uid</param>
        /// <param name="path">the machine path of this file under webserver diretory with file name</param>
        /// <param name="fileName">fileName with extension</param>
        public static object ProcessPostFile(object state)
        {
            string[] files = state as string[];

            using (FileStream stream = new FileStream(files[0], FileMode.Open, FileAccess.Read))
            {
                WorkItem item = new WorkItem();
                item.FolderID = Convert.ToInt32(files[1]);
                item.DividerID = Convert.ToInt32(files[2]);
                item.Path = CurrentVolume + Path.DirectorySeparatorChar +
                            item.FolderID + Path.DirectorySeparatorChar + item.DividerID;

                item.MachinePath = files[4];

                if (!Directory.Exists(files[4]))
                {
                    Directory.CreateDirectory(files[4]);
                }

                item.FileName = Path.GetFileNameWithoutExtension(files[5]);
                item.FullFileName = Path.GetFileName(files[5]);//with extension
                item.UID = files[3];
                item.contentType = getFileExtention(Path.GetExtension(files[0]));
                item.Content = stream;
                try
                {
                    IConvert task = EngineProvider.GetEngine(item);
                    PdfDesc result = task.SmartExecute();
                    if (result != null)
                    {
                        task.callbackExecute(result);
                    }

                }
            catch (Exception exp)
                {
                    
                    //using (StreamWriter sw = new StreamWriter(fileName + ".txt"))
                    //{
                    //    sw.WriteLine(exp.ToString());
                    //    sw.WriteLine(DateTime.Now);
                    //}
                }

                return state;
            }
        }

        /// <summary>
        /// Convert file to eff
        /// </summary>
        /// <param name="file"></param>
        /// <param name="folderId"></param>
        /// <param name="tabId"></param>
        /// <param name="uid"></param>
        /// <param name="path"></param>
        /// <param name="fileName"></param>
        /// <param name="fileId">FormFile ID</param>
        public static void ProcessPostFile(string file, string folderId, string tabId, string uid, string path, string fileName,int fileId)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                File.Copy(file, Path.Combine(path, fileName), true);
            }

            using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                WorkItem item = new WorkItem();
                item.FolderID = Convert.ToInt32(folderId);
                item.DividerID = Convert.ToInt32(tabId);
                item.Path = CurrentVolume + Path.DirectorySeparatorChar +
                            item.FolderID + Path.DirectorySeparatorChar + item.DividerID;

                item.MachinePath = path;
                item.FileID = fileId;

                item.FileName = Path.GetFileNameWithoutExtension(fileName);
                item.FullFileName = Path.GetFileName(fileName);//with extension
                item.UID = uid;
                item.contentType = getFileExtention(Path.GetExtension(file));
                item.Content = stream;
                try
                {
                    IConvert task = EngineProvider.GetEngine(item);
                    PdfDesc result = task.Execute();
                    if (result != null)
                    {
                        task.callbackExecute(result);
                    }

                }
                catch (Exception exp)
                {
                    using (StreamWriter sw = new StreamWriter(fileName + ".txt"))
                    {
                        sw.WriteLine(exp.ToString());
                        sw.WriteLine(DateTime.Now);
                    }

                    string strWrongInfor = String.Format("Failed to Convert the File- " + fileName);
                    throw new ApplicationException(strWrongInfor +": "+exp.Message);
                }
            }
        }

        public static void ProcessPostFile(string file, string folderId, string tabId, string uid, string path, string fileName, int fileId,int uFileId)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                string encodeName = HttpUtility.UrlPathEncode(fileName);
                encodeName = encodeName.Replace("%", "$");
                File.Copy(file, Path.Combine(path, encodeName), true);
            }

            using (FileStream stream = new FileStream(file, FileMode.Open, FileAccess.Read))
            {
                WorkItem item = new WorkItem();
                item.FolderID = Convert.ToInt32(folderId);
                item.DividerID = Convert.ToInt32(tabId);
                item.Path = CurrentVolume + Path.DirectorySeparatorChar +
                            item.FolderID + Path.DirectorySeparatorChar + item.DividerID;

                item.MachinePath = path;
                item.FileID = fileId;
                item.UFileID = uFileId;

                item.FileName = Path.GetFileNameWithoutExtension(fileName);
                item.FullFileName = Path.GetFileName(fileName);//with extension
                item.UID = uid;
                item.contentType = getFileExtention(Path.GetExtension(file));
                item.Content = stream;
                try
                {
                    IConvert task = EngineProvider.GetEngine(item);
                    PdfDesc result = task.Execute();
                    if (result != null)
                    {
                        task.callbackExecute(result);
                    }

                }
                catch (Exception exp)
                {
                    using (StreamWriter sw = new StreamWriter(fileName + ".txt"))
                    {
                        sw.WriteLine(exp.ToString());
                        sw.WriteLine(DateTime.Now);
                    }

                    string strWrongInfor = String.Format("Failed to Convert the File- " + fileName);
                    throw new ApplicationException(strWrongInfor + ": " + exp.Message);
                }
            }
        }

        protected static StreamType getFileExtention(string ext)
        {
            StreamType streamType = StreamType.PDF;

            switch (ext.ToLower())
            {
                case ".pdf":
                    streamType = StreamType.PDF;
                    break;
                case ".docx":
                case ".doc":
                    streamType = StreamType.Word;
                    break;
                case ".xls":
                    streamType = StreamType.Excel;
                    break;
                case ".ppt":
                    streamType = StreamType.PPT;
                    break;
                case ".rtf":
                    streamType = StreamType.RTF;
                    break;
                case ".odt":
                    streamType = StreamType.ODT;
                    break;
                case ".ods":
                    streamType = StreamType.ODS;
                    break;
                case ".odp":
                    streamType = StreamType.ODP;
                    break;
                case ".jpeg":
                case ".jpg":
                    streamType = StreamType.JPG;
                    break;
                case ".png":
                    streamType = StreamType.PNG;
                    break;
                default:
                    break;
            }

            return streamType;
        }

        protected virtual void OnFileSaveCompleted(FileSaveCompletedEventArgs args)
        {
            if (this.FileSaveCompleted != null)
            {
                this.FileSaveCompleted(this, args);
            }
        }

        public string FolderName
        {
            get { return this._folderName; }
            set { this._folderName = value; }
        }

        public string PathUrl
        {
            get { return this._imagePath; }
            set { this._imagePath = value; }
        }
    }

    public class FileSaveCompletedEventArgs : EventArgs
    {

        private System.IO.Stream _file;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        public FileSaveCompletedEventArgs(System.IO.Stream file)
        {
            this._file = file;
        }
        /// <summary>
        /// 
        /// </summary>
        public System.IO.Stream File
        {
            get { return this._file; }
        }

        
    }
}
