
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Configuration;
//using Amib.Threading;
using DataQuicker2.Framework;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using Shinetech.DAL;
//using WebSupergoo.ABCpdf7;
//using WebSupergoo.ABCpdf7.Objects;
//using DocV7 = WebSupergoo.ABCpdf7.Doc;
using DocPage = Shinetech.DAL.Page;
using WebPage = System.Web.UI.Page;



namespace Shinetech.Engines
{
    public class AnyFileEngine : PDF2Engine
    {
        protected static string openOfficeService = WebConfigurationManager.AppSettings["OpenOfficeService"];

        public AnyFileEngine(WorkItem item)
            : base(item)
        {
            
        }

        public override PdfDesc Execute()
        {
            PdfDesc pdfDoc = new PdfDesc(_workItem.FileName);
            pdfDoc.EFFID = _workItem.FolderID;
            pdfDoc.DividerID = _workItem.DividerID;
            pdfDoc.UID = _workItem.UID;
            pdfDoc.FilePath = _workItem.Path;
            pdfDoc.FileFormat = _workItem.ContentType;

            //any to pdf
            string encodeName = HttpUtility.UrlPathEncode(_workItem.FullFileName);
            encodeName = encodeName.Replace("%", "$");
            string source = string.Concat(_workItem.MachinePath, Path.DirectorySeparatorChar, encodeName); ;

            encodeName = HttpUtility.UrlPathEncode(_workItem.FileName);
            encodeName = encodeName.Replace("%", "$");

            string tempV = string.Concat(encodeName, "_", _workItem.FolderID, "_", _workItem.DividerID, ".pdf");
            string dstpdf = string.Concat(_workItem.MachinePath, Path.DirectorySeparatorChar, tempV);

            //OpenOfficeService.Objects.GenericSender.Init(openOfficeService);
            //OpenOfficeService.Objects.GenericSender.Receiver.ConvertToPDF(source, dstpdf);

            PdfDocument reader = PdfReader.Open(dstpdf);
            pdfDoc.TotalPages = reader.PageCount;
            reader.Close();

            //pdf to swf
            tempV = string.Concat(encodeName, "_", _workItem.FolderID, "_", _workItem.DividerID, "_%.swf");

            string dst = string.Concat(_workItem.MachinePath, Path.DirectorySeparatorChar, tempV);

            SwfToolService.Objects.GenericSender.Init(swfService);
            string result = SwfToolService.Objects.GenericSender.Receiver.ConvertToSWF(dstpdf, dst);

            string filename = string.Empty;
            for (int i = 1; i <= pdfDoc.TotalPages; i++)
            {

                filename = string.Concat(encodeName, "_", _workItem.FolderID, "_", _workItem.DividerID, "_", i);

                PageDesc pageDesc = new PageDesc(i, filename);

                pdfDoc.AddPage(pageDesc);
            }

            pdfDoc.Size = ((float)_workItem.Content.Length) / (1024 * 1024);

            return pdfDoc;
        }

    }

}