using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Net.Mime;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using Amib.Threading;
using Shinetech.Engines;
using Shinetech.Framework;

namespace Shinetech.Engines
{
/// <summary>
/// Summary description for EngineController
/// </summary>
    public class EngineProvider
    {
        static EngineProvider()
        {}

        static public IConvert GetEngine(WorkItem item)
        {
            IConvert convertor = new PDF3Engine(item);

            switch (item.ContentType)
            {
                case StreamType.PDF:
                    convertor = new PDF3Engine(item);
                    break;
                case StreamType.Word:
                case StreamType.Excel:
                case StreamType.PPT:
                case StreamType.RTF:
                case StreamType.ODP:
                case StreamType.ODS:
                case StreamType.ODT:
                    convertor = new AnyFileEngine(item);
                    break;
                case StreamType.JPG:
                    convertor = new Jpeg2Engine(item);
                    break;
                case StreamType.PNG:
                    convertor = new Png2Engine(item);
                    break;
                default:
                    break;
            }

            return convertor;
        }
    }
}
