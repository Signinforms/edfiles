
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Configuration;

using DataQuicker2.Framework;
using Shinetech.DAL;
using Aspose.Cells;
using XDocument = Aspose.Cells.Workbook;
using WebSupergoo.ABCpdf8;
using WebSupergoo.ABCpdf8.Objects;
using DocV8 = WebSupergoo.ABCpdf8.Doc;

using DocPage = Shinetech.DAL.Page;
using WebPage = System.Web.UI.Page;
using Aspose.Cells.Rendering.PdfSecurity;



namespace Shinetech.Engines
{
    public class Excel2Engine : PDF3Engine
    {
        public Excel2Engine(WorkItem item)
            : base(item)
        {
            
        }

        public override PdfDesc Execute()
        {
            PdfDesc pdfDoc = new PdfDesc(_workItem.FileName);
            pdfDoc.EFFID = _workItem.FolderID;
            pdfDoc.DividerID = _workItem.DividerID;
            pdfDoc.UID = _workItem.UID;
            pdfDoc.FilePath = _workItem.Path;
            pdfDoc.FileFormat = _workItem.ContentType;

            //any to pdf
            string encodeName = HttpUtility.UrlPathEncode(_workItem.FullFileName);
            encodeName = encodeName.Replace("%", "$");
            string source = string.Concat(_workItem.MachinePath, Path.DirectorySeparatorChar, encodeName); 

            string dstfile = string.Concat(encodeName, "_", _workItem.FolderID, "_", _workItem.DividerID, ".pdf");
            string dstpdf = string.Concat(_workItem.MachinePath, Path.DirectorySeparatorChar, dstfile);

            //ת��Ϊpdf
            XDocument xExcel = new XDocument(_workItem.Content);
            PdfSaveOptions options = new PdfSaveOptions(SaveFormat.Pdf);
            options.AllColumnsInOnePagePerSheet = true;

            xExcel.Save(dstpdf, options);

            //pdf to swf
            using (DocV8 v8 = new DocV8())
            {
                v8.Read(dstpdf);
                pdfDoc.TotalPages = v8.PageCount;

                string encodeName1 = HttpUtility.UrlPathEncode(_workItem.FileName);
                string filename = string.Empty;
                string tempV = string.Empty;

                encodeName1 = encodeName1.Replace("%", "$");

                for (int i = 1; i <= v8.PageCount; i++)
                {
                    Doc singlePagePdf = new Doc();
                    singlePagePdf.Append(v8);
                    singlePagePdf.RemapPages(i.ToString());
                    singlePagePdf.FrameRect();

                    filename = string.Concat(encodeName1, "_", _workItem.FolderID, "_", _workItem.DividerID, "_", i);
                    tempV = string.Concat(filename, ".swf");

                    singlePagePdf.Save(Path.Combine(_workItem.MachinePath, tempV));
                    singlePagePdf.Clear();

                    PageDesc pageDesc = new PageDesc(i, filename);
                    pdfDoc.AddPage(pageDesc);
                }


                pdfDoc.Size = ((float)_workItem.Content.Length) / (1024 * 1024);

                v8.Clear();
            }

            return pdfDoc;
        }

    }

}