
using System;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
//using Amib.Threading;
using DataQuicker2.Framework;
using Shinetech.DAL;
//using WebSupergoo.ABCpdf7;
//using WebSupergoo.ABCpdf7.Objects;
//using DocV7 = WebSupergoo.ABCpdf7.Doc;
using DocPage = Shinetech.DAL.Page;
using WebPage = System.Web.UI.Page;
using PdfSharp.Drawing;
using PdfSharp.Pdf;



namespace Shinetech.Engines
{
    public class Jpeg2Engine : PDF2Engine
    {

        public Jpeg2Engine(WorkItem item)
            : base(item)
        {

        }

        public override PdfDesc Execute()
        {
            PdfDesc pdfDoc = new PdfDesc(_workItem.FileName);
            pdfDoc.EFFID = _workItem.FolderID;
            pdfDoc.DividerID = _workItem.DividerID;
            pdfDoc.UID = _workItem.UID;
            pdfDoc.FilePath = _workItem.Path;
            pdfDoc.FileFormat = _workItem.ContentType;

            string encodeName = HttpUtility.UrlPathEncode(_workItem.FullFileName);
            encodeName = encodeName.Replace("%", "$");

            string source = string.Concat(_workItem.MachinePath, Path.DirectorySeparatorChar, encodeName);

            encodeName = HttpUtility.UrlPathEncode(_workItem.FileName);
            encodeName = encodeName.Replace("%", "$");
            string tempV = string.Concat(encodeName, "_", _workItem.FolderID, "_", _workItem.DividerID, "_1.swf");

            string dst = string.Concat(_workItem.MachinePath, Path.DirectorySeparatorChar, tempV);

            SwfToolService.Objects.GenericSender.Init(swfService);

            //convert image to pdf
            generatePDFfromImage(source, source.Split('.')[0] + ".pdf");


            string result = SwfToolService.Objects.GenericSender.Receiver.ConvertJpeg2SWF(source, dst);

            string filename = string.Concat(encodeName, "_", _workItem.FolderID, "_",
                       _workItem.DividerID, "_1");

            PageDesc pageDesc = new PageDesc(1, filename);

            pdfDoc.AddPage(pageDesc);

            pdfDoc.Size = ((float)_workItem.Content.Length) / (1024 * 1024);
            return pdfDoc;
        }

        public void generatePDFfromImage(string source, string destinaton)
        {
            PdfPage pg = new PdfPage();
            PdfDocument doc = new PdfDocument();
            doc.Pages.Add(pg);
            XGraphics xgr = XGraphics.FromPdfPage(doc.Pages[0]);
            XImage img = XImage.FromFile(source);

            xgr.DrawImage(img, 0, 0, img.PixelWidth > pg.Width.Value ? pg.Width.Value : img.PixelWidth, img.PixelHeight > pg.Height.Value ? pg.Height.Value : img.PixelHeight);
            doc.Save(destinaton);
            doc.Close();
            img.Dispose();
            img = null;
            xgr.Dispose();
            xgr = null;
            doc.Dispose();
            doc = null;
            return;
        }
    }

}