
using System;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
//using Amib.Threading;
using DataQuicker2.Framework;
using Shinetech.DAL;
//using WebSupergoo.ABCpdf7;
//using WebSupergoo.ABCpdf7.Objects;
//using DocV7 = WebSupergoo.ABCpdf7.Doc;
using DocPage = Shinetech.DAL.Page;
using WebPage = System.Web.UI.Page;



namespace Shinetech.Engines
{
    public class JpegEngine : AbstractEngine
    {

        public JpegEngine(WorkItem item)
        {
            _workItem = item;
        }

        public override PdfDesc Execute()
        {
            PdfDesc pdfDoc = new PdfDesc(_workItem.FileName);
            pdfDoc.EFFID = _workItem.FolderID;
            pdfDoc.DividerID = _workItem.DividerID;
            pdfDoc.UID = _workItem.UID;
            pdfDoc.FilePath = _workItem.Path;
            pdfDoc.FileFormat = _workItem.ContentType;

            pdfDoc.TotalPages = 1;


            string tempV = string.Format("{0}_{1}_{2}_{3}", _workItem.FileName, _workItem.FolderID,
                        _workItem.DividerID, 1);
            PageDesc pageDesc = new PageDesc(1, tempV);
            pdfDoc.Size = (int)_workItem.Content.Length/(float)(1024 * 1024);

            try
            {
                Image image = Image.FromStream(_workItem.Content);
                string pathname0 = _workItem.MachinePath + Path.DirectorySeparatorChar + pageDesc._Name;
                SavePictures(image, (int)_fixedWidth, (int)_fixedHeight, pathname0);

                string pathname1 = _workItem.MachinePath + Path.DirectorySeparatorChar + pageDesc._Name1;

                SavePictures(image, image.Width, image.Height, pathname1);

                pdfDoc.AddPage(pageDesc);
            }
            finally
            {
                _workItem.Content.Close();
            }

            return pdfDoc;
        }

        private static void SavePictures(Image img, int width, int height, string pathname)
        {
            try
            {
                double wscale = (double)width / (double)img.Width;
                double hscale = (double)height / (double)img.Height;
                double scale = wscale <= hscale ? wscale : hscale;
                int imgWidth = (int)((double)img.Width * scale);
                int imgHeight = (int)((double)img.Height * scale);
             
                
                Bitmap bmp = new Bitmap(imgWidth, imgHeight);
                Graphics gp = Graphics.FromImage(bmp);
                gp.SmoothingMode = SmoothingMode.HighQuality;
                gp.CompositingQuality = CompositingQuality.HighQuality;
                gp.InterpolationMode = InterpolationMode.High;
                Rectangle rect = new Rectangle(0, 0, imgWidth, imgHeight);
                gp.DrawImage(img, rect, 0, 0, img.Width, img.Height, GraphicsUnit.Pixel);

                bmp.Save(pathname, ImageFormat.Jpeg);
             
            }
            catch (Exception e)
            {
               
            }
        }
    }

}