using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Drawing;
using System.Web;
using System.Web.Configuration;
using System.Web.SessionState;
//using Amib.Threading;
using DataQuicker2.Framework;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using PdfSharp.Pdf.Security;
using Shinetech.DAL;
using Shinetech.Engines;
//using WebSupergoo.ABCpdf7;
//using WebSupergoo.ABCpdf7.Objects;
//using DocV7 = WebSupergoo.ABCpdf7.Doc;
using DocPage = Shinetech.DAL.Page;
using WebPage = System.Web.UI.Page;
using WELog;

namespace Shinetech.Engines
{

    public class PDF2Engine : PDFEngine
    {

        protected static string swfService= WebConfigurationManager.AppSettings["SwfToolsService"];

        public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];

        public PDF2Engine(WorkItem item)
            : base(item)
        {
           
        }

        public override int callbackExecute(PdfDesc result)
        {
            int DocumentId = 0;
            Document doc = new Document();
            doc.Name.Value = result.Name;
            doc.PagesCount.Value = result.Count;
            doc.PathName.Value = result.FilePath;
            doc.Size.Value = result.Size;
            doc.FileFolderID.Value = result.EFFID;
            doc.DividerID.Value = result.DividerID;
            doc.DOB.Value = DateTime.Now;
            doc.FileExtention.Value = (int)result.FileFormat;
            doc.UID.Value = result.UID;
           doc.DisplayName.Value = result.Name;
           doc.DocumentOrder.Value = result.DocumentOrder;

            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();

            IDbTransaction transaction = connection.BeginTransaction();
            try
            {
                doc.Create(transaction);

                foreach (PageDesc item in result.Pages.Values)
                {
                    DocumentId = doc.DocumentID.Value;
                    DocPage page = new DocPage();
                    page.PageNumber.Value = item._PageNumber;
                    page.DocumentID.Value = doc.DocumentID.Value;
                    page.ImageName.Value = item._swfName;
                    page.ImagePath.Value = CurrentVolume + Path.DirectorySeparatorChar +
                            doc.FileFolderID.Value + Path.DirectorySeparatorChar + doc.DividerID.Value + Path.DirectorySeparatorChar + item._swfName;  //result.FilePath + Path.DirectorySeparatorChar + item._swfName;
                  
                    page.DividerID.Value = doc.DividerID.Value;
                    page.Size.Value = 0;
                    page.Create(transaction);
                }

                FileFolder eff = new FileFolder(result.EFFID);
                eff.OtherInfo2.Value += doc.Size.Value;
                eff.Update(transaction);

                if(result.FileID>0)
                {
                    FormFile formFile = new FormFile(result.FileID);
                    if(formFile.IsExist)
                    {
                        formFile.Flag.Value = true;
                        formFile.ConvertDate.Value = DateTime.Now;
                        formFile.Update(transaction);
                    }
                }

                if (result.UFileID > 0)
                {
                    UploadFile uFile = new UploadFile(result.UFileID);
                    if (uFile.IsExist)
                    {
                        uFile.Flag.Value = "y";
                        uFile.ConvertedDate.Value = DateTime.Now;
                        uFile.Update(transaction);
                    }
                }
 
                transaction.Commit();
                result.DocumentID = DocumentId;
                return DocumentId;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                    connection = null;
                }
            }
        }

        public override PdfDesc Execute()
        {
            PdfDesc pdfDoc = new PdfDesc(_workItem.FileName);
            pdfDoc.EFFID = _workItem.FolderID;
            pdfDoc.DividerID = _workItem.DividerID;
            pdfDoc.UID = _workItem.UID;
            pdfDoc.FilePath = _workItem.Path;
            pdfDoc.FileFormat = _workItem.ContentType;
            pdfDoc.FileID = _workItem.FileID;
            pdfDoc.UFileID = _workItem.UFileID;

            string encodeName = HttpUtility.UrlPathEncode(_workItem.FullFileName);
            encodeName = encodeName.Replace("%", "$");

            string source = string.Concat(_workItem.MachinePath, Path.DirectorySeparatorChar, encodeName);

            /*try
            {
                PdfDocument reader = PdfReader.Open(_workItem.Content);
                PdfSecuritySettings security = reader.SecuritySettings;
                if (!security.PermitExtractContent)
                {
                    //skip this pdf
                    pdfDoc.ErrorStr = "Security Error";
                    IgnorePDF.Add(pdfDoc);
                    return null;
                }
                pdfDoc.TotalPages = reader.PageCount;
                reader.Close();

            }catch
            {
                //skip this pdf
                pdfDoc.ErrorStr = "Security Error";
                IgnorePDF.Add(pdfDoc);
                return null;
            }*/


            pdfDoc.TotalPages = PDFEngine.GetNoOfPagesPDF(_workItem.Content);
         
            string encodeName1 = HttpUtility.UrlPathEncode(_workItem.FileName);//without extension 
            encodeName1 = encodeName1.Replace("%", "$");
            string tempV = string.Concat(encodeName1, "_", _workItem.FolderID, "_", _workItem.DividerID, "_%.swf");

            string dst = string.Concat(_workItem.MachinePath, Path.DirectorySeparatorChar, tempV);

            WELog.ClsEventLog objClsEventLog = new ClsEventLog();

            try
            {
                SwfToolService.Objects.GenericSender.Init(swfService);
                string result = SwfToolService.Objects.GenericSender.Receiver.ConvertToSWF(source, dst);
                objClsEventLog.WriteToEventLog("MyEvent", "FormatEngine", result);
                string filename = string.Empty;
                for (int i = 1; i <= pdfDoc.TotalPages; i++)
                {
                    filename = string.Concat(encodeName1, "_", _workItem.FolderID, "_", _workItem.DividerID, "_", i);
                    PageDesc pageDesc = new PageDesc(i, filename);
               
                    pdfDoc.AddPage(pageDesc);
                }
            }
            catch (Exception exp )
            {

                objClsEventLog.WriteToEventLog("MyEvent", "FormatEngine", exp.Message);
            }

            pdfDoc.Size = ((float)_workItem.Content.Length)/(1024*1024);
            return pdfDoc;
        }

        public ArrayList IgnorePDF
        {
            get
            {
                Object ars = HttpContext.Current.Session["_IgnoredPDFs"];
                if(ars==null)
                {
                    ars = new ArrayList();
                    HttpContext.Current.Session["_IgnoredPDFs"] = ars;
                }
                return ars as ArrayList;
            }
        }
        

    }
}