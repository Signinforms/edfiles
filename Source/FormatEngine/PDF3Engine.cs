using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Drawing;
using System.Web;
using System.Web.Configuration;
using System.Web.SessionState;
using DataQuicker2.Framework;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using PdfSharp.Pdf.Security;
using Shinetech.DAL;
using Shinetech.Engines;
using WebSupergoo.ABCpdf8;
using WebSupergoo.ABCpdf8.Objects;
using DocV8 = WebSupergoo.ABCpdf8.Doc;
using DocPage = Shinetech.DAL.Page;
using WebPage = System.Web.UI.Page;
using WELog;
using System.Threading;

namespace Shinetech.Engines
{

    public class PDF3Engine : PDFEngine
    {

        protected static string swfService= WebConfigurationManager.AppSettings["SwfToolsService"];
        public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];

        public PDF3Engine(WorkItem item)
            : base(item)
        {
           
        }

        //添加元数据
        public override int callbackExecute(PdfDesc result)
        {
            int DocumentId = 0;
            Document doc = new Document();
            doc.Name.Value = result.Name;
            doc.PagesCount.Value = result.Count;
            doc.PathName.Value = result.FilePath;
            doc.Size.Value = result.Size;
            doc.FileFolderID.Value = result.EFFID;
            doc.DividerID.Value = result.DividerID;
            doc.DOB.Value = DateTime.Now;
            doc.UID.Value = result.UID;
            doc.DisplayName.Value = result.Name;
            doc.FileExtention.Value = (int)result.FileFormat;
            doc.DocumentOrder.Value = result.DocumentOrder;
          


            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();

            IDbTransaction transaction = connection.BeginTransaction();
            try
            {
                doc.Create(transaction);

                foreach (PageDesc item in result.Pages.Values)
                {
                    DocumentId = doc.DocumentID.Value;
                    DocPage page = new DocPage();
                    page.PageNumber.Value = item._PageNumber;
                    page.DocumentID.Value = doc.DocumentID.Value;
                    page.ImageName.Value = item._swfName;
                    page.ImagePath.Value = CurrentVolume + Path.DirectorySeparatorChar +
                            doc.FileFolderID.Value + Path.DirectorySeparatorChar + doc.DividerID.Value + Path.DirectorySeparatorChar + item._swfName;  //result.FilePath + Path.DirectorySeparatorChar + item._swfName;
                  
                    page.DividerID.Value = doc.DividerID.Value;
                    page.Size.Value = 0;
                    page.Create(transaction);
                }

                FileFolder eff = new FileFolder(result.EFFID);
                eff.OtherInfo2.Value += doc.Size.Value;
                eff.Update(transaction);

                if(result.FileID>0)
                {
                    FormFile formFile = new FormFile(result.FileID);
                    if(formFile.IsExist)
                    {
                        formFile.Flag.Value = true;
                        formFile.ConvertDate.Value = DateTime.Now;
                        formFile.Update(transaction);
                    }
                }

                if (result.UFileID > 0)
                {
                    UploadFile uFile = new UploadFile(result.UFileID);
                    if (uFile.IsExist)
                    {
                        uFile.Flag.Value = "y";
                        uFile.ConvertedDate.Value = DateTime.Now;
                        uFile.Update(transaction);
                    }
                }
 
                transaction.Commit();
                result.DocumentID = DocumentId;
                return DocumentId;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                using (StreamWriter sw = new StreamWriter(String.Format("{0}{1}{2}",result.Name,DateTime.Now.ToLongDateString(), ".txt")))
                {
                    sw.WriteLine(ex.ToString());
                    sw.WriteLine(DateTime.Now);
                }

                throw new ApplicationException(ex.Message);
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                    connection = null;
                }
            }
        }


        //添加信息的转换函数，先添加元数据，同时进行转换
        public override PdfDesc Execute()
        {
            PdfDesc pdfDoc = new PdfDesc(_workItem.FileName);
            pdfDoc.EFFID = _workItem.FolderID;
            pdfDoc.DividerID = _workItem.DividerID;
            pdfDoc.UID = _workItem.UID;
            pdfDoc.FilePath = _workItem.Path;
            pdfDoc.FileFormat = _workItem.ContentType;
            pdfDoc.FileID = _workItem.FileID;
            pdfDoc.UFileID = _workItem.UFileID;

            string encodeName = HttpUtility.UrlPathEncode(_workItem.FullFileName);
            encodeName = encodeName.Replace("%", "$");

            string source = string.Concat(_workItem.MachinePath, Path.DirectorySeparatorChar, encodeName);
            using (DocV8 v8 = new DocV8())
            {
                v8.Read(_workItem.Content);
                pdfDoc.TotalPages = v8.PageCount;

                string encodeName1 = HttpUtility.UrlPathEncode(_workItem.FileName);
                string filename = string.Empty;
                string tempV = string.Empty;

                encodeName1 = encodeName1.Replace("%", "$");

                for (int i = 1; i <= v8.PageCount; i++)
                {
                    //Doc singlePagePdf = new Doc();
                    //singlePagePdf.Units = v8.Units;
                    
                    //singlePagePdf.MediaBox.String = v8.MediaBox.String;
                    //singlePagePdf.Rect.String = v8.MediaBox.String;
                    //singlePagePdf.Units = UnitType.Points; 
                    //singlePagePdf.Append(v8);
                    //singlePagePdf.RemapPages(i.ToString());
                    //singlePagePdf.SaveOptions.Remap = true;
                    //singlePagePdf.SaveOptions.TemplateData = new XSaveTemplateData();
                    //singlePagePdf.SaveOptions.TemplateData.SetMeasureResolution(45);

                    filename = string.Concat(encodeName1, "_", _workItem.FolderID, "_", _workItem.DividerID, "_", i);
                    tempV = string.Concat(filename, ".swf");

                    //singlePagePdf.Save(Path.Combine(_workItem.MachinePath, tempV));
                    //singlePagePdf.Clear();

                    PageDesc pageDesc = new PageDesc(i, filename);
                    pdfDoc.AddPage(pageDesc);
                }
                

                pdfDoc.Size = ((float)_workItem.Content.Length) / (1024 * 1024);

                v8.Clear();
            }


            //把实际的转换放在线程池中执行
            ThreadPool.QueueUserWorkItem(new WaitCallback(this.ExecuteProc), pdfDoc);

            
            return pdfDoc;
        }

        private void ExecuteProc(object stateInfo)
        {
            PdfDesc pdfDoc = (PdfDesc)stateInfo;

            using (DocV8 v8 = new DocV8())
            {
                _workItem.Content.Position = 0;

                v8.Read(_workItem.Content);                

                for (int i = 1; i <= pdfDoc.Pages.Count; i++)
                {
                    PageDesc pageDesc = pdfDoc.Pages[i];

                    Doc singlePagePdf = new Doc();
                    singlePagePdf.Units = v8.Units;

                    singlePagePdf.MediaBox.String = v8.MediaBox.String;
                    singlePagePdf.Rect.String = v8.MediaBox.String;
                    singlePagePdf.Units = UnitType.Points;
                    singlePagePdf.Append(v8);
                    singlePagePdf.RemapPages(i.ToString());
                    singlePagePdf.SaveOptions.Remap = true;
                    singlePagePdf.SaveOptions.TemplateData = new XSaveTemplateData();
                    singlePagePdf.SaveOptions.TemplateData.SetMeasureResolution(45);


                    singlePagePdf.Save(Path.Combine(_workItem.MachinePath, pageDesc._swfName));
                    singlePagePdf.Clear();                  
                }

                v8.Clear();
            }
        }

        public override PdfDesc SmartExecute()
        {
            PdfDesc pdfDoc = new PdfDesc(_workItem.FileName);
            pdfDoc.EFFID = _workItem.FolderID;
            pdfDoc.DividerID = _workItem.DividerID;
            pdfDoc.UID = _workItem.UID;
            pdfDoc.FilePath = _workItem.Path;
            pdfDoc.FileFormat = _workItem.ContentType;
            pdfDoc.FileID = _workItem.FileID;
            pdfDoc.UFileID = _workItem.UFileID;

            string encodeName = HttpUtility.UrlPathEncode(_workItem.FullFileName);
            encodeName = encodeName.Replace("%", "$");

            string source = string.Concat(_workItem.MachinePath, Path.DirectorySeparatorChar, encodeName);
            using (DocV8 v8 = new DocV8())
            {
                v8.Read(_workItem.Content);
                pdfDoc.TotalPages = v8.PageCount;

                string encodeName1 = HttpUtility.UrlPathEncode(_workItem.FileName);
                string filename = string.Empty;
                string tempV = string.Empty;

                encodeName1 = encodeName1.Replace("%", "$");

                for (int i = 1; i <= v8.PageCount; i++)
                {
                    Doc singlePagePdf = new Doc();
                    singlePagePdf.Units = v8.Units;

                    singlePagePdf.MediaBox.String = v8.MediaBox.String;
                    singlePagePdf.Rect.String = v8.MediaBox.String;
                    singlePagePdf.Units = UnitType.Points;
                    singlePagePdf.Append(v8);
                    singlePagePdf.RemapPages(i.ToString());
                    singlePagePdf.SaveOptions.Remap = true;
                    singlePagePdf.SaveOptions.TemplateData = new XSaveTemplateData();
                    singlePagePdf.SaveOptions.TemplateData.SetMeasureResolution(45);

                    filename = string.Concat(encodeName1, "_", _workItem.FolderID, "_", _workItem.DividerID, "_", i);
                    tempV = string.Concat(filename, ".swf");

                    singlePagePdf.Save(Path.Combine(_workItem.MachinePath, tempV));
                    singlePagePdf.Clear();

                    PageDesc pageDesc = new PageDesc(i, filename);
                    pdfDoc.AddPage(pageDesc);
                }


                pdfDoc.Size = ((float)_workItem.Content.Length) / (1024 * 1024);

                v8.Clear();
            }


            //把实际的转换放在线程池中执行 // 因为批量转换非常多，因此采用动态线程池方式
           //ThreadPool.QueueUserWorkItem(new WaitCallback(this.ExecuteProc), pdfDoc);


            return pdfDoc;
        }


        public ArrayList IgnorePDF
        {
            get
            {
                Object ars = HttpContext.Current.Session["_IgnoredPDFs"];
                if(ars==null)
                {
                    ars = new ArrayList();
                    HttpContext.Current.Session["_IgnoredPDFs"] = ars;
                }
                return ars as ArrayList;
            }
        }
        

    }
}