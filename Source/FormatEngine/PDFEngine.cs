using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
//using Amib.Threading;
using DataQuicker2.Framework;
using Shinetech.DAL;
using Shinetech.Engines;
//using WebSupergoo.ABCpdf7;
//using WebSupergoo.ABCpdf7.Objects;
//using DocV7 = WebSupergoo.ABCpdf7.Doc;
using DocPage = Shinetech.DAL.Page;
using WebPage = System.Web.UI.Page;

namespace Shinetech.Engines
{
    public class PdfDesc
    {
        private string _Name;
        private int _totalCount;
        private float _totalSize;
        private static string _Path = WebConfigurationManager.AppSettings["PdfPath"];
        private Dictionary<int, PageDesc> _Pages = new Dictionary<int, PageDesc>();
        private int _EFFID;
        private int _DividerID;
        private string _uid;        
        private StreamType _fileFormat = StreamType.PDF;
        private string _error = string.Empty;

        private int _fileID;
        private int _uFileID;
        private int _documentOrder;
        private int _documentID;


        public PdfDesc(string name)
        {
            _Name = name;
        }

        public void AddPage(PageDesc page)
        {
            _Pages.Add(page._PageNumber, page);
        }

        #region Properties

        public string UID
        {
            get
            {
                return _uid;
            }
            set
            {
                _uid = value;
            }

        }
        public int Count
        {
            get
            {
                return _Pages.Count;
            }
        }

        public int TotalPages
        {
            get
            {
                return _totalCount;
            }
            set
            {
                _totalCount = value;
            }
        }

        public Dictionary<int, PageDesc> Pages
        {
            get
            {
                return _Pages;
            }
        }

        public string Name
        {
            get
            {
                return this._Name;
            }
        }

        public float Size
        {
            get
            {
                return this._totalSize;
            }
            set
            {
                _totalSize = value;
            }
        }

        public string FilePath
        {
            get
            {
                return _Path;
            }
            set
            {
                _Path = value;
            }
        }

        public int EFFID
        {
            get
            {
                return _EFFID;
            }
            set
            {
                _EFFID = value;
            }
        }

        public StreamType FileFormat
        {
            get
            {
                return _fileFormat;
            }

            set
            {
                _fileFormat = value;
            }
        }

        public int DividerID
        {
            get
            {
                return _DividerID;
            }

            set
            {
                _DividerID = value;
            }
       }

        public string ErrorStr
        {
            get
            {
                return _error;
            }
            set
            {
                _error = value;
            }

        }

        public int FileID
        {
            get
            {
                return _fileID;
            }
            set
            {
                _fileID = value;
            }
        }

        public int UFileID
        {
            get
            {
                return _uFileID;
            }
            set
            {
                _uFileID = value;
            }
        }
        public int DocumentOrder
        {
            get
            {
                return _documentOrder;
            }
            set
            {
                _documentOrder = value;
            }
        }
        public int DocumentID
        {
            get
            {
                return _documentID;
            }
            set
            {
                _documentID = value;
            }
        }

        #endregion
    }

    public class PageDesc
    {
        public string _Name;
        public string _Name1;
        public string _swfName;
        public string _Path;
        public int _PageNumber;
        public int _Size;

        public PageDesc(int index,string name)
        {
            _PageNumber = index;
            _Name = name + "_0.jpg";//level 0
            _Name1 = name + "_1.jpg";//level 1
            _swfName = name+".swf";
        }

        public string Path
        {
            get
            {
                return _Path;
            }

            set
            {
                _Path = value;
            }
        }
    }

	public class PDFEngine : AbstractEngine
    {
        //这显然是引用类传值，不是拷贝
        public PDFEngine(WorkItem item)
        {
            _workItem = item;
        }

        public static int GetNoOfPagesPDF(Stream fs)
        {
            int result = 0;

            StreamReader r = new StreamReader(fs);

            string pdfText = r.ReadToEnd();

            Regex regx = new Regex(@"/Type\s*/Page[^s]");

            MatchCollection matches = regx.Matches(pdfText);

            result = matches.Count;

            return result;
        }
        
	}

}