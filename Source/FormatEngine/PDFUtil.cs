using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.Configuration;

//using WebSupergoo.ABCpdf7;
//using WebSupergoo.ABCpdf7.Objects;
//using DocV7 = WebSupergoo.ABCpdf7.Doc;

namespace Shinetech.Engines
{
    public class PDFUtil
    {
        protected static string swfService = WebConfigurationManager.AppSettings["SwfToolsService"];

        public static bool Pdf2Swf(string pdfFile, int pageNumber, ref string fileNPage)
        {

            FileStream fs = null;
            try
            {
                string file1 = Path.GetFileNameWithoutExtension(pdfFile);
                string filePath = Path.GetDirectoryName(pdfFile);

                fileNPage = string.Concat(file1, "_", pageNumber, ".swf");
                string swfFile = string.Concat(filePath, Path.DirectorySeparatorChar, file1, "_", pageNumber, ".swf");
                if (File.Exists(swfFile))
                {
                    return true;
                }

                
                fs = new FileStream(pdfFile,FileMode.Open,FileAccess.Read);
                byte[] data = new byte[fs.Length];
                fs.Read(data, 0, (int)fs.Length);

                SwfToolService.Objects.GenericSender.Init(swfService);
                string result = SwfToolService.Objects.GenericSender.Receiver.ConvertToSWF(data, swfFile, pageNumber,1);

                return true;
            }
            catch (Exception exp)
            {
                string path = Path.GetDirectoryName(pdfFile);
                if (!path.EndsWith("\\"))
                {
                    path += "\\";
                }
                StringBuilder sb = new StringBuilder();
                sb.AppendLine(exp.Message);
                sb.AppendLine(pdfFile);
                sb.AppendLine("Page: " + pageNumber.ToString());
                string logPath = string.Concat(path, "error.log");
                WriteLog(logPath, sb.ToString());

                fileNPage = null;
                return false;
            }
            finally
            {
               if(fs!=null)
               {
                   fs.Close();
                   fs = null;
               }
            }
        }

        protected static void WriteLog(string fullName, string msg)
        {
            using (StreamWriter sw = new StreamWriter(fullName,true))
            {
                sw.WriteLine(msg+"  "+DateTime.Now.ToString());
                sw.Close();
            }

        }
    }
}
