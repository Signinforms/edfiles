
using System;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
//using Amib.Threading;
using DataQuicker2.Framework;
using Shinetech.DAL;
//using WebSupergoo.ABCpdf7;
//using WebSupergoo.ABCpdf7.Objects;
//using DocV7 = WebSupergoo.ABCpdf7.Doc;
using DocPage = Shinetech.DAL.Page;
using WebPage = System.Web.UI.Page;



namespace Shinetech.Engines
{
    public class Png2Engine : PDF2Engine
    {

        public Png2Engine(WorkItem item)
            : base(item)
        {
          
        }

        public override PdfDesc Execute()
        {
            PdfDesc pdfDoc = new PdfDesc(_workItem.FileName);
            pdfDoc.EFFID = _workItem.FolderID;
            pdfDoc.DividerID = _workItem.DividerID;
            pdfDoc.UID = _workItem.UID;
            pdfDoc.FilePath = _workItem.Path;
            pdfDoc.FileFormat = _workItem.ContentType;

            string encodeName = HttpUtility.UrlPathEncode(_workItem.FullFileName);
            encodeName = encodeName.Replace("%", "$");
            string source = string.Concat(_workItem.MachinePath, Path.DirectorySeparatorChar, encodeName);

            encodeName = HttpUtility.UrlPathEncode(_workItem.FileName);
            encodeName = encodeName.Replace("%", "$");
            string tempV = string.Concat(_workItem.FileName, "_", _workItem.FolderID, "_", _workItem.DividerID, "_1.swf");

            string dst = string.Concat(_workItem.MachinePath, Path.DirectorySeparatorChar, tempV);

            SwfToolService.Objects.GenericSender.Init(swfService);
            string result = SwfToolService.Objects.GenericSender.Receiver.ConvertPng2SWF(source, dst);

            string filename = string.Concat(encodeName, "_", _workItem.FolderID, "_",
                       _workItem.DividerID, "_1");

            PageDesc pageDesc = new PageDesc(1, filename);

            pdfDoc.AddPage(pageDesc);

            pdfDoc.Size = ((float)_workItem.Content.Length) / (1024 * 1024);
            return pdfDoc;
        } 
    }

}