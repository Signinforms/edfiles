
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Configuration;

using DataQuicker2.Framework;
using Shinetech.DAL;
using Aspose.Words;
using XDocument = Aspose.Words.Document;
using WebSupergoo.ABCpdf8;
using WebSupergoo.ABCpdf8.Objects;
using DocV8 = WebSupergoo.ABCpdf8.Doc;

using DocPage = Shinetech.DAL.Page;
using WebPage = System.Web.UI.Page;



namespace Shinetech.Engines
{
    public class WordEngine : PDF3Engine
    {
        public WordEngine(WorkItem item)
            : base(item)
        {
            
        }

        public override PdfDesc Execute()
        {
            PdfDesc pdfDoc = new PdfDesc(_workItem.FileName);
            pdfDoc.EFFID = _workItem.FolderID;
            pdfDoc.DividerID = _workItem.DividerID;
            pdfDoc.UID = _workItem.UID;
            pdfDoc.FilePath = _workItem.Path;
            pdfDoc.FileFormat = _workItem.ContentType;

            //any to pdf
            string encodeName = HttpUtility.UrlPathEncode(_workItem.FullFileName);
            encodeName = encodeName.Replace("%", "$");
            string source = string.Concat(_workItem.MachinePath, Path.DirectorySeparatorChar, encodeName); 

            string dstfile = string.Concat(encodeName, "_", _workItem.FolderID, "_", _workItem.DividerID, ".pdf");
            string dstpdf = string.Concat(_workItem.MachinePath, Path.DirectorySeparatorChar, dstfile);

            //ת��Ϊpdf
            XDocument xdoc = new XDocument(_workItem.Content);
            xdoc.Save(dstpdf, SaveFormat.Pdf);

            //pdf to swf
            using (DocV8 v8 = new DocV8())
            {
                v8.Read(dstpdf);
                pdfDoc.TotalPages = v8.PageCount;

                string encodeName1 = HttpUtility.UrlPathEncode(_workItem.FileName);
                string filename = string.Empty;
                string tempV = string.Empty;

                encodeName1 = encodeName1.Replace("%", "$");

                for (int i = 1; i <= v8.PageCount; i++)
                {
                    Doc singlePagePdf = new Doc();

                    singlePagePdf.MediaBox.String = v8.MediaBox.String;
                    singlePagePdf.Rect.String = v8.MediaBox.String;
                    singlePagePdf.Units = UnitType.Points;
                    singlePagePdf.Append(v8);
                    singlePagePdf.RemapPages(i.ToString());
                    singlePagePdf.SaveOptions.Remap = true;
                    singlePagePdf.SaveOptions.TemplateData = new XSaveTemplateData();
                    singlePagePdf.SaveOptions.TemplateData.SetMeasureResolution(45);

                    filename = string.Concat(encodeName1, "_", _workItem.FolderID, "_", _workItem.DividerID, "_", i);
                    tempV = string.Concat(filename, ".swf");

                    singlePagePdf.Save(Path.Combine(_workItem.MachinePath, tempV));
                    singlePagePdf.Clear();

                    PageDesc pageDesc = new PageDesc(i, filename);
                    pdfDoc.AddPage(pageDesc);
                }


                pdfDoc.Size = ((float)_workItem.Content.Length) / (1024 * 1024);

                v8.Clear();
            }

            return pdfDoc;
        }

    }

}