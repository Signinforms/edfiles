using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Shinetech.Engines
{
    public class WorkItem
    {
        private string fileName;
        private string fullfileName;
        private System.IO.Stream stream;
        private string VPath;
        private int folderID;
        private int dividerID;
        private string uID;
        private string machinePath;

        private int fileId;
        private int uFileId;

        public StreamType contentType;

        public string FileName
        {
            get
            {
                return fileName;
            }
            set
            {
                fileName = value;
            }
        }

        public string FullFileName
        {
            get
            {
                return fullfileName;
            }
            set
            {
                fullfileName = value;
            }
        }



        public string Path
        {
            get
            {
                return VPath;
            }
            set
            {
                VPath = value;
            }
        }

        public int FolderID
        {
            get
            {
                return folderID;
            }
            set
            {
                folderID = value;
            }
        }

        public int DividerID
        {
            get
            {
                return dividerID;
            }
            set
            {
                dividerID = value;
            }
        }

        public string UID
        {
            get
            {
                return uID;
            }
            set
            {
                uID = value;
            }
        }

        public System.IO.Stream Content
        {
            get
            {
                return stream;
            }
            set
            {
                stream = value;
            }
        }

        public StreamType ContentType
        {
            get
            {
                return contentType;
            }
            set
            {
                contentType = value;
            }
        }

        public string MachinePath
        {
            get
            {
                return machinePath;
            }
            set
            {
                machinePath = value;
            }
        }

        public int FileID
        {
            get
            {
                return fileId;
            }
            set
            {
                fileId = value;
            }
        }

        public int UFileID
        {
            get
            {
                return uFileId;
            }
            set
            {
                uFileId = value;
            }
        }
    }

    public enum StreamType
    {
        PDF=1,
        Word,
        Excel,
        PPT,
        Html,
        XPS,
        Text,
        RTF,
        ODT,
        ODS,
        ODP,
        JPG,
        PNG,
        OTHERS
    }
}
