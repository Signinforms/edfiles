//Author: ?Luis Ramirez 2008
//Web site: http://www.sqlnetframework.com
//Creation date: May 8, 2008

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Shinetech.Framework
{    
    public abstract class ColumnSortField : BoundField
    {
        private DataControlCellType _cellType;
        private DataControlRowState _rowState;

        public ColumnSortField()
        {
        }

        public virtual string SortedColumnCssClass
        {
            get
            {
                object obj = this.ViewState["SortedColumnCssClass"];
                if (obj != null)
                {
                    return (string)obj;
                }
                return string.Empty;
            }
            set
            {
                if (!object.Equals(value, this.ViewState["SortedColumnCssClass"]))
                {
                    this.ViewState["SortedColumnCssClass"] = value;
                    this.OnFieldChanged();
                }
            }
        }

        public virtual string ColumnCssClass
        {
            get
            {
                object obj = this.ViewState["ColumnCssClass"];
                if (obj != null)
                {
                    return (string)obj;
                }
                return string.Empty;
            }
            set
            {
                if (!object.Equals(value, this.ViewState["ColumnCssClass"]))
                {
                    this.ViewState["ColumnCssClass"] = value;
                    this.OnFieldChanged();
                }
            }
        }

        protected abstract bool IsValidDataBoundControl(Control namingContainer);

        protected abstract bool IsSelectedSortingColumn(Control dataBoundControl);

        protected abstract SortDirection GetColumnSortDirection(Control dataBoundControl);
 
        public override void InitializeCell(DataControlFieldCell cell, DataControlCellType cellType, DataControlRowState rowState, int rowIndex)
        {
            base.InitializeCell(cell, cellType, rowState, rowIndex);

            this._cellType = cellType;
            this._rowState = rowState;
    
            cell.Load += new EventHandler(this.OnCellLoad);            
        }

        protected virtual Control GetDataBoundControl(Control namingContainer)
        {
            if (this.IsValidDataBoundControl(namingContainer))
                return namingContainer;

            if (namingContainer.NamingContainer != null)
            {
                if (this.IsValidDataBoundControl(namingContainer.NamingContainer))
                    return namingContainer.NamingContainer;
            }

            return null;
        }
        
        private void OnCellLoad(object sender, EventArgs args)
        {
            DataControlFieldCell cell = (DataControlFieldCell)sender;

            Control dataBoundControl = this.GetDataBoundControl(cell.NamingContainer);

            bool isColumnSorted = this.IsSelectedSortingColumn(dataBoundControl);
            string columCssClass = isColumnSorted ? this.SortedColumnCssClass : this.ColumnCssClass;

            switch (this._cellType)
            {
                case DataControlCellType.DataCell:
                    if (this._rowState == DataControlRowState.Normal)
                        cell.CssClass = columCssClass + "Normal";
                    else if (this._rowState == DataControlRowState.Alternate)
                        cell.CssClass = columCssClass + "Alternate";
                    else if (this._rowState == DataControlRowState.Selected)
                        cell.CssClass = columCssClass + "Selected";
                    else if (this._rowState == DataControlRowState.Edit)
                        cell.CssClass = columCssClass + "Edit";
                    else if (this._rowState == DataControlRowState.Insert)
                        cell.CssClass = columCssClass + "Insert";
                    
                    break;
                case DataControlCellType.Header:
                    if (isColumnSorted)
                        cell.CssClass = (this.GetColumnSortDirection(dataBoundControl) == SortDirection.Ascending) ? this.SortedColumnCssClass + "HeaderAscending" : this.SortedColumnCssClass + "HeaderDescending";
                    else
                        cell.CssClass = columCssClass + "Header";

                    break;
            }
        }
    }
}