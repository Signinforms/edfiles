using System;
using System.IO;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Collections;

namespace Shinetech.Framework.Controls
{
    #region PagerStyle enum
    /// <summary>
    /// 分页样式
    /// </summary>
    public enum PagerStyle
    {
        NextPrev,
        NumericPages,
        OnlyNextPrev
    }
    #endregion

    #region PageChangedEventArgs class
    public class PageChangedEventArgs : EventArgs
    {
        public int OldPageIndex;
        public int NewPageIndex;
    }

    public class PageSizeChangeEventArgs : System.EventArgs
    {
        public int NewPageSize;
        public PageSizeChangeEventArgs(int pNewPageSize)
        {
            NewPageSize = pNewPageSize;
        }
    }

    #endregion

    #region WebPager Control
    [DefaultEvent("PageIndexChanged")]
    [ToolboxData("<{0}:WebPager runat=\"server\" />")]
    public class WebPager : WebControl, INamingContainer
    {
        #region  private members
        private PagedDataSource _pageDataSource;
        private Control _controlToPaginate;
        private DropDownList ddlNumberOfPages = new DropDownList();

        public delegate void PageSizeChangeHandler(object s, PageSizeChangeEventArgs e);
        public event PageSizeChangeHandler PageSizeChanged;

        private string CacheKeyName
        {
            get { return Page.Request.FilePath + "_" + UniqueID + "_Data"; }
        }
        private string CurrentPageText = "共&nbsp{2}&nbsp条记录&nbsp--&nbsp第&nbsp{0}&nbsp页&nbsp--&nbsp共&nbsp{1}&nbsp页";
        private string NoPageSelectedText = "";
        #endregion

        #region 构造函数
        public WebPager()
            : base()
        {
            _pageDataSource = null;
            _controlToPaginate = null;

            PagerStyle = PagerStyle.NextPrev;
            CurrentPageIndex = 0;
            this.PageSize = 15;
            TotalPages = -1;
            TotalRecord = -1;
        }
        #endregion

        #region 属性

        #region 属性DataMember
        [
        Category("Data"),
        DefaultValue(""),
        Description("获取或者设置绑定的数据成员.")
        ]
        public virtual string DataMember
        {
            get
            {
                string s = (string)ViewState["DataMember"];
                return (s == null) ? String.Empty : s;
            }
            set
            {
                ViewState["DataMember"] = value;
            }
        }
        #endregion

        #region 属性DataSource
        private DataTable _dataSource;
        [Bindable(true),
        Category("Data"),
        DefaultValue(null),
        Description("获取或者设置数据源"),
        DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public virtual DataTable DataSource
        {
            get
            {
                return _dataSource;
            }
            set
            {
                //if ((value != null) || (value is IListSource) || (value is IEnumerable))
                {
                    _dataSource = value;
                }
                //else
                //{
                //    throw new ArgumentException();
                //}
            }
        }
        #endregion

        #region PagerStyle
        /// <summary>
        /// 分页样式
        /// </summary>
        [Description("设置分页样式")]
        public PagerStyle PagerStyle
        {
            get { return (PagerStyle)ViewState["PagerStyle"]; }
            set { ViewState["PagerStyle"] = value; }
        }
        #endregion

        #region ControlToPaginate
        /// <summary>
        /// 获取或设置要实现分页的控件ID
        /// </summary>
        [Description("获取或设置要实现分页的控件")]
        public string ControlToPaginate
        {
            get { return (string)(ViewState["ControlToPaginate"]); }
            set { ViewState["ControlToPaginate"] = value; }
        }
        #endregion

        #region PageSize
        /// <summary>
        /// 获取或设置要在单页上显示的项数。
        /// </summary>
        [Description("获取或设置要在单页上显示的项数。")]
        public int PageSize
        {
            get { return Convert.ToInt32(ViewState["PageSize"]); }
            set { ViewState["PageSize"] = value; }
        }
        #endregion

        #region CurrentPageIndex
        /// <summary>
        /// 获取或设置当前页的索引
        /// </summary>
        [Description("获取或设置当前页的索引")]
        public int CurrentPageIndex
        {
            get { return Convert.ToInt32(ViewState["CurrentPageIndex"]); }
            set { ViewState["CurrentPageIndex"] = value; }
        }
        #endregion

        #region PageCount,TotalPages,TotalRecord
        /// <summary>
        /// Gets the number of displayable pages 
        /// </summary>
        [Browsable(false)]
        public int PageCount
        {
            get { return TotalPages; }
        }

        /// <summary>
        /// Gets and sets the number of pages to display 
        /// </summary>
        protected int TotalPages
        {
            get { return Convert.ToInt32(ViewState["TotalPages"]); }
            set { ViewState["TotalPages"] = value; }
        }

        /// <summary>
        /// Gets and sets the number of pages to display 
        /// </summary>
        protected int TotalRecord
        {
            get { return Convert.ToInt32(ViewState["TotalRecord"]); }
            set { ViewState["TotalRecord"] = value; }
        }
        #endregion

        #endregion

        #region 公共函数

        #region 事件 PageIndexChanged
        // Fires when the pager is about to switch to a new page
        public delegate void PageChangedEventHandler(object sender, PageChangedEventArgs e);
        public event PageChangedEventHandler PageIndexChanged;
        protected virtual void OnPageIndexChanged(PageChangedEventArgs e)
        {
            if (PageIndexChanged != null)
                PageIndexChanged(this, e);
        }

        protected virtual void OnPageSizeChanged(PageSizeChangeEventArgs e)
        {
            if (PageSizeChanged != null)
                PageSizeChanged(this, e);
        }
        #endregion

        #region OVERRIDE DataBind
        // Fetches and stores the data
        public override void DataBind()
        {
            // Fires the data binding event
            base.DataBind();

            // Controls must be recreated after data binding 
            ChildControlsCreated = false;

            // Ensures the control exists and is a list control
            if (ControlToPaginate == null) return;
            _controlToPaginate = LoopingControls(Page);// Page.FindControl(ControlToPaginate);
            if (_controlToPaginate == null) return;

            if (!(_controlToPaginate is BaseDataList
                || _controlToPaginate is GridView
                || _controlToPaginate is Repeater))
                return;

            // Fetch data
            FetchPageData();

            // Bind data to the buddy control
            BaseDataList baseDataListControl = null;
            if (_controlToPaginate is BaseDataList)
            {
                baseDataListControl = (BaseDataList)_controlToPaginate;
                baseDataListControl.DataSource = _pageDataSource;
                baseDataListControl.DataBind();
                return;
            }

            GridView gridView1 = null;
            if (_controlToPaginate is GridView)
            {
                gridView1 = (GridView)_controlToPaginate;
                gridView1.AllowPaging = true;
                gridView1.PageSize = this.PageSize;
                gridView1.PagerSettings.Visible = false;

                gridView1.PageIndex = _pageDataSource.CurrentPageIndex;
                gridView1.DataSource = _pageDataSource;
                gridView1.DataBind();
                return;
            }

            Repeater repeater1 = null;
            if (_controlToPaginate is Repeater)
            {
                repeater1 = (Repeater)_controlToPaginate;
                repeater1.DataSource = _pageDataSource;
                repeater1.DataBind();
                return;
            }
        }
        #endregion

        #region OVERRIDE Render
        // Writes the content to be rendered on the client
        protected override void Render(HtmlTextWriter output)
        {
            if (Site != null && Site.DesignMode)
                CreateChildControls();

            base.Render(output);
        }

        // OVERRIDE CreateChildControls
        // Outputs the HTML markup for the control
        protected override void CreateChildControls()
        {
            Controls.Clear();
            ClearChildViewState();

            BuildControlHierarchy();
        }
        #endregion

        #endregion

        #region 私有函数

        #region BuildControlHierarchy
        // Control the building of the control's hierarchy
        private void BuildControlHierarchy()
        {
            if (this.TotalRecord <= 0) return;

            // Build the surrounding table (one row, two cells)
            Table t = new Table();
            t.Width = new Unit("100%");
            // Build the table row
            TableRow row = new TableRow();
            t.Rows.Add(row);

           if (PagerStyle == PagerStyle.OnlyNextPrev)
            {
                TableCell cellPagePrev = new TableCell();
                cellPagePrev.HorizontalAlign = HorizontalAlign.Left;
                BuildPagePrevious(cellPagePrev);
                row.Cells.Add(cellPagePrev);

                TableCell cellNavBar = new TableCell();
                BuildPageSizeUI(cellNavBar);
                cellNavBar.HorizontalAlign = HorizontalAlign.Center;
                row.Cells.Add(cellNavBar);

                TableCell cellPageNext = new TableCell();
                cellPageNext.HorizontalAlign = HorizontalAlign.Right;
                BuildNumericPagesUI(cellPageNext);
                BuildPageNext(cellPageNext);

                row.Cells.Add(cellPageNext);
            }
            else
            {
                // Build the cell with the page index
                TableCell cellPageDesc = new TableCell();
                cellPageDesc.HorizontalAlign = HorizontalAlign.Left;
                BuildCurrentPage(cellPageDesc);
                row.Cells.Add(cellPageDesc);

                // Build the cell with navigation bar
                TableCell cellNavBar = new TableCell();
                if (PagerStyle == PagerStyle.NextPrev)
                    BuildNextPrevUI(cellNavBar);
                else
                {
                    BuildNumericPagesUI(cellNavBar);
                }
                cellNavBar.HorizontalAlign = HorizontalAlign.Right;
                row.Cells.Add(cellNavBar);
            }
          
            // Add the table to the control tree
            Controls.Add(t);
        }

        #endregion

        #region	BuildNextPrevUI
        //Generates the only next and prev navigation bar
        private void BuildPageNext(TableCell cell)
        {
            bool isValidPage = (CurrentPageIndex >= 0 && CurrentPageIndex <= TotalPages - 1);
            bool canMoveBack = (CurrentPageIndex > 0);
            bool canMoveForward = (CurrentPageIndex < TotalPages - 1);

            // Render the > button
            LinkButton next = new LinkButton();
            next.CssClass = "";
            next.ID = "Next";
            next.CausesValidation = false;
            next.Click += new EventHandler(next_Click);
            next.Text = "Next "+ this.PageSize+" > ";
            next.Enabled = isValidPage && canMoveForward;
            cell.Controls.Add(next);
        }

        private void BuildPagePrevious(TableCell cell)
        {
            bool isValidPage = (CurrentPageIndex >= 0 && CurrentPageIndex <= TotalPages - 1);
            bool canMoveBack = (CurrentPageIndex > 0);
            bool canMoveForward = (CurrentPageIndex < TotalPages - 1);

            // Render the < button
            LinkButton prev = new LinkButton();
            prev.CssClass = "";
            prev.ID = "Prev";
            prev.CausesValidation = false;
            prev.Click += new EventHandler(prev_Click);
            prev.Text = " < Prev " + this.PageSize;
            prev.Enabled = isValidPage && canMoveBack;
            cell.Controls.Add(prev);
        }


        // Generates the HTML markup for the Next/Prev navigation bar
        private void BuildNextPrevUI(TableCell cell)
        {
            bool isValidPage = (CurrentPageIndex >= 0 && CurrentPageIndex <= TotalPages - 1);
            bool canMoveBack = (CurrentPageIndex > 0);
            bool canMoveForward = (CurrentPageIndex < TotalPages - 1);

            // Render the << button
            LinkButton first = new LinkButton();
            first.ID = "First";
            first.CausesValidation = false;
            first.Click += new EventHandler(first_Click);
            first.Text = " First ";//首页
            first.Enabled = isValidPage && canMoveBack;
            cell.Controls.Add(first);

            // Add a separator
            cell.Controls.Add(new LiteralControl("&nbsp;"));

            // Render the < button
            LinkButton prev = new LinkButton();
            prev.ID = "Prev";
            prev.CausesValidation = false;
            prev.Click += new EventHandler(prev_Click);
            prev.Text = " < Prev ";
            prev.Enabled = isValidPage && canMoveBack;
            cell.Controls.Add(prev);

            // Add a separator
            cell.Controls.Add(new LiteralControl("&nbsp;"));

            // Render the > button
            LinkButton next = new LinkButton();
            next.ID = "Next";
            next.CausesValidation = false;
            next.Click += new EventHandler(next_Click);
            next.Text = " Next " + " > ";
            next.Enabled = isValidPage && canMoveForward;
            cell.Controls.Add(next);

            // Add a separator
            cell.Controls.Add(new LiteralControl("&nbsp;"));

            // Render the >> button
            LinkButton last = new LinkButton();
            last.ID = "Last";
            last.CausesValidation = false;
            last.Click += new EventHandler(last_Click);
            last.Text = " Last ";//尾页
            last.Enabled = isValidPage && canMoveForward;
            cell.Controls.Add(last);

            // Render a drop-down list  
            DropDownList pageList = new DropDownList();
            pageList.ID = "PageList";
            pageList.AutoPostBack = true;
            pageList.SelectedIndexChanged += new EventHandler(PageList_Click);
            pageList.Font.Name = Font.Name;
            pageList.Font.Size = Font.Size;
            pageList.ForeColor = ForeColor;

            // Embellish the list when there are no pages to list 
            if (TotalPages <= 0 || CurrentPageIndex == -1)
            {
                pageList.Items.Add("");
                pageList.Enabled = false;
                pageList.SelectedIndex = 0;
            }
            else // Populate the list
            {
                for (int i = 1; i <= TotalPages; i++)
                {
                    ListItem item = new ListItem(i.ToString(), (i - 1).ToString());
                    pageList.Items.Add(item);
                }
                pageList.SelectedIndex = CurrentPageIndex;
            }
            cell.Controls.Add(pageList);
        }
        #endregion

        #region	BuildNumericPagesUI
        // Generates the HTML markup for the Numeric Pages button bar
        private void BuildNumericPagesUI(TableCell cell)
        {
            // Render a drop-down list  
            DropDownList pageList = new DropDownList();
            pageList.ID = "PageList";
            pageList.AutoPostBack = true;
            pageList.SelectedIndexChanged += new EventHandler(PageList_Click);
            pageList.Font.Name = Font.Name;
            pageList.Font.Size = Font.Size;
            pageList.ForeColor = ForeColor;

            // Embellish the list when there are no pages to list 
            if (TotalPages <= 0 || CurrentPageIndex == -1)
            {
                pageList.Items.Add("No pages");
                pageList.Enabled = false;
                pageList.SelectedIndex = 0;
            }
            else // Populate the list
            {
                for (int i = 1; i <= TotalPages; i++)
                {
                    ListItem item = new ListItem(i.ToString(), (i - 1).ToString());
                    pageList.Items.Add(item);
                }
                pageList.SelectedIndex = CurrentPageIndex;
            }
            cell.Controls.Add(pageList);
        }

        private void BuildPageSizeUI(TableCell cell)
        {
            // Render a drop-down list  
            DropDownList pageSizeList = new DropDownList();
            pageSizeList.ID = "PageSizeList";
         
            pageSizeList.AutoPostBack = true;
            pageSizeList.SelectedIndexChanged += new EventHandler(PageSizeList_Click);
            pageSizeList.Font.Name = Font.Name;
            pageSizeList.Font.Size = Font.Size;
            pageSizeList.ForeColor = ForeColor;

            ListItem item15 = new ListItem("- 15 -", "15");
            pageSizeList.Items.Add(item15);

            ListItem item25 = new ListItem("- 25 -", "25");
            pageSizeList.Items.Add(item25);

            ListItem item50 = new ListItem("- 50 -", "50");
            pageSizeList.Items.Add(item50);

            pageSizeList.SelectedValue = this.PageSize.ToString();
            cell.Controls.Add(pageSizeList);
        }
        #endregion

        #region BuildCurrentPage
        // Generates the HTML markup to describe the current page (0-based)
        private void BuildCurrentPage(TableCell cell)
        {
            // Use a standard template: Page X of Y
            if (CurrentPageIndex < 0 || CurrentPageIndex >= TotalPages)
                cell.Text = NoPageSelectedText;
            else
                cell.Text = NoPageSelectedText;//String.Format(CurrentPageText, (CurrentPageIndex + 1), TotalPages, TotalRecord);
        }
        #endregion

        #region FetchPageData
        // Runs the query to get only the data that fit into the current page
        private void FetchPageData()
        {
            if (_pageDataSource == null) _pageDataSource = new PagedDataSource();
            _pageDataSource.DataSource = this.DataSource.DefaultView; // must be IEnumerable!;//GetDataSource();
            _pageDataSource.AllowPaging = true;
            _pageDataSource.PageSize = this.PageSize;
            _pageDataSource.VirtualCount = this.PageSize;

            TotalRecord = _pageDataSource.DataSourceCount;
            TotalPages = _pageDataSource.PageCount;

            if (0 <= CurrentPageIndex && CurrentPageIndex < TotalPages)//注意PageIndex是从0开始的
                _pageDataSource.CurrentPageIndex = CurrentPageIndex;
            else //if (CurrentPageIndex > TotalPages)
            {
                _pageDataSource.CurrentPageIndex = TotalPages - 1;//设为最后一页
                CurrentPageIndex = TotalPages - 1;
            }

        }
        #endregion

        #region GoToPage
        // Sets the current page index
        private void GoToPage(int pageIndex)
        {
            // Prepares event data
            PageChangedEventArgs e = new PageChangedEventArgs();
            e.OldPageIndex = CurrentPageIndex;
            e.NewPageIndex = pageIndex;

            // Updates the current index
            CurrentPageIndex = pageIndex;

            // Fires the page changed event
            OnPageIndexChanged(e);

            // Binds new data
            DataBind();
        }
        #endregion

        #region 翻页按钮事件
        // Event handler for the << button
        private void first_Click(object sender, EventArgs e)
        {
            GoToPage(0);
        }

        // PRIVATE prev_Click
        // Event handler for the < button
        private void prev_Click(object sender, EventArgs e)
        {
            GoToPage(CurrentPageIndex - 1);
        }

        // PRIVATE next_Click
        // Event handler for the > button
        private void next_Click(object sender, EventArgs e)
        {
            GoToPage(CurrentPageIndex + 1);
        }

        // PRIVATE last_Click
        // Event handler for the >> button
        private void last_Click(object sender, EventArgs e)
        {
            GoToPage(TotalPages - 1);
        }

        // PRIVATE PageList_Click
        // Event handler for any page selected from the drop-down page list 
        private void PageList_Click(object sender, EventArgs e)
        {
            DropDownList pageList = (DropDownList)sender;
            int pageIndex = Convert.ToInt32(pageList.SelectedItem.Value);
            GoToPage(pageIndex);
        }

        private void PageSizeList_Click(object sender, EventArgs e)
        {
            DropDownList pageSizeList = (DropDownList)sender;
            this.PageSize =  Convert.ToInt32(pageSizeList.SelectedItem.Value);

            OnPageSizeChanged(new PageSizeChangeEventArgs(this.PageSize));

            this.DataBind();
        }
        #endregion

        #region LoopingControls
        private Control returnCtrl;
        /// <summary>
        /// 递归遍历页面控件
        /// </summary>
        /// <param name="oControl"></param>
        private Control LoopingControls(Control oControl)
        {
            foreach (Control frmCtrl in oControl.Controls)
            {
                if (frmCtrl is GridView)
                    if (((GridView)frmCtrl).ID == ControlToPaginate)
                    {
                        returnCtrl = frmCtrl; break;
                    }

                if (frmCtrl is Repeater)
                    if (((Repeater)frmCtrl).ID == ControlToPaginate)
                    {
                        returnCtrl = frmCtrl; break;
                    }

                if (frmCtrl is BaseDataList)
                    if (((BaseDataList)frmCtrl).ID == ControlToPaginate)
                    {
                        returnCtrl = frmCtrl; break;
                    }

                if (frmCtrl.HasControls())
                    LoopingControls(frmCtrl);//递归遍历
            }
            return returnCtrl;
        }
        #endregion

        #endregion
    }//end class WebPager
    #endregion

    public class LabelExender : System.Web.UI.WebControls.Label
    {
        public string Color
        {
            get
            {
                return ColorTranslator.ToHtml(this.BackColor);
            }

            set
            {
                
               this.BackColor = ColorTranslator.FromHtml(value);
            }
        }
    }
}