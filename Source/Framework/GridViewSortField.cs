//Author: ?Luis Ramirez 2008
//Web site: http://www.sqlnetframework.com
//Creation date: May 8, 2008

using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Framework;

namespace Shinetech.Framework
{
    public class GridViewSortField : ColumnSortField
    {
        public GridViewSortField()
        {
        }

        protected override bool IsValidDataBoundControl(Control namingContainer)
        {
            if (namingContainer == null)
                return false;

            return (namingContainer is GridView);
        }

        protected override bool IsSelectedSortingColumn(Control dataBoundControl)
        {
            GridView gridView = dataBoundControl as GridView;

            if (gridView == null)
                throw new HttpException("The GridViewColumnSortField can only be used within a GridView control.");
            
            return (gridView.SortExpression == this.SortExpression);                       
        }

        protected override SortDirection GetColumnSortDirection(Control dataBoundControl)
        {
            GridView gridView = dataBoundControl as GridView;

            if (gridView == null)
                throw new HttpException("The GridViewColumnSortField can only be used within a GridView control.");

            return gridView.SortDirection;
        }

        protected override DataControlField CreateField()
        {
            return new GridViewSortField();
        }

    }
}
