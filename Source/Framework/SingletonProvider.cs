﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shinetech.Framework
{

    public class SingletonProvider<T> where T : new()
    {
        SingletonProvider() { }

        public static T Singleton
        {
            get { return SingletonCreator.instance; }
        }

        class SingletonCreator
        {
            static SingletonCreator() { }

            internal static readonly T instance = new T();
        }
    }


}
