using System;
using System.Collections;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Shinetech.Framework
{
    [XmlRoot("eff",IsNullable = false)]
    public class XmlEFFolder
    {
        [XmlElement(typeof(Head), ElementName = "head", IsNullable = false)]
        public Head head;

        [XmlArray("folders")]
        [XmlArrayItem(typeof(Folder),ElementName="folder")]
        public Folder[] folders = new Folder[1];
    }

    
    public class Head
    {
        public string version = "1.0";
        public DateTime date = DateTime.Now;
        public string username = String.Empty;

        public Head()
        {
            
        }
    }

    
    public class Folder
    {
        public Folder(int id)
        {
            this.id = id;
        }

        public Folder()
        {
            
        }
        public int id;
    }
}
