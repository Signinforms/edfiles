﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace MTOM_Library.MtomWebService {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="MTOMSoap", Namespace="http://www.codeproject.com/soap/MTOMWebServices.asp")]
    public partial class MTOM : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback PingOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetMaxRequestLengthOperationCompleted;
        
        private System.Threading.SendOrPostCallback AppendChunkOperationCompleted;
        
        private System.Threading.SendOrPostCallback DownloadChunkOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetFileSizeOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetFilesListOperationCompleted;
        
        private System.Threading.SendOrPostCallback CheckFileHashOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public MTOM() {
            this.Url = global::MTOM_Library.Properties.Settings.Default.MTOM_Library_MtomWebService_MTOM;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event PingCompletedEventHandler PingCompleted;
        
        /// <remarks/>
        public event GetMaxRequestLengthCompletedEventHandler GetMaxRequestLengthCompleted;
        
        /// <remarks/>
        public event AppendChunkCompletedEventHandler AppendChunkCompleted;
        
        /// <remarks/>
        public event DownloadChunkCompletedEventHandler DownloadChunkCompleted;
        
        /// <remarks/>
        public event GetFileSizeCompletedEventHandler GetFileSizeCompleted;
        
        /// <remarks/>
        public event GetFilesListCompletedEventHandler GetFilesListCompleted;
        
        /// <remarks/>
        public event CheckFileHashCompletedEventHandler CheckFileHashCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.codeproject.com/soap/MTOMWebServices.asp/Ping", RequestNamespace="http://www.codeproject.com/soap/MTOMWebServices.asp", ResponseNamespace="http://www.codeproject.com/soap/MTOMWebServices.asp", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void Ping() {
            this.Invoke("Ping", new object[0]);
        }
        
        /// <remarks/>
        public void PingAsync() {
            this.PingAsync(null);
        }
        
        /// <remarks/>
        public void PingAsync(object userState) {
            if ((this.PingOperationCompleted == null)) {
                this.PingOperationCompleted = new System.Threading.SendOrPostCallback(this.OnPingOperationCompleted);
            }
            this.InvokeAsync("Ping", new object[0], this.PingOperationCompleted, userState);
        }
        
        private void OnPingOperationCompleted(object arg) {
            if ((this.PingCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.PingCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.codeproject.com/soap/MTOMWebServices.asp/GetMaxRequestLength", RequestNamespace="http://www.codeproject.com/soap/MTOMWebServices.asp", ResponseNamespace="http://www.codeproject.com/soap/MTOMWebServices.asp", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public long GetMaxRequestLength() {
            object[] results = this.Invoke("GetMaxRequestLength", new object[0]);
            return ((long)(results[0]));
        }
        
        /// <remarks/>
        public void GetMaxRequestLengthAsync() {
            this.GetMaxRequestLengthAsync(null);
        }
        
        /// <remarks/>
        public void GetMaxRequestLengthAsync(object userState) {
            if ((this.GetMaxRequestLengthOperationCompleted == null)) {
                this.GetMaxRequestLengthOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetMaxRequestLengthOperationCompleted);
            }
            this.InvokeAsync("GetMaxRequestLength", new object[0], this.GetMaxRequestLengthOperationCompleted, userState);
        }
        
        private void OnGetMaxRequestLengthOperationCompleted(object arg) {
            if ((this.GetMaxRequestLengthCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetMaxRequestLengthCompleted(this, new GetMaxRequestLengthCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.codeproject.com/soap/MTOMWebServices.asp/AppendChunk", RequestNamespace="http://www.codeproject.com/soap/MTOMWebServices.asp", ResponseNamespace="http://www.codeproject.com/soap/MTOMWebServices.asp", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public void AppendChunk(string FileName, [System.Xml.Serialization.XmlElementAttribute(DataType="base64Binary")] byte[] buffer, long Offset) {
            this.Invoke("AppendChunk", new object[] {
                        FileName,
                        buffer,
                        Offset});
        }
        
        /// <remarks/>
        public void AppendChunkAsync(string FileName, byte[] buffer, long Offset) {
            this.AppendChunkAsync(FileName, buffer, Offset, null);
        }
        
        /// <remarks/>
        public void AppendChunkAsync(string FileName, byte[] buffer, long Offset, object userState) {
            if ((this.AppendChunkOperationCompleted == null)) {
                this.AppendChunkOperationCompleted = new System.Threading.SendOrPostCallback(this.OnAppendChunkOperationCompleted);
            }
            this.InvokeAsync("AppendChunk", new object[] {
                        FileName,
                        buffer,
                        Offset}, this.AppendChunkOperationCompleted, userState);
        }
        
        private void OnAppendChunkOperationCompleted(object arg) {
            if ((this.AppendChunkCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.AppendChunkCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.codeproject.com/soap/MTOMWebServices.asp/DownloadChunk", RequestNamespace="http://www.codeproject.com/soap/MTOMWebServices.asp", ResponseNamespace="http://www.codeproject.com/soap/MTOMWebServices.asp", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(DataType="base64Binary")]
        public byte[] DownloadChunk(string FileName, long Offset, int BufferSize) {
            object[] results = this.Invoke("DownloadChunk", new object[] {
                        FileName,
                        Offset,
                        BufferSize});
            return ((byte[])(results[0]));
        }
        
        /// <remarks/>
        public void DownloadChunkAsync(string FileName, long Offset, int BufferSize) {
            this.DownloadChunkAsync(FileName, Offset, BufferSize, null);
        }
        
        /// <remarks/>
        public void DownloadChunkAsync(string FileName, long Offset, int BufferSize, object userState) {
            if ((this.DownloadChunkOperationCompleted == null)) {
                this.DownloadChunkOperationCompleted = new System.Threading.SendOrPostCallback(this.OnDownloadChunkOperationCompleted);
            }
            this.InvokeAsync("DownloadChunk", new object[] {
                        FileName,
                        Offset,
                        BufferSize}, this.DownloadChunkOperationCompleted, userState);
        }
        
        private void OnDownloadChunkOperationCompleted(object arg) {
            if ((this.DownloadChunkCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.DownloadChunkCompleted(this, new DownloadChunkCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.codeproject.com/soap/MTOMWebServices.asp/GetFileSize", RequestNamespace="http://www.codeproject.com/soap/MTOMWebServices.asp", ResponseNamespace="http://www.codeproject.com/soap/MTOMWebServices.asp", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public long GetFileSize(string FileName) {
            object[] results = this.Invoke("GetFileSize", new object[] {
                        FileName});
            return ((long)(results[0]));
        }
        
        /// <remarks/>
        public void GetFileSizeAsync(string FileName) {
            this.GetFileSizeAsync(FileName, null);
        }
        
        /// <remarks/>
        public void GetFileSizeAsync(string FileName, object userState) {
            if ((this.GetFileSizeOperationCompleted == null)) {
                this.GetFileSizeOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetFileSizeOperationCompleted);
            }
            this.InvokeAsync("GetFileSize", new object[] {
                        FileName}, this.GetFileSizeOperationCompleted, userState);
        }
        
        private void OnGetFileSizeOperationCompleted(object arg) {
            if ((this.GetFileSizeCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetFileSizeCompleted(this, new GetFileSizeCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.codeproject.com/soap/MTOMWebServices.asp/GetFilesList", RequestNamespace="http://www.codeproject.com/soap/MTOMWebServices.asp", ResponseNamespace="http://www.codeproject.com/soap/MTOMWebServices.asp", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string[] GetFilesList() {
            object[] results = this.Invoke("GetFilesList", new object[0]);
            return ((string[])(results[0]));
        }
        
        /// <remarks/>
        public void GetFilesListAsync() {
            this.GetFilesListAsync(null);
        }
        
        /// <remarks/>
        public void GetFilesListAsync(object userState) {
            if ((this.GetFilesListOperationCompleted == null)) {
                this.GetFilesListOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetFilesListOperationCompleted);
            }
            this.InvokeAsync("GetFilesList", new object[0], this.GetFilesListOperationCompleted, userState);
        }
        
        private void OnGetFilesListOperationCompleted(object arg) {
            if ((this.GetFilesListCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetFilesListCompleted(this, new GetFilesListCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://www.codeproject.com/soap/MTOMWebServices.asp/CheckFileHash", RequestNamespace="http://www.codeproject.com/soap/MTOMWebServices.asp", ResponseNamespace="http://www.codeproject.com/soap/MTOMWebServices.asp", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string CheckFileHash(string FileName) {
            object[] results = this.Invoke("CheckFileHash", new object[] {
                        FileName});
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void CheckFileHashAsync(string FileName) {
            this.CheckFileHashAsync(FileName, null);
        }
        
        /// <remarks/>
        public void CheckFileHashAsync(string FileName, object userState) {
            if ((this.CheckFileHashOperationCompleted == null)) {
                this.CheckFileHashOperationCompleted = new System.Threading.SendOrPostCallback(this.OnCheckFileHashOperationCompleted);
            }
            this.InvokeAsync("CheckFileHash", new object[] {
                        FileName}, this.CheckFileHashOperationCompleted, userState);
        }
        
        private void OnCheckFileHashOperationCompleted(object arg) {
            if ((this.CheckFileHashCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.CheckFileHashCompleted(this, new CheckFileHashCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    public delegate void PingCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    public delegate void GetMaxRequestLengthCompletedEventHandler(object sender, GetMaxRequestLengthCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetMaxRequestLengthCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetMaxRequestLengthCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public long Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((long)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    public delegate void AppendChunkCompletedEventHandler(object sender, System.ComponentModel.AsyncCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    public delegate void DownloadChunkCompletedEventHandler(object sender, DownloadChunkCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class DownloadChunkCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal DownloadChunkCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public byte[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((byte[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    public delegate void GetFileSizeCompletedEventHandler(object sender, GetFileSizeCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetFileSizeCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetFileSizeCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public long Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((long)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    public delegate void GetFilesListCompletedEventHandler(object sender, GetFilesListCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetFilesListCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetFilesListCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    public delegate void CheckFileHashCompletedEventHandler(object sender, CheckFileHashCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.6.81.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class CheckFileHashCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal CheckFileHashCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591