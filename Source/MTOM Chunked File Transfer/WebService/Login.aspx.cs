using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Log_in : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		if(Request["Username"] != null)
		{
			// web service is requesting authentication via a HTTP request
			if(FormsAuthentication.Authenticate(Request["Username"], Request["Password"]))
				FormsAuthentication.SetAuthCookie(Request["Username"], false);
		}
	}
}