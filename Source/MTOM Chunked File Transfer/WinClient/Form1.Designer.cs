namespace UploadWinClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatusBarText = new System.Windows.Forms.ToolStripStatusLabel();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.dudChunkSize = new System.Windows.Forms.NumericUpDown();
            this.lblKb = new System.Windows.Forms.Label();
            this.chkHash = new System.Windows.Forms.CheckBox();
            this.chkAutoChunksize = new System.Windows.Forms.CheckBox();
            this.btnRefreshFiles = new System.Windows.Forms.Button();
            this.workerGetFileList = new System.ComponentModel.BackgroundWorker();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualResumeUploadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualResumeDownloadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.computeMD5HashToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dudMaxThreads = new System.Windows.Forms.NumericUpDown();
            this.lblChunkSize = new System.Windows.Forms.Label();
            this.lblFileSizeUpload = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblFileSizeDownload = new System.Windows.Forms.Label();
            this.btnBrowseSaveFolder = new System.Windows.Forms.Button();
            this.txtSaveFolder = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lstDownloadFiles = new System.Windows.Forms.ListView();
            this.btnDownload = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnTestAuth = new System.Windows.Forms.Button();
            this.chkLoginRequired = new System.Windows.Forms.CheckBox();
            this.lblPassword = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.workerFileHash = new System.ComponentModel.BackgroundWorker();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.lblErrors = new System.Windows.Forms.Label();
            this.taskPanel1 = new UploadWinClient.TaskPanel();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dudChunkSize)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dudMaxThreads)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatusBarText});
            this.statusStrip1.Location = new System.Drawing.Point(0, 563);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(912, 22);
            this.statusStrip1.TabIndex = 0;
            // 
            // lblStatusBarText
            // 
            this.lblStatusBarText.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.lblStatusBarText.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblStatusBarText.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.lblStatusBarText.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.lblStatusBarText.Name = "lblStatusBarText";
            this.lblStatusBarText.Size = new System.Drawing.Size(35, 17);
            this.lblStatusBarText.Text = "Ready";
            this.lblStatusBarText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Multiselect = true;
            // 
            // dudChunkSize
            // 
            this.dudChunkSize.Enabled = false;
            this.dudChunkSize.Location = new System.Drawing.Point(85, 44);
            this.dudChunkSize.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.dudChunkSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.dudChunkSize.Name = "dudChunkSize";
            this.dudChunkSize.Size = new System.Drawing.Size(71, 21);
            this.dudChunkSize.TabIndex = 4;
            this.dudChunkSize.Value = new decimal(new int[] {
            64,
            0,
            0,
            0});
            // 
            // lblKb
            // 
            this.lblKb.AutoSize = true;
            this.lblKb.Location = new System.Drawing.Point(156, 46);
            this.lblKb.Name = "lblKb";
            this.lblKb.Size = new System.Drawing.Size(17, 12);
            this.lblKb.TabIndex = 6;
            this.lblKb.Text = "Kb";
            // 
            // chkHash
            // 
            this.chkHash.AutoSize = true;
            this.chkHash.Checked = true;
            this.chkHash.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkHash.Location = new System.Drawing.Point(16, 23);
            this.chkHash.Name = "chkHash";
            this.chkHash.Size = new System.Drawing.Size(216, 16);
            this.chkHash.TabIndex = 9;
            this.chkHash.Text = "Verify file upload with MD5 Hash";
            this.chkHash.UseVisualStyleBackColor = true;
            // 
            // chkAutoChunksize
            // 
            this.chkAutoChunksize.AutoSize = true;
            this.chkAutoChunksize.Checked = true;
            this.chkAutoChunksize.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAutoChunksize.Location = new System.Drawing.Point(181, 46);
            this.chkAutoChunksize.Name = "chkAutoChunksize";
            this.chkAutoChunksize.Size = new System.Drawing.Size(72, 16);
            this.chkAutoChunksize.TabIndex = 13;
            this.chkAutoChunksize.Text = "Auto set";
            this.toolTip1.SetToolTip(this.chkAutoChunksize, "Adjust the size of each chunk automatically based on the network performance.  ");
            this.chkAutoChunksize.UseVisualStyleBackColor = true;
            this.chkAutoChunksize.CheckedChanged += new System.EventHandler(this.chkUploadAutoChunksize_CheckedChanged);
            // 
            // btnRefreshFiles
            // 
            this.btnRefreshFiles.Location = new System.Drawing.Point(230, 42);
            this.btnRefreshFiles.Name = "btnRefreshFiles";
            this.btnRefreshFiles.Size = new System.Drawing.Size(95, 21);
            this.btnRefreshFiles.TabIndex = 5;
            this.btnRefreshFiles.Text = "List Files";
            this.btnRefreshFiles.UseVisualStyleBackColor = true;
            this.btnRefreshFiles.Click += new System.EventHandler(this.btnRefreshFiles_Click);
            // 
            // workerGetFileList
            // 
            this.workerGetFileList.WorkerSupportsCancellation = true;
            this.workerGetFileList.DoWork += new System.ComponentModel.DoWorkEventHandler(this.workerGetFileList_DoWork);
            this.workerGetFileList.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.workerGetFileList_RunWorkerCompleted);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(912, 24);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manualResumeUploadToolStripMenuItem,
            this.manualResumeDownloadToolStripMenuItem,
            this.toolStripSeparator1,
            this.computeMD5HashToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // manualResumeUploadToolStripMenuItem
            // 
            this.manualResumeUploadToolStripMenuItem.Name = "manualResumeUploadToolStripMenuItem";
            this.manualResumeUploadToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.manualResumeUploadToolStripMenuItem.Text = "Manual Resume &Upload";
            this.manualResumeUploadToolStripMenuItem.ToolTipText = "Locate a partially completed upload file and resume from the last location";
            this.manualResumeUploadToolStripMenuItem.Click += new System.EventHandler(this.manualResumeUploadToolStripMenuItem_Click);
            // 
            // manualResumeDownloadToolStripMenuItem
            // 
            this.manualResumeDownloadToolStripMenuItem.Name = "manualResumeDownloadToolStripMenuItem";
            this.manualResumeDownloadToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.manualResumeDownloadToolStripMenuItem.Text = "Manual Resume &Download";
            this.manualResumeDownloadToolStripMenuItem.ToolTipText = "Locate a partially completed download file and resume from the last location";
            this.manualResumeDownloadToolStripMenuItem.Click += new System.EventHandler(this.manualResumeDownloadToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(199, 6);
            // 
            // computeMD5HashToolStripMenuItem
            // 
            this.computeMD5HashToolStripMenuItem.Name = "computeMD5HashToolStripMenuItem";
            this.computeMD5HashToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.computeMD5HashToolStripMenuItem.Text = "Compute MD5 &Hash";
            this.computeMD5HashToolStripMenuItem.Click += new System.EventHandler(this.computeMD5HashToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(199, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dudMaxThreads);
            this.groupBox1.Controls.Add(this.lblChunkSize);
            this.groupBox1.Controls.Add(this.chkAutoChunksize);
            this.groupBox1.Controls.Add(this.lblKb);
            this.groupBox1.Controls.Add(this.dudChunkSize);
            this.groupBox1.Controls.Add(this.chkHash);
            this.groupBox1.Location = new System.Drawing.Point(7, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(255, 87);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Transfer Options";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 12);
            this.label3.TabIndex = 15;
            this.label3.Text = "Max Threads";
            // 
            // dudMaxThreads
            // 
            this.dudMaxThreads.Location = new System.Drawing.Point(85, 63);
            this.dudMaxThreads.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.dudMaxThreads.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.dudMaxThreads.Name = "dudMaxThreads";
            this.dudMaxThreads.Size = new System.Drawing.Size(71, 21);
            this.dudMaxThreads.TabIndex = 14;
            this.dudMaxThreads.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.dudMaxThreads.ValueChanged += new System.EventHandler(this.dudMaxThreads_ValueChanged);
            // 
            // lblChunkSize
            // 
            this.lblChunkSize.AutoSize = true;
            this.lblChunkSize.Location = new System.Drawing.Point(13, 46);
            this.lblChunkSize.Name = "lblChunkSize";
            this.lblChunkSize.Size = new System.Drawing.Size(65, 12);
            this.lblChunkSize.TabIndex = 6;
            this.lblChunkSize.Text = "Chunk Size";
            // 
            // lblFileSizeUpload
            // 
            this.lblFileSizeUpload.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFileSizeUpload.Location = new System.Drawing.Point(257, 22);
            this.lblFileSizeUpload.Name = "lblFileSizeUpload";
            this.lblFileSizeUpload.Size = new System.Drawing.Size(630, 17);
            this.lblFileSizeUpload.TabIndex = 8;
            this.lblFileSizeUpload.Text = "...";
            this.lblFileSizeUpload.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(16, 18);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(216, 21);
            this.btnBrowse.TabIndex = 12;
            this.btnBrowse.Text = "Select file(s) to upload";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnBrowse);
            this.groupBox2.Controls.Add(this.lblFileSizeUpload);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.groupBox2.Location = new System.Drawing.Point(7, 159);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(893, 52);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Upload";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox3.Controls.Add(this.lblFileSizeDownload);
            this.groupBox3.Controls.Add(this.btnBrowseSaveFolder);
            this.groupBox3.Controls.Add(this.txtSaveFolder);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.lstDownloadFiles);
            this.groupBox3.Controls.Add(this.btnDownload);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.btnRefreshFiles);
            this.groupBox3.Location = new System.Drawing.Point(7, 216);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(331, 346);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Download";
            // 
            // lblFileSizeDownload
            // 
            this.lblFileSizeDownload.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFileSizeDownload.Location = new System.Drawing.Point(16, 320);
            this.lblFileSizeDownload.Name = "lblFileSizeDownload";
            this.lblFileSizeDownload.Size = new System.Drawing.Size(309, 17);
            this.lblFileSizeDownload.TabIndex = 20;
            this.lblFileSizeDownload.Text = "...";
            this.lblFileSizeDownload.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnBrowseSaveFolder
            // 
            this.btnBrowseSaveFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBrowseSaveFolder.Location = new System.Drawing.Point(260, 20);
            this.btnBrowseSaveFolder.Name = "btnBrowseSaveFolder";
            this.btnBrowseSaveFolder.Size = new System.Drawing.Size(65, 21);
            this.btnBrowseSaveFolder.TabIndex = 18;
            this.btnBrowseSaveFolder.Text = "Browse";
            this.btnBrowseSaveFolder.UseVisualStyleBackColor = true;
            this.btnBrowseSaveFolder.Click += new System.EventHandler(this.btnBrowseSaveFolder_Click);
            // 
            // txtSaveFolder
            // 
            this.txtSaveFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSaveFolder.Location = new System.Drawing.Point(85, 20);
            this.txtSaveFolder.Name = "txtSaveFolder";
            this.txtSaveFolder.Size = new System.Drawing.Size(160, 21);
            this.txtSaveFolder.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 16;
            this.label2.Text = "Save folder:";
            // 
            // lstDownloadFiles
            // 
            this.lstDownloadFiles.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstDownloadFiles.Location = new System.Drawing.Point(16, 66);
            this.lstDownloadFiles.Name = "lstDownloadFiles";
            this.lstDownloadFiles.Size = new System.Drawing.Size(309, 252);
            this.lstDownloadFiles.TabIndex = 15;
            this.lstDownloadFiles.UseCompatibleStateImageBehavior = false;
            this.lstDownloadFiles.View = System.Windows.Forms.View.List;
            this.lstDownloadFiles.SelectedIndexChanged += new System.EventHandler(this.lstDownloadFiles_SelectedIndexChanged);
            // 
            // btnDownload
            // 
            this.btnDownload.Enabled = false;
            this.btnDownload.Location = new System.Drawing.Point(119, 42);
            this.btnDownload.Name = "btnDownload";
            this.btnDownload.Size = new System.Drawing.Size(103, 21);
            this.btnDownload.TabIndex = 14;
            this.btnDownload.Text = "Start Download";
            this.btnDownload.UseVisualStyleBackColor = true;
            this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 12);
            this.label1.TabIndex = 13;
            this.label1.Text = "File(s) to download:";
            // 
            // txtPassword
            // 
            this.txtPassword.Enabled = false;
            this.txtPassword.Location = new System.Drawing.Point(72, 58);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(122, 21);
            this.txtPassword.TabIndex = 21;
            this.toolTip1.SetToolTip(this.txtPassword, "If the web service requires authentication, provide username and password here");
            // 
            // txtUsername
            // 
            this.txtUsername.Enabled = false;
            this.txtUsername.Location = new System.Drawing.Point(72, 40);
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(122, 21);
            this.txtUsername.TabIndex = 19;
            this.toolTip1.SetToolTip(this.txtUsername, "If the web service requires authentication, provide username and password here");
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnTestAuth);
            this.groupBox5.Controls.Add(this.chkLoginRequired);
            this.groupBox5.Controls.Add(this.lblPassword);
            this.groupBox5.Controls.Add(this.txtPassword);
            this.groupBox5.Controls.Add(this.lblUsername);
            this.groupBox5.Controls.Add(this.txtUsername);
            this.groupBox5.Location = new System.Drawing.Point(279, 25);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(228, 87);
            this.groupBox5.TabIndex = 17;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Forms Authentication";
            // 
            // btnTestAuth
            // 
            this.btnTestAuth.Location = new System.Drawing.Point(129, 18);
            this.btnTestAuth.Name = "btnTestAuth";
            this.btnTestAuth.Size = new System.Drawing.Size(65, 21);
            this.btnTestAuth.TabIndex = 13;
            this.btnTestAuth.Text = "Login";
            this.btnTestAuth.UseVisualStyleBackColor = true;
            this.btnTestAuth.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // chkLoginRequired
            // 
            this.chkLoginRequired.AutoSize = true;
            this.chkLoginRequired.Location = new System.Drawing.Point(9, 23);
            this.chkLoginRequired.Name = "chkLoginRequired";
            this.chkLoginRequired.Size = new System.Drawing.Size(114, 16);
            this.chkLoginRequired.TabIndex = 23;
            this.chkLoginRequired.Text = "Login Required?";
            this.chkLoginRequired.UseVisualStyleBackColor = true;
            this.chkLoginRequired.CheckedChanged += new System.EventHandler(this.chkLoginRequired_CheckedChanged);
            // 
            // lblPassword
            // 
            this.lblPassword.AutoSize = true;
            this.lblPassword.Enabled = false;
            this.lblPassword.Location = new System.Drawing.Point(6, 62);
            this.lblPassword.Name = "lblPassword";
            this.lblPassword.Size = new System.Drawing.Size(53, 12);
            this.lblPassword.TabIndex = 22;
            this.lblPassword.Text = "Password";
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Enabled = false;
            this.lblUsername.Location = new System.Drawing.Point(6, 43);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(53, 12);
            this.lblUsername.TabIndex = 20;
            this.lblUsername.Text = "Username";
            // 
            // workerFileHash
            // 
            this.workerFileHash.DoWork += new System.ComponentModel.DoWorkEventHandler(this.workerFileHash_DoWork);
            this.workerFileHash.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.workerFileHash_RunWorkerCompleted);
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.Description = "Select Save Folder";
            // 
            // lblErrors
            // 
            this.lblErrors.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblErrors.AutoEllipsis = true;
            this.lblErrors.ForeColor = System.Drawing.Color.Red;
            this.lblErrors.Location = new System.Drawing.Point(4, 114);
            this.lblErrors.Name = "lblErrors";
            this.lblErrors.Size = new System.Drawing.Size(896, 42);
            this.lblErrors.TabIndex = 18;
            // 
            // taskPanel1
            // 
            this.taskPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.taskPanel1.AutoScroll = true;
            this.taskPanel1.Location = new System.Drawing.Point(344, 219);
            this.taskPanel1.Name = "taskPanel1";
            this.taskPanel1.Size = new System.Drawing.Size(556, 343);
            this.taskPanel1.TabIndex = 19;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(912, 585);
            this.Controls.Add(this.taskPanel1);
            this.Controls.Add(this.lblErrors);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(600, 464);
            this.Name = "Form1";
            this.Text = "MTOM Chunked File Transfer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dudChunkSize)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dudMaxThreads)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.ToolStripStatusLabel lblStatusBarText;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.NumericUpDown dudChunkSize;
		private System.Windows.Forms.Label lblKb;
		private System.Windows.Forms.CheckBox chkHash;
        private System.ComponentModel.BackgroundWorker workerGetFileList;
		private System.Windows.Forms.Button btnRefreshFiles;
		private System.Windows.Forms.CheckBox chkAutoChunksize;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem manualResumeUploadToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label lblFileSizeUpload;
		private System.Windows.Forms.Button btnBrowse;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label lblChunkSize;
		private System.Windows.Forms.Button btnDownload;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ListView lstDownloadFiles;
		private System.Windows.Forms.ToolTip toolTip1;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.CheckBox chkLoginRequired;
		private System.Windows.Forms.Label lblPassword;
		private System.Windows.Forms.TextBox txtPassword;
		private System.Windows.Forms.Label lblUsername;
		private System.Windows.Forms.TextBox txtUsername;
		private System.Windows.Forms.Button btnTestAuth;
		private System.Windows.Forms.ToolStripMenuItem computeMD5HashToolStripMenuItem;
		private System.ComponentModel.BackgroundWorker workerFileHash;
		private System.Windows.Forms.ToolStripMenuItem manualResumeDownloadToolStripMenuItem;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
		private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
		private System.Windows.Forms.Button btnBrowseSaveFolder;
		private System.Windows.Forms.TextBox txtSaveFolder;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
		private System.Windows.Forms.Label lblErrors;
		private TaskPanel taskPanel1;
		private System.Windows.Forms.Label lblFileSizeDownload;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown dudMaxThreads;
    }
}

