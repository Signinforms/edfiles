using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MTOM_Library;
using System.ComponentModel;
using System.IO;
using System.Threading;

namespace UploadWinClient
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());			
        }
    }

	/// Console App Implementation...
	///	put the following in main: new Module1().Start();
	/// 
	/*class Module1
	{

		public void Start()
		{
			string[] arrfi = { "C:\\123.exe"};
			FileTransferUpload ftu = new FileTransferUpload();
			foreach(string fi in arrfi)
			{				
				ftu.LocalFilePath = fi;
				ftu.RunWorkerCompleted += new RunWorkerCompletedEventHandler(ftu_RunWorkerCompleted);
				ftu.ProgressChanged += new ProgressChangedEventHandler(ftu_ProgressChanged);
				ftu.RunWorkerSync(new DoWorkEventArgs(0));
				Thread.Sleep(0);	// allow pending events to execute
			}
			Console.WriteLine("Finished");
		}

		void ftu_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			Console.WriteLine("ftu1_RunWorkerCompleted");
		}

		void ftu_ProgressChanged(object sender, ProgressChangedEventArgs e)
		{
			Console.WriteLine(e.ProgressPercentage.ToString() + "%");
		}
	}*/
}