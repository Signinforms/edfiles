﻿using MoveEdFiles.ServiceReference1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace MoveEdFiles
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();
            if (string.IsNullOrEmpty(txtUsername.Text))
            {
                MessageBox.Show("Please enter valid username");
            }
            else
            {
                lblWait.Visible = true;
                string fileName = obj.CopyEdFilesByUsername(txtUsername.Text);
                lblWait.Visible = false;
                if (fileName.Contains(".zip"))
                {
                    lblOutput.Visible = true;
                    textOutput.Visible = true;
                    textOutput.Text = fileName;
                    MessageBox.Show("You can download the zip from this URL : " + fileName);
                }
                else
                {
                    MessageBox.Show(fileName);
                }
            }
        }

        private void txtUsername_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
