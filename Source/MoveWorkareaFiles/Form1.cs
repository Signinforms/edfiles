﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using net.openstack.Core.Domain;
using net.openstack.Providers.Rackspace;
using System.Configuration;
using System.IO;
using NLog;
using System.Web;

namespace MoveWorkareaFiles
{
    public partial class Form1 : Form
    {

        #region Properties
        public static CloudIdentity user = new CloudIdentity
        {
            Username = ConfigurationSettings.AppSettings["RackSpaceUserName"],
            APIKey = ConfigurationSettings.AppSettings["RackSpaceAPIKey"]
        };
        public static CloudFilesProvider cloudfilesProvider = new CloudFilesProvider(user);
        public static string SourceContainer = ConfigurationSettings.AppSettings["RackSpaceSource"];
        public static string DestContainer = ConfigurationSettings.AppSettings["RackSpaceDestination"];
        public static string StrExpression = ConfigurationSettings.AppSettings["RackSpaceExpression"];
        public static string StrServerPath = ConfigurationSettings.AppSettings["ServerPath"];
        NLogLogger _logger = new NLogLogger();
        #endregion

        public Form1()
        {
            //_logger.Debug(DateTime.Now + " -----------Started----------- ");
            InitializeComponent();
            //_logger.Debug(DateTime.Now + " -----------Ended----------- ");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _logger.Debug(DateTime.Now + " Clicked on move");
            label1.Text = "Downloading.......";
            DownloadAndMove(SourceContainer, StrExpression);
            label1.Text = "Completed.";
        }

        public void DownloadAndMove(string containerName, string matchExp)
        {
            //RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            IEnumerable<ContainerObject> containerObjectList;
            TreeView treeView = new TreeView();
            //matchExp = matchExp.Replace("WorkArea", StrExpression);
            containerObjectList = cloudfilesProvider.ListObjects(containerName, null, StrExpression);
            string mainFolderName = string.Empty;
            if (containerObjectList.ToArray<ContainerObject>().Length > 0)
                mainFolderName = containerObjectList.ToArray<ContainerObject>()[0].Name.Split('/')[0];

            int index = 1;
            foreach (ContainerObject containerObj in containerObjectList)
            {
                if ((string.IsNullOrEmpty(matchExp) || (!string.IsNullOrEmpty(matchExp) && containerObj.Name.IndexOf(matchExp) >= 0)) && containerObj.ContentType != "application/directory")
                {
                    string newObjectPath = containerObj.Name;
                    string[] objectNames = containerObj.Name.Split('/');
                    if (!string.Equals(mainFolderName, objectNames[0]))
                    {
                        objectNames[0] = objectNames[0].Replace("workArea", "WorkArea");
                        mainFolderName = objectNames[0];
                    }
                    try
                    {
                        MemoryStream memStrmToSave = new MemoryStream();
                        cloudfilesProvider.GetObject(SourceContainer, containerObj.Name, memStrmToSave);
                        if (memStrmToSave.Length > 0)
                        {
                            FileStream file = null;
                            string path = StrServerPath + Path.DirectorySeparatorChar + SourceContainer;
                            string filePath = path + Path.DirectorySeparatorChar + containerObj.Name.Replace('/', '\\');

                            if (!Directory.Exists(filePath.Substring(0, filePath.LastIndexOf('\\') + 1)))
                                Directory.CreateDirectory(filePath.Substring(0, filePath.LastIndexOf('\\') + 1));

                            file = new FileStream(filePath, FileMode.Create, FileAccess.Write);
                            memStrmToSave.WriteTo(file);
                            file.Close();
                            file.Dispose();
                            memStrmToSave.Close();
                        }
                        else
                        {
                            _logger.Error("Can not find file in " + SourceContainer + " where filename = " + containerObj.Name);
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Error("Error occured while downloading the object " + containerObj.Name, ex.Message, ex);
                    }
                    //MemoryStream memStrmToCopy = new MemoryStream();
                    //try
                    //{
                    //    cloudfilesProvider.GetObject(DestContainer, containerObj.Name, memStrmToCopy);
                    //}
                    //catch 
                    //{
                    //    _logger.Info(containerObj.Name + " file not found in " + DestContainer);
                    //}
                    //if (memStrmToCopy.Length > 0)
                    //{
                    //    string folderPath = string.Empty;
                    //    int length = (containerObj.Name.LastIndexOf('/') - (StrExpression.Length + 1));
                    //    if (length > 0)
                    //        folderPath = containerObj.Name.Substring(StrExpression.Length + 1, length);

                    //    string fileName = treeView.GetNewFileName(containerObj.Name.Substring(containerObj.Name.LastIndexOf('/') + 1).ToLower(), folderPath);
                    //    fileName = fileName.Substring(fileName.LastIndexOf('\\') + 1);
                    //    newObjectPath = treeView.GeneratePath(fileName, folderPath);
                    //}
                    //try
                    //{
                    //    cloudfilesProvider.CopyObject(SourceContainer, containerObj.Name, DestContainer, newObjectPath);
                    //}
                    //catch (Exception ex)
                    //{
                    //    _logger.Error("Error occured while copying the object " + containerObj.Name, ex.Message, ex);
                    //}
                }
                _logger.Info(index + " : Filename : " + containerObj.Name);
                index++;
            }
        }
    }
}
