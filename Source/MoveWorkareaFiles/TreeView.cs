﻿using net.openstack.Core.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.IO;
using System.Configuration;
using net.openstack.Core.Domain;
using net.openstack.Providers.Rackspace;

#region TreeView
/// <summary>
/// Summary description for TreeView
/// </summary>
public class TreeView
{

    #region Properties
    public static CloudIdentity user = new CloudIdentity
    {
        Username = ConfigurationSettings.AppSettings["RackSpaceUserName"],
        APIKey = ConfigurationSettings.AppSettings["RackSpaceAPIKey"]
    };
    public static CloudFilesProvider cloudfilesProvider = new CloudFilesProvider(user);
    public static string SourceContainer = ConfigurationSettings.AppSettings["RackSpaceSource"];
    public static string DestContainer = ConfigurationSettings.AppSettings["RackSpaceDestination"];
    public static string StrExpression = ConfigurationSettings.AppSettings["RackSpaceExpression"];
    NLogLogger _logger = new NLogLogger();
    #endregion

    public TreeView()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetTable()
    {
        DataTable table = new DataTable("RackSpaceObject");
        table.Columns.Add("ObjectID", typeof(int));
        table.Columns.Add("ObjectName", typeof(string));
        table.Columns.Add("Level", typeof(int));
        table.Columns.Add("ParentID", typeof(int));
        table.Columns.Add("Path", typeof(string));
        table.Columns.Add("Size", typeof(decimal));
        table.Columns.Add("ModifiedDate", typeof(string));

        return table;
    }

    /// <summary>
    /// Returns list of object from specified container
    /// </summary>
    /// <param name="containerName">Name of container to list objects</param>
    /// <param name="markerStart">Name of folder from which objects to be retrived</param>
    /// <returns>Returns list of object from specified container</returns>
    public IEnumerable<ContainerObject> GetObjectList(string containerName, string markerStart = null)
    {
        return cloudfilesProvider.ListObjects(containerName, null, "workArea");
    }

    /// <summary>
    /// Generates new file name if file already exists
    /// </summary>
    /// <param name="destinationFile">Name of file to be renamed</param>
    /// <param name="folder">Folder where file resides</param>
    /// <returns>New file name</returns>
    public string GetNewFileName(string destinationFile, string folderID = null, string uid = null)
    {
        try
        {
            if (string.IsNullOrEmpty(uid))
                uid = DestContainer;
            string fileName = Path.GetFileNameWithoutExtension(destinationFile);
            string fileExtension = Path.GetExtension(destinationFile);
            int i = 1;
            IEnumerable<ContainerObject> objectList = GetObjectList(uid);

            ContainerObject[] containerObject = objectList.ToArray<ContainerObject>();
            for (int index = 0; index < containerObject.Length; index++)
            {
                string renameFileName = fileName + "_v" + i + fileExtension;
                string fileNameToCompare = string.Empty;
                if (!string.IsNullOrEmpty(folderID))
                {
                    if (containerObject[index].Name.Contains(folderID) && string.Equals(renameFileName, containerObject[index].Name.Substring(containerObject[index].Name.LastIndexOf('/') + 1)))
                    {
                        i++;
                        index = -1;
                        continue;
                    }
                }
                else
                {
                    string comparePath = GeneratePath(renameFileName.ToLower(), folderID);
                    if (string.Equals(comparePath, containerObject[index].Name))
                    {
                        i++;
                        index = -1;
                        continue;
                    }
                }
                destinationFile = renameFileName;
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
        return destinationFile;
    }

    /// <summary>
    /// Generates path according to folder type for uploading object
    /// </summary>
    /// <param name="fileName">Name of file to be uploaded</param>
    /// <param name="folder">Name of folder into which file is to be uploaded</param>
    /// <param name="folderID">ID of share folder</param>
    /// <returns>Path to which file is to be uploaded</returns>
    public string GeneratePath(string fileName, string folderPath)
    {
        string filePath = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(folderPath))
            {
                filePath = "workArea" + "/" + folderPath + "/" + fileName;
            }
            else
            {
                filePath = "workArea" + "/" + fileName; ;
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
        return filePath;
    }

    
}
#endregion


#region RackSpaceObject
public class RackSpaceObject
{
    public int ObjectID { get; set; }
    public string ObjectName { get; set; }
    public int Level { get; set; }
    public int ParentID { get; set; }
    public string Path { get; set; }
    public decimal Size { get; set; }
    public string ModifiedDate { get; set; }
}
#endregion