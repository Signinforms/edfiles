﻿//using System.Windows.Forms;
//using wpfControl = System.Windows.Controls;
using System.Windows.Controls;


namespace MultiUpload.Interfaces
{
    /// <summary>
    /// Interface IInitializable
    /// </summary>
    internal interface IInitializable
    {

        /// <summary>
        /// Gets or sets the ancestor control.
        /// </summary>
        /// <value>The ancestor control.</value>
        Control AncestorControl
        {
            get;
            set;
        }

        /// <summary>
        /// Initializes the specified initialization data.
        /// </summary>
        /// <param name="initializationData">The initialization data.</param>
        void Initialize(object initializationData);
    }
}
