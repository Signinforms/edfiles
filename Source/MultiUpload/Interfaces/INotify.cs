﻿using System;
//using System.Windows.Forms;

namespace MultiUpload.Interfaces
{
    //public delegate void NotifyEventHandler(Control sender, Type targetControl, object initializationData);
    /// <summary>
    /// Delegate NotifyEventHandler
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="targetControl">The target control.</param>
    /// <param name="initializationData">The initialization data.</param>
    public delegate void NotifyEventHandler(object sender, Type targetControl, object initializationData);

    /// <summary>
    /// Interface INotify
    /// </summary>
    internal interface INotify
    {
        /// <summary>
        /// Occurs when [notify].
        /// </summary>
        event NotifyEventHandler Notify;
    }
}
