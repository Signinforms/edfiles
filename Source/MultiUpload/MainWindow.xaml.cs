﻿using MultiUpload.Interfaces;
using MultiUpload.ServiceReference1;
using MultiUpload.UserControls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MultiUpload
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Shows the control.
        /// </summary>
        /// <param name="controlToShow">The control to show.</param>
        /// <param name="needsScrolling">if set to <c>true</c> [needs scrolling].</param>
        private void ShowControl(Control controlToShow, bool needsScrolling)
        {
            if (SystemParameters.PrimaryScreenHeight < 870 || SystemParameters.PrimaryScreenWidth < 1042)
            {
                needsScrolling = true;
            }
            else
            {
                //this.MinHeight = 870;
                //this.MinWidth = 1042;
            }

            // needsScrolling = false;
            //if (controlToShow.ToString().ToLower().Contains("scaneffwindow"))
            //{
            //    dockContainerMain.Children.Clear();
            //    dockContainerMain.Children.Add(controlToShow);
            //}
            //else 
            if (controlToShow.ToString().ToLower().Contains("archiveupload"))
            {
                dockContainerMain.Children.Clear();
                dockContainerMain.Children.Add(controlToShow);
            }
            else if (controlToShow.ToString().ToLower().Contains("batchupload"))
            {
                dockContainerMain.Children.Clear();
                dockContainerMain.Children.Add(controlToShow);
            }
            else if (controlToShow.ToString().ToLower().Contains("renameandmovetoarchive"))
            {
                dockContainerMain.Children.Clear();
                dockContainerMain.Children.Add(controlToShow);
            }
            else if (controlToShow.ToString().ToLower().Contains("elasticupload"))
            {
                dockContainerMain.Children.Clear();
                dockContainerMain.Children.Add(controlToShow);
            }
            this.RaiseEvent(new RoutedEventArgs(System.Windows.FrameworkElement.SizeChangedEvent));
        }

        /// <summary>
        /// Views the control.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="ancestor">The ancestor.</param>
        public void ViewControl(Control control, object ancestor)
        {
            try
            {
                LoadControl(control, true, null, ancestor, false);
            }
            catch (Exception exception)
            {

            }
        }

        /// <summary>
        /// Loads the control.
        /// </summary>
        /// <param name="controlToLoad">The control to load.</param>
        /// <param name="isInvokedFromMenu">if set to <c>true</c> [is invoked from menu].</param>
        /// <param name="initializationData">The initialization data.</param>
        /// <param name="ancestorControl">The ancestor control.</param>
        /// <param name="needsScrolling">if set to <c>true</c> [needs scrolling].</param>
        private void LoadControl(Control controlToLoad, bool isInvokedFromMenu, object initializationData, object ancestorControl, bool needsScrolling)
        {
            if (controlToLoad is INotify)
            {
                ((INotify)controlToLoad).Notify -= new NotifyEventHandler(OnNotified);
                ((INotify)controlToLoad).Notify += new NotifyEventHandler(OnNotified);
            }

            if (controlToLoad is IInitializable)
            {
                IInitializable initializableControl = (IInitializable)controlToLoad;
                if (ancestorControl != null)
                    initializableControl.AncestorControl = (Control)ancestorControl;
                if (initializationData != null)
                    initializableControl.Initialize(initializationData);
            }
            //Todo: consider change later on
            if (!isInvokedFromMenu)
            {
                if (controlToLoad is IRefreshable)
                    ((IRefreshable)controlToLoad).Refresh();
            }
            ShowControl(controlToLoad, needsScrolling);
        }

        /// <summary>
        /// Called when [notified].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="targetControl">The target control.</param>
        /// <param name="initializationData">The initialization data.</param>
        private void OnNotified(object sender, Type targetControl, object initializationData)
        {
            try
            {
                Control controlToLoad = null;
                if (targetControl != null)
                {
                    controlToLoad = (Control)Activator.CreateInstance(targetControl);
                }
                else if (sender is IInitializable)
                {
                    controlToLoad = ((IInitializable)sender).AncestorControl; //ParentControl;
                    if (controlToLoad is IInitializable)
                    {
                        initializationData = null;
                        sender = null;
                    }
                }
                LoadControl(controlToLoad, false, initializationData, sender, false);
            }
            catch (Exception exception)
            {

            }
        }

        private void BatchUpload_Click(object sender, RoutedEventArgs e)
        {
            foreach (MenuItem menuItem in TabMainWindow.Items.OfType<MenuItem>())
            {
                menuItem.SetValue(MenuItem.BackgroundProperty, null);
                menuItem.SetValue(MenuItem.BorderBrushProperty, null);
            }
            MenuItem mi = TabBatchUpload as MenuItem;
            var converter = new System.Windows.Media.BrushConverter();
            mi.Background = (SolidColorBrush)converter.ConvertFromString("#3D26A0DA");
            mi.BorderBrush = (SolidColorBrush)converter.ConvertFromString("#FF26A0DA");
            //TabBatchUpload.Background
            ViewControl(new BatchUpload(), null);
        }

        private void ArchiveUpload_Click(object sender, RoutedEventArgs e)
        {
            foreach (MenuItem menuItem in TabMainWindow.Items.OfType<MenuItem>())
            {
                menuItem.SetValue(MenuItem.BackgroundProperty, null);
                menuItem.SetValue(MenuItem.BorderBrushProperty, null);
            }
            MenuItem mi = TabArchiveUpload as MenuItem;
            var converter = new System.Windows.Media.BrushConverter();
            mi.Background = (SolidColorBrush)converter.ConvertFromString("#3D26A0DA");
            mi.BorderBrush = (SolidColorBrush)converter.ConvertFromString("#FF26A0DA");
            ViewControl(new ArchiveUpload(), null);
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            BatchUpload_Click(sender, e);
        }

        private void RenameMoveArchive_Click(object sender, RoutedEventArgs e)
        {
            foreach (MenuItem menuItem in TabMainWindow.Items.OfType<MenuItem>())
            {
                menuItem.SetValue(MenuItem.BackgroundProperty, null);
                menuItem.SetValue(MenuItem.BorderBrushProperty, null);
            }
            MenuItem mi = TabRenameWorkArea as MenuItem;
            var converter = new System.Windows.Media.BrushConverter();
            mi.Background = (SolidColorBrush)converter.ConvertFromString("#3D26A0DA");
            mi.BorderBrush = (SolidColorBrush)converter.ConvertFromString("#FF26A0DA");
            ViewControl(new RenameAndMoveToArchive(), null);
        }

        //private void TabElasticUpload_Click(object sender, RoutedEventArgs e)
        //{
        //    foreach (MenuItem menuItem in TabMainWindow.Items.OfType<MenuItem>())
        //    {
        //        menuItem.SetValue(MenuItem.BackgroundProperty, null);
        //        menuItem.SetValue(MenuItem.BorderBrushProperty, null);
        //    }
        //    MenuItem mi = TabElasticUpload as MenuItem;
        //    var converter = new System.Windows.Media.BrushConverter();
        //    mi.Background = (SolidColorBrush)converter.ConvertFromString("#3D26A0DA");
        //    mi.BorderBrush = (SolidColorBrush)converter.ConvertFromString("#FF26A0DA");
        //    ViewControl(new ElasticUpload(), null);
        //}

    }
}
