﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiUpload.Model
{
    public class FolderStructure
    {
        public string FolderName { get; set; }
        public int FolderID { get; set; }
        public int DividerID { get; set; }
        public string Name { get; set; }
        public string OfficeID { get; set; }
        public int EFFID { get; set; }

    }
}
