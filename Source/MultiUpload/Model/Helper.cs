﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace MultiUpload.Model
{
    public static class Helper
    {
        private static string DeletedPath
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["DeletedWorkArea"]);
            }
        }

        public static int fileCount = 1;
        /// <summary>
        /// Get file names by folder.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static int GetFileNameByFolder(string path, TextBox textbox_queue)
        {
            
            try
            {

                if (Directory.Exists(path))
                {
                    string[] files = Directory.GetFiles(path);
                    if (files.Length > 0)
                    {
                        foreach (string folder in files)
                        {
                            try
                            {
                                if (folder.Substring(folder.LastIndexOf(".") + 1).ToLower() == "pdf")
                                {
                                    textbox_queue.AppendText("(" + fileCount + ")  " + folder);
                                    textbox_queue.AppendText(Environment.NewLine);
                                    fileCount++;
                                }
                                else
                                {
                                    continue;
                                }
                            }
                            catch (Exception ex) { }
                        }
                    }
                    string[] foldersList = Directory.GetDirectories(path);
                    if (foldersList.Length > 0)
                    {
                        foreach (string folder in foldersList)
                        {
                            try
                            {
                                if (!folder.Contains(DeletedPath.Split('\\')[0]))
                                {
                                    GetFileNameByFolder(folder, textbox_queue);
                                }
                            }
                            catch (Exception ex) { }

                        }
                    }
                }
                return fileCount;
            }

            catch (Exception ex) {
                MessageBoxResult value = System.Windows.MessageBox.Show("Access is denied:" + path,
                                                      "Alert");
                return 0; }
        }
    }
}
