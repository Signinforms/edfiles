﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiUpload.Model
{
    public class UserStructure
    {
        public string UID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OfficeUID { get; set; }
        public string IsSubUser { get; set; }
    }
}
