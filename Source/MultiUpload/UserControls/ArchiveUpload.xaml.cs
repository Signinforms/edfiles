﻿using MultiUpload.ServiceReference1;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Threading;
using System.Windows.Forms;
using MultiUpload.Model;
using System.Text.RegularExpressions;

namespace MultiUpload.UserControls
{
    /// <summary>
    /// Interaction logic for ArchiveUpload.xaml
    /// </summary>
    public partial class ArchiveUpload : System.Windows.Controls.UserControl
    {
        #region Properties
        private int _fileCount = 1;
        private int _fileSuccessCount = 1;
        private string _uploadPath = string.Empty;
        private string _successTabHeader = string.Empty;

        private static string RootPath
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["ArchiveUploadRootPath"]);
            }
        }

        private static string DeletedPath
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["DeletedArchive"]);
            }
        }
        EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();
        private static int _index = 1;
        #endregion

        #region Enums
        public enum Action
        {
            FolderCreated,
            TabCreated,
            FileUploaded,
            UploadError,
            FolderProcessFinished,
            FileDeleted,
            TabDeleted,
            FolderDeleted,
            TabDeleteError,
            FolderDeleteError,
            FileDeleteError,
            UnknownException
        }

        public enum FileAction
        {
            Create = 1,
            Delete = 2,
        }
        #endregion

        #region ctor
        public ArchiveUpload()
        {
            InitializeComponent();
        }
        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (string s in Directory.GetLogicalDrives())
                {
                    TreeViewItem item = new TreeViewItem();
                    item.Header = s;
                    item.Tag = s;
                    item.FontWeight = FontWeights.Normal;
                    item.FontSize = 15;
                    item.Items.Add(s);
                    item.Cursor = System.Windows.Input.Cursors.Hand;
                    item.Expanded += new RoutedEventHandler(folder_Expanded);
                    foldersItem.Items.Add(item);
                }
            }
            catch (Exception ex) { }
        }

        /// <summary>
        /// Get the treeview of Directory
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void folder_Expanded(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)sender;
            if (item != null && item.Items != null && item.Items.Count == 1 && item.Items[0] == Convert.ToString(item.Tag))
            {
                item.Items.Clear();
                try
                {
                    foreach (string s in Directory.GetDirectories(item.Tag.ToString()))
                    {
                        TreeViewItem subitem = new TreeViewItem();
                        subitem.Header = s.Substring(s.LastIndexOf("\\") + 1);
                        subitem.Tag = s;
                        subitem.FontWeight = FontWeights.Normal;
                        subitem.Items.Add(s);
                        subitem.Expanded += new RoutedEventHandler(folder_Expanded);
                        item.Items.Add(subitem);
                    }
                }
                catch (Exception)
                { }
            }
        }

        #region Upload
        /// <summary>
        /// Upload the queued files
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadData(object sender, RoutedEventArgs e)
        {
            if (textbox_queue.Text != "")
            {
                try
                {
                    string queueMsg = string.Empty;
                    int count = 1;
                    string notUploadedFile = string.Empty;
                    textbox_success.Text = "";
                    _fileSuccessCount = 1;
                    _successTabHeader = "Successful transfer";
                    SetSuccessTabHeader(_successTabHeader);

                    if (!string.IsNullOrEmpty(_uploadPath))
                    {
                        string[] path = new string[] { };
                        path = _uploadPath.Split(',');
                        foreach (string splitPath in path)
                        {
                            if (splitPath == "")
                                continue;
                            string userName = obj.CheckIfUserExists(splitPath);
                            string msg = "Files are getting uploaded . . . \n";
                            DisplayTextBoxValue(textBox1, msg);

                            if (userName == "")
                            {
                                string[] userFolders = Directory.GetDirectories(splitPath);
                                foreach (string user in userFolders)
                                {
                                    List<UserStructure> userDetails = JsonConvert.DeserializeObject<List<UserStructure>>(obj.GetUserIdFromUserName(user.Substring(user.LastIndexOf('\\') + 1)));
                                    if (userDetails != null && userDetails.Count > 0)
                                    {
                                        string uid = string.Empty;
                                        if (Convert.ToBoolean(Convert.ToInt32(userDetails[0].IsSubUser)))
                                            uid = userDetails[0].OfficeUID;
                                        else
                                            uid = userDetails[0].UID;
                                        Start(uid, user);
                                        count++;
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(notUploadedFile))
                                            notUploadedFile = user;
                                        else
                                            notUploadedFile = notUploadedFile + "," + user;
                                    }
                                }
                            }
                            else
                            {
                                List<UserStructure> userDetails = JsonConvert.DeserializeObject<List<UserStructure>>(obj.GetUserIdFromUserName(userName));
                                if (userDetails != null && userDetails.Count > 0)
                                {
                                    string uid = string.Empty;
                                    if (Convert.ToBoolean(Convert.ToInt32(userDetails[0].IsSubUser)))
                                        uid = userDetails[0].OfficeUID;
                                    else
                                        uid = userDetails[0].UID;
                                    Start(uid, splitPath);
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(notUploadedFile))
                        {
                            string[] filenames = notUploadedFile.Split(',');
                            textbox_queue.Text = "";
                            _fileCount = 1;
                            Helper.fileCount = 1;
                            string folderName = string.Empty;
                            foreach (string splitPath in filenames)
                            {
                                if (splitPath == "")
                                    continue;
                                Helper.GetFileNameByFolder(splitPath, textbox_queue);
                                _fileCount = Helper.fileCount;
                                queueMsg = "Queued Files(" + (_fileCount - 1) + ")";
                                SetQueueTabHeader(queueMsg);
                                if (string.IsNullOrEmpty(folderName))
                                    folderName = splitPath.Substring(splitPath.LastIndexOf("\\") + 1);
                                else
                                    folderName = folderName + "," + splitPath.Substring(splitPath.LastIndexOf("\\") + 1);
                            }
                            if (textbox_success.Text != "")
                            {
                                _successTabHeader = "Successful transfer(" + (_fileSuccessCount - 1) + ")";
                                SetSuccessTabHeader(_successTabHeader);
                            }
                            MessageBoxResult value = System.Windows.MessageBox.Show("We are not able to get User Details of " + folderName + " folder!! \n Please choose another folder",
                                                "Alert");
                        }
                        else
                        {
                            if (textbox_success.Text != "")
                            {
                                _successTabHeader = "Successful transfer(" + (_fileSuccessCount - 1) + ")";
                                SetSuccessTabHeader(_successTabHeader);
                                if (textbox_queue.Text == "")
                                {
                                    queueMsg = "Queued Files";
                                    SetQueueTabHeader(queueMsg);
                                }
                                _fileSuccessCount = 1;
                                MessageBoxResult value = System.Windows.MessageBox.Show("All files have been uploaded successfully",
                                                              "Success");
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    WriteWindowProcessLog(ex.Message);
                }
            }
            else
            {
                MessageBoxResult value = System.Windows.MessageBox.Show("Please add files to queue to upload!! \n (To add items in queue, right click the directory and click on Add to Queue.)",
                                                                                "Alert");
            }
            _uploadPath = "";
        }

        private async void FindOrCreateArchive(string[] subFolders, string folderName, string uid, ServiceReference1.EFileFolderJSONWSSoapClient obj, string folderpath, int level)
        {
            try
            {
                string msg = string.Empty;
                foreach (string folder in subFolders)
                {
                    int newLevel = level;
                    int folderID = obj.InsertFolderArchiveTree(folder.Substring(folder.LastIndexOf('\\') + 1).Split('#')[0].Trim(), Convert.ToInt32(folderpath.Substring(folderpath.LastIndexOf('/') + 1)), uid, newLevel, folderpath);
                    newLevel++;
                    folderpath = folderpath + "/" + folderID.ToString();
                    if (folderID > 0)
                    {
                        string[] files = Directory.GetFiles(folder);
                        if (files.Length > 0)
                        {
                            try
                            {
                                foreach (string file in files)
                                {
                                    if (file.Substring(file.LastIndexOf(".") + 1).ToLower() != "pdf")
                                        continue;
                                    string fileName = System.IO.Path.GetFileName(file);
                                    try
                                    {
                                        fileName = Regex.Replace(fileName, "[^0-9a-zA-Z-:,_.]+", "");
                                    }
                                    catch (Exception ex) { }
                                    string base64String = string.Empty;
                                    try
                                    {
                                        base64String = Convert.ToBase64String(File.ReadAllBytes(file));
                                    }
                                    catch (Exception) { }
                                    msg = "File " + fileName + " is uploading....\n";
                                    DisplayTextBoxValue(textBox1, msg);
                                    if (textbox_queue.Text != "")
                                        RemoveLinesContainingToken(textbox_queue, file);
                                    int archiveId = obj.InsertDocumentArchive(fileName, folderpath, uid, null, base64String, true);
                                    if (archiveId > 0)
                                    {
                                        try
                                        {
                                            if (File.Exists(file))
                                            {
                                                string[] splitData = file.Split(new string[] { RootPath }, StringSplitOptions.None); ;
                                                if (splitData.Length > 1)
                                                {
                                                    string dest = RootPath + DeletedPath + splitData[1];
                                                    if (!Directory.Exists(System.IO.Path.GetDirectoryName(dest)))
                                                        Directory.CreateDirectory(System.IO.Path.GetDirectoryName(dest));
                                                    File.Move(file, dest);
                                                }
                                            }
                                        }
                                        catch (Exception ex) { }

                                        msg = "File " + fileName + " has been uploaded successfully !!! \n";
                                        DisplayTextBoxValue(textBox1, msg);
                                        msg = "(" + _fileSuccessCount + ")  " + file;
                                        DisplayTextBoxValue(textbox_success, msg);
                                        textBox1.AppendText(Environment.NewLine);
                                        _successTabHeader = "Successful transfer(" + (_fileSuccessCount - 1) + ")";
                                        SetSuccessTabHeader(_successTabHeader);
                                        textbox_success.AppendText(Environment.NewLine);
                                        _fileSuccessCount++;
                                    }
                                    else
                                    {
                                        msg = "Failed to upload " + fileName + " !!! \n";
                                        DisplayTextBoxValue(textBox1, msg);
                                        textBox1.AppendText(Environment.NewLine);
                                        WriteWindowServiceLog(0, 0, uid, Action.UploadError.ToString(), 0, file.Substring(file.LastIndexOf('\\') + 1), "Document insert error.");
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                msg = "Failed to upload !!! \n";
                                DisplayTextBoxValue(textBox1, msg);
                                textBox1.AppendText(Environment.NewLine);
                            }
                        }
                        if (Directory.GetDirectories(folder).Length > 0)
                        {
                            try
                            {
                                string[] folders = Directory.GetDirectories(folder);
                                FindOrCreateArchive(folders, folder, uid, obj, folderpath, newLevel);
                                if (folderpath.LastIndexOf('/') > 0)
                                {
                                    folderpath = folderpath.Substring(0, folderpath.LastIndexOf('/'));
                                    newLevel--;
                                }
                            }
                            catch (Exception ex)
                            {
                                WriteWindowProcessLog(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, ex.Message);
                            }
                        }
                        else
                        {
                            try
                            {
                                if (folderpath.LastIndexOf('/') > 0)
                                {
                                    folderpath = folderpath.Substring(0, folderpath.LastIndexOf('/'));
                                    newLevel--;
                                }
                            }
                            catch (Exception) { }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteWindowProcessLog(string.Empty, string.Empty, uid, string.Empty, string.Empty, ex.Message);
            }
        }


        /// <summary>
        /// Upload files from menu
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UploadMenuItem_Click(object sender, RoutedEventArgs e)
        {
            cmTreeView.Visibility = Visibility.Hidden;
            textbox_success.Text = "";
            _successTabHeader = "Successful transfer";
            SetSuccessTabHeader(_successTabHeader);
            if (textbox_queue.Text == "")
            {
                string queueMsg = "Queued Files";
                SetQueueTabHeader(queueMsg);
            }
            string notUploadedFile = string.Empty;

            System.Windows.Controls.MenuItem mi = sender as System.Windows.Controls.MenuItem;
            try
            {
                if (mi != null)
                {

                    string path = GetStructurePath(mi);

                    string userName = obj.CheckIfUserExists(path);
                    string msg = "Files are getting uploaded . . . \n";
                    DisplayTextBoxValue(textBox1, msg);

                    if (userName == "")
                    {
                        try
                        {
                            string[] userFolders = Directory.GetDirectories(path);
                            foreach (string user in userFolders)
                            {
                                List<UserStructure> userDetails = JsonConvert.DeserializeObject<List<UserStructure>>(obj.GetUserIdFromUserName(user.Substring(user.LastIndexOf('\\') + 1)));
                                if (userDetails != null && userDetails.Count > 0)
                                {
                                    string uid = string.Empty;
                                    if (Convert.ToBoolean(Convert.ToInt32(userDetails[0].IsSubUser)))
                                        uid = userDetails[0].OfficeUID;
                                    else
                                        uid = userDetails[0].UID;
                                    Start(uid, user);
                                }
                                else
                                {
                                    if (string.IsNullOrEmpty(notUploadedFile))
                                        notUploadedFile = user;
                                    else
                                        notUploadedFile = notUploadedFile + "," + user;
                                }
                            }
                        }
                        catch (Exception ex) { }
                    }
                    else
                    {
                        List<UserStructure> userDetails = JsonConvert.DeserializeObject<List<UserStructure>>(obj.GetUserIdFromUserName(userName));
                        if (userDetails != null && userDetails.Count > 0)
                        {
                            string uid = string.Empty;
                            if (Convert.ToBoolean(Convert.ToInt32(userDetails[0].IsSubUser)))
                                uid = userDetails[0].OfficeUID;
                            else
                                uid = userDetails[0].UID;
                            Start(uid, path);
                        }
                    }
                    if (!string.IsNullOrEmpty(notUploadedFile))
                    {
                        string[] filenames = notUploadedFile.Split(',');
                        string folderName = string.Empty;
                        foreach (string splitPath in filenames)
                        {
                            if (splitPath == "")
                                continue;
                            if (string.IsNullOrEmpty(folderName))
                                folderName = splitPath.Substring(splitPath.LastIndexOf("\\") + 1);
                            else
                                folderName = folderName + "," + splitPath.Substring(splitPath.LastIndexOf("\\") + 1);
                        }

                        if (textbox_success.Text != "")
                        {
                            _successTabHeader = "Successful transfer(" + (_fileSuccessCount - 1) + ")";
                            SetSuccessTabHeader(_successTabHeader);
                        }
                        MessageBoxResult value = System.Windows.MessageBox.Show("We are not able to get User Details of " + folderName + " folder!! \n Please choose another folder",
                                            "Alert");
                    }
                    else
                    {
                        if (textbox_success.Text != "")
                        {
                            textbox_success.UpdateLayout();
                            _successTabHeader = "Successful transfer(" + (_fileSuccessCount - 1) + ")";
                            SetSuccessTabHeader(_successTabHeader);
                            _fileSuccessCount = 1;
                            MessageBoxResult value = System.Windows.MessageBox.Show("All files have been uploaded successfully",
                                                           "Success");
                        }
                    }
                }
            }
            catch (Exception ex) { }
        }

        public void Start(string uid, string folder)
        {
            try
            {
                string msg = string.Empty;
                int level = 1;
                int newparentid = obj.InsertFolderArchiveTree(folder.Substring(folder.LastIndexOf('\\') + 1), 0, uid, level, string.Empty);
                level++;
                WriteWindowProcessLog("Step 4: Fetch Folderid", newparentid.ToString(), uid, "");
                int tabId = 0;
                string[] filesList = Directory.GetFiles(folder);
                if (filesList.Length > 0)
                {
                    int i = 1;
                    foreach (string file in filesList)
                    {
                        try
                        {
                            if (file.Substring(file.LastIndexOf(".") + 1).ToLower() != "pdf")
                                continue;
                            int archiveId = 0;
                            WriteWindowProcessLog("Step 5: upload file", newparentid.ToString(), uid, "", file);
                            string fileName = System.IO.Path.GetFileName(file);
                            fileName = fileName.Replace("#", "");
                            fileName = fileName.Replace("&", "");
                            msg = "File " + fileName + " is uploading....\n";
                            DisplayTextBoxValue(textBox1, msg);

                            if (textbox_queue.Text != "")
                                RemoveLinesContainingToken(textbox_queue, file);
                            string base64String = string.Empty;
                            try
                            {
                                base64String = Convert.ToBase64String(File.ReadAllBytes(file));
                            }
                            catch (Exception ex) { }

                            archiveId = obj.InsertDocumentArchive(fileName, newparentid.ToString(), uid, null, base64String, true);
                            if (archiveId > 0)
                            {
                                try
                                {
                                    if (File.Exists(file))
                                    {
                                        string[] splitData = file.Split(new string[] { RootPath }, StringSplitOptions.None); ;
                                        if (splitData.Length > 1)
                                        {
                                            string dest = RootPath + DeletedPath + splitData[1];
                                            if (!Directory.Exists(System.IO.Path.GetDirectoryName(dest)))
                                                Directory.CreateDirectory(System.IO.Path.GetDirectoryName(dest));
                                            File.Move(file, dest);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    WriteWindowServiceLog(newparentid, tabId, uid, Action.UploadError.ToString(), 0, file.Substring(file.LastIndexOf('\\') + 1), ex.Message);
                                }
                                msg = "File " + fileName + "has been uploaded successfully !!! \n";
                                DisplayTextBoxValue(textBox1, msg);

                                msg = "(" + _fileSuccessCount + ")  " + file;
                                DisplayTextBoxValue(textbox_success, msg);
                                _successTabHeader = "Successful transfer(" + (_fileSuccessCount - 1) + ")";
                                SetSuccessTabHeader(_successTabHeader);
                                textbox_success.AppendText(Environment.NewLine);
                                i++; _fileSuccessCount++;
                            }
                            else
                            {
                                msg = "Failed to upload " + fileName + " !!! \n";
                                DisplayTextBoxValue(textBox1, msg);
                                textBox1.AppendText(Environment.NewLine);
                                WriteWindowServiceLog(newparentid, tabId, uid, Action.UploadError.ToString(), 0, file.Substring(file.LastIndexOf('\\') + 1), "Document insert error.");
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteWindowServiceLog(newparentid, tabId, uid, Action.UploadError.ToString(), 0, file.Substring(file.LastIndexOf('\\') + 1), ex.Message);
                            continue;
                        }
                    }
                }
                string[] subFolders = Directory.GetDirectories(folder);
                if (newparentid > 0 && subFolders.Length != 0)
                {
                    FindOrCreateArchive(subFolders, folder, uid, obj, newparentid.ToString(), level);
                }
            }
            catch (Exception ex)
            {
                WriteWindowServiceLog(0, 0, uid, Action.UploadError.ToString(), 0, string.Empty, ex.Message);
            }
        }

        /// <summary>
        /// Remove the uploaded file from Textbox
        /// </summary>
        /// <param name="tb">Textbox where file exist</param>
        /// <param name="Token">Filename to remove</param>
        private void RemoveLinesContainingToken(System.Windows.Controls.TextBox tb, string Token)
        {
            try
            {
                String newText = String.Empty;
                List<String> lines = tb.Text.Split(new String[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();
                int index = lines.FindIndex(str => str.Contains(Token));
                if (index != -1)
                {
                    lines.RemoveAt(index);
                    lines.ForEach(str => newText += str + Environment.NewLine);
                    textbox_queue.Text = newText;
                    _fileCount--;
                    string queueMsg = "Queued Files(" + (_fileCount - 1) + ")";
                    SetQueueTabHeader(queueMsg);
                }
            }
            catch (Exception ex) { }
        }
        #endregion

        /// <summary>
        /// Add Files to queue
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddToQueue_Click(object sender, RoutedEventArgs e)
        {
            textbox_success.Text = "";
            _successTabHeader = "Successful transfer";
            SetSuccessTabHeader(_successTabHeader);
            System.Windows.Controls.MenuItem mi = sender as System.Windows.Controls.MenuItem;
            if (mi != null)
            {
                string path = GetStructurePath(mi);
                int value = Helper.GetFileNameByFolder(path, textbox_queue);
                string queueMsg = "Queued Files(" + (Helper.fileCount - 1) + ")";
                SetQueueTabHeader(queueMsg);
                _fileCount = Helper.fileCount;
                _uploadPath = _uploadPath + "," + path;
            }
        }

        /// <summary>
        /// Open selcted folder in File Explorer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenExplorer_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.MenuItem mi = sender as System.Windows.Controls.MenuItem;
            if (mi != null)
            {
                string path = GetStructurePath(mi);
                if (path != "")
                    Process.Start(path);
            }
        }

        private void OnPreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            TreeViewItem treeViewItem = VisualUpwardSearch(e.OriginalSource as DependencyObject);
            if (treeViewItem != null)
            {
                treeViewItem.Focus();
                e.Handled = true;
            }
        }

        private static TreeViewItem VisualUpwardSearch(DependencyObject source)
        {
            while (source != null && !(source is TreeViewItem))
                source = VisualTreeHelper.GetParent(source);
            return source as TreeViewItem;
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        /// <summary>
        /// Remove files from Queue
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RemoveText_Click(object sender, RoutedEventArgs e)
        {
            if (textbox_queue.Text == "")
            {
                MessageBoxResult message = System.Windows.MessageBox.Show("There is no file in the queue",
                                                       "Alert");
            }
            else
            {
                MessageBoxResult result = System.Windows.MessageBox.Show("Are you sure, you want to clear the queue?",
                                              "Confirmation",
                                              MessageBoxButton.YesNo,
                                              MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    textbox_queue.Text = "";
                    string queueMsg = "Queued Files";
                    SetQueueTabHeader(queueMsg);
                    Helper.fileCount = 1;
                    _fileCount = 1;
                    _uploadPath = "";
                }
            }
        }

        /// <summary>
        /// Get path of selected folder
        /// </summary>
        /// <param name="menuItem"></param>
        /// <returns></returns>
        public string GetStructurePath(System.Windows.Controls.MenuItem menuItem)
        {
            string path = "";
            string temp1 = "";
            string temp2 = "";
            System.Windows.Controls.ContextMenu contextMenu = menuItem.CommandParameter as System.Windows.Controls.ContextMenu;
            if (contextMenu != null)
            {
                System.Windows.Controls.TreeView treeView = contextMenu.PlacementTarget as System.Windows.Controls.TreeView;
                if (treeView != null)
                {
                    TreeViewItem treeViewItem = ((TreeViewItem)treeView.SelectedItem);
                    if (treeViewItem == null)
                        return "";
                    while (true)
                    {
                        temp1 = treeViewItem.Header.ToString();
                        if (temp1.Contains(@"\"))
                        {
                            temp2 = "";
                        }
                        path = temp1 + temp2 + path;
                        if (treeViewItem.Parent.GetType().ToString() == "System.Windows.Controls.TreeView")
                            break;
                        treeViewItem = ((TreeViewItem)treeViewItem.Parent);
                        temp2 = @"\";
                    }
                }
            }
            return path;

        }

        /// <summary>
        /// Set value in textbox
        /// </summary>
        /// <param name="textbox"></param>
        /// <param name="msg"></param>
        private void DisplayTextBoxValue(System.Windows.Controls.TextBox textbox, string msg)
        {
            Dispatcher.Invoke(new System.Action(() =>
            {
                textbox.Text += msg;
            }), DispatcherPriority.Background);
        }

        /// <summary>
        /// Set value of Success tab
        /// </summary>
        /// <param name="message"></param>
        private void SetSuccessTabHeader(string message)
        {
            SuccessTabHeader.Header = message;
        }

        /// <summary>
        /// Set value of Queue tab
        /// </summary>
        /// <param name="message"></param>
        private void SetQueueTabHeader(string message)
        {
            QueueTabHeader.Header = message;
        }

        #region Logs
        /// <summary>
        /// write document logs
        /// </summary>
        /// <param name="folderId"></param>
        /// <param name="dividerId"></param>
        /// <param name="uid"></param>
        /// <param name="action"></param>
        /// <param name="documentId"></param>
        /// <param name="fileName"></param>
        /// <param name="errorMsg"></param>
        public static void WriteWindowServiceLog(int folderId, int dividerId, string uid, string action, int documentId = 0, string fileName = "", string errorMsg = "")
        {
            try
            {
                if (!Directory.Exists("C:\\CopyFolderLog"))
                    Directory.CreateDirectory("C:\\CopyFolderLog");

                using (StreamWriter swData2 = new StreamWriter("C:\\CopyFolderLog\\CopyFolderLog.txt", true))
                {
                    swData2.WriteLine("===== Log " + DateTime.Now);
                    swData2.WriteLine("FolderID = " + folderId);
                    swData2.WriteLine("DividerId = " + dividerId);
                    swData2.WriteLine("fileName = " + fileName);
                    swData2.WriteLine("documentId = " + documentId);
                    swData2.WriteLine("uid = " + uid);
                    swData2.WriteLine("action = " + action);
                    swData2.WriteLine("error = " + errorMsg);
                    swData2.WriteLine("==============================================================");
                    swData2.Close();
                    swData2.Dispose();
                }
            }
            catch (Exception ex1)
            {
                string errorMessage = ex1.Message;
            }
        }

        /// <summary>
        /// Write process logs
        /// </summary>
        /// <param name="process"></param>
        /// <param name="folderId"></param>
        /// <param name="uid"></param>
        /// <param name="action"></param>
        /// <param name="fileName"></param>
        /// <param name="errorMsg"></param>
        public static void WriteWindowProcessLog(string process, string folderId = "", string uid = "", string action = "", string fileName = "", string errorMsg = "")
        {
            try
            {
                if (!Directory.Exists("C:\\FolderProcessLog"))
                    Directory.CreateDirectory("C:\\FolderProcessLog");

                using (StreamWriter swData2 = new StreamWriter("C:\\FolderProcessLog\\FolderProcessLog.txt", true))
                {
                    swData2.WriteLine("===== Log " + DateTime.Now);
                    swData2.WriteLine("Step = " + process);
                    swData2.WriteLine("FolderID = " + folderId);
                    swData2.WriteLine("fileName = " + fileName);
                    swData2.WriteLine("uid = " + uid);
                    swData2.WriteLine("error = " + errorMsg);
                    swData2.WriteLine("==============================================================");
                    swData2.Close();
                    swData2.Dispose();
                }
            }
            catch (Exception ex1)
            {
                string errorMessage = ex1.Message;
            }
        }
        #endregion
    }
}
