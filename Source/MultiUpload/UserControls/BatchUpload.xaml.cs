﻿using MultiUpload.ServiceReference1;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Threading;
using MultiUpload.Model;
using System.Text.RegularExpressions;
//using System.Windows.Shapes;

namespace MultiUpload.UserControls
{
    /// <summary>
    /// Interaction logic for BatchUpload.xaml
    /// </summary>
    public partial class BatchUpload : UserControl
    {
        #region Properties
        private int _fileCount = 1;
        private int _fileSuccessCount = 1;
        private string _newFileName = string.Empty;

        private static string RootPath
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["BatchUploadRootPath"]);
            }
        }

        private static string CurrentVolume
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["CurrentVolume"]);
            }
        }

        private static string BasePath
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["BatchUploadBasePath"]);
            }
        }

        private static string DeletedPath
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["DeletedWorkArea"]);
            }
        }
        ServiceReference1.EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();
        #endregion

        #region Enum
        public enum Action
        {
            FolderCreated,
            TabCreated,
            FileUploaded,
            UploadError,
            FolderProcessFinished,
            FileDeleted,
            TabDeleted,
            FolderDeleted,
            TabDeleteError,
            FolderDeleteError,
            FileDeleteError,
            UnknownException
        }

        public enum FileAction
        {
            Create = 1,
            Delete = 2,
        }
        #endregion

        #region Constructor
        public BatchUpload()
        {
            InitializeComponent();
        }
        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ShowFiles();
        }

        #region Upload
        /// <summary>
        /// Upload the files 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BatchUpload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (textbox_queue.Text == "")
                {
                    MessageBoxResult message = MessageBox.Show("Please add files in Queue",
                                                                "Error");
                }
                else
                {
                    textbox_success.Text = "";
                    textBox1.Text = "";
                    Start();
                    if (textbox_success.Text != "")
                    {
                        SuccessTabHeader.Header = "Successful transfer(" + (_fileSuccessCount - 1) + ")";
                        if (textbox_queue.Text == "")
                            QueueTabHeader.Header = "Queued Files";

                        _fileSuccessCount = 1;
                        MessageBoxResult value = MessageBox.Show("All files have been uploaded successfully",
                                                      "Success");
                    }
                }
            }
            catch (Exception ex) { }
        }

        private void FindOrCreateTab(string[] tabFolders, List<FolderStructure> tabsDetails, string folderName, string uid, ServiceReference1.EFileFolderJSONWSSoapClient obj, int folderId, string fileName = null)
        {
            int tabId = 0;
            string color = null;
            foreach (string tab in tabFolders)
            {
                if (string.IsNullOrEmpty(tab))
                {
                    break;
                }

                bool isTabFound = false;
                foreach (FolderStructure tabRow in tabsDetails)
                {
                    folderId = Convert.ToInt32(tabRow.FolderID);
                    //Check if tab exists in folder in database
                    if (string.Compare(tab.Substring(tab.LastIndexOf('\\') + 1).Split('#')[0].Trim(), tabRow.Name.ToString(), true) == 0)
                    {
                        isTabFound = true;
                        tabId = Convert.ToInt32(tabRow.DividerID);
                        break;
                    }
                }
                //Upload files if tab exists
                if (isTabFound && folderId > 0)
                {
                    try
                    {
                        //Upload files
                        string[] files = new string[] { fileName };
                        if (string.IsNullOrEmpty(fileName))
                            files = Directory.GetFiles(tab);
                        CopyFile(files, tabId, folderId, folderName.Substring(folderName.LastIndexOf('\\') + 1), uid);
                        if (string.IsNullOrEmpty(fileName) && Directory.GetFiles(tab).Length <= 0)
                        {
                            try
                            {
                                Directory.Delete(tab);
                                WriteWindowServiceLog(folderId, tabId, uid, Action.TabDeleted.ToString(), 0, tab);
                            }
                            catch (Exception ex)
                            {
                                WriteWindowServiceLog(folderId, tabId, uid, Action.TabDeleteError.ToString(), 0, tab, ex.Message);
                            }
                        }
                    }
                    catch (Exception ex) { }
                }
                else
                {
                    //Create tab
                    if (tab.Substring(tab.LastIndexOf('\\') + 1).Split('#').Length > 1)
                    {
                        color = '#' + tab.Substring(tab.LastIndexOf('\\') + 1).Split('#')[1].ToUpper();
                    }
                    else
                    {
                        color = "#FFCCFF";
                    }
                    if (folderId > 0)
                    {
                        try
                        {
                            tabId = Convert.ToInt32(obj.CreateDivider(tab.Substring(tab.LastIndexOf('\\') + 1).Split('#')[0].Trim(), uid, folderId, color));
                            WriteWindowServiceLog(folderId, tabId, uid, Action.TabCreated.ToString());
                            //Get files from folder
                            string[] files = new string[] { fileName };
                            if (string.IsNullOrEmpty(fileName))
                                files = Directory.GetFiles(tab);
                            //Upload files                                         
                            CopyFile(files, tabId, folderId, folderName.Substring(folderName.LastIndexOf('\\') + 1), uid);
                            if (string.IsNullOrEmpty(fileName) && Directory.GetFiles(tab).Length <= 0)
                            {
                                try
                                {
                                    Directory.Delete(tab);
                                    WriteWindowServiceLog(folderId, tabId, uid, Action.TabDeleted.ToString(), 0, tab);
                                }
                                catch (Exception ex)
                                {
                                    WriteWindowServiceLog(folderId, tabId, uid, Action.TabDeleteError.ToString(), 0, tab, ex.Message);
                                }
                            }
                        }
                        catch (Exception ex) { }
                    }
                }
            }
        }

        private void Start()
        {
            try
            {
                //Create Sync folder if does not exist
                if (!Directory.Exists(RootPath))
                {
                    Directory.CreateDirectory(RootPath);
                }
                //Check if Sync folder exists
                if (Directory.Exists(RootPath))
                {
                    //Get all UserName folders from Sync folder
                    string[] userFolders = Directory.GetDirectories(RootPath);

                    foreach (string userName in userFolders)
                    {
                        string filepath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                        List<UserStructure> userDetails = JsonConvert.DeserializeObject<List<UserStructure>>(obj.GetUserIdFromUserName(userName.Substring(userName.LastIndexOf('\\') + 1)));
                        if (userDetails != null && userDetails.Count > 0)
                        {
                            //Get UID from UserName
                            string uid = userDetails[0].OfficeUID == null ? userDetails[0].UID : userDetails[0].OfficeUID;
                            //Get all folders inside UserName folder.
                            string[] folders = Directory.GetDirectories(userName);
                            string[] files = Directory.GetFiles(userName);
                            string msg = "Files are getting uploaded . . . \n";
                            DataTable officeid = obj.GetOFFiceID(uid);
                            string officeId = string.Empty;
                            if (officeid == null)
                                officeId = uid;
                            else
                                officeId = officeid.Rows[0]["OfficeUID"].ToString();
                            if (files.Length > 0)
                            {
                                foreach (string fileName in files)
                                {
                                    string[] splitFiles = Path.GetFileNameWithoutExtension(fileName).Split('-');
                                    string folderName = splitFiles[0].Trim();
                                    string tabName = string.Empty, firstName = string.Empty, lastName = string.Empty, fileNumber = null, alert = null, status = null;
                                    //string templateName = string.Empty;
                                    //int startTemplateIndex = 0;
                                    //int endTemplateIndex = 0;
                                    string[] splitFolderName = folderName.Split(',');
                                    lastName = folderName;
                                    firstName = folderName;
                                    tabName = folderName;

                                    //startTemplateIndex = fileName.IndexOf("(") + ("(".Length);
                                    //endTemplateIndex = fileName.LastIndexOf(")");

                                    //templateName = fileName.Substring(startTemplateIndex, endTemplateIndex - startTemplateIndex);
                                    string folderNameAsName = fileName.Split('-')[0].Substring(fileName.Split('-')[0].LastIndexOf('\\')).Split('\\')[1].Trim();
                                    string templateName = string.Empty;
                                    if (fileName.IndexOf('(') > 0)
                                    {
                                        string temp = fileName.Split('(')[1].Trim();
                                        if (temp.IndexOf(')') > 0)
                                        {
                                            templateName = temp.Substring(0, temp.LastIndexOf(')'));
                                            if (folderNameAsName.LastIndexOf('(') > 0 && !string.IsNullOrEmpty(folderNameAsName.Substring(0, folderNameAsName.LastIndexOf('('))))
                                                folderNameAsName = folderNameAsName.Substring(0, folderNameAsName.LastIndexOf('('));
                                        }
                                    }

                                    //if (!string.IsNullOrEmpty(templateName))
                                    //{
                                    //    tabName = string.Empty;
                                    //}

                                    if (splitFolderName.Length > 1)
                                    {
                                        lastName = splitFolderName[0].Trim();
                                        firstName = splitFolderName[1].Trim();
                                    }
                                    if (splitFiles.Length > 3)
                                    {
                                        //tabName = splitFiles[1].Trim();
                                        fileNumber = splitFiles[1].Trim();
                                        alert = splitFiles[2].Trim();

                                        if (!string.IsNullOrEmpty(templateName) && splitFiles[3].Trim().Contains(templateName))
                                        {
                                            status = splitFiles[3].Trim().Replace("(" + templateName + ")", "");
                                        }
                                        else
                                        {
                                            status = splitFiles[3].Trim();
                                        }
                                    }
                                    else if (splitFiles.Length > 2)
                                    {
                                        fileNumber = splitFiles[1].Trim();
                                        alert = splitFiles[2].Trim();
                                    }
                                    else if (splitFiles.Length > 1)
                                    {
                                        fileNumber = splitFiles[1].Trim();
                                    }
                                    try
                                    {
                                        List<FolderStructure> tabsDetails = JsonConvert.DeserializeObject<List<FolderStructure>>(obj.IsSameFolderExist(uid, folderName.Substring(folderName.LastIndexOf('\\') + 1).Split('-')[0].Trim(), fileNumber, true));
                                        int tabId = 0, folderId = 0;

                                        if (tabsDetails != null && tabsDetails.Count > 0)
                                        {

                                            //FindOrCreateTab(new string[] { tabName }, tabsDetails, folderName, uid, obj, folderId, fileName);
                                        }
                                        else
                                        {
                                            DataSet dt = obj.CreateFolder(false, folderName, uid, firstName, lastName, 1, fileNumber, alert, templateName, "", "", "", "", "", "", status, "", officeId, true);
                                            //DataSet dt = obj.CreateFolder(false, folderNameAsName, uid, firstName, lastName, tabFolders.Length, fileNumber, alert, templateName, "", "", "", "", "", "", status, "", officeId, true);

                                            folderId = Convert.ToInt32(dt.Tables[0].Rows[0]["Column1"]);

                                            WriteWindowServiceLog(folderId, tabId, uid, Action.FolderCreated.ToString());
                                            if (dt.Tables[1].Rows.Count > 0)
                                                tabsDetails = JsonConvert.DeserializeObject<List<FolderStructure>>(JsonConvert.SerializeObject(dt.Tables[1]));

                                            //FindOrCreateTab(new string[] { tabName }, tabsDetails, folderName, uid, obj, folderId, fileName);
                                        }

                                        string[] totalTabDetails = new string[15];

                                        if (tabsDetails.Count > 0)
                                        {
                                            for (int i = 0; i < tabsDetails.Count; i++)
                                            {
                                                totalTabDetails[i] = tabsDetails[i].Name;
                                            }
                                        }
                                        else
                                        {
                                            totalTabDetails[0] = tabName;
                                        }

                                        FindOrCreateTab(totalTabDetails, tabsDetails, folderName, uid, obj, folderId, fileName);

                                    }
                                    catch (Exception ex)
                                    {
                                        WriteWindowServiceLog(0, 0, uid, Action.UploadError.ToString(), 0, string.Empty, ex.Message);
                                    }
                                }
                            }
                            if (folders.Length > 0)
                            {
                                foreach (string folderName in folders)
                                {
                                    DisplayTextBoxValue(textBox1, msg);
                                    string folder = folderName.Substring(folderName.LastIndexOf('\\') + 1);
                                    string[] splitFolderName = folder.Split(',');
                                    string firstName = string.Empty, lastName = string.Empty, fileNumber = null, alert = null, status = null;
                                    if (splitFolderName.Length > 1)
                                    {
                                        if (splitFolderName.Length > 1)
                                        {
                                            if (splitFolderName[1].Trim().IndexOf('-') > 0)
                                            {
                                                string[] info = splitFolderName[1].Trim().Split('-');
                                                if (!string.IsNullOrEmpty(info[1].Trim()))
                                                    fileNumber = info[1].Trim();
                                            }
                                        }
                                    }
                                    //Check if folder exists and if exists get all tabs
                                    List<FolderStructure> tabsDetails = JsonConvert.DeserializeObject<List<FolderStructure>>(obj.IsSameFolderExist(uid, folderName.Substring(folderName.LastIndexOf('\\') + 1).Split('-')[0].Trim(), fileNumber, true));

                                    //Get sub folders from folder (tabs)
                                    string[] tabFolders = Directory.GetDirectories(folderName);
                                    int tabId = 0, folderId = 0;
                                    string color = null;
                                    if (tabFolders.Length > 0 && tabsDetails != null && tabsDetails.Count > 0)
                                    {
                                        FindOrCreateTab(tabFolders, tabsDetails, folderName, uid, obj, folderId);
                                    }
                                    else if (tabFolders.Length > 0)
                                    {
                                        if (splitFolderName.Length > 1)
                                        {
                                            lastName = splitFolderName[0].Trim();
                                            if (splitFolderName[1].Trim().IndexOf('-') > 0)
                                            {
                                                string[] info = splitFolderName[1].Trim().Split('-');
                                                firstName = info[0].Trim();
                                                if (!string.IsNullOrEmpty(info[1].Trim()))
                                                    fileNumber = info[1].Trim();
                                                if (info.Length > 2 && !string.IsNullOrEmpty(info[2].Trim()))
                                                {
                                                    if (info[2].LastIndexOf('(') >= 0)
                                                        alert = info[2].Substring(0, info[2].LastIndexOf('(')).Trim();
                                                    else
                                                        alert = info[2].Trim();
                                                }
                                                if (info.Length > 3 && !string.IsNullOrEmpty(info[3].Trim()))
                                                {
                                                    if (info[3].LastIndexOf('(') >= 0)
                                                        status = info[3].Substring(0, info[3].LastIndexOf('(')).Trim();
                                                    else
                                                        status = info[3].Trim();
                                                }
                                            }
                                            else
                                            {
                                                if (!string.IsNullOrEmpty(splitFolderName[1].Trim()))
                                                    firstName = splitFolderName[1].Trim();
                                                else
                                                    firstName = userName.Substring(userName.LastIndexOf('\\') + 1);
                                            }
                                        }
                                        else
                                            firstName = lastName = userName.Substring(userName.LastIndexOf('\\') + 1);

                                        string folderNameAsName = folderName.Split('-')[0].Substring(folderName.Split('-')[0].LastIndexOf('\\')).Split('\\')[1].Trim();
                                        string templateName = string.Empty;
                                        if (folderName.IndexOf('(') > 0)
                                        {
                                            string temp = folderName.Split('(')[1].Trim();
                                            if (temp.IndexOf(')') > 0)
                                            {
                                                templateName = temp.Substring(0, temp.LastIndexOf(')'));
                                                if (folderNameAsName.LastIndexOf('(') > 0 && !string.IsNullOrEmpty(folderNameAsName.Substring(0, folderNameAsName.LastIndexOf('('))))
                                                    folderNameAsName = folderNameAsName.Substring(0, folderNameAsName.LastIndexOf('('));
                                            }
                                        }

                                        //folderId = Convert.ToInt32(obj.CreateFolder(folderName.Substring(folderName.LastIndexOf('\\') + 1), uid, firstName, lastName, tabFolders.Length, fileNumber, alert));
                                        DataSet dt = obj.CreateFolder(false, folderNameAsName, uid, firstName, lastName, tabFolders.Length, fileNumber, alert, templateName, "", "", "", "", "", "", status, "", officeId, true);
                                        folderId = Convert.ToInt32(dt.Tables[0].Rows[0]["Column1"]);

                                        WriteWindowServiceLog(folderId, tabId, uid, Action.FolderCreated.ToString());
                                        if (dt.Tables[1].Rows.Count > 0)
                                            tabsDetails = JsonConvert.DeserializeObject<List<FolderStructure>>(JsonConvert.SerializeObject(dt.Tables[1]));
                                        //Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/" + CurrentVolume + Path.DirectorySeparatorChar + folderId));
                                        FindOrCreateTab(tabFolders, tabsDetails, folderName, uid, obj, folderId);
                                    }
                                    WriteWindowServiceLog(folderId, tabId, uid, Action.FolderProcessFinished.ToString());
                                    if (Directory.GetDirectories(folderName).Length <= 0 && Directory.GetFiles(folderName).Length <= 0)
                                    {
                                        try
                                        {
                                            Directory.Delete(folderName);
                                            WriteWindowServiceLog(folderId, tabId, uid, Action.FolderDeleted.ToString(), 0, folderName);
                                        }
                                        catch (Exception ex)
                                        {
                                            WriteWindowServiceLog(folderId, tabId, uid, Action.FolderDeleteError.ToString(), 0, folderName, ex.Message);
                                        }
                                    }
                                }
                            }
                            if (files.Length == 0 && folders.Length == 0)
                            {
                                MessageBoxResult value = MessageBox.Show("There is no file in the folder : " + userName + "",
                                                                       "Alert");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteWindowServiceLog(0, 0, "", Action.UnknownException.ToString(), 0, "", ex.Message);
                System.Windows.Forms.Application.Exit();
            }
        }

        private void CopyFile(string[] files, int tabId, int folderId, string folderName, string uid)
        {
            try
            {
                string msg = string.Empty;
                if (files.Length > 0)
                {
                    foreach (string file in files)
                    {
                        msg = "File " + file + " is uploading....\n";
                        DisplayTextBoxValue(textBox1, msg);

                        if (textbox_queue.Text != "")
                            RemoveLinesContainingToken(textbox_queue, file);

                        string documentID = string.Empty;
                        documentID = ProcessFile(tabId, folderId, folderName.Substring(folderName.LastIndexOf('\\') + 1), file.Substring(file.LastIndexOf('\\') + 1), file.Substring(0, file.LastIndexOf('\\')), uid);
                        if (!string.IsNullOrEmpty(documentID) && Convert.ToInt32(documentID) > 0)
                        {
                            try
                            {
                                obj.AuditLogByDocId(Convert.ToInt32(documentID), uid, FileAction.Create.GetHashCode());
                                WriteWindowServiceLog(folderId, tabId, uid, Action.FileUploaded.ToString(), Convert.ToInt32(documentID), file.Substring(file.LastIndexOf('\\') + 1));
                                try
                                {
                                    msg = "File " + file + " has been uploaded successfully !!! \n";
                                    DisplayTextBoxValue(textBox1, msg);
                                    textBox1.AppendText(Environment.NewLine);
                                    msg = "(" + _fileSuccessCount + ")  " + file;
                                    DisplayTextBoxValue(textbox_success, msg);

                                    SuccessTabHeader.Header = "Successful transfer(" + (_fileSuccessCount - 1) + ")";
                                    textbox_success.AppendText(Environment.NewLine);
                                    _fileSuccessCount++;
                                    try
                                    {
                                        string[] splitData = file.Split(new string[] { RootPath }, StringSplitOptions.None); ;
                                        if (splitData.Length > 1)
                                        {
                                            string dest = RootPath + DeletedPath + splitData[1];
                                            if (!Directory.Exists(System.IO.Path.GetDirectoryName(dest)))
                                                Directory.CreateDirectory(System.IO.Path.GetDirectoryName(dest));
                                            try { File.Move(file, dest); }
                                            catch (Exception ex) { }
                                        }
                                    }
                                    catch (Exception ex) { }
                                    File.Delete(file);
                                    WriteWindowServiceLog(folderId, tabId, uid, Action.FileDeleted.ToString(), 0, file.Substring(file.LastIndexOf('\\') + 1));
                                }
                                catch (Exception ex)
                                {
                                    WriteWindowServiceLog(folderId, tabId, uid, Action.FileDeleteError.ToString(), 0, file.Substring(file.LastIndexOf('\\') + 1), ex.Message);
                                }
                            }
                            catch (Exception ex)
                            {
                                WriteWindowServiceLog(folderId, tabId, uid, Action.UploadError.ToString(), 0, file.Substring(file.LastIndexOf('\\') + 1), ex.Message);
                            }
                        }
                        else
                        {
                            WriteWindowServiceLog(folderId, tabId, uid, Action.UploadError.ToString(), 0, file.Substring(file.LastIndexOf('\\') + 1), "Document insert error.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteWindowServiceLog(folderId, tabId, uid, Action.UploadError.ToString(), 0, string.Empty, ex.Message);
            }
        }

        /// <summary>
        /// Uploads the file
        /// </summary>
        /// <param name="dividerIdForFile"></param>
        /// <param name="folderId"></param>
        /// <param name="folderName"></param>
        /// <param name="filename"></param>
        /// <param name="sourcePath"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        public string ProcessFile(int dividerIdForFile, int folderId, string folderName, string filename, string sourcePath, string uid)
        {
            try
            {
                if (!String.IsNullOrEmpty(folderName))
                {
                    if (Path.GetExtension(filename).ToLower() == ".pdf" || Path.GetExtension(filename).ToLower() == ".jpg" || Path.GetExtension(filename).ToLower() == ".jpeg")
                    {
                        string folderID = folderId.ToString();
                        string dividerID = Convert.ToString(dividerIdForFile);
                        string path = BasePath + Path.DirectorySeparatorChar +
                                    folderID + Path.DirectorySeparatorChar + dividerIdForFile;
                        string encodeName = HttpUtility.UrlPathEncode(filename);
                        encodeName = encodeName.Replace("%", "$");

                        string fileName = String.Concat(
                                path,
                                Path.DirectorySeparatorChar,
                                encodeName
                            );

                        fileName = String.Concat(path, Path.DirectorySeparatorChar, ChangeExtension(fileName));
                        _newFileName = Path.GetFileName(fileName);
                        encodeName = _newFileName;
                        _newFileName = HttpUtility.UrlDecode(_newFileName.Replace('$', '%').ToString());

                        int Offset = 0; // starting offset.
                        //define the chunk size
                        int ChunkSize = 5242880; // 10 mb
                        byte[] Buffer = new byte[ChunkSize];
                        long FileSize = new FileInfo(string.Concat(sourcePath, Path.DirectorySeparatorChar, filename)).Length;
                        string documentId = string.Empty;
                        FileStream objfilestream = new FileStream(string.Concat(sourcePath, Path.DirectorySeparatorChar, filename), FileMode.Open, FileAccess.Read);
                        try
                        {
                            string ChunkAppened = "";
                            try
                            {
                                _newFileName = Regex.Replace(_newFileName, "[^0-9a-zA-Z-:,_.]+", "");
                            }
                            catch (Exception ex) { }

                            objfilestream.Position = Offset;
                            int BytesRead = 0;
                            while (Offset != FileSize) // continue uploading the file chunks until offset = file size.
                            {
                                BytesRead = objfilestream.Read(Buffer, 0, ChunkSize); // read the next chunk 
                                if (BytesRead != Buffer.Length)
                                {
                                    ChunkSize = BytesRead;
                                    byte[] TrimmedBuffer = new byte[BytesRead];
                                    Array.Copy(Buffer, TrimmedBuffer, BytesRead);
                                    Buffer = TrimmedBuffer; // the trimmed buffer should become the new 'buffer'
                                }
                                String base64String = Convert.ToBase64String(Buffer);
                                ChunkAppened = obj.ProcessFile(folderId, dividerIdForFile, folderName, uid, BasePath, base64String, _newFileName, Offset);
                                if (string.IsNullOrEmpty(ChunkAppened))
                                {
                                    WriteWindowServiceLog(folderId, dividerIdForFile, uid, Action.UploadError.ToString(), 0, _newFileName, "Failed to process file");
                                    break;
                                }
                                Offset += BytesRead;
                            }
                            if (!string.IsNullOrEmpty(ChunkAppened))
                            {
                                string renameFileName = Path.GetFileName(ChunkAppened);
                                _newFileName = HttpUtility.UrlDecode(renameFileName.Replace('$', '%').ToString());
                                documentId = obj.InsertDocument(dividerIdForFile, folderId, _newFileName, renameFileName, uid);
                            }
                        }
                        catch (Exception ex)
                        {
                            WriteWindowServiceLog(folderId, dividerIdForFile, uid, Action.UploadError.ToString(), 0, _newFileName, ex.Message);
                        }
                        finally
                        {
                            objfilestream.Close();
                        }
                        return documentId.Split('#')[0].Trim();
                    }
                }
            }
            catch (Exception ex)
            {
                WriteWindowServiceLog(folderId, dividerIdForFile, uid, Action.UploadError.ToString(), 0, _newFileName, ex.Message);
                return null;
            }
            return null;
        }
        #endregion

        /// <summary>
        /// Change extension of file
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private string ChangeExtension(string path)
        {
            string renameFileName = string.Empty;
            renameFileName = Path.GetFileNameWithoutExtension(path) + Path.GetExtension(path).ToLower().Replace(".jpeg", ".jpg");
            return renameFileName;
        }

        /// <summary>
        /// Write Window service logs
        /// </summary>
        /// <param name="folderId"></param>
        /// <param name="dividerId"></param>
        /// <param name="uid"></param>
        /// <param name="action"></param>
        /// <param name="documentId"></param>
        /// <param name="fileName"></param>
        /// <param name="errorMsg"></param>
        public static void WriteWindowServiceLog(int folderId, int dividerId, string uid, string action, int documentId = 0, string fileName = "", string errorMsg = "")
        {
            try
            {
                if (!Directory.Exists("C:\\BatchUploadLog"))
                    Directory.CreateDirectory("C:\\BatchUploadLog");

                using (StreamWriter swData2 = new StreamWriter("C:\\BatchUploadLog\\BatchUploadLog.txt", true))
                {
                    swData2.WriteLine("===== Log " + DateTime.Now);
                    swData2.WriteLine("FolderID = " + folderId);
                    swData2.WriteLine("DividerId = " + dividerId);
                    swData2.WriteLine("fileName = " + fileName);
                    swData2.WriteLine("documentId = " + documentId);
                    swData2.WriteLine("uid = " + uid);
                    swData2.WriteLine("action = " + action);
                    swData2.WriteLine("error = " + errorMsg);
                    swData2.WriteLine("==============================================================");
                    swData2.Close();
                    swData2.Dispose();
                }
            }
            catch (Exception ex1)
            {
                string errorMessage = ex1.Message;
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        /// <summary>
        /// Show files of folder on page load
        /// </summary>
        private void ShowFiles()
        {
            try
            {
                var counts = Helper.GetFileNameByFolder(RootPath, textbox_queue);
                _fileCount += counts;
                if (textbox_queue.Text != "")
                {
                    QueueTabHeader.Header = "Queued Files(" + (Helper.fileCount - 1) + ")";
                }
                Helper.fileCount = 1;
            }
            catch (Exception ex) { }
        }


        /// <summary>
        /// Remove the uploaded file from Textbox
        /// </summary>
        /// <param name="tb">Textbox where file exist</param>
        /// <param name="Token">Filename to remove</param>
        private void RemoveLinesContainingToken(TextBox tb, string Token)
        {
            try
            {
                String newText = String.Empty;
                List<String> lines = tb.Text.Split(new String[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();
                int index = lines.FindIndex(str => str.Contains(Token));
                if (index != -1)
                {
                    lines.RemoveAt(index);
                    lines.ForEach(str => newText += str + Environment.NewLine);
                    textbox_queue.Text = newText;
                    _fileCount--;
                    QueueTabHeader.Header = "Queued Files(" + (_fileCount - 1) + ")";
                }
            }
            catch (Exception ex) { }
        }

        /// <summary>
        /// Set value in textbox
        /// </summary>
        /// <param name="textbox"></param>
        /// <param name="msg"></param>
        private void DisplayTextBoxValue(System.Windows.Controls.TextBox textbox, string msg)
        {
            Dispatcher.Invoke(new System.Action(() =>
            {
                textbox.Text += msg;
            }), DispatcherPriority.Background);
        }
    }
}
