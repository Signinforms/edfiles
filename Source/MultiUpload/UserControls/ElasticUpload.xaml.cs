﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MultiUpload.ServiceReference1;

namespace MultiUpload.UserControls
{
    /// <summary>
    /// Interaction logic for ElasticUpload.xaml
    /// </summary>
    public partial class ElasticUpload : UserControl
    {
        ServiceReference1.EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();

        public ElasticUpload()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            DataTable dtUsers = obj.GetAllUsers();

            ComboBoxZone.ItemsSource = dtUsers.DefaultView;
            ComboBoxZone.DisplayMemberPath = dtUsers.Columns["Name"].ToString();
            ComboBoxZone.SelectedValuePath = dtUsers.Columns["UID"].ToString();
            //BindComboBox(ComboBoxZone);
            //List<User> userList = new List<User>();
            //try
            //{
            //    DataTable dtUsers = obj.GetAllUsers();
            //    if (dtUsers != null && dtUsers.Rows.Count > 0)
            //    {
            //        foreach (DataRow dr in dtUsers.Rows)
            //        {
            //            try
            //            {
            //                userList.Add(new User() { UID = Convert.ToString(dr["UID"]), Username = Convert.ToString(dr["Name"]) });
            //            }
            //            catch (Exception ex) { }
            //        }
            //    }
            //}
            //catch (Exception ex) 
            //{
            //    string msg = ex.Message;
            //}
            //ComboBoxZone.ItemsSource = userList;
        }

        private void btnElasticUpload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                obj.GetArchiveForBatchUpload(ComboBoxZone.SelectedValue.ToString());
            }
            catch (Exception ex) 
            {
                string obj = ex.Message;
            }
        }
    }

    public class User
    {
        public string UID { get; set; }

        public string Username { get; set; }
    }
}
