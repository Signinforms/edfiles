﻿using MultiUpload.ServiceReference1;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using Aspose.Cells;
using System.Windows.Threading;
using MultiUpload.Model;
using System.Text.RegularExpressions;
//using System.Windows.Forms;
//using System.Windows.Shapes;

namespace MultiUpload.UserControls
{
    /// <summary>
    /// Interaction logic for RenameAndMoveToArchive.xaml
    /// </summary>
    public partial class RenameAndMoveToArchive : UserControl
    {
        #region Properties
        private int _fileCount = 1;
        private int _fileSuccessCount = 1;
        private string _notUploadFiles = string.Empty;
        private static string RootPath
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["RenameArchiveRootPath"]);
            }
        }

        private static string DeletedPath
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["DeletedWorkArea"]);
            }
        }
        #endregion

        public static ServiceReference1.EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();

        #region Constructor
        public RenameAndMoveToArchive()
        {
            InitializeComponent();
        }
        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ShowFile();
        }

        #region Upload
        /// <summary>
        /// Upload button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BatchUpload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                textbox_success.Text = "";
                SuccessTabHeader.Header = "Successful transfer";
                GetUserDetails();
                if (textbox_success.Text != "")
                {
                    SuccessTabHeader.Header = "Successful transfer(" + (_fileSuccessCount - 1) + ")";
                    if (textbox_queue.Text == "")
                        QueueTabHeader.Header = "Queued Files";
                    else
                    {
                        string[] filenames = _notUploadFiles.Split(',');
                        textbox_queue.Text = "";
                        _fileCount = 1;
                        if (!string.IsNullOrEmpty(_notUploadFiles))
                        {
                            foreach (string splitPath in filenames)
                            {
                                if (splitPath == "")
                                    continue;
                                else
                                {
                                    Helper.GetFileNameByFolder(splitPath, textbox_queue);
                                    _fileCount = Helper.fileCount;
                                    Helper.fileCount = 1;
                                }
                            }
                        }
                    }
                    _fileSuccessCount = 1;
                    MessageBoxResult message = MessageBox.Show("All files have been uploaded successfully",
                                                  "Success");
                }
                else
                {
                    MessageBoxResult message = MessageBox.Show("Please add files in Queue",
                                                                "Error");
                }
            }
            catch (Exception ex) { }
        }

        /// <summary>
        /// get details of user to upload
        /// </summary>
        public void GetUserDetails()
        {
            try
            {
                if (Directory.Exists(RootPath))
                {
                    string[] userFolders = Directory.GetDirectories(RootPath);
                    foreach (string user in userFolders)
                    {
                        if (!user.Contains(DeletedPath.Split('\\')[0]))
                        {
                            List<UserStructure> userDetails = JsonConvert.DeserializeObject<List<UserStructure>>(obj.GetUserIdFromUserName(user.Substring(user.LastIndexOf('\\') + 1)));
                            if (userDetails != null && userDetails.Count > 0)
                            {
                                //Get UID from UserName
                                string uid = string.Empty;
                                if (Convert.ToBoolean(Convert.ToInt32(userDetails[0].IsSubUser)))
                                    uid = userDetails[0].OfficeUID;
                                else
                                    uid = userDetails[0].UID;
                                FindDocumentsAndRename(user, uid);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
        }

        /// <summary>
        /// Find the document by excel and rename it
        /// </summary>
        /// <param name="userPath"></param>
        /// <param name="uid"></param>
        public void FindDocumentsAndRename(string userPath, string uid)
        {
            string msg = string.Empty;
            msg = "Files are getting uploaded . . . \n";
            DisplayTextBoxValue(textBox1, msg);
            textBox1.AppendText(Environment.NewLine);
            try
            {
                foreach (string folder in Directory.GetDirectories(userPath))
                {
                    string excelFile = folder + Path.DirectorySeparatorChar + Path.GetFileName(folder) + ".csv";
                    if (File.Exists(excelFile))
                    {
                        Workbook workbook = new Workbook(excelFile);
                        Worksheet ws = workbook.Worksheets[0];
                        Cells cells = ws.Cells;
                        Row header = cells.GetRow(0);
                        var rows = cells.Rows;
                        int columnCount = 0;
                        try
                        {
                            if (header != null && header.LastDataCell != null)
                                columnCount = CellsHelper.ColumnNameToIndex(header.LastDataCell.Name[0].ToString());
                        }
                        catch (Exception) { }
                        int index = 0;
                        for (int i = 1; i < rows.Count; i++)
                        {
                            var row = rows[i];
                            string fileName = row.LastDataCell != null ? Convert.ToString(row.LastDataCell.Value) : string.Empty;
                            if (!string.IsNullOrEmpty(fileName))
                            {
                                string fileNameOnly = Path.GetFileName(fileName);
                                string newFileName = string.Empty;
                                for (int c = 0; c < columnCount; c++)
                                {
                                    if (cells.GetCell(i, c) != null)
                                        newFileName += Convert.ToString(cells.GetCell(i, c).Value).Replace("/", "_").Replace("\\", "_") + ", ";
                                }
                                try
                                {
                                    newFileName = Regex.Replace(newFileName, "[^0-9a-zA-Z-:,_.]+", "");
                                }
                                catch (Exception ex) { }
                                string objectName = fileName.Substring(0, fileName.LastIndexOf('\\') + 1);
                                objectName = objectName + newFileName.Trim().Remove(newFileName.LastIndexOf(','));
                                fileName = userPath + Path.DirectorySeparatorChar + fileName;
                                if (File.Exists(fileName))
                                {
                                    string newfilename = Path.GetFileName(fileName);
                                    msg = "File " + newfilename + " is uploading....\n";
                                    DisplayTextBoxValue(textBox1, msg);

                                    if (textbox_queue.Text != "")
                                        RemoveLinesContainingToken(textbox_queue, fileName);

                                    byte[] fileData = null;
                                    try
                                    {
                                        string newObjectName = objectName.Substring(0, objectName.LastIndexOf('\\'));
                                        newObjectName = newObjectName.Substring(0, newObjectName.LastIndexOf('\\'));
                                        objectName = newObjectName + objectName.Substring(objectName.LastIndexOf('\\'));
                                        fileData = File.ReadAllBytes(fileName);
                                    }
                                    catch (Exception) { }
                                    int success = obj.MoveDocumentsToWorkArea(objectName, 1, fileData, uid);
                                    if (success == 1)
                                    {
                                        try
                                        {
                                            string[] splitData = fileName.Split(new string[] { RootPath }, StringSplitOptions.None); ;
                                            if (splitData.Length > 1)
                                            {
                                                string dest = RootPath + DeletedPath + splitData[1];
                                                if (!Directory.Exists(System.IO.Path.GetDirectoryName(dest)))
                                                    Directory.CreateDirectory(System.IO.Path.GetDirectoryName(dest));
                                                try { File.Move(fileName, dest); }
                                                catch (Exception) { }
                                            }
                                            msg = "(" + _fileSuccessCount + ")  " + fileName;
                                            DisplayTextBoxValue(textbox_success, msg);
                                            SuccessTabHeader.Header = "Successful transfer(" + (_fileSuccessCount - 1) + ")";
                                            textbox_success.AppendText(Environment.NewLine);
                                            msg = "File " + newfilename + " has been uploaded successfully !!! \n";
                                            DisplayTextBoxValue(textBox1, msg);
                                            textBox1.AppendText(Environment.NewLine);
                                            index++; _fileSuccessCount++;
                                        }
                                        catch (Exception) { }
                                    }
                                    else
                                    {
                                        msg = "Failed to upload " + fileName + " !!! \n";
                                        DisplayTextBoxValue(textBox1, msg);
                                        textBox1.AppendText(Environment.NewLine);
                                    }
                                }
                            }

                        }
                    }
                    else
                    {
                        string folderName = Path.GetFileName(folder);
                        MessageBoxResult result = MessageBox.Show("There is no Excel or CSV file for Folder \"" + folderName + "\". Are you sure, you want to proceed uploading without renaming ?",
                                                  "No Excel File Found",
                                                  MessageBoxButton.YesNo,
                                                  MessageBoxImage.Question);
                        if (result == MessageBoxResult.Yes)
                        {
                            foreach (string subFolder in Directory.GetDirectories(folder))
                            {
                                int index = 1;
                                foreach (string file in Directory.GetFiles(subFolder))
                                {
                                    byte[] fileData = null;
                                    try
                                    {
                                        fileData = File.ReadAllBytes(file);
                                    }
                                    catch (Exception) { }
                                    string fileName = Path.GetFileNameWithoutExtension(file);
                                    msg = "File " + fileName + " is being uploading....\n";
                                    DisplayTextBoxValue(textBox1, msg);

                                    if (textbox_queue.Text != "")
                                        RemoveLinesContainingToken(textbox_queue, file);

                                    string fileToUpload = folderName + Path.DirectorySeparatorChar + fileName;
                                    int success = obj.MoveDocumentsToWorkArea(fileToUpload, 1, fileData, uid);
                                    if (success == 1)
                                    {
                                        try
                                        {
                                            string[] splitData = file.Split(new string[] { RootPath }, StringSplitOptions.None); ;
                                            if (splitData.Length > 1)
                                            {
                                                string dest = RootPath + DeletedPath + splitData[1];
                                                if (!Directory.Exists(System.IO.Path.GetDirectoryName(dest)))
                                                    Directory.CreateDirectory(System.IO.Path.GetDirectoryName(dest));
                                                try { File.Move(file, dest); }
                                                catch (Exception) { }
                                            }
                                            msg = "(" + _fileSuccessCount + ")  " + file;
                                            DisplayTextBoxValue(textbox_success, msg);
                                            SuccessTabHeader.Header = "Successful transfer(" + (_fileSuccessCount - 1) + ")";
                                            textbox_success.AppendText(Environment.NewLine);

                                            msg = "File " + fileName + " has been uploaded successfully !!! \n";
                                            DisplayTextBoxValue(textBox1, msg);
                                            textBox1.AppendText(Environment.NewLine);
                                            index++; _fileSuccessCount++;
                                        }
                                        catch (Exception) { }
                                    }
                                    else
                                    {
                                        msg = "Failed to upload " + fileName + " !!! \n";
                                        DisplayTextBoxValue(textBox1, msg);
                                        textBox1.AppendText(Environment.NewLine);
                                    }
                                }
                            }
                        }
                        else
                        {
                            _notUploadFiles = _notUploadFiles + "," + folder;
                            continue;
                        }
                    }
                }
            }
            catch (Exception ex) { }
        }
        #endregion

        /// <summary>
        /// Remove the uploaded file from Textbox
        /// </summary>
        /// <param name="tb">Textbox where file exist</param>
        /// <param name="Token">Filename to remove</param>
        private void RemoveLinesContainingToken(TextBox tb, string Token)
        {
            try
            {
                String newText = String.Empty;
                List<String> lines = tb.Text.Split(new String[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList();
                int index = lines.FindIndex(str => str.Contains(Token));
                if (index != -1)
                {
                    lines.RemoveAt(index);
                    lines.ForEach(str => newText += str + Environment.NewLine);
                    textbox_queue.Text = newText;
                    _fileCount--;
                    QueueTabHeader.Header = "Queued Files(" + (_fileCount - 1) + ")";
                }
            }
            catch (Exception ex) { }
        }

        /// <summary>
        /// Show files of Rootpath in queue
        /// </summary>
        private void ShowFile()
        {
            try
            {
                Helper.GetFileNameByFolder(RootPath, textbox_queue);
                _fileCount = Helper.fileCount;
                if (textbox_queue.Text != "")
                {
                    QueueTabHeader.Header = "Queued Files(" + (Helper.fileCount - 1) + ")";
                }
                Helper.fileCount = 1;
            }
            catch (Exception ex) { }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        /// <summary>
        /// Set value in textbox
        /// </summary>
        /// <param name="textbox"></param>
        /// <param name="msg"></param>
        private void DisplayTextBoxValue(System.Windows.Controls.TextBox textbox, string msg)
        {
            Dispatcher.Invoke(new System.Action(() =>
            {
                textbox.Text += msg;
            }), DispatcherPriority.Background);
        }
    }
}
