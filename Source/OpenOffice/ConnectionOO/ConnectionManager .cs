﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using Microsoft.Win32;
using uno.util;
using unoidl.com.sun.star.bridge;
using unoidl.com.sun.star.connection;
using unoidl.com.sun.star.frame;
using unoidl.com.sun.star.lang;
using unoidl.com.sun.star.uno;

namespace ConnectionOO
{
    public class ConnectionManager
    {

        static ConnectionManager()
        {
            _pool = new Semaphore(0, MAX_CONNECTIONS_NUMBER);
        }


        #region Configuration Items

        const string OO_PATH_SUFFIX = @"bin";
        const string OO_REG_UNO_INSTALL_KEY = @"Software\OpenOffice.org\UNO\InstallPath";
        const string OO_REG_UNO_INSTALL_NAME = "OpenOffice.org 3.0";
        const string OO_REG_INSTALL_KEY = @"Software\OpenOffice.org\Layers\URE\1";//HKEY_LOCAL_MACHINE
        const string OO_REG_INSTALL_NAME = "UREINSTALLLOCATION";

        #endregion Configuration Items

        #region Properties

        private static int _numberOfConnections = 0;
        private const int MAX_CONNECTIONS_NUMBER = 5;

        public static Semaphore _pool;

        /// <summary>
        /// The number of connection attempts made.
        /// </summary>
        public static int NumberOfConnections
        {
            get { return _numberOfConnections; }
        }

        private static Hashtable _connections = new Hashtable();
        private static Stack _unused = new Stack();
        /// <summary>
        /// A registery of all the connections made.
        /// </summary>
        public static Hashtable Connections
        {
            get { return _connections; }
        }

        #endregion Properties

        #region Private Variables.

        private static bool _started = false;

        #endregion Private Variables.

        /// <summary>
        /// Creates an Connection.
        /// </summary>
        /// <returns>An Connection object.</returns>
        public static Connection CreateConnection()
        {
            lock (_connections)
            {
                Connection connection = null;

                if (_numberOfConnections >= MAX_CONNECTIONS_NUMBER)
                {
                    _pool.WaitOne();

                    object id = _unused.Pop();
                    connection = _connections[id] as Connection;
                }
                else
                {
                    StartOpenOffice();

                    string oo_UNO_Path = Reg_Retrieve_String(OO_REG_UNO_INSTALL_KEY, OO_REG_UNO_INSTALL_NAME);
                    string oo_Path_Mod = Reg_Retrieve_String(OO_REG_INSTALL_KEY, OO_REG_INSTALL_NAME);
                    oo_Path_Mod += OO_PATH_SUFFIX;

                    Environment.SetEnvironmentVariable("UNO_PATH", oo_UNO_Path);
                    Environment.SetEnvironmentVariable("PATH",
                                                       Environment.GetEnvironmentVariable("PATH") + ";" + oo_Path_Mod);

                    //Get a ComponentContext
                    unoidl.com.sun.star.uno.XComponentContext xLocalContext = uno.util.Bootstrap.bootstrap();

                    _numberOfConnections++;
                    _pool.WaitOne();

                    connection = new Connection(xLocalContext);
                    Connections.Add(connection.ID, connection);
                }
                return connection;
            }
        }

        public static string Reg_Retrieve_String(string path, string key)
        {
            RegistryKey root = Registry.LocalMachine.OpenSubKey(path);
            string objstr = root.GetValue(key) as string;

            return objstr;
        }

        /// <summary>
        public static void StartOpenOffice()
        {
        
            Process[] ps = Process.GetProcessesByName(Properties.Settings.Default.ProcessName);
            if (ps.Equals(null)|| ps.Length == 0)
            {
                Process p = Process.Start(Properties.Settings.Default.PathToOpenOffice, "-headless -nologo -norestore");
                //spent some time to start
                System.Threading.Thread.Sleep(3000);
            }
        }
    }
}
