﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using unoidl.com.sun.star.lang;

namespace ConnectionOO
{
    /// <summary>
    /// Standard XEventListener implementation,
    /// use it where ever you want be informed
    /// about objects that disposed.
    /// </summary>
    public class MyDisposeListener : unoidl.com.sun.star.lang.XEventListener
    {
        /// <summary>
        /// Delegate
        /// </summary>
        public delegate void Disposed();
        /// <summary>
        /// Event
        /// </summary>
        public static event Disposed OnDisposed;

        #region XEventListener Member

        /// <summary>
        /// Disposings the specified source.
        /// </summary>
        /// <param name="Source">The source.</param>
        public void disposing(EventObject Source)
        {
            //Forward the event to registered objects
            OnDisposed();
        }

        #endregion
    } 
}
