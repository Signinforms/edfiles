﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using unoidl.com.sun.star.lang;

namespace ConnectionOO
{
    /// <summary>
    /// Standard XTerminateListener implementation.
    /// use it when ever you want to be informed
    /// about the termination of the desktop.
    /// </summary>
    public class MyTerminateListener : unoidl.com.sun.star.frame.XTerminateListener
    {
        /// <summary>
        /// Delegate
        /// </summary>
        public delegate void Terminate();
        /// <summary>
        /// Event
        /// </summary>
        public event Terminate OnTerminate;

        #region XTerminateListener Member

        /// <summary>
        /// Send notifie about the termination of the desktop
        /// </summary>
        /// <param name="Event">The event.</param>
        public void notifyTermination(EventObject Event)
        {
            OnTerminate();
        }

        /// <summary>
        /// unused
        /// </summary>
        /// <param name="Event">The event.</param>
        public void queryTermination(EventObject Event)
        {
            //not implemented
        }

        #endregion

        #region XEventListener Member

        /// <summary>
        /// Disposings the specified source.
        /// </summary>
        /// <param name="Source">The source.</param>
        public void disposing(EventObject Source)
        {
            //not implemented
        }

        #endregion
    } 
}
