﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;

namespace OpenOfficeService.Objects
{
    /// <summary>
    /// Generic Sender class that can be used when communicating with OpenOffice Wrapper Service
    /// </summary>
    public class GenericSender
    {
        private static object _initLock = new object();
        private static bool _initialied = false;
        public static void Init(string address)
        {
            lock (_initLock)
            {
                if (!_initialied)
                {
                    TcpClientChannel channel = new TcpClientChannel("tcpOpenOfficeServiceClientChannel",
                        new BinaryClientFormatterSinkProvider());
                    ChannelServices.RegisterChannel(channel, false);

                    _receiver = (IReceiver)Activator.GetObject(typeof(IReceiver), address);

                    _initialied = true;
                }
            }
        }


        private static IReceiver _receiver;
        public static IReceiver Receiver
        {
            get { return GenericSender._receiver; }
            set { GenericSender._receiver = value; }
        }
    }
}
