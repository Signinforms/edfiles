﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OpenOfficeService.Objects
{
    /// <summary>
    /// Describes wrapped funcionality, extend this interface if you are 
    /// wrapping additional OpenOffice funcionality
    /// </summary>
    public interface IReceiver
    {
        byte[] ConvertToPDF(byte[] wordMl);
        byte[] ConvertToPDF(string source, out TimeSpan timer);
        byte[] ConvertToPDF(byte[] inbuffer, out TimeSpan timer);
        string ConvertToPDF(string source, string destination);
    }
}
