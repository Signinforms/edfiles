﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace OpenOfficeService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {

            Receiver.Init();

            //Application.Run(new TrayForm());
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new OOService() 
            };
            ServiceBase.Run(ServicesToRun);
           
        }
    }
}
