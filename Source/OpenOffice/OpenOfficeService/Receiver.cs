﻿using System;
using System.IO;
using OpenOfficeService.Objects;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;
using OpenOfficeWrapper;

namespace OpenOfficeService
{
    /// <summary>
    /// Class that is registred for Remoting usage and serves as a proxy
    /// between applications and ConvertToPDF class
    /// </summary>
    public class Receiver : MarshalByRefObject, IReceiver
    {
        public static string ReceiverName
        {
            get { return "OpenOfficeServiceReceiver"; }
        }

        private static object _initLock = new object();
        private static bool initialied = false;
        private static TcpServerChannel serverChannel;
        public static void Init()
        {
            lock (_initLock)
            {
                if (!initialied)
                {
                    serverChannel = new TcpServerChannel(
                        "tcpOpenOfficeServiceServerChannel", Properties.Settings.Default.Port);
                   
                    ChannelServices.RegisterChannel(serverChannel, false);

                    RemotingConfiguration.RegisterWellKnownServiceType
                        (typeof(Receiver), ReceiverName, WellKnownObjectMode.Singleton);

                    RemotingConfiguration.CustomErrorsMode = CustomErrorsModes.Off;
                   

                    initialied = true;
                }
            }
        }

        public static void UnInit()
        {
            lock (_initLock)
            {
                ChannelServices.UnregisterChannel(serverChannel);
                initialied = false;
            }
            
        }

        #region IReceiver Members

        public byte[] ConvertToPDF(byte[] wordMl)
        {
            return ConversionToPDF.Generate(wordMl);
        }

        public byte[] ConvertToPDF(string source, out TimeSpan timer)
        {
            return ConversionToPDF.Generate(source, out timer);
        }

        public byte[] ConvertToPDF(byte[] inbuffer, out TimeSpan timer)
        {
            return ConversionToPDF.Generate(inbuffer, out timer);
        }

        public string ConvertToPDF(string source, string destination)
        {
            return ConversionToPDF.Generate(source, destination);
        }

        #endregion
    }
}
