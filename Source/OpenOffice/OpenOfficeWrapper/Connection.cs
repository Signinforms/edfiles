﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using unoidl.com.sun.star.frame;
using unoidl.com.sun.star.lang;
using unoidl.com.sun.star.uno;

namespace OpenOfficeWrapper
{
    public class Connection : IDisposable
    {
        #region Properties

        private Guid _id;
        /// <summary>
        /// The id of the connection. A registry of the connections is kept
        /// by the connection manager. This is a key that can be
        /// used to access them.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        private int _connectionNumber;
        /// <summary>
        /// This is the number of the connection.
        /// If five connections have been made before this one,
        /// it will be number 6.
        /// </summary>
        public int ConnectionNumber
        {
            get { return _connectionNumber; }
        }


        private XMultiServiceFactory _serviceManager;
        /// <summary>
        /// This is the remote service manager for the connection.
        /// </summary>
        public XMultiServiceFactory ServiceManager
        {
            get { return _serviceManager; }
        }

        private XDesktop _desktop;
        /// <summary>
        /// This is the remote "Desktop". This desktop object
        /// hosts the frames in openoffice.
        /// </summary>
        public XDesktop Desktop
        {
            get { return _desktop; }
        }

        private bool _connected = false;
        /// <summary>
        /// This property tells us if the connection is still active.
        /// </summary>
        public bool Connected
        {
            get { return _connected; }
        }

        private MyTerminateListener _terminateListener = new MyTerminateListener();
        /// <summary>
        /// Listens for the remote OpenOffice desktop to be terminated.
        /// </summary>
        public MyTerminateListener TerminateListener
        {
            get { return _terminateListener; }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// This constructor is called by the connections manager.
        /// </summary>
        /// <param name="remoteContext">A remote context object, this should be connected to the UNO Server</param>
        public Connection(XComponentContext remoteContext)
        {
            // Initialize properties.
            _connectionNumber = ConnectionManager.NumberOfConnections;
            _id = Guid.NewGuid();

            // We then pull down the service manager. As i understand it,
            // this object is similar to our remoting agent, allowing us to create
            // remote instances of objects exposed by the UNO server.
            XMultiServiceFactory remoteFactory = remoteContext.getServiceManager() as XMultiServiceFactory;

            // Get the OpenOffice desktop object.
            XDesktop remoteDesktop = remoteFactory.createInstance("com.sun.star.frame.Desktop") as XDesktop;
           
            // Register the local terminate listener with the remote desktop.
            _terminateListener.OnTerminate += new MyTerminateListener.Terminate(this.TerminateListener_OnTerminate);
            remoteDesktop.addTerminateListener(this._terminateListener);

            MyDisposeListener.OnDisposed += new MyDisposeListener.Disposed(this.Dispose);

            ((XComponent)remoteDesktop).addEventListener(new MyDisposeListener()); 

            // Publicly expose the remoteFactory & the remoteDesktop objects.
            _serviceManager = remoteFactory;
            _desktop = remoteDesktop;
            _connected = true;
       
        }

        #endregion Constructor

        #region IDisposable Members

        public void Dispose()
        {
            // Tell the remote object we don't need to know about it anymore.
            if (_connected)
                try
                {
                    _desktop.removeTerminateListener(_terminateListener);
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
        }

        #endregion

        #region Internal Methods

        private void TerminateListener_OnTerminate()
        {
            _connected = false;

            lock (ConnectionManager.Connections)
            {
                ConnectionManager.Connections.Remove(this._id);
                ConnectionManager._pool.Release();
            }

            ConnectionManager._pool.Release();
        }

        #endregion Internal Methods
    }
}
