﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using Microsoft.Win32;
using uno.util;
using unoidl.com.sun.star.bridge;
using unoidl.com.sun.star.connection;
using unoidl.com.sun.star.frame;
using unoidl.com.sun.star.lang;
using unoidl.com.sun.star.uno;
using Exception=System.Exception;

namespace OpenOfficeWrapper
{
    public class ConnectionManager
    {

        static ConnectionManager()
        {
            _pool = new Semaphore(MAX_CONNECTIONS_NUMBER, MAX_CONNECTIONS_NUMBER);
        }


        #region Configuration Items

        const string OO_PATH_SUFFIX = @"bin";
        const string OO_REG_UNO_INSTALL_KEY = @"Software\Wow6432Node\OpenOffice.org\UNO\InstallPath";
        const string OO_REG_UNO_INSTALL_NAME = "OpenOffice.org 3.0";
        const string OO_REG_INSTALL_KEY = @"Software\Wow6432Node\OpenOffice.org\Layers\URE\1";//HKEY_LOCAL_MACHINE
        const string OO_REG_INSTALL_NAME = "UREINSTALLLOCATION";

        #endregion Configuration Items

        #region Properties

        private static int _numberOfConnections = 0;
        private const int MAX_CONNECTIONS_NUMBER = 1;

        public static Semaphore _pool;

        /// <summary>
        /// The number of connection attempts made.
        /// </summary>
        public static int NumberOfConnections
        {
            get { return _numberOfConnections; }
        }

        private static Hashtable _connections = new Hashtable();
        private static Stack _unused = new Stack();
        /// <summary>
        /// A registery of all the connections made.
        /// </summary>
        public static Hashtable Connections
        {
            get { return _connections; }
        }

        public static Stack UnStacks
        {
            get { return _unused; }
        }

        #endregion Properties

        /// <summary>
        /// Creates an Connection.
        /// </summary>
        /// <returns>An Connection object.</returns>
        public static Connection CreateConnection()
        {
            try
            {
                lock (_connections)
                {

                    //judge whether the openoffice.exe exit by manully
                    Connection connection = null;

                    if (_connections.Count >= MAX_CONNECTIONS_NUMBER)
                    {
                        Process[] ps = Process.GetProcessesByName(Properties.Settings.Default.ProcessName);
                        if (ps.Equals(null) || ps.Length == 0)
                        {
                            return RestartOpenOffice();
                        }
                        _pool.WaitOne();

                        object id = _unused.Pop();
                        connection = _connections[id] as Connection;
                    }
                    else
                    {
                        StartOpenOffice();

                        string oo_UNO_Path = Reg_Retrieve_String(OO_REG_UNO_INSTALL_KEY, OO_REG_UNO_INSTALL_NAME);
                        string oo_Path_Mod = Reg_Retrieve_String(OO_REG_INSTALL_KEY, OO_REG_INSTALL_NAME);
                        oo_Path_Mod += OO_PATH_SUFFIX;

                        Console.WriteLine(oo_UNO_Path);
                        Console.WriteLine(oo_Path_Mod);

                        Environment.SetEnvironmentVariable("UNO_PATH", oo_UNO_Path);
                        Environment.SetEnvironmentVariable("PATH",
                                                           Environment.GetEnvironmentVariable("PATH") + ";" +
                                                           oo_Path_Mod);

                        //Get a ComponentContext
                        unoidl.com.sun.star.uno.XComponentContext xLocalContext = uno.util.Bootstrap.bootstrap();

                        _pool.WaitOne();

                        connection = new Connection(xLocalContext);
                        Connections.Add(connection.ID, connection);
                    }
                    return connection;
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp);
                return null;
            }
        }

        protected static Connection RestartOpenOffice()
        {
            _connections.Clear();
            _unused.Clear();
            _pool.Release(MAX_CONNECTIONS_NUMBER);

            Connection connection = null;

            StartOpenOffice();

            string oo_UNO_Path = Reg_Retrieve_String(OO_REG_UNO_INSTALL_KEY, OO_REG_UNO_INSTALL_NAME);
            string oo_Path_Mod = Reg_Retrieve_String(OO_REG_INSTALL_KEY, OO_REG_INSTALL_NAME);
            oo_Path_Mod += OO_PATH_SUFFIX;

            _pool.WaitOne();

            Environment.SetEnvironmentVariable("UNO_PATH", oo_UNO_Path);
            Environment.SetEnvironmentVariable("PATH",
                                               Environment.GetEnvironmentVariable("PATH") + ";" + oo_Path_Mod);

            //Get a ComponentContext
            unoidl.com.sun.star.uno.XComponentContext xLocalContext = uno.util.Bootstrap.bootstrap();

            connection = new Connection(xLocalContext);
            Connections.Add(connection.ID, connection);

            return connection;
        }

        public static string Reg_Retrieve_String(string path, string key)
        {
            RegistryKey root = Registry.LocalMachine.OpenSubKey(path);
            string objstr = root.GetValue(key) as string;

            return objstr;
        }

        /// <summary>
        public static void StartOpenOffice()
        {
        
            Process[] ps = Process.GetProcessesByName(Properties.Settings.Default.ProcessName);
            if (ps.Equals(null)|| ps.Length == 0)
            {
                Process p = Process.Start(Properties.Settings.Default.PathToOpenOffice, "-headless -nologo -norestore");
                //spent some time to start
                System.Threading.Thread.Sleep(3000);
            }
        }
    }
}
