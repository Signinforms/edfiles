﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using Microsoft.Win32;
using uno;
using unoidl.com.sun.star.beans;
using unoidl.com.sun.star.frame;
using unoidl.com.sun.star.io;
using unoidl.com.sun.star.lang;
using unoidl.com.sun.star.uno;
using unoidl.com.sun.star.bridge;
using Exception = System.Exception;

namespace OpenOfficeWrapper
{
    public class ConversionToPDF
    {
      
        // For thread safety
        private static Mutex _openOfficeLock = new Mutex(false, "OpenOfficeMutexLock-MiloradCavic");

        /// <summary>
        /// Converts document to PDF
        /// </summary>
        /// <param name="wordMl">WordML document in binary form</param>
        /// <returns>PDF in binary form</returns>
        public static byte[] Generate(byte[] wordMl)
        {
            string tempDirectory = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData).TrimEnd('\\');

            if (!Directory.Exists(tempDirectory))
                Directory.CreateDirectory(tempDirectory);

            Random r = new Random();
            string sourcePath = string.Format("{0}\\gnr-source-{1}-{2}.xml",
                tempDirectory, r.Next(), DateTime.Now.Ticks);
            string destinationPath = string.Format("{0}\\gnr-destination-{1}-{2}.pdf",
                tempDirectory, r.Next(), DateTime.Now.Ticks);

            File.WriteAllBytes(sourcePath, wordMl);

            Generate(sourcePath, destinationPath);

            byte[] resulting = File.ReadAllBytes(destinationPath);

            File.Delete(sourcePath);
            File.Delete(destinationPath);

            return resulting;
        }

        /// <summary>
        /// Converts document to PDF
        /// </summary>
        /// <param name="sourcePath">Path to document to convert(e.g: C:\test.doc)</param>
        /// <returns>PDF in binary form</returns>
        public static byte[] Generate(string sourcePath, out TimeSpan timer)
        {
            string tempDirectory = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData).TrimEnd('\\');

            if (!Directory.Exists(tempDirectory))
                Directory.CreateDirectory(tempDirectory);

            Random r = new Random();
            string destinationPath = string.Format("{0}\\gnr-destination-{1}-{2}.pdf",
                tempDirectory, r.Next(), DateTime.Now.Ticks);

            GenerateInPool(sourcePath, destinationPath, out timer);

            byte[] resulting = File.ReadAllBytes(destinationPath);

            //File.Delete(destinationPath);

            return resulting;
        }

        /// <summary>
        /// Converts document to PDF
        /// </summary>
        string tempDirectory = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData).TrimEnd('\\');
        /// <param name="">Path to document to convert(e.g: C:\test.doc)</param>
        /// <returns>PDF in binary form</returns>
        public static byte[] Generate(byte[] inData, out TimeSpan timer)
        {
            return GenerateInPool(WriteFileToDBByStream(inData), out timer);
            //return GenerateInPool(inData, out timer);
        }

        public static Stream WriteFileToDBByStream(byte[] streamBytes)  
        {  
             MemoryStream memoryStream = new MemoryStream(streamBytes);  

             return memoryStream;
     
        } 

        /// <summary>
        /// Converts document to PDF
        /// </summary>
        /// <param name="sourcePath">Path to document to convert(e.g: C:\test.doc)</param>
        /// <param name="destinationPath">Path on which to save PDF (e.g: C:\test.pdf)</param>
        /// <returns>Path to destination file if operation is successful, or Exception text if it is not</returns>
        public static string Generate(string sourcePath, string destinationPath)
        {
            TimeSpan tm;
            return GenerateInPool(sourcePath, destinationPath, out tm);
        }

        /// <summary>
        /// Converts document to PDF
        /// </summary>
        /// <param name="sourcePath">Path to document to convert(e.g: C:\test.doc)</param>
        /// <param name="destinationPath">Path on which to save PDF (e.g: C:\test.pdf)</param>
        /// <returns>Path to destination file if operation is successful, or Exception text if it is not</returns>
        public static string GenerateInPool(string sourcePath, string destinationPath, out TimeSpan timer)
        {
            try
            {
                sourcePath = PathConverter(sourcePath);
                destinationPath = PathConverter(destinationPath);

                Connection connection = ConnectionManager.CreateConnection();


                Stopwatch stopw = new Stopwatch();
                
                //Get a CompontLoader
                XComponentLoader aLoader = (XComponentLoader)connection.Desktop;
                stopw.Start();
                //Load the sourcefile
                XComponent xComponent = aLoader.loadComponentFromURL(sourcePath, "_blank", 0,
                                                                     Properties);
                stopw.Stop();
                timer = stopw.Elapsed;
                saveDocument(xComponent, destinationPath);

                xComponent.dispose();

                ConnectionManager.UnStacks.Push(connection.ID);
                ConnectionManager._pool.Release();
                
                return destinationPath;
            }
            catch (Exception ex)
            {
                timer = new TimeSpan();
                return ex.ToString();
            }
        }

        public static byte[] GenerateInPool(Stream source, string destinationPath, out TimeSpan timer)
        {
            try
            {
                //destinationPath = PathConverter(destinationPath);

                Connection connection = ConnectionManager.CreateConnection();


                Stopwatch stopw = new Stopwatch();
                stopw.Start();
                //Get a CompontLoader
                XComponentLoader aLoader = (XComponentLoader)connection.Desktop;

                //Load the sourcefile
                XComponent xComponent = aLoader.loadComponentFromURL("private:stream", "_blank", 0,
                                                                     GetInputProperties(source));
                
                byte[] data = saveDocument(xComponent);

                xComponent.dispose();
                stopw.Stop();
                timer = stopw.Elapsed;

                ConnectionManager.UnStacks.Push(connection.ID);
                ConnectionManager._pool.Release();

                return data;
            }
            catch (Exception ex)
            {
                timer = new TimeSpan();
                return null;
            }
        }

        public static byte[] GenerateInPool(Stream source, out TimeSpan timer)
        {
            XComponent xComponent = null;

            try
            {
                Connection connection = ConnectionManager.CreateConnection();

                Stopwatch stopw = new Stopwatch();
                stopw.Start();

                //Get a CompontLoader
                XComponentLoader aLoader = (XComponentLoader)connection.Desktop;

                //Load the sourcefile
                xComponent = aLoader.loadComponentFromURL("private:stream", "_blank", 0,
                                                                     GetInputProperties(source));

                byte[] data = saveDocument(xComponent);

                xComponent.dispose();
                stopw.Stop();
                timer = stopw.Elapsed;

                ConnectionManager.UnStacks.Push(connection.ID);
                ConnectionManager._pool.Release();

                return data;
            }
            catch (Exception ex)
            {
                if (xComponent!=null)
                    xComponent.dispose();
                timer = new TimeSpan();
                return null;

            }
        }

        public static byte[] GenerateInPool(byte[] source, out TimeSpan timer)
        {
            XComponent xComponent = null;

            try
            {
                Connection connection = ConnectionManager.CreateConnection();

                Stopwatch stopw = new Stopwatch();
                stopw.Start();

                //Get a CompontLoader
                XComponentLoader aLoader = (XComponentLoader)connection.Desktop;

                //Load the sourcefile
                xComponent = aLoader.loadComponentFromURL("private:stream", "_blank", 0,
                                                                     GetInputProperties(source));

                byte[] data = saveDocument(xComponent);

                xComponent.dispose();
                stopw.Stop();
                timer = stopw.Elapsed;

                ConnectionManager.UnStacks.Push(connection.ID);
                ConnectionManager._pool.Release();

                return data;
            }
            catch (Exception ex)
            {
                if (xComponent != null)
                    xComponent.dispose();
                timer = new TimeSpan();
                return null;

            }
        }

        public static unoidl.com.sun.star.beans.PropertyValue[] GetInputProperties(Stream source)
        {
            //unoidl.com.sun.star.beans.PropertyValue[] props = new PropertyValue[3];
            unoidl.com.sun.star.beans.PropertyValue[] props = new PropertyValue[2];
            props[0] = new unoidl.com.sun.star.beans.PropertyValue();
            props[0].Name = "Hidden";
            props[0].Value = new uno.Any(true);

            props[1] = new unoidl.com.sun.star.beans.PropertyValue();
            props[1].Name = "InputStream";
            props[1].Value = new uno.Any(typeof(unoidl.com.sun.star.io.XInputStream),new XInputStreamWrapper(source));

            //props[2] = new unoidl.com.sun.star.beans.PropertyValue();
            //props[2].Name = "FilterName";
            //props[2].Value = new Any("MS Word 2007 XML");

            return props;
        }

        public static unoidl.com.sun.star.beans.PropertyValue[] GetInputProperties(byte[] source)
        {
            //unoidl.com.sun.star.beans.PropertyValue[] props = new PropertyValue[3];
            unoidl.com.sun.star.beans.PropertyValue[] props = new PropertyValue[2];
            props[0] = new unoidl.com.sun.star.beans.PropertyValue();
            props[0].Name = "Hidden";
            props[0].Value = new uno.Any(true);

            props[1] = new unoidl.com.sun.star.beans.PropertyValue();
            props[1].Name = "InputStream";
            props[1].Value = new uno.Any(typeof(unoidl.com.sun.star.io.XInputStream), new XInputByteWrapper(source));

            //props[2] = new unoidl.com.sun.star.beans.PropertyValue();
            //props[2].Name = "FilterName";
            //props[2].Value = new Any("MS Word 2007 XML");

            return props;
        }

        public static unoidl.com.sun.star.beans.PropertyValue[] GetOutputProperties(Stream ostream)
        {
            unoidl.com.sun.star.beans.PropertyValue[] props = new PropertyValue[3];
            props[0] = new unoidl.com.sun.star.beans.PropertyValue();
            props[0].Name = "FilterName";
            props[0].Value = new uno.Any("writer_pdf_Export");

            props[1] = new unoidl.com.sun.star.beans.PropertyValue();
            props[1].Name = "Overwrite";
            props[1].Value = new uno.Any(true); 

            props[2] = new unoidl.com.sun.star.beans.PropertyValue();
            props[2].Name = "OutputStream";
            props[2].Value = new uno.Any(typeof(unoidl.com.sun.star.io.XOutputStream), new XOutputStreamWrapper(ostream));

            return props;
        }

        public static unoidl.com.sun.star.beans.PropertyValue[] GetOutputProperties(XOutputStream ostream)
        {
            unoidl.com.sun.star.beans.PropertyValue[] props = new PropertyValue[3];
            props[0] = new unoidl.com.sun.star.beans.PropertyValue();
            props[0].Name = "FilterName";
            props[0].Value = new uno.Any("writer_pdf_Export");

            props[1] = new unoidl.com.sun.star.beans.PropertyValue();
            props[1].Name = "Overwrite";
            props[1].Value = new uno.Any(true);

            props[2] = new unoidl.com.sun.star.beans.PropertyValue();
            props[2].Name = "OutputStream";
            props[2].Value = new uno.Any(typeof(unoidl.com.sun.star.io.XOutputStream), ostream);

            return props;
        }

        public static unoidl.com.sun.star.beans.PropertyValue[] Properties
        {
            get
            {
                unoidl.com.sun.star.beans.PropertyValue[] properties = new PropertyValue[2];
                PropertyValue pro1 = new PropertyValue();
                pro1.Name = "Hidden";
                pro1.Value = new uno.Any(true);

                PropertyValue pro2 = new PropertyValue();
                pro2.Name = "Overwrite";
                pro2.Value = new uno.Any(true);

                properties[0] = pro1;
                properties[1] = pro2;

                return properties;
            }
        }

        public static string Reg_Retrieve_String(string path, string key)
        {
            RegistryKey root = Registry.LocalMachine.OpenSubKey(path);
            string objstr = root.GetValue(key) as string;

            return objstr;
        }


        /// <summary>
        /// Saves the document.
        /// </summary>
        /// <param name="xComponent">The x component.</param>
        /// <param name="fileName">Name of the file.</param>
        private static void saveDocument(XComponent xComponent, string fileName)
        {
            unoidl.com.sun.star.beans.PropertyValue[] propertyValue =
               new unoidl.com.sun.star.beans.PropertyValue[1];

            propertyValue[0] = new unoidl.com.sun.star.beans.PropertyValue();
            propertyValue[0].Name = "FilterName";
            propertyValue[0].Value = new uno.Any("writer_pdf_Export");

            XOutputStreamWrapper xwrapper = new XOutputStreamWrapper(new MemoryStream());

            ((XStorable)xComponent).storeToURL(fileName, propertyValue);
        }

        /// <summary>
        /// Saves the document.
        /// </summary>
        /// <param name="xComponent">The x component.</param>
        private static byte[] saveDocument(XComponent xComponent)
        {
            XOutputStreamWrapper xwrapper = new XOutputStreamWrapper(new MemoryStream());

            ((XStorable)xComponent).storeToURL("private:stream", GetOutputProperties(xwrapper));
            return xwrapper.outData;
        }

        /// <summary>
        /// Convert into OO file format
        /// </summary>
        /// <param name="file">The file.</param>
        /// <returns>The converted file</returns>
        private static string PathConverter(string file)
        {
            try
            {
                file = file.Replace(@"\", "/");

                return "file:///" + file;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
    }
}
