﻿using System;
using System.Diagnostics;

namespace OpenOfficeWrapper
{
    /// <summary>
    /// Controls life-cycle of OpenOffice process
    /// </summary>
    public class OfficeController : IDisposable
    {
        private static DateTime _lastUsedTime = DateTime.MaxValue;

        private static System.Timers.Timer _checkTimer = null;

        /// <summary>
        /// Start OpenOffice process if needed
        /// </summary>
        public static void StartOpenOffice()
        {
            _lastUsedTime = DateTime.Now;
            initCheckTimer();

            Process[] ps = Process.GetProcessesByName(Properties.Settings.Default.ProcessName);
            if (ps == null || ps.Length == 0)
            {
                Process p = Process.Start(Properties.Settings.Default.PathToOpenOffice, "-headless -nologo -norestore");
                //spent some time to start
                System.Threading.Thread.Sleep(3000);
            }
        }

        /// <summary>
        /// Initializes timer that checks for usage of OpenOffice process
        /// </summary>
        private static void initCheckTimer()
        {
            if (_checkTimer == null)
            {
                _checkTimer = new System.Timers.Timer(Properties.Settings.Default.CheckIntervalInSeconds * 1000);
                _checkTimer.Elapsed += new System.Timers.ElapsedEventHandler(_checkTimer_Elapsed);
                _checkTimer.Start();
            }
        }

        /// <summary>
        /// Performs checking of OpenOffice usage, kills it if not used
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void _checkTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Process[] ps = Process.GetProcessesByName(Properties.Settings.Default.ProcessName);
            double secondsIdle = DateTime.Now.Subtract(_lastUsedTime).TotalSeconds;
            if (ps != null && ps.Length > 0 && secondsIdle > Properties.Settings.Default.SecondsIdleAllowed)
            {
                ps[0].Kill();
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            // stop timer
            _checkTimer.Stop();

            // stop soffice process
            Process[] ps = Process.GetProcessesByName(Properties.Settings.Default.ProcessName);
            if (ps != null && ps.Length > 0)
                ps[0].Kill();
            
        }

        #endregion
    }
}
