﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenOfficeWrapper
{
    public class XInputByteWrapper : unoidl.com.sun.star.io.XInputStream, unoidl.com.sun.star.io.XSeekable
    {
        private byte[] _indata;
        private long _postion=0;

        public XInputByteWrapper(byte[] data)
        {
            this._indata = data;
        }

        // XInputStream interface
        public int readBytes(out byte[] data, int length)
        {
            int remaining = (int)(_indata.Length - _postion);
            int l = length > remaining ? remaining : length;
            data = new byte[length];
            Array.Copy((_indata),0,(data),0,l);
            _postion += l;
            return l;
          
        }

        public int readSomeBytes(out byte[] data, int length)
        {
            int remaining = (int)(_indata.Length - _postion);
            int l = length > remaining ? remaining : length;
            data = new byte[length];
            Array.Copy((_indata), 0, (data), 0, l);
            _postion += l;
            return l;
        }

        public void skipBytes(int nToSkip)
        {
            _postion += nToSkip;
            _postion = _postion > _indata.Length ? _indata.Length-1 : _postion;
        }

        public int available()
        {
            return (int)(_indata.Length - _postion);
        }

        public void closeInput()
        {
            _indata = null;
        }

        // XSeekable interface
        public long getPosition()
        {
            return _postion;
        }

        public long getLength()
        {
            return _indata.Length;
        }

        public void seek(long pos)
        {
            _postion = pos;
        }
    } 
}
