﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OpenOfficeWrapper
{
    public class XOutputStreamWrapper : unoidl.com.sun.star.io.XOutputStream
    {
        private System.IO.Stream ostream;
        public byte[] outData;

        public XOutputStreamWrapper(System.IO.Stream ostream)
        {
            this.ostream = ostream;
        }

        public void closeOutput()
        {
            outData = ((MemoryStream) ostream).GetBuffer();
            ostream.Close();
        }

        public void flush()
        {
            ostream.Flush();
        }

        public void writeBytes(byte[] b)
        {
            ostream.Write(b, 0, b.Length);
        }
    } 
}
