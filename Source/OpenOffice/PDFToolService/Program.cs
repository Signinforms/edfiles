﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;
using System.Windows.Forms;

namespace PDFToolService
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        static void Main()
        {
            Receiver.Init();

            //Application.Run(new TrayForm());
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new STService() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
