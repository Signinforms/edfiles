﻿using System;
using System.Collections;
using System.IO;
using SwfToolService.Objects;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting;
using SwfToolService.Objects;
using SwfToolWrapper;

namespace PDFToolService
{
    /// <summary>
    /// Class that is registred for Remoting usage and serves as a proxy
    /// between applications and ConvertToPDF class
    /// </summary>
    public class Receiver : MarshalByRefObject, IReceiver
    {
        public static string ReceiverName
        {
            get { return "PDFToolsServiceReceiver"; }
        }

        private static object _initLock = new object();
        private static bool initialied = false;
        public static void Init()
        {
            lock (_initLock)
            {
                if (!initialied)
                {
                    TcpServerChannel serverChannel = new TcpServerChannel(
                        "tcpOpenPDFServiceClientChannel", Properties.Settings.Default.Port);
                   
                    ChannelServices.RegisterChannel(serverChannel, false);

                    RemotingConfiguration.RegisterWellKnownServiceType
                        (typeof(Receiver), ReceiverName, WellKnownObjectMode.Singleton);

                    RemotingConfiguration.CustomErrorsMode = CustomErrorsModes.Off;
                   

                    initialied = true;
                }
            }
        }

        public string ConvertToSWF(byte[] pdfdata,string destination, int startindex, int count)
        {
            return ConversionToSwf.Generate(pdfdata, destination, startindex, count);
        }

        public ArrayList ConvertToSWF(string source, int startindex, int count)
        {
            throw new System.NotImplementedException();
        }

        public string ConvertToSWF(string source, string destination)
        {
            return ConversionToSwf.Generate(source, destination);
        }

        public string ConvertToSWF(byte[] pdfdata, string destination)
        {
            throw new System.NotImplementedException();
        }

        public string ConvertJpeg2SWF(string source, string destination)
        {
            return ConversionToSwf.Generate4Jpeg(source, destination);
        }

        public string ConvertPng2SWF(string source, string destination)
        {
            return ConversionToSwf.Generate4Png(source, destination);
        }
    }
}
