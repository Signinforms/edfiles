﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="PDFWeb._Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>PDF Test</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Button ID="GiveMePDFButton" runat="server" onclick="GiveMePDFButton_Click" 
            Text="Give me PDF from WordML!" />
            
        <br />
        <br />
            
        <asp:Button ID="ExcelToPDFButton" runat="server" onclick="ExcelToPDFButton_Click" 
            Text="Give me PDF from Excel!" />
             <br />
        <asp:Button ID="Button1" runat="server" onclick="ExcelToPDFButton1_Click" 
            Text="Give me PDF from Excel!" />
    </div>
    </form>
</body>
</html>
