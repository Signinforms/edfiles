﻿using System;
using System.IO;
using System.Configuration;

namespace PDFWeb
{
    public partial class _Default : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void GiveMePDFButton_Click(object sender, EventArgs e)
        {
            // Initialize Receiver in GenericSender
            OpenOfficeService.Objects.GenericSender.Init(
                "tcp://localhost:6543/OpenOfficeServiceReceiver");

            // Translate path and load up file in byte array, convert it
            string source = Server.MapPath("~/SomeWordML.xml");
            byte[] wordML = File.ReadAllBytes(source);

            byte[] result = OpenOfficeService.Objects.GenericSender.Receiver.ConvertToPDF(wordML);

            // Write response to client
            Response.AddHeader("content-type", "application/pdf");
            Response.AddHeader("Content-Disposition", "attachment; filename=result.pdf");

            Response.BinaryWrite(result);
        }

        protected void ExcelToPDFButton_Click(object sender, EventArgs e)
        {
            // Almost same as WordML to PDF
            OpenOfficeService.Objects.GenericSender.Init(
                "tcp://localhost:6543/OpenOfficeServiceReceiver");

            //string source = Server.MapPath("~/ExcelSheet.xls");
            string source = Server.MapPath("~/efilefolderppt.ppt");
            TimeSpan ts;
            byte[] result = OpenOfficeService.Objects.GenericSender.Receiver.ConvertToPDF(source, out ts);

            Response.AddHeader("content-type", "application/pdf");
            Response.AddHeader("Content-Disposition", "attachment; filename=result.pdf");

            Response.BinaryWrite(result);
        }

        protected void ExcelToPDFButton1_Click(object sender, EventArgs e)
        {
            // Almost same as WordML to PDF
            OpenOfficeService.Objects.GenericSender.Init(
                "tcp://localhost:6543/OpenOfficeServiceReceiver");

            //string source = Server.MapPath("~/ExcelSheet.xls");
            string source = Server.MapPath("~/面试题目.doc");

            TimeSpan ts;

            byte[] result = OpenOfficeService.Objects.GenericSender.Receiver.ConvertToPDF(source,out ts);

            Response.AddHeader("content-type", "application/pdf");
            Response.AddHeader("Content-Disposition", "attachment; filename=result.pdf");

            Response.BinaryWrite(result);
        }
    }
}
