﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Remoting.Channels;
using WELog;

namespace SwfToolService.Objects
{
    /// <summary>
    /// Generic Sender class that can be used when communicating with OpenOffice Wrapper Service
    /// </summary>
    public class GenericSender
    {
        private static object _initLock = new object();
        private static bool _initialied = false;
        public static void Init(string address)
        {
            lock (_initLock)
            {
                if (!_initialied)
                {
                    try
                    {
                        TcpClientChannel channel = new TcpClientChannel("tcpOpenSwfServiceClientChannel",
                        new BinaryClientFormatterSinkProvider());
                        ChannelServices.RegisterChannel(channel, false);

                        _receiver = (IReceiver)Activator.GetObject(typeof(IReceiver), address);

                        _initialied = true;
                    }
                    catch (Exception exp)
                    {
                        
                        WELog.ClsEventLog objClsEventLog = new ClsEventLog();
                        objClsEventLog.WriteToEventLog("MyEvent", "SwfToolService.Objects", exp.Message);
                    }

                    
                }
            }
        }


        private static IReceiver _receiver;
        public static IReceiver Receiver
        {
            get { return GenericSender._receiver; }
            set { GenericSender._receiver = value; }
        }
    }
}
