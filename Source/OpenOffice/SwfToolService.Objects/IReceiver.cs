﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SwfToolService.Objects
{
    /// <summary>
    /// Describes wrapped funcionality, extend this interface if you are 
    /// wrapping additional OpenOffice funcionality
    /// </summary>
    public interface IReceiver
    {
        string ConvertToSWF(byte[] pdfdata,string destination, int startindex, int count);
        ArrayList ConvertToSWF(string source, int startindex, int count);
        string ConvertToSWF(string source, string destination);
        string ConvertToSWF(byte[] pdfdata, string destination);

        string ConvertJpeg2SWF(string source, string destination);
        string ConvertPng2SWF(string source, string destination);

    }
}
