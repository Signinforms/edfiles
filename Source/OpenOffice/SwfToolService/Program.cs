﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace SwfToolService
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        static void Main(string[] args)
        {
            //Added self installation for service, so i note this section
            /*Receiver.Init();

            ////Application.Run(new TrayForm());
            //ServiceBase[] ServicesToRun;
            //ServicesToRun = new ServiceBase[] 
            //{ 
            //    new STService() 
            //};
            //ServiceBase.Run(ServicesToRun); */

            //Process currentProcess = Process.GetCurrentProcess();
            //currentProcess.PriorityClass = ProcessPriorityClass.Normal;

            if (args == null || args.Length == 0)
            {
                Receiver.Init();

                STService service = new STService();

                if (Environment.UserInteractive)
                {
                    service.Start();
                    Console.WriteLine("Servie Started. Press any key to exit.");
                    Console.ReadLine();

                    service.StopE();

                    Thread.Sleep(1000);

                    Environment.Exit(0);
                }
                else
                {
                    ServiceBase[] ServicesToRun;
                    ServicesToRun = new ServiceBase[] { service };

                    ServiceBase.Run(ServicesToRun);
                }
            }
            else
            {
                if (args.Length == 1 && args[0].Length > 1)
                {
                    if (args[0].StartsWith("-") || args[0].StartsWith("/"))
                    {
                        switch (args[0].Substring(1).ToLower())
                        {
                            case "i":
                            case "install":
                                ServiceSelfInstaller.Install();
                                break;
                            case "u":
                            case "uninstall":
                                ServiceSelfInstaller.Uninstall();
                                break;
                        }
                    }
                }
            }
        }
    }
}
