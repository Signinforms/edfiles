﻿using System.IO;
using System.Reflection;
using System.Configuration.Install;

namespace SwfToolService
{
    static class ServiceSelfInstaller
    {
        static readonly string exePath = Assembly.GetExecutingAssembly().Location;

        public static bool Install()
        {
            try
            {
                ManagedInstallerClass.InstallHelper(new string[] { exePath });
            }
            catch
            {
                return false;
            }

            return true;
        }

        public static bool Uninstall()
        {
            try
            {
                ManagedInstallerClass.InstallHelper(new string[] { "/u", exePath });
            }
            catch
            {
                return false;
            }

            return true;
        }

    }
}
