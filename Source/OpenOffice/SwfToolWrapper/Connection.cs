﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using WELog;

namespace SwfToolWrapper
{
    public class Connection : IDisposable
    {
        #region Properties

        private Guid _id;
        /// <summary>
        /// The id of the connection. A registry of the connections is kept
        /// by the connection manager. This is a key that can be
        /// used to access them.
        /// </summary>
        public Guid ID
        {
            get { return _id; }
        }

        private int _connectionNumber;
        /// <summary>
        /// This is the number of the connection.
        /// If five connections have been made before this one,
        /// it will be number 6.
        /// </summary>
        public int ConnectionNumber
        {
            get { return _connectionNumber; }
        }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// This constructor is called by the connections manager.
        /// </summary>
        public Connection()
        {
            // Initialize properties.
            _connectionNumber = ConnectionManager.NumberOfConnections;
            _id = Guid.NewGuid();

        }

        #endregion Constructor

        #region IDisposable Members

        public void Dispose()
        {
            
        }

        #endregion

        public string Generate4Jpeg(string sourcePath, string destinationPath)
        {
            Process swfprocess = new Process();

            swfprocess.StartInfo.UseShellExecute = true;
            swfprocess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            swfprocess.StartInfo.FileName = Properties.Settings.Default.JpegProcess;
            swfprocess.StartInfo.WorkingDirectory = Properties.Settings.Default.PathToSwfTools;
            string args = string.Format("-o {0} {1} -T 9", destinationPath, sourcePath);
            swfprocess.StartInfo.Arguments = args;
            try
            {
                swfprocess.Start();
                swfprocess.WaitForExit();
                swfprocess.Close();

                return "true";
            }
            catch (Exception exp)
            {
                WELog.ClsEventLog objClsEventLog = new ClsEventLog();
                objClsEventLog.WriteToEventLog("MyEvent", "SwfToolWrapper", exp.Message);
                return "false";
            }
            finally
            {
                ConnectionManager.UnStacks.Push(this.ID);
                ConnectionManager._pool.Release();
            }
        }


        public string Generate4Png(string sourcePath, string destinationPath)
        {
            Process swfprocess = new Process();

            swfprocess.StartInfo.UseShellExecute = true;
            swfprocess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            swfprocess.StartInfo.FileName = Properties.Settings.Default.PngProcess;
            swfprocess.StartInfo.WorkingDirectory = Properties.Settings.Default.PathToSwfTools;
            string args = string.Format("-o {0} {1} -T 9", destinationPath, sourcePath);
            swfprocess.StartInfo.Arguments = args;
            try
            {
                swfprocess.Start();
                swfprocess.WaitForExit();
                swfprocess.Close();

                return "true";
            }
            catch (Exception exp)
            {
                return "false";
            }
            finally
            {
                ConnectionManager.UnStacks.Push(this.ID);
                ConnectionManager._pool.Release();
            }
        }

        public string Generate(string sourcePath, string destinationPath)
        {
            Process swfprocess = new Process();
            //swfprocess.EnableRaisingEvents = true;
            swfprocess.StartInfo.UseShellExecute = true;
            swfprocess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            //swfprocess.Exited += new EventHandler(swfprocess_Exited);
            //swfprocess.ErrorDataReceived += new DataReceivedEventHandler(swfprocess_ErrorDataReceived);

            swfprocess.StartInfo.FileName = Properties.Settings.Default.ProcessName;
            swfprocess.StartInfo.WorkingDirectory = Properties.Settings.Default.PathToSwfTools;
            string args = string.Format("-o {0} {1} -t -T 9 -s internallinkfunction -s externallinkfunction -f", destinationPath, sourcePath);
            swfprocess.StartInfo.Arguments = args;
            //swfprocess.StartInfo.Arguments = "-o reader_overview.swf reader_overview.pdf -t -T 9";
            try
            {
                swfprocess.Start();
                swfprocess.WaitForExit();
                swfprocess.Close();

                return "true";
            }
            catch (Exception exp)
            {
                return "false";
            }
            finally
            {
                ConnectionManager.UnStacks.Push(this.ID);
                ConnectionManager._pool.Release();
            }
        }

        public string Generate(byte[] pdfdata,string destinationPath, int startindex, int count)
        {
            string path = Properties.Settings.Default.TempPath;
            string uidname = Path.GetRandomFileName()+".pdf";
            string filename = string.Concat(path, Path.DirectorySeparatorChar, uidname);

            FileStream fs = null;
            try
            {
                fs = new FileStream(filename, FileMode.Create, FileAccess.Write);
                fs.Write(pdfdata, 0, pdfdata.Length);
            }catch
            {
                return "false";
            }
            finally
            {
                if (fs!=null)
                {
                    fs.Close();
                    fs = null;
                }
                
            }

            Process swfprocess = new Process();
         
            swfprocess.StartInfo.UseShellExecute = false;
            swfprocess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            swfprocess.StartInfo.FileName = Properties.Settings.Default.PathToSwfTools;
            string args = string.Format("-o \"{0}\" \"{1}\" -t -T 9 -p {2} -s internallinkfunction -s externallinkfunction -f", filename, destinationPath,
                startindex);
            swfprocess.StartInfo.Arguments = args;

            try
            {
                swfprocess.Start();
                swfprocess.WaitForExit();
                swfprocess.Close();

                return "true";
            }
            catch (Exception exp)
            {
                return "false";
            }
            finally
            {
                ConnectionManager.UnStacks.Push(this.ID);
                ConnectionManager._pool.Release();
            }
            
        }

        public string Generate(string sourcePath, string destinationPath,int startpage,int endpage)
        {
            Process swfprocess = new Process();
            //swfprocess.EnableRaisingEvents = true;
            swfprocess.StartInfo.UseShellExecute = false;
            swfprocess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;

            //swfprocess.Exited += new EventHandler(swfprocess_Exited);
            //swfprocess.ErrorDataReceived += new DataReceivedEventHandler(swfprocess_ErrorDataReceived);

            swfprocess.StartInfo.FileName = Properties.Settings.Default.PathToSwfTools;
            string args = string.Format("-o \"{0}\" \"{1}\" -t -T 9 -p {2}-{3} -s internallinkfunction -s externallinkfunction -f", destinationPath, sourcePath,
                startpage, endpage);
            swfprocess.StartInfo.Arguments = args;

            try
            {
                swfprocess.Start();
                swfprocess.WaitForExit();
                swfprocess.Close();

                return "true";
            }
            catch(Exception exp)
            {
                return "false";
            }
            finally
            {
                ConnectionManager.UnStacks.Push(this.ID);
                ConnectionManager._pool.Release();
            }
        }


        private void swfprocess_ErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine(e.Data);
        }

        private void swfprocess_Exited(object sender, EventArgs e)
        {
            //ConnectionManager.UnStacks.Push(this.ID);
            //ConnectionManager._pool.Release();
            
        }

    }
}
