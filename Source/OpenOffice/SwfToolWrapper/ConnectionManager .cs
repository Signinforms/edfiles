﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using Microsoft.Win32;

using Exception=System.Exception;

namespace SwfToolWrapper
{
    public class ConnectionManager
    {

        static ConnectionManager()
        {
            _pool = new Semaphore(MAX_CONNECTIONS_NUMBER, MAX_CONNECTIONS_NUMBER);
        }

        #region Properties

        private static int _numberOfConnections = 0;
        private const int MAX_CONNECTIONS_NUMBER = 10;

        public static Semaphore _pool;

        /// <summary>
        /// The number of connection attempts made.
        /// </summary>
        public static int NumberOfConnections
        {
            get { return _numberOfConnections; }
        }

        private static Hashtable _connections = new Hashtable();
        private static Stack _unused = new Stack();
        /// <summary>
        /// A registery of all the connections made.
        /// </summary>
        public static Hashtable Connections
        {
            get { return _connections; }
        }

        public static Stack UnStacks
        {
            get { return _unused; }
        }

        #endregion Properties

        /// <summary>
        /// Creates an Connection.
        /// </summary>
        /// <returns>An Connection object.</returns>
        public static Connection CreateConnection()
        {
            try
            {
                lock (_connections)
                {
                    Connection connection = null;

                    if (_connections.Count >= MAX_CONNECTIONS_NUMBER)
                    {
                        _pool.WaitOne();

                        object id = _unused.Pop();
                        connection = _connections[id] as Connection;
                    }
                    else
                    {
                        _pool.WaitOne();

                        connection = new Connection();
                        Connections.Add(connection.ID, connection);
                    }
                    return connection;
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp);
                return null;
            }
        }

      
    }
}
