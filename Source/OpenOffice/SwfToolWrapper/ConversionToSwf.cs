﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using Microsoft.Win32;
using WELog;

namespace SwfToolWrapper
{
    public class ConversionToSwf
    {

        /// <summary>
        /// Converts document to PDF
        /// </summary>
        /// <param name="sourcePath">Path to document to convert(e.g: C:\test.doc)</param>
        /// <param name="destinationPath">Path on which to save PDF (e.g: C:\test.pdf)</param>
        /// <returns>Path to destination file if operation is successful, or Exception text if it is not</returns>
        public static string Generate(string sourcePath, string destinationPath)
        {
          
            try
            {
                Connection connection = ConnectionManager.CreateConnection();
                return connection.Generate(sourcePath, destinationPath);
            }
            catch (Exception exp)
            {
                WELog.ClsEventLog objClsEventLog = new ClsEventLog();
                objClsEventLog.WriteToEventLog("MyEvent", "SwfToolWrapper", exp.Message);

                return exp.Message;
            }
            
        }

        public static string Generate(byte[] data, string destinationPath,int startindex,int count)
        {

            try
            {
                Connection connection = ConnectionManager.CreateConnection();
                return connection.Generate(data, destinationPath, startindex, count);
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

        }


        public static string Generate4Jpeg(string sourcePath, string destinationPath)
        {

            try
            {
                Connection connection = ConnectionManager.CreateConnection();
                return connection.Generate4Jpeg(sourcePath, destinationPath);
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

        }

        public static string Generate4Png(string sourcePath, string destinationPath)
        {

            try
            {
                Connection connection = ConnectionManager.CreateConnection();
                return connection.Generate4Png(sourcePath, destinationPath);
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }

        }
    }
}
