﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Mime;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using Microsoft.Win32;

using uno.util;
using unoidl.com.sun.star.bridge;
using unoidl.com.sun.star.connection;
using unoidl.com.sun.star.frame;
using unoidl.com.sun.star.lang;
using unoidl.com.sun.star.uno;
using Exception = System.Exception;


namespace openOfficeConsole
{
    class Program
    {
        const string OO_PATH_SUFFIX = @"bin";
        const string OO_REG_UNO_INSTALL_KEY = @"Software\Wow6432Node\OpenOffice.org\UNO\InstallPath";
        const string OO_REG_UNO_INSTALL_NAME = "OpenOffice.org 3.0";
        const string OO_REG_INSTALL_KEY = @"Software\Wow6432Node\OpenOffice.org\Layers\URE\1";//HKEY_LOCAL_MACHINE
        const string OO_REG_INSTALL_NAME = "UREINSTALLLOCATION";

        // A semaphore that simulates a limited resource pool.
        //
        private static Semaphore _pool;
        private static TimeSpan _ts = new TimeSpan();
        // A padding interval to make the output more orderly.
        private static int _padding;

        public static void Main()
        {
            try
            {
                // Create a semaphore that can satisfy up to three
                // concurrent requests. Use an initial count of zero,
                // so that the entire semaphore count is initially
                // owned by the main program thread.
                //
                _pool = new Semaphore(0, 5);

                Console.WriteLine("Main thread running.");

                Console.ReadLine();

                // Create and start five numbered threads. 
                //
                for (int i = 1; i <= 1; i++)
                {
                    Thread t = new Thread(new ParameterizedThreadStart(Worker));

                    // Start the thread, passing the number.
                    //
                    t.Start(i);
                }

                // Wait for half a second, to allow all the
                // threads to start and to block on the semaphore.
                //
                Thread.Sleep(500);

                // The main thread starts out holding the entire
                // semaphore count. Calling Release(3) brings the 
                // semaphore count back to its maximum value, and
                // allows the waiting threads to enter the semaphore,
                // up to three at a time.
                //
                Console.WriteLine("Main thread calls Release(5).");
                _pool.Release(5);

                Console.WriteLine("Main thread exits.");
                Console.WriteLine("exits.");
                Console.ReadLine();
            }catch
            {
                Console.ReadLine();
            }
        }

        public static void StartOpenOffice()
        {

            Process[] ps = Process.GetProcessesByName(Properties.Settings.Default.ProcessName);
            if (ps.Equals(null) || ps.Length == 0)
            {
                Process p = Process.Start(Properties.Settings.Default.PathToOpenOffice, "-headless -nologo -norestore");
                //spent some time to start
                System.Threading.Thread.Sleep(3000);
            }
        }

        private static void Worker(object num)
        {
            // Each worker thread begins by requesting the
            // semaphore.
            Console.WriteLine("Thread {0} begins " +
                "and waits for the semaphore.", num);
            _pool.WaitOne();

            // A padding interval to make the output more orderly.
            int padding = Interlocked.Add(ref _padding, 1000);

            Console.WriteLine("Thread {0} enters the semaphore.", num);

            // The thread's "work" consists of sleeping for 
            // about a second. Each thread "works" a little 
            // longer, just to make the output more orderly.
            //
            //Thread.Sleep(1000 + padding);


            StartOpenOffice();

            string oo_UNO_Path = Reg_Retrieve_String(OO_REG_UNO_INSTALL_KEY, OO_REG_UNO_INSTALL_NAME);
            string oo_Path_Mod = Reg_Retrieve_String(OO_REG_INSTALL_KEY, OO_REG_INSTALL_NAME);
            oo_Path_Mod += OO_PATH_SUFFIX;

            Console.WriteLine(oo_UNO_Path);
            Console.WriteLine(oo_Path_Mod);

            Environment.SetEnvironmentVariable("UNO_PATH", oo_UNO_Path);
            Environment.SetEnvironmentVariable("PATH",
                                               Environment.GetEnvironmentVariable("PATH") + ";" +
                                               oo_Path_Mod);

            //Get a ComponentContext
            unoidl.com.sun.star.uno.XComponentContext xLocalContext = uno.util.Bootstrap.bootstrap();

            //

            OpenOfficeService.Objects.GenericSender.Init(
                "tcp://localhost:6543/OpenOfficeServiceReceiver");

            //string source = Server.MapPath("~/ExcelSheet.xls");
            Assembly assem = Assembly.GetExecutingAssembly();

            //Stopwatch watch = new Stopwatch();
            //watch.Start();
           
           TimeSpan ts ;

           string namef = "Shinetech_LuYuanZong2.doc";
           // string encodeName = HttpUtility.UrlPathEncode(namef);
            //encodeName = encodeName.Replace("%20", "$20");
           string source = Path.GetDirectoryName(assem.Location) + @"\" + namef;

           // string source = Path.GetDirectoryName(assem.Location) + @"\" + encodeName + ".pdf";
          

            //source = HttpUtility.UrlDecode(source);
           string dst = Path.GetDirectoryName(assem.Location) + @"\" +  "Shinetech_LuYuanZong2.pdf";
     
            //FileStream stream = File.OpenRead(source);

            //byte[] indata = new byte[stream.Length];
          
            //stream.Read(indata, 0, (int)stream.Length);
            //stream.Close();

           string result = OpenOfficeService.Objects.GenericSender.Receiver.ConvertToPDF(source, dst);

           Console.WriteLine(result);



           //string dest = Path.GetDirectoryName(assem.Location) + @"\efilefolderppt" + num + ".pdf";

            //FileStream sf = new FileStream(dest,FileMode.CreateNew);
            //sf.Write(result, 0, result.Length);
            //sf.Flush();
            //sf.Close();

            //_ts = _ts.Add(ts);
            
            // string stime = String.Format("{0:00}:{1:00}:{2:00}",
            //        ts.Hours, ts.Minutes, ts.Seconds);

            // Console.WriteLine("Thread {0} spend times.", stime);

            // string ttime = String.Format("{0:00}:{1:00}:{2:00}",
            //        _ts.Hours, _ts.Minutes, _ts.Seconds);

            // Console.WriteLine("Threads spend {0} total times.", ttime);

            //Console.WriteLine("Thread {0} releases the source.", source);

            //Console.WriteLine("Thread {0} releases the semaphore.", num);
            //Console.WriteLine("Thread {0} previous semaphore count: {1}",
            _pool.Release();


           
        }

        public static string Reg_Retrieve_String(string path, string key)
        {
            RegistryKey root = Registry.LocalMachine.OpenSubKey(path);
            string objstr = root.GetValue(key) as string;

            return objstr;
        }

    }
}
