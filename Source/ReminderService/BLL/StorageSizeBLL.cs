using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using DataQuicker2.Framework;
using Shinetech.DAL;

namespace Shinetech.Services.BLL
{
    public class StorageSizeBLL
    {
        public static DataTable GetAllStorageTasks()
        {
            string sqlstr = "SELECT a.UserID,a.UserName, b.Email,DateDIFF(day,b.CreateDate,GETDATE()) AS DCNT "+
                            "FROM dbo.TempAccount a LEFT JOIN Account b ON b.UID=a.UserID WHERE b.CreateDate=b.RegistryDate "+
                            "AND b.LicenseID=-1";
            SqlHelper sql = new SqlHelper("Default");
            DataTable table = new DataTable();
            try
            {
                sql.Fill(table, sqlstr);
                return table;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
  
}
