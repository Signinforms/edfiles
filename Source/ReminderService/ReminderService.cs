using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using System.Threading;
using Amib.Threading;
using DataQuicker2.Framework;
using Schedule;
using Shinetech.DAL;
using Shinetech.Services.BLL;
using Shinetech.Utility;
using Account=Shinetech.DAL.Account;

namespace Shinetech.Services
{
    public partial class ReminderService : ServiceBase
    {
        protected SmartThreadPool _ThreadPool;
        protected ScheduleTimer  _TickTimer = new ScheduleTimer();
        protected string _DailyTime;
        protected float _StorageFee;
        protected string errorFile = string.Empty;
        protected string _intervals = "10";
        protected const string strSubject = "eFileFolders' Trial Account Reminder";
        const string mailTempletUrl = "MailContent.htm";

        public ReminderService()
        {
            InitializeComponent();

        }

        protected override void OnStart(string[] args)
        {
            Start();
        }

        private void TickTimer_Elapsed(object sender, ScheduledEventArgs e)
        {
            WriteLogMessage("Start getting trial reminder tasks");

            DataTable table = StorageSizeBLL.GetAllStorageTasks();
            if(table!=null)
            {
                int dt=1 ;
                string uname, emailaddr;
                int intervals = Convert.ToInt32(_intervals);
                foreach(DataRow item in table.Rows)
                {
                    dt = Convert.ToInt32(item["DCNT"]);
                    uname = item["UserName"] as string;
                    emailaddr = item["Email"] as string;
                    if (dt % intervals == 0)
                    {
                        SendEmail(uname, emailaddr);
                    }
                    Thread.Sleep(1000);
                }

              
            }
            else
            {
                WriteLogMessage("table result is null");
            }
        }


        protected override void OnStop()
        {
            try
            {
                _TickTimer.Stop();
                _ThreadPool.Shutdown();
            }catch{}
            
        }

        public void Start()
        {
            try
            {
                _DailyTime = ConfigurationManager.AppSettings["ReminderTime"];
                _intervals = ConfigurationManager.AppSettings["Intervals"];
                errorFile = ConfigurationManager.AppSettings["LogPath"];
                WriteLogMessage("Starting service");
            }catch(Exception exp)
            {
                WriteErrorMessage(exp.Message);
                return;
            }

            _ThreadPool = new SmartThreadPool(5000,1,1);
            _ThreadPool.Start();
            _TickTimer.Elapsed += new ScheduledEventHandler(TickTimer_Elapsed);
            if(ConfigurationManager.AppSettings["test_mode"].ToLower()=="test")
            {
                _TickTimer.AddEvent(new Schedule.ScheduledTime("Hourly", "30,0"));
            }
            else
            {
                _TickTimer.AddEvent(new Schedule.ScheduledTime("Daily", _DailyTime));
            }
            
            _TickTimer.Start();

            WriteLogMessage("Starting Timer");
        }

        public void StopE()
        {
            WriteLogMessage("Stopping service");
            OnStop();
        }

        public void WriteErrorMessage(string errorMsg)
        {
            string applicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            if (!applicationBase.EndsWith(@"\"))
            {
                applicationBase += @"\";
            }

            try 
            {
                string errorlog = string.Format(applicationBase + errorFile + "{0}-{1}.log", DateTime.Now.Year, DateTime.Now.Month);
                using (StreamWriter sw1 = new StreamWriter(errorlog, true))
                {
                    sw1.WriteLine("-----------Error Log----------- " + DateTime.Now.ToString());
                    sw1.WriteLine(errorMsg);
                }
            }
            catch { }
        }

        public void WriteLogMessage(string logMsg)
        {
            string applicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            if (!applicationBase.EndsWith(@"\"))
            {
                applicationBase += @"\";
            }

            try
            {
                string errorlog = string.Format(applicationBase + errorFile + "{0}-{1}.log",DateTime.Now.Year,DateTime.Now.Month);
                using (StreamWriter sw1 = new StreamWriter(errorlog, true))
                {
                    sw1.WriteLine("-----------Message Log----------- " + DateTime.Now.ToString());
                    sw1.WriteLine(logMsg);
                }
                
            }
            catch { }
        }

        public void SendEmail(string uname,string emailaddr)
        {
            if (emailaddr == "") return;

            string strMailTemplet = getMailTempletStr();
            strMailTemplet = strMailTemplet.Replace("[username]", uname);

            Email.SendFromEFFService(emailaddr, strSubject, strMailTemplet);

            WriteLogMessage("End sending email");
        }

        public string getMailTempletStr()
        {
            string strMailTemplet = string.Empty;
            string applicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            try
            {
                StreamReader sr = new StreamReader(applicationBase+mailTempletUrl);
                string sLine = "";
                while (sLine != null)
                {
                    sLine = sr.ReadLine();
                    if (sLine != null)
                        strMailTemplet += sLine;
                }
                sr.Close();
            }
            catch(Exception exp)
            {
                throw new ApplicationException(exp.Message);
            }
            
            return strMailTemplet;
        }


    }

}
