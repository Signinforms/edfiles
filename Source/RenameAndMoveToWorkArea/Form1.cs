﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using RenameAndMoveToWorkArea.ServiceReference1;
using Newtonsoft.Json;
using Aspose.Cells;
using Style = Aspose.Cells.Style;

namespace RenameAndMoveToWorkArea
{
    public partial class Form1 : Form
    {
        public static string RootPath = ConfigurationSettings.AppSettings["RootPath"];
        public static string DeletedPath = ConfigurationSettings.AppSettings["DeletedWorkArea"];
        public static ServiceReference1.EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();


        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            GetUserDetails();
        }

        public void GetUserDetails()
        {
            if (Directory.Exists(RootPath))
            {
                string[] userFolders = Directory.GetDirectories(RootPath);
                foreach (string user in userFolders)
                {
                    if (!user.Contains(DeletedPath.Split('\\')[0]))
                    {
                        List<UserStructure> userDetails = JsonConvert.DeserializeObject<List<UserStructure>>(obj.GetUserIdFromUserName(user.Substring(user.LastIndexOf('\\') + 1)));

                        if (userDetails != null && userDetails.Count > 0)
                        {
                            //Get UID from UserName
                            string uid = string.Empty;
                            if (Convert.ToBoolean(Convert.ToInt32(userDetails[0].IsSubUser)))
                                uid = userDetails[0].OfficeUID;
                            else
                                uid = userDetails[0].UID;

                            FindDocumentsAndRename(user, uid);
                        }
                    }
                }
            }
        }

        public void FindDocumentsAndRename(string userPath, string uid)
        {
            foreach (string folder in Directory.GetDirectories(userPath))
            {
                string excelFile = folder + Path.DirectorySeparatorChar + Path.GetFileName(folder) + ".csv";
                if (File.Exists(excelFile))
                {
                    Workbook workbook = new Workbook(excelFile);
                    Worksheet ws = workbook.Worksheets[0];
                    Cells cells = ws.Cells;
                    Row header = cells.GetRow(0);
                    var rows = cells.Rows;
                    progressBar1.Maximum = cells.Rows.Count;
                    int columnCount = 0;
                    try
                    {
                        if (header != null && header.LastDataCell != null)
                            columnCount = CellsHelper.ColumnNameToIndex(header.LastDataCell.Name[0].ToString());
                    }
                    catch (Exception) { }
                    label2.Text = "Files Renamed And Uploaded 0/" + rows.Count;
                    label2.Refresh();
                    if (rows.Count > 2)
                    {
                        try
                        {
                            textBox1.AppendText("Folder : " + folder.Split(new string[] { RootPath }, StringSplitOptions.None)[1]);
                        }
                        catch (Exception) { }
                        textBox1.AppendText(Environment.NewLine);
                        textBox1.AppendText(Environment.NewLine);
                        textBox1.Refresh();
                    }
                    int index = 0;
                    for (int i = 1; i < rows.Count; i++)
                    {
                        var row = rows[i];
                        string fileName = row.LastDataCell != null ? Convert.ToString(row.LastDataCell.Value) : string.Empty;
                        if (!string.IsNullOrEmpty(fileName))
                        {
                            string fileNameOnly = Path.GetFileName(fileName);
                            label1.Text = "File " + fileNameOnly + " is being uploaded....";
                            label1.Refresh();
                            string newFileName = string.Empty;
                            for (int c = 0; c < columnCount; c++)
                            {
                                if (cells.GetCell(i, c) != null)
                                    newFileName += Convert.ToString(cells.GetCell(i, c).Value).Replace("/", "_").Replace("\\", "_") + ", ";
                            }

                            string objectName = fileName.Substring(0, fileName.LastIndexOf('\\') + 1);
                            objectName = objectName + newFileName.Trim().Remove(newFileName.LastIndexOf(','));
                            fileName = userPath + Path.DirectorySeparatorChar + fileName;
                            if (File.Exists(fileName))
                            {
                                byte[] fileData = null;
                                try
                                {
                                    string newObjectName = objectName.Substring(0, objectName.LastIndexOf('\\'));
                                    newObjectName = newObjectName.Substring(0, newObjectName.LastIndexOf('\\'));
                                    objectName = newObjectName + objectName.Substring(objectName.LastIndexOf('\\'));
                                    fileData = File.ReadAllBytes(fileName);
                                }
                                catch (Exception) { }
                                int success = obj.MoveDocumentsToWorkArea(objectName, 1, fileData, uid);
                                if (success == 1)
                                {
                                    try
                                    {
                                        string[] splitData = fileName.Split(new string[] { RootPath }, StringSplitOptions.None); ;
                                        if (splitData.Length > 1)
                                        {
                                            string dest = RootPath + DeletedPath + splitData[1];
                                            if (!Directory.Exists(System.IO.Path.GetDirectoryName(dest)))
                                                Directory.CreateDirectory(System.IO.Path.GetDirectoryName(dest));
                                            try { File.Move(fileName, dest); }
                                            catch (Exception) { }
                                        }
                                        textBox1.AppendText("(" + index + ")  " + fileNameOnly);
                                        textBox1.AppendText(Environment.NewLine);
                                        textBox1.Refresh();
                                        label1.Text = "File " + fileNameOnly + " has been uploaded successfully.";
                                        label1.Refresh();
                                        label2.Text = "Files Renamed And Uploaded : " + index + "/" + rows.Count;
                                        label2.Refresh();
                                        index++;
                                    }
                                    catch (Exception) { }
                                }
                                else { }
                            }
                        }
                        try { progressBar1.Value = i; }
                        catch (Exception) { }
                    }
                    textBox1.AppendText(Environment.NewLine);
                    textBox1.AppendText(Environment.NewLine);
                    textBox1.Refresh();
                }
                else 
                {
                    string folderName = Path.GetFileName(folder);
                    string message = "There is no Excel or CSV file for Folder \"" + folderName + "\". Are you sure, you want to proceed uploading without renaming ?";
                    string caption = "No Excel File Found";
                    MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                    DialogResult result;

                    result = MessageBox.Show(message, caption, buttons);

                    if (result == System.Windows.Forms.DialogResult.Yes)
                    {
                        foreach (string subFolder in Directory.GetDirectories(folder)) 
                        {
                            int index = 1;
                            foreach (string file in Directory.GetFiles(subFolder)) 
                            {
                                byte[] fileData = null;
                                try
                                {
                                    fileData = File.ReadAllBytes(file);
                                }
                                catch (Exception) { }
                                string fileName = Path.GetFileNameWithoutExtension(file);
                                label1.Text = "File " + fileName + " is being uploaded....";
                                label1.Refresh();
                                string fileToUpload = folderName + Path.DirectorySeparatorChar + fileName;
                                int success = obj.MoveDocumentsToWorkArea(fileToUpload, 1, fileData, uid);
                                if (success == 1)
                                {
                                    try
                                    {
                                        string[] splitData = file.Split(new string[] { RootPath }, StringSplitOptions.None); ;
                                        if (splitData.Length > 1)
                                        {
                                            string dest = RootPath + DeletedPath + splitData[1];
                                            if (!Directory.Exists(System.IO.Path.GetDirectoryName(dest)))
                                                Directory.CreateDirectory(System.IO.Path.GetDirectoryName(dest));
                                            try { File.Move(file, dest); }
                                            catch (Exception) { }
                                        }
                                        textBox1.AppendText("(" + index + ")  " + fileName);
                                        textBox1.AppendText(Environment.NewLine);
                                        textBox1.Refresh();
                                        label1.Text = "File " + fileName + " has been uploaded successfully.";
                                        label1.Refresh();
                                        label2.Text = "Files Uploaded : " + index + "/" + Directory.GetFiles(subFolder).Count();
                                        label2.Refresh();
                                        index++;
                                    }
                                    catch (Exception) { }
                                }
                                else { }
                            }
                        }
                    }
                    else 
                    {
                        continue;
                    }
                    textBox1.AppendText(Environment.NewLine);
                    textBox1.AppendText(Environment.NewLine);
                    textBox1.Refresh();
                }
            }
        }
    }

    public class UserStructure
    {
        public string UID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OfficeUID { get; set; }
        public string IsSubUser { get; set; }
    }
}
