﻿
namespace eScanner.Interfaces
{
    /// <summary>
    /// Interface IRefreshable
    /// </summary>
    internal interface IRefreshable
    {
        /// <summary>
        /// Refreshes this instance.
        /// </summary>
        void Refresh();
    }
}
