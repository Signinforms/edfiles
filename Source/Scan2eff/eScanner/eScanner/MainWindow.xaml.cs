﻿using eScanner.BO;
using eScanner.eScannerService;
using eScanner.Interfaces;
using eScanner.Model;
using eScanner.UserControls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace eScanner
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        PDFViewer pvMain = new PDFViewer("about:blank");

        /// <summary>
        /// MainWindow
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Shows the control.
        /// </summary>
        /// <param name="controlToShow">The control to show.</param>
        /// <param name="needsScrolling">if set to <c>true</c> [needs scrolling].</param>
        private void ShowControl(Control controlToShow, bool needsScrolling)
        {
            if (SystemParameters.PrimaryScreenHeight < 870 || SystemParameters.PrimaryScreenWidth < 1042)
            {
                needsScrolling = true;
            }
            else
            {
                //this.MinHeight = 870;
                //this.MinWidth = 1042;
            }

            // needsScrolling = false;
            if (controlToShow.ToString().ToLower().Contains("generalviewer") || controlToShow.ToString().ToLower().Contains("pdfviewer"))
            {
                dockContainer1.Children.Clear();
                dockContainer1.Children.Add(controlToShow);
            }
            else if (controlToShow.ToString().ToLower().Contains("login"))
            {
                dockContainer.Children.Clear();
                dockContainer.Children.Add(controlToShow);
            }
            else if (controlToShow.ToString().ToLower().Contains("actionfile"))
            {
                dockContainer.Children.Clear();
                dockContainer.Children.Add(controlToShow);
            }
            this.RaiseEvent(new RoutedEventArgs(System.Windows.FrameworkElement.SizeChangedEvent));
        }

        /// <summary>
        /// Views the control.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="ancestor">The ancestor.</param>
        public void ViewControl(Control control, object ancestor)
        {
            try
            {
                LoadControl(control, true, null, ancestor, false);
            }
            catch (Exception exception)
            {
               
            }
        }

        /// <summary>
        /// Loads the control.
        /// </summary>
        /// <param name="controlToLoad">The control to load.</param>
        /// <param name="isInvokedFromMenu">if set to <c>true</c> [is invoked from menu].</param>
        /// <param name="initializationData">The initialization data.</param>
        /// <param name="ancestorControl">The ancestor control.</param>
        /// <param name="needsScrolling">if set to <c>true</c> [needs scrolling].</param>
        private void LoadControl(Control controlToLoad, bool isInvokedFromMenu, object initializationData, object ancestorControl, bool needsScrolling)
        {
            if (controlToLoad is INotify)
            {
                ((INotify)controlToLoad).Notify -= new NotifyEventHandler(OnNotified);
                ((INotify)controlToLoad).Notify += new NotifyEventHandler(OnNotified);
            }

            if (controlToLoad is IInitializable)
            {
                IInitializable initializableControl = (IInitializable)controlToLoad;
                if (ancestorControl != null)
                    initializableControl.AncestorControl = (Control)ancestorControl;
                if (initializationData != null)
                    initializableControl.Initialize(initializationData);
            }
            //Todo: consider change later on
            if (!isInvokedFromMenu)
            {
                if (controlToLoad is IRefreshable)
                    ((IRefreshable)controlToLoad).Refresh();
            }

            ShowControl(controlToLoad, needsScrolling);
        }

        /// <summary>
        /// Called when [notified].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="targetControl">The target control.</param>
        /// <param name="initializationData">The initialization data.</param>
        private void OnNotified(object sender, Type targetControl, object initializationData)
        {
            try
            {
                Control controlToLoad = null;
                if (targetControl != null)
                {
                    controlToLoad = (Control)Activator.CreateInstance(targetControl);
                }
                else if (sender is IInitializable)
                {
                    controlToLoad = ((IInitializable)sender).AncestorControl; //ParentControl;
                    if (controlToLoad is IInitializable)
                    {
                        initializationData = null;
                        sender = null;
                    }
                }

                LoadControl(controlToLoad, false, initializationData, sender, false);
            }
            catch (Exception exception)
            {
                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TabControl_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            switch (((sender as TabControl).SelectedItem as TabItem).Name)
            {
                case "tabAction":
                    ViewControl(new ActionFile(), null);
                    break;

                case "tabLogin":
                    tabAction.IsEnabled = false;
                    btnWorkArea.IsEnabled = false;
                    btnEfileFolder.IsEnabled = false;
                    btnWorkArea.Opacity = 0.5;
                    btnEfileFolder.Opacity = 0.5;
                    ViewControl(new Login(), null);
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TabControl_SelectionChanged_2(object sender, SelectionChangedEventArgs e)
        {
            switch (((sender as TabControl).SelectedItem as TabItem).Name)
            {
                case "tabGeneral":
                    ViewControl(new GeneralViewer(), null);
                    break;

                case "tabViewer":
                    if (tabViewer.Tag != null)
                    {
                        pvMain.pdfWebViewer.Dispose();
                        pvMain = new PDFViewer(tabViewer.Tag.ToString());
                        ViewControl(pvMain, null);
                        tabViewer.Tag = null;
                    }
                    else
                        ViewControl(new PDFViewer(), null);
                    break;
            }
        }

        /// <summary>
        /// Edit Button Click Event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            GeneralViewer gv = dockContainer1.Children.OfType<GeneralViewer>().FirstOrDefault();
            eScanner.Model.FileItem item = ((gv.DataContext as eScanner.Model.FileList).SelectedItem as eScanner.Model.FileItem);
            try
            {
                if (item.FileName != null)
                {
                    string oldFile = ConfigurationManager.AppSettings["FileBasePath"] + "\\" + item.FileName;
                    EditViewer ev = new EditViewer(item);
                    Application.Current.MainWindow.Opacity = 0.2;
                    if (ev.ShowDialog() == true)
                    {
                        string newFile = ConfigurationManager.AppSettings["FileBasePath"] + "\\" + item.FileName;
                        if (File.Exists(newFile) && newFile != oldFile)
                        {
                            File.Delete(newFile);
                        }
                        File.Move(oldFile, newFile);
                    }

                    Application.Current.MainWindow.Opacity = 1;
                }
            }
            catch (Exception exc)
            {
 
            }

        }

        /// <summary>
        /// btnExit Click Event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Close();
        }

        /// <summary>
        /// btnViewer Click Event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnViewer_Click(object sender, RoutedEventArgs e)
        {
            GeneralViewer gv = dockContainer1.Children.OfType<GeneralViewer>().FirstOrDefault();
            eScanner.Model.FileItem item = ((gv.DataContext as eScanner.Model.FileList).SelectedItem as eScanner.Model.FileItem);
            if (item.FileName != null)
            {
                string path = item.FilePath.ToString() + "//" + item.FileName.ToString();
                (Application.Current.MainWindow as MainWindow).tabViewer.Tag = path;
                (Application.Current.MainWindow as MainWindow).tabViewer.IsSelected = true;
            }

        }

        /// <summary>
        /// btnViewer Click Event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnWorkArea_Click(object sender, RoutedEventArgs e)
        {
            GeneralViewer gv = dockContainer1.Children.OfType<GeneralViewer>().FirstOrDefault();
            PDFViewer pv =  dockContainer1.Children.OfType<PDFViewer>().FirstOrDefault();
            
            eScanner.Model.FileItem item = ((gv.DataContext as eScanner.Model.FileList).SelectedItem as eScanner.Model.FileItem);
            try
            {
                if (item.FileName != null)
                {
                    string pathName = item.FilePath;
                    string fileName = item.FileName;
                    int fileExtention = 1;   // For *.PDF
                    byte[] fileData = File.ReadAllBytes(ConfigurationManager.AppSettings["FileBasePath"] + "\\" + fileName);


                    eScannerService.EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();
                    AuthenticationInfo obj2 = new AuthenticationInfo();
                    obj2.username = GlobalContext.UserName;
                    obj2.password = GlobalContext.Password;


                    int id = obj.MoveDocumentsToWorkAreaFolder(obj2, pathName, fileName, fileExtention, fileData, GlobalContext.UserId);
                    if (id == 1)
                    {
                        string localFile = ConfigurationManager.AppSettings["FileBasePath"] + "\\" + item.FileName;
                        pvMain.pdfWebViewer.Dispose();
                        File.Delete(localFile);
                        gv.DataContext = new FileList();
                        MessageBox.Show("File copied successfully to WorkArea");
                    }
                    else
                        MessageBox.Show("No Such File exist!");

                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, "ToWorkArea");
            }


        }

        /// <summary>
        /// btnEFileFolder Click Event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEfileFolder_Click(object sender, RoutedEventArgs e)
        {
            ActionFile av = dockContainer.Children.OfType<ActionFile>().FirstOrDefault();
            eScanner.Model.FolderDetails folderDetails = ((av.DataContext as eScanner.Model.ActionFileViewModel).SelectedFolder as eScanner.Model.FolderDetails);
            eScanner.Model.DividerDetails dividerDetails = ((av.DataContext as eScanner.Model.ActionFileViewModel).SelectedDivider as eScanner.Model.DividerDetails);
            try
            {
                if (folderDetails != null && dividerDetails != null)
                {
                    if (folderDetails.ID.ToString() != "" && dividerDetails.ID.ToString() != "")
                    {
                        GeneralViewer gv = dockContainer1.Children.OfType<GeneralViewer>().FirstOrDefault();
                        eScanner.Model.FileItem item = ((gv.DataContext as eScanner.Model.FileList).SelectedItem as eScanner.Model.FileItem);
                        if (item.FileName != null)
                        {
                            string pathName = item.FilePath;
                            string fileName = item.FileName;
                            int fileExtention = 1;   // For *.PDF

                            byte[] fileData = File.ReadAllBytes(ConfigurationManager.AppSettings["FileBasePath"] + "\\" + fileName);

                            eScannerService.EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();
                            AuthenticationInfo obj2 = new AuthenticationInfo();
                            obj2.username = GlobalContext.UserName;
                            obj2.password = GlobalContext.Password;

                            int id = obj.MoveDocumentsToAnotherFolder(obj2, pathName, fileName, fileExtention, folderDetails.ID.ToString(), dividerDetails.ID.ToString(), fileData);
                            if (id == 1)
                            {
                                string localFile = ConfigurationManager.AppSettings["FileBasePath"] + "\\" + item.FileName;
                                pvMain.pdfWebViewer.Dispose();
                                File.Delete(localFile);
                                gv.DataContext = new FileList();
                                MessageBox.Show("File copied successfully to EFileFolders");
                            }
                            else
                                MessageBox.Show("No Such File exist!");

                        }
                    }
                    else
                    {
                        if (folderDetails.ID.ToString() == "")
                            MessageBox.Show("Please select folder");
                        if (dividerDetails.ID.ToString() == "")
                            MessageBox.Show("Please select divider");
                    }

                }
                else
                {
                    MessageBox.Show("Please select folder/divider");
                }
            }
            catch (Exception exc)
            {

            }
        }
    }
}
