﻿using eScanner.eScannerService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GlobalContext = eScanner.BO.GlobalContext;

namespace eScanner.Model
{
    public class ActionFileViewModel : BaseNotifyPropertyChanged
    {

        /// <summary>
        /// The folder data
        /// </summary>
        private IList<FolderDetails> _folderData = new List<FolderDetails>();
        private IList<DividerDetails> _dividerData = new List<DividerDetails>();
        private IList<DocumentDetails> _documentData = new List<DocumentDetails>();
        private FolderDetails _selectedFolder;
        private DividerDetails _selectedDivider;
        /// <summary>
        /// Gets the folder data.
        /// </summary>
        /// <value>The folder data.</value>
        public IList<FolderDetails> FolderData { get { return _folderData; } }
        public IList<DividerDetails> DividerData
        {
            get
            {
                return _dividerData;
            }
            set
            {
                _dividerData = value;
                RaisePropertyChanged(() => DividerData);
            }
        }

        public FolderDetails SelectedFolder
        {
            get
            {
                return _selectedFolder;
            }
            set
            {
                _selectedFolder = value;
                GetDividerData(_selectedFolder.ID);
            }
        }

        public DividerDetails SelectedDivider
        {
            get
            {
                return _selectedDivider;
            }
            set
            {
                _selectedDivider = value;
                if (_selectedDivider != null)
                BindData(_selectedDivider.ID);
            }
        }
        public IList<DocumentDetails> DocumentData { get { return _documentData; } }

        private List<DocumentDetails> _documentItems = new List<DocumentDetails>();
        public List<DocumentDetails> DocumentItems
        {
            get
            {
                return _documentItems;
            }
            set
            {
                _documentItems = value;
                RaisePropertyChanged(() => DocumentItems);
            }
        }

        private DocumentDetails _selectedDocument = new DocumentDetails();
        public DocumentDetails SelectedDocument
        {
            get
            {
                return _selectedDocument;
            }
            set
            {
                _selectedDocument = value;
                RaisePropertyChanged(() => SelectedDocument);
            }
        }


        string FolderID;
        

        public ActionFileViewModel()
        {
            GetFolderData();
        }

        public void GetFolderData()
        {
            try
            {
                eScannerService.EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();
                AuthenticationInfo obj2 = new AuthenticationInfo();
                obj2.username = GlobalContext.UserName;
                obj2.password = GlobalContext.Password;
                DataTable dt = obj.GetFileFolders(obj2, GlobalContext.UserName);

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        _folderData.Add( new FolderDetails(Convert.ToInt32(dr["FolderID"]),dr["FolderName"].ToString()));
                    }
                }

            }
            catch (Exception exc)
            {
 
            }
        }

        public void GetDividerData(int folderID)
        {
            try
            {
                eScannerService.EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();
                AuthenticationInfo obj2 = new AuthenticationInfo();
                obj2.username = GlobalContext.UserName;
                obj2.password = GlobalContext.Password;
                DataTable dt = obj.GetTabsByFolderID(obj2, folderID.ToString());
                List<DividerDetails> lst = new List<DividerDetails>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        lst.Add(new DividerDetails(Convert.ToInt32(dr["DividerID"].ToString()), dr["Name"].ToString()));
                    }
                }
                DividerData = lst;
            }
            catch (Exception exc)
            {
 
            }
        }

        public void BindData(int dividerID)
        {
            try
            {
                eScannerService.EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();
                AuthenticationInfo obj2 = new AuthenticationInfo();
                obj2.username = GlobalContext.UserName;
                obj2.password = GlobalContext.Password;
                DataTable dt = obj.GetDocumentsByDividerId(obj2, dividerID.ToString());

                List<DocumentDetails> lstdocument = new List<DocumentDetails>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        lstdocument.Add(new DocumentDetails(Convert.ToInt32(dr["DocumentID"].ToString()), dr["Name"].ToString(), (Convert.ToDateTime(dr["DOB"].ToString())), dr["Class"].ToString(), dr["PathName"].ToString(), Convert.ToInt32(dr["FileExtention"])));
                    }
                }
                this.DocumentItems = lstdocument.Where(x => x.FileExtension == 1).ToList();
               
            }
            catch (Exception exc)
            {
 
            }
        }

    }

    public class FolderDetails
    {
        public int ID { get; set; }
        public string FolderName { get; set; }
        public FolderDetails(int id,string fName)
        {
            this.ID = id;
            this.FolderName = fName;
        }
    }

    public class DividerDetails
    {
        public int ID { get; set; }
        public string DividerName { get; set; }
        public DividerDetails(int id, string name)
        {
            this.ID = id;
            this.DividerName = name;
        }
    }

    public class DocumentDetails
    {
        public int DocumentID { get; set; }
        public string Name { get; set; }
        public DateTime DOB { get; set; }
        public string Class { get; set; }
        public string Path { get; set; }
        public int FileExtension { get; set; }


        public DocumentDetails(int documentid, string documentname,DateTime dob, string Class, string path, int fileExtension)
        {
            this.DocumentID = documentid;
            this.Name = documentname;
            this.DOB = dob;
            this.Class = Class;
            this.Path = path;
            this.FileExtension = fileExtension;
        }

        public DocumentDetails()
        {

        }
    }
}
