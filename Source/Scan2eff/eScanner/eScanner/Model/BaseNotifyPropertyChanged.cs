﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace eScanner.Model
{
    public class BaseNotifyPropertyChanged : INotifyPropertyChanged //IFocusMover
    {
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        ///// <summary>
        ///// Raised when the input focus should move to
        ///// a control whose 'active' dependency property
        ///// is bound to the specified property.
        ///// </summary>
        //public event EventHandler<MoveFocusEventArgs> MoveFocus;

        ///**
        // * Example usage: RaiseMoveFocus(() => AccountNumber );
        // **/
        ///// <summary>
        ///// Raises the move focus.
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="property">The property.</param>
        //protected void RaiseMoveFocus<T>(Expression<Func<T>> property)
        //{
        //    string propertyName = NameFromProperty(property);
        //    VerifyPropertyName(propertyName);

        //    var handler = MoveFocus;
        //    if (handler != null)
        //    {
        //        handler(this, new MoveFocusEventArgs(propertyName));
        //    }
        //}

        /**
         * Example usage: RaisePropertyChanged(() => AccountNumber );
         **/
        /// <summary>
        /// Raises the property changed.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property">The property.</param>
        protected void RaisePropertyChanged<T>(params Expression<Func<T>>[] property)
        {
            foreach (Expression<Func<T>> p in property)
            {
                RaisePropertyChanged(NameFromProperty(p));
            }
        }

        /// <summary>
        /// Raises the property changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        private void RaisePropertyChanged(string propertyName)
        {
            VerifyPropertyName(propertyName);

            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Names from property.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="property">The property.</param>
        /// <returns>System.String.</returns>
        private string NameFromProperty<T>(Expression<Func<T>> property)
        {
            LambdaExpression lambda = (LambdaExpression)property;

            if (lambda.Body is UnaryExpression)
            {
                return ((MemberExpression)((UnaryExpression)lambda.Body).Operand).Member.Name;
            }
            else
            {
                return ((MemberExpression)lambda.Body).Member.Name;
            }
        }

        /// <summary>
        /// Verifies the name of the property.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <exception cref="System.Exception">Invalid property name:  + propertyName</exception>
        public void VerifyPropertyName(string propertyName)
        {
            // Verify that the property name matches a real,  
            // public, instance property on this object.
            if (TypeDescriptor.GetProperties(this)[propertyName] == null)
            {
                throw new Exception("Invalid property name: " + propertyName);
            }
        }
    }
}
