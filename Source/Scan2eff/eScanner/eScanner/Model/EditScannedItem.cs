﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eScanner.Model
{
    public class EditScannedItem : BaseNotifyPropertyChanged
    {

        private string _fileName;
        private string _alert1;
        private string _alert2;

        public event PropertyChangedEventHandler PropertyChanged;


        public string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
                RaisePropertyChanged(() => FileName);
            }
        }

        public string Alert1
        {
            get
            {
                return _alert1;
            }
            set
            {
                _alert1 = value;
                RaisePropertyChanged(() => Alert1);
            }
        }

        public string Alert2
        {
            get
            {
                return _alert2;
            }
            set
            {
                _alert2 = value;
                RaisePropertyChanged(() => Alert2);
            }
        }

        public EditScannedItem(ScannedItem item)
        {
            if (item != null)
            {
                FileName = item.SelectedItems.Row["FileName"].ToString();
                Alert1 = item.SelectedItems.Row["Alert1"].ToString();
                Alert2 = item.SelectedItems.Row["Alert2"].ToString();
            }
        }

    }
}
