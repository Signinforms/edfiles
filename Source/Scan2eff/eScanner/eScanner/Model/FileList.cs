﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace eScanner.Model
{
    public class FileList : BaseNotifyPropertyChanged
    {

        public FileList()
        {
            GetDetails();

        }

        public void GetDetails()
        {
            //var data = new List<FileItem>
            //{
            //    new FileItem("abc.pdf","abc","","a1","a2",DateTime.Today.ToString("dd/MM/yyyy"),"N/A"),
            //    new FileItem("def.pdf","abc","","a1","a2",DateTime.Today.ToString("dd/MM/yyyy"),"N/A"),
            //    new FileItem("123.pdf","abc","","a1","a2",DateTime.Today.ToString("dd/MM/yyyy"),"N/A")
            //};

            //this.Items = data;
            string filePath = "";
            string fileName = "";
            List<FileItem> lstFile = new List<FileItem>();
            foreach (string txtName in Directory.GetFiles(ConfigurationManager.AppSettings["FileBasePath"], "*.pdf"))
            {
                
                int idx = txtName.LastIndexOf(@"\");
                filePath = txtName.Substring(0,idx).Replace(@"\", "//");
                fileName = txtName.Substring(idx + 1);
                lstFile.Add(new FileItem(fileName, "", "", "", "", DateTime.Today.ToString("dd/MM/yyyy"), "",filePath));
                
            }
            this.Items = lstFile;

        }

        private FileItem _selectedItem = new FileItem();
        public FileItem SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;
                RaisePropertyChanged(() => SelectedItem);
            }
        }

        private List<FileItem> _items = new List<FileItem>();
        public List<FileItem> Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
                RaisePropertyChanged(() => Items);
            }
        }
    }

    public class FileItem : BaseNotifyPropertyChanged
    {
        private string _fileName;
        private string _folderName;
        private string _divider;
        private string _alert1;
        private string _alert2;
        private string _date;
        private string _status;
        private string _filePath;

        public FileItem()
        {

        }

        public FileItem(string fileName, string folderName, string divider, string alert1, string alert2, string date, string status, string filePath)
        {
            FileName = fileName;
            FolderName = folderName;
            Divider = divider;
            Alert1 = alert1;
            Alert2 = alert2;
            Date = date;
            FilePath = filePath;
        }

        public string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
                RaisePropertyChanged(() => FileName);
            }
        }

        public string FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
                RaisePropertyChanged(() => FilePath);
            }
        }

        public string FolderName
        {
            get
            {
                return _folderName;
            }
            set
            {
                _folderName = value;
                RaisePropertyChanged(() => FolderName);
            }
        }

        public string Divider
        {
            get
            {
                return _divider;
            }
            set
            {
                _divider = value;
                RaisePropertyChanged(() => Divider);
            }
        }

        public string Alert1
        {
            get
            {
                return _alert1;
            }
            set
            {
                _alert1 = value;
                RaisePropertyChanged(() => Alert1);
            }
        }

        public string Alert2
        {
            get
            {
                return _alert2;
            }
            set
            {
                _alert2 = value;
                RaisePropertyChanged(() => Alert2);
            }
        }

        public string Date
        {
            get
            {
                return _date;
            }
            set
            {
                _date = value;
                RaisePropertyChanged(() => Date);
            }
        }

        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                RaisePropertyChanged(() => Status);
            }
        }

    }
}
