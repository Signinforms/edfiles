﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;

namespace eScanner.Model
{
    public class ScannedItem : BaseNotifyPropertyChanged
    {
        #region Properties

        private string _fileName;
        private string _folderName;
        private string _divider;
        private string _alert1;
        private string _alert2;
        private string _date;
        private string _status;

        private DataView _scannedItems;
        private DataRowView _selectedItems;

        public event PropertyChangedEventHandler PropertyChanged;

        public ICommand SaveData { get; private set; }

        #endregion

        #region Properties Get/Set

        public string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
                RaisePropertyChanged(() => FileName);
            }
        }

        public string FolderName
        {
            get
            {
                return _folderName;
            }
            set
            {
                _folderName = value;
                RaisePropertyChanged(() => FolderName);
            }
        }

        public string Divider
        {
            get
            {
                return _divider;
            }
            set
            {
                _divider = value;
                RaisePropertyChanged(() => Divider);
            }
        }

        public string Alert1
        {
            get
            {
                return _alert1;
            }
            set
            {
                _alert1 = value;
                RaisePropertyChanged(() => Alert1);
            }
        }

        public string Alert2
        {
            get
            {
                return _alert2;
            }
            set
            {
                _alert2 = value;
                RaisePropertyChanged(() => Alert2);
            }
        }

        public string Date
        {
            get
            {
                return _date;
            }
            set
            {
                _date = value;
                RaisePropertyChanged(() => Date);
            }
        }

        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                RaisePropertyChanged(() => Status);
            }
        }

        public DataView ScannedItems
        {
            get 
            {
                return _scannedItems;
            }
            set
            {
                _scannedItems = value;
                RaisePropertyChanged(() => ScannedItems);
            }
        }

        public DataRowView SelectedItems
        {
            get { return _selectedItems; }
            set
            {
                _selectedItems = value;
                RaisePropertyChanged(() => SelectedItems);
            }
        }

        #endregion

        public ScannedItem()
        {
            SaveData = new MethodCommand(EditFileDetails);
            GetDetails();
        }

        public ScannedItem(EditScannedItem item)
        {
            FileName = item.FileName;
            Alert1 = item.Alert1;
            Alert2 = item.Alert2;
        }

        public ScannedItem(string fileName, string folderName, string divider, string alert1, string alert2, string date, string status)
        {
            FileName = fileName;
            FolderName = folderName;
            Divider = divider;
            Alert1 = alert1;
            Alert2 = alert2;
            Date = date;
        }

        public void EditFileDetails()
        {
            FileName = SelectedItems["FileName"].ToString();
            Alert1 = SelectedItems["Alert1"].ToString();
            Alert2 = SelectedItems["Alert2"].ToString();
        }

        public void GetDetails()
        {
            var data = new ObservableCollection<ScannedItem>
            {
                new ScannedItem("abc.pdf","abc","","a1","a2",DateTime.Today.ToString("dd/MM/yyyy"),"N/A"),
                new ScannedItem("def.pdf","abc","","a1","a2",DateTime.Today.ToString("dd/MM/yyyy"),"N/A"),
                new ScannedItem("123.pdf","abc","","a1","a2",DateTime.Today.ToString("dd/MM/yyyy"),"N/A")
            };

            //ListCollectionView collection = new ListCollectionView(data);
            //collection.GroupDescriptions.Add(new PropertyGroupDescription("FileName"));

            DataTable table = new DataTable();

            table.Columns.Add("FileName");
            table.Columns.Add("FolderName");
            table.Columns.Add("Divider");
            table.Columns.Add("Alert1");
            table.Columns.Add("Alert2");
            table.Columns.Add("Date");
            table.Columns.Add("Status");

            foreach (var element in data)
            {
                var newRow = table.NewRow();
                newRow["FileName"] = element.FileName;
                newRow["FolderName"] = element.FolderName;
                newRow["Divider"] = element.Divider;
                newRow["Alert1"] = element.Alert1;
                newRow["Alert2"] = element.Alert2;
                newRow["Date"] = element.Date;
                newRow["Status"] = element.Status;

                table.Rows.Add(newRow);
            }

            ScannedItems = table.DefaultView;

        }

    }
}
