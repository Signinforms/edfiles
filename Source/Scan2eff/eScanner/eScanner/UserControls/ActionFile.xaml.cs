﻿using eScanner.Interfaces;
using eScanner.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace eScanner.UserControls
{
    /// <summary>
    /// Interaction logic for Action.xaml
    /// </summary>
    public partial class ActionFile : UserControl,IInitializable,
        INotify,
        IRefreshable
    {
        public ActionFile()
        {
            InitializeComponent();
            this.DataContext = new ActionFileViewModel();
        }

        #region IInitializable Members
        /// <summary>
        /// Gets or sets the ancestor control.
        /// </summary>
        /// <value>The ancestor control.</value>
        public Control AncestorControl { get; set; }

        /// <summary>
        /// Initializes the specified initialization data.
        /// </summary>
        /// <param name="initializationData">The initialization data.</param>
        public void Initialize(object initializationData)
        {
        }

        #endregion IInitializable Members

        #region INotify Members
        /// <summary>
        /// Occurs when [notify].
        /// </summary>
        public event NotifyEventHandler Notify;
        #endregion INotify Members

        #region IRefreshable Members
        /// <summary>
        /// Refreshes this instance.
        /// </summary>
        public void Refresh()
        {
            this.RaiseEvent(new RoutedEventArgs(System.Windows.FrameworkElement.SizeChangedEvent));
        }
        #endregion IRefreshable Members

        private void cboFolder_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
