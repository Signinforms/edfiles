﻿using eScanner.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace eScanner.UserControls
{
    /// <summary>
    /// Interaction logic for EditViewer.xaml
    /// </summary>
    public partial class EditViewer : Window
    {
        public EditViewer()
        {
            InitializeComponent();
            this.DataContext = (DataContext as FileItem);
           
        }

        public FileItem OldFileItem;

        public EditViewer(FileItem data)
        {
            InitializeComponent();
            OldFileItem = new FileItem(data.FileName, data.FolderName, data.Divider, data.Alert1, data.Alert2, data.Date, data.Status, data.FilePath);
            this.DataContext = data;
            //this.DataContext = data.SelectedItems .Row;
            //txtFileName.Text = data.SelectedItems.Row["FileName"].ToString();
            //txtAlert1.Text = data.SelectedItems.Row["Alert1"].ToString();
            //txtAlert2.Text = data.SelectedItems.Row["Alert2"].ToString();
        }

        private void btnCancle_Click(object sender, RoutedEventArgs e)
        {
            this.DataContext = OldFileItem;
            DialogResult = false;
            this.Close();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {

            //EditViewer ev = (Application.Current.MainWindow as MainWindow).grdHeader.Children.OfType<EditViewer>().FirstOrDefault();
           // GeneralViewer gv = new GeneralViewer(this.DataContext as EditScannedItem);
            DialogResult = true;
            this.Close();
        }
    }
}
