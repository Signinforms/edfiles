﻿using eScanner.Interfaces;
using eScanner.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace eScanner.UserControls
{
    /// <summary>
    /// Interaction logic for GeneralViewer.xaml
    /// </summary>
    public partial class GeneralViewer : UserControl, INotify
    {
        public GeneralViewer()
        {
            InitializeComponent();
            this.DataContext = new FileList();
            //myDataGrid.ItemsSource = new DirectoryInfo(@"C:\TempFiles").GetFiles();
        }

        //public GeneralViewer(EditScannedItem item)
        //{
        //    this.DataContext = new ScannedItem(item);
        //}

        #region INotify Members
        /// <summary>
        /// Occurs when [notify].
        /// </summary>
        public event NotifyEventHandler Notify;
        #endregion INotify Members


        private void grid_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            string path = (this.DataContext as eScanner.Model.FileList).SelectedItem.FilePath.ToString() + "//" + (this.DataContext as eScanner.Model.FileList).SelectedItem.FileName.ToString();
            (Application.Current.MainWindow as MainWindow).tabViewer.Tag = path;
            (Application.Current.MainWindow as MainWindow).tabViewer.IsSelected = true;
        }
    }
}
