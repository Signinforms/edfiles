﻿using eScanner.BO;
using eScanner.eScannerService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace eScanner.UserControls
{
    /// <summary>
    /// Interaction logic for PDFViewer.xaml
    /// </summary>
    public partial class PDFViewer : UserControl
    {
        public PDFViewer()
        {
            InitializeComponent();
        }

        public PDFViewer(string path)
        {
            InitializeComponent();
            try
            {
                if (!string.IsNullOrEmpty(path))
                {
                    path = path.Replace("////", "/");
                    path = path.Replace("//", "/");
                    path = "file:///" + path;
                    pdfWebViewer.Navigate(path);
                }
            }
            catch (Exception exc)
            {
 
            }
            
        }
    }
}
