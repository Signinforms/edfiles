using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace MTOM_Library
{
    /// <summary>
    /// A class to upload a file to a web server using WSE 3.0 web services, with the MTOM standard.
    /// To use this class, drag/drop an instance onto a windows form, and create event handlers for ProgressChanged
    /// and RunWorkerCompleted.  
    /// Make sure to specify the LocalFilePath before you call RunWorkerAsync() to begin the upload
    /// </summary>
    public class FileTransferUpload : FileTransferBase
    {
        //subFileName为上传到服务器后，位于服务器的文件的物理相对路径和名称，可以与本地的物理文件不同
        public string subFileName = string.Empty;


        /// <summary>
        /// Start the upload operation synchronously.
        /// The argument must be the start position, usually 0
        /// </summary>
        public void RunWorkerSync(DoWorkEventArgs e)
        {
            OnDoWork(e);
            base.OnRunWorkerCompleted(new RunWorkerCompletedEventArgs(e.Result, null, false));
        }

        /// <summary>
        /// This method starts the uplaod process. It supports cancellation, reporting progress, and exception handling.
        /// The argument is the start position, usually 0
        /// </summary>
        protected override void OnDoWork(DoWorkEventArgs e)
        {
            base.OnDoWork(e);

            this.Offset = Int64.Parse(e.Argument.ToString());
            int numIterations = 0;

            //LocalFilePath为本地文件的物理路径, LocalFileName为本地文件的物理名称
            this.LocalFileName = Path.GetFileName(this.LocalFilePath); //这里就有一个非规范的文件名scan0001.pdf
            if (this.AutoSetChunkSize)
                this.ChunkSize = 16*1024; // 16Kb default

            if (!File.Exists(LocalFilePath))
                throw new Exception(String.Format("Could not find file {0}", LocalFilePath));

            byte[] buffer = new byte[ChunkSize];

            using (FileStream fs = new FileStream(this.LocalFilePath, FileMode.Open, FileAccess.Read))
            {
                fs.Position = this.Offset;
                int bytesRead;

                do
                {
                    bytesRead = fs.Read(buffer, 0, ChunkSize);

                    if (bytesRead != buffer.Length)
                    {
                        this.ChunkSize = bytesRead;
                        byte[] trimmedBuffer = new byte[bytesRead];
                        Array.Copy(buffer, trimmedBuffer, bytesRead);
                        buffer = trimmedBuffer;
                    }
                    if (buffer.Length == 0)
                        break;

                    try
                    {
                        //获取相对的路径，用于保存定位文件保存到服务器的文件路径
                        if (string.IsNullOrEmpty(this.subFileName))
                            this.subFileName = this.LocalFileName;
                        else
                            this.subFileName = this.subFileName.TrimStart('\\');
                        //OfficeUser\Alexander, Steve 203186SA\scan0001.pdf

                        if (this.Offset == 0)
                            Console.WriteLine("Uploading a file: " + this.LocalFileName);

                        //但是对于scan001.pdf类似的非规范格式的文件就需要处理了(OfficeUser\Baello, Sean.pdf)
                        this.WebService.AppendChunk(this.subFileName, buffer, this.Offset);

                        if (this.Offset == 0)
                            Console.WriteLine("Uploaded a file: " + this.LocalFileName);

                        this.Offset += bytesRead;
                    }
                    catch (Exception ex)
                    {
                        fs.Position -= bytesRead;

                        if (NumRetries++ >= MaxRetries) // 失败次数过多，退出
                        {
                            fs.Close();
                            throw new Exception(String.Format("Error occurred during upload, too many retries. \n{0}",
                                                              ex.ToString()));
                        }
                    }
                    numIterations++;
                } while (bytesRead > 0 && !this.CancellationPending);
            }

            try
            {
                //获取服务器端的文件名称
                string result, pureFileName = Path.GetFileNameWithoutExtension(this.subFileName);

                if (this.autoFile)
                {
                    result = this.WebService.UploadAutoSuccessStatus(pureFileName, this.subFileName, this.dividerName,
                                                                     this.folderName, this.userName);
                }
                else
                {
                    result = this.WebService.UploadSuccessStatus(pureFileName, this.subFileName);
                }

                Console.WriteLine(result);
                
            }
            catch (Exception exp)
            {

                Console.WriteLine("UploadSuccessStatus: " + exp.Message);
            }
        }
    }
}