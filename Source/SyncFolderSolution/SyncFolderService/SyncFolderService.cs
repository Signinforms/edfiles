﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Net;
using System.ServiceProcess;
using System.Threading;
using System.Web.UI;
using AJiwani.eFileFolders.Utils;
using MTOM_Library;
using MTOM_Library.MtomWebService;
using Schedule;
using AJiwani.eFileFolders.Service;

namespace AJiwani.eFileFolders.Service
{
    public partial class SyncFolderService : ServiceBase
    {
        public static MTOMWse webService;
        protected string hourMinute;
        protected string localRoot;

        protected string password;
        protected ScheduleTimer tickTimer;
        protected string userId;

        public SyncFolderService()
        {
            InitializeComponent();

            AttachWindows = new ArrayList();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                EventLogHelper.WriteInformation("Starting SyncFolder Service");
                hourMinute = ConfigurationManager.AppSettings["Timer"];
                userId = ConfigurationManager.AppSettings["UserID"];
                password = ConfigurationManager.AppSettings["Password"];
                localRoot = ConfigurationManager.AppSettings["Volume"];

                ThreadPool.SetMaxThreads(4, 8);
            }
            catch (Exception exp)
            {
                EventLogHelper.WriteError(exp.Message);
                return;
            }

            try
            {
                webService = new MTOMWse();
                webService.RequireMtom = true;
                webService.Timeout = Timeout.Infinite;

                tickTimer = new ScheduleTimer();
                tickTimer.Elapsed += TickTimer_Elapsed;
                tickTimer.AddEvent(new ScheduledTime(EventTimeBase.Daily, TimeSpan.Parse(hourMinute)));

                tickTimer.Start();
                EventLogHelper.WriteInformation("Started SyncFolder Service");
            }
            catch (Exception exp)
            {
                EventLogHelper.WriteError(exp.Message);
            }
            
        }

        protected override void OnStop()
        {
            try
            {
                EventLogHelper.WriteInformation("Stop SyncFolder Service");
                tickTimer.Stop();
                tickTimer.Dispose();
                tickTimer = null;
            }
            catch(Exception exp)
            {
                EventLogHelper.WriteError("Stop SyncFolder Service: " + exp.Message);
            }
        }

        public void Start()
        {
            OnStart(null);
        }

        public void StopE()
        {
            OnStop();
        }

        private void TickTimer_Elapsed(object sender, ScheduledEventArgs e)
        {
            try
            {
                EventLogHelper.WriteInformation("Started TickTimer_Elapsed");

                #region uploading 
                AuthenticateWebService();
                if (Directory.Exists(localRoot))
                {
                    //Get users list
                    string[] users = Directory.GetDirectories(localRoot);
                    string username = string.Empty, foldername = string.Empty, dividername = string.Empty;

                    //Go through this users (identify a user)
                    foreach (string user in users)
                    {
                        username = user.Substring(user.LastIndexOf('\\') + 1);

                        #region Case 1. Go through the pdf files list(format pdf names)

                        //writed by johnlu655 2011-01-06
                        string[] pdfFilesWithFormatName = Directory.GetFiles(user);
                        string formatFileNameWithoutPath;
                        foreach (string pdfFile in pdfFilesWithFormatName)
                        {
                            formatFileNameWithoutPath = Path.GetFileNameWithoutExtension(pdfFile);
                            //Just only upload format ones
                            if (IsValidFormatName(formatFileNameWithoutPath))
                            {
                                UploadAutoFiles(new string[] {pdfFile},
                                                formatFileNameWithoutPath,
                                                formatFileNameWithoutPath,
                                                username);
                            }
                        }

                        #endregion end pdf files list

                        #region Case 2. Go through the folder list(format folder names)

                        //Get the folder name list of eff
                        string[] folderPaths = Directory.GetDirectories(user);
                        string[] files;
                        foreach (string folder in folderPaths)
                        {
                            #region Case 2.1 Get files with invalid and valid format

                            foldername = folder.Substring(folder.LastIndexOf('\\') + 1);
                            files = Directory.GetFiles(folder);

                            if (IsValidFormatName(foldername))
                            {
                                UploadFiles(files, foldername, username);
                            }
                            else
                            {
                                //upload file request files which is under a folder
                                UploadFiles(files);
                            }

                            #endregion

                            #region Case 2.2 Get divider name

                            if (Directory.Exists(folder))
                            {
                                string[] dividers = Directory.GetDirectories(folder);

                                foreach (string divider in dividers)
                                {
                                    //Get the files with invalid and valid format under divider folder, 
                                    //we needn't take care of the format here
                                    string[] pdfFiles = Directory.GetFiles(divider);
                                    dividername = divider.Substring(divider.LastIndexOf('\\') + 1);

                                    UploadAutoFiles(pdfFiles, dividername, foldername, username);
                                }
                            }

                            #endregion
                        }

                        #endregion go through folder list
                    }
                }

                #endregion
            }
            catch (Exception)
            {}
        }

        private void UploadFiles(string[] files, string folderName, string userName)
        {
            bool isValidFolderName = IsValidFormatName(folderName);

            foreach (string file in files)
            {
                string guid = Guid.NewGuid().ToString();
                string fileName = Path.GetFileNameWithoutExtension(file);
                if (IsValidFormatName(fileName))
                {
                    UploadFile(new Triplet(guid, file, 0));
                }
                else
                {
                    if (isValidFolderName) //c:\storage\OfficeUser\Alexander, Steve 203186SA\scan0001.pdf
                    {
                        UploadAutoFiles(new string[] {file},
                                        folderName,
                                        folderName,
                                        userName);
                    }
                }
            }
        }

        private bool IsValidFormatName(string fileName)
        {
            string[] tokens = fileName.Split(new char[] {'-',','}, StringSplitOptions.RemoveEmptyEntries);
            return tokens.Length == 2;
        }

        private void UploadFiles(string[] files)
        {
            foreach (string file in files)
            {
                string guid = Guid.NewGuid().ToString();
                UploadFile(new Triplet(guid, file, 0));
            }
        }

        private void UploadAutoFiles(string[] files, string divider, string folder, string username)
        {
            foreach (string file in files)
            {
                string guid = Guid.NewGuid().ToString();
                UploadAutoFile(new Triplet(guid, file, 0), divider, folder, username);
            }
        }

        private bool AuthenticateWebService()
        {
            if (webService.CookieContainer != null && webService.CookieContainer.Count > 0)
                return true; // already authenticated.

            // send a HTTP web request to the login.aspx page, using the querystring to pass in username and password
            string postData = String.Format("?Username={0}&Password={1}", userId, password);
            string url = webService.Url.Replace("MTOM.asmx", "") + "Login.aspx" + postData;
            // get the path of the login page, assuming it is in the same folder as the web service
            HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;
            req.CookieContainer = new CookieContainer();
            HttpWebResponse response = (HttpWebResponse) req.GetResponse();

            // copy the cookie container to the web servicreqes
            webService.CookieContainer = req.CookieContainer;

            return (response.Cookies.Count > 0);
            // true if the server sent an auth cookie, i.e. authenticated successfully
        }


        /// <summary>
        /// This method is used as a thread start function.
        /// The parameter is a Triplet because the ThreadStart can only take one object parameter
        /// </summary>
        /// <param name="triplet">First=guid; Second=path; Third=offset</param>
        private void UploadFile(object triplet)
        {
            string guid = (triplet as Triplet).First.ToString();
            string path = (triplet as Triplet).Second.ToString();
            long offset = Int64.Parse((triplet as Triplet).Third.ToString());

            FileTransferUpload ftu = new FileTransferUpload();
            ftu.WebService.CookieContainer = webService.CookieContainer;
            // copy the CookieContainer into the transfer object (for auth cookie, if relevant)
            ftu.Guid = guid;

            // set up the chunking options
            ftu.AutoSetChunkSize = false;
            ftu.ChunkSize = 16*1024; // kb

            // set the remote file name and start the background worker
            ftu.LocalFilePath = path;
            ftu.subFileName = path.Substring(localRoot.Length);
            ftu.IncludeHashVerification = false;

            ftu.RunWorkerCompleted += ft_RunWorkerCompleted;
            ftu.RunWorkerSync(new DoWorkEventArgs(offset));
        }

        /// <summary>
        /// This method is used as a thread start function.
        /// The parameter is a Triplet because the ThreadStart can only take one object parameter
        /// </summary>
        /// <param name="triplet">First=guid; Second=path; Third=offset</param>
        /// <param name="divider"></param>
        /// <param name="folder"></param>
        /// <param name="username"></param>
        private void UploadAutoFile(object triplet, string divider, string folder, string username)
        {
            string guid = (triplet as Triplet).First.ToString();
            string path = (triplet as Triplet).Second.ToString();
            long offset = Int64.Parse((triplet as Triplet).Third.ToString());

            FileTransferUpload ftu = new FileTransferUpload();
            ftu.WebService.CookieContainer = webService.CookieContainer;
            // copy the CookieContainer into the transfer object (for auth cookie, if relevant)
            ftu.Guid = guid;

            // set up the chunking options
            ftu.AutoSetChunkSize = false;
            ftu.autoFile = true;
            ftu.ChunkSize = 16*1024; // kb
            ftu.userName = username;
            ftu.folderName = folder;
            ftu.dividerName = divider;

            // set the remote file name and start the background worker
            ftu.LocalFilePath = path;
            ftu.subFileName = GetFormatServerName(path, folder);
            ftu.IncludeHashVerification = false;

            ftu.RunWorkerCompleted += ft_RunWorkerCompleted;
            ftu.RunWorkerSync(new DoWorkEventArgs(offset));
        }

        private string GetFormatServerName(string path, string folder)
        {
            string relativePath = path.Substring(localRoot.Length);
            string localFileName = Path.GetFileNameWithoutExtension(path);
            string extension = Path.GetExtension(path);
            string directory = Path.GetDirectoryName(relativePath);

            if (!IsValidFormatName(localFileName) && IsValidFormatName(folder))
            {
                if (directory != null) return Path.Combine(directory, string.Concat(folder, extension));
            }

            return relativePath;
        }

        private void ft_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            string localfile = (sender as FileTransferBase).LocalFilePath;

            Console.WriteLine("Complete the file: " + localfile);

            if (e.Error != null || e.Cancelled)
            {
                return;
            }
            else
            {
                try
                {
                    // Remove the old file
                    if (File.Exists(localfile))
                    {
                        File.Delete(localfile);
                    }

                    //
                    //string path = Path.GetDirectoryName(localfile);
                    //if (Directory.GetDirectories(path).Length == 0 && Directory.GetFiles(path).Length == 0)
                    //{
                    //    Directory.Delete(path);
                    //}
                }
                catch (Exception exp)
                {
                    Console.WriteLine("Error the file: " + localfile + "- " + exp.Message);
                }
            }
        }

        private bool connected = false;
        private ArrayList AttachWindows;
        private IntPtr thisHandle;

        public void SendMessage(string message)//发送消息
        {
            if (!connected)
                throw new Exception("not connected!");

            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(message);
            int length = bytes.Length;

            COPYDATASTRUCT copyData = new COPYDATASTRUCT();
            copyData.dwData = (IntPtr)1;
            copyData.lpData = message;
            copyData.cbData = length + 1;

            System.Threading.Thread.Sleep(100);
            lock (AttachWindows.SyncRoot)
            {
                foreach (int attachWin in AttachWindows)
                {
                    try
                    {
                        User32.SendMessage((IntPtr)attachWin, User32.WM_COPYDATA, thisHandle.ToInt32(), ref copyData);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("windows send message error", e);
                    }
                }
            }
        }
    }
}