/********************************
 * by PeanutLee 
 * jiecongli@gmail.com
 * http://www.cnblogs.com/peanutlee 
 ********************************/

using System;
using System.Runtime.InteropServices;
using HANDLE = System.IntPtr;
using HWND = System.IntPtr;

namespace AJiwani.eFileFolders.Utils
{
    [StructLayout(LayoutKind.Sequential)]
    public struct CWPSTRUCT
    {
        public IntPtr lparam;
        public IntPtr wparam;
        public int message;
        public IntPtr hwnd;
    }

    public struct COPYDATASTRUCT
    {
        public IntPtr dwData;
        public int cbData;
        [MarshalAs(UnmanagedType.LPStr)]
        public string lpData;
    }

    public struct POINT
    {
        public int x;
        public int y;
    }

    public struct MSG
    {
        public HWND hwnd;
        public int message;
        public int wParam;
        public int lParam;
        public int time;
        public POINT pt;
    }

    public class User32
    {
        public const int WM_COPYDATA = 0x4A;
        public const int WM_QUEUE_NOTIFY = 0x401;

        public delegate int DHookProc(int nCode, IntPtr wParam, ref CWPSTRUCT cwp);

        [DllImport("user32", EntryPoint = "PostMessage")]
        public static extern int PostMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);
        [DllImport("user32", EntryPoint = "RegisterWindowMessage")]
        public static extern int RegisterWindowMessage(string lpString);
        [DllImport("user32", EntryPoint = "PeekMessage")]
        public static extern int PeekMessage(ref MSG lpMsg, HWND hwnd, int wMsgFilterMin, int wMsgFilterMax, int wRemoveMsg);
        [DllImport("user32", EntryPoint = "DispatchMessage")]
        public static extern int DispatchMessage(ref MSG lpMsg);
        [DllImport("user32", EntryPoint = "SendMessage")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, ref COPYDATASTRUCT lParam);

        [DllImport("user32.dll")]
        public static extern int FindWindow(string lpclassname, string lpwindowname);

        [DllImport("user32.dll")]
        public static extern int FindWindowEx(IntPtr hwndparent, IntPtr hwndchildafter, string lpszclass, string lpszwindow); 

        public static IntPtr HWND_BROADCAST
        {
            get { return (IntPtr)0xFFFF; }
        }
    }
}

