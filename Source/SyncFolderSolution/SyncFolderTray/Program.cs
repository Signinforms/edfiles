﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.ServiceProcess;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.IO;

namespace AJiwani.eFileFolder.Tray
{
    static class Program
    {
        static Mutex mutex = null;

        const string  SERVICE_NAME="SyncFolderService";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            mutex = new Mutex(true, "SyncFolderService");
            if (mutex.WaitOne(0, false))
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                ServiceController serviceController = new ServiceController();

                serviceController.MachineName = Environment.MachineName;
                serviceController.ServiceName = SERVICE_NAME;

                bool isSuccessed= true;

                if (!CheckServiceExisted(serviceController))
                {
                    isSuccessed = false;
                }
                
                if(isSuccessed)
                {
                    Application.Run(new TrayForm(serviceController));
                }
            }
        }

        private static bool CheckServiceExisted(ServiceController serviceController)
        {
            try
            {
                if (serviceController.ServiceHandle != null)
                    return true;
            }
            catch
            {
                MessageBox.Show("Cound not find SyncFolder service, be sure the service has installed already.");
            }               
            
            return false;
        }
    }
}