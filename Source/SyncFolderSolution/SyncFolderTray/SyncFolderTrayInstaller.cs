using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;

namespace AJiwani.eFileFolder.Tray
{
    [RunInstaller(true)]
    public partial class SyncFolderTrayInstaller : Installer
    {
        public SyncFolderTrayInstaller()
        {
            InitializeComponent();
        }
    }
}