﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.ServiceProcess;
using System.Configuration;

namespace AJiwani.eFileFolder.Tray
{
    public partial class TrayForm : WinMsgBase
    {

        ServiceController serviceController = null;

        public TrayForm(ServiceController serviceController)
        {
            this.serviceController = serviceController;
    
            InitializeComponent();
            UpdateMenuItem();

            base.OnMessage += new WindowsMessageHandler(TrayForm_OnMessage);

        }

        private void TrayForm_OnMessage(object sender, WindowsMessageEventArgs e)
        {
            
        }

        private void UpdateMenuItem()
        {
            serviceController.Refresh();
            startMenuItem.Enabled = serviceController.Status == ServiceControllerStatus.Stopped;
            stopMenuItem.Enabled = serviceController.CanStop;
            restartMenuItem.Enabled = serviceController.CanStop;
            
            if (startMenuItem.Enabled)
                notifyIcon.Icon = Properties.Resources.p3oEnetresdis;            
            else
                notifyIcon.Icon = Properties.Resources.p3oEnetres;

        }

        private void contextMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Tag.ToString())
            {
                case "Exit":
                    Close();
                    return;
                case "Start":
                    StartService();
                    break;
                case "Stop":
                    StopService();
                    break;
                case "Restart":
                    RestartService();
                    break;
                case "Settings":
                    SettingsForm settngsForm = new SettingsForm();
                    if(settngsForm.ShowDialog()==DialogResult.OK)
                    {
                        RestartService();
                    }
                    
                    break;
            }

            UpdateMenuItem();
        }

        private void RestartService()
        {
            serviceController.Stop();
            serviceController.WaitForStatus(ServiceControllerStatus.Stopped);
            serviceController.Start();
            serviceController.WaitForStatus(ServiceControllerStatus.Running);
        }

        private void StopService()
        {
            serviceController.Stop();
            serviceController.WaitForStatus(ServiceControllerStatus.Stopped);
        }

        private void StartService()
        {
            try
            {
                serviceController.Start();
                serviceController.WaitForStatus(ServiceControllerStatus.Running);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void contextMenu_Opening(object sender, CancelEventArgs e)
        {
            //check the service status to set menu items correctly
            UpdateMenuItem();
        }
    }
}