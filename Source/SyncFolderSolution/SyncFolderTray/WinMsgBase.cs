/********************************
 * by PeanutLee 
 * jiecongli@gmail.com
 * http://www.cnblogs.com/peanutlee 
 ********************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Data;

namespace AJiwani.eFileFolder.Tray
{
    public class WinMsgBase : System.Windows.Forms.Form
    {
        /// <summary>
        /// 已做消息队列，保证所有接收到的消息都将触发OnMessage事件，并且不会阻塞消息发送放。
        /// 但是如果该事件没有人订阅，那么将丢弃消息
        /// </summary>
        public event WindowsMessageHandler OnMessage;
        public delegate void WindowsMessageHandler(object sender, WindowsMessageEventArgs e);

        #region [ ==== Fields ==== ]
        private const string DefaultAttachWindowMessage = "PeanutLee.DefaultAttachWinMsgString";
        private Queue<WindowsMessageEventArgs> incomingMessageQueue = new Queue<WindowsMessageEventArgs>();
        private object messagsQueueLock = new object();

        private int attachMessage;
        private string attachMessageString;
        private bool connected = false;
        private ArrayList AttachWindows;
        private IntPtr thisHandle;
      
        #endregion

        #region [ ==== Properties ==== ]
        public string AttachMessageString
        {
            set { attachMessageString = value; }
        }
        
        #endregion

        #region [ ==== Construct ==== ]
        /// <summary>
        /// RegisterWindowMessage with default string : "PeanutLee.DefaultAttachWinMsgString".
        /// </summary>
        public WinMsgBase() : this (DefaultAttachWindowMessage){}

        /// <summary>
        /// RegisterWindowMessage with param string 
        /// </summary>
        /// <param name="attachMessageString"></param>
        public WinMsgBase(string attachMessageString)
        {
            thisHandle = this.Handle;
            AttachWindows = new ArrayList();
            this.attachMessageString = attachMessageString;
            this.attachMessage = User32.RegisterWindowMessage(attachMessageString);
            Connect();
        }

        #endregion

        #region [ ==== Override ==== ]
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == attachMessage &&
                m.WParam.ToInt32() != 0 &&
                m.WParam.ToInt32() != this.Handle.ToInt32())
            {
                if (m.LParam.ToInt32() == 1)
                    User32.PostMessage(m.WParam, (int)this.attachMessage, this.Handle.ToInt32(), 0);//回复

                lock (AttachWindows)
                {
                    if (m.LParam.ToInt32() == -1)
                    {
                        AttachWindows.Remove(m.WParam.ToInt32());//删除窗体句柄
                        if (AttachWindows.Count == 0)
                            MessageNotify("All AttachWindows closed");
                    }
                    else //0，1
                        AttachWindows.Add(m.WParam.ToInt32());//保存窗体句柄
                }
            }
            switch (m.Msg)
            {
                case User32.WM_COPYDATA:
                    COPYDATASTRUCT copyData = new COPYDATASTRUCT();
                    copyData = (COPYDATASTRUCT)m.GetLParam(copyData.GetType());
                    MessageNotify(m.WParam.ToInt32(), copyData.lpData);
                    break;
                case User32.WM_QUEUE_NOTIFY:
                    if (incomingMessageQueue.Count == 0)
                        break;

                    WindowsMessageEventArgs tempArgs;
                    lock (messagsQueueLock)
                    {
                        tempArgs = incomingMessageQueue.Dequeue();
                    }
                    if (OnMessage != null)
                        OnMessage(this.Handle.ToInt32(), tempArgs);

                    if (incomingMessageQueue.Count > 0)
                        User32.PostMessage(this.Handle, User32.WM_QUEUE_NOTIFY, 0, 0); //下一个
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }

        protected override void Dispose(bool disposing)
        {
            User32.PostMessage(User32.HWND_BROADCAST, (int)this.attachMessage, 
                thisHandle.ToInt32(), -1);//让其他人删除别再给我发消息
            base.Dispose(disposing);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region [ ==== Public Methods ==== ]
        public void SendMessage(string message)//发送消息
        {
            if (!connected)
                throw new Exception("not connected!");

            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(message);
            int length = bytes.Length;

            COPYDATASTRUCT copyData = new COPYDATASTRUCT();
            copyData.dwData = (IntPtr)1;
            copyData.lpData = message;
            copyData.cbData = length + 1;

            System.Threading.Thread.Sleep(100);
            lock (AttachWindows.SyncRoot)
            {
                foreach (int attachWin in AttachWindows)
                {
                    try
                    {
                        User32.SendMessage((IntPtr)attachWin, User32.WM_COPYDATA, thisHandle.ToInt32(), ref copyData);
                    }
                    catch (Exception e)
                    {
                        throw new Exception("windows send message error", e);
                    }
                }
            }
        }

        /// <summary>
        /// 发送消息到目标窗体
        /// </summary>
        /// <param name="tarWin">目标窗体的句柄</param>
        /// <param name="message">消息体</param>
        public void SendMessage(int tarWin, string message)
        {
            if (!connected)
                throw new Exception("not connected!");

            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(message);
            int length = bytes.Length;

            COPYDATASTRUCT copyData = new COPYDATASTRUCT();
            copyData.dwData = (IntPtr)1;
            copyData.lpData = message;
            copyData.cbData = length + 1;

            System.Threading.Thread.Sleep(100);
            try
            {
                User32.SendMessage((IntPtr)tarWin, User32.WM_COPYDATA, thisHandle.ToInt32(), ref copyData);
            }
            catch (Exception e)
            {
                throw new Exception("windows send message error", e);
            }
        }

        /// <summary>
        /// 消息通知,不阻塞消息,将消息的处理交由本窗体线程,将激发OnMessage
        /// </summary>
        /// <param name="message"></param>
        public void MessageNotify(string message)
        {
            MessageNotify(thisHandle.ToInt32(), message);
        }

        /// <summary>
        /// 消息通知,
        /// 为了不阻塞消息,启用消息队列:对收到的消息进行入队,并PostMessage本窗体线程
        /// </summary>
        /// <param name="target">消息来源</param>
        /// <param name="message">消息体</param>
        private void MessageNotify(int target, string message)
        {
            lock (messagsQueueLock)
            {
                incomingMessageQueue.Enqueue(new WindowsMessageEventArgs(target, message));
            }
            User32.PostMessage(thisHandle, User32.WM_QUEUE_NOTIFY, 0, 0);
        }

        private void Connect()
        {
            if (User32.PostMessage(User32.HWND_BROADCAST, (int)this.attachMessage, thisHandle.ToInt32(), 1) == 0)//連接
                throw new Exception("PostMessage failed.");
            else
            {
                //AttachWindows.Add(this.Handle.ToInt32());//自己将收到自己发送的消息
                connected = true;
            }
        }

        #endregion 
    }

    public class WindowsMessageEventArgs : EventArgs
    {
        #region [ ==== Fields ==== ]
        private int targetWinHandle;
        private string data; 

        #endregion

        public WindowsMessageEventArgs(int targetWinHandle, string data)
        {
            this.targetWinHandle = targetWinHandle;
            this.data = data;
        }

        #region [ ==== Properties ==== ]
        /// <summary>
        /// 消息发送者窗体句柄值
        /// </summary>
        public int TargetWinHandle
        {
            get { return targetWinHandle; }
        }

        public string Data
        {
            get { return data; }
        } 

        #endregion
    }
}