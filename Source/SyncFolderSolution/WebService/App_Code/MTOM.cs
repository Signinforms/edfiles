using System;
using System.Data;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Security;
using System.Security.Cryptography;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.Services;
using System.Web.Services.Protocols;
using DataQuicker2.Framework;
using Shinetech.DAL;

/// <summary>
/// A set of methods to upload and download chunks of a file using MTOM
/// </summary>
[WebService(Namespace = "http://www.efilefolders.com/soap/MTOMWebServices.asp")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class MTOM : System.Web.Services.WebService
{
    private string UploadPath;
    private static char splitter_token = '-';
    private static char splitter_token1 = ',';
     
    public MTOM()
    {
		// check that the upload folder exists
		string uploadConfigSetting = ConfigurationManager.AppSettings["UploadPath"].ToString();
		if(Path.IsPathRooted(uploadConfigSetting))
			UploadPath = uploadConfigSetting;
		else
			UploadPath = Server.MapPath(uploadConfigSetting);
        if (!Directory.Exists(UploadPath))
            CustomSoapException("Upload Folder not found", "The folder " + UploadPath + " does not exist");
    }    
	
    /// <summary>
    /// A dummy method to check the connection to the web service
    /// </summary>
    [WebMethod]
	public void Ping()
    {
    }


	/// <summary>
	/// The winforms client needs to know what is the max size of chunk that the server 
	/// will accept.  this is defined by MaxRequestLength, which can be overridden in
	/// web.config.
	/// </summary>
	[WebMethod]
	public long GetMaxRequestLength()
	{
		try
		{
			return (ConfigurationManager.GetSection("system.web/httpRuntime") as HttpRuntimeSection).MaxRequestLength;
		}
		catch(Exception ex)
		{
			CustomSoapException(ex.GetType().Name, ex.Message);
			return 4096;
		}
	}

    #region Upload
    /// <summary>
    /// Append a chunk of bytes to a file.
    /// The client should ensure that all messages are sent in sequence. 
    /// This method always overwrites any existing file with the same name
    /// </summary>
    /// <param name="fileName">The name of the file that this chunk belongs to, e.g. Vista.ISO</param>
    /// <param name="buffer">The byte array, i.e. the chunk being transferred</param>
    /// <param name="Offset">The offset at which to write the buffer to</param>
    [WebMethod]
	public void AppendChunk(string fileName, byte[] buffer, long Offset)
    {
        string fullFileName = Path.Combine(UploadPath, fileName);
        string filePath = Path.GetDirectoryName(fullFileName);

        if (!Directory.Exists(filePath))
        {
            Directory.CreateDirectory(filePath);
        }

        if (Offset == 0)	// new file, create an empty file
            File.Create(fullFileName).Close();

		// open a file stream and write the buffer.  Don't open with FileMode.Append because the transfer may wish to start a different point
        using (FileStream fs = new FileStream(fullFileName, FileMode.Open, FileAccess.ReadWrite, FileShare.Read))
		{
			fs.Seek(Offset, SeekOrigin.Begin);
			fs.Write(buffer, 0, buffer.Length);
		}
    }
    #endregion

    #region download

    /// <summary>
    /// Download a chunk of a file from the Upload folder on the server. 
    /// </summary>
    /// <param name="FileName">The FileName to download</param>
    /// <param name="Offset">The offset at which to fetch the next chunk</param>
    /// <param name="BufferSize">The size of the chunk</param>
    /// <returns>The chunk as a byte[]</returns>
    [WebMethod]
	public byte[] DownloadChunk(string FileName, long Offset, int BufferSize)
    {		
        string FilePath = Path.Combine(UploadPath, FileName);

        // check that requested file exists
        if (!File.Exists(FilePath))
            CustomSoapException("File not found", String.Format("The file {0} does not exist", FilePath));

        long FileSize = new FileInfo(FilePath).Length;

        // if the requested Offset is larger than the file, quit.
        if (Offset > FileSize)
            CustomSoapException("Invalid Download Offset", String.Format("The file size is {0}, received request for offset {1}", FileSize, Offset));
        
        // open the file to return the requested chunk as a byte[]
        byte[] TmpBuffer;
        int BytesRead;

		try
		{
			using (FileStream fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
			{
				fs.Seek(Offset, SeekOrigin.Begin);	// this is relevent during a retry. otherwise, it just seeks to the start
				TmpBuffer = new byte[BufferSize];
				BytesRead = fs.Read(TmpBuffer, 0, BufferSize);	// read the first chunk in the buffer (which is re-used for every chunk)
			}
			if (BytesRead != BufferSize)
			{
				// the last chunk will almost certainly not fill the buffer, so it must be trimmed before returning
				byte[] TrimmedBuffer = new byte[BytesRead];
				Array.Copy(TmpBuffer, TrimmedBuffer, BytesRead);
				return TrimmedBuffer;
			}
			else
				return TmpBuffer;
		}
		catch(Exception ex)
		{
			CustomSoapException("Error reading file", ex.Message);
			return null;
		}
    }

    [WebMethod]
	public string UploadSuccessStatus(string fileName,string filePath)
    {
        //Tortoise_Merge//OfficeUser\efilefolders\Tortoise_Merge.pdf
        //We need to switch this format to be 'Merge,Tortoise.pdf' - Lastname, firstname.pdf
        string[] tokens = fileName.Split(new char[] { splitter_token, splitter_token1 }, StringSplitOptions.RemoveEmptyEntries);  //fileName.Split('_');
        if (tokens.Length != 2) return "false";

        string userName = Path.GetDirectoryName(filePath);//OfficeUser/foldeName
        string folderName="";
        //要分离userName 和 foldeName
        string[] items = userName.Split('\\');
        if(items.Length==1)
        {
            userName = items[0];
            folderName = items[0];
        }
        else
        {
            userName = items[0];
            folderName = items[1];
        }

        //string firstName = tokens[0];
        //string lastName = tokens[1];

        //switch to lastname, firstname, edited by johnlu655 2010-12-21
        string firstName = tokens[1];
        string lastName = tokens[0];

        Account user = new Account();
        ObjectQuery query1 = user.CreateQuery();
        query1.SetCriteria(user.Name,userName);

        DataTable uTable = new DataTable();

        try
        {
            query1.Fill(uTable);
        }
        catch (Exception exp)
        {
           return exp.Message;
        } 

        if(uTable.Rows.Count==1) 
        {
            string uid = uTable.Rows[0]["UID"].ToString();
            string officeid = uid;

            //Create Folder and Tab for this file
            int folderId;
            int dividerId;
            if(!ExistFileFolder(folderName,uid,out folderId))
            {
                FileFolder folder = new FileFolder();
                folder.FolderName.Value = folderName;
                folder.FirstName.Value = (string) uTable.Rows[0]["Firstname"];
                folder.Lastname.Value = (string) uTable.Rows[0]["Lastname"];
                folder.SecurityLevel.Value = 2;
                // this is correct, for existing folder, need to update tab count
                folder.OtherInfo.Value = 0; 
                folder.DOB.Value = DateTime.Now;
               
                if (uTable.Rows[0]["IsSubUser"].Equals(1))
                {
                    officeid = (string) uTable.Rows[0]["OfficeUID"];
                }

                folder.OfficeID.Value = uid;
                folder.OfficeID1.Value = officeid;

                folder.Create();

                folderId = folder.FolderID.Value;
            }

            if (!ExistDivider(fileName, uid,folderId, out dividerId))
            {
                Divider divider = new Divider();
                divider.Name.Value = fileName;
                divider.Color.Value = "#FF0000";
                divider.OfficeID.Value = uid;
                divider.EffID.Value = folderId;

                divider.Create();

                dividerId = divider.DividerID.Value;

                //Writed by johnlu655 in 2011-01-05
                FileFolder folder = new FileFolder(folderId);
                if (folder.IsExist)
                {
                    folder.OtherInfo.Value++;
                    folder.Update();
                }
            }

            FormFile file = new FormFile();
            ObjectQuery query = file.CreateQuery();
            query.SetCriteria(file.FirstName, firstName);
            query.SetCriteria(file.LastName, lastName);

            query.SetCriteria(file.UserID, uTable.Rows[0]["UID"].ToString());

            
            DataTable fTable = new DataTable();
            try
            {
                query.Fill(fTable);

                int formFileId = 0;
                if (fTable.Rows.Count==1)
                {
                    formFileId = Convert.ToInt32(fTable.Rows[0]["FileID"]);
                    FormFile file1 = new FormFile(formFileId);

                    file1.FileName.Value = filePath;// OfficeUser/foldeName/firstname_lastname.pdf
                    file1.UploadDate.Value = DateTime.Now;
                    file1.Update();
                }
                
                //Insert Upload Table
                UploadFile ufile = new UploadFile();
                ufile.FirstName.Value = firstName;
                ufile.LastName.Value = lastName;
                ufile.UserID.Value = uTable.Rows[0]["UID"].ToString();
                ufile.FileName.Value = filePath;// the relative path against the upload path
                ufile.FolderName.Value = folderName;
                ufile.FolderId.Value = folderId;
                ufile.DividerId.Value = dividerId;
                ufile.FormFileID.Value = formFileId;

                ufile.Create();

                return "true";
               
            }catch(Exception exp)
            {
                return exp.Message;
            } 
            
        }
        else
        {
            return "User: " + userName+ " does't exist.";
        }

    }

    private bool IsValidFormatName(string fileName)
    {
        string[] tokens = fileName.Split(new char[] { splitter_token, splitter_token1 }, StringSplitOptions.RemoveEmptyEntries);
        return tokens.Length == 2;
    }

    [WebMethod]
    public string UploadAutoSuccessStatus(string fileName, string filePath, string dividerName, string folderName, string userName)
    {
        Account user = new Account();
        ObjectQuery query1 = user.CreateQuery();
        query1.SetCriteria(user.Name, userName);

        DataTable uTable = new DataTable();

        try
        {
            query1.Fill(uTable);
        }
        catch (Exception exp)
        {
            return exp.Message;
        }

        if (uTable.Rows.Count == 1)
        {
            string uid = uTable.Rows[0]["UID"].ToString();
            string officeid = uid;

            //Create Folder and Tab for this file
            int folderId;
            int dividerId;

            string firstName = fileName;
            string lastName = string.Empty;

            if (IsValidFormatName(fileName))
            {
                string[] tokens = fileName.Split(new char[] { splitter_token, splitter_token1 }, StringSplitOptions.RemoveEmptyEntries);
                firstName = tokens[1];
                lastName = tokens[0];
            }

            if (!ExistFileFolder(folderName, uid, out folderId))
            {
                FileFolder folder = new FileFolder();
                folder.FolderName.Value = folderName;
                //edited by johnlu655 2010-01-11
                folder.FirstName.Value = firstName;//(string)uTable.Rows[0]["Firstname"];
                folder.Lastname.Value = lastName;//(string)uTable.Rows[0]["Lastname"];
                folder.SecurityLevel.Value = 2;
                folder.OtherInfo.Value = 0;
                folder.DOB.Value = DateTime.Now;

                if (uTable.Rows[0]["IsSubUser"].Equals(1))
                {
                    officeid = (string)uTable.Rows[0]["OfficeUID"];
                }

                folder.OfficeID.Value = uid;
                folder.OfficeID1.Value = officeid;

                folder.Create();

                folderId = folder.FolderID.Value;
            }

            if (!ExistDivider(dividerName, uid, folderId, out dividerId))
            {
                Divider divider = new Divider();
                divider.Name.Value = dividerName;
                divider.Color.Value = "#FF0000";
                divider.OfficeID.Value = uid;
                divider.EffID.Value = folderId;

                divider.Create();

                dividerId = divider.DividerID.Value;

                //Writed by johnlu655 in 2011-01-05
                FileFolder folder = new FileFolder(folderId);
                if (folder.IsExist)
                {
                    folder.OtherInfo.Value++;
                    folder.Update();
                }
            }


            DataTable fTable = new DataTable();
            try
            {
                //Insert Upload Table
                UploadFile ufile = new UploadFile();
                //对于规范名称的文件是否应该分出LastName, FirstName呢?，跟Conversion Service有关系吗
                ufile.FirstName.Value = firstName;
                ufile.LastName.Value = lastName;
                ufile.UserID.Value = uTable.Rows[0]["UID"].ToString();
                ufile.FileName.Value = filePath;// the relative path against the upload path
                ufile.FolderName.Value = folderName;
                ufile.FolderId.Value = folderId;
                ufile.DividerId.Value = dividerId;
           
                ufile.Create();

                return "true";

            }
            catch (Exception exp)
            {
                return exp.Message;
            }

        }
        else
        {
            return "User: " + userName + " does't exist.";
        }

    }

    /// <summary>
    /// judge whether the divider name exists in the folder id and officeId
    /// need to refer to a special folderId, because folders can have same divider name
    /// </summary>
    /// <param name="name"></param>
    /// <param name="officeid"></param>
    /// <param name="folderId"></param>
    /// <param name="dividerId"></param>
    /// <returns></returns>
    private bool ExistDivider(string name, string officeid,int folderId, out int dividerId)
    {
        dividerId = 0;
        try
        {
            Divider tab = new Divider();
            ObjectQuery query = tab.CreateQuery();
            query.SetCriteria(tab.OfficeID, officeid);
            query.SetCriteria(tab.EffID, folderId);
            query.SetCriteria(tab.Name, name);

            DataTable dt = new DataTable();
            query.Fill(dt);

            if (dt.Rows.Count>0)
            {
                dividerId = (int) dt.Rows[0]["DividerID"];
            }

            return dt.Rows.Count > 0;
            
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    private bool ExistFileFolder(string strFoldeName, string uid, out int folderId)
    {
        folderId = 0;

        try
        {
            FileFolder folder = new FileFolder();
            
            ObjectQuery query = folder.CreateQuery();
            query.SetCriteria(folder.FolderName, strFoldeName);
            query.SetCriteria(folder.OfficeID, uid);

            DataTable dt = new DataTable();
            query.Fill(dt);

            if (dt.Rows.Count > 0)
            {
                folderId = (int) dt.Rows[0]["FolderID"];
            }

            return dt.Rows.Count > 0;
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// Get the number of bytes in a file in the Upload folder on the server.
    /// The client needs to know this to know when to stop downloading
    /// </summary>
    [WebMethod]
	public long GetFileSize(string FileName)
    {
		
        string FilePath = UploadPath + "\\" + FileName;

        // check that requested file exists
        if (!File.Exists(FilePath))
            CustomSoapException("File not found", "The file " + FilePath + " does not exist");

        return new FileInfo(FilePath).Length;
    }

    /// <summary>
    /// Return a list of filenames from the Upload folder on the server
    /// </summary>
    [WebMethod]
	public List<string> GetFilesList()
    {
		
        List<string> files = new List<string>();
        foreach (string s in Directory.GetFiles(UploadPath))
            files.Add(Path.GetFileName(s));
        return files;
    }
    #endregion

    #region file hashing
    [WebMethod]
	public string CheckFileHash(string FileName)
    {
		
        string FilePath = UploadPath + "\\" + FileName;
        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
		byte[] hash;
		using(FileStream fs = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read, 4096))
			hash = md5.ComputeHash(fs);
		return BitConverter.ToString(hash);
    }
    #endregion

    #region Exception Handling
    /// <summary>
    /// Throws a soap exception.  It is formatted in a way that is more readable to the client, after being put through the xml serialisation process
    /// Typed exceptions don't work well across web services, so these exceptions are sent in such a way that the client
    /// can determine the 'name' or type of the exception thrown, and any message that went with it, appended after a : character.
    /// </summary>
    /// <param name="exceptionName"></param>
    /// <param name="message"></param>
    public static void CustomSoapException(string exceptionName, string message)
    {
        throw new System.Web.Services.Protocols.SoapException(exceptionName + ": " + message, new System.Xml.XmlQualifiedName("BufferedUpload"));
    }

    #endregion
}