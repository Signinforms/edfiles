using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace UploadWinClient
{
	public partial class TaskPanelItem : UserControl
	{
		public TaskPanelOperation op;

		public TaskPanelItem()
		{
			InitializeComponent();
		}

		public TaskPanelItem(TaskPanelOperation op)
			: this()
		{
			this.op = op;
			this.lblTitle.Text = op.Text;
			this.Name = "pnl" + op.Guid.Replace("-","");
			this.lblStatus.Text = "";
			this.progressBar1.Style = op.ProgressStyle;
			this.progressBar1.Visible = false;
		}
	}
}
