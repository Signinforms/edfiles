using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace UploadWinClient
{
	public class TaskPanelOperation
	{
		public TaskStatus Status;
		public string Text, Guid;
		public ProgressBarStyle ProgressStyle;

		public TaskPanelOperation(string Guid, string Text, ProgressBarStyle ProgressStyle)
		{
			this.Text = Text;
			this.Guid = Guid;
			this.ProgressStyle = ProgressStyle;
		}
	}

	public class TaskPanelOperationStartedEventArgs : EventArgs
	{
		public string Guid;
		public TaskPanelOperationStartedEventArgs(string Guid)
		{
			this.Guid = Guid;
		}
	}

	public class TaskPanelOperationAddedEventArgs : EventArgs
	{
		public TaskPanelOperation op;
		public TaskPanelOperationAddedEventArgs (TaskPanelOperation op)
		{
			this.op = op;
		}
	}

	public class TaskPanelOperationEndedEventArgs : EventArgs
	{
		public TaskPanelOperationEndedEventArgs(string Guid, Exception Error)
		{
			this.Guid = Guid;
			this.Error = Error;
		}
		public string Guid;
		public Exception Error;
	}

	public class TaskPanelOperationProgressChangedEventArgs : EventArgs
	{
		public TaskPanelOperationProgressChangedEventArgs(string Guid, int ProgressPercentage, string Message)
		{
			this.Guid = Guid;
			this.ProgressPercentage = ProgressPercentage;
			this.Message = Message;
		}
		public string Guid;
		public string Message;
		public int ProgressPercentage;
	}

	public class TaskPanelStatusUpdate : EventArgs
	{
		public TaskPanelStatusUpdate(string Guid, string Message)
		{
			this.Guid = Guid;
			this.Message = Message;
		}
		public string Guid;
		public string Message;
	}

	public enum TaskStatus
	{
		Waiting,
		Running,
		Success,
		Failed
	}
}
