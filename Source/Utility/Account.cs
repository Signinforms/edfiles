using System;

namespace Shinetech.Utility
{
	/// <summary>
	/// Summary description for Account.
	/// </summary>
	public class Account
	{
		private string _AccountName;

		public Account()
		{
			//
			// TODO: Add constructor logic here
			//
		}
		/// <summary>
		/// Account Name
		/// </summary>
		public string AccountName
		{
			get
			{
				return this._AccountName;
			}
			set
			{
				this._AccountName = value;
			}
		}
	}
}
