using System;
using System.Collections;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Configuration;
using System.Text;
using System.IO;

namespace Shinetech.Utility
{
    /// <summary>
    /// Summary description for Email.
    /// </summary>
    public class Email
    {
        public static string MAIL_SERVER;
        public static string MAIL_SENDER;
        public static string MAIL_PASSWORD;
        public static string emailServer = GetMailServer();


        public static void SendFromEFileFolder(string to, string subject, string body)
        {
            //string emailServer = ConfigurationManager.AppSettings["EMAILSERVER"];
            string emailFrom = ConfigurationManager.AppSettings["EMAIL"];
            string emailSender = ConfigurationManager.AppSettings["EMAILSENDER"];
            string emailPassword = ConfigurationManager.AppSettings["EMAILPASSWORD"];
            Email.Send(emailServer, emailSender, emailPassword, emailFrom, to == "" ? emailFrom : to, subject, body);

        }

        public static void SendFromEFFService(string to, string subject, string body)
        {
            //string emailServer = ConfigurationManager.AppSettings["EMAILSERVER"];
            string emailFrom = ConfigurationManager.AppSettings["EMAIL"];
            string emailSender = ConfigurationManager.AppSettings["EMAILSENDER"];
            string emailPassword = ConfigurationManager.AppSettings["EMAILPASSWORD"];
            Email.Send(emailServer, emailSender, emailPassword, emailFrom, to == "" ? emailFrom : to, subject, body);

        }

        public static void SendFileFormService(string emailFrom, string subject, string body)
        {
            //string emailServer = ConfigurationManager.AppSettings["EMAILSERVER"];
            string emailTo = ConfigurationManager.AppSettings["EMAIL"];
            string emailSender = ConfigurationManager.AppSettings["EMAILSENDER"];
            string emailPassword = ConfigurationManager.AppSettings["EMAILPASSWORD"];

            Email.Send(emailServer, emailSender, emailPassword, emailFrom, emailTo, subject, body);

        }

        public static void SendFileRequestService(string emailFrom, string subject, string body)
        {
            //string emailServer = ConfigurationManager.AppSettings["EMAILSERVER"];
            string emailTo = ConfigurationManager.AppSettings["INFOEMAIL"];
            string emailSender = ConfigurationManager.AppSettings["EMAILSENDER"];
            string emailPassword = ConfigurationManager.AppSettings["EMAILPASSWORD"];

            Email.Send(emailServer, emailSender, emailPassword, emailFrom, emailTo, subject, body);

        }



        public static void SendPartnerFromEFileFolder(string to, string subject, string body)
        {
            //string emailServer = ConfigurationManager.AppSettings["EMAILSERVER"];
            string emailFrom = ConfigurationManager.AppSettings["EMAIL"];
            string emailSender = ConfigurationManager.AppSettings["EMAILSENDER"];
            string emailPassword = ConfigurationManager.AppSettings["EMAILPASSWORD"];
            string emailcc = ConfigurationManager.AppSettings["PartnerCC"];
            Email.Send(emailServer, emailSender, emailPassword, emailFrom, to == "" ? emailFrom : to, subject, body, emailcc);

        }

        public static void SendPartnerFromExcelReportService(string to, string subject, string body, string filename)
        {
            //string emailServer = ConfigurationManager.AppSettings["EMAILSERVER"];
            string emailFrom = ConfigurationManager.AppSettings["EMAIL"];
            string emailSender = ConfigurationManager.AppSettings["EMAILSENDER"];
            string emailPassword = ConfigurationManager.AppSettings["EMAILPASSWORD"];
            string emailcc = ConfigurationManager.AppSettings["PartnerCC"];
            Email.Send(emailServer, emailSender, emailPassword, emailFrom, to == "" ? emailFrom : to, subject, body, emailcc, filename);

        }

        /// <summary>
        /// Sends the estimation.
        /// </summary>
        /// <param name="to">To.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="isUser">if set to <c>true</c> [is user].</param>
        public static void SendEstimation(string to, string subject, string body, string fileName, bool isUser = false)
        {
            //string emailServer = ConfigurationManager.AppSettings["EMAILSERVER"];
            string emailFrom = ConfigurationManager.AppSettings["EMAIL"];
            string emailSender = ConfigurationManager.AppSettings["EMAILSENDER"];
            string emailPassword = ConfigurationManager.AppSettings["EMAILPASSWORD"];
            string emailpartner = ConfigurationManager.AppSettings["PARTNEREMAIL"];

            Email.SendEstmation(emailServer, emailSender, emailPassword, emailSender, isUser ? to : emailpartner, subject, body, fileName);
        }

        /// <summary>
        /// Sends the estmation.
        /// </summary>
        /// <param name="server">邮件服务器地址<example>smtp.126.com</example></param>
        /// <param name="Sender">发送帐号</param>
        /// <param name="pwd">密码</param>
        /// <param name="from">发件人Email</param>
        /// <param name="to">收件人Email</param>
        /// <param name="subject">标题</param>
        /// <param name="body">内容</param>
        /// <param name="fileName">Name of the file.</param>
        public static void SendEstmation(string server, string Sender, string pwd, string from, string to, string subject, string body, string fileName = null)
        {
            try
            {
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(server);
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(Sender, pwd);
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(from, to, subject, body);

                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.IsBodyHtml = true;
                if (!string.IsNullOrEmpty(fileName))
                    message.Attachments.Add(new Attachment(fileName));
                client.Send(message);
            }
            catch (Exception exp)
            {
                WriteErrorLog(null, null, exp.Message, exp);
                Console.WriteLine(exp.Message);
            }

        }


        /// <summary>
        /// 邮件发送(Smtp)
        /// </summary>
        /// <param name="server">邮件服务器地址<example>smtp.126.com</example></param>
        /// <param name="Sender">发送帐号</param>
        /// <param name="pwd">密码</param>
        /// <param name="from">发件人Email</param>
        /// <param name="to">收件人Email</param>
        /// <param name="subject">标题</param>
        /// <param name="body">内容</param>
        public static void Send(string server, string Sender, string pwd, string from, string to, string subject, string body)
        {
            try
            {
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(server);
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(Sender, pwd);
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(from, to, subject, body);

                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.IsBodyHtml = true;

                client.Send(message);
            }
            catch (Exception exp)
            {
                WriteErrorLog(null, null, exp.Message, exp);
                Console.WriteLine(exp.Message);
            }

        }

        public static void WriteErrorLog(string folderId, string dividerId, string fileName, Exception ex)
        {
            try
            {
                if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/eff")))
                    Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/eff"));

                StreamWriter swData2 = new StreamWriter(System.Web.Hosting.HostingEnvironment.MapPath("~/eff") + "\\ErrorLog.txt", true);
                swData2.WriteLine("=====Unhandled Exception occured at  : " + DateTime.Now);
                swData2.WriteLine("Folder = " + folderId);
                swData2.WriteLine("Divider = " + dividerId);
                swData2.WriteLine("fileName = " + fileName);
                swData2.WriteLine("Error = " + ex.Message);
                swData2.WriteLine(ex.StackTrace);
                swData2.WriteLine(ex.Source);
                swData2.WriteLine(ex.InnerException);
                swData2.WriteLine("====");
                swData2.Close();
                swData2.Dispose();
            }
            catch (Exception ex1)
            {
                string errorMessage = ex1.Message;
            }
        }

        public static void Send(string server, string Sender, string pwd, string from, string to, string subject, string body, string cc)
        {
            try
            {
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(server);
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(Sender, pwd);
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(from, to, subject, body);
                message.CC.Add(cc);
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.IsBodyHtml = true;

                client.Send(message);
            }
            catch { }

        }

        public static void Send(string server, string Sender, string pwd, string from, string to, string subject, string body, string cc, string filename)
        {
            try
            {
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(server);
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(Sender, pwd);
                client.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;

                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage(from, to, subject, body);

                Attachment data = new Attachment(filename, MediaTypeNames.Text.Xml);
                // Add time stamp information for the file.
                ContentDisposition disposition = data.ContentDisposition;
                disposition.CreationDate = System.IO.File.GetCreationTime(filename);
                disposition.ModificationDate = System.IO.File.GetLastWriteTime(filename);
                disposition.ReadDate = System.IO.File.GetLastAccessTime(filename);
                // Add the file attachment to this e-mail message.
                message.Attachments.Add(data);

                message.CC.Add(cc);
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.IsBodyHtml = true;

                client.Send(message);
            }
            catch { }

        }

        public static void Send(string from, string to, string subject, string body)
        {
            if (string.IsNullOrEmpty(MAIL_SERVER) || string.IsNullOrEmpty(MAIL_SENDER) || string.IsNullOrEmpty(MAIL_PASSWORD))
                throw new System.Net.Mail.SmtpFailedRecipientsException("Mail is not integral Arguments about server,account or password! please check it and try again.");
            Send(MAIL_SERVER, MAIL_SENDER, MAIL_PASSWORD, from, to, subject, body);
        }

        public static string GetMailServer()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.ToString() == ConfigurationManager.AppSettings["EMAILSERVERPRIVATE"].ToString())
                {
                    return ConfigurationManager.AppSettings["EMAILSERVERPRIVATE"];
                }
            }
            return ConfigurationManager.AppSettings["EMAILSERVER"];
        }
    }
}