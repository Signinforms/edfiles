using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;

namespace Shinetech.Utility
{
    public class ModalPopupDialog
    {
		public const int _DEFAUTL_WIDTH = 200;
		public const int _DEFAUTL_HEIGHT = 200;

		public static void Show(Page page,string Url)
		{
			Show(page, Url, _DEFAUTL_WIDTH, _DEFAUTL_HEIGHT);
		}
		public static void Show(Page page,string Url,int Width,int Height)
		{
			if (string.IsNullOrEmpty(Url))
				throw new NullReferenceException("the Url can not null or empty!");
			if(!page.ClientScript.IsStartupScriptRegistered(Url))
			{
				string _script;
				_script = "window.showModalDialog(\"modalDialogSource.htm\", null, \"dialogHeight:" + Width + "px; dialogWidth:" + Height + "px; resizable:no;\");";
				page.ClientScript.RegisterStartupScript(page.GetType(), Url, _script);
			}
		}
    }
}
