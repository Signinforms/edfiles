using System;

namespace Shinetech.Utility
{
	/// <summary>
	/// Summary description for User.
	/// </summary>
	public class User
	{
		private string _UserName;
		private string _EmailAddress;

		public User()
		{
			
		}

		/// <summary>
		/// User Name
		/// </summary>
		public string UserName
		{
			get
			{
				return this._UserName;
			}
			set
			{
				this._UserName = value;
			}
		}

		
		/// <summary>
		/// Users Email Address
		/// </summary>
		public string EmailAddress
		{
			get
			{
				return _EmailAddress;
			}
			set
			{
				_EmailAddress = value;
			}
		}
	}
}
