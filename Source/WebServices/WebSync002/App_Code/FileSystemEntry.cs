public class FileSystemEntry
{
    public string Name, Path;
    public bool IsDirectory, IsSymbolic;

    public FileSystemEntry() { }
    public FileSystemEntry(string name, string path, bool isDirectory, bool isSymbolic)
    {
        this.Name = name;
        this.Path = path;
        this.IsDirectory = isDirectory;
        this.IsSymbolic = isSymbolic;
    }
}
