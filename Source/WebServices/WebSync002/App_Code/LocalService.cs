using System.Configuration;

public class LocalService : Synchronize.FileSystem
{
    public LocalService() : base()
    {
        base.Url = ConfigurationManager.AppSettings["Local.FileSystem"];
    }
}
