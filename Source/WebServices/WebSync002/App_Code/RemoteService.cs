using System.Configuration;

public class RemoteService : Synchronize.FileSystem
{
    public RemoteService() : base()
    {
        base.Url = ConfigurationManager.AppSettings["Remote.FileSystem"];
    }
}
