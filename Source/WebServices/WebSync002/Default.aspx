﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>WebSynchronizer</title>
    <style type="text/css">        
        body,ul {margin:0px; }
        ul { list-style-type:none; }
        ul.left {float: left; width: 50%;}
        ul.right {float: right; width: 50%;}
        div.button {width:100%;background-color:lightgrey; }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="button">
            <asp:Button ID="Button1" runat="server" Text="Synchronize" OnClick="Synchronize" />
        </div>
        <ul class="left">
            <li>
                <label>
                    Folder
                </label>
                [<asp:Label ID="LocalFolder" runat="server" />]
            </li>
            <asp:Repeater runat="server" ID="LocalItems" DataSourceID="LocalDataSource">
                <ItemTemplate>
                    <li>
                        <input type="checkbox" name="LocalChecked" 
                        value='<%# Eval("IsDirectory") %>:<%# Eval("Path") %>' 
                        <%# (bool)Eval("IsSymbolic") ? "disabled=disabled" : "" %> />
                        <asp:LinkButton runat="server" Text='<%# Eval("Name") %>' 
                            Enabled='<%# Eval("IsDirectory") %>' ID="Item"
                            OnClick="SelectLocalDirectory" CommandArgument='<%# Eval("Path") %>' />
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
        <ul class="right">
            <li>
                <label>
                    Folder
                </label>
                [<asp:Label ID="RemoteFolder" runat="server" />]
            </li>
            <asp:Repeater runat="server" ID="RemoteItems" DataSourceID="RemoteDataSource">
                <ItemTemplate>
                    <li>
                        <input type="checkbox" name="RemoteChecked" 
                        value='<%# Eval("IsDirectory") %>:<%# Eval("Path") %>' 
                        <%# (bool)Eval("IsSymbolic") ? "disabled=disabled" : "" %> />
                        <asp:LinkButton runat="server" Text='<%# Eval("Name") %>' 
                            Enabled='<%# Eval("IsDirectory") %>' ID="Item"
                            OnClick="SelectRemoteDirectory" CommandArgument='<%# Eval("Path") %>' />
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
        <div class="button">
            <asp:Button runat="server" Text="Synchronize" OnClick="Synchronize" />
        </div>
        <asp:ObjectDataSource ID="LocalDataSource" runat="server" SelectMethod="GetFileSystemEntries"
            TypeName="LocalService">
            <SelectParameters>
                <asp:ControlParameter ControlID="LocalFolder" Name="folder" PropertyName="Text" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
        <asp:ObjectDataSource ID="RemoteDataSource" runat="server" SelectMethod="GetFileSystemEntries"
            TypeName="RemoteService">
            <SelectParameters>
                <asp:ControlParameter ControlID="RemoteFolder" Name="folder" PropertyName="Text"
                    Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>        
    </form>
</body>
</html>
