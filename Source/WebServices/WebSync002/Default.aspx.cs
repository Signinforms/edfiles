using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : Page
{
    protected void SelectLocalDirectory(object sender, EventArgs e)
    {
        LocalFolder.Text = (sender as LinkButton).CommandArgument;
    }
    protected void SelectRemoteDirectory(object sender, EventArgs e)
    {
        RemoteFolder.Text = (sender as LinkButton).CommandArgument;
    }
    protected void Synchronize(object sender, EventArgs e)
    {
        SyncHelper.Synchronize("LocalChecked", RemoteFolder.Text, RemoteItems, new LocalService(), new RemoteService());
        SyncHelper.Synchronize("RemoteChecked", LocalFolder.Text, LocalItems, new RemoteService(), new LocalService());
    }
}