using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using DataQuicker2.Framework;
using Shinetech.DAL;

namespace Shinetech.Services.BLL
{
    public class StorageFeeBLL
    {
        public static DataTable GetAllStorageTasks()
        {
            string sqlstr = "SELECT * FROM View_GetStorageTasks ";
            SqlHelper sql = new SqlHelper("Default");
            DataTable table = new DataTable();
            try
            {
                sql.Fill(table, sqlstr);
                return table;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static Hashtable ReadHtmlPage(StorageState item)
        {
            string x_login = ConfigurationManager.AppSettings["x_login"];
            string x_tran_key = ConfigurationManager.AppSettings["x_tran_key"];
            string transmodel = ConfigurationManager.AppSettings["x_transmode"];
            string result = "";
            string strPost = "x_login={0}&x_tran_key={1}&x_method=CC" +
                "&x_type=AUTH_CAPTURE&x_amount={2}&x_delim_data=TRUE&x_delim_char=|&x_relay_response=FALSE" +
                "&x_card_num={3}&x_exp_date={4}&x_test_request={5}&x_version=3.1&x_Address={6}&x_City={7}&x_Country={8}" +
                "&x_State={9}&x_Zip={10}";
            StreamWriter myWriter = null;
            transmodel = transmodel.ToLower() == "test" ? "TRUE" : "FALSE";
            string strPost1 = "";
            string url = "https://test.authorize.net/gateway/transact.dll";
            if(transmodel=="TRUE")
            {
                strPost = "x_login={0}&x_tran_key={1}&x_method=CC" +
                          "&x_type=AUTH_CAPTURE&x_amount={2}&x_delim_data=TRUE&x_delim_char=|&x_relay_response=FALSE" +
                          "&x_card_num={3}&x_exp_date={4}&x_test_request={5}&x_version=3.1";
                strPost1 = string.Format(strPost, x_login, x_tran_key, item.Amount,
                                         item.CreditCard, item.ExpirationDate.ToString("MMyyyy"), transmodel);
            }
            else
            {
                url = "https://secure.authorize.net/gateway/transact.dll";
                strPost1 = string.Format(strPost, x_login, x_tran_key, item.Amount,
                item.CreditCard, item.ExpirationDate.ToString("MMyyyy"), transmodel,
                item.Address1, item.BillingCity, item.BillingCountry, item.BillingState, item.BillingPostal);
            }

            
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url);
            objRequest.Method = "POST";
            objRequest.ContentLength = strPost1.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(strPost1);
            }
            catch (Exception e)
            {
                return null;
            }
            finally
            {
                myWriter.Close();
            }

            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader sr =
               new StreamReader(objResponse.GetResponseStream()))
            {
                result = sr.ReadToEnd();
                sr.Close();
            }

            string[] output = result.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            int counter = 0;
           
            Hashtable vars = new Hashtable();
            foreach (string var in output)
            {
                vars.Add(counter, var);
                counter += 1;
            }

            if (vars.Count < 7)
            {
                return null;
            }

            return vars;
        }

        public static float GetStorageFeePrice()
        {
            StorageFee fee = new StorageFee();
            ObjectQuery query = fee.CreateQuery();
            query.TopN = 1;

            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            DataTable table = new DataTable();
            try
            {
                query.Fill(table, connection);
                double var = (double) table.Rows[0]["Price"];
                float price = (float)var;

                return price;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
  
}
