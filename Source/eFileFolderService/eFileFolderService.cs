using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.IO;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using System.Threading;
using Amib.Threading;
using DataQuicker2.Framework;
using Schedule;
using Shinetech.DAL;
using Shinetech.Services.BLL;
using Shinetech.Utility;
using Account=Shinetech.DAL.Account;

namespace Shinetech.Services
{
    public partial class eFileFolderService : ServiceBase
    {
        protected SmartThreadPool _ThreadPool;
        protected ScheduleTimer _TickTimer;
        protected string _DailyTime;
        protected float _StorageFee;
        protected string errorFile = string.Empty;
        protected const string strSubject = "Storage fee transaction notification from efile folder";
        const string mailTempletUrl = "MailContent.htm";

        public eFileFolderService()
        {
            InitializeComponent();

        }

        protected override void OnStart(string[] args)
        {
            Start();
        }

        private void TickTimer_Elapsed(object sender, ScheduledEventArgs e)
        {
            WriteLogMessage("Start getting storage tasks");
            this._TickTimer.Stop();

            //要单独处理totalsize==0的情况
            DataTable table = StorageFeeBLL.GetAllStorageTasks();
            if(table!=null)
            {
                foreach(DataRow item in table.Rows)
                {
                    //get personal storage charge fee moidified by johnlu655 2010-01-18
                    //SubscriptionFee fee = new SubscriptionFee(item["UID"].ToString());
                    //if (fee.IsExist)
                    //{
                    //    _StorageFee = (float)fee.Price.Value;
                    //}

                    float price = 9.95f;

                    LicenseKit lit = new LicenseKit(Convert.ToInt32(item["LicenseID"]));
                    price = lit.MonthPricePerUser.Value;

                    if (!item["PersonalMonthPrice"].Equals(DBNull.Value) && !item["PersonalMonthPrice"].Equals(0))
                    {
                        price =(float)Convert.ToDouble(item["PersonalMonthPrice"]);
                    }

                    StorageState storage = new StorageState(item, price, lit);

                    if (storage.Amount == 0) //不要提交付款，直接标识StoragePaymentDate已经
                    {
                        Account account = new Account(storage.UID);
                        if(account.IsExist)
                        {
                            try
                            {
                                WriteLogMessage(string.Format("User {0}: total size is 0", storage.UID));
                                account.StoragePaymentDate.Value = DateTime.Now;
                                account.StorageTransID.Value = -1;
                                account.Update();
                            }
                            catch (Exception exp) 
                            {
                                WriteLogMessage(string.Format("User {0}: Account updating failed,{1}", storage.UID, exp.Message));
                            }
                            
                        }
                    }
                    else
                    {
                        IWorkItemResult wir = _ThreadPool.QueueWorkItem(new WorkItemCallback(this.DoRealWork),
                                  storage, new PostExecuteWorkItemCallback(this.callbackExecute));
                    }

                    Thread.Sleep(1000);
                    
                }

                WriteLogMessage("Ending storage tasks");
            }
            else
            {
                WriteLogMessage("GetAllStorageTasks result is null");
            }
        }

        private object DoRealWork(object state)
        {
            StorageTransaction result = new StorageTransaction();

            StorageState storage = null;
            try
            {
                WriteLogMessage("Start DoRealWork making transfer");

                storage = state as StorageState;
                Hashtable vars = StorageFeeBLL.ReadHtmlPage(storage);
                if (vars == null)
                {
                    result.ResponseCode.Value = "-1";
                    result.ResponseAuthCode.Value = "";
                    result.ResponseAVSCode.Value = "";
                    result.ResponseDescription.Value = "Error:Transaction Failed";
                    result.ResponseReferenceCode.Value = "";
                }
                else
                {
                    result.ResponseAuthCode.Value = (string)vars[4];
                    result.ResponseAVSCode.Value = (string)vars[5] + ":" + ParseAvsCode((string)vars[5]);
                    result.ResponseCode.Value = (string)vars[0];
                    result.ResponseDescription.Value = (string)vars[3];
                    result.ResponseReferenceCode.Value = (string)vars[6];
                }

                result.UserName.Value = storage.UserName;
                result.EmailAddress.Value = storage.EmailAddress;
                result.Amount.Value = storage.Amount;
                result.CardNumber.Value = storage.CreditCard;
                result.CardType.Value = storage.CardType;
                result.ExchangeDate.Value = DateTime.Now;
                result.ExpiredDate.Value = storage.ExpirationDate;
                result.FirstName.Value = storage.FirstName;
                result.LastName.Value = storage.LastName;
                result.OfficeID.Value = storage.UID;
                result.Price.Value = storage.Price;
                result.Size.Value = storage.Size;

                WriteLogMessage(string.Format("Storage fee charge on user:{0} successful.", storage.UserName));
            }
            catch (Exception exp)
            {
                if (storage != null)
                {
                    WriteErrorMessage("DoRealWork Error: {" + storage.UserName + "} " + exp.ToString());
                    result.Amount.Value = storage.Amount;
                    result.OfficeID.Value = storage.UID;
                    result.Price.Value = storage.Price;
                    result.Size.Value = storage.Size;
                    result.ResponseCode.Value = "-1";
                    result.ResponseAuthCode.Value = "";
                    result.ResponseAVSCode.Value = "";
                    result.ResponseDescription.Value = "Error:Transaction Failed";
                    result.ResponseReferenceCode.Value = "";
                }
                else
                {
                    WriteErrorMessage("DoRealWork Error: " + exp.ToString());
                    result.ResponseCode.Value = "-2";
                }
                
            }
            
            return result;
        }

        public void callbackExecute(IWorkItemResult wir)
        {
            WriteLogMessage("Begin callbackExecute");

            if(wir.Result == null)
            {
                WriteErrorMessage("wir.Result is null");
                return;
            }
            StorageTransaction result = wir.Result as StorageTransaction;

            if (result.ResponseCode.Value == "-2") return;
            try
            {
                IDbConnection connection = DbFactory.CreateConnection();
                connection.Open();
           
                WriteLogMessage("Begin Create StorageTransaction");
                if(result.Amount ==null)
                {
                   WriteErrorMessage("result.Amount ==null");
                    return;
                }
                string outRef = "UserName: " + result.UserName.Value + " Amount:" + result.Amount.Value;
                WriteLogMessage(outRef);

                result.Create(connection);

                WriteLogMessage("End Create StorageTransaction");

                Account account = new Account(result.OfficeID.Value);
                if (account.IsExist)
                {
                    if (result.ResponseCode.Value == "1")
                    {
                        account.StorageTransID.Value = result.StorageTransID.Value;
                    }
                    else
                    {
                        account.StorageTransID.Value = -1;
                    }

                    account.StoragePaymentDate.Value = account.StoragePaymentDate.Value.AddDays(30);
                    account.Update(connection);
                }

                WriteLogMessage(string.Format("Updating storage transaction on user:{0} successful.", result.UserName.Value));

                SendEmail(result, account);

                WriteLogMessage(string.Format("Send email to user:{0} successful.", result.UserName.Value));

            }
            catch (Exception exp)
            {
                WriteErrorMessage("callbackExecute Error:" + exp.ToString());
                WriteErrorMessage("transaction id [" + result.StorageTransID.Value + "]: " + result.UserName.Value + " " + result.Amount.Value);
            }
        }

        bool IsValidEmail(string strIn)
        {
            // Return true if strIn is in valid e-mail format. 
            return Regex.IsMatch(strIn, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }

        protected override void OnStop()
        {
            try
            {
                _TickTimer.Stop();
                _TickTimer.Dispose();
                _TickTimer = null;
                _ThreadPool.Shutdown();
                _ThreadPool.Dispose();
                _ThreadPool = null;
            }catch{}
            
        }

        public void Start()
        {
            try
            {
                _StorageFee = StorageFeeBLL.GetStorageFeePrice();
                _DailyTime = ConfigurationManager.AppSettings["StorageFeeTime"];
                errorFile = ConfigurationManager.AppSettings["LogPath"];
                WriteLogMessage("Starting service");
            }catch(Exception exp)
            {
                WriteErrorMessage(exp.Message);
                return;
            }

            _ThreadPool = new SmartThreadPool(5000,1,1);
            _ThreadPool.Start();
            _TickTimer = new ScheduleTimer();
            _TickTimer.Elapsed += new ScheduledEventHandler(TickTimer_Elapsed);
            if(ConfigurationManager.AppSettings["test_mode"].ToLower()=="test")
            {
                _TickTimer.AddEvent(new Schedule.ScheduledTime("Hourly", "30,0"));
               
            }
            else
            {
                _TickTimer.AddEvent(new Schedule.ScheduledTime("Daily", _DailyTime));
            }
            
            _TickTimer.Start();

            WriteLogMessage("Starting Timer");
        }

        public void StopE()
        {
            WriteLogMessage("Stopping service");
            OnStop();
        }

        public void WriteErrorMessage(string errorMsg)
        {
            string applicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            if (!applicationBase.EndsWith(@"\"))
            {
                applicationBase += @"\";
            }

            try 
            {
                string errorlog = string.Format(applicationBase + errorFile + "{0}-{1}.log", DateTime.Now.Year, DateTime.Now.Month);
                using (StreamWriter sw1 = new StreamWriter(errorlog, true))
                {
                    sw1.WriteLine("-----------Error Log----------- " + DateTime.Now.ToString());
                    sw1.WriteLine(errorMsg);
                }
            }
            catch { }
        }

        public void WriteLogMessage(string logMsg)
        {
            string applicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            if (!applicationBase.EndsWith(@"\"))
            {
                applicationBase += @"\";
            }

            try
            {
                string errorlog = string.Format(applicationBase + errorFile + "{0}-{1}.log",DateTime.Now.Year,DateTime.Now.Month);
                //fs = new FileStream(applicationBase + errorFile, FileMode.Append,FileAccess.Write);
                using (StreamWriter sw1 = new StreamWriter(errorlog, true))
                {
                    sw1.WriteLine("-----------Message Log----------- " + DateTime.Now.ToString());
                    sw1.WriteLine(logMsg);
                }
                
            }
            catch { }
        }

        public void SendEmail(StorageTransaction result, Account account)
        {
            if (result.ResponseCode.Value == "-1" || result.EmailAddress.Value=="") return;

            WriteLogMessage("Start sending email to " + result.UserName.Value);
            string strMailTemplet = getMailTempletStr();
            strMailTemplet = strMailTemplet.Replace("[username]", result.UserName.Value);

            strMailTemplet = strMailTemplet.Replace("[transid]", result.ResponseReferenceCode.Value.ToString());
            strMailTemplet = strMailTemplet.Replace("[Amount]", string.Format("{0:F2}", result.Amount.Value));
            strMailTemplet = strMailTemplet.Replace("[TotalSize]", string.Format("{0:F2}", result.Size.Value));
            strMailTemplet = strMailTemplet.Replace("[TransactionDate]", result.ExchangeDate.Value.ToString());

            strMailTemplet = strMailTemplet.Replace("[CardType]", result.CardType.Value);
            if (result.CardNumber.IsNull || result.CardNumber.Value.Length == 0)
            {
                strMailTemplet = strMailTemplet.Replace("[CardNo]", "");
            }
            else
            {
                char[] xxCardNo = result.CardNumber.Value.ToCharArray();
                int cnt = xxCardNo.Length;
                if (cnt > 4)
                {
                    for (int i = 0; i < cnt - 4; i++)
                    {
                        xxCardNo[i] = 'x';
                    }
                }
                strMailTemplet = strMailTemplet.Replace("[CardNo]", new string(xxCardNo));
                
            }
            
            result.ResponseDescription.Value = result.ResponseDescription.Value.Replace("has been", "was");
            strMailTemplet = strMailTemplet.Replace("[TransactionResult]", result.ResponseDescription.Value);
            
            string transStatus = result.ResponseCode.Value == "1" ? "successful" : "failed";
            strMailTemplet = strMailTemplet.Replace("[ResponseCode]", transStatus);

            Email.SendFromEFFService(result.EmailAddress.Value, strSubject, strMailTemplet);

            WriteLogMessage("End sending email");
        }

        public string getMailTempletStr()
        {
            WriteLogMessage("Start getting mail templet"); 

            string strMailTemplet = string.Empty;
            string applicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            try
            {
                StreamReader sr = new StreamReader(applicationBase+mailTempletUrl);
                string sLine = "";
                while (sLine != null)
                {
                    sLine = sr.ReadLine();
                    if (sLine != null)
                        strMailTemplet += sLine;
                }
                sr.Close();
            }
            catch(Exception exp)
            {
                throw new ApplicationException(exp.Message);
            }
            
            return strMailTemplet;
        }


        private static string ParseAvsCode(string code)
        {
            string result = "Uknown";

            switch (code.ToUpper())
            {
                case "A":
                    result = " [AVS - Address (Street) matches, ZIP does not]";
                    break;
                case "B":
                    result = " [AVS - Address information not provided for AVS check]";
                    break;
                case "E":
                    result = " [AVS - Error]";
                    break;
                case "G":
                    result = " [AVS - Non-U.S. Card Issuing Bank]";
                    break;
                case "N":
                    result = " [AVS - No Match on Address (Street) or ZIP]";
                    break;
                case "P":
                    result = " [AVS - AVS not applicable for this transaction]";
                    break;
                case "R":
                    result = " [AVS - Retry – System unavailable or timed out]";
                    break;
                case "S":
                    result = " [AVS - Service not supported by issuer]";
                    break;
                case "U":
                    result = " [AVS - Address information is unavailable]";
                    break;
                case "W":
                    result = " [AVS - 9 digit ZIP matches, Address (Street) does not]";
                    break;
                case "X":
                    result = " [AVS - Address (Street) and 9 digit ZIP match]";
                    break;
                case "Y":
                    result = " [AVS - Address (Street) and 5 digit ZIP match]";
                    break;
                case "Z":
                    result = " [AVS - 5 digit ZIP matches, Address (Street) does not]";
                    break;
            }

            return result;
        }

        private static string ParseSecurityCode(string code)
        {
            string result = "Uknown";
            switch (code.ToUpper())
            {
                case "M":
                    result = " [CVV - Match]";
                    break;
                case "N":
                    result = " [CVV - No Match]";
                    break;
                case "P":
                    result = " [CVV - Not Processed]";
                    break;
                case "S":
                    result = " [CVV - Should have been present]";
                    break;
                case "U":
                    result = " [CVV - Issuer unable to process request]";
                    break;
            }
            return result;
        }
    }


    public class StorageState
    {
        private DataRow _item = null;
        private float _storagePrice;
        private LicenseKit _kit;
        public StorageState(DataRow item, float price,LicenseKit kit)
        {
            _item = item;
            _storagePrice = price;
            _kit = kit;
        }

        public string CreditCard
        {
            get
            {
                return _item["CreditCard"] == System.DBNull.Value ? "" : _item["CreditCard"] as string;
            }
        }

        public string CardType
        {
            get
            {
                return _item["CardType"] == System.DBNull.Value ? "" : _item["CardType"] as string;
            }
        }

        public DateTime ExpirationDate
        {
            get
            {
                return _item["ExpirationDate"] == System.DBNull.Value ? DateTime.Now : (
                    DateTime)_item["ExpirationDate"];
            }
        }

        public double Amount
        {
            get
            {
                double perMonthCharge = _kit.MonthPricePerUser.Value;//每月必收费用
                int addUsers = Convert.ToInt32(_item["CNT"]) - _kit.MaximalUserCount.Value;
                if (addUsers <= 0)
                {
                    addUsers = 0;
                }

                double perMonthAddUsers = _kit.AdditionalUserPrice.Value * addUsers;//每月多余用户数费用
                double perMonthAddStorageCharge  = 0;

                double varGb = this.Size / 1024f;
                double addStorage = (varGb-_kit.MaxStorageVolume.Value)/5;
                if (addStorage <= 0)
                {
                    addStorage = 0;
                }
                double addStorageCharge = Math.Ceiling(addStorage) * _kit.AdditionalStoragePrice.Value;

                return (double)(perMonthCharge + perMonthAddUsers + addStorageCharge);
            }
        }

        public float Price
        {
            get
            {
                return _storagePrice;
            }
        }

        public float Size
        {
            get
            {
                double var = Convert.ToDouble(_item["TotalSize"] == System.DBNull.Value ? 0 : _item["TotalSize"]);
                return (float)var;
            }
        }

        public string FirstName
        {
            get
            {
                return _item["FirstName"] == System.DBNull.Value ? "" : _item["FirstName"] as string;
            }
        }

        public string LastName
        {
            get
            {
                return _item["LastName"] == System.DBNull.Value ? "" : _item["LastName"] as string;
            }
        }

        public string UID
        {
            get
            {
                System.Guid uid = (Guid)_item["UID"];
                return (string)uid.ToString("D");
            }
        }

        public string EmailAddress
        {
            get
            {
                return _item["Email"] == System.DBNull.Value ? "" : _item["Email"] as string;
            }
        }

        public string UserName
        {
            get
            {
                return (string)_item["Name"];
            }
        }

        public string Address1
        {
            get
            {
                return _item["BillingAddress"] == System.DBNull.Value ? "" : _item["BillingAddress"] as string;
            }
        }

        public string BillingCity
        {
            get
            {
                return _item["BillingCity"] == System.DBNull.Value ? "" : _item["BillingCity"] as string;
            }
        }

        public string BillingState
        {
            get
            {
                if (_item["BillingOtherState"] == System.DBNull.Value)
                {
                    return (string)_item["BillingState"];
                }
                else
                {
                    return (string)_item["BillingOtherState"];
                }
            }
        }

        public string BillingCountry
        {
            get
            {
                return (string)_item["BillingCountryID"];
            }
        }

        public string BillingPostal
        {
            get
            {
                return _item["BillingPostal"] == System.DBNull.Value ? "" : _item["BillingPostal"] as string;
            }
        }

    }
}
