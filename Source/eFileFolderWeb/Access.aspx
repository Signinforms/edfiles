﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Access.aspx.cs" Inherits="Access" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="title-container aboutus-title" style="padding-bottom:0px;">
        <div class="inner-wrapper">
            <h1 style="padding-top:15px">Our belief…The reason for our existence is:</h1>
            <p>To Store, Organize and Manage Information…Not Paper!</p>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="inner-wrapper">
        <div class="page-container">
            <div class="inner-wrapper">
                <div class="aboutus-contant">
                    <div class="securityheading" style="line-height:normal">We firmly believe that, all the valuable and necessary information, which currently resides in paper form, i.e., file storage boxes, file cabinets and records storage facilities, needs to be:</div>
                    <ul>
                        <li>Conveniently and securely accessible electronically from anywhere.
                        </li>
                        <li>This Information needs to be easily searchable.</li>
                        <li>It must be Preserved and Backed up <i>(If it is worth storing, it needs to be preserved.  Otherwise, why store it?) </i></li>
                        <li>Needs to be formatted for Analysis, Reporting and Integration.</li>
                        <li>Freely retrievable without ANY retrieval and delivery costs & hassles.</li>
                        <li>And Most of all…It must be available to all type and size budgets.</li>
                    </ul>

                    <div class="securityheading">How do we help organizations?</div>

                    <ul>
                        <li>By simplifying the process of converting large amounts of paper based files to useful electronic data.
                        </li>
                        <li>By eliminating the fear of critical business disruptions and loss of valuable information, utilizing 100s of contingencies and case study findings.</li>
                        <li>With our exclusive, 3D file folder interface for scanned paper file folders and by adhering to a 100% commitment to quality scanning service!</li>
                    </ul>

                    <div class="securityheading">What is it that we do?</div>
                    <ul>
                        <li>Assist in converting, current & historical paper files to digital/electronic format.
                        </li>
                        <li>Provide insight in the gradual implementation of efficient and compliant, reduced paper dependent, filing systems.</li>
                        <li>Most of all, we help you store, organize and manage information…not paper.</li>
                    </ul>

                </div>
            </div>
        </div>
    </div>
</asp:Content>

