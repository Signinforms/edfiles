<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" Title="" %>

<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register Src="../Controls/QuickFind.ascx" TagName="QuickFind" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script language="JavaScript" type="text/jscript">
        $(document).ready(function () {
            $("#<%= ImageButton1.ClientID %>").click(function (e) {
                 e.preventDefault();
                 ChangePassword();
             });

             $("#<%= textfield5.ClientID %>").keypress(function (e) {
                 if (e.keyCode === 13) {
                     e.preventDefault();
                     ChangePassword();
                 }
             });

             function ChangePassword() {
                 $.ajax({
                     type: "POST",
                     url: '../Account/ChangePassword.aspx/ChangePasswordOfUser',
                     contentType: "application/json; charset=utf-8",
                     data: "{ oldPwd:'" + $("#<%= textfield3.ClientID %>").val() + "', newPwd:'" + $("#<%= textfield6.ClientID %>").val() + "'}",
                     dataType: "json",
                     success: function (data) {
                         if (data != null || data.d.length > 0) {
                             alert(data.d);
                         }
                         else {
                             alert("Failed To Update PassWord");
                         }
                     },
                     error: function (result) {
                         console.log('Failed' + result.responseText);
                     }
                 });
             }


         });
     </script>

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Change Password</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div class="left-content">
                <div class="content-title">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="content-box listing-view">
                    <fieldset>
                        <label>Older Password</label>
                        <asp:TextBox ID="textfield3" name="textfield3" TextMode="Password" runat="server" size="30" MaxLength="50" />
                    </fieldset>
                    <fieldset>
                        <label>New Password</label>
                        <asp:TextBox ID="textfield6" name="textfield6" runat="server" TextMode="Password" size="30" MaxLength="50" />
                    </fieldset>
                    <fieldset>
                        <label>Confirm Password</label>
                        <asp:TextBox name="textfield5" runat="server" TextMode="Password" ID="textfield5" size="30" MaxLength="50" />
                    </fieldset>
                    <asp:RequiredFieldValidator runat="server" ID="OPReq" ControlToValidate="textfield3"
                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />A old password is required." />
                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="OPReqE" TargetControlID="OPReq"
                        HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                    <asp:RequiredFieldValidator runat="server" ID="NPReq" ControlToValidate="textfield6"
                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />A new password is required." />
                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="NPReqE" TargetControlID="NPReq"
                        HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                    <asp:RequiredFieldValidator runat="server" ID="RNPReq" ControlToValidate="textfield5"
                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />A confirm password is required." />
                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="RNPReqE" TargetControlID="RNPReq"
                        HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                    <asp:CompareValidator ID="NPV" ControlToCompare="textfield6" ControlToValidate="textfield5"
                        runat="Server" Display="None" ErrorMessage="<b>Compare Password Fields</b><br />A confirm password isn't fit." />
                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="NPVE" TargetControlID="NPV"
                        HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>

                    <fieldset>
                        <label>&nbsp;</label>
                        <asp:Button runat="server" ID="ImageButton1" name="ImageButton1" CssClass="create-btn btn green" Text="Submit" />
                    </fieldset>
                </div>
                </div>
                <div class="right-content">
                <asp:LoginView ID="LoginView1" runat="server">
                    <RoleGroups>
                        <asp:RoleGroup Roles="Offices,Users">
                            <ContentTemplate>
                                <%--<div class="quick-find">
                                    <uc1:QuickFind ID="QuickFind1" runat="server" />
                                </div>--%>
                                <div class="quick-find" style="margin-top: 15px;">
                                    <uc2:ReminderControl ID="ReminderControl1" runat="server" />
                                </div>
                            </ContentTemplate>
                        </asp:RoleGroup>
                    </RoleGroups>
                </asp:LoginView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
