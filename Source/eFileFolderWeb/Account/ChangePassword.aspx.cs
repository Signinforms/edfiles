using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Utility;
using System.Web.Services;

public partial class ChangePassword : LicensePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //string mode = WebConfigurationManager.AppSettings["https_http"].ToLower();
            //if (mode.Equals("https") && !Page.Request.IsSecureConnection)
            //{
            //    string lsURL = "https://";
            //    string port = WebConfigurationManager.AppSettings["https_port"];
            //    lsURL = string.Concat(lsURL, Request.Url.Host, ":", port, Request.Url.PathAndQuery);
            //    lsURL = Server.UrlDecode(lsURL);
            //    Response.Redirect(lsURL, true);
            //    return;
            //}
        }
    }

    [WebMethod]
    public static string ChangePasswordOfUser(string oldPwd, string newPwd)
    {
        ChangePassword changePassword = new ChangePassword();
        return changePassword.ChangePwd(oldPwd, newPwd);
    }

    private string ChangePwd(string oldPwd, string newPwd)
    {
        string password1 = PasswordGenerator.GetMD5(newPwd.Trim());
        string password2 = PasswordGenerator.GetMD5(oldPwd.Trim());
        string strWrongInfor = string.Empty;
        try
        {
            if (password2 == Membership.Provider.GetPassword(Sessions.SwitchedUserName, ""))
                //if (password2 == Membership.Provider.GetPassword(User.Identity.Name, ""))
            {
                MembershipUser user = Membership.GetUser(Sessions.SwitchedUserName);
                //MembershipUser user = Membership.GetUser(User.Identity.Name);
                if (user.ChangePassword(password2, password1))
                {
                    strWrongInfor = "Your password has been updated.";
                    //ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                    //lblMessage.Text = String.Format("Message: successful to change the {0} user's password.", User.Identity.Name);
                }
                else
                {
                    strWrongInfor = "Error: Your password failed to change your password, please try again.";
                    //ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                    //lblMessage.Text = String.Format("Error: failed to change the {0} user's password, please try again.", User.Identity.Name);
                }
            }
            else
            {
                strWrongInfor = String.Format("Error: the old password isn't correct, please input.");
                // ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                //lblMessage.Text = String.Format("Error: the old password isn't correct, please input.");
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = String.Format("Error: {0}.", ex.Message);
        }
        return strWrongInfor;
    }
}
