﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


using Shinetech.DAL;
using Shinetech.Engines;
using Aspose.Cells;
using Style = Aspose.Cells.Style;

public partial class Office_FolderExporter : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (this.Page.User.IsInRole("Offices") || Common_Tatva.IsUserSwitched())
            {
                Account officeUser = new Account(Sessions.SwitchedSessionId);
                //Account officeUser = new Account(Membership.GetUser().ProviderUserKey.ToString());
                string groupsName = officeUser.GroupsName.Value;
                Hashtable hash = new Hashtable();
                string[] groupNames = null;
                if (groupsName != null && !string.IsNullOrEmpty(groupsName.Trim()))
                {
                    groupNames = groupsName.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                    string p = officeUser.Groups.Value == null ? "" : officeUser.Groups.Value;
                  
                }

                DataTable dset = FileFolderManagement.GetFileFoldersForReport(officeUser.UID.Value);

                DataTable dividers = FileFolderManagement.GetDocumentsForReport(officeUser.UID.Value);

                Hashtable hd = new Hashtable();
                
                foreach (DataRow row in dividers.Rows)
                {
                    if (hd.ContainsKey(row["EFFID"].ToString()))
                    {
                        ArrayList dlist = hd[row["EFFID"].ToString()] as ArrayList;
                        if (row["CNT"] == DBNull.Value) row["CNT"] = 0;
                        if (Convert.ToInt32(row["CNT"]).Equals(0))
                        {
                            dlist.Add( row["Name"].ToString());
                        }
                       
                    }
                    else
                    {
                        ArrayList dlist = new ArrayList();
                        if (row["CNT"] == DBNull.Value) row["CNT"] = 0;

                        if (Convert.ToInt32(row["CNT"]).Equals(0))
                        {
                            dlist.Add(row["Name"].ToString());
                        }

                        hd.Add(row["EFFID"].ToString(), dlist);
                    }     
                      
                }
             
                foreach (DataRow row in dset.Rows)
                {
                    if (row["SecurityLevel"].Equals(0))
                    {
                        row["Security"] = "Private";
                        
                        if(row["Groups"] != null){
                            
                            string[] groups= row["Groups"].ToString().Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                            string v = string.Empty;
                            foreach(string g in groups){
                                v += groupNames[Convert.ToInt32(g) - 1] + ";";
                            }

                           row["Groups"] =  v.TrimEnd(';');

                        }
                    }
                    else if (row["SecurityLevel"].Equals(1))
                    {
                        row["Security"] = "Protected";
                    }
                    else if (row["SecurityLevel"].Equals(2))
                    {
                        row["Security"] = "Public";
                    }

                    if(hd.ContainsKey(row["FolderID"].ToString())){
                        ArrayList dlist = hd[row["FolderID"].ToString()] as ArrayList;

                        string nodivider = string.Empty;

                        foreach (string name in dlist)
                        {
                            nodivider += name+";";
                        }

                        row["Tabs with No Documents"] = nodivider;
                    }
                }

                OutFileToStream(dset,"edFiles Report");
              
            }
        }
    }

    public void OutFileToStream(DataTable dt, string tableName)
    {
        dt.Columns.Remove("OfficeID");
        dt.Columns.Remove("FolderID");
        dt.Columns.Remove("SecurityLevel");
  
        Workbook workbook = new Workbook(); //工作簿
        Worksheet sheet = workbook.Worksheets[0]; //工作表
        Cells cells = sheet.Cells;//单元格
        

        //为标题设置样式    
        Aspose.Cells.Style styleTitle = workbook.Styles[workbook.Styles.Add()];//新增样式
        styleTitle.HorizontalAlignment = TextAlignmentType.Center;//文字居中
        styleTitle.Font.Name = "Arial";//文字字体
        styleTitle.Font.Size = 16;//文字大小
        styleTitle.Font.IsBold = true;//粗体

        //样式2
        Style style2 = workbook.Styles[workbook.Styles.Add()];//新增样式
        style2.HorizontalAlignment = TextAlignmentType.Center;//文字居中
        style2.Font.Name = "Arial";//文字字体
        style2.Font.Size = 12;//文字大小
        style2.Font.IsBold = true;//粗体
        style2.IsTextWrapped = true;//单元格内容自动换行
        style2.Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thin;
        style2.Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thin;
        style2.Borders[BorderType.TopBorder].LineStyle = CellBorderType.Thin;
        style2.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thin;

        //样式3
        Style style3 = workbook.Styles[workbook.Styles.Add()];//新增样式
        style3.HorizontalAlignment = TextAlignmentType.Center;//文字居中
        style3.Font.Name = "Arial";//文字字体
        style3.Font.Size = 12;//文字大小
        style3.Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thin;
        style3.Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thin;
        style3.Borders[BorderType.TopBorder].LineStyle = CellBorderType.Thin;
        style3.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thin;

        int Colnum = dt.Columns.Count;//表格列数
        int Rownum = dt.Rows.Count;//表格行数

        //生成行1 标题行   
        cells.Merge(0, 0, 1, Colnum);//合并单元格
        cells[0, 0].PutValue(tableName);//填写内容
        cells[0, 0].SetStyle(styleTitle);
        cells.SetRowHeight(0, 58);

        //生成行2 列名行
        for (int i = 0; i < Colnum; i++)
        {
            cells[1, i].PutValue(dt.Columns[i].ColumnName);
            cells[1, i].SetStyle(style2);
            cells.SetRowHeight(1, 25);
        }

        //生成数据行
        for (int i = 0; i < Rownum; i++)
        {
            for (int k = 0; k < Colnum; k++)
            {
                cells[2 + i, k].PutValue(dt.Rows[i][k].ToString());
                cells[2 + i, k].SetStyle(style3);
            }
            cells.SetRowHeight(2 + i, 24);
        }

       // workbook.SaveOptions.SaveFormat = SaveFormat.Xlsx;
        sheet.AutoFitColumns();
        workbook.Save(Response, tableName+".xls", ContentDisposition.Attachment, workbook.SaveOptions);
       
    } 

    protected string GetFileExtention(StreamType ext)
    {
        string extname = ".pdf";
        switch (ext)
        {
            case StreamType.PDF:
                extname = ".pdf";
                break;
            case StreamType.Word:
                extname = ".doc";
                break;
            case StreamType.Excel:
                extname = ".xls";
                break;
            case StreamType.PPT:
                extname = ".ppt";
                break;
            case StreamType.RTF:
                extname = ".rtf";
                break;
            case StreamType.JPG:
                extname = ".jpg";
                break;
            case StreamType.PNG:
                extname = ".png";
                break;
            default:
                break;
        }

        return extname;
    }


    protected DataSet GetFolderList(string uid)
    {
        return null;

    }
}
