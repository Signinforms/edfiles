<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MyAccount.aspx.cs" Inherits="MyAccount" Title="" %>

<%--<%@ Register Src="../Controls/ReminderControl1.ascx" TagName="ReminderControl" TagPrefix="uc2" %>--%>
<%@ Register Src="../Controls/ReminderControl1.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register Src="../Controls/QuickFind.ascx" TagName="QuickFind" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript">
        //console.log(getRootPath1());
        function gotoHomePage() {
            //window.location = "https://www.edfiles.com/"; //getRootPath1();
            window.location = "http://www.edfiles.com/"; //getRootPath1();
            // window.redirectURL();
        }

        function getRootPath1() {
            var strFullPath = window.document.location.href;
            var strPath = window.document.location.pathname;
            var pos = strFullPath.indexOf(strPath);
            var prePath = strFullPath.substring(0, pos);
            var postPath = strPath.substring(0, strPath.substr(1).indexOf('/') + 1) + "/";

            return (prePath + postPath);
        }

        function LinkButton2_Click() {
            var url = 'FolderExporter.aspx';
            var name = 'folderlist';
            var iWidth = 250;
            var iHeight = 200;
            var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
            var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
            window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',toolbar=no,menubar=no,scrollbars=no,resizeable=no,location=no,status=no');
        }

        var validFilesTypes = ["gif", "png", "jpg", "jpeg"];
        function ValidateFile() {
            var file = document.getElementById("<%=fileExlt1.ClientID%>");
            var path = file.value;
            var ext = path.substring(path.lastIndexOf(".") + 1, path.length).toLowerCase();
            var isValidFile = false;
            for (var i = 0; i < validFilesTypes.length; i++) {
                if (ext == validFilesTypes[i]) {
                    isValidFile = true;
                    break;
                }
            }
            if (!isValidFile) {
                alert("Invalid File. Please upload a File with" +
                 " extension:\n\n" + validFilesTypes.join(", "));
            }
            return isValidFile;
        }

        function pageLoad() {
            var temp = "<%= ConfigurationManager.AppSettings["VirtualDir"]%>" + "<%= Common_Tatva.UploadFolder.Replace("/","") + Common_Tatva.UserLogoFolder %>" + "/" + "<%= Sessions.SwitchedSessionId%>" + ".png";

            $.get(temp)
                .done(function () {
                    $("#<%= imgUserLog.ClientID%>").attr("src", "<%= ConfigurationManager.AppSettings["VirtualDir"]%>" + "<%= Common_Tatva.UploadFolder.Replace("/","") + Common_Tatva.UserLogoFolder %>"
                                                                       + "/" + "<%= Sessions.SwitchedSessionId%>" + ".png");

                }).fail(function () {
                    $("#<%= imgUserLog.ClientID%>").attr("src", "<%= ConfigurationManager.AppSettings["VirtualDir"]%>" + "New/Images/edFile_logo.png");
                })
        }


    </script>
    <style>
        .ajax__fileupload_topFileStatus {
            display: none;
        }
        .filetype, .filesize, .uploadstatus {
            display:none;
        }
        .input-file:before {
            width:85px;
        }
        .userLogoDisplay {
            display: inline-block !important;
            position: absolute;
            max-width: 250px;
            max-height: 50px;
            margin-top: -7px !important;
        }
    </style>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Account Information</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="form-containt listing-contant">
											<div class="left-content">
                        <div class="content-title">My Information &nbsp;<asp:Label ID="labUserName" CssClass="text-green name-text" runat="server" Text=""></asp:Label></div>
                        <div class="content-title">
                            <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                        </div>
                        <div class="content-box listing-view">
                            <div class="content-title">Personal Details</div>

                            <fieldset>
                                <label>First&nbsp;Name</label>
                                <asp:TextBox ID="textfield3" name="textfield3" runat="server"
                                    size="30" MaxLength="20" />
                                <asp:RequiredFieldValidator runat="server" ID="firstNameV" ControlToValidate="textfield3"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The first name is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                    TargetControlID="firstNameV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                            </fieldset>
                            <fieldset>
                                <label>Last&nbsp;Name</label>
                                <asp:TextBox ID="textfield6" name="textfield6" runat="server"
                                    size="30" MaxLength="20" />
                                <asp:RequiredFieldValidator runat="server" ID="lastNameV" ControlToValidate="textfield6"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The last name is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10"
                                    TargetControlID="lastNameV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                            </fieldset>
                            <fieldset>
                                <label>DOB</label>
                                <asp:TextBox ID="textfield15" name="textfield15" runat="server"
                                    size="30" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" PopupPosition="BottomLeft" runat="server"
                                    TargetControlID="textfield15" Format="MM-dd-yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:RequiredFieldValidator runat="server" ID="DobV" ControlToValidate="textfield15"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The DOB is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5"
                                    TargetControlID="DobV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                                <asp:RegularExpressionValidator ID="DobC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The DOB is not a date"
                                    ControlToValidate="textfield15" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                    Visible="true" Display="None"></asp:RegularExpressionValidator><ajaxToolkit:ValidatorCalloutExtender
                                        runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="DobC" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                            </fieldset>
                            <fieldset>
                                <label>Email</label>
                                <asp:TextBox ID="textfield13" name="textfield13" runat="server"
                                    size="30" MaxLength="50" />
                                <asp:RequiredFieldValidator runat="server" ID="EmailReq" ControlToValidate="textfield13"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />An email is required." />
                                <asp:RegularExpressionValidator ID="rev_Email" runat="server" ErrorMessage="<b>Required Field Missing</b><br />It is not an email format."
                                    ControlToValidate="textfield13" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    Visible="true" Display="None"></asp:RegularExpressionValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="EmailReqE" TargetControlID="EmailReq"
                                    HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                    TargetControlID="rev_Email" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                            </fieldset>
                            <fieldset class="full-fieldset">
                                <label>Telephone</label>
                                <asp:TextBox ID="textfield8" name="textfield8" runat="server"
                                    size="30" MaxLength="23" />
                                <asp:RequiredFieldValidator runat="server" ID="telN" ControlToValidate="textfield8"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The telephone is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11"
                                    TargetControlID="telN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                                <asp:RegularExpressionValidator ID="TelV" ControlToValidate="textfield8" runat="server"
                                    ErrorMessage="<b>Required Field Missing</b><br />It is not a telphone format."
                                    ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                    TargetControlID="TelV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                            </fieldset>
                            <fieldset>
                                <label>MobilePhone</label>
                                <asp:TextBox name="textfield11" runat="server" ID="textfield11"
                                    size="20" MaxLength="20" />
                                <asp:RegularExpressionValidator ID="mobileV" ControlToValidate="textfield11" runat="server"
                                    ErrorMessage="<b>Required Field Missing</b><br />It is not a Mobile format."
                                    ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9"
                                    TargetControlID="mobileV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                            </fieldset>
                            <fieldset class="last">
                                <label>Fax</label>
                                <asp:TextBox name="textfield9" type="text" runat="server"
                                    ID="textfield9" size="20" MaxLength="23" />
                                <asp:RegularExpressionValidator ID="faxV" ControlToValidate="textfield9" runat="server"
                                    ErrorMessage="<b>Required Field Missing</b><br />It is not a FAX format." ValidationExpression="^\d{7,}"
                                    Display="None"></asp:RegularExpressionValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8"
                                    TargetControlID="faxV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                            </fieldset>
                            <fieldset>
                                <label>CompanyName</label>
                                <asp:TextBox name="textfield14" runat="server" ID="textfield14"
                                    size="40" MaxLength="50" />
                            </fieldset>
                            <fieldset>
                                <label>Address</label>
                                <asp:TextBox name="textfield10" runat="server" ID="textfield10"
                                    size="40" MaxLength="200" />
                            </fieldset>
                            <fieldset>
                                <label>City</label>
                                <asp:TextBox runat="server" ID="txtCity" size="30" MaxLength="30" />
                            </fieldset>
                            <fieldset>
                                <label>State/Province</label>
                                <asp:DropDownList ID="drpState" runat="server" />
                            </fieldset>
                            <fieldset>
                                <label>Other</label>
                                <asp:TextBox runat="server" ID="txtOtherState" size="30"
                                    MaxLength="30" />
                            </fieldset>
                            <fieldset>
                                <label>Country</label>
                                <asp:DropDownList ID="drpCountry" runat="server" />
                            </fieldset>
                            <fieldset>
                                <label>Zip/Postal</label>
                                <asp:TextBox ID="txtPostal" runat="server" size="30" MaxLength="30" />
                            </fieldset>
                            <fieldset>
                                <label>User Logo</label>
                                <asp:FileUpload ID="fileExlt1" runat="server" CssClass="input-file" Style="float: none;cursor:pointer;max-width:190px;"></asp:FileUpload>

                                <asp:Button runat="server" name="buttonUpload1" ID="buttonUpload1" OnClick="buttonUpload1_Click" ValidateRequestMode="Disabled"
                                    Text="Save" CssClass="create-btn btn-small blue radius-none savebox" OnClientClick="return ValidateFile()" />
                                <img runat="server" id="imgUserLog" class="userLogoDisplay"/>
                            </fieldset>
                        </div>

                        <div class="content-box listing-view" style="margin-top:15px;">
                            <div class="content-title">Credit Card Information</div>

                            <asp:LoginView ID="LoginView2" runat="server">
                                <RoleGroups>
                                    <asp:RoleGroup Roles="Offices">
                                        <ContentTemplate>
                                            <fieldset>
                                                <label>Type&nbsp;of&nbsp;Card</label>
                                                <asp:DropDownList ID="drpTypeOfCard" runat="server" />
                                            </fieldset>

                                            <fieldset>
                                                <label>Card&nbsp;No.</label>
                                                <asp:TextBox runat="server" ID="txtCardNO" MaxLength="50"
                                                    size="30" />
                                                <%--<asp:RequiredFieldValidator runat="server" ID="CardNOV" ControlToValidate="txtCardNO"
                                                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The Card No is required!" />--%>
                                                <%--<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
                                                                            TargetControlID="CardNOV" HighlightCssClass="validatorCalloutHighlight" />--%>
                                                <asp:RegularExpressionValidator ID="CNV" ControlToValidate="txtCardNO" runat="server" ValidationGroup="CardCheck"
                                                    ErrorMessage="<b>Required Field Format</b><br />It is not a Card No format."
                                                    ValidationExpression="^\d{0,20}" Display="None"></asp:RegularExpressionValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" 
                                                    TargetControlID="CNV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                                            </fieldset>
                                            <fieldset>
                                                <label>Expiration&nbsp;Date</label>
                                                <asp:TextBox runat="server" ID="txtExPDate" size="15" /><label
                                                    align="right">(Format: MM/yyyy)</label>
                                                <%-- <asp:RequiredFieldValidator runat="server" ID="CardExpDate" ControlToValidate="txtExPDate"
                                                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The Exp.Date is required!" /><ajaxToolkit:ValidatorCalloutExtender
                                                                                runat="Server" ID="CardExpDateAjax" TargetControlID="CardExpDate" HighlightCssClass="validatorCalloutHighlight" />--%>
                                                <asp:RegularExpressionValidator ID="CardExpDateFormat" ControlToValidate="txtExPDate" ValidationGroup="CardCheck"
                                                    runat="server" ErrorMessage="<b>Required Field Format</b><br />It is not a Exp.Date format."
                                                    ValidationExpression="^\d{2}\/\d{4}" Display="None"></asp:RegularExpressionValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="CardExpDateFormatV" TargetControlID="CardExpDateFormat"
                                                    HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                                            </fieldset>
                                            <fieldset>
                                                <label>City</label>
                                                <asp:TextBox runat="server" ID="txtBillingCity" size="30"
                                                    MaxLength="30" />
                                            </fieldset>
                                            <fieldset>
                                                <label>State/Province</label>
                                                <asp:DropDownList ID="drpBillingState" runat="server" />
                                            </fieldset>
                                            <fieldset>
                                                <label>Other</label>
                                                <asp:TextBox runat="server" ID="txtBillingOtherState" size="30"
                                                    MaxLength="30" />
                                            </fieldset>
                                            <fieldset>
                                                <label>Country</label>
                                                <asp:DropDownList ID="drpBillingCountry" runat="server" />
                                            </fieldset>
                                            <fieldset>
                                                <label>Zip/Postal</label>
                                                <asp:TextBox ID="txtBillingPostal" runat="server" size="30"
                                                    MaxLength="30" />
                                            </fieldset>
                                            <fieldset>
                                                <label>Per File Request Fee</label>
                                                <asp:TextBox ID="txtPerRequestFee" runat="server" ReadOnly="true"
                                                    size="15" Style="background-color: white"
                                                    MaxLength="15" />
                                            </fieldset>
                                            <fieldset>
                                                <label>Total File Requests</label>
                                                <asp:TextBox ID="txtTotalRequest" runat="server" ReadOnly="true" Style="background-color: white"
                                                    size="15"
                                                    MaxLength="15" />
                                            </fieldset>
                                            <fieldset>
                                                <span>Please click
                                    <asp:HyperLink ID="hyperLink2" NavigateUrl="~/Office/FileRequestMonthly.aspx" Style="color: #000000; font-weight: bold;"
                                        Text="here" runat="server" />
                                                    to see more details about your file requests monthly</span>
                                            </fieldset>

                                        </ContentTemplate>
                                    </asp:RoleGroup>
                                </RoleGroups>
                            </asp:LoginView>
                            <asp:HiddenField ID="textBoxReadonly" runat="server" Value="false" />
                        </div>

                        <div class="content-box listing-view" style="border: none; padding: 0;">
                            <div class="form-submit-btn" style="margin-bottom: 30px;">
                                <asp:Button runat="server" CssClass="create-btn btn green" name="imageField" ID="imageField" OnClick="imageField_Click" ValidationGroup="CardCheck" Text="Edit" />
                            </div>

                            <div style="font-size:16px;">
                                If need change your password? Please click
                                    <asp:HyperLink ID="hyperLink1" NavigateUrl="~/Account/ChangePassword.aspx" Style="color: #000000; font-weight: bold;"
                                        Text="here" runat="server" /><span id="dotspan" runat="server">,</span><span style="margin-left: 10px" runat="server" id="flistspan1">All EdFiles list report,click </span>
                                <asp:LinkButton ID="LinkButton2" CausesValidation="false" runat="server" Text="here" Style="color: #000000; font-weight: bold;" OnClientClick="LinkButton2_Click()" />
                                to export
                            </div>
                        </div>
											</div>
											<div class="right-content">
                        <asp:LoginView ID="LoginView1" runat="server">
                            <RoleGroups>
                                <asp:RoleGroup Roles="Offices,Users">
                                    <ContentTemplate>
                                       <%-- <div class="quick-find">
                                            <uc1:QuickFind ID="QuickFind1" runat="server" />
                                        </div>--%>
                                        <%--<div class="quick-find" style="margin-top: 15px;">
                                            <uc2:ReminderControl ID="ReminderControl1" runat="server" />
                                        </div>--%>
                                    </ContentTemplate>
                                </asp:RoleGroup>
                            </RoleGroups>
                        </asp:LoginView>
											</div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="imageField" EventName="Click" />
                    <asp:PostBackTrigger  ControlID="buttonUpload1" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
