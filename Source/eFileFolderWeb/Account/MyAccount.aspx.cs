using System;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.DAL;
using System.IO;

public partial class MyAccount : LicensePage
{
    public string OffcieUserID = string.Empty;
    public string strCardNO;
    public string strCardNOEdit;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Form.Enctype = "multipart/form-data";
        if (!Page.IsPostBack)
        {
            InitTextBox();
            SetTextBoxStatus(Edited);
            imageField.CausesValidation = this.Edited;

            if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users"))
            {
                flistspan1.Visible = false;
                LinkButton2.Visible = false;
                dotspan.Visible = false;
            }
        }
        
    }

    protected void imageField_Click(object sender, EventArgs e)
    {
        if (this.Edited)
        {
            Edited = false;
            //Change the user profile information
            SubmitUserProfile();
            SetTextBoxStatus(Edited);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert('The changes you have made have been saved.');gotoHomePage();", true);
        }
        else
        {
            Edited = true;
            InitTextBox();
            SetTextBoxStatus(Edited);
        }

        imageField.CausesValidation = this.Edited;
    }

    [Bindable(true)]
    [DefaultValue("false")]
    public Boolean Edited
    {
        get
        {
            string b = textBoxReadonly.Value;
            return Boolean.Parse(b);
        }

        set
        {
            textBoxReadonly.Value = value.ToString();
        }
    }

    protected void SetTextBoxStatus(bool status)
    {
        this.textfield3.ReadOnly = !status;
        this.textfield6.ReadOnly = !status;
        this.textfield8.ReadOnly = !status;
        this.textfield9.ReadOnly = !status;
        this.textfield10.ReadOnly = !status;
        this.textfield11.ReadOnly = !status;

        this.textfield11.ReadOnly = !status;
        this.textfield13.ReadOnly = !status;
        this.textfield14.ReadOnly = !status;
        this.textfield15.ReadOnly = !status;

        this.drpCountry.Enabled = status;
        this.drpState.Enabled = status;
        this.txtOtherState.ReadOnly = !status;
        this.txtCity.ReadOnly = !status;
        this.txtPostal.ReadOnly = !status;


        this.CalendarExtender2.Enabled = status;

        if (User.IsInRole("Offices"))
        {
            DropDownList TypeOfCard = LoginView2.FindControl("drpTypeOfCard") as DropDownList;
            TypeOfCard.Enabled = status;

            TextBox CardNO = LoginView2.FindControl("txtCardNO") as TextBox;
            CardNO.ReadOnly = !status;
            if (status)
            {
                CardNO.Text = strCardNO;
            }
            else
            {
                CardNO.Text = strCardNOEdit;
            }

            TextBox ExPDate = LoginView2.FindControl("txtExPDate") as TextBox;
            ExPDate.ReadOnly = !status;

            DropDownList BillingCountry = LoginView2.FindControl("drpBillingCountry") as DropDownList;
            BillingCountry.Enabled = status;

            DropDownList BillingState = LoginView2.FindControl("drpBillingState") as DropDownList;
            BillingState.Enabled = status;

            TextBox BillingOtherState = LoginView2.FindControl("txtBillingOtherState") as TextBox;
            BillingOtherState.ReadOnly = !status;

            TextBox BillingCity = LoginView2.FindControl("txtBillingCity") as TextBox;
            BillingCity.ReadOnly = !status;

            TextBox BillingPostal = LoginView2.FindControl("txtBillingPostal") as TextBox;
            BillingPostal.ReadOnly = !status;

        }

        //string imageName = (status == false) ? "edit_btn.gif" : "submit_btn.gif";
        //this.imageField.ImageUrl = "~/images/" + imageName;
        string imageName = (status == false) ? "Edit" : "Submit";
        this.imageField.Text = imageName;
    }

    protected void SubmitUserProfile()
    {
        Account user = UserManagement.GetWebUser(Sessions.SwitchedSessionId);
        //Account user = UserManagement.GetWebUser(Membership.GetUser().ProviderUserKey.ToString());
        if (user == null) return;

        user.Firstname.Value = this.textfield3.Text.Trim();
        user.Lastname.Value = this.textfield6.Text.Trim();
        if(!string.IsNullOrEmpty(this.textfield15.Text.Trim()))
            user.DOB.Value = DateTime.ParseExact(this.textfield15.Text.Trim(), "mm-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
        else
            user.DOB.Value = DateTime.ParseExact(DateTime.Now.ToString(), "mm-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);

        user.Email.Value = this.textfield13.Text.Trim();
        user.Telephone.Value = this.textfield8.Text.Trim();
        user.MobilePhone.Value = this.textfield11.Text.Trim();
        user.FaxNumber.Value = this.textfield9.Text.Trim();
        user.CompanyName.Value = this.textfield14.Text.Trim();
        user.Address.Value = this.textfield10.Text.Trim();

        user.CountryID.Value = this.drpCountry.SelectedValue;
        user.StateID.Value = this.drpState.SelectedValue;
        user.OtherState.Value = this.txtOtherState.Text.Trim();
        user.City.Value = this.txtCity.Text.Trim();
        user.Postal.Value = this.txtPostal.Text.Trim();


        if (User.IsInRole("Offices"))
        {
            DropDownList TypeOfCard = LoginView2.FindControl("drpTypeOfCard") as DropDownList;
            user.CardType.Value = TypeOfCard.SelectedValue;

            TextBox CardNO = LoginView2.FindControl("txtCardNO") as TextBox;
            user.CreditCard.Value = CardNO.Text.Trim();
            strCardNO = CardNO.Text.Trim();
            if (strCardNO.Length > 4)
            {
                string strLast4 = strCardNO.Substring(strCardNO.Length - 4, 4);
                string strFirst = strCardNO.Substring(0, strCardNO.Length - 4);

                for (int i = 0; i < strFirst.Length; i++)
                {
                    strCardNOEdit += "*";
                }
                strCardNOEdit += strLast4;
                CardNO.Text = strCardNOEdit;
            }
            else
            {
                CardNO.Text = strCardNO;
            }


            TextBox ExPDate = LoginView2.FindControl("txtExpDate") as TextBox;
            if (!string.IsNullOrEmpty(ExPDate.Text.Trim()))
            {
                try
                {
                    user.ExpirationDate.Value = DateTime.Parse(ExPDate.Text.Trim());
                }
                catch (Exception)
                {

                    user.ExpirationDate.Value = DateTime.Now.AddYears(1);
                }
                
            }
            else
            {
                user.ExpirationDate.Value = DateTime.Now.AddYears(1);
            }

            DropDownList BillingCountry = LoginView2.FindControl("drpBillingCountry") as DropDownList;
            user.BillingCountryId.Value = BillingCountry.SelectedValue;

            DropDownList BillingState = LoginView2.FindControl("drpBillingState") as DropDownList;
            user.BillingStateId.Value = BillingState.SelectedValue;

            TextBox BillingOtherState = LoginView2.FindControl("txtBillingOtherState") as TextBox;
            user.BillingOtherState.Value = BillingOtherState.Text.Trim();

            TextBox BillingCity = LoginView2.FindControl("txtBillingCity") as TextBox;
            user.BillingCity.Value = BillingCity.Text.Trim();

            TextBox BillingPostal = LoginView2.FindControl("txtBillingPostal") as TextBox;
            user.BillingPostal.Value = BillingPostal.Text.Trim();
        }

        user.Update();
    }

    protected void InitTextBox()
    {
        Account user = UserManagement.GetWebUser(Sessions.SwitchedSessionId);
        //Account user = UserManagement.GetWebUser(Membership.GetUser().ProviderUserKey.ToString());
        if (user == null) return;
        labUserName.Text = user.Name.Value;
        this.textfield3.Text = user.Firstname.Value;
        this.textfield6.Text = user.Lastname.Value;
        this.textfield15.Text = user.DOB.Value.ToString("MM-dd-yyyy");
        this.textfield13.Text = user.Email.Value;

        this.textfield8.Text = user.Telephone.Value;
        this.textfield11.Text = user.MobilePhone.Value;
        this.textfield9.Text = user.FaxNumber.Value;
        this.textfield14.Text = user.CompanyName.Value;
        this.textfield10.Text = user.Address.Value;

        this.drpCountry.DataSource = Countries;
        this.drpCountry.DataValueField = "CountryID";
        this.drpCountry.DataTextField = "CountryName";
        this.drpCountry.DataBind();
        DataRow[] foundCountryID = Countries.Select("CountryID = '" + user.CountryID.Value + "'");
        if (foundCountryID.Length != 0)
        {
            this.drpCountry.SelectedValue = user.CountryID.Value;
        }
        else
        {
            this.drpCountry.SelectedValue = "US";
        }

        this.drpState.DataSource = States;
        this.drpState.DataValueField = "StateID";
        this.drpState.DataTextField = "StateName";
        this.drpState.DataBind();
        DataRow[] foundStateID = States.Select("StateID = '" + user.StateID.Value + "'");
        if (foundStateID.Length != 0)
        {
            this.drpState.SelectedValue = user.StateID.Value;
        }
        else
        {
            this.drpState.SelectedValue = States.Rows[0][0].ToString();
        }

        this.txtOtherState.Text = user.OtherState.Value;
        this.txtCity.Text = user.City.Value;
        this.txtPostal.Text = user.Postal.Value;
        
        //if (User.IsInRole("Offices") || User.IsInRole("Users"))
        //{
        //    eFileFolderTR.Visible = true;
        //    bindInfor();
        //}



        if (User.IsInRole("Offices"))
        {

            DropDownList TypeOfCard = LoginView2.FindControl("drpTypeOfCard") as DropDownList;
            TypeOfCard.DataSource = TypeofCard;
            TypeOfCard.DataBind();
            if (user.CardType.Value != "")
            {
                TypeOfCard.SelectedValue = user.CardType.Value;
            }

            TextBox CardNO = LoginView2.FindControl("txtCardNO") as TextBox;
            strCardNO = user.CreditCard.Value;
            if (strCardNO == null)
            {
                CardNO.Text = "";
            }
            else
            {
                if (strCardNO.Length > 4)
                {
                    string strLast4 = strCardNO.Substring(strCardNO.Length - 4, 4);
                    string strFirst = strCardNO.Substring(0, strCardNO.Length - 4);

                    for (int i = 0; i < strFirst.Length; i++)
                    {
                        strCardNOEdit += "*";
                    }
                    strCardNOEdit += strLast4;
                    CardNO.Text = strCardNOEdit;
                }
                else
                {
                    CardNO.Text = strCardNO;
                }
            }



            TextBox ExPDate = LoginView2.FindControl("txtExpDate") as TextBox;
            DateTimeFormatInfo myDTFI = new CultureInfo("en-US", false).DateTimeFormat;
            ExPDate.Text = user.ExpirationDate.Value.ToString("MM/yyyy", myDTFI);

            try
            {
                DateTime.Parse(ExPDate.Text);
            }
            catch (Exception)
            {

                ExPDate.Text = DateTime.Now.AddYears(1).ToString("MM/yyyy", myDTFI);
            }

            DropDownList BillingCountry = LoginView2.FindControl("drpBillingCountry") as DropDownList;
            BillingCountry.DataSource = Countries;
            BillingCountry.DataValueField = "CountryID";
            BillingCountry.DataTextField = "CountryName";
            BillingCountry.DataBind();
            BillingCountry.SelectedValue = user.BillingCountryId.Value;

            DropDownList BillingState = LoginView2.FindControl("drpBillingState") as DropDownList;
            BillingState.DataSource = States;
            BillingState.DataValueField = "StateID";
            BillingState.DataTextField = "StateName";
            BillingState.DataBind();
            if (user.StateID.Value != "")
            {
                BillingState.SelectedValue = user.BillingStateId.Value;
            }

            TextBox BillingOtherState = LoginView2.FindControl("txtBillingOtherState") as TextBox;
            BillingOtherState.Text = user.BillingOtherState.Value;

            TextBox BillingCity = LoginView2.FindControl("txtBillingCity") as TextBox;
            BillingCity.Text = user.BillingCity.Value;

            TextBox BillingPostal = LoginView2.FindControl("txtBillingPostal") as TextBox;
            BillingPostal.Text = user.BillingPostal.Value;

            TextBox PerRequestFee = LoginView2.FindControl("txtPerRequestFee") as TextBox;
            PerRequestFee.Text = string.Format("${0}",user.PerFileRequestFee.Value.ToString("F2"));

            TextBox TotalRequest = LoginView2.FindControl("txtTotalRequest") as TextBox;
            TotalRequest.Text = Convert.ToString(UserManagement.GetTotalFileRequestsNumber(user.UID.Value));//user.TotalFileRequest.Value.ToString();
           
        }



    }

    public RecentFolder RecentFolder
    {
        get
        {
            RecentFolder recent = null;

            if (this.Session["RecentFolder"] == null)
            {
                recent = new RecentFolder(OffcieUserID);
            }
            else
            {
                recent = this.Session["RecentFolder"] as RecentFolder;
            }

            return recent;
        }
        set { this.Session["RecentFolder"] = value; }
    }

    public ArrayList TypeofCard
    {
        get
        {
            object obj = Application["TypeofCard"];

            ArrayList list = new ArrayList();
            if (obj == null)
            {
                list.Add("Visa");
                list.Add("MasterCard");
                list.Add("Amex");
                list.Add("Discover");

                Application["TypeofCard"] = list;
            }
            else
            {
                list = obj as ArrayList;
            }

            return list;
        }
    }

    public DataTable States
    {
        get
        {
            object obj = Application["States"];

            DataTable table = null;
            //if (obj == null)
           // {
                table = UserManagement.GetStates();
                Application["States"] = table;
            //}
           // else
           // {
            //    table = obj as DataTable;
           // }

            return table;
        }
    }

    public DataTable Countries
    {
        get
        {
            object obj = Application["Countries"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetCountries();
                Application["Countries"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    protected void LinkButton2_Click(object sender, EventArgs e)
    {
    }

    protected void buttonUpload1_Click(object sender, EventArgs e)
    {
        try
        {
            if (fileExlt1.PostedFile != null)
            {
                string path = Server.MapPath("~/" + Common_Tatva.UploadFolder + Common_Tatva.UserLogoFolder);

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                fileExlt1.PostedFile.SaveAs(path + "\\" + Sessions.SwitchedSessionId + ".png");
                //fileExlt1.PostedFile.SaveAs(path + "\\" + Sessions.UserId + ".png");
            }
        }
        catch (Exception ex)
        { }
    }
}
