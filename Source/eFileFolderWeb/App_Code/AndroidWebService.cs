﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Shinetech.Utility;
using DataQuicker2.Framework;

/// <summary>
///AndroidWebService 的摘要说明
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
//若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消对下行的注释。 
// [System.Web.Script.Services.ScriptService]
public class AndroidWebService : System.Web.Services.WebService {

    public AndroidWebService () {

        //如果使用设计的组件，请取消注释以下行 
        //InitializeComponent(); 
    }

    [WebMethod]
    public bool LoginUser(string userId, string pwd)
    {
        return UserManagement.LoginUser(userId, pwd);
    }
    
}
