﻿using System.Web;
using System.Web.Optimization;

/// <summary>
/// Summary description for BundleConfig
/// </summary>
public class BundleConfig
{
    public static void RegisterBundles(BundleCollection bundles)
    {
        bundles.Add(new ScriptBundle("~/bundles/pdfjs").Include(
                        "~/New/js/PdfJS/util.js",
                        "~/New/js/PdfJS/api.js",
                        "~/New/js/PdfJS/canvas.js",
                        "~/New/js/PdfJS/webgl.js",
                        "~/New/js/PdfJS/font_loader.js",
                        "~/New/js/PdfJS/annotation_helper.js",
                        "~/New/js/PdfJS/worker_loader.js",
                        "~/New/js/PdfJS/util.js",
                        "~/New/js/PdfJS/util.js"
                        ));
    }
}