﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(eFileFolderWeb.Startup))]
namespace eFileFolderWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
