﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Archive
/// </summary>
public class Archive
{
    public int ArchiveId { get; set; }
    public string FileName { get; set; }
    public string PathName { get; set; }
    public int FolderId { get; set; }
    public float Size { get; set; }
    public object JsonData { get; set; }
    public bool IsDeleted { get; set; }
}