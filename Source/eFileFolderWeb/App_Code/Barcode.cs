﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Barcode 的摘要说明
/// </summary>
[Serializable()]
public class Barcode
{
	public Barcode(int th, string code)
	{
        BarNumber = code;
        Number = th;
	}

    public string BarNumber { get; set; }

    public int Number { get; set; }
}