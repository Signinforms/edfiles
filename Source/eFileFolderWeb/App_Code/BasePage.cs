using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for BasePage
/// </summary>
public class BasePage : Page {

	public override string StyleSheetTheme {
		get {
		    return base.StyleSheetTheme;
		}
	}

	public BasePage() : base() {
      
	}

}
