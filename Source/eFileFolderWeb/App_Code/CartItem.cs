using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for CartItem
/// </summary>
[Serializable]
public class CartItem
{
    private int _ID;
    private string _Name;
    private decimal _Price;
    private int _Quantity = 1;//for license always is 1;

    private int _LicenseID = 0;
    private DateTime _PurchaseDate;
    private DateTime _OverdueDate;
    private string _Flag = "N";//N for normal status, O for overdue status.

    public int ID
    {
        get { return _ID; }
    }

    public string Name
    {
        get { return _Name; }
    }

    public decimal Price
    {
        get { return _Price; }
    }

    public int Quantity
    {
        get { return _Quantity; }
        set { _Quantity = value; }
    }

    public int LicenseID
    {
        get { return _LicenseID; }
        set { _LicenseID = value; }
    }

    public DateTime PurchaseDate
    {
        get { return _PurchaseDate; }
        set { _PurchaseDate = value; }
    }

    public DateTime OverdueDate
    {
        get { return _OverdueDate; }
        set { _OverdueDate = value; }
    }

    public CartItem(int ID, string Name, decimal Price)
    {
        _ID = ID;
        _Name = Name;
        _Price = Price;
    }
}
