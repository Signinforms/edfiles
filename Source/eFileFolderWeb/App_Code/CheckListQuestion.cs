﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;

using DataQuicker2.Framework;
using System.Collections.Specialized;

/// <summary>
/// Summary description for CheckListQuestion
/// </summary>

namespace Shinetech.DAL
{
    public class CheckListQuestion
    {
        #region Properties
        public int ID { get; set; }
        public int FolderID { get; set; }
        public string Question { get; set; }
        public DateTime CreatedOn { get; set; }
        public int FolderLogID { get; set; }
        #endregion

        public static string ConnectionName = "Default";
        private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;

        #region Add
        public static int CreateCheckListQuestion(CheckListQuestion question)
        {
            int id = 0;
            try
            {
                //string sql = "INSERT INTO CheckListQuestion(FolderID, Question,CreatedOn,FolderLogID) Values(" + question.FolderID + ",'" + question.Question + "','" + question.CreatedOn + "', "+ question.FolderLogID +")";
                 string sql = "INSERT INTO CheckListQuestion(FolderID, Question,CreatedOn,FolderLogID) Values(" + question.FolderID + ",'" + question.Question + "','" + question.CreatedOn.ToString("yyyy-MM-dd HH:mm:ss.fff") + "', "+ question.FolderLogID +")";
                SqlHelper helper = new SqlHelper("Default");
                id = helper.ExecuteNonQuery(sql);
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
            return id;
        }
        #endregion

        #region Get List
        public static DataTable GetCheckListQuestion(string folderId)
        {
            string sqlstr = "SELECT ID,FolderID,Question,CreatedOn FROM dbo.CheckListQuestion WHERE FolderId = " + folderId + " ORDER BY CreatedOn DESC";
            SqlHelper sql = new SqlHelper("Default");
            DataTable table = new DataTable();
            try
            {
                sql.Fill(table, sqlstr);
                return table;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static DataTable GetCheckListQuestion1()
        {
            string sqlstr = "SELECT Question FROM dbo.CheckListQuestion";
            SqlHelper sql = new SqlHelper("Default");
            DataTable table = new DataTable();
            try
            {
                sql.Fill(table, sqlstr);
                return table;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion

        #region Get Question Answer List
        public static DataTable GetCheckListQuestionAnswer(string folderId)
        {
            string sqlstr = "Select cq.Question, CA.Answer,CA.CreatedOn From CheckListQuestion CQ "+
                            " Inner Join CheckListAnswer CA On CA.FK_QuestionID = CQ.ID" + 
                            " WHERE CQ.FolderId = " + folderId + " ORDER BY CreatedOn DESC";
            SqlHelper sql = new SqlHelper("Default");
            DataTable table = new DataTable();
            try
            {
                sql.Fill(table, sqlstr);
                return table;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static DataTable GetCheckListQuestionAnswer1(string folderId)
          {
            string sqlstr = "Select cq.Question,CA.ID, CA.Answer,A.Name,CA.CreatedOn From CheckListQuestion CQ " +
                            " Left Join CheckListAnswer CA On CA.FK_QuestionID = CQ.ID " +
                            "Inner Join account A On A.UID = CA.UserID" +
                             " WHERE CQ.FolderId = " + folderId + " ORDER BY CreatedOn DESC";
            SqlHelper sql = new SqlHelper("Default");
            DataTable table = new DataTable();
            try
            {
                sql.Fill(table, sqlstr);
                return table;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static DataTable UpdateCheckListQuestionAnswer1(string answer,int id)
        {
            string sqlstr = "Update CheckListAnswer set answer = '" + answer + "' WHERE Id = '" + id + "'";
            //string sql = "UPDATE main SET s_name='" + TextBox1.Text + "',inst_code='" + DropDownList1.SelectedItem + "',ms_oms='" + Label7.Text + "',elligiblity='" + Label12.Text + "',Board='" + DropDownList5.SelectedItem + "',percentage='" + TextBox4.Text + "' WHERE elg_id = '" + DropDownList4.SelectedItem + "'";
            SqlHelper sql = new SqlHelper("Default");
            DataTable table = new DataTable();
            try
            {
                sql.Fill(table, sqlstr);
                return table;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region Save Answer
        public static void AddAnswer(DataTable dt)
        {
            SqlConnection conn = new SqlConnection(ConnectionString);
            try
            {
                SqlBulkCopy bulkCopy = new SqlBulkCopy(conn);
                conn.Open();
                bulkCopy.ColumnMappings.Add("UserID", "UserID");
                bulkCopy.ColumnMappings.Add("FK_QuestionID", "FK_QuestionID");
                bulkCopy.ColumnMappings.Add("Answer", "Answer");
                bulkCopy.ColumnMappings.Add("CreatedOn", "CreatedOn");
                bulkCopy.DestinationTableName = "CheckListAnswer";
                bulkCopy.WriteToServer(dt);
            }
            catch (Exception ex)
            {
            }
            finally
            {
                conn.Close();
            }
        }
        #endregion
    }
}