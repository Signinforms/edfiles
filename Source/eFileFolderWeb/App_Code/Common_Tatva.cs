﻿using Shinetech.Engines;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Configuration;
using System.Data.SqlClient;
using Shinetech.DAL;
using Shinetech.Utility;
using iTextSharp.text.pdf;
using System.Reflection;
using System.Web.Hosting;
using Elasticsearch.Net;
using Nest;
using System.Web.UI;

/// <summary>
/// Summary description for Common_Tatva
/// </summary>
public static class Common_Tatva
{
    #region Properties
    private static readonly string ConnectionString =
       ConfigurationManager.ConnectionStrings["Default"].ConnectionString;

    public static readonly string Region = ConfigurationManager.AppSettings["RackSpaceRegion"];

    public static string RackSpaceDownloadDirectory = ConfigurationManager.AppSettings["RackSpaceServerFolder"];
    public static string RackSpaceThumbnailDownload = ConfigurationManager.AppSettings["RackSpaceServerThumbnailFolder"];
    public static string RackSpaceWorkareaDownload = ConfigurationManager.AppSettings["RackSpaceServerWorkareaFolder"];
    public static string RackSpaceEdFormsDownload = ConfigurationManager.AppSettings["RackSpaceServerEdFormsFolder"];
    public static string RackSpaceNewSplitWorkareaDownload = ConfigurationManager.AppSettings["RackSpaceServerNewSplitWorkareaFolder"];
    public static string NewSplitEdFilesDownload = ConfigurationManager.AppSettings["ServerNewEdFilesFolder"];
    public static string RackSpaceArchiveDownload = ConfigurationManager.AppSettings["RackSpaceServerArchiveFolder"];
    public static string RackspaceDeletedDirectory = ConfigurationManager.AppSettings["deletedDirectory"];
    public static string UploadFolder = ConfigurationManager.AppSettings["UploadFolder"];
    public static string CoverSheetFolder = ConfigurationManager.AppSettings["CoverSheet"];
    public static string EstimationUserName = ConfigurationManager.AppSettings["EstimationUserName"];
    public static string UserLogoFolder = ConfigurationManager.AppSettings["UserLogoFolder"];

    /// <summary>
    /// The estimations path
    /// </summary>
    private static readonly string ProposalTemplatePath = ConfigurationManager.AppSettings.Get("ProposalTemplatePath");
    //public  Common_Tatva()
    //{
    //    //
    //    // TODO: Add constructor logic here
    //    //

    //}

    private static string UrlSuffix
    {
        get
        {
            if (HttpContext.Current.Request.ApplicationPath == "/")
            {
                if (HttpContext.Current.Request.Url.Port == 80)
                    return HttpContext.Current.Request.Url.Host + HttpContext.Current.Request.ApplicationPath;
                else
                    return HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port.ToString() + HttpContext.Current.Request.ApplicationPath;
            }
            else
            {
                if (HttpContext.Current.Request.Url.Port == 80)
                    return HttpContext.Current.Request.Url.Host + HttpContext.Current.Request.ApplicationPath + "/";
                else
                    return HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port.ToString() + HttpContext.Current.Request.ApplicationPath + "/";
            }
        }

    }

    public static string UrlBase
    {
        get
        {
            return "http://" + UrlSuffix;
        }
    }

    public static string CurrentVolume
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
        }
    }

    public static string Inbox
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["Inbox"];
        }
    }

    public static string eFileFlow
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["eFileFlow"];
        }
    }

    public static string ScanInBox
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["ScanInBox"];
        }
    }

    public static string WorkAreaPath
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["WorkareaPath"];
        }
    }

    public static string eFileFlowPath
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["eFileFlowPath"];
        }
    }

    public static string InboxPath
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["InboxPath"];
        }
    }

    public static string ExpirationTime
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["ValidShareIdHours"];
        }
    }
    #endregion

    #region Elastic Properties

    public static string hostString = ConfigurationManager.AppSettings.Get("ElasticHostString");
    public static string elasticUserName = ConfigurationManager.AppSettings.Get("ElasticUserName");
    public static string elasticUserSecret = ConfigurationManager.AppSettings.Get("ElasticUserSecret");
    public static string elasticIndexName = ConfigurationManager.AppSettings.Get("ElasticDocIndices");
    public static Uri[] hosts = hostString.Split(',').Select(x => new Uri(x)).ToArray();
    public static StaticConnectionPool connectionPool = new StaticConnectionPool(hosts);
    public static ConnectionSettings settings = new ConnectionSettings(connectionPool)
                                                                         .DefaultMappingFor<DocumentIndex>(i => i
                                                                         .IndexName(elasticIndexName)
                                                                         .TypeName("pdf"))
                                                                         .BasicAuthentication(elasticUserName, elasticUserSecret).DisableDirectStreaming();
    public static ElasticClient highClient = new ElasticClient(settings);

    #endregion

    #region General Methods

    /// <summary>
    /// Get WorkAreaId by Path name
    /// </summary>
    /// <param name="workareaPath"></param>
    /// <returns></returns>
    public static int GetWorkAreaId(string workareaPath)
    {
        DataTable dtWorkArea = (DataTable)System.Web.HttpContext.Current.Session["WorkArea_DataSource"];
        if (dtWorkArea.Rows.Count > 0)
        {
            for (int i = 0; i < dtWorkArea.Rows.Count; i++)
            {
                if (dtWorkArea.Rows[i]["PathName"].ToString() == workareaPath.Replace("WorkArea", "workArea"))
                {
                    return Convert.ToInt32(dtWorkArea.Rows[i]["WorkAreaId"].ToString());
                }
            }
        }
        return 0;
    }

    /// <summary>
    /// Move only document from source to target folder(tab)
    /// </summary>
    /// <param name="newPath"></param>
    /// <param name="oldPath"></param>
    /// <returns></returns>
    public static int MoveFilesToAnotherFolder(string newPath, string oldPath)
    {
        try
        {
            string sourceFile = string.Empty;
            string destinationFile = string.Empty;
            sourceFile = HttpContext.Current.Server.MapPath("~/" + oldPath);
            destinationFile = HttpContext.Current.Server.MapPath("~/" + newPath);
            if (Convert.ToString(Path.GetExtension(sourceFile)).ToLower() == ".jpg" || Convert.ToString(Path.GetExtension(sourceFile)).ToLower() == ".jpeg")
            {
                var fileExceptExtension = Path.GetDirectoryName(sourceFile) + "\\" + Path.GetFileNameWithoutExtension(sourceFile);
                var pdfFileExceptExtension = Path.GetDirectoryName(destinationFile) + "\\" + Path.GetFileNameWithoutExtension(destinationFile);
                if (File.Exists(fileExceptExtension + ".jpeg"))
                    File.Move(fileExceptExtension + ".jpeg", destinationFile);
                else if (File.Exists(fileExceptExtension + ".jpg"))
                    File.Move(fileExceptExtension + ".jpg", destinationFile);
                if (File.Exists(fileExceptExtension + ".pdf"))
                    File.Move(fileExceptExtension + ".pdf", pdfFileExceptExtension + ".pdf");
            }
            else
                File.Move(sourceFile, destinationFile);
            return Convert.ToInt32(Enum_Tatva.FileMove.FileMoveSuccess);
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, " newPath = " + newPath, " oldPath = " + oldPath, ex);
            return Convert.ToInt32(Enum_Tatva.FileMove.FileMoveError);
        }
    }

    /// <summary>
    /// After moving document to target folder, delete all pages like(SWF file) from source folder
    /// </summary>
    /// <param name="page"></param>
    /// <returns></returns>
    public static bool DeletePagesFromFolder(DataSet page)
    {
        try
        {
            string file = string.Empty;
            for (int i = 0; i < page.Tables[0].Rows.Count; i++)
            {
                file = HttpContext.Current.Server.MapPath(Convert.ToString(page.Tables[0].Rows[i]["ImagePath"]));
                if ((File.Exists(file)))
                    File.Delete(file);
            }
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    /// <summary>
    /// Insert document into database and generate its SWF file in target folder.
    /// </summary>
    /// <param name="dividerId">The divider identifier.</param>
    /// <param name="newPath">The new path.</param>
    /// <param name="documentOrder">The document order.</param>
    /// <param name="displayFileName">Display name of the file.</param>
    /// <param name="className">Name of the class.</param>
    /// <param name="uid">The uid.</param>
    /// <param name="workItemPath">The work item path.</param>
    /// <returns></returns>
    public static string InsertDocument(int dividerId, string newPath, int documentOrder, string displayFileName, string className, string uid = null, string workItemPath = null, string fullFileName = null)
    {
        string guid = Guid.NewGuid().ToString();
        int newDocumentId = 0;

        byte[] fileBytes = System.IO.File.ReadAllBytes(HttpContext.Current.Server.MapPath("~/" + newPath));

        WorkItem item = GetWorkItem(GetFolderIdFromPath(newPath), dividerId, !string.IsNullOrEmpty(workItemPath) ? Path.GetFileName(HttpContext.Current.Server.MapPath("~/" + workItemPath)) : Path.GetFileName(HttpContext.Current.Server.MapPath(newPath)),
                        Path.GetExtension(HttpContext.Current.Server.MapPath("~/" + newPath)), fileBytes, uid);
        int order = FlashViewer.GetDocumentOrder(item.DividerID);
        WriteLogs(guid + " Step 1 order: " + order);
        //item.MachinePath = HttpContext.Current.Server.MapPath(item.Path);
        //item.MachinePath = string.Concat(basePath, Path.DirectorySeparatorChar, item.FolderID,
        //Path.DirectorySeparatorChar, dividerIdForFile);

        if (!Directory.Exists(item.MachinePath))
        {
            Directory.CreateDirectory(item.MachinePath);
        }
        WriteLogs(guid + " Step 2 order: " + order + " machinePath " + item.MachinePath);
        try
        {
            float size = fileBytes.Length / (1024 * 1024); // IN MB.
            WriteLogs(guid + " Step 3 order: " + order + " machinePath " + item.MachinePath + " size " + size);
            string encodeName = HttpUtility.UrlPathEncode(displayFileName);
            encodeName = encodeName.Replace("%", "$");
            WriteLogs(guid + " Step 4 order: " + order + " machinePath " + item.MachinePath + " size " + size + " encodeName " + encodeName);
            string source = string.Concat(item.MachinePath, Path.DirectorySeparatorChar, encodeName);
            WriteLogs(guid + " Step 5 order: " + order + " machinePath " + item.MachinePath + " size " + size + " encodeName " + encodeName + " source " + source);
            var pageCount = 1;
            if (item.contentType == StreamType.JPG)
            {
                Jpeg2Engine jpeg2Engine = new Jpeg2Engine(item);
                jpeg2Engine.generatePDFfromImage(source + ".jpg", source + ".pdf");
            }
            else if (item.contentType == StreamType.PDF)
            {
                PdfReader pdfReader = new PdfReader(source + ".pdf");
                pageCount = pdfReader.NumberOfPages;
                pdfReader.Close();
            }
            else if ((item.contentType != StreamType.JPG) || (item.contentType != StreamType.PDF))
            {
                item.contentType = StreamType.OTHERS;              
            }
            WriteLogs(guid + " Step 6 order: " + order + " machinePath " + item.MachinePath + " size " + size + " encodeName " + encodeName + " source " + source + " pageCount " + pageCount);
            string outputValue = General_Class.DocumentsInsert(displayFileName, item.FolderID, item.UID, item.Path, item.DividerID, pageCount, size, displayFileName, (int)item.contentType, order + 1, className, (int)item.contentType == 14 ? fullFileName : null);
            WriteErrorLog(Convert.ToString(item.FolderID), Convert.ToString(item.DividerID), displayFileName + "output value is " + outputValue, new Exception());
            newDocumentId = Convert.ToInt32(outputValue);
            return newDocumentId + "#" + (order + 1);
        }
        catch (Exception ex)
        {
            WriteErrorLog(Convert.ToString(item.FolderID), Convert.ToString(item.DividerID), displayFileName, ex);
            return Convert.ToString(newDocumentId);
        }

        //try
        //{
        //    IConvert task = EngineProvider.GetEngine(item);
        //    PdfDesc result = task.Execute();

        //    if (documentOrder == 0)
        //    {
        //        int order = FlashViewer.GetDocumentOrder(Convert.ToInt32(dividerId));
        //        result.DocumentOrder = order + 1;
        //    }
        //    else
        //        result.DocumentOrder = documentOrder;

        //    if (result != null)
        //    {
        //        newDocumentId = task.callbackExecute(result);
        //    }
        //    return newDocumentId + "#" + result.DocumentOrder;
        //}
        //catch (Exception exp)
        //{
        //    return Convert.ToString(newDocumentId);
        //}
    }

    /// <summary>
    /// Insert scan document into database and generate its SWF file in target folder.
    /// </summary>
    /// <param name="dividerId"></param>
    /// <param name="newPath"></param>
    /// <returns></returns>
    public static int InsertForScanDocument(int dividerId, string newPath, int documentOrder, string displayFileName, string className, string uid)
    {
        int newDocumentId = 0;
        byte[] fileBytes = System.IO.File.ReadAllBytes(HttpContext.Current.Server.MapPath(newPath));

        WorkItem item = GetWorkItem(GetFolderIdFromPath(newPath), dividerId, Path.GetFileName(HttpContext.Current.Server.MapPath(newPath)),
                         Path.GetExtension(HttpContext.Current.Server.MapPath(newPath)), fileBytes, uid);
        int order = FlashViewer.GetDocumentOrder(item.DividerID);


        if (!Directory.Exists(item.MachinePath))
        {
            Directory.CreateDirectory(item.MachinePath);
        }

        try
        {
            float size = fileBytes.Length / (1024 * 1024); // IN MB.
            string encodeName = HttpUtility.UrlPathEncode(item.FullFileName);
            encodeName = encodeName.Replace("%", "$");
            string source = string.Concat(item.MachinePath, Path.DirectorySeparatorChar, encodeName);
            var pageCount = 1;
            if (item.contentType == StreamType.JPG)
            {
                Jpeg2Engine jpeg2Engine = new Jpeg2Engine(item);
                jpeg2Engine.generatePDFfromImage(source, source.Split('.')[0] + ".pdf");
            }
            else if (item.contentType == StreamType.PDF)
            {
                PdfReader pdfReader = new PdfReader(source);
                pageCount = pdfReader.NumberOfPages;
            }

            Int32.TryParse(General_Class.DocumentsInsert(item.FileName, item.FolderID, item.UID, item.Path, item.DividerID, pageCount, size, displayFileName, (int)item.contentType, order + 1, className), out newDocumentId);
            return newDocumentId;
        }
        catch (Exception ex)
        {
            return newDocumentId;
        }


    }

    public static int AddDocumentToTab(int docId, int folderId, int dividerId, DataRow d)
    {
        try
        {
            var file = d.Field<string>("Name");
            string encodeName = HttpUtility.UrlPathEncode(file).Replace("%", "$");
            encodeName = encodeName.Replace(encodeName, encodeName.Replace("$20", " "));
            encodeName = SubStringFilename(encodeName);
            encodeName += d.Field<int>("FileExtention") == 14 ? Path.GetExtension(d.Field<string>("FullFileName")) : GetExtension(d.Field<int>("FileExtention")) ;
            encodeName = encodeName.Replace(encodeName, encodeName.Replace(" ", "$20"));
            string sourcePath = HttpContext.Current.Server.MapPath("~/" + d.Field<string>("PathName"));
            sourcePath = string.Concat(sourcePath, Path.DirectorySeparatorChar, encodeName);
            if (File.Exists(sourcePath))
            {
                string vpath = HttpContext.Current.Server.MapPath(string.Format("~/{0}", CurrentVolume));
                string pathName = CurrentVolume + Path.DirectorySeparatorChar + folderId + Path.DirectorySeparatorChar + dividerId + Path.DirectorySeparatorChar + encodeName;

                int documentOrder = FlashViewer.GetDocumentOrder(Convert.ToInt32(dividerId));
                string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, folderId, Path.DirectorySeparatorChar, dividerId);
                if (!Directory.Exists(dstpath))
                {
                    Directory.CreateDirectory(dstpath);
                }

                dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);


                if (File.Exists(dstpath))
                {
                    dstpath = GetRenameFileName(dstpath);
                }
                File.Move(sourcePath, dstpath);
                string newFilename = Path.GetFileNameWithoutExtension(dstpath);
                string displayName = HttpUtility.UrlDecode(newFilename.Replace('$', '%').ToString());

                string documentIDOrder = InsertDocument(dividerId, pathName, documentOrder, displayName, Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()), null, null, Path.GetFileName(dstpath));

                #region Elastic Update

                int newDocumentId = Convert.ToInt32(documentIDOrder.Split('#')[0]);
                if (newDocumentId > 0)
                    UpdateDocument(docId, newDocumentId, Convert.ToInt32(dividerId), displayName, pathName.Substring(0, pathName.LastIndexOf("\\")).Substring(2), Convert.ToInt32(folderId));

                #endregion

                return newDocumentId;
            }
            return 0;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    /// <summary>
    /// Get folder id form path
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static int GetFolderIdFromPath(string path)
    {
        int folderId = 0;
        if (path.Contains("\\"))
        {
            folderId = Convert.ToInt32(path.Split('\\')[1]);
        }
        else
        {
            folderId = Convert.ToInt32(path.Split('/')[1]);
        }
        //folderId = path.Split(new string[] { CurrentVolume + "/" }, StringSplitOptions.None)[1].ToString().Split('/')[0];

        return folderId;
    }

    public static string GetNewPathOnly(string oldPath, int newDividerId, string renameFile)
    {
        string newPath = string.Empty;

        if (!string.IsNullOrEmpty(renameFile) && renameFile != "")
            newPath = CurrentVolume + "/" + GetFolderIdFromPath(oldPath) + "/" + newDividerId + "/" + renameFile + Path.GetExtension(HttpContext.Current.Server.MapPath(oldPath));
        else
            newPath = CurrentVolume + "/" + GetFolderIdFromPath(oldPath) + "/" + newDividerId + "/" + Path.GetFileName(HttpContext.Current.Server.MapPath(oldPath));
        return newPath;
    }

    /// <summary>
    /// Get new path from oldpath and target id(new dividerId)
    /// </summary>
    /// <param name="oldPath"></param>
    /// <param name="newDividerId"></param>
    /// <returns></returns>
    public static string GetNewPath(string oldPath, string newDividerId, string renameFile)
    {
        string newPath = string.Empty;

        if (!string.IsNullOrEmpty(renameFile) && renameFile != "")
            newPath = CurrentVolume + "/" + GetFolderIdFromPath(oldPath) + "/" + newDividerId + "/" + renameFile + Path.GetExtension(HttpContext.Current.Server.MapPath(oldPath));
        else
            newPath = CurrentVolume + "/" + GetFolderIdFromPath(oldPath) + "/" + newDividerId + "/" + Path.GetFileName(HttpContext.Current.Server.MapPath(oldPath));

        string destinationFile = HttpContext.Current.Server.MapPath(newPath);

        if (!Directory.Exists(Path.GetDirectoryName(destinationFile)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(destinationFile));
        }
        if (File.Exists(destinationFile))
        {
            destinationFile = GetNewFileName(destinationFile);
        }

        string encodeName = HttpUtility.UrlPathEncode(Path.GetFileName(destinationFile));
        //encodeName = encodeName.Replace("%", "$");

        newPath = CurrentVolume + "/" + GetFolderIdFromPath(oldPath) + "/" + newDividerId + "/" + Path.GetFileName(destinationFile);
        return newPath;
    }

    /// <summary>
    /// Generates new file name if file already exists
    /// </summary>
    /// <param name="destinationFile">Name of file to be renamed</param>
    /// <returns>New file name</returns>
    public static string GetNewFileName(string destinationFile)
    {
        string fileName = Path.GetFileNameWithoutExtension(destinationFile);
        string fileExtension = Path.GetExtension(destinationFile);
        string fileDirectory = Path.GetDirectoryName(destinationFile);
        int i = 1;
        while (true)
        {
            string renameFileName = fileDirectory + @"\" + fileName + "_v" + i + fileExtension;
            if (File.Exists(renameFileName))
                i++;
            else
            {
                destinationFile = renameFileName;
                break;
            }
        }
        return destinationFile;
    }

    public static string ChangeExtension(string path)
    {
        string renameFileName = string.Empty;
        renameFileName = Path.GetFileNameWithoutExtension(path) + Path.GetExtension(path).ToLower().Replace(".jpeg", ".jpg");
        return renameFileName;
    }

    public static string GetRenameFileName(string path)
    {
        WriteLogs("Step 0: Entered in GetRenameFilename & Path : " + path);
        if (File.Exists(path))
        {
            WriteLogs("Step 1: Path Exists & Path : " + path);
            string fileName = Path.GetFileNameWithoutExtension(path);
            WriteLogs("Step 2: Path Exists & Filename : " + fileName);
            string fileExtension = Path.GetExtension(path);
            string fileDirectory = Path.GetDirectoryName(path);
            int i = 1;
            while (true)
            {
                string renameFileName = fileDirectory + @"\" + fileName + "_v" + i + fileExtension;
                WriteLogs("Step 3: Path Exists & Filename : " + fileName + " & RenameFileName : " + renameFileName);
                if (File.Exists(renameFileName))
                    i++;
                else
                {
                    path = renameFileName;
                    WriteLogs("Step 4: Path Exists & Filename : " + fileName + " & Final Renamed File : " + renameFileName);
                    break;
                }
            }
            WriteLogs("Step 5: Path Exists & Filename : " + fileName + " & Final Path : " + path);
        }
        return path;
    }

    public static void WriteErrorLog(string folderId, string dividerId, string fileName, Exception ex)
    {
        try
        {
            if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/eff")))
                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/eff"));

            StreamWriter swData2 = new StreamWriter(System.Web.Hosting.HostingEnvironment.MapPath("~/eff") + "\\ErrorLog.txt", true);
            swData2.WriteLine("=====Unhandled Exception occured at  : " + DateTime.Now);
            swData2.WriteLine("Folder = " + folderId);
            swData2.WriteLine("Divider = " + dividerId);
            swData2.WriteLine("fileName = " + fileName);
            swData2.WriteLine("Error = " + ex.Message);
            swData2.WriteLine(ex.StackTrace);
            swData2.WriteLine(ex.Source);
            swData2.WriteLine(ex.InnerException);
            swData2.WriteLine("====");
            swData2.Close();
            swData2.Dispose();
        }
        catch (Exception ex1)
        {
            string errorMessage = ex1.Message;
        }
    }

    public static void WriteElasticLog(string archiveId, string archiveTreeId, string fileName, Exception ex)
    {
        try
        {
            if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/eff")))
                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/eff"));

            StreamWriter swData2 = new StreamWriter(System.Web.Hosting.HostingEnvironment.MapPath("~/eff") + "\\ElasticLog.txt", true);
            swData2.WriteLine("=====Unhandled Exception occured at  : " + DateTime.Now);
            swData2.WriteLine("Folder = " + archiveTreeId);
            swData2.WriteLine("Document = " + archiveId);
            swData2.WriteLine("FileName = " + fileName);
            swData2.WriteLine("Error = " + ex.Message);
            swData2.WriteLine(ex.StackTrace);
            swData2.WriteLine(ex.Source);
            swData2.WriteLine(ex.InnerException);
            swData2.WriteLine("====");
            swData2.Close();
            swData2.Dispose();
        }
        catch (Exception ex1)
        {
            string errorMessage = ex1.Message;
        }
    }

    public static void WriteElasticResponse(string archiveId, string archiveTreeId, string fileName, string message)
    {
        try
        {
            if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/eff")))
                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/eff"));

            StreamWriter swData2 = new StreamWriter(System.Web.Hosting.HostingEnvironment.MapPath("~/eff") + "\\ElasticResponse.txt", true);
            swData2.WriteLine("=====Unhandled Exception occured at  : " + DateTime.Now);
            swData2.WriteLine("Folder = " + archiveTreeId);
            swData2.WriteLine("Document = " + archiveId);
            swData2.WriteLine("FileName = " + fileName);
            swData2.WriteLine("Mesage = " + message);
            swData2.WriteLine("====");
            swData2.Close();
            swData2.Dispose();
        }
        catch (Exception ex1)
        {
            string errorMessage = ex1.Message;
        }
    }

    //Remove this once log form works properly.
    public static void WriteLogFomLog(string logformId, string functionName, string message)
    {
        try
        {
            if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/LogFormLogs")))
                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/LogFormLogs"));

            StreamWriter swData2 = new StreamWriter(System.Web.Hosting.HostingEnvironment.MapPath("~/LogFormLogs") + "ErrorLog.txt", true);
            swData2.WriteLine("=====Unhandled Exception occured at  : " + DateTime.Now);
            swData2.WriteLine("logformId = " + logformId);
            swData2.WriteLine("functionName = " + functionName);
            swData2.WriteLine("message = " + message);
            swData2.WriteLine("====");
            swData2.Close();
            swData2.Dispose();
        }
        catch (Exception ex1)
        {
            string errorMessage = ex1.Message;
        }
    }

    public static void WriteLogs(string message)
    {
        try
        {
            if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/eff")))
                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/eff"));

            StreamWriter swData2 = new StreamWriter(System.Web.Hosting.HostingEnvironment.MapPath("~/eff") + "\\log.txt", true);
            swData2.WriteLine("=====Log : " + DateTime.Now);
            swData2.WriteLine(message);
            swData2.WriteLine("====");
            swData2.Close();
            swData2.Dispose();
        }
        catch (Exception ex1)
        {
            string errorMessage = ex1.Message;
        }
    }

    public static void WriteDocumentsAndFoldersLog(int? folderId, int? dividerId, int? docId, int? folderLogId, int? logFormId, int? Action, string message)
    {
        try
        {
            if (!Directory.Exists(System.Web.Hosting.HostingEnvironment.MapPath("~/DocumentAndFolderLogs")))
                Directory.CreateDirectory(System.Web.Hosting.HostingEnvironment.MapPath("~/DocumentAndFolderLogs"));

            StreamWriter swData2 = new StreamWriter(System.Web.Hosting.HostingEnvironment.MapPath("~/DocumentAndFolderLogs") + "ErrorLog.txt", true);
            swData2.WriteLine("=====Unhandled Exception occured at  : " + DateTime.Now);
            swData2.WriteLine("folderId = " + folderId);
            swData2.WriteLine("dividerId = " + dividerId);
            swData2.WriteLine("docId = " + docId);
            swData2.WriteLine("logformId = " + logFormId);
            swData2.WriteLine("folderlogId = " + folderLogId);
            swData2.WriteLine("action = " + Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Action));
            swData2.WriteLine("message = " + message);
            swData2.WriteLine("====");
            swData2.Close();
            swData2.Dispose();
        }
        catch (Exception ex)
        {
            string errorMessage = ex.Message;
        }
    }

    public static DataSet eFileFlowShareGetData()
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        DataSet ds = new DataSet();
        SqlDataAdapter da = new SqlDataAdapter();
        try
        {
            connection.Open();
            SqlCommand sqlComm = new SqlCommand("WorkArea_PendingSelectData", connection);
            sqlComm.CommandType = CommandType.StoredProcedure;
            da.SelectCommand = sqlComm;
            da.Fill(ds);
            return ds;
        }
        catch (Exception ex)
        {
            return new DataSet();
        }
        finally
        {
            connection.Close();
        }
    }

    public static void eFileFlowShareUpdate(int eFileFlowId, string FileName)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand sqlComm = new SqlCommand("WorkArea_PendingSelectData", connection);
            sqlComm.CommandType = CommandType.StoredProcedure;
            sqlComm.Parameters.AddWithValue("@eFileFlowId", eFileFlowId);
            sqlComm.Parameters.AddWithValue("@FileName", FileName);
            sqlComm.ExecuteNonQuery();
        }
        catch (Exception ex)
        {

        }
        finally
        {
            connection.Close();
        }
    }

    public static StreamType getFileExtention(string ext)
    {
        StreamType streamType = StreamType.PDF;

        switch (ext.ToLower())
        {
            case ".pdf":
                streamType = StreamType.PDF;
                break;
            case ".docx":
            case ".doc":
                streamType = StreamType.Word;
                break;
            case ".xls":
                streamType = StreamType.Excel;
                break;
            case ".ppt":
                streamType = StreamType.PPT;
                break;
            case ".rtf":
                streamType = StreamType.RTF;
                break;
            case ".odt":
                streamType = StreamType.ODT;
                break;
            case ".ods":
                streamType = StreamType.ODS;
                break;
            case ".odp":
                streamType = StreamType.ODP;
                break;
            case ".jpeg":
            case ".jpg":
                streamType = StreamType.JPG;
                break;
            case ".png":
                streamType = StreamType.PNG;
                break;
            default:
                streamType = StreamType.OTHERS;
                break;
        }

        return streamType;
    }
    #endregion

    #region Private Methods

    /// <summary>
    /// Prepares and returns WorkItem class as per provided inputs
    /// </summary>
    /// <param name="folderId">The folder identifier.</param>
    /// <param name="dividerId">The divider identifier.</param>
    /// <param name="fileName">Name of the file.</param>
    /// <param name="extension">The extension.</param>
    /// <param name="fileBytes">The file bytes.</param>
    /// <param name="uid">The uid.</param>
    /// <returns></returns>
    private static WorkItem GetWorkItem(int folderId, int dividerId, string fileName, string extension, byte[] fileBytes, string uid = null)
    {
        WorkItem item = new WorkItem();
        try
        {
            item.FolderID = folderId;
            item.DividerID = dividerId; ;
            item.Path = CurrentVolume + Path.DirectorySeparatorChar +
                        item.FolderID + Path.DirectorySeparatorChar + item.DividerID;

            item.FileName = Path.GetFileNameWithoutExtension(fileName);
            item.FullFileName = fileName;
            item.UID = string.IsNullOrEmpty(uid) ? Sessions.SwitchedSessionId : uid;
            item.contentType = getFileExtention(extension);
            item.Content = new MemoryStream(fileBytes);

            item.MachinePath = HttpContext.Current.Server.MapPath("~/" + item.Path);
        }
        catch (Exception e)
        { }
        return item;
    }

    public static string GetExtension(int value)
    {
        string strValue = null;
        switch (value)
        {
            case (int)StreamType.PDF:
                strValue = ".pdf";
                break;
            case (int)StreamType.Word:
                strValue = ".doc";
                break;
            case (int)StreamType.Excel:
                strValue = ".xls";
                break;
            case (int)StreamType.PPT:
                strValue = ".ppt";
                break;
            case (int)StreamType.RTF:
                strValue = ".rtf";
                break;
            case (int)StreamType.ODT:
                strValue = ".odt";
                break;
            case (int)StreamType.ODS:
                strValue = ".ods";
                break;
            case (int)StreamType.ODP:
                strValue = ".odp";
                break;
            case (int)StreamType.JPG:
                strValue = ".jpg";
                break;
            case (int)StreamType.PNG:
                strValue = ".png";
                break;
            default:
                break;
        }
        return strValue;
    }

    public static DataTable getDocumentByDeviderId(string folderId)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter();
        try
        {
            connection.Open();
            SqlCommand sqlComm = new SqlCommand("GetDocumentbyFolderId", connection);
            sqlComm.CommandType = CommandType.StoredProcedure;
            sqlComm.Parameters.AddWithValue("@FolderId", folderId);
            da.SelectCommand = sqlComm;
            da.Fill(ds);
            dt = ds.Tables[0];
            return dt;
        }
        catch (Exception ex)
        {
            return new DataTable();
        }
        finally
        {
            connection.Close();
        }
    }
    #endregion

    public static bool IsUserSwitched()
    {
        if (Sessions.UserId == Sessions.SwitchedSessionId)
            return false;
        else
            return true;
    }

    public static bool HasViewPrivilegeOnly()
    {
        Shinetech.DAL.Account user = new Shinetech.DAL.Account(Sessions.SwitchedSessionId);
        if (user.ViewPrivilege.Value)
            return true;
        else
            return false;
    }

    public static bool HasPrivilegeToDelete()
    {
        Shinetech.DAL.Account user = new Shinetech.DAL.Account(Sessions.SwitchedSessionId);
        if (user.HasPrivilegeDelete.Value)
            return true;
        else
            return false;
    }

    public static bool HasPrivilegeToDownload()
    {
        try
        {
            Shinetech.DAL.Account user = new Shinetech.DAL.Account(Sessions.SwitchedSessionId);
            if (user.HasPrivilegeDownload.IsNull || Convert.ToBoolean(user.HasPrivilegeDownload.ToString()))
                return true;
            else
                return false;
        }
        catch (Exception ex)
        {
            WriteDocumentsAndFoldersLog(0, 0, 0, 0, 0, 0, ex.Message);
            return true;
        }
    }

    #region Estimation Email

    /// <summary>
    /// Sends the estimator form email.
    /// </summary>
    /// <param name="userName">Name of the user.</param>
    /// <param name="fileName">Name of the file.</param>
    /// <param name="userEmail">The user email.</param>
    /// <param name="school">The school.</param>
    /// <param name="tel">The tel.</param>
    /// <param name="address">The address.</param>
    /// <param name="nameWithPosition">The name with position.</param>
    /// <param name="isUser">if set to <c>true</c> [is user].</param>
    public static void SendEstimatorFormEmail(string userName, string fileName, string userEmail, string school = null, string tel = null, string address = null, string nameWithPosition = null, bool isUser = true)
    {
        string strTemplate = string.Empty;
        if (isUser)
        {
            strTemplate = GetFileForUserTemplate();
            strTemplate = strTemplate.Replace("[FirstName]", userName);
        }
        else
        {
            strTemplate = GetFileForEdFilesTemplate();
            strTemplate = strTemplate.Replace("[FirstName]", EstimationUserName);
        }


        strTemplate = strTemplate.Replace("[Name]", nameWithPosition);
        strTemplate = strTemplate.Replace("[Email]", userEmail);
        strTemplate = strTemplate.Replace("[School]", school);
        strTemplate = strTemplate.Replace("[Tel]", tel);
        strTemplate = strTemplate.Replace("[Address]", address);

        string strEmailSubject = "Estimator From EdFiles";

        Email.SendEstimation(userEmail, strEmailSubject, strTemplate, fileName, isUser);
    }

    /// <summary>
    /// Gets the file for user template.
    /// </summary>
    /// <returns></returns>
    private static string GetFileForUserTemplate()
    {

        string strMailTemplet = string.Empty;
        string physicalPath = HostingEnvironment.MapPath("~/" + ProposalTemplatePath + "/");
        string mailTempletUrl = physicalPath + "EstimationForUsers.htm";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    /// <summary>
    /// Gets the file for ed files template.
    /// </summary>
    /// <returns></returns>
    private static string GetFileForEdFilesTemplate()
    {

        string strMailTemplet = string.Empty;
        string physicalPath = HostingEnvironment.MapPath("~/" + ProposalTemplatePath + "/");
        string mailTempletUrl = physicalPath + "EstimationForEdFiles.htm";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    #endregion

    #region SubStringMethod
    /// <summary>
    /// Method to remove last space element from filename
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public static string SubStringFilename(string fileName)
    {
        if (fileName.LastIndexOf(".pdf") <= 0)
            fileName = fileName.Trim();
        else
            fileName = Path.GetFileNameWithoutExtension(fileName).Trim() + Path.GetExtension(fileName);
        return fileName;
    }
    #endregion

    #region Elastic Methods

    /// <summary>
    /// Updates the document.
    /// </summary>
    /// <param name="documentId">The document identifier.</param>
    /// <param name="newDocumentId">The new document identifier.</param>
    /// <param name="dividerId">The divider identifier.</param>
    /// <param name="name">The name.</param>
    /// <param name="path">The path.</param>
    /// <param name="folderId">The folder identifier.</param>
    public static void UpdateDocument(int documentId, int newDocumentId, int dividerId, string name, string path, int folderId = 0, string fullFilename = null)
    {
        try
        {
            var document = GetOcrDocument(Convert.ToInt32(documentId));
            if (document.IsValid)
            {
                var newDocument = new DocumentIndex()
                {
                    DocumentId = newDocumentId,
                    FolderId = folderId > 0 ? folderId : document.Source.FolderId,
                    DividerId = dividerId,
                    Name = name,
                    Path = path,
                    Size = document.Source.Size,
                    JsonData = document.Source.JsonData,
                    IsDeleted = false,
                    DisplayName = name,
                    FullFileName = fullFilename
                };

                //First need to delete the previously uploaded document
                highClient.Delete<DocumentIndex>(documentId);

                //Create new entry when renaming, moving or adding to tab
                highClient.Index(newDocument, idx => idx.Index(elasticIndexName).Id(newDocumentId));
            }
        }
        catch (Exception ex) { }
    }

    /// <summary>
    /// Deletes the document.
    /// </summary>
    /// <param name="documentId">The document identifier.</param>
    public static void DeleteDocument(int documentId)
    {
        try
        {
            var document = GetOcrDocument(Convert.ToInt32(documentId));
            if (document.IsValid)
            {
                var updatedDocument = new DocumentIndex()
                {
                    IsDeleted = true
                };

                //Update the existing one when deleting
                var response = highClient.Update<DocumentIndex, DocumentIndex>(documentId, d => d
                    .Index(elasticIndexName)
                    .Doc(updatedDocument));
            }
        }
        catch (Exception ex) { }
    }

    /// <summary>
    /// Uploads the document.
    /// </summary>
    /// <param name="documentIndex">Index of the document.</param>
    /// <returns></returns>
    public static bool UploadDocument(DocumentIndex documentIndex)
    {
        var response = highClient.Index(documentIndex, idx => idx.Index(elasticIndexName).Id(documentIndex.DocumentId));
        return response.IsValid;
    }

    /// <summary>
    /// Creates the index.
    /// </summary>
    public static void CreateIndex()
    {
        var testRes = highClient
            .CreateIndex(elasticIndexName, es => es
                .Settings(s => s
                   .Analysis(an => an
                       .Analyzers(ana => ana
                           .Custom("lowercase_analyzer", c => c
                               .Tokenizer("keyword")
                               .Filters("lowercase_token_filter")
                            )
                           .Custom("edge_ngram_analyzer", c => c
                               .Tokenizer("edge_ngram_tokenizer")
                               .Filters("lowercase")
                            )
                            .Custom("ngram_analyzer", c => c
                               .Tokenizer("ngram_tokenizer")
                               .Filters("lowercase")
                            )
                       )
                       .TokenFilters(tf => tf
                           .Lowercase("lowercase_token_filter")
                           )
                       .Tokenizers(t => t
                          .EdgeNGram("edge_ngram_tokenizer", ef => ef.TokenChars(TokenChar.Letter, TokenChar.Digit).MinGram(2).MaxGram(30))
                          .NGram("ngram_tokenizer", ef => ef.TokenChars(TokenChar.Letter, TokenChar.Digit).MinGram(4).MaxGram(5))
                        )
                    )
                )
                .Mappings(m => m
                    .Map<DocumentIndex>(a => a
                        .Properties(t => t
                            .Number(n => n.Name("documentId"))
                            .Number(n => n.Name("folderId"))
                            .Number(n => n.Name("dividerId"))
                            .Text(k => k
                                .Fields(f => f
                                    .Text(w => w.Analyzer("edge_ngram_analyzer").Name("edgengram"))
                                    .Text(w => w.Analyzer("ngram_analyzer").Name("ngram"))
                                    .Text(w => w.Analyzer("lowercase_analyzer").Name("lowercase").Index(false))
                                    .Text(w => w.Index(false).Name("raw"))
                                    )
                                .Name("jsonData")
                                )
                            .Text(d => d.Name("path"))
                            .Number(n => n.Name("size"))
                            .Text(k => k
                                .Fields(f => f
                                    .Text(w => w.Analyzer("edge_ngram_analyzer").Name("edgengram"))
                                    .Text(w => w.Analyzer("ngram_analyzer").Name("ngram"))
                                    .Text(w => w.Analyzer("lowercase_analyzer").Name("lowercase"))
                                    .Text(w => w.Index().Name("raw"))
                                    )
                                .Name("name")
                                )
                            .Boolean(n => n.Name("isDeleted"))
                        )
                    )
                )
            );
    }

    /// <summary>
    /// Gets the document.
    /// </summary>
    /// <param name="documentId">The document identifier.</param>
    /// <returns></returns>
    public static IGetResponse<DocumentIndex> GetOcrDocument(int documentId)
    {
        return highClient.Get<DocumentIndex>(documentId);
    }

    public static void PutMapping()
    {
        var result = highClient
                        .Map<DocumentIndex>(a => a
                            .Properties(p => p
                                .Text(t => t
                                    .Fields(f => f
                                        .Text(w => w
                                            .Analyzer("lowercase_analyzer")
                                            .Name("lowercase")
                                            .Index(false)
                                        )
                                    )
                                    .Name("jsonData")
                                )
                            )
                        );
    }

    public static void DeleteIndex()
    {
        var result = highClient.DeleteIndex(elasticIndexName);
    }

    #endregion
}