﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///DividerData 的摘要说明
/// </summary>
public class DividerData
{
    public DividerData()
    {
        FileItemList = new List<FileItem>();
        QuiteNoteList = new List<QuitNoteItem>();
        NewNoteList = new List<NewNoteItem>();
    }

    public int Id { get; set; }

    public int FolderId { get; set; }

    public string Name { get; set; }

    public string MetaXML { get; set; }

    public List<FileItem> FileItemList { get; set; }

    public List<QuitNoteItem> QuiteNoteList { get; set; }

    public List<NewNoteItem> NewNoteList { get; set; }

    public int ErrorCode { get; set; }

    public int TotalPages { get; set; }


}