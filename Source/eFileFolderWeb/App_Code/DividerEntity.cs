using System;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for DividerEntity
/// </summary>
[Serializable]
public class DividerEntity
{
    public DividerEntity(string name)
    {
        _DividerID = -1;
        _Name = name;
        _DOB = DateTime.Now;
        _Color = "#e78302";

    }

    public DividerEntity(string name,string color, DateTime date)
    {
        _DividerID = -1;
        _Name = name;
        _DOB = date;
        _Color = color;

    }

    private int _DividerID;
    [Bindable(true)]
    public int DividerID
    {
        get { return _DividerID; }

        set { _DividerID= value; }
    }

    private string _Name;
    [Bindable(true)]
    public string Name
    {
        get { return _Name; }

        set { _Name=value; }
    }

    private DateTime _DOB;
    [Bindable(true)]
    public DateTime DOB
    {
        get { return _DOB; }

        set { _DOB=value; }
    }

    private string _Color;
    [Bindable(true)]
    public string Color
    {
        get { return _Color; }

        set { _Color = value; }
    }

    private string _OfficeID;
    [Bindable(true)]
    public string OfficeID
    {
        get { return _OfficeID; }

        set { _OfficeID=value; }
    }
}
