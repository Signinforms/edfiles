using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Services.Protocols;
using System.Data;
using System.Configuration;
using System.Web.Security;
using System.Xml;
using DataQuicker2.Framework;
using Shinetech.DAL;
using Shinetech.Engines;
using System.IO;
using Account = Shinetech.DAL.Account;
using DocPage = Shinetech.DAL.Page;
using Shinetech.Engines.Adapters;
using Newtonsoft.Json;
using Elasticsearch.Net;
using Nest;
using System.Linq;
using System.IO.Compression;
using Ionic.Zip;

/// <summary>
/// Summary description for EFileFolderWebService for viewer2 
/// </summary>
[WebService(Namespace = "http://www.edfiles.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.Web.Script.Services.ScriptService]
public class EFileFolderJSONWS : System.Web.Services.WebService
{
    public AuthenticationInfo authenticationInfo;
    protected static string hostString = ConfigurationManager.AppSettings.Get("ElasticHostString");
    protected static string elasticUserName = ConfigurationManager.AppSettings.Get("ElasticUserName");
    protected static string elasticUserSecret = ConfigurationManager.AppSettings.Get("ElasticUserSecret");
    protected static string elasticIndexName = ConfigurationManager.AppSettings.Get("ElasticIndices");
    protected static Uri[] hosts = hostString.Split(',').Select(x => new Uri(x)).ToArray();
    protected static StaticConnectionPool connectionPool = new StaticConnectionPool(hosts);
    protected static ConnectionSettings settings = new ConnectionSettings(connectionPool)
                                                                         .DefaultMappingFor<Archive>(i => i
                                                                         .IndexName(elasticIndexName)
                                                                         .TypeName("pdf"))
                                                                         .BasicAuthentication(elasticUserName, elasticUserSecret).DisableDirectStreaming();
    protected static ElasticClient highClient = new ElasticClient(settings);
    //卢远宗 2015-10-29
    public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    public static string UserDataFolder = System.Configuration.ConfigurationManager.AppSettings["UserDataFolder"];
    public static string WEBSITE = System.Configuration.ConfigurationManager.AppSettings["WEBSITE"];

    public EFileFolderJSONWS()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
        var auth = this.Context.Request.Headers["AuthenticationInfo"];
        if (auth != null)
        {
            string[] items = auth.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);

            authenticationInfo = new AuthenticationInfo(items[0], items[1]);
        }

    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public string CallMethod()
    {
        if (User.Identity.IsAuthenticated)
        {
            return ("Valid User");
        }
        else
        {
            return ("INVALID USER");
        }

    }

    /// <summary>
    /// Check the validate code
    /// </summary>
    /// <returns></returns>
    private bool CheckValidateCode(string code)
    {
        return FlashViewer.FlashViewer_CheckValidateCode(code);
    }

    /// <summary>
    /// Check the validate code
    /// </summary>
    /// <returns></returns>
    private bool CheckValidateCode_FileFolderShare(string code)
    {
        return FlashViewer.FlashViewer_CheckValidateCode_FileFolderShare(code);
    }

    private bool isMySelf(FileFolder folder)
    {
        var uid = Sessions.SwitchedSessionId;
        //var uid = Membership.GetUser().ProviderUserKey.ToString();
        return folder.OfficeID.Value.ToLower().Equals(uid.ToLower());
    }

    private bool isMySubUser(FileFolder folder)
    {
        var uid = Membership.GetUser();

        var officeid = UserManagement.GetOfficeUserIDBySubUserID(uid.ProviderUserKey.ToString());
        if (officeid.Rows.Count <= 0) return false;

        return folder.OfficeID1.Value == (officeid.Rows[0][0].ToString() == "" ? uid.ProviderUserKey.ToString() : officeid.Rows[0][0].ToString());
    }

    private bool BelongToGroup(string p, string[] groups)
    {
        foreach (string group in groups)
        {
            if (p.Contains(group))
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Get xml data for one Folder
    /// </summary>
    /// <param name="folderId"></param>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public FolderData GetFolderXML(int folderId, string validateCode, bool isEdit, bool isAccessDenied)
    {
        //如果登录的是本人，文档拥有者，则直接可以
        //如果不是本人，说明是本站登录者，则只有public和protected可以访问，而如果有securitycode需要验证
        //如果为非登录者，则只有public下可以访问，而如果有securitycode需要验证

        //XmlDataDocument errorXml = new XmlDataDocument();
        //FolderData ds = new FolderData();

        //ds = FlashViewer.FlashViewer_GetFolderJsonData(folderId);
        //if (ds != null)
        //{
        //    ds.ErrorCode = 0;
        //    return ds;
        //}


        XmlDataDocument errorXml = new XmlDataDocument();
        FolderData ds = new FolderData();

        if (CheckValidateCode_FileFolderShare(validateCode)) //|| trues
        {
            FileFolder folder = new FileFolder(folderId);

            if (folder.IsExist)
            {
                ds.AllowEdit = false;
                //if (this.IsAuthorized())
                //{
                ds.AllowEdit = true;
                // if (folder.SecurityLevel.Value == 0 && !isMySelf(folder))
                if (folder.SecurityLevel.Value == 0 && isEdit == false)
                {
                    //如果不是本人则，验证是否该用户和Folder是同一Group
                    //var uid = Membership.GetUser().ProviderUserKey.ToString();
                    //var ac = new Shinetech.DAL.Account(uid);
                    //string[] groups = ac.Groups.Value != null ? ac.Groups.Value.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries) : new string[] { };

                    //if (uid.Equals(folder.OfficeID1.Value, StringComparison.CurrentCultureIgnoreCase))
                    //{
                    //    groups = "1|2|3|4|5".Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                    //}

                    //if (BelongToGroup(folder.Groups.Value, groups))
                    //{
                    //ds = FlashViewer.FlashViewer_GetFolderJsonData(folderId, isEdit);

                    //if (ds != null)
                    //{
                    //    ds.AllowEdit = true;
                    //    ds.ErrorCode = 0;
                    //    return ds;
                    //}
                    ////}
                    //else
                    //{
                    ds.AllowEdit = true;
                    ds.ErrorCode = -2;
                    return ds;
                    //}

                }
                else if (folder.SecurityLevel.Value == 0 && isEdit)
                {
                    ds = FlashViewer.FlashViewer_GetFolderJsonData(folderId, isEdit, isAccessDenied);

                    if (ds != null)
                    {
                        ds.AllowEdit = true;
                        ds.ErrorCode = 0;
                        return ds;
                    }
                }

                //if (folder.SecurityLevel.Value == 1 && (!isMySubUser(folder) && !isMySelf(folder)))
                //{
                //    ds.AllowEdit = true;
                //    ds.ErrorCode = -2;
                //    return ds;
                //}

                #region IsAuthorized

                //if (folder.SecurityLevel.Value == 0 && isMySelf(folder))
                //{
                //    ds = FlashViewer.FlashViewer_GetFolderJsonData(folderId, isEdit);

                //    if (ds != null)
                //    {

                //        ds.AllowEdit = true;
                //        ds.ErrorCode = 0;
                //        return ds;
                //    }
                //}

                //Account act = new Account(folder.OfficeID1.Value);

                //if (act.Name.Value.Equals(this.User.Identity.Name, StringComparison.OrdinalIgnoreCase))
                //{
                //    ds = FlashViewer.FlashViewer_GetFolderJsonData(folderId, isEdit);
                //    ds.AllowEdit = true;
                //    if (ds != null)
                //    {
                //        ds.AllowEdit = true;
                //        ds.ErrorCode = 0;
                //        return ds;
                //    }
                //}

                if (folder.SecurityLevel.Value == 1 || folder.SecurityLevel.Value == 2)
                {
                    #region be able to read
                    if (!string.IsNullOrEmpty(folder.FolderCode.Value))
                    {
                        ds.AllowEdit = true;
                        //errorXml.InnerXml = "<ErrorCode>-9</ErrorCode>";//提示输入验证码
                        ds.ErrorCode = -9;
                        return ds;
                    }
                    else
                    {
                        ds = FlashViewer.FlashViewer_GetFolderJsonData(folderId, isEdit, isAccessDenied);

                        if (ds != null)
                        {
                            ds.AllowEdit = true;
                            //XmlDataDocument xmlDoc = new XmlDataDocument(ds);
                            ds.ErrorCode = 0;
                            return ds;
                        }
                    }
                    #endregion
                }

                //其它情况则不允许读取
                // errorXml.InnerXml = "<ErrorCode>-3</ErrorCode>";
                ds.ErrorCode = -3;
                return ds;

                ds = FlashViewer.FlashViewer_GetFolderJsonData(folderId, isEdit, isAccessDenied);
                if (ds != null)
                {
                    return ds;
                }
                #endregion
                //}
                //else
                //{
                //    if (folder.SecurityLevel.Value != 2)
                //    {

                //        ds.ErrorCode = -2;
                //        return ds;
                //    }

                //    #region unAuthorized
                //    if (folder.SecurityLevel.Value == 2)
                //    {
                //        if (!string.IsNullOrEmpty(folder.FolderCode.Value))
                //        {
                //            // errorXml.InnerXml = "<ErrorCode>-9</ErrorCode>";//提示输入验证码
                //            ds.ErrorCode = -9;
                //            return ds;
                //        }
                //        else
                //        {


                //            ds = FlashViewer.FlashViewer_GetFolderJsonData(folderId, isEdit);
                //            ds.AllowEdit = false;
                //            if (ds != null)
                //            {
                //                return ds;
                //            }
                //        }
                //    }
                //    else
                //    {
                //        //errorXml.InnerXml = "<ErrorCode>-3</ErrorCode>";
                //        ds.ErrorCode = -3;
                //        return ds;
                //    }

                //    #endregion
                //}
            }

            //errorXml.InnerXml = "<ErrorCode>-2</ErrorCode>";
            ds.ErrorCode = -2;
            return ds;
        }
        else
        {
            if (validateCode == folderId + "_AAAAAAAAAAAAAAA")
            {
                //errorXml.InnerXml = "<ErrorCode>-3</ErrorCode>";
                ds.ErrorCode = -3;
                return ds;
            }
            else
            {
                //errorXml.InnerXml = "<ErrorCode>-2</ErrorCode>";
                ds.ErrorCode = -2;
                return ds;
            }

        }
    }

    /// <summary>
    /// Get xml data for one Folder
    /// </summary>
    /// <param name="folderId"></param>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetFolderXMLWithCode(int folderId, string validateCode)
    {
        XmlDataDocument errorXml = new XmlDataDocument();
        FileFolder folder = new FileFolder(folderId);
        if (folder.IsExist)
        {
            if (folder.FolderCode.Value.Equals(validateCode, StringComparison.OrdinalIgnoreCase))
            {
                DataSet ds = FlashViewer.FlashViewer_GetFolderData(folderId);
                if (ds != null && ds.Tables.Count == 2 && ds.Tables["Folder"].Rows.Count >= 1)
                {
                    XmlDataDocument xmlDoc = new XmlDataDocument(ds);
                    return XmlToJson.XmlToJSON(xmlDoc);
                }
                else
                {
                    errorXml.InnerXml = "<ErrorCode>-1</ErrorCode>";
                    return XmlToJson.XmlToJSON(errorXml);
                }
            }
            else
            {
                errorXml.InnerXml = "<ErrorCode>-9</ErrorCode>";
                return XmlToJson.XmlToJSON(errorXml);
            }
        }
        else
        {
            errorXml.InnerXml = "<ErrorCode>-1</ErrorCode>";
            return XmlToJson.XmlToJSON(errorXml);
        }
    }

    /// <summary>
    /// Get xml data for one Folder
    /// </summary>
    /// <param name="folderId"></param>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public FolderData GetFolderXMLSecurity(int folderId, string validateCode)
    {
        FolderData ds = new FolderData();
        FileFolder folder = new FileFolder(folderId);
        if (!string.IsNullOrEmpty(validateCode.Trim()) && folder.IsExist)
        {
            if (folder.FolderCode.Value.Equals(validateCode, StringComparison.OrdinalIgnoreCase))
            {
                FolderData result = null;
                //if (folder.SecurityLevel.Value == 0 || folder.SecurityLevel.Value == 1)
                //{
                result = FlashViewer.FlashViewer_GetFolderJsonData(folderId);
                //}
                //else
                //{
                //    result = FlashViewer.FlashViewer_GetFolderJsonData(folderId, true);
                //}

                if (result != null)
                {
                    ds = result;
                    ds.ErrorCode = 0;
                    return ds;
                }
            }
        }

        ds.ErrorCode = -1;
        return ds;
    }



    /// <summary>
    /// Get xml data for one Tab
    /// </summary>
    /// <param name="tabId"></param>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DividerData GetTabXML(int tabId, string validateCode)
    {
        DividerData ds = new DividerData();

        XmlDataDocument errorXml = new XmlDataDocument();
        if (CheckValidateCode(validateCode)) //|| true
        {
            ds = FlashViewer.FlashViewer_GetTabPDFJson(tabId);
            ds.ErrorCode = 0;
            return ds;

        }
        else
        {
            ds.ErrorCode = -2;
            return ds;
        }
    }

    /// <summary>
    /// Move file from one tab to another tab.
    /// </summary>
    /// <param name="newDividerId"></param>
    /// <param name="oldPath"></param>
    /// <param name="oldDividerId"></param>
    /// <param name="documentId"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<KeyValuePair<string, string>> MoveRenameFile(int newDividerId, string oldPath, int oldDividerId, string documentId, int documentOrder, int isMove, string renameFile, string className)
    {
        List<KeyValuePair<string, string>> documentData = new List<KeyValuePair<string, string>>();
        int moveRename = 0;
        bool isDelete = false;
        int newDocumentId = 0;
        int documentOrderId = Convert.ToInt32(documentOrder);
        string newPath = string.Empty;
        string displayFileName = string.Empty;
        int folderId = 0;
        DataSet page = new DataSet();
        try
        {
            //string renameFilename =renameFile.Substring(renameFile.Length - 1);
            //if (renameFilename == " ")
            renameFile = Common_Tatva.SubStringFilename(renameFile);
            renameFile = HttpUtility.UrlPathEncode(renameFile).Replace("%", "$");
            var checkNewPath = Common_Tatva.GetNewPathOnly(oldPath, newDividerId, renameFile);
            if (checkNewPath == oldPath)
            {
                Common_Tatva.WriteLogs("Step 1: " + checkNewPath + "==" + oldPath);
                //DocumentClassUpdate
                //Common_Tatva.DocumentClassUpdate(documentId, className);
                General_Class.DocumentClassUpdate(documentId, className);
                Common_Tatva.WriteLogs("Step 2: " + checkNewPath + "==" + oldPath + "------" + "class updated");
                return documentData;
            }
            //oldPath = HttpUtility.UrlDecode(oldPath);
            newPath = Common_Tatva.GetNewPath(oldPath, Convert.ToString(newDividerId), renameFile);
            DataTable dtDoc = General_Class.GetDocumentFromDocumentID(Convert.ToInt32(documentId));
            string uid = dtDoc.Rows[0]["UID"].ToString();
            if (!string.IsNullOrEmpty(renameFile) && renameFile != "")
                displayFileName = HttpUtility.UrlDecode(Path.GetFileNameWithoutExtension(HttpContext.Current.Server.MapPath(newPath)).Replace("$", "%"));
            else
            {
                displayFileName = HttpUtility.UrlDecode(Path.GetFileNameWithoutExtension(HttpContext.Current.Server.MapPath(newPath)).Replace("$", "%"));
                displayFileName = Common_Tatva.SubStringFilename(displayFileName);
            }
            string oldFileName = Path.GetFileName(HttpContext.Current.Server.MapPath(oldPath));

            string newFileName = Path.GetFileName(HttpContext.Current.Server.MapPath(newPath));
            oldPath = oldPath.Replace(oldFileName, oldFileName.Replace("%", "$"));
            if (isMove == 1)
            {
                newFileName = newFileName.Replace("$20", " ");
                newFileName = Common_Tatva.SubStringFilename(newFileName);
                newFileName = HttpUtility.UrlPathEncode(newFileName);
            }

            newPath = newPath.Replace(newFileName, HttpUtility.UrlPathEncode(newFileName));
            newPath = newPath.Replace(HttpUtility.UrlPathEncode(newFileName), HttpUtility.UrlPathEncode(newFileName).Replace("%", "$"));

            try
            {
                using (FileStream stream = File.Open(HttpContext.Current.Server.MapPath("~/" + oldPath), FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteErrorLog(string.Empty, " check file is open or not = " + oldPath, " oldPath = " + oldPath, ex);
            }
            // move document to new folder
            moveRename = Common_Tatva.MoveFilesToAnotherFolder(newPath, oldPath);

            //insert file in database and folder
            if (moveRename == Convert.ToInt32(Enum_Tatva.FileMove.FileMoveSuccess))
            {
                string documentIDOrder = Common_Tatva.InsertDocument(newDividerId, newPath, Convert.ToInt32(documentOrder), displayFileName, className, uid, null, Path.GetFileName(newPath));
                if (documentIDOrder.Contains("#"))
                {
                    newDocumentId = Convert.ToInt32(documentIDOrder.Split('#')[0]);
                    documentOrderId = Convert.ToInt32(documentIDOrder.Split('#')[1]);
                }
                else
                    newDocumentId = Convert.ToInt32(documentIDOrder);
            }

            #region Elastic Update

            if (newDocumentId > 0)
                Common_Tatva.UpdateDocument(Convert.ToInt32(documentId), newDocumentId, newDividerId, displayFileName, newPath.Substring(0, newPath.LastIndexOf("/")), 0, Convert.ToInt32(dtDoc.Rows[0]["FileExtention"]) == 14 ? Path.GetFileName(checkNewPath) : null);

            #endregion


            // delete document and pages from database // No need to pass switch user
            if (newDocumentId > 0)
                page = FlashViewer.DeleteDocumentAndPages(documentId, documentOrder, Membership.GetUser().ProviderUserKey.ToString());

            // delete document and pages from folder
            if (page != null && page.Tables[0].Rows.Count > 0)
                isDelete = Common_Tatva.DeletePagesFromFolder(page);

            //update quick note divider id and pagekey value
            if (isDelete)
                FlashViewer.UpdateQuickNoteDividerId(newDividerId, newDocumentId, Convert.ToInt32(documentId));
            int logAction = 0;
            //DataTable dtDoc = General_Class.GetDocumentFromDocumentID(Convert.ToInt32(documentId));
            if (Convert.ToBoolean(isMove))
                logAction = Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode();
            else
                logAction = Enum_Tatva.DocumentsAndFoldersLogs.Rename.GetHashCode();
            folderId = Convert.ToInt32(oldPath.Split('/')[1]);
            InsertDocumentsAndFolderLogs(folderId, oldDividerId, Convert.ToInt32(documentId), 0, 0, Convert.ToBoolean(isMove) ? Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode() : Enum_Tatva.DocumentsAndFoldersLogs.Rename.GetHashCode(), 0, 0);
            InsertDocumentsAndFolderLogs(folderId, oldDividerId, Convert.ToInt32(newDocumentId), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Create.GetHashCode(), 0, 0);
            string tempPath = newPath.Split(new[] { "/" + Path.GetFileName(newPath) }, StringSplitOptions.RemoveEmptyEntries)[0];
            tempPath = tempPath.Replace("/", @"\");

            documentData.Add(new KeyValuePair<string, string>("Status", "true"));
            documentData.Add(new KeyValuePair<string, string>("FullName", newPath));
            documentData.Add(new KeyValuePair<string, string>("DocumentOrder", Convert.ToString(documentOrderId)));
            documentData.Add(new KeyValuePair<string, string>("DisplayName", displayFileName));
            documentData.Add(new KeyValuePair<string, string>("Name", Path.GetFileNameWithoutExtension(HttpContext.Current.Server.MapPath(newPath))));
            documentData.Add(new KeyValuePair<string, string>("Id", Convert.ToString(newDocumentId)));
            documentData.Add(new KeyValuePair<string, string>("Idd", Convert.ToString(newDividerId)));
            documentData.Add(new KeyValuePair<string, string>("Path", tempPath));
        }
        catch (Exception ex)
        {
            // if not move then ?
            //if (moveRename == Convert.ToInt32(Enum_Tatva.FileMove.FileExist))
            //    documentData.Add(new KeyValuePair<string, string>("Status", "This file is already exist in target tab, so you can not move it."));
            //else
            //{
            if (moveRename == Convert.ToInt32(Enum_Tatva.FileMove.FileMoveSuccess))
                Common_Tatva.MoveFilesToAnotherFolder(oldPath, newPath);

            if (Convert.ToBoolean(isMove))
                documentData.Add(new KeyValuePair<string, string>("Status", "File is not moved."));
            else
                documentData.Add(new KeyValuePair<string, string>("Status", "File is not renamed."));
            //}
        }
        return documentData;
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string UpdateFileName(string path, string newName, string docId)
    {
        try
        {
            Document doc = new Document(int.Parse(docId));
            if (doc.IsExist && newName != "")
            {
                //doc.Name.Value = newName;
                doc.DisplayName.Value = newName;
                doc.Update();
                return "1";
            }
            else
            {
                return "0";
            }

        }
        catch (Exception exp)
        {
            //WriteErrorMessage(exp.Message);
            return "0";
        }

    }



    /// <summary>
    /// Insert or update Quick Note
    /// </summary>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public long UpdateQuickNote(string uid, long id, int pageId, string content, int x, int y, bool bFixed, bool bOpened, string validateCode)
    {
        if (CheckValidateCode(validateCode))
        {
            long result = FlashViewer.FlashViewer_UpdateQuickNote(uid, id, pageId, content, x, y, bFixed, bOpened);
            return result;
        }
        else
        {
            //todo:binguo
            return -2;
        }
    }

    /// <summary>
    /// Insert or update Quick Note
    /// </summary>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public DividerData UpdateQuickNoteWithKey(string uid, long id, string pagekey, string content, int dividerid, int x, int y, bool bFixed, bool bOpened, string validateCode)
    {
        DividerData ds = new DividerData();

        if (CheckValidateCode(validateCode)) // || true
        {
            //long result = FlashViewer.FlashViewer_UpdateQuickNote(uid, id, pagekey, content, dividerid, x, y, bFixed, bOpened);
            //return result;
            ds = FlashViewer.FlashViewer_UpdateQuickNote(uid, id, pagekey, content, dividerid, x, y, bFixed, bOpened);
            return ds;
        }
        else
        {
            return null;
            ////todo:binguo
            //return -2;
        }
    }

    /// <summary>
    ///  Delete one quick note
    /// </summary>
    /// <param name="quickNoteId"></param>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public int DeleteQuickNote(int quickNoteId, string validateCode)
    {
        if (CheckValidateCode(validateCode)) // || true
        {
            bool result = FlashViewer.FlashViewer_DeleteQuickNote(quickNoteId);
            return result ? 1 : 0;
        }
        else
        {
            //todo:binguo
            return -2;
        }
    }


    /// <summary>
    /// Insert or update Quick Note
    /// </summary>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public long UpdateURLNote(string uid, long id, int pageId, string title, string content, int x, int y, string validateCode)
    {
        if (CheckValidateCode(validateCode))
        {
            long result = FlashViewer.FlashViewer_UpdateURLNote(uid, id, pageId, title, content, x, y);
            return result;
        }
        else
        {
            //todo:binguo
            return -2;
        }
    }

    /// <summary>
    ///  Delete one quick note
    /// </summary>
    /// <param name="urlId"></param>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public int DeleteURLNote(int urlId, string validateCode)
    {
        if (CheckValidateCode(validateCode))
        {
            bool result = FlashViewer.FlashViewer_DeleteURLNote(urlId);
            return result ? 1 : 0;
        }
        else
        {
            //todo:binguo
            return -2;
        }
    }


    /// <summary>
    /// Insert or update new note
    /// </summary>
    /// <param name="uid"></param>
    /// <param name="id"></param>
    /// <param name="tabId"></param>
    /// <param name="content"></param>
    /// <param name="validateCode"></param>
    /// <param name="strTitle"></param>
    /// <param name="strAuthor"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public long UpdateNewNote(string uid, long id, int tabId, string content, string validateCode, string strTitle, string strAuthor)
    {
        if (CheckValidateCode(validateCode)) //|| true
        {
            //content = content.Replace("'", "''");
            long result = FlashViewer.FlashViewer_UpdateNewNote(uid, id, tabId, content, strTitle, strAuthor);
            return result;
        }
        else
        {
            //todo:binguo
            return -2;
        }
    }

    /// <summary>
    /// Delete one new note
    /// </summary>
    /// <param name="newNoteId"></param>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public int DeleteNewNote(int newNoteId, string validateCode)
    {
        if (CheckValidateCode(validateCode)) //|| true
        {
            bool result = FlashViewer.FlashViewer_DeleteNewNote(newNoteId);
            return result ? 1 : 0;
        }
        else
        {
            //todo:binguo
            return -2;
        }
    }

    /// <summary>
    /// Update expiration time for Office User
    /// </summary>
    /// <param name="expirationTime"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public bool UpdateExpirationTime(int expirationTime)
    {
        bool isUpdated = Convert.ToBoolean(General_Class.UpdateExpirationTime(new Guid(Sessions.SwitchedRackspaceId), expirationTime));
        if (isUpdated)
            Sessions.ExpirationTime = expirationTime;
        return isUpdated;
    }

    //[WebMethod(EnableSession = true)]
    //public void SetSessionValueForExpirationTime(int expirationTime)
    //{
    //    Sessions.ExpirationTime = expirationTime;
    //}

    /// <summary>
    /// Get Expiration Time & User Identity for Folder Viewer
    /// </summary>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public object GetExpirationTime()
    {
        int expirationTime = General_Class.GetExpirationTime(new Guid(Sessions.SwitchedRackspaceId));
        if (expirationTime < 1)
            expirationTime = Sessions.ExpirationTime;
        bool isOffice;
        if (User.IsInRole("Offices"))
            isOffice = true;
        else
            isOffice = false;
        var data = new { expirationTime = expirationTime, isOffice = isOffice, email = Sessions.SwitchedEmailId };
        return data;
    }


    /// <summary>
    /// Get Expiration Time Validation for Shared Folder Viewer
    /// </summary>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public object GetExpirationTimeForValidation(string uid)
    {
        int expirationTime = General_Class.GetExpirationTime(new Guid(uid));
        if (expirationTime < 1)
            expirationTime = Convert.ToInt32(Common_Tatva.ExpirationTime);
        var data = new { expirationTime = expirationTime };
        return data;
    }

    /// <summary>
    /// for  sharing Document.
    /// </summary>
    /// <param name="strSendMail"></param>
    /// <param name="strToMail"></param>
    /// <param name="documentId"></param>
    /// <param name="strMailInfor"></param>
    /// <param name="SendName"></param>
    /// <param name="ToName"></param>
    /// <param name="MailTitle"></param>
    /// <param name="Path"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public bool ShareMail(string strSendMail, string strToMail, string documentIds, string strMailInfor, string SendName, string ToName, string MailTitle, string Path)
    {
        Guid uid = new Guid(Sessions.SwitchedSessionId);
        //Guid uid = new Guid(Membership.GetUser().ProviderUserKey.ToString());
        bool success = true;
        var arrEmailName = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(strToMail).ToArray();
        string[] docIds = documentIds.Split(',');
        int[] mailIds = new int[docIds.Length];
        int[] shareIds = new int[docIds.Length];
        for (int i = 0; i < arrEmailName.Length; i++)
        {
            for (int j = 0; j < docIds.Length; j++)
            {
                int mailId = FlashViewer.FlashViewer_ShareMail(strSendMail, arrEmailName[i]["Email"], Convert.ToInt32(docIds[j]), MailTitle, strMailInfor);
                mailIds[j] = mailId;
                int shareId = General_Class.SaveShareLink(uid, Enum_Tatva.Folders.inbox.GetHashCode(), Convert.ToInt32(docIds[j]), null, null, null, null);
                shareIds[j] = shareId;
            }
            FlashViewer.HtmlViewer_SendShareMail(arrEmailName[i]["ToName"], SendName, docIds, strSendMail, MailTitle, strMailInfor, arrEmailName[i]["Email"], Path, mailIds, shareIds);
            if (mailIds.Any(a => a.Equals(0)))
                success = false;
        }
        return success;
    }

    /// <summary>
    /// Share folder and divider
    /// </summary>
    /// <param name="strSendMail"></param>
    /// <param name="strToMail"></param>
    /// <param name="folderId"></param>
    /// <param name="strMailInfor"></param>
    /// <param name="SendName"></param>
    /// <param name="ToName"></param>
    /// <param name="MailTitle"></param>
    /// <param name="dividerId"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    [SoapHeader("authenticationInfo")]
    public bool ShareMailForFileFolderBox(string strSendMail, string strToMail, string folderId, string strMailInfor, string SendName, string ToName, string MailTitle, string dividerId, bool isDivider)
    {
        Guid uid = new Guid(Sessions.SwitchedSessionId);
        NLogLogger _logger = new NLogLogger();
        bool success = true;
        var arrEmailName = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(strToMail).ToArray();
        for (int i = 0; i < arrEmailName.Length; i++)
        {
            List<Tuple<string>> ELink = new List<Tuple<string>>();

            int mailId = FlashViewer.FlashViewer_MailFriend(strSendMail, arrEmailName[i]["Email"], Convert.ToInt32(folderId), MailTitle, strMailInfor);

            int shareId = General_Class.SaveShareLink(uid, Enum_Tatva.Folders.FileFolder.GetHashCode(), Convert.ToInt32(folderId), null, null, null, null);
            if (!string.IsNullOrEmpty(dividerId) && isDivider)
                ELink.Add(Tuple.Create(HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("EFileFolderJSONService.asmx")) + "Office/ShareFileFolder.aspx?data=" + QueryString.QueryStringEncode("id=" + folderId + "&dividerId=" + dividerId)));
            else
                ELink.Add(Tuple.Create(HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("EFileFolderJSONService.asmx")) + "Office/ShareFileFolder.aspx?data=" + QueryString.QueryStringEncode("id=" + folderId)));

            try
            {
                FlashViewer.HtmlViewer_SendShareMailForFileFolderBox(ToName, SendName, strSendMail, MailTitle, strMailInfor, arrEmailName[i]["Email"], ELink);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            if (mailId == 0)
                success = false;
        }
        return success;
    }

    /// <summary>
    ///  mail firend
    /// </summary>
    /// <param name="xml"></param>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string MailFirend(string strSendMail, string strToMail, string folderID, string strMailInfor, string validateCode, string SendName, string ToName, string MailTitle)
    {
        string errormsg = string.Empty;
        string[] folderIDs = folderID.Split(',');
        if (folderIDs.Length > 0 && validateCode == "undefined")
        {
            int MailID = 0;
            string retval = string.Empty;
            var arrEmailName = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(strToMail).ToArray();
            for (int i = 0; i < arrEmailName.Length; i++)
            {
                List<Tuple<string, string>> ELink = new List<Tuple<string, string>>();

                foreach (string efolderId in folderIDs)
                {

                    Guid guid = Guid.NewGuid();
                    int folderId = Convert.ToInt32(efolderId);
                    FileFolder folder = new FileFolder(folderId);
                    string FileName = (folder.FolderName).ToString();
                    if (!folder.IsExist) continue;

                    bool success = true;
                    if (folder.SecurityLevel.Value == 0) continue;
                    Guid uid = new Guid(Sessions.SwitchedSessionId);
                    //Guid uid = new Guid(Membership.GetUser().ProviderUserKey.ToString());
                    string strvalidateCode = FlashViewer.FlashViewer_GetValidateCode_FileFolderShare(folderId);

                    int shareID = General_Class.SaveShareLink(uid, Enum_Tatva.Folders.FileFolder.GetHashCode(), null, folderId, null, null, null);
                    MailID = FlashViewer.FlashViewer_MailFriend(strSendMail, arrEmailName[i]["Email"], folderId, MailTitle, strMailInfor);
                    if (MailID < 0 || MailID == 0)
                        success = false;
                    if (success)
                    {
                        ELink.Add(Tuple.Create(HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("EFileFolderJSONService.asmx")) + "EFileFolderV2.html?MailId=" + MailID + "&ID=" + folderId + "&DATA=" + strvalidateCode + "&ISEDIT=" + false + "&IsDenied=false&t=" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff") + "&l=" + shareID + "&U=" + Sessions.SwitchedRackspaceId, FileName));
                        retval += folderId.ToString() + ",";
                    }
                }
                FlashViewer.HtmlViewer_SendMail(arrEmailName[i]["ToName"], SendName, strSendMail, MailTitle, strMailInfor, arrEmailName[i]["Email"], ELink);

            }
            return retval;
        }
        else if (CheckValidateCode_FileFolderShare(validateCode)) // || true
        {
            List<Tuple<string, string>> ELink = new List<Tuple<string, string>>();

            int folderId = Convert.ToInt32(folderID);
            FileFolder folder = new FileFolder(folderId);
            string FileName = (folder.FolderName).ToString();
            if (!folder.IsExist) return "Failed to share document";
            int MailID = 0;
            if (folder.SecurityLevel.Value == 0) return "Failed to check the security code";
            Guid uid = new Guid(Sessions.SwitchedSessionId);
            //Guid uid = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            string strvalidateCode = FlashViewer.FlashViewer_GetValidateCode_FileFolderShare(folderId);
            bool success = true;
            var arrEmailName = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(strToMail).ToArray();
            for (int i = 0; i < arrEmailName.Length; i++)
            {
                int shareID = General_Class.SaveShareLink(uid, Enum_Tatva.Folders.FileFolder.GetHashCode(), null, folderId, null, null, null);
                MailID = FlashViewer.FlashViewer_MailFriend(strSendMail, arrEmailName[i]["Email"], folderId, MailTitle, strMailInfor);

                ELink.Add(Tuple.Create(HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("EFileFolderJSONService.asmx")) + "EFileFolderV2.html??MailId=" + MailID + "&ID=" + folderId + "&DATA=" + strvalidateCode + "&ISEDIT=" + false + "&IsDenied=false&t=" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff") + "&l=" + shareID + "&U=" + Sessions.SwitchedRackspaceId, FileName));

                FlashViewer.HtmlViewer_SendMail(arrEmailName[i]["ToName"], SendName, strSendMail, MailTitle, strMailInfor, arrEmailName[i]["Email"], ELink);

                if (MailID < 0 || MailID == 0)
                    success = false;
            }
            if (success)
                return "";
            else
                return "Failed to share document";
        }

        else
        {
            return "Failed to share document";
        }
    }

    /// <summary>
    ///  Back validateCode
    /// </summary>
    /// <param name="folderID"></param>
    /// <param name="userid"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public string backVCode(int folderID, string userid)
    {
        string strvalidateCode = "";
        try
        {
            FileFolder ffd = new FileFolder(folderID);
            if (ffd.IsExist)
            {
                if (ffd.SecurityLevel.Value == 0)//private level
                {
                    DataTable table = UserManagement.getUserIDByUserName(userid);
                    if (table.Rows.Count > 0)
                    {
                        if (table.Rows[0]["UID"].ToString() == ffd.OfficeID.Value //要不是自己创建的或者自己就是OfficeUser
                         || table.Rows[0]["UID"].ToString() == ffd.OfficeID1.Value)
                        {
                            strvalidateCode = FlashViewer.FlashViewer_GetValidateCode(folderID, 0);
                            return strvalidateCode;
                        }
                        else
                        {
                            strvalidateCode = "1";
                        }
                    }
                }
                else
                {
                    strvalidateCode = FlashViewer.FlashViewer_GetValidateCode(folderID, 0);
                    return strvalidateCode;
                }
            }

        }
        catch
        {
            strvalidateCode = "";
        }

        return strvalidateCode;
    }


    /// <summary>
    /// Get a swf file by the special page name,这个函数要测试下，检查哪里调用了这个函数，pageName的值一般是什么
    /// </summary>
    /// <param name="pageName"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public string GetSwfByPageName(string pageName, string validateCode)
    {
        string[] items = pageName.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
        int pageNumber = 1;
        string fileName = Path.GetFileNameWithoutExtension(items[0]);
        string filePathV = Path.GetDirectoryName(items[0]);//virtual path

        string filePath = "";
        try
        {
            if (!CheckValidateCode(validateCode))
            {
                return "";
            }

            //卢远宗 2015-10-29，Volume Or Volume2,3,...
            int startVolumeX = filePathV.IndexOf(CurrentVolume);
            int startVolume = filePathV.IndexOf("Volume");

            int startV = Math.Max(startVolumeX, startVolume);

            string tempPath = filePathV.Substring(startV);


            filePath = Server.MapPath(tempPath);//directory path
            string extanme = Path.GetExtension(items[0]);
            //if page file is swf 
            if (extanme.ToLower() == ".swf")
            {
                return items[0];
            }

        }
        catch
        {
            filePath = "";
        }

        //there are a bugs here, if the pdf file has _ figure.
        string[] vars = fileName.Split('_');//ARB_guide_81_361_1_0.jpg
        if (vars.Length < 5)
        {
            return null;
        }
        else
        {
            int count = vars.Length;
            string pdfFileName = vars[0], swfFileName = "";
            for (int i = 1; i < count - 4; i++)
            {
                pdfFileName = string.Concat(pdfFileName, "_", vars[i]);
            }

            Document doc = new Document(int.Parse(items[1]));
            if (!doc.IsExist)
            {
                return null;
            }



            string ext = ".pdf";
            StreamType extValue = StreamType.PDF;
            try
            {
                extValue = (StreamType)doc.FileExtention.Value;
                ext = this.getFileExtention(extValue);
            }
            catch { }

            pdfFileName = string.Concat(pdfFileName, ext);
            pdfFileName = string.Concat(filePath, Path.DirectorySeparatorChar, pdfFileName);
            try
            {
                pageNumber = Convert.ToInt32(vars[count - 4 + 2]);
                if (PDFUtil.Pdf2Swf(pdfFileName, pageNumber, ref swfFileName))
                {
                    swfFileName = string.Concat(filePathV, Path.DirectorySeparatorChar, swfFileName);
                    return swfFileName;
                }
                else
                {
                    return null;
                }
            }
            catch
            {

                return null;
            }
        }
    }

    protected string getFileExtention(StreamType ext)
    {
        string extname = ".pdf";
        switch (ext)
        {
            case StreamType.PDF:
                extname = ".pdf";
                break;
            case StreamType.Word:
                extname = ".doc";
                break;
            case StreamType.Excel:
                extname = ".xls";
                break;
            case StreamType.PPT:
                extname = ".ppt";
                break;
            case StreamType.RTF:
                extname = ".rtf";
                break;
            default:
                break;
        }

        return extname;
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public bool LoginUser(string userId, string pwd)
    {
        if (authenticationInfo == null)
        {
            return false;
        }
        else
        {
            return UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password);
        }
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public string CheckUser(string userId, string pwd)
    {
        if (authenticationInfo == null)
        {
            return null;
        }
        else
        {
            if (UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
            {
                DataTable table = new DataTable("Accounts");

                try
                {
                    Account dv = new Account();
                    ObjectQuery query = dv.CreateQuery();
                    query.SetCriteria(dv.Name, userId);

                    query.Fill(table);

                    if (table.Rows.Count > 0)
                    {
                        return Convert.ToString(table.Rows[0]["UID"]);
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }


            return null;

        }
    }


    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public string GetEFFXML(string userName)
    {

        XmlDataDocument errorXml = new XmlDataDocument();

        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(FileFolderManagement.GetFileFoldersXML(userName));
            XmlDataDocument xmlDoc = new XmlDataDocument(ds);
            return XmlToJson.XmlToJSON(xmlDoc);
        }
        catch
        {
            errorXml.InnerXml = "<ErrorCode>-1</ErrorCode>";
            return XmlToJson.XmlToJSON(errorXml);
        }

    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
    public string GetMetaXML(int tabID, string validateCode)
    {
        XmlDataDocument xml = new XmlDataDocument();

        try
        {
            if (!CheckValidateCode(validateCode))
            {
                throw new ApplicationException();
            }

            Divider tab = new Divider(tabID);
            if (tab.IsExist)
            {
                xml.LoadXml(tab.MetaXML.Value ?? "<ErrorCode>0</ErrorCode>");
            }
            else
            {
                xml.InnerXml = "<ErrorCode>-1</ErrorCode>";
            }
        }
        catch
        {
            xml.InnerXml = "<ErrorCode>-1</ErrorCode>";
        }

        return XmlToJson.XmlToJSON(xml);

    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public bool SetMetaXML(int tabID, string metaXml, string validateCode)
    {
        XmlDataDocument xml = new XmlDataDocument();

        try
        {
            if (!CheckValidateCode(validateCode))
            {
                return false;
            }

            Divider tab = new Divider(tabID);
            if (tab.IsExist)
            {
                tab.MetaXML.Value = metaXml;
                tab.Update();

                return true;
            }
        }
        catch
        {
        }

        return false;

    }

    #region ActionScript AIR Functons

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public string CreateFileFolder(string foldername, string firstname, string lastname, string dob,
        string security, string[] tabs, string[] colors, string username)
    {
        DateTime dt = DateTime.Now;
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            dt = DateTime.Parse(dob);
        }
        catch { }
        XmlDataDocument xml = new XmlDataDocument();

        DataTable table = UserManagement.ExistUserName1(username);
        if (table == null || table.Rows.Count == 0)
        {
            xml.InnerXml = "<folder><result>false</result><message>The user{" + username + "} doesn't exist</message></folder>";
            return XmlToJson.XmlToJSON(xml);
        }
        else if (table.Rows.Count == 0)
        {
            xml.InnerXml = "<folder><result>false</result><message>The user{" + username + "} doesn't exist</message></folder>";
            return XmlToJson.XmlToJSON(xml);
        }

        Dictionary<string, Divider> list = new Dictionary<string, Divider>();
        for (int i = 0; i < tabs.Length; i++)
        {
            string tabname = tabs[i];
            string color = colors[i];

            Divider newEntity = new Divider();
            newEntity.Name.Value = tabname;
            newEntity.Color.Value = color;

            list.Add(tabname, newEntity);
        }

        try
        {
            //check the folder number
            Account user = new Account(table.Rows[0]["UID"].ToString());
            if (user.IsSubUser.Value == "1")
            {
                user = new Account(table.Rows[0]["OfficeUID"].ToString());
            }

            if (user.FileFolders.Value <= FileFolderManagement.GetUsedFoldersNumber(user.UID.Value))
            {
                xml.InnerXml = "<folder><result>false</result><message>You have no more unused EdFiles.</message></folder>";
                return XmlToJson.XmlToJSON(xml);
            }

            FileFolder folder = new FileFolder();
            folder.FolderName.Value = foldername;
            folder.FirstName.Value = firstname;
            folder.Lastname.Value = lastname;
            folder.SecurityLevel.Value = Convert.ToInt32(security);
            folder.FileNumber.Value = "";
            folder.DOB.Value = dob;
            folder.CreatedDate.Value = DateTime.Now;
            folder.OfficeID.Value = table.Rows[0]["UID"].ToString();

            if (table.Rows[0]["IsSubUser"].Equals("0"))
            {
                folder.OfficeID1.Value = table.Rows[0]["UID"].ToString();
            }
            else
            {
                folder.OfficeID1.Value = table.Rows[0]["OfficeUID"].ToString();
            }

            int newfolderId = FileFolderManagement.CreatNewFileFolder(folder, list);

            xml.InnerXml = "<folder><result>" + newfolderId + "</result></folder>";
        }
        catch (Exception exp)
        {
            xml.InnerXml = "<folder><result>false</result><message>" + exp.Message + "</message></folder>";

        }

        return XmlToJson.XmlToJSON(xml);
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public string CreateFileFolder1(string foldername, string firstname, string lastname, string dob, string fileNumber,
        string security, string[] tabs, string[] colors, string username)
    {
        XmlDataDocument xml = new XmlDataDocument();

        if (authenticationInfo == null)
        {
            xml.InnerXml = "<folder><result>false</result><message>The user{" + username + "} doesn't exist</message></folder>";
            return XmlToJson.XmlToJSON(xml);
        }
        else
        {
            if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
            {
                xml.InnerXml = "<folder><result>false</result><message>The user{" + username + "} doesn't exist</message></folder>";
                return XmlToJson.XmlToJSON(xml);
            }
        }


        DataTable table = UserManagement.ExistUserName1(username);
        if (table == null || table.Rows.Count == 0)
        {
            xml.InnerXml = "<folder><result>false</result><message>The user{" + username + "} doesn't exist</message></folder>";
            return XmlToJson.XmlToJSON(xml);
        }
        else if (table.Rows.Count == 0)
        {
            xml.InnerXml = "<folder><result>false</result><message>The user{" + username + "} doesn't exist</message></folder>";
            return XmlToJson.XmlToJSON(xml);
        }

        Dictionary<string, Divider> list = new Dictionary<string, Divider>();
        for (int i = 0; i < tabs.Length; i++)
        {
            string tabname = tabs[i];
            string color = colors[i];

            Divider newEntity = new Divider();
            newEntity.Name.Value = tabname;
            newEntity.Color.Value = color;

            list.Add(tabname, newEntity);
        }

        try
        {
            //check the folder number
            Account user = new Account(table.Rows[0]["UID"].ToString());
            if (user.IsSubUser.Value == "1")
            {
                user = new Account(table.Rows[0]["OfficeUID"].ToString());
            }

            if (user.FileFolders.Value <= FileFolderManagement.GetUsedFoldersNumber(user.UID.Value))
            {
                xml.InnerXml = "<folder><result>false</result><message>You have no more unused EdFiles.</message></folder>";
                return XmlToJson.XmlToJSON(xml);
            }

            FileFolder folder = new FileFolder();
            folder.FolderName.Value = foldername;
            folder.FirstName.Value = firstname;
            folder.Lastname.Value = lastname;
            folder.SecurityLevel.Value = Convert.ToInt32(security);
            folder.FileNumber.Value = fileNumber;
            folder.DOB.Value = dob;
            folder.CreatedDate.Value = DateTime.Now;
            folder.OfficeID.Value = table.Rows[0]["UID"].ToString();

            if (table.Rows[0]["IsSubUser"].Equals("0"))
            {
                folder.OfficeID1.Value = table.Rows[0]["UID"].ToString();
            }
            else
            {
                folder.OfficeID1.Value = table.Rows[0]["OfficeUID"].ToString();
            }

            int newfolderId = FileFolderManagement.CreatNewFileFolder(folder, list);

            xml.InnerXml = "<folder><result>" + newfolderId + "</result></folder>";
        }
        catch (Exception exp)
        {
            xml.InnerXml = "<folder><result>false</result><message>" + exp.Message + "</message></folder>";

        }

        return XmlToJson.XmlToJSON(xml);
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public DataTable GetFileFolders(string userName)
    {
        DataTable dt = new DataTable("Folder");
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            DataTable uTable = UserManagement.getUserIDByUserName(userName);
            if (uTable.Rows.Count == 0) throw new ApplicationException("-1");
            Account user = new Account(uTable.Rows[0][0].ToString());
            if (user.IsExist)
            {
                string officeID1 = string.Empty;

                if (user.RoleID.Value == "Offices")
                {
                    officeID1 = user.UID.Value;
                }
                else if (user.RoleID.Value == "Users")
                {
                    officeID1 = user.OfficeUID.Value;
                }

                dt = FileFolderManagement.GetFileFoldersInfor(officeID1);

            }

        }
        catch (Exception ex)
        {

        }

        return dt;
    }

    /// <summary>
    /// 这个函数也要测试
    /// </summary>
    /// <param name="folderId"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public string RemoveEFileFolder(string folderId)
    {
        XmlDataDocument xml = new XmlDataDocument();
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            //这样好像不对的，应该检查folderId的位置
            if (CurrentVolume.ToLower().Equals("Volume".ToLower()))
            {
                string folderPath = string.Format("~/{0}/", CurrentVolume) + folderId;
                if (Directory.Exists(Server.MapPath(folderPath)))
                {
                    Directory.Delete(Server.MapPath(folderPath), true);
                }
            }
            else
            {
                int startVolumeX;
                string serial = CurrentVolume.Substring("Volume".Length);

                if (Int32.TryParse(serial, out startVolumeX))
                {
                    for (int i = 0; i <= startVolumeX; i++)
                    {
                        string v = i == 0 ? "Volume" : "Volume" + i;

                        string folderPath = string.Format("~/{0}/", CurrentVolume) + folderId;
                        if (Directory.Exists(Server.MapPath(folderPath)))
                        {
                            Directory.Delete(Server.MapPath(folderPath), true);
                        }
                    }
                }
            }




        }
        catch (Exception ex)
        {
            xml.InnerXml = "<result>" + ex.Message + "</result>"; ;

            return XmlToJson.XmlToJSON(xml);
        }

        FileFolderManagement.DelFileFolderByFolderID(folderId);
        xml.InnerXml = "<result>true</result>";
        return XmlToJson.XmlToJSON(xml);
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public string ModifyEFileFolder(string fid, string foldername, string firstname, string lastname, string dob, string security)
    {
        DateTime dt = DateTime.Now;
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            dt = DateTime.Parse(dob);
        }
        catch { }

        XmlDataDocument xml = new XmlDataDocument();

        try
        {

            FileFolder folder = new FileFolder(int.Parse(fid));
            if (folder.IsExist)
            {
                folder.FolderName.Value = foldername;
                folder.FirstName.Value = firstname;
                folder.Lastname.Value = lastname;
                folder.FileNumber.Value = "";
                folder.DOB.Value = dob;
                folder.CreatedDate.Value = DateTime.Now;
                folder.SecurityLevel.Value = int.Parse(security);

                folder.Update();

                xml.InnerXml = "<result>true</result>";
            }
            else
            {
                xml.InnerXml = "<result>the folder doesn't exist.</result>"; ;
            }
        }
        catch (Exception exp)
        {
            xml.InnerXml = "<result>" + exp.Message + "</result>";
        }

        return XmlToJson.XmlToJSON(xml);

    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public string ModifyEFileFolder1(string fid, string foldername, string firstname, string lastname, string dob, string fileNumber,
        string security)
    {
        XmlDataDocument xml = new XmlDataDocument();

        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            FileFolder folder = new FileFolder(int.Parse(fid));
            if (folder.IsExist)
            {
                folder.FolderName.Value = foldername;
                folder.FirstName.Value = firstname;
                folder.Lastname.Value = lastname;
                folder.FileNumber.Value = fileNumber;
                folder.DOB.Value = dob;
                folder.CreatedDate.Value = DateTime.Now;
                folder.SecurityLevel.Value = int.Parse(security);

                folder.Update();

                xml.InnerXml = "<result>true</result>";
            }
            else
            {
                xml.InnerXml = "<result>the folder doesn't exist.</result>"; ;
            }
        }
        catch (Exception exp)
        {
            xml.InnerXml = "<result>" + exp.Message + "</result>";
        }

        return XmlToJson.XmlToJSON(xml);

    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool RemoveTab1(string tabid)
    {
        FileFolderManagement.DeleteDivider(int.Parse(tabid));
        return true;
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool ModifyTab(string tabid, string tabname, string color)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            Divider objDivider = new Divider(int.Parse(tabid));
            if (objDivider.IsExist)
            {
                objDivider.Name.Value = tabname.Trim();
                objDivider.Color.Value = color;
                objDivider.Update();
                return true;
            }
        }
        catch { }

        return false;

    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public string InsertTab1(string tabname, string color, string username, string officeid)
    {
        XmlDataDocument xml = new XmlDataDocument();
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            string uid = UserManagement.ExistUserName(username);
            if (uid == null)
            {
                xml.InnerXml = "<result>false</result>";
                return XmlToJson.XmlToJSON(xml);
            }

            Divider objDivider = new Divider();
            objDivider.Name.Value = tabname;
            objDivider.Color.Value = color;
            objDivider.EffID.Value = Convert.ToInt32(officeid);
            objDivider.OfficeID.Value = uid;

            FileFolderManagement.InsertDivider(objDivider);
            xml.InnerXml = "<result>" + objDivider.DividerID.Value.ToString() + "</result>";
        }
        catch
        {
            xml.InnerXml = "<result>false</result>";
        }
        return XmlToJson.XmlToJSON(xml);
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public DataTable GetTabsByFolderID(string folderid)
    {
        DataTable table = new DataTable("Tabs");

        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            Divider dv = new Divider();
            ObjectQuery query = dv.CreateQuery();
            query.SetCriteria(dv.EffID, Convert.ToInt32(folderid));

            query.Fill(table);

        }
        catch
        { }
        return table;
    }
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public DataTable GetDocumentsByDividerId(string dividerid)
    {
        DataTable table = new DataTable("Document");

        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            Document doc = new Document();
            ObjectQuery query = doc.CreateQuery();
            query.SetCriteria(doc.DividerID, Convert.ToInt32(dividerid));
            query.Fill(table);
        }
        catch
        { }
        return table;
    }

    public string ScanInBox
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["ScanInBox"];
        }
    }

    public string WorkArea2
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["WorkArea"];
        }
    }

    public static string eFileFlowShare
    {
        get
        {
            return System.Configuration.ConfigurationManager.AppSettings["eFileFlowShare"];
        }
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public int MoveDocumentsToAnotherFolder(string pathName, string fileName, int FileExtention, string folderId, string dividerId, byte[] fileData, string uid, string alert1, string alert2) //byte[] filedata
    {
        int newDocumentId = 0;
        try
        {
            string sourceFile = string.Empty;
            string destinationFile = string.Empty;
            string extname = Enum.GetName(typeof(Shinetech.Engines.StreamType), FileExtention);
            //sourceFile = HttpContext.Current.Server.MapPath(pathName) + "\\" + fileName + "." + extname;
            sourceFile = pathName + "\\" + fileName + "." + extname;
            string encodeName = HttpUtility.UrlPathEncode(fileName);
            encodeName = encodeName.Replace("%", "$");

            string newPath = CurrentVolume + "\\" + folderId + "\\" + dividerId + "\\" + encodeName + "." + extname;
            string newPathForDocumentInsert = CurrentVolume + "/" + folderId + "/" + dividerId + "/" + encodeName + "." + extname;

            destinationFile = HttpContext.Current.Server.MapPath(newPath);

            if (!Directory.Exists(Path.GetDirectoryName(destinationFile)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(destinationFile));
            }
            if (File.Exists(destinationFile))
            {
                destinationFile = GetRenameFileName(destinationFile);
                fileName = Path.GetFileNameWithoutExtension(destinationFile);
                newPath = newPath.Substring(0, newPath.LastIndexOf(Path.DirectorySeparatorChar)) + Path.DirectorySeparatorChar + fileName + Path.GetExtension(destinationFile);
            }
            // Open file for reading
            System.IO.FileStream _FileStream =
              new System.IO.FileStream(destinationFile, System.IO.FileMode.Create,
                                        System.IO.FileAccess.Write);
            // Writes a block of bytes to this stream using data from
            // a byte array.
            _FileStream.Write(fileData, 0, fileData.Length);

            // close file stream
            _FileStream.Close();

            //File.Copy(sourceFile, destinationFile, true);

            //insert document in database 
            int documentOrder = FlashViewer.GetDocumentOrder(Convert.ToInt32(dividerId));
            newDocumentId = Common_Tatva.InsertForScanDocument(Convert.ToInt32(dividerId), newPath, documentOrder, fileName, Enum_Tatva.Class.Permanent.GetHashCode().ToString(), uid);
            if (newDocumentId > 0)
            {
                General_Class.AuditLogByDocId(newDocumentId, uid, Enum_Tatva.Action.Create.GetHashCode());
                InsertDocumentsAndFolderLogs(Convert.ToInt32(folderId), Convert.ToInt32(dividerId), newDocumentId, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Scan2EdfUpload.GetHashCode(), 0, 0);
            }
            if ((alert1 != "" && alert1 != null) || (alert2 != "" && alert2 != null))
            {
                General_Class.UpdateFileFolder(folderId, alert1, alert2);
            }
            //File.Move(sourceFile, destinationFile);
            return Convert.ToInt32(Enum_Tatva.FileMove.FileMoveSuccess);
        }
        catch (Exception ex)
        {
            NLogLogger _logger = new NLogLogger();
            _logger.Info(ex.Message, ex);
            return Convert.ToInt32(Enum_Tatva.FileMove.FileMoveError);
        }
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public int MoveDocumentsToWorkAreaFolder(string pathName, string fileName, int FileExtention, byte[] fileData, string uid)
    {
        try
        {
            string sourceFile = string.Empty;
            string destinationFile = string.Empty;

            string extname = Enum.GetName(typeof(Shinetech.Engines.StreamType), FileExtention);
            //sourceFile = HttpContext.Current.Server.MapPath(pathName) + "\\" + fileName + "." + extname;
            // sourceFile = pathName + "//" + fileName + "." + extname;

            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            string errorMsg = string.Empty;
            string newPath = rackSpaceFileUpload.UploadObject(ref errorMsg, fileName + "." + extname, Enum_Tatva.Folders.WorkArea, new MemoryStream(fileData), null, uid);
            if (string.IsNullOrEmpty(errorMsg))
            {
                decimal size = Math.Round((decimal)(rackSpaceFileUpload.GetSizeOfFile(ref errorMsg, newPath, uid) / 1024) / 1024, 2);
                string workAreaID = General_Class.DocumentsInsertForWorkArea(newPath.Substring(newPath.LastIndexOf('/') + 1), uid, uid, size, Enum_Tatva.IsDelete.No.GetHashCode(), newPath, Enum_Tatva.WorkAreaType.UtilityUpload.GetHashCode());
                if (Convert.ToInt32(workAreaID) > 0)
                    InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Upload.GetHashCode(), 0, 1, 0, Convert.ToInt32(workAreaID));
            }
            else
            {
                return Convert.ToInt32(Enum_Tatva.FileMove.FileMoveError);
            }
            return Convert.ToInt32(Enum_Tatva.FileMove.FileMoveSuccess);
        }
        catch (Exception exc)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, exc);
            return Convert.ToInt32(Enum_Tatva.FileMove.FileMoveError);
        }
    }

    [WebMethod]
    public int MoveDocumentsToWorkArea(string fileName, int FileExtention, byte[] fileData, string uid)
    {
        try
        {
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            string sourceFile = string.Empty;
            string destinationFile = string.Empty;

            string extname = Enum.GetName(typeof(Shinetech.Engines.StreamType), FileExtention);
            string errorMsg = string.Empty;
            string folderPath = Path.GetDirectoryName(fileName);
            string newPath = rackSpaceFileUpload.UploadObject(ref errorMsg, fileName + "." + extname, Enum_Tatva.Folders.WorkArea, new MemoryStream(fileData), folderPath.Replace('\\', '/'), uid);
            if (string.IsNullOrEmpty(errorMsg))
            {
                decimal size = 0;
                try
                {
                    size = Math.Round((decimal)(rackSpaceFileUpload.GetSizeOfFile(ref errorMsg, newPath, uid) / 1024) / 1024, 2);
                }
                catch (Exception) { }
                string[] splitedFolders = newPath.Substring(0, newPath.LastIndexOf('/')).Split('/');
                int index = 0;
                string pathName = string.Empty;
                int parentId = 0;
                for (int i = 0; i < splitedFolders.Length; i++)
                {
                    pathName += splitedFolders[i] + "/";
                    if (index > 0)
                    {
                        parentId = General_Class.CreateFolderForWorkArea(splitedFolders[i], pathName.Remove(pathName.Length - 1, 1), uid, index, parentId);
                    }
                    index++;
                }
                string workAreaID = General_Class.DocumentsInsertForWorkArea(newPath.Substring(newPath.LastIndexOf('/') + 1), uid, uid, size, Enum_Tatva.IsDelete.No.GetHashCode(), newPath, Enum_Tatva.WorkAreaType.RenameUpload.GetHashCode(), parentId);
                if (Convert.ToInt32(workAreaID) > 0)
                    InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Upload.GetHashCode(), 0, 1, 0, Convert.ToInt32(workAreaID));
                else
                    return Convert.ToInt32(Enum_Tatva.FileMove.FileMoveError);
            }
            else
            {
                return Convert.ToInt32(Enum_Tatva.FileMove.FileMoveError);
            }
            return Convert.ToInt32(Enum_Tatva.FileMove.FileMoveSuccess);
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            return Convert.ToInt32(Enum_Tatva.FileMove.FileMoveError);
        }
    }

    public string GetFolderIdFromPath(string path)
    {
        string folderId = string.Empty;
        folderId = path.Split('/')[1];
        return folderId;
    }

    public string GetDividerIdFromPath(string path)
    {
        string dividerId = string.Empty;
        dividerId = path.Split('/')[2];
        return dividerId;
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public string GetDocumentPath(string pathName, string fileName, int FileExtention)
    {
        try
        {
            string sourceFile = string.Empty;
            string destinationFile = string.Empty;
            string extname = Enum.GetName(typeof(Shinetech.Engines.StreamType), FileExtention);
            sourceFile = HttpContext.Current.Server.MapPath(pathName) + "\\" + fileName + "." + extname;

            return sourceFile;
        }
        catch (Exception)
        {
            return "";
        }
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public string ExitsFileNameByUID(string fileName, string uid)
    {
        if (authenticationInfo == null)
        {
            return "NA";
        }
        else
        {
            if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
            {
                return "NA";
            }
        }


        string id = UserManagement.ExistUserName(uid);
        if (id == null)
        {
            return "No:" + fileName;
        }

        string path = Server.MapPath("~/ScanInBox");
        string filepath = string.Concat(path, Path.DirectorySeparatorChar, id, Path.DirectorySeparatorChar, fileName);

        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);

            return "No:" + fileName;
        }

        if (File.Exists(filepath))
        {
            return "Yes:" + fileName;
        }
        else
        {
            return "No:" + fileName;
        }

    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public string GetCurrentVersion()
    {
        if (authenticationInfo == null)
        {
            return "false";
        }
        else
        {
            if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
            {
                return "false";
            }
        }


        XmlDocument doc = new XmlDocument();

        string path = Server.MapPath("~/");
        string filepath = string.Concat(path, Path.DirectorySeparatorChar, "update.xml");

        if (File.Exists(filepath))
        {

            doc.Load(filepath);
            XmlNode node = doc.GetElementsByTagName("version")[0];

            return node.InnerText;
        }

        return "false";
    }

    [WebMethod]
    public string VerifySecurityCode(int folderid, string code)
    {
        XmlDataDocument errorXml = new XmlDataDocument();

        try
        {
            FileFolder folder = new FileFolder(folderid);
            if (folder.IsExist)
            {
                if (folder.SecurityLevel.Value == 2)
                {

                    if (folder.FolderCode.Value.Equals(code, StringComparison.OrdinalIgnoreCase))
                    {
                        errorXml.InnerXml = "<ErrorCode>" + code + "</ErrorCode>";
                        return XmlToJson.XmlToJSON(errorXml);
                    }
                    else
                    {
                        errorXml.InnerXml = "<ErrorCode>0</ErrorCode>";
                        return XmlToJson.XmlToJSON(errorXml);
                    }
                }
                else
                {
                    errorXml.InnerXml = "<ErrorCode>" + code + "</ErrorCode>";
                    return XmlToJson.XmlToJSON(errorXml);
                }
            }

        }
        catch
        {

        }

        errorXml.InnerXml = "<ErrorCode>0</ErrorCode>";
        return XmlToJson.XmlToJSON(errorXml);
    }

    [WebMethod(Description = "Whether this User is authorized")]
    public bool IsAuthorized()
    {
        try
        {
            return this.User.Identity.IsAuthenticated;
        }
        catch { }

        return false;


    }
    #endregion


    #region API for File Manager
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public int CreateFileFolder3(string foldername, string firstname, string lastname, DateTime dob,
        string security, string filenumber, string guid)
    {
        DateTime dt = DateTime.Now;
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            dt = dob;
        }
        catch
        {
            return -1;
        }


        try
        {
            int id = ExistFolderByGUID(guid);

            if (id != -1) return id;


            DataTable table = UserManagement.ExistUserName1(authenticationInfo.username);

            //check the folder number
            Account user = new Account(table.Rows[0]["UID"].ToString());
            string ouid = user.UID.Value;
            bool bSubUser = false;
            if (user.IsSubUser.Value == "1")
            {
                bSubUser = true;
                user = new Account(user.OfficeUID.Value);
            }

            if (user.FileFolders.Value <= FileFolderManagement.GetUsedFoldersNumber(user.UID.Value))
            {
                return -1;
            }

            FileFolder folder = new FileFolder();
            folder.FolderName.Value = foldername;
            folder.FirstName.Value = firstname;
            folder.Lastname.Value = lastname;
            folder.SecurityLevel.Value = Convert.ToInt32(security);
            folder.FileNumber.Value = "";
            folder.DOB.Value = dob.ToString("MM-dd-yyyy");
            folder.CreatedDate.Value = DateTime.Now;
            folder.OfficeID.Value = ouid;
            folder.GUID.Value = guid; // for filemanager

            if (!bSubUser)
            {
                folder.OfficeID1.Value = ouid;
            }
            else
            {
                folder.OfficeID1.Value = user.UID.Value;
            }

            return FileFolderManagement.CreatNewFileFolder(folder);


        }
        catch (Exception exp)
        {
            return -1;

        }
    }

    public int ExistFolderByGUID(string guid)
    {

        IDbConnection connection = DbFactory.CreateConnection();

        try
        {
            connection.Open();

            DataTable table = new DataTable();
            FileFolder folder = new FileFolder();
            ObjectQuery query = folder.CreateQuery();
            query.SetCriteria(folder.GUID, guid);
            query.SetSelectFields(folder.FolderID);
            query.Fill(table);
            if (table.Rows.Count > 0)
            {
                return (int)table.Rows[0]["FolderID"];
            }
            else
            {
                return -1;
            }

        }
        catch (Exception ex)
        {
            return -1;
        }
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool UpdateFileFolder(int id, string foldername, string firstname, string lastname, string security, string filenumber, string guid)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }
        }
        catch
        {
            return false;
        }


        try
        {
            if (id == -1)
            {
                id = GetFileFolderByGUID(guid);
            }

            FileFolder folder = new FileFolder(id);
            if (folder.IsExist)
            {
                folder.FolderName.Value = foldername;
                folder.FirstName.Value = firstname;
                folder.Lastname.Value = lastname;
                folder.SecurityLevel.Value = Convert.ToInt32(security);
                folder.FileNumber.Value = filenumber;
            }
            else
            {
                return false;
            }


            return FileFolderManagement.UpdateFileFolder(folder);


        }
        catch (Exception exp)
        {
            return false;

        }
    }

    private int GetFileFolderByGUID(string guid)
    {
        IDbConnection connection = DbFactory.CreateConnection();

        try
        {
            connection.Open();
            DataTable table = new DataTable();
            FileFolder folder = new FileFolder();
            ObjectQuery query = folder.CreateQuery();
            query.SetSelectFields(folder.FolderID);
            query.SetCriteria(folder.GUID, guid);

            query.Fill(table);

            if (table.Rows.Count == 1)
            {
                return (int)table.Rows[0]["FolderID"];
            }
            else
            {
                throw new ApplicationException();
            }
        }
        catch (Exception ex)
        {
            throw new ApplicationException(ex.Message);
        }
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool RemoveFileFolder(int fid)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            return FileFolderManagement.DelFileFolderByFolderID(fid.ToString());

        }
        catch
        {
            return false;
        }
    }


    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public int InsertTab(string tabname, string color, int folderid, string guid)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            FileFolder folder = new FileFolder(folderid);
            if (folder.IsExist)
            {
                int id = ExistDividerByGUID(guid);
                if (id != -1) return id;

                Divider objDivider = new Divider();
                objDivider.Name.Value = tabname;
                objDivider.Color.Value = color;
                objDivider.EffID.Value = folder.FolderID.Value;
                objDivider.OfficeID.Value = folder.OfficeID.Value;
                objDivider.GUID.Value = guid;


                int ret = FileFolderManagement.InsertDivider(objDivider);
                if (ret == -1) return -1;

                folder.OtherInfo.Value = folder.OtherInfo.Value + 1;

                FileFolderManagement.UpdateFileFolder(folder);

                return ret;
            }
            else
            {
                return -1;
            }

        }
        catch
        {
            return -1;
        }
    }

    public int ExistDividerByGUID(string guid)
    {

        IDbConnection connection = DbFactory.CreateConnection();

        try
        {
            connection.Open();
            DataTable table = new DataTable();
            Divider divider = new Divider();
            ObjectQuery query = divider.CreateQuery();
            query.SetCriteria(divider.GUID, guid);
            query.SetSelectFields(divider.DividerID);

            query.Fill(table);

            if (table.Rows.Count > 0)
            {
                return (int)table.Rows[0]["DividerID"];
            }
            else
            {
                return -1;
            }

        }
        catch (Exception ex)
        {
            return -1;
        }
    }


    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool UpdateTab(int tabid, string tabname, string color, int folderid, string guid)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }


            if (tabid == -1)
            {
                tabid = GetDividerByGUID(guid);
            }

            Divider objDivider = new Divider(tabid);
            if (objDivider.IsExist)
            {
                objDivider.Name.Value = tabname;
                objDivider.Color.Value = color;

                return FileFolderManagement.UpdateDivider(objDivider);
            }
            else
            {
                return false;
            }


        }
        catch
        {
            return false;
        }
    }

    public int GetDividerByGUID(string guid)
    {
        IDbConnection connection = DbFactory.CreateConnection();

        try
        {
            connection.Open();
            DataTable table = new DataTable();
            Divider divider = new Divider();
            ObjectQuery query = divider.CreateQuery();
            query.SetSelectFields(divider.DividerID);
            query.SetCriteria(divider.GUID, guid);

            query.Fill(table);

            if (table.Rows.Count == 1)
            {
                return (int)table.Rows[0]["DividerID"];
            }
            else
            {
                throw new ApplicationException();
            }
        }
        catch (Exception ex)
        {
            throw new ApplicationException(ex.Message);
        }
    }


    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool RemoveTab(int tabid, string guid)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            if (tabid == -1)
            {
                tabid = this.GetDividerByGUID(guid);
            }
            return FileFolderManagement.DeleteDivider(tabid);

        }
        catch
        {
            return false;
        }
    }


    public int ExistFileByGUID(string guid)
    {

        IDbConnection connection = DbFactory.CreateConnection();

        try
        {
            connection.Open();

            DataTable table = new DataTable();
            Document document = new Document();
            ObjectQuery query = document.CreateQuery();
            query.SetCriteria(document.GUID, guid);
            query.SetSelectFields(document.DocumentID);
            query.Fill(table);
            if (table.Rows.Count > 0)
            {
                return (int)table.Rows[0]["DocumentID"];
            }
            else
            {
                return -1;
            }

        }
        catch (Exception ex)
        {
            return -1;
        }
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool AppendFromScan(string fileNameWithExtend, string folderName, string uid, string folderId, string dividerId)
    {
        //源文件路径，在ScanSnap目录中
        string filePath = Path.Combine(Path.Combine(Server.MapPath("~/ScanSnap"), uid), fileNameWithExtend);
        if (File.Exists(filePath))
        {

            string fid = folderId;
            string tid = dividerId;

            string vpath = Server.MapPath(string.Format("~/{0}", CurrentVolume)); //目的路径
            string pathname = string.Format("{0}\\{1}\\{2}", vpath, fid, tid); //要拷贝到这里

            //
            string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, fid, Path.DirectorySeparatorChar, tid); //转换目标路径
            if (!Directory.Exists(dstpath)) //没有则创建该目录
            {
                Directory.CreateDirectory(dstpath);
            }

            string filename = Path.GetFileName(filePath); //文件名称
            string encodeName = HttpUtility.UrlPathEncode(filename); //对文件进行编码
            encodeName = encodeName.Replace("%", "$");  //编码后的文件名称

            dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);//合成完整路径,包括了文件名称

            bool existFile = false;

            if (File.Exists(filePath))
            {
                //拷贝文件并转换格式
                try
                {
                    existFile = true;

                    File.Copy(filePath, dstpath, true); //把文件拷贝到dstpath
                    ImageAdapter.ProcessPostFile(dstpath, fid, tid, uid, pathname, filePath);

                    File.Delete(filePath); //添加后删除原文  

                    return true;
                }
                catch
                {

                }
            }
        }

        return false;

    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public int AppendFile(string filename, int pagecount, string filepath, float size, int format, int folderid,
        string fguid, int tabid)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            int id = ExistFileByGUID(fguid);

            if (id != -1) return id;

            Divider divider = new Divider(tabid);
            if (divider.IsExist)
            {
                Document doc = new Document();
                doc.Name.Value = filename;
                doc.PagesCount.Value = pagecount;
                doc.PathName.Value = filepath;
                doc.Size.Value = size;
                doc.FileFolderID.Value = folderid;
                doc.DividerID.Value = tabid;
                doc.DOB.Value = DateTime.Now;
                doc.UID.Value = divider.OfficeID.Value;
                doc.FileExtention.Value = format;
                doc.GUID.Value = fguid;


                IDbConnection connection = DbFactory.CreateConnection();
                connection.Open();

                string encodeName = HttpUtility.UrlPathEncode(filename);
                encodeName = encodeName.Replace("%", "$");

                IDbTransaction transaction = connection.BeginTransaction();
                try
                {
                    doc.Create(transaction);

                    for (int i = 1; i <= doc.PagesCount.Value; i++)
                    {
                        DocPage page = new DocPage();
                        page.PageNumber.Value = i;
                        page.DocumentID.Value = doc.DocumentID.Value;
                        page.ImageName.Value = string.Format("{0}_{1}_{2}_{3}.swf", encodeName, folderid, tabid, i);
                        page.ImagePath.Value = filepath + Path.DirectorySeparatorChar + page.ImageName.Value;

                        page.DividerID.Value = doc.DividerID.Value;
                        page.Size.Value = 0;
                        page.Create(transaction);
                    }

                    FileFolder eff = new FileFolder(folderid);
                    eff.OtherInfo2.Value += doc.Size.Value;
                    eff.Update(transaction);

                    transaction.Commit();

                    return doc.DocumentID.Value;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new ApplicationException(ex.Message);
                }
            }

            return -1;

        }
        catch
        {
            return -1;
        }
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool RemoveFile(int fid, string fguid)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            if (fid == -1)
            {
                fid = FileFolderManagement.GetFileIdByGUID(fguid);
                if (fid == -1) return true;
            }
            FileFolderManagement.DelPageByDocumentID(fid.ToString());

            DataTable dtFileFolder = FileFolderManagement.GetFileFolderSizeByDocumentID(fid.ToString());
            string strFolderSize = dtFileFolder.Rows[0]["Otherinfo2"].ToString();
            string strFolderID = dtFileFolder.Rows[0]["FolderID"].ToString();

            DataTable dtDocInfor = FileFolderManagement.GetDocInforByDocumentID(fid.ToString());
            string strDocSize = dtDocInfor.Rows[0]["size"].ToString();

            Double newFolderSize = Convert.ToDouble(strFolderSize) - Convert.ToDouble(strDocSize);
            FileFolderManagement.UpdateFolderSize(strFolderID, newFolderSize);


            return FileFolderManagement.DelDocumentByDocumentID(fid.ToString());

        }
        catch
        {
            return false;
        }
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool UploadWorkarea(string FileName, string uid, byte[] buffer, long Offset)
    {
        if (authenticationInfo == null)
        {
            throw new ApplicationException("");
        }

        bool retVal = false;
        try
        {
            // setting the file location to be saved in the server. 
            // reading from the web.config file 
            string folderPath = Path.Combine(ConfigurationManager.AppSettings["ScanBox_Path"] as string, uid);

            if (!Directory.Exists(folderPath)) Directory.CreateDirectory(folderPath);

            string FilePath = Path.Combine(folderPath, FileName);

            if (Offset == 0) // new file, create an empty file
                File.Create(FilePath).Close();
            // open a file stream and write the buffer. 
            // Don't open with FileMode.Append because the transfer may wish to 
            // start a different point
            using (FileStream fs = new FileStream(FilePath, FileMode.Open,
                FileAccess.ReadWrite, FileShare.Read))
            {
                fs.Seek(Offset, SeekOrigin.Begin);
                fs.Write(buffer, 0, buffer.Length);
            }
            retVal = true;
        }
        catch (Exception ex)
        {
            //sending error to an email id

        }
        return retVal;
    }


    [WebMethod(BufferResponse = false)]
    [SoapHeader("authenticationInfo")]
    public bool UploadFile(byte[] filedata, int dividerId, int folderId, string namewithext)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            MemoryStream ms = new MemoryStream(filedata);
            string rootPath = System.Web.Hosting.HostingEnvironment.MapPath(string.Format("~/{0}/", CurrentVolume)) + folderId + Path.DirectorySeparatorChar + dividerId;

            if (!Directory.Exists(rootPath))
            {
                Directory.CreateDirectory(rootPath);
            }

            string filename = rootPath + Path.DirectorySeparatorChar + namewithext;

            if (File.Exists(filename))
            {
                return true;
            }

            FileStream fs = new FileStream(filename, FileMode.Create);

            ms.WriteTo(fs);

            ms.Close();
            fs.Close();
            fs.Dispose();
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }

    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public void AppendChunk(int dividerId, int folderId, string namewithext, byte[] filedata, long Offset)
    {
        if (authenticationInfo == null)
        {
            return;
        }
        else
        {
            if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
            {
                return;
            }
        }

        string rootPath = System.Web.Hosting.HostingEnvironment.MapPath(string.Format("~/{0}/", CurrentVolume)) + folderId + Path.DirectorySeparatorChar + dividerId;

        if (!Directory.Exists(rootPath))
        {
            Directory.CreateDirectory(rootPath);
        }

        string filename = rootPath + Path.DirectorySeparatorChar + namewithext;

        if (Offset == 0)	// new file, create an empty file
        {
            File.Create(filename).Close();
        }

        // open a file stream and write the buffer.  Don't open with FileMode.Append because the transfer may wish to start a different point
        using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.ReadWrite, FileShare.Read))
        {
            try
            {
                fs.Seek(Offset, SeekOrigin.Begin);
                fs.Write(filedata, 0, filedata.Length);
            }
            catch
            {
            }

        }
    }

    #endregion

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public FolderData GetFolderXMLForFront(int folderId, string validateCode)
    {
        XmlDataDocument errorXml = new XmlDataDocument();
        FolderData ds = new FolderData();

        //if (CheckValidateCode(validateCode)) //|| trues
        //{
        FileFolder folder = new FileFolder(folderId);

        if (folder.IsExist)
        {
            ds = FlashViewer.FlashViewer_GetFolderJsonData(folderId);

            if (ds != null)
            {
                ds.ErrorCode = 0;
                return ds;
            }
            else
            {
                ds.ErrorCode = -2;
                return ds;
            }
        }
        else
        {
            //errorXml.InnerXml = "<ErrorCode>-3</ErrorCode>";
            ds.ErrorCode = -3;
            return ds;
        }
        //}

    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DividerData GetTabXMLForFront(int tabId, string validateCode)
    {
        DividerData ds = new DividerData();

        XmlDataDocument errorXml = new XmlDataDocument();

        ds = FlashViewer.FlashViewer_GetTabPDFJson(tabId);
        ds.ErrorCode = 0;
        return ds;
    }

    /// <summary>
    ///  update new note
    /// </summary>
    /// <param name="uid"></param>
    /// <param name="id"></param>
    /// <param name="tabId"></param>
    /// <param name="content"></param>
    /// <param name="validateCode"></param>
    /// <param name="strTitle"></param>
    /// <param name="strAuthor"></param>
    /// <returns></returns>

    [WebMethod(EnableSession = true)]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string UpdateDocumentOrder1(int sourceId, int targetId, int dividerId, int SourceDocOrder, int TargetDocOrder)
    {
        DataSet ds = new DataSet();
        string docList = string.Empty;
        try
        {
            ds.Tables.Add(FlashViewer.UpdateDocumentOrder(sourceId, targetId, dividerId, SourceDocOrder, TargetDocOrder));
            if (ds != null && ds.Tables.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    docList += (!string.IsNullOrEmpty(docList) ? ",{" : "{") +
                                                          "\"DocumentID\"" + ":\"" + Convert.ToInt32(ds.Tables[0].Rows[i]["DocumentID"]) + "\"," +
                                                          "\"DividerID\"" + ":\"" + Convert.ToInt32(ds.Tables[0].Rows[i]["DividerID"]) + "\"," +
                                                          "\"DocumentOrder\"" + ":\"" + Convert.ToInt32(ds.Tables[0].Rows[i]["DocumentOrder"]) + "\"," +
                                                          "\"PagesCount\"" + ":\"" + Convert.ToInt32(ds.Tables[0].Rows[i]["PagesCount"]) + "\"}";
                }
            }
            docList = !string.IsNullOrEmpty(docList) ? "[" + docList + "]" : "";
            if (ds.Tables[0].Select("DocumentOrder = '" + TargetDocOrder + "'").Length > 0)
            {
                DataRow dr = ds.Tables[0].Select("DocumentOrder = '" + TargetDocOrder + "'").CopyToDataTable().Rows[0];
                int folderId = Convert.ToInt32(dr["FileFolderId"]);
                int documentId = Convert.ToInt32(dr["DocumentId"]);
                InsertDocumentsAndFolderLogs(folderId, dividerId, documentId, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Order.GetHashCode(), 0, 0);
            }
            return docList;
            //InsertDocumentsAndFolderLogs(
        }

        catch (Exception exp)
        {
            //WriteErrorMessage(exp.Message);
            return null;
        }
    }

    /// <summary>
    /// Update CheckListAnswers
    /// </summary>
    /// <param name="Answer"></param>
    /// <param name="Id"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string UpdateCheckListAnswer(string Answer, int Id)
    {
        DataSet ds = new DataSet();
        ds.Tables.Add(General_Class.CheckListAnswersUpdate(Answer, Id));
        if (ds != null && ds.Tables.Count > 0)
        {
            return "1";
        }
        else
        {
            return "0";
        }

        //DataTable dtAnswer = CheckListQuestion.UpdateCheckListQuestionAnswer1(Answer, Id);
        //return dtAnswer;  
    }

    /// <summary>
    /// renameFile Foe eFileFlow
    /// </summary>
    /// <param name="renameFile">New file name</param>
    /// <param name="path"></param>
    /// <param name="eFileFlowId">Unique id of file</param>
    /// <param name="oldFileName">File name to be renamed</param>
    [WebMethod(EnableSession = true)]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string RenameDocumentForEdForms(string renameFile, string edFormsId, string oldFileName, string departmentId, string edFormsUserId = null)
    {
        string errorMsg = string.Empty;
        try
        {
            string rackspaceUId = string.Empty;
            if (string.IsNullOrEmpty(Sessions.SwitchedRackspaceId))
            {
                if (!string.IsNullOrEmpty(Sessions.SwitchedUserName))
                {
                    string uid = Membership.GetUser(Sessions.SwitchedUserName).ProviderUserKey.ToString();

                    Account account = new Account(uid);

                    if (account.IsSubUser.ToString() == "1")
                        rackspaceUId = account.OfficeUID.ToString();
                    else
                        rackspaceUId = uid;
                }
            }
            else
                rackspaceUId = Sessions.SwitchedRackspaceId;
            renameFile = Common_Tatva.SubStringFilename(renameFile);
            if (renameFile == oldFileName)
                return errorMsg;
            if (renameFile.LastIndexOf('.') <= 0)
                renameFile = renameFile + ".pdf";

            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();

            string oldObjectPath = rackSpaceFileUpload.GeneratePath(oldFileName.ToLower(), Enum_Tatva.Folders.EdForms, departmentId);
            string newName = rackSpaceFileUpload.RenameObject(ref errorMsg, oldObjectPath, renameFile, Enum_Tatva.Folders.EdForms, rackspaceUId);
            if (string.IsNullOrEmpty(errorMsg))
            {
                if (string.IsNullOrEmpty(edFormsId))
                    General_Class.UpdateEdFormsForUser(newName.Substring(newName.LastIndexOf('/') + 1), Convert.ToInt32(edFormsUserId), true);
                else
                    General_Class.UpdateEdFormsForAdmin(newName.Substring(newName.LastIndexOf('/') + 1), Convert.ToInt32(edFormsId), true);
                //InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Rename.GetHashCode(), 0, 0, Convert.ToInt32(edFormsId));
            }
        }
        catch (Exception ex)
        { }
        return errorMsg;
    }

    [WebMethod(EnableSession = true)]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string RenameDocumentForEdFormsShare(string renameFile, string edFormsShareId, string oldFileName)
    {
        string errorMsg = string.Empty;
        try
        {
            string rackspaceUId = string.Empty;
            if (string.IsNullOrEmpty(Sessions.SwitchedRackspaceId))
            {
                if (!string.IsNullOrEmpty(Sessions.SwitchedUserName))
                {
                    string uid = Membership.GetUser(Sessions.SwitchedUserName).ProviderUserKey.ToString();

                    Account account = new Account(uid);

                    if (account.IsSubUser.ToString() == "1")
                        rackspaceUId = account.OfficeUID.ToString();
                    else
                        rackspaceUId = uid;
                }
            }
            else
                rackspaceUId = Sessions.SwitchedRackspaceId;
            renameFile = Common_Tatva.SubStringFilename(renameFile);
            if (renameFile == oldFileName)
                return errorMsg;
            if (renameFile.LastIndexOf('.') <= 0)
                renameFile = renameFile + ".pdf";

            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();

            string oldObjectPath = rackSpaceFileUpload.GeneratePath(oldFileName.ToLower(), Enum_Tatva.Folders.EdFormsShare, edFormsShareId.ToLower());
            string newName = rackSpaceFileUpload.RenameObject(ref errorMsg, oldObjectPath, renameFile, Enum_Tatva.Folders.EdFormsShare, rackspaceUId);
            if (string.IsNullOrEmpty(errorMsg))
            {
                if (!string.IsNullOrEmpty(edFormsShareId))
                    General_Class.UpdateEdFormsForUserShare(newName.Substring(newName.LastIndexOf('/') + 1), edFormsShareId, true);
            }
        }
        catch (Exception ex)
        { }
        return errorMsg;
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string RenamePublicHoliday(string rename, int holidayId)
    {
        string errorMsg = string.Empty;
        try
        {
            int holidayID = Convert.ToInt32(holidayId);
            General_Class.UpdatePublicHoliday(rename.Trim(), holidayId);
        }
        catch (Exception ex)
        {
            errorMsg = ex.Message;
        }
        return errorMsg;
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string EditPublicHolidayDate(int holidayId, int yearOfHoliday, string holidDayDate)
    {
        string errorMsg = string.Empty;
        try
        {
            int holidayID = Convert.ToInt32(holidayId);
            holidDayDate = holidDayDate.Replace('-', '/');
            string month = holidDayDate.Substring(0, 2);
            string day = holidDayDate.Substring(3, 2);
            DateTime temp = DateTime.Parse(holidDayDate, System.Globalization.CultureInfo.InvariantCulture);
            DateTime dt = new DateTime(yearOfHoliday, int.Parse(month), int.Parse(day));
            General_Class.UpdatePublicHolidayDates(holidayID, yearOfHoliday, dt);
        }
        catch (Exception ex)
        {
            errorMsg = ex.Message;
        }
        return errorMsg;
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string RenameRequestDocument(string renameFile, string path, string inboxId, string eFileFlowShareId, string fileExtention, string type, string userId)
    {
        string errorMsg = string.Empty;
        //try
        //{
        //    RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
        //    if (type == "Inbox")
        //    {
        //        int inboxID = Convert.ToInt32(inboxId);

        //        if (renameFile.LastIndexOf('.') <= 0)
        //            renameFile = renameFile + fileExtention;

        //        string oldFileName = path.Substring(path.LastIndexOf('/') + 1);
        //        string oldObjectPath = rackSpaceFileUpload.GeneratePath(oldFileName.ToLower(), Enum_Tatva.Folders.inbox);
        //        string newName = rackSpaceFileUpload.RenameObject(ref errorMsg, oldObjectPath, renameFile, Enum_Tatva.Folders.inbox, userId);
        //        if (string.IsNullOrEmpty(errorMsg))
        //        {
        //            General_Class.UpdateInbox(newName.Substring(newName.LastIndexOf('/') + 1), inboxID, 0, 0, Enum_Tatva.IsDelete.No.GetHashCode(), 1, 0);
        //            InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Edit.GetHashCode(), 0, 0, 0, 0, null, inboxID);
        //        }
        //    }
        //    else if (type == "Pending")
        //    {
        //        Guid eFileFlowShareID = new Guid(eFileFlowShareId);

        //        if (renameFile.LastIndexOf('.') <= 0)
        //            renameFile = renameFile + fileExtention;

        //        string oldFileName = path.Substring(path.LastIndexOf('/') + 1);
        //        string oldObjectPath = rackSpaceFileUpload.GeneratePath(oldFileName.ToLower(), Enum_Tatva.Folders.EFileShare, eFileFlowShareId);

        //        string newName = rackSpaceFileUpload.RenameObject(ref errorMsg, oldObjectPath, renameFile, Enum_Tatva.Folders.EFileShare, userId);
        //        if (string.IsNullOrEmpty(errorMsg))
        //        {
        //            General_Class.UpdateShare(newName.Substring(newName.LastIndexOf('/') + 1), eFileFlowShareID, 0, 0, Enum_Tatva.IsDelete.No.GetHashCode(), 1, 0);
        //            InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Edit.GetHashCode(), 0, 0, 0, 0, eFileFlowShareId);
        //        }
        //    }

        //}
        try
        {
            renameFile = Common_Tatva.SubStringFilename(renameFile);
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            if (type == "Inbox")
            {
                int inboxID = Convert.ToInt32(inboxId);

                if (renameFile.LastIndexOf('.') <= 0)
                    renameFile = renameFile + fileExtention;


                string oldFileName = path.Substring(path.LastIndexOf('/') + 1);
                if (renameFile == oldFileName)
                    return errorMsg;
                string oldObjectPath = rackSpaceFileUpload.GeneratePath(oldFileName.ToLower(), Enum_Tatva.Folders.inbox);
                string newName = rackSpaceFileUpload.RenameObject(ref errorMsg, oldObjectPath, renameFile, Enum_Tatva.Folders.inbox, userId);
                if (string.IsNullOrEmpty(errorMsg))
                {
                    General_Class.UpdateInbox(newName.Substring(newName.LastIndexOf('/') + 1), inboxID, 0, 0, Enum_Tatva.IsDelete.No.GetHashCode(), 1, 0);
                    InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Edit.GetHashCode(), 0, 0, 0, 0, null, inboxID);
                }
            }
            else if (type == "Pending")
            {
                Guid eFileFlowShareID = new Guid(eFileFlowShareId);

                if (renameFile.LastIndexOf('.') <= 0)
                    renameFile = renameFile + fileExtention;

                string oldFileName = path.Substring(path.LastIndexOf('/') + 1);
                if (renameFile == oldFileName)
                    return errorMsg;
                string oldObjectPath = rackSpaceFileUpload.GeneratePath(oldFileName.ToLower(), Enum_Tatva.Folders.EFileShare, eFileFlowShareId);

                string newName = rackSpaceFileUpload.RenameObject(ref errorMsg, oldObjectPath, renameFile, Enum_Tatva.Folders.EFileShare, userId);
                if (string.IsNullOrEmpty(errorMsg))
                {
                    General_Class.UpdateShare(newName.Substring(newName.LastIndexOf('/') + 1), eFileFlowShareID, 0, 0, Enum_Tatva.IsDelete.No.GetHashCode(), 1, 0);
                    InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Edit.GetHashCode(), 0, 0, 0, 0, eFileFlowShareId);
                }
            }

        }
        catch (Exception ex)
        { }
        return errorMsg;
    }

    [WebMethod(EnableSession = true)]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public bool ShareMailForEdFileShare(string strToMail, string strSendMail, int documentId, string mailTitle, string strMailInfor, string sendName, string newPdfName, decimal size, Guid edFileShareId)
    {
        bool success = true;
        try
        {
            Guid uid = new Guid(Sessions.SwitchedSessionId);
            //Guid uid = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            var arrEmailName = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(strToMail).ToArray();
            for (int i = 0; i < arrEmailName.Length; i++)
            {
                General_Class.InsertIntoEdFileShare(edFileShareId, documentId, newPdfName, arrEmailName[i]["Email"], mailTitle, size);
                int MailID = FlashViewer.FlashViewer_ShareMail(strSendMail, arrEmailName[i]["Email"], documentId, mailTitle, strMailInfor);

                int shareID = General_Class.SaveShareLink(uid, Enum_Tatva.Folders.inbox.GetHashCode(), documentId, null, null, null, null);

                FlashViewer.HtmlViewer_SendSplitShareMail(arrEmailName[i]["ToName"], sendName, documentId, strSendMail, mailTitle, strMailInfor, arrEmailName[i]["Email"], edFileShareId, shareID);
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(documentId.ToString(), string.Empty, newPdfName, ex);
            success = false;
        }
        return success;
    }

    [WebMethod(EnableSession = true)]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ShareMailForeFileFlow(string eFileFlowId, string FileName, string strToMail, string MailTitle, string ToName, string SendName, string strSendMail, string strMailInfor, string pathName, string userId)
    {
        Guid uid = new Guid(Sessions.SwitchedSessionId);
        //Guid uid = new Guid(Membership.GetUser().ProviderUserKey.ToString());
        string retErrorMsg = string.Empty;
        try
        {
            DataTable dt = new System.Data.DataTable();
            string errorMsg = string.Empty;
            var arrEmailName = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(strToMail).ToArray();

            string[] toEmails = new string[] { };
            string[] eFileFlowIDs = eFileFlowId.Split(',');

            DataTable eFileFlowDocs = General_Class.GeteFileFlowDocs(userId);
            string emails = string.Empty;

            for (int i = 0; i < arrEmailName.Length; i++)
            {
                if (arrEmailName[i]["Email"] == "All Folders")
                    dt = General_Class.GetEdFormsEmail(Sessions.SwitchedRackspaceId, null, null, null, true);
                else if ((arrEmailName[i]["Email"]).Contains("("))
                {
                    if ((arrEmailName[i]["Email"]).Split('(', ')')[1] == "Alert")
                        dt = General_Class.GetEdFormsEmail(Sessions.SwitchedRackspaceId, (arrEmailName[0]["Email"]).Substring(0, (arrEmailName[0]["Email"]).LastIndexOf('(')), null);
                    else if ((arrEmailName[i]["Email"]).Split('(', ')')[1] == " Status")
                        dt = General_Class.GetEdFormsEmail(Sessions.SwitchedRackspaceId, null, (arrEmailName[0]["Email"]).Substring(0, (arrEmailName[0]["Email"]).LastIndexOf('(')));
                }
                else
                    emails = arrEmailName[i]["Email"] + ",";
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        emails = emails + "," + Convert.ToString(dr["Email"]);
                    }
                    //for (int a = 0; a < dt.Rows.Count; a++)
                    //{
                    //    emails = emails + "," + dt.Rows[a][0].ToString();
                    //}
                }
                toEmails = emails.Split(',');
                foreach (string emailDetail in toEmails)
                {
                    if (emailDetail == "")
                        continue;
                    List<Tuple<string, string>> ELink = new List<Tuple<string, string>>();
                    foreach (string eFileId in eFileFlowIDs)
                    {
                        Guid guid = Guid.NewGuid();
                        int eFileFlowID = Convert.ToInt32(eFileId);
                        string eFileFlowShareId = Convert.ToString(guid);
                        string extname = "pdf";
                        var results = from myRow in eFileFlowDocs.AsEnumerable() where myRow.Field<int>("eFileFlowId") == eFileFlowID select myRow;

                        if (results != null && results.CopyToDataTable().Rows.Count > 0)
                        {
                            int shareId = 0;
                            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                            string uploadObject = string.Empty;

                            FileName = results.CopyToDataTable().Rows[0].Field<string>("Name");

                            if (FileName.IndexOf('.') > 0)
                                uploadObject = rackSpaceFileUpload.UploadObject(ref errorMsg, FileName.ToLower(), Enum_Tatva.Folders.EFileShare, null, eFileFlowShareId, userId);
                            else
                                uploadObject = rackSpaceFileUpload.UploadObject(ref errorMsg, FileName.ToLower() + "." + extname, Enum_Tatva.Folders.EFileShare, null, eFileFlowShareId, userId);

                            if (string.IsNullOrEmpty(errorMsg))
                            {
                                decimal size = Math.Round((decimal)(rackSpaceFileUpload.GetSizeOfFile(ref errorMsg, uploadObject, userId) / 1024) / 1024, 2);
                                if (string.IsNullOrEmpty(errorMsg))
                                    shareId = General_Class.SaveShareLink(uid, Enum_Tatva.Folders.EFileFlow.GetHashCode(), null, null, null, eFileFlowID, null);
                                General_Class.eFileFlowShareMail(guid, eFileFlowID, FileName, emailDetail, MailTitle, Convert.ToInt32(Enum_Tatva.Status.Send.GetHashCode()), Enum_Tatva.IsDelete.No.GetHashCode(), size);
                                InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Share.GetHashCode(), 0, 0, Convert.ToInt32(eFileId));
                            }

                            if (!string.IsNullOrEmpty(errorMsg))
                            {
                                if (retErrorMsg.IndexOf("Failed to share Document(s) : ") < 0)
                                    retErrorMsg = "Failed to share Document(s) : ";

                                if (retErrorMsg.IndexOf(FileName.ToLower()) < 0)
                                    retErrorMsg += FileName.ToLower() + ", ";
                                errorMsg = string.Empty;
                            }
                            else
                                ELink.Add(Tuple.Create(HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("EFileFolderJSONService.asmx")) + "Viewer.aspx?data=" + eFileFlowShareId + "&created=" + userId + "&id=" + QueryString.QueryStringEncode(shareId.ToString()), FileName));
                        }
                    }
                    FlashViewer.HtmlViewer_SendShareMailForeFileFlow(arrEmailName[i]["ToName"], SendName, strSendMail, MailTitle, strMailInfor, emailDetail, ELink);
                }
                toEmails = new string[] { };
            }
            if (retErrorMsg.IndexOf(',') > 0)
                return retErrorMsg.Substring(0, retErrorMsg.LastIndexOf(','));
            else
                return retErrorMsg;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            return "Failed to share Document(s)!";
        }
    }

    [WebMethod(EnableSession = true)]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ShareMailForEdForms(string edFormsUserList, string strToMail, string MailTitle, string ToName, string SendName, string strSendMail, string strMailInfor, string pathName, string departmentId, string usersList)
    {
        NLogLogger _logger = new NLogLogger();
        string userId = Sessions.SwitchedRackspaceId;
        Guid uid = new Guid(Sessions.SwitchedSessionId);
        string retErrorMsg = string.Empty;
        try
        {
            DataTable dtEdFormsUser = new DataTable();
            dtEdFormsUser.Columns.Add("Index", typeof(int));
            dtEdFormsUser.Columns.Add("EdFormsUserId", typeof(int));
            dtEdFormsUser.Columns.Add("FileFolderId", typeof(int));
            dtEdFormsUser.Columns.Add("EdFormsUserShareId", typeof(string));
            string errorMsg = string.Empty;
            Dictionary<string, string>[] arrEmailName = null;
            Dictionary<string, string>[] edFormsUsers = null;
            AssignedUser[] users = null;
            try
            {
                arrEmailName = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(strToMail).ToArray();
                edFormsUsers = JsonConvert.DeserializeObject<List<Dictionary<string, string>>>(edFormsUserList).ToArray();
                users = JsonConvert.DeserializeObject<List<AssignedUser>>(usersList).ToArray();
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            DataTable dtUsers = new DataTable();
            dtUsers.Columns.Add("Index", typeof(int));
            dtUsers.Columns.Add("UID", typeof(string));
            int userIndex = 1;
            foreach (var user in users)
            {
                dtUsers.Rows.Add(userIndex, user.UID.ToString());
                userIndex++;
            }

            string emails = string.Empty;
            int index = 1;

            for (int i = 0; i < arrEmailName.Length; i++)
            {
                DataTable dt = new System.Data.DataTable();
                List<Tuple<int?, string, string>> folderList = new List<Tuple<int?, string, string>>();
                if (arrEmailName[i]["Email"] == "All Folders")
                    dt = General_Class.GetEdFormsEmail(userId, null, null, null, true);
                else if ((arrEmailName[i]["Email"]).Contains("("))
                {
                    if ((arrEmailName[i]["Email"]).Split('(', ')')[1] == "Alert")
                        dt = General_Class.GetEdFormsEmail(userId, (arrEmailName[i]["Email"]).Substring(0, (arrEmailName[i]["Email"]).LastIndexOf('(')));
                    else if ((arrEmailName[i]["Email"]).Split('(', ')')[1] == "Status")
                        dt = General_Class.GetEdFormsEmail(userId, null, (arrEmailName[i]["Email"]).Substring(0, (arrEmailName[i]["Email"]).LastIndexOf('(')));
                }
                else
                {
                    dt = General_Class.GetEdFormsEmail(userId, null, null, arrEmailName[i]["Email"]);
                    if (dt == null)
                    {
                        dt.Columns.Add("Email", typeof(string));
                        dt.Columns.Add("FolderID", typeof(int));
                        dt.Columns.Add("FolderName", typeof(string));
                        dt.Rows.Add(arrEmailName[i]["Email"], arrEmailName[i]["ToName"]);
                    }
                    else if (dt != null && dt.Rows.Count < 1)
                    {
                        dt.Rows.Add(dt.NewRow());
                        dt.Rows[0]["Email"] = arrEmailName[i]["Email"];
                        dt.Rows[0]["FolderID"] = DBNull.Value;
                        dt.Rows[0]["FolderName"] = arrEmailName[i]["ToName"];
                    }
                }
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        int? folderId = string.IsNullOrEmpty(Convert.ToString(dr["FolderID"])) ? (int?)null : Convert.ToInt32(dr["FolderID"]);
                        string email = Convert.ToString(dr["Email"]);
                        string name = Convert.ToString(dr["FolderName"]);
                        if (!folderList.Any(m => m.Item1 == folderId) && !string.IsNullOrEmpty(email))
                        {
                            var item = new Tuple<int?, string, string>(folderId, email, name);
                            folderList.Add(item);
                        }
                    }
                }
                foreach (var emailDetail in folderList)
                {
                    List<Tuple<string, string>> ELink = new List<Tuple<string, string>>();
                    foreach (var edForms in edFormsUsers)
                    {
                        try
                        {
                            Guid guid = Guid.NewGuid();
                            int id = Convert.ToInt32(edForms["EdFormsUserId"]);
                            string edFormsShareId = Convert.ToString(guid);
                            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                            string uploadObject = string.Empty;
                            string fileName = edForms["FileName"];
                            uploadObject = rackSpaceFileUpload.UploadObject(ref errorMsg, fileName.ToLower(), Enum_Tatva.Folders.EdFormsShare, null, edFormsShareId, userId, departmentId);

                            if (string.IsNullOrEmpty(errorMsg))
                            {
                                if (emailDetail.Item1 == null || !dtEdFormsUser.AsEnumerable().Any(a => a.Field<int>("EdFormsUserId") == id && a.Field<int?>("FileFolderId") == emailDetail.Item1))
                                    dtEdFormsUser.Rows.Add(index, id, emailDetail.Item1, edFormsShareId);
                                General_Class.DocumentInsertForEdFormsShare(edFormsShareId, id, emailDetail.Item2, MailTitle, (int)Enum_Tatva.EdFormsStatus.UnOpen, fileName, Sessions.SwitchedSessionId);
                                string IP = string.Empty;
                                try
                                {
                                    IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                                }
                                catch (Exception ex) { }
                                General_Class.InsertIntoEdFormsUserLogs(edFormsShareId, (byte)Enum_Tatva.EdFormsLog.Share, IP);
                                General_Class.InsertIntoEdFormsUserReminder(dtUsers, edFormsShareId);
                                index++;
                                decimal size = Math.Round((decimal)(rackSpaceFileUpload.GetSizeOfFile(ref errorMsg, uploadObject, userId) / 1024) / 1024, 2);
                                InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Share.GetHashCode(), 0, 0, 0, 0, null, 0, 0, null, 0, id);
                            }

                            if (!string.IsNullOrEmpty(errorMsg))
                            {
                                if (retErrorMsg.IndexOf("Failed to share Document(s) : ") < 0)
                                    retErrorMsg = "Failed to share Document(s) : ";

                                if (retErrorMsg.IndexOf(fileName.ToLower()) < 0)
                                    retErrorMsg += fileName.ToLower() + ", ";
                                errorMsg = string.Empty;
                            }
                            else
                                ELink.Add(Tuple.Create(HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("EFileFolderJSONService.asmx")) + "EdFormsViewer.aspx?data=" + QueryString.QueryStringEncode(edFormsShareId), fileName));
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                        }
                    }
                    try
                    {
                        FlashViewer.HtmlViewer_SendShareMailForEdForms(emailDetail.Item3, SendName, strSendMail, MailTitle, strMailInfor, emailDetail.Item2, ELink);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                    }
                }
            }
            try
            {
                General_Class.DocumentsInsertForEdFormsUserFolder(dtEdFormsUser, uid);
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            if (retErrorMsg.IndexOf(',') > 0)
                return retErrorMsg.Substring(0, retErrorMsg.LastIndexOf(','));
            else
                return retErrorMsg;
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            return "Failed to share Document(s)!";
        }
    }

    public bool NotifyBackForEdForms(string edFormsUserShareId, string fileName, string toEmail, string toName, string message, string senderMail)
    {
        try
        {
            var url = HttpContext.Current.Request.Url.AbsoluteUri;
            var eLink = Tuple.Create(url.Substring(0, url.IndexOf("Office")) + "EdFormsViewer.aspx?data=" + QueryString.QueryStringEncode(edFormsUserShareId), fileName);
            string IP = string.Empty;
            try
            {
                IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            catch (Exception ex) { }
            General_Class.InsertIntoEdFormsUserLogs(edFormsUserShareId, (byte)Enum_Tatva.EdFormsLog.Share, IP);
            General_Class.ChangeEdFormsUserShareStatus(edFormsUserShareId, (int)Enum_Tatva.EdFormsStatus.UnOpen);
            FlashViewer.HtmlViewer_SendShareMailForEdFormsNotifyBack(toName, string.Empty, senderMail, string.Empty, message, toEmail, eLink);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ShareMailForInbox(string strToMail, string MailTitle, string ToName, string SendName, string strSendMail, string strMailInfor)
    {
        return FlashViewer.HtmlViewer_SendShareMailForInbox(ToName, SendName, strSendMail, MailTitle, strMailInfor, strToMail);
    }

    [WebMethod(EnableSession = true)]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void ShareMailForWorkArea(string strToMail, string MailTitle, string ToName, string SendName, string strSendMail, string strMailInfor, string URL, string fileName, int workAreaId)
    {
        int mailID = 0;
        DataTable workArea = (DataTable)this.Session["WorkArea_DataSource"];
        Guid uid = new Guid(Sessions.SwitchedSessionId);
        URL = URL.Replace("WorkArea", "workArea");
        int shareID = General_Class.SaveShareLink(uid, Enum_Tatva.Folders.WorkArea.GetHashCode(), null, null, workAreaId, null, null);
        mailID = FlashViewer.FlashViewer_ShareMail(strSendMail, strToMail, 0, MailTitle, strMailInfor, true);
        string[] split = URL.Split(new string[] { "Data=" }, StringSplitOptions.None);
        string strMailId = split[0] + "MailId=" + mailID;
        string share = QueryString.QueryStringDecode(split[1]);
        share += "&shareId=" + shareID;
        string newUrl = strMailId + "&Data=" + QueryString.QueryStringEncode(share);
        FlashViewer.HtmlViewer_SendShareMailForWorkArea(ToName, SendName, strSendMail, MailTitle, strMailInfor, strToMail, newUrl);
        InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Share.GetHashCode(), 0, 0, 0, workAreaId);
    }

    [WebMethod(EnableSession = true)]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void ShareMailForArchive(string strToMail, string MailTitle, string ToName, string SendName, string strSendMail, string strMailInfor, string URL, int archiveId)
    {
        //int? archiveId = 0;

        //archiveId = Common_Tatva.GetWorkAreaId(fileName);
        //Guid uid = new Guid(Sessions.SwitchedSessionId);
        ////Guid uid = new Guid(Sessions.UserId);
        //int shareID = General_Class.SaveShareLink(uid, Enum_Tatva.Folders.Archive.GetHashCode(), null, null, null, null, archiveId);
        //string[] split = URL.Split(new string[] { "Data=" }, StringSplitOptions.None);
        //string share = QueryString.QueryStringDecode(split[1]);
        //share += "&shareId=" + shareID;
        //string newUrl = split[0] + "&Data=" + QueryString.QueryStringEncode(share);
        //FlashViewer.HtmlViewer_SendShareMailForArchive(ToName, SendName, strSendMail, MailTitle, strMailInfor, strToMail, newUrl);
        //if (archiveId > 0)
        //    InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Share.GetHashCode(), 0, 0, 0, 0, null, 0, archiveId);

        int mailID = 0;
        Guid uid = new Guid(Sessions.SwitchedSessionId);
        int shareID = General_Class.SaveShareLink(uid, Enum_Tatva.Folders.Archive.GetHashCode(), null, null, null, null, archiveId);
        mailID = FlashViewer.FlashViewer_ShareMail(strSendMail, strToMail, 0, MailTitle, strMailInfor, true);
        string[] split = URL.Split(new string[] { "Data=" }, StringSplitOptions.None);
        string strMailId = split[0] + "MailId=" + mailID;
        string share = QueryString.QueryStringDecode(split[1]);
        share += "&shareId=" + shareID;
        string newUrl = strMailId + "&Data=" + QueryString.QueryStringEncode(share);
        FlashViewer.HtmlViewer_SendShareMailForArchive(ToName, SendName, strSendMail, MailTitle, strMailInfor, strToMail, newUrl);
        if (archiveId > 0)
            InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Share.GetHashCode(), 0, 0, 0, 0, null, 0, archiveId);
    }

    [WebMethod(EnableSession = true)]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public void ShareMailForComplianceTest(string orgName, string phoneNumber, string email, int step1, int step2, int step3)
    {
        FlashViewer.HtmlViewer_SendComplianceMail(orgName, phoneNumber, email, step1, step2, step3);
    }

    #region ElasticUpload

    /// <summary>
    /// Get Data From cloud then parse it and ipload to elastic server
    /// </summary>
    /// <returns></returns> 
    [WebMethod]
    public void GetArchiveForBatchUpload(string uid)
    {
        RackSpaceFileUpload rackspacefileupload = new RackSpaceFileUpload();
        NLogLogger _logger = new NLogLogger();
        string errorMsg = string.Empty;
        DataTable dtArchive = General_Class.GetArchiveByUID(uid);
        try
        {
            if (dtArchive != null && dtArchive.Rows.Count > 0)
            {
                foreach (DataRow dr in dtArchive.Rows)
                {
                    int archiveId = 0, folderId = 0;
                    string fileName = string.Empty, pathName = string.Empty;
                    try
                    {
                        try
                        {
                            archiveId = Convert.ToInt32(dr["ArchiveId"]);
                            folderId = Convert.ToInt32(dr["ArchiveTreeId"]);
                            fileName = Convert.ToString(dr["FileName"]);
                            pathName = Convert.ToString(dr["PathName"]);
                        }
                        catch (Exception ex)
                        {
                            _logger.Info(ex);
                        }
                        string newFileName = rackspacefileupload.GetObject(ref errorMsg, pathName, uid, (int)Enum_Tatva.Folders.Archive);
                        if (!string.IsNullOrEmpty(newFileName) && string.IsNullOrEmpty(errorMsg))
                        {
                            try
                            {
                                string physicalPath = System.Web.Hosting.HostingEnvironment.MapPath("~/" + Common_Tatva.RackSpaceArchiveDownload + Path.DirectorySeparatorChar + uid + Path.DirectorySeparatorChar) + newFileName;
                                byte[] dataBytes = Convert.FromBase64String(Convert.ToBase64String(File.ReadAllBytes(physicalPath)));
                                Stream stream = new MemoryStream(dataBytes);
                                float size = (float)Math.Round((decimal)(stream.Length / 1024) / 1024, 2);

                                string pdfText = PDFParser.ExtractTextFromPdfImages(fileName, stream);
                                var archive = new Archive()
                                {
                                    ArchiveId = archiveId,
                                    FolderId = folderId,
                                    FileName = fileName,
                                    PathName = pathName,
                                    Size = size,
                                    JsonData = pdfText
                                };
                                var createResponse = highClient.Index(archive, idx => idx.Index(elasticIndexName).Id(archiveId));
                                if (createResponse.IsValid)
                                    General_Class.InsertIntoArchiveLog(archive.ArchiveId, true);
                                else
                                    General_Class.InsertIntoArchiveLog(archive.ArchiveId, false);
                                _logger.Info("Archive Uploaded at " + DateTime.Now + " for ArchiveId " + archive.ArchiveId);
                                try
                                {
                                    if (File.Exists(physicalPath))
                                    {
                                        File.Delete(physicalPath);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    _logger.Info(ex);
                                }
                            }
                            catch (Exception ex)
                            {
                                General_Class.InsertIntoArchiveLog(archiveId, false);
                                _logger.Error(ex);
                            }
                        }
                        errorMsg = string.Empty;
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    [WebMethod]
    /// <summary>
    /// Get All the Office User
    /// </summary>
    /// <returns></returns>
    public DataTable GetAllUsers()
    {
        NLogLogger _logger = new NLogLogger();
        DataTable dtUsers = General_Class.GetAllUsers();
        dtUsers.TableName = "TableUser";
        //if (dtUsers != null && dtUsers.Rows.Count > 0)
        //{
        //    foreach (DataRow dr in dtUsers.Rows)
        //    {
        //        try
        //        {
        //            users.Add(dr["UID"].ToString(), dr["Name"].ToString());
        //        }
        //        catch (Exception ex)
        //        {
        //            _logger.Error(ex);
        //        }
        //    }
        //}
        return dtUsers;
    }
    #endregion


    #region BatchUpload
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string CheckFolderIdExists(string folderName, string uid)
    {
        return JsonConvert.SerializeObject(General_Class.CheckFolderIdExists(folderName, uid));
    }

    [WebMethod]
    public string CreateDivider(string dividerName, string uid, int effId, string color)
    {
        return General_Class.CreateDivider(dividerName, uid, effId, color);
    }

    ///// <summary>
    ///// Check if folder exists 
    ///// </summary>
    ///// <param name="uid"></param>
    ///// <param name="folderName"></param>
    ///// <param name="fileNumber"></param>
    ///// <param name="folderID"></param>
    ///// <returns></returns>
    //[WebMethod]
    //public bool CheckIfFolderExist(string uid, string folderName, string fileNumber, int folderID = 0)
    //{
    //    bool IsExist = FileFolderManagement.IsFolderNameExist(uid, folderName, fileNumber, folderID);
    //    return IsExist;
    //}

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string IsSameFolderExist(string uid, string folderName, string fileNumber, bool isBatch = false)
    {
        return JsonConvert.SerializeObject(FileFolderManagement.IsFolderNameExist(uid, folderName, fileNumber, 0, isBatch));
    }

    [WebMethod]
    public DataSet CreateFolder(bool isCheck, string folderName, string uid, string firstName, string lastName, int otherInfo, string fileNumber, string alert, string templateName,
        string address = "", string city = "", string postal = "", string tel = "", string ssn = "", string email = "", string alert2 = "", string comments = "", string officeId = null, bool isBatch = false)
    {
        return General_Class.CreateFileFolder(isCheck, isBatch, folderName, uid, string.IsNullOrEmpty(officeId) ? uid : officeId, firstName, lastName, otherInfo, fileNumber, alert, templateName,
            string.IsNullOrEmpty(address) ? null : address,
            string.IsNullOrEmpty(city) ? null : city,
            string.IsNullOrEmpty(postal) ? null : postal,
            string.IsNullOrEmpty(tel) ? null : tel,
            string.IsNullOrEmpty(ssn) ? null : ssn,
            string.IsNullOrEmpty(email) ? null : email,
            string.IsNullOrEmpty(alert2) ? null : alert2,
            string.IsNullOrEmpty(comments) ? null : comments);
    }

    [WebMethod]
    public DataTable GetOFFiceID(string subUserId)
    {
        DataTable dt = UserManagement.GetOfficeUserIDBySubUserID(subUserId);
        if (dt != null)
            dt.TableName = "OfficeDetail";
        return dt;
    }

    [WebMethod]
    public string ProcessFile(int folderId, int dividerIdForFile, string folderName, string uid, string basePath, string base64Str, string newFileName, long Offset)
    {
        string documentId = string.Empty;
        try
        {

            WorkItem item = new WorkItem();
            byte[] fileStream = Convert.FromBase64String(base64Str);
            string tempPath = HttpContext.Current.Server.MapPath("~/" + CurrentVolume);
            item.MachinePath = string.Concat(tempPath, Path.DirectorySeparatorChar, folderId,
            Path.DirectorySeparatorChar, dividerIdForFile);
            if (!Directory.Exists(item.MachinePath))
            {
                Directory.CreateDirectory(item.MachinePath);
            }

            string encodeName = HttpUtility.UrlPathEncode(Path.GetFileName(newFileName));
            encodeName = encodeName.Replace("%", "$");
            string source = string.Concat(item.MachinePath, Path.DirectorySeparatorChar, encodeName);
            if (Offset == 0)
            { // new file, create an empty file
                source = GetRenameFileName(source);
                File.Create(source).Close();
            }
            using (FileStream fs = new FileStream(source, FileMode.Open, FileAccess.ReadWrite, FileShare.Read))
            {
                fs.Seek(Offset, SeekOrigin.Begin);
                fs.Write(fileStream, 0, fileStream.Length);
            }
            return source;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(Convert.ToString(folderId), Convert.ToString(dividerIdForFile), "ProcessFile()", ex);
        }
        return string.Empty;
    }

    private string GetRenameFileName(string path)
    {
        try
        {
            if (File.Exists(path))
            {
                string fileName = Path.GetFileNameWithoutExtension(path);
                string fileExtension = Path.GetExtension(path);
                string fileDirectory = Path.GetDirectoryName(path);
                int i = 1;
                while (true)
                {
                    string renameFileName = fileDirectory + @"\" + fileName + "_v" + i + fileExtension;
                    if (File.Exists(renameFileName))
                        i++;
                    else
                    {
                        path = renameFileName;
                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            NLogLogger _logger = new NLogLogger();
            _logger.Info(ex.Message, ex);
        }
        return path;
    }

    [WebMethod]
    public string InsertDocument(int dividerId, int folderId, string newFileName, string encodeName, string uid)
    {
        try
        {
            string newPath = CurrentVolume + Path.DirectorySeparatorChar +
                            folderId + Path.DirectorySeparatorChar + dividerId + Path.DirectorySeparatorChar + encodeName;
            string workItemPath = CurrentVolume + Path.DirectorySeparatorChar +
                            folderId + Path.DirectorySeparatorChar + dividerId + Path.DirectorySeparatorChar + newFileName;
            string displayFileName = Path.GetFileNameWithoutExtension(newFileName);
            string newDocument = Common_Tatva.InsertDocument(dividerId, newPath, 0, displayFileName, Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()), uid, workItemPath);
            if (!string.IsNullOrEmpty(newDocument))
            {
                int docId = Convert.ToInt32(newDocument.Split('#')[0].Trim());
                InsertDocumentsAndFolderLogs(folderId, dividerId, docId, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.BatchUpload.GetHashCode(), 0, 0);
            }
            return newDocument;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
    }

    [WebMethod]
    public int InsertDocumentArchive(string filename, string folderId, string uid, Stream stream = null, string base64String = null, bool isBatchUpload = false)
    {
        try
        {
            string errorMsg = string.Empty;
            string newFilename = string.Empty;
            RackSpaceFileUpload rackspacefileupload = new RackSpaceFileUpload();
            if (stream == null)
            {
                byte[] dataBytes = Convert.FromBase64String(base64String);
                stream = new MemoryStream(dataBytes);
            }
            float size = (float)Math.Round((decimal)(stream.Length / 1024) / 1024, 2);
            if (filename.IndexOf('/') > 0)
                newFilename = filename.Substring(filename.LastIndexOf('\\') + 1);
            else
                newFilename = filename;
            string newPath = rackspacefileupload.UploadObject(ref errorMsg, newFilename, Enum_Tatva.Folders.Archive, stream, folderId, uid);
            int archiveId = 0;
            if (string.IsNullOrEmpty(errorMsg))
            {
                archiveId = General_Class.DocumentInsertForArchive(newPath.Substring(newPath.LastIndexOf('/') + 1), newPath, Convert.ToInt32(folderId.Substring(folderId.LastIndexOf('/') + 1)), size, isBatchUpload);
                if (archiveId > 0)
                {
                    string pdfText = PDFParser.ExtractTextFromPdfImages(filename, stream);
                    var archive = new Archive()
                    {
                        ArchiveId = archiveId,
                        FolderId = Convert.ToInt32(folderId.Substring(folderId.LastIndexOf('/') + 1)),
                        FileName = newPath.Substring(newPath.LastIndexOf('/') + 1),
                        PathName = newPath,
                        Size = size,
                        JsonData = pdfText
                    };
                    var createResponse = highClient.Index(archive, idx => idx.Index(elasticIndexName).Id(archive.ArchiveId));
                    if (createResponse.IsValid)
                        Common_Tatva.WriteElasticResponse(archiveId.ToString(), archive.FolderId.ToString(), archive.FileName, "ResponseId : " + createResponse.Id + " Result : " + createResponse.Result + " IsValid : " + createResponse.IsValid);
                    else
                        Common_Tatva.WriteElasticResponse(archiveId.ToString(), archive.FolderId.ToString(), archive.FileName, "ResponseId : " + createResponse.Id + " Result : " + createResponse.Result + " IsValid : " + createResponse.IsValid + " Response : " + createResponse.DebugInformation);
                }
            }
            return archiveId;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(folderId, ex.Message, filename, ex);
            return 0;
        }
    }

    /// <summary>
    /// Update the document in elastic server
    /// </summary>
    /// <param name="archiveId"></param>
    /// <param name="fileName"></param>
    /// <param name="pathName"></param>
    /// <param name="folderId"></param>
    /// <param name="isDeleted"></param>
    /// <returns></returns>
    public bool UpdateArchiveInElastic(string archiveId, string fileName, string pathName, string folderId, bool isDeleted = false)
    {
        try
        {
            var response = highClient.Update<Archive, Archive>(archiveId, d => d
                    .Index(elasticIndexName)
                    .Doc(new Archive
                    {
                        ArchiveId = Convert.ToInt32(archiveId),
                        FileName = fileName,
                        PathName = pathName,
                        FolderId = Convert.ToInt32(folderId),
                        IsDeleted = isDeleted
                    }));
            if (response.IsValid)
                return true;
            else
            {
                Common_Tatva.WriteElasticResponse(archiveId, folderId, fileName, response.DebugInformation);
                return false;
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteElasticLog(archiveId, folderId, fileName, ex);
            return false;
        }
    }

    [WebMethod]
    public int InsertFolderArchiveTree(string FolderName, int? parentId, string uid, int level, string path)
    {
        try
        {
            int value = General_Class.DocumentInsertforArchiveTree(FolderName, parentId, uid, level, path);
            return value;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    [WebMethod]
    public int DeleteArchive(string oldPath, string uid, int archiveId, string folderId)
    {
        try
        {
            string errorMsg = string.Empty;
            RackSpaceFileUpload rackspacefileupload = new RackSpaceFileUpload();
            string newObjectPath = DeleteObjectFromCloud(oldPath, ref errorMsg, uid, Enum_Tatva.Folders.Archive);
            if (string.IsNullOrEmpty(errorMsg))
            {
                int value = General_Class.DeleteArchive(archiveId);
                var response = UpdateArchiveInElastic(archiveId.ToString(), Path.GetFileName(newObjectPath), newObjectPath, folderId, true);
                return value;
            }
            else
                return 0;

        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    [WebMethod]
    public string DeleteWorkArea(string oldPath, string uid, int workAreaId, string filename)
    {
        try
        {
            string errorMsg = string.Empty;
            string newPath = DeleteObjectFromCloud(oldPath, ref errorMsg, uid, Enum_Tatva.Folders.WorkArea);
            if (string.IsNullOrEmpty(errorMsg))
            {
                string value = General_Class.UpdateWorkArea(filename, Convert.ToInt32(workAreaId), 0, Enum_Tatva.IsDelete.Yes.GetHashCode(), 0, 0, oldPath);
                return value;
            }
            else
                return errorMsg;
        }
        catch (Exception ex)
        {
            return "Error: " + ex;
        }
    }

    public string DeleteEdForms(string oldPath, string uid, string fileName, ref string errorMsg, int edFormsId = 0, int edFormsUserId = 0)
    {
        try
        {
            string newPath = DeleteObjectFromCloud(oldPath, ref errorMsg, uid, Enum_Tatva.Folders.EdForms);
            if (string.IsNullOrEmpty(errorMsg))
            {
                string value = string.Empty;
                if (edFormsId > 0)
                    General_Class.UpdateEdFormsForAdmin(fileName, Convert.ToInt32(edFormsId), false, true);
                else
                    General_Class.UpdateEdFormsForUser(fileName, Convert.ToInt32(edFormsUserId), false, true);
                return value;
            }
            else
                return errorMsg;
        }
        catch (Exception ex)
        {
            return "Error: " + ex;
        }
    }

    /// <summary>
    /// Common function to move old object to deleted directoty in Cloud
    /// </summary>
    /// <param name="oldPath">Old Path</param>
    /// <param name="errorMsg">Error Message</param>
    /// <param name="uid">UID</param>
    /// <param name="folder">Folder Enum</param>
    /// <returns></returns>
    public string DeleteObjectFromCloud(string oldPath, ref string errorMsg, string uid, Enum_Tatva.Folders folder)
    {
        try
        {
            string newPath = Common_Tatva.RackspaceDeletedDirectory + oldPath;
            RackSpaceFileUpload rackspacefileupload = new RackSpaceFileUpload();
            string newObjectPath = rackspacefileupload.MoveObject(ref errorMsg, oldPath, newPath, folder, uid);
            return newObjectPath;
        }
        catch (Exception ex)
        {
            return "Error: " + ex;
        }
    }

    [WebMethod]
    public string CheckIfUserExists(string path)
    {
        try
        {
            string[] userName = path.Split('\\');
            DataTable dtUsers = General_Class.GetAllUsers();
            if (dtUsers != null)
            {
                foreach (string user in userName)
                {
                    for (int i = 0; i < dtUsers.Rows.Count; i++)
                    {
                        bool result = string.Equals(user, dtUsers.Rows[i]["Name"]);
                        if (result)
                            return user;
                        else
                            continue;
                    }
                }
            }
            return "";

        }
        catch (Exception ex)
        {
            return "";
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetUserIdFromUserName(string userName)
    {
        return JsonConvert.SerializeObject(General_Class.GetUserIdFromUserName(userName.Substring(userName.LastIndexOf('\\') + 1)));
    }

    [WebMethod]
    public string AuditLogByDocId(int documentId, string modifyBy, int action)
    {
        return General_Class.AuditLogByDocId(documentId, modifyBy, action);
    }

    #endregion

    [WebMethod(EnableSession = true)]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string InsertDocumentsAndFolderLogs(int folderId, int dividerId, int docId, int logFormId, int folderLogId, int action, int subAction, int type, int eFileFlowId = 0, int workAreaId = 0, string eFileFlowShareId = null, int inboxId = 0, int archiveId = 0, string officeId = null, int edFormsId = 0, int edFormsUserId = 0)
    {
        try
        {
            Guid uid;
            if (string.IsNullOrEmpty(Convert.ToString(officeId)))
                uid = (Guid)Membership.GetUser().ProviderUserKey;
            else
                uid = new Guid(officeId);
            Common_Tatva.WriteDocumentsAndFoldersLog(folderId, dividerId, docId, folderLogId, logFormId, action, "Entered in InsertDocumentsAndFolderLogs");
            string IP = HttpContext.Current.Request.UserHostAddress;
            return General_Class.CreateDocumentsAndFolderLogs(uid, folderId, folderLogId, docId, dividerId, logFormId, eFileFlowId, workAreaId, eFileFlowShareId, inboxId, action, subAction, type, archiveId, edFormsId, edFormsUserId, IP);
        }
        catch (Exception)
        {
            return string.Empty;
        }
    }

    [WebMethod(EnableSession = true)]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string InsertUserLogs(string accountId, int action)
    {
        Guid modifiedBy = (Guid)Membership.GetUser().ProviderUserKey;
        return General_Class.CreateUserLogs(modifiedBy, accountId, action);
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string InsertCasboLogs(int fileId, int action, string custId, string firstName, string lastName)
    {
        if (!string.IsNullOrEmpty(custId) && !string.IsNullOrEmpty(firstName) && !string.IsNullOrEmpty(lastName))
            return General_Class.InsertCasboLogs(custId, fileId, action, firstName, lastName);
        else
            return string.Empty;
    }

    [WebMethod(EnableSession = true)]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string RenameDocumentForSplitPdf(string renameFile, string path, string eFileSplitId, string oldFileName)
    {
        string errorMsg = string.Empty;
        try
        {
            int eFileSplitID = Convert.ToInt32(eFileSplitId);
            renameFile = Common_Tatva.SubStringFilename(renameFile);
            if (renameFile == oldFileName)
                return errorMsg;
            if (renameFile.LastIndexOf('.') <= 0)
                renameFile = renameFile + ".pdf";

            renameFile = renameFile.Replace("#", "");
            renameFile = renameFile.Replace("&", "");

            string uid = Sessions.SwitchedRackspaceId;
            //string uid = Membership.GetUser(Sessions.UserName).ProviderUserKey.ToString();
            string oldEncodedName = HttpUtility.UrlPathEncode(oldFileName);
            oldEncodedName = oldEncodedName.Replace("%", "$");
            path = HttpContext.Current.Server.MapPath("~/" + System.Configuration.ConfigurationManager.AppSettings["SplitPdf"] + Path.AltDirectorySeparatorChar
                + uid + Path.AltDirectorySeparatorChar + oldEncodedName);

            if (File.Exists(path))
            {
                FileInfo fileInfo = new FileInfo(path);

                string filePath = Path.GetDirectoryName(path);
                string encodeName = HttpUtility.UrlPathEncode(renameFile);
                encodeName = encodeName.Replace("%", "$");
                string newFile = string.Concat(filePath + Path.DirectorySeparatorChar + encodeName);
                File.Move(fileInfo.FullName, newFile);

            }
            else
                errorMsg = "File does not exist in given path";

            //No need to pass switch user
            if (string.IsNullOrEmpty(errorMsg))
                General_Class.UpdateFileSplit(renameFile.Substring(renameFile.LastIndexOf('/') + 1), eFileSplitID, Enum_Tatva.IsDelete.No.GetHashCode(), 1, 0, Sessions.UserId);
        }
        catch (Exception ex)
        { }
        return errorMsg;
    }

    /// <summary>
    /// CHeck valid EMail
    /// </summary>
    /// <param name="strEmail"></param>
    /// <param name="MailId"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public int IsValidEMail(string strEmail, int MailId)
    {
        try
        {
            int mailId = General_Class.CheckValidEMailByMailId(MailId, strEmail);
            return mailId;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    /// <summary>
    /// for saving shareLink logs.
    /// </summary>
    /// <param name="strName"></param>
    /// <param name="strEmail"></param>
    /// <param name="strMailInfor"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public int SaveShareLinkLog(string strName, string strEmail, string strMailInfor, string shareId, string strSharePersonMailId)
    {
        try
        {
            string IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            int logId = General_Class.SaveShareLinkLog(strName, strEmail, strMailInfor, Convert.ToInt32(shareId), IP);
            return logId;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    /// <summary>
    /// To Check whether the user logged in or not
    /// </summary>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public bool CheckUserLogin()
    {
        if (Membership.GetUser() != null && !string.IsNullOrEmpty(Sessions.SwitchedSessionId))
            return true;
        else
            return false;
    }

    /// <summary>
    /// To Check whether the user logged in or not
    /// </summary>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public object GetSesionPrivilege(int need)
    {
        if (Sessions.HasViewPrivilegeOnly)
            return new { hasViewPrivilegeOnly = true };
        else if (Sessions.HasPrevilegeToDownload)
            return new { hasPrivilegeToDownload = true };
        else
            return new { hasViewPrivilegeOnly = false, hasPrivilegeToDownload = false };
    }

    [WebMethod(EnableSession = true)]
    public void ClearFolderIdSource()
    {
        Session["FolderIdSource"] = null;
    }

    [WebMethod]
    public string CopyEdFilesByUsername(string userName)
    {
        NLogLogger _logger = new NLogLogger();
        _logger.Debug("Entered in CopyEdFilesByUsername");
        DataTable dt = new DataTable("Folder");
        try
        {
            DataTable uTable = UserManagement.getUserIDByUserName(userName);
            if (uTable.Rows.Count == 0) { return "User not exist"; };
            Account user = new Account(uTable.Rows[0][0].ToString());
            if (user.IsExist)
            {
                string officeID1 = string.Empty;

                if (user.RoleID.Value == "Offices")
                {
                    officeID1 = user.UID.Value;
                }
                //else if (user.RoleID.Value == "Users")
                //{
                //    officeID1 = user.OfficeUID.Value;
                //}
                dt = FileFolderManagement.GetFileFoldersInfor(officeID1);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ZipFile zipEntries = new ZipFile();
                    foreach (DataRow folder in dt.Rows)
                    {
                        string folderId = Convert.ToString(folder["FolderID"]);
                        string folderName = Convert.ToString(folder["FolderName"]);
                        string folderPath = System.Web.Hosting.HostingEnvironment.MapPath(string.Format("~/{0}/", CurrentVolume)) + folderId;
                        if (Directory.Exists(folderPath))
                        {
                            DataTable dtTabs = FileFolderManagement.GetDividerByEffid(Convert.ToInt32(folderId));
                            if (dtTabs != null && dtTabs.Rows.Count > 0)
                            {
                                foreach (string divider in Directory.GetDirectories(folderPath))
                                {
                                    try
                                    {
                                        string dividerId = divider.Substring(divider.LastIndexOf("\\") + 1);
                                        if (Directory.Exists(divider) && dividerId.ToLower() != "logform" && Convert.ToInt32(dividerId) > 0)
                                        {
                                            string dividerName = Convert.ToString(dtTabs.Select("DividerID = " + dividerId)[0]["Name"]);
                                            foreach (string fileName in Directory.GetFiles(divider))
                                            {
                                                string destFile = fileName.Replace(CurrentVolume, UserDataFolder + Path.DirectorySeparatorChar + userName)
                                                    .Replace(folderId, folderName)
                                                    .Replace(dividerId, dividerName);
                                                if (!Directory.Exists(destFile))
                                                {
                                                    Directory.CreateDirectory(destFile.Substring(0, destFile.LastIndexOf("\\")));
                                                }
                                                try
                                                {
                                                    File.Copy(fileName, destFile);
                                                }
                                                catch (Exception ex)
                                                {
                                                    _logger.Error("CopyEdFilesByUsername", ex);
                                                }
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        _logger.Error("CopyEdFilesByUsername", ex);
                                    }
                                }
                            }
                        }
                    }
                    string zipFolder = System.Web.Hosting.HostingEnvironment.MapPath(string.Format("~/{0}/", UserDataFolder)) + userName;
                    string zipFile = zipFolder + ".zip";
                    zipEntries.AddDirectory(zipFolder);
                    zipEntries.Save(zipFile);
                    Directory.Delete(zipFolder, true);
                    return WEBSITE + "/" + UserDataFolder + "/" + userName + ".zip";
                }
                else
                {
                    return "No Folders Found";
                }
            }
            else
            {
                return "User not exist";
            }
        }
        catch (Exception ex)
        {
            _logger.Error("CopyEdFilesByUsername", ex);
            return "Something went wrong, please try again later.";
        }
    }

    #region Department
    [WebMethod(EnableSession = true)]
    [SoapHeader("authenticationInfo")]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public int RenameDepartment(string newName, string departmentId)
    {
        try
        {
            return General_Class.UpdateDepartment(Convert.ToInt32(departmentId), newName);
        }
        catch (Exception ex)
        {
            return 0;
        }
    }
    #endregion

    #region WorkAreaRecords

    [WebMethod]
    public string GetOfficeUsers()
    {
        try
        {
            return JsonConvert.SerializeObject(UserManagement.GetOfficeUsers());
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, ex.Message, string.Empty, ex);
            return null;
        }
    }

    [WebMethod]
    public string GetWorkAreaDocumentsByUID(string uid)
    {
        try
        {
            return JsonConvert.SerializeObject(General_Class.GetWorkAreaDocuments(uid));
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, ex.Message, string.Empty, ex);
            return null;
        }
    }

    [WebMethod]
    public void CheckWorkAreaExists(string uid, string fileName, string pathName, decimal size, int workAreaId)
    {
        try
        {
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            bool isExist = rackSpaceFileUpload.ObjectExists(pathName, uid);
            if (!isExist)
                General_Class.InsertIntoWorkAreaRecords(workAreaId, fileName, pathName, uid, size);
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, ex.Message, string.Empty, ex);
        }
    }

    #endregion
}
