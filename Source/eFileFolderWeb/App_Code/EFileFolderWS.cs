using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Data;
using System.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using DataQuicker2.Framework;
using Shinetech.DAL;
using Shinetech.Engines;
using Shinetech.Utility;
using System.IO;
using Account=Shinetech.DAL.Account;
using DocPage = Shinetech.DAL.Page;

/// <summary>
/// Summary description for EFileFolderWebService
/// </summary>
[WebService(Namespace = "http://www.edfiles.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class EFileFolderWS : System.Web.Services.WebService
{
    public AuthenticationInfo authenticationInfo;

    public EFileFolderWS()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public string CallMethod()
    {
        if (User.Identity.IsAuthenticated)
        {
            return ("Valid User");
        }
        else
        {
            return ("INVALID USER");
        }

    }

    /// <summary>
    /// Check the validate code
    /// </summary>
    /// <returns></returns>
    private bool CheckValidateCode(string code)
    {
        return FlashViewer.FlashViewer_CheckValidateCode(code);
    }

    private bool isMySelf(FileFolder folder)
    {
        var uid = Sessions.SwitchedSessionId;
        //var uid = Membership.GetUser().ProviderUserKey.ToString();
        return folder.OfficeID.Value.ToLower().Equals(uid.ToLower());      
    }

    private bool isMySubUser(FileFolder folder)
    {
        var uid = Membership.GetUser();

        var officeid = UserManagement.GetOfficeUserIDBySubUserID(uid.ProviderUserKey.ToString());
        if (officeid.Rows.Count <= 0) return false;

        return folder.OfficeID1.Value == (officeid.Rows[0][0].ToString() == "" ? uid.ProviderUserKey.ToString() : officeid.Rows[0][0].ToString());
    }

    private bool BelongToGroup(string p, string[] groups)
    {
        foreach (string group in groups)
        {
            if (p.Contains(group))
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Get xml data for one Folder
    /// </summary>
    /// <param name="folderId"></param>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public XmlDataDocument GetFolderXML(int folderId, string validateCode)
    {
        //如果登录的是本人，文档拥有者，则直接可以
        //如果不是本人，说明是本站登录者，则只有public和protected可以访问，而如果有securitycode需要验证
        //如果为非登录者，则只有public下可以访问，而如果有securitycode需要验证

        XmlDataDocument errorXml = new XmlDataDocument();
        if (CheckValidateCode(validateCode))
        {
            FileFolder folder = new FileFolder(folderId);
            DataSet ds = null;
            if (folder.IsExist)
            {
                if (this.IsAuthorized())
                {
                    if (folder.SecurityLevel.Value == 0 && !isMySelf(folder))
                    {
                        //如果不是本人则，验证是否该用户和Folder是同一Group
                        var uid = Sessions.SwitchedSessionId;
                        //var uid = Membership.GetUser().ProviderUserKey.ToString();
                        var ac = new Shinetech.DAL.Account(uid);
                        string[] groups = ac.Groups.Value != null ? ac.Groups.Value.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries) : new string[] { };


                        if (BelongToGroup(folder.Groups.Value, groups))
                        {
                            ds = FlashViewer.FlashViewer_GetFolderData(folderId);

                            if (ds != null && ds.Tables.Count == 2 && ds.Tables["Folder"].Rows.Count >= 1)
                            {
                                XmlDataDocument xmlDoc = new XmlDataDocument(ds);
                                return xmlDoc;
                            }
                        }
                        else
                        {
                            errorXml.InnerXml = "<ErrorCode>-2</ErrorCode>";
                            return errorXml;
                        }
                    }
                                      
                    #region IsAuthorized
                    Account act = new Account(folder.OfficeID1.Value);

                    //是本人
                    if (act.Name.Value.Equals(this.User.Identity.Name,StringComparison.OrdinalIgnoreCase))
                    {
                        ds = FlashViewer.FlashViewer_GetFolderData(folderId);
                       
                        if (ds != null && ds.Tables.Count == 2 && ds.Tables["Folder"].Rows.Count >= 1)
                        {
                            XmlDataDocument xmlDoc = new XmlDataDocument(ds);
                            return xmlDoc;
                        }
                    }
                    
                    if(folder.SecurityLevel.Value==1||folder.SecurityLevel.Value==2)
                    {
                        #region be able to read
                        if (!string.IsNullOrEmpty(folder.FolderCode.Value))
                        {
                            errorXml.InnerXml = "<ErrorCode>-9</ErrorCode>";//提示输入验证码
                            return errorXml;
                        }
                        else
                        {
                            ds = FlashViewer.FlashViewer_GetFolderData(folderId);
                                                     
                            if (ds != null && ds.Tables.Count == 2 && ds.Tables["Folder"].Rows.Count >= 1)
                            {
                                XmlDataDocument xmlDoc = new XmlDataDocument(ds);
                                return xmlDoc;
                            }
                        }
                        #endregion
                    }

                    //其它情况则不允许读取
                    errorXml.InnerXml = "<ErrorCode>-3</ErrorCode>";
                    return errorXml;

                    ds = FlashViewer.FlashViewer_GetFolderData(folderId);
                    if (ds != null && ds.Tables.Count == 2 && ds.Tables["Folder"].Rows.Count >= 1)
                    {
                        XmlDataDocument xmlDoc = new XmlDataDocument(ds);
                        return xmlDoc;
                    }
                    #endregion 
                }
                else
                {
                    #region unAuthorized
                    if (folder.SecurityLevel.Value != 2)
                    {
                        errorXml.InnerXml = "<ErrorCode>-2</ErrorCode>";
                        return errorXml;
                    }

                    if (folder.SecurityLevel.Value==2)
                    {
                        if(!string.IsNullOrEmpty(folder.FolderCode.Value))
                        {
                            errorXml.InnerXml = "<ErrorCode>-9</ErrorCode>";//提示输入验证码
                            return errorXml;
                        }
                        else
                        {
                            ds = FlashViewer.FlashViewer_GetFolderData(folderId,true);
                            if (ds != null && ds.Tables.Count == 2 && ds.Tables["Folder"].Rows.Count >= 1)
                            {
                                XmlDataDocument xmlDoc = new XmlDataDocument(ds);
                                return xmlDoc;
                            }
                        }
                    }
                    else
                    {
                        errorXml.InnerXml = "<ErrorCode>-3</ErrorCode>";
                        return errorXml;
                    }

                    #endregion
                }
            }

            errorXml.InnerXml = "<ErrorCode>-2</ErrorCode>";
            return errorXml;
        }
        else
        {
            if (validateCode == folderId + "_AAAAAAAAAAAAAAA")
            {
                errorXml.InnerXml = "<ErrorCode>-3</ErrorCode>";
                return errorXml;
            }
            else
            {
                errorXml.InnerXml = "<ErrorCode>-2</ErrorCode>";
                return errorXml;
            }

        }
    }

    /// <summary>
    /// Get xml data for one Folder
    /// </summary>
    /// <param name="folderId"></param>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public XmlDataDocument GetFolderXMLWithCode(int folderId, string validateCode)
    {
        XmlDataDocument errorXml = new XmlDataDocument();
        
        FileFolder folder = new FileFolder(folderId);
        if (folder.IsExist)
        {
            if (folder.FolderCode.Value.Equals(validateCode, StringComparison.OrdinalIgnoreCase))
            {
                DataSet ds = null;
                if (folder.SecurityLevel.Value == 0 || folder.SecurityLevel.Value == 1)
                {
                    ds = FlashViewer.FlashViewer_GetFolderData(folderId);
                }
                else
                {
                    ds = FlashViewer.FlashViewer_GetFolderData(folderId, true);
                }
                         
                if (ds != null && ds.Tables.Count == 2 && ds.Tables["Folder"].Rows.Count >= 1)
                {
                    XmlDataDocument xmlDoc = new XmlDataDocument(ds);
                    return xmlDoc;
                }
                else
                {
                    errorXml.InnerXml = "<ErrorCode>-1</ErrorCode>";
                    return errorXml;
                }
            }
            else
            {
                errorXml.InnerXml = "<ErrorCode>-9</ErrorCode>";
                return errorXml;
            }
        }
        else
        {
            errorXml.InnerXml = "<ErrorCode>-1</ErrorCode>";
            return errorXml;
        } 
    }


    /// <summary>
    /// Get xml data for one Tab
    /// </summary>
    /// <param name="tabId"></param>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public XmlDataDocument GetTabXML(int tabId, string validateCode)
    {
        XmlDataDocument errorXml = new XmlDataDocument();
        if (CheckValidateCode(validateCode))
        {
            DataSet ds = FlashViewer.FlashViewer_GetTabData(tabId);
            XmlDataDocument xmlDoc = new XmlDataDocument(ds);
            return xmlDoc;

        }
        else
        {
            errorXml.InnerXml = "<ErrorCode>-2</ErrorCode>";
            return errorXml;
        }
    }

    /// <summary>
    /// Insert or update Quick Note
    /// </summary>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public long UpdateQuickNote(string uid, long id, int pageId, string content, int x, int y, bool bFixed, bool bOpened, string validateCode)
    {
        if (CheckValidateCode(validateCode))
        {
            long result = FlashViewer.FlashViewer_UpdateQuickNote(uid, id, pageId, content, x, y, bFixed, bOpened);
            return result;
        }
        else
        {
            //todo:binguo
            return -2;
        }
    }

    /// <summary>
    /// Insert or update Quick Note
    /// </summary>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public long UpdateQuickNote(string uid, long id, string pagekey, string content, int x, int y, bool bFixed, bool bOpened, string validateCode)
    {
        if (CheckValidateCode(validateCode))
        {
            long result = FlashViewer.FlashViewer_UpdateQuickNote(uid, id, pagekey, content, x, y, bFixed, bOpened);
            return result;
        }
        else
        {
            //todo:binguo
            return -2;
        }
    }

    /// <summary>
    ///  Delete one quick note
    /// </summary>
    /// <param name="quickNoteId"></param>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public int DeleteQuickNote(int quickNoteId, string validateCode)
    {
        if (CheckValidateCode(validateCode))
        {
            bool result = FlashViewer.FlashViewer_DeleteQuickNote(quickNoteId);
            return result ? 1 : 0;
        }
        else
        {
            //todo:binguo
            return -2;
        }
    }


    /// <summary>
    /// Insert or update Quick Note
    /// </summary>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public long UpdateURLNote(string uid, long id, int pageId, string title, string content, int x, int y, string validateCode)
    {
        if (CheckValidateCode(validateCode))
        {
            long result = FlashViewer.FlashViewer_UpdateURLNote(uid, id, pageId,title, content, x, y);
            return result;
        }
        else
        {
            //todo:binguo
            return -2;
        }
    }

    /// <summary>
    ///  Delete one quick note
    /// </summary>
    /// <param name="urlId"></param>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public int DeleteURLNote(int urlId, string validateCode)
    {
        if (CheckValidateCode(validateCode))
        {
            bool result = FlashViewer.FlashViewer_DeleteURLNote(urlId);
            return result ? 1 : 0;
        }
        else
        {
            //todo:binguo
            return -2;
        }
    }


    /// <summary>
    /// Insert or update new note
    /// </summary>
    /// <param name="uid"></param>
    /// <param name="id"></param>
    /// <param name="tabId"></param>
    /// <param name="content"></param>
    /// <param name="validateCode"></param>
    /// <param name="strTitle"></param>
    /// <param name="strAuthor"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public long UpdateNewNote(string uid, long id, int tabId, string content, string validateCode, string strTitle, string strAuthor)
    {
        if (CheckValidateCode(validateCode))
        {
            //content = content.Replace("'", "''");
            long result = FlashViewer.FlashViewer_UpdateNewNote(uid, id, tabId, content, strTitle, strAuthor);
            return result;
        }
        else
        {
            //todo:binguo
            return -2;
        }
    }

    /// <summary>
    /// Delete one new note
    /// </summary>
    /// <param name="newNoteId"></param>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public int DeleteNewNote(int newNoteId, string validateCode)
    {
        if (CheckValidateCode(validateCode))
        {
            bool result = FlashViewer.FlashViewer_DeleteNewNote(newNoteId);
            return result ? 1 : 0;
        }
        else
        {
            //todo:binguo
            return -2;
        }
    }

    /// <summary>
    ///  mail firend
    /// </summary>
    /// <param name="xml"></param>
    /// <param name="validateCode"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public int MailFirend(string strSendMail, string strToMail, int folderID, string strMailInfor, string validateCode, string SendName, string ToName, string MailTitle)
    {
        if (CheckValidateCode(validateCode))
        {
            FileFolder folder = new FileFolder(folderID);
            if (!folder.IsExist) return -2;

            if (folder.SecurityLevel.Value == 0) return -2;

            string strvalidateCode = FlashViewer.FlashViewer_GetValidateCode(folderID, 0);

            FlashViewer.FlashViewer_SendMail(ToName, SendName, folderID, strSendMail, MailTitle, strMailInfor, strToMail, strvalidateCode);

            int MailID = FlashViewer.FlashViewer_MailFriend(strSendMail, strToMail, folderID, MailTitle, strMailInfor);
            return MailID;
        }
        else
        {
            return -2;
        }
    }

    /// <summary>
    ///  Back validateCode
    /// </summary>
    /// <param name="folderID"></param>
    /// <param name="userid"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public string backVCode(int folderID,string userid)
    {
        string strvalidateCode = "";
        try
        {
            FileFolder ffd = new FileFolder(folderID);
            if (ffd.IsExist)
            {
                if(ffd.SecurityLevel.Value==0)//private level
                {
                    DataTable table = UserManagement.getUserIDByUserName(userid);
                    if(table.Rows.Count>0)
                    {
                        if (table.Rows[0]["UID"].ToString() == ffd.OfficeID.Value //要不是自己创建的或者自己就是OfficeUser
                         || table.Rows[0]["UID"].ToString() == ffd.OfficeID1.Value)
                        {
                            strvalidateCode = FlashViewer.FlashViewer_GetValidateCode(folderID, 0);
                            return strvalidateCode;
                        }
                        else
                        {
                            strvalidateCode = "1";
                        }
                    }
                }
                else
                {
                    strvalidateCode = FlashViewer.FlashViewer_GetValidateCode(folderID, 0);
                    return strvalidateCode;
                }
            }
            
        }
        catch
        {
            strvalidateCode = "";
        }

        return strvalidateCode;
    }


    //==================================================
    //==================== for test ==================
    public XmlDataDocument getTestFolderXml()
    {
        //init dataset
        DataSet ds = new DataSet("FolderData");

        DataTable table1 = new DataTable("Folder");
        table1.Columns.Add("Id", typeof(int));
        table1.Columns.Add("Name", typeof(string));
        table1.Columns.Add("FirstName", typeof(string));
        table1.Columns.Add("LastName", typeof(string));
        //insert rows
        DataRow row = table1.NewRow();
        row["Id"] = 14;
        row["Name"] = "Surgery";
        row["FirstName"] = "bingo";
        row["LastName"] = "xyx";
        table1.Rows.Add(row);

        ds.Tables.Add(table1);

        //second table for tabs
        DataTable table2 = new DataTable("Tab");
        table2.Columns.Add("Id", typeof(int));
        table2.Columns.Add("Name", typeof(string));
        table2.Columns.Add("Color", typeof(string));

        //insert row1
        DataRow row1 = table2.NewRow();
        row1["Id"] = 101;
        row1["Name"] = "History/Physical";
        row1["Color"] = "0xc71011";
        table2.Rows.Add(row1);
        //insert row2
        DataRow row2 = table2.NewRow();
        row2["Id"] = 121;
        row2["Name"] = "Medications";
        row2["Color"] = "0xff8608";
        table2.Rows.Add(row2);
        //insert row3
        DataRow row3 = table2.NewRow();
        row3["Id"] = 131;
        row3["Name"] = "Progress Notes";
        row3["Color"] = "0xdcb603";
        table2.Rows.Add(row3);

        ds.Tables.Add(table2);


        //convert dataset to XmlDataDocument
        XmlDataDocument xmlDoc = new XmlDataDocument(ds);
        return xmlDoc;
    }

    /// <summary>
    /// Get a swf file by the special page name
    /// </summary>
    /// <param name="pageName"></param>
    /// <returns></returns>
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public string GetSwfByPageName(string pageName, string validateCode)
    {
        string[] items = pageName.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
        int pageNumber = 1;
        string fileName = Path.GetFileNameWithoutExtension(items[0]);
        string filePathV = Path.GetDirectoryName(items[0]);//virtual path
        
        string filePath = "";
        try
        {
            if (!CheckValidateCode(validateCode))
            {
                return "";
            }

            string tempPath = filePathV.Substring(filePathV.IndexOf("Volume"));
            filePath = Server.MapPath(tempPath);//directory path
            string extanme = Path.GetExtension(items[0]);
            //if page file is swf 
            if (extanme.ToLower() == ".swf")
            {
                return items[0];
            }

        }catch
        {
            filePath = "";
        }
      
        //there are a bugs here, if the pdf file has _ figure.
        string[] vars = fileName.Split('_');//ARB_guide_81_361_1_0.jpg
        if (vars.Length < 5)
        {
            return null;
        }
        else
        {
            int count = vars.Length;
            string pdfFileName = vars[0], swfFileName = "";
            for (int i =1; i < count - 4; i++)
            {
                pdfFileName = string.Concat(pdfFileName,"_", vars[i]);
            }

            Document doc = new Document(int.Parse(items[1]));
            if (!doc.IsExist)
            {
                return null;
            }

            

            string ext = ".pdf";
            StreamType extValue = StreamType.PDF;
            try
            {
                extValue  = (StreamType)doc.FileExtention.Value;
                ext = this.getFileExtention(extValue);
            }catch{}

            pdfFileName = string.Concat(pdfFileName, ext);
            pdfFileName = string.Concat(filePath, Path.DirectorySeparatorChar, pdfFileName);
            try
            {
                pageNumber = Convert.ToInt32(vars[count-4+2]);
                if (PDFUtil.Pdf2Swf(pdfFileName, pageNumber, ref swfFileName))
                {
                    swfFileName = string.Concat(filePathV, Path.DirectorySeparatorChar, swfFileName);
                    return swfFileName;
                }
                else
                {
                    return null;
                }
            }
            catch
            {

                return null;
            }
        }
    }

    protected string getFileExtention(StreamType ext)
    {
        string extname = ".pdf";
        switch (ext)
        {
            case StreamType.PDF:
                extname = ".pdf";
                break;
            case StreamType.Word:
                extname = ".doc";
                break;
            case StreamType.Excel:
                extname = ".xls";
                break;
            case StreamType.PPT:
                extname = ".ppt";
                break;
            case StreamType.RTF:
                extname = ".rtf";
                break;
            default:
                break;
        }

        return extname;
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool LoginUser(string userId,string pwd)
    {
        if(authenticationInfo==null)
        {
            return false;
        }
        else
        {
            return UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password);
        }
    }
        

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public XmlDataDocument GetEFFXML(string userName)
    {

        XmlDataDocument errorXml = new XmlDataDocument();

        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if(!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(FileFolderManagement.GetFileFoldersXML(userName));
            XmlDataDocument xmlDoc = new XmlDataDocument(ds);
            return xmlDoc;
        }
        catch
        {
            errorXml.InnerXml = "<ErrorCode>-1</ErrorCode>";
            return errorXml;
        }

    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public XmlDataDocument GetMetaXML(int tabID, string validateCode)
    {
        XmlDataDocument xml = new XmlDataDocument();

        try
        {
            if (!CheckValidateCode(validateCode))
            {
                throw new ApplicationException();
            }

            Divider tab = new Divider(tabID);
            if (tab.IsExist)
            {
                xml.LoadXml(tab.MetaXML.Value ?? "<ErrorCode>0</ErrorCode>");
            } 
            else
            {
                xml.InnerXml = "<ErrorCode>-1</ErrorCode>";
            }
        }catch
        {
            xml.InnerXml = "<ErrorCode>-1</ErrorCode>";
        }

        return xml;
       
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool SetMetaXML(int tabID, string metaXml, string validateCode)
    {
        XmlDataDocument xml = new XmlDataDocument();

        try
        {
            if (!CheckValidateCode(validateCode))
            {
                return false;
            }
        
            Divider tab = new Divider(tabID);
            if (tab.IsExist)
            {
                tab.MetaXML.Value = metaXml;
                tab.Update();

                return true;
            }
        }
        catch
        {
        }

        return false;

    }

    #region ActionScript AIR Functons

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public XmlDataDocument CreateFileFolder(string foldername, string firstname, string lastname, string dob,
        string security, string[] tabs, string[] colors, string username)
    {
        DateTime dt = DateTime.Now;
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            dt = DateTime.Parse(dob);
        }catch{}
        XmlDataDocument xml = new XmlDataDocument();

        DataTable table = UserManagement.ExistUserName1(username);
        if (table == null || table.Rows.Count==0)
        {
            xml.InnerXml = "<folder><result>false</result><message>The user{" + username + "} doesn't exist</message></folder>";
            return xml;
        }
        else if ( table.Rows.Count==0)
        {
            xml.InnerXml = "<folder><result>false</result><message>The user{" + username + "} doesn't exist</message></folder>";
            return xml;
        }

        Dictionary<string, Divider> list = new Dictionary<string, Divider>();
        for (int i = 0; i < tabs.Length;i++ )
        {
            string tabname = tabs[i];
            string color = colors[i];

            Divider newEntity = new Divider();
            newEntity.Name.Value = tabname;
            newEntity.Color.Value = color;

            list.Add(tabname, newEntity);
        }

        try
        {
            //check the folder number
            Account user = new Account(table.Rows[0]["UID"].ToString());
            if(user.IsSubUser.Value=="1")
            {
                user = new Account(table.Rows[0]["OfficeUID"].ToString());
            }

            if (user.FileFolders.Value <= FileFolderManagement.GetUsedFoldersNumber(user.UID.Value))
            {
                xml.InnerXml = "<folder><result>false</result><message>You have no more unused EdFiles.</message></folder>";
                return xml;
            }

            FileFolder folder = new FileFolder();
            folder.FolderName.Value = foldername;
            folder.FirstName.Value = firstname;
            folder.Lastname.Value = lastname;
            folder.SecurityLevel.Value = Convert.ToInt32(security);
            folder.FileNumber.Value = "";
            folder.DOB.Value = dob;
            folder.CreatedDate.Value = DateTime.Now;
            folder.OfficeID.Value = table.Rows[0]["UID"].ToString();

            if (table.Rows[0]["IsSubUser"].Equals("0"))
            {
                folder.OfficeID1.Value = table.Rows[0]["UID"].ToString();
            }
            else
            {
                folder.OfficeID1.Value = table.Rows[0]["OfficeUID"].ToString();
            }

            int newfolderId = FileFolderManagement.CreatNewFileFolder(folder, list);

            xml.InnerXml = "<folder><result>" + newfolderId + "</result></folder>";
        }
        catch (Exception exp)
        {
            xml.InnerXml = "<folder><result>false</result><message>" + exp.Message + "</message></folder>";
           
        }

        return xml;
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public XmlDataDocument CreateFileFolder1(string foldername, string firstname, string lastname, string dob,string fileNumber,
        string security, string[] tabs, string[] colors, string username)
    {
        XmlDataDocument xml = new XmlDataDocument();

        if (authenticationInfo == null)
        {
            xml.InnerXml = "<folder><result>false</result><message>The user{" + username + "} doesn't exist</message></folder>";
            return xml;
        }
        else
        {
            if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
            {
                xml.InnerXml = "<folder><result>false</result><message>The user{" + username + "} doesn't exist</message></folder>";
                return xml;
            }
        }


        DataTable table = UserManagement.ExistUserName1(username);
        if (table == null || table.Rows.Count == 0)
        {
            xml.InnerXml = "<folder><result>false</result><message>The user{" + username + "} doesn't exist</message></folder>";
            return xml;
        }
        else if (table.Rows.Count == 0)
        {
            xml.InnerXml = "<folder><result>false</result><message>The user{" + username + "} doesn't exist</message></folder>";
            return xml;
        }

        Dictionary<string, Divider> list = new Dictionary<string, Divider>();
        for (int i = 0; i < tabs.Length; i++)
        {
            string tabname = tabs[i];
            string color = colors[i];

            Divider newEntity = new Divider();
            newEntity.Name.Value = tabname;
            newEntity.Color.Value = color;

            list.Add(tabname, newEntity);
        }

        try
        {
            //check the folder number
            Account user = new Account(table.Rows[0]["UID"].ToString());
            if (user.IsSubUser.Value == "1")
            {
                user = new Account(table.Rows[0]["OfficeUID"].ToString());
            }

            if (user.FileFolders.Value <= FileFolderManagement.GetUsedFoldersNumber(user.UID.Value))
            {
                xml.InnerXml = "<folder><result>false</result><message>You have no more unused EdFiles.</message></folder>";
                return xml;
            }

            FileFolder folder = new FileFolder();
            folder.FolderName.Value = foldername;
            folder.FirstName.Value = firstname;
            folder.Lastname.Value = lastname;
            folder.SecurityLevel.Value = Convert.ToInt32(security);
            folder.FileNumber.Value = fileNumber;
            folder.DOB.Value = dob;
            folder.CreatedDate.Value = DateTime.Now;
            folder.OfficeID.Value = table.Rows[0]["UID"].ToString();

            if (table.Rows[0]["IsSubUser"].Equals("0"))
            {
                folder.OfficeID1.Value = table.Rows[0]["UID"].ToString();
            }
            else
            {
                folder.OfficeID1.Value = table.Rows[0]["OfficeUID"].ToString();
            }

            int newfolderId = FileFolderManagement.CreatNewFileFolder(folder, list);

            xml.InnerXml = "<folder><result>" + newfolderId + "</result></folder>";
        }
        catch (Exception exp)
        {
            xml.InnerXml = "<folder><result>false</result><message>" + exp.Message + "</message></folder>";

        }

        return xml;
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public DataTable GetFileFolders(string userName)
    {
        DataTable dt = new DataTable("Folder");
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            DataTable uTable = UserManagement.getUserIDByUserName(userName);
            if (uTable.Rows.Count == 0) throw new ApplicationException("-1");
            Account user = new Account(uTable.Rows[0][0].ToString());
            if(user.IsExist)
            {
                string officeID1 = string.Empty;

                if(user.RoleID.Value=="Offices")
                {
                    officeID1 = user.UID.Value;
                }
                else if (user.RoleID.Value == "Users")
                {
                    officeID1 = user.OfficeUID.Value;
                }

                dt = FileFolderManagement.GetFileFoldersInfor(officeID1);

            }
           
        }
        catch (Exception ex)
        {
           
        }

        return dt;
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public XmlDataDocument RemoveEFileFolder(string folderId)
    {
        XmlDataDocument xml = new XmlDataDocument();
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            string folderPath = "~/Volume/" + folderId;
            if (Directory.Exists(Server.MapPath(folderPath)))
            {
                Directory.Delete(Server.MapPath(folderPath), true);
            }

        }
        catch (Exception ex)
        {
            xml.InnerXml = "<result>" + ex.Message + "</result>"; ;

            return xml;
        }

        FileFolderManagement.DelFileFolderByFolderID(folderId);
        xml.InnerXml = "<result>true</result>";
        return xml;
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public XmlDataDocument ModifyEFileFolder(string fid, string foldername, string firstname, string lastname, string dob, string security)
    {
        DateTime dt = DateTime.Now;
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            dt = DateTime.Parse(dob);
        }
        catch { }

        XmlDataDocument xml = new XmlDataDocument();

        try
        {

            FileFolder folder = new FileFolder(int.Parse(fid));
            if (folder.IsExist)
            {
                folder.FolderName.Value = foldername;
                folder.FirstName.Value = firstname;
                folder.Lastname.Value = lastname;
                folder.FileNumber.Value = "";
                folder.DOB.Value = dob;
                folder.CreatedDate.Value = DateTime.Now;
                folder.SecurityLevel.Value = int.Parse(security);

                folder.Update();

                xml.InnerXml = "<result>true</result>";
            }
            else
            {
                xml.InnerXml = "<result>the folder doesn't exist.</result>"; ;
            }
        }
        catch(Exception exp)
        {
            xml.InnerXml = "<result>"+exp.Message+"</result>";
        }

        return xml;
        
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public XmlDataDocument ModifyEFileFolder1(string fid, string foldername, string firstname, string lastname, string dob, string fileNumber,
        string security)
    {
        XmlDataDocument xml = new XmlDataDocument();

        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            FileFolder folder = new FileFolder(int.Parse(fid));
            if (folder.IsExist)
            {
                folder.FolderName.Value = foldername;
                folder.FirstName.Value = firstname;
                folder.Lastname.Value = lastname;
                folder.FileNumber.Value = fileNumber;
                folder.DOB.Value = dob;
                folder.CreatedDate.Value = DateTime.Now;
                folder.SecurityLevel.Value = int.Parse(security);

                folder.Update();

                xml.InnerXml = "<result>true</result>";
            }
            else
            {
                xml.InnerXml = "<result>the folder doesn't exist.</result>"; ;
            }
        }
        catch (Exception exp)
        {
            xml.InnerXml = "<result>" + exp.Message + "</result>";
        }

        return xml;

    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool RemoveTab1(string tabid)
    {
        FileFolderManagement.DeleteDivider(int.Parse(tabid));
        return true;
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool ModifyTab(string tabid,string tabname,string color)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            Divider objDivider = new Divider(int.Parse(tabid));
            if (objDivider.IsExist)
            {
                objDivider.Name.Value = tabname.Trim();
                objDivider.Color.Value = color;
                objDivider.Update();
                return true;
            }
        }catch{}

        return false;
        
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public XmlDataDocument InsertTab1(string tabname, string color,string username,string officeid)
    {
        XmlDataDocument xml = new XmlDataDocument();
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            string uid = UserManagement.ExistUserName(username);
            if (uid == null)
            {
                xml.InnerXml = "<result>false</result>";
                return xml;
            }

            Divider objDivider = new Divider();
            objDivider.Name.Value = tabname;
            objDivider.Color.Value = color;
            objDivider.EffID.Value = Convert.ToInt32(officeid);
            objDivider.OfficeID.Value = uid;

            FileFolderManagement.InsertDivider(objDivider);
            xml.InnerXml = "<result>" + objDivider.DividerID.Value.ToString() + "</result>";
        }catch
        {
            xml.InnerXml = "<result>false</result>";
        }
        return xml;
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public DataTable GetTabsByFolderID(string folderid)
    {
        DataTable table = new DataTable("Tabs");

        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            Divider dv = new Divider();
            ObjectQuery query = dv.CreateQuery();
            query.SetCriteria(dv.EffID, Convert.ToInt32(folderid));

            query.Fill(table);

        }
        catch
        {}
        return table;
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public string ExitsFileNameByUID(string fileName,string uid)
    {
        if (authenticationInfo == null)
        {
            return "NA";
        }
        else
        {
            if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
            {
                return "NA";
            }
        }


        string id = UserManagement.ExistUserName(uid);
        if (id == null)
        {
            return "No:" + fileName;
        }

        string path = Server.MapPath("~/ScanInBox");
        string filepath = string.Concat(path, Path.DirectorySeparatorChar, id, Path.DirectorySeparatorChar, fileName);

        if(!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);

            return "No:" + fileName;
        }

        if(File.Exists(filepath))
        {
            return "Yes:" + fileName;
        }
        else
        {
            return "No:" + fileName;
        }

    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public string GetCurrentVersion()
    {
        if (authenticationInfo == null)
        {
            return "false";
        }
        else
        {
            if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
            {
                return "false";
            }
        }


        XmlDocument doc = new XmlDocument();
        
        string path = Server.MapPath("~/");
        string filepath = string.Concat(path, Path.DirectorySeparatorChar, "update.xml");

        if (File.Exists(filepath))
        {
           
            doc.Load(filepath);
            XmlNode node = doc.GetElementsByTagName("version")[0];

            return node.InnerText;
        }

        return "false";
    }

    [WebMethod]
    public XmlDataDocument VerifySecurityCode(int folderid, string code)
    {
        XmlDataDocument errorXml = new XmlDataDocument();
       
        try
        {
            FileFolder folder = new FileFolder(folderid);
            if (folder.IsExist)
            {
                if (folder.SecurityLevel.Value == 2)
                {
                    
                    if (folder.FolderCode.Value.Equals(code, StringComparison.OrdinalIgnoreCase))
                    {
                        errorXml.InnerXml = "<ErrorCode>" + code + "</ErrorCode>";
                        return errorXml;
                    }
                    else
                    {
                        errorXml.InnerXml = "<ErrorCode>0</ErrorCode>";
                        return errorXml;
                    }
                }
                else
                {
                    errorXml.InnerXml = "<ErrorCode>" + code + "</ErrorCode>";
                    return errorXml;
                }
            }
           
        }
        catch
        {
            
        }

        errorXml.InnerXml = "<ErrorCode>0</ErrorCode>";
        return errorXml;
    }

    [WebMethod(Description="Whether this User is authorized")]
    public bool IsAuthorized()
    {
        try
        {
            return this.User.Identity.IsAuthenticated;
        }
        catch { }

        return false;
        
       
    }
    #endregion


    #region API for File Manager
    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public int CreateFileFolder3(string foldername, string firstname, string lastname, DateTime dob,
        string security,string filenumber,string guid )
    {
        DateTime dt = DateTime.Now;
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            dt = dob;
        }
        catch {
            return -1; 
        }
        

        try
        {
            int id = ExistFolderByGUID(guid);
           
            if (id!=-1) return id ;


            DataTable table = UserManagement.ExistUserName1(authenticationInfo.username);

            //check the folder number
            Account user = new Account(table.Rows[0]["UID"].ToString());
            string ouid = user.UID.Value;
            bool bSubUser = false;
            if (user.IsSubUser.Value == "1")
            {
                bSubUser = true;
                user = new Account(user.OfficeUID.Value);
            }

            if (user.FileFolders.Value <= FileFolderManagement.GetUsedFoldersNumber(user.UID.Value))
            {
                return -1;
            }

            FileFolder folder = new FileFolder();
            folder.FolderName.Value = foldername;
            folder.FirstName.Value = firstname;
            folder.Lastname.Value = lastname;
            folder.SecurityLevel.Value = Convert.ToInt32(security);
            folder.FileNumber.Value = "";
            folder.DOB.Value = dob.ToString("MM-dd-yyyy");
            folder.CreatedDate.Value = DateTime.Now;
            folder.OfficeID.Value = ouid;
            folder.GUID.Value = guid; // for filemanager

            if (!bSubUser)
            {
                folder.OfficeID1.Value = ouid;
            }
            else
            {
                folder.OfficeID1.Value = user.UID.Value;
            }

           return FileFolderManagement.CreatNewFileFolder(folder);

           
        }
        catch (Exception exp)
        {
            return -1;

        }
    }

    public int ExistFolderByGUID(string guid)
    {

        IDbConnection connection = DbFactory.CreateConnection();
        
        try
        {
            connection.Open();

            DataTable table = new DataTable();
            FileFolder folder = new FileFolder();
            ObjectQuery query = folder.CreateQuery();
            query.SetCriteria(folder.GUID, guid);
            query.SetSelectFields(folder.FolderID);
            query.Fill(table);
            if (table.Rows.Count >0)
            {
                return (int)table.Rows[0]["FolderID"];
            }
            else
            {
                return -1;
            }
           
        }
        catch (Exception ex)
        {
            return -1;
        }
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool UpdateFileFolder(int id,string foldername, string firstname, string lastname,string security, string filenumber, string guid)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }
        }
        catch
        {
            return false;
        }


        try
        {
            if (id == -1)
            {
                id = GetFileFolderByGUID(guid);
            }

            FileFolder folder = new FileFolder(id);
            if (folder.IsExist)
            {
                folder.FolderName.Value = foldername;
                folder.FirstName.Value = firstname;
                folder.Lastname.Value = lastname;
                folder.SecurityLevel.Value = Convert.ToInt32(security);
                folder.FileNumber.Value = filenumber;
            }
            else
            {
                return false;
            }
                      
            return FileFolderManagement.UpdateFileFolder(folder);            
        }
        catch (Exception exp)
        {
            return false;

        }
    }

    private int GetFileFolderByGUID(string guid)
    {
        IDbConnection connection = DbFactory.CreateConnection();

        try
        {
            connection.Open();
            DataTable table = new DataTable();
            FileFolder folder = new FileFolder();
            ObjectQuery query = folder.CreateQuery();
            query.SetSelectFields(folder.FolderID);
            query.SetCriteria(folder.GUID, guid);

            query.Fill(table);

            if (table.Rows.Count == 1)
            {
                return (int)table.Rows[0]["FolderID"];
            }
            else
            {
                throw new ApplicationException();
            }
        }
        catch (Exception ex)
        {
            throw new ApplicationException(ex.Message);
        }
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool RemoveFileFolder(int fid)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            return FileFolderManagement.DelFileFolderByFolderID(fid.ToString());

        }
        catch
        {
            return false;
        }
    }


    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public int InsertTab(string tabname, string color, int folderid,string guid)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            FileFolder folder = new FileFolder(folderid);
            if(folder.IsExist)
            {
                int id = ExistDividerByGUID(guid);
                if (id != -1) return id;

                Divider objDivider = new Divider();
                objDivider.Name.Value = tabname;
                objDivider.Color.Value = color;
                objDivider.EffID.Value = folder.FolderID.Value;
                objDivider.OfficeID.Value = folder.OfficeID.Value;
                objDivider.GUID.Value = guid;
               

                int ret =  FileFolderManagement.InsertDivider(objDivider);
                if (ret == -1) return -1;

                folder.OtherInfo.Value = folder.OtherInfo.Value+1;

                FileFolderManagement.UpdateFileFolder(folder);

                return ret;
            }
            else
            {
                return -1;
            }
           
        }
        catch
        {
            return -1;
        }
    }

    public int ExistDividerByGUID(string guid)
    {

        IDbConnection connection = DbFactory.CreateConnection();

        try
        {
            connection.Open();
            DataTable table = new DataTable();
            Divider divider = new Divider();
            ObjectQuery query = divider.CreateQuery();
            query.SetCriteria(divider.GUID, guid);
            query.SetSelectFields(divider.DividerID);

            query.Fill(table);

            if (table.Rows.Count>0)
            {
                return (int) table.Rows[0]["DividerID"];
            }
            else
            {
                return -1;
            }

        }
        catch (Exception ex)
        {
            return -1;
        }
    }


    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool UpdateTab(int tabid, string tabname, string color, int folderid, string guid)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }


            if(tabid==-1)
            {
                tabid = GetDividerByGUID(guid);
            }

            Divider objDivider = new Divider(tabid);
            if (objDivider.IsExist)
            {
                objDivider.Name.Value = tabname;
                objDivider.Color.Value = color;

                return FileFolderManagement.UpdateDivider(objDivider);
            }
            else
            {
                return false;
            }
            

        }
        catch
        {
            return false;
        }
    }

    public int GetDividerByGUID(string guid)
    {
        IDbConnection connection = DbFactory.CreateConnection();

        try
        {
            connection.Open();
            DataTable table = new DataTable();
            Divider divider = new Divider();
            ObjectQuery query = divider.CreateQuery();
            query.SetSelectFields(divider.DividerID);
            query.SetCriteria(divider.GUID, guid);

            query.Fill(table);

            if(table.Rows.Count==1)
            {
                return (int)table.Rows[0]["DividerID"];
            }
            else
            {
                throw new ApplicationException();
            }
        }
        catch (Exception ex)
        {
            throw new ApplicationException(ex.Message);
        }
    }


    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool RemoveTab(int tabid,string guid)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            if (tabid==-1)
            {
                tabid = this.GetDividerByGUID(guid);
            }
            return FileFolderManagement.DeleteDivider(tabid);

        }
        catch
        {
            return false;
        }
    }


    public int ExistFileByGUID(string guid)
    {

        IDbConnection connection = DbFactory.CreateConnection();

        try
        {
            connection.Open();

            DataTable table = new DataTable();
            Document document = new Document();
            ObjectQuery query = document.CreateQuery();
            query.SetCriteria(document.GUID, guid);
            query.SetSelectFields(document.DocumentID);
            query.Fill(table);
            if (table.Rows.Count > 0)
            {
                return (int)table.Rows[0]["DocumentID"];
            }
            else
            {
                return -1;
            }

        }
        catch (Exception ex)
        {
            return -1;
        }
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public int AppendFile(string filename, int pagecount, string filepath, float size, int format, int folderid, 
        string fguid, int tabid)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            int id = ExistFileByGUID(fguid);

            if(id!=-1) return id;

            Divider divider = new Divider(tabid);
            if(divider.IsExist)
            {
                Document doc = new Document();
                doc.Name.Value = filename;
                doc.PagesCount.Value = pagecount;
                doc.PathName.Value = filepath;
                doc.Size.Value = size;
                doc.FileFolderID.Value = folderid;
                doc.DividerID.Value = tabid;
                doc.DOB.Value = DateTime.Now;
                doc.UID.Value = divider.OfficeID.Value;
                doc.FileExtention.Value = format;
                doc.GUID.Value = fguid;


                IDbConnection connection = DbFactory.CreateConnection();
                connection.Open();

                string encodeName = HttpUtility.UrlPathEncode(filename);
                encodeName = encodeName.Replace("%", "$");

                IDbTransaction transaction = connection.BeginTransaction();
                try
                {
                    doc.Create(transaction);

                    for (int i = 1; i <= doc.PagesCount.Value;i++ )
                    {
                        DocPage page = new DocPage();
                        page.PageNumber.Value = i;
                        page.DocumentID.Value = doc.DocumentID.Value;
                        page.ImageName.Value = string.Format("{0}_{1}_{2}_{3}.swf", encodeName, folderid, tabid, i);
                        page.ImagePath.Value = filepath + Path.DirectorySeparatorChar + page.ImageName.Value;

                        page.DividerID.Value = doc.DividerID.Value;
                        page.Size.Value = 0;
                        page.Create(transaction);
                    }

                    FileFolder eff = new FileFolder(folderid);
                    eff.OtherInfo2.Value += doc.Size.Value;
                    eff.Update(transaction);

                    transaction.Commit();

                    return doc.DocumentID.Value ;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new ApplicationException(ex.Message);
                }
            }

            return -1;

        }
        catch
        {
            return -1;
        }
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public bool RemoveFile(int fid,string fguid)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            if (fid==-1)
            {
                fid = FileFolderManagement.GetFileIdByGUID(fguid);
                if (fid == -1) return true;
            }
            FileFolderManagement.DelPageByDocumentID(fid.ToString());

            DataTable dtFileFolder = FileFolderManagement.GetFileFolderSizeByDocumentID(fid.ToString());
            string strFolderSize = dtFileFolder.Rows[0]["Otherinfo2"].ToString();
            string strFolderID = dtFileFolder.Rows[0]["FolderID"].ToString();

            DataTable dtDocInfor = FileFolderManagement.GetDocInforByDocumentID(fid.ToString());
            string strDocSize = dtDocInfor.Rows[0]["size"].ToString();

            Double newFolderSize = Convert.ToDouble(strFolderSize) - Convert.ToDouble(strDocSize);
            FileFolderManagement.UpdateFolderSize(strFolderID, newFolderSize);


            return FileFolderManagement.DelDocumentByDocumentID(fid.ToString());

        }
        catch
        {
            return false;
        }
    }


    [WebMethod(BufferResponse = false)]
    [SoapHeader("authenticationInfo")]
    public bool UploadFile(byte[] filedata,int dividerId, int folderId,string namewithext)
    {
        try
        {
            if (authenticationInfo == null)
            {
                throw new ApplicationException("");
            }
            else
            {
                if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
                {
                    throw new ApplicationException("");
                }
            }

            MemoryStream ms = new MemoryStream(filedata);
            string rootPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Volume/") + folderId + Path.DirectorySeparatorChar + dividerId;
            
            if(!Directory.Exists(rootPath))
            {
                Directory.CreateDirectory(rootPath);
            }

            string filename = rootPath + Path.DirectorySeparatorChar + namewithext;

            if(File.Exists(filename))
            {
                return true;
            }

            FileStream fs = new FileStream(filename, FileMode.Create);

            ms.WriteTo(fs);

            ms.Close();
            fs.Close();
            fs.Dispose();
            return  true;
        }
        catch (Exception ex)
        {
            return false;
        }
 
    }

    [WebMethod]
    [SoapHeader("authenticationInfo")]
    public void AppendChunk(int dividerId, int folderId, string namewithext, byte[] filedata, long Offset)
    {
        if (authenticationInfo == null)
        {
            return;
        }
        else
        {
            if (!UserManagement.LoginUser(authenticationInfo.username, authenticationInfo.password))
            {
                return;
            }
        }

        string rootPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Volume/") + folderId + Path.DirectorySeparatorChar + dividerId;

        if (!Directory.Exists(rootPath))
        {
            Directory.CreateDirectory(rootPath);
        }

        string filename = rootPath + Path.DirectorySeparatorChar + namewithext;

        if (Offset == 0)	// new file, create an empty file
        {
            File.Create(filename).Close();
        }

        // open a file stream and write the buffer.  Don't open with FileMode.Append because the transfer may wish to start a different point
        using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.ReadWrite, FileShare.Read))
        {
            try
            {
                fs.Seek(Offset, SeekOrigin.Begin);
                fs.Write(filedata, 0, filedata.Length);
            }catch
            {
            }
            
        }
    }

    #endregion
}
