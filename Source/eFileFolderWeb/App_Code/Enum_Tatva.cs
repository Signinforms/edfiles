﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Web;

/// <summary>
/// Summary description for Enum_Tatva
/// </summary>
public class Enum_Tatva
{
    public Enum_Tatva()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public enum FileMove
    {
        FileExist = 0,
        FileMoveSuccess = 1,
        FileMoveError = 2
    }

    public enum Action
    {
        Create = 1,
        Delete = 2,
        Restore = 3,
        Move = 4
    }

    public enum Class
    {
        Permanent = 1,
        Optional = 2,
        Disposable = 3
    }

    public enum IsDelete
    {
        No = 0,
        Yes = 1
    }

    public enum WorkAreaType
    {
        Upload = 1,
        SMTPUpload = 2,
        RenameUpload = 3,
        SplitUpload = 4,
        UtilityUpload = 5
    }

    /// <summary>
    /// To use eFileFlowShare
    /// </summary>
    public enum Status
    {
        Send = 1,
        Saved = 2,
        Submitted = 3,
        Moved = 4,
        Rejected = 5
    }

    /// <summary>
    /// To use EdFormsShare
    /// </summary>
    public enum EdFormsStatus
    {
        [Description("Unopened")]
        UnOpen = 1,
        [Description("Pending")]
        Pending = 2,
        [Description("Submitted")]
        Submit = 3,
        [Description("Saved")]
        Save = 4,
        [Description("Moved")]
        Move = 5
    }

    /// <summary>
    /// To use EdFormsFilter
    /// </summary>
    public enum EdFormsFilterStatus
    {
        [Description("Submitted")]
        Submit = 3,
        [Description("Openned")]
        Open = 2,
        [Description("Shared")]
        Share = 1,
        [Description("Moved")]
        Moved = 5
    }

    /// <summary>
    /// To use EdFormslogs
    /// </summary>
    public enum EdFormsLog
    {
        [Description("Share")]
        Share = 1,
        [Description("Open")]
        Open = 2,
        [Description("Save")]
        Save = 3,
        [Description("Submit")]
        Submit = 4,
        [Description("Move")]
        Move = 5
    }

    public enum Folders
    {
        [Description("eFileFlow")]
        EFileFlow = 1,
        [Description("eFileShare")]
        EFileShare = 2,
        [Description("inbox")]
        inbox = 3,
        [Description("workArea")]
        WorkArea = 4,
        [Description("fileFolder")]
        FileFolder = 5,
        [Description("viewer")]
        ViewerDownload = 6,
        [Description("eFileSplit")]
        EFileSplit = 7,
        [Description("archive")]
        Archive = 8,
        [Description("edForms")]
        EdForms = 9,
        [Description("edFormsShare")]
        EdFormsShare = 10,
    }

    public static string GetEnumDescription(Enum value)
    {
        FieldInfo fi = value.GetType().GetField(value.ToString());

        DescriptionAttribute[] attributes =
            (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
        if (attributes != null && attributes.Length > 0)
            return attributes[0].Description;
        else
            return value.ToString();
    }

    public enum WeekDays
    {
        Sunday,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        AllDays
    }

    public enum DocumentsAndFoldersLogs
    {
        Share = 1,
        Edit = 2,
        View = 3,
        Preview = 4,
        Delete = 5,
        Rename = 6,
        DocIndex = 7,
        Download = 8,
        BookView = 9,
        Create = 10,
        EditReminder = 11,
        Upload = 12,
        Move = 13,
        Split = 14,
        Submit = 15,
        Order = 16,
        Archive = 17,
        Copy = 18,
        Restore = 19,
        Lock = 20,
        Unlock = 21,
        WorkAreaMoveUpload = 22,
        WorkAreaSplitUpload = 23,
        InboxUpload = 24,
        PendingUpload = 25,
        FileSplitUpload = 26,
        EFileSplitUpload = 27,
        CopyFolderUpload = 28,
        BatchUpload = 29,
        Scan2EdfUpload = 30,
        ScannedUpload = 31
    }

    public enum UserLogs
    {
        Create = 1,
        Edit = 2,
        Delete = 3,
        View = 4,
        ChangePassword = 5,
        EditGroup = 6
    }

    public enum DocumentsAndFoldersLogsSubAction
    {
        FileNotFound = 1
    }

    public enum TypeOfTable
    {
        OtherTable = 1
    }

    public enum CopyFoldersFromOneUserToAnother
    {
        Move = 1
    }

    public enum RetentionCompliance
    {
        [Description("Yes")]
        Yes = 0,
        [Description("No")]
        No = 1,
        [Description("Don't Know")]
        DontKnow = 2
    }

    public enum EstimatorPricing
    {
        [Description("Standard Banker Boxes")]
        StandardBox = 1,
        [Description("Large Banker Box ")]
        LargeBox = 2,
        [Description("Lateral File Cabinet (Active Files)")]
        LateralCabinet = 3,
        [Description("Vertical Cabinets (Files)")]
        VerticalCabinet = 4,
        [Description("Blue Prints")]
        BluePrints = 5,
        [Description("Microfilm & Microfiche Rolls")]
        MicrofilmAndMicroficheRolls = 6
    }

    public enum RecordType
    {
        Student_CUM_Files = 1,
        Special_Ed_Department_Files = 2,
        Personnel_HR_Department_Files = 3,
        Payroll_Department_Files = 4,
        Business_Department_Files = 5,
        Risk_Management_Workers_Comp_Files = 6,
        Board_Superintendent_Office_Files = 7,
        Facilities_Department_Files = 8,
        Other_Files = 9,
        Default = 10
    }

    public enum OcrStatus
    {
        NoOcr = 0,
        OcrRequested = 1,
        OcrDone = 2
    }
}