﻿/// <summary>
/// Estimator Details Model Class
/// </summary>
public class EstimatorDetails
{
    /// <summary>
    /// Gets or sets the state identifier.
    /// </summary>
    /// <value>
    /// The state identifier.
    /// </value>
    public string StateId { get; set; }

    /// <summary>
    /// Gets or sets the street.
    /// </summary>
    /// <value>
    /// The street.
    /// </value>
    public string Street { get; set; }

    /// <summary>
    /// Gets or sets the city.
    /// </summary>
    /// <value>
    /// The city.
    /// </value>
    public string City { get; set; }

    /// <summary>
    /// Gets or sets the zip code.
    /// </summary>
    /// <value>
    /// The zip code.
    /// </value>
    public string ZipCode { get; set; }

    /// <summary>
    /// Gets or sets the name of the school.
    /// </summary>
    /// <value>
    /// The name of the school.
    /// </value>
    public string SchoolName { get; set; }

    /// <summary>
    /// Gets or sets the name.
    /// </summary>
    /// <value>
    /// The name.
    /// </value>
    public string Name { get; set; }

    /// <summary>
    /// Gets or sets the position.
    /// </summary>
    /// <value>
    /// The position.
    /// </value>
    public string Position { get; set; }

    /// <summary>
    /// Gets or sets the email.
    /// </summary>
    /// <value>
    /// The email.
    /// </value>
    public string Email { get; set; }

    /// <summary>
    /// Gets or sets the phone number.
    /// </summary>
    /// <value>
    /// The phone number.
    /// </value>
    public string PhoneNumber { get; set; }

    /// <summary>
    /// Gets or sets the extension.
    /// </summary>
    /// <value>
    /// The extension.
    /// </value>
    public string Extension { get; set; }

    /// <summary>
    /// Gets or sets the standard box count.
    /// </summary>
    /// <value>
    /// The standard box count.
    /// </value>
    public int? StdBoxCount { get; set; }

    /// <summary>
    /// Gets or sets the large box count.
    /// </summary>
    /// <value>
    /// The large box count.
    /// </value>
    public int? LargeBoxCount { get; set; }

    /// <summary>
    /// Gets or sets the lateral cabinet count.
    /// </summary>
    /// <value>
    /// The lateral cabinet count.
    /// </value>
    public int? LateralCabinetCount { get; set; }

    /// <summary>
    /// Gets or sets the vertical cabinet count.
    /// </summary>
    /// <value>
    /// The vertical cabinet count.
    /// </value>
    public int? VerticalCabinetCount { get; set; }

    /// <summary>
    /// Gets or sets the blue prints count.
    /// </summary>
    /// <value>
    /// The blue prints count.
    /// </value>
    public int? BluePrintsCount { get; set; }

    /// <summary>
    /// Gets or sets the microfiche rolls count.
    /// </summary>
    /// <value>
    /// The microfiche rolls count.
    /// </value>
    public int? MicroficheRollsCount { get; set; }

    /// <summary>
    /// Gets or sets the record type identifier.
    /// </summary>
    /// <value>
    /// The record type identifier.
    /// </value>
    public int RecordTypeId { get; set; }
}