using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using DataQuicker2.Framework;
using Shinetech.DAL;
using System.Collections.Specialized;
using Newtonsoft.Json;
using System.IO;
using Shinetech.Engines;

/// <summary>
/// Summary description for FileFolderManagement
/// </summary>
public class FileFolderManagement
{
    public static string ConnectionName = "Default";
    private static readonly string ConnectionString =
        ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
    //EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();

    public FileFolderManagement()
    {

    }

    public static bool ExistBox(string name, string uid)
    {
        try
        {
            Box box = new Box();
            ObjectQuery query = box.CreateQuery();
            query.SetCriteria(box.UserId, uid);
            query.SetCriteria(box.BoxName, name);

            DataTable dt = new DataTable();
            query.Fill(dt);

            return dt.Rows.Count > 0;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return true;
    }

    public static void CreatNewDivider(Divider tab)
    {

        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            tab.Create(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }
    }

    public static int CreatNewFileFolder(FileFolder folder)
    {

        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            folder.Create(transaction);
            transaction.Commit();

            return folder.FolderID.Value;
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }
    }

    public static int CreatNewFileFolder(FileFolder folder, Dictionary<string, Divider> list)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            folder.OtherInfo.Value = list.Count;
            folder.Create(transaction);

            foreach (Divider item in list.Values)
            {
                item.OfficeID.Value = folder.OfficeID.Value;
                item.EffID.Value = folder.FolderID.Value;
                item.Create(transaction);
            }

            transaction.Commit();
            return folder.FolderID.Value;   //todo:binguo
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }
    }

    public static int CreatNewFileFolder(FileFolder folder, Divider divider)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            folder.OtherInfo.Value = 1;
            folder.Create(transaction);

            divider.OfficeID.Value = folder.OfficeID.Value;
            divider.EffID.Value = folder.FolderID.Value;
            divider.Create(transaction);

            transaction.Commit();
            return folder.FolderID.Value;
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }
    }


    public static bool IsExistDivider(string name, string officeid)
    {
        try
        {
            Divider tab = new Divider();
            ObjectQuery query = tab.CreateQuery();
            query.SetCriteria(tab.OfficeID, officeid);
            query.SetCriteria(tab.Name, name);

            DataTable dt = new DataTable();
            query.Fill(dt);

            return dt.Rows.Count > 0;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return true;
    }

    public static int GetDividerId(string guid, string officeid)
    {
        string sqlstr = "SELECT DividerID FROM dbo.Divider " +
            "WHERE OfficeID='" + officeid + "' AND " + "GUID='" + guid + "'";

        SqlHelper sql = new SqlHelper("Default");

        try
        {
            object t = sql.ExecuteScalar(sqlstr);
            return Convert.ToInt32(t);
        }
        catch (Exception ex)
        {
            return -1;
        }
    }

    public static bool IsExistFileFolder(string strFoldeName)
    {
        try
        {
            FileFolder folder = new FileFolder();

            ObjectQuery query = folder.CreateQuery();
            query.SetCriteria(folder.FolderName, strFoldeName);

            DataTable dt = new DataTable();
            query.Fill(dt);

            return dt.Rows.Count > 0;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return true;
    }


    public static DataTable GetFileFolders(string folderName, string firstname, string lastname, string date, string officeid)
    {
        FileFolder folder = new FileFolder();
        ObjectQuery query = folder.CreateQuery();

        query.SetCriteria(folder.OfficeID1, officeid); //change OfficeID to OfficeID1 by johnlu655 in 2013-11-23

        if (!string.IsNullOrEmpty(folderName))
            query.SetCriteria(folder.FolderName, Operator.BothSidesLike, folderName);

        if (!string.IsNullOrEmpty(firstname))
            query.SetCriteria(folder.FirstName, Operator.BothSidesLike, firstname);

        if (!string.IsNullOrEmpty(lastname))
            query.SetCriteria(folder.Lastname, Operator.BothSidesLike, lastname);

        if (!string.IsNullOrEmpty(date))
            query.SetCriteria(folder.FileNumber, Operator.BothSidesLike, date);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static DataTable GetFileFolders(string keyname, string officeid)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            // keyname = string.Join(",", keyname.Split(new char[0], StringSplitOptions.RemoveEmptyEntries));
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SelectFileFolders";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@Key", keyname);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@OfficeID", officeid);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static Int32 GetFileFolderWithItsName(string keyname, string officeid)
    {
        FileFolder folder = new FileFolder();
        ObjectQuery query = folder.CreateQuery();

        query.SetCriteria(folder.OfficeID1, officeid);

        string[] folderNames = keyname.Split(new string[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);

        if (folderNames.Length > 1)
        {
            query.SetCriteria(folder.FirstName, folderNames[0]);
            query.SetCriteria(folder.Lastname, folderNames[1]);
        }
        else
        {
            query.SetCriteria(folder.FolderName, keyname);
        }


        query.SetSelectFields(folder.FolderID);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            if (table.Rows.Count <= 0)
            {
                return -1;
            }

            return Convert.ToInt32(table.Rows[0][0]);
        }
        catch (Exception ex)
        {
            return -1;
        }

    }

    //public static DataTable GetFileFoldersWithPrefx(string keyname, string officeid)
    //{
    //    SqlConnection connection = new SqlConnection(ConnectionString);

    //    try
    //    {
    //        connection.Open();
    //        SqlCommand command = new SqlCommand();
    //        command.Connection = connection;
    //        command.CommandText = "eFileFolder_SelectFileFoldersWithPrefx";
    //        command.CommandType = CommandType.StoredProcedure;

    //        SqlParameter param1 = command.Parameters.AddWithValue("@Key", keyname);
    //        param1.Direction = ParameterDirection.Input;
    //        param1.DbType = DbType.String;

    //        SqlParameter param2 = command.Parameters.AddWithValue("@OfficeID", officeid);
    //        param2.Direction = ParameterDirection.Input;
    //        param2.DbType = DbType.String;

    //        SqlDataAdapter dataAdapter = new SqlDataAdapter();
    //        dataAdapter.SelectCommand = command;

    //        DataTable table = new DataTable();

    //        dataAdapter.Fill(table);

    //        return table;
    //    }
    //    catch (Exception ex)
    //    {
    //        return null;
    //    }
    //    finally
    //    {
    //        connection.Close();
    //    }
    //}

    public static DataTable GetFileFoldersWithPrefx(string keyname, string officeid)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SelectFileFoldersWithPrefx_karan";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@Key", keyname);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@OfficeID", officeid);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetFileFoldersNameWithPrefx1(string keyname, string officeid)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SelectFileFoldersNameWithPrefx1";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@Key", keyname);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@OfficeID", officeid);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetAllUsersForAdmin()
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "GetAllUsersForAdmin";
            command.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetRequestFiles(string uid)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SearchRequestFiles";
            //command.CommandText = "eFileFolder_SearchBoxFiles";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@UID", uid);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetRequestBoxes(string uid)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SearchBoxFiles";
            //command.CommandText = "eFileFolder_SearchBoxFiles";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@UID", uid);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetOfficeFileFolders(string folderName, string firstname, string lastname, string date, string officeid)
    {
        FileFolder folder = new FileFolder();
        ObjectQuery query = folder.CreateQuery();

        query.SetCriteria(folder.OfficeID1, officeid);

        if (!string.IsNullOrEmpty(folderName))
            query.SetCriteria(folder.FolderName, Operator.BothSidesLike, folderName);

        if (!string.IsNullOrEmpty(firstname))
            query.SetCriteria(folder.FirstName, Operator.BothSidesLike, firstname);

        if (!string.IsNullOrEmpty(lastname))
            query.SetCriteria(folder.Lastname, Operator.BothSidesLike, lastname);

        if (!string.IsNullOrEmpty(date))
            query.SetCriteria(folder.FileNumber, Operator.BothSidesLike, date);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static DataTable GetOfficeFileFolders(string keyname, string officeid)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SelectOfficeFileFolders";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@Key", keyname);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@OfficeID", officeid);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetOfficeFileFoldersDocs(string keyname, string officeid)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SelectOfficeFileFoldersDocs";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@Key", keyname);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@OfficeID", officeid);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetOfficeFileFoldersWithPrefx(string keyname, string officeid)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SelectOfficeFileFoldersWithPrefx_karan";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@Key", keyname);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@OfficeID", officeid);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetOfficeFileFoldersWithPrefxDoc(string keyname, string officeid)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {

            keyname = string.Join(",", keyname.Split(new char[0], StringSplitOptions.RemoveEmptyEntries));
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SelectFileFoldersWithPrefxDoc";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@Key", keyname);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@OfficeID", officeid);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetFileFoldersByUserID(string uid)
    {
        FileFolder folder = new FileFolder();
        DataTable table = new DataTable();
        try
        {
            ObjectQuery query = folder.CreateQuery();
            query.SetCriteria(folder.OfficeID1, uid);
            query.SetSelectFields(folder.FolderID, folder.FolderName);
            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }



    public static DataTable GetRestoredeFolders()
    {
        FileFolder folder = new FileFolder();
        Account user = new Account();
        DataTable table = new DataTable();
        try
        {
            ObjectQuery query = folder.CreateQuery();
            query.SetAssociation(new IColumn[] { folder.SavedID }, new IColumn[] { user.UID }, AssociationType.LeftJoin);
            query.SetCriteria(folder.OfficeID, Operator.Equal, "00000000-0000-0000-0000-000000000000");
            query.SetCriteria(folder.OfficeID1, Operator.Equal, "00000000-0000-0000-0000-000000000000");
            query.SetSelectFields(folder.FolderID, folder.FolderName, folder.OfficeID, folder.SavedID, folder.DeleteDate, folder.FileNumber,
                user.Name, user.Firstname, user.Lastname);
            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetFileFoldersWithDisplayName(string userid, string officeid)
    {
        string sqlstr = "SELECT FolderID,DisplayName=(FolderName+'   ('+convert(varchar(10),DOB,101)+')') FROM dbo.FileFolder " +
            "WHERE OfficeID='" + userid + "' OR" +
            "(OfficeID1='" + officeid + "' AND SecurityLevel!='0'" + ") ORDER BY DOB DESC";
        SqlHelper sql = new SqlHelper("Default");
        DataTable table = new DataTable();
        try
        {
            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static DataTable GetMyFileFoldersWithDisplayName(string userid)
    {
        string sqlstr =
            "SELECT FolderID,DisplayName=(FolderName+'('+convert(varchar(10),DOB,101)+')') FROM dbo.FileFolder " +
            "WHERE OfficeID='" + userid + "'ORDER BY DOB DESC";
        SqlHelper sql = new SqlHelper("Default");
        DataTable table = new DataTable();
        try
        {
            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static DataTable GetFileFoldersInfor(string officeid1)
    {
        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchFileFoldersInfor] '" + officeid1 + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

        //FileFolder folder = new FileFolder();
        //Account user = new Account();
        //Reminder item = new Reminder();
        //DataTable dt = new DataTable("Folder");
        //try
        //{
        //    ObjectQuery query = folder.CreateQuery();
        //    query.SetCriteria(folder.OfficeID1, officeid1);
        //    //query.SetAssociation(new IColumn[] { folder.OfficeID }, new IColumn[] { user.UID }, AssociationType.LeftJoin);
        //    query.SetAssociation(new IColumn[] { folder.FolderID }, new IColumn[] { item.EffID }, AssociationType.LeftJoin);
        //    query.SetAssociation(new IColumn[] { item.UID }, new IColumn[] { user.UID }, AssociationType.LeftJoin);

        //    query.SetSelectFields(folder.FolderID, folder.FolderName, folder.FirstName, folder.Lastname,folder.FileNumber,
        //        folder.Alert1, folder.Alert2, folder.Alert3, folder.Alert4, folder.Comments, item.DOB, folder.OtherInfo, folder.OtherInfo2,
        //        folder.OfficeID, folder.OfficeID1, folder.SecurityLevel, folder.FolderCode,folder.LastDate, user.Name);

        //    query.SetOrderBy(folder.LastDate,false);
        //    query.Fill(dt);
        //}
        //catch (Exception ex)
        //{
        //    new ApplicationException(ex.Message);
        //}

        //return dt;
    }

    public static DataTable GetFileFoldersForReport(string officeid1)
    {
        DataTable ds = new DataTable("FileFolder");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SelectFileFoldersForReport] '" + officeid1 + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(ds, sqlstr);
            return ds;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return ds;
    }

    public static DataTable GetDocumentsForReport(string officeid1)
    {
        DataTable ds = new DataTable("FileDocument");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SelectDocumentForReport] '" + officeid1 + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(ds, sqlstr);
            return ds;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return ds;
    }

    public static DataTable GetFileForms()
    {
        FileForm form = new FileForm();
        Account user = new Account();

        DataTable dt = new DataTable("FileForm");
        try
        {
            ObjectQuery query = form.CreateQuery();
            query.SetAssociation(new IColumn[] { form.UserID }, new IColumn[] { user.UID }, AssociationType.LeftJoin);
            query.SetSelectFields(form.FormId, form.FileQuantity, form.OfficeName, form.PhoneNumber, form.UserID,
                                form.RequestedBy, form.RequestDate, form.ConvertDate, form.Comment, form.Status,
                                user.Name, user.Firstname, user.Lastname);

            query.SetOrderBy(form.CreateDate, false);
            query.Fill(dt);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return dt;
    }

    public static DataTable GetFileFormsWithStatus()
    {
        FormFile file = new FormFile();
        FileForm form = new FileForm();
        Account user = new Account();

        DataTable dt = new DataTable("FileForm");
        try
        {
            ObjectQuery query = file.CreateQuery();
            query.SetAssociation(new IColumn[] { file.UserID }, new IColumn[] { user.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { file.FormId }, new IColumn[] { form.FormId }, AssociationType.LeftJoin);

            query.SetSelectFields(file.FileID, file.FileName, file.FileNumber, file.FirstName, file.LastName, file.YearDate,
                file.UploadDate, file.ConvertDate, file.CreateDate, file.Flag, file.Status,
                form.FormId, form.FileQuantity, form.OfficeName, form.UserName, form.PhoneNumber, form.UserID,
                                form.RequestedBy, form.RequestDate, form.Comment,
                                user.Name);
            query.TopN = 1000;
            query.SetCriteria(file.FirstName, Operator.NotEqual, "");
            query.SetOrderBy(form.CreateDate, false);
            query.Fill(dt);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return dt;
    }

    public static DataTable GetLogFormDocsAdmin()
    {
        return General_Class.GetLogFormDocsAdmin();
    }

    public static DataTable GetPublicHolidays()
    {
        return General_Class.GetPublicHolidays();
    }

    public static DataTable GetPublicHolidayDates(int year)
    {
        return General_Class.GetPublicHolidayDates(year);
    }

    public static string InsertPublicHoliday(string holidayName)
    {
        return General_Class.InsertPublicHoliday(holidayName);
    }

    public static string DeleteLogForm(int id)
    {
        //No need to pass switch
        return General_Class.DeleteLogForm(id, Sessions.UserId);
    }

    public static DataTable GetFilteredLogForms(string uid, string fileName, string folderName)
    {
        return General_Class.GetFilteredLogForms(uid, fileName, folderName);
    }

    public static int ChangeDefaultPassword(string name, string password)
    {
        return General_Class.ChangeDefaultPassword(name, password);
    }

    public static DataTable GetFileFormsByUID(string uid)
    {
        FileForm form = new FileForm();
        Account user = new Account();

        DataTable dt = new DataTable("FileForm");
        try
        {
            ObjectQuery query = form.CreateQuery();
            query.SetAssociation(new IColumn[] { form.UserID }, new IColumn[] { user.UID }, AssociationType.LeftJoin);
            query.SetSelectFields(form.FormId, form.FileQuantity, form.OfficeName, form.PhoneNumber, form.UserID,
                                form.RequestedBy, form.RequestDate, form.ConvertDate, form.Comment, form.Status,
                                user.Name, user.Firstname, user.Lastname);

            query.SetOrderBy(form.CreateDate, false);
            query.SetCriteria(form.UserID, uid);
            query.Fill(dt);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return dt;
    }

    public static DataTable GetFileFormsWithStatus(string uid)
    {
        FormFile file = new FormFile();
        FileForm form = new FileForm();
        Account user = new Account();

        DataTable dt = new DataTable("FileForm");
        try
        {
            ObjectQuery query = file.CreateQuery();
            query.SetAssociation(new IColumn[] { file.UserID }, new IColumn[] { user.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { file.FormId }, new IColumn[] { form.FormId }, AssociationType.LeftJoin);

            query.SetSelectFields(file.FileID, file.FileName, file.FileNumber, file.FirstName, file.LastName, file.YearDate,
                file.UploadDate, file.ConvertDate, file.CreateDate, file.Flag, file.Status,
                form.FormId, form.FileQuantity, form.OfficeName, form.UserName, form.PhoneNumber, form.UserID,
                                form.RequestedBy, form.RequestDate, form.Comment, user.Name);

            query.SetCriteria(file.FirstName, Operator.NotEqual, "");
            query.SetCriteria(file.UserID, uid);
            query.SetOrderBy(form.CreateDate, false);
            query.Fill(dt);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return dt;
    }


    public static DataTable GetFileFormsByUID(string uid, string key)
    {
        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchFileFormsByKey] '%" + key + "%' ," + "'" + uid + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetFileFormsWithStatusByUID(string uid, string key)
    {
        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchFileFormsWithStatusByKey] '%" + key + "%' ," + "'" + uid + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }


    public static DataTable GetAllFileFormsByKey(string key, string date)
    {
        DateTime v = DateTime.Parse(date, new CultureInfo("en-us", false));

        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllFileFormsByKeyDate] '%" + key + "%','" + v.ToShortDateString() + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetAllFileFormsWithStatusByKey(string key, string date)
    {
        DateTime v = DateTime.Parse(date, new CultureInfo("en-us", false));

        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllFileFormsWithStatusByKeyDate] '%" + key + "%','" + v.ToShortDateString() + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetAllFileFormsByKey(string key, string date, string date1)
    {
        DateTime v = DateTime.Parse(date, new CultureInfo("en-us", false));
        DateTime v1 = DateTime.Parse(date1, new CultureInfo("en-us", false));

        if (v > v1)
        {
            var temp = v1;
            v1 = v;
            v = temp;
        }

        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllFileFormsByKeyDateSE] '%" + key + "%','" + v.ToShortDateString() + "','" + v1.ToShortDateString() + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetAllFileFormsWithStatusByKey(string key, string date, string date1)
    {
        DateTime v = DateTime.Parse(date, new CultureInfo("en-us", false));
        DateTime v1 = DateTime.Parse(date1, new CultureInfo("en-us", false));

        if (v > v1)
        {
            var temp = v1;
            v1 = v;
            v = temp;
        }

        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllFileFormsWithStatusByKeyDateSE] '%" + key + "%','" + v.ToShortDateString() + "','" + v1.ToShortDateString() + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetAllFileFormsByKey(string key)
    {
        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllFileFormsByKey] '%" + key + "%'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetAllFileFormsWithStatusByKey(string key)
    {
        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllFileFormsWithStatusByKey] '%" + key + "%'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }


    public static DataTable GetAllPaperFilesByKey(string key, string date)
    {
        DateTime v = DateTime.Parse(date, new CultureInfo("en-us", false));

        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllPaperFilesByKeyDate] '%" + key + "%','" + v.ToShortDateString() + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetAllPaperFilesByKey(string key, string date, string date1)
    {
        DateTime v = DateTime.Parse(date, new CultureInfo("en-us", false));
        DateTime v1 = DateTime.Parse(date1, new CultureInfo("en-us", false));

        if (v > v1)
        {
            var temp = v1;
            v1 = v;
            v = temp;
        }

        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllPaperFilesByKeyDateSE] '%" + key + "%','" + v.ToShortDateString() + "','" + v1.ToShortDateString() + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetAllPaperFilesByKey(string key)
    {
        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchAllPaperFilesByKey] '%" + key + "%'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static DataTable GetUploadedFiles()
    {
        UploadFile uploadFile = new UploadFile();
        Account user = new Account();

        DataTable dt = new DataTable("UploadFile");
        try
        {
            ObjectQuery query = uploadFile.CreateQuery();
            query.SetAssociation(new IColumn[] { uploadFile.UserID }, new IColumn[] { user.UID }, AssociationType.LeftJoin);

            query.SetSelectFields(uploadFile.FileID, uploadFile.ConvertedDate, uploadFile.DividerId, uploadFile.FolderId, uploadFile.FolderName,
                    uploadFile.FirstName, uploadFile.LastName, uploadFile.Flag, uploadFile.FormFileID, uploadFile.UploadDate, uploadFile.UserID,
                    user.Name);

            query.SetOrderBy(uploadFile.UploadDate, false);
            query.Fill(dt);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return dt;
    }

    public static DataTable GetFormFiles(String key)
    {
        FormFile ffile = new FormFile();
        DataTable table = new DataTable("FormFile");

        try
        {
            ObjectQuery query = ffile.CreateQuery();
            query.SetCriteria(ffile.FormId, key);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetFormFilesById(string id)
    {
        FormFile ffile = new FormFile();
        DataTable table = new DataTable("FormFile");

        try
        {
            ObjectQuery query = ffile.CreateQuery();
            query.SetCriteria(ffile.FormId, id);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetPaperFiles(string userId, string boxkey)
    {
        DataTable table = new DataTable("PagerFile");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchPagerFilesByKey] '%" + boxkey + "%' ," + "'" + userId + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
        }

        return table;

    }



    public static DataTable GetPaperFiles(String userId, int boxId)
    {
        PaperFile pfile = new PaperFile();
        DataTable table = new DataTable("PagerFile");
        Account account = new Account();
        Box box = new Box();
        try
        {
            ObjectQuery query = pfile.CreateQuery();
            query.SetCriteria(pfile.FirstName, Operator.NotEqual, "");
            query.SetCriteria(pfile.UserID, userId);
            query.SetCriteria(pfile.Flag, false);
            query.SetCriteria(box.BoxId, boxId);

            query.SetAssociation(new IColumn[] { pfile.UserID }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { pfile.BoxId }, new IColumn[] { box.BoxId }, AssociationType.LeftJoin);

            query.SetSelectFields(pfile.FileID, pfile.FileName, pfile.FileNumber, pfile.FirstName, pfile.LastName,
                pfile.CreateDate, pfile.UserID, pfile.BoxId, pfile.RequestDate, pfile.Flag, pfile.Status, box.BoxName, box.BoxNumber,
                box.Warehouse, box.Section, box.Row, box.Space, box.Other, box.DestroyDate, account.Name, box.DepartmentName, box.ScannedDate, box.RetentionClass, box.BoxPickupDate);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetPaperFilesByBoxId(string boxId)
    {
        PaperFile pfile = new PaperFile();
        DataTable table = new DataTable("PagerFile");

        try
        {
            ObjectQuery query = pfile.CreateQuery();
            query.SetCriteria(pfile.BoxId, boxId);
            query.SetOrderBy(pfile.FileIndex, true);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetPaperFiles(String userId)
    {
        PaperFile pfile = new PaperFile();
        Account account = new Account();
        Box box = new Box();

        DataTable table = new DataTable("PagerFile");

        try
        {
            ObjectQuery query = pfile.CreateQuery();
            query.SetCriteria(pfile.FirstName, Operator.NotEqual, "");
            query.SetCriteria(pfile.UserID, userId);
            query.SetCriteria(pfile.Flag, false);

            query.SetAssociation(new IColumn[] { pfile.UserID }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { pfile.BoxId }, new IColumn[] { box.BoxId }, AssociationType.LeftJoin);

            query.SetSelectFields(pfile.FileID, pfile.FileName, pfile.FileNumber, pfile.FirstName, pfile.LastName,
                pfile.CreateDate, pfile.UserID, pfile.BoxId, pfile.RequestDate, pfile.Flag, pfile.Status, box.BoxName, box.BoxNumber,
                box.Warehouse, box.Section, box.Row, box.Space, box.Other, box.DestroyDate, account.Name);
            query.SetOrderBy(pfile.CreateDate, false);
            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetBoxPaperFiles(String userId)
    {
        Account account = new Account();
        Box box = new Box();

        DataTable table = new DataTable("PagerFile");

        try
        {
            ObjectQuery query = box.CreateQuery();
            // query.SetCriteria(pfile.FirstName, Operator.NotEqual, "");
            query.SetCriteria(box.UserId, userId);
            query.SetOrderBy(box.CreateDate, false);
            //query.SetCriteria(box.Flag, false);

            //query.SetAssociation(new IColumn[] { pfile.UserID }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
            // query.SetAssociation(new IColumn[] { pfile.BoxId }, new IColumn[] { box.BoxId }, AssociationType.LeftJoin);

            //query.SetSelectFields(pfile.FileID, pfile.FileName, pfile.FileNumber, pfile.FirstName, pfile.LastName,
            //    pfile.CreateDate, pfile.UserID, pfile.BoxId, pfile.RequestDate, pfile.Flag, box.BoxName, box.BoxNumber,
            //    box.Warehouse, box.Section, box.Row, box.Space, box.Other, box.DestroyDate, account.Name);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetPaperFiles(String userId, string firstName, string lastName, string boxName,
        string boxNumber, string fileName, string fileNumber)
    {
        PaperFile pfile = new PaperFile();
        Account account = new Account();
        Box box = new Box();

        DataTable table = new DataTable("PagerFile");

        try
        {
            ObjectQuery query = pfile.CreateQuery();

            query.TopN = 200;

            if (!string.IsNullOrEmpty(firstName))
            {
                query.SetCriteria(pfile.FirstName, Operator.BothSidesLike, firstName);
            }
            else
            {
                query.SetCriteria(pfile.FirstName, Operator.NotEqual, "");
            }

            if (!string.IsNullOrEmpty(lastName))
            {
                query.SetCriteria(pfile.LastName, Operator.BothSidesLike, lastName);
            }
            else
            {
                query.SetCriteria(pfile.LastName, Operator.NotEqual, "");
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                query.SetCriteria(pfile.FileName, Operator.BothSidesLike, fileName);
            }

            if (!string.IsNullOrEmpty(fileNumber))
            {
                query.SetCriteria(pfile.FileNumber, Operator.BothSidesLike, fileNumber);
            }

            if (!string.IsNullOrEmpty(boxName))
            {
                query.SetCriteria(box.BoxName, Operator.BothSidesLike, boxName);
            }

            if (!string.IsNullOrEmpty(boxNumber))
            {
                query.SetCriteria(box.BoxNumber, Operator.BothSidesLike, boxNumber);
            }

            query.SetOrderBy(pfile.RequestDate, false);

            query.SetAssociation(new IColumn[] { pfile.UserID }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { pfile.BoxId }, new IColumn[] { box.BoxId }, AssociationType.LeftJoin);

            query.SetSelectFields(pfile.FileID, pfile.FileName, pfile.FileNumber, pfile.FirstName, pfile.LastName,
                pfile.CreateDate, pfile.UserID, pfile.BoxId, pfile.RequestDate, pfile.Flag, box.BoxName, box.BoxNumber,
                box.Warehouse, box.Section, box.Row, box.Space, box.Other, box.DestroyDate, account.Name);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }


    public static DataTable GetBoxPaperFiles(String userId, string firstName, string lastName, string boxName, string fileName, string destroyDate, string retentionClass)
    {
        PaperFile pfile = new PaperFile();
        Account account = new Account();
        Box box = new Box();

        DataTable table = new DataTable("PagerFile");

        try
        {
            ObjectQuery query = box.CreateQuery();

            query.TopN = 200;

            if (!string.IsNullOrEmpty(firstName))
            {
                query.SetCriteria(pfile.FirstName, Operator.BothSidesLike, firstName);
            }
            else
            {
                query.SetCriteria(pfile.FirstName, Operator.NotEqual, "");
            }

            if (!string.IsNullOrEmpty(lastName))
            {
                query.SetCriteria(pfile.LastName, Operator.BothSidesLike, lastName);
            }
            else
            {
                query.SetCriteria(pfile.LastName, Operator.NotEqual, "");
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                query.SetCriteria(pfile.FileName, Operator.BothSidesLike, fileName);
            }


            if (!string.IsNullOrEmpty(boxName))
            {
                query.SetCriteria(box.BoxName, Operator.BothSidesLike, boxName);
            }

            if (!string.IsNullOrEmpty(destroyDate))
            {
                query.SetCriteria(box.DestroyDate, Convert.ToDateTime(destroyDate));
            }

            if (!string.IsNullOrEmpty(retentionClass))
            {
                query.SetCriteria(box.RetentionClass, Convert.ToInt16(retentionClass));
            }

            query.SetCriteria(box.UserId, userId);
            query.SetOrderBy(pfile.RequestDate, false);


            query.SetAssociation(new IColumn[] { box.UserId }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { box.BoxId }, new IColumn[] { pfile.BoxId }, AssociationType.LeftJoin);

            query.SetSelectFields(box.BoxId, box.BoxName, box.BoxNumber, box.Warehouse, box.Section, box.Row, box.Space, box.Other, box.DestroyDate, box.DepartmentName, box.RetentionClass, box.ScannedDate, box.BoxPickupDate, account.Name);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetPaperFiles(String userId, string firstName, string lastName, string boxName,
        string fileName)
    {
        PaperFile pfile = new PaperFile();
        Account account = new Account();
        Box box = new Box();

        DataTable table = new DataTable("PagerFile");

        try
        {
            ObjectQuery query = pfile.CreateQuery();

            if (!string.IsNullOrEmpty(firstName))
            {
                query.SetCriteria(pfile.FirstName, Operator.BothSidesLike, firstName);
            }
            else
            {
                query.SetCriteria(pfile.FirstName, Operator.NotEqual, "");
            }

            if (!string.IsNullOrEmpty(lastName))
            {
                query.SetCriteria(pfile.LastName, Operator.BothSidesLike, lastName);
            }
            else
            {
                query.SetCriteria(pfile.LastName, Operator.NotEqual, "");
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                query.SetCriteria(pfile.FileName, Operator.BothSidesLike, fileName);
            }

            if (!string.IsNullOrEmpty(boxName))
            {
                query.SetCriteria(box.BoxName, Operator.BothSidesLike, boxName);
            }

            query.SetCriteria(pfile.UserID, userId);
            query.SetOrderBy(pfile.RequestDate, false);

            query.SetAssociation(new IColumn[] { pfile.UserID }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { pfile.BoxId }, new IColumn[] { box.BoxId }, AssociationType.LeftJoin);

            query.SetSelectFields(pfile.FileID, pfile.FileName, pfile.FileNumber, pfile.FirstName, pfile.LastName,
                pfile.CreateDate, pfile.UserID, pfile.BoxId, pfile.RequestDate, pfile.Flag, box.BoxName, box.BoxNumber,
                box.Warehouse, box.Section, box.Row, box.Space, box.Other, box.DestroyDate, account.Name);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetPaperFiles(String userId, string firstName, string lastName, string boxName,
        string fileName, int boxId)
    {
        PaperFile pfile = new PaperFile();
        Account account = new Account();
        Box box = new Box();

        DataTable table = new DataTable("PagerFile");

        try
        {
            ObjectQuery query = pfile.CreateQuery();

            query.SetCriteria(box.BoxId, boxId);

            if (!string.IsNullOrEmpty(firstName))
            {
                query.SetCriteria(pfile.FirstName, Operator.BothSidesLike, firstName);
            }
            else
            {
                query.SetCriteria(pfile.FirstName, Operator.NotEqual, "");
            }

            if (!string.IsNullOrEmpty(lastName))
            {
                query.SetCriteria(pfile.LastName, Operator.BothSidesLike, lastName);
            }
            else
            {
                query.SetCriteria(pfile.LastName, Operator.NotEqual, "");
            }

            if (!string.IsNullOrEmpty(fileName))
            {
                query.SetCriteria(pfile.FileName, Operator.BothSidesLike, fileName);
            }

            if (!string.IsNullOrEmpty(boxName))
            {
                query.SetCriteria(box.BoxName, Operator.BothSidesLike, boxName);
            }

            query.SetCriteria(pfile.UserID, userId);

            query.SetOrderBy(pfile.RequestDate, false);

            query.SetAssociation(new IColumn[] { pfile.UserID }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { pfile.BoxId }, new IColumn[] { box.BoxId }, AssociationType.LeftJoin);

            query.SetSelectFields(pfile.FileID, pfile.FileName, pfile.FileNumber, pfile.FirstName, pfile.LastName,
                pfile.CreateDate, pfile.UserID, pfile.BoxId, pfile.RequestDate, pfile.Flag, box.BoxName, box.BoxNumber,
                box.Warehouse, box.Section, box.Row, box.Space, box.Other, box.DestroyDate, account.Name);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetPaperFiles()
    {
        PaperFile pfile = new PaperFile();
        Account account = new Account();
        Box box = new Box();

        DataTable table = new DataTable("PagerFile");

        try
        {
            ObjectQuery query = pfile.CreateQuery();
            query.SetCriteria(pfile.FirstName, Operator.NotEqual, "");

            query.SetAssociation(new IColumn[] { pfile.UserID }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { pfile.BoxId }, new IColumn[] { box.BoxId }, AssociationType.LeftJoin);
            query.SetOrderBy(pfile.RequestDate, false);

            query.SetSelectFields(pfile.FileID, pfile.FileName, pfile.FileNumber, pfile.FirstName, pfile.LastName,
                pfile.CreateDate, pfile.UserID, pfile.BoxId, pfile.RequestDate, pfile.Flag, pfile.Status, box.BoxName, box.BoxNumber,
                box.Warehouse, box.Section, box.Row, box.Space, box.Other, account.Name);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetRequestPaperFiles()
    {
        PaperFile pfile = new PaperFile();
        Account account = new Account();
        Box box = new Box();

        DataTable table = new DataTable("PagerFile");

        try
        {
            ObjectQuery query = pfile.CreateQuery();
            query.SetCriteria(pfile.FirstName, Operator.NotEqual, "");
            query.SetCriteria(pfile.Flag, true);

            query.SetAssociation(new IColumn[] { pfile.UserID }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { pfile.BoxId }, new IColumn[] { box.BoxId }, AssociationType.LeftJoin);
            query.SetOrderBy(pfile.RequestDate, false);

            query.SetSelectFields(pfile.FileID, pfile.FileName, pfile.FileNumber, pfile.FirstName, pfile.LastName,
                pfile.CreateDate, pfile.UserID, pfile.BoxId, pfile.RequestDate, pfile.Flag, box.BoxName, box.BoxNumber,
                box.Warehouse, box.Section, box.Row, box.Space, box.Other, account.Name);

            query.SetOrderBy(pfile.RequestDate, false);
            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }


    public static DataTable GetUploadFile(string firstName, string lastName, string uid)
    {
        UploadFile ufile = new UploadFile();
        DataTable table = new DataTable("UploadFile");

        try
        {
            ObjectQuery query = ufile.CreateQuery();
            query.SetCriteria(ufile.UserID, uid);
            query.SetCriteria(ufile.FirstName, firstName);
            query.SetCriteria(ufile.LastName, lastName);

            query.SetOrderBy(ufile.FileID, false);

            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;

    }

    public static DataTable GetFileForms(String key)
    {
        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchUploadFiles] '%" + key + "%'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }


    public static DataTable GetFileFoldersXML(string userName)
    {
        FileFolder folder = new FileFolder();
        DataTable dt = new DataTable("Folder");
        try
        {
            DataTable uTable = UserManagement.getUserIDByUserName(userName);
            if (uTable.Rows.Count == 0) return dt;

            Account user = new Account(uTable.Rows[0][0].ToString());
            if (user.IsExist)
            {
                string officeID1 = string.Empty;

                if (user.RoleID.Value == "Offices")
                {
                    officeID1 = user.UID.Value;
                }
                else if (user.RoleID.Value == "Users")
                {
                    officeID1 = user.OfficeUID.Value;
                }

                ObjectQuery query = folder.CreateQuery();
                query.SetCriteria(folder.OfficeID1, officeID1);
                query.SetAssociation(new IColumn[] { folder.OfficeID }, new IColumn[] { user.UID }, AssociationType.LeftJoin);

                query.SetSelectFields(folder.FolderID, folder.FolderName, folder.FirstName, folder.Lastname, folder.DOB, folder.FileNumber, user.Name);
                query.Fill(dt);

            }


        }
        catch (Exception ex)
        {
        }

        return dt;
    }

    public static bool UpdateFileFolder(FileFolder folder)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            folder.Update(transaction);
            transaction.Commit();

            return true;
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }

    }


    public static DataTable GetReminders(string uid)
    {
        //Reminder reminder = new Reminder();
        FileFolder folder = new FileFolder();

        ObjectQuery query = folder.CreateQuery();

        query.SetCriteria(folder.OfficeID1, uid);
        query.SetOrderBy(folder.FolderID, false);
        // query.SetAssociation(new IColumn[] { reminder.EffID }, new IColumn[] { folder.FolderID }, AssociationType.LeftJoin);
        query.TopN = 10;
        query.SetSelectFields(folder.FolderID, folder.FolderName, folder.FirstName, folder.Lastname, folder.DOB, folder.CreatedDate, folder.OfficeID);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static int GetNumEFF(string uid)
    {
        int numEFF;
        string SQL = "select count(1)  from FileFolder where FileFolder.OfficeID1 = '";
        SQL += uid;
        SQL += "'";
        SqlHelper sqlH = new SqlHelper(ConnectionName);
        numEFF = Convert.ToInt16(sqlH.ExecuteScalar(SQL));
        return numEFF;
    }

    public static int GetUsedEFFNum(string uid)
    {
        int usedEFFNum;
        string SQL = "select count(1)  from FileFolder where FileFolder.OfficeID1 = '";
        SQL += uid;
        SQL += "'";
        //SQL += "' and OtherInfo2 > 0";
        SqlHelper sqlH = new SqlHelper(ConnectionName);
        usedEFFNum = Convert.ToInt16(sqlH.ExecuteScalar(SQL));
        return usedEFFNum;
    }

    public static int GetAllUsedEFFNum(string uid)
    {
        int allUsedEFFNum;
        string SQL = "select count(1)  from FileFolder where FileFolder.OfficeID1 = '";
        SQL += uid;
        SQL += "'";
        //SQL += "' and OtherInfo2 > 0";
        SqlHelper sqlH = new SqlHelper(ConnectionName);
        allUsedEFFNum = Convert.ToInt16(sqlH.ExecuteScalar(SQL));
        return allUsedEFFNum;
    }

    public static int GetUnUsedEFFNum(string uid)
    {
        int unUsedEFFNum;
        string SQL = "select count(1)  from FileFolder where FileFolder.OfficeID1 = '";
        SQL += uid;
        SQL += "' and OtherInfo2 <= 0";
        SqlHelper sqlH = new SqlHelper(ConnectionName);
        unUsedEFFNum = Convert.ToInt16(sqlH.ExecuteScalar(SQL));
        return unUsedEFFNum;
    }

    public static double GetUsedEFFSizes(string uid)
    {
        double sizes = 0.0d;
        string SQL = "SELECT SUM(OtherInfo2)  FROM FileFolder WHERE FileFolder.OfficeID1 = '" + uid + "'";

        try
        {
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sizes = Convert.ToDouble(sqlH.ExecuteScalar(SQL));
            return sizes;
        }
        catch (Exception ex)
        {
            return sizes;
        }
    }

    public static void UpdateReminder(Reminder item)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            item.Update(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }

    }

    public static void CreatReminder(string folderID, string uid, DateTime date)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();

        Reminder item = new Reminder();
        item.UID.Value = uid;
        item.EffID.Value = Convert.ToInt32(folderID);
        item.DOB.Value = date;

        ObjectQuery query = item.CreateQuery();
        query.SetCriteria(item.EffID, folderID);
        query.SetCriteria(item.UID, uid);
        query.SetSelectFields(item.ReminderID);

        DataTable table = new DataTable();
        int count = 0;
        try
        {
            query.Fill(table);

            count = table.Rows.Count;
            if (count > 0)
            {
                item.ReminderID.Value = (int)table.Rows[0][0];
                item.Load(connection);
                item.DOB.Value = date;
            }
        }
        finally
        {
            table.Dispose();
            table = null;
        }


        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            if (count > 0)
            {
                item.Update(transaction);
            }
            else
            {
                item.Create(transaction);
            }

            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
        }
    }

    public static bool DeleteDivider(int dividerID)
    {
        Divider objDivider = new Divider(dividerID);
        if (objDivider.IsExist)
        {
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            IDbTransaction transaction = connection.BeginTransaction();
            try
            {
                int effid = objDivider.EffID.Value;
                objDivider.Delete(transaction);
                FileFolder eff = new FileFolder(effid);
                eff.OtherInfo.Value--;
                eff.Update(transaction);
                transaction.Commit();

                return true;
            }
            catch
            {
                transaction.Rollback();
            }
        }

        return false;
    }

    public static int InsertDivider(Divider item)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();

        try
        {
            item.Create(transaction);
            FileFolder eff = new FileFolder(item.EffID.Value);
            eff.OtherInfo.Value++;
            eff.Update(transaction);

            transaction.Commit();

            return item.DividerID.Value;
        }
        catch (Exception ex)
        {
            //Console.Write(ex.Message);
            transaction.Rollback();

            return -1;
        }
    }

    public static bool UpdateDivider(Divider item)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();

        try
        {
            item.Update(connection);

            return true;
        }
        catch (Exception ex)
        {

            return false;
        }
    }

    public static DataTable GetDividers(int effid)
    {
        Divider divider = new Divider();
        ObjectQuery query = divider.CreateQuery();

        query.SetCriteria(divider.EffID, effid);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static int GetDividersWithName(string dividerName, int effid)
    {
        Divider divider = new Divider();
        ObjectQuery query = divider.CreateQuery();

        query.SetCriteria(divider.EffID, effid);
        query.SetCriteria(divider.Name, dividerName);
        query.SetSelectFields(divider.DividerID);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            if (table.Rows.Count <= 0)
            {
                return -1;
            }

            return Convert.ToInt32(table.Rows[0][0]);
        }
        catch (Exception ex)
        {
            return -1;
        }
    }

    public static DataTable GetDividerIDByFolderID(string strFolderID)
    {
        Divider divider = new Divider();
        DataTable dt = new DataTable();
        try
        {
            ObjectQuery query = divider.CreateQuery();
            query.SetCriteria(divider.EffID, strFolderID);
            query.SetOrderBy(divider.DividerID, true);
            query.Fill(dt);
        }
        catch (Exception ex)
        {

            new ApplicationException(ex.Message);
        }
        return dt;
    }

    public static DataTable GetFolderReminders(string strFolderID)
    {
        FolderReminder divider = new FolderReminder();
        DataTable dt = new DataTable();
        try
        {
            ObjectQuery query = divider.CreateQuery();
            query.SetCriteria(divider.FolderId, strFolderID);
            query.Fill(dt);
        }
        catch (Exception ex)
        {

            new ApplicationException(ex.Message);
        }
        return dt;
    }

    public static DataTable GetFolderRemindersByUID(string strUserId)
    {
        FolderReminder divider = new FolderReminder();
        DataTable dt = new DataTable();
        try
        {
            var current = DateTime.Now;
            ObjectQuery query = divider.CreateQuery();
            query.SetCriteria(divider.UID, strUserId);
            query.SetCriteria(divider.ReminderDate, Operator.MoreEqualThan, new DateTime(
               current.Year, current.Month, current.Day));

            query.SetCriteria(divider.ReminderDate, Operator.LessEqualThan, new DateTime(
              current.Year, current.Month, current.Day, 23, 59, 59));

            query.Fill(dt);
        }
        catch (Exception ex)
        {

            new ApplicationException(ex.Message);
        }
        return dt;
    }

    public static DataTable GetFileFolderReminders(string officeid1, DateTime startDate, DateTime endDate)
    {
        using (SqlConnection connection = new SqlConnection(ConnectionString))
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "GetReminders";
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter[] parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@OfficeID1", SqlDbType.NVarChar, 50);
                parameters[0].Value = officeid1;
                parameters[1] = new SqlParameter("@StartDate", SqlDbType.DateTime);
                parameters[1].Value = startDate;
                parameters[2] = new SqlParameter("@EndDate", SqlDbType.DateTime);
                parameters[2].Value = endDate;
                command.Parameters.AddRange(parameters);

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;

                DataTable table = new DataTable();
                dataAdapter.Fill(table);

                return table;
            }
            catch (Exception ex)
            {
                new ApplicationException(ex.Message);
                return null;
            }
            finally
            {
                connection.Close();
            }
    }


    public static void AddFolderReminders(List<FolderReminder> reminders, int folderId)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            string SQL = "Delete from FolderReminder where FolderId = ";
            SQL += folderId;
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL, transaction);

            foreach (FolderReminder reminder in reminders)
            {
                reminder.Create(transaction);
            }

            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }
        finally
        {
            if (connection.State != ConnectionState.Closed)
            {
                connection.Close();
                connection = null;
            }
        }
    }


    public static void DelQuickNoteByDivierID(string DivierID)
    {
        try
        {
            string SQL = "Delete from QuickNote where DividerID = '";
            SQL += DivierID;
            SQL += "'";
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);
        }
        catch { }

    }

    public static void DelNewNoteByDivierID(string DivierID)
    {
        try
        {
            string SQL = "Delete from NewNote where DividerID = '";
            SQL += DivierID;
            SQL += "'";
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);
        }
        catch { }

    }

    public static bool DelDocumentByDocumentID(string DocumentID)
    {
        try
        {
            string SQL = "Delete from Document where DocumentID = '";
            SQL += DocumentID;
            SQL += "'";
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);

            return true;
        }
        catch { }
        {
            return false;
        }

    }

    public static int GetFileIdByGUID(string guid)
    {
        try
        {
            string SQL = "SELECT DocumentID from Document where GUID = '";
            SQL += guid;
            SQL += "'";
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            return (int)sqlH.ExecuteScalar(SQL);
        }
        catch
        {
            return -1;
        }
    }

    public static DataTable GetPageInforByDividerID(string strDividerID)
    {
        Shinetech.DAL.Page page = new Shinetech.DAL.Page();
        DataTable dt = new DataTable();
        try
        {
            ObjectQuery query = page.CreateQuery();
            query.SetCriteria(page.DividerID, strDividerID);
            query.Fill(dt);
        }
        catch (Exception ex)
        {

            new ApplicationException(ex.Message);
        }
        return dt;
    }

    public static void DelPathByDividerID(string strDividerID)
    {
        try
        {
            string SQL = "Delete from Page where DividerID = '";
            SQL += strDividerID;
            SQL += "'";
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);
        }
        catch
        { }

    }

    public static bool DelFileFolderByFolderID(string strFolderID)
    {
        try
        {
            string SQL = "Delete from FileFolder where FolderID = '";
            SQL += strFolderID;
            SQL += "'";
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);

            return true;
        }
        catch
        {
            return false;
        }

    }

    public static bool RestoreFolderByID(string strFolderID)
    {
        try
        {
            string SQL = "UPDATE FileFolder SET OfficeID = SavedID,OfficeID1=SavedID1, DeletedBy=NULL, RestoreDate='" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + "' where FolderID = ";
            SQL += strFolderID;
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }

    }

    public static bool RecycleFolderByID(string strFolderID, string uid, string folderPath)
    {
        try
        {
            string SQL = "UPDATE FileFolder SET SavedID=OfficeID,SavedID1=OfficeID1,Path='" + folderPath + "',DeletedBy='" + uid + "',OfficeID='00000000-0000-0000-0000-000000000000', OfficeID1='00000000-0000-0000-0000-000000000000', DeleteDate='" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + "' where FolderID = ";
            SQL += strFolderID;
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }

    }

    public static Dictionary<bool, string> MoveDeletedFolder(string folderId)
    {
        try
        {
            string folderPath = string.Empty;
            if (!Directory.Exists(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFolders")))
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFolders"));
            Shinetech.DAL.Account user = new Shinetech.DAL.Account(Sessions.SwitchedSessionId);
            //Shinetech.DAL.Account user = new Shinetech.DAL.Account(Membership.GetUser().ProviderUserKey.ToString());

            if ((user.IsSubUser.ToString() == "1" && user.HasPrivilegeDelete.Value) || user.IsSubUser.ToString() == "0")
            {
                folderPath = "~/" + ConfigurationManager.AppSettings["PdfPath"] + "/" + folderId;
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(folderPath)))
                {
                    folderPath = "~/" + ConfigurationManager.AppSettings["CurrentVolume"] + "/" + folderId;
                }

                string destPath = ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFolders" + folderPath.Substring(folderPath.IndexOf('/'));
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(destPath)))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(destPath));
                if (Directory.Exists(HttpContext.Current.Server.MapPath(folderPath)))
                {
                    string[] dividers = Directory.GetDirectories(HttpContext.Current.Server.MapPath(folderPath));
                    foreach (string divider in dividers)
                    {
                        if (Directory.GetDirectories(divider).Length > 0)
                        {
                            string[] subFolders = Directory.GetDirectories(divider);
                            foreach (string subFolder in subFolders)
                            {
                                string[] files = Directory.GetFiles(subFolder);

                                // Copy the files and overwrite destination files if they already exist.
                                foreach (string file in files)
                                {
                                    // Use static Path methods to extract only the file name from the path.
                                    string fileName = Path.GetFileName(file);
                                    string destFolder = subFolder.Substring(0, subFolder.LastIndexOf('\\'));
                                    string destintaionPath = destPath + destFolder.Substring(destFolder.LastIndexOf('\\')) + subFolder.Substring(subFolder.LastIndexOf('\\'));
                                    if (!Directory.Exists(HttpContext.Current.Server.MapPath(destintaionPath)))
                                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath(destintaionPath));
                                    string destFile = Path.Combine(HttpContext.Current.Server.MapPath(destintaionPath), fileName);
                                    File.Move(file, destFile);
                                }
                                if (Directory.GetFiles(subFolder).Length == 0 && Directory.GetDirectories(subFolder).Length == 0)
                                    Directory.Delete(subFolder);
                            }
                        }

                        string[] files1 = Directory.GetFiles(divider);

                        // Copy the files and overwrite destination files if they already exist.
                        foreach (string file in files1)
                        {
                            // Use static Path methods to extract only the file name from the path.
                            string fileName = Path.GetFileName(file);
                            string destintaionPath = Path.Combine(HttpContext.Current.Server.MapPath(destPath), divider.Substring(divider.LastIndexOf('\\') + 1));
                            if (!Directory.Exists(destintaionPath))
                                Directory.CreateDirectory(destintaionPath);
                            string destFile = Path.Combine(destintaionPath, fileName);
                            File.Move(file, destFile);
                        }

                        if (Directory.GetFiles(divider).Length == 0 && Directory.GetDirectories(divider).Length == 0)
                            Directory.Delete(divider);
                    }
                    if (Directory.GetDirectories(HttpContext.Current.Server.MapPath(folderPath)).Length == 0 && Directory.GetFiles(HttpContext.Current.Server.MapPath(folderPath)).Length == 0)
                        Directory.Delete(HttpContext.Current.Server.MapPath(folderPath));
                }
            }
            return new Dictionary<bool, string> { { true, folderPath } };
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(folderId, string.Empty, string.Empty, ex);
            return new Dictionary<bool, string> { { false, string.Empty } };
        }
    }


    public static bool RestoreDeletedFolder(string folderId, string path)
    {
        try
        {
            string sourceFolderPath = ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFolders/" + path;
            if (Directory.Exists(HttpContext.Current.Server.MapPath(sourceFolderPath)))
            {
                string destFolderPath = "~/" + path;
                if (!Directory.Exists(HttpContext.Current.Server.MapPath(destFolderPath)))
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(destFolderPath));
                string[] dividers = Directory.GetDirectories(HttpContext.Current.Server.MapPath(sourceFolderPath));
                foreach (string divider in dividers)
                {
                    if (Directory.GetDirectories(divider).Length > 0)
                    {
                        string[] subFolders = Directory.GetDirectories(divider);
                        foreach (string subFolder in subFolders)
                        {
                            string[] files = Directory.GetFiles(subFolder);

                            // Copy the files and overwrite destination files if they already exist.
                            foreach (string file in files)
                            {
                                // Use static Path methods to extract only the file name from the path.
                                string fileName = Path.GetFileName(file);
                                string destFolder = subFolder.Substring(0, subFolder.LastIndexOf('\\'));
                                string destintaionPath = destFolderPath + destFolder.Substring(destFolder.LastIndexOf('\\')) + subFolder.Substring(subFolder.LastIndexOf('\\'));
                                if (!Directory.Exists(HttpContext.Current.Server.MapPath(destintaionPath)))
                                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(destintaionPath));
                                string destFile = Path.Combine(HttpContext.Current.Server.MapPath(destintaionPath), fileName);
                                File.Move(file, destFile);
                            }
                            if (Directory.GetFiles(subFolder).Length == 0 && Directory.GetDirectories(subFolder).Length == 0)
                                Directory.Delete(subFolder);
                        }
                    }

                    string[] files1 = Directory.GetFiles(divider);
                    foreach (string file in files1)
                    {
                        string fileName = Path.GetFileName(file);
                        string destintaionPath = Path.Combine(HttpContext.Current.Server.MapPath(destFolderPath), divider.Substring(divider.LastIndexOf('\\') + 1));
                        if (!Directory.Exists(destintaionPath))
                            Directory.CreateDirectory(destintaionPath);
                        string destFile = Path.Combine(destintaionPath, fileName);
                        File.Move(file, destFile);
                    }
                    //Change By Jenish 29 MAY 2018
                    if (Directory.GetFiles(divider).Length == 0 && Directory.GetDirectories(divider).Length == 0)
                        Directory.Delete(divider);
                }
                if (Directory.GetDirectories(HttpContext.Current.Server.MapPath(sourceFolderPath)).Length == 0)
                    Directory.Delete(HttpContext.Current.Server.MapPath(sourceFolderPath));

            }
            FileFolderManagement.RestoreFolderByID(folderId);
            General_Class.AuditLogByFolderId(Convert.ToInt32(folderId), Sessions.SwitchedSessionId, Enum_Tatva.Action.Restore.GetHashCode());
            //General_Class.AuditLogByFolderId(Convert.ToInt32(folderId), Membership.GetUser().ProviderUserKey.ToString(), Enum_Tatva.Action.Restore.GetHashCode());
            return true;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(folderId, string.Empty, string.Empty, ex);
            return false;
        }
    }

    public static bool RestoreDeletedFile(string strDocumentID)
    {
        try
        {
            string destPathTo = string.Empty;
            EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
            Document doc = new Document(int.Parse(strDocumentID));
            string ext = ".pdf";
            string strPDFName = string.Empty;
            string strFileName = string.Empty;
            StreamType extValue = StreamType.PDF;
            try
            {
                extValue = (StreamType)doc.FileExtention.Value;
                if (extValue == StreamType.OTHERS)
                    ext = Path.GetExtension(doc.FullFileName.Value);
            }
            catch { }

            if (extValue == StreamType.OTHERS)
            {
                strPDFName = strFileName = doc.FullFileName.Value;
            }
            else
            {
                strPDFName = doc.Name.Value + ext;
                strFileName = doc.Name.Value + GetFileExtention(extValue);
            }

            strPDFName = GetFileName(strPDFName);
            string strPDFPath = "~/" + doc.PathName.Value + "/" + strPDFName;
            string sourcePDFPath = ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles/" + doc.PathName.Value + "/" + strPDFName;
            if (doc.IsExist)
            {
                if (File.Exists(System.Web.HttpContext.Current.Server.MapPath(sourcePDFPath)))
                {
                    try
                    {
                        //   File.Delete(Server.MapPath(strPDFPath));
                        if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(strPDFPath.Substring(0, strPDFPath.LastIndexOf('/')))))
                            Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(strPDFPath.Substring(0, strPDFPath.LastIndexOf('/'))));
                        destPathTo = System.Web.HttpContext.Current.Server.MapPath(strPDFPath);
                        destPathTo = Common_Tatva.GetRenameFileName(destPathTo);
                        File.Move(System.Web.HttpContext.Current.Server.MapPath(sourcePDFPath), destPathTo);
                        DataTable dtPage = FileFolderManagement.GetDocumentByDocumentID(strDocumentID);
                        for (int i = 0; i < dtPage.Rows.Count; i++)
                        {
                            string strPageID = dtPage.Rows[i]["PageID"].ToString();

                            //delete quick note
                            FileFolderManagement.DelQucikNoteByPageID(strPageID);

                            string strImagePath;
                            string strImagePath1;
                            string strImagePath2;
                            string strImagePath3;

                            if (dtPage.Rows[i]["ImagePath"].ToString() != "")
                            {
                                strImagePath = "~/" + dtPage.Rows[i]["ImagePath"].ToString();
                                string sourcePath = ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles/" + strImagePath.Substring(2);
                                if (File.Exists(System.Web.HttpContext.Current.Server.MapPath(sourcePath)))
                                {
                                    if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(strImagePath.Substring(0, strImagePath.LastIndexOf("\\")))))
                                        Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(strImagePath.Substring(0, strImagePath.LastIndexOf("\\"))));
                                    string destPath = System.Web.HttpContext.Current.Server.MapPath(strImagePath);
                                    destPath = Common_Tatva.GetRenameFileName(destPath);
                                    File.Move(System.Web.HttpContext.Current.Server.MapPath(sourcePath), destPath);
                                    // File.Delete(Server.MapPath(strImagePath));
                                }

                            }
                            if (dtPage.Rows[i]["ImagePath2"].ToString() != "")
                            {
                                strImagePath1 = "~/" + dtPage.Rows[i]["ImagePath2"].ToString();
                                string sourcePath = ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles/" + strImagePath1.Substring(2);
                                if (File.Exists(System.Web.HttpContext.Current.Server.MapPath(sourcePath)))
                                {
                                    if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(strImagePath1.Substring(0, strImagePath1.LastIndexOf("\\")))))
                                        Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(strImagePath1.Substring(0, strImagePath1.LastIndexOf("\\"))));
                                    string destPath = System.Web.HttpContext.Current.Server.MapPath(strImagePath1);
                                    destPath = Common_Tatva.GetRenameFileName(destPath);
                                    File.Move(System.Web.HttpContext.Current.Server.MapPath(sourcePath), destPath);
                                    // File.Delete(Server.MapPath(strImagePath));
                                }
                            }
                            if (dtPage.Rows[i]["ImagePath3"].ToString() != "")
                            {
                                strImagePath2 = "~/" + dtPage.Rows[i]["ImagePath3"].ToString();
                                string sourcePath = ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles/" + strImagePath2.Substring(2);
                                if (File.Exists(System.Web.HttpContext.Current.Server.MapPath(sourcePath)))
                                {
                                    if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(strImagePath2.Substring(0, strImagePath2.LastIndexOf("\\")))))
                                        Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(strImagePath2.Substring(0, strImagePath2.LastIndexOf("\\"))));
                                    string destPath = System.Web.HttpContext.Current.Server.MapPath(strImagePath2);
                                    destPath = Common_Tatva.GetRenameFileName(destPath);
                                    File.Move(System.Web.HttpContext.Current.Server.MapPath(sourcePath), destPath);
                                    // File.Delete(Server.MapPath(strImagePath));
                                }
                            }
                            if (dtPage.Rows[i]["ImagePath4"].ToString() != "")
                            {
                                strImagePath3 = "~/" + dtPage.Rows[i]["ImagePath4"].ToString();
                                string sourcePath = ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles/" + strImagePath3.Substring(2);
                                if (File.Exists(System.Web.HttpContext.Current.Server.MapPath(sourcePath)))
                                {
                                    if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath(strImagePath3.Substring(0, strImagePath3.LastIndexOf("\\")))))
                                        Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath(strImagePath3.Substring(0, strImagePath3.LastIndexOf("\\"))));
                                    string destPath = System.Web.HttpContext.Current.Server.MapPath(strImagePath3);
                                    destPath = Common_Tatva.GetRenameFileName(destPath);
                                    File.Move(System.Web.HttpContext.Current.Server.MapPath(sourcePath), destPath);
                                    // File.Delete(Server.MapPath(strImagePath));
                                }
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        Common_Tatva.WriteErrorLog(string.Empty, string.Empty, strFileName, ex);
                    }
                }
                string newFileName = Path.GetFileNameWithoutExtension(destPathTo);
                string displayName = HttpUtility.UrlDecode(newFileName.Replace('$', '%').ToString());
                FileFolderManagement.RestoreDocumentByID(strDocumentID, newFileName, displayName);
                Shinetech.DAL.General_Class.AuditLogByDocId(Convert.ToInt32(strDocumentID), Sessions.SwitchedSessionId, Enum_Tatva.Action.Restore.GetHashCode());
                //Shinetech.DAL.General_Class.AuditLogByDocId(Convert.ToInt32(strDocumentID), Membership.GetUser().ProviderUserKey.ToString(), Enum_Tatva.Action.Restore.GetHashCode());
            }
            return true;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            return false;
        }
    }

    private static string GetFileName(string fileName)
    {
        string encodeName = HttpUtility.UrlPathEncode(fileName);
        return encodeName.Replace("%", "$");
    }
    protected static string GetFileExtention(StreamType ext)
    {
        string extname = ".pdf";
        switch (ext)
        {
            case StreamType.PDF:
                extname = ".pdf";
                break;
            case StreamType.Word:
                extname = ".doc";
                break;
            case StreamType.Excel:
                extname = ".xls";
                break;
            case StreamType.PPT:
                extname = ".ppt";
                break;
            case StreamType.RTF:
                extname = ".rtf";
                break;
            case StreamType.ODP:
                extname = ".odp";
                break;
            case StreamType.ODS:
                extname = ".ods";
                break;
            case StreamType.ODT:
                extname = ".odt";
                break;
            case StreamType.PNG:
                extname = ".png";
                break;
            case StreamType.JPG:
                extname = ".jpg";
                break;
            default:
                break;
        }

        return extname;
    }

    public static DataTable GetSharedEFFs(string userId)
    {
        EFFolder efolder = new EFFolder();
        FileFolder folder = new FileFolder();


        DataTable dt = new DataTable();
        try
        {
            ObjectQuery query = efolder.CreateQuery();
            query.SetCriteria(efolder.UserID, userId);

            query.SetOrderBy(efolder.CreateDate, false);
            query.SetAssociation(new IColumn[] { efolder.FolderID }, new IColumn[] { folder.FolderID }, AssociationType.LeftJoin);
            query.SetSelectFields(efolder.EFFID, efolder.FolderID, folder.FolderName, folder.FirstName, folder.Lastname, folder.DOB,
                folder.OfficeID, folder.OtherInfo, folder.OtherInfo2, folder.SecurityLevel);


            query.Fill(dt);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return dt;
    }

    public static void RemoveEFFolder(int Id)
    {
        EFFolder efolder = new EFFolder(Id);
        if (efolder.IsExist)
        {
            efolder.Delete();
        }
    }

    public static DataTable GetFolderInforByEFFID(string strFolderID)
    {
        FileFolder folder = new FileFolder();
        DataTable dtFolder = new DataTable();

        try
        {
            ObjectQuery query = folder.CreateQuery();
            query.SetCriteria(folder.FolderID, strFolderID);
            query.Fill(dtFolder);
        }
        catch (Exception ex)
        {

            new ApplicationException(ex.Message);
        }
        return dtFolder;
    }

    public static int GetUsedFoldersNumber(string uid)
    {
        string SQL = "SELECT COUNT(FolderID) AS CNT FROM FileFolder WHERE OfficeID1 = '";
        SQL += uid;
        SQL += "'";

        try
        {
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            int count = (int)sqlH.ExecuteScalar(SQL);

            return count;
        }
        catch (Exception ex)
        {
            throw new ApplicationException(ex.Message);
        }

    }

    public static DataTable GetDocumentByUID(string uid)
    {
        Document document = new Document();
        FileFolder folder = new FileFolder();
        Divider divider = new Divider();

        ObjectQuery query = document.CreateQuery();

        query.SetCriteria(document.UID, uid);
        query.SetCriteria(document.DocumentID, Operator.MoreThan, 0); //用于过滤删除的Document
        query.SetOrderBy(document.DOB, false);
        query.SetAssociation(new IColumn[] { document.FileFolderID }, new IColumn[] { folder.FolderID }, AssociationType.LeftJoin);
        query.SetAssociation(new IColumn[] { document.DividerID }, new IColumn[] { divider.DividerID }, AssociationType.LeftJoin);
        query.SetSelectFields(document.DocumentID);
        query.SetSelectFields(document.Name, "DocumentName");
        query.SetSelectFields(folder.FolderName);
        query.SetSelectFields(document.DOB, "CreateDate");
        query.SetSelectFields(divider.Name, "TabName");
        query.SetSelectFields(document.PathName);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    /// <summary>
    /// Gets the document by folder identifier.
    /// </summary>
    /// <param name="folderId">The folder identifier.</param>
    /// <returns></returns>
    public static DataTable GetDocumentByFolderId(string folderId)
    {
        //Document document = new Document();       

        //ObjectQuery query = document.CreateQuery();

        //query.SetCriteria(document.FileFolderID, folderId );

        //query.SetOrderBy(document.DOB, false);

        //DataTable table = new DataTable();
        //try
        //{
        //    query.Fill(table);
        //    return table;
        //}
        //catch (Exception ex)
        //{
        //    return table;
        //}

        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "GetDocumentsFolderWise";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@FolderID", folderId);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@UID", Sessions.SwitchedSessionId);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;
            DataTable table = new DataTable();
            dataAdapter.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    /// <summary>
    /// Gets the document by folder identifier.
    /// </summary>
    /// <param name="folderId">The folder identifier.</param>
    /// <param name="dividerId">The divider identifier.</param>
    /// <param name="pageIndex">Index of the page.</param>
    /// <param name="pageSize">Size of the page.</param>
    /// <returns></returns>
    public static DataSet GetDocumentsOnDemand(int folderId, int dividerId = 0, int pageIndex = 1, int pageSize = 10)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "GetDocumentsOnDemand";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@FolderId", folderId);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@DividerId", dividerId);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlParameter param3 = command.Parameters.AddWithValue("@UID", Sessions.SwitchedSessionId);
            param3.Direction = ParameterDirection.Input;
            param3.DbType = DbType.String;

            SqlParameter param4 = command.Parameters.AddWithValue("@PageIndex", pageIndex);
            param4.Direction = ParameterDirection.Input;
            param4.DbType = DbType.Int32;

            SqlParameter param5 = command.Parameters.AddWithValue("@PageSize", pageSize);
            param5.Direction = ParameterDirection.Input;
            param5.DbType = DbType.Int32;

            SqlParameter param6 = command.Parameters.Add("@PageCount", SqlDbType.Int);
            param6.Direction = ParameterDirection.Output;
            param6.DbType = DbType.Int32;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds, "Documents");
            DataTable dt = new DataTable("PageCount");
            //DataTable dtOrder = new DataTable("DcoumentOrder");
            dt.Columns.Add("PageCount");
            //dtOrder.Columns.Add("DocumentOrder");
            dt.Rows.Add();
            dt.Rows[0][0] = command.Parameters["@PageCount"].Value;
            ds.Tables.Add(dt);
            return ds;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    /// <summary>
    /// Gets the document by folder identifier.
    /// </summary>
    /// <param name="folderId">The folder identifier.</param>
    /// <param name="dividerId">The divider identifier.</param>
    /// <param name="pageIndex">Index of the page.</param>
    /// <param name="pageSize">Size of the page.</param>
    /// <returns></returns>
    public static DataSet GetDocumentsForShareFolder(int folderId, int dividerId = 0)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "GetDocumentsForShareFolder";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@FolderId", folderId);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@DividerId", dividerId);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds, "Documents");
            //DataTable dt = new DataTable("PageCount");
            //DataTable dtOrder = new DataTable("DcoumentOrder");
            //dt.Columns.Add("PageCount");
            //dtOrder.Columns.Add("DocumentOrder");
            //dt.Rows.Add();
            //dt.Rows[0][0] = command.Parameters["@PageCount"].Value;
            //ds.Tables.Add(dt);
            return ds;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    /// <summary>
    /// Get documents by Filter
    /// </summary>
    /// <param name="folderId">The folder identifier.</param>
    /// <param name="dividerId">The divider identifier.</param>
    /// <param name="name">The name.</param>
    /// <param name="className">Name of the class.</param>
    /// <param name="fromDate">From date.</param>
    /// <param name="toDate">To date.</param>
    /// <param name="folderIndex">Index of the folder.</param>
    /// <param name="pageIndex">Index of the page.</param>
    /// <param name="pageSize">Size of the page.</param>
    /// <returns></returns>
    public static DataSet GetFilteredDocsOnDemand(int folderId, int dividerId = 0, string name = null, string className = null, DateTime? fromDate = null, DateTime? toDate = null, string folderIndex = null, int pageIndex = 1, int pageSize = 10)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "GetFilteredDocsOnDemand";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@Name", name);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@Class", className);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlParameter param3 = command.Parameters.AddWithValue("@From", fromDate);
            param3.Direction = ParameterDirection.Input;
            param3.DbType = DbType.DateTime;

            SqlParameter param4 = command.Parameters.AddWithValue("@To", toDate);
            param4.Direction = ParameterDirection.Input;
            param4.DbType = DbType.DateTime;

            SqlParameter param5 = command.Parameters.AddWithValue("@Index", folderIndex);
            param5.Direction = ParameterDirection.Input;
            param5.DbType = DbType.String;

            SqlParameter param6 = command.Parameters.AddWithValue("@FolderId", folderId);
            param6.Direction = ParameterDirection.Input;
            param6.DbType = DbType.String;

            SqlParameter param7 = command.Parameters.AddWithValue("@DividerId", dividerId);
            param7.Direction = ParameterDirection.Input;
            param7.DbType = DbType.String;

            SqlParameter param8 = command.Parameters.AddWithValue("@UID", Sessions.SwitchedSessionId);
            param8.Direction = ParameterDirection.Input;
            param8.DbType = DbType.String;

            SqlParameter param9 = command.Parameters.AddWithValue("@PageIndex", pageIndex);
            param9.Direction = ParameterDirection.Input;
            param9.DbType = DbType.Int32;

            SqlParameter param10 = command.Parameters.AddWithValue("@PageSize", pageSize);
            param10.Direction = ParameterDirection.Input;
            param10.DbType = DbType.Int32;

            SqlParameter param11 = command.Parameters.Add("@PageCount", SqlDbType.Int);
            param11.Direction = ParameterDirection.Output;
            param11.DbType = DbType.Int32;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds, "Documents");
            DataTable dt = new DataTable("PageCount");
            dt.Columns.Add("PageCount");
            dt.Rows.Add();
            dt.Rows[0][0] = command.Parameters["@PageCount"].Value;
            ds.Tables.Add(dt);
            return ds;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    /// <summary>
    /// Gets the document by uid.
    /// </summary>
    /// <param name="uid">The uid.</param>
    /// <param name="fileName">Name of the file.</param>
    /// <param name="foldeName">Name of the folde.</param>
    /// <returns></returns>
    public static DataTable GetDocumentByUID(string uid, string fileName, string foldeName)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            //command.CommandText = "eFileFolder_FindDocumentsByFields"; 
            command.CommandText = "eFileFolder_FindDocumentsByAuditLog";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@UID", uid);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@DocumentName", "%" + fileName + "%");
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlParameter param3 = command.Parameters.AddWithValue("@FolderName", "%" + foldeName + "%");
            param3.Direction = ParameterDirection.Input;
            param3.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);
            var count = table.Rows.Count.ToString();

            foreach (DataRow row in table.Rows)
            {

                if ((row["Action"].ToString()) == "2")
                {
                    row["Action"] = Enum_Tatva.Action.Delete;
                }
                else
                {
                    //row["Action"] = "Create";
                    row["Action"] = Enum_Tatva.Action.Create;
                }
            }

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetSubDocumentByUID(string uid, string fileName, string foldeName)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_FindSubDocumentsByFields";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@UID", uid);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@DocumentName", "%" + fileName + "%");
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlParameter param3 = command.Parameters.AddWithValue("@FolderName", "%" + foldeName + "%");
            param3.Direction = ParameterDirection.Input;
            param3.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            foreach (DataRow row in table.Rows)
            {

                if ((row["Action"].ToString()) == "2")
                {
                    row["Action"] = Enum_Tatva.Action.Delete;
                }
                else
                {
                    //row["Action"] = "Create";
                    row["Action"] = Enum_Tatva.Action.Create;
                }
            }

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetDocumentByUID(string uid, string folderId)
    {
        Document document = new Document();
        FileFolder folder = new FileFolder();
        Divider divider = new Divider();

        ObjectQuery query = document.CreateQuery();

        query.SetCriteria(document.UID, uid);
        query.SetCriteria(document.FileFolderID, folderId);
        query.SetCriteria(document.DocumentID, Operator.MoreThan, 0); //用于过滤删除的Document
        query.SetOrderBy(document.DOB, false);
        query.SetAssociation(new IColumn[] { document.FileFolderID }, new IColumn[] { folder.FolderID }, AssociationType.LeftJoin);
        query.SetAssociation(new IColumn[] { document.DividerID }, new IColumn[] { divider.DividerID }, AssociationType.LeftJoin);
        query.SetSelectFields(document.DocumentID);
        query.SetSelectFields(document.Name, "DocumentName");
        query.SetSelectFields(folder.FolderName);
        query.SetSelectFields(document.DOB, "CreateDate");
        query.SetSelectFields(divider.Name, "TabName");
        query.SetSelectFields(document.PathName);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }


    public static void DelPageByDocumentID(string DocumentID)
    {
        try
        {
            string SQL = "Delete from Page where DocumentID = '";
            SQL += DocumentID;
            SQL += "'";

            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);
        }
        catch { }

    }

    public static DataTable GetDocumentByDocumentID(string DID)
    {
        Shinetech.DAL.Page page = new Shinetech.DAL.Page();

        ObjectQuery query = page.CreateQuery();

        query.SetCriteria(page.DocumentID, DID);
        query.SetSelectFields(page.PageID, page.ImagePath, page.ImagePath2, page.ImagePath3, page.ImagePath4);


        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static void DelQucikNoteByPageID(string strPageID)
    {
        try
        {
            string SQL = "Delete from quicknote where pageID = '";
            SQL += strPageID;
            SQL += "'";
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);
        }
        catch { }

    }

    public static DataTable GetPathNameByFolderID(string FolderID)
    {
        Document document = new Document();

        ObjectQuery query = document.CreateQuery();

        query.SetCriteria(document.FileFolderID, FolderID);
        query.SetSelectFields(document.PathName);


        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }


    public static DataTable GetDocumentByKey(string keyname, string UID)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SelectDocument";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter param1 = command.Parameters.AddWithValue("@Key", keyname);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@UID", UID);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetDocInforByDocumentID(string DID)
    {
        Document document = new Document();

        ObjectQuery query = document.CreateQuery();


        query.SetCriteria(document.DocumentID, DID);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static DataTable GetFileFolderSizeByDocumentID(string DID)
    {
        Document document = new Document();
        FileFolder folder = new FileFolder();

        ObjectQuery query = folder.CreateQuery();


        query.SetCriteria(document.DocumentID, DID);

        query.SetAssociation(new IColumn[] { folder.FolderID }, new IColumn[] { document.FileFolderID }, AssociationType.LeftJoin);
        query.SetSelectFields(folder.OtherInfo2, folder.FolderID);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static void UpdateFolderSize(string folderID, Double size)
    {
        string SQL = "UPDATE filefolder SET OtherInfo2 = '";
        SQL += size;
        SQL += "' ";
        SQL += "WHERE FolderID = '";
        SQL += folderID;
        SQL += "'";

        SqlHelper sqlH = new SqlHelper(ConnectionName);
        sqlH.ExecuteNonQuery(SQL);


    }

    public static DataTable GetAnalysticTrackers(DateTime date)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_AnalystVisitors";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter param1 = command.Parameters.AddWithValue("@date", date);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.DateTime;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetGiftNumbers(DateTime sdate, DateTime edate)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_GiftNumbers";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter param1 = command.Parameters.AddWithValue("@sdate", sdate);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.DateTime;

            SqlParameter param2 = command.Parameters.AddWithValue("@edate", edate);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.DateTime;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetTemplates(string uid)
    {
        FolderTemplate template = new FolderTemplate();

        ObjectQuery query = template.CreateQuery();
        query.SetCriteria(template.UID, uid);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
        }
        catch { }

        return table;
    }

    public static DataTable GetTemplatesByUID(string uid)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "GetFolderTemplateByUID";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter param1 = command.Parameters.AddWithValue("@uid", uid);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static bool RemoveTemplates(int id)
    {
        FolderTemplate template = new FolderTemplate(id);

        if (template.IsExist)
        {
            try
            {
                template.Delete();

                return true;
            }
            catch { }

            return false;
        }

        return false;

    }

    public static int SaveFolderTemplate(TemplateEntity template)
    {
        FolderTemplate ft = new FolderTemplate();
        ft.Name.Value = template.Name;
        ft.Content.Value = template.Content;
        ft.UID.Value = template.OfficeID;
        ft.IsLocked.Value = template.IsLocked;

        try
        {
            ft.Create();

            return ft.TemplateID.Value;
        }
        catch { }

        return -1;
    }

    public static bool IsTemplateNameExist(string uid, string tempName, int tid)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            using (SqlCommand cmd = new SqlCommand("IsSameTemplateNameExist", connection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@uid", uid);
                cmd.Parameters.AddWithValue("@name", tempName);
                cmd.Parameters.AddWithValue("@tid", tid);
                cmd.Parameters.Add("@isExist", SqlDbType.Int);
                cmd.Parameters["@isExist"].Direction = ParameterDirection.Output;
                connection.Open();
                cmd.ExecuteNonQuery();
                bool isExist = Convert.ToBoolean(cmd.Parameters["@isExist"].Value);
                return isExist;
            }
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
            connection.Close();
        }
    }

    //public static bool IsFolderNameExist(string uid, string folderName, string fileNumber, int folderID = 0)
    //{
    //    SqlConnection connection = new SqlConnection(ConnectionString);

    //    try
    //    {
    //        using (SqlCommand cmd = new SqlCommand("IsSameFolderNameExist", connection))
    //        {
    //            cmd.CommandType = CommandType.StoredProcedure;
    //            cmd.Parameters.AddWithValue("@uid", uid);
    //            cmd.Parameters.AddWithValue("@folderName", folderName);
    //            cmd.Parameters.AddWithValue("@fileNumber", fileNumber);
    //            cmd.Parameters.AddWithValue("@folderId", folderID);
    //            cmd.Parameters.Add("@isExist", SqlDbType.Int);
    //            cmd.Parameters["@isExist"].Direction = ParameterDirection.Output;
    //            connection.Open();
    //            cmd.ExecuteNonQuery();
    //            bool isExist = Convert.ToBoolean(cmd.Parameters["@isExist"].Value);
    //            return isExist;
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        return false;
    //    }
    //    finally
    //    {
    //        connection.Close();
    //    }
    //}

    /// <summary>
    /// Determines whether [is folder name exist] [the specified uid].
    /// </summary>
    /// <param name="uid">The uid.</param>
    /// <param name="folderName">Name of the folder.</param>
    /// <param name="fileNumber">The file number.</param>
    /// <param name="folderID">The folder identifier.</param>
    /// <returns></returns>
    public static DataTable IsFolderNameExist(string uid, string folderName, string fileNumber, int folderID = 0, bool isBatch = false)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "IsSameFolderNameExist";
            command.CommandType = CommandType.StoredProcedure;
            SqlParameter[] parameters = new SqlParameter[5];
            parameters[0] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
            parameters[0].Value = uid;
            parameters[1] = new SqlParameter("@FolderName", SqlDbType.NVarChar, 50);
            parameters[1].Value = folderName;
            parameters[2] = new SqlParameter("@FileNumber", SqlDbType.NVarChar, 50);
            parameters[2].Value = fileNumber;
            parameters[3] = new SqlParameter("@FolderId", SqlDbType.Int);
            parameters[3].Value = folderID;
            parameters[4] = new SqlParameter("@IsBatch", SqlDbType.Bit);
            parameters[4].Value = isBatch;

            command.Parameters.AddRange(parameters);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable dt = new DataTable();

            dataAdapter.Fill(dt);

            return dt;
        }
        catch (Exception ex)
        {
            return new DataTable();
        }
        finally
        {
            connection.Close();
        }
    }

    public static void UpdateFolderTemplate(int tid, string tempName, string content, bool? isLocked = null)
    {
        FolderTemplate ft = new FolderTemplate(tid);

        if (ft.IsExist)
        {
            try
            {
                ft.Content.Value = content;
                ft.Name.Value = tempName;
                if (isLocked != null && isLocked.Value != null)
                    ft.IsLocked.Value = isLocked;
                ft.Update();
            }
            catch (Exception ex)
            {
            }
        }
    }
    public static void DeleteFolderTemplate(int tid)
    {
        FolderTemplate ft = new FolderTemplate(tid);

        if (ft.IsExist)
        {
            try
            {
                ft.IsDeleted.Value = true;
                ft.Update();
            }
            catch { }
        }

    }

    public static DataTable GetOfficeSignatures(string name, string sdate, string edate, string officeid)
    {
        string whereSqlS = string.Empty, whereSqlE = string.Empty;
        if (!string.IsNullOrEmpty(sdate))
            whereSqlS += "a.SignInDate>='" + sdate + "' ";

        if (!string.IsNullOrEmpty(edate))
            whereSqlE += "a.SignInDate<='" + edate + "' ";

        string sqlstr =
               "SELECT TOP 100 a.*,DATEDIFF(minute,a.SignInDate,GetDate()) as WaitTime FROM dbo.Signature a " +
               "WHERE a.DoctorID='" + officeid + "'" +
               " AND a.PrintName LIKE '%" + name + "%' ";

        if (!string.IsNullOrEmpty(whereSqlS))
        {
            sqlstr += " AND " + whereSqlS;
        }

        if (!string.IsNullOrEmpty(whereSqlE))
        {
            sqlstr += " AND " + whereSqlE;
        }

        sqlstr += "ORDER BY a.CreateDate DESC";

        SqlHelper sql = new SqlHelper("Default");
        DataTable table = new DataTable();
        try
        {
            sql.Fill(table, sqlstr);

        }
        catch (Exception ex)
        {

        }

        return table;

        /*Signature signin = new Signature();
        ObjectQuery query = signin.CreateQuery();

        query.SetCriteria(signin.DoctorID, officeid);
        query.SetOrderBy(signin.CreateDate, false);

        if (!string.IsNullOrEmpty(name))
            query.SetCriteria(signin.PrintName, Operator.BothSidesLike, name);

        if (!string.IsNullOrEmpty(sdate))
            query.SetCriteria(signin.SignInDate, Operator.MoreEqualThan, sdate);

        if (!string.IsNullOrEmpty(edate))

            query.SetCriteria(signin.SignInDate, Operator.LessEqualThan, edate);
        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }*/
    }

    public static DataTable Get50OfficeSignatures(string officeId)
    {
        string sqlstr =
               "SELECT TOP 100 a.*,DATEDIFF(second,a.SignInDate,a.CheckInDate) as WaitTime FROM dbo.Signature a " +
               "WHERE a.DoctorID='" + officeId + "'ORDER BY a.CreateDate DESC";
        SqlHelper sql = new SqlHelper("Default");
        DataTable table = new DataTable();
        try
        {
            sql.Fill(table, sqlstr);

        }
        catch (Exception ex)
        {

        }

        return table;

        /*Signature signin = new Signature();
        ObjectQuery query = signin.CreateQuery();

        query.SetCriteria(signin.DoctorID, officeId);
        query.SetOrderBy(signin.CreateDate,false);

        query.TopN = 50;

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            return table;
        }
        catch (Exception ex)
        {
            return null;
        }*/
    }

    public static DataTable GetRestoredPaperFiles()
    {
        Document document = new Document();
        FileFolder folder = new FileFolder();
        Account user = new Account();
        DataTable table = new DataTable();
        try
        {
            ObjectQuery query = document.CreateQuery();
            query.SetAssociation(new IColumn[] { document.UID1 }, new IColumn[] { user.UID }, AssociationType.LeftJoin);
            query.SetAssociation(new IColumn[] { document.SavedID1 }, new IColumn[] { folder.FolderID }, AssociationType.LeftJoin);
            query.SetCriteria(document.DividerID, Operator.Equal, 0);
            query.SetCriteria(document.FileFolderID, Operator.Equal, 0);

            query.SetSelectFields(document.DocumentID, document.Name, folder.FolderName, document.PathName, folder.FolderID, document.Size, document.DeleteDate,
                user.Firstname, user.Lastname);
            query.Fill(table);
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static bool RestoreDocumentByID(string documentId, string fileName, string displayName)
    {
        try
        {
            //string SQL = "UPDATE Document SET DividerID = SavedID, UID=UID1,  FileFolderID=SavedID1, RestoreDate=convert(datetime, '" + DateTime.Now + "', 103) where DocumentID = ";
            string SQL = "UPDATE Document SET DividerID = SavedID, UID=UID1, DeletedBy=NULL, FileFolderID=SavedID1, RestoreDate='" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() + "', Name='" + fileName + "', DisplayName='" + displayName + "' where DocumentID = ";
            SQL += documentId;
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }

    }

    public static void ClearFolderValidCode(int p)
    {
        try
        {
            string SQL = "DELETE ValidateCode WHERE EFFID =" + p.ToString();

            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);
        }
        catch (Exception ex)
        {

        }
    }

    public static int GetFileWithName(string dividerName, int dividerId, int fodlerId)
    {
        Document doc = new Document();
        ObjectQuery query = doc.CreateQuery();

        query.SetCriteria(doc.FileFolderID, fodlerId);
        query.SetCriteria(doc.DividerID, dividerId);
        query.SetCriteria(doc.Name, dividerName);
        query.SetSelectFields(doc.DocumentID);

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            if (table.Rows.Count <= 0)
            {
                return -1;
            }

            return Convert.ToInt32(table.Rows[0][0]);
        }
        catch (Exception ex)
        {
            return -1;
        }
    }

    public static DataTable GetFileFoldersInfor(string officeID1, string letter)
    {
        DataTable table = new DataTable("FileForm");
        try
        {
            string sqlstr = "EXEC [dbo].[eFileFolder_SearchFileFoldersInforByLetter] '" + officeID1 + "','" + letter + "%'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }

        return table;
    }

    public static void InsertDocumentTags(string UID, string TagKey, string TagValue, int FileID)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_AddDcoumentTags";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter param1 = command.Parameters.AddWithValue("@Tag", TagKey);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@TagValue", TagValue);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlParameter param3 = command.Parameters.AddWithValue("@UserId", UID);
            param3.Direction = ParameterDirection.Input;
            param3.DbType = DbType.String;

            SqlParameter param4 = command.Parameters.AddWithValue("@FileId", FileID);
            param4.Direction = ParameterDirection.Input;
            param4.DbType = DbType.Int32;

            command.ExecuteNonQuery();

        }
        catch (Exception ex)
        {

        }
    }

    public static DataTable GetUserDocumentSearchCriteria(string UserID)
    {
        DataTable table = new DataTable("SearchCriteria");
        try
        {
            string sqlstr = "Select DscID,UserID,TagKey FROM DocumentSearchCriteria Where UserID='" + UserID + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }
        return table;
    }

    public static DataTable GetUserDocumentAvailableSearchKeys(string UserID)
    {
        DataTable table = new DataTable("SearchKeys");
        try
        {
            string sqlstr = "Select DISTINCT Tag AS Tag FROM Tags Except Select DISTINCT TagKey AS Tag  FROM DocumentSearchCriteria WHERE UserID ='" + UserID + "'";

            SqlHelper sql = new SqlHelper("Default");

            sql.Fill(table, sqlstr);
            return table;
        }
        catch (Exception ex)
        {
            new ApplicationException(ex.Message);
        }
        return table;
    }

    public static void AddUserSearchCriteria(string TagKey, string UserId)
    {
        try
        {
            string SQL = "INSERT INTO DocumentSearchCriteria (UserID,TagKey) VALUES ('" + UserId + "','" + TagKey + "')";
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);
        }
        catch (Exception ex)
        {

        }
    }

    public static void DeleteUserSearchCriteria(string TagKey, string UserId)
    {
        try
        {
            string SQL = "DELETE FROM DocumentSearchCriteria WHERE UserId =  '" + UserId + "' AND TagKey = '" + TagKey + "'";
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);
        }
        catch (Exception ex)
        {

        }
    }


    public static DataTable SearchDocumentByTags(NameValueCollection Params, string UID, string DocumentName)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SearchDocuments";
            command.CommandType = CommandType.StoredProcedure;

            string ParamKey = "@TagKey{0}";
            string ParamKeyValue = "@TagKey{0}_Val";

            SqlParameter param0 = command.Parameters.AddWithValue("@UID", UID);
            param0.Direction = ParameterDirection.Input;
            param0.DbType = DbType.String;

            SqlParameter param12 = command.Parameters.AddWithValue("@DocumentName", DocumentName);
            param12.Direction = ParameterDirection.Input;
            param12.DbType = DbType.String;


            int i = 1;
            foreach (string key in Params.AllKeys)
            {
                if (key != "@UID")
                {
                    var values = Params.GetValues(key);
                    foreach (string value in values)
                    {
                        SqlParameter param1 = command.Parameters.AddWithValue(string.Format(ParamKey, i.ToString()), key);
                        param1.Direction = ParameterDirection.Input;
                        param1.DbType = DbType.String;

                        SqlParameter param2 = command.Parameters.AddWithValue(string.Format(ParamKeyValue, i.ToString()), value);
                        param2.Direction = ParameterDirection.Input;
                        param2.DbType = DbType.String;
                    }
                    i += 1;
                }
            }

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable PrepareLoginRestStruct(string hdnValue, string uid, bool isChecked)
    {
        DataTable dtIndex = new DataTable();
        try
        {
            LoginTimeRestriction[] loginRest = JsonConvert.DeserializeObject<List<LoginTimeRestriction>>(hdnValue).ToArray();
            dtIndex.Columns.Add("UID", typeof(string));
            dtIndex.Columns.Add("FromTime", typeof(TimeSpan));
            dtIndex.Columns.Add("ToTime", typeof(TimeSpan));
            dtIndex.Columns.Add("Day", typeof(int));
            dtIndex.Columns.Add("IsTimeRestrictied", typeof(bool));

            foreach (LoginTimeRestriction item in loginRest)
            {

                TimeSpan fromTime = new TimeSpan();
                TimeSpan toTime = new TimeSpan();
                DateTime dt = DateTime.ParseExact(item.From, "h:mm tt", CultureInfo.InvariantCulture);
                if (dt != null)
                    fromTime = dt.TimeOfDay;
                dt = DateTime.ParseExact(item.To, "h:mm tt", CultureInfo.InvariantCulture);
                if (dt != null)
                    toTime = dt.TimeOfDay;
                if (item.Day == (int)Enum_Tatva.WeekDays.AllDays)
                {
                    for (int i = 0; i <= (int)Enum_Tatva.WeekDays.AllDays - 1; i++)
                    {
                        dtIndex.Rows.Add(new object[] { uid, fromTime, toTime, i, isChecked });
                    }
                }
                else
                    dtIndex.Rows.Add(new object[] { uid, fromTime, toTime, item.Day, isChecked });
            }

        }
        catch (Exception ex)
        { }
        return dtIndex;
    }

    public static DataTable GetDividerByEffid(int EFFID)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            // keyname = string.Join(",", keyname.Split(new char[0], StringSplitOptions.RemoveEmptyEntries));
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "GetDividersByEffid";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@Effid", EFFID);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.Int32;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }
    #region DocumentSearching

    /// <summary>
    /// Get Searched Documents on Retention Logs Page
    /// </summary>
    /// <param name="uid"></param>
    /// <param name="folderName"></param>
    /// <param name="documentName"></param>
    /// <returns></returns>
    public static DataTable GetSearchedDocsByUID(string uid, string folderName, string documentName)
    {
        using (SqlConnection connection = new SqlConnection(ConnectionString))
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "eFileFolder_SelectSearchedDocs";
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter[] parameters = new SqlParameter[3];
                parameters[0] = new SqlParameter("@UID", SqlDbType.NVarChar, 50);
                parameters[0].Value = uid;
                parameters[1] = new SqlParameter("@FolderName", SqlDbType.NVarChar, 50);
                parameters[1].Value = folderName;
                parameters[2] = new SqlParameter("@DocumentName", SqlDbType.NVarChar, 50);
                parameters[2].Value = documentName;
                command.Parameters.AddRange(parameters);

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;

                DataTable dt = new DataTable();

                dataAdapter.Fill(dt);
                foreach (DataRow row in dt.Rows)
                {
                    if ((row["Action"].ToString()) == "1")
                        row["Action"] = Enum_Tatva.Action.Create;
                }
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                connection.Close();
            }
    }
    #endregion
}