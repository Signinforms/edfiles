﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///FileItem 的摘要说明
/// </summary>
public class FileItem
{
    public FileItem()
    {
        PageItemList = new List<PageItem>();
    }

    public int Id { get; set; }

    public int Idd { get; set; }

    public string Name { get; set; }

    public string DisplayName { get; set; }

    public string FullName { get; set; }

    public string Path { get; set; }

    public int Count { get; set; }

    public List<PageItem> PageItemList { get; set; }

    public int DocumentOrder { get; set; }

    public string ClassName { get; set; }
    public int FileExtention { get; set; }
}