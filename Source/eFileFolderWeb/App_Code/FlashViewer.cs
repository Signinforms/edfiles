using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using DataQuicker2.Framework;
using Shinetech.DAL;
using Shinetech.Utility;
using System.IO;

/// <summary>
/// Summary description for FileFolderManagement
/// </summary>
public class FlashViewer
{
    public static string ConnectionName = "Default";
    private static readonly string ConnectionString =
        ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
    private static readonly string WEBSITE = ConfigurationManager.AppSettings["WEBSITE"];
    private static readonly string MailTo = ConfigurationManager.AppSettings["COMPLIANCEEMAIL"];
    public FlashViewer()
    {

    }

    //binguo - get dataset for filefolder xml data
    public static FolderData FlashViewer_GetFolderJsonData(int folderId, bool isEdit = true, bool isAccessDenied = false)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_GetFolderData";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@FolderId", folderId);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.Int32;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataSet ds = new DataSet("FolderData");

            dataAdapter.Fill(ds);

            //set tables' name
            if (ds.Tables.Count == 2)
            {
                ds.Tables[0].TableName = "Folder";
                ds.Tables[1].TableName = "Tab";
            }

            var folderdata = new FolderData();
            folderdata.alertsList = new List<Alert>();

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                folderdata.FirstName = Convert.ToString(item["Firstname"]);
                folderdata.LastName = Convert.ToString(item["LastName"]);
                folderdata.Name = Convert.ToString(item["Name"]);
                folderdata.Id = Convert.ToInt32(item["Id"]);
                folderdata.UID = Convert.ToString(item["UID"]);
                folderdata.SecurityLevel = Convert.ToString(item["SecurityLevel"]);
                folderdata.alertsList.Add(new Alert { alert = "Alert1: " + Convert.ToString(item["Alert1"]) });
                folderdata.alertsList.Add(new Alert { alert = "Alert2: " + Convert.ToString(item["Alert2"]) });
                folderdata.alertsList.Add(new Alert { alert = "Comments: " + Convert.ToString(item["Comments"]) });
            }

            folderdata.tabsList = new List<Tab>();

            foreach (DataRow item in ds.Tables[1].Rows)
            {
                if (((isEdit == false || (isEdit && isAccessDenied)) && Convert.ToString(item["Locked"]) == "True"))
                    continue;
                Tab tab = new Tab();

                tab.Color = Convert.ToString(item["Color"]);
                tab.Name = Convert.ToString(item["Name"]);
                tab.Id = Convert.ToInt32(item["Id"]);

                folderdata.tabsList.Add(tab);
            }

            return folderdata;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    //binguo - get dataset for filefolder xml data
    public static DataSet FlashViewer_GetFolderData(int folderId)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_GetFolderData";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@FolderId", folderId);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.Int32;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataSet ds = new DataSet("FolderData");

            dataAdapter.Fill(ds);

            //set tables' name
            if (ds.Tables.Count == 2)
            {
                ds.Tables[0].TableName = "Folder";
                ds.Tables[1].TableName = "Tab";
            }

            return ds;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    //luyuanzong - get dataset for filefolder xml data
    public static FolderData FlashViewer_GetFolderJsonData(int folderId)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_GetFolderData_With_Locked";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@FolderId", folderId);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.Int32;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataSet ds = new DataSet("FolderData");

            dataAdapter.Fill(ds);

            //set tables' name
            if (ds.Tables.Count == 2)
            {
                ds.Tables[0].TableName = "Folder";
                ds.Tables[1].TableName = "Tab";
            }

            var folderdata = new FolderData();
            folderdata.alertsList = new List<Alert>();

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                folderdata.FirstName = Convert.ToString(item["Firstname"]);
                folderdata.LastName = Convert.ToString(item["LastName"]);
                folderdata.Name = Convert.ToString(item["Name"]);
                folderdata.Id = Convert.ToInt32(item["Id"]);
                folderdata.SecurityLevel = Convert.ToString(item["SecurityLevel"]);
                folderdata.alertsList.Add(new Alert { alert = "Alert1: " + Convert.ToString(item["Alert1"]) });
                folderdata.alertsList.Add(new Alert { alert = "Alert2: " + Convert.ToString(item["Alert2"]) });
                folderdata.alertsList.Add(new Alert { alert = "Comments: " + Convert.ToString(item["Comments"]) });
            }

            folderdata.tabsList = new List<Tab>();

            foreach (DataRow item in ds.Tables[1].Rows)
            {
                Tab tab = new Tab();
                tab.Color = Convert.ToString(item["Color"]);
                tab.Name = Convert.ToString(item["Name"]);
                tab.Id = Convert.ToInt32(item["Id"]);

                folderdata.tabsList.Add(tab);
            }

            return folderdata;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    //luyuanzong  - get dataset for filefolder xml data
    public static DataSet FlashViewer_GetFolderData(int folderId, bool locked)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_GetFolderData_With_Locked";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@FolderId", folderId);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.Int32;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataSet ds = new DataSet("FolderData");

            dataAdapter.Fill(ds);

            //set tables' name
            if (ds.Tables.Count == 2)
            {
                ds.Tables[0].TableName = "Folder";
                ds.Tables[1].TableName = "Tab";
            }

            return ds;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }


    //binguo - get dataset for filefolder xml data
    public static DividerData FlashViewer_GetTabDataJson(int tabId)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_GetTabData";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@TabId", tabId);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.Int32;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataSet ds = new DataSet("FolderData");

            dataAdapter.Fill(ds);

            //set tables' name
            if (ds.Tables.Count == 5)
            {
                ds.Tables[0].TableName = "Tab";
                ds.Tables[1].TableName = "File";
                ds.Tables[2].TableName = "Page";
                ds.Tables[3].TableName = "QuickNote";
                ds.Tables[4].TableName = "NewNote";
            }

            var dividerData = new DividerData();

            if (ds.Tables.Count > 0)
            {
                dividerData.Id = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
                dividerData.FolderId = Convert.ToInt32(ds.Tables[0].Rows[0]["EFFID"]);
                dividerData.Name = Convert.ToString(ds.Tables[0].Rows[0]["Name"]);
                dividerData.MetaXML = Convert.ToString(ds.Tables[0].Rows[0]["MetaXML"]);

                dividerData.TotalPages = 0;
            }

            if (ds.Tables.Count > 1)
            {
                dividerData.TotalPages = ds.Tables[2].Rows.Count;

                foreach (DataRow file in ds.Tables[1].Rows)
                {
                    var fileItem = new FileItem();
                    fileItem.Id = Convert.ToInt32(file["Id"]);
                    fileItem.Idd = tabId;
                    fileItem.Name = Convert.ToString(file["Name"]);
                    fileItem.DisplayName = Convert.ToString(file["DisplayName"]);

                    dividerData.FileItemList.Add(fileItem);

                    foreach (DataRow page in ds.Tables[2].Rows)
                    {
                        if (Convert.ToInt32(page["FileId"]) == fileItem.Id)
                        {
                            var pageItem = new PageItem();
                            pageItem.Id = Convert.ToInt32(page["Id"]);
                            pageItem.FileId = Convert.ToInt32(page["FileId"]);

                            pageItem.Url_1 = Convert.ToString(page["Url_1"]).Replace("\\", "/");
                            pageItem.Url_2 = Convert.ToString(page["Url_2"]).Replace("\\", "/");
                            pageItem.Ext = Path.GetExtension(pageItem.Url_1);
                            fileItem.PageItemList.Add(pageItem);
                        }
                    }

                }
            }


            if (ds.Tables.Count > 3)
            {
                foreach (DataRow note in ds.Tables[3].Rows)
                {
                    var quitNoteItem = new QuitNoteItem();
                    quitNoteItem.Id = Convert.ToInt32(note["Id"]);
                    quitNoteItem.PageId = Convert.ToInt32(note["PageID"]);

                    quitNoteItem.Content = Convert.ToString(note["Content"]);
                    quitNoteItem.LastModDate = Convert.ToDateTime(note["DateTime"]);

                    quitNoteItem.X = Convert.ToInt32(note["x"]);
                    quitNoteItem.Y = Convert.ToInt32(note["y"]);
                    quitNoteItem.Fixed = Convert.ToBoolean(note["Fixed"]);
                    quitNoteItem.Opened = Convert.ToBoolean(note["Opened"]);

                    dividerData.QuiteNoteList.Add(quitNoteItem);
                }
            }

            if (ds.Tables.Count > 4)
            {
                dividerData.TotalPages += ds.Tables[4].Rows.Count;
                NewNoteItem noteItem;
                foreach (DataRow note in ds.Tables[4].Rows)
                {
                    noteItem = new NewNoteItem();
                    noteItem.Id = Convert.ToInt32(note["Id"]);
                    noteItem.DividerId = tabId;
                    noteItem.Title = Convert.ToString(note["Title"]);
                    noteItem.Author = Convert.ToString(note["Author"]);
                    noteItem.Content = Convert.ToString(note["Content"]);
                    noteItem.LastModDate = Convert.ToDateTime(note["DateTime"]);

                    dividerData.NewNoteList.Add(noteItem);
                }

                dividerData.TotalPages += 1;

                noteItem = new NewNoteItem();
                noteItem.Id = 123456;
                noteItem.DividerId = tabId;
                noteItem.Title = "用于测试Note的分页";
                noteItem.Author = "卢远宗";
                noteItem.Content = "用于测试Note的分页";
                noteItem.LastModDate = DateTime.Now;

                dividerData.NewNoteList.Add(noteItem);
            }

            return dividerData;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DividerData FlashViewer_GetTabPDFJson(int tabId)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_GetTabPDFData_PageKey";//"FlashViewer_GetTabData";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@TabId", tabId);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.Int32;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataSet ds = new DataSet("FolderData");

            dataAdapter.Fill(ds);

            //set tables' name
            if (ds.Tables.Count == 4)
            {
                ds.Tables[0].TableName = "Tab";
                ds.Tables[1].TableName = "File";
                //ds.Tables[2].TableName = "Page";
                ds.Tables[2].TableName = "QuickNote";
                ds.Tables[3].TableName = "NewNote";
            }

            var dividerData = new DividerData();

            if (ds.Tables.Count > 0)
            {
                dividerData.Id = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
                dividerData.FolderId = Convert.ToInt32(ds.Tables[0].Rows[0]["EFFID"]);
                dividerData.Name = Convert.ToString(ds.Tables[0].Rows[0]["Name"]);
                dividerData.MetaXML = Convert.ToString(ds.Tables[0].Rows[0]["MetaXML"]);

                dividerData.TotalPages = 0;
            }

            if (ds.Tables.Count > 1)
            {
                //dividerData.TotalPages = ds.Tables[2].Rows.Count;
                string extname = ".pdf";
                foreach (DataRow file in ds.Tables[1].Rows)
                {
                    var fileItem = new FileItem();
                    fileItem.Id = Convert.ToInt32(file["Id"]);
                    fileItem.Idd = tabId;
                    fileItem.Name = Convert.ToString(file["Name"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(file["DisplayName"])))
                    {
                        fileItem.DisplayName = Convert.ToString(file["DisplayName"]);
                    }
                    else
                    {
                        fileItem.Name = Convert.ToString(file["Name"]);
                    }
                    //fileItem.DisplayName = Convert.ToString(file["DisplayName"]);
                    fileItem.DocumentOrder = Convert.ToInt32(file["DocumentOrder"]);

                    fileItem.Path = Convert.ToString(file["Path"]);
                    fileItem.ClassName = Convert.ToString(file["Class"]);

                    // start get all file of pdf and jpg extension
                    extname = "";
                    if (Convert.ToInt32(file["FileExtention"]) == Shinetech.Engines.StreamType.PDF.GetHashCode() ||
                        Convert.ToInt32(file["FileExtention"]) == Shinetech.Engines.StreamType.JPG.GetHashCode())
                    {
                        extname = "." + Enum.GetName(typeof(Shinetech.Engines.StreamType), Convert.ToInt32(file["FileExtention"]));
                    }
                    else
                    {
                        extname = Path.GetExtension(Convert.ToString(file["FullFileName"]));
                    }

                    //if (Convert.ToString(file["FileExtention"]).Trim() != "1")
                    //{
                    //    //extname = string.Format("_{0}_{1}.pdf", Convert.ToString(file["FileFolderID"]), Convert.ToString(file["DividerID"]));
                    //    //extname = "." + Convert.ToString(file["FileExtention"]).Trim();
                    //    extname = Path.GetExtension(fileItem.Name);
                    //}

                    // end  get all file of pdf and jpg extension
                    fileItem.FileExtention = Convert.ToInt32(file["FileExtention"]);
                    fileItem.FullName = Path.Combine(fileItem.Path, HttpUtility.UrlPathEncode(fileItem.Name) + extname).Replace("\\", "/");
                    fileItem.Count = Convert.ToInt32(file["PagesCount"]);

                    dividerData.TotalPages += fileItem.Count;

                    dividerData.FileItemList.Add(fileItem);
                }
            }


            if (ds.Tables.Count > 2)
            {
                foreach (DataRow note in ds.Tables[2].Rows)
                {
                    var quitNoteItem = new QuitNoteItem();
                    quitNoteItem.Id = Convert.ToInt32(note["Id"]);
                    quitNoteItem.PageId = Convert.ToInt32(note["PageID"] == DBNull.Value ? 0 : note["PageID"]);
                    quitNoteItem.PageKey = Convert.ToString(note["PageKey"] == DBNull.Value ? "" : note["PageKey"]);

                    quitNoteItem.Content = Convert.ToString(note["Content"]);
                    quitNoteItem.LastModDate = Convert.ToDateTime(note["DateTime"]);

                    quitNoteItem.X = Convert.ToInt32(note["x"]);
                    quitNoteItem.Y = Convert.ToInt32(note["y"]);
                    quitNoteItem.Fixed = Convert.ToBoolean(note["Fixed"]);
                    quitNoteItem.Opened = Convert.ToBoolean(note["Opened"]);

                    dividerData.QuiteNoteList.Add(quitNoteItem);
                }
            }

            if (ds.Tables.Count > 3)
            {
                dividerData.TotalPages += ds.Tables[3].Rows.Count;
                NewNoteItem noteItem;
                foreach (DataRow note in ds.Tables[3].Rows)
                {
                    noteItem = new NewNoteItem();
                    noteItem.Id = Convert.ToInt32(note["Id"]);
                    noteItem.DividerId = tabId;
                    noteItem.Title = Convert.ToString(note["Title"]);
                    noteItem.Author = Convert.ToString(note["Author"]);
                    noteItem.Content = Convert.ToString(note["Content"]);
                    noteItem.LastModDate = Convert.ToDateTime(note["DateTime"]);

                    dividerData.NewNoteList.Add(noteItem);
                }
                // dividerData.TotalPages为真实的总页数
            }

            return dividerData;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    /// <summary>
    /// Delete document and all pages related documentId
    /// </summary>
    /// <param name="DocumentId"></param>
    /// <returns></returns>
    public static DataSet DeleteDocumentAndPages(string DocumentId, int DocumentOrder, string deletedBy)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "DeleteDocumentAndPages";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters = new SqlParameter[3];
            parameters[0] = new SqlParameter("@DocumentId", SqlDbType.Int);
            parameters[0].Value = DocumentId;
            parameters[1] = new SqlParameter("@DocumentOrder", SqlDbType.Int);
            parameters[1].Value = DocumentOrder;
            parameters[2] = new SqlParameter("@DeletedBy", SqlDbType.NVarChar, 50);
            parameters[2].Value = deletedBy;

            command.Parameters.AddRange(parameters);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataSet page = new DataSet();

            dataAdapter.Fill(page);

            return page;
        }
        catch (Exception ex)
        {
            throw;
        }
        finally
        {
            connection.Close();
        }
    }

    /// <summary>
    /// update quick note based on changed divider id and document id
    /// </summary>
    /// <param name="dividerId"></param>
    /// <param name="newDocumentId"></param>
    /// <param name="oldDocumentId"></param>
    /// <returns></returns>
    public static bool UpdateQuickNoteDividerId(int dividerId, int newDocumentId, int oldDocumentId)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "UpdateQuickNoteDividerId";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters = new SqlParameter[3];
            parameters[0] = new SqlParameter("@DividerId", SqlDbType.Int);
            parameters[0].Value = dividerId;
            parameters[1] = new SqlParameter("@NewDocumentId", SqlDbType.Int);
            parameters[1].Value = newDocumentId;
            parameters[2] = new SqlParameter("@OldDocumentId", SqlDbType.Int);
            parameters[2].Value = oldDocumentId;

            command.Parameters.AddRange(parameters);
            command.ExecuteNonQuery();

            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
            connection.Close();
        }
    }

    //public static DataSet GetImageName(string DocumentID)
    //{
    //    SqlConnection connection = new SqlConnection(ConnectionString);
    //    try
    //    {
    //        connection.Open();
    //        SqlCommand command = new SqlCommand();
    //        command.Connection = connection;
    //        command.CommandText = "GetImageName";
    //        command.CommandType = CommandType.StoredProcedure;

    //        SqlParameter[] parameters = new SqlParameter[1];
    //        parameters[0] = new SqlParameter("@DocumnetId", SqlDbType.Int);
    //        parameters[0].Value = DocumentID;

    //        SqlDataAdapter dataAdapter = new SqlDataAdapter();
    //        dataAdapter.SelectCommand = command;

    //        DataSet dsImageName = new DataSet();

    //        dataAdapter.Fill(dsImageName);

    //        return dsImageName;
    //    }
    //    catch (Exception ex)
    //    {
    //        return null;
    //    }
    //    finally
    //    {
    //        connection.Close();
    //    }
    //}

    //binguo - get dataset for filefolder xml data
    public static DataSet FlashViewer_GetTabData(int tabId)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_GetTabData";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@TabId", tabId);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.Int32;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataSet ds = new DataSet("FolderData");

            dataAdapter.Fill(ds);

            //set tables' name
            if (ds.Tables.Count == 5)
            {
                ds.Tables[0].TableName = "Tab";
                ds.Tables[1].TableName = "File";
                ds.Tables[2].TableName = "Page";
                ds.Tables[3].TableName = "QuickNote";
                ds.Tables[4].TableName = "NewNote";
            }

            foreach (DataRow row in ds.Tables[2].Rows)
            {
                string url = row[2] as string;

                row[2] = url.Replace('\\', '/');
            }

            return ds;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static long FlashViewer_UpdateQuickNote(string uid, long id, int pageId, string content, int x, int y, bool bFixed, bool bOpened)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_UpdateQuickNote";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter[] parameters = new SqlParameter[8];
            parameters[0] = new SqlParameter("@UID", SqlDbType.VarChar, 36);
            parameters[0].Value = DBNull.Value;     // temp uid:"54674792-27ff-4b19-9eb7-1a3e05289c2e";   //uid;
            parameters[1] = new SqlParameter("@Id", SqlDbType.BigInt);
            parameters[1].Value = id;
            parameters[1].Direction = ParameterDirection.InputOutput;
            parameters[2] = new SqlParameter("@PageId", SqlDbType.Int);
            parameters[2].Value = pageId;
            parameters[3] = new SqlParameter("@Content", SqlDbType.NVarChar, 1000);
            parameters[3].Value = content;
            parameters[4] = new SqlParameter("@X", SqlDbType.Int);
            parameters[4].Value = x;
            parameters[5] = new SqlParameter("@Y", SqlDbType.Int);
            parameters[5].Value = y;
            parameters[6] = new SqlParameter("@Fixed", SqlDbType.Bit);
            parameters[6].Value = bFixed ? 1 : 0;
            parameters[7] = new SqlParameter("@Opened", SqlDbType.Bit);
            parameters[7].Value = bOpened ? 1 : 0;

            command.Parameters.AddRange(parameters);
            command.ExecuteNonQuery();
            return long.Parse(parameters[1].Value.ToString());
        }
        catch (Exception ex)
        {
            return 0;
        }
        finally
        {
            connection.Close();
        }
    }

    public static long FlashViewer_UpdateQuickNote(string uid, long id, string pagekey, string content, int x, int y, bool bFixed, bool bOpened)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_UpdateQuickNoteWithKey";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter[] parameters = new SqlParameter[8];
            parameters[0] = new SqlParameter("@UID", SqlDbType.VarChar, 36);
            parameters[0].Value = DBNull.Value;     // temp uid:"54674792-27ff-4b19-9eb7-1a3e05289c2e";   //uid;
            parameters[1] = new SqlParameter("@Id", SqlDbType.BigInt);
            parameters[1].Value = id;
            parameters[1].Direction = ParameterDirection.InputOutput;
            parameters[2] = new SqlParameter("@PageKey", SqlDbType.NVarChar, 10);
            parameters[2].Value = pagekey;
            parameters[3] = new SqlParameter("@Content", SqlDbType.NVarChar, 1000);
            parameters[3].Value = content;
            parameters[4] = new SqlParameter("@X", SqlDbType.Int);
            parameters[4].Value = x;
            parameters[5] = new SqlParameter("@Y", SqlDbType.Int);
            parameters[5].Value = y;
            parameters[6] = new SqlParameter("@Fixed", SqlDbType.Bit);
            parameters[6].Value = bFixed ? 1 : 0;
            parameters[7] = new SqlParameter("@Opened", SqlDbType.Bit);
            parameters[7].Value = bOpened ? 1 : 0;

            command.Parameters.AddRange(parameters);
            command.ExecuteNonQuery();
            return long.Parse(parameters[1].Value.ToString());
        }
        catch (Exception ex)
        {
            return 0;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DividerData FlashViewer_UpdateQuickNote(string uid, long id, string pagekey, string content, int dividerid,
        int x, int y, bool bFixed, bool bOpened)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_UpdateQuickNoteWithKey";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter[] parameters = new SqlParameter[9];
            parameters[0] = new SqlParameter("@UID", SqlDbType.VarChar, 36);
            parameters[0].Value = DBNull.Value;     // temp uid:"54674792-27ff-4b19-9eb7-1a3e05289c2e";   //uid;
            parameters[1] = new SqlParameter("@Id", SqlDbType.BigInt);
            parameters[1].Value = id;
            parameters[1].Direction = ParameterDirection.InputOutput;
            parameters[2] = new SqlParameter("@PageKey", SqlDbType.NVarChar, 10);
            parameters[2].Value = pagekey;
            parameters[3] = new SqlParameter("@Content", SqlDbType.NVarChar, 1000);
            parameters[3].Value = content;
            parameters[4] = new SqlParameter("@DividerId", SqlDbType.Int);
            parameters[4].Value = dividerid;
            parameters[5] = new SqlParameter("@X", SqlDbType.Int);
            parameters[5].Value = x;
            parameters[6] = new SqlParameter("@Y", SqlDbType.Int);
            parameters[6].Value = y;
            parameters[7] = new SqlParameter("@Fixed", SqlDbType.Bit);
            parameters[7].Value = bFixed ? 1 : 0;
            parameters[8] = new SqlParameter("@Opened", SqlDbType.Bit);
            parameters[8].Value = bOpened ? 1 : 0;

            command.Parameters.AddRange(parameters);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;
            DataSet ds = new DataSet();
            dataAdapter.Fill(ds);
            var dividerData = new DividerData();

            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {

                var quitNoteItem = new QuitNoteItem();
                quitNoteItem.Id = Convert.ToInt32(ds.Tables[0].Rows[0]["Id"]);
                quitNoteItem.PageId = Convert.ToInt32(ds.Tables[0].Rows[0]["PageID"] == DBNull.Value ? 0 : ds.Tables[0].Rows[0]["PageID"]);
                quitNoteItem.PageKey = Convert.ToString(ds.Tables[0].Rows[0]["PageKey"] == DBNull.Value ? "" : ds.Tables[0].Rows[0]["PageKey"]);

                quitNoteItem.Content = Convert.ToString(ds.Tables[0].Rows[0]["Content"]);
                quitNoteItem.LastModDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["DateTime"]);

                quitNoteItem.X = Convert.ToInt32(ds.Tables[0].Rows[0]["x"]);
                quitNoteItem.Y = Convert.ToInt32(ds.Tables[0].Rows[0]["y"]);
                quitNoteItem.Fixed = Convert.ToBoolean(ds.Tables[0].Rows[0]["Fixed"]);
                quitNoteItem.Opened = Convert.ToBoolean(ds.Tables[0].Rows[0]["Opened"]);

                dividerData.QuiteNoteList.Add(quitNoteItem);
            }
            return dividerData;
            //command.ExecuteNonQuery();
            //return long.Parse(parameters[1].Value.ToString());
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            connection.Close();
        }
    }

    public static long FlashViewer_UpdateURLNote(string uid, long id, int pageId, string title, string content, int x, int y)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_UpdateURLNote";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter[] parameters = new SqlParameter[8];
            parameters[0] = new SqlParameter("@UID", SqlDbType.VarChar, 36);
            parameters[0].Value = DBNull.Value;     // temp uid:"54674792-27ff-4b19-9eb7-1a3e05289c2e";   //uid;
            parameters[1] = new SqlParameter("@Id", SqlDbType.BigInt);
            parameters[1].Value = id;
            parameters[1].Direction = ParameterDirection.InputOutput;
            parameters[2] = new SqlParameter("@PageId", SqlDbType.Int);
            parameters[2].Value = pageId;
            parameters[3] = new SqlParameter("@title", SqlDbType.NVarChar, 50);

            parameters[3] = new SqlParameter("@Content", SqlDbType.NVarChar, 300);
            parameters[3].Value = content;
            parameters[4] = new SqlParameter("@X", SqlDbType.Int);
            parameters[4].Value = x;
            parameters[5] = new SqlParameter("@Y", SqlDbType.Int);
            parameters[5].Value = y;


            command.Parameters.AddRange(parameters);
            command.ExecuteNonQuery();
            return long.Parse(parameters[1].Value.ToString());
        }
        catch (Exception ex)
        {
            return 0;
        }
        finally
        {
            connection.Close();
        }
    }

    public static long FlashViewer_UpdateNewNote(string uid, long id, int tabId, string content, string strTitle, string strAuthor)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_UpdateNewNote";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter[] parameters = new SqlParameter[6];
            parameters[0] = new SqlParameter("@UID", SqlDbType.VarChar, 36);
            parameters[0].Value = DBNull.Value;     // temp uid:"54674792-27ff-4b19-9eb7-1a3e05289c2e";   //uid;
            parameters[1] = new SqlParameter("@Id", SqlDbType.BigInt);
            parameters[1].Value = id;
            parameters[1].Direction = ParameterDirection.InputOutput;
            parameters[2] = new SqlParameter("@TabId", SqlDbType.Int);
            parameters[2].Value = tabId;
            parameters[3] = new SqlParameter("@Content", SqlDbType.NText);
            parameters[3].Value = content;
            parameters[4] = new SqlParameter("@Title", SqlDbType.NVarChar, 60);
            parameters[4].Value = strTitle;
            parameters[5] = new SqlParameter("@Author", SqlDbType.NVarChar, 50);
            parameters[5].Value = strAuthor;


            command.Parameters.AddRange(parameters);
            command.ExecuteNonQuery();

            return long.Parse(parameters[1].Value.ToString());
        }
        catch (Exception ex)
        {
            return 0;
        }
        finally
        {
            connection.Close();
        }
    }

    public static bool FlashViewer_DeleteQuickNote(long id)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_DeleteQuickNote";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = new SqlParameter("@Id", SqlDbType.BigInt);
            parameters[0].Value = id;

            command.Parameters.AddRange(parameters);
            command.ExecuteNonQuery();

            //todo:binguo - if this new note dosn't exist, then faild.
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
            connection.Close();
        }
    }

    public static bool FlashViewer_DeleteURLNote(long id)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_DeleteURLNote";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = new SqlParameter("@Id", SqlDbType.BigInt);
            parameters[0].Value = id;

            command.Parameters.AddRange(parameters);
            command.ExecuteNonQuery();

            //todo:binguo - if this new note dosn't exist, then faild.
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
            connection.Close();
        }
    }

    public static bool FlashViewer_DeleteNewNote(long id)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_DeleteNewNote";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter[] parameters = new SqlParameter[1];
            parameters[0] = new SqlParameter("@Id", SqlDbType.BigInt);
            parameters[0].Value = id;

            command.Parameters.AddRange(parameters);
            command.ExecuteNonQuery();

            //todo:binguo - if this new note dosn't exist, then faild.
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
            connection.Close();
        }
    }

    /// <summary>
    /// get a validate code for one folder
    /// </summary>
    /// <param name="folderId"></param>
    /// <param name="mode">if mode=0 the code canbe used always until it is deleted; if mode=1 the code is only can be used for 1 day</param>
    /// <returns></returns>
    public static string FlashViewer_GetValidateCode(int folderId, int mode)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_GetValidateCode";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter[] parameters = new SqlParameter[3];
            parameters[0] = new SqlParameter("@FolderId", SqlDbType.Int);
            parameters[0].Value = folderId;
            parameters[1] = new SqlParameter("@Mode", SqlDbType.Int);
            parameters[1].Value = mode;
            parameters[2] = new SqlParameter("@ValidateCode", SqlDbType.NChar, 15);
            parameters[2].Value = "";
            parameters[2].Direction = ParameterDirection.Output;
            command.Parameters.AddRange(parameters);
            command.ExecuteNonQuery();
            return parameters[2].Value.ToString();
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
        finally
        {
            connection.Close();
        }
    }

    /// <summary>
    /// get a validate code for one folder
    /// </summary>
    /// <param name="folderId"></param>
    /// <returns></returns>
    public static string FlashViewer_GetValidateCode_FileFolderShare(int folderId)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_GetValidateCode_FileFolderShare";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter[] parameters = new SqlParameter[2];
            parameters[0] = new SqlParameter("@FolderId", SqlDbType.Int);
            parameters[0].Value = folderId;
            parameters[1] = new SqlParameter("@ValidateCode", SqlDbType.NChar, 15);
            parameters[1].Value = "";
            parameters[1].Direction = ParameterDirection.Output;
            command.Parameters.AddRange(parameters);
            command.ExecuteNonQuery();
            return parameters[1].Value.ToString();
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
        finally
        {
            connection.Close();
        }
    }

    /// <summary>
    /// check if the validatecode is available
    /// </summary>
    /// <param name="validateCode">this validatecode consistants of folderId and real code like 2_C3F291003340423.</param>
    /// <returns></returns>
    public static bool FlashViewer_CheckValidateCode(string validateCode)
    {
        //check the string format like folderid_code
        if (!Regex.IsMatch(validateCode, @"^[1-9]\d{0,11}_(\w|\d){15}$"))
            return false;
        string[] results = Regex.Split(validateCode, "_");
        int folderId = Convert.ToInt32(results[0]);
        string realCode = results[1];

        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_CheckValidateCode";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter[] parameters = new SqlParameter[3];
            parameters[0] = new SqlParameter("@FolderId", SqlDbType.Int);
            parameters[0].Value = folderId;
            parameters[1] = new SqlParameter("@ValidateCode", SqlDbType.NChar, 15);
            parameters[1].Value = realCode;
            parameters[2] = new SqlParameter("@Result", SqlDbType.Int);
            parameters[2].Value = -1;
            parameters[2].Direction = ParameterDirection.Output;
            command.Parameters.AddRange(parameters);
            command.ExecuteNonQuery();
            return (parameters[2].Value.ToString() == "1");
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
            connection.Close();
        }
    }

    /// <summary>
    /// check if the validatecode is available
    /// </summary>
    /// <param name="validateCode">this validatecode consistants of folderId and real code like 2_C3F291003340423.</param>
    /// <returns></returns>
    public static bool FlashViewer_CheckValidateCode_FileFolderShare(string validateCode)
    {
        //check the string format like folderid_code
        if (!Regex.IsMatch(validateCode, @"^[1-9]\d{0,11}_(\w|\d){15}$"))
            return false;
        string[] results = Regex.Split(validateCode, "_");
        int folderId = Convert.ToInt32(results[0]);
        string realCode = results[1];

        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_CheckValidateCode_FileFolderShare";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter[] parameters = new SqlParameter[3];
            parameters[0] = new SqlParameter("@FolderId", SqlDbType.Int);
            parameters[0].Value = folderId;
            parameters[1] = new SqlParameter("@ValidateCode", SqlDbType.NChar, 15);
            parameters[1].Value = realCode;
            parameters[2] = new SqlParameter("@Result", SqlDbType.Int);
            parameters[2].Value = -1;
            parameters[2].Direction = ParameterDirection.Output;
            command.Parameters.AddRange(parameters);
            command.ExecuteNonQuery();
            return (parameters[2].Value.ToString() == "1");
        }
        catch (Exception ex)
        {
            return false;
        }
        finally
        {
            connection.Close();
        }
    }

    public static int FlashViewer_MailFriend(string strSendMail, string strToMail, int folderId, string strMailInfor, string strMailTitle)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_UpdateMailFriend";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter[] parameters = new SqlParameter[7];
            parameters[0] = new SqlParameter("@SendMail", SqlDbType.NVarChar, 50);
            parameters[0].Value = strSendMail;
            parameters[1] = new SqlParameter("@ToMail", SqlDbType.NVarChar, 50);
            parameters[1].Value = strToMail;
            parameters[2] = new SqlParameter("@FolderID", SqlDbType.Int);
            parameters[2].Value = folderId;
            parameters[3] = new SqlParameter("@MailTitle", SqlDbType.NVarChar, 50);
            parameters[3].Value = strMailTitle;
            parameters[4] = new SqlParameter("@MailInfor", SqlDbType.NVarChar, 500);
            parameters[4].Value = strMailInfor;
            parameters[5] = new SqlParameter("@CreateTime", SqlDbType.DateTime);
            parameters[5].Value = System.DateTime.Now;



            parameters[6] = new SqlParameter("@MailID", SqlDbType.Int);
            parameters[6].Direction = ParameterDirection.Output;


            command.Parameters.AddRange(parameters);
            command.ExecuteNonQuery();

            return int.Parse(parameters[6].Value.ToString());
        }
        catch (Exception ex)
        {
            return 0;
        }
        finally
        {
            connection.Close();
        }
    }
    /// <summary>
    ///for  sharing Document.
    /// </summary>
    /// <param name="strSendMail"></param>
    /// <param name="strToMail"></param>
    /// <param name="documentId"></param>
    /// <param name="strMailInfor"></param>
    /// <param name="strMailTitle"></param>
    /// <returns></returns>
    public static int FlashViewer_ShareMail(string strSendMail, string strToMail, int documentId, string strMailInfor, string strMailTitle, bool isWorkAreaOrArchive = false)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_UpdateShareMail";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter[] parameters = new SqlParameter[8];
            parameters[0] = new SqlParameter("@SendMail", SqlDbType.NVarChar, 50);
            parameters[0].Value = strSendMail;
            parameters[1] = new SqlParameter("@ToMail", SqlDbType.NVarChar, 50);
            parameters[1].Value = strToMail;
            parameters[2] = new SqlParameter("@DocumentID", SqlDbType.Int);
            parameters[2].Value = documentId;
            parameters[3] = new SqlParameter("@MailTitle", SqlDbType.NVarChar, 50);
            parameters[3].Value = strMailTitle;
            parameters[4] = new SqlParameter("@MailInfor", SqlDbType.NVarChar, 500);
            parameters[4].Value = strMailInfor;
            parameters[5] = new SqlParameter("@CreateTime", SqlDbType.DateTime);
            parameters[5].Value = System.DateTime.Now;
            parameters[6] = new SqlParameter("@isWorkAreaOrArchive", SqlDbType.Bit);
            parameters[6].Value = isWorkAreaOrArchive;


            parameters[7] = new SqlParameter("@MailID", SqlDbType.Int);
            parameters[7].Direction = ParameterDirection.Output;


            command.Parameters.AddRange(parameters);
            command.ExecuteNonQuery();

            return int.Parse(parameters[7].Value.ToString());
        }
        catch (Exception ex)
        {
            return 0;
        }
        finally
        {
            connection.Close();
        }
    }

    public static void FlashViewer_SendMail(string strToName, string strSendName, int folderID, string strSendMail, string strMailTitle, string strMailInfor, string strToMail, string strValidateCode)
    {
        string ELink = WEBSITE + "/EFileFolderV1.html?ID=" + folderID + "&DATA=" + strValidateCode;
        string strELink = "<a href = ' " + ELink + "'";
        strELink += " target='_blank'>EdFiles</a>. ";

        string strWebLink = "<a href = ' " + WEBSITE + "'";
        strWebLink += " target='_blank'>www.edfiles.com</a>. ";

        string strMailTemplet = getMailTempletStr();
        strMailTemplet = strMailTemplet.Replace("[[Toname]]", strToName);
        strMailTemplet = strMailTemplet.Replace("[[SendName]]", strSendName);
        strMailTemplet = strMailTemplet.Replace("[[eFileFolders]]", ELink);
        strMailTemplet = strMailTemplet.Replace("[[SendMail]]", strSendMail);
        strMailTemplet = strMailTemplet.Replace("[[MailTitle]]", strMailTitle);
        strMailTemplet = strMailTemplet.Replace("[[MailContent]]", strMailInfor);
        strMailTemplet = strMailTemplet.Replace("[[WebLink]]", strWebLink);

        string strEmailSubject = string.IsNullOrEmpty(strMailTitle) ? "An invitation from EdFiles" : strMailTitle;

        Email.SendFromEFileFolder(strToMail, strEmailSubject, strMailTemplet);

    }

    public static void FlashViewer_SendReminderMail(string strToName, string fileName, string submittedBy, string strToMail)
    {
        string strWebLink = "<a href = ' " + WEBSITE + "'";
        strWebLink += " target='_blank'>www.edfiles.com</a>. ";

        string strMailTemplet = GetMailTempletReminder();
        strMailTemplet = strMailTemplet.Replace("[[ToName]]", strToName);
        strMailTemplet = strMailTemplet.Replace("[[FileName]]", fileName);
        strMailTemplet = strMailTemplet.Replace("[[SubmittedBy]]", submittedBy);
        strMailTemplet = strMailTemplet.Replace("[[WebLink]]", strWebLink);
        strMailTemplet = strMailTemplet.Replace("[[Logo]]", GetLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[CasboLogo]]", GetCasboLogoPath());

        string strEmailSubject = "Reminder For EdForms";

        Email.SendFromEFileFolder(strToMail, strEmailSubject, strMailTemplet);

    }

    private static string getMailTempletStr()
    {

        string strMailTemplet = string.Empty;
        string mailTempletUrl = HttpContext.Current.Server.MapPath("~/Office/MailFirend.htm");
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    private static string GetMailTempletReminder()
    {

        string strMailTemplet = string.Empty;
        string mailTempletUrl = HttpContext.Current.Server.MapPath("~/Office/Mail/MailEdFormsReminder.html");
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    private static string GetMailTemplateCompliance()
    {

        string strMailTemplet = string.Empty;
        string mailTempletUrl = HttpContext.Current.Server.MapPath("~/Office/Mail/MailEdFilesCompliance.html");
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    private static string GetMailTempletEdForms()
    {

        string strMailTemplet = string.Empty;
        string mailTempletUrl = HttpContext.Current.Server.MapPath("~/Office/Mail/MailForEdForms.html");
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    private static string GetMailTemplateForNotifyBack()
    {

        string strMailTemplet = string.Empty;
        string mailTempletUrl = HttpContext.Current.Server.MapPath("~/Office/Mail/MailForNotifyBack.html");
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    private static string getMailTempletStrForeFileFlow()
    {

        string strMailTemplet = string.Empty;
        string mailTempletUrl = HttpContext.Current.Server.MapPath("~/Office/Mail/MailFriendForeFileFlow.htm");
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    private static string getMailTempletStrForInbox()
    {

        string strMailTemplet = string.Empty;
        string mailTempletUrl = HttpContext.Current.Server.MapPath("~/Office/Mail/MailFriendForInbox.htm");
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    private static string getMailTempletStrForWorkArea()
    {

        string strMailTemplet = string.Empty;
        string mailTempletUrl = HttpContext.Current.Server.MapPath("~/Office/Mail/MailFriendForWorkArea.htm");
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    private static string GetMailTempStrForDeleteEdFile()
    {
        string strMailTemplet = string.Empty;
        string mailTempletUrl = HttpContext.Current.Server.MapPath("~/Office/Mail/MailForDeleteEdFile.htm");
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    private static string GetMailTempStrForAllShare()
    {
        string strMailTemplet = string.Empty;
        string mailTempletUrl = HttpContext.Current.Server.MapPath("~/Office/Mail/MailForAllShare.html");
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    /// <summary>
    /// Get  Document Order for document.
    /// </summary>

    public static int GetDocumentOrder(int DividerID)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_GetDocumentOrder";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@DividerID", DividerID);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;


            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);
            int order;
            if (table != null && table.Rows.Count > 0)
            {
                order = Convert.ToInt32(table.Rows[0]["DocumentOrder"]);
            }
            else
            {
                order = 0;
            }

            return order;
        }
        catch (Exception ex)
        {
            return 0;
        }
        finally
        {
            connection.Close();
        }
    }

    /// <summary>
    /// update  Document Order for document.
    /// </summary>
    public static DataTable UpdateDocumentOrder(int sourceId, int targetId, int dividerId, int SourceDocOrder, int TargetDocOrder)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "FlashViewer_UpdateDocumentOrder";
            command.CommandType = CommandType.StoredProcedure;


            SqlParameter[] parameters = new SqlParameter[5];
            parameters[0] = new SqlParameter("@SourceId", SqlDbType.Int);
            parameters[0].Value = sourceId;     // temp uid:"54674792-27ff-4b19-9eb7-1a3e05289c2e";   //uid;
            parameters[1] = new SqlParameter("@TargetId", SqlDbType.Int);
            parameters[1].Value = targetId;
            parameters[2] = new SqlParameter("@DividerId", SqlDbType.Int);
            parameters[2].Value = dividerId;
            parameters[3] = new SqlParameter("@SourceDocOrder", SqlDbType.Int);
            parameters[3].Value = SourceDocOrder;
            parameters[4] = new SqlParameter("@TargetDocOrder", SqlDbType.Int);
            parameters[4].Value = TargetDocOrder;




            command.Parameters.AddRange(parameters);

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable dt = new DataTable();

            dataAdapter.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            return null;
            //throw;
        }
        finally
        {
            connection.Close();
        }
    }


    public static void HtmlViewer_SendMail(string strToName, string strSendName, string strSendMail, string strMailTitle, string strMailInfor, string strToMail, List<Tuple<string, string>> ELink)
    {
        string strWebLink = "<a href = ' " + WEBSITE + "'";
        strWebLink += " target='_blank'>www.edfiles.com</a>. ";

        string strMailTemplet = GetMailTempStrForAllShare();
        strMailTemplet = strMailTemplet.Replace("[[Toname]]", strToName);
        strMailTemplet = strMailTemplet.Replace("[[SendName]]", strSendName);
        strMailTemplet = strMailTemplet.Replace("[[DocType]]", "An EdFile");
        strMailTemplet = strMailTemplet.Replace("[[DocTypes]]", "EdFile(s)");
        strMailTemplet = strMailTemplet.Replace("[[SendMail]]", strSendMail);
        strMailTemplet = strMailTemplet.Replace("[[MailTitle]]", strMailTitle);
        strMailTemplet = strMailTemplet.Replace("[[MailContent]]", strMailInfor);
        strMailTemplet = strMailTemplet.Replace("[[WebLink]]", strWebLink);
        strMailTemplet = strMailTemplet.Replace("[[Logo]]", GetLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[CasboLogo]]", GetCasboLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[ExpirationTime]]", Sessions.ExpirationTime.ToString());
        //strMailTemplet = strMailTemplet.Replace("[[UserLogo]]", GetUserLogoPath());

        string strEmailSubject = string.IsNullOrEmpty(strMailTitle) ? "An invitation from EdFiles" : strMailTitle;
        Tuple<string, string>[] elinks = ELink.ToArray();
        string finalLink = string.Empty;
        foreach (Tuple<string, string> link in elinks)
        {
            string strELink = "<a href = ' " + link.Item1 + "'";
            strELink += " target='_blank'>" + link.Item1 + "</a>";

            finalLink += "<br />EdFile Name: " + link.Item2 + "<br />URL: " + strELink + "<br />";

        }
        strMailTemplet = strMailTemplet.Replace("[[eFileFolders]]", finalLink);
        Email.SendFromEFileFolder(strToMail, strEmailSubject, strMailTemplet);

    }

    public static void HtmlViewer_SendSplitShareMail(string strToName, string strSendName, int documentId, string strSendMail, string strMailTitle, string strMailInfor, string strToMail, Guid edFileShareId, int shareID)
    {
        //string ELink = url;

        string ELink = WEBSITE + "/ShareDocument.aspx?Data=" + QueryString.QueryStringEncode("ID=" + edFileShareId + "&dateTime=" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff") + "&linkID=" + shareID + "&uid=" + Sessions.SwitchedRackspaceId);

        string strELink = "<a href = ' " + ELink + "'";
        strELink += " target='_blank'>" + ELink + "</a>";

        string strWebLink = "<a href = ' " + WEBSITE + "'";
        strWebLink += " target='_blank'>www.edfiles.com</a>";

        string strMailTemplet = GetMailTempStrForAllShare();
        strMailTemplet = strMailTemplet.Replace("[[Toname]]", strToName);
        strMailTemplet = strMailTemplet.Replace("[[SendName]]", strSendName);
        strMailTemplet = strMailTemplet.Replace("[[DocType]]", "A Document");
        strMailTemplet = strMailTemplet.Replace("[[DocTypes]]", "document(s)");
        strMailTemplet = strMailTemplet.Replace("[[eFileFolders]]", strELink);
        strMailTemplet = strMailTemplet.Replace("[[SendMail]]", strSendMail);
        strMailTemplet = strMailTemplet.Replace("[[MailTitle]]", strMailTitle);
        strMailTemplet = strMailTemplet.Replace("[[MailContent]]", strMailInfor);
        strMailTemplet = strMailTemplet.Replace("[[WebLink]]", strWebLink);
        strMailTemplet = strMailTemplet.Replace("[[Logo]]", GetLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[CasboLogo]]", GetCasboLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[ExpirationTime]]", Sessions.ExpirationTime.ToString());
        //strMailTemplet = strMailTemplet.Replace("[[UserLogo]]", GetUserLogoPath());

        string strEmailSubject = string.IsNullOrEmpty(strMailTitle) ? "An invitation from EdFiles" : strMailTitle;

        Email.SendFromEFileFolder(strToMail, strEmailSubject, strMailTemplet);

    }

    public static void HtmlViewer_SendShareMail(string strToName, string strSendName, string[] documentIds, string strSendMail, string strMailTitle, string strMailInfor, string strToMail, string url, int[] mailIds, int[] shareIds)
    {
        string finalLink = string.Empty;
        for (int i = 0; i < documentIds.Length; i++)
        {
            string ELink = WEBSITE + "/ShareDocument.aspx?MailId=" + mailIds[i] + "&Data="
                    + QueryString.QueryStringEncode("ID=&dateTime=" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff")
                    + "&linkID=" + shareIds[i] + "&uid=" + Sessions.SwitchedRackspaceId);

            string strELink = "<a href = ' " + ELink + "'";
            strELink += " target='_blank'>" + ELink + "</a>";

            finalLink += "<br />URL: " + strELink + "<br />";
        }

        string strWebLink = "<a href = ' " + WEBSITE + "'";
        strWebLink += " target='_blank'>www.edfiles.com</a>";

        string strMailTemplet = GetMailTempStrForAllShare();
        strMailTemplet = strMailTemplet.Replace("[[Toname]]", strToName);
        strMailTemplet = strMailTemplet.Replace("[[SendName]]", strSendName);
        strMailTemplet = strMailTemplet.Replace("[[DocType]]", "A Document");
        strMailTemplet = strMailTemplet.Replace("[[DocTypes]]", "document(s)");
        strMailTemplet = strMailTemplet.Replace("[[eFileFolders]]", finalLink);
        strMailTemplet = strMailTemplet.Replace("[[SendMail]]", strSendMail);
        strMailTemplet = strMailTemplet.Replace("[[MailTitle]]", strMailTitle);
        strMailTemplet = strMailTemplet.Replace("[[MailContent]]", strMailInfor);
        strMailTemplet = strMailTemplet.Replace("[[WebLink]]", strWebLink);
        strMailTemplet = strMailTemplet.Replace("[[Logo]]", GetLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[CasboLogo]]", GetCasboLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[ExpirationTime]]", Sessions.ExpirationTime.ToString());
        //strMailTemplet = strMailTemplet.Replace("[[UserLogo]]", GetUserLogoPath());

        string strEmailSubject = string.IsNullOrEmpty(strMailTitle) ? "An invitation from EdFiles" : strMailTitle;

        Email.SendFromEFileFolder(strToMail, strEmailSubject, strMailTemplet);

    }

    public static void HtmlViewer_SendComplianceMail(string orgName, string phoneNumber, string email, int step1, int step2, int step3)
    {
        string strWebLink = "<a href = ' " + WEBSITE + "'";
        strWebLink += " target='_blank'>www.edfiles.com</a>";

        string strMailTemplet = GetMailTemplateCompliance();
        strMailTemplet = strMailTemplet.Replace("[[OrgName]]", orgName);
        strMailTemplet = strMailTemplet.Replace("[[PhoneNumber]]", phoneNumber);
        strMailTemplet = strMailTemplet.Replace("[[Email]]", email);
        strMailTemplet = strMailTemplet.Replace("[[Step1]]", Enum_Tatva.GetEnumDescription((Enum_Tatva.RetentionCompliance)step1));
        strMailTemplet = strMailTemplet.Replace("[[Step2]]", Enum_Tatva.GetEnumDescription((Enum_Tatva.RetentionCompliance)step2));
        strMailTemplet = strMailTemplet.Replace("[[Step3]]", Enum_Tatva.GetEnumDescription((Enum_Tatva.RetentionCompliance)step3));
        strMailTemplet = strMailTemplet.Replace("[[WebLink]]", strWebLink);
        strMailTemplet = strMailTemplet.Replace("[[Logo]]", GetLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[CasboLogo]]", GetCasboLogoPath());

        string strEmailSubject = "A Retention Compliance Test Result";

        Email.SendFromEFileFolder(MailTo, strEmailSubject, strMailTemplet);
    }

    private static string GetLogoPath()
    {
        try
        {
            string filePath = Common_Tatva.UploadFolder.Replace("/", "") + Common_Tatva.UserLogoFolder + "/" + Sessions.SwitchedRackspaceId + ".png";
            //string filePath = Common_Tatva.UploadFolder.Replace("/", "") + Common_Tatva.UserLogoFolder + "/" + Sessions.UserId + ".png";
            int toSub = HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("EFileFolderJSONService.asmx") > 0 ? HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("EFileFolderJSONService.asmx") : HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("Office/FileFolderBox.aspx");
            if (!File.Exists(HttpContext.Current.Server.MapPath("~/" + filePath)))
                filePath = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, toSub) + "New/images/edFile_logo.png";
            else
                filePath = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, toSub) + Common_Tatva.UploadFolder.Replace("/", "") + Common_Tatva.UserLogoFolder + "/" + Sessions.SwitchedRackspaceId + ".png";
            //filePath = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("EFileFolderJSONService.asmx")) + Common_Tatva.UploadFolder.Replace("/", "") + Common_Tatva.UserLogoFolder + "/" + Sessions.UserId + ".png";
            return filePath;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
    }

    private static string GetCasboLogoPath()
    {
        try
        {
            int toSub = HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("EFileFolderJSONService.asmx") > 0 ? HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("EFileFolderJSONService.asmx") : HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("Office/FileFolderBox.aspx");
            string filePath = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, toSub) + "New/images/Casbo/casbo-logo.png";
            return filePath;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
    }

    private static string GetUserLogoPath()
    {
        try
        {
            int toSub = HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("EFileFolderJSONService.asmx") > 0 ? HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("EFileFolderJSONService.asmx") : HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("Office/FileFolderBox.aspx");
            string filePath = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, toSub) + "Upload/UserLogo/" + Sessions.SwitchedRackspaceId + ".png";
            return filePath;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
    }

    public static void HtmlViewer_SendShareMailForeFileFlow(string strToName, string strSendName, string strSendMail, string strMailTitle, string strMailInfor, string strToMail, List<Tuple<string, string>> ELink)
    {
        //string ELink = url;

        //string ELink = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("EFileFolderJSONService.asmx")) + "Viewer.aspx?data=" + UserId + "&created=" + loggedInID;

        string strWebLink = "<a href = ' " + WEBSITE + "'";
        strWebLink += " target='_blank'>www.edfiles.com</a>";

        string strMailTemplet = GetMailTempStrForAllShare();
        strMailTemplet = strMailTemplet.Replace("[[Toname]]", strToName);
        strMailTemplet = strMailTemplet.Replace("[[SendName]]", strSendName);
        strMailTemplet = strMailTemplet.Replace("[[DocType]]", "A Document");
        strMailTemplet = strMailTemplet.Replace("[[DocTypes]]", "document(s)");
        strMailTemplet = strMailTemplet.Replace("[[SendMail]]", strSendMail);
        strMailTemplet = strMailTemplet.Replace("[[MailTitle]]", strMailTitle);
        strMailTemplet = strMailTemplet.Replace("[[MailContent]]", strMailInfor);
        strMailTemplet = strMailTemplet.Replace("[[WebLink]]", strWebLink);
        strMailTemplet = strMailTemplet.Replace("[[Logo]]", GetLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[CasboLogo]]", GetCasboLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[ExpirationTime]]", Sessions.ExpirationTime.ToString());
        //strMailTemplet = strMailTemplet.Replace("[[UserLogo]]", GetUserLogoPath());

        string strEmailSubject = "An invitation from EdFiles";

        Tuple<string, string>[] elinks = ELink.ToArray();
        string finalLink = string.Empty;
        foreach (Tuple<string, string> link in elinks)
        {
            string strELink = "<a href = ' " + link.Item1 + "'";
            strELink += " target='_blank'>" + link.Item1 + "</a>";

            finalLink += "<br />Document Name: " + link.Item2 + "<br />URL: " + strELink + "<br />";

        }
        strMailTemplet = strMailTemplet.Replace("[[eFileFolders]]", finalLink);

        Email.SendFromEFileFolder(strToMail, strEmailSubject, strMailTemplet);

    }

    public static void HtmlViewer_SendShareMailForFileFolderBox(string strToName, string strSendName, string strSendMail, string strMailTitle, string strMailInfor, string strToMail, List<Tuple<string>> ELink)
    {
        string strWebLink = "<a href = ' " + WEBSITE + "'";
        strWebLink += " target='_blank'>www.edfiles.com</a>";

        string strMailTemplet = GetMailTempletEdForms();
        strMailTemplet = strMailTemplet.Replace("[[Toname]]", strToName);
        strMailTemplet = strMailTemplet.Replace("[[SendName]]", strSendName);
        strMailTemplet = strMailTemplet.Replace("[[DocType]]", "A Folder");
        strMailTemplet = strMailTemplet.Replace("[[DocTypes]]", "folder(s)");
        strMailTemplet = strMailTemplet.Replace("[[SendMail]]", strSendMail);
        strMailTemplet = strMailTemplet.Replace("[[MailTitle]]", strMailTitle);
        strMailTemplet = strMailTemplet.Replace("[[MailContent]]", strMailInfor);
        strMailTemplet = strMailTemplet.Replace("[[WebLink]]", strWebLink);
        strMailTemplet = strMailTemplet.Replace("[[Logo]]", GetLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[CasboLogo]]", GetCasboLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[ExpirationTime]]", Sessions.ExpirationTime.ToString());
        //strMailTemplet = strMailTemplet.Replace("[[UserLogo]]", GetUserLogoPath());

        string strEmailSubject = string.IsNullOrEmpty(strMailTitle) ? "An invitation from EdFiles" : strMailTitle;

        Tuple<string>[] elinks = ELink.ToArray();
        string finalLink = string.Empty;
        foreach (Tuple<string> link in elinks)
        {
            string strELink = "<a href = ' " + link.Item1 + "'";
            strELink += " target='_blank'>" + link.Item1 + "</a>";

            //finalLink += "<br />Document Name: " + link.Item2 + "<br />URL: " + strELink + "<br />";
            finalLink += "<br />URL: " + strELink + "<br />";

        }
        strMailTemplet = strMailTemplet.Replace("[[eFileFolders]]", finalLink);

        Email.SendFromEFileFolder(strToMail, strEmailSubject, strMailTemplet);
    }

    public static void HtmlViewer_SendShareMailForEdForms(string strToName, string strSendName, string strSendMail, string strMailTitle, string strMailInfor, string strToMail, List<Tuple<string, string>> ELink)
    {
        //string ELink = url;

        //string ELink = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("EFileFolderJSONService.asmx")) + "Viewer.aspx?data=" + UserId + "&created=" + loggedInID;

        string strWebLink = "<a href = ' " + WEBSITE + "'";
        strWebLink += " target='_blank'>www.edfiles.com</a>";

        string strMailTemplet = GetMailTempletEdForms();
        strMailTemplet = strMailTemplet.Replace("[[Toname]]", strToName);
        strMailTemplet = strMailTemplet.Replace("[[SendName]]", strSendName);
        strMailTemplet = strMailTemplet.Replace("[[DocType]]", "A Document");
        strMailTemplet = strMailTemplet.Replace("[[DocTypes]]", "document(s)");
        strMailTemplet = strMailTemplet.Replace("[[SendMail]]", strSendMail);
        strMailTemplet = strMailTemplet.Replace("[[MailTitle]]", strMailTitle);
        strMailTemplet = strMailTemplet.Replace("[[MailContent]]", strMailInfor);
        strMailTemplet = strMailTemplet.Replace("[[WebLink]]", strWebLink);
        strMailTemplet = strMailTemplet.Replace("[[Logo]]", GetLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[CasboLogo]]", GetCasboLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[ExpirationTime]]", Sessions.ExpirationTime.ToString());
        //strMailTemplet = strMailTemplet.Replace("[[UserLogo]]", GetUserLogoPath());

        string strEmailSubject = string.IsNullOrEmpty(strMailTitle) ? "An invitation from EdFiles" : strMailTitle;

        Tuple<string, string>[] elinks = ELink.ToArray();
        string finalLink = string.Empty;
        foreach (Tuple<string, string> link in elinks)
        {
            string strELink = "<a href = ' " + link.Item1 + "'";
            strELink += " target='_blank'>" + link.Item1 + "</a>";

            finalLink += "<br />Document Name: " + link.Item2 + "<br />URL: " + strELink + "<br />";

        }
        strMailTemplet = strMailTemplet.Replace("[[eFileFolders]]", finalLink);

        Email.SendFromEFileFolder(strToMail, strEmailSubject, strMailTemplet);
    }

    public static void HtmlViewer_SendShareMailForEdFormsNotifyBack(string strToName, string strSendName, string strSendMail, string strMailTitle, string strMailInfor, string strToMail, Tuple<string, string> ELink)
    {
        //string ELink = url;

        //string ELink = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("EFileFolderJSONService.asmx")) + "Viewer.aspx?data=" + UserId + "&created=" + loggedInID;

        string strWebLink = "<a href = ' " + WEBSITE + "'";
        strWebLink += " target='_blank'>www.edfiles.com</a>";

        string strMailTemplet = GetMailTemplateForNotifyBack();
        strMailTemplet = strMailTemplet.Replace("[[Toname]]", strToName);
        strMailTemplet = strMailTemplet.Replace("[[SendName]]", strSendName);
        strMailTemplet = strMailTemplet.Replace("[[DocType]]", "A Document");
        strMailTemplet = strMailTemplet.Replace("[[SendMail]]", strSendMail);
        strMailTemplet = strMailTemplet.Replace("[[MailContent]]", strMailInfor);
        strMailTemplet = strMailTemplet.Replace("[[WebLink]]", strWebLink);
        strMailTemplet = strMailTemplet.Replace("[[Logo]]", GetLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[CasboLogo]]", GetCasboLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[ExpirationTime]]", Sessions.ExpirationTime.ToString());

        string strEmailSubject = "Incomplete EdForm";

        string finalLink = string.Empty;
        string strELink = "<a href = ' " + ELink.Item1 + "'";
        strELink += " target='_blank'>" + ELink.Item1 + "</a>";

        finalLink += "<br />Document Name: " + ELink.Item2 + "<br />URL: " + strELink + "<br />";

        strMailTemplet = strMailTemplet.Replace("[[eFileFolders]]", finalLink);

        Email.SendFromEFileFolder(strToMail, strEmailSubject, strMailTemplet);
    }

    public static string HtmlViewer_SendShareMailForInbox(string strToName, string strSendName, string strSendMail, string strMailTitle, string strMailInfor, string strToMail)
    {
        string ELink = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.IndexOf("EFileFolderJSONService.asmx")) + Sessions.UserName;

        string strELink = "<a href = ' " + ELink + "'";
        strELink += " target='_blank'>EdFiles</a>";

        string strWebLink = "<a href = ' " + WEBSITE + "'";
        strWebLink += " target='_blank'>www.edfiles.com</a>";

        string strMailTemplet = getMailTempletStrForInbox();
        strMailTemplet = strMailTemplet.Replace("[[Toname]]", strToName);
        strMailTemplet = strMailTemplet.Replace("[[SendName]]", strSendName);
        strMailTemplet = strMailTemplet.Replace("[[eFileFolders]]", ELink);
        strMailTemplet = strMailTemplet.Replace("[[SendMail]]", strSendMail);
        strMailTemplet = strMailTemplet.Replace("[[MailTitle]]", strMailTitle);
        strMailTemplet = strMailTemplet.Replace("[[MailContent]]", strMailInfor);
        strMailTemplet = strMailTemplet.Replace("[[WebLink]]", strWebLink);
        strMailTemplet = strMailTemplet.Replace("[[Logo]]", GetLogoPath());
        //strMailTemplet = strMailTemplet.Replace("[[UserLogo]]", GetUserLogoPath());

        string strEmailSubject = "An invitation from EdFiles";

        Email.SendFromEFileFolder(strToMail, strEmailSubject, strMailTemplet);

        return ELink;
    }

    public static void HtmlViewer_SendShareMailForWorkArea(string strToName, string strSendName, string strSendMail, string strMailTitle, string strMailInfor, string strToMail, string URL)
    {
        string ELink = URL;

        string strELink = "<a href = ' " + ELink + "'";
        strELink += " target='_blank'>" + ELink + "</a>";

        string strWebLink = "<a href = ' " + WEBSITE + "'";
        strWebLink += " target='_blank'>www.edfiles.com</a>";


        string strMailTemplet = GetMailTempStrForAllShare();
        strMailTemplet = strMailTemplet.Replace("[[Toname]]", strToName);
        strMailTemplet = strMailTemplet.Replace("[[SendName]]", strSendName);
        strMailTemplet = strMailTemplet.Replace("[[DocType]]", "A Document");
        strMailTemplet = strMailTemplet.Replace("[[DocTypes]]", "document(s)");
        strMailTemplet = strMailTemplet.Replace("[[eFileFolders]]", strELink);
        strMailTemplet = strMailTemplet.Replace("[[SendMail]]", strSendMail);
        strMailTemplet = strMailTemplet.Replace("[[MailTitle]]", strMailTitle);
        strMailTemplet = strMailTemplet.Replace("[[MailContent]]", strMailInfor);
        strMailTemplet = strMailTemplet.Replace("[[WebLink]]", strWebLink);
        strMailTemplet = strMailTemplet.Replace("[[Logo]]", GetLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[CasboLogo]]", GetCasboLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[ExpirationTime]]", Sessions.ExpirationTime.ToString());
        //strMailTemplet = strMailTemplet.Replace("[[UserLogo]]", GetUserLogoPath());

        string strEmailSubject = string.IsNullOrEmpty(strMailTitle) ? "An invitation from EdFiles" : strMailTitle;

        Email.SendFromEFileFolder(strToMail, strEmailSubject, strMailTemplet);

    }

    public static void HtmlViewer_SendShareMailForArchive(string strToName, string strSendName, string strSendMail, string strMailTitle, string strMailInfor, string strToMail, string URL)
    {
        string ELink = URL;

        string strELink = "<a href = ' " + ELink + "'";
        strELink += " target='_blank'>" + ELink + "</a>";

        string strWebLink = "<a href = ' " + WEBSITE + "'";
        strWebLink += " target='_blank'>www.edfiles.com</a>";


        string strMailTemplet = GetMailTempStrForAllShare();
        strMailTemplet = strMailTemplet.Replace("[[Toname]]", strToName);
        strMailTemplet = strMailTemplet.Replace("[[SendName]]", strSendName);
        strMailTemplet = strMailTemplet.Replace("[[DocType]]", "A Document");
        strMailTemplet = strMailTemplet.Replace("[[DocTypes]]", "document(s)");
        strMailTemplet = strMailTemplet.Replace("[[eFileFolders]]", strELink);
        strMailTemplet = strMailTemplet.Replace("[[SendMail]]", strSendMail);
        strMailTemplet = strMailTemplet.Replace("[[MailTitle]]", strMailTitle);
        strMailTemplet = strMailTemplet.Replace("[[MailContent]]", strMailInfor);
        strMailTemplet = strMailTemplet.Replace("[[WebLink]]", strWebLink);
        strMailTemplet = strMailTemplet.Replace("[[Logo]]", GetLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[CasboLogo]]", GetCasboLogoPath());
        strMailTemplet = strMailTemplet.Replace("[[ExpirationTime]]", Sessions.ExpirationTime.ToString());
        //strMailTemplet = strMailTemplet.Replace("[[UserLogo]]", GetUserLogoPath());

        string strEmailSubject = string.IsNullOrEmpty(strMailTitle) ? "An invitation from EdFiles" : strMailTitle;

        Email.SendFromEFileFolder(strToMail, strEmailSubject, strMailTemplet);

    }

    public static void HtmlViewer_SendMailForDeleteFolder(string strToMail, string folderName, string userName, string toName)
    {

        try
        {
            string strWebLink = "<a href = ' " + WEBSITE + "'";
            strWebLink += " target='_blank'>www.edfiles.com</a>";

            string strMailTemplet = GetMailTempStrForDeleteEdFile();
            try
            {
                strMailTemplet = strMailTemplet.Replace("[[FolderName]]", folderName);
                strMailTemplet = strMailTemplet.Replace("[[UserName]]", userName);
                strMailTemplet = strMailTemplet.Replace("[[WebLink]]", strWebLink);
                strMailTemplet = strMailTemplet.Replace("[[ToUserName]]", toName);
                string logoPath = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.LastIndexOf('/'));
                logoPath = logoPath.Substring(0, logoPath.LastIndexOf('/'));
                logoPath += "/New/images/edFile_logo.png";
                strMailTemplet = strMailTemplet.Replace("[[Logo]]", logoPath);
            }
            catch (Exception ex)
            { }

            string strEmailSubject = "EdFiles Alert";

            Email.SendFromEFileFolder(strToMail, strEmailSubject, strMailTemplet);
        }
        catch (Exception e)
        { }

    }

    public static DataSet GetDocumentFromMailID(int mailID)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "GetDocumentFromMailID";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@mailID", mailID);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.Int32;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataSet dsDocument = new DataSet();

            dataAdapter.Fill(dsDocument);

            return dsDocument;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    #region EdFileShare

    /// <summary>
    /// Get Splitted Document details by EdFileShareId
    /// </summary>
    /// <param name="edFileShareId">EdFileShareId</param>
    /// <returns></returns>
    public static DataTable GetDocumentFromEdFileShareId(Guid edFileShareId)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "GetDocumentFromEdFileShareId";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@EdFileShareId", edFileShareId);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.Guid;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable dtDocument = new DataTable();

            dataAdapter.Fill(dtDocument);

            return dtDocument;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    #endregion
}
