﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Web.Script.Services;
using System.Web.Security;
using System.Web.Services;
using System.Web.Services.Protocols;
using Shinetech.DAL;
using Microsoft.VisualBasic;
using System.Linq;
using Newtonsoft.Json;


/// <summary>
/// FlexWebService 的摘要说明
/// </summary>
[WebService(Namespace = "http://www.edfiles.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class FlexWebService : System.Web.Services.WebService
{

    public FlexWebService()
    {

        //如果使用设计的组件，请取消注释以下行 
        //InitializeComponent(); 
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string TestMultiJson1(string fields, string ids)
    {
        return "OK";
    }

    [WebMethod(Description = "Receive the SignIn Data", EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string TestMultiJson(string fields, string ids, string doctorid, string data)
    {
        string[] items = fields.Split(new string[] { "|" }, StringSplitOptions.None);
        string[] idArray = ids.Split(new string[] { "|" }, StringSplitOptions.None);

        List<QuestionEntity> list = new List<QuestionEntity>();

        //search the customized questions
        //string doctorId = Membership.GetUser().ProviderUserKey.ToString();
        DataTable doctorTable = UserManagement.GetDoctorQuestions(doctorid);


        DataTable table = UserManagement.GetQuestionDefines();

        foreach (DataRow dataRow in table.Rows)
        {
            QuestionEntity entity = new QuestionEntity();
            entity.QuestionID = Convert.ToInt32(dataRow["QuestionID"]);
            entity.QuestionTitle = Convert.ToString(dataRow["QuestionTitle"]);
            entity.OfficeID = doctorid;
            if (!string.IsNullOrEmpty(entity.QuestionTitle))
            {
                list.Add(entity);
            }
        }

        //Save the answers
        int idx = 1;
        foreach (string id in idArray)
        {
            QuestionEntity entity = FindQuestion(id, list);
            if (entity != null)
            {
                entity.AnswerContent = items[idx];
            }

            idx++;

        }

        Signature signature = new Signature();
        signature.PrintName.Value = items[0];
        signature.SignInDate.Value = DateTime.Now;
        signature.DoctorID.Value = doctorid;
        signature.QuestionQty.Value = list.Count;
        signature.SignatureData.Value = data;
        try
        {

            UserManagement.SavePatientAnswers(signature, list);
            return "Sign In Successfull,<br/>Welcome.Please be seated...someone will be with you shortly.";
        }
        catch (Exception e)
        {

            return "Failed to Sign In";
        }
    }

    public QuestionEntity FindQuestion(int qid, List<QuestionEntity> questions)
    {
        foreach (QuestionEntity item in questions)
        {
            if (item.QuestionID == qid)
            {
                return item;
            }
        }

        return null;
    }

    public QuestionEntity FindQuestion(string qid, List<QuestionEntity> questions)
    {
        foreach (QuestionEntity item in questions)
        {
            if (item.QuestionID.ToString() == qid)
            {
                return item;
            }
        }

        return null;
    }

    // Decompress an LZW-encoded string
    public string lzw_decode(string s)
    {
        Dictionary<int, string> dict = new Dictionary<int, string>(); //创建一个数组Object {}
        char[] data = s.ToCharArray(); //把字符串分隔成数组["d", "a", "t", 2421 more...]
        char currChar = data[0]; //获取第一个字符"d"
        string oldPhrase = currChar.ToString(); //"d"
        StringBuilder outstr = new StringBuilder();//["d"]
        outstr.Append(currChar);

        int code = 256; //256
        string phrase;
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < data.Length; i++) //data.length 数组长度
        {
            int currCode = Convert.ToInt32(data[i]);//data[i].charCodeAt(0); //"a"->97 =currCode)
            if (currCode < 256)
            {
                phrase = data[i].ToString(); //phrase ="a"
            }
            else
            {
                //sb.Append(oldPhrase);
                //sb.Append(currChar);
                phrase = dict.ContainsKey(currCode) ? dict[currCode] : oldPhrase + currChar;
            }
            outstr.Append(phrase); //out: ["d"] -> ["d", "a"]
            currChar = phrase[0];
            dict[code] = oldPhrase + currChar; // dict[256]= "d"+"a" =Object { 256="da"}
            code++; //256->257
            oldPhrase = phrase; //oldPhrase:"d"->"a" 
        }
        return outstr.ToString();
    }

    [WebMethod(EnableSession = true)]
    public string[] GetCompletionList(string prefixText, int count, string contextKey)
    {
        string uid = contextKey;
        DataTable table = null;
        if (!prefixText.Contains(","))
            prefixText = string.Join(":", prefixText.Split(new char[0], StringSplitOptions.RemoveEmptyEntries));

        if (this.Context.User.IsInRole("Users") && !Common_Tatva.IsUserSwitched())
        {
            DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(Sessions.SwitchedSessionId);
            uid = dtOfficeUser.Rows[0]["OfficeUID"].ToString();
        }
        table = FileFolderManagement.GetOfficeFileFoldersWithPrefx(prefixText, uid);


        IList<string> list = new List<string>();
        int max = 20;
        if (table != null)
        {
            foreach (DataRow row in table.Rows)
            {
                list.Add(row["PrefxName"].ToString());
                if (list.Count >= max) break;
            }
        }

        string[] arr = new string[list.Count];
        list.CopyTo(arr, 0);
        return arr;
    }

    [WebMethod(EnableSession = true)]
    public string[] GetCompleteDocumentList(string prefixText, int count, string contextKey)
    {
        string uid = contextKey;
        DataTable table = null;
        prefixText = string.Join(",", prefixText.Split(new char[0], StringSplitOptions.RemoveEmptyEntries));

        //if (this.Context.User.IsInRole("Users") && Common_Tatva.IsUserSwitched())
        //{
        //    DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(Sessions.SwitchedSessionId);
        //    uid = dtOfficeUser.Rows[0]["OfficeUID"].ToString();
        //}
        table = FileFolderManagement.GetOfficeFileFoldersWithPrefxDoc(prefixText, uid);

        IList<string> list = new List<string>();
        int max = 20;
        if (table != null)
        {
            foreach (DataRow row in table.Rows)
            {
                list.Add(row["DocumentName"].ToString());
                if (list.Count >= max) break;
            }
        }

        string[] arr = new string[list.Count];
        list.CopyTo(arr, 0);
        return arr;
    }

    [WebMethod]
    public string[] GetCompletionFolderList(string prefixText, int count, string contextKey)
    {
        string uid = contextKey;
        DataTable table = null;
        if (this.Context.User.IsInRole("Users") && !Common_Tatva.IsUserSwitched())
        {
            DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(Sessions.SwitchedSessionId);
            uid = dtOfficeUser.Rows[0]["OfficeUID"].ToString();
        }
        table = FileFolderManagement.GetFileFoldersNameWithPrefx1(prefixText, uid);

        IList<string> list = new List<string>();
        int max = 20;
        if (table != null)
        {
            foreach (DataRow row in table.Rows)
            {
                string str = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row["PrefxName"].ToString(), Convert.ToString(row["FolderId"].ToString()));
                list.Add(str);
                //list.Add(row["PrefxName"].ToString());
                if (list.Count >= max) break;
            }
        }

        string[] arr = new string[list.Count];
        list.CopyTo(arr, 0);
        return arr;
    }

    [WebMethod]
    public Dictionary<string, string> GetOnlyOfficeForAdmin()
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        try
        {
            DataTable dtUsers = UserManagement.GetOfficeUsers();
            IList<string> list = new List<string>();
            if (dtUsers != null)
            {
                dtUsers.AsEnumerable().ToList().ForEach(d =>
                {
                    dict.Add(Convert.ToString(d.Field<Guid>("UID")), d.Field<string>("Name"));

                });
            }
        }
        catch (Exception ex)
        { }
        return dict;
    }

    [WebMethod]
    public Dictionary<string, string> GetAssignedUsersByOfficeId(string officeId)
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        try
        {
            DataTable dtUsers = General_Class.GetAssignedUserByOfficeId(officeId);
            if (dtUsers != null)
            {
                dtUsers.AsEnumerable().ToList().ForEach(d =>
                {
                    dict.Add(Convert.ToString(d.Field<Guid>("UID")), d.Field<string>("Name"));
                });
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
        }
        return dict;
    }

    [WebMethod]
    public Tuple<Dictionary<string, string>, Dictionary<string, string>> GetAssignedUsersForSubUsers(string officeId, string subUserId)
    {
        var dictForOffice = new Dictionary<string, string>();
        var dictForUser = new Dictionary<string, string>();
        try
        {
            DataSet dtForOffice = General_Class.GetAssignedUserForSubUser(officeId, subUserId);
            if (dtForOffice != null & dtForOffice.Tables.Count > 1)
            {
                dtForOffice.Tables[0].AsEnumerable().ToList().ForEach(d =>
                {
                    dictForOffice.Add(Convert.ToString(d.Field<Guid>("UID")), d.Field<string>("Name"));
                });

                dtForOffice.Tables[1].AsEnumerable().ToList().ForEach(d =>
                {
                    dictForUser.Add(Convert.ToString(d.Field<Guid>("UID")), d.Field<string>("Name"));
                });
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
        }
        return Tuple.Create(dictForOffice, dictForUser);
    }

    [WebMethod]
    public Tuple<Dictionary<string, string>, Dictionary<string, string>> GetAssignedOfficesForOffice(string officeId)
    {
        var dictForAllOffice = new Dictionary<string, string>();
        var dictForAssigned = new Dictionary<string, string>();
        try
        {
            DataSet dtForAdmin = General_Class.GetAssignedUserForOffice(officeId);
            if (dtForAdmin != null & dtForAdmin.Tables.Count > 1)
            {
                dtForAdmin.Tables[0].AsEnumerable().ToList().ForEach(d =>
                {
                    dictForAllOffice.Add(Convert.ToString(d.Field<Guid>("UID")), d.Field<string>("Name"));
                });

                dtForAdmin.Tables[1].AsEnumerable().ToList().ForEach(d =>
                {
                    dictForAssigned.Add(Convert.ToString(d.Field<Guid>("UID")), d.Field<string>("Name"));
                });
            }
        }
        catch (Exception ex)
        { }
        return Tuple.Create(dictForAllOffice, dictForAssigned);
    }

    /// <summary>
    /// Get all the assigned departments
    /// </summary>
    /// <param name="officeId"></param>
    /// <returns></returns>
    [WebMethod]
    public Tuple<Dictionary<string, string>, Dictionary<string, string>> GetAssignedDepartmentsForOffice(string officeId)
    {
        var dictForAllDepartments = new Dictionary<string, string>();
        var dictForAssigned = new Dictionary<string, string>();
        try
        {
            DataSet dtForAdmin = General_Class.GetAssignedDepartment(new Guid(officeId));
            if (dtForAdmin != null & dtForAdmin.Tables.Count > 1)
            {
                dtForAdmin.Tables[0].AsEnumerable().ToList().ForEach(d =>
                {
                    dictForAllDepartments.Add(Convert.ToString(d.Field<Int32>("DepartmentId")), d.Field<string>("DepartmentName"));
                });

                dtForAdmin.Tables[1].AsEnumerable().ToList().ForEach(d =>
                {
                    dictForAssigned.Add(Convert.ToString(d.Field<Int32>("DepartmentId")), d.Field<string>("DepartmentName"));
                });
            }
        }
        catch (Exception ex)
        { }
        return Tuple.Create(dictForAllDepartments, dictForAssigned);
    }

    /// <summary>
    /// Get all the departments
    /// </summary>
    /// <param name="officeId"></param>
    /// <returns></returns>
    [WebMethod]
    public Dictionary<string, string> GetDepartmentsForAdmin()
    {
        var dictForAllDepartments = new Dictionary<string, string>();
        try
        {
            DataTable dtForAdmin = General_Class.GetDepartmentsForAdmin();
            if (dtForAdmin != null & dtForAdmin.Rows.Count > 1)
            {
                dtForAdmin.AsEnumerable().ToList().ForEach(d =>
                {
                    dictForAllDepartments.Add(Convert.ToString(d.Field<Int32>("DepartmentId")), d.Field<string>("DepartmentName"));
                });
            }
        }
        catch (Exception ex)
        { }
        return dictForAllDepartments;
    }

    [WebMethod]
    public string UnAssignOffice(string assignedUid, string assignedTo)
    {
        return General_Class.UnAssignOffice(assignedUid, assignedTo);
    }

    /// <summary>
    /// Unassign department for particular Office user
    /// </summary>
    /// <param name="departmentId"></param>
    /// <param name="uid"></param>
    /// <returns></returns>
    [WebMethod]
    public string UnAssignDepartment(string departmentId, string uid)
    {
        return General_Class.UnAssignDepartment(Convert.ToInt32(departmentId), uid);
    }

    [WebMethod]
    public Dictionary<string, string> GetAllUsersForAdmin()
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        try
        {
            DataTable dtUsers = FileFolderManagement.GetAllUsersForAdmin();
            if (dtUsers != null)
            {
                dtUsers.AsEnumerable().ToList().ForEach(d =>
                {
                    dict.Add(Convert.ToString(d.Field<Guid>("UID")), d.Field<string>("Name"));

                });
            }
        }
        catch (Exception ex)
        { }
        return dict;
    }

    [WebMethod]
    public Dictionary<string, string> GetOfficeUsersForAdmin()
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        try
        {
            DataTable dtUsers = General_Class.GetAllUsers();
            if (dtUsers != null)
            {
                dtUsers.AsEnumerable().ToList().ForEach(d =>
                {
                    dict.Add(Convert.ToString(d.Field<Guid>("UID")), d.Field<string>("Name"));

                });
            }
        }
        catch (Exception ex)
        { }
        return dict;
    }

    [WebMethod]
    public Dictionary<string, string> GetFolderDetailsById(string officeId)
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        try
        {
            DataTable dtFolderDetails = General_Class.GetFolderIdByOfficeID(officeId);
            if (dtFolderDetails != null)
            {
                dtFolderDetails.AsEnumerable().ToList().ForEach(d =>
                {
                    dict.Add(Convert.ToString(d.Field<Int32>("folderID")), d.Field<string>("FolderName"));

                });
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
        }

        return dict;
    }

    #region ElasticSearch

    [WebMethod]
    public Tuple<Dictionary<string, string>, Dictionary<string, string>> GetArchiveFolders(string officeId)
    {
        var dict = new Dictionary<string, string>();
        var dictpath = new Dictionary<string, string>();
        try
        {
            DataTable dtArchiveDetails = General_Class.GetArchiveTreeList(officeId);
            if (dtArchiveDetails != null)
            {
                dtArchiveDetails.AsEnumerable().ToList().ForEach(d =>
                {
                    dict.Add(Convert.ToString(d.Field<Int32>("ArchiveTreeId")), d.Field<string>("FolderName"));

                });
                dtArchiveDetails.AsEnumerable().ToList().ForEach(d =>
                {
                    dictpath.Add(Convert.ToString(d.Field<Int32>("ArchiveTreeId")), d.Field<string>("Path"));

                });
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
        }

        return Tuple.Create(dict, dictpath);
    }

    [WebMethod]
    public Tuple<Dictionary<string, string>, Dictionary<string, string>> GetWorkAreaFolders(string officeId)
    {
        var dict = new Dictionary<string, string>();
        var dictpath = new Dictionary<string, string>();
        try
        {
            DataTable dtWorkareaDetails = General_Class.GetWorkAreaTreeList(officeId);
            if (dtWorkareaDetails != null)
            {
                dtWorkareaDetails.AsEnumerable().ToList().ForEach(d =>
                {
                    dict.Add(Convert.ToString(d.Field<Int32>("WorkAreaTreeId")), d.Field<string>("FolderName"));

                });
                dtWorkareaDetails.AsEnumerable().ToList().ForEach(d =>
                {
                    dictpath.Add(Convert.ToString(d.Field<Int32>("WorkAreaTreeId")), d.Field<string>("Path"));

                });
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
        }

        return Tuple.Create(dict, dictpath);
    }

    #endregion

    [WebMethod]
    public string GetEmailGroups(string officeId)
    {
        try
        {
            return JsonConvert.SerializeObject(General_Class.GetEmailGroups(officeId));
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            return string.Empty;
        }
    }

    [WebMethod]
    public string GetEmails(string GroupID)
    {
        try
        {
            if (!string.IsNullOrEmpty(GroupID))
            {
                string[] groups = GroupID.Split(',');
                Dictionary<string, string> emailList = new Dictionary<string, string>();
                DataTable dtAll = new DataTable();
                foreach (string group in groups)
                {
                    DataTable dtEmails = General_Class.GetEmailsFromGroup(int.Parse(group.Trim()));
                    dtAll.Merge(dtEmails);
                }

                return JsonConvert.SerializeObject(JsonConvert.SerializeObject(dtAll));
            }
            return string.Empty;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            return string.Empty;
        }
    }

    [WebMethod]
    public string[] GetCompletionFileNameList(string prefixText, int count, string contextKey)
    {
        return GetWorkareaFiles(contextKey, prefixText);
    }

    protected string[] GetWorkareaFiles(string uid, string keyword)
    {
        string path = Server.MapPath("~/ScanInBox");
        path = string.Concat(path, Path.DirectorySeparatorChar, uid);
        IList<string> list = new List<string>();

        try
        {
            if (Directory.Exists(path))
            {
                string[] files = Directory.GetFiles(path);
                FileInfo fi1;
                string fileName = string.Empty;


                foreach (string file in files)
                {
                    fileName = Path.GetFileNameWithoutExtension(file);
                    if (fileName.StartsWith(keyword, true, null))
                    {
                        list.Add(fileName);

                    }
                }
            }
            else
            {
                Directory.CreateDirectory(path);

            }
        }
        catch { }


        string[] arr = new string[list.Count];
        list.CopyTo(arr, 0);
        return arr;
    }

    [WebMethod(EnableSession = true)]
    public Dictionary<string, string> GetFoldersData()
    {
        DataTable folders = new DataTable();
        string officeID1 = Sessions.SwitchedRackspaceId;
        //if (this.Context.User.IsInRole("Offices"))
        //{
        //    officeID1 = Membership.GetUser().ProviderUserKey.ToString();
        //}

        //else if (this.Context.User.IsInRole("Users"))
        //{
        //    DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(Membership.GetUser().ProviderUserKey.ToString());
        //    officeID1 = dtOfficeUser.Rows[0]["OfficeUID"].ToString();
        //}

        // modified by luyuanzong 2014-10-20
        //this.DataSource = FileFolderManagement.GetFileFoldersInfor(officeID1);
        if (!Common_Tatva.IsUserSwitched() && this.Context.User.IsInRole("Users")) // 
        {
            folders = FileFolderManagement.GetFileFoldersInfor(officeID1);

            //Èç¹ûÊÇSub user Ôò½øÐÐÉ¸Ñ¡groups
            var uid = Sessions.SwitchedSessionId;
            //var uid = Membership.GetUser().ProviderUserKey.ToString();
            var act = new Shinetech.DAL.Account(uid);
            string[] groups = act.Groups.Value != null ? act.Groups.Value.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries) : new string[] { };

            int count = folders.Rows.Count;

            for (int i = 0; i < count; i++)
            {
                DataRow row = folders.Rows[i];
                //µ±¸ÃFolder²»ÊÇÕâ¸öÓÃ»§´´½¨µÄfolder£¬ÇÒµ±¸ÃfolderÊÇË½ÓÐµÄ£¬²¢ÇÒ¸ÃFolder²¢²»ÔÚ¸ÃÓÃ»§ËùÊôµÄGroupsÖÐ
                if (!uid.Equals(row["OfficeID"].ToString()))
                {
                    if (row["SecurityLevel"].ToString().Equals("0") && !BelongToGroup(row["Groups"].ToString(), groups))
                    {
                        row.Delete();
                    }

                }
            }

            folders.AcceptChanges();
        }
        else
        {
            folders = FileFolderManagement.GetFileFoldersInfor(officeID1);
        }
        Dictionary<string, string> dict = new Dictionary<string, string>();
        folders.AsEnumerable().ToList().ForEach(d =>
        {
            dict.Add(Convert.ToString(d.Field<int>("FolderID")), d.Field<string>("FolderName") + " (" + (d.Field<DateTime>("CreatedDate") != null ? d.Field<DateTime>("CreatedDate").ToString("MM/dd/yyyy") : "") + ")" + (!string.IsNullOrEmpty(d.Field<string>("Alert1")) ? " Alert : " + d.Field<string>("Alert1") : "") + (!string.IsNullOrEmpty(d.Field<string>("FileNumber")) ? " FileNumber : " + d.Field<string>("FileNumber") : ""));
        });
        return dict;
    }

    [WebMethod]
    public List<string> GetWorkAreaIndexKeys()
    {
        List<string> dict = new List<string>();
        try
        {
            DataTable indexKeys = General_Class.GetWorkAreaIndexKeys(Sessions.SwitchedSessionId);
            //DataTable indexKeys = General_Class.GetWorkAreaIndexKeys(Sessions.UserId);
            indexKeys.AsEnumerable().ToList().ForEach(d =>
            {
                dict.Add(Convert.ToString(d.Field<string>("IndexKey")));
            });
        }
        catch (Exception ex)
        {
        }
        return dict;
    }

    private bool BelongToGroup(string p, string[] groups)
    {
        foreach (string group in groups)
        {
            if (p.Contains(group))
            {
                return true;
            }
        }

        return false;
    }

    [WebMethod]
    public Dictionary<string, string> GetDividersByEFF(int eff)
    {
        DataTable dividerTable = FileFolderManagement.GetDividers(eff);
        Dictionary<string, string> dict = new Dictionary<string, string>();
        dividerTable.AsEnumerable().ToList().ForEach(d =>
        {
            dict.Add(Convert.ToString(d.Field<int>("DividerID")), d.Field<string>("Name"));
        });
        return dict;
    }

}

