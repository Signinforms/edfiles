﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///FolderData 的摘要说明
/// </summary>
public class FolderData
{
    public string FirstName {get;set;}
    public string LastName {get;set;}
    public int Id {get;set;}
    
    public string Name {get;set;}
    public string SecurityLevel {get;set;}

    public List<Alert> alertsList { get; set; }
    public List<Tab> tabsList { get; set; }

    public int ErrorCode { get; set; }

    public bool AllowEdit { get; set; }
    public string UID { get; set; }
}