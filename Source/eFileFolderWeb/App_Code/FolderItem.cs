using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for FolderItem
/// </summary>
[Serializable]
public class FolderItem
{
    private int number;

    public FolderItem()
    {
       
    }

    public FolderItem(string id)
    {
        folderID = id;
    }

    public FolderItem(DataRow item)
    {
        this.Parse(item);
    }

    protected void Parse(DataRow item)
    {
        //reminderID = (int)item["ReminderID"];
        folderID = item["FolderID"].ToString();
        folderName = item["FolderName"].ToString();
        firstName = item["Firstname"].ToString();
        lastName = item["Lastname"].ToString();
        dob = (DateTime)item["CreatedDate"];
        uid = item["OfficeID"].ToString();
    }

    private string folderID;

    public string FolderID
    {
       get
       {
           return folderID;
       }
       set
       {
           folderID = value;
       }
    }

    private int reminderID;

    public int ReminderID
    {
        get
        {
            return reminderID;
        }
        set
        {
            reminderID = value;
        }
    }

    public int Number
    {
        get
        {
            return number;
        }
        set
        {
            number = value;
        }
    }

    private string folderName;

    public string FolderName
    {
        get
        {
            return folderName;
        }
        set
        {
            folderName = value;
        }
    }

    private string firstName;

    public string FirstName
    {
        get
        {
            return firstName;
        }
        set
        {
            firstName = value;
        }
    }

    private string lastName;

    public string LastName
    {
        get
        {
            return lastName;
        }
        set
        {
            lastName = value;
        }
    }

    private DateTime dob;

    public DateTime DOB
    {
        get
        {
            return dob;
        }
        set
        {
            dob = value;
        }
    }

    private string uid;

    public string UID
    {
        get
        {
            return uid;
        }
        set
        {
            uid = value;
        }
    }
}
