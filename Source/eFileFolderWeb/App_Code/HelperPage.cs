using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;      
using System.Web.UI ;
using System.Web;

public class HelperPage : System.Web.UI.Page
{
    public static void ForceAdd3wSubDomain()
    {
        if (!HttpContext.Current.Request.IsLocal && !HttpContext.Current.Request.IsSecureConnection)
        {
            string redirectUrl = HttpContext.Current.Request.Url.ToString().Replace("http:", "https:");
            HttpContext.Current.Response.Redirect(redirectUrl, false);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
        //if (!GetServerDomain().StartsWith("www.") && !GetServerDomain().StartsWith("localhost") && !GetServerDomain().StartsWith("192.168.0.104")
        //     && !GetServerDomain().StartsWith("115.112.143.20") && !GetServerDomain().StartsWith("192.168.0.190") && !GetServerDomain().StartsWith("192.168.0.114"))
        //{
        //    HttpContext.Current.Response.Status = "301 Moved Permanently";
        //    string url = ContainHttps() + GetServerDomain() + HttpContext.Current.Request.RawUrl;
        //    HttpContext.Current.Response.AddHeader("Location",url);
        //}
    }

    public static string GetServerDomain()
    {
        string myURL = HttpContext.Current.Request.Url.ToString();
        Regex re = new Regex("^(?:(?:https?\\:)?(?:\\/\\/)?)?([^\\/]+)");
        Match m = re.Match(myURL);
        return m.Groups[1].Value;
    }

    public static string ContainHttps()
    {
        string myURL = HttpContext.Current.Request.Url.ToString();
        if(myURL.Contains("https:"))
        {
            return "https://www.";
        }
        else
        {
            return "http://www.";
        }
    }
        
      
}