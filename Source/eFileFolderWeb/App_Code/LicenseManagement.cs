using System;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DataQuicker2.Framework;
using Shinetech.DAL;
/// <summary>
/// Summary description for LicenseManagement
/// </summary>
public class LicenseManagement
{
    public LicenseManagement()
    {
        
    }

    //abandoned
    public static DataTable GetCurrentLicense(string uid)
    {
        Account user = new Account(uid);
        int iFileFolder = 0;
        int iLicense = -1;
        if (user.IsExist)
        {
            if (user.IsSubUser.Value=="1")
            {
                iLicense = user.LicenseID.Value;
                user = new Account(user.OfficeUID.Value);
                if (user.IsExist)
                {
                    iFileFolder = user.FileFolders.Value;
                    if(iLicense==-1)
                    {
                        iLicense = user.LicenseID.Value;
                    }
                }
                else
                {
                    return null;
                }
               
            }
            else
            {
                iFileFolder = user.FileFolders.Value;
                iLicense = user.LicenseID.Value;
            }

            LicenseKit objLicense = new LicenseKit();
            ObjectQuery query = objLicense.CreateQuery();
            query.SetCriteria(objLicense.LicenseKitID, Operator.Equal, iLicense);

            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();

            DataTable table = new DataTable();
            try
            {
                query.Fill(table);
                //table.Rows[0]["UID"] = iLicense==-1?-1:0;
                //table.Rows[0]["Name"] = iFileFolder+" eFolders";
                //table.Rows[0]["Quantity"] = iFileFolder;
                //if(iLicense!=-1)
                //{
                //    table.Rows[0]["Description"] = "";
                //}
            }
            catch { table = null; }

            return table;
        }

        return null;
    }

    //updated in 2010-02-09
    public static DataTable GetLicenseWithoutTrial()
    {
        FolderPrice license = new FolderPrice();

        ObjectQuery query = license.CreateQuery();
        query.SetCriteria(license.UId, Operator.NotEqual, "-2");
        query.SetOrderBy(license.Count,true);
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
        }
        catch { table = null; }

        return table;

        return null;
    }

    public static DataTable GetFolderPrices()
    {
        FolderPrice folders = new FolderPrice();

        ObjectQuery query = folders.CreateQuery();
        query.SetOrderBy(folders.Count, true);
        query.SetCriteria(folders.UId,Operator.NotEqual,"-2");
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
        }
        catch { table = null; }

        return table;
    }

    //abandoned
    public static DataTable GetLicenseWithoutTrialLast(string uid)
    {
        Account user = new Account(uid);

        if (user.IsExist)
        {
            if (user.IsSubUser.Value == "1")
            {
                user = new Account(user.OfficeUID.Value);
                if (user.IsExist)
                {
                    
                }
                return null;
            }

            LicenseKit objLicense = new LicenseKit();
            ObjectQuery query = objLicense.CreateQuery();
            //query.SetCriteria(objLicense.UID, Operator.NotEqual,-1);//

            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();

            DataTable table = new DataTable();
            try
            {
                query.Fill(table);
            }
            catch { table = null; }

            return table;
        }

        return null;
    }

    public static DataTable GetLicenseWithTrial()
    {
        LicenseKit license = new LicenseKit();

        ObjectQuery query = license.CreateQuery();
        query.SetSelectFields(license.LicenseKitID, license.KitName);

        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
            DataRow row = table.NewRow();
            row["LicenseKitID"] = -1;
            row["KitName"] = "Trial User";
            table.Rows.Add(row);
            table.AcceptChanges();

        }
        catch { table = null; }

        return table;
    }

    //EFFLicense Page
    public static DataTable GetLicenseWithTrial(string uid)
    {
        SubscriptionLicense sublicense = new SubscriptionLicense();
        IDbConnection connection = DbFactory.CreateConnection();

        DataTable table = new DataTable();
        try
        {
            connection.Open();

            ObjectQuery query = sublicense.CreateQuery();
            query.SetCriteria(sublicense.UserID, uid);

            query.Fill(table);

            if (table.Rows.Count == 0)
            {
                //table = new DataTable(); //just only get the trial row
                //License license = new License();
                //ObjectQuery query1 = license.CreateQuery();
                //query1.SetCriteria(license.UID,-1);
                //query1.Fill(table);
            }

        }
        catch { table = null; }

        return table;
    }

    //abandoned
    public static int GetLicenseDaysWithTrial(string uid)
    {
        SubscriptionLicense sublicense = new SubscriptionLicense();
        IDbConnection connection = DbFactory.CreateConnection();

        DataTable table = new DataTable();
        try
        {
            connection.Open();

            ObjectQuery query = sublicense.CreateQuery();
            query.SetSelectFields(sublicense.UID, sublicense.LicenseID, sublicense.UserID, sublicense.Quantity);
            query.SetCriteria(sublicense.UserID, uid);
            query.SetCriteria(sublicense.LicenseID,-1);

            query.Fill(table);

            if (table.Rows.Count == 0)
            {
                return 30;//trial days
            }
            else
            {
                return (int)table.Rows[0]["Quantity"];
            }

        }
        catch { return 30; }

    }



    public static DataTable GetStorageFee()
    {
        StorageFee fee = new StorageFee();

        ObjectQuery query = fee.CreateQuery();

        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();

        DataTable table = new DataTable();
        try
        {
            query.Fill(table);
        }
        catch { table = null; }

        return table;
    }

    //need updated for subscription EFFLicense
    public static DataTable GetStorageFee(string uid)
    {
        DataTable table = new DataTable();
        ObjectQuery query = null;
        IDbConnection connection = DbFactory.CreateConnection();

        Account subfee = new Account(uid);
        if(subfee.IsExist)
        {

            query = subfee.CreateQuery();
            connection.Open();

            try
            {
                query.SetSelectFields(subfee.UID, subfee.Name, subfee.FileFolders,subfee.EnableChargeFlag, 
                    subfee.LicenseID, subfee.PersonalMonthPrice, subfee.TrialDays,subfee.PerFileRequestFee,subfee.TotalFileRequest);
                query.SetCriteria(subfee.UID, uid);
                query.Fill(table);

                if(subfee.LicenseID.Value!=-1)
                {
                     LicenseKit fee = new LicenseKit(subfee.LicenseID.Value);
                     if (table.Rows[0]["TrialDays"] == System.DBNull.Value || table.Rows[0]["TrialDays"].Equals(0))
                     {
                         table.Rows[0]["TrialDays"] = 30;
                     }

                     if (table.Rows[0]["PersonalMonthPrice"] == System.DBNull.Value || table.Rows[0]["PersonalMonthPrice"].Equals(0))
                     {
                         table.Rows[0]["PersonalMonthPrice"] = fee.MonthPricePerUser.Value;
                     }
                }
            }
            catch { table = null; }
        }
       
        return table;
        
    }


    public static void UpdateLicenseKit(LicenseKit item)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            item.Update(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }

    }

    public static void UpdateLicenseKit(SubscriptionLicense item)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            item.Update(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }

    }

    public static void AddLicenseKit(SubscriptionLicense item)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            item.Create(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }

    }




    public static void UpdateStorageFee(StorageFee item)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            item.Update(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }

    }

    public static void UpdateSubscriptionFee(SubscriptionFee item)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            item.Update(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }

    }

    public static void AddSubscriptionFee(SubscriptionFee item)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            item.Create(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }

    }


    public static DataTable GetEFFTransactions(bool bUser, string userId,DateTime startDate,DateTime endDate)
    {
        EFFTransaction trans = new EFFTransaction();
        LicenseKit license = new LicenseKit();
        ObjectQuery query = trans.CreateQuery();
        query.SetAssociation(new IColumn[] { trans.LicenseID }, new IColumn[] { license.LicenseKitID }, AssociationType.LeftJoin);
        query.SetSelectFields(trans.TransactionID, trans.FirstName, trans.LastName, trans.CreatedDate,
            trans.UserID, trans.CardType, trans.CardNumber, trans.ResponseCode, license.MaximalUserCount, license.MaxStorageVolume,
            license.MonthPricePerUser, license.KitName, license.AdditionalStoragePrice, license.AdditionalUserPrice);
        if (bUser)
        {
            query.SetCriteria(trans.UserID, userId);
        }
        
        query.SetCriteria(trans.CreatedDate, Operator.MoreEqualThan, startDate);
        query.SetCriteria(trans.CreatedDate, Operator.LessEqualThan, endDate);
        IDbConnection connection = DbFactory.CreateConnection();

        DataTable table = new DataTable();
        try
        {
            connection.Open();
            query.Fill(table);
        }
        catch { table = null; }

        return table;
    }

    public static DataTable GetEFFTransactions(bool bUser, string userId, DateTime startDate, DateTime endDate,
        string firstname,string lastname)
    {
        EFFTransaction trans = new EFFTransaction();
        LicenseKit license = new LicenseKit();
        ObjectQuery query = trans.CreateQuery();
        query.SetAssociation(new IColumn[] { trans.LicenseID }, new IColumn[] { license.LicenseKitID }, AssociationType.LeftJoin);
        query.SetSelectFields(trans.TransactionID, trans.FirstName, trans.LastName, trans.CreatedDate,
            trans.UserID, trans.CardType, trans.CardNumber, trans.ResponseCode, license.MaximalUserCount, license.MaxStorageVolume, 
            license.MonthPricePerUser, license.KitName, license.AdditionalStoragePrice, license.AdditionalUserPrice);
        if (bUser)
        {
            query.SetCriteria(trans.UserID, userId);
        }

        query.SetCriteria(trans.CreatedDate, Operator.MoreEqualThan, startDate);
        query.SetCriteria(trans.CreatedDate, Operator.LessEqualThan, endDate);

        if (firstname.Trim()!="")
        {
            query.SetCriteria(trans.FirstName, Operator.SuffixLike, firstname);
        }

        if (lastname.Trim() != "")
        {
            query.SetCriteria(trans.LastName, Operator.SuffixLike, lastname);
        }

        IDbConnection connection = DbFactory.CreateConnection();

        DataTable table = new DataTable();
        try
        {
            connection.Open();
            query.Fill(table);
        }
        catch { table = null; }

        return table;
    }

    public static DataTable GetStorageTransactions(bool bUser, string userId, DateTime startDate, DateTime endDate,
        string firstname, string lastname,int status)
    {
        StorageTransaction trans = new StorageTransaction();
        LicenseKit license = new LicenseKit();
        ObjectQuery query = trans.CreateQuery();
        query.SetSelectFields(trans.StorageTransID, trans.OfficeID, trans.Price, trans.Size,trans.ExchangeDate,
            trans.ExpiredDate, trans.Amount, trans.CardNumber, trans.CardType, trans.FirstName, trans.LastName,
            trans.ResponseAuthCode,trans.ResponseAVSCode,trans.ResponseCode,trans.ResponseReferenceCode,trans.ResponseDescription);
        if (bUser)
        {
            query.SetCriteria(trans.OfficeID, userId);
        }

        query.SetCriteria(trans.ExchangeDate, Operator.MoreEqualThan, startDate);
        query.SetCriteria(trans.ExchangeDate, Operator.LessEqualThan, endDate);
        if (status==2)
        {
            query.SetCriteria(trans.ResponseCode, Operator.NotEqual, 1);
        }
        else if (status == 1)
        {
            query.SetCriteria(trans.ResponseCode, Operator.Equal, 1);
        }
        
        if (firstname.Trim() != "")
        {
            query.SetCriteria(trans.FirstName, Operator.SuffixLike, firstname);
        }

        if (lastname.Trim() != "")
        {
            query.SetCriteria(trans.LastName, Operator.SuffixLike, lastname);
        }

        IDbConnection connection = DbFactory.CreateConnection();

        DataTable table = new DataTable();
        try
        {
            connection.Open();
            query.Fill(table);
        }
        catch { table = null; }

        return table;
    }

    public static DataTable GetGiftNumberTransactions()
    {
        Gift gift = new Gift();
        Account account = new Account();
        ObjectQuery query = gift.CreateQuery();

        query.SetAssociation(new IColumn[] { gift.UserID }, new IColumn[] { account.UID }, AssociationType.LeftJoin);
        query.SetCriteria(gift.UseFlag,true);
        query.SetSelectFields(new IColumn[] { gift.GiftNumber, gift.UserID, gift.UserName,gift.InputDate, account.Firstname,account.Lastname});

        IDbConnection connection = DbFactory.CreateConnection();

        DataTable table = new DataTable();
        try
        {
            connection.Open();
            query.Fill(table);
        }
        catch { table = null; }

        return table;
    }
}
