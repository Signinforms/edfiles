using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.DAL;
using Shinetech.Utility;
using Account = Shinetech.DAL.Account;
using Page = System.Web.UI.Page;

/// <summary>
/// Summary description for LicensePage
/// </summary>
public class LicensePage : BasePage
{
    public LicensePage()
    {
        if (Membership.GetUser() != null)
        {
            if (User.IsInRole("Offices") || User.IsInRole("Users"))
            {
                //string abc = Session["SwitchedSessionId"].ToString();
                string uid = Sessions.SwitchedSessionId;
                //string uid = Membership.GetUser().ProviderUserKey.ToString();
                Account user = new Account(uid);
                if (user.IsExist)
                {
                    if (user.IsSubUser.Value == "1" && !Common_Tatva.IsUserSwitched())
                    {
                        user = new Account(user.OfficeUID.Value);
                    }

                    if (user.LicenseID.Value == -1)//trial version liecense
                    {
                        if (user.CreateDate.Value.AddDays(TrialDays(uid, user)) < DateTime.Now)
                        {
                            Server.Transfer("~/InvalidPage.aspx");
                        }

                        double masiz = FileFolderManagement.GetUsedEFFSizes(user.UID.Value);
                        if (masiz >= 50F)
                        {
                            Server.Transfer("~/InvalidPage.aspx");
                        }

                    }

                }
            }
        }
    }
    public int TrialDays(string uid, Account user)
    {

        if (user.TrialDays.Value.Equals(DBNull.Value) || user.TrialDays.Value.Equals(0))
        {

            return 30;
        }
        else
        {
            return user.TrialDays.Value;

        }
    }
}

public class StoragePage : LicensePage
{
    public StoragePage()
    {
        if (Membership.GetUser() != null)
        {
            if (User.IsInRole("Offices") || User.IsInRole("Users"))
            {
                string uid = Membership.GetUser().ProviderUserKey.ToString();
                Account user = new Account(uid);
                if (user.IsExist)
                {
                    if (user.IsSubUser.Value == "1")
                    {
                        user = new Account(user.OfficeUID.Value);
                    }

                    if (user.LicenseID.Value == -1)//trial version license
                    {

                        double masiz = FileFolderManagement.GetUsedEFFSizes(user.UID.Value);
                        if (masiz >= 50F)
                        {
                            sendWelcomeEmail(user.Name.Value, user.Email.Value);
                            Server.Transfer("~/InvalidPage.aspx");
                        }

                    }

                }
            }
        }
    }

    public int TrialDays
    {
        get
        {

            int iDays = 30;
            //License trial = new License(-1);
            //if (trial.IsExist)
            //{
            //    iDays = trial.Period.Value;
            //}
            return iDays;
        }
    }

    private void sendWelcomeEmail(string username, string emailaddr)
    {
        string strMailTemplet = getStorageMailTemplate();

        strMailTemplet = strMailTemplet.Replace("[username]", username);

        string strEmailSubject = "Storage Size Reminder Form EdFiles";

        Email.SendFromEFileFolder(emailaddr, strEmailSubject, strMailTemplet);

    }

    private string getStorageMailTemplate()
    {
        string strMailTemplet = string.Empty;
        string physicalPath = Request.PhysicalPath;
        int index = physicalPath.LastIndexOf("\\");
        string mailTempletUrl = physicalPath.Substring(0, index + 1) + "StorageTrialUser.htm";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }
}

public class OverLicensePage : Page //modified by jonlu655 2009-07-28
{
    public OverLicensePage()
    {
        string uid = Membership.GetUser().ProviderUserKey.ToString();
        Account user = new Account(uid);
        if (user.IsExist)
        {
            if (user.IsSubUser.Value == "1")
            {
                user = new Account(user.OfficeUID.Value);
            }

            uid = user.UID.Value;
            if (user.FileFolders.Value <= FileFolderManagement.GetUsedFoldersNumber(uid))
            {
                Server.Transfer("~/OverrValidPage.aspx");
            }
        }
        else
        {
            throw new ApplicationException(string.Format("The user:{0} don't exist", uid));
        }
    }

    public int TrialDays
    {
        get
        {

            int iDays = 30;
            //License trial = new License(-1);
            //if (trial.IsExist)
            //{
            //    iDays = trial.Period.Value;
            //}
            return iDays;
        }
    }
}
