﻿using log4net;
using log4net.Config;
using log4net.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for Logger
/// </summary>

    public class Logger 
    {
        private readonly ILog _log;

        public Logger()
        {
            XmlConfigurator.Configure();
            _log = LogManager.GetLogger((MethodBase.GetCurrentMethod().DeclaringType));
        }

        public void LogException(Severity severity, string message, Exception ex)
        {
            try
            {
                StringBuilder formattedMessage = new StringBuilder();
                formattedMessage.Append(string.Format("{0} Message : {1}\n InnerException : {2}\n StackTrace : {3}", message,
                    ex.Message, ex.InnerException, ex.StackTrace));
                formattedMessage.Append(
                    "\n=================================================================================================================================================");
                Log(severity, formattedMessage.ToString());

                if (ex.InnerException != null)
                {
                    LogException(severity, message, ex.InnerException);
                }
            }
            catch (Exception e)
            { }
        }

        public void LogException(Severity severity, string message)
        {
            try
            {
                StringBuilder formattedMessage = new StringBuilder();
                formattedMessage.Append(string.Format("Message : {0}", message));
                Log(severity, formattedMessage.ToString());
            }
            catch (Exception e)
            { }
        }

        public void LogException(string message, Exception ex)
        {
            try
            {
                LogException(Severity.Error, message, ex);
            }
            catch (Exception e)
            { }
        }

        public void LogFormat(Severity severity, string message, params object[] args)
        {
            string formattedMessage = string.Format(message, args);

            Log(severity, formattedMessage);
        }

        public void Log(Severity severity, string message)
        {
            switch (severity)
            {
                case (Severity.Debug):
                    {
                        _log.Debug(message);
                        break;
                    }
                case (Severity.Info):
                    {
                        _log.Info(message);
                        break;
                    }
                case (Severity.Warning):
                    {
                        _log.Warn(message);
                        break;
                    }
                case (Severity.Error):
                    {
                        _log.ErrorFormat(message);
                        break;
                    }
            }
        }
    }

    public enum Severity
    {
        Debug,
        Info,
        Warning,
        Error
    }
