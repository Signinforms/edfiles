﻿using System;
using NLog;
using System.Linq;
using System.Diagnostics;
using System.Net;

/// <summary>
/// This class is used for all method of NLog
/// </summary>
public class NLogLogger
{
    #region Fields

    private const string _loggerName = "NLogLogger";
    private NLog.Logger _logger;

    #endregion

    #region Ctor

    public NLogLogger()
    {
        _logger = LogManager.GetCurrentClassLogger();
    }

    public static ILogger GetLoggingService()
    {
        ILogger logger = (ILogger)LogManager.GetLogger("NLogLogger", typeof(NLogLogger));
        return logger;
    }

    #endregion

    #region Utilities

    /// <summary>
    /// get event log
    /// </summary>
    /// <param name="loggerName">loggername</param>
    /// <param name="level">log level</param>
    /// <param name="exception">exception</param>
    /// <param name="format">message</param>
    /// <param name="args">arguments</param>
    /// <returns>log event info</returns>
    private LogEventInfo GetLogEvent(string loggerName, LogLevel level, Exception exception, string format, object[] args)
    {
        string assemblyProp = string.Empty;
        string classProp = string.Empty;
        string methodProp = string.Empty;
        string messageProp = string.Empty;
        string innerMessageProp = string.Empty;

        var logEvent = new LogEventInfo
            (level, loggerName, string.Format(format, args));

        if (exception != null)
        {
            assemblyProp = exception.Source;
            //classProp = exception.TargetSite != null ? exception.TargetSite.DeclaringType.FullName : null;
            //methodProp = exception.TargetSite != null ? exception.TargetSite.Name : null;
            messageProp = exception.Message;

            if (exception.InnerException != null)
            {
                innerMessageProp = exception.InnerException.Message;
            }
        }

        logEvent.Properties["error-source"] = assemblyProp;
        logEvent.Properties["error-class"] = classProp;
        logEvent.Properties["error-method"] = methodProp;
        logEvent.Properties["error-message"] = messageProp;
        logEvent.Properties["inner-error-message"] = innerMessageProp;
        logEvent.Exception = exception;

        return logEvent;
    }

    #endregion

    #region Properties

    /// <summary>
    /// get or set value of is debug enabled or not?
    /// </summary>
    public bool IsDebugEnabled
    {
        get
        {
            return true; // _logger.IsDebugEnabled;
        }
    }

    /// <summary>
    /// get or set value of is error enabled or not?
    /// </summary>
    public bool IsErrorEnabled
    {
        get
        {
            return _logger.IsErrorEnabled;
        }
    }

    /// <summary>
    /// get or set value of is fatal enabled or not?
    /// </summary>
    public bool IsFatalEnabled
    {
        get
        {
            return _logger.IsFatalEnabled;
        }
    }

    /// <summary>
    /// get or set value of is info enabled or not?
    /// </summary>
    public bool IsInfoEnabled
    {
        get
        {
            return _logger.IsInfoEnabled;
        }
    }

    /// <summary>
    /// get or set value of is trace enabled or not?
    /// </summary>
    public bool IsTraceEnabled
    {
        get
        {
            return _logger.IsTraceEnabled;
        }
    }

    /// <summary>
    /// get or set value of is warn enabled or not?
    /// </summary>
    public bool IsWarnEnabled
    {
        get
        {
            return _logger.IsWarnEnabled;
        }
    }

    #endregion

    #region Debug

    /// <summary>
    /// debug
    /// </summary>
    /// <param name="format">message</param>
    /// <param name="args">arguments</param>
    public void Debug(string format, params object[] args)
    {
        _logger.Debug(format, args);
    }

    /// <summary>
    /// debug exception
    /// </summary>
    /// <param name="exception">exception</param>
    public void Debug(Exception exception)
    {
        this.Debug(exception, string.Empty);
    }

    /// <summary>
    /// debug exception with message
    /// </summary>
    /// <param name="exception">exception</param>
    /// <param name="format">message</param>
    /// <param name="args">arguments</param>
    public void Debug(Exception exception, string format, params object[] args)
    {
        if (!_logger.IsDebugEnabled) return;
        var logEvent = GetLogEvent(_loggerName, LogLevel.Debug, exception, format, args);
        _logger.Log(typeof(NLogLogger), logEvent);
    }

    #endregion

    #region Error

    /// <summary>
    /// error exception with message
    /// </summary>
    /// <param name="exception">exception</param>
    /// <param name="format">message</param>
    /// <param name="args">arguments</param>
    public void Error(Exception exception, string format, params object[] args)
    {
        if (!_logger.IsErrorEnabled) return;
        var logEvent = GetLogEvent(_loggerName, LogLevel.Error, exception, format, args);
        _logger.Log(typeof(NLogLogger), logEvent);
    }

    /// <summary>
    /// error exceptions
    /// </summary>
    /// <param name="exception">exception</param>
    public void Error(Exception exception)
    {
        Error(exception, string.Empty);
    }

    /// <summary>
    /// error message
    /// </summary>
    /// <param name="format">message</param>
    /// <param name="args">arguments</param>
    public void Error(string format, params object[] args)
    {
        _logger.Error(format, args);
    }

    #endregion

    #region Fatal

    /// <summary>
    /// fatal exception with message
    /// </summary>
    /// <param name="exception">exception</param>
    /// <param name="format">message</param>
    /// <param name="args">arguments</param>
    public void Fatal(Exception exception, string format, params object[] args)
    {
        if (!_logger.IsFatalEnabled) return;
        var logEvent = GetLogEvent(_loggerName, LogLevel.Fatal, exception, format, args);
        _logger.Log(typeof(NLogLogger), logEvent);
    }

    /// <summary>
    /// fatal excptions
    /// </summary>
    /// <param name="exception">exception</param>
    public void Fatal(Exception exception)
    {
        this.Fatal(exception, string.Empty);
    }

    /// <summary>
    /// fatal message
    /// </summary>
    /// <param name="format">message</param>
    /// <param name="args">arguments</param>
    public void Fatal(string format, params object[] args)
    {
        _logger.Fatal(format, args);
    }

    #endregion

    #region Info

    /// <summary>
    /// info exception with message
    /// </summary>
    /// <param name="exception">exceptions</param>
    /// <param name="format">message</param>
    /// <param name="args">arguments</param>
    public void Info(Exception exception, string format, params object[] args)
    {
        if (!_logger.IsInfoEnabled) return;
        var logEvent = GetLogEvent(_loggerName, LogLevel.Info, exception, format, args);
        _logger.Log(typeof(NLogLogger), logEvent);
    }

    /// <summary>
    /// info exceptions
    /// </summary>
    /// <param name="exception">excepions</param>
    public void Info(Exception exception)
    {
        Info(exception, string.Empty);
    }

    /// <summary>
    /// info message
    /// </summary>
    /// <param name="format">message</param>
    /// <param name="args">arguments</param>
    public void Info(string format, params object[] args)
    {
        _logger.Info(format, args);
    }

    #endregion

    #region Trace

    /// <summary>
    /// trace exception with message
    /// </summary>
    /// <param name="exception">exception</param>
    /// <param name="format">message</param>
    /// <param name="args">arguments</param>
    public void Trace(Exception exception, string format, params object[] args)
    {
        if (!_logger.IsTraceEnabled) return;
        var logEvent = GetLogEvent(_loggerName, LogLevel.Trace, exception, format, args);
        _logger.Log(typeof(NLogLogger), logEvent);
    }

    /// <summary>
    /// trace exception
    /// </summary>
    /// <param name="exception">exceptions</param>
    public void Trace(Exception exception)
    {
        Trace(exception, string.Empty);
    }

    /// <summary>
    /// trace message
    /// </summary>
    /// <param name="format">message</param>
    /// <param name="args">arguments</param>
    public void Trace(string format, params object[] args)
    {
        _logger.Trace(format, args);
    }

    #endregion

    #region Warn

    /// <summary>
    /// warn exception with message
    /// </summary>
    /// <param name="exception">exception</param>
    /// <param name="format">message</param>
    /// <param name="args">arguments</param>
    public void Warn(Exception exception, string format, params object[] args)
    {
        if (!_logger.IsWarnEnabled) return;
        var logEvent = GetLogEvent(_loggerName, LogLevel.Warn, exception, format, args);
        _logger.Log(typeof(NLogLogger), logEvent);
    }

    /// <summary>
    /// warn exceptions
    /// </summary>
    /// <param name="exception">exceptions</param>
    public void Warn(Exception exception)
    {
        Warn(exception, string.Empty);
    }

    /// <summary>
    /// warn message
    /// </summary>
    /// <param name="format">message</param>
    /// <param name="args">arguments</param>
    public void Warn(string format, params object[] args)
    {
        _logger.Warn(format, args);
    }

    #endregion
}
