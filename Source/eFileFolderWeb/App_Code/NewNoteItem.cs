﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///NewNoteItem 的摘要说明
/// </summary>
public class NewNoteItem
{
    public int Id { get; set; }

    public int DividerId { get; set; }

    public string Title { get; set; }

    public string Author { get; set; }

    public string Content { get; set; }

    public DateTime LastModDate { get; set; }

}