﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Diagnostics;
using PdfSharp.Drawing;

/// <summary>
/// Summary description for PDFParser
/// </summary>
public class PDFParser
{
    /// <summary>
    /// Fetch texts from pdf file
    /// </summary>
    /// <param name="filename"></param>
    /// <param name="stream"></param>
    /// <returns></returns>
    public static string ExtractTextFromPdfImages(string filename, Stream stream)
    {
        //List<String> pages = new List<String>();
        var ocrtext = string.Empty;
        var tesseractPath = HttpContext.Current.Server.MapPath("~/tesseract-master.1153");
        try
        {
            string prevPage = "";
            //var sb = new StringBuilder();
            var images = new Dictionary<string, System.Drawing.Image>();
            if (System.IO.Path.GetExtension(filename).ToLower() == ".tiff" || System.IO.Path.GetExtension(filename).ToLower() == ".tif")
            {
                try
                {
                    Bitmap imgSrc = (Bitmap)Image.FromStream(stream);
                    int count = imgSrc.GetFrameCount(FrameDimension.Page);
                    for (int i = 0; i < count; i++)
                    {
                        try
                        {
                            imgSrc.SelectActiveFrame(FrameDimension.Page, i);
                            MemoryStream strm = new MemoryStream();
                            imgSrc.Save(strm, System.Drawing.Imaging.ImageFormat.Tiff);
                            Bitmap bitMap = new Bitmap(strm);
                            ImageConverter converter = new ImageConverter();
                            var imgFile = (byte[])converter.ConvertTo(bitMap, typeof(byte[]));
                            ocrtext += ParseText(tesseractPath, imgFile, "eng", "fra");
                        }
                        catch (Exception ex)
                        {
                            Common_Tatva.WriteElasticLog(string.Empty, string.Empty, filename, ex);
                        }
                    }
                    //pages.Add(ocrtext.ToLower());
                    //return pages.ToArray();
                    return ocrtext;
                }
                catch (Exception ex)
                {
                    Common_Tatva.WriteElasticLog(string.Empty, string.Empty, filename, ex);
                    //return pages.ToArray();
                    return ocrtext;
                }
            }
            else
            {
                byte[] bytes = ReadToEnd(stream);
                using (var reader = new PdfReader(bytes))
                {
                    var parser = new PdfReaderContentParser(reader);
                    ImageRenderListener listener = null;
                    Common_Tatva.WriteElasticResponse(string.Empty, string.Empty, filename, "Step 1 : Parsing started & Page numbers = " + reader.NumberOfPages);
                    for (var i = 1; i <= reader.NumberOfPages; i++)
                    {
                        var sb = new StringBuilder();
                        Common_Tatva.WriteElasticResponse(string.Empty, string.Empty, filename, "Step 2(" + i + ") : Parsing for page number : " + i);
                        parser.ProcessContent(i, (listener = new ImageRenderListener()));
                        var index = 1;
                        if (listener.Images.Count > 0)
                        {
                            Common_Tatva.WriteElasticResponse(string.Empty, string.Empty, filename, "Step 3(" + i + ") : Parsing for page number : " + i + " Found the image & count is : " + listener.Images.Count);
                            try
                            {
                                foreach (KeyValuePair<System.Drawing.Image, string> pair in listener.Images)
                                {
                                    images.Add(string.Format("{0}_Page_{1}_Image_{2}{3}",
                                       System.IO.Path.GetFileNameWithoutExtension(filename), i.ToString("D4"), index.ToString("D4"), pair.Value), pair.Key);
                                    Bitmap imgsource = new Bitmap(pair.Key);
                                    ImageConverter converter = new ImageConverter();
                                    var imgFile = (byte[])converter.ConvertTo(imgsource, typeof(byte[]));
                                    ocrtext += ParseText(tesseractPath, imgFile, "eng", "fra");
                                    index++;
                                }
                                ITextExtractionStrategy its = new SimpleTextExtractionStrategy();
                                var s = PdfTextExtractor.GetTextFromPage(reader, i, its);
                                if (prevPage != s) sb.Append(s);
                                prevPage = s;
                                ocrtext += sb.ToString();
                                Common_Tatva.WriteElasticResponse(string.Empty, string.Empty, filename, "Step 4(" + i + ") : Parsing for page number : " + i + " has been completed");
                            }
                            catch (Exception ex)
                            {
                                Common_Tatva.WriteElasticResponse(string.Empty, string.Empty, filename, "Page number = " + i + " Error = " + ex.Message);
                                continue;
                            }
                        }
                        else
                        {
                            Common_Tatva.WriteElasticResponse(string.Empty, string.Empty, filename, "Step 3(" + i + ") : Parsing for page number : " + i + "and No image has been found");
                            ITextExtractionStrategy its = new SimpleTextExtractionStrategy();
                            var s = PdfTextExtractor.GetTextFromPage(reader, i, its);
                            if (prevPage != s) sb.Append(s);
                            prevPage = s;
                            ocrtext += sb.ToString();
                            Common_Tatva.WriteElasticResponse(string.Empty, string.Empty, filename, "Step 4(" + i + ") : Parsing for page number : " + i + " has been completed");
                        }
                        //pages.Add(pagetext.ToLower());
                    }
                    Common_Tatva.WriteElasticResponse(string.Empty, string.Empty, filename, "Step 5 : Parsing completed");
                    //return pages.ToArray();
                    return ocrtext;
                }
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteElasticLog(ex.Message, "", null, ex);
            //return pages.ToArray();
            return ocrtext;
        }
    }

    internal class ImageRenderListener : IRenderListener
    {
        #region Fields
        Dictionary<System.Drawing.Image, string> images = new Dictionary<System.Drawing.Image, string>();
        #endregion Fields
        #region Properties
        public Dictionary<System.Drawing.Image, string> Images
        {
            get { return images; }
        }
        #endregion Properties
        #region Methods
        #region Public Methods
        public void BeginTextBlock() { }
        public void EndTextBlock() { }
        public void RenderImage(ImageRenderInfo renderInfo)
        {
            try
            {
                //var image1 = renderInfo.GetImageCTM();
                PdfImageObject image = renderInfo.GetImage();
                PdfName filter = (PdfName)image.Get(PdfName.FILTER);

                //int width = Convert.ToInt32(image.Get(PdfName.WIDTH).ToString());
                //int bitsPerComponent = Convert.ToInt32(image.Get(PdfName.BITSPERCOMPONENT).ToString());
                //string subtype = image.Get(PdfName.SUBTYPE).ToString();
                //int height = Convert.ToInt32(image.Get(PdfName.HEIGHT).ToString());
                //int length = Convert.ToInt32(image.Get(PdfName.LENGTH).ToString());
                //string colorSpace = image.Get(PdfName.COLORSPACE).ToString();
                /* It appears to be safe to assume that when filter == null, PdfImageObject
                 * does not know how to decode the image to a System.Drawing.Image.
                 *
                 * Uncomment the code above to verify, but when I’ve seen this happen,
                 * width, height and bits per component all equal zero as well. */
                if (filter != null)
                {
                    System.Drawing.Image drawingImage = image.GetDrawingImage();
                    string extension = ".";
                    if (filter == PdfName.DCTDECODE)
                    {
                        extension += PdfImageObject.ImageBytesType.JPG.FileExtension;
                    }
                    else if (filter == PdfName.JPXDECODE)
                    {
                        extension += PdfImageObject.ImageBytesType.JP2.FileExtension;
                    }
                    else if (filter == PdfName.FLATEDECODE)
                    {
                        extension += PdfImageObject.ImageBytesType.PNG.FileExtension;
                    }
                    else if (filter == PdfName.LZWDECODE || filter == PdfName.CCITTFAXDECODE)
                    {
                        extension += PdfImageObject.ImageBytesType.CCITT.FileExtension;
                    }
                    /* Rather than struggle with the image stream and try to figure out how to handle
                     * BitMapData scan lines in various formats (like virtually every sample I’ve found
                     * online), use the PdfImageObject.GetDrawingImage() method, which does the work for us. */
                    this.Images.Add(drawingImage, extension);
                }
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteElasticLog(string.Empty, string.Empty, string.Empty, ex);
            }
        }
        public void RenderText(TextRenderInfo renderInfo) { }
        #endregion Public Methods
        #endregion Methods
    }

    /// <summary>
    /// Convert stream to bytes array
    /// </summary>
    /// <param name="stream"></param>
    /// <returns></returns>
    public static byte[] ReadToEnd(System.IO.Stream stream)
    {
        long originalPosition = 0;

        if (stream.CanSeek)
        {
            originalPosition = stream.Position;
            stream.Position = 0;
        }

        try
        {
            byte[] readBuffer = new byte[4096];

            int totalBytesRead = 0;
            int bytesRead;

            while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
            {
                totalBytesRead += bytesRead;

                if (totalBytesRead == readBuffer.Length)
                {
                    int nextByte = stream.ReadByte();
                    if (nextByte != -1)
                    {
                        byte[] temp = new byte[readBuffer.Length * 2];
                        Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                        Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                        readBuffer = temp;
                        totalBytesRead++;
                    }
                }
            }

            byte[] buffer = readBuffer;
            if (readBuffer.Length != totalBytesRead)
            {
                buffer = new byte[totalBytesRead];
                Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
            }
            return buffer;
        }
        finally
        {
            if (stream.CanSeek)
            {
                stream.Position = originalPosition;
            }
        }
    }

    public static Stream ConvertTiffToPdf(string filename, Stream stream = null, string tiffFile = null)
    {
        Stream strm = new MemoryStream();
        try
        {
            if (stream != null && string.IsNullOrEmpty(tiffFile))
                tiffFile = HttpContext.Current.Server.MapPath("~/" + Common_Tatva.RackSpaceArchiveDownload + System.IO.Path.DirectorySeparatorChar + System.IO.Path.GetFileNameWithoutExtension(filename) + DateTime.UtcNow.ToString("hhMMssmm") + System.IO.Path.GetExtension(filename));
            PdfSharp.Pdf.PdfDocument s_document = new PdfSharp.Pdf.PdfDocument();

            PdfSharp.Pdf.PdfPage pagepdf = s_document.AddPage();

            if (!File.Exists(tiffFile))
            {
                var fileStream = File.Create(tiffFile);
                stream.Seek(0, SeekOrigin.Begin);
                stream.CopyTo(fileStream);
                fileStream.Close();
            }

            XGraphics gfx = XGraphics.FromPdfPage(pagepdf);
            XImage image = XImage.FromFile(tiffFile);

            pagepdf.Width = image.PointWidth;
            pagepdf.Height = image.PointHeight;
            gfx.DrawImage(image, 0, 0);
            s_document.Save(System.IO.Path.ChangeExtension(tiffFile, ".pdf"));
            byte[] bytes = File.ReadAllBytes(System.IO.Path.ChangeExtension(tiffFile, ".pdf"));
            strm = new MemoryStream(bytes);
            try
            {
                if (File.Exists(tiffFile))
                    File.Delete(tiffFile);
            }
            catch (Exception) { }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteElasticLog(string.Empty, string.Empty, filename, ex);
        }
        return strm;
    }

    private static string ParseText(string tesseractPath, byte[] imageFile, params string[] lang)
    {
        string output = string.Empty;
        var tempOutputFile = System.IO.Path.GetTempPath() + Guid.NewGuid();
        var tempImageFile = System.IO.Path.GetTempFileName();

        try
        {
            File.WriteAllBytes(tempImageFile, imageFile);

            ProcessStartInfo info = new ProcessStartInfo();
            info.WorkingDirectory = tesseractPath;
            info.WindowStyle = ProcessWindowStyle.Hidden;
            info.UseShellExecute = false;
            info.FileName = "cmd.exe";
            info.Arguments =
                "/c tesseract.exe " +
                // Image file.
                tempImageFile + " " +
                // Output file (tesseract add '.txt' at the end)
                tempOutputFile +
                // Languages.
                " -l " + string.Join("+", lang);

            // Start tesseract.
            Process process = Process.Start(info);
            process.WaitForExit();
            if (process.ExitCode == 0)
            {
                // Exit code: success.
                output = File.ReadAllText(tempOutputFile + ".txt");
            }
            else
            {
                throw new Exception("Error. Tesseract stopped with an error code = " + process.ExitCode);
            }
        }
        finally
        {
            File.Delete(tempImageFile);
            File.Delete(tempOutputFile + ".txt");
        }
        return output;
    }

}