﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///PageItem 的摘要说明
/// </summary>
public class PageItem
{
    public int Id { get; set; }

    public int FileId { get; set; }

    public string Url_1 { get; set; }
    public string Url_2 { get; set; }

    public string Ext { get; set; }
}