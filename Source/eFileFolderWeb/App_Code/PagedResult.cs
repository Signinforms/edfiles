using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for PagedResult
/// </summary>
public class PagedResult<T> where T : UserBase
{
    private int _total;
    private List<T> _rows;

    public int Total
    {
        get
        {
            return _total;
        }
        set
        {
            _total = value;
        }
    }

    public List<T> Rows
    {
        get
        {
            return _rows;
        }
        set
        {
            _rows = value;
        }
    }
}
