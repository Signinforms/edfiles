﻿using Shinetech.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

/// <summary>
/// Summary description for PdfViewerContent
/// </summary>
public class PdfViewerContent
{
	public PdfViewerContent()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    [WebMethod]
    public static string PrepareURL(int documentID, int folderID, string shareID = null)
    {
        string url = string.Empty;
        try
        {
            string httpConnection = string.Empty;
            url = ConfigurationManager.AppSettings["PreviewURL"];
            if (HttpContext.Current.Request.IsSecureConnection)
                httpConnection = "https://";
            else
                httpConnection = "http://";
            if (folderID == (int)Enum_Tatva.Folders.EFileShare)
                url = httpConnection + url + "url=" + httpConnection + HttpContext.Current.Request.Url.Authority + ConfigurationManager.AppSettings["VirtualDir"] + "Office/PdfViewer.aspx?data=" + QueryString.QueryStringEncode("ID=" + shareID + "&folderID=" + folderID + "&sessionID=" + Sessions.SwitchedSessionId) + "&embedded=true";
            else if(folderID == (int)Enum_Tatva.Folders.WorkArea)
                url = httpConnection + url + "url=" + httpConnection + HttpContext.Current.Request.Url.Authority + ConfigurationManager.AppSettings["VirtualDir"] + "Office/PdfViewer.aspx?data=" + QueryString.QueryStringEncode("ID=" + shareID + "&folderID=" + folderID + "&sessionID=" + Sessions.SwitchedRackspaceId) + "&embedded=true";
            else
                url = httpConnection + url + "url=" + httpConnection + HttpContext.Current.Request.Url.Authority + ConfigurationManager.AppSettings["VirtualDir"] + "Office/PdfViewer.aspx?data=" + QueryString.QueryStringEncode("ID=" + documentID + "&folderID=" + folderID + "&sessionID=" + Sessions.SwitchedRackspaceId) + "&embedded=true";
        }
        catch (Exception ex)
        { }
        return url;
    }
}