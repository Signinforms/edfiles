using System;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for QuestionEntity
/// </summary>
[Serializable]
public class QuestionEntity
{
    public QuestionEntity()
    {
        _QuestionID = -1;
        _QuestionTitle = "";
    }

    public QuestionEntity(int questionId, string questioTitle)
    {
        _QuestionID = questionId;
        _QuestionTitle = questioTitle;
     

    }

    private int _QuestionOrder;
    [Bindable(true)]
    public int QuestionOrder
    {
        get { return _QuestionOrder; }

        set { _QuestionOrder = value; }
    }

    private int _QuestionID;
    [Bindable(true)]
    public int QuestionID
    {
        get { return _QuestionID; }

        set { _QuestionID = value; }
    }

    private string _QuestionTitle;
    [Bindable(true)]
    public string QuestionTitle
    {
        get { return _QuestionTitle; }

        set { _QuestionTitle = value; }
    }

    private string _AnswerContent;
    [Bindable(true)]
    public string AnswerContent
    {
        get { return _AnswerContent; }

        set { _AnswerContent = value; }
    }

    private string _OfficeID;
    [Bindable(true)]
    public string OfficeID
    {
        get { return _OfficeID; }

        set { _OfficeID=value; }
    }

    private bool _IsTextBox;
    [Bindable(true)]
    public bool IsTextBox
    {
        get { return _IsTextBox; }

        set { _IsTextBox = value; }
    }
}
