﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///QuitNoteItem 的摘要说明
/// </summary>
public class QuitNoteItem
{
    public int Id { get; set; }

    public int PageId { get; set; }

    public string PageKey { get; set; }

    public string Content { get; set; }

    public DateTime LastModDate { get; set; }

    public int X { get; set; }

    public int Y { get; set; }

    public bool Fixed { get; set; }

    public bool Opened { get; set; }
}