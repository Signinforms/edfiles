using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections;
using Shinetech.DAL;

/// <summary>
/// Summary description for RecentFolder
/// </summary>
[Serializable]
public class RecentFolder
{
    private List<FolderItem> _FoldertItems = new List<FolderItem>();

    public RecentFolder()
    {
    }

    public RecentFolder(string uid)
    {
        this.GetData(uid);
    }
    public int GetNumEFF(string uID)
    {
        int numEFF;
        numEFF = FileFolderManagement.GetNumEFF(uID);
        return numEFF;
    }

    public int GetUsedEFFNum(string uID)
    {
        int usedEFFNum;
        usedEFFNum = FileFolderManagement.GetUsedEFFNum(uID);
        return usedEFFNum;
    }

    public int GetUnUsedEFFNum(string uID)
    {
        int unUsedEFFNum;
        unUsedEFFNum = FileFolderManagement.GetUnUsedEFFNum(uID);
        return unUsedEFFNum;
    }

    public float GetUsedEFFSizes(string uID)
    {
        double totalSizes = 0;
       
        totalSizes = FileFolderManagement.GetUsedEFFSizes(uID);

        return (float)totalSizes;
      
    }
    // Return all the items from the Shopping Cart
    public List<FolderItem> FoldertItems
    {
        get { return _FoldertItems; }
    }

    public void Add(FolderItem folder)
    {
        FileFolderManagement.CreatReminder(folder.FolderID, folder.UID, DateTime.Now);

        /*foreach (FolderItem item in _FoldertItems)
        {
            if (item.FolderID.Equals(folder.FolderID))
            {
                _FoldertItems.Remove(item);
                _FoldertItems.Add(folder);
                break;
            }
        }

        if (this.Count > 10)
        {
            _FoldertItems.RemoveAt(this.Count - 1);
        }

        _FoldertItems.Add(folder);

        int i = 1;
        foreach (FolderItem item in _FoldertItems)
        {
            item.Number = i++;        
        }*/
        
    }

    public int Count
    {
        get
        {
            return _FoldertItems.Count;
        }
    }

    public void Remove(string key)
    {   
        foreach(FolderItem item in _FoldertItems)
        {
            if(item.FolderID.Equals(key))
            {
                _FoldertItems.Remove(item);
                break;
            }  
        }
    }

    public void GetData(string uID)
    {
        DataTable table = FileFolderManagement.GetReminders(uID);

        int i = 1;
        foreach (DataRow row in table.Rows)
        {
            FolderItem item = new FolderItem(row);
            item.Number = i;
            this.FoldertItems.Add(item);
            i++;
        }
    }



}

[Serializable]
public class LastEFFolders
{
    private List<FolderItem> _FoldertItems = new List<FolderItem>();

    public LastEFFolders()
    {
    }

    public void AppendItem(FolderItem item)
    {
        FolderItem fItem;
        if (ContainKey(item.FolderID, out fItem))
        {
            _FoldertItems.Remove(fItem);
            _FoldertItems.Insert(0, item);
        }
        else
        {
            if (_FoldertItems.Count >= 10)
            {
                _FoldertItems.RemoveAt(9);
                _FoldertItems.Insert(0, item);
            }
            else
            {
                FoldertItems.Insert(0, item);
            }
        }

        for (int i = 0; i < 10; i++)
        {
            FolderItem objItem = _FoldertItems[i];
            objItem.Number = i + 1;
        }

    }

    public void RemoveItem(int itemId)
    {
        int count = _FoldertItems.Count;
        for (int i = 0; i < count; i++)
        {
            FolderItem objItem = _FoldertItems[i];
            if (objItem.FolderID == itemId.ToString())
            {
                _FoldertItems.Remove(objItem);
                break;
            }
        }

        for (int i = 0; i < 10; i++)
        {
            FolderItem objItem = _FoldertItems[i];
            objItem.Number = i + 1;
        }
    }

    protected bool ContainKey(string key, out FolderItem target)
    {
        foreach (FolderItem item in _FoldertItems)
        {
            if (item.FolderID == key)
            {
                target = item;
                return true;
            }
        }
        target = null;
        return false;
    }

    public List<FolderItem> FoldertItems
    {
        get { return _FoldertItems; }
    }
}
