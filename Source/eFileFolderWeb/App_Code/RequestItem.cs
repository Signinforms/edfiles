using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for FolderItem
/// </summary>
[Serializable]
public class RequestItem
{
    private int number;

    public RequestItem()
    {
       
    }

    public RequestItem(string id)
    {
        fileId = id;
        //boxId = id;
    }

    public RequestItem(DataRow item)
    {
        this.Parse(item);
    }

    protected void Parse(DataRow item)
    {
        fileId = item["FileID"].ToString();
        uid = item["UserID"].ToString();
        firstName = item["Firstname"].ToString();
        lastName = item["Lastname"].ToString();
        createDate = (DateTime)item["CreateDate"];

        //boxId = item["BoxId"].ToString();
        //uid = item["UserID"].ToString();
        //boxName = item["BoxName"].ToString();
        //destroyDate = (DateTime)item["DestroyDate"];
    }

    private string fileId;

        private string boxId;

    public string FileId
    {
       get
       {
           return fileId;
       }
       set
       {
           fileId = value;
       }
    }

    public string BoxId
    {
        get
        {
            return boxId;
        }
        set
        {
            boxId = value;
        }
    }

   
    public int Number
    {
        get
        {
            return number;
        }
        set
        {
            number = value;
        }
    }

    private string firstName;
    private string boxName;

    public string BoxName
    {
        get
        {
            return boxName;
        }
        set
        {
            boxName = value;
        }
    }
    public string FirstName
    {
        get
        {
            return firstName;
        }
        set
        {
            firstName = value;
        }
    }
    private string lastName;

    public string LastName
    {
        get
        {
            return lastName;
        }
        set
        {
            lastName = value;
        }
    }

    private DateTime createDate;

    public DateTime CreateDate
    {
        get
        {
            return createDate;
        }
        set
        {
            createDate = value;
        }
    }

    private DateTime destroyDate;

    public DateTime DestroyDate
    {
        get
        {
            return destroyDate;
        }
        set
        {
            destroyDate = value;
        }
    }

    private string uid;

    public string UID
    {
        get
        {
            return uid;
        }
        set
        {
            uid = value;
        }
    }
}
