﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using Shinetech.DAL;
using System.Configuration;

/// <summary>
/// Summary description for Sessions
/// </summary>
public class Sessions
{
    public Sessions()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static string UserId
    {
        get
        {
            try
            {
                if (HttpContext.Current.Session == null || HttpContext.Current.Session["UserId"] == null)
                {
                    return Membership.GetUser().ProviderUserKey.ToString();
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["UserId"]);
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        set
        {
            HttpContext.Current.Session["UserId"] = value;
        }
    }

    public static string RackSpaceUserID
    {
        get
        {
            try
            {
                if (HttpContext.Current.Session == null || HttpContext.Current.Session["UserId"] == null || HttpContext.Current.Session["UserId"].ToString() == string.Empty)
                {
                    Shinetech.DAL.Account account = new Shinetech.DAL.Account(Membership.GetUser().ProviderUserKey.ToString());
                    if (account.IsSubUser.ToString() == "1")
                        return account.OfficeUID.ToString();
                    else
                        return Membership.GetUser().ProviderUserKey.ToString();
                }
                else
                    return Convert.ToString(HttpContext.Current.Session["RackSpaceUserID"]);
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        set
        {
            HttpContext.Current.Session["RackSpaceUserID"] = value;
        }
    }

    public static string SwitchedEmailId 
    {
        get
        {
            try
            {
                if (HttpContext.Current.Session == null || HttpContext.Current.Session["SwitchedEmailId"] == null)
                {
                    return Membership.GetUser(Membership.GetUser().ProviderUserKey).Email.ToString();
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["SwitchedEmailId"]);
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        set
        {
            HttpContext.Current.Session["SwitchedEmailId"] = value;
        }
    }

    public static string SwitchedSessionId
    {
        get
        {
            try
            {
                if (HttpContext.Current.Session == null || HttpContext.Current.Session["SwitchedSessionId"] == null)
                {
                    return Membership.GetUser(Membership.GetUser().ProviderUserKey).ProviderUserKey.ToString();
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["SwitchedSessionId"]);
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        set
        {
            HttpContext.Current.Session["SwitchedSessionId"] = value;
        }
    }

    public static string SwitchedRackspaceId
    {
        get
        {
            try
            {
                if (HttpContext.Current.Session == null || HttpContext.Current.Session["SwitchedRackspaceId"] == null)
                {
                    Shinetech.DAL.Account account = new Shinetech.DAL.Account(Membership.GetUser().ProviderUserKey.ToString());
                    if (account.IsSubUser.ToString() == "1")
                        return account.OfficeUID.ToString();
                    else
                        return Membership.GetUser().ProviderUserKey.ToString();
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["SwitchedRackspaceId"]);
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        set
        {
            HttpContext.Current.Session["SwitchedRackspaceId"] = value;
        }
    }

    public static string SwitchedUserName
    {
        get
        {
            try
            {
                if (HttpContext.Current.Session == null || HttpContext.Current.Session["SwitchedUserName"] == null)
                {
                    return Membership.GetUser(Membership.GetUser().ProviderUserKey).UserName;
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["SwitchedUserName"]);
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        set
        {
            HttpContext.Current.Session["SwitchedUserName"] = value;
        }
    }

    public static string SwitchedRackspaceUserName
    {
        get
        {
            try
            {
                if (HttpContext.Current.Session == null || HttpContext.Current.Session["SwitchedRackspaceUserName"] == null)
                {
                    Shinetech.DAL.Account account = new Shinetech.DAL.Account(Sessions.SwitchedRackspaceId);
                    return Convert.ToString(account.Name);
                }
                else
                {
                    return Convert.ToString(HttpContext.Current.Session["SwitchedRackspaceUserName"]);
                }
            }
            catch (Exception ex)
            {
                return "";
            }
        }
        set
        {
            HttpContext.Current.Session["SwitchedRackspaceUserName"] = value;
        }
    }

    public static string UserName
    {
        get
        {
            if (HttpContext.Current.Session == null || HttpContext.Current.Session["UserName"] == null)
            {
                return Membership.GetUser(Membership.GetUser().ProviderUserKey).UserName;
            }
            else
            {
                return Convert.ToString(HttpContext.Current.Session["UserName"]);
            }
        }
        set
        {
            HttpContext.Current.Session["UserName"] = value;
        }
    }

    public static string SelectedPath
    {
        get
        {
            if (HttpContext.Current.Session == null || HttpContext.Current.Session["SelectedPath"] == null)
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToString(HttpContext.Current.Session["SelectedPath"]);
            }
        }
        set
        {
            HttpContext.Current.Session["SelectedPath"] = value;
        }
    }

    public static int SelectedTreeId
    {
        get
        {
            if (HttpContext.Current.Session == null || HttpContext.Current.Session["SelectedTreeId"] == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(HttpContext.Current.Session["SelectedTreeId"]);
            }
        }
        set
        {
            HttpContext.Current.Session["SelectedTreeId"] = value;
        }
    }

    public static int? DepartmentId
    {
        get
        {
            if (HttpContext.Current.Session == null || HttpContext.Current.Session["DepartmentId"] == null)
            {
                return null;
            }
            else
            {
                return Convert.ToInt32(HttpContext.Current.Session["DepartmentId"]);
            }
        }
        set
        {
            HttpContext.Current.Session["DepartmentId"] = value;
        }
    }

    public static int EdFormsUserId
    {
        get
        {
            if (HttpContext.Current.Session == null || HttpContext.Current.Session["EdFormsUserId"] == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(HttpContext.Current.Session["EdFormsUserId"]);
            }
        }
        set
        {
            HttpContext.Current.Session["EdFormsUserId"] = value;
        }
    }

    public static string EdFormsOfficeId
    {
        get
        {
            if (HttpContext.Current.Session == null || HttpContext.Current.Session["EdFormsOfficeId"] == null)
            {
                return string.Empty;
            }
            else
            {
                return Convert.ToString(HttpContext.Current.Session["EdFormsOfficeId"]);
            }
        }
        set
        {
            HttpContext.Current.Session["EdFormsOfficeId"] = value;
        }
    }

    public static int FolderId
    {
        get
        {
            if (HttpContext.Current.Session == null || HttpContext.Current.Session["EdForms_FolderId"] == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(HttpContext.Current.Session["EdForms_FolderId"]);
            }
        }
        set
        {
            HttpContext.Current.Session["EdForms_FolderId"] = value;
        }
    }

    public static int ExpirationTime
    {
        get
        {
            if (HttpContext.Current.Session == null || HttpContext.Current.Session["ExpirationTime"] == null)
            {
                int expiration = General_Class.GetExpirationTime(new Guid(SwitchedRackspaceId));
                if (expiration > 0)
                {
                    return expiration;
                }
                else
                {
                    return Convert.ToInt32(ConfigurationManager.AppSettings["ValidShareIdHours"]);
                }
            }
            else
            {
                return Convert.ToInt32(HttpContext.Current.Session["ExpirationTime"]);
            }
        }
        set { HttpContext.Current.Session["ExpirationTime"] = value; }
    }

    public static bool HasViewPrivilegeOnly
    {
        get
        {
            if (HttpContext.Current.Session == null || HttpContext.Current.Session["HasViewPrivilegeOnly"] == null)
            {
                return Common_Tatva.HasViewPrivilegeOnly();
            }
            else
            {
                return Convert.ToBoolean(HttpContext.Current.Session["HasViewPrivilegeOnly"]);
            }
        }
        set
        {
            HttpContext.Current.Session["HasViewPrivilegeOnly"] = value;
        }
    }

    public static bool HasPrevilegeToDelete
    {
        get
        {
            if (HttpContext.Current.Session == null || HttpContext.Current.Session["HasPrevilegeToDelete"] == null)
            {
                return Common_Tatva.HasPrivilegeToDelete();
            }
            else
            {
                return Convert.ToBoolean(HttpContext.Current.Session["HasPrevilegeToDelete"]);
            }
        }
        set
        {
            HttpContext.Current.Session["HasPrevilegeToDelete"] = value;
        }
    }

    public static bool HasPrevilegeToDownload
    {
        get
        {
            if (HttpContext.Current.Session == null || HttpContext.Current.Session["HasPrevilegeToDownload"] == null)
            {
                return Common_Tatva.HasPrivilegeToDownload();
            }
            else
            {
                return Convert.ToBoolean(HttpContext.Current.Session["HasPrevilegeToDownload"]);
            }
        }
        set
        {
            HttpContext.Current.Session["HasPrevilegeToDownload"] = value;
        }
    }

    public static string DividerIds
    {
        get
        {
            if (HttpContext.Current.Session == null || HttpContext.Current.Session["DividerIds"] == null)
            {
                return null;
            }
            else
            {
                return Convert.ToString(HttpContext.Current.Session["DividerIds"]);
            }
        }
        set
        {
            HttpContext.Current.Session["DividerIds"] = value;
        }
    }
}