using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


/// <summary>
/// Summary description for ShoppingCart
/// </summary>
[Serializable]
public class ShoppingCart
{
    public SortedList<int, CartItem> _CartItems = new SortedList<int, CartItem>();

    // Return all the items from the Shopping Cart
    public IList<CartItem> CartItems
    {
        get { return _CartItems.Values; }
    }

    // The sum total of the prices
    public decimal Total
    {
        get
        {
            decimal sum = 0;
            foreach (CartItem item in _CartItems.Values)
                sum += item.Price * item.Quantity;
            return sum;
        }
    }

    // Add a new item to the shopping cart
    public void AddItem(int ID, string Name, decimal Price)
    {
        CartItem item = _CartItems[ID];
        if (item == null)
            _CartItems.Add(ID, new CartItem(ID, Name, Price));
        else
        {
            item.Quantity++;
            _CartItems[ID] = item;
        }
    }

    // Remove an item from the shopping cart
    public void RemoveItem(int ID)
    {
        CartItem item = _CartItems[ID];
        if (item == null)
            return;
        item.Quantity--;
        if (item.Quantity == 0)
            _CartItems.Remove(ID);
        else
            _CartItems[ID] = item;
    }
}
