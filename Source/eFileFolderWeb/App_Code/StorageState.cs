using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.DAL;
using Account = Shinetech.DAL.Account;
/// <summary>
/// Summary description for StorageState
/// </summary>
public class StorageState
{
        private Account _item = null;
        private float _storagePrice;
        private float _totalsize = -1;
        private LicenseKit _kit;

    public StorageState(Account item, float price, LicenseKit kit)
        {
            _item = item;
            _kit = kit;
            _storagePrice = price;
        }

        public string CreditCard
        {
            get
            {
                return _item.CreditCard.Value.Equals(System.DBNull.Value) ? "" : _item.CreditCard.Value;
            }
        }

        public string CardType
        {
            get
            {
                return _item.CardType.Value.Equals(System.DBNull.Value) ? "" : _item.CardType.Value;
            }
        }

        public DateTime ExpirationDate
        {
            get
            {
                return _item.ExpirationDate.Value.Equals(System.DBNull.Value) ? DateTime.Now : (
                    DateTime)_item.ExpirationDate.Value;
            }
        }

        public double Amount
        {
            get
            {
                double perMonthCharge = _kit.MonthPricePerUser.Value;//每月必收费用
                int addUsers = SubUserCount - _kit.MaximalUserCount.Value;
                if(addUsers<=0)
                {
                    addUsers = 0;
                }
                double perMonthAddUsers = _kit.AdditionalUserPrice.Value * addUsers;//每月多余用户数费用
                double perMonthAddStorageCharge = 0;

                double varGb = this.Size / 1024f;
                double addStorage = (varGb - _kit.MaxStorageVolume.Value) / 5;
                if(addStorage<=0)
                {
                    addStorage = 0;
                }
                double addStorageCharge = Math.Ceiling(addStorage) * _kit.AdditionalStoragePrice.Value;

                return (double)(perMonthCharge + perMonthAddUsers + addStorageCharge);
            }
        }

        public float Price
        {
            get
            {
                return _storagePrice;
            }
        }

        public int SubUserCount
        {
            get
            {
                return UserManagement.GetWebUsersCount(_item.UID.Value);
            }
        }

        public float Size
        {
            get
            {
                if (_totalsize==-1)
                {
                    DataTable table = UserManagement.GetTotalSizeByUID(_item.UID.Value);
                    if (table==null || table.Rows.Count==0)
                    {
                        _totalsize = 0;
                    }
                    else
                    {
                        _totalsize = (float) Convert.ToDouble(table.Rows[0]["TotalSize"]);
                    }
                }

                return _totalsize;
            }
        }

        public string FirstName
        {
            get
            {
                return _item.Firstname.Value.Equals(System.DBNull.Value) ? "" : _item.Firstname.Value;
            }
        }

        public string LastName
        {
            get
            {
                return _item.Lastname.Value.Equals(System.DBNull.Value) ? "" : _item.Lastname.Value;
            }
        }

        public string UID
        {
            get
            {
                System.Guid uid = new Guid(_item.UID.Value);
                return (string)uid.ToString("D");
            }
        }

        public string EmailAddress
        {
            get
            {
                return _item.Email.Value.Equals(System.DBNull.Value) ? "" : _item.Email.Value;
            }
        }

        public string UserName
        {
            get
            {
                return _item.Name.Value.Equals(System.DBNull.Value) ? "" : _item.Name.Value;
            }
        }

        public string Address1
        {
            get
            {
                return _item.BillingAddress.Value.Equals(System.DBNull.Value) ? "" : _item.BillingAddress.Value;
            }
        }

        public string BillingCity
        {
            get
            {
                return _item.BillingCity.Value.Equals(System.DBNull.Value) ? "" : _item.BillingCity.Value;
            }
        }

        public string BillingState
        {
            get
            {
                if (_item.BillingOtherState.Value.Equals(System.DBNull.Value))
                {
                    return (string)_item.BillingStateId.Value;
                }
                else
                {
                    return (string)_item.BillingOtherState.Value;
                }
            }
        }

        public string BillingCountry
        {
            get
            {
                return (string)_item.BillingCountryId.Value;
            }
        }

        public string BillingPostal
        {
            get
            {
                return _item.BillingPostal.Value.Equals(System.DBNull.Value) ? "" : _item.BillingPostal.Value;
            }
        }

    }
