using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Collections;
using System.IO;
using System.Net;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;

using DataQuicker2.Framework;
using Shinetech.DAL;
using Shinetech.Utility;
using Account=Shinetech.DAL.Account;

/// <summary>
/// Summary description for StorageTransManage
/// </summary>
public class StorageTransManage
{
    const string mailTempletUrl = "MailContent.htm";
    protected const string strSubject = "Storage fee charge transaction from Electronic File Folders";

    public StorageTransManage()
    {
        
    }

    public static Hashtable ReadHtmlPage(Account item, double amount)
    {
        string x_login = WebConfigurationManager.AppSettings["x_login"];
        string x_tran_key = WebConfigurationManager.AppSettings["x_tran_key"];
        string transmodel = WebConfigurationManager.AppSettings["transmode"];
        string result = "";
        string strPost = "x_login={0}&x_tran_key={1}&x_method=CC" +
                "&x_type=AUTH_CAPTURE&x_amount={2}&x_delim_data=TRUE&x_delim_char=|&x_relay_response=FALSE" +
                "&x_card_num={3}&x_exp_date={4}&x_test_request={5}&x_version=3.1&x_Address={6}&x_City={7}&x_Country={8}" +
                "&x_State={9}&x_Zip={10}";
        StreamWriter myWriter = null;
        transmodel = transmodel.ToLower() == "test" ? "TRUE" : "FALSE";
        string strPost1 = "";
        string url = "https://test.authorize.net/gateway/transact.dll";
        if (transmodel == "TRUE")
        {
            strPost = "x_login={0}&x_tran_key={1}&x_method=CC" +
                      "&x_type=AUTH_CAPTURE&x_amount={2}&x_delim_data=TRUE&x_delim_char=|&x_relay_response=FALSE" +
                      "&x_card_num={3}&x_exp_date={4}&x_test_request={5}&x_version=3.1";
            strPost1 = string.Format(strPost, x_login, x_tran_key, amount,
                                     item.CreditCard.Value, item.ExpirationDate.Value.ToString("MMyyyy"), transmodel);
        }
        else
        {
            url = "https://secure.authorize.net/gateway/transact.dll";
            string stateId = item.BillingCountryId.Value != "US" ? item.BillingOtherState.Value : item.BillingStateId.Value;
            strPost1 = string.Format(strPost, x_login, x_tran_key, amount,
            item.CreditCard.Value, item.ExpirationDate.Value.ToString("MMyyyy"), transmodel,
            item.BillingAddress.Value, item.BillingCity.Value, item.BillingCountryId.Value, stateId, item.BillingPostal.Value);
        }
        HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url);
        objRequest.Method = "POST";
        objRequest.ContentLength = strPost1.Length;
        objRequest.ContentType = "application/x-www-form-urlencoded";

        try
        {
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(strPost1);
        }
        catch (Exception e)
        {
            return null ;
        }
        finally
        {
            myWriter.Close();
        }

        HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
        using (StreamReader sr =
           new StreamReader(objResponse.GetResponseStream()))
        {
            result = sr.ReadToEnd();
            sr.Close();
        }

        string[] output = result.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
        int counter = 0;

        Hashtable vars = new Hashtable();
        foreach (string var in output)
        {
            vars.Add(counter, var);
            counter += 1;
        }

        if (vars.Count < 7)
        {
            return null;
        }

        return vars;
    }

    public static float GetStorageFeePrice()
    {
        StorageFee fee = new StorageFee();
        ObjectQuery query = fee.CreateQuery();
        query.TopN = 1;

        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        DataTable table = new DataTable();
        try
        {
            query.Fill(table, connection);
            double var = (double)table.Rows[0]["Price"];
            float price = (float)var;

            return price;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public static void InsertEFFTransaction(Account item, int quantity, string responseCode, string responseDescription, string responseAuthCode,
                              string responseAVSCode, string responseReferenceCode,int partnerId)
    {
        EFFTransaction trans = new EFFTransaction();
        trans.LicenseID.Value = item.LicenseID.Value;
        trans.UserID.Value = item.UID.Value;
        trans.OfficeID.Value = item.OfficeUID.Value;
        trans.FirstName.Value = item.Firstname.Value;
        trans.LastName.Value = item.Lastname.Value;
        trans.CardType.Value = item.CardType.Value;
        trans.CardNumber.Value = item.CreditCard.Value;
        trans.ExpiredDate.Value = item.ExpirationDate.Value;
        trans.CompanyName.Value = item.CompanyName.Value;
        trans.Address.Value = item.BillingAddress.Value;
        trans.City.Value = item.BillingCity.Value;
        trans.State.Value = item.BillingOtherState.Value == "" ? item.BillingStateId.Value: item.BillingOtherState.Value;
        trans.Country.Value = item.BillingCountryId.Value;
        trans.Postal.Value = item.BillingPostal.Value;
        trans.Email.Value = item.Email.Value;
        trans.Telephone.Value = item.Telephone.Value;
        trans.Amount.Value = quantity;
        trans.ResponseCode.Value = responseCode;
        trans.ResponseDescription.Value = responseDescription;
        trans.ResponseAuthCode.Value = responseAuthCode;
        trans.ResponseAVSCode.Value = responseAVSCode;
        trans.ResponseReferenceCode.Value = responseReferenceCode;

        trans.PartnerID.Value = partnerId;

        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        try
        {
            trans.Create(connection);
        }
        catch
        { }
    }

    public static void InsertEFFTransactionKit(Account item, double amount,string folderPriceId,bool availableEFF, string responseCode, string responseDescription, string responseAuthCode,
                              string responseAVSCode, string responseReferenceCode, int partnerId)
    {
        EFFTransaction trans = new EFFTransaction();
        trans.LicenseID.Value = item.LicenseID.Value;
        trans.UserID.Value = item.UID.Value;
        trans.OfficeID.Value = item.OfficeUID.Value;
        trans.FirstName.Value = item.Firstname.Value;
        trans.LastName.Value = item.Lastname.Value;
        trans.CardType.Value = item.CardType.Value;
        trans.CardNumber.Value = item.CreditCard.Value;
        trans.ExpiredDate.Value = item.ExpirationDate.Value;
        trans.CompanyName.Value = item.CompanyName.Value;
        trans.Address.Value = item.BillingAddress.Value;
        trans.City.Value = item.BillingCity.Value;
        trans.State.Value = item.BillingOtherState.Value == "" ? item.BillingStateId.Value : item.BillingOtherState.Value;
        trans.Country.Value = item.BillingCountryId.Value;
        trans.Postal.Value = item.BillingPostal.Value;
        trans.Email.Value = item.Email.Value;
        trans.Telephone.Value = item.Telephone.Value;
        trans.Amount.Value = amount;//money for trans
        trans.ResponseCode.Value = responseCode;
        trans.ResponseDescription.Value = responseDescription;
        trans.ResponseAuthCode.Value = responseAuthCode;
        trans.ResponseAVSCode.Value = responseAVSCode;
        trans.ResponseReferenceCode.Value = responseReferenceCode;

        trans.PartnerID.Value = partnerId;

        trans.FolderPriceId.Value = folderPriceId;

        trans.AvailableEFF.Value = availableEFF;


        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        try
        {
            trans.Create(connection);
        }
        catch
        { }
    }

    public static void SendEmail(StorageTransaction result, Account account,string path)
    {
        if (result.ResponseCode.Value == "-1" || result.EmailAddress.Value == "") return;

        string strMailTemplet = getMailTempletStr(path);
        strMailTemplet = strMailTemplet.Replace("[username]", result.UserName.Value);

        strMailTemplet = strMailTemplet.Replace("[transid]", result.ResponseReferenceCode.Value);
        strMailTemplet = strMailTemplet.Replace("[Amount]", string.Format("{0:F2}",result.Amount.Value));
        strMailTemplet = strMailTemplet.Replace("[TotalSize]", string.Format("{0:F2}",result.Size.Value));
        strMailTemplet = strMailTemplet.Replace("[TransactionDate]", result.ExchangeDate.Value.ToString());

        strMailTemplet = strMailTemplet.Replace("[CardType]", result.CardType.Value);
        char[] xxCardNo = result.CardNumber.Value.ToCharArray();
        int cnt = xxCardNo.Length;
        if (cnt > 4)
        {
            for (int i = 0; i < cnt - 4; i++)
            {
                xxCardNo[i] = 'x';
            }
        }
        strMailTemplet = strMailTemplet.Replace("[CardNo]", new string(xxCardNo));
        result.ResponseDescription.Value = result.ResponseDescription.Value.Replace("has been", "was");
        strMailTemplet = strMailTemplet.Replace("[TransactionResult]", result.ResponseDescription.Value);

        string transStatus = result.ResponseCode.Value == "1" ? "successful" : "failed";
        strMailTemplet = strMailTemplet.Replace("[ResponseCode]", transStatus);

        Email.SendFromEFFService(result.EmailAddress.Value, strSubject, strMailTemplet);
    }

    public static string getMailTempletStr(string path)
    {
        string strMailTemplet = string.Empty;
        try
        {
            if(path.EndsWith("\\"))
            {
                path += mailTempletUrl;
            }
            else
            {
                path += "\\"+mailTempletUrl;
            }
            StreamReader sr = new StreamReader(path);
            string sLine = "";
            while (sLine != null)
            {
                sLine = sr.ReadLine();
                if (sLine != null)
                    strMailTemplet += sLine;
            }
            sr.Close();
        }
        catch { }

        return strMailTemplet;
    }
}
