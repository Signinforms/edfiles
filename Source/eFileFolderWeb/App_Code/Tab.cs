﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
///Tab 的摘要说明
/// </summary>
public class Tab
{
    public string Color { get; set; }
    public string Name { get; set; }
    public int Id { get; set; }
    public bool Locked { get; set; }
}