using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for DividerEntity
/// </summary>
[Serializable]
public class TemplateEntity
{
    public TemplateEntity(string name)
    {
        _TemplateID = -1;
        _Name = name;
    }

    private int _TemplateID;
    [Bindable(true)]
    public int TemplateID
    {
        get { return _TemplateID; }

        set { _TemplateID= value; }
    }

    private string _Name;
    [Bindable(true)]
    public string Name
    {
        get { return _Name; }

        set { _Name=value; }
    }

    private string _OfficeID;
    [Bindable(true)]
    public string OfficeID
    {
        get { return _OfficeID; }

        set { _OfficeID=value; }
    }

    private bool _IsLocked;
    [Bindable(true)]
    public bool IsLocked
    {
        get { return _IsLocked; }

        set { _IsLocked = value; }
    }

    private string _content;
    [Bindable(true)]
    public string Content
    {
        get { return _content; }

        set
        {
            _dividers.Clear();
            _content = value;

            string[] items = _content.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            foreach(string divider in items)
            {
                string[] dcs = divider.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                DividerEntity entity = new DividerEntity(dcs[0]);
                entity.Color = dcs[1];
                _dividers.Add(entity);
            }
        }
    }

    List<DividerEntity> _dividers = new List<DividerEntity>();

    public  List<DividerEntity> Dividers
    {
        get
        {
            return _dividers;
           
        }
    }

}
