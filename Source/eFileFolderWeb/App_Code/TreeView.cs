﻿using net.openstack.Core.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using Shinetech.DAL;

#region TreeView
/// <summary>
/// Summary description for TreeView
/// </summary>
public class TreeView
{
    public TreeView()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public DataTable GetTable()
    {
        DataTable table = new DataTable("RackSpaceObject");
        table.Columns.Add("ObjectID", typeof(int));
        table.Columns.Add("ObjectName", typeof(string));
        table.Columns.Add("Level", typeof(int));
        table.Columns.Add("ParentID", typeof(int));
        table.Columns.Add("Path", typeof(string));
        table.Columns.Add("Size", typeof(decimal));
        table.Columns.Add("ModifiedDate", typeof(string));

        return table;
    }

    /// <summary>
    /// Returns json string to create folder treeview
    /// </summary>
    /// <param name="containerName">Name of container to display treeview for</param>
    /// <returns>Returns json string to create folder treeview</returns>
    public string GetJsonDataForTree(string containerName)
    {
        try
        {
            List<RackSpaceObject> objectList = GetWorkAreaTreeList(containerName);
            //Converts list to json string
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string jsonData = serializer.Serialize(objectList);
            return jsonData;
        }
        catch (Exception ex)
        {
            List<RackSpaceObject> objectList = new List<RackSpaceObject>();
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            return serializer.Serialize(objectList);
        }
    }

    /// <summary>
    /// Returns json string to create folder treeview
    /// </summary>
    /// <param name="containerName">Name of container to display treeview for</param>
    /// <returns>Returns json string to create folder treeview</returns>
    public string GetJsonDataForArchiveTree(string containerName)
    {
        try
        {
            IEnumerable<ArchiveObject> objectList = GetArchiveTreeList(containerName);
            //Converts list to json string
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string jsonData = serializer.Serialize(objectList);
            return jsonData;
        }
        catch (Exception ex)
        {
            List<ArchiveObject> objectList = new List<ArchiveObject>();
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            return serializer.Serialize(objectList);
        }
    }

    public IEnumerable<ArchiveObject> GetArchiveTreeList(string containerName)
    {

        List<ArchiveObject> objectList = new List<ArchiveObject>();
        DataTable archiveTreeList = General_Class.GetArchiveTreeList(containerName);
        objectList.Add(new ArchiveObject()
        {
            ObjectID = 0,
            ObjectName = "Archive",
            Level = 0,
            ParentID = 0,
            Path = string.Empty,
            Size = 0,
            ModifiedDate = string.Empty,
        });
        objectList = (from DataRow dr in archiveTreeList.Rows
                      select new ArchiveObject()
                      {
                          ObjectID = Convert.ToInt32(dr["FolderId"]),
                          ObjectName = dr["FolderName"].ToString(),
                          Level = string.IsNullOrEmpty(Convert.ToString(dr["Level"])) ? 0 : Convert.ToInt32(Convert.ToString(dr["Level"])),
                          ParentID = string.IsNullOrEmpty(Convert.ToString(dr["ParentId"])) ? 0 : Convert.ToInt32(Convert.ToString(dr["ParentId"])),
                          Path = string.Empty,
                          Size = 0,
                          ModifiedDate = string.Empty,
                      }).ToList();
        return objectList;
    }

    public List<RackSpaceObject> GetWorkAreaTreeList(string containerName)
    {

        List<RackSpaceObject> objectList = new List<RackSpaceObject>();
        DataTable archiveTreeList = General_Class.GetWorkAreaTreeList(containerName);
        objectList = (from DataRow dr in archiveTreeList.Rows
                      select new RackSpaceObject()
                      {
                          ObjectID = Convert.ToInt32(dr["WorkAreaTreeId"]),
                          ObjectName = dr["FolderName"].ToString(),
                          Level = string.IsNullOrEmpty(Convert.ToString(dr["Level"])) ? 0 : Convert.ToInt32(Convert.ToString(dr["Level"])),
                          ParentID = string.IsNullOrEmpty(Convert.ToString(dr["ParentId"])) ? 0 : Convert.ToInt32(Convert.ToString(dr["ParentId"])),
                          Path = dr["Path"].ToString(),
                          Size = 0,
                          ModifiedDate = string.Empty,
                      }).ToList();
        return objectList;
    }

    public List<RackSpaceObject> GetWorkareList(string containerName, string matchExp = "")
    {
        RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
        IEnumerable<ContainerObject> containerObjectList;
        matchExp = matchExp.Replace("WorkArea", "workArea");
        if (!string.IsNullOrEmpty(matchExp))
            containerObjectList = rackSpaceFileUpload.GetObjectList(containerName, matchExp);
        else
            containerObjectList = rackSpaceFileUpload.GetObjectList(containerName, "workArea");
        DataTable cloudFileTable = GetTable();
        int id = 1, parentID = 0;
        string mainFolderName = string.Empty;
        if (containerObjectList.ToArray<ContainerObject>().Length > 0)
            mainFolderName = containerObjectList.ToArray<ContainerObject>()[0].Name.Split('/')[0];

        foreach (ContainerObject containerObj in containerObjectList)
        {
            if (string.IsNullOrEmpty(matchExp) || (!string.IsNullOrEmpty(matchExp) && containerObj.Name.IndexOf(matchExp) >= 0))
            {
                string[] objectNames = containerObj.Name.Split('/');
                if (!string.Equals(mainFolderName, objectNames[0]))
                {
                    objectNames[0] = objectNames[0].Replace("workArea", "WorkArea");
                    mainFolderName = objectNames[0];
                    parentID = 0;
                }
                DataRow[] foundRows;
                bool isBreak = false;
                int objectid = 0; //ID of matched object from datatable
                for (int i = 0; i < objectNames.Length; i++)
                {
                    objectNames[i] = objectNames[i].Replace("workArea", "WorkArea");
                    if (cloudFileTable.AsEnumerable().Any(row => objectNames[i] == row.Field<string>("ObjectName")))
                    {

                        foundRows = cloudFileTable.Select("ObjectName = '" + objectNames[i] + "'"); //Returns rows of same object name as current
                        objectid = foundRows[foundRows.Length - 1].Field<int>("ObjectID"); //Returns id of last matched row from above rows

                        //Iterate to next level if it is root level
                        if (foundRows[foundRows.Length - 1].Field<int>("Level") == 0)
                            continue;

                        DataRow[] matchedRows = cloudFileTable.Select("ObjectID = '" + (objectid - 1) + "'"); //Finds parent of current object
                        DataRow[] rows = null;
                        try
                        {
                            rows = cloudFileTable.Select("ObjectName = '" + objectNames[i - 1] + "'");
                        }
                        catch (Exception ex)
                        {
                            string er = string.Empty;
                        }

                        if (matchedRows.Length > 0 && rows.Length > 0 && matchedRows[matchedRows.Length - 1].Field<string>("ObjectName") == rows[rows.Length - 1].Field<string>("ObjectName"))
                        {
                            //break;
                            do
                            {
                                if (objectid - 1 >= 0)
                                {
                                    matchedRows = cloudFileTable.Select("ObjectID = '" + (objectid - 1) + "'"); //Finds parent of current object
                                    if (matchedRows.Length > 0 && matchedRows[0].Field<int>("Level") == 0)
                                    {
                                        if (matchedRows[matchedRows.Length - 1].Field<string>("ObjectName") == mainFolderName)
                                        {
                                            isBreak = true;
                                            break;
                                        }
                                    }

                                }
                                objectid = objectid - 1;
                            } while (objectid >= 0);
                        }
                        else if (foundRows[foundRows.Length - 1].Field<int>("ParentID") == rows[rows.Length - 1].Field<int>("ObjectID"))
                        {
                            continue;
                        }

                        //Continue if object is already inserted in datatable
                        if (isBreak || i == 0)
                        {
                            isBreak = false;
                            continue;
                        }
                    }

                    string path = string.Empty;
                    if (parentID == 0)
                    {
                        path += objectNames[i];
                    }
                    //To get parent ID
                    int preObjIndex = i - 1;
                    decimal size = 0;
                    string modifiedDate = string.Empty;
                    if (preObjIndex >= 0)
                    {
                        if (i == 0)
                            continue;

                        foundRows = cloudFileTable.Select("ObjectName = '" + objectNames[preObjIndex] + "'"); //Returns parent row
                        parentID = foundRows[foundRows.Length - 1].Field<int>("ObjectID"); //Parent ID to store in datatable
                        path = foundRows[foundRows.Length - 1].Field<string>("Path") + "/" + objectNames[i].Replace("\"", "%22").Replace("'", "%27");
                        //path = foundRows[foundRows.Length - 1].Field<string>("Path") + "/" + objectNames[i];
                        size = Math.Round((decimal)(containerObj.Bytes / 1024) / 1024, 2);
                        modifiedDate = containerObj.LastModified.DateTime.ToString();
                    }
                    if (string.IsNullOrEmpty(modifiedDate))
                        modifiedDate = DateTime.UtcNow.AddHours(24).ToString();
                    cloudFileTable.Rows.Add(id++, objectNames[i].Replace("\"", "%22").Replace("'", "%27"), i, parentID++, path, size, modifiedDate);
                }
            }
        }

        //Converts datatable to list
        List<RackSpaceObject> objectList = new List<RackSpaceObject>();
        objectList = (from DataRow dr in cloudFileTable.Rows
                      select new RackSpaceObject()
                      {
                          ObjectID = Convert.ToInt32(dr["ObjectID"]),
                          ObjectName = dr["ObjectName"].ToString(),
                          Level = Convert.ToInt32(dr["Level"]),
                          ParentID = Convert.ToInt32(dr["ParentID"]),
                          Path = dr["Path"].ToString(),
                          Size = Convert.ToDecimal(dr["Size"]),
                          ModifiedDate = dr["ModifiedDate"].ToString(),
                      }).ToList();

        objectList.Sort((x, y) => DateTime.Parse(y.ModifiedDate).CompareTo(DateTime.Parse(x.ModifiedDate)));
        return objectList;
    }
}
#endregion


#region RackSpaceObject
public class RackSpaceObject
{
    public int ObjectID { get; set; }
    public string ObjectName { get; set; }
    public int Level { get; set; }
    public int ParentID { get; set; }
    public string Path { get; set; }
    public decimal Size { get; set; }
    public string ModifiedDate { get; set; }
}
#endregion

#region Archive
public class ArchiveObject
{
    public int ObjectID { get; set; }
    public string ObjectName { get; set; }
    public int Level { get; set; }
    public int? ParentID { get; set; }
    public string Path { get; set; }
    public decimal Size { get; set; }
    public string ModifiedDate { get; set; }
}
#endregion