using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Web.Profile;
using System.Web.Security;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using DataQuicker2.Framework;
using Shinetech.DAL;
using Shinetech.Utility;
using Account = Shinetech.DAL.Account;

public class UserManagement
{
    public static string ConnectionName = "Default";
    private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;

    public UserManagement()
    {
    }

    public static List<UserBase> GetWebUserList()
    {
        string[] offices = Roles.GetUsersInRole("Users");
        int total = 0;
        List<UserBase> list = new List<UserBase>();

        foreach (string userId in offices)
        {
            UserProfile profile = UserProfile.GetUserProfile(userId);
            WebUser user = BuildWebUser(profile);
            list.Add(user);
            total++;
        }

        if ((list.Count == 0) || (total == 0))
        {
            return null;
        }


        return list;
    }

    public static Account GetWebUser(string userId)
    {
        Account user = new Account(userId);
        if (user.IsExist)
        {
            return user;
        }
        else
        {
            return null;
        }
    }

    public static ArrayList GetSubUserList(string officeId)
    {
        Account user = new Account();

        ObjectQuery query = user.CreateQuery();

        query.SetSelectFields(new IColumn[] { user.UID });
        query.SetCriteria(user.IsSubUser, true);
        query.SetCriteria(user.RoleID, "Users");
        query.SetCriteria(user.OfficeUID, officeId);

        DataTable dt = new DataTable();
        ArrayList list = new ArrayList();
        try
        {
            query.Fill(dt);

            foreach (DataRow row in dt.Rows)
            {
                list.Add(Convert.ToString(row[0]));
            }
        }
        catch (Exception exp)
        {
        }

        return list;
    }

    public static DataTable GetWebUserList(string officeId)
    {
        Account user = new Account();
        Aspnet_Membership membership = new Aspnet_Membership();
        TabAccessRestriction tabs = new TabAccessRestriction();
        ObjectQuery query = user.CreateQuery();
        query.SetAssociation(new IColumn[] { user.UID }, new IColumn[] { membership.UserId }, AssociationType.LeftJoin);
        query.SetAssociation(new IColumn[] { user.UID }, new IColumn[] { tabs.UID }, AssociationType.LeftJoin);
        query.SetSelectFields(new IColumn[] { user.UID, user.Name, user.Firstname, user.Lastname, 
            user.DOB,user.Telephone,user.Email,user.MobilePhone,user.FaxNumber,user.CompanyName,
            user.Address, user.RoleID,membership.IsApproved, user.IsDisabled,membership.LastLoginDate, membership.CreateDate,user.OtherInfo1,
            user.OtherInfo2,user.HasPrivilegeDelete, user.HasPrivilegeDownload, user.HasProtectedSecurity, user.HasPublicSecurity, user.City,user.StateID,user.OtherState,user.CountryID,user.Postal,user.EnableChargeFlag, tabs.IsHideLockedTabs, user.ViewPrivilege });
        query.SetCriteria(user.IsSubUser, true);
        query.SetCriteria(user.RoleID, "Users");
        query.SetCriteria(user.OfficeUID, officeId);

        DataTable dt = new DataTable();
        try
        {
            query.Fill(dt);
        }
        catch (Exception exp)
        {
        }

        return dt;
    }

    public static int GetWebUsersCount(string uid)
    {
        string SQL = "SELECT (*) AS CNT FROM Account WHERE OfficeUID ={0}";

        SQL = string.Format(SQL, uid);

        try
        {
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            return Convert.ToInt32(sqlH.ExecuteScalar(SQL));

        }
        catch (Exception)
        {
            return 1;
        }

    }

    public static DataTable GetGiftUsers()
    {

        DataTable dt = new DataTable();
        try
        {
            string sqlstr = "SELECT b.UID,b.Name,b.Firstname,b.Lastname,b.Email,b.Telephone FROM Account b ";
            sqlstr += "WHERE b.UID IN (SELECT a.UserID FROM Gift a WHERE a.UserID IS NOT NULL GROUP BY a.UserID)";
            SqlHelper sqlH = new SqlHelper(ConnectionName);

            sqlH.Fill(dt, sqlstr);
        }
        catch (Exception exp)
        {
        }

        return dt;

    }

    public static string GetPassword(string strUserName)
    {
        string strPassword;
        string SQL = "select password from aspnet_Membership where UserID = (select UserID from aspnet_Users where UserName = '";
        SQL += strUserName;
        SQL += "')";
        SqlHelper sqlH = new SqlHelper(ConnectionName);
        strPassword = Convert.ToString(sqlH.ExecuteScalar(SQL));
        return strPassword;
    }

    public static string GetDefaultPassword(string name)
    {
        string strPassword;
        string SQL = "select password from Password where Name = '";
        SQL += name;
        SQL += "'";
        SqlHelper sqlH = new SqlHelper(ConnectionName);
        strPassword = Convert.ToString(sqlH.ExecuteScalar(SQL));
        return strPassword;
    }

    public static Boolean IsApproved(string strUserName)
    {
        Boolean flag;
        string SQL = "select IsApproved from aspnet_Membership where UserID = (select UserID from aspnet_Users where UserName = '";
        SQL += strUserName;
        SQL += "')";
        SqlHelper sqlH = new SqlHelper(ConnectionName);
        flag = Convert.ToBoolean(sqlH.ExecuteScalar(SQL));
        return flag;
    }


    public static DataTable getUserInfor(string strUserName)
    {
        DataTable userInfor = new DataTable();
        string SQL = "select t1.Name,t2.password,t1.DOB,t1.Email,t1.IsSubUser from account t1 left join aspnet_membership t2 on t1.UID = t2.userID where t1.UID = (select UserID from aspnet_Users where userName = '";
        SQL += strUserName;
        SQL += "') ";
        SqlHelper sqlH = new SqlHelper(ConnectionName);

        sqlH.Fill(userInfor, SQL);
        return userInfor;
    }
    public static void ChangePassword(string strNewPassword, string strUserName)
    {
        string SQL = "update aspnet_Membership set password = '";
        SQL += strNewPassword;
        SQL += "'";
        SQL += " where UserId = (select userid from dbo.aspnet_Users where userName = '";
        SQL += strUserName;
        SQL += "') ";

        SqlHelper sqlH = new SqlHelper(ConnectionName);
        sqlH.ExecuteNonQuery(SQL);
    }

    public static void UpdatePassword(string newPassword, string uid)
    {
        string SQL = "update aspnet_Membership set password = '";
        SQL += newPassword;
        SQL += "'";
        SQL += " where UserId = '";
        SQL += uid;
        SQL += "'";

        SqlHelper sqlH = new SqlHelper(ConnectionName);
        sqlH.ExecuteNonQuery(SQL);
    }

    public static DataTable getUserIDByUserName(string strUserName)
    {
        Account user = new Account();
        ObjectQuery query = user.CreateQuery();

        query.SetSelectFields(new IColumn[] { user.UID });
        query.SetCriteria(user.Name, strUserName);

        try
        {
            DataTable dt = new DataTable();
            query.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static string ExistUserName(string strUserName)
    {
        Account user = new Account();
        ObjectQuery query = user.CreateQuery();

        query.SetSelectFields(new IColumn[] { user.UID });
        query.SetCriteria(user.Name, strUserName);

        try
        {
            DataTable dt = new DataTable();
            query.Fill(dt);
            if (dt.Rows.Count == 0) return null;

            return dt.Rows[0][0].ToString();
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static DataTable ExistUserName1(string strUserName)
    {
        Account user = new Account();
        ObjectQuery query = user.CreateQuery();

        query.SetSelectFields(new IColumn[] { user.UID, user.OfficeUID, user.IsSubUser });
        query.SetCriteria(user.Name, strUserName);

        try
        {
            DataTable dt = new DataTable();
            query.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static DataTable GetOfficeUserList()
    {
        Account user = new Account();
        Aspnet_Membership membership = new Aspnet_Membership();
        TabAccessRestriction tabs = new TabAccessRestriction();
        ObjectQuery query = user.CreateQuery();
        query.SetAssociation(new IColumn[] { user.UID }, new IColumn[] { membership.UserId }, AssociationType.LeftJoin);
        query.SetAssociation(new IColumn[] { user.UID }, new IColumn[] { tabs.UID }, AssociationType.LeftJoin);
        query.SetSelectFields(new IColumn[] { user.UID, user.Name, user.Firstname, user.Lastname, 
            user.DOB,user.Telephone,user.Email,user.MobilePhone,user.FaxNumber,user.CompanyName,
            user.Address, user.RoleID,membership.IsApproved,membership.LastLoginDate, membership.CreateDate, user.OtherInfo1,
            user.OtherInfo2,user.HasPrivilegeDelete, user.HasPrivilegeDownload,user.City,user.StateID,user.OtherState,user.CountryID,user.Postal,user.EnableChargeFlag, tabs.IsHideLockedTabs });
        query.SetCriteria(user.IsSubUser, false);
        query.SetCriteria(user.RoleID, "Offices");

        query.SetOrderBy(membership.CreateDate, false);

        DataTable dt = new DataTable();
        try
        {
            query.Fill(dt);
        }
        catch (Exception exp)
        {
        }

        return dt;
    }

    public static DataTable GetOfficeUsers()
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_GetOfficeUsers";
            command.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetOfficeSubUsersForAdmin()
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_GetOfficeUsersForAdmin";
            command.CommandType = CommandType.StoredProcedure;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetParterList()
    {
        Partner user = new Partner();

        ObjectQuery query = user.CreateQuery();

        DataTable dt = new DataTable();
        try
        {
            query.Fill(dt);
        }
        catch (Exception exp)
        {
        }

        return dt;
    }


    public static void UpdateTempAccount(string UserID, string UserName, string Password)
    {

        string SQL = "INSERT tempAccount(UserID,USerName,Password) VALUES ('";
        SQL += UserID;
        SQL += "','";
        SQL += UserName;
        SQL += "','";
        SQL += Password;
        SQL += "')";

        SqlHelper sqlH = new SqlHelper(ConnectionName);
        sqlH.ExecuteNonQuery(SQL);
    }

    public static void DeleteTempAccount(string UserName)
    {
        string SQL = "DELETE tempAccount WHERE UserName = '";
        SQL += UserName;
        SQL += "'";

        SqlHelper sqlH = new SqlHelper(ConnectionName);
        sqlH.ExecuteNonQuery(SQL);
    }

    public static string GetPasswordFromTempAccount(string UserName)
    {
        string strPassword;
        string SQL = "SELECT Password FROM tempAccount WHERE UserName = '";
        SQL += UserName;
        SQL += "'";

        SqlHelper sqlH = new SqlHelper(ConnectionName);
        strPassword = Convert.ToString(sqlH.ExecuteScalar(SQL));
        return strPassword;
    }

    public static DataTable IsApprovedInTemp(string strUserName)
    {
        string SQL = "select UserName from TempAccount where UserName = '";
        SQL += strUserName;
        SQL += "'";
        DataTable dt = new DataTable();
        SqlHelper sqlH = new SqlHelper(ConnectionName);
        sqlH.Fill(dt, SQL);
        return dt;
    }

    public static DataTable GetTotalSizeByUID(string uid)
    {
        string SQL = "SELECT  OfficeID1, SUM(OtherInfo2) AS TotalSize" +
                     " FROM dbo.FileFolder WHERE OfficeID1='" + uid + "' GROUP BY OfficeID1";

        DataTable dt = new DataTable();
        SqlHelper sqlH = new SqlHelper(ConnectionName);
        sqlH.Fill(dt, SQL);
        return dt;
    }

    public static Int32 GetTotalFolderNumberByUID(string uid)
    {
        string SQL = "SELECT   COUNT(FolderID) AS FN" +
                     " FROM dbo.FileFolder WHERE OfficeID='" + uid + "'";

        SqlHelper sqlH = new SqlHelper(ConnectionName);
        Int32 FN = Convert.ToInt32(sqlH.ExecuteScalar(SQL));
        return FN;
    }

    public static Int32 GetTotalBoxNumberByUID(string uid)
    {
        string SQL = "SELECT   COUNT(BoxId) AS BN" +
                     " FROM dbo.Box WHERE UserId='" + uid + "'";

        SqlHelper sqlH = new SqlHelper(ConnectionName);
        Int32 bN = Convert.ToInt32(sqlH.ExecuteScalar(SQL));
        return bN;
    }

    public static bool DeleteWebUser(int userId)
    {

        return false;
    }

    public static bool UpdateWebUser(int userId, string lastName, string firstName, DateTime birthDate, string phone)
    {

        return false;
    }

    public bool UpdateOfficeUser(string userID, string company, string contact, string title)
    {


        return false;
    }

    public static bool DeleteOfficeUser(string officeID)
    {

        return false;
    }


    private static WebUser BuildWebUser(UserProfile profile)
    {
        WebUser webuser = new WebUser();
        webuser.ID = profile.UserName;
        return webuser;
    }

    private static OfficeUser BuildOfficeUser(UserProfile profile)
    {
        OfficeUser office = new OfficeUser();

        /*office.ID = profile.UserName;
        office.Firstname = profile.Firstname;
        office.Lastname = profile.Lastname;
        office.DOB = profile.DOB;
        office.OtherInfo1 = profile.OtherInfo1;
        office.OtherInfo2 = profile.OtherInfo2;*/

        return office;
    }


    private static IDbConnection CreateConnection()
    {
        IDbConnection cnn = new System.Data.SqlClient.SqlConnection(ConnectionString);
        cnn.Open();

        return cnn;
    }

    public static void CreatNewAccount(MembershipUser user, DateTime date, bool isSubuser)
    {
        Account webUser = new Account(user.ProviderUserKey.ToString());
        webUser.Name.Value = user.UserName;
        webUser.ApplicationName.Value = Membership.ApplicationName;
        webUser.IsSubUser.Value = isSubuser ? "1" : "0";
        webUser.DOB.Value = date;

        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();

        try
        {
            webUser.Create(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }


    }

    public static void CreatNewAccount(Account user)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            user.Create(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }

    }

    public static void CreateNewPartner(Partner partner)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            partner.Create(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }
    }

    public static void CreatVerificationNumber(VerificationNumber number)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            number.Create(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }

    }

    public static void UpdateVerificationNumber(VerificationNumber number)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            number.Update(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }

    }

    public static void UpdateUser(Account acount)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            acount.Update(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }

    }

    public static void UpdatePartner(Partner partner)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            partner.Update(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }

    }

    //modify by johnlu655 2010-02-03, remove the license class
    // Abandoned in 2010-02-09
    public static void UpdateUserLicense(Account acount, LicenseKit license)
    {
        //to do
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            if (acount.IsSubUser.Value == "1")
            {
                acount = new Account(acount.OfficeUID.Value);
                //office.FileFolders.Value += license.Quantity.Value;
                //office.Update(transaction);
            }
            //else if (acount.LicenseID.Value==-1)//trial user, then give this man start kit
            //{
            //    //acount.FileFolders.Value = license.Quantity.Value;
            //    acount.LicenseID.Value = license.UID.Value;//100, or 200
            //}
            //else //normal people then increase only
            //{
            //    //acount.FileFolders.Value += license.Quantity.Value;
            //}
            acount.LicenseID.Value = license.LicenseKitID.Value;
            acount.Update(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }

    }

    public static DataTable GetStates()
    {
        State state = new State();

        ObjectQuery query = state.CreateQuery();

        try
        {
            DataTable dt = new DataTable();
            query.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            return null;
        }
    }


    public static DataTable GetCountries()
    {
        Country country = new Country();

        ObjectQuery query = country.CreateQuery();
        query.SetOrderBy(country.CountryName);
        try
        {
            DataTable dt = new DataTable();
            query.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static DataTable GetOfficeUserIDBySubUserID(string strSubUserID)
    {
        Account user = new Account();
        ObjectQuery query = user.CreateQuery();
        ObjectQuery query1 = user.CreateQuery();

        query.SetSelectFields(new IColumn[] { user.OfficeUID });
        query1.SetSelectFields(new IColumn[] { user.Name });
        query.SetCriteria(user.UID, strSubUserID);

        try
        {
            DataTable dt = new DataTable();
            query.Fill(dt);
            query1.SetCriteria(user.UID, dt.Rows[0]["OfficeUID"].ToString());
            query1.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static DataTable GetNumByOfficeID(string strOfficeID)
    {
        Account user = new Account();
        ObjectQuery query = user.CreateQuery();

        query.SetSelectFields(new IColumn[] { user.FileFolders });
        query.SetCriteria(user.UID, strOfficeID);

        try
        {
            DataTable dt = new DataTable();
            query.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static DataTable GetUsers(string keyname)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SelectUsers";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@Key", keyname);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable SearchUsers(string keyname)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SearchUsers";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@Key", keyname);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static DataTable GetUsersByOffice(string keyname, string uid)
    {
        SqlConnection connection = new SqlConnection(ConnectionString);

        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            command.CommandText = "eFileFolder_SelectUsersByOffice";
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter param1 = command.Parameters.AddWithValue("@Key", keyname);
            param1.Direction = ParameterDirection.Input;
            param1.DbType = DbType.String;

            SqlParameter param2 = command.Parameters.AddWithValue("@UID", uid);
            param2.Direction = ParameterDirection.Input;
            param2.DbType = DbType.String;

            SqlDataAdapter dataAdapter = new SqlDataAdapter();
            dataAdapter.SelectCommand = command;

            DataTable table = new DataTable();

            dataAdapter.Fill(table);

            return table;
        }
        catch (Exception ex)
        {
            return null;
        }
        finally
        {
            connection.Close();
        }
    }

    public static bool LoginUser(string userid, string pwd)
    {
        string strUserName = userid;
        string strPassword = PasswordGenerator.GetMD5(pwd);

        try
        {
            if (strPassword == GetPassword(strUserName))
            {
                return true;
            }
        }
        catch { }

        return false;

    }

    public static DataTable GetPartnerByHashCode(string hash)
    {
        Partner partner = new Partner();
        ObjectQuery query = partner.CreateQuery();
        query.SetCriteria(partner.HashCode, hash);

        try
        {
            DataTable dt = new DataTable();
            query.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            return null;
        }

    }

    public static void InsertVisitor(Account account, string ipAddr, string sessionId)
    {
        if (account.IsExist)//有几种情况：1.直接从partner过来的，现在登录，2.直接访问efilefolder的partener用户，但是登录
        {
            if (account.PartnerID.Value != 0)//
            {
                Visitor vst = new Visitor(sessionId);

                if (vst.IsExist)
                {
                    vst.PartnerID.Value = account.PartnerID.Value;
                    vst.Tel.Value = account.Telephone.Value;
                    vst.UserName.Value = account.Name.Value;
                    vst.UUID.Value = account.UID.ToString();
                    vst.EmailAddr.Value = account.Email.Value;
                    vst.ContactName.Value = account.Firstname.Value + '.' + account.Lastname.Value;
                    vst.TrialFlag.Value = (account.LicenseID.Value == -1 || account.LicenseID.Value == 0) ? "Y" : "N";
                    vst.VisitorFlag.Value = "Y";
                    vst.Update();
                }
                else
                {

                    vst.IPAddress.Value = ipAddr;

                    vst.SessionID.Value = sessionId;
                    vst.PartnerID.Value = account.PartnerID.Value;
                    vst.Tel.Value = account.Telephone.Value;
                    vst.UserName.Value = account.Name.Value;
                    vst.UUID.Value = account.UID.Value;
                    vst.EmailAddr.Value = account.Email.Value;
                    vst.ContactName.Value = account.Firstname.Value + '.' + account.Lastname.Value;
                    vst.TrialFlag.Value = (account.LicenseID.Value == -1 || account.LicenseID.Value == 0) ? "Y" : "N";
                    vst.VisitorFlag.Value = "Y";
                    vst.Create();
                }
            }

        }
    }

    public static void InsertVisitor(Account account, string ipAddr, string sessionId, bool loginUser)
    {
        if (account.IsExist)//有几种情况：1.直接从partner过来的，现在登录，2.直接访问efilefolder的partener用户，但是登录
        {
            if (account.PartnerID.Value != 0)//
            {
                Visitor vst = new Visitor(sessionId);

                if (vst.IsExist)
                {
                    vst.PartnerID.Value = account.PartnerID.Value;
                    vst.Tel.Value = account.Telephone.Value;
                    vst.UserName.Value = account.Name.Value;
                    vst.UUID.Value = account.UID.ToString();
                    vst.EmailAddr.Value = account.Email.Value;
                    vst.ContactName.Value = account.Firstname.Value + '.' + account.Lastname.Value;
                    vst.TrialFlag.Value = "N";
                    vst.VisitorFlag.Value = "Y";
                    vst.Update();
                }
                else
                {

                    vst.IPAddress.Value = ipAddr;

                    vst.SessionID.Value = sessionId;
                    vst.PartnerID.Value = account.PartnerID.Value;
                    vst.Tel.Value = account.Telephone.Value;
                    vst.UserName.Value = account.Name.Value;
                    vst.UUID.Value = account.UID.Value;
                    vst.EmailAddr.Value = account.Email.Value;
                    vst.ContactName.Value = account.Firstname.Value + '.' + account.Lastname.Value;
                    vst.TrialFlag.Value = "N";
                    vst.VisitorFlag.Value = "Y";
                    vst.Create();
                }
            }

        }
    }

    public static DataTable GetQuestionDefines()
    {
        DataTable dt = new DataTable();

        QuestionSchema schema = new QuestionSchema();

        ObjectQuery query = schema.CreateQuery();
        query.SetOrderBy(schema.QuestionID, true);
        try
        {
            query.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            return dt;
        }
    }

    public static DataTable GetDoctorQuestions(string officeId)
    {
        DoctorQuestion schema = new DoctorQuestion();

        ObjectQuery query = schema.CreateQuery();
        query.SetCriteria(schema.DoctorID, officeId);
        query.SetOrderBy(schema.QuestionID, true);
        try
        {
            DataTable dt = new DataTable();
            query.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static void DefineDoctorQuestions(string doctorId, List<DoctorQuestion> list)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();

        string sql = string.Format("DELETE FROM DoctorQuestion WHERE DoctorID='{0}'", doctorId);

        SqlHelper sqlH = new SqlHelper(ConnectionName);
        sqlH.ExecuteNonQuery(sql);

        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            DoctorQuestion question = new DoctorQuestion();

            foreach (DoctorQuestion item in list)
            {
                item.Create(transaction);
            }

            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }
    }

    public static void SavePatientAnswers(Signature signature, List<QuestionEntity> list)
    {
        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            signature.Create(transaction);

            foreach (QuestionEntity item in list)
            {
                PatientQuestion answer = new PatientQuestion();
                answer.QuestionID.Value = item.QuestionID;
                answer.QuestionTitle.Value = item.QuestionTitle;
                answer.DoctorID.Value = item.OfficeID;
                answer.QuestionID.Value = item.QuestionID;
                answer.AnswerContent.Value = item.AnswerContent;
                answer.SignatureID.Value = signature.SignatureID.Value;

                answer.Create(transaction);
            }

            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }
    }

    public static DataTable GetUsersList()
    {
        string SQL = "select UID, Name, Name+'('+Firstname+','+Lastname+')' AS FullName from Account WHERE RoleID!='Administrators'";

        DataTable dt = new DataTable();
        SqlHelper sqlH = new SqlHelper(ConnectionName);
        sqlH.Fill(dt, SQL);
        return dt;
    }

    public static DataTable GetUsersGroups()
    {
        string SQL = "select UID, Groups, GroupsName from Account";

        DataTable dt = new DataTable();
        SqlHelper sqlH = new SqlHelper(ConnectionName);
        sqlH.Fill(dt, SQL);
        return dt;
    }


    public static DataTable GetAccessReports(string officeId)
    {
        AccessReport report = new AccessReport();
        ObjectQuery query = report.CreateQuery();

        query.SetCriteria(report.OfficeID, officeId);
        query.SetOrderBy(report.AccessDate, false);

        try
        {
            DataTable dt = new DataTable();
            query.Fill(dt);
            return dt;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public static void InsertUserAccess(Account account, string sessionId, string ip)
    {
        AccessReport access = new AccessReport();
        access.SessionId.Value = sessionId;
        access.UserID.Value = account.UID.Value;
        access.OfficeID.Value = account.IsSubUser.Value == "1" ? account.OfficeUID.Value : account.UID.Value;
        access.IsSubUser.Value = account.IsSubUser.Value;
        access.AddressIP.Value = ip;
        access.AccessDate.Value = DateTime.Now;
        access.FirstName.Value = account.Firstname.Value;
        access.LastName.Value = account.Lastname.Value;
        access.Name.Value = account.Name.Value;

        access.WeekDay.Value = DateTime.Now.DayOfWeek.ToString();


        IDbConnection connection = DbFactory.CreateConnection();
        connection.Open();
        IDbTransaction transaction = connection.BeginTransaction();
        try
        {
            access.Create(transaction);
            transaction.Commit();
        }
        catch (Exception ex)
        {
            transaction.Rollback();
            throw new ApplicationException(ex.Message);
        }
    }

    public static void UpdateUserAccess(string sessionId)
    {
        string SQL = string.Format("UPDATE AccessReport SET LogoutDate='{0}' " +
                                   "WHERE  AccessId IN ( SELECT TOP (1) AccessId FROM AccessReport " +
                                   "WHERE SessionId = '{1}' ORDER BY AccessDate DESC )", DateTime.Now.ToString(), sessionId);


        try
        {
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.ExecuteNonQuery(SQL);
        }
        catch { }

    }

    public static DataTable SearchUserAccesss(string officeId, string strKey)
    {
        string SQL = string.Format("SELECT * FROM AccessReport WHERE  OfficeID='{0}'" +
                                   "AND ( Name LIKE '%{1}%' OR FirstName LIKE '%{1}%' OR LastName LIKE '%{1}%') ORDER BY AccessDate DESC", officeId, strKey);

        DataTable dt = new DataTable();

        try
        {

            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.Fill(dt, SQL);
        }
        catch { }

        return dt;

    }

    public static int GetTotalFileRequestsNumber(string uid)
    {
        string SQL = "exec eFileFolder_GetFileRequestsCountByUID '";
        SQL += uid;
        SQL += "'";

        SqlHelper sqlH = new SqlHelper(ConnectionName);

        object ret = sqlH.ExecuteScalar(SQL);

        return ret == DBNull.Value ? 0 : Convert.ToInt32(sqlH.ExecuteScalar(SQL));
    }


    public static DataTable GetFileRequestMonthly(string uid)
    {
        DataTable dt = new DataTable();

        string SQL = "exec eFileFolder_GetFileRequestMonthly '";
        SQL += uid;
        SQL += "'";

        try
        {
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.Fill(dt, SQL);
        }
        catch { }

        return dt;
    }

    public static DataTable GetRetentionList()
    {
        Retention user = new Retention();

        ObjectQuery query = user.CreateQuery();

        query.SetOrderBy(user.Department);

        DataTable dt = new DataTable();

        try
        {
            query.Fill(dt);
        }
        catch (Exception exp)
        {
        }

        return dt;
    }

    public static DataTable GetRetentionList(string key)
    {
        string SQL = string.Format("SELECT * FROM Retention WHERE  ( Department LIKE '%{0}%' OR ClassRecord LIKE '%{0}%' OR Title LIKE '%{0}%') ORDER BY Department ASC", key);

        DataTable dt = new DataTable();

        try
        {
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.Fill(dt, SQL);
        }
        catch (Exception exp)
        {
        }

        return dt;
    }

    public static DataTable GetRetentionList(string p, string key)
    {
        string SQL = string.Format("SELECT * FROM Retention WHERE  ( {0} LIKE '%{1}%') ORDER BY Department ASC", p, key);


        DataTable dt = new DataTable();

        try
        {
            SqlHelper sqlH = new SqlHelper(ConnectionName);
            sqlH.Fill(dt, SQL);
        }
        catch (Exception exp)
        {
        }

        return dt;
    }
}