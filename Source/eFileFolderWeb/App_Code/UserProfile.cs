using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Profile;
using System.Web.Security;

/// <summary>
/// Summary description for UserProfile
/// </summary>

public class UserProfile: ProfileBase
{
    public static UserProfile GetUserProfile(string username)
    {
        return Create(username) as UserProfile;
    }

    [SettingsAllowAnonymous(false)]
    public string Description
    {
        get { return base["Description"] as string; }
        set { base["Description"] = value; }
    }
    
    [SettingsAllowAnonymous(false)]
    public ShoppingCart ShoppingCart
    {
        get { return base["ShoppingCart"] as ShoppingCart; }
        set { base["ShoppingCart"] = value; }
    }

    [SettingsAllowAnonymous(true)] 
    public LastEFFolders LastEFFolders
    {
        get { return base["LastEFFolders"] as LastEFFolders; }
        set { base["LastEFFolders"] = value; }
    }
        
    }
