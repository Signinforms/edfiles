﻿using iTextSharp.text.pdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

/// <summary>
/// Summary description for ViewerEditablePdf
/// </summary>
public class ViewerEditablePdf
{
    public static string PDFUrl = string.Empty;
    public static string rackSpaceURL = string.Empty;
    public static string documentFullPath = string.Empty;
    public static string shareID = string.Empty;
    protected string PDFData = "";
    public string Signature = string.Empty;
    public string viewerUID = string.Empty;
    public static string UID = string.Empty;
    public static string SharedByID = string.Empty;
    public string logoURL
    {
        get
        {
            string filePath = Common_Tatva.UploadFolder.Replace("/", "") + Common_Tatva.UserLogoFolder + "/" + viewerUID + ".png";
            if (!File.Exists(HttpContext.Current.Server.MapPath("~/" + filePath)))
                filePath = "";
            return filePath;
        }
    }

    public ViewerEditablePdf()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public class Base64String
    {
        public string baseString { get; set; }
        public string name { get; set; }
    }

    public DocumentCollectionView GetPDFData(string documentPath)
    {
        DocumentCollectionView documentCollectionView = new DocumentCollectionView();
        try
        {
            Common_Tatva.WriteLogFomLog("documentPath: " + documentPath, "ViewerEditable.pdf : GetPDFData()", "Function Entered");
            if (System.IO.File.Exists(documentPath))
            {
                documentCollectionView.PdfItems = GetPDFItems(documentPath);
                documentCollectionView.PDFSource = documentPath;
                documentCollectionView.Data = documentCollectionView.ParsedData;
                documentCollectionView.PDFData = documentCollectionView.Data = documentCollectionView.Data.Replace(@"\\", @"\").Replace(@"\", @"\\").Replace(System.Environment.NewLine, @"<br />");
            }
            Common_Tatva.WriteLogFomLog("documentPath: " + documentPath, "ViewerEditable.pdf : GetPDFData()", "Function Exited");
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteLogFomLog("documentPath: " + documentPath, "ViewerEditable.pdf : GetPDFData()", "Catch : ErrorMessage : " + ex.Message);
        }
        return documentCollectionView;
    }

    /// <summary>
    /// Get all pdf inputs with it's value in list of PdfItems result. It will use iTextSharp to read pdf.
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    private List<PdfItems> GetPDFItems(string fileName)
    {
        string pdfTemplate = fileName;//Server.MapPath("~/pdfs/f1040ezt.pdf");

        MemoryStream memStream = new MemoryStream();
        FileStream fileStream = null;
        string newFile = fileName;
        PdfReader pdfReader = null;
        List<PdfItems> lstPdfItems = new List<PdfItems>();
        try
        {
            Common_Tatva.WriteLogFomLog("fileName: " + fileName, "ViewerEditable.pdf : GetPDFItems", "Function Entered");
            fileStream = System.IO.File.OpenRead(pdfTemplate);
            memStream.SetLength(fileStream.Length);
            fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
            fileStream.Close();
            fileStream.Dispose();

            Common_Tatva.WriteLogFomLog("fileName: " + fileName, "ViewerEditable.pdf : GetPDFItems", "After fileStream closed");

            pdfReader = new PdfReader(memStream);
            AcroFields af = pdfReader.AcroFields;
            StringBuilder sb = new StringBuilder();

            Common_Tatva.WriteLogFomLog("fileName: " + fileName, "ViewerEditable.pdf : GetPDFItems", "Before iteration of af.Fields");
            foreach (var field in af.Fields)
            {
                if (Convert.ToString(field.Key) == "untitled46")
                {

                }
                PdfItems k = new PdfItems(lstPdfItems.Count, field.Key, af.GetField(Convert.ToString(field.Key)), af.GetFieldType(Convert.ToString(field.Key)));
                if (k.Type == (int)InputTypes.RADIO_BUTTON || k.Type == (int)InputTypes.CHECK_BOX || k.Type == (int)InputTypes.LIST_BOX || k.Type == (int)InputTypes.COMBO_BOX)
                {
                    try { k.AvailableValues.AddRange(GetCheckBoxExportValue(af, Convert.ToString(field.Key))); }
                    catch (Exception ex)
                    {
                    }
                }
                else if (k.Type == (int)InputTypes.TEXT_FIELD)
                {
                    try
                    {
                        AcroFields.Item fieldItem = af.GetFieldItem(Convert.ToString(field.Key));
                        PdfDictionary pdfDictionary = (PdfDictionary)fieldItem.GetWidget(0);
                        int maxFieldLength = Int32.Parse(pdfDictionary.GetAsNumber(PdfName.MAXLEN).ToString());
                        k.PdfItemProperties.MaxLength = maxFieldLength;
                    }
                    catch (Exception ex)
                    {

                    }
                }
                if (k.Type == (int)InputTypes.CHECK_BOX)
                {
                    string a = af.GetField(Convert.ToString(field.Key));
                }
                lstPdfItems.Add(k);

                if (Convert.ToString(field.Key) == "topmostSubform[0].Page1[0].Entity[0].p1-t4[0]" || Convert.ToString(field.Key) == "topmostSubform[0].Page1[0].f1_040_0_[0]")
                {

                }
            }
            Common_Tatva.WriteLogFomLog("fileName: " + fileName, "ViewerEditable.pdf : GetPDFItems", "After iteration of af.Fields");
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteLogFomLog("fileName: " + fileName, "ViewerEditable.pdf : GetPDFItems", "Catch : ErrorMessage : " + ex.Message);
        }
        finally
        {
            if (pdfReader != null)
                pdfReader.Close();
            memStream.Close();
            memStream.Dispose();
            if (fileStream != null)
            {
                fileStream.Close();
                fileStream.Dispose();
            }
        }
        Common_Tatva.WriteLogFomLog("fileName: " + fileName, "ViewerEditable.pdf : GetPDFItems", "Function Exited");
        return lstPdfItems;
    }

    private void DownloadFile()
    {
        try
        {
            System.IO.FileInfo file = new System.IO.FileInfo(documentFullPath);
            if (file.Exists)
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.WriteFile(file.FullName);

            }
            else
            {
                HttpContext.Current.Response.Write("This file does not exist.");
            }
        }
        catch (Exception ex)
        { }
    }

    //[WebMethod]
    //public static string SubmitPdfData(string strDocumentCollectionView, Base64String[] base64String, bool isDownload)
    //{
    //    string destinationPath = string.Empty;
    //    //  var uid = Convert.ToString(HttpContext.Current.Request.QueryString["Data"]);
    //    try
    //    {
    //        if (isDownload)
    //        {
    //            destinationPath = HttpContext.Current.Server.MapPath("~/" + Common_Tatva.RackSpaceDownloadDirectory) + "\\" + documentFullPath.Substring(documentFullPath.LastIndexOf("//") + 2);
    //            File.Copy(documentFullPath, destinationPath);
    //            destinationPath = destinationPath.Replace("\\", "//");
    //        }
    //        List<PdfItems> documentCollectionView = new List<PdfItems>();

    //        documentCollectionView = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PdfItems>>(strDocumentCollectionView);

    //        JavaScriptSerializer serializer = new JavaScriptSerializer();
    //        List<Base64String> baseData = base64String.ToList<Base64String>();
    //        FillPDFAndSave(documentCollectionView, documentFullPath, Enum_Tatva.Status.Submitted, baseData, UID);

    //    }
    //    catch (Exception ex)
    //    { }
    //    return destinationPath;
    //}

    /// <summary>
    /// This method will resize image as per aspect ratio. Here we are passing maxHeight and maxWidth. Based on these params, it will find lowest value and resize it accordingly.
    /// </summary>
    /// <param name="original"></param>
    /// <param name="maxWidth"></param>
    /// <param name="maxHeight"></param>
    /// <returns></returns>
    public static Bitmap ScaleImage(Bitmap original, int maxWidth, int maxHeight)
    {
        int originalWidth = original.Width;
        int originalHeight = original.Height;

        // To preserve the aspect ratio
        float ratioX = (float)maxWidth / (float)originalWidth;
        float ratioY = (float)maxHeight / (float)originalHeight;
        float ratio = Math.Min(ratioX, ratioY);

        // New width and height based on aspect ratio
        int newWidth = (int)(originalWidth * ratio);
        int newHeight = (int)(originalHeight * ratio);

        // Convert other formats (including CMYK) to RGB.
        Bitmap newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format64bppArgb);

        //ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
        EncoderParameters myEncoderParameters = new EncoderParameters(1);
        myEncoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.ColorDepth, 32L);

        // Draws the image in the specified size with quality mode set to HighQuality
        using (Graphics graphics = Graphics.FromImage(newImage))
        {
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.CompositingQuality = CompositingQuality.HighQuality;
            graphics.PixelOffsetMode = PixelOffsetMode.HighSpeed;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            graphics.DrawImage(original, 0, 0, newWidth, newHeight);
        }
        return newImage;
    }

    /// <summary>
    /// It has file name which we are going to fill with default vlaues also it has default values then it will just fill pdf contents with default values.
    /// </summary>
    /// <param name="lstPdfItems"></param>
    /// <param name="fileName"></param>
    public Dictionary<bool, string> FillPDFAndSave(List<PdfItems> lstPdfItems, string fileName, Enum_Tatva.Status status, List<Base64String> base64String = null, string uid = null)
    {
        MemoryStream memStream = new MemoryStream();
        PdfReader pdfReader = null;
        PdfStamper pdfStamper = null;
        FileStream fs = null;
        FileStream fileStream = null;
        try
        {
            Common_Tatva.WriteLogFomLog("fileName : " + fileName, "FillPDFAndSave", "Function Entered");
            fileStream = System.IO.File.OpenRead(fileName);

            memStream.SetLength(fileStream.Length);
            fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
            fileStream.Close();
            fileStream.Dispose();

            Common_Tatva.WriteLogFomLog("fileName : " + fileName, "FillPDFAndSave", "After Filestream closed");

            pdfReader = new PdfReader(memStream);
            //    PdfReader.unethicalreading = true;
            fs = new FileStream(fileName, FileMode.Create);
            pdfStamper = new PdfStamper(pdfReader, fs);

            AcroFields pdfFormFields = pdfStamper.AcroFields;

            Common_Tatva.WriteLogFomLog("fileName : " + fileName, "FillPDFAndSave", "Before lstPdfItems iteration");
            foreach (PdfItems item in lstPdfItems)
            {
                if (Convert.ToString(item.FieldName).ToLower().Contains("signature"))
                {
                    try
                    {
                        IList<AcroFields.FieldPosition> fieldPositions = pdfFormFields.GetFieldPositions(Convert.ToString(item.FieldName));
                        if (fieldPositions != null)
                        {
                            AcroFields.FieldPosition fieldPosition = fieldPositions[0];
                            string data = string.Empty;
                            foreach (Base64String str in base64String)
                            {
                                if (str.name.Equals(item.FieldName))
                                    data = str.baseString;

                            }

                            byte[] byteConvertedImage = Convert.FromBase64String(data);
                            MemoryStream ms = new MemoryStream(byteConvertedImage, 0, byteConvertedImage.Length);
                            // Convert byte[] to Image
                            ms.Write(byteConvertedImage, 0, byteConvertedImage.Length);
                            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);

                            // New width and height based on aspect ratio
                            int originalWidth = image.Width;
                            int originalHeight = image.Height;

                            //Bitmap newImage = ScaleImage(new Bitmap(image), Convert.ToInt32(Math.Abs(fieldPosition[3] - fieldPosition[1] - 3)), Convert.ToInt32(Math.Abs(fieldPosition[4] - fieldPosition[2] - 3)));
                            float ratioX = (float)(Convert.ToInt32(Math.Abs(fieldPosition.position.Right - fieldPosition.position.Left - 3))) / (float)originalWidth;
                            float ratioY = (float)(Convert.ToInt32(Math.Abs(fieldPosition.position.Top - fieldPosition.position.Bottom - 3))) / (float)originalHeight;

                            float ratio = Math.Min(ratioX, ratioY);
                            int newWidth = (int)(originalWidth * ratio);
                            int newHeight = (int)(originalHeight * ratio);

                            Bitmap newImage = ScaleImage(new Bitmap(image), newWidth, newHeight);

                            using (var b = new Bitmap(image.Width, image.Height))
                            {
                                b.SetResolution(image.HorizontalResolution, image.VerticalResolution);

                                using (var g = Graphics.FromImage(b))
                                {
                                    g.Clear(Color.White);
                                    g.DrawImageUnscaled(image, 0, 0);
                                }

                                iTextSharp.text.Image image1 = iTextSharp.text.Image.GetInstance(b, System.Drawing.Imaging.ImageFormat.Jpeg);
                                image1.SetAbsolutePosition(fieldPosition.position.Left, fieldPosition.position.Bottom);
                                image1.ScaleAbsolute(fieldPosition.position.Width, fieldPosition.position.Height);
                                // change the content on top of page 1
                                var pageNum = (fieldPositions[0].page > 0 && !string.IsNullOrEmpty(Convert.ToString(fieldPositions[0].page))) ? fieldPositions[0].page : 1;
                                PdfContentByte overContent = pdfStamper.GetOverContent(pageNum);
                                overContent.AddImage(image1);
                                pdfFormFields.SetFieldProperty(Convert.ToString(item.FieldName),
                                           "setfflags",
                                            PdfFormField.FF_READ_ONLY,
                                            null);
                                pdfFormFields.SetFieldProperty(Convert.ToString(item.FieldName),
                                           "clrfflags",
                                            PdfFormField.FF_REQUIRED,
                                            null);
                            }
                        }
                    }

                    catch (Exception ex)
                    {
                        Common_Tatva.WriteLogFomLog("fileName : " + fileName, "FillPDFAndSave", "Signature Saving catch : ErrorMsg : " + ex.Message);
                    }
                }
                else
                {
                    pdfFormFields.SetField(Convert.ToString(item.FieldName), item.Value);
                }
            }
            Common_Tatva.WriteLogFomLog("fileName : " + fileName, "FillPDFAndSave", "After lstPdfItems iteration");

            pdfStamper.FormFlattening = false;
            pdfStamper.Close();
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteLogFomLog("fileName : " + fileName, "FillPDFAndSave", "Catch : ErrorMsg : " + ex.Message);
            return new Dictionary<bool, string> { { false, ex.Message } };
        }
        finally
        {
            if (pdfStamper != null)
                pdfStamper.Close();
            if (pdfReader != null)
                pdfReader.Close();
            memStream.Close();
            memStream.Dispose();
            if (fs != null)
            {
                fs.Close();
                fs.Dispose();
            }
            if (fileStream != null)
            {
                fileStream.Close();
                fileStream.Dispose();
            }
        }
        Common_Tatva.WriteLogFomLog("fileName : " + fileName, "FillPDFAndSave", "Function Existed");
        return new Dictionary<bool, string> { { true, string.Empty } };
    }

    public Dictionary<bool, string> SavePdfData(string strDocumentCollectionView, Base64String[] base64String, string documentPath)
    {

        try
        {
            Common_Tatva.WriteLogFomLog("DocumentPath: " + documentPath, "SavePdfData : ViwerEditablePdf.cs", "Function Entered");
            List<PdfItems> documentCollectionView = new List<PdfItems>();
            documentCollectionView = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PdfItems>>(strDocumentCollectionView);
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<Base64String> baseData = base64String.ToList<Base64String>();
            return FillPDFAndSave(documentCollectionView, documentPath, Enum_Tatva.Status.Saved, baseData, Sessions.SwitchedSessionId);
        }
        catch (Exception ex)
        {
            return new Dictionary<bool, string> { { false, ex.Message } };
        }
    }


    public string[] GetCheckBoxExportValue(AcroFields fields, string cbFieldName)
    {
        AcroFields.Item fd = ((AcroFields.Item)fields.GetFieldItem(cbFieldName));
        var vals = fd.GetValue(0);
        Hashtable names = new Hashtable();
        string[] outs = new string[fd.Size];
        PdfDictionary pd = ((PdfDictionary)fd.GetWidget(0)).GetAsDict(PdfName.AP);
        for (int k1 = 0; k1 < fd.Size; ++k1)
        {
            PdfDictionary dic = (PdfDictionary)fd.GetWidget(k1);
            dic = dic.GetAsDict(PdfName.AP);
            if (dic == null)
                continue;
            dic = dic.GetAsDict(PdfName.N);
            if (dic == null)
                continue;
            foreach (PdfName pname in dic.Keys)
            {
                String name = PdfName.DecodeName(pname.ToString());
                if (name.ToLower() != "off")
                {
                    names[name] = null;
                    outs[(outs.Length - k1) - 1] = name;
                }
            }
        }
        return outs;
    }

}

public enum InputTypes
{
    BUTTON = 1,
    CHECK_BOX = 2,
    RADIO_BUTTON = 3,
    TEXT_FIELD = 4,
    LIST_BOX = 5,
    COMBO_BOX = 6,
    SIGN_BOX = 7
}

public class DocumentCollectionView
{
    public string Data { get; set; }
    public List<PdfItems> PdfItems { get; set; }
    public string PDFURL { get; set; }
    public string DocId { get; set; }
    public string PDFSource { get; set; }
    public string PDFData { get; set; }
    public string ParsedData
    {
        get
        {
            try
            {
                foreach (PdfItems pdfItem in (PdfItems))
                {
                    pdfItem.Value = pdfItem.Value.Replace("\"", "");
                    pdfItem.Value = pdfItem.Value.Replace("'", "");
                }
                return Newtonsoft.Json.JsonConvert.SerializeObject(PdfItems);
            }
            catch (Exception)
            {
                return "";
            }
        }
    }

    public List<PdfItems> ParsedPdfItems
    {
        get
        {
            List<PdfItems> items = new List<PdfItems>();
            try
            {
                items = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PdfItems>>(Data);
            }
            catch (Exception)
            {
            }
            return items;
        }

    }
}

public class PdfItems
{
    public PdfItems()
    { }

    public PdfItems(int SrNo, object fieldName, string value, int type)
    {
        this.SrNo = SrNo;
        this.Type = type;
        FieldName = fieldName;
        Value = value;
        AvailableValues = new List<string>();
        PdfItemProperties = new PdfItemProperties();
    }
    private int? _SrNo;
    public int? SrNo { get { return _SrNo; } set { _SrNo = value; } }
    private object _FieldName;
    public object FieldName { get { return _FieldName; } set { _FieldName = value; } }
    private string _Value;
    public string Value { get { return _Value; } set { _Value = value; } }
    private int _Type;
    public int Type { get { return _Type; } set { _Type = value; } }
    private List<string> _AvailableValues;
    public List<string> AvailableValues { get { return _AvailableValues; } set { _AvailableValues = value; } }
    private PdfItemProperties _PdfItemProperties;
    public PdfItemProperties PdfItemProperties { get { return _PdfItemProperties; } set { _PdfItemProperties = value; } }
}

public class PdfItemProperties
{
    private int _MaxLength;
    public int MaxLength { get { return _MaxLength; } set { _MaxLength = value; } }
}