using System;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for UserBase
/// </summary>
[Bindable(true)]
public abstract class UserBase
{
    private string _id;
    public string ID
    {
        get
        {
            return _id;
        }
        set
        {
            _id = value;
        }
    }
    private string _Firstname;
    [Bindable(true)]
    public string Firstname
    {
        set { _Firstname = value; }
        get { return _Firstname; }
    }

    private string _Lastname;
    [Bindable(true)]
    public string Lastname
    {
        set { _Lastname = value; }
        get { return _Lastname; }
    }

    private DateTime _dob;
    [Bindable(true)]
    public DateTime DOB
    {
        set { _dob = value; }
        get { return _dob; }
    }

    private DateTime _FaxNumber;
    [Bindable(true)]
    public DateTime FaxNumber
    {
        set { _FaxNumber = value; }
        get { return _FaxNumber; }
    }

    private string _PhoneNumber;
    [Bindable(true)]
    public string PhoneNumber
    {
        get { return _PhoneNumber; }
        set { _PhoneNumber = value; }
    }

    private int _OtherInfo1;
    [Bindable(true)]
    public int OtherInfo1
    {
        get { return _OtherInfo1; }
        set { _OtherInfo1 = value; }
    }

    private int _OtherInfo2;
    [Bindable(true)]
    public int OtherInfo2
    {
        get { return _OtherInfo2; }
        set { _OtherInfo2 = value; }
    }
}
