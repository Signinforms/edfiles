﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BlankMasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    public string DateTimeInfo
    {
        get
        {
            DateTimeFormatInfo myDTFI = new CultureInfo("en-US", false).DateTimeFormat;

            DateTime dt = DateTime.Now;
            return dt.ToString("dddd dd", myDTFI) + " " + dt.ToString("MMMM, yyyy", myDTFI);
        }
    }

}
