<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="BoughtAtStore.aspx.cs" Inherits="BoughtAtStore" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="100%" height="600px" >
        <tr align="center">
            <td valign="top" align="Center" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                style="background-repeat: repeat-x;">
                <div class="Naslov_Plav" style="width:200px;">
                    Bought at Store
                </div>
                <div class="podnaslov">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" >
                        <contenttemplate>
                            <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                            <table border="0" align="Center" cellpadding="2" cellspacing="0" style="margin: 20px 0 20px 10px;">
                            <tr>
                                <td align="left" colspan="2" class="podnaslov">
                                   <asp:RadioButton ID="rdbCreateAccount" runat="server" AutoPostBack="true"  Text="Create New Account" OnCheckedChanged="OnCheckChanged1"></asp:RadioButton></td>
                            </tr>
                            <tr>
                                <td align="left" colspan="2" class="podnaslov">
                                   <asp:RadioButton ID="rdbAddeFileFolders" runat="server"  AutoPostBack="true"  Text="Add eFileFolders to current account" OnCheckedChanged="OnCheckChanged2"></asp:RadioButton></td>
                               
                            </tr>
                            
                           <tr ID="trWebLogin" style="margin-left:50px" runat="server">
                                <td align="left" class="podnaslov_store">
                                   <div> &nbsp;&nbsp;Username&nbsp;</div><div>&nbsp;&nbsp;<asp:TextBox ID="UserName" runat="server" Width="85px" MaxLength="18" /></div></td>
                                
                                <td align="left" class="podnaslov_store"><div>Password&nbsp;</div><div>
                                    <asp:TextBox ID="Password" TextMode="Password" Width="85px" runat="server" MaxLength="28" />
                                    <asp:LinkButton ID="libForgotP" runat="server" Text="Forgot Your Password?" OnClick="libForgotP_Click"></asp:LinkButton></div>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" colspan="4" class="podnaslov">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2" >
                                    <%--<asp:ImageButton ID="LoginButton"  runat="server" OnClick="LoginButton_Click" ImageUrl="~/images/continue_btn.gif" />--%>
                                    <asp:Button ID="LoginButton"  runat="server" OnClick="LoginButton_Click" Text="Continue" />
                                </td>
                            </tr>
                                
                        </table>
                        </contenttemplate>
                        <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="rdbCreateAccount" EventName="CheckedChanged" />
                                <asp:AsyncPostBackTrigger ControlID="rdbAddeFileFolders" EventName="CheckedChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="call" align="center">
                    Register directly by calling</div>
                <div align="center" class="phone">
                    800-579-9190</div>
            </td>
        </tr>
        
    </table>
</asp:Content>
