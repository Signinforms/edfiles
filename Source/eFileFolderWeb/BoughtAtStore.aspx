<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
			CodeFile="BoughtAtStore.aspx.cs" Inherits="BoughtAtStore" Title="" %>
<%@ Register Src="~/Controls/RegisterDirectly.ascx" TagName="ucRegisterDirectly" TagPrefix="ucRegisterDirectly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        fieldset a {
            color:inherit;
            display:inline;
            font-size:16px;
        }
    </style>
 <div class="title-container">
  <div class="inner-wrapper">
   <h1 style="padding-top:15px"> Bought at Store</h1>
   <div class="clearfix"></div>
  </div>
 </div>
 <div class="page-container register-container bought-store">
  <div class="inner-wrapper">
   <div class="aboutus-contant">
    <div class="left-content">
     <div class="form-containt">
      <div class="content-box">
       <asp:UpdatePanel ID="UpdatePanel3" runat="server" >
        <contenttemplate>
         <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
         <table border="0" align="Center" cellpadding="2" cellspacing="0" style="width:100%;border-collapse:collapse;">
          <tr>
           <td align="left" colspan="2" class="podnaslov" ID="trWebLogin" runat="server"><fieldset>
            <asp:RadioButton ID="rdbCreateAccount" runat="server" AutoPostBack="true"  Text="Create New Account" OnCheckedChanged="OnCheckChanged1"></asp:RadioButton>
            <asp:RadioButton ID="rdbAddeFileFolders" runat="server"  AutoPostBack="true"  Text="Add EdFiles to current account" OnCheckedChanged="OnCheckChanged2"></asp:RadioButton>
            </fieldset>
            <fieldset>
            <label>Username</label>
            <asp:TextBox ID="UserName" runat="server" MaxLength="18" />
            </fieldset>
            <fieldset>
            <label>Password</label>
            <asp:TextBox ID="Password" TextMode="Password" runat="server" MaxLength="28" />
            </fieldset>
            <fieldset>
            <label></label>
            <asp:Button ID="LoginButton"  runat="server" OnClick="LoginButton_Click" Text="Continue" class="create-btn btn green" />
            </fieldset>
            <fieldset>
            <label></label>
            <asp:LinkButton ID="libForgotP" runat="server" Text="Forgot Your Password?" OnClick="libForgotP_Click"></asp:LinkButton>
            </fieldset></td>
          </tr>
         </table>
        </contenttemplate>
        <Triggers>
         <asp:AsyncPostBackTrigger ControlID="rdbCreateAccount" EventName="CheckedChanged" />
         <asp:AsyncPostBackTrigger ControlID="rdbAddeFileFolders" EventName="CheckedChanged" />
        </Triggers>
       </asp:UpdatePanel>
      </div>
      </td>
      </tr>
      </table>
     </div>
    </div>
    <div class="right-content">
     <ucRegisterDirectly:ucRegisterDirectly ID="ucRegister" runat="server" />
    </div>
   </div>
  </div>
 </div>
</asp:Content>
