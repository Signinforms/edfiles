using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.DAL;
using Shinetech.Utility;
using Account = Shinetech.DAL.Account;

public partial class BoughtAtStore : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.rdbAddeFileFolders.Checked = true;
            this.lblMessage.Text = "";
            this.UserName.Text = "";
            this.Password.Text = "";
        }
    }
    
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!this.Page.User.Identity.IsAuthenticated)
        {
            this.MasterPageFile = "~/LoginMasterPage.master";
        }
    }

    protected void libForgotP_Click(object sender, EventArgs e)
    {
        string strUserName;
        strUserName = UserName.Text.Trim();
        if (strUserName != "")
        {
            if (Membership.GetUser(strUserName) == null)
            {
                this.lblMessage.Text = "this user name is not exist.";
                return;
            }
            else
            {
                Response.Redirect("ForgetPassword.aspx?UserName=" + strUserName);
            }
        }
        else
        {
            Response.Redirect("ForgetPassword.aspx");
        }
    }

    protected void OnCheckChanged1(object sender, EventArgs e)
    {
        rdbAddeFileFolders.Checked = false;
        this.lblMessage.Text = "";
        this.UserName.Text = "";
        this.Password.Text = "";
        this.UserName.Enabled = false;
        this.Password.Enabled = false;
    }

    protected void OnCheckChanged2(object sender, EventArgs e)
    {
        rdbCreateAccount.Checked = false;
        this.lblMessage.Text = "";
        this.UserName.Text = "";
        this.Password.Text = "";
        this.UserName.Enabled = true;
        this.Password.Enabled = true;
    }

    protected void LoginButton_Click(object sender, EventArgs e)
    {
        if (rdbCreateAccount.Checked)
        {
            Response.Redirect("~/WebUser/PartnerUser.aspx?bought=1", true);
            return;
        }

        if (this.UserName.Text.Trim() == "")
        {
            this.lblMessage.Text = "The User Name is required!";
            return;
        }
        if (this.Password.Text.Trim() == "")
        {
            this.lblMessage.Text = "The Password is required!";
            return;
        }

        string strUserName = this.UserName.Text.Trim();
        string strPassword = PasswordGenerator.GetMD5(this.Password.Text.Trim());

        if (strPassword == UserManagement.GetPassword(strUserName))
        {
            if (UserManagement.IsApproved(strUserName))
            {

                FormsAuthentication.RedirectFromLoginPage(strUserName,false);

                Response.Redirect("~/WebUser/BoughtGiftCard.aspx");
            }
            else
            {
                DataTable dtTemp = new DataTable();
                dtTemp = UserManagement.IsApprovedInTemp(strUserName);
                if (dtTemp.Rows.Count == 0)
                {
                    string strWrongInfor = String.Format("Error : The user {0} is locked by administrator.", strUserName);
                    Page.RegisterStartupScript("RssOk", "<script type='text/javascript'>alert('" + strWrongInfor + "')</script>");
                }
            }
        }
        else
        {
            this.lblMessage.Text = "The UserName or Password is wrong!";
        }
    }
}
