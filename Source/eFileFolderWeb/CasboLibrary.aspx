﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CasboLibrary.aspx.cs" Inherits="Office_CasboLibrary" MasterPageFile="~/BlankMasterPage.master" Title="Casbo Digital Library-EdFiles" %>

<%--<%@ Register Src="~/Controls/DocumentPreview.ascx" TagName="DocumentPreview" TagPrefix="uc1" %>--%>
<%@ Import Namespace="System.Web.Configuration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<!doctype html>
<html>
  <head runat="server">
      <title></title>--%>
    <link rel="shortcut icon" href="New/images/UpdatedHome/favicon.ico" />
    <link type="text/css" href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet" />
    <link href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/UpdatedHome/reset.css" rel="stylesheet" />
    <link href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/UpdatedHome/font-awesome.css" rel="stylesheet" />
    <link href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/UpdatedHome/bootstrap.min.css" rel="stylesheet" />
    <link href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/UpdatedHome/owl.carousel.css" rel="stylesheet" />
    <link href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/UpdatedHome/style.css" rel="stylesheet" />
    <link href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/UpdatedHome/media.css" rel="stylesheet" />
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/js/UpdatedHome/jquery.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/js/UpdatedHome/bootstrap.min.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/js/UpdatedHome/modernizr.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/js/UpdatedHome/fastclick.min.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/js/UpdatedHome/owl.carousel.min.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/js/UpdatedHome/general.js"></script>

    <style>
        .wrapper {
            padding: 0px;
        }

        .inner-wrapper {
            display: none;
        }

        .header-container {
            display: none;
        }

        .preview-popup {
            width: 100%;
            max-width: 1100px;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 10%;
            right: 0;
            margin: auto;
            background: #fff;
            height: 85%;
        }

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }

        #popupclose {
            float: right;
            cursor: pointer;
        }

        .popupcontent {
            padding: 10px;
        }

        #reviewContent {
            border-width: 2px;
            border-style: inset;
            border-color: initial;
            border-image: initial;
        }

        .owl-prev, .owl-next {
            font-size: 0px;
        }

        .item > div > a > h5 {
            height: 120px !important;
            text-overflow: ellipsis;
            font-family: 'open sans', sans-serif;
        }

        .footer-container {
            background: inherit;
            padding: 0px;
        }

        .footer-copy {
            display: none;
        }

        .casbo-content {
            padding: 20px 0 270px 0;
            position: relative;
        }

        .casbo-footer {
            height: 250px;
            border-top: 1px solid #ddd;
            background: #FFF;
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
        }

            .casbo-footer p {
                padding: 0 0 0 0;
                color: #666666;
            }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-90615842-2"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'UA-90615842-2');
    </script>


    <script type="text/javascript">
        function pageLoad() {
            var closePopup = document.getElementById("popupclose");
            closePopup.onclick = function () {
                var popup = document.getElementById("preview_popup");
                var overlay = document.getElementById("overlay");
                overlay.style.display = 'none';
                popup.style.display = 'none';
                $('#floatingCirclesG').css('display', 'block');
                enableScrollbar();
                $('#reviewContent').removeAttr('src');
            };
        }

        function showPreview(fileName, fileID) {
            hideLoader();
            disableScrollbar();
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//"
                    + window.location.hostname
                    + (window.location.port ? ':' + window.location.port : '');
            }

            fileName = fileName.substring(0, fileName.lastIndexOf('.'));
            fileName = fileName.trim() + ".pdf";

            var popup = document.getElementById("preview_popup");
            var overlay = document.getElementById("overlay");
            overlay.style.display = 'block';
            popup.style.display = 'block';
            $('#floatingCirclesG').css('display', 'none');
            $('#preview_popup').focus();
            $('#reviewContent').attr('src', './Office/Preview.aspx?data=' + window.location.origin + "/Casbo/" + fileName.trim() + "?v=" + "<%= DateTime.Now.Ticks%>");
            $("#" + '<%= hdnFilePath.ClientID %>').val(window.location.origin + "/Casbo/" + fileName.trim());

            CreateCasboLogs(fileID, ActionEnum.Preview, $("#" + '<%= hdnCustId.ClientID %>').val(), $("#" + '<%= hdnFirstName.ClientID %>').val(), $("#" + '<%= hdnLastName.ClientID %>').val());
            return false;
        }

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
        }

        function disableScrollbar() {
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }

        function enableScrollbar() {
            $('html, body').css({
                'overflow': 'auto',
                'height': 'auto'
            });
        }


        $(document).ready(function () {

            if ($("#" + "<%= hdnValidated.ClientID%>").val() == "true")
                GetCasboImages("");
            else
                alert("Failed to get Casbo Data!");

            $(".searchlink").click(function () {
                //$(".owl-carousel").removeClass("owl-loaded");
                //$(".owl-carousel").removeClass("owl-drag");
                $('#banner').owlCarousel('destroy');
                GetCasboImages($("#casboSearch").val())
            });

            $("#casboSearch").keypress(function (e) {
                if (e.keyCode == 13) {
                    e.preventDefault();
                    $(".searchlink").click();
                }
            });

            function GetCasboImages(fileName) {
                $(".owl-carousel").empty();
                $.ajax({
                    type: "POST",
                    url: 'CasboLibrary.aspx/GetCasboImages',
                    contentType: "application/json; charset=utf-8",
                    data: "{ fileName:'" + fileName + "'}",
                    dataType: "json",
                    success: function (data) {
                        if (data.d.length > 0) {
                            var fileNames = JSON.parse(data.d);
                            var parent = $(".owl-carousel");
                            if (parent != null) {
                                for (var i = 0; i < fileNames.length; i++) {
                                    var parentDiv = document.createElement('div');
                                    $(parentDiv).addClass("item");
                                    //var hiddenField = '<input type="hidden" name="hdnFileID" value="' + fileNames[i].ID + '">';
                                    var childDiv = document.createElement('div');
                                    var titleAnchorTag = $('<a href="#"></a>');
                                    var titleTag = '<h5>' + fileNames[i].FileName.substring(0, fileNames[i].FileName.lastIndexOf('.')) + '</h5>';
                                    $(titleAnchorTag).attr("onclick", "showPreview('" + fileNames[i].FileName + "'" + ", " + fileNames[i].ID + ")");
                                    $(titleAnchorTag).append($(titleTag));
                                    var bodyDiv = document.createElement('div');
                                    $(bodyDiv).addClass("item-body");
                                    var imgTag = img = $('<img alt="">');
                                    var imageAnchorTag = $('<a href="#"></a>');
                                    $(imageAnchorTag).attr("onclick", "showPreview('" + fileNames[i].FileName + "'" + ", " + fileNames[i].ID + ")");
                                    var src = window.location.origin;
                                    $(imgTag).attr("src", src + "/casbo/" + fileNames[i].FileName);
                                    $(imageAnchorTag).append($(imgTag));
                                    var fileName = fileNames[i].FileName.substring(0, fileNames[i].FileName.lastIndexOf('.'));
                                    fileName = fileName.trim() + ".pdf";
                                    var downLoadLink = $('<a href="' + src + "/CasboLibrary.aspx?fileName=" + fileName + '"></a>');
                                    var imgTag1 = img = $('<img alt="">');
                                    $(imgTag1).attr("src", src + "/images/icon-download.png");
                                    $(imgTag1).css("width", "45px");
                                    $(imgTag1).css("height", "28px");
                                    $(downLoadLink).append($(imgTag1));
                                    var bodyChildDiv = document.createElement('div');
                                    var bodyChildSiblingDiv = document.createElement('div');
                                    $(bodyChildSiblingDiv).css("text-align", "center");
                                    $(childDiv).append($(titleAnchorTag));
                                    $(childDiv).append($(bodyDiv));
                                    $(bodyDiv).append($(bodyChildDiv));
                                    $(bodyChildDiv).append($(imageAnchorTag));
                                    $(bodyChildSiblingDiv).append($(downLoadLink));
                                    $(parentDiv).append($(childDiv));
                                    $(parentDiv).append($(bodyChildSiblingDiv));
                                    $(parent).append($(parentDiv));
                                }
                                $('.owl-carousel').owlCarousel({
                                    loop: false,
                                    // margin: 10,
                                    nav: true,
                                    autoHeight: true,
                                    responsive: {
                                        0: {
                                            items: 1
                                        },
                                        600: {
                                            items: 3
                                        },
                                        1000: {
                                            items: 5
                                        }
                                    }
                                })

                            }
                        }

                    },
                    error: function (result) {
                        alert("Failed to get Casbo Data!");
                        console.log('Failed' + result.responseText);
                    }
                });
            }

        });


    </script>

    <%--</head>--%>
    <div class="casbo-library">
        <asp:HiddenField ID="hdnValidated" runat="server" />
        <asp:HiddenField ID="hdnFirstName" runat="server" />
        <asp:HiddenField ID="hdnLastName" runat="server" />
        <asp:HiddenField ID="hdnCustId" runat="server" />
        <asp:HiddenField runat="server" ID="hdnFilePath" />
        <div class="casbo-wrapper">
            <header class="casbo-header">
    <div class="container">
     <div class="casbo-logo"><img src="New/images/UpdatedHome/casbo-logo.png" alt=""></div>
   </div>
 </header>

            <div class="casbo-content">
                <div class="container">
                    <%--<p class="bigtext">Welcome to the new online Casbo library of manuals. We are pleased to be able to bring you a digital Library of all Casbo manuals going forward. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent facilisis sapien volutpat lectus varius, vel ultrices tortor tempor. Sed faucibus nulla diam, in finibus est euismod ut. Vestibulum non metus sagittis, viverra libero in, placerat libero. Nam augue nisl, placerat in diam ac, mollis facilisis magna.</p>--%>
                    <h4>Providing you the resources you need. Just like before ... only better!</h4>
                    <p class="bigtext">Welcome to our digital library of professional development manuals. Here, you'll find the information you need to grow your skills and do your job right.</p>
                    <p class="bigtext">All at your fingertips, in an easy-to-share digital format designed to keep a lid on your district's recycle bins!</p>
                    <div class="titlebar">
                        <%--<h2>Records Retention</h2>--%>
                        <div class="searchbar">
                            <a href="#" class="searchlink"></a>
                            <input class="form-control" type="text" placeholder="Search by Name" id="casboSearch" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="owl-carousel casbo-carousel" id="banner">
                        </div>
                    </div>
                </div>
            </div>
            <footer class="casbo-footer">
          <div class="container">
              <div class="row">
                  <div class="content">
                      <div class="field field-name-field-section-title field-type-text-long field-label-hidden">
                          <div class="field-items">
                              <div class="field-item even">
                                  <p style="color:#666666; text-align: center;font-family: 'Open Sans','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif";">Need assistance?</p>
                                  <h3 style="text-align: center;font-family: 'Raleway';padding: 10px 0;font-weight: 400;color: #2D3237; font-size:24px;">Melissa Martinez</h3>
                                  <p style="color: #666666;text-align: center;font-family: 'Open Sans','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif";">
                                      Professional Development Coordinator<br /> & <br />COE Membership Services Account Manager
                                <br>
                                      <a href="mailto:&#8203;mmartinez@casbo.org" style="color: #255AA8;font-family:'Open Sans','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif"">mmartinez@casbo.org</a>
                                      <br>
                                      <span style="line-height: 1.538em;font-family:'Open Sans','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif">(916) 447-3783</span>
                                  </p>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-6" style="padding-top:35px;">
                      <p>Copyright <%= DateTime.Now.Year %> &copy; EdFiles.com</p>
                  </div>
                  <div class="col-sm-6">
                      <div class="foot-logo"><a href="http://www.edfiles.com">
                          <img src="./New/images/edFile_logo.png" alt="" /></a></div>
                  </div>
              </div>
          </div>
      </footer>
        </div>
        <%--    </body>
    </html>--%>
    </div>

    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <div class="popup-mainbox preview-popup" id="preview_popup" style="display: none">
        <div class="popupcontrols" style="width: 100%; height: 5%;">
            <span id="popupclose" class="ic-icon ic-close"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%;">
            <iframe id="reviewContent" style="width: 100%; height: 100%;"></iframe>
            <%--<uc1:DocumentPreview ID="DocumentPreview" runat="server" />--%>
        </div>
    </div>
</asp:Content>
