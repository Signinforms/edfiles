﻿using Newtonsoft.Json;
using Shinetech.DAL;
using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Web.Services;
using System.Web.UI;

public partial class Office_CasboLibrary : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string id = Request.QueryString["id"];
            if (!string.IsNullOrEmpty(id))
                ValidateUser(int.Parse(id));
            string fileName = Request.QueryString["fileName"];
            if (!string.IsNullOrEmpty(fileName))
                DownloadFile(fileName);
        }
    }

    private void ValidateUser(int id)
    {
        string url = ConfigurationManager.AppSettings["CasboURL"] + ConfigurationManager.AppSettings["CasboAPI"] + "/" + id;
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

        request.Method = "GET";
        try
        {
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();

            string content = string.Empty;
            using (Stream stream = response.GetResponseStream())
            {
                using (StreamReader sr = new StreamReader(stream))
                {
                    content = sr.ReadToEnd();
                }
            }

            RootObject jsonResult = JsonConvert.DeserializeObject<RootObject>(content);
            if (jsonResult.esuiteapi.Validate == 1)
            {
                hdnValidated.Value = "true";
                hdnCustId.Value = jsonResult.esuiteapi.CustId;
                hdnFirstName.Value = jsonResult.esuiteapi.FirstName;
                hdnLastName.Value = jsonResult.esuiteapi.LastName;
            }
            else
                hdnValidated.Value = "false";
        }
        catch (WebException ex)
        {
            //string message = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
        }
    }

    [WebMethod]
    public static string GetCasboImages(string fileName)
    {
        DataTable dt = General_Class.GetCasboPdfsForAdmin(fileName);

        foreach (DataRow row in dt.Rows)
        {
            string fName = row["FileName"].ToString();
            fName = fName.Substring(0, fName.LastIndexOf('.')) + ".png";
            row["FileName"] = fName;
        }
        return JsonConvert.SerializeObject(dt);
    }

    public void DownloadFile(string fileName)
    {
        var path = Server.MapPath("~/casbo/" + fileName);
        var fileData = System.IO.File.ReadAllBytes(path);
        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        Response.AddHeader("content-disposition", "attachment;  filename=\"" + fileName + "\"");
        Response.BinaryWrite(fileData);
        Response.End();
    }

}

public class Esuiteapi
{
    public int Validate { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string CustId { get; set; }
}

public class RootObject
{
    public Esuiteapi esuiteapi { get; set; }
}