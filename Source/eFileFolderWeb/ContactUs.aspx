<%@ Page Language="C#" MasterPageFile="~/HomeMasterPage.master" AutoEventWireup="true"
    CodeFile="ContactUs.aspx.cs" Inherits="ContactUs"%>

<%@ Register Src="~/Controls/RegisterDirectly.ascx" TagName="ucRegisterDirectly" TagPrefix="ucRegisterDirectly" %>

<%@ Register TagPrefix="fjx" Namespace="com.flajaxian" Assembly="com.flajaxian.FileUploader" %>
<%@ Register TagPrefix="cust" Namespace="Shinetech.Engines.Adapters" Assembly="FormatEngine" %>
<%@ Import Namespace="System.Web.Configuration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Start Banner -->
		<section class="inner-banner text-center about-banner">
			<div class="container">
				<h2>Contact Us</h2>
				<h4><span>We are always ready to help you!</span></h4>				
			</div>
		</section>
		<!-- End Banner -->

		<!-- Start Main Content -->
		<main class="content-block cms-content text-center">
			<div class="container">
				<p class="call-detail">please Call <a href="tel:1833EdFiles" title="1-833-EdFiles" class="call-link-mobile">1-833-EdFiles</a> or <a href="tel:16572173260" title="1-657-217-3260" class="call-link-mobile">1-657-217-3260</a>.</p>	
				<address>
					<span>EdFiles, Inc.</span>
					<span>261 E. Imperial Hwy.  #550</span>
					<span>Fullerton,  CA. 92835</span>
					<span><a href="mailto:support@edfiles.com" title="support@edfiles.com">support@edfiles.com</a></span>
				</address>				
			</div>
		</main>
		<!-- End Main Content -->
    <a href="tel:16572173260" class="call-link" style="font-size:18px;"><span class="icon"><img src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/images/edFiles/call-icon-white.png" alt=""></span> For further assistance, Call <span class="phone-no" style="font-size:24px;">1-657-217-3260</span></a>
</asp:Content>
