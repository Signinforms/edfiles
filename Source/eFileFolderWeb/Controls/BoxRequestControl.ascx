﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="BoxRequestControl.ascx.cs" Inherits="Controls_BoxRequestControl" %>

<h3>Box Requests</h3>
<p></p>
<script language="javascript" type="text/javascript">
    function openwindow(id) {
        var url = 'ModifyBox.aspx?id=' + id;
        var name = 'ModifyBox';
        window.open(url, name, '');
    }

    </script>
<asp:UpdatePanel ID="UpdatePanelBox" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Repeater ID="RepeaterBox" runat="server">
            <ItemTemplate>
                <p style="margin: 10px 0 10px">
                    <asp:Label ID="Label1" Text='<%# string.Format("{0}.",Eval("Number"))%>'
                        runat="server" />
                    <%--<asp:Label ID="LinkButton1" CausesValidation="false" ForeColor="Blue" runat="server" Text='<%# string.Format("{0}",Eval("BoxName"))%>' />--%>

                    <asp:LinkButton ID="LinkButton21" runat="server" ForeColor="Blue" Text='<%# string.Format("{0}",Eval("BoxName"))%>' CausesValidation="false"
                        OnClientClick='<%#string.Format("javascript:openwindow({0})", Eval("BoxId"))%>' />

                    <asp:Label ID="Label2" Text='<%# string.Format("{0:MM.dd.yyyy}",Eval("DestroyDate"))%>' runat="server" />
                </p>
            </ItemTemplate>

            <AlternatingItemTemplate>
                <p style="margin: 10px 0 10px">
                    <asp:Label ID="Label3" Text='<%# string.Format("{0}.",Eval("Number"))%>'
                        runat="server" />
                    <%--<asp:Label ID="LinkButton2" CausesValidation="false" ForeColor="Blue" runat="server" Text='<%# string.Format("{0}",Eval("BoxName"))%>' />--%>

                     <asp:LinkButton ID="LinkButton21" runat="server" ForeColor="Blue" Text='<%# string.Format("{0}",Eval("BoxName"))%>' CausesValidation="false"
                        OnClientClick='<%#string.Format("javascript:openwindow({0})", Eval("BoxId"))%>' />
                    <asp:Label ID="Label4" Text='<%# string.Format("{0:MM.dd.yyyy}",Eval("DestroyDate"))%>' runat="server" />
                </p>
            </AlternatingItemTemplate>
        </asp:Repeater>
    </ContentTemplate>
</asp:UpdatePanel>
