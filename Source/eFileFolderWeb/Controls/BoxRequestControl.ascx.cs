﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;



public partial class Controls_BoxRequestControl : System.Web.UI.UserControl
{
    public int numEFF;
    public int usedEFFNum;
    public int unUsedEFFNum;
    public float usedEFFSizes;
    string officeUserID = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
        {        
        if (!Page.IsPostBack)
        {
            if (Membership.GetUser() != null)
            {
                officeUserID = Sessions.SwitchedSessionId;
                //officeUserID = Membership.GetUser().ProviderUserKey.ToString();
                BindData();
            }
        }        
    }
    public void BindData()
    {
        RepeaterBox.DataSource = RequestBoxes;
        RepeaterBox.DataBind();
    }

    public ArrayList RequestBoxes
    {
        get
        {
            ArrayList recent = new ArrayList();
            //if (this.Session["Recent_RequestFiles_" + offcieUserID] == null)
            //{
                DataTable table = FileFolderManagement.GetRequestBoxes(officeUserID);
                if (table != null)
                {
                    int n = table.Rows.Count;
                    RequestBoxItem item;

                    for (int i = 0; i < n; i++)
                    {
                        item = new RequestBoxItem(table.Rows[i]);
                        item.Number = i + 1;

                        recent.Add(item);
                    }
                    //this.Session["Recent_RequestFiles_" + offcieUserID] = recent;
                }
            //}
            //else
            //{
            //    recent = this.Session["Recent_RequestFiles_" + offcieUserID] as ArrayList;
            //}

            return recent;
        }
        set
        {
            //this.Session["Recent_RequestFiles_" + offcieUserID] = value;
        }
    }
}