using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Framework.Controls;

public partial class Controls_DocFind : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string strKey = this.txtKey.Text;


        //Control control = this.NamingContainer.Parent.FindControl("UpdatePanel1");
        Control control = this.NamingContainer.FindControl("UpdatePanel1");


        GetUser(strKey);
        BindGrid(control);
    }

    #region 数据绑定
    public void GetUser(string key)
    {
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        this.DataSource = FileFolderManagement.GetDocumentByKey(key, uid);
    }

    /// <summary>
    /// 初始化绑定
    /// </summary>
    public void BindGrid(Control updatpanel)
    {
        WebPager WebPager1 = updatpanel.FindControl("WebPager2") as WebPager;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();

    }
    #endregion


    public DataTable DataSource
    {
        get
        {
            return this.Session["FileSource"] as DataTable;
        }
        set
        {
            this.Session["FileSource"] = value;
        }
    }
}
