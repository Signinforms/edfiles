<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FileRequestControl.ascx.cs"
    Inherits="FileRequestControl" %>
<h3>File Requests</h3>
<p></p>
<asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <%--<asp:Repeater ID="Repeater1" runat="server">
            <ItemTemplate>
                <p style="margin:10px 0 10px">
                    <asp:Label ID="Label1" Text='<%# string.Format("{0}.",Eval("Number"))%>'
                        runat="server" />
                    <asp:Label ID="LinkButton1" CausesValidation="false" ForeColor="Blue" runat="server" Text='<%# string.Format("{0}",Eval("BoxName"))%>' />
                    <asp:Label ID="Label2" Text='<%# string.Format("{0:MM.dd.yyyy}",Eval("CreateDate"))%>' runat="server" />
                </p>
            </ItemTemplate>

            <AlternatingItemTemplate>
                <p style="margin:10px 0 10px">
                    <asp:Label ID="Label3" Text='<%# string.Format("{0}.",Eval("Number"))%>'
                        runat="server" />
                    <asp:Label ID="LinkButton2" CausesValidation="false" ForeColor="Blue" runat="server" Text='<%# string.Format("{0}",Eval("BoxName"))%>' />
                    <asp:Label ID="Label4" Text='<%# string.Format("{0:MM.dd.yyyy}",Eval("CreateDate"))%>' runat="server" />
                </p>
            </AlternatingItemTemplate>
        </asp:Repeater>--%>
        <asp:Repeater ID="Repeater1" runat="server">
            <ItemTemplate>
                <p style="margin:10px 0 10px">
                    <asp:Label ID="Label1" Text='<%# string.Format("{0}.",Eval("Number"))%>'
                        runat="server" />
                    <asp:Label ID="LinkButton1" CausesValidation="false" ForeColor="Blue" runat="server" Text='<%# string.Format("{0} {1}",Eval("FirstName"),Eval("LastName"))%>' />
                    <asp:Label ID="Label2" Text='<%# string.Format("{0:MM.dd.yyyy}",Eval("CreateDate"))%>' runat="server" />
                </p>
            </ItemTemplate>

            <AlternatingItemTemplate>
                <p style="margin:10px 0 10px">
                    <asp:Label ID="Label3" Text='<%# string.Format("{0}.",Eval("Number"))%>'
                        runat="server" />
                    <asp:Label ID="LinkButton2" CausesValidation="false" ForeColor="Blue" runat="server" Text='<%# string.Format("{0} {1}",Eval("FirstName"),Eval("LastName"))%>' />
                    <asp:Label ID="Label4" Text='<%# string.Format("{0:MM.dd.yyyy}",Eval("CreateDate"))%>' runat="server" />
                </p>
            </AlternatingItemTemplate>
        </asp:Repeater>
    </ContentTemplate>
</asp:UpdatePanel>