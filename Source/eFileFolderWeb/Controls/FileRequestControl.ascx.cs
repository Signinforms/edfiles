using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class FileRequestControl : System.Web.UI.UserControl
{
    public int numEFF ;
    public int usedEFFNum;
    public int unUsedEFFNum;
    public float usedEFFSizes;
    string offcieUserID = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if(!Page.IsPostBack)
        {
            if (Sessions.SwitchedSessionId != null)
            {
                offcieUserID = Sessions.SwitchedSessionId;
                //offcieUserID = Membership.GetUser().ProviderUserKey.ToString();

                BindData();
            }
            
        }
    }

    public void BindData()
    {
        Repeater1.DataSource = RequestFiles;
        Repeater1.DataBind();
    }
    public ArrayList RequestFiles
    {
        get
        {
            ArrayList recent = new ArrayList();

            if (this.Session["Recent_RequestFiles_" + offcieUserID] == null)
            {
                DataTable table = FileFolderManagement.GetRequestFiles(offcieUserID);
                if(table!=null)
                {
                    int n = table.Rows.Count;
                    RequestItem item;

                    for(int i=0;i<n;i++)
                    {
                        item = new RequestItem(table.Rows[i]);
                        item.Number = i + 1;

                        recent.Add(item);
                    }

                    this.Session["Recent_RequestFiles_" + offcieUserID] = recent;

                }
            }
            else
            {
                recent = this.Session["Recent_RequestFiles_" + offcieUserID] as ArrayList;
            }

            return recent;
        }
        set
        {
            this.Session["Recent_RequestFiles_" + offcieUserID] = value;
        }
    }


}
