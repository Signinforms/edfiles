<%@ Control Language="C#" AutoEventWireup="true" CodeFile="FolderReminderControl.ascx.cs"
    Inherits="FolderReminderControl" %>
<div >
   <div style="width: 200px; margin-left: -32px;display: inline;
    margin-bottom: 0px;
    font-size: 20px;
    vertical-align: middle;
    font-weight: normal;color: #609d00;border-bottom: 2px solid">
        Folder Reminders
    </div>

    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Repeater ID="Repeater1" runat="server" OnItemDataBound="Repeater1_OnItemDataBound">
                <HeaderTemplate>
                    <table width="380" border="0" cellpadding="2" cellspacing="0" style="margin: 10px 0 20px -30px;">
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="left" class="tekstDef">
                            <asp:Label ID="Label1" CssClass="reminder_tekst" Font-Bold="true" runat="server" />

                            <asp:LinkButton ID="LinkButton1" CausesValidation="false" CssClass="reminder_tekst"
                                ForeColor="Blue" Font-Bold="true" runat="server" Text='<%# string.Format("{0}",Eval("Name"))%>'
                                OnClientClick='<%#string.Format("ClearFolderIdSource();window.open(\"FileFolderBox.aspx?id={0}\",\" _self\")",Eval("FolderId"))%>' />
                            <asp:Label ID="Label5" CssClass="reminder_tekst" Font-Bold="true" Text='<%# string.Format("({0})",Eval("FolderName"))%>'
                                runat="server" />
                            <asp:Label ID="Label2" CssClass="reminder_tekst" Font-Bold="true" Text='<%# string.Format("{0:MM.dd.yyyy}",Eval("ReminderDate"))%>'
                                runat="server" /></td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr>
                        <td align="left" bgcolor="#e8e7e1" class="tekstDef">
                            <asp:Label ID="Label3" CssClass="reminder_tekst" Font-Bold="true" runat="server" />
                            <asp:LinkButton ID="LinkButton2" CausesValidation="false" CssClass="reminder_tekst"
                                ForeColor="Blue" Font-Bold="true" runat="server" Text='<%# string.Format("{0} ",Eval("Name"))%>'
                                OnClientClick='<%#string.Format("ClearFolderIdSource();window.open(\"FileFolderBox.aspx?id={0}\",\" _self\")",Eval("FolderId"))%>' />
                            <asp:Label ID="Label5" CssClass="reminder_tekst" Font-Bold="true" Text='<%# string.Format("({0})",Eval("FolderName"))%>'
                                runat="server" />
                            <asp:Label ID="Label4" CssClass="reminder_tekst" Font-Bold="true" Text='<%# string.Format("{0:MM.dd.yyyy}",Eval("ReminderDate"))%>'
                                runat="server" /></td>
                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="Naslov_Y_Crven">
        <%--<asp:HyperLink ID="HyperLink1" CssClass="Naslov_Y_Crven" runat="server" NavigateUrl="../InstallAIR.aspx"
            Text="Download eFileFolder Viewer"></asp:HyperLink>--%>
    </div>
    <script type="text/javascript">
        function ClearFolderIdSource() {
            $.ajax(
                    {
                        type: "POST",
                        url: '../EFileFolderJSONService.asmx/ClearFolderIdSource',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json"
                    });
        } 
    </script>
</div>
