using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class FolderReminderControl : System.Web.UI.UserControl
{
    public int numEFF;
    public int usedEFFNum;
    public int unUsedEFFNum;
    public float usedEFFSizes;
    string OfficeUserID = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            if (Membership.GetUser() != null)
            {
                //OffcieUserID = Membership.GetUser().ProviderUserKey.ToString();

                OfficeUserID = Sessions.SwitchedRackspaceId;
                //if (this.Page.User.IsInRole("Offices"))
                //{
                //    OffcieUserID = Membership.GetUser().ProviderUserKey.ToString();
                //}

                //else if (this.Page.User.IsInRole("Users"))
                //{
                //    DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(Membership.GetUser().ProviderUserKey.ToString());
                //    OffcieUserID = dtOfficeUser.Rows[0]["OfficeUID"].ToString();
                //}

                //BindTextData();
                BindData();
            }

        }
    }


    public void BindTextData()
    {
        //RecentFolder recent = new RecentFolder();

        ////numEFF = recent.GetNumEFF(OffcieUserID);

        //DataTable dtNumEFF = UserManagement.GetNumByOfficeID(OffcieUserID);
        //numEFF = Convert.ToInt16(dtNumEFF.Rows[0]["FileFolders"].ToString());
        //usedEFFNum = recent.GetUsedEFFNum(OffcieUserID);
        //int  allUsedEFFNum = FileFolderManagement.GetAllUsedEFFNum(OffcieUserID);
        //unUsedEFFNum = Convert.ToInt16(dtNumEFF.Rows[0]["FileFolders"].ToString()) - Convert.ToInt16( allUsedEFFNum);
        //usedEFFSizes = recent.GetUsedEFFSizes(OffcieUserID);

        //labNumEFF.Text = numEFF.ToString();
        //labUsedEFFNum.Text = usedEFFNum.ToString();
        //labUnUsedEFFNum.Text = unUsedEFFNum.ToString();
        //labUsedEFFSizes.Text = string.Format("{0:F1}", usedEFFSizes);
    }

    public void BindData()
    {
        Repeater1.DataSource = RecentFolderReminders;
        Repeater1.DataBind();
    }
    public DataTable RecentFolderReminders
    {
        get
        {
            DataTable recent = null;

            if (this.Session["RecentFolderReminders"] == null)
            {
                //string uid = Membership.GetUser().ProviderUserKey.ToString();
                var current = DateTime.Now;
                DateTime endDate = current.AddDays(30);
                recent = FileFolderManagement.GetFileFolderReminders(OfficeUserID, new DateTime(current.Year, current.Month, current.Day), endDate);
            }
            else
            {
                recent = this.Session["RecentFolderReminders"] as DataTable;
            }

            return recent;
        }
        set { this.Session["RecentFolderReminders"] = value; }
    }

    protected void Repeater1_OnItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {

        // This event is raised for the header, the footer, separators, and items.

        // Execute the following logic for Items and Alternating Items.
        if (e.Item.ItemType == ListItemType.Item)
        {
            ((Label)e.Item.FindControl("Label1")).Text = string.Format("{0}.", (e.Item.ItemIndex + 1));
        }
        else if (e.Item.ItemType == ListItemType.AlternatingItem)
        {
            ((Label)e.Item.FindControl("Label3")).Text = string.Format("{0}.", (e.Item.ItemIndex + 1));
        }
    }
}
