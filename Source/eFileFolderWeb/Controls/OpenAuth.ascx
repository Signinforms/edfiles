﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="OpenAuth.ascx.cs" Inherits="Controls_OpenAuth" %>

<div id="socialLoginList">
    <p style="text-align:center;color:#000;font-weight:bold">Login using</p>
    <asp:ListView runat="server" ID="providerDetails" ItemType="System.String"
        SelectMethod="GetProviderNames" ViewStateMode="Disabled">
        <ItemTemplate>
            <p>
                <button type="submit" class="btn create-btn loginOption <%#: Item %>" name="provider" value="<%#: Item %>"
                    title="Log in using your <%#: Item %> account.">
                    <%#: Item %>
                </button>
            </p>
        </ItemTemplate>
        <%--<EmptyDataTemplate>
            <div>
                <p>There are no external authentication services configured. See <a href="http://go.microsoft.com/fwlink/?LinkId=252803">this article</a> for details on setting up this ASP.NET application to support logging in via external services.</p>
            </div>
        </EmptyDataTemplate>--%>
    </asp:ListView>
</div>