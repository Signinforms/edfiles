<%@ Control Language="C#" AutoEventWireup="true" CodeFile="QuickFind.ascx.cs" Inherits="QuickFind_New" %>

<%--<fieldset>
    <div class="Naslov_Plav" style="width: 200px;">
        Quick Find
    </div>
    Enter search term: (lastname, firstname..)
    <div class="clearfix"></div>

    
</fieldset>--%>


<%--<h3>Quick Find</h3>--%>
<%--<p>Enter search term: (lastname, firstname..)</p>--%>
<%--<table>
    <tr>
        <td>--%>
    <div class="find-inputbox" style="width:48%">
        <asp:TextBox ID="textfield3" runat="server" class="find-input" MaxLength="50" autocomplete="off" placeholder="Enter folder name or file number:" AutoPostBack="false" />
        <ajaxToolkit:AutoCompleteExtender
            runat="server" UseContextKey="true"
            BehaviorID="AutoCompleteEx"
            ID="autoComplete1"
            TargetControlID="textfield3" ServicePath="~/FlexWebService.asmx"
            ServiceMethod="GetCompletionList"
            MinimumPrefixLength="1"
            CompletionInterval="1000"
            EnableCaching="true"
            CompletionSetCount="20"
            CompletionListCssClass="autocomplete_completionListElement"
            CompletionListItemCssClass="autocomplete_listItem"
            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
            ShowOnlyCurrentWordInCompletionListItem="true" OnClientItemSelected="autoCompleteEx_ItemSelected">
            <Animations>
                    <OnShow>
                        <Sequence>
                            <%-- Make the completion list transparent and then show it --%>
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            <%--Cache the original size of the completion list the first time
                                the animation is played and then set it to zero --%>
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            <%-- Expand from 0px to the appropriate size while fading in --%>
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        <%-- Collapse down to 0pxc and fade out --%>
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx')._height" EndValue="0" />
                        </Parallel>
                    </OnHide>
            </Animations>
        </ajaxToolkit:AutoCompleteExtender>

        <asp:Button ID="searchButton1" runat="server" ImageAlign="AbsMiddle" CausesValidation="false" Text="Search" OnClick="search_btn1_click" class="quick-find-btn" />
    </div>
         <%--   </td>
        </tr>
    <tr>
        <td>--%>
    <div class="find-inputbox" style="width: 50%;display: inline-block;margin-left: 2px">
        <asp:TextBox ID="textfield" runat="server" class="find-input" MaxLength="50" autocomplete="off" placeholder="Enter document name:" AutoPostBack="false" />
        <ajaxToolkit:AutoCompleteExtender
            runat="server" UseContextKey="true"
            BehaviorID="AutoCompleteEx1"
            ID="autoComplete3"
            TargetControlID="textfield" ServicePath="~/FlexWebService.asmx"
            ServiceMethod="GetCompleteDocumentList"
            MinimumPrefixLength="2"
            CompletionInterval="1000"
            EnableCaching="true"
            CompletionSetCount="20"
            CompletionListCssClass="autocomplete_completionListElement"
            CompletionListItemCssClass="autocomplete_listItem"
            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
            ShowOnlyCurrentWordInCompletionListItem="true" OnClientItemSelected="autoCompleteEx1_ItemSelected">
            <Animations>
                    <OnShow>
                        <Sequence>
                            <%-- Make the completion list transparent and then show it --%>
                            <OpacityAction Opacity="0" />
                            <HideAction Visible="true" />
                            
                            <%--Cache the original size of the completion list the first time
                                the animation is played and then set it to zero --%>
                            <ScriptAction Script="
                                // Cache the size and setup the initial size
                                var behavior = $find('AutoCompleteEx1');
                                if (!behavior._height) {
                                    var target = behavior.get_completionList();
                                    behavior._height = target.offsetHeight - 2;
                                    target.style.height = '0px';
                                }" />
                            
                            <%-- Expand from 0px to the appropriate size while fading in --%>
                            <Parallel Duration=".4">
                                <FadeIn />
                                <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                            </Parallel>
                        </Sequence>
                    </OnShow>
                    <OnHide>
                        <%-- Collapse down to 0pxc and fade out --%>
                        <Parallel Duration=".4">
                            <FadeOut />
                            <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                        </Parallel>
                    </OnHide>
            </Animations>
        </ajaxToolkit:AutoCompleteExtender>

        <asp:Button ID="searchBtn2" runat="server" ImageAlign="AbsMiddle" CausesValidation="false" Text="Search" OnClick="searchBtn2_Click" class="quick-find-btn" />
    </div>
            <%--</td>
        </tr>
</table>--%>
<script type="text/javascript">

    function autoCompleteEx_ItemSelected(sender, args) {
        __doPostBack(args._text + '@searchFolder', "");
    }

    function autoCompleteEx1_ItemSelected(sender, args) {
        __doPostBack(args._text + '@searchDocument', "");
    }

    $(document).ready(function () {

        $("#" + '<%=textfield.ClientID %>').keypress(function (event) {
            if (event.keyCode == 13) {
                $("#" + '<%=searchBtn2.ClientID %>').click();
                return false;
            }
        });

        $("#" + '<%= textfield3.ClientID%>').keypress(function (event) {
            if (event.keyCode == 13) {
                $("#" + '<%=searchButton1.ClientID %>').click();
                return false;
            }
        });
    });
</script>
