using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Framework.Controls;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Collections.Generic;

public partial class QuickFind_New : System.Web.UI.UserControl
{
    private static string UrlSuffix
    {
        get
        {
            if (HttpContext.Current.Request.ApplicationPath == "/")
            {
                if (HttpContext.Current.Request.Url.Port == 80)
                    return HttpContext.Current.Request.Url.Host + HttpContext.Current.Request.ApplicationPath;
                else
                    return HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port.ToString() + HttpContext.Current.Request.ApplicationPath;
            }
            else
            {
                if (HttpContext.Current.Request.Url.Port == 80)
                    return HttpContext.Current.Request.Url.Host + HttpContext.Current.Request.ApplicationPath + "/";
                else
                    return HttpContext.Current.Request.Url.Host + ":" + HttpContext.Current.Request.Url.Port.ToString() + HttpContext.Current.Request.ApplicationPath + "/";
            }
        }
    }
    public static string UrlBase
    {
        get
        {
            return "http://" + UrlSuffix;
        }
    }

    public string searchURL = UrlBase + "Search/SearchFolderResult.aspx?key=";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"] != string.Empty)
        {
            //  was raised by a __doPostBack('search item name', '')                
            if (Convert.ToString(Request.Form["__EVENTTARGET"]).Contains("searchFolder"))
            {
                string key = Convert.ToString(Request.Form["__EVENTTARGET"]).Split('@')[0];
                SearchFolder(key);
            }
            if (Convert.ToString(Request.Form["__EVENTTARGET"]).Contains("searchDocument"))
            {
                string key = Convert.ToString(Request.Form["__EVENTTARGET"]).Split('@')[0];
                SearchDocument(key);
            }
        }
        if (!IsPostBack)
        {
            // this.Page.User.Identity.ToString()
            try
            {
                if (Membership.GetUser() != null && Membership.GetUser().ProviderUserKey != null)
                {
                    autoComplete1.ContextKey = Sessions.SwitchedSessionId;
                    autoComplete3.ContextKey = Sessions.SwitchedRackspaceId;
                }
            }
            catch (Exception ex)
            {

            }
        }
    }

    public void search_btn1_click(object sender, EventArgs e)
    {
        //if (TestCustomEvent != null)
        //    TestCustomEvent(sender, e);

        string key = this.textfield3.Text.Trim();
        SearchFolder(key);
        // Control control = this.NamingContainer.FindControl("UpdatePanelResult");

        //StringBuilder builder = new StringBuilder();

        //builder.Append("?key=" + key);
        //Page.Response.Redirect("~/Search/SearchFolderResult.aspx" + builder.ToString());

        // Edited by johnlu655 2011-03-12
        //if (control != null)
        //{
        //    GetData(key);
        //    BindGrid(control);
        //}
        //else
        //{
        //    //StringBuilder builder = new StringBuilder();
        //    builder.Append("?key=" + key);
        //    Page.Response.Redirect("~/Search/SearchFolderResult.aspx" + builder.ToString());

        //}
    }

    private void SearchFolder(string key)
    {
        StringBuilder builder = new StringBuilder();

        builder.Append("?key=" + key);
        Page.Response.Redirect("~/Search/SearchFolderResult.aspx" + builder.ToString());
    }

    private void SearchDocument(string docKey)
    {
        StringBuilder builder = new StringBuilder();

        builder.Append("?dockey=" + docKey);
        Page.Response.Redirect("~/Office/ManageDocument.aspx" + builder.ToString());
    }


    #region 数据绑定
    /// <summary>
    /// 获取数据源, edited by johnlu655 2011-01-15
    /// </summary>
    public void GetData(string key)
    {
        //commented by johnlu655 2011-01-15
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        //this.DataSource = FileFolderManagement.GetFileFolders(key, uid);

        string uid = Membership.GetUser().ProviderUserKey.ToString();
        if (this.Page.User.IsInRole("Offices"))
        {
            this.DataSource = FileFolderManagement.GetOfficeFileFolders(key, uid);
        }
        else if (this.Page.User.IsInRole("Users"))
        {
            DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(Membership.GetUser().ProviderUserKey.ToString());
            uid = dtOfficeUser.Rows[0]["OfficeUID"].ToString();

            this.DataSource = FileFolderManagement.GetFileFolders(key, uid);
        }
    }

    /// <summary>
    /// 初始化绑定
    /// </summary>
    public void BindGrid(Control updatpanel)
    {
        WebPager WebPager1 = updatpanel.FindControl("WebPager1") as WebPager;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();

        UpdatePanel panel = updatpanel as UpdatePanel;
        if (panel != null)
        {
            panel.Update();
        }

    }
    #endregion

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource"] as DataTable;
        }
        set
        {
            this.Session["DataSource"] = value;
        }
    }

    protected void searchBtn2_Click(object sender, EventArgs e)
    {
        string key = this.textfield.Text.Trim();
        SearchDocument(key);
    }
}