<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReminderControl.ascx.cs"
    Inherits="ReminderControl" %>

<h3>Reminders</h3>
<asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:Repeater ID="Repeater1" runat="server">
            <ItemTemplate>
                <p style="margin: 10px 0 10px">
                    <asp:Label ID="Label1" CssClass="reminder_tekst" Font-Bold="true" Text='<%# string.Format("{0}.",Eval("Number"))%>'
                        runat="server" />
                    <%--<asp:LinkButton ID="LinkButton1" CausesValidation="false" CssClass="reminder_tekst"
                                ForeColor="Blue" Font-Bold="true" runat="server" Text='<%# string.Format("{0}-{1} ",Eval("FirstName"),Eval("LastName"))%>'
                                OnClientClick='<%# string.Format("window.open(\"../EFileFolderV1.html?ID={0}\",\"eBook\",\"toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no\")",Eval("FolderID"))%>' />--%>
                           &nbsp;<asp:LinkButton CssClass="reminder_tekst" ID="LinkButton1" CausesValidation="false" ForeColor="Blue" Font-Bold="true" runat="server" Text='<%# string.Format("{0}",Eval("FolderName"))%>'
                               OnClientClick='<%#string.Format("window.open(\"WebFlashViewer.aspx?ID={0}\",\"\",\"toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no\")",Eval("FolderID"))%>' />
                    &nbsp;<asp:Label CssClass="reminder_tekst" ID="Label2" Font-Bold="true" Text='<%# string.Format("{0:MM.dd.yyyy}",Eval("DOB"))%>' runat="server" />
                </p>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <p style="margin: 10px 0 10px">
                    <asp:Label CssClass="reminder_tekst" ID="Label3" Font-Bold="true" Text='<%# string.Format("{0}.",Eval("Number"))%>' runat="server" />
                    &nbsp;<asp:LinkButton CssClass="reminder_tekst" ID="LinkButton2" CausesValidation="false"
                        ForeColor="Blue" Font-Bold="true" runat="server" Text='<%# string.Format("{0} ",Eval("FolderName"))%>'
                        OnClientClick='<%#string.Format("window.open(\"WebFlashViewer.aspx?ID={0}\",\"\",\"toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no\")",Eval("FolderID"))%>' />
                    &nbsp;<asp:Label ID="Label4" CssClass="reminder_tekst" Font-Bold="true" Text='<%# string.Format("{0:MM.dd.yyyy}",Eval("DOB"))%>'
                        runat="server" />
                </p>
            </AlternatingItemTemplate>
        </asp:Repeater>
    </ContentTemplate>
</asp:UpdatePanel>
<p>
    If you need purchase more EdFiles, please click
        <asp:HyperLink ID="HyperLink35" CssClass="foot_url2" runat="server" NavigateUrl="../Office/PurchasePage.aspx"
            Text="here"></asp:HyperLink>

</p>
