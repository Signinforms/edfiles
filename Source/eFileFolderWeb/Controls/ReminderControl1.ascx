<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ReminderControl1.ascx.cs"
    Inherits="ReminderControl1" %>

<h3>Reminders</h3>
<p>
    You have <strong style="color: Red">
        <asp:Label ID="labNumEFF" runat="server" Text=""></asp:Label></strong> EdFiles.
</p>
<p>
    (<strong><span style="color: #DC0087"><asp:Label ID="labUsedEFFNum" runat="server"
        Text=""></asp:Label>
        used.</span> <span style="color: #142561">
            <asp:Label ID="labUnUsedEFFNum" runat="server" Text=""></asp:Label>
            unused.</span> <span style="color: #00AEFF">
                <asp:Label ID="labUsedEFFSizes" runat="server" Text=""></asp:Label>
                MB used.</span></strong>)
</p>


    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Repeater ID="Repeater1" runat="server">
                <ItemTemplate>
                    <p style="margin:10px 0 10px">
                    <asp:Label ID="Label1" CssClass="reminder_tekst" Font-Bold="true" Text='<%# string.Format("{0}.",Eval("Number"))%>'
                        runat="server" />
                    <%--string.Format("window.open(\"WebFlashViewer.aspx?ID={0}--%>
                    <asp:LinkButton ID="LinkButton1" CausesValidation="false" CssClass="reminder_tekst"
                        ForeColor="Blue" Font-Bold="true" runat="server" Text='<%# string.Format("{0}",Eval("FolderName"))%>'
                        OnClientClick='<%#string.Format("window.open(\"/Office/WebFlashViewer.aspx?V=2&ID={0}\",\"\",\"toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no\")",Eval("FolderID"))%>' />
                    <asp:Label ID="Label2" CssClass="reminder_tekst" Font-Bold="true" Text='<%# string.Format("{0:MM.dd.yyyy}",Eval("DOB"))%>'
                        runat="server" />
                        </p>
                    
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <p style="margin:10px 0 10px">
                    <asp:Label ID="Label3" CssClass="reminder_tekst" Font-Bold="true" Text='<%# string.Format("{0}.",Eval("Number"))%>'
                        runat="server" />
                    <%--string.Format("window.open(\"WebFlashViewer.aspx?ID={0}--%>
                    <asp:LinkButton ID="LinkButton2" CausesValidation="false" CssClass="reminder_tekst"
                        ForeColor="Blue" Font-Bold="true" runat="server" Text='<%# string.Format("{0} ",Eval("FolderName"))%>'
                        OnClientClick='<%#string.Format("window.open(\"/Office/WebFlashViewer.aspx?V=2&ID={0}\",\"\",\"toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no\")",Eval("FolderID"))%>' />
                    <asp:Label ID="Label4" CssClass="reminder_tekst" Font-Bold="true" Text='<%# string.Format("{0:MM.dd.yyyy}",Eval("DOB"))%>'
                        runat="server" />
                        </p>
                   
                </AlternatingItemTemplate>
            </asp:Repeater>
        </ContentTemplate>
    </asp:UpdatePanel>

<%--<p>
    If you need purchase more EdFiles, please click
    <asp:HyperLink ID="HyperLink35" CssClass="foot_url2" runat="server" NavigateUrl="../Office/PurchasePage.aspx" Text="here"></asp:HyperLink>
</p>--%>
