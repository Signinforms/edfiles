using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ReminderControl1 : System.Web.UI.UserControl
{
    public int numEFF ;
    public int usedEFFNum;
    public int unUsedEFFNum;
    public float usedEFFSizes;
    string OfficeUserID = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if(!Page.IsPostBack)
        {
            if (Membership.GetUser()!=null)
            {
                OfficeUserID = Sessions.SwitchedRackspaceId;
                //if (this.Page.User.IsInRole("Offices"))
                //{
                //    OffcieUserID = Membership.GetUser().ProviderUserKey.ToString();
                //}

                //else if (this.Page.User.IsInRole("Users"))
                //{
                //    DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(Membership.GetUser().ProviderUserKey.ToString());
                //    OffcieUserID = dtOfficeUser.Rows[0]["OfficeUID"].ToString();
                //}

                BindTextData();
                BindData();
            }
            
        }
    }


    public void BindTextData()
    {
        RecentFolder recent = new RecentFolder();
        
        //numEFF = recent.GetNumEFF(OffcieUserID);

        DataTable dtNumEFF = UserManagement.GetNumByOfficeID(OfficeUserID);
        numEFF = Convert.ToInt32(dtNumEFF.Rows[0]["FileFolders"].ToString());
        usedEFFNum = recent.GetUsedEFFNum(OfficeUserID);
        int allUsedEFFNum = FileFolderManagement.GetAllUsedEFFNum(OfficeUserID);
        unUsedEFFNum = Convert.ToInt32(dtNumEFF.Rows[0]["FileFolders"].ToString()) - Convert.ToInt32(allUsedEFFNum);
        usedEFFSizes = recent.GetUsedEFFSizes(OfficeUserID);

        labNumEFF.Text = numEFF.ToString();
        labUsedEFFNum.Text = usedEFFNum.ToString();
        labUnUsedEFFNum.Text = unUsedEFFNum.ToString();
        labUsedEFFSizes.Text = string.Format("{0:F1}", usedEFFSizes);
    }

    public void BindData()
    {
        Repeater1.DataSource = RecentFolder.FoldertItems;
        Repeater1.DataBind();
    }
    public RecentFolder RecentFolder
    {
        get
        {
            RecentFolder recent = null;

            if (this.Session["RecentFolder"] == null)
            {
                //string uid = Membership.GetUser().ProviderUserKey.ToString();
                recent = new RecentFolder(OfficeUserID);
            }
            else
            {
                recent = this.Session["RecentFolder"] as RecentFolder;
            }

            return recent;
        }
        set { this.Session["RecentFolder"] = value; }
    }
}
