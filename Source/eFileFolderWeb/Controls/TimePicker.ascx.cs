using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
/// <summary>
/// Time Picker control has 4 public property Hour, Minute, AmPm and Time. This control
/// when added to a page renders 3 drpdown of Hour, Minute and AmPm.
/// The 'Time' property can be used directly to get a combined time attributes 
/// to be pushed into the DB.
/// TimePicker.Time returns the Time in 24Hour format. i.e 15:36:00
/// The TimePicker has 'set' property of 'Time' eg TimePickerTo.Time = "16:48:00" this will set the 
/// TimePicker dropdown to 04:48 PM 
/// This has inbuilt requiredfield Validators.
/// 
/// Created By : Adarsh Nair 
///              mail2adarsh@yahoo.com
/// </summary>
public partial class UserControl_TimePicker : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //BindHour();
            //BindMinutes();
        }
    }

    private string _Hour;
    private string _Minute;
    private string _AmPm;
    private string _Time;
    
    
    /// <summary>
    /// This attribute returns the combined Time attribute containing the 'Hour:Minute:00 Am/Pm'
    /// </summary>
    public string Time
    {
        get { 
                int intHH = 0;
                if(ddlAmPm.SelectedItem.Text == "PM")
                    if(ddlHour.SelectedItem.Text == "12")
                    intHH = 0;
                    else
                    intHH = Convert.ToInt32(ddlHour.SelectedItem.Text) + 12 ;
                else
                    intHH = Convert.ToInt32(ddlHour.SelectedItem.Text) ;
            return " " + intHH.ToString() + ":" + ddlMinute.SelectedItem.Text +":00 " ; }
        set {
            string[] arrTime = value.Split(':');
            bool IsAM = true;
            int intHH = Convert.ToInt32(arrTime[0]);
            if (intHH > 12)
            {
                IsAM = false;
                intHH = intHH - 12;
            }
            BindHour();
            BindMinutes();
            ddlHour.ClearSelection();
            ddlMinute.ClearSelection();
            ddlAmPm.ClearSelection();
            ddlHour.Items.FindByValue(intHH.ToString()).Selected = true;
            ddlMinute.Items.FindByValue(arrTime[1].ToString().Trim()).Selected = true;
            if(IsAM)
                ddlAmPm.Items.FindByValue("AM").Selected = true;
            else
                ddlAmPm.Items.FindByValue("PM").Selected = true;
           
            }
       
    }

    public string AmPm
    {
        get { return ddlAmPm.SelectedItem.Text; }
        set { _AmPm = value;  }
    }

    public string Minute
    {
        get { return ddlMinute.SelectedItem.Text; }
        set { _Minute = value; }
    }

    public string Hour
    {
        get { return ddlHour.SelectedItem.Text; }
        set { _Hour = value; }
    }

    public void BindHour() 
    {
        ddlHour.Items.Insert(0,new ListItem() { Text = "-", Value = "",Selected=true });
        for (int i = 1; i <= 12; i++)
            ddlHour.Items.Insert(i,new ListItem() { Text = i.ToString(), Value = i.ToString() });
        ddlHour.SelectedIndex = 0;
    }

    public void BindMinutes()
    {
        ddlMinute.Items.Insert(0, new ListItem() { Text = "-", Value = "", Selected = true });
        for (int i = 1; i <= 59; i++)
            ddlMinute.Items.Insert(i, new ListItem() { Text = i.ToString(), Value = i.ToString() });
        ddlMinute.SelectedIndex = 0;
    }
}
