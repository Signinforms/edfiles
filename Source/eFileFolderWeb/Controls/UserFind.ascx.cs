using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Framework.Controls;


public partial class Controls_UserFind : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string strKey = this.txtKey.Text;


        Control control = this.NamingContainer.Parent.FindControl("UpdatePanel1");
        //Control control = this.NamingContainer.FindControl("UpdatePanel1");


        GetUser(strKey);
        BindGrid(control);
    }

    #region 数据绑定
    public void GetUser(string key)
    {
        if (this.Page.User.IsInRole("Offices") || Sessions.SwitchedRackspaceId == Sessions.SwitchedRackspaceId)
        {
            string uid = Sessions.SwitchedSessionId;
            this.DataSource = UserManagement.GetUsersByOffice(key, uid);
        }
        if (this.Page.User.IsInRole("administrators"))
        {
            this.DataSource = UserManagement.GetUsers(key);
        }


    }

    /// <summary>
    /// 初始化绑定
    /// </summary>
    public void BindGrid(Control updatpanel)
    {
        WebPager WebPager1 = updatpanel.FindControl("WebPager1") as WebPager;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();

    }
    #endregion


    public DataTable DataSource
    {
        get
        {
            if (this.Page.User.IsInRole("Offices") || Sessions.SwitchedRackspaceId == Sessions.SwitchedRackspaceId)
            {
                return this.Session["DataSource"] as DataTable;
            }
            if (this.Page.User.IsInRole("administrators"))
            {
                return this.Session["DataSource_Users_Admin"] as DataTable;
            }
            return this.Session["DataSource"] as DataTable;
        }
        set
        {
            if (this.Page.User.IsInRole("Offices") || Sessions.SwitchedRackspaceId == Sessions.SwitchedRackspaceId)
            {
                this.Session["DataSource"] = value;
            }
            if (this.Page.User.IsInRole("administrators"))
            {
                this.Session["DataSource_Users_Admin"] = value;
            }
            this.Session["DataSource"] = value;
        }
    }
}
