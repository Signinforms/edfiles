<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ConversionProgram.aspx.cs" Inherits="ConversionProgram" Title="" %>

<%@ Register Src="~/Controls/RegisterDirectly.ascx" TagName="ucRegisterDirectly" TagPrefix="ucRegisterDirectly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="title-container aboutus-title">
        <div class="inner-wrapper">
            <h1>Option 3:</h1>
            <p>Storage Conversion Program. Exclusively from EdFiles Not just storage��storage conversion.</p>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="inner-wrapper">
        <div class="page-container">
            <div class="inner-wrapper">
                <div class="aboutus-contant">
                    <div class="calling-contant">
                        <ucRegisterDirectly:ucRegisterDirectly ID="ucRegister" runat="server" />

                        <p>
                            Very simple.  Instead of paying for storage fees year after year, 
                           why not use the same monthly fee towards the gradual scanning, backup and destruction.  
                           This way your monthly fee goes towards the conversion.   For a comfortable monthly budget, 
                           which you decide and depending on the volume of storage and how quickly you want to get it all scanned, 
                           the storage conversion program can be customized for your office.
                        </p>

                        <p>
                           <%-- To get started, Call 855-edFiles or 1-855-334-5336--%>                            
                          To get started, Call 1-657-217-3260
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
