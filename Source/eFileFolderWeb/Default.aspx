<%@ Page Language="C#" MasterPageFile="~/HomeMasterPage.master" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" Title="" %>

<%--<%@ Register TagPrefix="fjx" Namespace="com.flajaxian" Assembly="com.flajaxian.FileUploader" %>
<%@ Register TagPrefix="cust" Namespace="Shinetech.Engines.Adapters" Assembly="FormatEngine" %>
<%@ Import Namespace="System.Web.Configuration" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!-- Start Banner -->
    <section class="banner text-center">
			<div class="container">
				<div class="row">
					<div class="col-md-7">						
						<h2><span>Ed-Records Management,</span>Retention &amp; Compliance Solutions</h2>
						<img src="New/images/UpdatedHome/banner-img.png" alt="" class="banner-img">
					</div>

					<div class="col-md-5">
						<div class="banner-content-block has-shadow">
							<img src="New/images/UpdatedHome/logo-white.png" alt="" class="banner-logo">
							<h4>Is your Ed-Institution in Records Retention Compliance?</h4>
							<button class="btn btn-default btn-lg btnOpen" data-toggle="modal" data-target="#check-now-modal-details"><span>Check Now!</span></button>
						</div>

						<div class="banner-content-block">
							<img src="New/images/UpdatedHome/logo-ed-records.png" alt="" class="banner-logo">
							<h4>Does your Ed-Institution have a Permanent Records Retention Policy?</h4>
							<a onclick="LoadEdRecords();" class="btn btn-default btn-lg"><span>Check Now!</span></a>
						</div>
					</div>
				</div>
			</div>
		</section>
    <!-- End Banner -->

    <!-- Start Main Content -->
    <main class="content-block text-center">
			<div class="container">
				<p class="large-text pb50">We believe all of the permanent personnel, student &amp; administrative files that are vital for educators to serve students, employees and communities must be preserved &amp; backed up.  We further believe these records must be conveniently &amp; securely <b>searchable and accessible electronically</b> in a variety of ways.  And lastly, this endeavor must be available to education institutions of all sizes and budgets.</p>
				
				<!-- Start thumbnails -->				
				<div class="row thumbnail-outer">
					<div class="col-sm-6 col-md-3">
						<div class="thumbnail">
							<div class="thumbnail-icon icon-01"></div>
							<div class="caption">
								<h3>Convert</h3>
								<p>We have made converting paper file folders into EdFiles very simple. </p>
								<a href="StorageConversion.aspx" class="btn btn-default" role="button" title="Read More"><span>Read More</span></a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="thumbnail">
							<div class="thumbnail-icon icon-02"></div>							
							<div class="caption">
								<h3>Access</h3>
								<p>We firmly believe that, all the valuable and necessary information..</p>
								<a href="Access.aspx" class="btn btn-default" role="button" title="Read More"><span>Read More</span></a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="thumbnail">
							<div class="thumbnail-icon icon-03"></div>							
							<div class="caption">
								<h3>Calculator</h3>
								<p>Have you ever considered what it would cost if all your paper storage...</p>
								<a href="Estimator.aspx" class="btn btn-default" role="button" title="Read More"><span>Read More</span></a>
							</div>
						</div>
					</div>
					<div class="col-sm-6 col-md-3">
						<div class="thumbnail">
							<div class="thumbnail-icon icon-04"></div>							
							<div class="caption">
								<h3>Secure</h3>
								<p>Our Data Center covers all three critical security areas: physical security...</p>
								<a href="Security.aspx" class="btn btn-default" role="button" title="Read More"><span>Read More</span></a>
							</div>
						</div>
					</div>
				</div>
				<!-- End thumbnails -->				

				<img src="New/images/UpdatedHome/company-logos.jpg" alt="" class="company-logos">

			</div>
		</main>
    <!-- End Main Content -->

    <!-- Modal -->

    <div class="modal fade" id="check-now-modal-details" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document" style="max-width: 500px;">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="clearfix">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <img src="New/images/UpdatedHome/modal-close.png" alt=""></button>
                    </div>
                    <h3 class="bold-text">Organization Details</h3>
                    <%--<h5>Organization Details</h5>--%>
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <%--<div class="col-md-3">
                                    <span class="lblSpan">Organization Name</span><span class="redText">*</span>
                                </div>--%>
                                <div class="col-md-12">
                                    <input type="text" placeholder="organization name" class="form-control organizationName" onblur="ValidateOrganization()" onkeyup="ValidateOrganization()" />
                                    <%--<span id="requiredOrg" style="display: none;" class="redText">Organization Name is required !!</span>--%>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%--<div class="col-md-3">
                                    <span class="lblSpan">Phone Number</span><span class="redText">*</span>
                                </div>--%>
                                <div class="col-md-12">
                                    <input type="text" placeholder="phone number" class="form-control phoneNumber" onblur="ValidatePhoneNumber()" onkeyup="ValidatePhoneNumber()" />
                                    <%--<span id="requiredNumber" style="display: none;" class="redText">Phone number is required !!</span>
                                    <span id="validNumber" style="display: none;" class="redText">Please enter valid Phonenumber !!</span>--%>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%--<div class="col-md-3">
                                    <span class="lblSpan">Email</span><span class="redText">*</span>
                                </div>--%>
                                <div class="col-md-12">
                                    <input type="text" placeholder="email" class="form-control email" onblur="ValidateEmailAddress()" onkeyup="ValidateEmailAddress()" />
                                    <%--<span id="requiredEmail" style="display: none;" class="redText">Email is required !!</span>
                                    <span id="validEmail" style="display: none;" class="redText">Please enter valid email !!</span>--%>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <button type="button" class="btn btn-success btn-details" data-direction="right"><span>Next</span></button>
                    <hr>
                    <p class="notes">Please note, this is completely a confidential excercise, the responses will NOT be linked to you and your district under any circumstances. The objective is understand the extent of the risk.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="check-now-modal-1" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="clearfix">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <img src="New/images/UpdatedHome/modal-close.png" alt=""></button>
                    </div>
                    <h3 class="bold-text">Retention Compliance Check</h3>
                    <h5>Is there a designated Expert or a group that is responsible for Records Classification and Records Storage &amp; Inventory Maintenance at your district and or for inidividual departments?</h5>
                    <ul class="radio-list">
                        <li>
                            <label>
                                <input type="radio" name="radiolist-1" value="0"><span>Yes</span></label></li>
                        <li>
                            <label>
                                <input type="radio" name="radiolist-1" value="1" checked="checked"><span>No</span></label></li>
                        <li>
                            <label>
                                <input type="radio" name="radiolist-1" value="2"><span>Dont Know</span></label></li>
                    </ul>
                    <button type="button" class="btn btn-default btnNav" data-direction="left" data-dismiss="modal" data-target="#check-now-modal-details"><span>Previous</span></button>
                    &nbsp;&nbsp; 
                    <button type="button" class="btn btn-next btn-success btnNav" data-direction="right" data-dismiss="modal" data-target="#check-now-modal-2"><span>Next</span></button>
                    <hr>
                    <p class="notes">Please note, this is completely a confidential excercise, the responses will NOT be linked to you and your district under any circumstances. The objective is understand the extent of the risk.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="check-now-modal-2" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="clearfix">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <img src="New/images/UpdatedHome/modal-close.png" alt=""></button>
                    </div>
                    <h3 class="bold-text">Retention Compliance Check</h3>
                    <h5>In the event of a catastrophic fire, would your districts and or individual departments be able to ascertain the exact extent of what records were lost. Is there an accurate inventory of all records stored all around the district offices and other locations?</h5>
                    <ul class="radio-list">
                        <li>
                            <label>
                                <input type="radio" name="radiolist-2" value="0"><span>Yes</span></label></li>
                        <li>
                            <label>
                                <input type="radio" name="radiolist-2" value="1" checked="checked"><span>No</span></label></li>
                        <li>
                            <label>
                                <input type="radio" name="radiolist-2" value="2"><span>Dont Know</span></label></li>
                    </ul>

                    <button type="button" class="btn btn-default btnNav" data-direction="left" data-dismiss="modal" data-target="#check-now-modal-1"><span>Previous</span></button>
                    &nbsp;&nbsp;	
					<button type="button" class="btn btn-next btn-success btnNav" data-direction="right" data-dismiss="modal" data-target="#check-now-modal-3"><span>Next</span></button>
                    <hr>
                    <p class="notes">Please note, this is completely a confidential excercise, the responses will NOT be linked to you and your district under any circumstances. The objective is understand the extent of the risk.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="check-now-modal-3" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="clearfix">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <img src="New/images/UpdatedHome/modal-close.png" alt=""></button>
                    </div>
                    <h3 class="bold-text">Retention Compliance Check</h3>
                    <h5>Has the district, at any point, develop a Records Retention Compliance Policy which emphasize the ongoing classification of historical, current and future Records created by and or presented to the district?</h5>
                    <ul class="radio-list">
                        <li>
                            <label>
                                <input type="radio" name="radiolist-3" value="0"><span>Yes</span></label></li>
                        <li>
                            <label>
                                <input type="radio" name="radiolist-3" value="1" checked="checked"><span>No</span></label></li>
                        <li>
                            <label>
                                <input type="radio" name="radiolist-3" value="2"><span>Dont Know</span></label></li>
                    </ul>

                    <button type="button" class="btn btn-default btnNav" data-direction="left" data-dismiss="modal" data-target="#check-now-modal-2"><span>Previous</span></button>
                    &nbsp;&nbsp;	
					<button type="button" class="btn btn-success" data-direction="right" data-dismiss="modal" onclick="CheckComplicanceTest(this);"><span>Next</span></button>
                    <hr>
                    <p class="notes">Please note, this is completely a confidential excercise, the responses will NOT be linked to you and your district under any circumstances. The objective is understand the extent of the risk.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade right" id="check-now-modal-failure" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="clearfix">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <img src="New/images/UpdatedHome/modal-close.png" alt=""></button>
                    </div>
                    <h3 class="bold-text">Retention Compliance Check</h3>
                    <h4>It looks like your organization can use our assistance in developing a Retention Compliance Policy</h4>

                    <a href="ContactUs.aspx" class="btn btn-success"><span>Contact Us</span></a>
                    <hr>
                    <p class="notes">Please note, this is completely a confidential excercise, the responses will NOT be linked to you and your district under any circumstances. The objective is understand the extent of the risk.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade right" id="check-now-modal-success" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="clearfix">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <img src="New/images/UpdatedHome/modal-close.png" alt=""></button>
                    </div>
                    <h3 class="bold-text">Retention Compliance Check</h3>
                    <h4><span class="green-text">Congratulations!</span> It looks like you are taking the appropriate measures to comply with retention guidelines.  Please contact us for any additional assistance.  We can help you convert and backup paper records.</h4>

                    <a href="ContactUs.aspx" class="btn btn-success"><span>Contact Us</span></a>
                    <hr>
                    <p class="notes">Please note, this is completely a confidential excercise, the responses will NOT be linked to you and your district under any circumstances. The objective is understand the extent of the risk.</p>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        var isCompliace = true;
        $(document).ready(function () {
            //$('.btn-next').click(function () {
            //    if ($(this).parent().find("input:checked").val() != "0") {
            //        if (isCompliace)
            //            isCompliace = false;
            //    }
            //});

            $('.btn-details').click(function () {
                var isNameValid = ValidateOrganization();
                var isEmailValid = ValidateEmailAddress();
                var isPhoneValid = ValidatePhoneNumber();
                if (isNameValid && isEmailValid && isPhoneValid) {
                    $("#check-now-modal-details").prop('class', 'modal fade') // revert to default
                        .addClass("left");
                    $("#check-now-modal-details").modal("hide");
                    $("#check-now-modal-1").prop('class', 'modal fade') // revert to default
                        .addClass($(this).data('direction'));
                    $("#check-now-modal-1").modal("show");
                }
            });

            $('.btnOpen').click(function () {
                isCompliace = true;
                $("#check-now-modal-details").prop('class', 'modal fade');
                $("#check-now-modal-details").find("input").val("");
                $("#check-now-modal-details").find("input").removeClass('errorText');
            });

            $('.btnNav').click(function () {
                var nextDirection = $(this).data('direction');
                var prevDirection = '';
                if (nextDirection == "right") { prevDirection = "left"; }
                else { prevDirection = "right"; }
                $(this).parent().parent().parent().parent().prop('class', 'modal fade') // revert to default
                    .addClass(prevDirection);
                $($(this).data('target'))
                    .prop('class', 'modal fade') // revert to default
                    .addClass(nextDirection);
                $(this).parent().parent().parent().parent().modal('hide');
                $($(this).data('target')).modal('show');
            });
        });

        function ValidateOrganization() {
            if ($('.organizationName').val() == undefined || $('.organizationName').val() == '') {
                $('.organizationName').addClass('errorText');
                return false;
            }
            else {
                $('.organizationName').removeClass('errorText');
                return true;
            }
        }

        function ValidatePhoneNumber() {
            if ($('.phoneNumber').val() == undefined || $('.phoneNumber').val() == '') {
                $('.phoneNumber').addClass('errorText');
                return false;
            }
            else {
                var phoneno = /^\(?(\d{1})?[- ]?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
                if (phoneno.test($('.phoneNumber').val())) {
                    $('.phoneNumber').removeClass('errorText');
                    return true;
                }
                else {
                    $('.phoneNumber').addClass('errorText');
                    return false;
                }
            }
        }

        function ValidateEmailAddress() {
            if ($('.email').val() == undefined || $('.email').val() == '') {
                $('.email').addClass('errorText');
                return false;
            }
            else {
                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($('.email').val())) {
                    $('.email').removeClass('errorText');
                    return true;
                }
                else {
                    $('.email').addClass('errorText');
                    return false;
                }
            }
        }

        function LoadEdRecords() {
            window.open("https://EdRecords.org", "_blank");
        }

        function CheckComplicanceTest() {
            if ($("input[name=radiolist-1]:checked").val() != "0" || $("input[name=radiolist-2]:checked").val() != "0" || $("input[name=radiolist-3]:checked").val() != "0") {
                isCompliace = false;
            }
            $("#check-now-modal-3").prop('class', 'modal fade') // revert to default
                .addClass("left");
            $("#check-now-modal-3").modal("hide");
            if (isCompliace) {
                $("#check-now-modal-success").modal("show");
            }
            else {
                $("#check-now-modal-failure").modal("show");
            }
            $.ajax(
                {

                    type: "POST",
                    url: '../EFileFolderJSONService.asmx/ShareMailForComplianceTest',
                    data: "{orgName:'" + $('.organizationName').val() + "',phoneNumber:'" + $('.phoneNumber').val() + "',email:'" + $('.email').val() + "',step1:'" + $("input[name=radiolist-1]:checked").val() + "',step2:'" + $("input[name=radiolist-2]:checked").val() + "',step3:'" + $("input[name=radiolist-3]:checked").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                    },
                    fail: function (data) {
                    }
                });
        }

    </script>
</asp:Content>
