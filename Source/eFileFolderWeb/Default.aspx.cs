using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using com.flajaxian;
using Shinetech.DAL;
using Shinetech.Framework;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        AddMetas();
        if (!Page.IsPostBack)
        {
            //if (!FileUploader1.FileIsPosted)
            {

                BindData();
                if (Page.User.IsInRole("Offices") || Page.User.IsInRole("Users"))
                    Page.Response.Redirect("~/Office/Welcome.aspx");
                else if (Page.User.IsInRole("Administrators"))
                {
                    Page.Response.Redirect("~/Manager/Welcome.aspx");
                }
            }
        }
        else
        {
            BindData();
        }
    }

    protected virtual void AddMetas()
    {
        //Description
        HtmlMeta tag = new HtmlMeta();
        tag.Name = "Description";
        tag.Content = "EdFiles(tm) is an electronic filing system, that creates electronic file folders of documents that look, feel and work just like paper file folders!";
        Page.Header.Controls.Add(tag);

        tag = new HtmlMeta();
        tag.Name = "Keywords";
        tag.Content = "file folders, file organizer, folder organizer, document management, file management, paper management, filing system, records organizer, records management, filing software, electronic document management, paperless filing, paperless, electronic file storage, file storage, online storage.";
        Page.Header.Controls.Add(tag);

        tag = new HtmlMeta();
        tag.Name = "copyright";
        tag.Content = "(C) 2009 The Hotline Group, Inc.";
        Page.Header.Controls.Add(tag);

        tag = new HtmlMeta();
        tag.Name = "abstract";
        tag.Content = "EdFiles(tm) help eliminate and manage documents that are stored in filing cabinets and storage.";
        Page.Header.Controls.Add(tag);

        tag = new HtmlMeta();
        tag.Name = "robots";
        tag.Content = "index,all";
        Page.Header.Controls.Add(tag);

        tag = new HtmlMeta();
        tag.Name = "robot";
        tag.Content = "all";
        Page.Header.Controls.Add(tag);

        tag = new HtmlMeta();
        tag.Name = "audience";
        tag.Content = "all";
        Page.Header.Controls.Add(tag);

        tag = new HtmlMeta();
        tag.Name = "revisit";
        tag.Content = "8 days";
        Page.Header.Controls.Add(tag);

        tag = new HtmlMeta();
        tag.Name = "revisit-after";
        tag.Content = "8 days";
        Page.Header.Controls.Add(tag);

        tag = new HtmlMeta();
        tag.Name = "rating";
        tag.Content = "General";
        Page.Header.Controls.Add(tag);

        tag = new HtmlMeta();
        tag.Name = "Author";
        tag.Content = "The Hotline Group, Inc. - www.edfiles.com";
        Page.Header.Controls.Add(tag);

        tag = new HtmlMeta();
        tag.Name = "verify-v1";
        tag.Content = "jkt7rqM7h0G6bE3eu/mCioULJg9xLg/BhL/YNNjmTwo=";
        Page.Header.Controls.Add(tag);
    }

    public void BindData()
    {
        //   Repeater1.DataSource = Profile.LastEFFolders.FoldertItems;

        // Repeater1.DataBind();
    }

    protected void FileUploader1_OnFileReceived(object sender, FileReceivedEventArgs e)
    {
        byte[] rawdata = new byte[e.File.InputStream.Length];
        e.File.InputStream.Read(rawdata, 0, (int)e.File.InputStream.Length);
        MemoryStream ms = new MemoryStream(rawdata);

        try
        {
            XmlSerializer xmls = new XmlSerializer(typeof(XmlEFFolder));
            XmlEFFolder eff = xmls.Deserialize(ms) as XmlEFFolder;
            int id = eff.folders[0].id;

            FileFolder fFolder = new FileFolder(id);
            if (fFolder.IsExist)
            {
                FolderItem item = new FolderItem(id.ToString());
                item.FolderName = fFolder.FolderName.Value;
                item.FolderID = fFolder.FolderID.Value.ToString();
                item.DOB = DateTime.Now;
                Profile.LastEFFolders.AppendItem(item);
                Profile.Save();
            }

        }
        catch (Exception exp)
        {
            Console.Write(exp.Message);
        }
    }

}
