﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.pdf;
using net.openstack.Core.Domain;
using Shinetech.DAL;
using Newtonsoft.Json;
using System.Web.Script.Services;
using NLog;

public partial class EdFormsViewer : System.Web.UI.Page
{
    public static string PDFUrl = string.Empty;
    public static string ObjectName = string.Empty;
    public static string documentFullPath = string.Empty;
    public static string shareID = string.Empty;
    public static string fileName = string.Empty;
    public static bool IsSubmitted = false;
    protected string PDFData = "";
    public static string UID = string.Empty;
    public ViewerEditablePdf viewerEditablePdf = new ViewerEditablePdf();
    private readonly NLogLogger _logger = new NLogLogger();


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                //  UID = (string)Session["viewerID"];
                string errorMsg = string.Empty;
                shareID = QueryString.QueryStringDecode(Request.QueryString["data"]);
                hdnShareID.Value = shareID;
                errorMsg = GetFileInfo(shareID);
                errorMessageText.Value = errorMsg;
                if (string.IsNullOrEmpty(errorMsg))
                {
                    DocumentCollectionView docView = viewerEditablePdf.GetPDFData(documentFullPath);
                    docView.DocId = shareID;
                    PDFData = docView.PDFData;
                }
            }
        }
        catch (Exception ex)
        {
            _logger.Info(ex);
        }
    }

    private static string GetFileInfo(string shareID)
    {
        RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
        EdFormsViewer edForms = new EdFormsViewer();
        string errorMsg = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(shareID))
            {
                DataTable dt = General_Class.GetEdFormsUserShareById(shareID);
                if (dt.Rows.Count > 0)
                {
                    dt.AsEnumerable().ToList().ForEach(d =>
                    {
                        if (d.Field<byte>("Status") == (int)Enum_Tatva.EdFormsStatus.Submit)
                        {
                            IsSubmitted = true;
                            PDFUrl = string.Empty;
                        }
                        else
                        {
                            string IP = string.Empty;
                            try
                            {
                                IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                            }
                            catch (Exception ex)
                            {
                                edForms._logger.Info(ex);
                            }
                            General_Class.InsertIntoEdFormsUserLogs(shareID, (byte)Enum_Tatva.EdFormsLog.Open, IP);
                            shareID = d.Field<string>("EdFormsUserShareId").ToString();
                            if (d.Field<byte>("Status") == (int)Enum_Tatva.EdFormsStatus.UnOpen)
                            {
                                General_Class.ChangeEdFormsUserShareStatus(shareID, (int)Enum_Tatva.EdFormsStatus.Pending);
                            }
                            UID = d.Field<Guid>("UID").ToString();
                            fileName = d.Field<string>("FileName").ToString();
                            ObjectName = rackSpaceFileUpload.GeneratePath(fileName, Enum_Tatva.Folders.EdFormsShare, shareID.ToLower());
                            string newPathName = rackSpaceFileUpload.GetObject(ref errorMsg, ObjectName, UID, Enum_Tatva.Folders.EdFormsShare.GetHashCode());
                            documentFullPath = HttpContext.Current.Server.MapPath("~/" + Common_Tatva.RackSpaceEdFormsDownload + Path.DirectorySeparatorChar + UID + Path.DirectorySeparatorChar + newPathName);
                            PDFUrl = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.LastIndexOf('/')) + Common_Tatva.RackSpaceEdFormsDownload + "/" + UID + "/" + newPathName;
                        }
                    });
                }
                else
                {
                    errorMsg = "Not a valid EdForm";
                }
            }
        }
        catch (Exception ex)
        {
            edForms._logger.Error(ex);
            errorMsg = ex.Message;
        }
        return errorMsg;
    }

    /// <summary>
    /// Get all pdf inputs with it's value in list of PdfItems result. It will use iTextSharp to read pdf.
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    private List<PdfItems> GetPDFItems(string fileName)
    {
        string pdfTemplate = fileName;//Server.MapPath("~/pdfs/f1040ezt.pdf");

        MemoryStream memStream = new MemoryStream();
        using (FileStream fileStream = System.IO.File.OpenRead(pdfTemplate))
        {
            memStream.SetLength(fileStream.Length);
            fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
        }

        string newFile = fileName;
        PdfReader pdfReader = null;
        List<PdfItems> lstPdfItems = new List<PdfItems>();
        try
        {
            pdfReader = new PdfReader(memStream);
            AcroFields af = pdfReader.AcroFields;
            StringBuilder sb = new StringBuilder();

            foreach (var field in af.Fields)
            {
                if (Convert.ToString(field.Key) == "untitled46")
                {

                }
                PdfItems k = new PdfItems(lstPdfItems.Count, field.Key, af.GetField(Convert.ToString(field.Key)), af.GetFieldType(Convert.ToString(field.Key)));
                if (k.Type == (int)InputTypes.RADIO_BUTTON || k.Type == (int)InputTypes.CHECK_BOX || k.Type == (int)InputTypes.LIST_BOX || k.Type == (int)InputTypes.COMBO_BOX)
                {
                    try { k.AvailableValues.AddRange(GetCheckBoxExportValue(af, Convert.ToString(field.Key))); }
                    catch { }
                }
                else if (k.Type == (int)InputTypes.TEXT_FIELD)
                {
                    try
                    {
                        AcroFields.Item fieldItem = af.GetFieldItem(Convert.ToString(field.Key));
                        PdfDictionary pdfDictionary = (PdfDictionary)fieldItem.GetWidget(0);
                        int maxFieldLength = Int32.Parse(pdfDictionary.GetAsNumber(PdfName.MAXLEN).ToString());
                        k.PdfItemProperties.MaxLength = maxFieldLength;
                    }
                    catch (Exception ex)
                    {
                        _logger.Info(ex);
                    }
                }
                if (k.Type == (int)InputTypes.CHECK_BOX)
                {
                    string a = af.GetField(Convert.ToString(field.Key));
                }
                lstPdfItems.Add(k);

                if (Convert.ToString(field.Key) == "topmostSubform[0].Page1[0].Entity[0].p1-t4[0]" || Convert.ToString(field.Key) == "topmostSubform[0].Page1[0].f1_040_0_[0]")
                {

                }
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
        finally
        {
            if (pdfReader != null)
                pdfReader.Close();
            memStream.Close();
            memStream.Dispose();
        }
        return lstPdfItems;
    }

    public string[] GetCheckBoxExportValue(AcroFields fields, string cbFieldName)
    {
        AcroFields.Item fd = ((AcroFields.Item)fields.GetFieldItem(cbFieldName));
        Hashtable names = new Hashtable();
        string[] outs = new string[fd.GetValue(0).Length];
        PdfDictionary pd = ((PdfDictionary)fd.GetWidget(0)).GetAsDict(PdfName.AP);
        for (int k1 = 0; k1 < fd.GetValue(0).Length; ++k1)
        {
            PdfDictionary dic = (PdfDictionary)fd.GetWidget(k1);
            dic = dic.GetAsDict(PdfName.AP);
            if (dic == null)
                continue;
            dic = dic.GetAsDict(PdfName.N);
            if (dic == null)
                continue;
            foreach (PdfName pname in dic.Keys)
            {
                String name = PdfName.DecodeName(pname.ToString());
                if (name.ToLower() != "off")
                {
                    //k.AvailableValues.Add(name);
                    names[name] = null;
                    outs[(outs.Length - k1) - 1] = name;
                }
            }
        }
        return outs;
    }

    [WebMethod]
    public static string SavePdfData(string strDocumentCollectionView, ViewerEditablePdf.Base64String[] base64String)
    {
        Dictionary<bool, string> isSuccess = null;
        try
        {
            string reqFields = GetRequiredFields(strDocumentCollectionView, base64String);
            ViewerEditablePdf editablePdf = new ViewerEditablePdf();
            isSuccess = editablePdf.SavePdfData(strDocumentCollectionView, base64String, documentFullPath);
            if (isSuccess.Keys.FirstOrDefault())
            {
                isSuccess = UploadAndDeleteObject(UID, Enum_Tatva.EdFormsStatus.Save.GetHashCode(), strDocumentCollectionView);
            }
            else
            {
                General_Class.InsertEdFormsUserShareLogs(DateTime.Now, shareID, GetSavedValues(strDocumentCollectionView), false);
                return JsonConvert.SerializeObject((new Dictionary<bool, string> { { false, string.Empty } }).ToArray());
            }
            if (!string.IsNullOrEmpty(reqFields))
                return JsonConvert.SerializeObject((new Dictionary<bool, string> { { true, reqFields } }).ToArray());
            else
                return JsonConvert.SerializeObject((new Dictionary<bool, string> { { true, string.Empty } }).ToArray());
        }
        catch (Exception ex)
        {
            EdFormsViewer edFormsViwer = new EdFormsViewer();
            edFormsViwer._logger.Error(ex);
            General_Class.InsertEdFormsUserShareLogs(DateTime.Now, shareID, GetSavedValues(strDocumentCollectionView), true, ex.Message);
            return JsonConvert.SerializeObject((new Dictionary<bool, string> { { false, string.Empty } }).ToArray());
        }
    }

    [WebMethod]
    public static string SubmitPdfData(string strDocumentCollectionView, ViewerEditablePdf.Base64String[] base64String, bool isDownload)
    {
        Dictionary<bool, string> isSuccess = null;
        try
        {
            string reqFields = GetRequiredFields(strDocumentCollectionView, base64String);
            if (!string.IsNullOrEmpty(reqFields))
                return JsonConvert.SerializeObject((new Dictionary<bool, string> { { false, reqFields } }).ToArray());
            ViewerEditablePdf editablePdf = new ViewerEditablePdf();
            isSuccess = editablePdf.SavePdfData(strDocumentCollectionView, base64String, documentFullPath);
            if (isSuccess.Keys.FirstOrDefault())
            {
                isSuccess = UploadAndDeleteObject(UID, Enum_Tatva.EdFormsStatus.Submit.GetHashCode(), strDocumentCollectionView);
            }

            DataTable dtUsers = General_Class.GetEdFormsUserReminder(shareID);
            if (dtUsers != null && dtUsers.Rows.Count > 0)
            {
                foreach (DataRow dr in dtUsers.Rows)
                {
                    string toMail = dr.Field<string>("Email");
                    string toName = dr.Field<string>("FirstName");
                    string fileName = dr.Field<string>("FileName");
                    string submittedBy = dr.Field<string>("ToEmail");
                    FlashViewer.FlashViewer_SendReminderMail(toName, fileName, submittedBy, toMail);
                }
            }
            if (isDownload)
            {
                string urlPath = "ID=" + shareID + "&data=" + QueryString.QueryStringEncode("ID=" + fileName + "&folderID=" + (int)Enum_Tatva.Folders.EdFormsShare + "&sessionID=" + UID);
                return JsonConvert.SerializeObject((new Dictionary<bool, string> { { true, urlPath } }).ToArray());
            }

            return JsonConvert.SerializeObject((new Dictionary<bool, string> { { true, string.Empty } }).ToArray());
        }
        catch (Exception ex)
        {
            EdFormsViewer edFormsViwer = new EdFormsViewer();
            edFormsViwer._logger.Error(ex);
            return JsonConvert.SerializeObject((new Dictionary<bool, string> { { false, string.Empty } }).ToArray());
        }
    }

    public static string GetRequiredFields(string strDocumentCollectionView, ViewerEditablePdf.Base64String[] base64String)
    {
        try
        {
            List<PdfItems> documentCollectionView = new List<PdfItems>();
            documentCollectionView = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PdfItems>>(strDocumentCollectionView);
            FileInfo file = new FileInfo(documentFullPath);
            string pdfTemplate = file.FullName;
            PdfReader reader = new PdfReader(pdfTemplate);
            AcroFields form = reader.AcroFields;
            //AcroFields.Item item;
            PdfDictionary dict;
            PdfNumber flags;
            List<string> requiredFields = new List<string>();
            int index = 0;
            foreach (var field in form.Fields)
            {
                var item = documentCollectionView[index];
                if (item.Type != (int)InputTypes.SIGN_BOX)
                {
                    AcroFields.Item entry = reader.AcroFields.GetFieldItem(field.Key);
                    dict = entry.GetWidget(0);
                    flags = dict.GetAsNumber(PdfName.FF);
                    string value = item.Value;
                    if (field.Key.ToLower().Contains("signature"))
                    {
                        foreach (ViewerEditablePdf.Base64String str in base64String)
                        {
                            if (str.name.Equals(field.Key))
                                value = str.baseString;
                        }
                    }
                    if (flags != null && (flags.IntValue & BaseField.REQUIRED) > 0 && string.IsNullOrEmpty(value))
                    {
                        requiredFields.Add(field.Key);
                    }
                }
                index++;
            }
            reader.Close();
            if (requiredFields.Count > 0) return JsonConvert.SerializeObject(requiredFields);
            else return string.Empty;
        }
        catch (Exception ex)
        {
            EdFormsViewer edFormsViwer = new EdFormsViewer();
            edFormsViwer._logger.Error(ex);
            return string.Empty;
        }
    }

    private static string GetSavedValues(string strDocumentCollectionView)
    {
        try
        {
            Common_Tatva.WriteLogFomLog("0", "GetSavedValues", "Function entered");
            List<PdfItems> documentCollectionView = new List<PdfItems>();
            documentCollectionView = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PdfItems>>(strDocumentCollectionView);
            List<LogKeyValuePairNew> logs = new List<LogKeyValuePairNew>();
            foreach (PdfItems item in documentCollectionView)
            {
                if (!string.IsNullOrEmpty(item.Value))
                {
                    LogKeyValuePairNew logPair = new LogKeyValuePairNew();
                    if (item.FieldName.ToString().IndexOf('_') >= 0)
                        logPair.Key = item.FieldName.ToString().Substring(0, item.FieldName.ToString().IndexOf('_'));
                    else
                        logPair.Key = item.FieldName.ToString();
                    logPair.Value = item.Value;
                    logs.Add(logPair);
                }
            }
            Common_Tatva.WriteLogFomLog("0", "GetSavedValues", "Function exited");
            return JsonConvert.SerializeObject(logs);
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteLogFomLog("0", "GetSavedValues", "Catch : ErrorMessage : " + ex.Message);
            return string.Empty;
        }
    }
    /// <summary>
    /// Uploads saved data to rackspace
    /// </summary>
    /// <param name="uid"></param>
    private static Dictionary<bool, string> UploadAndDeleteObject(string uid, int status, string strDocumentCollectionView)
    {
        string savedValues = GetSavedValues(strDocumentCollectionView);
        try
        {
            EdFormsViewer edForms = new EdFormsViewer();
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            using (FileStream fileStream = new FileStream(documentFullPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None))
            {
                MemoryStream memoryStream = new MemoryStream();
                memoryStream.SetLength(fileStream.Length);
                fileStream.Read(memoryStream.GetBuffer(), 0, (int)fileStream.Length);
                string errorMsg = string.Empty;
                rackSpaceFileUpload.UploadObjectForViewer(ref errorMsg, ObjectName, memoryStream, uid);
                fileStream.Close();
                if (status == (int)Enum_Tatva.EdFormsStatus.Submit)
                    General_Class.ChangeEdFormsUserShareStatus(shareID, status);
                string IP = string.Empty;
                try
                {
                    IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                }
                catch (Exception ex)
                {
                    edForms._logger.Info(ex);
                }
                General_Class.InsertEdFormsUserShareLogs(DateTime.Now, shareID, savedValues, false);
                General_Class.InsertIntoEdFormsUserLogs(shareID, (byte)status == (byte)Enum_Tatva.EdFormsStatus.Submit ? (byte)Enum_Tatva.EdFormsLog.Submit : (byte)Enum_Tatva.EdFormsLog.Save, IP);
            }
            return new Dictionary<bool, string> { { true, string.Empty } };
        }
        catch (Exception ex)
        {
            EdFormsViewer edFormsViwer = new EdFormsViewer();
            edFormsViwer._logger.Error(ex);
            General_Class.InsertEdFormsUserShareLogs(DateTime.Now, shareID, savedValues, true, ex.Message);
            return new Dictionary<bool, string> { { false, ex.Message } };
        }
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        DownloadFile();
    }

    private void DownloadFile()
    {
        try
        {
            FileInfo file = new FileInfo(documentFullPath);
            if (file.Exists)
            {
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.WriteFile(file.FullName);
            }
            else
            {
                HttpContext.Current.Response.Write("This file does not exist.");
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// This method will resize image as per aspect ratio. Here we are passing maxHeight and maxWidth. Based on these params, it will find lowest value and resize it accordingly.
    /// </summary>
    /// <param name="original"></param>
    /// <param name="maxWidth"></param>
    /// <param name="maxHeight"></param>
    /// <returns></returns>
    public static Bitmap ScaleImage(Bitmap original, int maxWidth, int maxHeight)
    {
        int originalWidth = original.Width;
        int originalHeight = original.Height;

        // To preserve the aspect ratio
        float ratioX = (float)maxWidth / (float)originalWidth;
        float ratioY = (float)maxHeight / (float)originalHeight;
        float ratio = Math.Min(ratioX, ratioY);

        // New width and height based on aspect ratio
        int newWidth = (int)(originalWidth * ratio);
        int newHeight = (int)(originalHeight * ratio);

        // Convert other formats (including CMYK) to RGB.
        Bitmap newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format64bppArgb);

        //ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
        EncoderParameters myEncoderParameters = new EncoderParameters(1);
        myEncoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.ColorDepth, 32L);

        // Draws the image in the specified size with quality mode set to HighQuality
        using (Graphics graphics = Graphics.FromImage(newImage))
        {
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.CompositingQuality = CompositingQuality.HighQuality;
            graphics.PixelOffsetMode = PixelOffsetMode.HighSpeed;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            graphics.DrawImage(original, 0, 0, newWidth, newHeight);
        }
        return newImage;
    }
}