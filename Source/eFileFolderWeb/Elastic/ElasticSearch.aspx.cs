﻿// Ensure you have added the Elasticsearch.Net and Nest packages to your project
using System;
using System.Linq;
using System.Net;
using Elasticsearch.Net;
using Nest;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.IO;
using System.Collections.Generic;

public partial class ElasticSearch : System.Web.UI.Page
{
    //protected static IElasticClient _client;
    protected static tatvasoft archive = new tatvasoft
    {
        ArchiveId = 9,
        FileName = "soft.pdf",
        PathName = "folder/sub/tatva.pdf",
        FolderId = 5,
        Size = (float)0.01,
        JsonData = "s a field with large values, for example, the body of a blog post, but typically only another field is needed, for example, the title of the blog post. In this case, we don’t want to pay the cost of Elasticsearch deserializing the entire _soure just to get a small field."
    };

    protected void Page_Load(object sender, EventArgs e)
    {
        var hostString = "http://iad1-11274-0.es.objectrocket.com:11274,http://iad1-11274-1.es.objectrocket.com:11274,http://iad1-11274-2.es.objectrocket.com:11274,http://iad1-11274-3.es.objectrocket.com:11274";
        var hosts = hostString.Split(',').Select(x => new Uri(x)).ToArray();

        try
        {
            //var config = new ConnectionConfiguration(connectionPool)
            //  .BasicAuthentication("jenish", "jenish@123");
            //var lowClient = new ElasticLowLevelClient(config);
            var connectionPool = new StaticConnectionPool(hosts);
            var settings = new ConnectionSettings(connectionPool).DefaultMappingFor<tatvasoft>(i => i
                            .IndexName("tatvasoft")
                            .TypeName("pdf"))
                //.PrettyJson(true)
                //.RequestTimeout(TimeSpan.FromMinutes(2))
                //.DisableDirectStreaming(true)
                            .BasicAuthentication("jenish", "jenish@123");
            var highClient = new ElasticClient(settings);

            var searchResponse = highClient.Search<tatvasoft>(a => a
                .Query(q => q
                    .QueryString(qs => qs
                        .Query("*jenish*")
                        .Fields(fs => fs
                            .Fields(f1 => f1.FileName))
                            .Query("5").Fields(fs => fs.Fields(f1 => f1.FolderId)))
                    )
             );
            //var indexList = highClient.CatIndices();
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error: " + ex);
        }

        Console.WriteLine("Done!");
    }
}


public class tatvasoft
{
    public int ArchiveId { get; set; }
    public string FileName { get; set; }
    public string PathName { get; set; }
    public int FolderId { get; set; }
    public float Size { get; set; }
    public object JsonData { get; set; }
}

//var isExist = highClient.IndexExists(request);
//if (!isExist.Exists)
//{
//    var indexResponse = highClient.Index(archive, idx => idx.Index("tatvasoft").Id(archive.ArchiveId));
//}
//var request = new IndexExistsRequest("archivtest123");
//            var requestDelete = new DeleteIndexRequest("archivtest123");
//            if (highClient.IndexExists(request).Exists) 
//            {
//                highClient.DeleteIndex(requestDelete);
//            }


//var searchResponse = highClient.Search<tatvasoft>(a => a
//    .Query(q => q
//        .Bool(b => b
//            .Must(mu => mu
//                .Match(m => m
//                .Field(f => f.FolderId)
//                .Query("2")),
//                mu => mu
//                .Match(m => m
//                .Field(f => f.JsonData)
//                .Query("*fi*")))
//            )
//     ).DefaultOperator(DefaultOperator.And)
// );

//var indexResponse = _client.Index<StringResponse>("tatvasoft", "pdf", archive.ArchiveId.ToString(), PostData.Serializable(archive));
//string responseString = indexResponse.Body;
//var searchResponse = _client.Search<StringResponse>("tatvasoft", "pdf",);

//var indexName = "tatvasoft";
//var mustClauses = new List<QueryContainer>();
//var filterClauses = new List<QueryContainer>();

//mustClauses.Add(new TermQuery
//{
//    Field = new Field("FileName"),
//    Value = "soft"
//});

//filterClauses.Add(new TermQuery
//{
//    Field = new Field("FolderId"),
//    Value = "2"
//});

//var searchResponse = new SearchRequest<tatvasoft>(indexName, "pdf")
//{
//    From = 0,
//    Size = 10,
//    Query = new BoolQuery
//    {
//        Must = mustClauses,
//        Filter = filterClauses
//    }
//};
// create the index if it doesn't exist
//if (!_client.IndexExists("jenish").Exists)
//{
//    var indexCreated = _client.CreateIndex("jenish");
//}
//var myJson = new { hello = "world" };
//var index = _client.Index(myJson, i => i.Index("jenish").Type("pdf").Id(1));
//var searchRes = _client.Search<tatvasoft>(searchResponse);

//var searchRes = _client.Search<StringResponse>(PostData.Serializable(new
//{
//    from = 0,
//    size = 10,
//    query = new BoolQuery
//    {
//        Must = filterClauses,
//        Filter = mustClauses
//    }
//}));

//var successful = searchRes.IsValid;
//var responseJson = new JavaScriptSerializer().Serialize(searchRes.);
//var data = JsonConvert.DeserializeObject<tatvasoft>(responseJson);
//var res = _client.Get<StringResponse>("tatvasoft", "pdf", "1");


//var res = _client.Delete<StringResponse>("archivetest123", "size", "2");
//var allIndices = _client.CatIndices<StringResponse>();