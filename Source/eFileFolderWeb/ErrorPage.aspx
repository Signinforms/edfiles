﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ErrorPage.aspx.cs" Inherits="ErrorPage" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table height="353px" width="100%">
        <tr>
            <td align="center" height="50px"></td>
        </tr>
        <tr>
            <td align="center" valign="middle">
                <p class="podnaslov" style="font-size: medium; padding: 5px; background-color: #f6f5f2; text-align: center;">
                    Something went wrong. Please
                    <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Default.aspx" Text="click here" Style="color: #83af33; text-decoration: underline;" runat="server" />
                    to go back to home page.
                </p>
            </td>
        </tr>
        <tr>
            <td align="center" height="50px"></td>
        </tr>
    </table>
</asp:Content>


