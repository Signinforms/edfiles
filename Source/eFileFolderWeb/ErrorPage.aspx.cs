﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Utility;
using System.Text;

public partial class ErrorPage : System.Web.UI.Page
{
    private readonly NLogLogger _logger = new NLogLogger();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Server.GetLastError() != null)
                _logger.Info(Server.GetLastError().Message, Server.GetLastError());
            Server.ClearError();
        }

    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!this.Page.User.Identity.IsAuthenticated)
        {
            this.MasterPageFile = "~/LoginMasterPage.master";
        }
    }
}
