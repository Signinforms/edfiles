﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/HomeMasterPage.master" CodeFile="Estimator.aspx.cs" Inherits="Estimator" %>

<%@ Register Src="~/Controls/RegisterDirectly.ascx" TagName="ucRegisterDirectly" TagPrefix="ucRegisterDirectly" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />--%>
    <style type="text/css">
        h2 {
            padding-bottom: 0px !important;
        }

        p:not(.container) {
            font-size: 15px;
        }

        .mainContent {
            border: solid 1px lightgrey;
            border-radius: 25px;
            display: inline-block;
            padding: 20px;
            width: 65%;
        }

        .btnNext {
            text-align: right;
        }

        .btnRounded {
            border-radius: 25px;
        }

        .detailsTbl {
            width: 80%;
            display: inline-table;
        }

        .priceTbl {
            width: 80%;
            display: inline-table;
        }

        .dropRecords {
            margin-top: 10px;
        }

        .imgContainer {
            height: 100px !important;
            width: 100px !important;
        }

        .divContainer {
            text-align: center;
        }

        .loadingClass img {
            height: 100px;
        }

        .btn-back {
            float: left;
        }

        .reviewFrame {
            width: 100%;
            margin-top: 5%;
            margin-bottom: 5%;
            height: 1000px;
            border-radius: 25px;
        }

        .btnEmail {
            margin-left: 2%;
        }

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }

        #floatingCirclesG {
            top: 50%;
            left: 50%;
            position: fixed;
        }

        #floatingCirclesG {
            width: 128px;
            height: 128px;
            -webkit-transform: scale(0.6);
            -moz-transform: scale(0.6);
            -o-transform: scale(0.6);
            -ms-transform: scale(0.6);
        }

        .f_circleG {
            position: absolute;
            background-color: #f6f4f4;
            height: 23px;
            width: 23px;
            -moz-border-radius: 12px;
            -webkit-border-radius: 12px;
            border-radius: 12px;
            -webkit-animation-name: f_fadeG;
            -webkit-animation-duration: 1.04s;
            -webkit-animation-iteration-count: infinite;
            -webkit-animation-direction: linear;
            -moz-animation-name: f_fadeG;
            -moz-animation-duration: 1.04s;
            -moz-animation-iteration-count: infinite;
            -moz-animation-direction: linear;
            -o-animation-name: f_fadeG;
            -o-animation-duration: 1.04s;
            -o-animation-iteration-count: infinite;
            -o-animation-direction: linear;
            -ms-animation-name: f_fadeG;
            -ms-animation-duration: 1.04s;
            -ms-animation-iteration-count: infinite;
            -ms-animation-direction: linear;
        }

        #frotateG_01 {
            left: 0;
            top: 52px;
            -webkit-animation-delay: 0.39s;
            -moz-animation-delay: 0.39s;
            -o-animation-delay: 0.39s;
            -ms-animation-delay: 0.39s;
        }

        #frotateG_02 {
            left: 15px;
            top: 15px;
            -webkit-animation-delay: 0.52s;
            -moz-animation-delay: 0.52s;
            -o-animation-delay: 0.52s;
            -ms-animation-delay: 0.52s;
        }

        #frotateG_03 {
            left: 52px;
            top: 0;
            -webkit-animation-delay: 0.65s;
            -moz-animation-delay: 0.65s;
            -o-animation-delay: 0.65s;
            -ms-animation-delay: 0.65s;
        }

        #frotateG_04 {
            right: 15px;
            top: 15px;
            -webkit-animation-delay: 0.78s;
            -moz-animation-delay: 0.78s;
            -o-animation-delay: 0.78s;
            -ms-animation-delay: 0.78s;
        }

        #frotateG_05 {
            right: 0;
            top: 52px;
            -webkit-animation-delay: 0.91s;
            -moz-animation-delay: 0.91s;
            -o-animation-delay: 0.91s;
            -ms-animation-delay: 0.91s;
        }

        #frotateG_06 {
            right: 15px;
            bottom: 15px;
            -webkit-animation-delay: 1.04s;
            -moz-animation-delay: 1.04s;
            -o-animation-delay: 1.04s;
            -ms-animation-delay: 1.04s;
        }

        #frotateG_07 {
            left: 52px;
            bottom: 0;
            -webkit-animation-delay: 1.17s;
            -moz-animation-delay: 1.17s;
        }

        #frotateG_08 {
            left: 15px;
            bottom: 15px;
            -webkit-animation-delay: 1.3s;
            -moz-animation-delay: 1.3s;
            -o-animation-delay: 1.17s;
            -ms-animation-delay: 1.17s;
        }

        #frotateG_08 {
            left: 15px;
            bottom: 15px;
            -webkit-animation-delay: 1.3s;
            -moz-animation-delay: 1.3s;
            -o-animation-delay: 1.3s;
            -ms-animation-delay: 1.3s;
        }

        .redAlert {
            border-color: red;
        }

        @media only screen and (max-width: 1025px) {
            .detailsTbl {
                width: 100% !important;
            }

            .priceTbl {
                width: 80% !important;
            }

            .mainContent {
                width: 90%;
            }
        }

        @media only screen and (max-width: 768px) {
            .detailsTbl {
                width: 100% !important;
            }

            .mainContent {
                width: 100%;
                padding: 10px;
            }

            .btnNext {
                text-align: center;
            }

            .priceTbl {
                width: 100% !important;
            }

            .btn-details {
                margin-top: 20px;
            }

            .divContainer {
                text-align: left !important;
            }
        }

        @media only screen and (max-width: 479px) {
            .reqRightText {
                margin-top: 10%;
            }
        }
    </style>
    <div id="overlay" style="left: 0">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <section class="banner text-center">
			<div class="container">
				<div class="row">
					<div>						
						<h2>Ed-Records Conversion Estimation Tool from EdFiles</h2>
					</div>
				</div>
			</div>
    </section>
    <!-- Start Main Content -->
    <main class="content-block text-center">
        <div class="mainContent">
            <form runat="server" id="form1" enctype='multipart/form-data'>
                <div id="detailsForm">
                    <p class="large-text pb50">Give us 10 Minutes and we will help you BACKUP decades of permanent paper records!</p>
			        <div class="container">
				        <p class="notes">If you are like the over 80% of the School Districts, you are storing permanent paper records throughout the district offices and school sites.  At some point, these paper records will have to be scanned and backed up.  The process of converting these paper records to permanently backed up, searchable and scanned documents in our expertise.  We have made the estimating process and the logistics around the entire process easy to manage for district staff and hassle free!  Having scanned millions of records for all school district departments, we have put in a place a high-quality service process with qualified experts that focus on each department’s needs and retention compliance guidelines.</p>
			        </div>
                    <p class="large-text pb50"><span style="font-weight:bold">Instantly Estimate</span> what it will take to convert boxes and file cabinets full of paper files to easily searchable scanned documents!</p>
                    <div class="container" style="text-align:left !important">
				        <p class="notes">Please tell us who you are.  We will not spam you or sell your email address, our only business focus is to help school district backup paper records!  You will receive one call and one follow up email until you are ready!</p>
			        </div>
                    <table class="detailsTbl">
                        <tr>
                            <td>
                                <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <asp:DropDownList ID="dropState" DataTextField="StateName" DataValueField="StateID" placeholder="state" CssClass="form-control" runat="server">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <asp:TextBox ID="txtStreet" placeholder="street" CssClass="form-control street reqText reqRightText" runat="server"></asp:TextBox>
                                </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <asp:TextBox ID="txtCity" placeholder="city" CssClass="form-control city reqText" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <asp:TextBox ID="txtZipCode" placeholder="zip code" CssClass="form-control zipCode reqText reqRightText" runat="server"></asp:TextBox>
                                </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <asp:TextBox ID="txtSchool" placeholder="school district name" CssClass="form-control school reqText" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <asp:TextBox ID="txtName" placeholder="name" CssClass="form-control name reqText reqRightText" runat="server"></asp:TextBox>
                                </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <asp:TextBox ID="txtPosition" placeholder="position" CssClass="form-control position reqText" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <asp:TextBox ID="txtEmail" placeholder="email" CssClass="form-control email reqText reqRightText" runat="server"></asp:TextBox>
                                </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="row">
                                <div class="col-md-2 col-sm-2 col-xs-4">
                                    <asp:TextBox ID="txtExtension" placeholder="extension" CssClass="form-control" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <asp:TextBox ID="txtPhone" placeholder="phone number" CssClass="form-control phone reqText reqRightText" runat="server"></asp:TextBox>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12 btnNext">
                                    <button type="button" class="btn btn-success btn-nav btn-details btnRounded"><span>Next</span></button>
                                </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="priceDetails" style="display:none;">
                    <div class="container" style="text-align:left !important">
                        <p class="notes">(Please note, we recommend you do separate estimate for each type of records) For Example for Student CUMS, do one estimate, For Personnel Files, do a separate estimate, etc.) The time and effort associated with preparing and scanning different records types vary significantly.</p>
                    </div>
                    <table class="priceTbl">
                        <tr>
                            <td>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <asp:Label style="font-weight:bold;" runat="server">Records Type</asp:Label>
                                        <asp:DropDownList ID="dropRecords" DataTextField="Type" DataValueField="Id" placeholder="Records" CssClass="form-control dropRecords" runat="server">
                                        </asp:DropDownList>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                        <asp:TextBox ID="textOther" style="display:none;" placeholder="other details" CssClass="form-control" runat="server">
                                        </asp:TextBox>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                        <p>How many of the following you need scanned?(Don’t worry, even if the final number is not the same, you will be able to easily adjust the estimate)</p>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="col-md-4 col-sm-4 col-xs-12 divContainer">
                                    <asp:Image ImageUrl="images/standardBox.jpg" CssClass="imgContainer" ID="img1" runat="server"/>
                                    </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <p><span style="font-weight:bold">Standard Size Banker Boxes</span></p>
                                    </div>
                                <div class="col-md-2 col-sm-2 col-xs-6 divContainer">
                                    <asp:TextBox ID="txtStandardBox" placeholder="count" runat="server" CssClass="form-control onlyNumeric"></asp:TextBox>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="col-md-4 col-sm-4 col-xs-12 divContainer">
                                    <asp:Image ImageUrl="images/largeBox.jpg" CssClass="imgContainer" ID="img2" runat="server"/>
                                    </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <p><span style="font-weight:bold">Legal Size Banker Boxes</span></p>
                                    </div>
                                <div class="col-md-2 col-sm-2 col-xs-6 divContainer">
                                    <asp:TextBox ID="txtLargerBox" placeholder="count" runat="server" CssClass="form-control onlyNumeric"></asp:TextBox>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="col-md-4 col-sm-4 col-xs-12 divContainer">
                                    <asp:Image ImageUrl="images/verticalCabinet.jpg" CssClass="imgContainer" ID="img3" runat="server"/>
                                    </div>
                                <div class="col-md-6 col-sm-6 col-xs-9">
                                    <p><span style="font-weight:bold">Vertical File Cabinet Drawers</span>(For example if you have one 4 drawer file cabinet, then enter 4.  We are estimate the number of drawers, not cabinets.)</p>
                                    </div>
                                <div class="col-md-2 col-sm-2 col-xs-3 divContainer">
                                    <asp:TextBox ID="txtVerticalCabinet" placeholder="count" runat="server" CssClass="form-control onlyNumeric"></asp:TextBox>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="col-md-4 col-sm-4 col-xs-12 divContainer">
                                    <asp:Image ImageUrl="images/lateralCabinet.jpg" CssClass="imgContainer" ID="img4" runat="server"/>
                                    </div>
                                <div class="col-md-6 col-sm-6 col-xs-9">
                                    <p><span style="font-weight:bold">Lateral File Cabinet Drawers</span>For example if you have one 4 drawer file cabinet, then enter 4.  We are estimate the number of drawers, not cabinets.)</p>
                                    </div>
                                <div class="col-md-2 col-sm-2 col-xs-3 divContainer">
                                    <asp:TextBox ID="txtLateralCabinet" placeholder="count" runat="server" CssClass="form-control onlyNumeric"></asp:TextBox>
                                    </div>
                            </td>
                        </tr>
                        <%--<tr>
                            <td>
                                <div class="col-md-4 col-sm-4 col-xs-12 divContainer">
                                    <asp:Image ImageUrl="images/bluePrints.jpg" CssClass="imgContainer" ID="img5" runat="server"/>
                                    </div>
                                <div class="col-md-6 col-sm-6 col-xs-9">
                                    <p><span style="font-weight:bold">Blue Prints</span></p>
                                    </div>
                                <div class="col-md-2 col-sm-2 col-xs-3 divContainer">
                                    <asp:TextBox ID="txtBluePrints" placeholder="count" runat="server" CssClass="form-control onlyNumeric"></asp:TextBox>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="col-md-4 col-sm-4 col-xs-12 divContainer">
                                    <asp:Image ImageUrl="images/microfilmAndMicroficheRolls.jpg" CssClass="imgContainer" ID="img6" runat="server"/>
                                    </div>
                                <div class="col-md-6 col-sm-6 col-xs-9">
                                    <p><span style="font-weight:bold">Microfilm & Microfiche Rolls</span></p>
                                    </div>
                                <div class="col-md-2 col-sm-2 col-xs-3 divContainer">
                                    <asp:TextBox ID="txtMicroficheRolls" placeholder="count" runat="server" CssClass="form-control onlyNumeric"></asp:TextBox>
                                    </div>
                            </td>
                        </tr>--%>
                        <tr>
                            <td style="text-align:center;">
                                 <span style="font-weight:bold;font-size:17px;">Please call 1-657-217-3260 for a no cost consultation on the most effective solution for converting these to digital format.</span>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;">
                                <button type="button" class="btn btn-PrevDetails btn-default btnNav btnRounded"><span>Back</span></button>&nbsp;&nbsp;
					            <button type="button" class="btn btn-ConfirmDetails btn-success btnRounded"><span>Confirm</span></button>
                            </td>
                        </tr>
                 </table>
                </div>
                <div id="confirmDetails" style="display:none;">
                    <div class="row" style="text-align:center;">
                        <div class="container loadingClass">
                            <p class="notes">Please wait while we are estimating !!</p>
                            <img src="images/Spinner-1s-200px.gif" alt="" />
                        </div>
                        <div class="container reviewContent" style="display:none;">
                            <button type="button" class="btn btn-back btn-default btnNav btnRounded"><span>Back</span></button>
                            <asp:HiddenField runat="server" ID="hdnFileName" />
                            <h3>Here is your Estimate...</h3>
                            <embed class="reviewFrame" id="reviewFrame"></embed>
                            <div>
                                <button type="button" class="btn btn-default btnNav btnRounded" onclick="DownloadFile()"><span>Download</span></button>
                                <button type="button" class="btn btn-success btnRounded btnEmail" data-toggle="modal" data-target="#email-modal"><span>Email</span></button>
                                <asp:Button ID="hdnBtn" runat="server" OnClick="hdnBtn_Click" style="display:none" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="email-modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document" style="max-width: 500px;">
                        <div class="modal-content">
                            <div class="modal-body text-center">
                                <div class="clearfix">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <img src="New/images/UpdatedHome/modal-close.png" alt="">
                                    </button>
                                </div>
                                <h3 class="bold-text">Send Estimate</h3>
                                <table style="width: 100%">
                                    <tr>
                                        <td>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="email" class="form-control emailPop" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <button type="button" class="btn btn-success" onclick="SendMail()"><span>Send</span></button>
                                <hr>
                                <p class="notes">An Estimate will be sent to above given email address.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
			<img src="New/images/UpdatedHome/company-logos.jpg" alt="" class="company-logos">
        </div>
    </main>
    <!-- End Main Content -->

    <script type="text/javascript">
        $(document).ready(function () {
            $('.btn-details').click(function () {
                var isValid = true;
                $('.reqText').each(function () {
                    if ($(this).hasClass('email')) {
                        if (!ValidateEmail($(this))) isValid = false;
                    }
                    else if ($(this).hasClass('phone')) {
                        if (!ValidatePhone($(this))) isValid = false;
                    }
                    else {
                        if (!ValidateInput($(this))) isValid = false;
                    }
                });
                if (isValid) {
                    $("#priceDetails").show();
                    $("#detailsForm").hide();
                }
            });

            $('.btn-PrevDetails').click(function () {
                $("#priceDetails").hide();
                $("#detailsForm").show();
            });

            $('.btn-ConfirmDetails').click(function () {
                var isValid = false;
                $('.onlyNumeric').each(function () {
                    if ($(this).val() != '' && $(this).val() != null && $(this).val() != undefined) {
                        isValid = true;
                    }
                });
                if (isValid) {
                    $("#priceDetails").hide();
                    $("#confirmDetails").show();
                    GenerateEstimation();
                }
                else {
                    $('.onlyNumeric').addClass('redAlert');
                }
            });

            $('.btn-back').click(function () {
                $("#priceDetails").show();
                $("#confirmDetails").hide();
                $('.loadingClass').show();
                $('.reviewContent').hide();
            });

            $('.dropRecords').change(function () {
                if ($(this).val() == '9') {
                    $("#<%= textOther.ClientID %>").show();
                }
                else {
                    $("#<%= textOther.ClientID %>").hide();
                }
            });

            $('.onlyNumeric').keypress(function (evt) {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if ((charCode > 31
                    && (charCode < 48 || charCode > 57)) || $(this).val().length > 9)
                    return false;

                $('.onlyNumeric').removeClass('redAlert');
                return true;
            });

            $('.reqText').keyup(function (evt) {
                if ($(this).hasClass('email')) {
                    ValidateEmail($(this));
                }
                else if ($(this).hasClass('phone')) {
                    ValidatePhone($(this));
                }
                else {
                    ValidateInput($(this));
                }
            });
        });

        function ValidateInput(ele) {
            if (ele.val().length > 0) {
                ele.removeClass('redAlert');
                return true;
            }
            else {
                ele.addClass('redAlert');
                return false;
            }
        }

        function ValidateEmail(ele) {
            var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (!pattern.test(ele.val())) {
                ele.addClass('redAlert');
                return false;
            }
            else {
                ele.removeClass('redAlert');
                return true;
            }
        }

        function ValidatePhone(ele) {
            var pattern = /^\(?(\d{1})?[- ]?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
            if (!pattern.test(ele.val())) {
                ele.addClass('redAlert');
                return false;
            }
            else {
                ele.removeClass('redAlert');
                return true;
            }
        }

        function GenerateEstimation() {
            var estimatorDetails = {
                StateId: $("#<%= dropState.ClientID %>").val(),
                Street: $("#<%= txtStreet.ClientID %>").val(),
                City: $("#<%= txtCity.ClientID %>").val(),
                ZipCode: $("#<%= txtZipCode.ClientID %>").val(),
                SchoolName: $("#<%= txtSchool.ClientID %>").val(),
                Name: $("#<%= txtName.ClientID %>").val(),
                Position: $("#<%= txtPosition.ClientID %>").val(),
                Email: $("#<%= txtEmail.ClientID %>").val(),
                PhoneNumber: $("#<%= txtPhone.ClientID %>").val(),
                StdBoxCount: parseInt($("#<%= txtStandardBox.ClientID %>").val()),
                LargeBoxCount: parseInt($("#<%= txtLargerBox.ClientID %>").val()),
                LateralCabinetCount: parseInt($("#<%= txtLateralCabinet.ClientID %>").val()),
                VerticalCabinetCount: parseInt($("#<%= txtVerticalCabinet.ClientID %>").val()),
                <%--BluePrintsCount: parseInt($("#<%= txtBluePrints.ClientID %>").val()),--%>
                <%--MicroficheRollsCount: parseInt($("#<%= txtMicroficheRolls.ClientID %>").val()),--%>
                RecordTypeId: $("#<%= dropRecords.ClientID %>").val()
            };
            $.ajax(
                {
                    type: "POST",
                    url: 'Estimator.aspx/GenerateEstimation',
                    contentType: "application/json; charset=utf-8",
                    data: '{"estimatorDetails":' + JSON.stringify(estimatorDetails) + '}',
                    dataType: "json",
                    success: function (result) {
                        if (result != null && result.d != '' && result.d != null) {
                            var data = result.d;
                            data = $.parseJSON(data);
                            if (data.fileName != null && data.fileName != '') {
                                $('.loadingClass').hide();
                                $('.reviewContent').show();
                                var embed = document.getElementById("reviewFrame");
                                var clone = embed.cloneNode(true);
                                clone.setAttribute('src', "https://docs.google.com/gview?url=" + window.location.origin + "/" + data.fileName + "&embedded=true");
                                embed.parentNode.replaceChild(clone, embed)
                                //$('.reviewFrame').attr("src", "https://docs.google.com/gview?url=" + window.location.origin + "/" + data.fileName + "&embedded=true");
                                $("#<%= hdnFileName.ClientID %>").val(data.fileName);
                            }
                            else {
                                $("#confirmDetails").hide();
                                $("#detailsForm").show();
                                alert("Something went wrong. Please try again later.")
                            }
                        }
                        else {
                            $("#confirmDetails").hide();
                            $("#detailsForm").show();
                            alert("Something went wrong. Please try again later.")
                        }
                    },
                    fail: function () {
                        $("#confirmDetails").hide();
                        $("#detailsForm").show();
                        alert("Something went wrong. Please try again later.")
                    }
                });
        }

        function SendMail() {
            var name = $("#<%= txtName.ClientID %>").val();
            var email = $('.emailPop').val();
            var fileName = $("#<%= hdnFileName.ClientID %>").val();
            var isValid = ValidateEmail($('.emailPop'));
            if (isValid) {
                showLoader();
                $.ajax(
                    {
                        type: "POST",
                        url: 'Estimator.aspx/SendMail',
                        contentType: "application/json; charset=utf-8",
                        data: '{"name":"' + name + '","email":"' + email + '","fileName":"' + fileName + '"}',
                        dataType: "json",
                        success: function (result) {
                            if (result != null && result.d != '' && result.d != null) {
                                var data = result.d;
                                data = $.parseJSON(data);
                                hideLoader();
                                if (data.success) {
                                    $('.emailPop').val('');
                                    $("#email-modal").modal("hide");
                                    alert("An email with an attached proposal has been sent to the address provided.  Please call 1-657-217-3260 to discuss the details of this project");
                                }
                                else {
                                    alert("Something went wrong. Please try again later.")
                                }
                            }
                            else {
                                hideLoader();
                                alert("Something went wrong. Please try again later.")
                            }
                        },
                        fail: function () {
                            hideLoader();
                            alert("Something went wrong. Please try again later.")
                        }
                    });
            }
        }

        function DownloadFile() {
            $("#<%= hdnBtn.ClientID%>").click();
        }

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
        }
    </script>
</asp:Content>
