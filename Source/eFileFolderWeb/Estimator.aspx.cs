﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using Shinetech.DAL;
using Shinetech.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Estimator : System.Web.UI.Page
{
    /// <summary>
    /// The logger
    /// </summary>
    private readonly NLogLogger _logger = new NLogLogger();

    /// <summary>
    /// The template path
    /// </summary>
    private static readonly string TemplatePath = ConfigurationManager.AppSettings.Get("ProposalTemplatePath");

    /// <summary>
    /// The template name
    /// </summary>
    private static readonly string TemplateName = ConfigurationManager.AppSettings.Get("ProposalTemplate");

    /// <summary>
    /// The estimations path
    /// </summary>
    private static readonly string EstimationsPath = ConfigurationManager.AppSettings.Get("EstimationsPath");

    /// <summary>
    /// The image path
    /// </summary>
    private static readonly string ImagePath = ConfigurationManager.AppSettings.Get("ImagePath");

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            dropState.DataSource = UserManagement.GetStates();
            dropState.DataBind();
            dropState.SelectedValue = "CA";

            DataTable recordsTable = General_Class.GetRecordTypes();
            if (recordsTable != null && recordsTable.Rows.Count > 0)
            {
                DataRow dr = recordsTable.Select("Id = " + Enum_Tatva.RecordType.Default.GetHashCode()).FirstOrDefault();
                if (dr != null)
                    recordsTable.Rows.Remove(dr);
            }
            dropRecords.DataSource = recordsTable;
            dropRecords.DataBind();
        }
    }

    /// <summary>
    /// Generates the estimation.
    /// </summary>
    /// <param name="estimatorDetails">The estimator details.</param>
    /// <returns></returns>
    [WebMethod]
    public static string GenerateEstimation(EstimatorDetails estimatorDetails)
    {
        try
        {
            string fileName = FillPDFAndSave(estimatorDetails);
            return JsonConvert.SerializeObject(new { fileName = fileName });
        }
        catch (Exception ex)
        {
            Estimator estimator = new Estimator();
            estimator._logger.Error(ex);
            return string.Empty;
        }
    }

    /// <summary>
    /// Sends the mail.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <param name="email">The email.</param>
    /// <param name="fileName">Name of the file.</param>
    /// <returns></returns>
    [WebMethod]
    public static string SendMail(string name, string email, string fileName)
    {
        try
        {
            fileName = HostingEnvironment.MapPath("~/" + fileName);
            Common_Tatva.SendEstimatorFormEmail(name, fileName, email);
            return JsonConvert.SerializeObject(new { success = true });
        }
        catch (Exception ex)
        {
            return JsonConvert.SerializeObject(new { success = true });
        }
    }

    /// <summary>
    /// It has file name which we are going to fill with default vlaues also it has default values then it will just fill pdf contents with default values.
    /// </summary>
    /// <param name="estimatorDetails">The estimator details.</param>
    /// <returns></returns>
    public static string FillPDFAndSave(EstimatorDetails estimatorDetails)
    {
        string filename = EstimationsPath + "/" + estimatorDetails.SchoolName + "_" + EstimationsPath + "_" + DateTime.Now.ToString("hhssMMmmyyyy") + ".pdf";
        string templateFile = HostingEnvironment.MapPath("~/" + TemplatePath + "/" + TemplateName);
        string newFilename = HostingEnvironment.MapPath("~/" + filename);
        if (!Directory.Exists(Path.GetDirectoryName(newFilename)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(newFilename));
        }
        if (File.Exists(templateFile))
            File.Copy(templateFile, newFilename);
        else
            return null;

        DataTable pricing = General_Class.GetEstimatorPricing(estimatorDetails.RecordTypeId == Enum_Tatva.RecordType.Other_Files.GetHashCode() ?
            Enum_Tatva.RecordType.Default.GetHashCode() : estimatorDetails.RecordTypeId);
        if (pricing != null)
        {
            MemoryStream memStream = new MemoryStream();
            PdfReader pdfReader = null;
            PdfStamper pdfStamper = null;
            FileStream fs = null;
            FileStream fileStream = null;
            try
            {
                fileStream = System.IO.File.OpenRead(newFilename);

                memStream.SetLength(fileStream.Length);
                fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
                fileStream.Close();
                fileStream.Dispose();

                pdfReader = new PdfReader(memStream);
                fs = new FileStream(newFilename, FileMode.Create);
                pdfStamper = new PdfStamper(pdfReader, fs);
                Rectangle pagesize = pdfReader.GetPageSize(6);
                AcroFields pdfFormFields = pdfStamper.AcroFields;

                #region Append Table
                PdfPTable table = GenerateTable(estimatorDetails, pricing, pdfFormFields);
                ColumnText column = new ColumnText(pdfStamper.GetOverContent(6));
                Rectangle rectPage1 = new Rectangle(36, 36, 559, 670);
                column.SetSimpleColumn(rectPage1);
                column.AddElement(table);
                Rectangle rectPage2 = new Rectangle(36, 36, 559, 770);
                int status = column.Go();
                while (ColumnText.HasMoreText(status))
                {
                    status = TriggerNewPage(pdfStamper, pagesize, column, rectPage2, 7);
                }
                #endregion

                BaseFont font = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.EMBEDDED);
                foreach (string key in pdfFormFields.Fields.Keys)
                {
                    pdfFormFields.SetFieldProperty(key, "textsize", (float)12, null);
                    pdfFormFields.RegenerateField(key);
                }

                #region Set Fields
                pdfFormFields.SetField("date", DateTime.Now.ToString("MMMM dd, yyyy"));
                pdfFormFields.SetField("school1", estimatorDetails.SchoolName);
                pdfFormFields.SetField("street1", estimatorDetails.Street);
                pdfFormFields.SetField("address1", string.Join(", ", estimatorDetails.City, estimatorDetails.StateId + " " + estimatorDetails.ZipCode));
                pdfFormFields.SetField("name1", "Attn: " + estimatorDetails.Name);
                pdfFormFields.SetField("position1", estimatorDetails.Position);
                pdfFormFields.SetField("nameOnly", estimatorDetails.Name);
                pdfFormFields.SetField("count1", Convert.ToString(estimatorDetails.StdBoxCount > 0 ? estimatorDetails.StdBoxCount : 0));
                pdfFormFields.SetField("count2", Convert.ToString(estimatorDetails.LargeBoxCount > 0 ? estimatorDetails.LargeBoxCount : 0));
                pdfFormFields.SetField("count3", Convert.ToString(estimatorDetails.LateralCabinetCount > 0 ? estimatorDetails.LateralCabinetCount : 0));
                pdfFormFields.SetField("count4", Convert.ToString(estimatorDetails.VerticalCabinetCount > 0 ? estimatorDetails.VerticalCabinetCount : 0));
                //pdfFormFields.SetField("count5", Convert.ToString(estimatorDetails.BluePrintsCount > 0 ? estimatorDetails.BluePrintsCount : 0));
                //pdfFormFields.SetField("count6", Convert.ToString(estimatorDetails.MicroficheRollsCount > 0 ? estimatorDetails.MicroficheRollsCount : 0));
                pdfFormFields.SetField("school2", estimatorDetails.SchoolName);
                pdfFormFields.SetField("street2", estimatorDetails.Street);
                pdfFormFields.SetField("address2", string.Join(", ", estimatorDetails.City, estimatorDetails.StateId + " " + estimatorDetails.ZipCode));
                pdfFormFields.SetField("namePosition", "Attn: " + estimatorDetails.Name + ", " + estimatorDetails.Position);
                #endregion

                pdfStamper.FormFlattening = true;
                pdfStamper.Close();
                Common_Tatva.SendEstimatorFormEmail(estimatorDetails.Name, newFilename, estimatorDetails.Email, estimatorDetails.SchoolName, string.IsNullOrEmpty(estimatorDetails.Extension)
                    ? estimatorDetails.PhoneNumber : estimatorDetails.Extension + " - " + estimatorDetails.PhoneNumber,
                    string.Join(", ", estimatorDetails.Street, estimatorDetails.City, estimatorDetails.StateId + " " + estimatorDetails.ZipCode),
                    string.Join(", ", estimatorDetails.Name, estimatorDetails.Position), false);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            finally
            {
                if (pdfStamper != null)
                    pdfStamper.Close();
                if (pdfReader != null)
                    pdfReader.Close();
                memStream.Close();
                memStream.Dispose();
                if (fs != null)
                {
                    fs.Close();
                    fs.Dispose();
                }
                if (fileStream != null)
                {
                    fileStream.Close();
                    fileStream.Dispose();
                }
            }
        }
        return filename;
    }

    public static PdfPTable GenerateTable(EstimatorDetails estimatorDetails, DataTable pricing, AcroFields pdfFormFields)
    {
        PdfPTable table = new PdfPTable(3);
        table.AddCell("Types Of Files");
        table.AddCell("Estimated No. of Boxes and Drawers");
        table.AddCell("Approximate Cost Estimate");
        table.HeaderRows = 1;
        table.SetWidths(new int[] { 8, 8, 8 });
        decimal? totalEstimation = 0;
        for (int i = 1; i < 5; i++)
        {
            DataRow dr = pricing.Select("ContainerTypeId = " + i).FirstOrDefault();
            if (dr != null)
            {
                if ((i == Enum_Tatva.EstimatorPricing.StandardBox.GetHashCode() && estimatorDetails.StdBoxCount > 0)
                    || (i == Enum_Tatva.EstimatorPricing.LargeBox.GetHashCode() && estimatorDetails.LargeBoxCount > 0)
                    || (i == Enum_Tatva.EstimatorPricing.VerticalCabinet.GetHashCode() && estimatorDetails.VerticalCabinetCount > 0)
                    || (i == Enum_Tatva.EstimatorPricing.LateralCabinet.GetHashCode() && estimatorDetails.LateralCabinetCount > 0)
                    || (i == Enum_Tatva.EstimatorPricing.BluePrints.GetHashCode() && estimatorDetails.BluePrintsCount > 0)
                    || (i == Enum_Tatva.EstimatorPricing.MicrofilmAndMicroficheRolls.GetHashCode() && estimatorDetails.MicroficheRollsCount > 0))
                {
                    string containerValue = Enum_Tatva.GetEnumDescription((Enum_Tatva.EstimatorPricing)i);
                    string imageName = Convert.ToString((Enum_Tatva.EstimatorPricing)i);
                    Tuple<int?, string, decimal?> estimation = GetEstimationPrice(i, Convert.ToDecimal(dr["Price"]), estimatorDetails);
                    imageName = HostingEnvironment.MapPath("~/" + ImagePath + "/" + imageName + ".jpg");
                    iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(imageName);
                    image.ScaleAbsolute(100f, 100f);

                    table.AddCell(GenerateCell(containerValue, true, image));
                    table.AddCell(GenerateCell(estimation.Item1.ToString()));
                    table.AddCell(GenerateCell(estimation.Item2));

                    totalEstimation += estimation.Item3;
                }

                pdfFormFields.SetField("price" + i, "$ " + Convert.ToString(Convert.ToDecimal(dr["Price"])));
            }
        }
        totalEstimation += 180;

        table.AddCell(GenerateCell("Approximate Total"));
        table.AddCell(string.Empty);
        table.AddCell(GenerateCell("$" + totalEstimation));

        table.AddCell(GenerateCell("Total includes $180 in pickup fees", false));
        return table;
    }

    /// <summary>
    /// Generates the cell.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="image">The image.</param>
    /// <returns></returns>
    public static PdfPCell GenerateCell(string value, bool isBorder = true, iTextSharp.text.Image image = null)
    {
        PdfPCell cell = new PdfPCell();
        cell.VerticalAlignment = Element.ALIGN_MIDDLE;
        if (!isBorder)
        {
            cell.Border = Rectangle.NO_BORDER;
            cell.AddElement(new Paragraph(value, FontFactory.GetFont("Arial", 10, 23, BaseColor.GRAY)));
            cell.Colspan = 3;
            return cell;
        }
        if (image != null)
        {
            cell.AddElement(image);
        }
        Paragraph p = new Paragraph(value);
        p.Alignment = Element.ALIGN_CENTER;
        cell.AddElement(p);
        return cell;
    }

    /// <summary>
    /// Gets the estimation price.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="price">The price.</param>
    /// <param name="estimatorDetails">The estimator details.</param>
    /// <returns></returns>
    public static Tuple<int?, string, decimal?> GetEstimationPrice(int value, decimal price, EstimatorDetails estimatorDetails)
    {
        if (value == Enum_Tatva.EstimatorPricing.StandardBox.GetHashCode())
        {
            return Tuple.Create(estimatorDetails.StdBoxCount, "$" + price + " x " + estimatorDetails.StdBoxCount + " = " + price * estimatorDetails.StdBoxCount, price * estimatorDetails.StdBoxCount);
        }
        else if (value == Enum_Tatva.EstimatorPricing.LargeBox.GetHashCode())
        {
            return Tuple.Create(estimatorDetails.LargeBoxCount, "$" + price + " x " + estimatorDetails.LargeBoxCount + " = " + price * estimatorDetails.LargeBoxCount, price * estimatorDetails.LargeBoxCount);
        }
        else if (value == Enum_Tatva.EstimatorPricing.VerticalCabinet.GetHashCode())
        {
            return Tuple.Create(estimatorDetails.VerticalCabinetCount, "$" + price + " x " + estimatorDetails.VerticalCabinetCount + " = " + price * estimatorDetails.VerticalCabinetCount, price * estimatorDetails.VerticalCabinetCount);
        }
        else if (value == Enum_Tatva.EstimatorPricing.LateralCabinet.GetHashCode())
        {
            return Tuple.Create(estimatorDetails.LateralCabinetCount, "$" + price + " x " + estimatorDetails.LateralCabinetCount + " = " + price * estimatorDetails.LateralCabinetCount, price * estimatorDetails.LateralCabinetCount);
        }
        else if (value == Enum_Tatva.EstimatorPricing.BluePrints.GetHashCode())
        {
            return Tuple.Create(estimatorDetails.BluePrintsCount, "$" + price + " x " + estimatorDetails.BluePrintsCount + " = " + price * estimatorDetails.BluePrintsCount, price * estimatorDetails.BluePrintsCount);
        }
        else
        {
            return Tuple.Create(estimatorDetails.MicroficheRollsCount, "$" + price + " x " + estimatorDetails.MicroficheRollsCount + " = " + price * estimatorDetails.MicroficheRollsCount, price * estimatorDetails.MicroficheRollsCount);
        }
    }

    /// <summary>
    /// Triggers the new page.
    /// </summary>
    /// <param name="stamper">The stamper.</param>
    /// <param name="pagesize">The pagesize.</param>
    /// <param name="column">The column.</param>
    /// <param name="rect">The rect.</param>
    /// <param name="pagecount">The pagecount.</param>
    /// <returns></returns>
    public static int TriggerNewPage(PdfStamper stamper, Rectangle pagesize, ColumnText column, Rectangle rect, int pagecount)
    {
        stamper.InsertPage(pagecount, pagesize);
        PdfContentByte canvas = stamper.GetOverContent(pagecount);
        column.Canvas = canvas;
        column.SetSimpleColumn(rect);
        return column.Go();
    }

    protected void hdnBtn_Click(object sender, EventArgs e)
    {
        string fileName = hdnFileName.Value;
        fileName = HostingEnvironment.MapPath("~/" + fileName);
        string onlyFileName = Path.GetFileName(fileName);
        Response.AppendHeader("content-disposition", "attachment;filename=" + onlyFileName);
        Response.Charset = "";
        Response.ContentType = "application/pdf";
        Response.TransmitFile(fileName);
        Response.End();
    }
}