<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Estimator_.aspx.cs" Inherits="Estimator" Title="" %>

<%@ Register Src="~/Controls/RegisterDirectly.ascx" TagName="ucRegisterDirectly" TagPrefix="ucRegisterDirectly" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        il {
            font-size: 16px !important;
            line-height: normal !important;
            color: rgb(0,0,0);
            font-family: 'Open Sans', sans-serif;
        }

        td {
            padding: 1px;
        }

        .inputBox {
            font-size: initial;
            color: initial;
            font-size: 14px;
        }

        .ajax__validatorcallout_error_message_cell {
            line-height: normal;
            vertical-align: top
        }

        .link-head {
            height: 47px !important;
        }
    </style>
    <script type="text/javascript">

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-31985908-1']);
        _gaq.push(['_trackPageview']);

        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>

    <div class="title-container aboutus-title">
        <div class="inner-wrapper">
            <h1>Option 1:</h1>
            <p>SCAN EVERYTHING NOW��File Imaging & Scanning Service.</p>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="left-content">
                <div class="work_content">
                    <p>Have you ever considered what it would cost if all your paper storage was scanned away and backed up securely online or disks? See for yourself�</p>
                </div>
                <div class="form-containt listing-contant">
                    <div class="content-title">Paper to Electronic Conversion Estimator</div>
                    <div class="content-box listing-view">
                        <table style="width: 100%">
                            <tr>
                                <td style="width: 20%; vertical-align: middle">
                                    <img src="images/boxthumb.jpg" style="vertical-align: middle; width: 100%" />
                                </td>
                                <td style="width: 55%; vertical-align: middle">
                                    <il> No. of Std. Storage Boxes</il>
                                </td>
                                <td style="width: 24%; vertical-align: middle">
                                    <fieldset>
                                        <il>
                        <asp:TextBox runat="server" ID="TextSmallBox" Width="65" MaxLength="5" CssClass="inputBox"/> (box)</il>
                                        <asp:RegularExpressionValidator ID="SmallBoxV" ControlToValidate="TextSmallBox" runat="server"
                                            ErrorMessage="<b>Required Field Format</b><br />It is not a number format." ValidationExpression="^\d*"
                                            Display="None"></asp:RegularExpressionValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11"
                                            TargetControlID="SmallBoxV" HighlightCssClass="validatorCalloutHighlight" />
                                    </fieldset>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 20%; vertical-align: middle">
                                    <img src="images/largeboxthumb.jpg" style="vertical-align: middle; width: 100%" /></td>
                                <td style="width: 55%; vertical-align: middle">
                                    <il> No. of Large Storage Boxes </il>
                                </td>
                                <td style="width: 24%; vertical-align: middle">
                                    <fieldset>
                                        <il><asp:TextBox runat="server" ID="TextLargeBox" Width="65" MaxLength="5" CssClass="inputBox"/> (box)</il>
                                        <asp:RegularExpressionValidator ID="LargeBoxV" ControlToValidate="TextLargeBox" runat="server"
                                            ErrorMessage="<b>Required Field Format</b><br />It is not a number format." ValidationExpression="^\d*"
                                            Display="None"></asp:RegularExpressionValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12"
                                            TargetControlID="LargeBoxV" HighlightCssClass="validatorCalloutHighlight" />
                                    </fieldset>
                                </td>
                            </tr>

                            <tr>
                                <td style="width: 20%; vertical-align: middle">
                                    <img src="images/filedawerthumb.jpg" style="vertical-align: middle; width: 100%" />
                                </td>
                                <td style="width: 55%; vertical-align: middle">
                                    <il>Vertical File Cabinets. No of Drawers</il>
                                </td>
                                <td style="width: 24%; vertical-align: middle">
                                    <fieldset>
                                        <il><asp:TextBox runat="server" ID="TextCabinet" Width="65" MaxLength="5" CssClass="inputBox"/> (drawer)</il>
                                        <asp:RegularExpressionValidator ID="CabinetV" ControlToValidate="TextCabinet" runat="server"
                                            ErrorMessage="<b>Required Field Format</b><br />It is not a number format." ValidationExpression="^\d*"
                                            Display="None"></asp:RegularExpressionValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13"
                                            TargetControlID="CabinetV" HighlightCssClass="validatorCalloutHighlight" />
                                    </fieldset>
                                </td>
                            </tr>

                            <tr>
                                <td style="width: 20%; vertical-align: middle">
                                    <img src="images/lateralthumb.jpg" style="vertical-align: middle; width: 100%" />
                                </td>
                                <td style="width: 55%; vertical-align: middle">
                                    <il> Lateral File Cabinet drawers</il>
                                </td>
                                <td style="width: 24%; vertical-align: middle">
                                    <fieldset>
                                        <il><asp:TextBox runat="server" ID="TextDrawers" Width="65" MaxLength="5" CssClass="inputBox"/> (drawer)</il>
                                        <asp:RegularExpressionValidator ID="DrawersV" ControlToValidate="TextDrawers" runat="server"
                                            ErrorMessage="<b>Required Field Format</b><br />It is not a number format." ValidationExpression="^\d*"
                                            Display="None"></asp:RegularExpressionValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14"
                                            TargetControlID="DrawersV" HighlightCssClass="validatorCalloutHighlight" />
                                    </fieldset>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 20%; vertical-align: middle">
                                    <img src="images/shelvingthumb.jpg" style="vertical-align: middle; width: 100%" />
                                </td>
                                <td style="width: 55%; vertical-align: middle">
                                    <il> Open Shelves. How many total Horizontal feet</il>
                                    <br />
                                    <il>All the shelf space combine see picture</il>
                                </td>
                                <td style="width: 24%; vertical-align: middle">
                                    <fieldset>
                                        <il><asp:TextBox runat="server" ID="TextShelve" Width="65" MaxLength="5" CssClass="inputBox"/> (feet)
                                                    <asp:RegularExpressionValidator ID="ShelveV" ControlToValidate="TextShelve" runat="server"
                                                        ErrorMessage="<b>Required Field Format</b><br />It is not a number format."
                                                        ValidationExpression="^\d*" Display="None"></asp:RegularExpressionValidator>
                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15"
                                                        TargetControlID="ShelveV" HighlightCssClass="validatorCalloutHighlight" /></il>

                                    </fieldset>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 20%; vertical-align: middle">
                                    <img src="images/paperstackthumb.jpg" style="vertical-align: middle; width: 100%" />
                                </td>
                                <td style="width: 55%; vertical-align: middle">
                                    <il>Unbound loose Paper (per inch) How many inches</il>
                                </td>
                                <td style="width: 24%; vertical-align: middle">
                                    <fieldset>
                                        <il><asp:TextBox runat="server" ID="TextUnbound" Width="65" MaxLength="5" CssClass="inputBox"/> (inch) </il>
                                        <asp:RegularExpressionValidator ID="UnboundV" ControlToValidate="TextUnbound" runat="server"
                                            ErrorMessage="<b>Required Field Format</b><br />It is not a number format." ValidationExpression="^\d*"
                                            Display="None"></asp:RegularExpressionValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender16"
                                            TargetControlID="UnboundV" HighlightCssClass="validatorCalloutHighlight" />
                                    </fieldset>
                                </td>
                            </tr>

                        </table>
                    </div>
                    <div class="content-box listing-view" style="margin-top: 10px">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <fieldset>
                                    <label>Your Name</label>
                                    <asp:TextBox runat="server" ID="TextBoxName" MaxLength="50" CssClass="inputBox" />
                                    <asp:RequiredFieldValidator runat="server" ID="telN" ControlToValidate="textBoxName"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />Name is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                        TargetControlID="telN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />

                                </fieldset>
                                <fieldset>
                                    <label>Your Email</label>
                                    <asp:TextBox runat="server" ID="TextBoxEmail" MaxLength="50" CssClass="inputBox" />
                                    <asp:RequiredFieldValidator runat="server" ID="telEmail" ControlToValidate="TextBoxEmail"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />Email is required!" />
                                    <asp:RegularExpressionValidator ID="rev_Email" runat="server" ErrorMessage="<b>Required Field Missing</b><br />It is not an email format."
                                        ControlToValidate="TextBoxEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        Visible="true" Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8"
                                        TargetControlID="telEmail" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                        TargetControlID="rev_Email" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                </fieldset>
                                <fieldset>
                                    <label>Company Name</label>
                                    <asp:TextBox runat="server" ID="TextBoxCompany" CssClass="inputBox" />
                                </fieldset>
                                <fieldset>
                                    <label>Telephone</label>
                                    <asp:TextBox runat="server" ID="TextBoxTel" CssClass="inputBox" />
                                </fieldset>
                                <fieldset>
                                    <label>Country/Location</label>
                                    <asp:TextBox runat="server" ID="TextBoxLocation" CssClass="inputBox" />
                                </fieldset>
                                <fieldset>
                                    <label>&nbsp;</label>
                                    <asp:Button runat="server" Text="Calculate" ID="ButtonCalculate" OnClick="ButtonCalculate_Click" CssClass="btn btn-small green" />
                                </fieldset>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ButtonCalculate" EventName="Click" />

                            </Triggers>
                        </asp:UpdatePanel>

                        <div style="margin-top: 20px; font-size: 16px; line-height: normal">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:HiddenField ID="hidForModel" runat="server" />
                                    <ajaxToolkit:ModalPopupExtender ID="WarningModal" TargetControlID="hidForModel" runat="server"
                                        CancelControlID="btnWarning" DropShadow="true" PopupControlID="pnlIssues">
                                    </ajaxToolkit:ModalPopupExtender>
                                    <!-- Panel -->
                                    <asp:Panel ID="pnlIssues" runat="server" BorderColor="Black" BorderStyle="Solid"
                                        BorderWidth="1" BackColor="White" Width="450px" Height="220px">
                                        <div style="padding-left: 10px; padding-right: 10px; padding-top: 20px;">
                                            Dear <strong>
                                                <asp:Label ID="lblUserName" runat="server" /></strong><br />
                                        </div>
                                        <div style="padding-left: 10px; padding-right: 10px; padding-bottom: 15px; padding-top: 10px;">
                                            Thank you for your interest, you will be receiving an email shortly with the estimate.  Please call 657-217-3260 if it is not received within 5 minutes.<br />
                                            <br />
                                            Don�t forget to check your spam folder as well.
                                    <center>
                                        <div>
                                            <asp:ImageButton ID="btnWarning" runat="server" Style="margin-top:20px;" ImageUrl="~/images/close.gif" />
                                        </div>
                                    </center>
                                    </asp:Panel>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right-content">
                <ucRegisterDirectly:ucRegisterDirectly ID="ucRegister" runat="server" />
            </div>


        </div>
    </div>
    </div>

</asp:Content>
