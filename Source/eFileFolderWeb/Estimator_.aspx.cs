using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Utility;
using System.IO;

public partial class Estimator : System.Web.UI.Page
{
    private static readonly string AJIWANIEMAIL = ConfigurationManager.AppSettings["AJIWANIEMAIL"];
    private static readonly double ESTIMATORPRICE = Convert.ToDouble(ConfigurationManager.AppSettings["EstimatorPrice"]);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!this.Page.User.Identity.IsAuthenticated)
        {
            this.MasterPageFile = "~/LoginMasterPage.master";
        }
    }

    #region 


    protected void ButtonCalculate_Click(object sender, EventArgs e)
    {
        string userName = TextBoxName.Text.Trim();
        string userEmail = TextBoxEmail.Text.Trim();
        string companyName = TextBoxCompany.Text.Trim();
        string telNo = TextBoxTel.Text.Trim();
        string countryName = TextBoxLocation.Text.Trim();

        if (string.IsNullOrEmpty(userName))
        {
            string strWrongInfor = string.Format("Error : Your name can't be empty.");
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
        }

        if (string.IsNullOrEmpty(userEmail))
        {
            string strWrongInfor = string.Format("Error : Your email format isn't valid.");
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
        }

        try
        {
            double total = 0.0;
            string totalFormula = string.Empty;
            string smallboxStr = TextSmallBox.Text.Trim();
            if (!string.IsNullOrEmpty(smallboxStr))
            {
                total += int.Parse(smallboxStr) * 2100 * (ESTIMATORPRICE);
                totalFormula += smallboxStr + "(Small Box)";
            }

            string largeboxStr = TextLargeBox.Text.Trim();
            if (!string.IsNullOrEmpty(largeboxStr))
            {
                total += int.Parse(largeboxStr) * 4000 * (ESTIMATORPRICE);
                totalFormula += "+" + largeboxStr + "(Large Box)";
            }

            string cabinetStr = TextCabinet.Text.Trim();
            if (!string.IsNullOrEmpty(cabinetStr))
            {
                total += int.Parse(cabinetStr) * 3500 * (ESTIMATORPRICE);
                totalFormula += "+" + cabinetStr + "(Vertical Cabinet)";
            }

            string drawersStr = TextDrawers.Text.Trim();
            if (!string.IsNullOrEmpty(drawersStr))
            {
                total += int.Parse(drawersStr) * 5500 * (ESTIMATORPRICE);
                totalFormula += "+" + drawersStr + "(Lateral Cabinet)";
            }

            string shelveStr = TextShelve.Text.Trim();
            if (!string.IsNullOrEmpty(shelveStr))
            {
                total += int.Parse(shelveStr) * 1500 * (ESTIMATORPRICE);
                totalFormula += "+" + shelveStr + "(Open Shelve)";
            }

            string unboundStr = TextUnbound.Text.Trim();
            if (!string.IsNullOrEmpty(unboundStr))
            {
                total += int.Parse(unboundStr) * 150 * (ESTIMATORPRICE);
                totalFormula += "+" + unboundStr + "(Unbound Paper)";
            }

            if (total < 0.001)
            {
                string strWrongInfor = string.Format("Error : Your result is zero.");
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
            }
            else
            {
                IFormatProvider formatProvider = new CultureInfo("en-US", false);
                string totalCurrency = string.Format(formatProvider, "{0:C}", total);

                sendEstimatorFormEmail(userName, totalCurrency, userEmail, totalFormula, companyName, telNo, countryName);

                lblUserName.Text = userName;
                //  lblWarning.Text = totalCurrency;
                UpdatePanel3.Update();
                WarningModal.Show();
            }
        }
        catch (Exception exp)
        {
            string strWrongInfor = exp.Message;
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
        }

    }

    private void sendEstimatorFormEmail(string userName, string result, string userEmail, string formula,
        string company, string tel, string country)
    {
        string strMailTemplet = getFileFormMailTemplate();
        string strMailTempletPartner = getParnterFileFormMailTemplate();

        strMailTemplet = strMailTemplet.Replace("[FirstName]", userName);
        strMailTemplet = strMailTemplet.Replace("[result]", result);

        strMailTempletPartner = strMailTempletPartner.Replace("[FirstName]", userName);
        result += "= " + formula;
        strMailTempletPartner = strMailTempletPartner.Replace("[result]", result);

        strMailTempletPartner = strMailTempletPartner.Replace("[Email]", userEmail);
        strMailTempletPartner = strMailTempletPartner.Replace("[Company]", company);
        strMailTempletPartner = strMailTempletPartner.Replace("[Tel]", tel);
        strMailTempletPartner = strMailTempletPartner.Replace("[Country]", country);

        string strEmailSubject = "Calculator From EdFiles";

        //Email.SendEstimatorFromEFileFolder(userEmail, strEmailSubject, strMailTemplet, strMailTempletPartner);

    }


    private string getFileFormMailTemplate()
    {

        string strMailTemplet = string.Empty;
        string physicalPath = Request.PhysicalPath;
        int index = physicalPath.LastIndexOf("\\");
        string mailTempletUrl = physicalPath.Substring(0, index + 1) + "PaperCalculator.htm";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    private string getParnterFileFormMailTemplate()
    {

        string strMailTemplet = string.Empty;
        string physicalPath = Request.PhysicalPath;
        int index = physicalPath.LastIndexOf("\\");
        string mailTempletUrl = physicalPath.Substring(0, index + 1) + "PaperCalculator4Partner.htm";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    #endregion
}
