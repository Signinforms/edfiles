<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:template match="/" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
		<HTML>
			<HEAD>
				<STYLE>
					.stdPVTblLCell {
					background-color: #00a7e7;
					color: white;
					font-weight: bold;
					text-align: left;
					padding-left: 4px;
					padding-top: 4px;
					padding-bottom: 4px;
					width: 100%;
					font-size: 12pt;
					}
					.stdPageHdr {
					color: DarkBlue;
					font-weight: bold;
					font-style:italic;
					font-family:Verdana;
					text-align: left;
					padding-left: 4px;
					padding-top: 4px;
					padding-bottom: 4px;
					width: 100%;
					font-size: 20pt;
					}
					.gridHeader {
					background-color: #C0C0C0;
					color: DarkBlue;
					font-size: 9pt;
					font-weight: bold;
					font-family:Verdana;
					vertical-align:middle;
					text-align:center;
					border: solid thin Black;
					}
					.totalHeader {
					background-color: #CCFFCC;
					color: DarkBlue;
					font-size: 9pt;
					font-weight: bold;
					font-family:Verdana;
					vertical-align:middle;
					text-align:right;
					}
					.SearchHeader {
					color: DarkBlue;
					font-size: 9pt;
					font-weight: bold;
					font-family:Verdana;
					}
					.SearchKey {
					color: DarkBlue;
					font-size: 9pt;
					vertical-align:middle;
					text-align:right;
					font-family:Verdana;
					}
					.SearchValue {
					color: Black;
					font-size: 9pt;
					font-weight: bold;
					vertical-align:middle;
					text-align:left;
					font-family:Verdana;
					}
					.SearchResultHeader {
					background-color: #CCFFCC;
					color: DarkBlue;
					font-size: 9pt;
					font-weight: bold;
					font-family:Verdana;
					}
					.SearchResultItem {
					background-color: #CCFFFF;
					color: Black;
					font-size: 8pt;
					font-family:Verdana;
					border: solid thin Black;
					}
					.SearchResultAltItem {
					background-color: #99CCFF;
					color: Black;
					font-size: 8pt;
					font-family:Verdana;
					border: solid thin Black;
					}
				</STYLE>
			</HEAD>
			<BODY>
				<TABLE width="38%">
					<TR>
						<TD/>
					</TR>
					<TR>
						<TD class="stdPageHdr">Signature Details</TD>
					</TR>
					<TR>
						<TD colspan="4">
							<TABLE width="99%">
								<TR>
									<TD width="24%" class="gridHeader">
							Id						</TD>
							<TD width="20%" class="gridHeader">
						  Print Name					</TD>
									<TD width="19%" class="gridHeader">
							SignIn					</TD>
									<TD width="20%" class="gridHeader">Questions</TD>
							<TD width="17%" class="gridHeader">
							CheckIn</TD>
							</TR>
								<xsl:for-each select="NewDataSet/Table1">
									<xsl:choose>
										<xsl:when test="position() mod 2 = 1">
											<TR>
												<TD class="SearchResultItem">
													<xsl:value-of select="SignatureID"/>
												</TD>
												<TD class="SearchResultItem">
													<xsl:value-of select="PrintName"/>
												</TD>
												<TD class="SearchResultItem">
													<xsl:value-of select="FormatDateTime"/>
												</TD>
												<TD class="SearchResultItem">
													<xsl:value-of select="QuestionQty"/>
												</TD>
												<TD class="SearchResultItem">
													<xsl:value-of select="FormatCheckInDate"/>
												</TD>
											</TR>
										</xsl:when>
										<xsl:otherwise>
											<TR>
												<TD class="SearchResultAltItem">
													<xsl:value-of select="SignatureID"/>
												</TD>
												<TD class="SearchResultAltItem">
													<xsl:value-of select="PrintName"/>
												</TD>
												<TD class="SearchResultAltItem">
													<xsl:value-of select="FormatDateTime"/>
												</TD>
												<TD class="SearchResultAltItem">
													<xsl:value-of select="QuestionQty"/>
												</TD>
												<TD class="SearchResultAltItem">
													<xsl:value-of select="FormatCheckInDate"/>
												</TD>
											</TR>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
							</TABLE>
						</TD>
					</TR>
				</TABLE>
			</BODY>
		</HTML>
	</xsl:template>
</xsl:stylesheet>
