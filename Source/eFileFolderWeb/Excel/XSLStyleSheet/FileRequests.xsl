<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:template match="/" xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:dt="urn:schemas-microsoft-com:datatypes">
    <HTML>
      <HEAD>
        <STYLE>
          .stdPVTblLCell {
          background-color: #00a7e7;
          color: white;
          font-weight: bold;
          text-align: left;
          padding-left: 4px;
          padding-top: 4px;
          padding-bottom: 4px;
          width: 100%;
          font-size: 12pt;
          }
          .stdPageHdr {
          color: DarkBlue;
          font-weight: bold;
          font-style:italic;
          font-family:Verdana;
          text-align: left;
          padding-left: 4px;
          padding-top: 4px;
          padding-bottom: 4px;
          width: 100%;
          font-size: 20pt;
          }
          .gridHeader {
          background-color: #C0C0C0;
          color: DarkBlue;
          font-size: 9pt;
          font-weight: bold;
          font-family:Verdana;
          vertical-align:middle;
          text-align:center;
          border: solid thin Black;
          }
          .totalHeader {
          background-color: #CCFFCC;
          color: DarkBlue;
          font-size: 9pt;
          font-weight: bold;
          font-family:Verdana;
          vertical-align:middle;
          text-align:right;
          }
          .SearchHeader {
          color: DarkBlue;
          font-size: 9pt;
          font-weight: bold;
          font-family:Verdana;
          }
          .SearchKey {
          color: DarkBlue;
          font-size: 9pt;
          vertical-align:middle;
          text-align:right;
          font-family:Verdana;
          }
          .SearchValue {
          color: Black;
          font-size: 9pt;
          font-weight: bold;
          vertical-align:middle;
          text-align:left;
          font-family:Verdana;
          }
          .SearchResultHeader {
          background-color: #CCFFCC;
          color: DarkBlue;
          font-size: 9pt;
          font-weight: bold;
          font-family:Verdana;
          }
          .SearchResultItem {
          background-color: #CCFFFF;
          color: Black;
          font-size: 8pt;
          font-family:Verdana;
          border: solid thin Black;
          }
          .SearchResultAltItem {
          background-color: #99CCFF;
          color: Black;
          font-size: 8pt;
          font-family:Verdana;
          border: solid thin Black;
          }
        </STYLE>
      </HEAD>
      <BODY>
        <TABLE width="38%">
          <TR>
            <TD/>
          </TR>
          <TR>
            <TD class="stdPageHdr">File Request Summary</TD>
          </TR>
          <TR>
            <TD colspan="4">
              <TABLE width="99%">
                <TR>
                  <TD width="10%" class="gridHeader">Id</TD>
                  <TD width="10%" class="gridHeader">OfficeName</TD>
                  <TD width="10%" class="gridHeader">Firstname</TD>
                  <TD width="10%" class="gridHeader">Lastname</TD>
                  <TD width="10%" class="gridHeader">FileNumber</TD>
                  <TD width="10%" class="gridHeader">RequestedBy</TD>
                  <TD width="20%" class="gridHeader">RequestDate</TD>
                  <TD width="20%" class="gridHeader">ConvertedDate</TD>                
                  <TD width="5%" class="gridHeader">Status</TD>
                </TR>
                <xsl:for-each select="NewDataSet/FileForm">
                  <xsl:choose>
                    <xsl:when test="position() mod 2 = 1">
                      <TR>
                        <TD class="SearchResultItem">
                          <xsl:value-of select="FileID"/>
                        </TD>
                        <TD class="SearchResultItem">
                          <xsl:value-of select="OfficeName"/>
                        </TD>
                        <TD class="SearchResultItem">
                          <xsl:value-of select="FirstName"/>
                        </TD>
                        <TD class="SearchResultItem">
                          <xsl:value-of select="LastName"/>
                        </TD>                        
                        <TD class="SearchResultItem">
                          <xsl:value-of select="FileNumber"/>
                        </TD>                        
                        <TD class="SearchResultItem">
                          <xsl:value-of select="RequestedBy"/>
                        </TD>
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="FormatRequestDate"/>
                        </TD>
                        <TD class="SearchResultItem">
                          <xsl:value-of select="FormatConvertedDate"/>
                        </TD>                        
                        <TD class="SearchResultItem">
                          <xsl:value-of select="Status"/>
                        </TD>
                      </TR>
                    </xsl:when>
                    <xsl:otherwise>
                      <TR>
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="FileID"/>
                        </TD>
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="OfficeName"/>
                        </TD>
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="FirstName"/>
                        </TD>
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="LastName"/>
                        </TD> 
                        <TD class="SearchResultItem">
                          <xsl:value-of select="FileNumber"/>
                        </TD>                      
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="RequestedBy"/>
                        </TD>
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="FormatRequestDate"/>
                        </TD>
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="FormatConvertedDate"/>
                        </TD>                       
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="Status"/>
                        </TD>
                      </TR>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </TABLE>
            </TD>
          </TR>
          <TR>
            <TD class="stdPageHdr">Storage File Request Summary</TD>
          </TR>
          <TR>
            <TD colspan="4">
              <TABLE width="99%">
                <TR>
                  <TD width="10%" class="gridHeader">Id</TD>
                  <TD width="10%" class="gridHeader">OfficeName</TD>
                  <TD width="10%" class="gridHeader">FirstName</TD>
                  <TD width="10%" class="gridHeader">LastName</TD>
                  <TD width="10%" class="gridHeader">FileName</TD>
                  <TD width="10%" class="gridHeader">FileNumber</TD>
                  <TD width="10%" class="gridHeader">BoxName</TD>
                  <TD width="20%" class="gridHeader">RequestDate</TD>
                  <TD width="5%" class="gridHeader">Status</TD>
                </TR>
                <xsl:for-each select="NewDataSet/PagerFile">
                  <xsl:choose>
                    <xsl:when test="position() mod 2 = 1">
                      <TR>
                        <TD class="SearchResultItem">
                          <xsl:value-of select="FileID"/>
                        </TD>
                        <TD class="SearchResultItem">
                          <xsl:value-of select="Name"/>
                        </TD>
                        <TD class="SearchResultItem">
                          <xsl:value-of select="FirstName"/>
                        </TD>
                        <TD class="SearchResultItem">
                          <xsl:value-of select="LastName"/>
                        </TD>
                        <TD class="SearchResultItem">
                          <xsl:value-of select="FileName"/>
                        </TD>
                        <TD class="SearchResultItem">
                          <xsl:value-of select="FileNumber"/>
                        </TD>
                        <TD class="SearchResultItem">
                          <xsl:value-of select="BoxName"/>
                        </TD>
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="FormatRequestDate"/>
                        </TD>
                        <TD class="SearchResultItem">
                          <xsl:value-of select="Status"/>
                        </TD>
                      </TR>
                    </xsl:when>
                    <xsl:otherwise>
                      <TR>
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="FileID"/>
                        </TD>
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="Name"/>
                        </TD>
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="FirstName"/>
                        </TD>
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="LastName"/>
                        </TD>
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="FileName"/>
                        </TD>
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="FileNumber"/>
                        </TD>
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="BoxName"/>
                        </TD>
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="FormatRequestDate"/>
                        </TD>
                        <TD class="SearchResultAltItem">
                          <xsl:value-of select="Status"/>
                        </TD>
                      </TR>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </TABLE>
            </TD>
          </TR>
        </TABLE>
      </BODY>
    </HTML>
  </xsl:template>
</xsl:stylesheet>
