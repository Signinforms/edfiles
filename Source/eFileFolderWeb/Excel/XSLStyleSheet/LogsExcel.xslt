<?xml version="1.0" encoding="Windows-1252" ?>
<xsl:stylesheet version="1.0" xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" >
  <xsl:output method="xml" version="1" indent="yes"/>
  <xsl:decimal-format name="us" decimal-separator='.' grouping-separator=',' infinity='-' NaN='-' />
  <xsl:template match="/" xmlns:ms="urn:schemas-microsoft-com:xslt">
    <xsl:processing-instruction name="mso-application">
      <xsl:text>progid="Excel.Sheet"</xsl:text>
    </xsl:processing-instruction>
    <Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"  xmlns:ms="urn:schemas-microsoft-com:xslt" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">
      <Styles>
        <Style ss:ID="Default" ss:Name="Normal">
          <Font ss:Bold="0" ss:FontName="Calibri" x:Family="Swiss" ss:Size="9" ss:Color="#000000" />
          <Borders />
          <Alignment ss:Horizontal="Left" ss:Vertical="Top" ss:WrapText="1" />
          <Interior ss:Color="#FFFFFF" ss:Pattern="Solid" />
          <NumberFormat ss:Format="General" />
        </Style>
        <Style ss:ID="Default2">
          <Font ss:Bold="0" ss:FontName="Calibri" x:Family="Swiss" ss:Size="9" ss:Color="#000000" />
          <Borders>
            <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#000000"/>
            <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#000000"/>
            <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#000000"/>
            <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1" ss:Color="#000000"/>
          </Borders>
          <Alignment ss:Horizontal="Center" ss:Vertical="Middle" ss:WrapText="1" />
          <Interior ss:Color="#FFFFFF" ss:Pattern="Solid" />
          <NumberFormat ss:Format="General" />
        </Style>
        <Style ss:ID="LHead" ss:Parent="Default2">
          <Font ss:Bold="1" ss:FontName="Calibri" x:Family="Swiss" ss:Size="9" ss:Color="#FFFFFF" />
          <Alignment ss:Horizontal="Center" ss:Vertical="Middle" ss:WrapText="1" />
          <Interior ss:Color="#0083BE" ss:Pattern="Solid" />
        </Style>
        <Style ss:ID="RHead" ss:Parent="Default2">
          <Font ss:Bold="1" ss:FontName="Calibri" x:Family="Swiss" ss:Size="9" ss:Color="#FFFFFF" />
          <Alignment ss:Horizontal="Right" ss:Vertical="Top" ss:WrapText="1" />
          <Interior ss:Color="#0083BE" ss:Pattern="Solid" />
        </Style>
        <Style ss:ID="Num" ss:Parent="Default2">
          <Alignment ss:Horizontal="Right" ss:Vertical="Top" />
          <NumberFormat ss:Format="_(* #,##0.00_);_(* \(#,##0.00\);_(* &quot;-&quot;??_);_(@_)" />
        </Style>
        <Style ss:ID="Date" ss:Parent="Default2">
          <NumberFormat ss:Format="dd\-mmm\-yyyy" />
        </Style>
        <Style ss:ID="NumBold" ss:Parent="Num">
          <Font ss:Bold="1" ss:FontName="Calibri" x:Family="Swiss" ss:Size="9" ss:Color="#000000" />
        </Style>
        <Style ss:ID="DefBold" ss:Parent="Default2">
          <Font ss:Bold="1" ss:FontName="Calibri" x:Family="Swiss" ss:Size="9" ss:Color="#000000" />
        </Style>
      </Styles>
      <Worksheet ss:Name="Folder Logs">
        <Table ss:ExpandedColumnCount="4" x:FullColumns="0" x:FullRows="0">
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <!--Header Section-->
          <Row ss:AutoFitHeight="1">
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">User</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Folder Name</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Action Date</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Action</Data>
            </Cell>
          </Row>
          <!--Report Section-->
          <xsl:for-each select="NewDataSet/Table">
            <Row ss:AutoFitHeight="1">
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="User"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="FolderName"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="ms:format-date(CreatedDate, 'yyyy-MM-dd')"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="ActionName"/>
                </Data>
              </Cell>
            </Row>
          </xsl:for-each>
        </Table>
        <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
          <PageSetup>
            <Header x:Margin="0.25" x:Data="&amp;R Date: &amp;D Time: &amp;T"/>
            <Footer x:Margin="0.25" x:Data="&amp;L Private and Confidential &amp;R Page &amp;P of &amp;N"/>
            <Layout x:Orientation="Landscape"/>
            <PageMargins x:Bottom="0.5" x:Left="0.5" x:Right="0.5" x:Top="0.5"/>
          </PageSetup>
          <Print>
            <ValidPrinterInfo/>
            <VerticalResolution>0</VerticalResolution>
          </Print>
          <Selected/>
          <FreezePanes/>
          <FrozenNoSplit/>
          <SplitHorizontal>1</SplitHorizontal>
          <TopRowBottomPane>1</TopRowBottomPane>
          <DoNotDisplayGridlines/>
        </WorksheetOptions>
      </Worksheet>
      <Worksheet ss:Name="Divider Logs">
        <Table ss:ExpandedColumnCount="5" x:FullColumns="0" x:FullRows="0">
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <!--Header Section-->
          <Row ss:AutoFitHeight="1">
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">User</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Folder Name</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Divider Name</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Action Date</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Action</Data>
            </Cell>
          </Row>
          <!--Report Section-->
          <xsl:for-each select="NewDataSet/Table1">
            <Row ss:AutoFitHeight="1">
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="User"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="FolderName"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="DividerName"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="ms:format-date(CreatedDate, 'yyyy-MM-dd')"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="ActionName"/>
                </Data>
              </Cell>
            </Row>
          </xsl:for-each>
          <!--Summary Section-->
        </Table>
        <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
          <PageSetup>
            <Header x:Margin="0.25" x:Data="&amp;R Date: &amp;D Time: &amp;T"/>
            <Footer x:Margin="0.25" x:Data="&amp;L Private and Confidential &amp;R Page &amp;P of &amp;N"/>
            <Layout x:Orientation="Landscape"/>
            <PageMargins x:Bottom="0.5" x:Left="0.5" x:Right="0.5" x:Top="0.5"/>
          </PageSetup>
          <Print>
            <ValidPrinterInfo/>
            <VerticalResolution>0</VerticalResolution>
          </Print>
          <Selected/>
          <FreezePanes/>
          <FrozenNoSplit/>
          <SplitHorizontal>1</SplitHorizontal>
          <TopRowBottomPane>1</TopRowBottomPane>
          <DoNotDisplayGridlines/>
        </WorksheetOptions>
      </Worksheet>
      <Worksheet ss:Name="Document Logs">
        <Table ss:ExpandedColumnCount="6" x:FullColumns="0" x:FullRows="0">
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <!--Header Section-->
          <Row ss:AutoFitHeight="1">
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">User</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Folder Name</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Divider Name</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Document Name</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Action Date</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Action</Data>
            </Cell>
          </Row>
          <!--Report Section-->
          <xsl:for-each select="NewDataSet/Table2">
            <Row ss:AutoFitHeight="1">
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="User"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="FolderName"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="DividerName"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="DocumentName"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="ms:format-date(CreatedDate, 'yyyy-MM-dd')"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="ActionName"/>
                </Data>
              </Cell>
            </Row>
          </xsl:for-each>
          <!--Summary Section-->
        </Table>
        <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
          <PageSetup>
            <Header x:Margin="0.25" x:Data="&amp;R Date: &amp;D Time: &amp;T"/>
            <Footer x:Margin="0.25" x:Data="&amp;L Private and Confidential &amp;R Page &amp;P of &amp;N"/>
            <Layout x:Orientation="Landscape"/>
            <PageMargins x:Bottom="0.5" x:Left="0.5" x:Right="0.5" x:Top="0.5"/>
          </PageSetup>
          <Print>
            <ValidPrinterInfo/>
            <VerticalResolution>0</VerticalResolution>
          </Print>
          <Selected/>
          <FreezePanes/>
          <FrozenNoSplit/>
          <SplitHorizontal>1</SplitHorizontal>
          <TopRowBottomPane>1</TopRowBottomPane>
          <DoNotDisplayGridlines/>
        </WorksheetOptions>
      </Worksheet>
      <Worksheet ss:Name="FolderLog Logs">
        <Table ss:ExpandedColumnCount="5" x:FullColumns="0" x:FullRows="0">
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <!--Header Section-->
          <Row ss:AutoFitHeight="1">
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">User</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Folder Name</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Log Name</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Action Date</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Action</Data>
            </Cell>
          </Row>
          <!--Report Section-->
          <xsl:for-each select="NewDataSet/Table3">
            <Row ss:AutoFitHeight="1">
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="User"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="FolderName"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="LogName"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="ms:format-date(CreatedDate, 'yyyy-MM-dd')"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="ActionName"/>
                </Data>
              </Cell>
            </Row>
          </xsl:for-each>
          <!--Summary Section-->
        </Table>
        <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
          <PageSetup>
            <Header x:Margin="0.25" x:Data="&amp;R Date: &amp;D Time: &amp;T"/>
            <Footer x:Margin="0.25" x:Data="&amp;L Private and Confidential &amp;R Page &amp;P of &amp;N"/>
            <Layout x:Orientation="Landscape"/>
            <PageMargins x:Bottom="0.5" x:Left="0.5" x:Right="0.5" x:Top="0.5"/>
          </PageSetup>
          <Print>
            <ValidPrinterInfo/>
            <VerticalResolution>0</VerticalResolution>
          </Print>
          <Selected/>
          <FreezePanes/>
          <FrozenNoSplit/>
          <SplitHorizontal>1</SplitHorizontal>
          <TopRowBottomPane>1</TopRowBottomPane>
          <DoNotDisplayGridlines/>
        </WorksheetOptions>
      </Worksheet>
      <Worksheet ss:Name="LogForm Logs">
        <Table ss:ExpandedColumnCount="5" x:FullColumns="0" x:FullRows="0">
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <!--Header Section-->
          <Row ss:AutoFitHeight="1">
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">User</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Folder Name</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">File Name</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Action Date</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Action</Data>
            </Cell>
          </Row>
          <!--Report Section-->
          <xsl:for-each select="NewDataSet/Table4">
            <Row ss:AutoFitHeight="1">
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="User"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="FolderName"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="FileName"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="ms:format-date(CreatedDate, 'yyyy-MM-dd')"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="ActionName"/>
                </Data>
              </Cell>
            </Row>
          </xsl:for-each>
          <!--Summary Section-->
        </Table>
        <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
          <PageSetup>
            <Header x:Margin="0.25" x:Data="&amp;R Date: &amp;D Time: &amp;T"/>
            <Footer x:Margin="0.25" x:Data="&amp;L Private and Confidential &amp;R Page &amp;P of &amp;N"/>
            <Layout x:Orientation="Landscape"/>
            <PageMargins x:Bottom="0.5" x:Left="0.5" x:Right="0.5" x:Top="0.5"/>
          </PageSetup>
          <Print>
            <ValidPrinterInfo/>
            <VerticalResolution>0</VerticalResolution>
          </Print>
          <Selected/>
          <FreezePanes/>
          <FrozenNoSplit/>
          <SplitHorizontal>1</SplitHorizontal>
          <TopRowBottomPane>1</TopRowBottomPane>
          <DoNotDisplayGridlines/>
        </WorksheetOptions>
      </Worksheet>
      <Worksheet ss:Name="WorkArea Logs">
        <Table ss:ExpandedColumnCount="4" x:FullColumns="0" x:FullRows="0">
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>

          <!--Header Section-->
          <Row ss:AutoFitHeight="1">
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">User</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">File Name</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Action Date</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Action</Data>
            </Cell>
          </Row>
          <!--Report Section-->
          <xsl:for-each select="NewDataSet/Table5">
            <Row ss:AutoFitHeight="1">
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="User"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="FileName"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="ms:format-date(CreatedDate, 'yyyy-MM-dd')"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="ActionName"/>
                </Data>
              </Cell>
            </Row>
          </xsl:for-each>
          <!--Summary Section-->
        </Table>
        <WorksheetOptions xmlns="urn:s:schemas-microsoft-com:o:office:excel">
          <PageSetup>
            <Header x:Margin="0.25" x:Data="&amp;R Date: &amp;D Time: &amp;T"/>
            <Footer x:Margin="0.25" x:Data="&amp;L Private and Confidential &amp;R Page &amp;P of &amp;N"/>
            <Layout x:Orientation="Landscape"/>
            <PageMargins x:Bottom="0.5" x:Left="0.5" x:Right="0.5" x:Top="0.5"/>
          </PageSetup>
          <Print>
            <ValidPrinterInfo/>
            <VerticalResolution>0</VerticalResolution>
          </Print>
          <Selected/>
          <FreezePanes/>
          <FrozenNoSplit/>
          <SplitHorizontal>1</SplitHorizontal>
          <TopRowBottomPane>1</TopRowBottomPane>
          <DoNotDisplayGridlines/>
        </WorksheetOptions>
      </Worksheet>

      <Worksheet ss:Name="Efileflow Logs">
        <Table ss:ExpandedColumnCount="4" x:FullColumns="0" x:FullRows="0">
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>

          <!--Header Section-->
          <Row ss:AutoFitHeight="1">
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">User</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">File Name</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Action Date</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Action</Data>
            </Cell>
          </Row>
          <!--Report Section-->
          <xsl:for-each select="NewDataSet/Table6">
            <Row ss:AutoFitHeight="1">
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="User"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="Name"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="ms:format-date(CreatedDate, 'yyyy-MM-dd')"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="ActionName"/>
                </Data>
              </Cell>
            </Row>
          </xsl:for-each>
          <!--Summary Section-->
        </Table>
        <WorksheetOptions xmlns="urn:s:schemas-microsoft-com:o:office:excel">
          <PageSetup>
            <Header x:Margin="0.25" x:Data="&amp;R Date: &amp;D Time: &amp;T"/>
            <Footer x:Margin="0.25" x:Data="&amp;L Private and Confidential &amp;R Page &amp;P of &amp;N"/>
            <Layout x:Orientation="Landscape"/>
            <PageMargins x:Bottom="0.5" x:Left="0.5" x:Right="0.5" x:Top="0.5"/>
          </PageSetup>
          <Print>
            <ValidPrinterInfo/>
            <VerticalResolution>0</VerticalResolution>
          </Print>
          <Selected/>
          <FreezePanes/>
          <FrozenNoSplit/>
          <SplitHorizontal>1</SplitHorizontal>
          <TopRowBottomPane>1</TopRowBottomPane>
          <DoNotDisplayGridlines/>
        </WorksheetOptions>
      </Worksheet>

      <Worksheet ss:Name="Inbox and Pending Logs">
        <Table ss:ExpandedColumnCount="5" x:FullColumns="0" x:FullRows="0">
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>
          <Column ss:AutoFitWidth="1" ss:Width="150"/>

          <!--Header Section-->
          <Row ss:AutoFitHeight="1">
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">User</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">File Name</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Action Date</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Action</Data>
            </Cell>
            <Cell ss:StyleID="LHead">
              <Data ss:Type="String">Type</Data>
            </Cell>
          </Row>
          <!--Report Section-->
          <xsl:for-each select="NewDataSet/Table7">
            <Row ss:AutoFitHeight="1">
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="User"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="FileName"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="ms:format-date(CreatedDate, 'yyyy-MM-dd')"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="ActionName"/>
                </Data>
              </Cell>
              <Cell ss:StyleID="Default2">
                <Data ss:Type="String">
                  <xsl:value-of select="typename"/>
                </Data>
              </Cell>
            </Row>
          </xsl:for-each>
          <!--Summary Section-->
        </Table>
        <WorksheetOptions xmlns="urn:s:schemas-microsoft-com:o:office:excel">
          <PageSetup>
            <Header x:Margin="0.25" x:Data="&amp;R Date: &amp;D Time: &amp;T"/>
            <Footer x:Margin="0.25" x:Data="&amp;L Private and Confidential &amp;R Page &amp;P of &amp;N"/>
            <Layout x:Orientation="Landscape"/>
            <PageMargins x:Bottom="0.5" x:Left="0.5" x:Right="0.5" x:Top="0.5"/>
          </PageSetup>
          <Print>
            <ValidPrinterInfo/>
            <VerticalResolution>0</VerticalResolution>
          </Print>
          <Selected/>
          <FreezePanes/>
          <FrozenNoSplit/>
          <SplitHorizontal>1</SplitHorizontal>
          <TopRowBottomPane>1</TopRowBottomPane>
          <DoNotDisplayGridlines/>
        </WorksheetOptions>
      </Worksheet>
    </Workbook>
  </xsl:template>
</xsl:stylesheet>