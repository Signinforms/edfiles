﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FileNotFound.aspx.cs" Inherits="FileNotFound" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <div class="title-container">
        <div class="inner-wrapper">
            <h1>File Not Found</h1>
            <div class="clearfix"></div>
        </div>
    </div>
      <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt" style="text-align:left">
                    <p>Click <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Office/ManageDocument.aspx" Text="here" runat="server" /> to go back to previous page.
                        
                    </p>
            </div>
        </div>
    </div>
</asp:Content>
