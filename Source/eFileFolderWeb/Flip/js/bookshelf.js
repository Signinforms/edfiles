/**
* Copyright (c) 2012 Emmanuel Garcia
* All rights reserved
*
* turnjs.com
* turnjs.com/license.txt
**/
var $dragPage = null;

(function ($) {
    var $jsonmotheds = new JSonService();
    $jsonmotheds.initialize();
    var currentFolderName,
        previewNumPages,
        zoomOutButton,
        scrollTimer,
        scrollPage,
        scrollX = null,
        demoHash = '',
        scrolling = false,
        slide_int,
        params = {
            id: null,
            name: null,
            start: 1,
            uid: null,
            pwd: null,
            sid: null,
            status: $status.loaded
        },
        loadingcircle = null,
        book_content;

    bookshelf = {
        login: function (id, sid) {
            $jsonmotheds.getFolderXML(id, sid, this.loginSuccess, this.loginFailed);
            return true;
        },

        loginSuccess: function (data) {
            bookshelf.loaded.apply();
            if (data.d !== null && data.d.ErrorCode === 0) {
                var args = new Array();
                args.push(data.d);
                bookshelf.init.apply(this, args);
            } else if (data.d.ErrorCode === -9) {
                $('#dialog-enter-pin').dialog('open');
            } else {
                $('#dialog-message-content').text("Failed to load Folder Details.");
                $('#dialog-message-all').dialog('option', 'title', " Error");

                $('#dialog-message-all').dialog("open");
            }

            return true;
        },

        verifySecurity: function (sender, pwd) {
            bookshelf.loading.apply();
            var success = function (data) {
                bookshelf.loaded.apply();

                var result = $.parseJSON(data.d);
                if (result.ErrorCode.value !== pwd) {
                    $('#shelf-pin').val('');
                    $('#dialog-enter-pin').dialog('open');
                } else {
                    bookshelf.loading.apply();
                    $jsonmotheds.getFolderXMLSecurity(params.id, pwd, bookshelf.loginSuccess, bookshelf.loginFailed);
                }
            };

            var failed = function (data) {
                bookshelf.loaded.apply();
                $('#dialog-message-content').text(data.responseText);
                $('#dialog-message-all').dialog('option', 'title', " Error");

                $('#dialog-message-all').dialog("open");

            };

            $jsonmotheds.verifySecurity(params.id, pwd, success, failed);
        },

        loginFailed: function (data) {
            bookshelf.unloaded.apply();
            $('#dialog-message-content').text("Failed to get folder details:" + data.responseText);
            $('#dialog-message-all').dialog('option', 'title', "Warning");

            $('#dialog-message-all').dialog("open");
        },

        //��ʼ��folderĿ¼��dividers
        init: function (d) {
            book_content = d;
            bookshelf.loading.apply();            
            //loading book
            bookshelf.setTitle(book_content.Name);
            bookshelf.initshelf.apply();
            initdividers(book_content);

            bookshelf.loaded.apply();
        },

        setTitle: function viewSetTitle(title) {
            //document.title = title;           
        },

        //load divider content by id and sid
        initdivider: function (id) {
            $jsonmotheds.getTabXML(id, params.sid, this.loaddividerSuccess, this.loaddivierFailed);
            return true;
        },

        loaddividerSuccess: function (data) {
            
            if (data.d !== null && data.d.ErrorCode === 0) {
                var items = $dividers.Items;
                items[$current_divider_nth]["cached"] = true;

                //save the divider's data in this items[current_divider_nth]
                items[$current_divider_nth]['fid'] = data.d.FolderId;
                items[$current_divider_nth]['files'] = data.d.FileItemList;
                items[$current_divider_nth]['notes'] = data.d.NewNoteList;
                items[$current_divider_nth]['qotes'] = data.d.QuiteNoteList;
                items[$current_divider_nth]['meta'] = data.d.MetaXML;
                items[$current_divider_nth]['total'] = data.d.TotalPages;

                trigger("initedtoc", $current_divider, items[$current_divider_nth]);
            }
        },

        loaddivierFailed: function (data) {
            bookshelf.unloaded.apply();

            setTimeout(function () {
                $('#dialog-message-content').text("Failed to get tab detials:" + data.statusText);
                $('#dialog-message-all').dialog('option', 'title', "Loading Tab Error");

                $('#dialog-message-all').dialog("open");
            }, 500);

            var items = $dividers.Items;
            items[$current_divider_nth]["cached"] = false;

            //save the divider's data in this items[current_divider_nth]
            items[$current_divider_nth]['fid'] = null;
            items[$current_divider_nth]['files'] = [];
            items[$current_divider_nth]['notes'] = [];
            items[$current_divider_nth]['qotes'] = [];
            items[$current_divider_nth]['meta'] = '';
            items[$current_divider_nth]['total'] = 1;

            trigger("initedtoc", $current_divider, items[$current_divider_nth]);

        },

        loaded: function () {
            $current_status = $status.loaded;
            setTimeout(function () { $('.flash').hide(); }, 500);
        },

        unloaded: function () {
            $current_status = $status.unloaded;
            setTimeout(function () { $('.flash').hide(); }, 500);
        },

        loading: function () {
            $current_status = $status.loading;
            if (loadingcircle === null) {
                loadingcircle = $('<div id="floatingCirclesG">\
						<div class="f_circleG" id="frotateG_01"> </div>\
						<div class="f_circleG" id="frotateG_02"> </div>\
						<div class="f_circleG" id="frotateG_03"> </div>\
						<div class="f_circleG" id="frotateG_04"> </div>\
						<div class="f_circleG" id="frotateG_05"> </div>\
						<div class="f_circleG" id="frotateG_06"> </div>\
						<div class="f_circleG" id="frotateG_07"> </div>\
						<div class="f_circleG" id="frotateG_08"> </div>\
					  </div>');
            }

            if ($('.flash').has('#floatingCirclesG').length === 0) {
                $('.flash').append(loadingcircle);
            }

            $('.flash').show();

        },

        // show the shelf div after folder ready
        initshelf: function () {
            jQXB.setDataSource('booktitle', book_content).doBind('booktitle');
            jQXB.setDataSource('bookalerts', book_content.alertsList).doBind('bookalerts');
        },

        regExp: function (names, callback) {
            var parts = [];
            for (var i = 0; i < names.length;i++){
                var reg = new RegExp("(^|\\?|&)" + names[i] + "=([^&]*)(\\s|&|$)", "i");
                if (reg.test(location.href)) {
                    parts.push(unescape(RegExp.$2.replace(/\+/g, " ")));
                } else {
                    callback.nop(names);
                    return;
                }

            }

            callback.yep(names, parts);

        },

        // close the folder and back the bookshelf 
        close: function () {


        },

        // after login valid, we open the folder by folder id
        open: function (folderName) {

        },

        preloadImgs: function (pics, path, callback) {

            var loaded = 0,
                load = function (src) {
                    var tmp = $('<img />').
                        bind('load', function () {
                            loaded++;
                            if (loaded == pics.length && typeof (callback) == 'function')
                                callback();
                            tmp = null;
                        }).attr('src', path + src);
                };

            if (pics.length === 0) {
                if (typeof (callback) == 'function')
                    callback();
            } else
                for (var i = 0; i < pics.length; i++)
                    load(pics[i]);

        },

        zoomOutButton: function (show) {

            if (!zoomOutButton) {
                zoomOutButton = $('<i />', { 'class': 'icon zoom-out' }).
                    appendTo($('.splash')).
                    mouseover(function () {
                        zoomOutButton.addClass('zoom-out-hover');
                    }).
                    mouseout(function () {
                        zoomOutButton.removeClass('zoom-out-hover');
                    }).
                    click(function () {
                        $('.splash').zoom('zoomOut');
                        $(this).hide();
                    });
            }

            zoomOutButton.css({ display: (show) ? '' : 'none' });

        },

        deleteNote: function (sender, id) {
            bookshelf.loading.apply();

            var success = function (data) {
                var result = data.d;
                if (result === 0) {
                    $('#dialog-message-content').text("Failed to delete this note.");
                    $('#dialog-message-all').dialog('option', 'title', "Delete Note Error");

                    $('#dialog-message-all').dialog("open");
                    return;
                }
                else if (result === -2) {
                    $('#dialog-message-content').text("Failed to check the security code.");
                    $('#dialog-message-all').dialog('option', 'title', "Delete Note Error");

                    $('#dialog-message-all').dialog("open");
                } else {
                    if ($book_contents[id]) {
                        try {
                            var pages = $(".sj-book").turn('pages');
                            var pageth = parseInt(sender.attr('pageth'));
                            
                            if (pages === pageth && pageth % 2 === 0) { //event and last, make it emtpy
                                var element = $('<div class="empty" pageth="' + pageth + '"/>');
                                $(".sj-book").turn('addPage', element, pageth);
                            }
                            else if (pages === pageth && pageth % 2 === 1) {
                                $(".sj-book").turn('removePage', pageth);
                            }
                            else if (pages > pageth && pageth % 2 === 0) {
                                if ($('.empty[pageth="' + pages + '"]').length > 0) {
                                    $(".sj-book").turn('removePage', pages);
                                    $(".sj-book").turn('removePage', pageth);
                                }
                                else {
                                    $(".sj-book").turn('removePage', pageth);
                                    var element = $('<div class="empty" pageth="' + pages + '"/>');
                                    $(".sj-book").turn('addPage', element);
                                }  
                            }
                            else if (pages > pageth && pageth % 2 === 1) {                               
                                if ($('.empty[pageth="' + pages + '"]').length > 0) {
                                    $(".sj-book").turn('removePage', pages);
                                    $(".sj-book").turn('removePage', pageth);                                    
                                }
                                else {
                                    $(".sj-book").turn('removePage', pageth);
                                    var element = $('<div class="empty" pageth="' + pages + '"/>');
                                    $(".sj-book").turn('addPage', element);
                                }                                
                            }

                        }
                        catch (err) {
                            //console.log(err);
                        }

                        bookshelf.updateNoteth(pageth, pages);
                        bookshelf.clearNotes(id);
                    }
                }

                bookshelf.loaded.apply();                
            };

            var failed = function (data) {
                bookshelf.loaded.apply();
                sender.dialog("close");

                $('#dialog-message-content').text(data.responseText);
                $('#dialog-message-all').dialog('option', 'title', "Delete Note Error");

                $('#dialog-message-all').dialog("open");
            };

            $jsonmotheds.deleteNote(id, params.sid, success, failed);
        },

        updateNoteth: function (page,pages) {
            $('div[noteref][pageth]').each(function (index, elment) {                
                var pageth = $(elment).attr('pageth');
                if (pageth && parseInt(pageth) > page) {
                    var noteref = $(elment).attr('noteref');
                    var note = $book_contents[noteref];
                    if (note) {
                        note.updatePage(parseInt(pageth)-1);
                    }
                }                
            });
        },

        clearNotes: function (id) {
            var items = $dividers.Items;
            var notes = items[$current_divider_nth]['notes'];
            var index = bookshelf.getNote(parseInt(id), notes);
            if (id !== null) {
                notes.remove(notes[index]);
                var item = $items_auto_source[$items_auto_source.length - 1];
                item.count--;
            }

            if (notes.length === 0) {
                bookshelf.cleartocWithNote();
            }
            
            var pages = $(".sj-book").turn('pages');
            items[$current_divider_nth]['total'] = pages;
            $('#slider-book').slider('option', 'max', pages);
            $('.sj-book').turn('previous');

            $total_page_numb = pages;
            delete $book_contents[id];
        },

        getNote: function (id, notes) {
            for (var i = 0; i < notes.length; i++) {
                if (notes[i].Id === id) return i;
            }

            return null;
        },

        getContent: function (id, contents) {
            for (var i = 0; i < contents.length; i++) {
                if (contents[i].id === id) return i;
            }

            return null;
        },

        cleartocWithNote: function () {
            var note = null;
            for (var i = 0; i < $items_auto_source.length; i++) {
                if ($items_auto_source[i].isNote) {
                    note = $items_auto_source[i];
                    break;
                }
            }

            if (note) {
                $items_auto_source.remove(note);
            }  
        },

        updateNote: function (sender, id) {            
            var title, author, content;
            if (id === null) {
                title = $('#dialog-add-note input[name="title"]').val();
                author = $('#dialog-add-note input[name="author"]').val();
                content = $('#dialog-add-note textarea[name="content"]').val();

            } else {
                title = $('#dialog-edit-note input[name="title"]').val();
                author = $('#dialog-edit-note input[name="author"]').val();
                content = $('#dialog-edit-note textarea[name="content"]').val();
            }

            var detail = { Id: id, Title: title, Content: content, Author: author };
            bookshelf.loading.apply();

            var success = function (data) {
                detail.Id = data.d;
                if (detail.Id === 0) {
                    $('#dialog-message-content').text("Failed to insert new note.");
                    $('#dialog-message-all').dialog('option', 'title', "Update Note Error");

                    $('#dialog-message-all').dialog("open");
                    return;
                }
                else if (detail.Id === -2) {
                    $('#dialog-message-content').text("Failed to check the security code.");
                    $('#dialog-message-all').dialog('option', 'title', "Update Note Error");

                    $('#dialog-message-all').dialog("open");
                } else {
                    bookshelf.insertNote(detail);
                }

                bookshelf.loaded.apply();
                sender.dialog("close");
            };

            var failed = function (data) {
                bookshelf.loaded.apply();
                sender.dialog("close");

                $('#dialog-message-content').text(data.responseText);
                $('#dialog-message-all').dialog('option', 'title', "Update Note Error");

                $('#dialog-message-all').dialog("open");
            };

            var dividerid = $dividers.GetValue($current_divider_nth).id;
            $jsonmotheds.updateNote(params.uid, id, dividerid, content, params.sid, title, author, success, failed);
        },

        getPageNote: function () {
            var item = null;
            for (var i = 0; i < $items_auto_source.length; i++) {
                if ($items_auto_source[i].isNote) {
                    item = $items_auto_source[i];
                    break;
                }
            }

            return item;
        },
        
        getNoteById:function(id,notes){
            var item = null;
            for (var i = 0; i < notes.length; i++) {
                if (notes[i].Id===id) {
                    item = notes[i];
                    break;
                }
            }

            return item;
        },

        insertNote: function (detail) {            
            var book = $(".sj-book");
            var pages = $(".sj-book").turn('pages');

            var toc = $("#toc");
            var items = $dividers.Items;
            var target = items[$current_divider_nth];
            var notes = target['notes'];
            var note = { Id: detail.Id, Author: detail.Author, Title: detail.Title, Content: detail.Content, DividerId: target["id"] };

            if (!$book_contents[detail.Id]) {
                try {
                    if ($('.empty[pageth="' + pages + '"]').length>0) {
                        var pageNode = new NotePage(detail.Id, pages, $current_divider, {}, detail);
                        $book_contents[detail.Id] = pageNode;
                        $(".sj-book").turn('addPage', pageNode.element, pages);

                    } else { 
                        pageNode = new NotePage(detail.Id, ++pages, $current_divider, {}, detail);

                        $book_contents[detail.Id] = pageNode;
                        $(".sj-book").turn('addPage', pageNode.element);                      
                        var element = $('<div class="empty" pageth="' + (pages+1) + '"/>');
                        $(".sj-book").turn('addPage', element);
                        
                        $('#slider-book').slider('option', 'max', pages);
                    }

                    pageNode.initialize();
                    notes.push(note);                   

                    if (notes.length === 1) {
                        $items_auto_source.push({
                            value: target["id"],
                            isNote: true,
                            label: 'Page Notes',
                            start: $total_page_numb,
                            count: 1,
                            pages: notes
                        });
                        addNote(toc, target, notes);
                    }
                    else {
                        var item = bookshelf.getPageNote();
                        item.count++;
                    }

                    $(".sj-book").turn('page', pages);
                } catch (err) {
                   // console.log(err);
                }
            } else {
                pageNode = $book_contents[detail.Id];
                var item = bookshelf.getNoteById(detail.Id,notes);
                if (item !== null) {
                    item.Title = note.Title;
                    item.Author = note.Author;
                    item.Content = note.Content;
                }

                pageNode.updateUI(note);
            }
        },

        updateQote: function (sender, details) {
            var pagekey = details.pagekey;

            bookshelf.loading.apply();

            var success = function (data) {
                var id = data.d;
                if (id === 0) {
                    $('#dialog-message-content').text("Failed to insert new quick note.");
                    $('#dialog-message-all').dialog('option', 'title', "Update Qote Error");

                    $('#dialog-message-all').dialog("open");
                    return;
                }
                else if (id === -2) {
                    $('#dialog-message-content').text("Failed to check the security code.");
                    $('#dialog-message-all').dialog('option', 'title', "Update Qote Error");

                    $('#dialog-message-all').dialog("open");
                } else {
                    var pageview = $book_contents[pagekey];
                    if (!pageview.qotes.ContainsKey(id)) {
                        try {
                            var qbox = new QoteBox(id, pageview.el, details);
                            pageview.qotes.Add(id, qbox);
                        } catch (e) { }
                    }
                }

                bookshelf.loaded.apply();

            };

            var failed = function (data) {
                $('#dialog-message-content').text(data.responseText);
                $('#dialog-message-all').dialog('option', 'title', "Update Qote Error");

                $('#dialog-message-all').dialog("open");

                bookshelf.loaded.apply();
            };

            var dividerid = $dividers.GetValue($current_divider_nth).id;
            $jsonmotheds.updateQote(params.uid, details.id, pagekey, details.content, dividerid, details.x,
                details.y, params.sid, success, failed);
        },

        deleteQote: function (sender, id) {
            bookshelf.loading.apply();

            var success = function (data) {
                sender.destroy();

                bookshelf.loaded.apply();
            };

            var failed = function (data) {
                $('#dialog-message-content').text(data.toString());
                $('#dialog-message-all').diloag('option', 'title', "Delete Qote Error");

                $('#dialog-message-all').dialog("open");

                bookshelf.loaded.apply();
            };

            $jsonmotheds.deleteQote(id, params.sid, success, failed);
        },

        mailTo: function (sender) {
            var email = $('#dialog-share-book input[name="email"]').val();
            var name = $('#dialog-share-book input[name="myname"]').val();
            var emailto = $('#dialog-share-book input[name="emailto"]').val();
            var nameto = $('#dialog-share-book input[name="nameto"]').val();

            var title = $('#dialog-share-book input[name="title"]').val();
            var content = $('#dialog-share-book textarea[name="content"]').val();

            var details = {
                title: title, content: content, email: email,
                name: name, emailto: emailto, nameto: nameto
            };
            bookshelf.loading.apply();

            var success = function (data) {
                bookshelf.loaded.apply();

                var id = data.d;
                if (id === 0) {
                    $('#dialog-message-content').text("Failed to share book.");
                    $('#dialog-message-all').dialog('option', 'title', "Share Book Error");

                    $('#dialog-message-all').dialog("open");
                    return;
                }
                else if (id === -2) {
                    $('#dialog-message-content').text("Failed to check the security code.");
                    $('#dialog-message-all').dialog('option', 'title', "Sharing Book Error");

                    $('#dialog-message-all').dialog("open");
                } else {
                    $('#dialog-message-content').text(data.responseText);
                    $('#dialog-message-all').dialog('option', 'title', "Sharing Book Success");

                    $('#dialog-message-all').dialog("open");
                }

                sender.dialog("close");
            };

            var failed = function (data) {
                bookshelf.loaded.apply();

                $('#dialog-message-content').text("Failed to share book:" + data.responseText);
                $('#dialog-message-all').dialog('option', 'title', "Sharing Book Error");

                $('#dialog-message-all').dialog("open");

                sender.dialog("close");
            };

            $jsonmotheds.mailTo(params.id, params.sid, details.email, details.emailto, details.name,
                details.nameto, details.title, details.content, success, failed);
        },

        slider: function (page) {
            var $slider = $('#slider-book');
            var sliderpage = $slider.slider('option', 'value');

            if (typeof (sliderpage) !== 'object' && sliderpage !== page) {
                $slider.slider('option', 'value', page);
                var max = $slider.slider('option', 'max');
                $('#slider-bar-number').val(page + '/' + max);
            }
        },

        render: function (view) {
            var pages = view, k = 0;
            var selectors = "div[pageref][pageth='" + pages[0] + "']";
            var noteselectors = "div[noteref][pageth='" + pages[0] + "']";
            var book = $('.sj-book');
            var next = [];
            if (book.turn('hasPage', pages[0] + 2)) {
                next = $('.sj-book').turn('view', pages[0] + 2);
            }

            while (++k < pages.length) {
                selectors += ",div[pageref][pageth='" + pages[k] + "']";
                noteselectors += ",div[noteref][pageth='" + pages[k] + "']";
            }

            PDFView.highPriorityPages = next;
            PDFView.highestPriorityPages = view;

            $('.sj-book').find(selectors).each(function (index, elment) {
                var key = $(this).attr('pageref');
                $page_promises.GetValue(key).then(function (data) { //onData to then
                    $book_contents[key].setPage(data);
                    $book_contents[key].initialize();
                    $book_contents[key].renderView('page');
                });
            });

            $('.sj-book').find(noteselectors).each(function (index, elment) {
                var key = $(this).attr('noteref');
                var note = $book_contents[key];
                if (note) {
                    note.initialize();
                }
            });

        },

        searchBook: function () {
            var size = $('.sj-book').turn('size');
            var height = size.height + 8;
            $('#book-search').css({ height: height });
            $('#book-search-result').css({ height: height - 100 });

            if ($('#book-search').is(':visible')) {
                $('#book-search').hide();
                PDFFindController.active = false;
            } else {
                $('#book-search').show();
                PDFFindController.active = true;
            }

            $('.sj-book').turn('center');

            $('#book-search').position({
                of: $("#book-zoom .sj-book"),
                my: "left top",
                at: "right top",
                offset: "45 -8",
                collision: "fit none"
            });
        }
    };


    function splashHeight() {
        return ($('.splash').hasClass('preview')) ? 800 : 700;
    }

    function scrollTop(top, speed) {
        scrolling = true;

        $('html,body').animate({ scrollTop: top }, speed, function () {
            scrolling = false;
            setCurrentHash();
        });
    }

    function navigation(where) {
        var tokens = where.split(' ');

        switch (tokens[0]) {
            case 'page-note':
                $('#dialog-add-note').dialog('open');
                break;
            case 'quick-note':
                var book = $('.sj-book');
                var view = book.turn('view');
                var viewLeft = $('.sj-book .p' + view[0]), viewRight = $('.sj-book .p' + view[1]);

                if (!viewLeft.hasClass('hard') && typeof (viewLeft.attr('pageref')) !== 'undefined') {

                    $('<div >', { 'class': 'drop-note' }).
                        html('<div>Press drop here</div>').
                        appendTo(viewLeft).
                        delay(1000).
                        animate({ opacity: 0 }, 500, function () {
                            $(this).remove();
                        });
                }

                if (!viewRight.hasClass('hard') && typeof (viewRight.attr('pageref')) !== 'undefined') {

                    $('<div >', { 'class': 'drop-note' }).
                        html('<div>Press drop here</div>').
                        appendTo(viewRight).
                        delay(1000).
                        animate({ opacity: 0 }, 500, function () {
                            $(this).remove();
                        });
                }

                break;
            case 'print-page':
                $('#dialog-print-page').dialog('open');

                break;
            case 'prevs-page':
                var sliderbar = $('#slider-book');
                if (sliderbar.slider('option', 'value') > 1) {
                    $('.sj-book').turn('previous');
                }
                break;
            case 'next-page':
                sliderbar = $('#slider-book');
                if (sliderbar.slider('option', 'value') < sliderbar.slider('option', 'max')) {
                    $('.sj-book').turn('next');
                }

                break;
            case 'table-contents':
                $('.sj-book').turn('page', 1);
                break;
            case 'share-email':
                $('#dialog-share-book').dialog('open');

                break;
            case 'share-book':
                $('#dialog-print-page').dialog('open'); // same as print
                break;
            case 'share-pinterest':
                break;
            case 'search-book':
                bookshelf.searchBook();
                break;
            case 'search-qotes':
                var size = $('.sj-book').turn('size');
                var height = size.height + 8;
                $('#book-qotes').css({ height: height });
                $('#qote-search-result').css({ height: height - 70 })

                if ($('#book-qotes').is(':visible')) {
                    $('#book-qotes').hide();
                } else {
                    $('#book-qotes').show();
                }

                $('.sj-book').turn('center');


                break;
        }

    }

    function update_page_slider() {
        var offset = $('#slider-book .ui-slider-handle').offset();
        var value = $('#slider-book').slider('option', 'value');
        $('#current_page_value').text(value).css({ left: offset.left });
        $('#current_page_value').fadeIn();
    }

    function update_zoom_slider() {
        var offset = $('#slider-zoom .ui-slider-handle').offset();
        var value = $('#slider-zoom').slider('option', 'value');
        $('#current_zoom_value').text(value * 100 + '%').css({ left: offset.left });
        $('#current_zoom_value').fadeIn();
    }

    // DOMReady

    $(document).ready(function () {

        if (!$.isTouch) {
            $('.go-up').click(function (e) {
                scrollTop(0, 'fast');
                e.preventDefault();
            });
        }

        jQXB.initialize();
        jQXB.compatibilitymode = true;
        // Share icons
        //$('.share .icon').bind($.mouseEvents.over, function(e) {

        //    var className = $.trim($(this).
        //        attr('class').
        //        replace(/\b([a-z-]*hover|icon)\b/g, ''));
        //    $(this).addClass(className + '-hover');

        //}).bind($.mouseEvents.out, function(e) {

        //    var className = $.trim($(this).
        //        attr('class').
        //        replace(/\b([a-z-]*hover|icon)\b/g, ''));
        //    $(this).removeClass(className + '-hover');

        //});

        clickElement($('.share .icon'), function (e) {

            navigation($.trim($(this).
                attr('class').
                replace(/\b([a-z-]*hover|icon)\b/g, '')));

        });

        // Close button
        clickElement($('.quit'), function () {
            window.open('', '_self');
            window.close();

        });

        // Back to the shelf
        clickElement($('.back'), function () {
            bookshelf.loading.apply();
            switchView(viewStatus.self);
            bookshelf.loaded.apply();
        });

        clickElement($('#dialog-print-page input'), function (event) {
            var nth = 0;
            switch (event.currentTarget.id) {
                case 'left-page':
                    nth = 0;
                    break;
                case 'right-page':
                    nth = 1;
                    break;
            }

            var view = $('.sj-book').turn('view');
            var page = $('.sj-book .p' + view[nth]);
            var pageid = null;

            if (page && !page.isNote) {
                pageid = page.attr('pageref');
                if (pageid) {
                    var pageView = $book_contents[pageid];
                    PDFView.beforePrint(pageView);
                }
            }


            $('#printPreviewContainer').dialog('open');
            $('#dialog-print-page').dialog('close');
        });


        clickElement($('#book-search-title .book-search-exit a'), function () {
            $('#book-search').hide();
            $('.sj-book').turn('center');
        });

        clickElement($('#qote-title .book-search-exit a'), function () {
            $('#book-qotes').hide();
            $('.sj-book').turn('center');
        });

        clickElement($('.share .download-file'), function () {
            var view = $('.sj-book').turn('view');
            var pageLeft = $('.sj-book .p' + view[0]);
            var pageRight = $('.sj-book .p' + view[1]);
            var pageid, id, url = null;

            if (pageRight && !pageRight.isNote) {
                pageid = pageRight.attr('pageref');
                if (pageid) {
                    id = pageid.split('-')[0];
                    url = getItemSource(id);
                    if (url)
                        var path = url;
                    if (url.indexOf("%") >= 0) {
                        path = url.replace(/\%/g, '$');
                    }
                    else if (url.indexOf('$') >= 0) {
                        path = url.replace(/\\\u0024/g, '%');
                    }
                    PDFView.download(path);
                    return;
                }
            }

            if (pageLeft && !pageLeft.isNote) {
                pageid = pageLeft.attr('pageref');

                if (pageid) {
                    id = pageid.split('-')[0];
                    url = getItemSource(id);
                    if (url)
                        PDFView.download(url);
                    return;
                }
            }

        });

        PDFFindBar.initialize();
        PDFFindController.initialize();

        // Initialize buttons

        $("'[placeholder]'").focus(function () {
            var input = $(this);
            if (input.val() == input.attr("'placeholder'")) {
                input.val("''");
                input.removeClass("'placeholder'");
            }
        })
            .blur(function () {
                var input = $(this);
                if (input.val() == "''" || input.val() == input.attr("'placeholder'")) {
                    input.addClass("'placeholder'");
                    input.val(input.attr("'placeholder'"));
                }
            }).blur();


        $(".tool .quick-note").draggable({
            appendTo: $('.sj-book'), //"body"
            helper: "clone",
            start: function (event, ui) {
                var book = $('.sj-book');
                var view = book.turn('view');
                var viewLeft = $('.sj-book .p' + view[0]), viewRight = $('.sj-book .p' + view[1]);

                if (!viewLeft.hasClass('hard') && typeof (viewLeft.attr('pageref')) !== 'undefined') {

                    $('<div >', { 'class': 'drop-note' }).
                        html('<div>Press drop here</div>').
                        appendTo(viewLeft).
                        delay(1000).
                        animate({ opacity: 0 }, 500, function () {
                            $(this).remove();
                        });

                    viewLeft.bind('mouseenter', function (eventObject) {
                        $dragPage = viewLeft;
                    });

                    viewLeft.bind('mouseleave', function (eventObject) {
                        $dragPage = null;
                    });
                }

                if (!viewRight.hasClass('hard') && typeof (viewRight.attr('pageref')) !== 'undefined') {

                    $('<div >', { 'class': 'drop-note' }).
                        html('<div>Press drop here</div>').
                        appendTo(viewRight).
                        delay(1000).
                        animate({ opacity: 0 }, 500, function () {
                            $(this).remove();
                        });

                    viewRight.bind('mouseenter', function (eventObject) {
                        $dragPage = viewRight;
                    });

                    viewRight.bind('mouseleave', function (eventObject) {
                        $dragPage = null;
                    });
                }

            },
            zIndex: 9999,
            drag: function (event, ui) {
            },
            stop: function (event, ui) {
                var view = $('.sj-book').turn('view');
                var pageLeft = $('.sj-book .p' + view[0]);
                var pageRight = $('.sj-book .p' + view[1]);

                pageLeft.unbind('mouseenter');
                pageLeft.unbind('mouseleave');

                pageRight.unbind('mouseenter');
                pageRight.unbind('mouseleave');

                if ($dragPage) {
                    var pos = $dragPage.offset();
                    var x = event.pageX - pos.left,
                      y = event.pageY - pos.top;

                    var pagekey = $dragPage.attr('pageref');
                    bookshelf.updateQote($dragPage, { id: null, pagekey: pagekey, x: x, y: y, content: '' }, true);

                }
            }
        });


        $("#slider-book").slider({
            value: 1,
            min: 1,
            max: 50,
            step: 1,
            start: function (event, ui) {
                var $current_value = $('<div id="current_page_value"></div>');
                $(this).append($current_value).append($current_value);
                $current_value.empty();
                slide_int = setInterval(update_page_slider, 10);
            },
            slide: function (event, ui) {
                setTimeout(update_page_slider, 10);
            },
            stop: function (event, ui) {
                clearInterval(slide_int);
                slide_int = null;

                var $current_value = $('#current_page_value');
                $current_value.fadeOut(500);
                setTimeout(function () { $('#current_page_value').remove(); }, 500);

                var current_page = $('#slider-book').slider('option', 'value');
                var max = $('#slider-book').slider('option', 'max');
                $('#slider-bar-number').val(current_page + '/' + max);
                $('.sj-book').turn('page', current_page);
            }
        });

        $("#slider-zoom").slider({
            value: 1,
            min: 1,
            max: 3,
            step: 0.5,
            start: function (event, ui) {
                var current_value = $('#slider-zoom').slider('option', 'value');
                var $current_value = $('<div id="current_zoom_value" zoom=' + current_value + '></div>');
                $(this).append($current_value);
                $current_value.empty();
                setInterval(update_zoom_slider, 10);
            },
            slide: function (event, ui) {
                setTimeout(update_zoom_slider, 10);
            },
            stop: function (event, ui) {
                clearInterval(slide_int);
                slide_int = null;

                var $current_zoom = $('#current_zoom_value');
                var native_zoom_value = $current_zoom.attr("zoom");

                $current_zoom.fadeOut(500);
                setTimeout(function () { $('#current_zoom_value').remove(); }, 500);

                var current_value = $('#slider-zoom').slider('option', 'value');

                if (current_value == 1) {
                    $('#book-zoom').zoom('zoomOut');
                } else {
                    setTimeout(function () { $('#book-zoom').zoom('zoomIn', event); }, 100);
                }

            }
        });

        $('#slider-bar-number').keypress(function (e) {
            if (e.keyCode === 13) {
                var v = $('#slider-bar-number').val();

                var max = $('#slider-book').slider('option', 'max');
                var min = $('#slider-book').slider('option', 'min');
                var current = $('#slider-book').slider('option', 'value');
                if (parseInt(v) > max) {
                    $('#slider-bar-number').val(current);
                    v = current;
                } else if (parseInt(v) < min) {
                    $('#slider-bar-number').val(current);
                    v = current;
                }

                $('#slider-book').slider('option', 'value', v);
                $('.sj-book').turn('page', v);
                return true;
            } else if (e.keyCode === 8 || e.keyCode === 46) { //|| e.keyCode === 0 || e.keyCode === 47
                return true;
            } else if (!isNumberKey(e)) {
                return false;
            }

        });

        $('#slider-bar-number').focus(function (e) {
            var v = $('#slider-bar-number').val();
            var strs = v.split('/', 2);
            if (strs.length > 0) {
                $('#slider-bar-number').val(strs[0]);
            }

        });

        $('#slider-bar-number').blur(function (e) {
            var max = $('#slider-book').slider('option', 'max');
            var v = $('#slider-bar-number').val();
            var strs = v.split('/', 2);
            if (strs.length > 0) {
                $('#slider-bar-number').val(strs[0] + '/' + max);
            }

        });

        $('#slider-bar-number').change(function (e) {
            var v = $('#slider-bar-number').val();
            var strs = v.split('/', 2);
            if (strs.length > 0) {
                v = strs[0];
            }

            var max = $('#slider-book').slider('option', 'max');
            var min = $('#slider-book').slider('option', 'min');
            var current = $('#slider-book').slider('option', 'value');
            if (parseInt(v) > max) {
                $('#slider-bar-number').val(current);
            }

            if (parseInt(v) < min) {
                $('#slider-bar-number').val(current);
            }
        });
    });

    // Window events
    //$(window).load(function () {
    $(document).ready(function () {		
		var id = 10, sid = 1;
		if (sid.length > 15) {
		    sid = sid.substring(0, 15);
		}
		if (id === '' || sid === '') {
		    return;
		}

		params.id = id;
		params.sid = id + "_" + sid;

		//begin login
		try {
		    bookshelf.loading.apply();
		    //alert(params.id+'--'+ params.sid);
		    bookshelf.login(params.id, params.sid);
		} catch (exp) { }  
	});
})(jQuery);