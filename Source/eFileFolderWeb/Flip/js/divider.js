/**
 * divider.js
 *
 * Copyright (C) 2012 edfiles.com
 * MIT Licensed
 *
 * ****************************************
 *
 **/

var $status = { unloaded: 0, loading: 1, loaded: 2 };
var $bookshelfPromises = new Hashtable;

var $divider_caches = new Hashtable;
var $dividers = new Hashtable;
var $dividers_front = null, $dividers_back = null;
var $current_divider,$current_divider_nth = null;
var $total_page_numb = null;

var $book_contents = {}; // store all pageviews
var $page_promises = new Hashtable; // store all pages promises

var $divider_details = {};

var $items_auto_source = [];

var viewStatus = { self: false, book: true };
var $current_status = $status.loaded;
var current_view_status = viewStatus.self;

function getFitWidth(width, count) {
    var unit = Math.floor((width - 30) / count);

    //by shoaib 
    var screenWidth = screen.width;
    if (screenWidth < 500) {
        //width = Math.floor(screenWidth - ((20 * screenWidth) / 100));
        //height = Math.floor(width / 1.4);
        //height1 = height;
        //width1 = Math.floor(height1 * 1.6);

        width = 256;
        height = 182;
        height1 = 182;
        width1 = 291;

        unit= Math.floor((height1 - 25) / 2);
        //alert(width1 + "-" + height1);
    }
    //by shoaib

    return Math.min(unit, 100);
}

function initdividers(p) {
    var container = $("#dividers");
    var width = $('#shelf .flipbook').width() - 50;
    
    var unit = getFitWidth(width, p.tabsList.length);
	for(var i=0;i<p.tabsList.length; i++)
	{
		var divider = p.tabsList[i];
		var divider_1 = new Hashtable;
		divider_1["name"] = divider.Name;
		divider_1["color"] = divider.Color;
		divider_1["id"] = divider.Id;
		divider_1["nth"] = $dividers.Count+1;
		divider_1["cached"] = false;

		$dividers.Add(divider_1["nth"], divider_1);

		container.append(
            adddivider(divider_1["id"], divider_1["nth"], divider_1["name"], divider_1["color"], unit)
            );
	}
};

function adddivider(id, nth, name, bcolor, width)
{
    var $divider = $("<li id=" + id + " nth=" + nth + " class='divider' style='width:" + width + "px;'"+" title='"+name+"'>" + name + "</li>");
	$divider.css("background",bcolor);
	
    $divider.click(function (e) {        
		 selectdivider(e.currentTarget);
	}).bind("inittoc", inittoc).bind("initedtoc", initedtoc);
	
	return $divider;
}

function switchView(status){
    if (status === viewStatus.self)
	{
		if(!$("#shelf").is(":visible"))
		{
			$("#shelf").fadeIn('slow');
		}
			
		if($("#book-wrapper .bar").is(":visible"))
		{
			$("#book-wrapper .bar").fadeOut('slow');
		}

		if ($("#book-wrapper .tool").is(":visible")) {

		    $("#book-wrapper .tool").fadeOut('slow');
		}

		
		if($("#book-zoom").is(":visible"))
		{
			$("#book-zoom").fadeOut('slow');
		}	
	}
	else
	{
		if($("#shelf").is(":visible"))
		{
			$("#shelf").fadeOut('slow');
		}
			
		if(!$("#book-wrapper .bar").is(":visible"))
		{
			$("#book-wrapper .bar").fadeIn('slow');
		}

		if (!$("#book-wrapper .tool").is(":visible")) {

		    $("#book-wrapper .tool").fadeIn('slow');
		}
		
		if(!$("#book-zoom").is(":visible"))
		{
			$("#book-zoom").fadeIn('slow');
		}	
	}
	
	current_view_status = status;
}

function selectdivider(target) {
    if ($('.sj-book').turn('zoom') > 1) return;
	switchView(viewStatus.book);
	
	if($current_status===$status.loading) return;
	
	$current_status = $status.loading;
	bookshelf.loading.apply();
	
	if(parseInt($(target).attr("nth"))!=$current_divider_nth){
		$current_divider_nth = parseInt($(target).attr("nth"));
	}
	else{
		if(current_view_status == viewStatus.self)
		{
			switchView(viewStatus.book);
		}
		
		bookshelf.loaded.apply();
		return;	
	}
	
	$current_divider = $(target);

	var dividers_front = $("#dividers-front");	
	var dividers_back = $("#dividers-back");
	
	dividers_front.empty();
	dividers_back.empty();
	
	$dividers_front = new Hashtable;
	$dividers_back =  new Hashtable;
	var items = $dividers.Items;
	var $divider;
	var unit = getFitWidth($('.sj-book').height(), $dividers.Count);
	
	for(nth=$current_divider_nth+1;nth<=$dividers.Count;nth++)
	{
	    $divider = $("<li id="+items[nth]["id"]+" nth="+nth+ " class='divider-back' style='background-color:"+items[nth]["color"]+";width:"+unit+"px;'"+" title='"+items[nth]["name"]+"'>"+items[nth]["name"]+"</li>");
		$divider.css({ 'margin-top': unit - 39});

		$divider.hover(function (event) {
		    if ($current_status === $status.loading) return;
		    $(this).animate({ 'margin-top': unit - 54, 'padding-bottom': 15 });
		}, function (event) {
		    $(this).animate({ 'margin-top': unit - 39, 'padding-bottom': 0 });
		});

		dividers_back.append($divider);
		$dividers_back.Add(nth,items[nth]);
		dividers_back.css("padding-top", unit * $current_divider_nth);
		
	    $divider.click(function (e) {	       
	        if ($current_status === $status.loading) return;
		 	selectdivider(e.currentTarget);
	    }).bind("inittoc", inittoc).bind("initedtoc", initedtoc);
	}
		
	for(nth=1;nth<=$current_divider_nth;nth++)
	{
	    $divider = $("<li id=divider" + items[nth]["id"] + " nth=" + nth + " class='divider-front' style='background-color:" + items[nth]["color"] + ";width:" + unit + "px;'" + " title='" + items[nth]["name"] + "'>" + items[nth]["name"] + "</li>");
        
        //commented by shoaib 18June2015
	    //$divider.css({ 'margin-top': unit - 39 });
	    $divider.css({ 'margin-top': unit - 30 });

	    $divider.hover(function (event) {
	        if ($current_status === $status.loading) return;
	        //commented by shoaib 18June2015
	        //$(this).animate({ 'margin-top': unit - 54, 'padding-bottom': 15 });
	        $(this).animate({ 'margin-top': unit - 45, 'padding-bottom': 15 });
	    }, function (event) {
	        //commented by shoaib 18June2015
	        //$(this).animate({ 'margin-top': unit - 39, 'padding-bottom': 0 });
	        $(this).animate({ 'margin-top': unit - 30, 'padding-bottom': 0 });
	    });
		dividers_front.append($divider);
		$dividers_front.Add(nth,items[nth]);

	    $divider.click(function (e) {
	        if ($current_status === $status.loading) return;
	        selectdivider(e.currentTarget);
	    }).bind("inittoc", inittoc).bind("initedtoc", initedtoc);
	}
	
	//initialize toc of divider
    if ($divider != null) 
    {
        $current_divider = $divider;
        trigger("inittoc", $divider, items[$current_divider_nth]);
	}
		
	
}

function initfilters(item_source){
	$( "#item-filter-input" ).autocomplete({
			minLength: 0,
			source: item_source,
			focus: function( event, ui ) {
				$( "#item-filter-input" ).val( ui.item.label );
				return false;
			},
			select: function( event, ui ) {
				$( "#item-filter-input" ).val( ui.item.label );
				$( "#item-id" ).val(ui.item.value );		
				
				//auto flip to the page
				var id = parseInt($("#toc li:[id="+ui.item.value+"] a").text());
				$(".sj-book").turn("page",id);
				return false;
			},
			close: function( event, ui ) {
				$( "#item-filter-input" ).val("");
				}
			
		})
		.data( "autocomplete" )._renderItem = function( ul, item ) {
			return $( "<li></li>" )
				.data( "item.autocomplete", item )
				.append( "<a>" + item.label+"</a>" )
				.appendTo( ul );
	};
}

function initnotes() {
}

//initialize the toc items
function inittoc(e, target) {    
    if (target["cached"] === false) {
        
        //download tab content
        bookshelf.initdivider(target["id"]);
    }
    else {
        initedtoc(e, target);
    }
}
//initialize the toc items
function initedtoc(e, target)
{
	if($current_divider_nth===null ||target===null) return;

	var items = target["files"]; // name,fullname, path,id,count
    //items = [];
	var toc = $("#toc");	
	toc.empty();
	$total_page_numb = 1; // 初始页数为1，表示目录页， // 1th: 1, item.start = 1, item.end = 1+ page count(1)-1
    // 2th: 2 ;item.start = 2, total(2)+pagecount(2)-1 // 3th: 1 item.start = 4, total(2)+pagecount(2)-1
    // 所以如果总数total_page_numb为even偶数，则应该加1
	
	var book = $('.sj-book');
	$items_auto_source = [];
	$divider_details = [];
	$page_promises = new Hashtable;
	
	// init toc with all files of a tab, 
    // if be pdf format, that are the pdf file names    
	for(var i=0;i<items.length;i++)
	{
	    $items_auto_source.push({ value: items[i].Id, label: items[i].Name, path: items[i].FullName,start:$total_page_numb, count: items[i].Count });
	    //add pages promises for this item
	    for (var j = 1; j <= items[i].Count; j++) {
	        var pagekey = items[i].Id + '-' + j;
	        $page_promises.Add(pagekey,new PDFJS.Promise());
	    }
	    
	    addItem(toc, items[i]);
	}
    
    //add documentloader linker
	//if (items.length === 0) {
	//    var loader = { Id: target.fid,Idd: target.id, Name: 'Upload Documents', path: '', start: 0, count: 0 };
	//    addLoadItem(toc, loader);
	//}
    
	var pagesPromise = PDFJS.Promise.all($bookshelfPromises.GetValues());
	pagesPromise.then(function (bookshelfPromises) {
	    bookshelf.loaded.apply();
	});
	
	// add note pages, if we could append these notes into the pdf,
	// that would be very good, I think we could use javascript pdf maker library to do it
	// these would be parts of pages in pdf files
	var notes = target["notes"];
	if(notes.length>0)
	{
	    $items_auto_source.push({ value: target["id"], isNote: true, label: 'Page Notes', start: $total_page_numb,
	        count: notes.length, pages: notes});
		addNote(toc,target,notes);
	}
	
	initfilters($items_auto_source);

	$total_page_numb = target.total;
	if ($total_page_numb % 2 === 1) {
	    $total_page_numb++;
	}
	
	// total pages
	$('#slider-book').slider('option', 'value', 1);
	$('#slider-bar-number').val(1 + '/' + $total_page_numb);
	$('#slider-book').slider('option', 'max', $total_page_numb);
	
	//flip the old pages to the first page
	var pages = book.turn('pages');
	var currentPage = book.turn('page');
	if(pages>1&&currentPage>1)
	{
		book.turn('page',1);
	}
	
	// then turn the new pages
	book.turn('pages', $total_page_numb, { id: target["id"], th: $current_divider_nth });
	
	if(target.total>0)
		book.turn('page', 1);
	
	if ($page_promises.Count === 0) {
	    bookshelf.loaded.apply();
	}
}

function getCopyrightPage(copyright,dividers)
{
	var $page = $('<div class="fixed hard static back-side" disable="true"></div>').append(copyright).append(dividers);
	return $page;
}

function getBlankPage(hard,disable)
{
	return $("<div class="+hard+" disable="+disable+"></div>");
}

function getBlankPage()
{
	return $("<div/>");
}


// add one of item(file name) in a divider
function addItem(toc,item)
{
	var $pagenum = $("<a href='#' class='li-a'>"+$total_page_numb+"</a>");
	var $span =  $("<span class='li-span'>"+item.Name+"</span>");
	var $item = $("<li id="+item.Id+" divider="+item.Idd+" pages="+item.Count+" class='toc-li'></li>");
	$item.append($span);
	$item.append($pagenum);
	
	item.start = $total_page_numb;

	$total_page_numb += item.Count; //计算后的$total_page_numb为下也项的起始页书，如果本项为最后一页，应该-1，作为总数
	
	toc.append($item);

	clickElement($pagenum, function(e) {	
		$('.sj-book').turn('page',parseInt($pagenum.text()));
	});
	
	//merge the page list to $divider_details, without duplicate items
	$divider_details = $divider_details.concat([item]);

    var path = item.FullName;
	if (item.FullName.indexOf("%") >= 0) {
	    path = item.FullName.replace(/\%/g, '$');
	}
	else if (item.FullName.indexOf('$') >= 0) {
	    path = item.FullName.replace(/\\\u0024/g, '%');
	}

	PDFView.open(item.Id, path, 0); //executing by async
    
    $bookshelfPromises.Add(item.Id,new PDFJS.Promise());
    
}

// add one of item(file name) in a divider
function addLoadItem(toc, item) {
    var $span = $("<a class='li-span' target='_blank'>" + item.Name + "</a>");
    var $item = $("<div id=" + item.Id + " divider=" + item.Idd + " class='toc-li'>" + item.Name + "</div>");
   // $item.append($span);
    
    toc.append($span);
    
    clickElement($span, function (e) {
        //window.open("WebFlashViewer.aspx?V=2&ID=238", "", "toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no");
        window.open("Office/DocumentLoader.aspx?id=" + item.Id + "&dd=" + item.Idd);
    });
}

// add one of item in a divider
function addNote(toc,target,notes)
{
	var $pagenum = $("<a href='#' class='li-a'>"+$total_page_numb+"</a>");
	var $span =  $("<span class='li-span'>"+'Page Note'+"</span>");
	var $item = $("<li id="+target['id']+" divider="+target['id']+" pages="+notes.length+" class='toc-li'></li>");
	$item.append($span);
	$item.append($pagenum);
	
	notes.start = $total_page_numb;

	$total_page_numb += notes.length;
	
	toc.append($item);

	clickElement($pagenum, function(e) {	
		$('.sj-book').turn('page',parseInt($pagenum.text()));
	});
	
	$divider_details.push(notes);
	
}

function initshadow(e, args)
{
	var shadow = $(e.currentTarget);
	var pagea = $('<div class="front-side"/>');
	var pageb = $('<div class="back-side"/>');
	
	var copyright = $('<div class="copyright"><span class="text-insert-effect">\
						Copyright 2012 - All rights reserved<br>www.edfiles.com\
						</span></div>'),
		divider_back = $('<ul id="dividers-back"></ul>'),
		front_side = $('<div class="front-side-content"><ul id="dividers-front"></ul></div>'),

	    front_side_container = $('<div class="front-side-container">    \
                  <div class="table-content">\
                    <div class="table-content-title"><span class="text-insert-effect text-bold-effect">Table Contents</span></div>\
                    <div class="table-content-filter">\
                      <input id="item-filter-input" name="item-filter-input" type="text" value="" maxlength="20"  class="input-filter-text" placeholder="search here..."/>\
                      <input type="hidden" id="item-id"/>\
                    </div>\
                    <div class="toc-wrapper">\
                      <ol id="toc" jqxb-templatecontainer="itemTemplate" jqxb-datasource="divideritems">\
                        <li class="toc-li" jqxb-template="itemTemplate" jqxb-templateitemidprfx="itemtrow">\
                        	<span class="li-span" jqxb-itemdatamember="name" jqxb-bindedattribute="text"></span>\
                            <a class="li-a" href="#" jqxb-itemdatamember="startpage" jqxb-bindedattribute="text"></a>\
                        </li>\
                      </ol>\
                    </div>\
                  </div>\
                  <div class="toc-logo"><span class="text-insert-effect">www.edfiles.com</span></div></div></div>');
	pagea.css(args.a);
	pageb.css(args.b);
	
	pagea.append(front_side).append(front_side_container);
	pageb.append(copyright).append(divider_back);
	
	shadow.append(pagea);
	shadow.append(pageb);
	
   $("#toc").sortable({
		  items: "li:not(.placeholder)",
		  sort: function () {
			  $(this).removeClass("ui-state-default");
		  }
	  }).selectable();
}

function updateshadow(e, args)
{
	var shadow = $(e.currentTarget),
		pagea = shadow.children('.front-side');
		pageb = shadow.children('.back-side');
	
	pagea.css(args.a);
	pageb.css(args.b);
}






