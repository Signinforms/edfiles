﻿
'use strict';

var HttpMotheds = {
    GET: 'GET',
    POST: 'POST'
};

var JSonService = function jsonWebService() {
    var self = this;
    this.baseurl = 'EFileFolderJSONService.asmx';
    //this.baseurl = 'http://localhost:65043/eFileFolderWeb/EFileFolderJSONService.asmx';
    
    this.initialize = function jsonInitialize() {
        this.initialized = true;
    };

    this.getFolderXML = function jsonGetFolderXml(id, code, isEdit, success, failed) {
        // Live : 
        id = 75460;
		//id = 43885;
		//code = 'BD0F514D2C75486';
        //alert(2);
        var url = self.baseurl + '/GetFolderXMLForFront';				
        jQXB.postJSON(url, HttpMotheds.POST, { folderId: id, validateCode: code, isEdit: isEdit },
                success, failed);
    };

    this.getFolderXMLSecurity = function jsonGetFolderXmlSecurity(id, code, success, failed) {
        var url = self.baseurl + '/GetFolderXMLSecurity';
        jQXB.postJSON(url, HttpMotheds.POST, { folderId: id, validateCode: code },
                success, failed);
    };

    this.verifySecurity = function jsonVerifySecurity(id,pwd, success, failed) {
        var url = self.baseurl + '/VerifySecurityCode';
        jQXB.postJSON(url, HttpMotheds.POST, { folderid: id, code: pwd },
                success, failed);
    };

    
    this.getTabXML = function jsonGetTabXml(id, code, success, failed) {
        var url = self.baseurl + '/GetTabXMLForFront';
        jQXB.postJSON(url, HttpMotheds.POST, { tabId: id, validateCode: code },
                success, failed);
    };
    
    this.verifySecurity = function jsonVerifySecurity(id, code, success, failed) {
        var url = self.baseurl + '/VerifySecurityCode';
        jQXB.postJSON(url, HttpMotheds.POST, { folderid: id, code: code },
                success, failed);
    };
    
    this.getMetaXML = function jsonGetMetaXml(id, code, success, failed) {
        var url = self.baseurl + '/GetMetaXML';
        jQXB.postJSON(url, HttpMotheds.POST, { tabID: id, validateCode: code },
                success, failed);
    };
    
    this.setMetaXML = function jsonSetMetaXml(id, code, meta, success, failed) {
        var url = self.baseurl + '/SetMetaXML';
        jQXB.postJSON(url, HttpMotheds.POST, { tabID: id, metaXml:meta, validateCode: code },
                success, failed);
    };
    
    this.updateNote = function jsonUpdateNewNote(uid, id, tabId, content, code, title,author, success, failed) {
        var url = self.baseurl + '/UpdateNewNote';
        jQXB.postJSON(url, HttpMotheds.POST,
            {
                uid: uid, id: id === null ? 0 : id, tabId: tabId, content: content,
                validateCode: code,strTitle:title,strAuthor:author
            },
            success, failed);
    };
    
    this.deleteNote = function jsonDeleteNewNote(id, code, success, failed) {
        var url = self.baseurl + '/DeleteNewNote';
        jQXB.postJSON(url, HttpMotheds.POST, { newNoteId: id, validateCode: code },
                success, failed);
    };
    

    this.updateQote = function jsonUpdateQote(uid, id, pagekey, content, dividerid, x, y, code, success, failed) {
        var url = self.baseurl + '/UpdateQuickNoteWithKey';
        jQXB.postJSON(url, HttpMotheds.POST,
            {
                uid: uid, id: id === null ? 0 : id, pagekey: pagekey, content: content, dividerid:dividerid,
                x: Math.round(x), y: Math.round(y), validateCode: code, bFixed: false, bOpened: false
            },
            success, failed);
    };

    this.deleteQote = function jsonDeleteQote(id, code, success, failed) {
        var url = self.baseurl + '/DeleteQuickNote';
        jQXB.postJSON(url, HttpMotheds.POST, { quickNoteId: id, validateCode: code },
                success, failed);
    };
    
    this.mailTo = function jsonMailTo(id, code, from, to, sender, receiver, title, content, success, failed) {
        var url = self.baseurl + '/MailFirend';
        jQXB.postJSON(url, HttpMotheds.POST,
            {
                strSendMail: from, strToMail: to, folderID: id,
                strMailInfor: content, validateCode: code, SendName: sender,
                ToName: receiver, MailTitle: title
            },
            success, failed);
    };
};
