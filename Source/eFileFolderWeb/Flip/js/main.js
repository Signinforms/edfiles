/* efilefolders book */
function openEdit(event) {
   // console.log('213');
}


function getPageKey(page) {
    var item = null;
    for (var i in $items_auto_source) {
        item = $items_auto_source[i];
        if (item && item.start <= page && (item.start + item.count - 1) >= page) {
            if (item.isNote) {
                return item.pages[page - item.start];
            }
            else {
                return item.value + "-" + (page - item.start + 1); // this page need be the inner page order
            }
        }
    }

    return null;
}

function loadPage(page, args) {
    var book = $(".sj-book");    
    if (!book.turn('hasPage', page)) {
        var id = getPageKey(page);
        if (typeof (id) === 'string') {
            var pageView = new PageView(id, page, args.divider, args.opts);
            $book_contents[id] = pageView;
            $(".sj-book").turn('addPage', $(pageView.element), page);
            pageView.initialize();
        }
        else if (id !== null) {
            var detail = id;           
            var pageNode = new NotePage(detail.Id, page, args.divider, args.opts, detail);
            $book_contents[detail.Id] = pageNode;
            $(".sj-book").turn('addPage', pageNode.element, page);
            pageNode.initialize();
        }
        else {
            var element = $('<div class="empty" pageth="'+page+'"/>');
            $(".sj-book").turn('addPage', element, page);
        }
        
    }
}

function getPageDetail(page) {
    return $divider_details[page - 1];
}

function numberOfViews(book) {
    return book.turn('pages') / 2 + 1;
}


function getViewNumber(book, page) {
    return parseInt((page || book.turn('page')) / 2 + 1, 10);
}

function zoomHandle(e) {
    if ($('.sj-book').data().zoomIn)
        zoomOut();
    else if (e.target && $(e.target).hasClass('zoom-this')) {
        zoomThis($(e.target));
    }
}

function zoomThis(pic) {

    var tmpContainer = $('<div />', { 'class': 'zoom-pic' }),
        tmpPic = $('<img />'),
        zCenterX = $('#book-zoom').width() / 2,
        zCenterY = $('#book-zoom').height() / 2,
        bookPos = $('#book-zoom').offset(),
        picPos = {
            left: pic.offset().left - bookPos.left,
            top: pic.offset().top - bookPos.top
        };

    $('.sj-book').data().zoomIn = true;

    $('.sj-book').turn('disable', true);

    $(window).resize(zoomOut);

    tmpContainer.click(zoomOut);

    tmpPic.load(function() {
        var realWidth = $(this)[0].width,
            realHeight = $(this)[0].height,
            zoomFactor = realWidth / pic.width(),
            picPosition = {
                top: (picPos.top - zCenterY) * zoomFactor + zCenterY + bookPos.top,
                left: (picPos.left - zCenterX) * zoomFactor + zCenterX + bookPos.left
            },
            position = {
                top: ($(window).height() - realHeight) / 2,
                left: ($(window).width() - realWidth) / 2
            },
            translate = {
                top: position.top - picPosition.top,
                left: position.left - picPosition.left
            };


        $('#book-zoom').transform(
            'translate(' + translate.left + 'px, ' + translate.top + 'px)' +
                'scale(' + zoomFactor + ', ' + zoomFactor + ')');


        $('#book-zoom').bind('webkitTransitionEnd', function(e) {
            $(this).unbind('webkitTransitionEnd');

            if ($('.sj-book').data().zoomIn) {
                tmpContainer.
                    appendTo($('body'));

                $('body').css({ 'overflow': 'hidden' });

                tmpPic.css({
                    margin: position.top + 'px ' + position.left + 'px'
                }).
                    appendTo(tmpContainer).
                    fadeOut(0).
                    fadeIn(500);
            }

        });

    });

    tmpPic.attr('src', pic.attr('src'));

}

function zoomOut() {

    $('.sj-book').data().zoomIn = false;

    $(window).unbind('resize', zoomOut);

    $('.zoom-pic').remove();
    $('#book-zoom').transform('scale(1, 1)');

    $('#book-zoom').bind('webkitTransitionEnd', function(e) {
        $(this).unbind('webkitTransitionEnd');
        $('.sj-book').turn('disable', false);
        $('body').css({ 'overflow': 'auto' });
        moveBar(false);
    });
}

function setPreview(view) {

    var previewWidth = 115,
        previewHeight = 73,
        previewSrc = 'pics/preview.jpg',
        preview = $(_thumbPreview.children(':first')),
        numPages = (view == 1 || view == $('#slider').slider('option', 'max')) ? 1 : 2;


    if (preview.css('background-image') == 'none') {

        var width = (numPages == 1) ? previewWidth / 2 : previewWidth;

        _thumbPreview.
            addClass('no-transition').
            css({
                width: width + 15,
                height: previewHeight + 15,
                top: -previewHeight - 30,
                left: ($($('#slider').children(':first')).width() - width - 15) / 2
            });

        preview.css({
            backgroundImage: 'url(' + previewSrc + ')',
            width: width,
            height: previewHeight
        });


        setTimeout(function() {
            _thumbPreview.removeClass('no-transition');
        }, 0);
    }

    preview.css({
        backgroundPosition:
            '0px -' + ((view - 1) * previewHeight) + 'px'
    });
}


function isIE() {
    return navigator.userAgent.indexOf('MSIE') != -1;
}

function isChrome() {

    return navigator.userAgent.indexOf('Chrome') != -1;

}

function resizeBookWrapper() {
    //var width = $(window).width();
    //var height = $(window).height();
    var width = 430;
    var height = 330;
    //by shoaib - 11June2015

    //added by shoaib
    var screenWidth = screen.width;
    if (screenWidth < 500) {
        //var width2 = Math.floor(screenWidth - ((20 * screenWidth) / 100));
        //var height2 = Math.floor(width2 / 1.4);
        //var height1 = height2;
        //var width1 = Math.floor(height1 * 1.6);

        width2 = 256;
        height2 = 182;
        height1 = 182;
        width1 = 291;

        width = width1;
        height = height1;
        //alert(width1 + "-" + height1);
    }
    //added by shoaib


    //make the bookshelf show in central
    $('#book-wrapper').css({
        position: 'relative',
        overflow: 'visible',
        width: width,
        height: height,
        margin: '0 auto' /*by shoaib*/       
    });
}

function resizeViewport() {

	//commented by shoaib
    /*var width = $(window).width(),
        height = $(window).height(),
        options = $('.sj-book').turn('options');*/
	
	var width = 440,
        height = 340,
        options = $('.sj-book').turn('options');

    //added by shoaib
	var screenWidth = screen.width;
	if (screenWidth < 500) {
	    //var width2 = Math.floor(screenWidth - ((20 * screenWidth) / 100));
	    //var height2 = Math.floor(width2 / 1.4);
	    //var height1 = height2;
	    //var width1 = Math.floor(height1 * 1.6);

	    width2 = 256;
	    height2 = 182;
	    height1 = 182;
	    width1 = 291;

	    width = width1;
	    height = height1;
	    //alert(width1 + "-" + height1);
	}
    //added by shoaib
	
	$('#book-zoom').css({
	    width: width,
	    height: height,
	    top: -10,
	    left: 0,
        position:'absolute'
	}).zoom('resize');
		
		
    /*$('#book-zoom').css({
        width: width,
        height: height,
        top: -height / 2,
        left: -width / 2
    }).zoom('resize');*/

    if ($('.sj-book').turn('zoom') == 1) {
        var bound = calculateBound({
            width: options.width,
            height: options.height,
            boundWidth: Math.min(options.width, width),
            boundHeight: Math.min(options.height, height)
        });

        if (bound.width % 2 !== 0)
            bound.width -= 1;

        //$('.sj-book').css({ top: -bound.height / 2, left: -bound.width / 2 });
        $('.sj-book').css({ top: 40, left: 50 });

        if (screenWidth < 500) {
            $('.sj-book').css({ top: 12, left: 34 });
        }
    }

}

function calculateBound(d) {

    var bound = { width: d.width, height: d.height };

    if (bound.width > d.boundWidth || bound.height > d.boundHeight) {

        var rel = bound.width / bound.height;

        if (d.boundWidth / rel > d.boundHeight && d.boundHeight * rel <= d.boundWidth) {

            bound.width = Math.round(d.boundHeight * rel);
            bound.height = d.boundHeight;

        } else {

            bound.width = d.boundWidth;
            bound.height = Math.round(d.boundWidth / rel);

        }
    }

    return bound;
}

function largeMagazineWidth() {

    return $('#slider-zoom').slider('option', 'value');

}

function clickElement(element, func) {
    if ($.isTouch) {
        element.bind($.mouseEvents.up, func);
    } else {
        element.click(func);
    }
}


// Triggers an event

function trigger(eventName, context, args) {

    var event = $.Event(eventName);
    context.trigger(event, args);
    if (event.isDefaultPrevented())
        return 'prevented';
    else if (event.isPropagationStopped())
        return 'stopped';
    else
        return '';
}

function updateTips(t, tips) {
    /* var tips = $( ".validateTips" );*/
    tips
        .text(t)
        .addClass("ui-state-highlight");
    setTimeout(function() {
        tips.removeClass("ui-state-highlight", 1500);
    }, 500);
}

function checkLength(o, n, min, max, tips) {
    if (o.val().length > max || o.val().length < min) {
        o.addClass("ui-state-error");
        updateTips("Length of " + n + " must be between " +
            min + " and " + max + ".", tips);
        return false;
    } else {
        return true;
    }
}

function checkRegexp(o, regexp, n, tips) {
    if (!(regexp.test(o.val()))) {
        o.addClass("ui-state-error");
        updateTips(n, tips);
        return false;
    } else {
        return true;
    }
}


function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function getItemSource(id) {
    var count = $items_auto_source.length;
    for (var i = 0; i < count;i++) {
        if ($items_auto_source[i].value.toString() === id) {
            return $items_auto_source[i].path;
        }
    }
    
    return null;
}

function contains(items, value) {
    for (var i = 0; i < items.length; i++) {
        if (value === items[i])
            return true;
    }

    return false;
}



