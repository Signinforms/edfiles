
'use strict';
//缺省的下载文档url
var DEFAULT_URL = '';
var DEFAULT_SCALE = 'auto';
var DEFAULT_SCALE_DELTA = 1.1;
var UNKNOWN_SCALE = 0;

var CACHE_SIZE = 20;

var CSS_UNITS = 96.0 / 72.0;
var SCROLLBAR_PADDING = 40;
var VERTICAL_PADDING = 5;

var MAX_AUTO_SCALE = 1.25;
var MIN_SCALE = 0.25;
var MAX_SCALE = 4.0;

var IMAGE_DIR = './images/';


var SETTINGS_MEMORY = 20;

var SCALE_SELECT_CONTAINER_PADDING = 8;
var SCALE_SELECT_PADDING = 22;
var THUMBNAIL_SCROLL_MARGIN = -19;
var USE_ONLY_CSS_ZOOM = false;
var CLEANUP_TIMEOUT = 30000;

//标注的最小值
var ANNOT_MIN_SIZE = 10;

//渲染的状态值
var RenderingStates = {
    INITIAL: 0,
    RUNNING: 1,
    PAUSED: 2,
    FINISHED: 3
};

//内容的状态值
var FindStates = {
    FIND_FOUND: 0,
    FIND_NOTFOUND: 1,
    FIND_WRAPPED: 2,
    FIND_PENDING: 3
};

PDFJS.imageResourcesPath = '../images/';
//PDFJS.workerSrc = 'lib/pdf.worker.js';//changed by shoaib
PDFJS.workerSrc = 'Flip/lib/pdf.worker.js';

//PDFView代表PDF页面视图
var PDFView = {
    pdfDocuments: {},
    currentScale: UNKNOWN_SCALE, //当前视图比例
    currentScaleValue: null,
    startedTextExtraction: false,
    pageTexts: {}, //页面文本
    container: null, //视图的容器
    initialized: false, //是否初始化
    fellback: false,
    isFullscreen: false, //是否全屏
    previousScale: null, //上一视图比例

    higherPriorityPages: [],
    highestPriorityPages: [0, 1],

    // called once when the document is loaded
    initialize: function pdfViewInitialize() {
        var self = this;
        this.initialized = true;
    },

    open: function pdfViewOpen(id, url, scale, password) {
        var parameters = { password: password };
        if (typeof url === 'string') { // URL
            parameters.url = url;
        } else if (url && 'byteLength' in url) { // ArrayBuffer
            parameters.data = url;
        }

        this.pdfDocument = null;
        var self = this;
        //self.loading = true;
        PDFJS.getDocument(parameters).then(
            function getDocumentCallback(pdfDocument) {
                self.load(id, pdfDocument, scale);
                //self.loading = false;
            },
            function getDocumentError(message, exception) {
                if (exception && exception.name === 'PasswordException') {
                    if (exception.code === 'needpassword') {
                        var promptString = 'PDF is protected by a password:';
                        password = prompt(promptString);
                        if (password && password.length > 0) {
                            return PDFView.open(url, scale, password);
                        }
                    }
                }

                var loadingErrorMessage = 'An error occurred while loading the PDF.';

                if (exception && exception.name === 'InvalidPDFException') {
                    loadingErrorMessage = 'Invalid or corrupted PDF file.';
                }

                $bookshelfPromises.GetValue(id).resolve(id);

                //self.loading = false;
            }
            //,
            //function getDocumentProgress(progressData) {

            //}
        );
    },

    fallback: function pdfViewFallback() {

    },

    load: function pdfViewLoad(fid, pdfDocument, scale) {
        this.pdfDocuments[fid] = pdfDocument;

        var pagesCount = pdfDocument.numPages;
        var pagePromises = [];
        for (var i = 1; i <= pagesCount; i++)
            pagePromises.push(pdfDocument.getPage(i));

        var pagesPromise = PDFJS.Promise.all(pagePromises);
        var pageKey = "";
        pagesPromise.then(function (promisedPages) {
            for (var i = 1; i <= pagesCount; i++) {
                var page = promisedPages[i - 1];
                var pageRef = page.pageInfo;
                pageKey = fid + '-' + (pageRef.pageIndex + 1); // page order from 1, not 0
                $page_promises.GetValue(pageKey).resolve(page);
            }
        });

        $bookshelfPromises.GetValue(fid).resolve(fid);

        // check current pages and draw
        var pageths = $('.sj-book').turn("view");
        var selectors = "div[pageref][pageth='" + pageths[0] + "']";
        var k = 0;
        while (++k < pageths.length) {
            selectors += ",div[pageref][pageth='" + pageths[k] + "']";
        }

        $('.sj-book').find(selectors).each(function (index, elment) {
            var key = $(this).attr('pageref');
            $page_promises.GetValue(key).then(function (pageData) { //onData to then
                $book_contents[key].setPage(pageData);
                $book_contents[key].renderView('page');
            });

        });

        // end for higherPages block
    },

    getOutputScale: function pdfViewGetOutputDPI() {
        var pixelRatio = 'devicePixelRatio' in window ? window.devicePixelRatio : 1;
        return {
            sx: pixelRatio,
            sy: pixelRatio,
            scaled: pixelRatio != 1
        };
    },

    parseScale: function pdfViewParseScale(value, resetAutoSettings, noScroll) {
        if ('custom' == value)
            return;

        var scale = parseFloat(value);
        this.currentScaleValue = value;
        if (scale) {
            this.setScale(scale, true, noScroll);
            return;
        }

        var container = this.container;
        var currentPage = this.pages[this.page - 1];
        if (!currentPage) {
            return;
        }

        var pageWidthScale = (container.clientWidth - SCROLLBAR_PADDING) /
            currentPage.width * currentPage.scale / CSS_UNITS;
        var pageHeightScale = (container.clientHeight - VERTICAL_PADDING) /
            currentPage.height * currentPage.scale / CSS_UNITS;
        switch (value) {
            case 'page-actual':
                scale = 1;
                break;
            case 'page-width':
                scale = pageWidthScale;
                break;
            case 'page-height':
                scale = pageHeightScale;
                break;
            case 'page-fit':
                scale = Math.min(pageWidthScale, pageHeightScale);
                break;
            case 'auto':
                scale = Math.min(1.0, pageWidthScale);
                break;
        }
        this.setScale(scale, resetAutoSettings, noScroll);

        selectScaleOption(value);
    },

    zoomIn: function pdfViewZoomIn() {
        var newScale = (this.currentScale * DEFAULT_SCALE_DELTA).toFixed(2);
        newScale = Math.min(MAX_SCALE, newScale);
        this.parseScale(newScale, true);
    },

    zoomOut: function pdfViewZoomOut() {
        var newScale = (this.currentScale / DEFAULT_SCALE_DELTA).toFixed(2);
        newScale = Math.max(MIN_SCALE, newScale);
        this.parseScale(newScale, true);
    },

    beforePrint: function pdfViewSetupBeforePrint(pageView) {
        pageView.beforePrint();
    },

    afterPrint: function pdfViewSetupAfterPrint() {
        var div = document.getElementById('printContainer');
        while (div.hasChildNodes())
            div.removeChild(div.lastChild);
    },

    get supportsFullscreen() {
        var doc = document.documentElement;
        var support = doc.requestFullscreen || doc.mozRequestFullScreen ||
                      doc.webkitRequestFullScreen;

        // Disable fullscreen button if we're in an iframe
        if (!!window.frameElement)
            support = false;

        Object.defineProperty(this, 'supportsFullScreen', {
            value: support,
            enumerable: true,
            configurable: true,
            writable: false
        });
        return support;
    },

    setTitleUsingUrl: function pdfViewSetTitleUsingUrl(url) {
        this.url = url;
        try {
            this.setTitle(decodeURIComponent(getFileName(url)) || url);
        } catch (e) {
            this.setTitle(url);
        }
    },

    setTitle: function pdfViewSetTitle(title) {
        //document.title = title;
    },

    download: function pdfViewDownload(pdfurl) {
        var url = pdfurl.split('#')[0];
        window.open(url, '_blank');
    }
};
//end of PDFView

//页面视图
//container:div#viewer
var PageView = function pageView(id, page, divider, opts) {
    this.id = id;
    this.page = page;

    this.pdfPage = null;

    this.rotation = 0;
    this.scale = 1.0;
    this.viewport = null;
    this.divider = divider;
    this.opts = opts;

    this.renderingState = RenderingStates.INITIAL;
    this.resume = null;

    this.textContent = null;
    this.textLayer = null;

    this.element = null;
    this.loadingIconDiv = null;
    this.div = null;

    this.qotes = new Hashtable();

    var div = this.el = $('<div id="p' + this.id + '"/>').css({ width: Math.floor(this.opts.width / 2) + 'px', height: Math.floor(this.opts.height) + 'px' });
    this.loadingIconDiv = $('<div class="loader"></div>');
    this.element = $('<div pageref=' + this.id + ' pageth=' + this.page + ' class="page-element"/>').append(this.loadingIconDiv).append(div);
    
    this.div = div;
    var self = this;

    var qnotes = $dividers.Items[$current_divider_nth]['qotes'];
    var count = qnotes.length;
    for (var i = 0; i < count; i++) {
        var qote = qnotes[i];
        if (qote.PageKey === id) {
            var qbox = new QoteBox(qote.Id, this.element, { pagekey: this.id, content: qote.Content, x: qote.X, y: qote.Y, visible: false }, false);
            this.qotes.Add(qote.Id,qbox);
        }
    }

    this.destroy = function pageViewDestroy() {
        this.update();
        this.pdfPage.destroy();
    };

    this.resize = function pageScaleResize(newScale) {
        if (typeof newScale === 'undefined' || this.scale === newScale) {
            return;
        }

        this.update(newScale, this.rotation);
        this.renderView('page');
    };

    this.updating = function pageUpdating(scale, rotation) {
        
        if (typeof scale === 'undefined' || this.scale === scale) {
            return;
        }

        this.renderingState = RenderingStates.INITIAL;
        this.resume = null;

        if (typeof rotation !== 'undefined') {
            this.rotation = rotation;
        }

        this.scale = scale || this.scale;

        //this.pdfPage.rotate为页面 自身偏转，this.rotation为整体旋转度
        var totalRotation = (this.rotation + this.pdfPage.rotate) % 360;
        var viewport = this.pdfPage.getViewport(this.scale, totalRotation);

        this.viewport = viewport;

        this.div.css({ width: Math.floor(this.opts.width * scale / 2) + 'px', height: Math.floor(this.opts.height * scale) + 'px' });

        this.div.empty();
        this.div.removeAttr('data-loaded');

        delete this.canvas;
    };

    this.update = function pageViewUpdate(scale, rotation) {
        this.renderingState = RenderingStates.INITIAL;
        this.resume = null;

        if (typeof rotation !== 'undefined') {
            this.rotation = rotation;
        }

        this.scale = scale || this.scale;

        //this.pdfPage.rotate为页面 自身偏转，this.rotation为整体旋转度
        var totalRotation = (this.rotation + this.pdfPage.rotate) % 360;
        var viewport = this.pdfPage.getViewport(this.scale, totalRotation);

        this.viewport = viewport;

        this.div.css({ width: Math.floor(this.opts.width * scale / 2) + 'px', height: Math.floor(this.opts.height * scale) + 'px' });

        this.div.empty();
        this.div.removeAttr('data-loaded');

        delete this.canvas;

        if (!this.loadingIconDiv) {
            this.loadingIconDiv = $('<div class="loader"></div>');
            this.div.append(this.loadingIconDiv);
        }        
    };

    Object.defineProperty(this, 'width', {
        get: function PageView_getWidth() {
            return this.viewport.width;
        },
        enumerable: true
    });

    Object.defineProperty(this, 'height', {
        get: function PageView_getHeight() {
            return this.viewport.height;
        },
        enumerable: true
    });

    this.getTextContent = function pageviewGetTextContent() {
        if (!this.textContent) {
            this.textContent = this.pdfPage.getTextContent();
        }
        return this.textContent;
    };

    //Render the page view and page text
    this.draw = function pageviewDraw(callback) {
        if (this.renderingState !== RenderingStates.INITIAL)
            error('Must be in new state before drawing');
     
        this.renderingState = RenderingStates.RUNNING;

        this.viewport = this.pdfPage.getViewport(1);

        var canvas = document.createElement('canvas');
        canvas.id = 'page' + this.id;
        canvas.mozOpaque = true;
        this.div.append(canvas);
        this.canvas = canvas;

        var textLayerDiv = null;

        if (!PDFJS.disableTextLayer) {
            textLayerDiv = document.createElement('div');
            textLayerDiv.className = 'textLayer';
            this.div.append(textLayerDiv);
        }

        var textLayer = this.textLayer =
            textLayerDiv ? new TextLayerBuilder(textLayerDiv, this.id) : null;

        var scale = this.scale, viewport = this.viewport;
        var pagexScale = (this.opts.width * scale) / (viewport.width * 2);
        var pageyScale = (this.opts.height * scale) / (viewport.height);
        var realScale = Math.min(pagexScale, pageyScale);

        canvas.width = Math.floor(this.opts.width * scale * 0.5);
        this.viewport = this.pdfPage.getViewport(canvas.width / viewport.width);
        canvas.height = Math.floor(this.opts.height * scale);

        CustomStyle.setProp('transformOrigin', canvas, '0% 0%');
        if (textLayerDiv)
            CustomStyle.setProp('transformOrigin', textLayerDiv, '0% 0%');

        var ctx = canvas.getContext('2d');
        ctx.save();
        ctx.fillStyle = 'rgb(255, 255, 255)';
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        ctx.restore();

        // Rendering area
        var self = this;

        function pageViewDrawCallback(error) {
            self.renderingState = RenderingStates.FINISHED;

            if (self.loadingIconDiv) {
                self.loadingIconDiv.remove();
                delete self.loadingIconDiv;
            }

            if (error) {
                PDFView.error("An error occurred while rendering the page.", error);
            }

            self.stats = self.pdfPage.stats;
            self.updateStats();

            callback();
        }

        var renderContext = {
            canvasContext: ctx,
            viewport: this.viewport,
            textLayer: textLayer,
            continueCallback: function pdfViewcContinueCallback(cont) {
                if (PDFView.highestPriorityPages.indexOf(self.page)<0) {
		            self.renderingState = RenderingStates.PAUSED;
		            self.resume = function resumeCallback() {
			            self.renderingState = RenderingStates.RUNNING;
			            cont();
		            };
		            return;
                }
                cont();
            }
        };

        this.pdfPage.render(renderContext).then(
            function pdfPageRenderCallback() {
                pageViewDrawCallback(null);
            },
            function pdfPageRenderError(error) {
                pageViewDrawCallback(error);
            }
        );

        //if textlayer is done, then get textcontent and set text on this div
        if (textLayer) {
            this.getTextContent().then(
                function textContentResolved(textContent) {
                    textLayer.setTextContent(textContent);
                }
            );
        }

        this.div.attr('data-loaded', true);
    };

    this.updateStats = function pageViewUpdateStats() {
        if (PDFJS.pdfBug && Stats.enabled) {
            var stats = this.stats;
            Stats.add(this.id, stats);
        }
    };

    this.setPage = function setPdfPage(val) {
        this.pdfPage = val;
    };
    this.getPage = function getPdfPage() {
        return this.pdfPage;
    };

    this.renderHighestPriority = function pdfViewRenderHighestPriority() {
        var pages = PDFView.highPriorityPages; //PDFView.highestPriorityPages;
        var selectors = "div[pageref][pageth='" + pages[0] + "']";
        var k = 0;
        while (++k < pages.length) {
            selectors += ",div[pageref][pageth='" + pages[k] + "']";
        }

        $('.sj-book').find(selectors).each(function (index, elment) {
            var key = $(this).attr('pageref');
            $page_promises.GetValue(key).then(function (data) {// onData to then
                $book_contents[key].setPage(data);
                $book_contents[key].renderView('page');
            });
        });
    };

    this.isViewFinished = function pdfViewNeedsRendering() {
        return this.renderingState === RenderingStates.FINISHED;
    };

    // Render a page or thumbnail view. This calls the appropriate function based
    // on the views state. If the view is already rendered it will return false.
    this.renderView = function pdfViewRender(type) {
        var state = this.renderingState;
        switch (state) {
            case RenderingStates.FINISHED:
                return false;
            case RenderingStates.PAUSED:
                this.resume();
                break;
            case RenderingStates.RUNNING:
                break;
            case RenderingStates.INITIAL:
                this.draw(this.renderHighestPriority.bind(this));
                break;
        }
        return true;
    };

    this.initialize = function initializePage() {
        var items = this.qotes.GetValues();
        for (var i = 0; i < items.length; i++) {
            items[i].initialize();
        }
    };

    this.include = function includePage(arr, obj) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == obj) return true;
        }
        return false;
    };

    this.beforePrint = function pageViewBeforePrint() {
        var pdfPage = this.pdfPage;
        var viewport = pdfPage.getViewport(1.5);
        // Use the same hack we use for high dpi displays for printing to get better
        // output until bug 811002 is fixed in FF.

        var printContainer = $('#printPreviewContainer');
        printContainer.attr('pageref', this.id);

        var PRINT_OUTPUT_SCALE = 1;
        var canvas = this.canvas = document.getElementById('print-canvas');
        canvas.width = Math.floor(viewport.width) * PRINT_OUTPUT_SCALE;
        canvas.height = Math.floor(viewport.height) * PRINT_OUTPUT_SCALE;
        canvas.style.width = (PRINT_OUTPUT_SCALE * viewport.width) + 'px';
        canvas.style.height = (PRINT_OUTPUT_SCALE * viewport.height) + 'px';

        var cssScale = 'scale(' + (1 / PRINT_OUTPUT_SCALE) + ', ' +
                                  (1 / PRINT_OUTPUT_SCALE) + ')';
        CustomStyle.setProp('transform', canvas, cssScale);
        CustomStyle.setProp('transformOrigin', canvas, '0% 0%');
        var ctx = canvas.getContext('2d');

        ctx.save();
        ctx.fillStyle = 'rgb(255, 255, 255)';
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        ctx.restore();
        ctx.scale(PRINT_OUTPUT_SCALE, PRINT_OUTPUT_SCALE);

        var renderContext = {
            canvasContext: ctx,
            viewport: viewport
        };

        pdfPage.render(renderContext).then(function () {
        }, function (error) {
            //console.error(error);
        });
    };
};

//文本大纲

// optimised CSS custom property getter/setter
var CustomStyle = (function CustomStyleClosure() {

    // As noted on: http://www.zachstronaut.com/posts/2009/02/17/
    //              animate-css-transforms-firefox-webkit.html
    // in some versions of IE9 it is critical that ms appear in this list
    // before Moz
    var prefixes = ['ms', 'Moz', 'Webkit', 'O'];
    var _cache = {};

    function CustomStyle() {
    }

    CustomStyle.getProp = function get(propName, element) {
        // check cache only when no element is given
        if (arguments.length == 1 && typeof _cache[propName] == 'string') {
            return _cache[propName];
        }

        element = element || document.documentElement;
        var style = element.style, prefixed, uPropName;

        // test standard property first
        if (typeof style[propName] == 'string') {
            return (_cache[propName] = propName);
        }

        // capitalize
        uPropName = propName.charAt(0).toUpperCase() + propName.slice(1);

        // test vendor specific properties
        for (var i = 0, l = prefixes.length; i < l; i++) {
            prefixed = prefixes[i] + uPropName;
            if (typeof style[prefixed] == 'string') {
                return (_cache[propName] = prefixed);
            }
        }

        //if all fails then set to undefined
        return (_cache[propName] = 'undefined');
    };

    CustomStyle.setProp = function set(propName, element, str) {
        var prop = this.getProp(propName);
        if (prop != 'undefined')
            element.style[prop] = str;
    };

    return CustomStyle;
})();

//文本层的对象
var TextLayerBuilder = function textLayerBuilder(textLayerDiv, pageIdx) {
    var textLayerFrag = document.createDocumentFragment();

    this.textLayerDiv = textLayerDiv;
    this.layoutDone = false;
    this.divContentDone = false;
    this.pageIdx = pageIdx;
    this.matches = [];

    this.beginLayout = function textLayerBuilderBeginLayout() {
        this.textDivs = [];
        this.textLayerQueue = [];
        this.renderingDone = false;
    };

    this.endLayout = function textLayerBuilderEndLayout() {
        this.layoutDone = true;
        this.insertDivContent();
    };

    //渲染文本层
    this.renderLayer = function textLayerBuilderRenderLayer() {
        var self = this;
        var textDivs = this.textDivs;
        var textLayerDiv = this.textLayerDiv;
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');

        // No point in rendering so many divs as it'd make the browser unusable
        // even after the divs are rendered
        var MAX_TEXT_DIVS_TO_RENDER = 100000;
        if (textDivs.length > MAX_TEXT_DIVS_TO_RENDER)
            return;

        for (var i = 0, ii = textDivs.length; i < ii; i++) {
            var textDiv = textDivs[i];
            textLayerFrag.appendChild(textDiv);

            ctx.font = textDiv.style.fontSize + ' ' + textDiv.style.fontFamily;
            var width = ctx.measureText(textDiv.textContent).width;

            if (width > 0) {
                var textScale = textDiv.dataset.canvasWidth / width;

                CustomStyle.setProp('transform', textDiv,
                    'scale(' + textScale + ', 1)');
                CustomStyle.setProp('transformOrigin', textDiv, '0% 0%');

                textLayerDiv.appendChild(textDiv);
            }
        }

        this.renderingDone = true;
   
        this.updateMatches();

        textLayerDiv.appendChild(textLayerFrag);
    };

    this.setupRenderLayoutTimer = function textLayerSetupRenderLayoutTimer() {
        var RENDER_DELAY = 50; // in ms
        var self = this;
        setTimeout(function () {
            self.renderLayer();
        }, RENDER_DELAY);
    };

    this.appendText = function textLayerBuilderAppendText(geom) {
        var textDiv = document.createElement('div');

        // vScale and hScale already contain the scaling to pixel units
        var fontHeight = geom.fontSize * geom.vScale;
        textDiv.dataset.canvasWidth = geom.canvasWidth * geom.hScale;
        textDiv.dataset.fontName = geom.fontName;

        textDiv.style.fontSize = fontHeight + 'px';
        textDiv.style.fontFamily = geom.fontFamily;
        textDiv.style.left = geom.x + 'px';
        textDiv.style.top = (geom.y - fontHeight) + 'px';

        // The content of the div is set in the `setTextContent` function.
        this.textDivs.push(textDiv);
    };

    this.insertDivContent = function textLayerUpdateTextContent() {
        // Only set the content of the divs once layout has finished, the content
        // for the divs is available and content is not yet set on the divs.
        if (!this.layoutDone || this.divContentDone || !this.textContent)
            return;

        this.divContentDone = true;

        var textDivs = this.textDivs;
        var bidiTexts = this.textContent.bidiTexts;

        for (var i = 0; i < bidiTexts.length; i++) {
            var bidiText = bidiTexts[i];
            var textDiv = textDivs[i];

            textDiv.textContent = bidiText.str;
            textDiv.dir = bidiText.ltr ? 'ltr' : 'rtl';
        }

        this.setupRenderLayoutTimer();
    };

    //insert text content for textdivlayer
    this.setTextContent = function textLayerBuilderSetTextContent(textContent) {
        this.textContent = textContent;
        this.insertDivContent();
    };

    this.convertMatches = function textLayerBuilderConvertMatches(matches) {
        var i = 0;
        var iIndex = 0;
        var bidiTexts = this.textContent.bidiTexts;
        var end = bidiTexts.length - 1;
        var queryLen = PDFFindController.state.query.length;

        var lastDivIdx = -1;
        var pos;

        var ret = [];

        // Loop over all the matches.
        for (var m = 0; m < matches.length; m++) {
            var matchIdx = matches[m];
            // # Calculate the begin position.

            // Loop over the divIdxs.
            while (i !== end && matchIdx >= (iIndex + bidiTexts[i].str.length)) {
                iIndex += bidiTexts[i].str.length;
                i++;
            }

            // TODO: Do proper handling here if something goes wrong.
            if (i == bidiTexts.length) {
                //console.error('Could not find matching mapping');
            }

            var match = {
                begin: {
                    divIdx: i,
                    offset: matchIdx - iIndex
                }
            };

            // # Calculate the end position.
            matchIdx += queryLen;

            // Somewhat same array as above, but use a > instead of >= to get the end
            // position right.
            while (i !== end && matchIdx > (iIndex + bidiTexts[i].str.length)) {
                iIndex += bidiTexts[i].str.length;
                i++;
            }

            match.end = {
                divIdx: i,
                offset: matchIdx - iIndex
            };
            ret.push(match);
        }

        return ret;
    };

    this.renderMatches = function textLayerBuilder_renderMatches(matches) {
        // Early exit if there is nothing to render.
        if (matches.length === 0) {
            return;
        }

        var bidiTexts = this.textContent.bidiTexts;
        var textDivs = this.textDivs;
        var prevEnd = null;
        var isSelectedPage = true; //this.pageIdx === PDFFindController.selected.pageIdx;
        var selectedMatchIdx = 0; //PDFFindController.selected.matchIdx;
        var highlightAll = PDFFindController.state.highlightAll;

        var infty = {
            divIdx: -1,
            offset: undefined
        };

        function beginText(begin, className) {
            var divIdx = begin.divIdx;
            var div = textDivs[divIdx];
            div.textContent = '';

            var content = bidiTexts[divIdx].str.substring(0, begin.offset);
            var node = document.createTextNode(content);
            if (className) {
                var isSelected = false; //isSelectedPage && divIdx === selectedMatchIdx;
                var span = document.createElement('span');
                span.className = className + (isSelected ? ' selected' : '');
                span.appendChild(node);
                div.appendChild(span);
                return;
            }
            div.appendChild(node);
        }

        function appendText(from, to, className) {
            var divIdx = from.divIdx;
            var div = textDivs[divIdx];

            var content = bidiTexts[divIdx].str.substring(from.offset, to.offset);
            var node = document.createTextNode(content);
            if (className) {
                var span = document.createElement('span');
                span.className = className;
                span.appendChild(node);
                div.appendChild(span);
                return;
            }
            div.appendChild(node);
        }

        function highlightDiv(divIdx, className) {
            textDivs[divIdx].className = className;
        }

        var i0 = selectedMatchIdx, i1 = i0 + 1, i;

        if (highlightAll) {
            i0 = 0;
            i1 = matches.length;
        } else if (!isSelectedPage) {
            // Not highlighting all and this isn't the selected page, so do nothing.
            return;
        }

        for (i = i0; i < i1; i++) {
            var match = matches[i];
            var begin = match.begin;
            var end = match.end;

            var isSelected = isSelectedPage; //&& i === selectedMatchIdx;
            var highlightSuffix = (isSelected ? ' selected' : '');
           
            // Match inside new div.
            if (!prevEnd || begin.divIdx !== prevEnd.divIdx) {
                // If there was a previous div, then add the text at the end
                if (prevEnd !== null) {
                    appendText(prevEnd, infty);
                }
                // clears the divs and set the content until the begin point.
                beginText(begin);
            } else {
                appendText(prevEnd, begin);
            }

            if (begin.divIdx === end.divIdx) {
                appendText(begin, end, 'highlight' + highlightSuffix);
            } else {
                appendText(begin, infty, 'highlight begin' + highlightSuffix);
                for (var n = begin.divIdx + 1; n < end.divIdx; n++) {
                    highlightDiv(n, 'highlight middle' + highlightSuffix);
                }
                beginText(end, 'highlight end' + highlightSuffix);
            }
            prevEnd = end;
        }

        if (prevEnd) {
            appendText(prevEnd, infty);
        }
    };

    this.updateMatches = function textLayerUpdateMatches() {
        // Only show matches, once all rendering is done.
        if (!this.renderingDone)
            return;

        // Clear out all matches.
        var matches = this.matches;
        var textDivs = this.textDivs;
        var bidiTexts = this.textContent.bidiTexts;
        var clearedUntilDivIdx = -1;

        // 清空当前所有的高亮匹配项
        for (var i = 0; i < matches.length; i++) {
            var match = matches[i];
            var begin = Math.max(clearedUntilDivIdx, match.begin.divIdx);
            for (var n = begin; n <= match.end.divIdx; n++) {
                var div = textDivs[n];
                div.textContent = bidiTexts[n].str;
                div.className = '';
            }
            clearedUntilDivIdx = match.end.divIdx + 1;
        }

        if (!PDFFindController.active) //PDFFindController查询栏关闭时不高亮
            return;

        this.matches = matches =
            this.convertMatches(PDFFindController.pageMatches.GetValue(this.pageIdx) || []);
  
        this.renderMatches(this.matches);
    };
};

var PDFFindController = {
    //PDFFindController：查找PDF的控制器
    startedTextExtraction: false, // 正在查询

    extractTextPromises: new Hashtable, //？

    // If active, find results will be highlighted.
    //表示高亮是否激活，即当查询栏显示时应该高亮，而隐藏时不高亮页面的匹配项
    active: false,

    // Stores the text for each page.
    pageContents: [], // 页内容数组

    pageMatches: [], // 匹配页数组

    // Where find algorithm currently is in the document.
    offset: {
        pageIdx: null,
        matchIdx: null
    },

    resumeCallback: null,

    state: null, //this.state功能状态，例如query

    dirtyMatch: false, //查询修改标志

    findTimeout: null, //查询超时

    //初始化函数：绑定查询的事件
    initialize: function () {
        var events = [
            'find'
        ];

        this.handleEvent = this.handleEvent.bind(this);
        //绑定查询的事件
        for (var i = 0; i < events.length; i++) {
            window.addEventListener(events[i], this.handleEvent);
        }
    },

    //查询指定页的文本内容，key为页面的key,查询时，有可能该页并没有开始绘制完成，
    //只是内容已经获取
    calcFindMatch: function (key) {
        var pageContent = this.pageContents.GetValue(key); //页面内容
        var query = this.state.query; // work具体要查询的值
        var caseSensitive = this.state.caseSensitive;
        var queryLen = query.length; //queryLen为查询值的长度

        if (queryLen === 0) { // 查询值的长度为0，则返回           
            return;
        }

        if (!caseSensitive) { 
            pageContent = pageContent.toLowerCase(); //该页内容变小写
            query = query.toLowerCase(); // query为输入的查询值，也变为小写
        }

        var matches = []; //匹配结果数组

        var matchIdx = -queryLen;
        while (true) {
            matchIdx = pageContent.indexOf(query, matchIdx + queryLen); //从指定索引查询
            if (matchIdx === -1) {
                break;
            }

            matches.push(matchIdx); //找到匹配项放入matches
        }
        //this.pageMatches保存所有页面匹配项索引的数组：key哪个页面，matches匹配项
        if (matches.length > 0) {
            this.pageMatches.Add(key, matches);

            //查询结果显示在搜索结果栏中<li>
            this.updateSearchBox(key, matches);

            //update page and highlight
            this.updatePage(key, matches);
        }

        return matches.length; //返回的matches长度，用于统计并显示查询结果的总数

    },

    updateSearchBox: function (key, matches) {
        var pageContent = this.pageContents.GetValue(key); //页面内容
        var length = this.state.query.length;
        var pageNumb = this.getPageNumb(key);
        var begin, end, textbegin, text, textEnd;

        var items = $('#book-search-items');

        for (var i = 0; i < matches.length; i++) {
            begin = matches[i] - 10;
            end = matches[i] + length;
            textbegin = pageContent.substring(begin, matches[i]);
            textEnd = pageContent.substring(end, end + 10);

            var pagelink = $('<a pageref = ' + pageNumb + '>[p' + pageNumb + ']</a>');
            var il = $('<li>' + textbegin + '<span class="highlight">' + this.state.query + '</span>'
                + textEnd + '..</li>');

            il.append(pagelink);

            pagelink.click(function () {
                var it = $(this).attr('pageref');
                $(".sj-book").turn('page', it);
            });

            items.append(il);
        }
    },

    getPageNumb: function (key) {
        var strs = key.split("-");
        var item = null;
        for (var i in $items_auto_source) {
            item = $items_auto_source[i];
            if (item && strs[0] === item.value.toString()) {
                return parseInt(item.start) + parseInt(strs[1]) - 1;
            }
        }

        return null;
    },

    // 抽取文本
    extractText: function () {
        //如果已经在抽取文本则返回
        if (this.startedTextExtraction) {
            return null;
        }
        this.startedTextExtraction = true;

        this.pageContents = new Hashtable;
        //抽取所有页面的内容压栈  
        for (var pdfx in $items_auto_source) {
            var item_source = $items_auto_source[pdfx];
            if (item_source && !item_source.isNote) {
                var pdfdocument = PDFView.pdfDocuments[item_source.value];
                if (pdfdocument) {
                    for (var i = 1, ii = item_source.count; i <= ii; i++) {
                        var key = item_source.value + "-" + i;

                        if (!this.extractTextPromises.ContainsKey(item_source.value))
                            this.extractTextPromises.Add(item_source.value, new Array);

                        var promises = this.extractTextPromises.GetValue(item_source.value);
                        promises.push(new PDFJS.Promise());
                    }
                }
            }

        }

        var self = this;
        //定义extractPageText函数，extractPageText(0)进行递归调用

        function extractPageText(id, pageIndex) {
            var promises = self.extractTextPromises.GetValue(id);
            var key = id + "-" + pageIndex;
            $page_promises.GetValue(key).then(function (pageData) { // onData to then
                pageData.getTextContent().then(function textContentResolved(data) {
                    // Build the find string.
                    var bidiTexts = data.bidiTexts;
                    var str = '';

                    for (var i = 0; i < bidiTexts.length; i++) {
                        str += bidiTexts[i].str;
                    }

                    // Store the pageContent as a string.
                    self.pageContents.Add(key, str);

                    promises[pageIndex - 1].resolve(key);
                    if ((pageIndex + 1) <= promises.length)
                        extractPageText(id, pageIndex + 1);
                }
                );
            });
        }

        var items = this.extractTextPromises.GetKeys();
        for (var idx in items) {
            extractPageText(items[idx], 1);
        }

        return this.extractTextPromise;
    },

    //所有查询文本的事件响应函数
    handleEvent: function (e) {

        if (this.trim(e.detail.query) === '') return;

        this.state = e.detail;

        this.extractText(); // 从页面中抽取文本

        clearTimeout(this.findTimeout);

        this.nextMatch();
    },

    updatePage: function (key, matches) {
        var pageView = $book_contents[key];
        if (pageView && pageView.textLayer) { //仅更新绘制完成的页面
            pageView.textLayer.updateMatches();
        }
    },

    trim: function (str) {
        for (var i = 0; i < str.length && str.charAt(i) == "   "; i++);
        for (var j = str.length; j > 0 && str.charAt(j - 1) == "   "; j--);
        if (i > j) return "";
        return str.substring(i, j);
    },

    nextMatch: function () {
        //保存上次查询的结果
        var previous = this.state.findPrevious;
        this.active = true;

        // Need to recalculate the matches, reset everything.
        this.dirtyMatch = false;

        this.hadMatch = false;
        this.resumeCallback = null;
        this.resumePageIdx = null;
        this.pageMatches = new Hashtable;
        var self = this;

        var items = $('#book-search-items');
        items.empty();
        var results_count = 0;

        var keys = this.extractTextPromises.GetKeys();
        for (var idx in keys) {
            var id = keys[idx];
            var pages = this.extractTextPromises.GetValue(id); // 搜索页集合
            var count = pages.length;
            for (var i = 0; i < count; i++) {
                pages[i].then(function (key) { // onData to then 
                    results_count += self.calcFindMatch(key);
                    $('#results-count').text(results_count);
                });
            }
        }


    },

    updateMatch: function (found) {
        var state = FindStates.FIND_NOTFOUND;
        var wrapped = this.offset.wrapped;
        this.offset.wrapped = false;
        if (found) {
            var previousPage = this.selected.pageIdx;
            this.selected.pageIdx = this.offset.pageIdx;
            this.selected.matchIdx = this.offset.matchIdx;
            state = wrapped ? FindStates.FIND_WRAPPED : FindStates.FIND_FOUND;
            // Update the currently selected page to wipe out any selected matches.
            if (previousPage !== -1 && previousPage !== this.selected.pageIdx) {
                this.updatePage(previousPage);
            }
        }
        this.updateUIState(state, this.state.findPrevious);
        if (this.selected.pageIdx !== -1) {
            this.updatePage(this.selected.pageIdx, true);
        }
    },

    updateUIState: function (state, previous) {
   
    }
};

//PDFFindBar代表搜索栏
var PDFFindBar = {
    // TODO: Enable the FindBar *AFTER* the pagesPromise in the load function
    // got resolved

    opened: false,
    //初始化与查询栏按钮相关的事件
    initialize: function () {
        this.bar = document.getElementById('book-search');
        this.findField = document.getElementById('search-item-text');

        var self = this;

        this.findField.addEventListener('input', function () {
            self.dirtyMatch = true;
        });

        this.bar.addEventListener('keydown', function (evt) {
            switch (evt.keyCode) {
                case 13:
                    // Enter
                    if (evt.target === self.findField) {
                        self.dispatchEvent('', evt.shiftKey);
                    }
                    break;
                case 27:
                    // Escape
                    self.close();
                    break;
            }
        });

        document.getElementById('book-search-button').addEventListener('click', function () {
            self.dispatchEvent('', false);
        });

    },

    dispatchEvent: function (aType, aFindPrevious) {
        var event = document.createEvent('CustomEvent');
        event.initCustomEvent('find' + aType, true, true, {
            query: this.findField.value,
            caseSensitive: false,
            highlightAll: true,
            findPrevious: aFindPrevious
        });
        return window.dispatchEvent(event);
    },

    updateUIState: function (state, previous) {
        var notFound = false;
        var findMsg = '';
        var status = '';

        switch (state) {
            case FindStates.FIND_FOUND:
                break;
            case FindStates.FIND_PENDING:
                status = 'pending';
                break;
            case FindStates.FIND_NOTFOUND:
                //findMsg = 'Phrase not found';
                notFound = true;
                break;
            case FindStates.FIND_WRAPPED:
                //                if (previous) {
                //                    findMsg = 'Reached top of document, continued from bottom';
                //                } else {
                //                    findMsg = 'Reached end of document, continued from top';
                //                }
                break;
        }

        if (notFound) {
            this.findField.classList.add('notFound');
        } else {
            this.findField.classList.remove('notFound');
        }

        this.findField.setAttribute('data-status', status);
        this.findMsg.textContent = findMsg;
    },

    close: function () {
        $('#book-search').hide();
        $('.sj-book').turn('center');
    }
};

var QoteBox = function qoteView(id, page, opts, showable) {

    this.id = id;
    this.page = page;
    this.pageid = page.attr('pageref');
    
    opts.id = id;
    this.opts = opts;
    this.state = false;
    this.toggleButton = null;
    this.content = null;
    this.showable = showable;
    this.dragging = false;

    var self = this;
    this.toggleButton = $('<div id="toggle' + id + '"  key="' + opts.pagekey + '"><span class="ui-icon ui-icon-pinthick" title="Quick Note"></span></div>');
    this.content = $('<textarea wrap="soft" maxlength=200 rows ="4" cols="22" draggable="false" class="textarea-qote">' + opts.content + '</textarea>');

    this.div = this.el = $('<div id="pin' + id + '" class="qote-container"></div>');
    this.qotesave = $('<li class="icon-qote-save" title="Save"/>');
    this.qotedelete = $('<li class="icon-qote-delete" title="Delete"/>');
    this.buttons = $('<ul class="qote-button-container"></ul>');

    this.textli = $('<li></li>').append(this.content);
    this.buttons.append(this.textli).append(this.qotedelete).append(this.qotesave);

    this.div.append(this.buttons);

    if (!opts.visible) this.div.css({ display: 'none' });

    this.toggleButton.css({ left: opts.x + 'px', top: opts.y + 'px', position: 'absolute' });
    
    this.toggleButton.append(this.div);
    this.toggleButton.hover(function() {
         self.content.addClass('textarea-qote-hover'); self.qotesave.addClass('icon-qote-save-hover');
        self.qotedelete.addClass('icon-qote-delete-hover');
         },
        function() {
            self.content.removeClass('textarea-qote-hover'); self.qotesave.removeClass('icon-qote-save-hover');
            self.qotedelete.removeClass('icon-qote-delete-hover');
        });
    page.append(this.toggleButton);

    this.destroy = function qoteViewDestroy() {
        self.el.remove();
        self.toggleButton.remove();
        var pageView = $book_contents[self.pageid];
        pageView.qotes.Remove(self.id);
    };

    this.initialize = function initializeQote() {
        self.qotesave.click(function (event) {
            self.opts.content = self.content.val();
            bookshelf.updateQote(self.page, self.opts, true);
        });

        self.qotedelete.click(function (event) {
            $('#dialog-confirm-content').empty();
            $('#dialog-confirm-content').append('<span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>');
            $('#dialog-confirm-content').append('You want to delete this quite note, Are you sure?');
            $('#dialog-confirm-all').dialog('option', 'title', "Delete Quite Note");

            $("#dialog-confirm-all").dialog("option", "buttons", {
                Yes: function () {
                    bookshelf.deleteQote(self, self.id);
                    $(this).dialog("close");
                },
                No: function () {
                    $(this).dialog("close");
                }
            }
            );

            $('#dialog-confirm-all').dialog("open");
        });

        self.toggleButton.draggable(
        {
            containment: "parent",
            start: function () {
                //console.log('draggable start');
                self.dragging = true;
            },
            drag: function () {
               // console.log('draggable dragging');
            },
            stop: function (event, ui) {
                var pos = self.page.offset();
                var x = event.pageX - pos.left,
                  y = event.pageY - pos.top;

                self.opts.x = x;
                self.opts.y = y;

                setTimeout(function() {
                    self.dragging = false;
                },200);

                bookshelf.updateQote(self.page, self.opts, true);
            }
        });

        self.toggleButton.unbind('click');
        self.toggleButton.bind('click', function (event) {
            if (!self.dragging && $(event.target).hasClass('ui-icon-pinthick')) {
                self.div.toggle();
            }
                
        });
    };
};

var NotePage = function noteView(id, page, divider, opts, detail) {
    this.id = id;
    this.page = page;

    this.divider = divider;
    this.opts = opts;
    this.detail = detail;

    this.div = this.el = $('<div class="note-container"/>');

    this.spanTitle = $("<span class='text-insert-effect text-bold-effect'>" + detail.Title + "</span>");
    var title = $("<div class='note-content-title'></div>");
    title.append(this.spanTitle);
    this.edit = $('<img class ="edit-note-button" src="images/editx24.png" alt="Edit"/>');

    this.spanAuthor = $("<span class='text-insert-effect'>" + detail.Author + "</span>");
    var author = $("<div class='note-content-author'></div>");
    author.append(this.spanAuthor);
    
    this.content = $('<div class="note-wrapper note-content">' + detail.Content + '</div>');

    title.append(this.edit);
    this.div.append(title).append(author).append(this.content);

    this.element = $("<div noteref='" + this.id + "' pageth='" + this.page + "' class='page-element'/>").append(this.div);

    var self = this;

    this.updatePage = function noteupdatePage(page) {
        self.page = page;
        self.element.attr('pageth', page);
    };

    this.destroy = function noteViewDestroy() {

    };

    this.initialize = function initializeNote() {
        self.edit.unbind('click');
        self.edit.bind('click',function () {
            $('#dialog-edit-note').attr('noteref', self.id);
            $('#dialog-edit-note').attr('pageth', self.page);
            $('#dialog-edit-note').dialog('open');
        });
    };

    this.updateUI = function (note) {
        self.detail.Title = note.Title;
        self.detail.Author = note.Author;
        self.detail.Content = note.Content;

        self.spanTitle.text(note.Title);
        self.spanAuthor.text(note.Author);
        self.content.text(note.Content);
    };
};