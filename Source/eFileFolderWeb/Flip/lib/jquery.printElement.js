(function ($) {
    var printAreaCount = 0;
    $.fn.printArea = function () {
        var ele = $(this);
        var idPrefix = "printArea_";
        removePrintArea(idPrefix + printAreaCount);
        printAreaCount++;
        var iframeId = idPrefix + printAreaCount;
        var iframeStyle = 'position:absolute;width:0px;height:0px;left:-500px;top:-500px;';
        iframe = document.createElement('IFRAME');
        $(iframe).attr({
            style: iframeStyle,
            id: iframeId
        });
        document.body.appendChild(iframe);
        var doc = iframe.contentWindow.document;
        $(document).find("link")
            .filter(function() {
                return $(this).attr("rel").toLowerCase() == "stylesheet";
            })
            .each(function() {
                doc.write('<link type="text/css" rel="stylesheet" href="' +
                    $(this).attr("href") + '" >');
            });
        doc.write('<div id="printDivContainer" class="' + $(ele).attr("class") + '">' + '</div>');
        var canvas = $(ele).find("canvas");

        if (canvas) {
            var img = document.createElement('img');
            img.src = canvas[0].toDataURL("image/png");
            $(img).attr('style', canvas.attr('style'));
            $(doc).find("#printDivContainer").append($(img));
        }

        doc.close();
 
        var frameWindow = iframe.contentWindow;
        frameWindow.close();
        frameWindow.focus();
        frameWindow.print();
    };
    var removePrintArea = function (id) {
        $("iframe#" + id).remove();
    };
})(jQuery);