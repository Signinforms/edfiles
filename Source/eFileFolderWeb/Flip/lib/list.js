//=============================================================================
// CLASS: System.Collections.List
//-----------------------------------------------------------------------------

List = function(){
	/// <summary>
	/// 
	/// </summary>
	this.KeyIsUnique;
	this.ValueIsUnique;
	//---------------------------------------------------------
	// PROPERTY: Items
	//---------------------------------------------------------
	this.Items;
	//---------------------------------------------------------
	// METHOD: _indexOf
	//---------------------------------------------------------
	this._indexOf = function(property, value){
		var index = -1;
		for (var i = 0; i < this.Items.length; i++){
			if (this.Items[i][property] == value){
				index = i;
				break;
			}
		}
		return;
	}
	//---------------------------------------------------------
	// METHOD: _getItems
	//---------------------------------------------------------
	// Returns: System.Collections.DictionaryEntry[]
	this._getItemsBy = function(property, value, single){
		var items = new Array();
		// Get all objects.
		if (typeof(value) == "undefined"){
			for (var i = 0; i < this.Items.length; i++){
				items.push(this.Items[i]);
			}
		}else{
			// Get objects by value.
			for (var i = 0; i < this.Items.length; i++){
				if (this.Items[i][property] == value){
					items.push(this.Items[i]);
					if (single) break;
				}
			}
		}
		return items;
	}
	//---------------------------------------------------------
	// METHOD: _getObjectsBy
	//---------------------------------------------------------
	// Returns: object[]
	this._getObjectsBy = function(property, value, single){
		var objects = new Array();
		// Get all objects.
		if (typeof(value) == "undefined"){
			for (var i = 0; i < this.Items.length; i++){
				objects.push(this.Items[i][property]);
			}
		}else{
			// Get objects by value.
			for (var i = 0; i < this.Items.length; i++){
				if (this.Items[i][property] == value){
					objects.push(this.Items[i][property]);
					if (single) break;
				}
			}
		}
		return objects;
	}
	//---------------------------------------------------------
	// METHOD: IndexOfKey
	//---------------------------------------------------------
	// Get index of key (only if not unique).
	this.IndexOfKey = function(key){
		return this._indexOf("Key", key);
	}
	//---------------------------------------------------------
	// METHOD: GetIndexByValue
	//---------------------------------------------------------
	// Get index of value (only if not unique).
	this.IndexOfValue = function(value){
		return this._indexOf("Value", value);
	}
	//---------------------------------------------------------
	// METHOD: GetKeys
	//---------------------------------------------------------
	// Get array of keys from the Hashtable.
	this.GetKeys = function(){
		return this._getObjectsBy("Key", key, this.KeyIsUnique);
	}
	//---------------------------------------------------------
	// METHOD: GetValues
	//---------------------------------------------------------
	// Get array of values from the Hashtable.
	this.GetValues = function(){
		return this._getObjectsBy("Value", value, this.ValueIsUnique);
	}
	//---------------------------------------------------------
	// METHOD: GetValue
	//---------------------------------------------------------
	// Get first value by key.
	this.GetValue = function(key){
		var value;
		var items = this._getItemsBy("Key", key, true);
		if (items.length > 0) value = items[0].Value;
		return value;
	}
	//---------------------------------------------------------
	// METHOD: GetKey
	//---------------------------------------------------------
	// Get first key by value;
	this.GetKey = function(value){
		var key;
		var items = this._getItemsBy("Value", value, true);
		if (items.length > 0) key = items[0].Key;
		return key;
	}
	//---------------------------------------------------------
	// METHOD: Add
	//---------------------------------------------------------
	// Adds an element with the specified key and value into the Hashtable.
	this.Add = function(key,value){
		var allow = true;
		if (this.KeyIsUnique) allow = allow && (this.IndexOfKey == -1);
		if (this.ValueIsUnique) allow = allow && (this.IndexOfValue == -1);
		if (allow){
			var item = new DictionaryEntry(key, value);
			this.Items.push(item);
		}
		return allow;
	}
	//---------------------------------------------------------
	// METHOD: Remove
	//---------------------------------------------------------
	// Removes the element with the specified key from the Hashtable.
	this.Remove = function(key){
		var index;
		var removed = false;
		while(index != -1){
			index = this._indexOf("Key", key);
			if (index > -1){
				this.Items.splice(index, 1);
				removed = true;
			}
		}
		return removed;
	}
	//---------------------------------------------------------
	// METHOD: Clear
	//---------------------------------------------------------
	// Removes all items.
	this.Clear = function(){
		this.Items = new Array();
	}
	//---------------------------------------------------------
	// METHOD: ContainsKey
	//---------------------------------------------------------
	// Determines whether the Hashtable contains a specific key.
	this.ContainsKey = function(key){
		return (this._indexOf("Key", key) > -1);
	}
	//---------------------------------------------------------
	// METHOD: ContainsValue
	//---------------------------------------------------------
	// Determines whether the Hashtable contains a specific value.
	this.ContainsValue = function(value){
		return (this._indexOf("Value", value) > -1);
	}
	//---------------------------------------------------------
	// METHOD: ToString
	//---------------------------------------------------------
	// Returns a String that represents the current Object.
	this.ToString = function(){
		return (this.GetType().Name+"[Items.length="+this.Items.length+"]");
	}
	//---------------------------------------------------------
	// INIT: Class
	//---------------------------------------------------------
	this.InitializeClass = function(){
		// Use array to store items (slower).
		this.Items = new Array();
		this.KeyIsUnique = false;
		this.ValueIsUnique = false;
	}
	this.InitializeClass();
}

DictionaryEntry = function(key, value){
	/// <summary>
	/// 
	/// </summary>
	this.Key;
	this.Value;
	//---------------------------------------------------------
	// METHOD: Equals
	//---------------------------------------------------------
	// Determines whether two instances are equal.
	this.Equals = function(item){
		var equal = true;
		equal = equal && (this.Key == item.Key);
		equal = equal && (this.Value == item.Value);
		return equal;
	}
	//---------------------------------------------------------
	// METHOD: ToString
	//---------------------------------------------------------
	// Returns a String that represents the current Object.
	this.ToString = function(){
		return "HashtableItem[Key='"+this.Key+"';Value='"+this.Value+"']";
	}
	//---------------------------------------------------------
	// INIT: Class
	//---------------------------------------------------------
	this.InitializeClass = function(){
		this.Key = key;
		this.Value = value;
	}
	this.InitializeClass();
}