<%@ Import Namespace="Shinetech.Framework" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="Shinetech.DAL" %>
<%@ Import Namespace="Shinetech.Engines" %>
<%@ Import Namespace="System.IO" %>
<%@ Application Language="C#" %>

<script RunAt="server">


    void Application_Start(object sender, EventArgs e)
    {

    }

    void Application_End(object sender, EventArgs e)
    {


    }

    void Application_Error(object sender, EventArgs e)
    {
        // Code that runs when an unhandled error occurs
        var ex = Server.GetLastError();
        NLogLogger _logger = new NLogLogger();
        if (ex != null)
            _logger.Info(ex.Message, ex);
        //Response.Redirect("~/ErrorPage.aspx");
        //try
        //{
        //    ErrorRecord.exe = Server.GetLastError();
        //}
        //catch (Exception ex)
        //{

        //}
    }

    void Session_Start(object sender, EventArgs e)
    {
        if (Session["LoggedInUser"] == null)
            Session["LoggedInUser"] = Guid.NewGuid();
        // Code that runs when a new session is started
        //Console.WriteLine(this.Request.Url);
        //string hash = FastRandom.getMd5Hash("2032242306");

        string path = Request.FilePath.ToLower();
        if (path.Contains("partnerhome.aspx"))
        {
            NameValueCollection coll = Request.QueryString;

            if (coll.HasKeys() && coll["pid"] != null)
            {
                string pid = coll.Get("pid");

                try
                {
                    DataTable table = UserManagement.GetPartnerByHashCode(pid);
                    if (table.Rows.Count > 0)  //valid invistor
                    {
                        Visitor vst = new Visitor();
                        vst.SessionID.Value = this.Session.SessionID;
                        vst.PartnerID.Value = Convert.ToInt32(table.Rows[0]["PartnerID"]);
                        vst.PartnerUrl.Value = Request.Url.ToString();
                        vst.IPAddress.Value = Request.ServerVariables["REMOTE_ADDR"];
                        vst.VisitorFlag.Value = "Y";
                        vst.Create();

                        Session["Session_Visitor"] = vst.PartnerID.Value; //表示该访问用户从partner的网站链接过来的
                    }
                }
                catch
                {
                    // Console.WriteLine(exp.Message);
                }

            }
        }
        else //从本地efilefodlers.com访问的
        {
            //Visitor vst = new Visitor();
            //vst.SessionID.Value = this.Session.SessionID;
            //vst.IPAddress.Value = Request.ServerVariables["REMOTE_ADDR"];

            //vst.Create();

            //Session["Session_Visitor"] = vst.PartnerID.Value;
        }

    }

    void Session_End(object sender, EventArgs e)
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.
        //Console.WriteLine(this.Request.Url); 

        if (Session["Session_Visitor"] != null)
        {
            string sid = this.Session.SessionID;
            Visitor visitor = new Visitor(sid);
            if (visitor.IsExist)
            {
                visitor.OutDate.Value = DateTime.Now;
                visitor.Update();
            }
        }

        UserManagement.UpdateUserAccess(this.Session.SessionID);
    }

    public override void Init()
    {
        base.Init();
        this.BeginRequest += new EventHandler(BeginRequest1);
    }

    void BeginRequest1(object sender, EventArgs e)
    {
        com.flajaxian.FileUploader.RegisterAspCookies();
    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {
        HttpContext context = HttpContext.Current;
        var absoluteURL = context.Request.Url.AbsolutePath;
        //Check Code while upload on live server for length
        if (absoluteURL.Split('/').Length == 2 && !absoluteURL.Contains("signin"))
        {
            string extension = (Path.GetExtension(absoluteURL.Split('/')[1]));
            string[] extensions = { ".pdf", ".jpeg", ".jpg", ".aspx", ".asmx", ".png", ".xlsx", ".css", ".js", ".axd", ".html", ".htm", ".asax", ".gif", ".svg" };
            if (string.IsNullOrEmpty(extension) || !extensions.Contains(extension.ToLower()))
            {
                var username = absoluteURL.Split('/')[1];
                var usernameDecoded = HttpUtility.UrlDecode(username);
                DataTable dtUsers = Shinetech.DAL.General_Class.GetUserIdFromUserName(usernameDecoded);
                if (dtUsers.Rows.Count > 0)
                {
                    string uid = dtUsers.Rows[0]["UID"].ToString();
                    var rewriteUrl = context.Request.Url.PathAndQuery.ToString().Replace(username, "InboxUpload.aspx");
                    rewriteUrl += (context.Request.Url.PathAndQuery.ToString().IndexOf("?") > 0 ? "&UserId=" + uid : "?UserId=" + uid);
                    context.RewritePath(rewriteUrl, true);
                }
                else
                    context.Response.Redirect("~/Default.aspx");
            }
        }
    }

    //protected void Application_BeginRequest(object sender, EventArgs e)
    //{
    //    HttpContext context = HttpContext.Current;
    //    var absoluteURL = context.Request.Url.AbsolutePath;
    //    //Check Code while upload on live server for length
    //    if (absoluteURL.Split('/').Length == 3)
    //    {
    //        string extension = (Path.GetExtension(absoluteURL.Split('/')[2]));
    //        string[] extensions = { ".pdf", ".jpeg", ".jpg", ".aspx", ".asmx", ".png", ".xlsx", ".css", ".js", ".axd", ".html", ".htm", ".asax", ".gif", ".svg" };
    //        if (string.IsNullOrEmpty(extension) || !extensions.Contains(extension.ToLower()))
    //        {

    //            var username = absoluteURL.Split('/')[2];
    //            var usernameDecoded = HttpUtility.UrlDecode(username);
    //            DataTable dtUsers = Shinetech.DAL.General_Class.GetUserIdFromUserName(usernameDecoded);
    //            if (dtUsers.Rows.Count > 0)
    //            {
    //                string uid = dtUsers.Rows[0]["UID"].ToString();
    //                var rewriteUrl = context.Request.Url.PathAndQuery.ToString().Replace(username, "InboxUpload.aspx");
    //                rewriteUrl += (context.Request.Url.PathAndQuery.ToString().IndexOf("?") > 0 ? "&UserId=" + uid : "?UserId=" + uid);
    //                context.RewritePath(rewriteUrl, true);
    //            }
    //            else
    //                context.Response.Redirect("~/default.aspx");
    //        }
    //    }
    //}


</script>
