<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="HowWork.aspx.cs" Inherits="HowWork" Title="" %>
<%@ Register Src="~/Controls/RegisterDirectly.ascx" TagName="ucRegisterDirectly" TagPrefix="ucRegisterDirectly" %>
<%@ Register TagPrefix="fjx" Namespace="com.flajaxian" Assembly="com.flajaxian.FileUploader" %>
<%@ Register TagPrefix="cust" Namespace="Shinetech.Engines.Adapters" Assembly="FormatEngine" %>
<%@ Import Namespace="System.Web.Configuration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <!-- by shoaib -->
    <link type="text/css" rel="stylesheet" href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/css/reset.css" />
    <link type="text/css" rel="stylesheet" href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/css/jquery-turnjs.ui.css" />
    <link type="text/css" rel="stylesheet" href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/css/jquery-ui-1.8.22.custom.css" />
    <%--<link type="text/css" rel="stylesheet" href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/css/main.css" />--%>

    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>Flip/js/helper.js"></script>
    <!--<script type="text/javascript" src="/eFileFolderWeb/Flip/extras/jquery.min.1.7.js"></script>-->
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/extras/jquery-ui-1.8.22.custom.min.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/extras/jquery.mousewheel.min.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/extras/modernizr.custom.70617.js"></script>

    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/lib/hashtable.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/lib/list.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/lib/item.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/lib/hash.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/lib/jquery.printElement.js"></script>

    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/lib/compatibility.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/lib/pdf.js"></script>
    <!--<script type="text/javascript" src="ttp://mozilla.github.io/pdf.js/build/pdf.js"></script>-->

    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/js/debugger.js"></script>

    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/lib/turn.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/lib/zoom.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/lib/jQXB.1.1.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/js/jsonservice.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/js/viewer.js"></script>

    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/js/divider.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/js/main.js"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/js/bookshelf.js"></script>
    <!-- by shoaib -->

    <style>
        div > span {
            font-size: 16px !important;
            line-height: 16px !important;
        }

        button > span {
            vertical-align: top;
        }

        .sj-book .shadow {
            box-shadow: 0 0 10px #999;
            border-bottom-left-radius: 15px;
            border-bottom-right-radius: 15px;
            border-top-right-radius: 0px;
            border-top-left-radius: 15px;
            border-color: #FCDB6A;
            background-color: #FC6;
            border-width: 0px;
            border-style: solid;
            background-image: url(../pics/spine.png);
            background-repeat: repeat-y;
            background-position: center;
            z-index: -99999999 !important;
        }

        .sj-book .book_header_shadow {
            height: 120px;
            width: 20px;
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 0px;
            border-top-right-radius: 15px;
            border-top-left-radius: 0px;
            box-shadow: 0 0 10px #999;
            background-color: #FC6;
            background-repeat: no-repeat;
            border-bottom: 0px #FCDB6A;
            border-left: 0px #FCDB6A;
            border-right: 2px #FCDB6A;
            border-top: 2px #FCDB6A;
            border-style: solid;
            float: right !important;
            position: absolute !important;
            top: 0px !important;
            right: 0px !important;
            margin-right: -13px !important;
            left: auto !important;
        }

        #book-search, #book-qotes {
            width: 300px;
        }

        #book-search-title, #qote-title {
            background-color: #CCC;
            border-color: #FCDB6A;
            border-bottom-color: #F6F6F6;
            border-radius: 5px 5px 0 0;
            border-style: solid;
            height: 35px;
            width: 100%;
            border-width: 1px;
            -webkit-transition: -webkit-box-shadow 0.5s;
            box-shadow: 0 0 3px #999;
            display: -webkit-box;
            -webkit-box-orient: horizontal;
            -webkit-box-align: center;
        }

        #book-search-box, #qotes-search-box {
            background-color: #EEE;
            border-color: #F6F6F6;
            border-left-color: #FCDB6A;
            border-right-color: #FCDB6A;
            border-radius: 0 0 0px 0px;
            border-style: solid;
            border-width: 1px;
            height: 35px;
            width: 100%;
            -webkit-transition: -webkit-box-shadow 0.5s;
            box-shadow: 0 0 3px #999;
            display: -webkit-box;
            -webkit-box-orient: horizontal;
            -webkit-box-align: center;
        }

        #result-search-title {
            background-color: #EEE;
            border-color: #F6F6F6;
            border-left-color: #FCDB6A;
            border-right-color: #FCDB6A;
            border-radius: 0 0 0px 0px;
            border-style: solid;
            border-width: 1px;
            height: 30px;
            width: 100%;
            -webkit-transition: -webkit-box-shadow 0.5s;
            box-shadow: 0 0 3px #999;
            display: -webkit-box;
            -webkit-box-orient: horizontal;
            -webkit-box-align: center;
        }

        #book-search-title .search-title-text span {
            color: #FFF;
            font-weight: bold;
        }

        #book-search-title .book-search-exit a {
            background-image: url("../pics/sprites.png");
            background-position: -24px 0;
            display: inline-block;
            height: 22px;
            width: 22px;
            margin-left: 180px;
        }

        #search-item-text {
            margin-left: 15px;
            border-radius: 5px;
            width: 180px;
        }

        #book-search-button {
            margin-left: 30px;
            width: 50px;
            height: 25px;
        }

        #result-search-title span {
            margin-left: 15px;
            color: #555;
            font-size: 1.4em;
            vertical-align: middle;
        }

        #book-search-result {
            overflow-y: scroll;
        }

        #book-search-result, #qote-search-result {
            background-color: #EEE;
            border-color: #FCDB6A;
            border-radius: 0 0 5px 5px;
            border-style: solid;
            border-width: 1px;
            border-top-color: #F6F6F6;
            height: 350px;
            width: 100%;
            -webkit-transition: -webkit-box-shadow 0.5s;
            box-shadow: 0 0 3px #999;
        }

            #book-search-result ul {
                list-style: decimal inside none;
            }

        .sj-book .p1 {
            background-position: 0 0;
            background-color: white;
            overflow: visible !important;
        }

        .sj-book .page {
            box-shadow: 0 0 20px rgba(0,0,0,0.2);
        }

        .sj-book .book_header {
            height: 120px;
            width: 60px;
            border-bottom-left-radius: 0px;
            border-bottom-right-radius: 0px;
            border-top-right-radius: 15px;
            border-top-left-radius: 0px;
            box-shadow: 0 0 0px #999;
            background-color: #FC6;
            background-repeat: no-repeat;
            border-bottom: 0px #FCDB6A;
            border-left: 0px #FCDB6A;
            border-right: 2px #FCDB6A;
            border-top: 2px #FCDB6A;
            border-style: solid;
            float: right !important;
            position: absolute !important;
            top: 0px !important;
            right: 0px !important;
            margin-right: -15px !important;
            left: auto !important;
            -webkit-transition: -webkit-box-shadow 0.5s;
        }

        .sj-book .triangle-bottom-shadow {
            width: 35px;
            height: 0px;
            border-top: 0px solid transparent;
            border-bottom: 2px solid #FCDB6A;
            border-left: 0px solid transparent;
            background-color: #FC6 !important;
            float: right !important;
            clear: both !important;
            position: absolute !important;
            top: 120px;
            right: 0px !important;
            margin-right: -2px !important;
            left: auto !important;
            box-shadow: 0 0 10px #999;
            -webkit-transform-origin: bottom right;
        }

        .sj-book .triangle-bottom {
            width: 35px;
            height: 35px;
            border-top: 0px solid transparent;
            border-bottom: 2px solid #FCDB6A;
            border-left: 0px solid transparent;
            background-color: #FC6 !important;
            float: right !important;
            clear: both !important;
            position: absolute !important;
            top: 85px;
            right: 0px !important;
            margin-right: -2px !important;
            left: auto !important;
            box-shadow: 0 0 0px #999;
            -webkit-transform-origin: bottom right;
        }

        .sj-book .front-side {
            background-color: #E6E6E6 !important;
            overflow: visible !important;
            position: absolute;
        }

        .front-side-content {
            position: absolute;
            width: 10%;
            height: 100%;
            overflow: visible;
            float: left;
            left: -2px;
        }

        #book-zoom #dividers-front {
            position: absolute;
            top: 0px;
            left: 0px;
            width: 95%;
            height: 100%;
            text-align: center;
            font-size: 1.6em;
            border: none;
            vertical-align: middle;
            overflow: visible;
            z-index: auto;
        }

        .front-side-container {
            width: 80%;
            height: 85%;
            margin: 2.5em auto;
        }

        .sj-book .table-content {
            margin: 20px auto;
            height: 90%;
        }

        .sj-book .table-content-title {
            text-align: center;
            font-size: 1.8em;
            margin-bottom: 1.1em;
            margin-top: 1.2em;
        }

            .sj-book .table-content-title span {
                font-size: 1em;
            }

        .sj-book .text-bold-effect {
            font-weight: bold;
        }

        .sj-book .text-insert-effect {
            color: #CCCCCC;
            background: none repeat scroll 0 0 transparent;
            text-shadow: -1px -1px #666, 1px 1px #FFF;
        }

        .sj-book .table-content-filter {
            text-align: center;
            font-size: 1.6em;
            margin: 16px auto;
            background: transparent;
        }

        #item-filter-input {
            border-radius: 5px;
            font-size: 11px;
            width: 10em;
            color:rgb(0,0,0); 
            line-height:normal;
        }

        .sj-book .toc-wrapper {
            font-size: 1.6em;
            margin: 1.5em auto;
            padding: 10px;
            overflow: hidden;
            border: none;
            background: transparent;
        }

        #toc {
            height: 8em;
            font-size: 1em;
            background: transparent;
            overflow: hidden;
        }

            #toc .toc-li {
                font-size: 1em;
                margin: 2px 2px 2px 1.8em;
                text-decoration: none;
                overflow: visible;
                padding: 3px;
                border-radius: 5px;
            }

            #toc .li-span {
                display: inline-block;
                position: relative;
                float: left;
                color: #000;
            }

            #toc .li-a {
                float: right;
                text-decoration: none;
                clear: none;
                font-size: 1em;
                display: inline-block;
                position: relative;
                color: #990000;
            }

        .sj-book .toc-logo {
            position: absolute;
            display: inline-block;
            text-align: center;
            bottom: 1em;
            margin: 0 auto;
            border: none;
            width: 85%;
            font-size: 1.8em;
            background: transparent;
        }

        .sj-book .text-insert-effect {
            color: #CCCCCC;
            background: none repeat scroll 0 0 transparent;
            text-shadow: -1px -1px #666, 1px 1px #FFF;
        }

        .sj-book .back-side {
            background-color: #E6E6E6;
            position: absolute;
        }

        .sj-book .copyright {
            background: none repeat scroll 0 0 transparent;
            border: medium none;
            font-size: 1.8em;
            overflow: hidden;
            position: relative !important;
            text-align: center;
            top: 40% !important;
            vertical-align: middle;
        }

        .sj-book .text-insert-effect {
            color: #CCCCCC;
            background: none repeat scroll 0 0 transparent;
            text-shadow: -1px -1px #666, 1px 1px #FFF;
        }

        #book-zoom #dividers-back {
            position: absolute;
            top: 0px;
            right: -1px;
            width: 5%;
            text-align: center;
            font-size: 1.6em;
            vertical-align: middle;
            padding-top: 0px;
            border: none;
            overflow: visible;
        }

        #floatingCirclesG {
            position: relative;
            width: 128px;
            height: 128px;
            -webkit-transform: scale(0.6);
            -moz-transform: scale(0.6);
            -o-transform: scale(0.6);
            -ms-transform: scale(0.6);
        }

        .f_circleG {
	position: absolute;
	background-color: #f6f4f4;
	height: 23px;
	width: 23px;
	-moz-border-radius: 12px;
	-webkit-border-radius: 12px;
	border-radius: 12px;
	-webkit-animation-name: f_fadeG;
	-webkit-animation-duration: 1.04s;
	-webkit-animation-iteration-count: infinite;
	-webkit-animation-direction: linear;
	-moz-animation-name: f_fadeG;
	-moz-animation-duration: 1.04s;
	-moz-animation-iteration-count: infinite;
	-moz-animation-direction: linear;
	-o-animation-name: f_fadeG;
	-o-animation-duration: 1.04s;
	-o-animation-iteration-count: infinite;
	-o-animation-direction: linear;
	-ms-animation-name: f_fadeG;
	-ms-animation-duration: 1.04s;
	-ms-animation-iteration-count: infinite;
	-ms-animation-direction: linear;
}
#frotateG_01 {
	left: 0;
	top: 52px;
	-webkit-animation-delay: 0.39s;
	-moz-animation-delay: 0.39s;
	-o-animation-delay: 0.39s;
	-ms-animation-delay: 0.39s
}
#frotateG_02 {
	left: 15px;
	top: 15px;
	-webkit-animation-delay: 0.52s;
	-moz-animation-delay: 0.52s;
	-o-animation-delay: 0.52s;
	-ms-animation-delay: 0.52s
}
#frotateG_03 {
	left: 52px;
	top: 0;
	-webkit-animation-delay: 0.65s;
	-moz-animation-delay: 0.65s;
	-o-animation-delay: 0.65s;
	-ms-animation-delay: 0.65s;
}
#frotateG_04 {
	right: 15px;
	top: 15px;
	-webkit-animation-delay: 0.78s;
	-moz-animation-delay: 0.78s;
	-o-animation-delay: 0.78s;
	-ms-animation-delay: 0.78s;
}
#frotateG_05 {
	right: 0;
	top: 52px;
	-webkit-animation-delay: 0.91s;
	-moz-animation-delay: 0.91s;
	-o-animation-delay: 0.91s;
	-ms-animation-delay: 0.91s;
}
#frotateG_06 {
	right: 15px;
	bottom: 15px;
	-webkit-animation-delay: 1.04s;
	-moz-animation-delay: 1.04s;
	-o-animation-delay: 1.04s;
	-ms-animation-delay: 1.04s;
}
#frotateG_07 {
	left: 52px;
	bottom: 0;
	-webkit-animation-delay: 1.17s;
	-moz-animation-delay: 1.17s
}
#frotateG_08 {
	left: 15px;
	bottom: 15px;
	-webkit-animation-delay: 1.3s;
	-moz-animation-delay: 1.3s;
	-o-animation-delay: 1.17s;
	-ms-animation-delay: 1.17s;
}
#frotateG_08 {
	left: 15px;
	bottom: 15px;
	-webkit-animation-delay: 1.3s;
	-moz-animation-delay: 1.3s;
	-o-animation-delay: 1.3s;
	-ms-animation-delay: 1.3s;
}

        #book-wrapper .container .flash {
            float: left;
            height: 128px;
            left: 156px;
            overflow: visible;
            position: absolute;
            top: 75px;
            width: 128px;
            z-index: 9999999;
        }

        #book-wrapper .container {
            height: 340px;
            left: 0;
            margin: 0px auto;
            position: absolute;
            top: 0;
            width: 440px;
            overflow: visible;
        }

    </style>
    <div class="title-container aboutus-title">
        <div class="inner-wrapper">
            <h1 >How do EdFiles Work?</h1>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="inner-wrapper">
        <div class="page-container">
            <div class="inner-wrapper">
                <div class="aboutus-contant67">
									<div class="left-content">
										<div class="virtual-containt">
											<div class="virtual-image" style="margin-bottom:-25px">
													<!--<img src="images/virtual-image.png" />-->

													<!-- by shoaib -->
													<div id="book-wrapper">
															<!--canvas of width and height-->
															<div class="container">
																	<div class="flash"></div>
																	<div id="shelf">
																			<!--style="width:500px;height:250px;margin:0;left:0:top:0;"-->
																			<div class="flipbook-back">
																					<div class="triangle-right"></div>
																			</div>
																			<ul id="dividers">
																					<!-- hostizonal dividers here 12 for 50px = 600px available,-->
																			</ul>
																			<div class="flipbook">
																					<div class="flipbook-tips">
																							<div class="flipbook-tip-title"><span id="book-title" jqxb-datasource="booktitle" jqxb-datamember="Name" jqxb-bindedattribute="text"></span></div>
																							<div class="flipbook-alert-container">
																									<ul class="flipbook-alerts" jqxb-templatecontainer="alertTemplate" jqxb-datasource="bookalerts">
																											<li jqxb-template="alertTemplate" jqxb-templateitemidprfx="alertrow"><span jqxb-itemdatamember="alert" jqxb-bindedattribute="text"></span></li>
																									</ul>
																							</div>
																					</div>
																					<div id="logo"></div>
																					<div class="logo"><b><a href="http://www.edfiles.com" target="_blank">www.edfiles.com</a></b></div>
																			</div>
																	</div>
																	<div id="book-zoom">
																			<div class="container">
																					<div class="sj-book">
																							<!--A4 - 21*29.7cm-->
																							<div></div>
																							<div></div>
																							<!-- search -->
																							<div id="book-search" style="display: none" ignore="1">
																									<div id="book-search-title">
																											<span class="ui-button-icon-primary ui-icon ui-icon-search" style="margin-left: 10px;"></span>
																											<div class="search-title-text"><span>Search</span></div>
																											<div class="book-search-exit"><a></a></div>
																									</div>
																									<div id="book-search-box">
																											<input id="search-item-text" name="search-item-text" type="text" value="" maxlength="30" class="input-filter-text" placeholder="Search ..." style="font-size:initial;color:initial;font-size:14px;"/>
																											<button id="book-search-button" name="book-search-button"></button>
																									</div>
																									<div id="result-search-title"><span>Search results</span><span id="results-count"></span></div>
																									<div id="book-search-result">
																											<ul id="book-search-items">
																											</ul>
																									</div>
																							</div>
																							<!-- search -->
																							<div id="book-qotes" style="display: none" ignore="1">
																									<div id="qote-title">
																											<span class="ui-button-icon-primary ui-icon ui-icon-search" style="margin-left: 10px;"></span>
																											<div class="qote-title-text"><span>Qotes</span></div>
																											<div class="book-search-exit"><a></a></div>
																									</div>
																									<div id="qotes-search-box">
																											<input id="qote-item-text" name="quote-item-text" type="text" value="" maxlength="30" class="input-filter-text, inputBox" placeholder="Search ..." style="font-size:initial;color:initial;font-size:14px;"/>
																											<button id="book-qote-button" name="book-search-button"></button>
																									</div>
																									<div id="qote-search-result">
																											<ul id="qote_search-items">
																											</ul>
																									</div>
																							</div>
																							<!-- search -->
																					</div>
																			</div>
																	</div>
															</div>
													</div>
													<!-- by shoaib -->
											</div>
											</div>
											<div class="work_content">
												<p><b>Secure, Simple yet Evolutionary.</b> Imagine buying and using File Folders right on your computer screen. Instead of going to office supplies stores and buying box of file folders, try EdFiles.</p>

												<p>
														<strong>1. Register for a FREE Trial</strong>, no credit card required. The free trial gives users
																														5 Free EdFiles to use to experience our patent pending, evolutionary
																														virtual file folder document viewer.
												</p>
												<p>
														<strong>2. After completing the registration</strong>, create your EdFiles by naming the Folder,
																														adding documents in them and viewing your folders in the viewer. (EdFiles currently
																														supports pdf file format to load in them for storage, the next version will support
																														many more file types.) There is a list of several FREE pdf creators below
												</p>
												<p><strong>3. Start using EdFiles for all your document storage.</strong></p>
												<p>
														<strong>The best part of using EdFiles is...</strong>your EdFiles are stored Virtually
																										and Securely. They take up no file cabinets, no wall shelving, no boxes, nothing.
																										They are available in same manner, format on demand anytime, anywhere and any place
																										with an internet connection.
												</p>
												<p>
														<strong>FREE PDF Converters</strong><br />
														<a href="http://www.primopdf.com/" target="_blank" class="foot_url2">http://www.primopdf.com/</a>
														<br />
														<a href="http://www.dopdf.com/download.php" target="_blank" class="foot_url2">http://www.dopdf.com/download.php</a><br />
														<a href="http://www.cutepdf.com/Products/CutePDF/writer.asp" target="_blank" class="foot_url2">http://www.cutepdf.com/Products/CutePDF/writer.asp</a>
												</p>
											</div>
									</div>
									<div class="right-content">
										<ucRegisterDirectly:ucRegisterDirectly ID="ucRegister" runat="server" />
									</div>
									</div>
									
                    
                </div>
            </div>
        </div>
    </div>
      <script language="javascript" type="text/javascript">
          $(document).ready(function () {

              //if ($("div").hasClass("inner-wrapper")) {
              $("#mainInnerWrapper").removeClass("inner-wrapper");
              // }
              // else {
              //      alert(2);
              // }


              $(document).keydown(function (e) {
                  if (e.keyCode == 13) {
                      var input = $('input[id$="LoginButton"]');
                      if (input) {
                          input.click();
                      }
                      return true;
                  } else if (e.keyCode == 8 || e.keyCode == 46) {
                      return true;
                  }
              });
          });

          function init() {
              $("#dialog:ui-dialog").dialog("destroy");

              //changed by shoaib
              /*var height = Math.max($(document).height() - 140,400);
              var width = Math.floor(height * 1.4);
  
              var height1 = Math.max($(document).height() - 200,400);
              var width1 = Math.floor(height1 * 1.6);*/

              var height = 250;
              var width = 350;

              var height1 = 250;
              var width1 = 400;

              //added by shoaib
              var screenWidth = screen.width;
              if (screenWidth < 500) {
                  //width = Math.floor((80 * screenWidth) / 100);
                  //height = Math.floor(width / 1.4);
                  //height1 = height;
                  //width1 = Math.floor(height1 * 1.6);

                  width = 256;
                  height = 182;
                  height1 = 182;
                  width1 = 291;

                  //alert(width1 + "-" + height1);
              }
              //added by shoaib

              //$('#shelf').css({ height: height1, width: width1, left: -width1 * 0.5, top: -height1 * 0.5 });
              $('#shelf').css({ height: height1, width: width1, left: 0, top: 0 });

              //$('#shelf').css({ height: height1, width: width1 });
              $('#shelf .flipbook').css({ height: height1, width: width1 });

              $('.flipbook .flipbook-tips').css({ height: height1 - 150 });

              // Flipbook	
              var flipbook = $('.sj-book');
              flipbook.turn({
                  elevation: 50,
                  acceleration: true,
                  gradients: !$.isTouch,
                  autoCenter: true,
                  duration: 1000,
                  display: 'double',
                  page: 1,
                  height: height - 25,
                  width: width - 25,
                  when: {
                      turning: function (e, page, view) {
                          //  console.log("turning:" + page);
                      },

                      turned: function (e, page, view) {
                          bookshelf.slider(page);
                          bookshelf.render(view);
                      },

                      start: function (e, pageObj) {
                          // console.log("start:" + pageObj);
                      },

                      end: function (e, pageObj) {
                      },

                      missing: function (e, args) {
                          for (var i = 0; i < args.pages.length; i++) {
                              var p = args.pages[i];
                              loadPage(p, args);
                          }
                      }
                  }
              });

              flipbook.addClass('animated');

              //for screen resolution < 400 {by shoaib}
              if (screenWidth < 500) {
                  var flipBookBackWidth = Math.floor((55 * width) / 100);
                  var flipBookBackHeight = Math.floor((14 * height) / 100);

                  $(".flipbook-back").css("width", flipBookBackWidth);
                  $(".flipbook-back").css("height", flipBookBackHeight);
                  $(".triangle-right").css("margin-left", flipBookBackWidth - 2);
                  $("#dividers").css("margin-top", ((flipBookBackHeight + 10) * -1));
                  $("#dividers").css("font-size", "1.4em");
                  //$(".divider").css("width", "75px !important;");

                  $("#book-wrapper .container").css("width", width1);
                  $("#book-wrapper .container").css("height", height1);
                  $("#book-wrapper").css("width", width1);
                  $("#book-wrapper").css("height", height1);

                  $(".book_header_shadow").css("height", Math.floor((height - 25) / 2));
                  $(".book_header").css("height", Math.floor((height - 25) / 2));
                  $(".triangle-bottom-shadow").css("margin-top", Math.floor((height - 25) / 2));
                  $(".triangle-bottom").css("top", Math.floor((height - 25) / 4));

                  $("#book-zoom .divider-front").css("height", 70);
                  $(".table-content-filter").css("margin", 6);
                  $("#item-filter-input").css("width", 80);

                  $(".table-content-title").css("width", 80);
                  $(".table-content-title").css("font-size", 13);

                  $(".toc-wrapper").css("margin-top", 5);
                  $(".toc-wrapper").css("margin-bottom", 5);
                  $(".toc-wrapper").css("padding", 0);
                  $(".toc-wrapper").css("font-size", 10);
                  $("#book-wrapper .container .flash").css({ top: 30, left: 85 });

              }

              //for screen resolution < 400 {by shoaib}

              if (screenWidth < 768) {
                  $(".virtual-containt").css("width", "100%");
                  $(".virtual-containt").css("margin-bottom", "40px");
              }

              $('#all').fadeIn(1000);

              // Zoom.js
              $('#book-zoom').zoom({
                  flipbook: $('.sj-book'),
                  max: function () {
                      return largeMagazineWidth();
                  },
                  when: {
                      doubleTap: function (event) {

                          if ($(this).zoom('value') == 1) {
                              var max = $('#slider-zoom').slider('option', 'max');
                              $('#slider-zoom').slider('option', 'value', max);

                              $('.sj-book').
                                  removeClass('animated').
                                  addClass('zoom-in');
                              $(this).zoom('zoomIn', event);
                          } else {
                              $(this).zoom('zoomOut');
                          }
                      },

                      resize: function (event, scale, page, pageElement) {
                          //console.log('resize:' + page);
                          $('.sj-book .toc-wrapper, .sj-book .table-content-filter').css('font-size', '' + scale * 1.6 + 'em');
                          $('.sj-book .table-content-title').css('font-size', '' + scale * 1.8 + 'em');
                          if (scale > 1) {
                              $('#book-search').hide();
                          }

                          var pageKey = pageElement.attr("pageref");
                          if (pageKey && $page_promises.ContainsKey(pageKey)) {
                              $page_promises.GetValue(pageKey).then(function (pageData) { // onData to then
                                  $book_contents[pageKey].setPage(pageData);
                                  $book_contents[pageKey].resize(scale);
                              }

                              );
                          }
                      },

                      zoomIn: function () {
                          //console.log("zoomIn:");
                          $('.sj-book').addClass('zoom-in');
                          if (!window.escTip && !$.isTouch) {
                              escTip = false;
                              var esc = $('<div>Press Here to exit</div>').click(function (e) {
                                  $(this).parent().remove();
                                  $('#book-zoom').zoom('zoomOut');
                              });
                              $('<div >', { 'class': 'esc' }).append(esc).
                                      appendTo($('body')).
                                      delay(1000);

                          };

                          $(document).keypress(function (e) {
                              if (e.keyCode === 27) {
                                  //console.log("esc press:");
                                  $('#book-zoom').zoom('zoomOut');
                                  $(document).unbind('keypress');
                              }
                          });

                          $(document).focus();

                          var $previous_page_button = $('<div >', { 'class': 'arrow_previous_page' }).appendTo($('body'));
                          var $next_page_button = $('<div >', { 'class': 'arrow_next_page' }).appendTo($('body'));

                          clickElement($previous_page_button, function (event) {
                              //console.log("$previous_page_button:");

                              $('.sj-book').turn('previous');
                          });

                          clickElement($next_page_button, function (event) {
                              //console.log("$next_page_button:");
                              $('.sj-book').turn('next');
                          });

                          $('#book-wrapper .bar').fadeOut(500);
                          $('#book-wrapper .tool').fadeOut(500);
                      },

                      zoomOut: function () {
                          // console.log("zoomOut:");
                          $('.esc').hide();
                          $('#slider-zoom').slider('option', 'value', 1);

                          setTimeout(function () {
                              $('.sj-book').addClass('animated').removeClass('zoom-in');
                              resizeBookWrapper();
                              resizeViewport();
                          }, 0);

                          setTimeout(function () {
                              for (var key in $book_contents) {
                                  $book_contents[key].updating(1.0);
                              }
                          }, 0);

                          $('.arrow_previous_page').remove();
                          $('.arrow_next_page').remove();

                          $('#book-wrapper .bar').fadeIn(500);
                          $('#book-wrapper .tool').fadeIn(500);



                      },
                      //???????,???
                      swipeLeft: function () {
                          //console.log("swipeLeft:");
                          var book = $('.sj-book');
                          var page = book.turn('page');

                          if (getViewNumber(book, page) < numberOfViews(book))
                              $('.sj-book').turn('next');

                      },

                      //???????,???
                      swipeRight: function () {
                          //console.log("swipeRight:");
                          var book = $('.sj-book');
                          var page = book.turn('page');

                          if (getViewNumber(book, page) > 1)
                              $('.sj-book').turn('previous');

                      }
                  }
              });

              // Arrows
              $(document).keydown(function (e) {
                  if ($('#book-zoom').css('display') == 'none') return;

                  var previous = 37, next = 39;
                  var book = $('.sj-book');
                  var page = book.turn('page');

                  switch (e.keyCode) {
                      case previous:
                          if (getViewNumber(book, page) > 1)
                              $('.sj-book').turn('previous');
                          break;
                      case next:
                          if (getViewNumber(book, page) < numberOfViews(book))
                              $('.sj-book').turn('next');
                          break;
                  }

              });

              $(window).resize(function () {
                  resizeBookWrapper();
                  resizeViewport();
              }).bind('orientationchange', function () {
                  resizeBookWrapper();
                  resizeViewport();
              });

              resizeBookWrapper();
              resizeViewport();

          };

          $('#slider-book').slider('option', 'value', 1);
          $('#slider-book').slider('option', 'max', 2);

          $('#dialog-add-note').dialog({
              autoOpen: false,
              width: 450,
              height: 410,
              resizable: false,
              modal: true,
              buttons: {
                  "New Note": function () {
                      bookshelf.updateNote($(this), null);
                  },
                  "Cancel": function () {
                      $(this).dialog("close");
                  }
              },
              close: function () {
                  //allFields.val( "" ).removeClass( "ui-state-error" );
              }
          });

          $('#dialog-edit-note').dialog({
              autoOpen: false,
              width: 450,
              height: 410,
              resizable: false,
              modal: true,
              buttons: {
                  "Save Note": function () {
                      var id = $(this).attr('noteref');
                      bookshelf.updateNote($(this), id);
                  },
                  "Delete": function () {
                      var id = $(this).attr('noteref');
                      var self = $(this);

                      $('#dialog-confirm-content').empty();
                      $('#dialog-confirm-content').append('<span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>');
                      $('#dialog-confirm-content').append('You want to delete this note, Are you sure?');
                      $('#dialog-confirm-all').dialog('option', 'title', "Delete Note");

                      $("#dialog-confirm-all").dialog("option", "buttons", {
                          Yes: function () {
                              self.dialog("close");
                              bookshelf.deleteNote(self, id);
                              $(this).dialog("close");
                          },
                          No: function () {
                              $(this).dialog("close");
                          }
                      });

                      $('#dialog-confirm-all').dialog("open");
                  },

                  "Cancel": function () {
                      $(this).dialog("close");
                  }
              },

              close: function () {
                  //allFields.val( "" ).removeClass( "ui-state-error" );
              },
              open: function (event, ui) {
                  var id = $(this).attr('noteref');
                  var pageNode = $book_contents[id];

                  $('#dialog-edit-note input[name="title"]').val(pageNode.detail.Title);
                  $('#dialog-edit-note input[name="author"]').val(pageNode.detail.Author);
                  $('#dialog-edit-note textarea[name="content"]').val(pageNode.detail.Content);
              }
          });

          $('#dialog-share-book').dialog({
              autoOpen: false,
              width: 450,
              height: 540,
              resizable: false,
              modal: true,
              buttons: {
                  "Share Now": function () {
                      bookshelf.mailTo($(this));
                  },
                  "Close": function () {
                      $(this).dialog("close");
                  }
              },
              close: function () {

              }
          });

          //verify password dialog
          $('#dialog-enter-pin').dialog({
              autoOpen: false,
              width: 300,
              height: 115,
              resizable: false,
              modal: true,
              buttons: {
                  "Ok": function () {
                      var pwd = $('#shelf-pin').val();
                      $(this).dialog("close");
                      bookshelf.verifySecurity($(this), pwd);
                  },
                  "Cancel": function () {
                      $(this).dialog("close");
                  }
              }
          });

          $('#dialog-print-page').dialog({
              autoOpen: false,
              width: 300,
              height: 140,
              resizable: false,
              modal: true
          });


          $('#printPreviewContainer').dialog({
              autoOpen: false,
              closeOnEscape: false,
              title: 'Print Preview Dialog',
              width: 800,
              height: 750,
              resizable: false,
              modal: true,
              buttons: {
                  "Print": function () {
                      $(this).printArea();
                      $(this).dialog("close");
                  },
                  Cancel: function () {
                      $(this).dialog("close");
                  }
              },
              open: function (event, ui) {
                  // $('#print-canvas').css({ margin: '0 ' + ($(this).width() - $('#print-canvas').width()) / 2 + 'px'});

              }
          });

          $("#dialog-confirm-all").dialog({
              autoOpen: false,
              resizable: false,
              height: 140,
              modal: true,
              close: function (event, ui) {
                  $(this).dialog("option", 'title', '');
                  $('#dialog-confirm-content').empty();
                  $(this).dialog("option", "buttons", {});
              }
          });

          $("#dialog-message-all").dialog({
              autoOpen: false,
              resizable: false,
              modal: true,
              Ok: function () {
                  $(this).dialog("close");
              },
              close: function (event, ui) {
                  $(this).dialog("option", 'title', '');
                  $('#dialog-confirm-content').empty();
              }

          });


          $("#dialog-print-page input").button();
          $("#book-search-button").button({
              icons: {
                  primary: "ui-icon-search"
              },
              text: false
          });

          $("#book-qote-button").button({
              icons: {
                  primary: "ui-icon-search"
              },
              text: false
          });



          $('#all').hide();

          yepnope({
              test: Modernizr.csstransforms,
              //yep: ['js/html5fault.js'],
              nope: ['js/html5fault.js'],
              complete: init
          });

          //added by shoaib 10June2015
          var firstTime = 0;
          var flag = 0; //0=next,1=previous
          function turnPageManually() {
              if (firstTime == 0) {
                  selectdivider($("#1"));
                  firstTime = 1;
              }

              if ($(".sj-book").turn("page") >= 8 || flag == 1) {
                  //$(".sj-book").turn("page", 1);
                  if ($(".sj-book").turn("page") == 1) {
                      flag = 0;
                  }
                  else {
                      $('.sj-book').turn("previous");
                      flag = 1;
                  }
              }
              else if ($(".sj-book").turn("page") == 1 || flag == 0) {
                  $('.sj-book').turn("next");
                  flag = 0;
              }
              else {
                  $('.sj-book').turn("next");
              }
          }

          setInterval(turnPageManually, 5000);
          //added by shoaib 10June2015
    </script>
</asp:Content>
