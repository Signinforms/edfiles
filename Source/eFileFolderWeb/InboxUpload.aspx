﻿<%@ Page Language="C#" MasterPageFile="~/BlankMasterPage.master" AutoEventWireup="true" CodeFile="InboxUpload.aspx.cs" Inherits="InboxUpload" %>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Contentinbox" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link type="text/css" rel="stylesheet" href='<%= Page.ResolveUrl("~/css/jquery-ui-1.8.22.custom.css") %>' />
    <link href='<%= Page.ResolveUrl("~/css/jquery-ui.css") %>' rel="stylesheet" type="text/css" />
    <link href='<%= Page.ResolveUrl("~/New/css/colorbox.css") %>' rel="stylesheet" />
    <link href='<%= Page.ResolveUrl("~/css/main.css?v=1") %>' rel="stylesheet" />

    <style type="text/css">
        .white_content {
            display: none;
            position: fixed;
            top: 20%;
            left: 20%;
            width: 60%;
            height: 50%;
            padding: 16px;
            background-color: white;
            z-index: 999999999;
            overflow: auto;
        }

        .ajax__fileupload_dropzone
        {
            font-size: 15px;
            line-height: 120px !important;
            height: 120px !important;
            font-family: "Signika",sans-serif;
        }

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }

        .boxWidth {
            width: 1100px;
            margin: auto;
        }

        .title {
            font-weight: bold;
            font-size: 18px;
            color: #030303;
        }
    </style>
    <script language="JavaScript" type="text/jscript">

        $(document).ready(function (e) {
            $(".ajax__fileupload_dropzone").text("Drag and Drop Pdf file(s) here");
        });

        function getOs() {
            var OsObject = "";
            if (navigator.userAgent.indexOf("MSIE") > 0) {
                return "MSIE";
            }
            if (isFirefox = navigator.userAgent.indexOf("Firefox") > 0) {
                return "Firefox";
            }
            if (isSafari = navigator.userAgent.indexOf("Safari") > 0) {
                return "Safari";
            }
            if (isCamino = navigator.userAgent.indexOf("Camino") > 0) {
                return "Camino";
            }
            if (isMozilla = navigator.userAgent.indexOf("Gecko/") > 0) {
                return "Gecko";
            }

        }
        var os = getOs();
        var myTimer;
        var fileIDDividerId = [];
        function onClientUploadStart(sender, e) {
            //document.getElementById('uploadCompleteInfo').innerHTML = "please wait while the " + e.get_filesInQueue() + " file is uploaded and converted to eff...";
            document.cookie = "Files=" + JSON.stringify(fileIDDividerId);
            showLoader();

            $('[id*=ajaxFileUploadInbox_Html5DropZone]').addClass('hideControl');
            //$('[id*ajaxFileUploadInbox_SelectFileContainer]').addClass('hideControl');
            $('[id*=FileItemDeleteButton]').addClass('hideControl').css('display', 'none');
            $('[id*=UploadOrCancelButton]').addClass('hideControl').css('display', 'none');

            $('.TabDropDown').prop('disabled', 'disabled');
            $('#uploadProgress').html('');
            $('#uploadProgress').html('').html($('#divUpload').html());

            document.getElementById('uploadProgress').style.display = 'block';
            myTimer = setInterval(function () {
                $('#uploadProgress').html('').html($('#divUpload').html());
            }, 1000);
        }

        function onClientUploadComplete(sender, e) {
            //onImageValidated("TRUE", e);
            return false;
        }

        function onClientUploadCompleteAll(sender, e) {
            $('.hideControl').removeClass('hideControl');
            $('.uploadProgress').find('select').removeAttr('disabled');

            //$('#divUpload').clone().appendTo('#inboxUploadFieldset');
            clearInterval(myTimer);
            document.getElementById('uploadProgress').style.display = 'none';
            hideLoader();
           <%-- setTimeout(function () {
                var vd = '<%= ConfigurationManager.AppSettings["VirtualDir"] %>';
                window.location.reload();
            }, 2000);--%>
            if (confirm("File(s) have been uploaded successfully. Would you like to upload additional documents?")) {
                window.location.reload();
            }
            else {
                <%--window.location.href = window.location.origin + '<%= ConfigurationManager.AppSettings["VirtualDir"] %>' + "default.aspx"; --%>
                window.location.href = window.location.origin + "/" + "default.aspx";
            }
        }

        function onClientButtonClick() {
            $find('pnlPopup2').hide();

            setTimeout(function () {
            }, 1000);
            return false;
        }

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');;
        }

        function hideLoader() {
            $('#overlay').hide();
        }
        function showerror(sender, e) {
        }

    </script>

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Inbox Upload File</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <asp:Button ID="Button1ShowPopup2" runat="server" Style="display: none" />
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BehaviorID="pnlPopup2"
        PopupControlID="pnlPopup2" DropShadow="true" BackgroundCssClass="modalBackground"
        Enabled="True" TargetControlID="Button1ShowPopup2" />
    <asp:Panel ID="pnlPopup2" Style="display: none"
        runat="server" class="popup-mainbox">
        <div>
            <div class="popup-head" id="Div1">
                Message
            </div>
            <div class="popup-scroller">
                <p>
                    <%--Your document has been uploaded successfully...It will be converting to the efilefolder format in our server over the next few minutes, depending on the size of the uploaded document. You can now select additional files to upload or navigate away from this web page. --%>
                </p>
            </div>
            <div class="popup-btn">
                <asp:Button ID="ShowPopup2Button2" runat="server" OnClientClick="return onClientButtonClick()"
                    Text="Close" class="btn green" />
            </div>
        </div>
    </asp:Panel>
  
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>

    <div class="inner-wrapper">
        <div class="page-container">
            <div class="dashboard-container">
                <div class="left-content">
                    <%-- <div>--%>
                    <div class="title" style="width:745px;">Securely submit requsted documents to the organization or individual using this page.</div>
                    <asp:Panel ID="panelMain" runat="server" Visible="true">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="uploadProgress" class="white_content" style="font-size:14px;"></div>
                                <fieldset id="inboxUploadFieldset" style="width: 650px; font: 20px bold">
                                    <%--Inbox--%>
                                    <div id="divUpload">
                                        <div class='clearfix'></div>
                                        <asp:Label ID="label1" runat="server"></asp:Label>
                                        <asp:Label runat="server" ID="Label2" Style="display: none;"></asp:Label>
                                        

                                        <ajaxToolkit:AjaxFileUpload ID="ajaxFileUploadInbox" runat="server" Padding-Bottom="4" CssClass="boxWidth" AzureContainerName=""
                                            Padding-Left="2" Padding-Right="10" Padding-Top="4" ThrobberID="myThrobber" MaximumNumberOfFiles="10" ContextKeys="2"
                                            OnUploadComplete="AjaxFileUploadInbox_OnUploadComplete"
                                            AllowedFileTypes="jpg,jpeg,pdf"
                                            OnClientUploadComplete="onClientUploadComplete"
                                            OnClientUploadCompleteAll="onClientUploadCompleteAll"
                                            OnUploadCompleteAll="AjaxFileUploadInbox_UploadCompleteAll"
                                            OnUploadStart="AjaxFileUploadInbox_UploadStart"
                                            OnClientUploadStart="onClientUploadStart"
                                            OnClientUploadError="showerror" />
                                        <div class='clearfix'></div>
                                        <div id="uploadCompleteInfo">
                                        </div>
                                    </div>
                                </fieldset>
                                <br />
                                <br />

                                <asp:HiddenField ID="FolderID" runat="server" />
                                <asp:HiddenField ID="hndIPAddress" runat="server" />
                                <br />
                                <br />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>

                    <%--</div>--%>
                </div>
            </div>
        </div>
    </div>
</asp:Content>



