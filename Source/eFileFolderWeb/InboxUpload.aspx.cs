﻿using System;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;

using System.Linq;
using DataQuicker2.Framework;
using Shinetech.DAL;
using Shinetech.Utility;
using BarcodeLib;
using Shinetech.Framework.Controls;
using AjaxControlToolkit;
using Shinetech.Engines;
using System.Web.Script.Serialization;
using System.Xml;
using System.Security.AccessControl;
using System.IO.Compression;
using System.Text;
using System.Collections.Generic;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Protocols;
using Shinetech.Framework.Controls;
using System.Xml;
using Shinetech.Utility;
using System.Web.Script.Services;
using System.Net;

//public partial class Office_Welcome : System.Web.UI.Page
public partial class InboxUpload : StoragePage
{
    public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    public string virtualDir = ConfigurationManager.AppSettings["VirtualDir"];
    string Uid = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        GetIPAddress();
        if (Convert.ToString(Request.QueryString["UserId"]) != null)
        {
            Uid = Convert.ToString(Request.QueryString["UserId"]);
            Shinetech.DAL.Account account = new Shinetech.DAL.Account(Uid);
            if (account.IsSubUser.ToString() == "1")
                if (!Common_Tatva.IsUserSwitched())
                    Sessions.RackSpaceUserID = account.OfficeUID.ToString();
                else
                    Sessions.SwitchedRackspaceId = account.OfficeUID.ToString();
            else
                if (!Common_Tatva.IsUserSwitched())
                    Sessions.RackSpaceUserID = Uid;
                else
                    Sessions.SwitchedRackspaceId = Uid;
        }
        if (!Page.IsPostBack)
        {
            var argument = Request.Form["__EVENTARGUMENT"];
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
            {

            }
            argument = "";
            ajaxFileUploadInbox.Enabled = true;
            if (!Page.IsPostBack)
            {
                if (!ajaxFileUploadInbox.IsInFileUploadPostBack)
                {

                }
            }
        }
        else
        {
            if (ajaxFileUploadInbox.IsInFileUploadPostBack)
            {
                // do for ajax file upload partial postback request
            }
            else
            {
                var argument = Request.Form["__EVENTARGUMENT"];
                if (argument != null && !string.IsNullOrEmpty(argument))
                {

                }
            }
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["Files_Workarea"] as DataTable;
        }
        set
        {
            this.Session["Files_Workarea"] = value;
        }
    }

    public string DateTimeInfo
    {
        get
        {
            DateTimeFormatInfo myDTFI = new CultureInfo("en-US", false).DateTimeFormat;
            DateTime dt = DateTime.Now;
            return dt.ToString("MM/dd/yyy", myDTFI);

        }
    }

    public string GetIPAddress()
    {
        string IP4Address = String.Empty;

        foreach (IPAddress IPA in Dns.GetHostAddresses(Request.ServerVariables["REMOTE_ADDR"].ToString()))
        {
            if (IPA.AddressFamily.ToString() == "InterNetwork")
            {
                IP4Address = IPA.ToString();
                break;
            }
        }

        if (IP4Address != String.Empty)
        {
            hndIPAddress.Value = IP4Address;
            return IP4Address;
        }

        foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
        {
            if (IPA.AddressFamily.ToString() == "InterNetwork")
            {
                IP4Address = IPA.ToString();
                break;
            }
        }
        hndIPAddress.Value = IP4Address;
        return IP4Address;
    }

    #region FileUpload
    protected void AjaxFileUploadInbox_OnUploadComplete(object sender, AjaxFileUploadEventArgs file)
    {
        try
        {
            string path = Server.MapPath("~/" + Common_Tatva.Inbox);
            string uid = Uid;

            string fullPath = Path.Combine(path, uid);

            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath);
            }

            if (ajaxFileUploadInbox.IsInFileUploadPostBack)
            {
                var fullname = Path.Combine(fullPath, file.FileName);
                string fileName = Path.GetFileName(fullname);
                decimal size = Math.Round((decimal)(file.FileSize / 1024) / 1024, 2);
                string ipAddress = hndIPAddress.Value;

                RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                string errorMsg = string.Empty;
                string newFileName = rackSpaceFileUpload.UploadObject(ref errorMsg, file.FileName, Enum_Tatva.Folders.inbox, file.GetStreamContents(), null, uid);
                if (string.IsNullOrEmpty(errorMsg))
                {
                    General_Class.DocumentsInsertForInbox(newFileName.Substring(newFileName.LastIndexOf('/') + 1), uid, size, Enum_Tatva.IsDelete.No.GetHashCode(), ipAddress);
                }
                else
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to upload file. Please try again after sometime." + "\");", true);
            }
        }
        catch (Exception)
        {
            throw;
        }
    }


    protected void AjaxFileUploadInbox_UploadCompleteAll(object sender, AjaxFileUploadCompleteAllEventArgs e)
    {
        var startedAt = (DateTime)Session["uploadTime"];
        var now = DateTime.Now;
        e.ServerArguments = new JavaScriptSerializer()
            .Serialize(new
            {
                duration = (now - startedAt).Seconds,
                time = DateTime.Now.ToShortTimeString()
            });
    }

    protected void AjaxFileUploadInbox_UploadStart(object sender, AjaxFileUploadStartEventArgs e)
    {
        var now = DateTime.Now;
        e.ServerArguments = now.ToShortTimeString();
        Session["uploadTime"] = now;
    }
    #endregion

}

