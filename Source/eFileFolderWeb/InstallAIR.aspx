﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="InstallAIR.aspx.cs" Inherits="InstallAIR" Title="" %>

<%@ Register Src="~/Controls/RegisterDirectly.ascx" TagName="ucRegisterDirectly" TagPrefix="ucRegisterDirectly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        <!--
        #AIRDownloadMessageTable {
            width: 217px;
            height: 180px;
            border: 1px solid #999;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            font-size: 14px;
        }

        #AIRDownloadMessageRuntime {
            font-size: 12px;
            color: #333;
        }
        -->
    </style>
    <script src="AC_RunActiveContent.js" type="text/javascript"></script>
    <script language="JavaScript" type="text/javascript">
<!--
    // -----------------------------------------------------------------------------
    // Globals
    // Major version of Flash required
    var requiredMajorVersion = 9;
    // Minor version of Flash required
    var requiredMinorVersion = 0;
    // Minor version of Flash required
    var requiredRevision = 115;		// This is Flash Player 9 Update 3
    // -----------------------------------------------------------------------------
    // -->

    </script>
    <div class="title-container aboutus-title">
        <div class="inner-wrapper">
            <h1>Download EdFiles Viewer</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="inner-wrapper">
        <div class="page-container">
            <div class="inner-wrapper">
                  <div class="form-containt listing-contant">
                <div class="aboutus-contant">
                    <div class="calling-contant">
                        <ucRegisterDirectly:ucRegisterDirectly ID="ucRegister" runat="server" />
                        <p>
                            Your online EdFiles account provides an offline EdFiles Viewer. You can directly drag a
                                                    .eff file into the viewer to view EdFiles your local computer.  You must have an online
                                                    account with <a href="www.edfiles.com">www.edfiles.com</a>  to view .eff files with EdFiles Viewer.  
                                                    Please click the below button to finish installation.
                        </p>
                    </div>
                    </div>
                 <div class="content-box listing-view">
                    <div>
                        <script language="JavaScript" type="text/javascript">
<!--
    // Version check based upon the values entered above in "Globals"
    var hasReqestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);

    // Check to see if the version meets the requirements for playback
    if (hasReqestedVersion) {
        // if we've detected an acceptable version
        // embed the Flash Content SWF when all tests are passed

        AC_FL_RunContent(
            'codebase', 'http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab',
            'width', '217',
            'height', '180',
            'id', 'badge',
            'align', 'middle',
            'src', 'badge',
            'quality', 'high',
            'bgcolor', '#FFFFF0',
            'name', 'badge',
            'allowscriptaccess', 'all',
            'pluginspage', 'http://www.macromedia.com/go/getflashplayer',
            'flashvars', 'appname=eFileFolder%20Viewer&appurl=http://www.edfiles.com/eFileFoldersInstaller.air&airversion=1.5.1&imageurl=logo_install.jpg&buttoncolor=5580FF&messagecolor=5580FF',
            'movie', 'badge'); //end AC code

    } else {  // Flash Player is too old or we can't detect the plugin
        document.write('<table id="AIRDownloadMessageTable"><tr><td>Download <a href="http://www.edfiles.com/eFileFoldersInstaller.air">EdFiles Viewer</a> now.<br /><br /><span id="AIRDownloadMessageRuntime">This application requires the <a href="');

        var platform = 'unknown';
        if (typeof (window.navigator.platform) != undefined) {
            platform = window.navigator.platform.toLowerCase();
            if (platform.indexOf('win') != -1)
                platform = 'win';
            else if (platform.indexOf('mac') != -1)
                platform = 'mac';
        }

        if (platform == 'win')
            document.write('http://airdownload.adobe.com/air/win/download/latest/AdobeAIRInstaller.exe');
        else if (platform == 'mac')
            document.write('http://airdownload.adobe.com/air/mac/download/latest/AdobeAIR.dmg');
        else
            document.write('http://www.adobe.com/go/getair/');


        document.write('">Adobe&#174;&nbsp;AIR&#8482; runtime</a>.</span></td></tr></table>');
    }
    // -->
                        </script>
                    </div>

                    <div style="margin-top:30px">
                        Call 800-579-9190, if you require additional assistance.
                    </div>
                    <div style="margin-top:30px">
                        FREE PDF Converters<br />
                        <a href="http://www.primopdf.com/" target="_blank" class="foot_url2">http://www.primopdf.com/</a>
                        <br />
                        <a href="http://www.dopdf.com/download.php" target="_blank" class="foot_url2">http://www.dopdf.com/download.php</a><br />
                        <a href="http://www.cutepdf.com/Products/CutePDF/writer.asp" target="_blank" class="foot_url2">http://www.cutepdf.com/Products/CutePDF/writer.asp</a>
                    </div>
                </div>
                      </div>
            </div>
        </div>
    </div>
  
</asp:Content>
