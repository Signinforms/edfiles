﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="InstallEFF.aspx.cs" Inherits="InstallEFF" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<script src="AC_RunActiveContent.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript">
<!--
// -----------------------------------------------------------------------------
// Globals
// Major version of Flash required
var requiredMajorVersion = 9;
// Minor version of Flash required
var requiredMinorVersion = 0;
// Minor version of Flash required
var requiredRevision = 115;		// This is Flash Player 9 Update 3
// -----------------------------------------------------------------------------
// -->

</script>
    <table>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="970" height="600" valign="top" background="images/bg_main.gif" bgcolor="#f6f5f2"
                                        style="background-repeat: repeat-x;">
                                        <div style="background: url(../images/inner_b.gif); background-position: right top;
                                            background-repeat: no-repeat; margin-top: 10px;">
                                            <div style="width: 725px; height: 60px;">
                                                <div class="Naslov_Crven">
                                                    Desktop eFF Manager</div>
                                                <div class="podnaslov" style="padding: 5px; background-color: #f6f5f2;">
                                                    If you prefer creating EdFiles on your computer and store them on your local drive and not on edfiles.com, we provide our Desktop eFF Manager.  By purchasing eFF 
                                                    Manager, you will receive 2 months of online storage and 
                                                    synchronization of all the folders you create in your 
                                                    account.  If you choose to continue with the optional 
                                                    online storage and synchronization, you will need to 
                                                    subscribe to one of our plans.  If you would like just use 
                                                    the eFF Manager Software to create and store EdFiles 
                                                    on your local computer, you will just need to buy the 
                                                    quantities of folders.</div>
                                                    
                                                    <div class="podnaslov" style="padding: 5px; background-color: #f6f5f2;">
                                                    To get started, you must create an account on 
                                                    edfiles.com and either purchase the eFF Manager and 
                                                    Folders or choose one of our Starter Kits.  Once you 
                                                    purchase and download the eFF Manager and Folders.  
                                                    EdFiles will allow to you create and customize 
                                                    folders.  For more information, please call 800-579-9190.  
                                                    Click <asp:HyperLink  Text="here" CssClass="user_url" NavigateUrl="~/Office/PurchasePage.aspx" runat="server"></asp:HyperLink > to purchase.</div>
                                                    <div class="podnaslov" style="padding: 5px; background-color: #f6f5f2;">
                                                    <asp:HyperLink ID="HyperLink2" CssClass="user_url" Text="Download Trial Version Now" NavigateUrl="~/setup.exe" runat="server"></asp:HyperLink ></div>
                                                

                                               </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="10" align="center">
                            <img src="images/lin.gif" width="1" height="453" vspace="10" /></td>
                        <td valign="top" background="images/bg_main.gif" bgcolor="#f6f5f2" style="background-repeat: repeat-x;">
                            <div class="Naslov_Plav" style="width: 200px;">
                                New User?</div>
                            <table width="200" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 20px 10px;">
                                <tr>
                                    <td align="center">
                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/WebUser/WebOfficeUser.aspx">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/register.gif" />
                                            <!--<input name="Submit2" type="image" value="GO" src="images/register.gif" align="absbottom"/> -->
                                        </asp:HyperLink></td>
                                </tr>
                            </table>
                            <div class="call" align="center">
                                Register directly by calling
                            </div>
                            <div align="center" class="phone">
                                800-579-9190</div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
