﻿<%@ Page Language="C#" MasterPageFile="~/BlankMasterPage.master" AutoEventWireup="true" CodeFile="LogFormLogs.aspx.cs" Inherits="LogFormLogs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        table {
    border-collapse: collapse;
}

table, td, th, select {
    border: 1px solid black;
}
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#" + '<%= modifiedDate.ClientID%>').change(function () {
                var selectedTime = $("#" + '<%= modifiedDate.ClientID%>').val();
                if (selectedTime != "Select Modified Date") {
                    $.ajax(
                       {
                           type: "POST",
                           url: 'LogFormLogs.aspx/GetLogData',
                           data: "{ logFormId: " + $("#" + '<%= hdnLogFormId.ClientID %>').val() + " , datetime: '" + selectedTime + "' }",
                           contentType: "application/json; charset=utf-8",
                           dataType: "json",
                           success: function (data) {
                               if (data.d.length > 0) {
                                   var docObj = JSON.parse(data.d);
                                   $("#tblLogFormLogs tr").remove();
                                   var tr = document.createElement("tr");
                                   tr.innerHTML = "<td ><label style='font-weight:bold;padding-left:5px;'>Changed Values</label></td>";
                                   $('table#tblLogFormLogs').prepend(tr);
                                   if (docObj !== undefined && docObj.length > 0) {
                                       for (var i = 0; i < docObj.length; i++) {

                                           var tr = document.createElement("tr");
                                           tr.innerHTML = "<td><label class='tdVal'></label></td>";
                                           $('table#tblLogFormLogs tr:last').after(tr);

                                           $('table#tblLogFormLogs tr:last').find('.tdVal').text(docObj[i].Value).css("padding-left", "5px");

                                       }
                                   }
                               }
                               else {
                                   alert("Failed to get data. Please try again after sometime.");
                               }
                           },
                           error: function (result) {
                               console.log('Failed' + result.responseText);
                           }
                       });
                }
            });
        });
        
    </script>
    <div class="title-container">
                <div class="inner-wrapper">
                    <h1 style="margin-bottom: 5px; margin-top: 5px;">Log Form Logs</h1>
                    <div class="clearfix"></div>
                </div>
            </div>
    <div class="inner-wrapper">
        <div class="page-container">
            <div class="dashboard-container">
                <div class="left-content">
                    <div>
                        <asp:Panel ID="panelMain" runat="server" Visible="true">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            
            <asp:HiddenField runat="server" ID="hdnLogFormId" />
            <lable style="font-size:14px;font-weight:bold;">Select Date to see logs:</lable>
                    <select id="modifiedDate" runat="server" style="height:25px;"></select>
                
                <table id="tblLogFormLogs" style="margin-top:15px;width:100%">
                </table>
        </ContentTemplate>
    </asp:UpdatePanel>
                            </asp:Panel>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</asp:Content>
