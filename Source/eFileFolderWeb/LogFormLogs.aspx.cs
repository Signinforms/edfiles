﻿using Newtonsoft.Json;
using Shinetech.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LogFormLogs : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string logFormID = Request.QueryString["id"];
            if (!string.IsNullOrEmpty(logFormID))
            {
                hdnLogFormId.Value = logFormID;
                DataTable dt = General_Class.GetLogFormLogs(int.Parse(logFormID));
                modifiedDate.Items.Add(new ListItem("Select Modified Date", "Select Modified Date"));                
                foreach (DataRow row in dt.Rows)
                {
                    modifiedDate.Items.Add(new ListItem(row.Field<DateTime>("ModifiedDate").ToString(), row.Field<DateTime>("ModifiedDate").ToString()));
                }
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
        }
    }

    [WebMethod]
    public static string GetLogData(string logFormId, string datetime)
    {
        try
        {
            DataTable dt = General_Class.GetLogFormLogs(int.Parse(logFormId));
            var row = dt.AsEnumerable().Where(r => r.Field<DateTime>("ModifiedDate").ToString() == datetime).FirstOrDefault();
            if (row != null)
            {
                return row.Field<string>("SavedValue");
            }
            return string.Empty;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            return string.Empty;
        }
    }
}