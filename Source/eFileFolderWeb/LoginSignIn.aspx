﻿<%@ Page Language="C#" AutoEventWireup="true"
    CodeFile="LoginSignIn.aspx.cs" Inherits="LoginSignIn" Title="EdFiles - SignIn Login" %>

<!DOCTYPE HTML>
<html lang="en">
<head id="Head1" runat="server">
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/keyboard.css"/>
    <meta name="viewport" content="width=768px, maximum-scale=1.0">
    <%--<link rel="stylesheet" href="css/jquery.mobile-1.0a3.min.css" />--%>
    <%--<script type="text/javascript" src="Scripts/jquery-1.4.4.min.js"></script>--%>
    <%--  <script type="text/javascript" src="Scripts/jquery.mobile-1.0a3.js"></script>--%>
    <script type="text/javascript" src="Scripts/keyboard.js" charset="UTF-8"></script>
    
    <title>Sign In Login</title>
    </head>
    <style type="text/css">
        body
        {
            margin: 0;
            padding: 0;
            font-family: Arial;
        }
        
        #container
        {
            position: relative;
            width: 600px;
            margin: 0 auto;
        }
    </style>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scriptManager" runat="server" />
    <div id="container">
    <table width="100%">
        <tr align="center">
            <td valign="top" align="Center" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                style="background-repeat: repeat-x;">
                <div class="Naslov_Plav" cstyle="width:200px;">
                    Sign In Login
                </div>
                <div class="podnaslov">
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                        <contenttemplate>
                            <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                        </contenttemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:Login ID="login1" RememberMeSet="false" runat="server" OnAuthenticate="Login" runat="server">
                    <LayoutTemplate>
                        <table border="0" align="Center" cellpadding="2" cellspacing="0" style="margin: 20px 0 20px 10px;">
                            <tr>
                                <td align="left" class="podnaslov">
                                    Username&nbsp;</td>
                                <td>
                                    &nbsp;</td>
                                <td>
                                    <asp:TextBox ID="UserName" runat="server" Width="150px" MaxLength="18" class="keyboardInput"/>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="podnaslov">
                                    Password&nbsp;</td>
                                <td align="right" class="podnaslov">
                                    &nbsp;</td>
                                <td>
                                    <asp:TextBox ID="Password" TextMode="Password" Width="150px" runat="server" MaxLength="28" class="keyboardInput"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" align="left">
                                    <asp:CheckBox ID="RememberMe" CssClass="podnaslov" runat="server" Text="Remember me next time." /></td>
                            </tr>
                            <tr>
                                <tr>
                                    <td align="right" colspan="4" class="podnaslov">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <asp:ImageButton ID="LoginButton" CommandName="Login" runat="server"
                                            ImageUrl="~/images/submit_btn.gif" />&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                        </table>
                    </LayoutTemplate>
                </asp:Login>
                <div class="call" align="center">
                    Register directly by calling
                </div>
                <div align="center" class="phone">
                    800-579-9190</div>
            </td>
        </tr>
        
    </table>
    </div>
    </form>
</body>
</html>
