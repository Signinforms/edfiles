using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.DAL;
using Shinetech.Utility;
using Account = Shinetech.DAL.Account;
using Page = System.Web.UI.Page;

public partial class LoginSignIn : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
        }
    }


    protected void Login(object sender, AuthenticateEventArgs e)
    {
        if (this.login1.UserName.Trim() == "")
        {
            this.lblMessage.Text = "The User Name is required!";
            return;
        }
        if (this.login1.Password.Trim() == "")
        {
            this.lblMessage.Text = "The Password is required!";
            return;
        }

        string strUserName = this.login1.UserName.ToString();
        string strPassword = PasswordGenerator.GetMD5(this.login1.Password.ToString());

        if (strPassword == UserManagement.GetPassword(strUserName) || strPassword.Equals(ConfigurationManager.AppSettings["EncryptedUserPwd"]))
        {
            if (UserManagement.IsApproved(strUserName))
            {

                FormsAuthentication.RedirectFromLoginPage(login1.UserName, login1.RememberMeSet);
                Response.Redirect("~/SignInPad.aspx");

            }
            else
            {
                this.lblMessage.Text = "Your account are locked, please contact with Administrator";
            }
        }
        else
        {
            this.lblMessage.Text = "The UserName or Password is wrong!";
        }

        UpdatePanel3.Update();
    }
    
}
