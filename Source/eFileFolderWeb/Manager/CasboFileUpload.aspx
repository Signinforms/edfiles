﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CasboFileUpload.aspx.cs" Inherits="Manager_CasboFileUpload" MasterPageFile="~/MasterPageOld.master"%>

<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />--%>

    <style>
        .Naslov_Protokalev {
            letter-spacing:0px;
        }
    </style>

    <script type="text/javascript">
        function OnBtnUploadclick() {
             var source = document.getElementById("<%=FileUpload1.ClientID%>").value;
             if (source.length > 0) {
                 if (source.substring(source.lastIndexOf(".") + 1, source.length).toLowerCase() != "pdf") {
                     alert("Only pdf extension is allowed!");
                     return false;
                 }
            }
            else {
                alert("Please select at least one file to upload!");
                return false;
            }
        }

        function pageLoad() {
            $('.editDocument').click(function (e) {
                e.preventDefault();
                removeAllEditable();
                showEdit($(this));
            });

            $('.cancelDocument').click(function (e) {
                e.preventDefault();
                removeAllEditable();
            });
        }

        function removeAllEditable() {
            $('.txtDocumentName,.saveDocument,.cancelDocument').hide();
            $('.lblDocumentName,.editDocument, .deleteDocument').show();
        }

        function showEdit(ele) {
            $('.saveDocument,.cancelDocument').hide();
            $('.deleteDocument,.editDocument').show();
            var $this = $(ele);
            $this.css('display', 'none');
            $this.parent().find('[name=lnkUpdate]').show();
            $this.parent().find('[name=lnkcancel]').show();
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                $(this).find('input[type=text]').css('border', '1px solid #c4c4c4');//1px solid #c4c4c4
                $(this).find('input[type = text]').show();
                if ($(this).find('input[type = text]').val() == undefined || $(this).find('input[type = text]').val() == "")
                { }
                else
                {
                    oldName = $(this).find('input[type = text]').val();
                }
                $(this).find('[name=DocumentName]').hide();
                $(this).closest('td').find('.deleteDocument').hide();
            });
            return false;
        }

        function OnSaveRename(txtRenameObj, fileName, fileId) {
            
            var newname = $('#' + txtRenameObj)[0].value;
            var oldFileName = fileName;
            if (newname == undefined || newname == "") {
                $('.txtDocumentName').css('border', '1px solid red');
                return false;
            }
            else if (!/^[a-z\d][a-z\d\()_\-.\s]+$/i.test(newname)) {
                alert("File name can not contain special characters.");
                $('.txtDocumentName').css('border', '1px solid red');
                return false;
            }
            else if (newname.indexOf('.') > 0 && newname.substring(newname.indexOf('.') + 1).toLowerCase() != 'pdf') {
                alert("Only File with type 'pdf' is allowed.");
                $('.txtDocumentName').css('border', '1px solid red');
                return false;
            }
            else {
                $('.txtDocumentName').css('border', '1px solid c4c4c4');
            }
            //showLoader();
            $.ajax(
    {
        type: "POST",
        url: '../Manager/CasboFileUpload.aspx/RenameCasboDocument',
        data: "{ newFileName:'" + newname + "',oldFileName:'" + oldFileName + "',fileId:'" + fileId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            if (result.d.length > 0)
                alert("Failed to rename document. Please try again after sometime.");
          //  hideLoader();
            __doPostBack('', 'RefreshGrid@');
        },
        error: function (result) {
            //hideLoader();
        }
    });
        }
    </script>

     <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 825px; height: 75px;">
                <div class="Naslov_Protokalev">
                    Upload Pdf for Casbo Users
                </div>
            </div>
            <table width="100%" style="margin-top: 10px;margin-left:30px;">
                <tr>
                    <td>
                        <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="true" />
                        <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" OnClientClick="return OnBtnUploadclick();" Text="Upload" Style="margin-bottom:15px;" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="GridView1" BorderWidth="0" runat="server" Width="100%" AutoGenerateColumns="false"
                            AllowSorting="True" OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound"
                            DataKeyNames="ID" >
                            <PagerSettings Visible="False" />
                            <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                            <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                            <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <Columns>
                                <%--asp:BoundField DataField="FileName" HeaderText="FileName" SortExpression="FileName">
                                    <HeaderStyle CssClass="form_title" />
                                </asp:BoundField>--%>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="400px" ItemStyle-Wrap="false" SortExpression="FileName">
                                    <HeaderTemplate>
                                        <%--<asp:Label ID="lblDocumentName" runat="server" Text="Document Name" />--%>
                                        <asp:LinkButton ID="lblDocumentName1" runat="server" Text="Form Name" CommandName="Sort" CommandArgument="FileName" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("FileName")%>' name="DocumentName" CssClass="lblDocumentName" />
                                        <asp:TextBox runat="server" MaxLength="50" Text='<%#Eval("FileName")%>'
                                            ID="TextDocumentName" Style="display: none; height: 30px;" name="txtDocumentName" CssClass="txtDocumentName" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" SortExpression="CreatedDate" ItemStyle-Width="350px"
                                    DataFormatString="{0:MM-dd-yyyy hh:mm}" />
                                <asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit">
                                    <HeaderTemplate>
                                        <asp:Label ID="lblAction" runat="server" Text="Action" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkBtnEdit" runat="server" Text="Edit" CommandArgument='<%# Eval("FileName")%>'
                                            CssClass="ic-icon ic-edit editDocument" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                        <asp:LinkButton Text="Update" ID="lnkBtnUpdate" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                            ToolTip="Update" CssClass="ic-icon ic-save saveDocument" Style="display: none" CommandArgument='<%# Eval("FileName")%>' name="lnkUpdate"></asp:LinkButton>
                                        <asp:LinkButton Text="Cancel" ID="lnkBtnCancel" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                            OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancelDocument" Style="display: none" name="lnkcancel"></asp:LinkButton>

                                        <asp:LinkButton ID="lnkBtnDelete" runat="server" CausesValidation="false" Text="Delete" CssClass="deleteDocument"
                                            OnCommand="lnkBtnDelete_Command" CommandArgument='<%#string.Format("{0}",Eval("ID")) %>' style="margin-left:10px;"/>
                                        <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                            CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="lnkBtnDelete"
                                            PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <ajaxToolkit:ConfirmButtonExtender ID="ButtonDeleteUser" DisplayModalPopupID="lnkDelete_ModalPopupExtender" runat="server" ConfirmText="Are you sure you want to delete this file ?"
                                            TargetControlID="lnkBtnDelete">
                                        </ajaxToolkit:ConfirmButtonExtender>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                    <tr>
                                        <td align="center" class="podnaslov">
                                            <asp:Label ID="Label411" runat="server" />There is no pdf.
                                        </td>
                                    </tr>
                                </table>
                            </EmptyDataTemplate>
                        </asp:GridView>

                        <asp:Panel class="popupConfirmation" ID="DivDeleteConfirmation" Style="display: none"
                            runat="server">
                            <div class="popup_Container">
                                <div class="popup_Titlebar" id="PopupHeader">
                                    <div class="TitlebarLeft">
                                        Delete File
                                    </div>
                                    <div class="TitlebarRight" onclick="$get('ButtonDeleteCancel').click();">
                                    </div>
                                </div>
                                <div class="popup_Body">
                                    <p>
                                        Are you sure you want to delete this file?
                                    </p>
                                    <p>
                                        Please enter password to delete file:
                                                                    <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                                    </p>
                                </div>
                                <div class="popup_Buttons">
                                    <input id="ButtonDeleleOkay" type="button" value="Yes" style="margin-left: 80px; width: 65px" />
                                    <input id="ButtonDeleteCancel" type="button" value="No" style="margin-left: 20px; width: 65px" />
                                </div>
                            </div>
                        </asp:Panel>

                    </td>
                </tr>
                <tr>
                    <td bgcolor="#F8F8F5" class="prevnext">
                        <cc1:WebPager ID="WebPager1" ControlToPaginate="GridView1" PagerStyle="OnlyNextPrev"
                            OnPageSizeChanged="WebPager1_PageSizeChanged" CssClass="prevnext" PageSize="15"
                            OnPageIndexChanged="WebPager1_PageIndexChanged" runat="server" />
                        <asp:HiddenField ID="SortDirection1" runat="server" Value="" />
                    </td>
                </tr>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />
        </Triggers>
    </asp:UpdatePanel>

      
    </asp:Content>
