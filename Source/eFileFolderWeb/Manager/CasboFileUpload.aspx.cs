﻿using iTextSharp.text.pdf;
using Shinetech.DAL;
using Shinetech.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class Manager_CasboFileUpload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
        else
        {
            var argument = Request.Form["__EVENTARGUMENT"];
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
            {
                GetData();
                BindGrid();
            }
        }
    }

    public static string dirPath = HttpContext.Current.Server.MapPath("~/" + "Casbo/");

    public DataTable CasboAdminDataSource
    {
        get
        {
            return this.Session["CasboAdminPdfs"] as DataTable;
        }
        set
        {
            this.Session["CasboAdminPdfs"] = value;
        }
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    private void GetData()
    {
        this.CasboAdminDataSource = General_Class.GetCasboPdfsForAdmin();
    }

    private void BindGrid()
    {
        WebPager1.DataSource = this.CasboAdminDataSource;

        if (this.CasboAdminDataSource != null)
        {
            WebPager1.DataBind();
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.CasboAdminDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.CasboAdminDataSource = Source.ToTable();
        BindGrid();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
            string fileName = drv.Row["FileName"].ToString();
            string fileId = drv.Row["ID"].ToString();

            TextBox txtDocumentName = (e.Row.FindControl("TextDocumentName") as TextBox);
            LinkButton lnkBtnUpdate = (e.Row.FindControl("lnkBtnUpdate") as LinkButton);
            if (lnkBtnUpdate != null)
            {
                lnkBtnUpdate.Attributes.Add("onclick", "OnSaveRename('" + txtDocumentName.ClientID + "','" + fileName + "','" + fileId + "');");
            }
        }
    }

    protected void WebPager1_PageSizeChanged(object s, Shinetech.Framework.Controls.PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.CasboAdminDataSource;
        WebPager1.DataBind();
    }
    protected void WebPager1_PageIndexChanged(object sender, Shinetech.Framework.Controls.PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.CasboAdminDataSource;
        WebPager1.DataBind();
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        string failedFileNames = string.Empty, fileName = string.Empty;
        if (FileUpload1.HasFiles)
        {

            try
            {
                if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }
                foreach (HttpPostedFile postedFile in FileUpload1.PostedFiles)
                {
                    try
                    {
                        if (postedFile.ContentType == "application/pdf")
                        {
                            fileName = string.Empty;
                            fileName = Path.GetFileName(postedFile.FileName);
                            fileName = fileName.Replace("'", "");
                            fileName = fileName.Replace("&", "");
                            string newName = string.Empty;
                            newName = GetNewFileName(dirPath, fileName);
                            string finalName = (string.IsNullOrEmpty(newName) ? fileName : newName);
                            postedFile.SaveAs(dirPath + finalName);
                            string error = string.Empty;
                            CreateThumbnailImage(finalName, fileName, ref error);
                            if (string.IsNullOrEmpty(error))
                                General_Class.DocumentsInsertForCasbo(finalName, Sessions.SwitchedSessionId, DateTime.Now);
                            //General_Class.DocumentsInsertForCasbo(finalName, Sessions.UserId, DateTime.Now);
                            else
                            {
                                failedFileNames += error;
                                error = string.Empty;
                            }
                        }
                        else
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Only pdf type is allowed!');", true);
                    }
                    catch (Exception ex)
                    {
                        Common_Tatva.WriteErrorLog(string.Empty, string.Empty, postedFile.FileName, ex);
                        if (failedFileNames.IndexOf(fileName) < 0)
                            failedFileNames += fileName + ",";
                    }
                }
                if (string.IsNullOrEmpty(failedFileNames))
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Pdf(s) have been uploaded!');", true);
                else
                {
                    string errorMsg = "Failed to upload pdfs: " + failedFileNames.Substring(0, failedFileNames.LastIndexOf(','));
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('" + errorMsg + "');", true);
                }
                GetData();
                BindGrid();
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Failed to upload Pdf. Please try again later.');", true);
            }
        }
    }

    private static void CreateThumbnailImage(string finalName, string fileName, ref string failedFileNames)
    {
        try
        {
            var xDpi = 100; //set the x DPI
            var yDpi = 100; //set the y DPI
            var pageNumber = 1; // the pages in a PDF document
            //using (var rasterizer = new GhostscriptRasterizer()) //create an instance for GhostscriptRasterizer
            //{
            //    rasterizer.Open(dirPath + finalName); //opens the PDF file for rasterizing

            //    //set the output image(png's) complete path
            //    var outputPNGPath = Path.Combine(dirPath, string.Format("{0}.png", Path.GetFileNameWithoutExtension(finalName)));

            //    //converts the PDF pages to png's 
            //    var pdf2PNG = rasterizer.GetPage(xDpi, yDpi, pageNumber);

            //    //save the png's
            //    // pdf2PNG.Save(outputPNGPath, ImageFormat.Png);
            //    if (pdf2PNG != null)
            //    {
            //        int newWidth = 150;
            //        int newHeight = 150;

            //        // Convert other formats (including CMYK) to RGB.
            //        Bitmap newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format24bppRgb);

            //        try
            //        {
            //            if (newImage != null)
            //            {
            //                // Draws the image in the specified size with quality mode set to HighQuality
            //                using (Graphics graphics = Graphics.FromImage(newImage))
            //                {
            //                    graphics.CompositingQuality = CompositingQuality.HighQuality;
            //                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            //                    graphics.SmoothingMode = SmoothingMode.HighQuality;
            //                    graphics.DrawImage(pdf2PNG, 0, 0, newWidth, newHeight);
            //                }
            //                newImage.Save(outputPNGPath);
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            Common_Tatva.WriteErrorLog(fileName, null, null, ex);
            //        }
            //    }
            //    else
            //    {
            //        if (failedFileNames.IndexOf(fileName) < 0)
            //            failedFileNames += fileName + ",";
            //        File.Delete(dirPath + finalName);
            //    }

            //}
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(fileName, null, null, ex);
            if (failedFileNames.IndexOf(fileName) < 0)
                failedFileNames += fileName + ",";

        }
    }

    private static string GetNewFileName(string dirPath, string fileName)
    {
        string newName = string.Empty;
        try
        {
            string[] files = Directory.GetFiles(dirPath);
            var fileExists = files.ToList().Where(item => item.ToLower().Contains(fileName.ToLower()));

            if (fileExists.Count() > 0)
            {
                string name = Path.GetFileNameWithoutExtension(fileName);
                string extension = Path.GetExtension(fileName);
                int index = 1;
                for (int i = 0; i < files.Length; i++)
                {
                    newName = name + "(" + index + ")" + extension;
                    if (files.Where(item => item.Substring(item.LastIndexOf("\\") + 1).ToLower().Contains(newName.ToLower())).Count() > 0)
                    {
                        index++;
                        continue;
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
        return newName;
    }

    protected void lnkBtnDelete_Command(object sender, CommandEventArgs e)
    {
        string password = textPassword.Text.Trim();
        string strPassword = PasswordGenerator.GetMD5(password);
        string errorMsg = string.Empty;

        if (strPassword != PasswordGenerator.GetMD5(ConfigurationManager.AppSettings["ANOTHER_PASSWORD"]))
        {
            //show the password is wrong        
            ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssYes", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);
        }
        else
        {
            try
            {
                LinkButton lnkbtnDelete = sender as LinkButton;
                GridViewRow gvrow = lnkbtnDelete.NamingContainer as GridViewRow;
                int fileId = Convert.ToInt32(GridView1.DataKeys[gvrow.RowIndex].Value.ToString());
                General_Class.DocumentsDeleteForCasbo(fileId);
                GetData();
                BindGrid();
            }
            catch (Exception ex)
            { }
        }
    }

    [WebMethod]
    public static string RenameCasboDocument(string newFileName, string oldFileName, string fileId)
    {
        string errorMsg = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(fileId))
            {
                if (newFileName.LastIndexOf('.') <= 0)
                    newFileName = newFileName + ".pdf";
                string newName = GetNewFileName(dirPath, newFileName);
                string newFName = (string.IsNullOrEmpty(newName) ? newFileName : newName);
                File.Move((dirPath + oldFileName), dirPath + newFName);
                try
                {
                    File.Delete(Path.Combine(dirPath, string.Format("{0}.png", Path.GetFileNameWithoutExtension(oldFileName))));
                }
                catch (Exception ex)
                { }
                string failedFileNames = string.Empty;
                CreateThumbnailImage(newFName, newFileName, ref failedFileNames);
                if (string.IsNullOrEmpty(failedFileNames))
                    General_Class.DocumentsUpdateForCasbo(newFName, fileId);
                else
                    errorMsg = "Failed to rename file!";
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            errorMsg = "Failed to rename file!";
        }
        return errorMsg;
    }
}