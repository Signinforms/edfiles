﻿<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    CodeFile="ChangeDefaultPassword.aspx.cs" Inherits="Manager_ChangeDefaultPassword" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="JavaScript" type="text/jscript">
        $(document).ready(function () {

        });
    </script>
    <div class="Naslov_Crven">Change Password</div>

    <table class="left" width="100%">
        <tr>
            <td style="margin-left: 10px;">
                <br />
                <br />
                <asp:DropDownList style="font-size:20px;margin-left:80px;" ID="drpPasswordName" runat="server">
                    <asp:ListItem Text="Default" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Administrator" Value="2"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanle1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="tekstDef" style="">
                            <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                <tr>
                                    <td align="left" class="podnaslov">New&nbsp;Password</td>
                                    <td width="1%">&nbsp;</td>
                                    <td width="30%">
                                        <asp:TextBox class="form_tekst_field" ID="textDefault" name="textDefault" runat="server" TextMode="Password" size="30" MaxLength="50" />
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="textDefault"
                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />A password is required." />
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="textDefaultValidate" TargetControlID="RequiredFieldValidator1"
                                            HighlightCssClass="validatorCalloutHighlight" />
                                    </td>
                                    <td width="67%" rowspan="4" valign="middle">
                                        <div class="tekstDef">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="podnaslov">Confirm&nbsp;Password</td>
                                    <td width="1%">&nbsp;</td>
                                    <td width="30%">
                                        <asp:TextBox class="form_tekst_field" name="textConfDefault" runat="server" TextMode="Password" ID="textConfDefault" size="30" MaxLength="50" />
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="textConfDefault"
                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />A confirm password is required." />
                                        <asp:CompareValidator ID="CompareValidator1" ControlToCompare="textDefault" ControlToValidate="textConfDefault"
                                            runat="Server" Display="None" ErrorMessage="<b>Compare Password Fields</b><br />A confirm password isn't fit." />
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="textConfDefaultValidate" TargetControlID="RequiredFieldValidator2"
                                            HighlightCssClass="validatorCalloutHighlight" />
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="textConfDefaultValidate3" TargetControlID="CompareValidator1"
                                            HighlightCssClass="validatorCalloutHighlight" />
                                    </td>
                                    <td width="67%" rowspan="4" valign="middle">
                                        <div class="tekstDef">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <br />
                                        <asp:Button ID="ImageButton1" runat="server" style="font-size:15px;" Text="Submit" OnClick="ImageButton1_Click" />
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <%--<tr>
            <td>
                <div class="Naslov_Crven">Change Administrator Password</div>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="tekstDef" style="">
                            <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                <tr>
                                    <td align="left" class="podnaslov">New&nbsp;Password</td>
                                    <td width="1%">&nbsp;</td>
                                    <td width="30%">
                                        <asp:TextBox class="form_tekst_field" ID="textDefault1" name="textDefault1" runat="server" TextMode="Password" size="30" MaxLength="50" />
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ControlToValidate="textDefault1"
                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />A password is required." />
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="textDefaultValidate1" TargetControlID="RequiredFieldValidator3"
                                            HighlightCssClass="validatorCalloutHighlight" />
                                    </td>
                                    <td width="67%" rowspan="4" valign="middle">
                                        <div class="tekstDef">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" class="podnaslov">Confirm&nbsp;Password</td>
                                    <td width="1%">&nbsp;</td>
                                    <td width="30%">
                                        <asp:TextBox class="form_tekst_field" name="textConfDefault1" runat="server" TextMode="Password" ID="textConfDefault1" size="30" MaxLength="50" />
                                        <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ControlToValidate="textConfDefault1"
                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />A confirm password is required." />
                                        <asp:CompareValidator ID="CompareValidator2" ControlToCompare="textDefault1" ControlToValidate="textConfDefault1"
                                            runat="Server" Display="None" ErrorMessage="<b>Compare Password Fields</b><br />A confirm password isn't fit." />
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="textConfDefaultValidate1" TargetControlID="RequiredFieldValidator4"
                                            HighlightCssClass="validatorCalloutHighlight" />
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="textConfDefaultValidate2" TargetControlID="CompareValidator2"
                                            HighlightCssClass="validatorCalloutHighlight" />
                                    </td>
                                    <td width="67%" rowspan="4" valign="middle">
                                        <div class="tekstDef">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="ImageButton2" runat="server" Text="Submit" OnClick="ImageButton2_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>--%>
    </table>
</asp:Content>
