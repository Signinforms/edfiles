﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Shinetech.Utility;

public partial class Manager_ChangeDefaultPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void ImageButton1_Click(object sender, EventArgs e)
    {
        try 
        {
            string def = "Default";
            int value = FileFolderManagement.ChangeDefaultPassword(drpPasswordName.SelectedItem.Text.ToString() == def ? "Default_Password" : "Admin_Delete_Password", PasswordGenerator.GetMD5(textDefault.Text.Trim()));
            if (value > 0)
                ScriptManager.RegisterClientScriptBlock(UpdatePanle1, typeof(UpdatePanel), "alertok", "alert('Password has been changed successfully')", true);
            else
                ScriptManager.RegisterClientScriptBlock(UpdatePanle1, typeof(UpdatePanel), "alertok", "alert('Error while updating password. Please try agian later.')", true);
        }
        catch (Exception ex) 
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanle1, typeof(UpdatePanel), "alertok", "alert('Error while updating password. Please try agian later.')", true);
        }
    }
}