<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    CodeFile="CreateFileFolders.aspx.cs" Inherits="CreateFileFolders_Admin" Title="" %>

<%@ Register Src="../Controls/FileRequestControl.ascx" TagName="FileRequestControl"
    TagPrefix="uc2" %>
<%--<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>--%>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
   
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <style>
          #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
            left:0;
        }

        .ui-button {
            background:#dcdcdc none repeat scroll 0 0 !important;
        }

        .ui-autocomplete {
            font-size:14px;
        }

    </style>

    <script type="text/javascript">

        function OnBtnUploadclick() {
            showLoader();
            var officeUser = $("#" + '<%= DDListOfficeName.ClientID %>').val();
            var template = $("#" + '<%= DropDownList3.ClientID%>').val();
            if (officeUser == undefined || officeUser == "")
            {
                alert("Please select Office User to create folder!");
                hideLoader();
                return false;
            }
            //else if (template == undefined || template == "" || template == "-- Options --")
            //{
            //    alert("Please select Template to create folder!");
            //    hideLoader();
            //    return false;
            //}

            var source = document.getElementById("<%=FileUpload1.ClientID%>").value;
             if (source.length > 0) {
                 if (source.substring(source.lastIndexOf(".") + 1, source.length).toLowerCase() != "xlsx" && source.substring(source.lastIndexOf(".") + 1, source.length).toLowerCase() != "xls") {
                     alert("Only excel file is allowed!");
                     hideLoader();
                     return false;
                 }
             }
             else {
                 alert("Please select at least one file to create folder!");
                 hideLoader();
                 return false;
             }
        }


        function showLoader() {
            $('#overlay').show();
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
        }
    </script>


       <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>

    <table>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="800" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                            style="background-repeat: repeat-x;">
                            <div style="background: url(images/inner_b.gif); background-position: right top;
                                background-repeat: no-repeat; margin-top: 10px;">
                                <div style="width: 725px; height: 75px;">
                                    <div class="Naslov_Crven">
                                        Create File Folders<asp:Label ID="labUserName" runat="server" Text="" CssClass="Naslov_CrvenName"></asp:Label>
                                    </div>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="tekstDef" style="">
                                            <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                <tr>
                                                    <td align="left" class="podnaslov">
                                                        Office&nbsp;Name
                                                    </td>
                                                    <td align="left">
                                                        <asp:DropDownList ID="DDListOfficeName" runat="server" Sorted="true"
                                                            DataValueField="UID" DataTextField="Name" Width="255px" OnOnClick="DropDownList1_OnSelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="podnaslov">
                                                        Templates
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="DropDownList3" runat="server" AutoPostBack="true"
                                                            DataTextField="Name" DataValueField="TemplateID" CausesValidation="false" Width="155px"
                                                            OnOnClick="DropDownList3_OnSelectedIndexChanged" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="true" />
                                                    </td>

                                                    <td>
                                                        <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" OnClientClick="return OnBtnUploadclick();" Text="Upload" Style="margin-bottom: 15px;" />
                                                    </td>
                                                </tr>
                                               
                                               
                                            </table>
                                        </div>
                                      
                                        <div class="podnaslov">
                                            <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnUpload" />
                                    </Triggers>
                                </asp:UpdatePanel>
                              
                        </td>
                        <td width="10" align="center">
                            <img src="../images/lin.gif" width="1" height="453" vspace="10" />
                        </td>
                        <td valign="top" background="images/bg_main.gif" bgcolor="#f6f5f2" style="background-repeat: repeat-x;">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
