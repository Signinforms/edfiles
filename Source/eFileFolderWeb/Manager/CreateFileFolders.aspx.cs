using System;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using DataQuicker2.Framework;
using Shinetech.DAL;
using Shinetech.Utility;
using Account = Shinetech.DAL.Account;
using AjaxControlToolkit;
using System.Web.Script.Serialization;
using Shinetech.Framework.Controls;
//using Excel;
using ExcelDataReader;
using System.Collections.Generic;
using Shinetech.Engines.Adapters;

using System.Security;
using System.Security.Permissions;
using System.Security.Principal;
using System.Threading;
using Newtonsoft.Json;

public partial class CreateFileFolders_Admin : LicensePage
{
    static string VolumePath = @"C:\inetpub\ftproot\Volume";
    Dictionary<int, string> colors = new Dictionary<int, string>();
    private NLogLogger _logger = new NLogLogger();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DDListOfficeName.DataSource = UserManagement.GetOfficeUsers();
            DDListOfficeName.DataBind();

            this.DataSource = this.MakeDataTable();

            // BindGrid();        
        }

        colors.Add(0, "#FF0000");
        colors.Add(1, "#FF9933");
        colors.Add(2, "#CC0066");
        colors.Add(3, "#CC9999");
        colors.Add(4, "#9900CC");
        colors.Add(5, "#9999FF");
        colors.Add(6, "#663300");
        colors.Add(7, "#66CC33");
        colors.Add(8, "#333366");
        colors.Add(9, "#33CC99");
        colors.Add(10, "#0033CC");
        colors.Add(11, "#00CCFF");

    }

    protected string GetUserPath(string uid)
    {
        if (!Directory.Exists(VolumePath))
            Directory.CreateDirectory(VolumePath);
        return VolumePath;
    }

    protected ArrayList GetTemplates(string uid)
    {

        DataTable table = FileFolderManagement.GetTemplatesByUID(uid);

        ArrayList temples = new ArrayList();
        TemplateEntity template;

        foreach (DataRow item in table.Rows)
        {
            template = new TemplateEntity(item["Name"] as string);
            template.TemplateID = (int)item["TemplateID"];
            template.OfficeID = uid;
            template.Content = item["Content"] as string;

            temples.Add(template);
        }

        //DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(uid);
        //var officeID1 = dtOfficeUser.Rows[0]["OfficeUID"].ToString();

        DataTable otable = FileFolderManagement.GetTemplatesByUID(uid);

        foreach (DataRow item in otable.Rows)
        {
            template = new TemplateEntity(item["Name"] as string);
            template.TemplateID = (int)item["TemplateID"];
            template.OfficeID = uid;
            template.Content = item["Content"] as string;

            temples.Add(template);
        }

        template = new TemplateEntity("-- Options --");
        template.TemplateID = -1;
        template.OfficeID = uid;
        template.Content = "";

        temples.Insert(0, template);

        return temples;

    }


    protected void imageField_Click(object sender, EventArgs e)
    {
        string uid = this.EffID;

        initFolders();

        //转换文件
        foreach (DataRow row in this.DataSource.Rows)
        {
            if (Convert.ToInt32(row["File_ID"]) > 0) //该文件已经存在
            {
                string path = row["File_Path"] as string;

                if (File.Exists(path))
                {
                    try
                    {
                        File.Delete(path);
                    }
                    catch (Exception)
                    { }
                }

                row["Status"] = "Pass";

                continue;
            }

            int fid = Convert.ToInt32(row["Folder_ID"]);

            var folder = new FileFolder(fid);

            int oldertid = Convert.ToInt32(row["Divider_ID"]);

            if (oldertid <= 0) //该文件已经存在            
            {
                #region 创建新的divider
                Divider divider = new Divider();

                divider.EffID.Value = folder.FolderID.Value;
                divider.DOB.Value = DateTime.Now;
                divider.Locked.Value = false;
                divider.OfficeID.Value = folder.OfficeID.Value;
                divider.GUID.Value = Guid.NewGuid().ToString("N");

                divider.Name.Value = Path.GetFileNameWithoutExtension(row["FileName"].ToString()); //带pdf扩展名

                var r = new Random();

                divider.Color.Value = colors[Convert.ToInt32(r.NextDouble() * 11)];

                divider.Create();

                folder.OtherInfo.Value++;

                folder.Update();

                int tid = FileFolderManagement.GetDividerId(divider.GUID.Value, divider.OfficeID.Value);
                if (tid == -1)
                {
                    tid = divider.DividerID.Value;
                }

                row["Divider_ID"] = tid;
                oldertid = tid;

                #endregion
            }

            string vpath = Server.MapPath("~/Volume");
            string pathname = string.Format("{0}\\{1}\\{2}", vpath, fid, oldertid);

            string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, fid, Path.DirectorySeparatorChar, oldertid); //转换目标路径
            if (!Directory.Exists(dstpath))
            {
                Directory.CreateDirectory(dstpath);
            }

            string filePath = row["File_Path"] as string;

            //该文件不在数据库但是物理文件也不存在，则只是创建divider等
            if (string.IsNullOrEmpty(filePath))
            {
                row["Status"] = "Pass";
                continue;
            }

            string filename = Path.GetFileName(filePath);
            string encodeName = HttpUtility.UrlPathEncode(filename);
            encodeName = encodeName.Replace("%", "$");
            dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);

            //拷贝文件并转换格式
            try
            {
                File.Copy(filePath, dstpath, true);
                ImageAdapter.ProcessPostFile(dstpath, fid.ToString(), oldertid.ToString(), uid, pathname, filePath);

                row["Status"] = "Success";

                try
                {
                    File.Delete(filePath); //添加后删除原文
                }
                catch (Exception)
                {
                }


            }
            catch (Exception exp)
            {
                row["Status"] = exp.Message;
            }
        }

        string fullPath = GetUserPath(this.EffID);

        string[] folders = Directory.GetDirectories(fullPath);

        foreach (var subfolder in folders)
        {
            string[] files = Directory.GetFiles(subfolder);
            if (files.Length == 0)
            {
                try
                {
                    Directory.Delete(subfolder);
                }
                catch (Exception)
                {
                }

            }
        }

        // this.BindGrid();

    }

    protected void DropDownList1_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(DDListOfficeName.SelectedValue)) return;

        this.EffID = DDListOfficeName.SelectedValue;

        this.DataSource.Rows.Clear();


        IList templates = GetTemplates(DDListOfficeName.SelectedValue);

        DropDownList3.DataSource = templates;
        DropDownList3.DataBind();

        DropDownList3.SelectedIndex = 0;

        initFiles();
    }

    protected void DropDownList3_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(DropDownList3.SelectedValue)) return;

        this.EffDividerID = DropDownList3.SelectedValue;

        this.DataSource.Rows.Clear();

        initFiles();
    }

    public string EffID
    {
        get
        {
            return (string)Session["Manage_Folder_ID"];
        }
        set
        {
            Session["Manage_Folder_ID"] = value;

        }
    }

    public string EffDividerID
    {
        get
        {
            return (string)Session["Manage_Divider_ID"];
        }
        set
        {
            Session["Manage_Divider_ID"] = value;

        }
    }


    protected void initFiles()
    {
        string fullPath = GetUserPath(this.EffID);

        //Get All Sub folders 
        string[] folders = Directory.GetDirectories(fullPath);
        var i = 0;

        var dividerNames = new ArrayList();
        var dividerNamesWithCase = new Hashtable();

        if (!DropDownList3.SelectedValue.Equals("-1"))
        {
            var template = new FolderTemplate(Convert.ToInt32(DropDownList3.SelectedValue));
            string[] dividers = template.Content.Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            var dname = "";
            foreach (var divider in dividers)
            {
                dname = divider.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries)[0];
                dividerNamesWithCase.Add(dname.ToLower(), dname);
                dividerNames.Add(dname.ToLower());
            }

        }

        //获取volume目录下的所有folder文件夹
        foreach (var subfolder in folders)
        {
            #region 获取folder的信息和是否已经创建并存在该folder

            var parentPath = Path.GetDirectoryName(subfolder);
            var folderName = subfolder.Substring(parentPath.Length + 1);
            string[] folderNames = folderName.Split(new string[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries); //folderName.Split(',',' '); 
            int fodlerId = -1;
            //判断该文件夹是否已经存在
            fodlerId = FileFolderManagement.GetFileFolderWithItsName(folderName, this.EffID);

            string subFirstName, subLastName;
            if (folderNames.Length > 1)
            {
                subFirstName = folderNames[0];
                subLastName = folderNames[1];
            }
            else
            {
                subFirstName = folderName;
                subLastName = "";
            }

            #endregion

            string[] files = Directory.GetFiles(subfolder);
            if (DropDownList3.SelectedValue.Equals("-1"))
            {
                // subfolder获取其下的文件
                foreach (var pdf in files)
                {
                    if (!Path.GetExtension(pdf).ToLower().Equals(".pdf"))
                    {
                        #region 删除非pdf文件
                        try
                        {
                            File.Delete(pdf); // 要删除否则后面无法删除目录
                        }
                        catch (Exception)
                        {


                        }
                        continue;

                        #endregion
                    }

                    #region 不选择任何模板时,以文件为准创建tab并导入pdf文件

                    i++;
                    var newRow = this.DataSource.NewRow();

                    newRow["ItemNo"] = i.ToString();
                    newRow["FileName"] = Path.GetFileName(pdf);
                    newRow["File_Path"] = pdf;

                    string[] fileNames = Path.GetFileNameWithoutExtension(pdf).Split(new string[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);
                    if (fileNames.Length > 1)
                    {
                        newRow["FirstName_File"] = fileNames[0];
                        newRow["LastName_File"] = fileNames[1];
                    }
                    else
                    {
                        newRow["FirstName_File"] = Path.GetFileNameWithoutExtension(pdf);
                        newRow["LastName_File"] = "";
                    }

                    newRow["FolderName"] = folderName;
                    newRow["FirstName_Folder"] = subFirstName;
                    newRow["LastName_Folder"] = subLastName;

                    newRow["Folder_ID"] = fodlerId;

                    if (fodlerId != -1) //该folder已经存在的话，判断divider和文件
                    {
                        newRow["Status"] = string.Format("Folder:{0} exists", folderName);

                        //判断该文件是否已经存在
                        int dividerId = FileFolderManagement.GetDividersWithName(Path.GetFileNameWithoutExtension(pdf), fodlerId);

                        if (dividerId > 0)
                        {
                            newRow["Divider_ID"] = dividerId;
                            newRow["Status"] = string.Format("Tab:{0} exists, Will No Convert", Path.GetFileNameWithoutExtension(pdf));

                            string divider = Path.GetFileNameWithoutExtension(pdf);//必然物理文件一定存在
                            int fileId = FileFolderManagement.GetFileWithName(divider, dividerId, fodlerId);

                            string fullfile = pdf; // 一定存在该divider

                            if (fileId > 0) // 物理文件和数据库文件已经存在
                            {
                                newRow["File_ID"] = fileId;// 后面检查该值不为-1则认为没有存在file在数据库中
                                newRow["Status"] = string.Format("File {0} exists, Will No Add this file again", divider + ".pdf");

                                //检查该物理文件是否存在，提示删除

                                if (!fullfile.Equals("")) //没有与tab匹配文件
                                {
                                    newRow["File_Path"] = fullfile;
                                    newRow["Status"] = string.Format("File {0} exists in volume and database, please delete it from disk", divider + ".pdf");
                                }
                            }
                            else
                            {
                                newRow["File_Path"] = fullfile;
                                newRow["Status"] = string.Format("File {0} exists, will add to this tab:{1}", divider + ".pdf", divider);
                            }
                        }
                    }

                    this.DataSource.Rows.Add(newRow);

                    #endregion
                }
            }
            else
            {
                #region 遍历模板并检查该tab是否已经存在
                foreach (string divider in dividerNames)
                {
                    i++;

                    #region
                    var newRow = this.DataSource.NewRow();

                    newRow["ItemNo"] = i.ToString();
                    newRow["FileName"] = dividerNamesWithCase[divider].ToString() + ".pdf"; //大小写敏感

                    string[] fileNames = dividerNamesWithCase[divider].ToString().Split(new string[] { ",", " " }, StringSplitOptions.RemoveEmptyEntries);
                    if (fileNames.Length > 1)
                    {
                        newRow["FirstName_File"] = fileNames[0];
                        newRow["LastName_File"] = fileNames[1];
                    }
                    else
                    {
                        newRow["FirstName_File"] = dividerNamesWithCase[divider].ToString();
                        newRow["LastName_File"] = "";
                    }

                    newRow["FolderName"] = folderName;
                    newRow["FirstName_Folder"] = subFirstName;
                    newRow["LastName_Folder"] = subLastName;

                    newRow["Folder_ID"] = fodlerId;

                    newRow["Divider_ID"] = -1;

                    //检查该tab是否已经存在
                    int dividerId = FileFolderManagement.GetDividersWithName(divider, fodlerId);

                    if (dividerId > 0)
                    {
                        newRow["Divider_ID"] = dividerId;

                        newRow["Status"] = string.Format("Template Tab:{0} exists, Will No Add this tab", divider);

                        //判断该文件是否已经存在

                        int fileId = FileFolderManagement.GetFileWithName(divider, dividerId, fodlerId);

                        if (fileId > 0)
                        {
                            newRow["File_ID"] = fileId;// 后面检查该值不为-1则认为没有存在file在数据库中
                            newRow["Status"] = string.Format("File {0} exists, Will No Add this file again", divider + ".pdf");

                            //检查该物理文件是否存在，提示删除
                            string fullfile = findMatchingFile(divider, files);
                            if (!fullfile.Equals("")) //没有与tab匹配文件
                            {
                                newRow["File_Path"] = fullfile;
                                newRow["Status"] = string.Format("File {0} exists in volume and database, please delete it from disk", divider + ".pdf");
                            }
                        }
                        else
                        {
                            string fullfile = findMatchingFile(divider, files);
                            if (fullfile.Equals("")) //没有与tab匹配文件
                            {
                                newRow["Status"] = string.Format("File {0} not exists", divider + ".pdf");
                            }
                            else
                            {
                                newRow["File_Path"] = fullfile;
                                newRow["Status"] = string.Format("File {0} exists, will add to this tab:{1}", divider + ".pdf", divider);
                            }
                        }

                    }
                    else
                    {
                        newRow["Status"] = string.Format("Create Tab:{0} and add matching file ", divider);
                    }


                    this.DataSource.Rows.Add(newRow);

                    #endregion
                }

                #endregion
            }

        }

        // this.BindGrid();

    }

    protected string findMatchingFile(string divider, string[] files)
    {
        foreach (string pdf in files)
        {
            string filename = Path.GetFileNameWithoutExtension(pdf).ToLower();

            if (divider.Equals(filename)) return pdf;
        }

        return "";
    }

    //创建没有的folder
    protected void initFolders()
    {
        //var template = new FolderTemplate(Convert.ToInt32(DropDownList3.SelectedValue));
        //string[] dividers = template.Content.Value.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);


        Account act = new Account(this.EffID);
        string officeUID1 = this.EffID;
        if (act.IsSubUser.Value == "1")
        {
            officeUID1 = act.OfficeUID.Value;
        }


        // Convert to efilefolders
        foreach (DataRow row in this.DataSource.Rows)
        {
            var foldername = row["FolderName"].ToString();
            int folderId = -1;
            folderId = FileFolderManagement.GetFileFolderWithItsName(foldername, this.EffID);

            if (folderId == -1)
            {
                FileFolder folder = null;

                folder = new FileFolder();
                folder.FolderName.Value = row["FolderName"] as string;
                folder.FirstName.Value = row["FirstName_Folder"] as string;
                folder.Lastname.Value = row["LastName_Folder"] as string;

                folder.DOB.Value = null;
                folder.CreatedDate.Value = DateTime.Now;
                folder.OfficeID.Value = this.EffID;
                folder.OfficeID1.Value = officeUID1;
                folder.OtherInfo.Value = 0;
                folder.SecurityLevel.Value = 1;

                Dictionary<string, Divider> list = new Dictionary<string, Divider>();

                folderId = FileFolderManagement.CreatNewFileFolder(folder, list);
            }

            //为该folder添加divider，根据filename

            row["Folder_ID"] = folderId;


        }
    }

    //protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    string sortDirection = "";
    //    string sortExpression = e.SortExpression;
    //    if (this.Sort_Direction == SortDirection.Ascending)
    //    {
    //        this.Sort_Direction = SortDirection.Descending;
    //        sortDirection = "DESC";
    //    }
    //    else
    //    {
    //        this.Sort_Direction = SortDirection.Ascending;
    //        sortDirection = "ASC";
    //    }
    //    DataView Source = new DataView(this.DataSource);
    //    Source.Sort = e.SortExpression + " " + sortDirection;

    //    this.DataSource = Source.ToTable();
    //    BindGrid();
    //}

    //private SortDirection Sort_Direction
    //{
    //    get
    //    {
    //        if (string.IsNullOrEmpty(SortDirection1.Value))
    //        {
    //            SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
    //        }

    //        return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

    //    }
    //    set
    //    {
    //        this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
    //    }
    //}

    //protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    //{
    //    WebPager1.CurrentPageIndex = e.NewPageIndex;
    //    WebPager1.DataSource = this.DataSource;
    //    WebPager1.DataBind();
    //}

    //protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    //{
    //    WebPager1.CurrentPageIndex = 0;
    //    WebPager1.DataSource = this.DataSource;
    //    WebPager1.DataBind();
    //}

    public DataTable DataSource
    {
        get
        {
            return this.Session["FileFolders_Source"] as DataTable;
        }
        set
        {
            this.Session["FileFolders_Source"] = value;
        }
    }

    //private void BindGrid()
    //{
    //    WebPager1.DataSource = this.DataSource;
    //    WebPager1.DataBind();
    //}

    private DataTable MakeDataTable()
    {
        // Create new DataTable.
        DataTable table = new DataTable();

        // Declare DataColumn and DataRow variables.
        DataColumn column;

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "ItemNo";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FileName";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FirstName_File";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "LastName_File";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "FileNo";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "FileYear";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "FirstName_Folder";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "LastName_Folder";
        column.DefaultValue = false;
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "FolderName";
        column.DefaultValue = "";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.Int32");
        column.ColumnName = "Folder_ID";
        column.DefaultValue = -1;
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "DividerNo";
        column.DefaultValue = "";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.Int32");
        column.ColumnName = "Divider_ID";
        column.DefaultValue = -1;
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.Int32");
        column.ColumnName = "File_ID";
        column.DefaultValue = -1;
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "Status";
        column.DefaultValue = "NA";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "File_Path";
        table.Columns.Add(column);


        return table;
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        try
        {
            if (FileUpload1.HasFiles)
            {

                IExcelDataReader reader = null;
                if (FileUpload1.FileName.Substring(FileUpload1.FileName.LastIndexOf(".")) == ".xls")
                    reader = ExcelReaderFactory.CreateBinaryReader(FileUpload1.PostedFile.InputStream);
                else
                    reader = ExcelReaderFactory.CreateOpenXmlReader(FileUpload1.PostedFile.InputStream);

                DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true,
                    }
                });

                string user = DDListOfficeName.SelectedItem.Text;
                string template = string.Empty;
                try { template = Convert.ToInt32(DropDownList3.SelectedItem.Value) == -1 ? string.Empty : DropDownList3.SelectedItem.Text; }
                catch (Exception ex)
                {
                    _logger.Info(ex);
                }
                bool isError = false;
                List<UserStructure> userDetails = JsonConvert.DeserializeObject<List<UserStructure>>(JsonConvert.SerializeObject(General_Class.GetUserIdFromUserName(user)));
                if (userDetails != null && userDetails.Count > 0)
                {
                    string uid = userDetails[0].UID;
                    for (int i = 0; i < result.Tables[0].Rows.Count; i++)
                    {
                        string foldername = null, lastName = null, firstName = null, fileNumber = null, address = null, city = null, alert1 = null, postal = null, tel = null, ssn = null,
                            email = null, alert2 = null, comments = null, dob = null;
                        try
                        {
                            DataRow row = result.Tables[0].Rows[i];

                            if (result.Tables[0].Columns.Contains("FolderName") && !string.IsNullOrEmpty(row.Field<string>("FolderName")))
                                foldername = row.Field<string>("FolderName").ToString().Trim();
                            else
                                foldername = row.Field<string>("LastName").ToString().Trim() + ", " + row.Field<string>("FirstName").ToString().Trim();

                            if (result.Tables[0].Columns.Contains("FirstName") && !string.IsNullOrEmpty(row.Field<string>("FirstName")))
                                firstName = row.Field<string>("FirstName").ToString().Trim();

                            if (result.Tables[0].Columns.Contains("Lastname") && !string.IsNullOrEmpty(row.Field<string>("LastName")))
                                lastName = row.Field<string>("LastName").ToString().Trim();

                            if (result.Tables[0].Columns.Contains("FileNumber") && !string.IsNullOrEmpty(Convert.ToString(row.Field<object>("FileNumber"))))
                                fileNumber = Convert.ToString(row.Field<object>("FileNumber"));

                            if (result.Tables[0].Columns.Contains("Email") && !string.IsNullOrEmpty(row.Field<string>("Email")))
                                email = row.Field<string>("Email").ToString().Trim();

                            if (result.Tables[0].Columns.Contains("Address") && !string.IsNullOrEmpty(row.Field<string>("Address")))
                                address = row.Field<string>("Address").ToString().Trim();

                            if (result.Tables[0].Columns.Contains("City") && !string.IsNullOrEmpty(row.Field<string>("City")))
                                city = row.Field<string>("City").ToString().Trim();

                            if (result.Tables[0].Columns.Contains("FaxNumber") && !string.IsNullOrEmpty(Convert.ToString(row.Field<object>("FaxNumber"))))
                                ssn = Convert.ToString(row.Field<object>("FaxNumber"));

                            if (result.Tables[0].Columns.Contains("Tel") && !string.IsNullOrEmpty(Convert.ToString(row.Field<object>("Tel"))))
                                tel = Convert.ToString(row.Field<object>("Tel"));

                            if (result.Tables[0].Columns.Contains("Alert1") && !string.IsNullOrEmpty(row.Field<string>("Alert1")))
                                alert1 = row.Field<string>("Alert1").ToString().Trim();

                            if (result.Tables[0].Columns.Contains("Alert2") && !string.IsNullOrEmpty(row.Field<string>("Alert2")))
                                alert2 = row.Field<string>("Alert2").ToString().Trim();

                            if (result.Tables[0].Columns.Contains("PostalCode") && row.Field<double?>("PostalCode") != null)
                                postal = row.Field<double>("PostalCode").ToString().Trim();

                            if (result.Tables[0].Columns.Contains("Comments") && !string.IsNullOrEmpty(row.Field<string>("Comments")))
                                comments = row.Field<string>("Comments").Trim();

                            if (result.Tables[0].Columns.Contains("SSN") && !string.IsNullOrEmpty(Convert.ToString(row.Field<double>("SSN"))))
                                ssn = Convert.ToString(row.Field<double>("SSN")).Trim();

                            if (result.Tables[0].Columns.Contains("DOB") && row.Field<string>("DOB") != null)
                                dob = row.Field<string>("DOB");

                            DataSet dt = General_Class.CreateFileFolder(true, false, foldername != null ? foldername : lastName + ", " + firstName, uid, uid, firstName, lastName, 0, fileNumber, alert1, template, address, city, postal, tel, ssn, email, alert2, comments, dob);
                        }
                        catch (Exception ex)
                        {
                            isError = true;
                            _logger.Info(ex);
                        }
                    }
                }
                string msg = isError ? "Failed to create some folder(s) due to invalid format!" : "Folder(s) have been created!";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('" + msg + "');", true);
                ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
            }
        }
        catch (Exception ex)
        {
            _logger.Info(ex);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Failed to create folder. Please try again later.');", true);
            ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
        }
    }

}

public class UserStructure
{
    public string UID { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
}

public class FolderStructure
{
    public string FolderName { get; set; }
    public int FolderID { get; set; }
    public int DividerID { get; set; }
    public string Name { get; set; }
    public string OfficeID { get; set; }
    public int EFFID { get; set; }

}
