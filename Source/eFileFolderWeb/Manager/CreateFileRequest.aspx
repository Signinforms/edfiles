<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    CodeFile="CreateFileRequest.aspx.cs" Inherits="CreateFileRequest_Admin" Title="" %>

<%@ Register Src="../Controls/FileRequestControl.ascx" TagName="FileRequestControl" TagPrefix="uc2" %>
<%@ Register Src="../Controls/QuickFind.ascx" TagName="QuickFind" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="800" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                            style="background-repeat: repeat-x;">
                            <div style="background: url(images/inner_b.gif); background-position: right top; background-repeat: no-repeat; margin-top: 10px;">
                                <div style="width: 725px; height: 75px;">
                                    <div class="Naslov_Crven">
                                        Create File Request<asp:Label ID="labUserName" runat="server" Text="" CssClass="Naslov_CrvenName"></asp:Label>
                                    </div>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="tekstDef" style="">
                                            <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                <tr>
                                                    <td align="left" class="podnaslov">Office&nbsp;Name</td>
                                                    <td align="left">
                                                        <ajaxToolkit:ComboBox ID="DDListOfficeName" runat="server" Width="255px" DropDownStyle="DropDownList"
                                                            AutoCompleteMode="SuggestAppend" CssClass="WindowsStyle" DataValueField="UID"
                                                            DataTextField="Name" AutoPostBack="true" CaseSensitive="false" EnableViewState="true"
                                                            OnSelectedIndexChanged="DropDownList1_OnSelectedIndexChanged" AppendDataBoundItems="false">
                                                        </ajaxToolkit:ComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="podnaslov">User&nbsp;Name</td>
                                                    <td>
                                                        <asp:TextBox ID="textUserName" name="textUserName" runat="server" class="form_tekst_field" size="50" MaxLength="23" />
                                                        <asp:RequiredFieldValidator runat="server" ID="userNameV" ControlToValidate="textUserName"
                                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The user name is required!" />
                                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                                            TargetControlID="userNameV" HighlightCssClass="validatorCalloutHighlight" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="podnaslov">Telephone</td>
                                                    <td>
                                                        <asp:TextBox ID="textPhone" name="textPhone" runat="server" class="form_tekst_field" size="50" MaxLength="23" />
                                                        <asp:RequiredFieldValidator runat="server" ID="telN" ControlToValidate="textPhone"
                                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The telephone is required!" />
                                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11"
                                                            TargetControlID="telN" HighlightCssClass="validatorCalloutHighlight" />
                                                        <asp:RegularExpressionValidator ID="TelV" ControlToValidate="textPhone" runat="server"
                                                            ErrorMessage="<b>Required Field Missing</b><br />It is not a telphone format."
                                                            ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                                            TargetControlID="TelV" HighlightCssClass="validatorCalloutHighlight" />
                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td align="left" class="podnaslov">Requseted By</td>

                                                    <td>
                                                        <asp:TextBox name="textRequestBy" runat="server" class="form_tekst_field" ID="textRequestBy" size="50" MaxLength="23" />
                                                        <asp:RequiredFieldValidator runat="server" ID="textRequestByN" ControlToValidate="textRequestBy"
                                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The telephone is required!" />
                                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14"
                                                            TargetControlID="textRequestByN" HighlightCssClass="validatorCalloutHighlight" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="podnaslov">Request Date</td>
                                                    <td>
                                                        <asp:TextBox ID="textRequestDate" name="textRequestDate" runat="server" class="form_tekst_field" size="50" />
                                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" PopupPosition="Right" runat="server" TargetControlID="textRequestDate" Format="MM-dd-yyyy">
                                                        </ajaxToolkit:CalendarExtender>
                                                        <asp:RequiredFieldValidator runat="server" ID="DobV" ControlToValidate="textRequestDate"
                                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The Request Date is required!" />
                                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5"
                                                            TargetControlID="DobV" HighlightCssClass="validatorCalloutHighlight" />
                                                        <asp:RegularExpressionValidator ID="DobC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The request date is not a date"
                                                            ControlToValidate="textRequestDate" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                                            Visible="true" Display="None"></asp:RegularExpressionValidator>
                                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6"
                                                            TargetControlID="DobC" HighlightCssClass="validatorCalloutHighlight" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td align="left" colspan="2" class="podnaslov">Please provide complete File information (Last Name, First Name, File#, Year)</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <table>
                                                            <tr>
                                                                <td align="left" class="podnaslov"><span>1.&nbsp;First Name </span>
                                                                    <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName1" size="30" MaxLength="30" /></td>
                                                                <td align="left" class="podnaslov"><span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                    <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName1" size="30" MaxLength="30" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="podnaslov"><span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox runat="server" class="form_tekst_field" ID="textFile1" size="30" MaxLength="30" /></td>
                                                                <td align="left" class="podnaslov"><span>&nbsp;&nbsp;&nbsp;Year&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox runat="server" class="form_tekst_field" ID="textYear1" size="30" MaxLength="30" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="podnaslov"><span>2.&nbsp;First Name </span>
                                                                    <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName2" size="30" MaxLength="30" /></td>
                                                                <td align="left" class="podnaslov"><span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                    <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName2" size="30" MaxLength="30" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="podnaslov"><span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox runat="server" class="form_tekst_field" ID="textFile2" size="30" MaxLength="30" /></td>
                                                                <td align="left" class="podnaslov"><span>&nbsp;&nbsp;&nbsp;Year&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox runat="server" class="form_tekst_field" ID="textYear2" size="30" MaxLength="30" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="podnaslov"><span>3.&nbsp;First Name </span>
                                                                    <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName3" size="30" MaxLength="30" /></td>
                                                                <td align="left" class="podnaslov"><span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                    <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName3" size="30" MaxLength="30" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="podnaslov"><span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox runat="server" class="form_tekst_field" ID="textFile3" size="30" MaxLength="30" /></td>
                                                                <td align="left" class="podnaslov"><span>&nbsp;&nbsp;&nbsp;Year&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox runat="server" class="form_tekst_field" ID="textYear3" size="30" MaxLength="30" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="podnaslov"><span>4.&nbsp;First Name </span>
                                                                    <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName4" size="30" MaxLength="30" /></td>
                                                                <td align="left" class="podnaslov"><span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                    <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName4" size="30" MaxLength="30" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="podnaslov"><span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox runat="server" class="form_tekst_field" ID="textFile4" size="30" MaxLength="30" /></td>
                                                                <td align="left" class="podnaslov"><span>&nbsp;&nbsp;&nbsp;Year&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox runat="server" class="form_tekst_field" ID="textYear4" size="30" MaxLength="30" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="podnaslov"><span>5.&nbsp;First Name </span>
                                                                    <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName5" size="30" MaxLength="30" /></td>
                                                                <td align="left" class="podnaslov"><span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                    <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName5" size="30" MaxLength="30" /></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left" class="podnaslov"><span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox runat="server" class="form_tekst_field" ID="textFile5" size="30" MaxLength="30" /></td>
                                                                <td align="left" class="podnaslov"><span>&nbsp;&nbsp;&nbsp;Year&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox runat="server" class="form_tekst_field" ID="textYear5" size="30" MaxLength="30" /></td>
                                                            </tr>
                                                            <tr></tr>
                                                            <tr>
                                                                <br />
                                                                <td align="left" colspan="2" class="podnaslov"><span>Additional Comments:&nbsp;&nbsp;&nbsp;</span><asp:TextBox runat="server" class="form_tekst_field" ID="textComment" size="72" MaxLength="200" /></td>

                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>


                                            </table>
                                        </div>
                                        <div align="center" style="height: 30px;">
                                            <%-- <asp:ImageButton runat="server" name="imageField" ID="imageField" OnClick="imageField_Click"
                        ImageUrl="~/images/submit_btn.gif" />--%>
                                            <asp:Button runat="server" name="imageField" ID="imageField" OnClick="imageField_Click"
                                                Text="Submit" />
                                        </div>
                                        <div class="podnaslov">
                                            <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="imageField" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                        </td>
                        <td width="10" align="center">
                            <img src="../images/lin.gif" width="1" height="453" vspace="10" /></td>
                        <td valign="top" background="images/bg_main.gif" bgcolor="#f6f5f2" style="background-repeat: repeat-x;">
                            <uc1:QuickFind ID="QuickFind1" runat="server" />
                            <uc2:FileRequestControl ID="fileRequest1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Button1ShowPopup" runat="server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" PopupControlID="pnlPopup" OnOkScript="anotherRequest()" CancelControlID="btnClose" OnCancelScript="closePage()" OkControlID="btnRequset"
                DropShadow="true" BackgroundCssClass="modalBackground" TargetControlID="Button1ShowPopup" />
            <!-- ModalPopup Panel-->
            <asp:Panel ID="pnlPopup" runat="server" Width="450px" Style="display: none; background-color: white">
                <div style="width: 100%; background-color: #C0C0C0"><strong>File Request</strong></div>
                <div class="tekstDef">
                    <table width="100%" border="0" cellpadding="2" cellspacing="0">
                        <tr>
                            <td align="left" class="podnaslov"><strong>Hello&nbsp;<asp:Label ID="LabelUserName" Text="" runat="server" />,</strong></td>
                        </tr>
                        <tr>
                            <td width="100%" align="left" class="podnaslov">Your request: {<strong><asp:Label ID="LabelResult" Text="" runat="server" /></strong>
                                }&nbsp;&nbsp;has been submitted.
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="podnaslov"></td>
                        </tr>

                    </table>
                </div>
                <div style="height: 10px">&nbsp;&nbsp;</div>
                <div align="center" style="width: 100%; background-color: white">
                    <asp:Button ID="btnRequset" runat="server" Text="Request Another" Width="120px" />
                    <asp:Button ID="btnClose" runat="server" Text="Close" Width="120px" />
                </div>
                s
            <div style="height: 10px">&nbsp;&nbsp;</div>

            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="imageField" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>

    <script language="javascript" type="text/javascript">
        function anotherRequest() {
            window.location = window.location;

        }
        function closePage() {
            window.location = "http://www.edfiles.com/Office/Welcome.aspx";
        }
    </script>
</asp:Content>
