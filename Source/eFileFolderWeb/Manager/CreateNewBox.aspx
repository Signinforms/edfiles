<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    CodeFile="CreateNewBox.aspx.cs" Inherits="CreateNewBox" Title="" %>

<%@ Register Src="../Controls/FileRequestControl.ascx" TagName="FileRequestControl"
    TagPrefix="uc2" %>
<%@ Register Src="../Controls/QuickFind.ascx" TagName="QuickFind" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="800" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                            style="background-repeat: repeat-x;">
                            <div style="background: url(images/inner_b.gif); background-position: right top;
                                background-repeat: no-repeat; margin-top: 10px;">
                                <div style="width: 725px; height: 75px;">
                                    <div class="Naslov_Crven">
                                        Create New Box<asp:Label ID="labUserName" runat="server" Text="" CssClass="Naslov_CrvenName"></asp:Label></div>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <div class="tekstDef" style="">
                                            <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                <tr>
                                                    <td align="left" class="podnaslov">
                                                        Office&nbsp;Name
                                                    </td>
                                                    <td align="left">
                                                        <ajaxToolkit:ComboBox ID="DDListOfficeName" runat="server" Width="255px" DropDownStyle="DropDownList"
                                                            AutoCompleteMode="SuggestAppend" CssClass="WindowsStyle" DataValueField="UID"
                                                            DataTextField="Name" AutoPostBack="true" CaseSensitive="false" EnableViewState="true"
                                                            OnSelectedIndexChanged="DropDownList1_OnSelectedIndexChanged" AppendDataBoundItems="false">
                                                        </ajaxToolkit:ComboBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="podnaslov">
                                                        Box Name
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="textBoxName" name="textBoxName" runat="server" class="form_tekst_field"
                                                            size="50" MaxLength="23" />
                                                        <asp:RequiredFieldValidator runat="server" ID="telN" ControlToValidate="textBoxName"
                                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The Box Name is required!" />
                                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                                            TargetControlID="telN" HighlightCssClass="validatorCalloutHighlight" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" class="podnaslov">
                                                        Box Number
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="textBoxNumber" name="textBoxNumber" runat="server" class="form_tekst_field"
                                                            size="20" MaxLength="15" ReadOnly="true" /><asp:Image  id="image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="" />
                                                                                                    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="2" class="podnaslov">
                                                        Please provide complete File information (Last Name, First Name, File#, File Type)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <ajaxToolkit:TabContainer runat="server" ID="fileTabs" Height="630px" ActiveTabIndex="0"
                                                            Width="630px">
                                                            <ajaxToolkit:TabPanel runat="server" ID="Panel1" HeaderText="File1-10">                                                                                                                               
                                                                <ContentTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>1.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName1" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName1" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile1" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName1" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>2.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName2" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName2" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile2" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName2" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>3.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName3" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName3" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile3" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName3" size="30" MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>4.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName4" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName4" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile4" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName4" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>5.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName5" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName5" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile5" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName5" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>6.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName6" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName6" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile6" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName6" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>7.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName7" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName7" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile7" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName7" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>8.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName8" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName8" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile8" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName8" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>9.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName9" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName9" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile9" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName9" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>10.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName10" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName10" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile10" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName10" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>   
                                                                </ContentTemplate>
                                                            </ajaxToolkit:TabPanel>
                                                            <ajaxToolkit:TabPanel runat="server" ID="Panel2" HeaderText="File11-20">
                                                                <ContentTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>1.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName11" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName11" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile11" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName11" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>2.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName12" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName12" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile12" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName12" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>3.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName13" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName13" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile13" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName13" size="30" MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>4.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName14" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName14" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile14" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName14" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>5.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName15" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName15" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile15" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName15" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>6.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName16" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName16" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile16" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName16" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>7.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName17" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName17" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile17" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName17" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>8.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName18" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName18" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile18" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName18" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>9.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName19" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName19" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile19" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName19" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>10.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName20" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName20" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile20" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName20" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </ajaxToolkit:TabPanel>
                                                            <ajaxToolkit:TabPanel runat="server" ID="Panel3" HeaderText="File21-30">
                                                                <ContentTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>1.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName21" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName21" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile21" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName21" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>2.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName22" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName22" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile22" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName22" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>3.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName23" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName23" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile23" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName23" size="30" MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>4.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName24" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName24" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile24" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName24" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>5.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName25" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName25" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile25" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName25" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>6.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName26" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName26" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile26" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName26" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>7.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName27" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName27" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile27" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName27" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>8.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName28" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName28" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile28" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName28" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>9.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName29" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName29" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile29" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName29" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>10.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName30" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName30" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile30" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName30" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </ajaxToolkit:TabPanel>
                                                            <ajaxToolkit:TabPanel runat="server" ID="Panel4" HeaderText="File31-40">
                                                                <ContentTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>1.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName31" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName31" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile31" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName31" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>2.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName32" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName32" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile32" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName32" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>3.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName33" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName33" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile33" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName33" size="30" MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>4.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName34" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName34" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile34" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName34" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>5.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName35" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName35" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile35" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName35" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>6.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName36" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName36" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile36" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName36" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>7.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName37" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName37" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile37" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName37" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>8.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName38" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName38" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile38" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName38" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>9.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName39" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName39" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile39" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName39" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>10.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName40" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName40" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile40" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName40" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </ajaxToolkit:TabPanel>
                                                             <ajaxToolkit:TabPanel runat="server" ID="Panel5" HeaderText="File41-50">
                                                                <ContentTemplate>
                                                                    <table>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>1.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName41" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName41" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile41" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName41" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>2.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName42" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName42" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile42" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName42" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>3.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName43" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName43" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile43" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName43" size="30" MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>4.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName44" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName44" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile44" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName44" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>5.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName45" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName45" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile45" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName45" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>6.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName46" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName46" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile46" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName46" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>7.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName47" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName47" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile47" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName47" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>8.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName48" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName48" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile48" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName48" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>9.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName49" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName49" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile49" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName49" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>10.&nbsp;First Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFirstName50" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;Last Name </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textLastName50" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;File #&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><asp:TextBox
                                                                                    runat="server" class="form_tekst_field" ID="textFile50" size="30" MaxLength="30" />
                                                                            </td>
                                                                            <td align="left" class="podnaslov">
                                                                                <span>&nbsp;&nbsp;&nbsp;File Type </span>
                                                                                <asp:TextBox runat="server" class="form_tekst_field" ID="textFileName50" size="30"
                                                                                    MaxLength="30" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </ContentTemplate>
                                                            </ajaxToolkit:TabPanel>
                                                            &nbsp;
                                                        </ajaxToolkit:TabContainer>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div align="center" style="height: 30px;">
                                            <asp:Button runat="server" name="imageField" ID="imageField" OnClick="imageField_Click"
                                                Text="Submit" />
                                        </div>
                                        <div class="podnaslov">
                                            <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="imageField" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                        </td>
                        <td width="10" align="center">
                            <img src="../images/lin.gif" width="1" height="453" vspace="10" />
                        </td>
                        <td valign="top" background="images/bg_main.gif" bgcolor="#f6f5f2" style="background-repeat: repeat-x;">
                            <uc1:QuickFind ID="QuickFind1" runat="server" />
                            <marquee scrollamount="1" id="marqueeside"  runat="server"
                                behavior="scroll" onmouseover="this.stop()" onmouseout="this.start()" scrolldelay="20"
                                direction="up" >
                                <%=MessageBody%></marquee>
                            <uc2:FileRequestControl ID="fileRequest1" runat="server" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Button1ShowPopup" runat="server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" PopupControlID="pnlPopup"
                OnOkScript="anotherRequest()" CancelControlID="btnClose" OnCancelScript="closePage()"
                OkControlID="btnRequset" DropShadow="true" BackgroundCssClass="modalBackground"
                TargetControlID="Button1ShowPopup" />
            <!-- ModalPopup Panel-->
            <asp:Panel ID="pnlPopup" runat="server" Width="450px" Style="display: none; background-color: white">
                <div style="width: 100%; background-color: #C0C0C0">
                    <strong>File Request</strong></div>
                <div class="tekstDef">
                    <table width="100%" border="0" cellpadding="2" cellspacing="0">
                        <tr>
                            <td align="left" class="podnaslov">
                                <strong>Hello&nbsp;<asp:Label ID="LabelUserName" Text="" runat="server" />,</strong>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="left" class="podnaslov">
                                Your request: {<strong><asp:Label ID="LabelResult" Text="" runat="server" /></strong>
                                }&nbsp;&nbsp;has been submitted.
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="podnaslov">
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="height: 10px">
                    &nbsp;&nbsp;</div>
                <div align="center" style="width: 100%; background-color: white">
                    <asp:Button ID="btnRequset" runat="server" Text="Request Another" Width="120px" />
                    <asp:Button ID="btnClose" runat="server" Text="Close" Width="120px" />
                </div>
                s
                <div style="height: 10px">
                    &nbsp;&nbsp;</div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="imageField" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <script language="javascript" type="text/javascript">
        function anotherRequest() {
            window.location = window.location;

        }

        function closePage() {
            window.location = "http://www.edfiles.com/Office/Welcome.aspx";
        }
    </script>
</asp:Content>
