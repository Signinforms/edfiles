using System;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;

using DataQuicker2.Framework;
using Shinetech.DAL;
using Shinetech.Utility;
using Account=Shinetech.DAL.Account;
using BarcodeLib;

public partial class CreateNewBox : LicensePage
{
    

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            BarcodeLib.Barcode b = new BarcodeLib.Barcode();
            b.IncludeLabel = false;
           
            b.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
            b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;
            BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39Extended;
            textBoxNumber.Text = Guid.NewGuid().ToString("N").Substring(0,15).ToUpper();
            b.Encode(type, textBoxNumber.Text,
                Color.Black, Color.Bisque, 400, 25);

            image1.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(b.GetImageData(SaveTypes.JPG));
            
            DDListOfficeName.DataSource = UserManagement.GetOfficeUsers();
            DDListOfficeName.DataBind();
        }
        
    }


    public string MessageBody
    {
        get
        {
            return this.Session["_Side_MessageBody"] as string;
        }

        set
        {
            this.Session["_Side_MessageBody"] = value;
        }
    }

    protected void imageField_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
           
            //if (!this.IsFileValid()) return;

            Box box = new Box();
            box.BoxName.Value = textBoxName.Text.Trim();
            box.BoxNumber.Value = textBoxNumber.Text.Trim();
            box.UserId.Value = DDListOfficeName.SelectedValue;

            box.CreateDate.Value = DateTime.Now;

            if (ExistBox(box.BoxName.Value, box.UserId.Value))
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "This Box Name existed already." + "\");", true);
                return;
            }

            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            IDbTransaction transaction = connection.BeginTransaction();
            try
            {
                ArrayList items = new ArrayList();
                int tabIndex = 0;
                for(int i=1;i<=50;i++)
                {
                    tabIndex = ((int)Math.Ceiling(i/10f)) - 1;
                    //create paper file
                    PaperFile file1 = new PaperFile();
                    file1.FirstName.Value = ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFirstName" + i)).Text.Trim();
                    file1.LastName.Value = ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textLastName" + i)).Text.Trim();
                    file1.FileNumber.Value = ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFile" + i)).Text.Trim();
                    file1.FileName.Value = ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFileName" + i)).Text.Trim();
                    file1.UserID.Value = box.UserId.Value;
                    file1.FileIndex.Value = i;

                    items.Add(file1);
                }
                
                box.Create(transaction);

                foreach (PaperFile file in items)
                {
                    file.BoxId.Value = box.BoxId.Value;
                    file.Create(transaction);
                }

                transaction.Commit();

               // string strWrongInfor = "Your request has been submited.";
                //LabelUserName.Text = Membership.GetUser().UserName;
                //LabelResult.Text = form.FormId.ToString();
                //mdlPopup.Show();
              
                //sendeFileFormEmail(form, items);
                
               ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Created Box Successfully" + "\");", true);
            }
            catch (Exception ex)
            {
                transaction.Rollback();
              
                string strWrongInfor = string.Format("Error : {0}",ex.Message);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
            } 
        }
                
    }

    public bool ExistBox(string name,string uid)
    {
        return FileFolderManagement.ExistBox(name, uid);
    }


    public bool IsFileValid()
    {
        if (textFirstName1.Text.Trim() == "" && textFirstName2.Text.Trim() == "" && textFirstName3.Text.Trim() == ""
                && textFirstName4.Text.Trim() == "" && textFirstName5.Text.Trim() == "")
        {
            string strWrongInfor = string.Format("Error : You have to insert one file at least.");
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
            return false;
        }

        if (textFirstName1.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName1.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 1th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }

        if (textFirstName2.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName2.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 2th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }

        if (textFirstName3.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName3.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 3th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }

        if (textFirstName4.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName3.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 4th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }

        if (textFirstName5.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName5.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 4th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }


        return true;
    }

    protected void DropDownList1_OnSelectedIndexChanged(object sender, EventArgs e)
    {
       
    }

    private void sendeFileFormEmail(FileForm form,ArrayList items)
    {
        string strMailTemplet = getFileFormMailTemplate();

        strMailTemplet = strMailTemplet.Replace("[textOfficeName]", form.OfficeName.Value);
        strMailTemplet = strMailTemplet.Replace("[textUserName]", Sessions.SwitchedUserName);
        //strMailTemplet = strMailTemplet.Replace("[textUserName]", Membership.GetUser().UserName);
        strMailTemplet = strMailTemplet.Replace("[textPhone]", form.PhoneNumber.Value);
        strMailTemplet = strMailTemplet.Replace("[textRequestBy]", form.RequestedBy.Value);
        strMailTemplet = strMailTemplet.Replace("[textRequestDate]", form.RequestDate.Value.ToShortDateString());
        strMailTemplet = strMailTemplet.Replace("[textOfficeName]", form.OfficeName.Value);

        strMailTemplet = strMailTemplet.Replace("[textComment]", form.Comment.Value);

        int count = items.Count;
        int delta = 5 - count;
        
        FormFile file;

        for(int i=0;i<count;i++)
        {
            file = items[i] as FormFile;
            strMailTemplet = strMailTemplet.Replace("[textFirstName" + (i+1) + "]", file.FirstName.Value);
            strMailTemplet = strMailTemplet.Replace("[textLastName" + (i + 1) + "]", file.LastName.Value);
            strMailTemplet = strMailTemplet.Replace("[textFile" + (i + 1) + "]", file.FileNumber.Value);
            strMailTemplet = strMailTemplet.Replace("[textYear" + (i + 1) + "]", file.YearDate.Value);

        }

        for (int i =5; i > count; i--)
        {
            strMailTemplet = strMailTemplet.Replace("[textFirstName" + i + "]", "");
            strMailTemplet = strMailTemplet.Replace("[textLastName" + i + "]", "");
            strMailTemplet = strMailTemplet.Replace("[textFile" + i + "]", "");
            strMailTemplet = strMailTemplet.Replace("[textYear" + i + "]", "");

        }
       
        string strEmailSubject = "File Form Request";

        //Need to ask
        Email.SendFileRequestService(Membership.GetUser().Email,strEmailSubject, strMailTemplet);
 
    }


    private string getFileFormMailTemplate()
    {

        string strMailTemplet = string.Empty;
        string physicalPath = Request.PhysicalPath;
        int index = physicalPath.LastIndexOf("\\");
        string mailTempletUrl = physicalPath.Substring(0, index + 1) + "FileForm.html";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    protected void btnClose_OnClick(object sender, EventArgs e)
    {
        Page.Response.Redirect("~/Office/Welcome.aspx");
    }

    protected void anotherRequest_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect("~/Office/CreateFileRequest.aspx");
    }
}
