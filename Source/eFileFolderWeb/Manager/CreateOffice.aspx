<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    CodeFile="CreateOffice.aspx.cs" Inherits="CreateOffice" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style>
        #comboBoxDepartments_chosen
        {
            display: block;
            margin-top: 5px;
        }

        .chosen-results
        {
            max-height: 200px !important;
        }

        .chosen-container
        {
            font-size: 15px;
        }

            .chosen-container .chosen-choices
            {
                max-height: 100px;
                overflow: auto;
            }
    </style>
    <script type="text/javascript">
        function pageLoad() {
            LoadDepartments();
        }

        function CreateOffice() {
            var depsList = $('#comboBoxDepartments_chosen .chosen-choices').children();
            var departments = [];
            if (depsList.length > 1) {
                for (var i = 0; i < depsList.length - 1; i++) {
                    if ($(depsList[i])) {
                        departments.push({ 'DepartmentId': $($('#comboBoxDepartments').find('option')[$(depsList[i]).find('a').attr('data-option-array-index')]).val() });
                    }
                }
            }

            $("#" + '<%=hdnDepartmentIds.ClientID %>').val(JSON.stringify(departments));
        }

        function LoadDepartments() {
            $("#comboBoxDepartments").html('');
            $("#comboBoxDepartments").chosen("destroy");
            $.ajax(
             {
                 type: "POST",
                 url: '../FlexWebService.asmx/GetDepartmentsForAdmin',
                 contentType: "application/json; charset=utf-8",
                 dataType: "json",
                 success: function (result) {
                     var offices = [];
                     if (result.d) {
                         $.each(result.d, function (i, text) {
                             $('<option />', { value: i, text: text }).appendTo($("#comboBoxDepartments"));
                         });
                     }
                     $("#comboBoxDepartments").chosen({ width: "315px" });
                     $("#comboBoxDepartments").val(offices).trigger("chosen:updated");
                     $chosen = $("#comboBoxDepartments").chosen();
                     var chosen = $chosen.data("chosen");
                     var _fn = chosen.result_select;
                     chosen.result_select = function (evt) {
                         evt["metaKey"] = true;
                         evt["ctrlKey"] = true;
                         chosen.result_highlight.addClass("result-selected");
                         return _fn.call(chosen, evt);
                     };
                 },
                 fail: function (data) {
                     alert("Failed to get Departments. Please try again!");
                 }
             });
        }
    </script>
    <table class="left" width="100%">
        <tr>
            <td width="970" height="453" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                style="background-repeat: repeat-x;">
                <div style="background: url(../images/inner_b.gif); background-position: right top; background-repeat: no-repeat; margin-top: 10px;">
                    <div style="width: 725px; height: 60px;">
                        <div class="Naslov_Crven">
                            Create Users
                        </div>
                    </div>
                    <div class="podnaslov" style="padding: 5px; background-color: #f6f5f2;">
                    </div>

                    <asp:UpdatePanel ID="UpdatePanelUser" runat="server">
                        <ContentTemplate>
                            <div class="tekstDef" style="">
                                <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                    <tr>
                                        <td width="7%" align="left" class="podnaslov">Username&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td width="1%">&nbsp;</td>
                                        <td width="30%">
                                            <asp:TextBox name="textfield3" runat="server" class="form_tekst_field" ID="textfield3"
                                                size="30" MaxLength="120" ControlToValidate="textfield3" />
                                            <asp:RequiredFieldValidator runat="server" ID="userNameV" ControlToValidate="textfield3"
                                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />The UserName is required!" />
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="uesrNameValidate" TargetControlID="userNameV"
                                                HighlightCssClass="validatorCalloutHighlight" />
                                        </td>
                                        <td width="67%" rowspan="4" valign="middle">
                                            <div class="tekstDef">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="podnaslov">Password</td>
                                        <td align="right" class="podnaslov">&nbsp;</td>
                                        <td>
                                            <asp:TextBox TextMode="password" name="textfield4" runat="server" class="form_tekst_field"
                                                ID="textfield4" size="30" MaxLength="20" />
                                            <asp:RequiredFieldValidator runat="server" ID="passwordV" ControlToValidate="textfield4"
                                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />The password is required!" />
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
                                                TargetControlID="passwordV" HighlightCssClass="validatorCalloutHighlight" />
                                            <asp:RegularExpressionValidator ID="passwordN" runat="server" ControlToValidate="textfield4"
                                                ErrorMessage="<b>Required Field Missing</b><br />The password must be more than 6 numbers!"
                                                ValidationExpression="^\w{7,20}$" Display="None"></asp:RegularExpressionValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
                                                TargetControlID="passwordN" HighlightCssClass="validatorCalloutHighlight" />
                                        </td>
                                    </tr>
                                    <%--<tr>
                                <td align="left" class="podnaslov">
                                    DOB</td>
                                <td align="right" class="podnaslov">
                                    &nbsp;</td>
                                <td>
                                    <asp:TextBox name="textfield5" runat="server" class="form_tekst_field" ID="textfield5"
                                        size="30" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" PopupPosition="Right" runat="server"
                                        TargetControlID="textfield5" Format="MM-dd-yyyy">
                                    </ajaxToolkit:CalendarExtender>
                                </td>
                                <asp:RequiredFieldValidator runat="server" ID="DobV" ControlToValidate="textfield5"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The DOB is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5"
                                    TargetControlID="DobV" HighlightCssClass="validatorCalloutHighlight" />
                               <asp:RegularExpressionValidator ID="DobC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The DOB is not a date"
                                    ControlToValidate="textfield5" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                    Visible="true" Display="None"></asp:RegularExpressionValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6"
                                    TargetControlID="DobC" HighlightCssClass="validatorCalloutHighlight" />
                            </tr>--%>
                                </table>
                            </div>
                            <div style="padding-left: 30px;">
                                <img src="../images/dotted.gif" vspace="5" />
                            </div>
                            <%-- <div class="form_title_1">
                    Form part no. 2</div>--%>
                            <div class="tekstDef">
                                <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 20px 10px;">
                                    <tr>
                                        <td align="left" class="podnaslov">First&nbsp;Name</td>
                                        <td width="30%">
                                            <asp:TextBox ID="textfield6" name="textfield6" runat="server" class="form_tekst_field"
                                                size="30" MaxLength="20" />
                                            <asp:RequiredFieldValidator runat="server" ID="firstNameV" ControlToValidate="textfield6"
                                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />The first name is required!" />
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                                TargetControlID="firstNameV" HighlightCssClass="validatorCalloutHighlight" />
                                        </td>
                                        <td width="67%" rowspan="4" valign="middle">
                                            <div class="tekstDef">
                                                <%--  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam enim risus, scelerisque
                                        eu, laoreet vel, rhoncus vitae, sem. Nulla et orci. Curabitur sit amet libero ut
                                        nisi vehicula mattis. Mauris porttitor ligula eu sapien. Etiam diam risus, ornare
                                        eget, gravida bibendum, placerat quis, ante. Etiam nisi nulla, cursus nec, pretium
                                        at,--%>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="podnaslov">Last&nbsp;Name</td>
                                        <td>
                                            <asp:TextBox ID="textfield7" runat="server" class="form_tekst_field" size="30" MaxLength="20" />
                                            <asp:RequiredFieldValidator runat="server" ID="lastNameV" ControlToValidate="textfield7"
                                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />The last name is required!" />
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10"
                                                TargetControlID="lastNameV" HighlightCssClass="validatorCalloutHighlight" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="podnaslov">Email</td>
                                        <td>
                                            <asp:TextBox ID="textfield13" runat="server" class="form_tekst_field" size="30" MaxLength="50" />
                                            <asp:RequiredFieldValidator runat="server" ID="EmailReq" ControlToValidate="textfield13"
                                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />An email is required." />
                                            <asp:RegularExpressionValidator ID="rev_Email" runat="server" ErrorMessage="<b>Required Field Missing</b><br />It is not an email format."
                                                ControlToValidate="textfield13" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                Visible="true" Display="None"></asp:RegularExpressionValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="EmailReqE" TargetControlID="EmailReq"
                                                HighlightCssClass="validatorCalloutHighlight" />
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                                TargetControlID="rev_Email" HighlightCssClass="validatorCalloutHighlight" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="podnaslov">Telephone</td>
                                        <td>
                                            <asp:TextBox ID="textfield8" name="textfield8" runat="server" class="form_tekst_field"
                                                size="30" MaxLength="23" />
                                            <asp:RequiredFieldValidator runat="server" ID="telN" ControlToValidate="textfield8"
                                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />The telephone is required!" />
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11"
                                                TargetControlID="telN" HighlightCssClass="validatorCalloutHighlight" />
                                            <asp:RegularExpressionValidator ID="TelV" ControlToValidate="textfield8" runat="server"
                                                ErrorMessage="<b>Required Field Missing</b><br />It is not a telphone format."
                                                ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                                TargetControlID="TelV" HighlightCssClass="validatorCalloutHighlight" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="podnaslov">Mobile</td>
                                        <td>
                                            <asp:TextBox name="textfield11" runat="server" class="form_tekst_field" ID="textfield11"
                                                size="30" MaxLength="20" />
                                            <asp:RegularExpressionValidator ID="mobileV" ControlToValidate="textfield11" runat="server"
                                                ErrorMessage="<b>Required Field Missing</b><br />It is not a Mobile format."
                                                ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9"
                                                TargetControlID="mobileV" HighlightCssClass="validatorCalloutHighlight" />
                                            <%--                                    <asp:RequiredFieldValidator runat="server" ID="mobileN" ControlToValidate="textfield11"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The mobile is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender12"
                                        TargetControlID="mobileN" HighlightCssClass="validatorCalloutHighlight" />--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="podnaslov">Fax</td>
                                        <td>
                                            <asp:TextBox name="textfield9" type="text" runat="server" class="form_tekst_field"
                                                ID="textfield9" size="30" MaxLength="23" />
                                            <asp:RegularExpressionValidator ID="faxV" ControlToValidate="textfield8" runat="server"
                                                ErrorMessage="<b>Required Field Missing</b><br />It is not a FAX format." ValidationExpression="[^A-Za-z]{7,}"
                                                Display="None"></asp:RegularExpressionValidator>
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8"
                                                TargetControlID="faxV" HighlightCssClass="validatorCalloutHighlight" />
                                            <%--<asp:RequiredFieldValidator runat="server" ID="FaxN" ControlToValidate="textfield9"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The mobile is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender13"
                                        TargetControlID="FaxN" HighlightCssClass="validatorCalloutHighlight" />--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="podnaslov">Organization</td>
                                        <td>
                                            <asp:TextBox name="textfield14" runat="server" class="form_tekst_field" ID="textfield14"
                                                size="40" MaxLength="50" />
                                            <asp:RequiredFieldValidator runat="server" ID="companyNameN" ControlToValidate="textfield14"
                                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />The company name is required!" />
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14"
                                                TargetControlID="companyNameN" HighlightCssClass="validatorCalloutHighlight" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="podnaslov">Address</td>
                                        <td>
                                            <asp:TextBox name="textfield10" runat="server" class="form_tekst_field" ID="textfield10"
                                                size="40" MaxLength="200" />
                                            <asp:RequiredFieldValidator runat="server" ID="addressN" ControlToValidate="textfield10"
                                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />The address is required!" />
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15"
                                                TargetControlID="addressN" HighlightCssClass="validatorCalloutHighlight" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="podnaslov">City</td>
                                        <td>
                                            <asp:TextBox runat="server" class="form_tekst_field" ID="txtCity" size="30" MaxLength="30" /></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="podnaslov">State/Province</td>
                                        <td>
                                            <asp:DropDownList ID="drpState" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td align="right" class="podnaslov">Other
                                        </td>
                                        <td>
                                            <asp:TextBox runat="server" class="form_tekst_field" ID="txtOtherState" size="30"
                                                MaxLength="30" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="podnaslov">Country</td>
                                        <td>
                                            <asp:DropDownList ID="drpCountry" runat="server" /></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="podnaslov">Zip/Postal</td>
                                        <td>
                                            <asp:TextBox ID="txtPostal" runat="server" class="form_tekst_field" size="30" MaxLength="30" />
                                            <asp:RequiredFieldValidator runat="server" ID="txtPostalN" ControlToValidate="txtPostal"
                                                Display="None" ErrorMessage="<b>Required Field Missing</b><br />The postal is required!" />
                                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17"
                                                TargetControlID="txtPostalN" HighlightCssClass="validatorCalloutHighlight" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="podnaslov">Departments</td>
                                        <td>
                                            <select id="comboBoxDepartments" multiple style="color: rgb(68,68,68); border-radius: 5px; background-color: #fff; border: 1px solid #aaa; box-shadow: 0 1px 0 #fff inset; background: linear-gradient(#fff 20%, #f6f6f6 50%, #eee 52%, #f4f4f4 100%); max-height: 100px;">
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="podnaslov">Maximal EdFiles</td>
                                        <td>
                                            <asp:CheckBox runat="server" ID="chkMaxCount"></asp:CheckBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div style="padding-left: 30px;">
                        <img src="../images/dotted.gif" vspace="5" />
                    </div>
                    <div class="podnaslov">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <%--<contenttemplate>
                                <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                            </contenttemplate>--%>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div align="center" style="height: 30px;">
                        <asp:Button ID="ImageButton1" runat="server" Text="Create" OnClientClick="CreateOffice();" OnClick="ImageButton1_Click" />
                        <asp:HiddenField ID="hdnDepartmentIds" runat="server" />
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
