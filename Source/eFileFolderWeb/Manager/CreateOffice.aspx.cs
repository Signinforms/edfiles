using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Utility;
using Shinetech.DAL;
using Account = Shinetech.DAL.Account;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;

public partial class CreateOffice : BasePage
{

    public string strRandom = string.Empty;
    private static readonly string PARTNEREMAIL = ConfigurationManager.AppSettings["PARTNEREMAIL"];
    public string domain = ConfigurationSettings.AppSettings["MailboxDomain"].ToString();
    public string rights = ConfigurationSettings.AppSettings["MailboxRight"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {

        getRandomStr();
        if (!Page.IsPostBack)
        {
            pageLoad();
        }
    }

    protected void pageLoad()
    {
        drpState.DataSource = States;
        drpState.DataValueField = "StateID";
        drpState.DataTextField = "StateName";
        drpState.DataBind();
        drpState.SelectedValue = "CA";

        drpCountry.DataSource = Countries;
        drpCountry.DataValueField = "CountryID";
        drpCountry.DataTextField = "CountryName";
        drpCountry.DataBind();
        drpCountry.SelectedValue = "US";
    }


    public DataTable States
    {
        get
        {
            object obj = Application["States"];

            DataTable table = null;
            //if (obj == null)
            //{
            table = UserManagement.GetStates();
            Application["States"] = table;
            //}
            //else
            //{
            //    table = obj as DataTable;
            //}

            return table;
        }
    }

    public DataTable Countries
    {
        get
        {
            object obj = Application["Countries"];

            DataTable table = null;
            //if (obj == null)
            //{
            table = UserManagement.GetCountries();
            Application["Countries"] = table;
            //}
            //else
            //{
            //    table = obj as DataTable;
            //}

            return table;
        }
    }

    protected void ImageButton1_Click(object sender, EventArgs e)
    {
        string userName = this.textfield3.Text.Trim();
        string password = PasswordGenerator.GetMD5(this.textfield4.Text.Trim());
        //string createDate = this.textfield5.Text.Trim();

        string email = this.textfield13.Text.Trim();
        string firstname = this.textfield6.Text.Trim();
        string lastname = this.textfield7.Text.Trim();

        string telephone = this.textfield8.Text.Trim();
        string mobile = this.textfield11.Text.Trim();
        string fax = this.textfield9.Text.Trim();
        string companyName = this.textfield14.Text.Trim();
        string address = this.textfield10.Text.Trim();

        string strCountryID = this.drpCountry.SelectedValue;
        string strState = this.drpState.SelectedValue;
        string strOtherState = this.txtOtherState.Text.Trim();
        string strCity = this.txtCity.Text.Trim();
        string strPostal = this.txtPostal.Text.Trim();

        bool maxNumber = this.chkMaxCount.Checked;

        getRandomStr();

        if (Membership.GetUser(userName) == null)
        {
            try
            {

                MembershipUser user = Membership.CreateUser(userName, password);
                user.Email = email;
                user.IsApproved = true;
                Roles.AddUserToRole(userName, "Offices");
                Membership.UpdateUser(user);


                try
                {
                    #region Create user
                    Account webUser = new Account(user.ProviderUserKey.ToString());
                    webUser.Name.Value = user.UserName;
                    webUser.ApplicationName.Value = Membership.ApplicationName;
                    webUser.IsSubUser.Value = "0";
                    //webUser.DOB.Value = DateTime.ParseExact(createDate, "mm-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    webUser.Firstname.Value = firstname;
                    webUser.Lastname.Value = lastname;
                    webUser.Email.Value = email;

                    webUser.Telephone.Value = telephone;
                    webUser.MobilePhone.Value = mobile;
                    webUser.FaxNumber.Value = fax;
                    webUser.CompanyName.Value = companyName;
                    webUser.Address.Value = address;

                    webUser.CountryID.Value = strCountryID;
                    webUser.StateID.Value = strState;
                    webUser.OtherState.Value = strOtherState;
                    webUser.City.Value = strCity;
                    webUser.Postal.Value = strPostal;
                    webUser.LicenseID.Value = 200;
                    webUser.RoleID.Value = "Offices";

                    if (maxNumber)
                    {
                        webUser.FileFolders.Value = 9999;
                    }
                    UserManagement.CreatNewAccount(webUser);

                    this.UserID = webUser.UID.ToString();
                    //#region Create Mailox
                    //if (UserID != null)
                    //{
                    //    try
                    //    {
                    //        int created = AddMailbox(this.textfield4.Text.ToString(), email);
                    //        if (created == 1)
                    //        {
                    //            General_Class.UpdateOrCreateMailBox(email, this.textfield4.Text.ToString(), UserID);
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        Membership.DeleteUser(user.UserName);
                    //        string strWrongInfor = String.Format("Error : failed to create mail box for {0} , please try again.", this.textfield3.Text);
                    //        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                    //        return;
                    //    }

                    //}
                    //#endregion
                    #endregion

                    #region AssignDepartment

                    try
                    {
                        if (!string.IsNullOrEmpty(this.hdnDepartmentIds.Value))
                        {
                            DataTable dtDeps = new DataTable();
                            dtDeps.Columns.Add("Index", typeof(int));
                            dtDeps.Columns.Add("DepartmentId", typeof(int));
                            AssignedDepartment[] userData = JsonConvert.DeserializeObject<List<AssignedDepartment>>(hdnDepartmentIds.Value).ToArray();
                            int index = 1;
                            foreach (var i in userData)
                            {
                                if (i.DepartmentId > 0)
                                {
                                    dtDeps.Rows.Add(new object[] { index, i.DepartmentId });
                                    index++;
                                }
                            }
                            General_Class.InsertIntoUserDepartments(dtDeps, new Guid(this.UserID));
                        }
                    }
                    catch (Exception ex) { }

                    #endregion
                }
                catch (Exception ex)
                {
                    Membership.DeleteUser(user.UserName);
                    string strWrongInfor = String.Format("Error : failed to create the {0} office, please try again.", this.textfield3.Text);
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                    // lblMessage.Text = String.Format("Error : failed to create the {0} office, please try again.", this.textfield3.Text);
                    return;
                }

                try
                {
                    VerificationNumber vn = new VerificationNumber(this.UserID);
                    vn.RandomNumber.Value = strRandom;
                    vn.UserName.Value = userName;
                    vn.InDate.Value = DateTime.Now;
                    UserManagement.CreatVerificationNumber(vn);

                    string strWrongInfor = String.Format("Message : create {0} office successfully.", this.textfield3.Text);
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);

                    sendEmailToPartner();

                    SetTextBoxNull();
                    //lblMessage.Text = String.Format("Message : create {0} office successfully.", this.textfield3.Text);
                }
                catch
                { }


            }
            catch
            {

                string strWrongInfor = String.Format("Error : failed to create the {0} office, please try again.", this.textfield3.Text);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                //lblMessage.Text = String.Format("Error : failed to create the {0} office, please try again.", this.textfield3.Text);
                return;
            }
        }
        else
        {
            string strWrongInfor = String.Format("Error : {0} office alreadt exits, please input again.", this.textfield3.Text);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);

            //lblMessage.Text = String.Format("Error : {0} office alreadt exits, please input again.", this.textfield3.Text);
            return;
        }


    }

    public int AddMailbox(string password, string Email)
    {
        MailEnable.Administration.Mailbox oMailbox = new MailEnable.Administration.Mailbox();
        MailEnable.Administration.Login oLogin = new MailEnable.Administration.Login();
        string sMailboxName = Email.Split('@')[0];
        oLogin.Account = domain;
        oLogin.LastAttempt = -1;
        oLogin.LastSuccessfulLogin = -1;
        oLogin.LoginAttempts = -1;
        oLogin.Password = "";
        oLogin.Rights = "";
        oLogin.Status = -1;
        oLogin.UserName = Email;
        //  If the login does not exist we need to create it
        if ((oLogin.GetLogin() == 0))
        {
            oLogin.Account = domain;
            oLogin.LastAttempt = 0;
            oLogin.LastSuccessfulLogin = 0;
            oLogin.LoginAttempts = 0;
            oLogin.Password = password;
            oLogin.Rights = rights;
            oLogin.Status = 1;
            //  0  Disabled, 1  Enabled
            oLogin.UserName = Email;
            if ((oLogin.AddLogin() != 1))
            {
                //  Error adding the Login
                return 0;
            }

        }
        //  Now we create the mailbox
        oMailbox.Postoffice = domain;
        oMailbox.MailboxName = sMailboxName;
        //  Set the Redirect Address if applicable
        // oMailbox.RedirectStatus = 1
        // oMailbox.RedirectAddress = sRedirectAddress
        oMailbox.Size = 0;
        oMailbox.Limit = -1;
        //  -1  Unlimited OR size value (in KB)
        oMailbox.Status = 1;
        if ((oMailbox.AddMailbox() != 1))
        {
            //  Failed to add mailbox
            return 0;
        }

        //  Now we need to add the Address Map entries for the Account
        MailEnable.Administration.AddressMap oAddressMap = new MailEnable.Administration.AddressMap();
        oAddressMap.Account = domain;
        oAddressMap.DestinationAddress = ("[SF:"
                    + (domain + ("/"
                    + (sMailboxName + "]"))));
        oAddressMap.SourceAddress = ("[SMTP:"
                    + Email + "]");
        oAddressMap.Scope = "0";
        if ((oAddressMap.AddAddressMap() != 1))
        {
            //  Failed to add Address Map for some reason!
            return 2;
        }
        return 1;
    }

    public string UserID
    {
        get
        {
            return this.Session["UserID"] as string;
        }
        set
        {
            this.Session["UserID"] = value;
        }
    }

    private string getRandomStr()
    {
        strRandom = System.Guid.NewGuid().ToString().Substring(0, 8);//随机生成8位即包含字符又包含数字的字符串
        return strRandom;
    }

    private void SetTextBoxNull()
    {

        this.textfield3.Text = "";
        this.textfield4.Text = "";
        //this.textfield5.Text = "";

        this.textfield13.Text = "";
        this.textfield6.Text = "";
        this.textfield7.Text = "";

        this.textfield8.Text = "";
        this.textfield11.Text = "";
        this.textfield9.Text = "";
        this.textfield14.Text = "";
        this.textfield10.Text = "";

        this.drpState.SelectedValue = "CA";
        this.drpCountry.SelectedValue = "US";
        this.txtOtherState.Text = "";
        this.txtCity.Text = "";
        this.txtPostal.Text = "";
    }

    private void sendEmailToPartner()
    {
        string strMailTemplet = getMailTempletStrToPartner();
        strMailTemplet = strMailTemplet.Replace("[[UserName]]", this.textfield3.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[[FirstName]]", this.textfield6.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[[LastName]]", this.textfield7.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[[Email]]", this.textfield13.Text.Trim());


        string strEmailSubject = "New Registration Notification";

        Email.SendFromEFileFolder(PARTNEREMAIL, strEmailSubject, strMailTemplet);
    }

    private string getMailTempletStrToPartner()
    {
        string strMailTemplet = string.Empty;
        string physicalPath = Request.PhysicalPath;
        int index = physicalPath.LastIndexOf("\\");
        string mailTempletUrl = physicalPath.Substring(0, index + 1) + "../WebUser/MailToPartner.htm";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

}
