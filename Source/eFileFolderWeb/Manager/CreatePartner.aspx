<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true" CodeFile="CreatePartner.aspx.cs" Inherits="Manager_CreatePartner" Title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table>
        <tr>
            <td width="970" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                style="background-repeat: repeat-x;">
                <div style="background: url(images/inner_b.gif); background-position: right top;
                    background-repeat: no-repeat; margin-top: 10px;">
                    <div style="width: 725px; height: 60px;">
                        <div class="Naslov_Crven">
                            Create Partners</div>
                    </div>
                    <div class="podnaslov" style="padding: 5px; background-color: #f6f5f2;">
                    </div>
                     <asp:UpdatePanel ID="UpdatePanelPartner" runat="server">
                     <contenttemplate>
                      <div class="tekstDef">
                        <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 20px 10px;">
                            <tr>
                                <td align="left" class="podnaslov">
                                    Company&nbsp;Name</td>
                                <td width="30%">
                                    <asp:TextBox ID="textfield6" name="textfield6" runat="server" class="form_tekst_field"
                                        size="30" MaxLength="20" />
                                    <asp:RequiredFieldValidator runat="server" ID="firstNameV" ControlToValidate="textfield6"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The company name is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                        TargetControlID="firstNameV" HighlightCssClass="validatorCalloutHighlight" />
                                </td>
                                <td width="67%" rowspan="4" valign="middle">
                                    <div class="tekstDef">
                                     
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="podnaslov">
                                    Contract&nbsp;Name</td>
                                <td>
                                    <asp:TextBox ID="textfield7" runat="server" class="form_tekst_field" size="30" MaxLength="20" />
                                    <asp:RequiredFieldValidator runat="server" ID="lastNameV" ControlToValidate="textfield7"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The contract name is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10"
                                        TargetControlID="lastNameV" HighlightCssClass="validatorCalloutHighlight" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="podnaslov">
                                    Contract&nbsp;Email</td>
                                <td>
                                    <asp:TextBox ID="textfield13" runat="server" class="form_tekst_field" size="30" MaxLength="50" />
                                    <asp:RequiredFieldValidator runat="server" ID="EmailReq" ControlToValidate="textfield13"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />An email is required." />
                                    <asp:RegularExpressionValidator ID="rev_Email" runat="server" ErrorMessage="<b>Email Format</b><br />It is not an email format."
                                        ControlToValidate="textfield13" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        Visible="true" Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4" TargetControlID="EmailReq"
                                        HighlightCssClass="validatorCalloutHighlight" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5"
                                        TargetControlID="rev_Email" HighlightCssClass="validatorCalloutHighlight" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="podnaslov">
                                    Contract&nbsp;Name2</td>
                                <td>
                                    <asp:TextBox ID="textfield15" runat="server" class="form_tekst_field" size="30" MaxLength="20" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="podnaslov">
                                    Contract&nbsp;Email2</td>
                                <td>
                                    <asp:TextBox ID="textfield14" runat="server" class="form_tekst_field" size="30" MaxLength="50" />
                                    <asp:RegularExpressionValidator ID="rev_Email2" runat="server" ErrorMessage="<b>Email Format</b><br />It is not an email format."
                                        ControlToValidate="textfield14" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        Visible="true" Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                        TargetControlID="rev_Email2" HighlightCssClass="validatorCalloutHighlight" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="podnaslov">
                                    Tel.</td>
                                <td>
                                    <asp:TextBox ID="textfield8" name="textfield8" runat="server" class="form_tekst_field"
                                        size="30" MaxLength="23" />
                                    <asp:RegularExpressionValidator ID="TelV" ControlToValidate="textfield8" runat="server"
                                        ErrorMessage="<b>Required Field Missing</b><br />It is not a telphone format."
                                        ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                        TargetControlID="TelV" HighlightCssClass="validatorCalloutHighlight" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="podnaslov">
                                    Fax</td>
                                <td>
                                    <asp:TextBox name="textfield9" type="text" runat="server" class="form_tekst_field"
                                        ID="textfield9" size="30" MaxLength="23" />
                                    <asp:RegularExpressionValidator ID="faxV" ControlToValidate="textfield9" runat="server"
                                        ErrorMessage="<b>Required Field Missing</b><br />It is not a telphone format."
                                        ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
                                        TargetControlID="faxV" HighlightCssClass="validatorCalloutHighlight" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="podnaslov">
                                    Address</td>
                                <td>
                                    <asp:TextBox name="textfield10" runat="server" class="form_tekst_field" ID="textfield10"
                                        size="40" MaxLength="100" />
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="podnaslov">
                                    WebSite</td>
                                <td>
                                    <asp:TextBox name="textfield16" runat="server" class="form_tekst_field" ID="textfield16"
                                        size="40" MaxLength="50" />
                                </td>
                            </tr>
                        </table>
                    </div>
                     </contenttemplate>
                    <triggers>
                            <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                    </triggers>
                    </asp:UpdatePanel>
                    <div style="padding-left: 30px;">
                        <img src="../images/dotted.gif" vspace="5" /></div>
                    <div class="podnaslov">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div align="center" style="height: 30px;">
                        <asp:Button ID="ImageButton1" runat="server" Text="Create" OnClick="ImageButton1_Click" />
                    </div>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>

