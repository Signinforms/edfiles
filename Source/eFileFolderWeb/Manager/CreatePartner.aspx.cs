using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Utility;

public partial class Manager_CreatePartner : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void ImageButton1_Click(object sender, EventArgs e)
    {

        string companyName = this.textfield6.Text.Trim();
        string contractName = this.textfield7.Text.Trim();
        string contractEmail = this.textfield13.Text.Trim();
        string contractName2 = this.textfield15.Text.Trim();
        string contractEmail2 = this.textfield14.Text.Trim();

        string telno = this.textfield8.Text.Trim();
        string faxno = this.textfield9.Text.Trim();
        string address = this.textfield10.Text.Trim();
        string wesite = this.textfield14.Text.Trim();

        try
        {
            Partner partner = new Partner();
            partner.PartnerID.Value = new FastRandom().NextInt();
            partner.HashCode.Value = FastRandom.getMd5Hash(partner.PartnerID.Value.ToString());
            partner.CompanyName.Value = companyName;
            partner.ContactName.Value = contractName;
            partner.ContactEmail.Value = contractEmail;
            partner.ContactName2.Value = contractName2;
            partner.ContactEmail2.Value = contractEmail2;
            partner.Tel.Value = telno;
            partner.Fax.Value = faxno;
            partner.Address.Value = address;
            partner.Website.Value = wesite;

            UserManagement.CreateNewPartner(partner);

            string strWrongInfor = String.Format("Message : create {0} partner successfully", companyName);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);

            //发送通知邮件给Partner
            SetTextBoxNull();

            sendWelcomeEmail(partner);

        }catch(Exception exp)
        {
            string strWrongInfor = String.Format("Error : failed to create the {0} partner, please try again. :{1}", companyName, exp.Message);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
        }
        
    }

    private void sendWelcomeEmail(Partner partner)
    {
  
        string strMailTemplet = getMailTemplate();

        strMailTemplet = strMailTemplet.Replace("[username]", partner.ContactName.Value);
        strMailTemplet = strMailTemplet.Replace("[companyname]", partner.CompanyName.Value);
        string website = ConfigurationManager.AppSettings["WEBSITE"] + "/PartnerHome.aspx?pid=" + partner.HashCode.ToString();
        strMailTemplet = strMailTemplet.Replace("[linktowebiste]", website);

        string strEmailSubject = "A link to EdFiles ";

        Email.SendPartnerFromEFileFolder(partner.ContactEmail.Value, strEmailSubject, strMailTemplet);

    }

    private string getMailTemplate()
    {

        string strMailTemplet = string.Empty;
        string physicalPath = Request.PhysicalPath;
        int index = physicalPath.LastIndexOf("\\");
        string mailTempletUrl = physicalPath.Substring(0, index + 1) + "MailPartner.htm";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    private void SetTextBoxNull()
    {

        this.textfield16.Text = "";
        this.textfield15.Text = "";
        this.textfield6.Text = "";
        this.textfield7.Text = "";
        this.textfield8.Text = "";
        this.textfield13.Text = "";
        this.textfield9.Text = "";
        this.textfield14.Text = "";
        this.textfield10.Text = "";
    }

}
