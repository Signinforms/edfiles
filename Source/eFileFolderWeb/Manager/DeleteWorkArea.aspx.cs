﻿using Elasticsearch.Net;
using Nest;
using Shinetech.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using PdfSharp.Drawing;

public partial class Manager_DeleteWorkArea : System.Web.UI.Page
{
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
    RackSpaceFileUpload rackspacefileupload = new RackSpaceFileUpload();

    protected void Page_Load(object sender, EventArgs e)
    {
        var argument = Request.Form["__EVENTARGUMENT"];
        if (!Page.IsPostBack)
        {
            GetData();
        }
        else
        {
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
            {
                BindDataGrid();
            }
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["LogForm"] as DataTable;
        }
        set
        {
            this.Session["LogForm"] = value;
        }
    }

    protected void WebPager1_PageIndexChanged(object sender, Shinetech.Framework.Controls.PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.WorkAreaDataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object s, Shinetech.Framework.Controls.PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.WorkAreaDataSource;
        WebPager1.DataBind();
    }

    /// <summary>
    /// Delete workarea files
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnkBtnDelete_Command(object sender, CommandEventArgs e)
    {
        LinkButton btnLink = (LinkButton)sender;
        string[] commandArgs = btnLink.CommandArgument.ToString().Split(new char[] { ';' });
        string workAreaId = commandArgs[0];
        string pathname = commandArgs[1];
        string filename = commandArgs[2];
        try
        {   
            string errorMsg = string.Empty;
            pathname = pathname.Replace("%22", "\"").Replace("%27", "'");
            pathname = pathname.Replace("WorkArea", "workArea");
            string value = eFileFolderJSONWS.DeleteWorkArea(pathname, hdnUserId.Value, Convert.ToInt32(workAreaId), filename);

            if (value == "")
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "File has been deleted successfully." + "\");", true);
            else
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Failed to delete file. Please try again." + "\");", true);
        }
        catch (Exception ex)
        {

        }
        GetData(hdnUserId.Value);
    }

    public void GetData(string uid = null)
    {
        try
        {
            this.WorkAreaDataSource = General_Class.GetWorkAreaByWorkAreaTreeId(0, uid);
            BindDataGrid();
        }
        catch(Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
        }
    }

    private void BindDataGrid()
    {
        GridView1.DataSource = this.WorkAreaDataSource.AsDataView();
        GridView1.DataBind();
        WebPager1.DataSource = this.WorkAreaDataSource;
        WebPager1.DataBind();
    }

    /// <summary>
    /// Get all workarea folders by uid
    /// </summary>
    /// <param name="folderId"></param>
    /// <param name="uid"></param>
    [WebMethod]
    public static void LoadWorkArea(string folderId = null, string uid = null)
    {
        Manager_DeleteWorkArea deleteWorkArea = new Manager_DeleteWorkArea();
        try
        {
            deleteWorkArea.WorkAreaDataSource = null;
            deleteWorkArea.WorkAreaDataSource = General_Class.GetWorkAreaByWorkAreaTreeId(0, uid);
        }
        catch (Exception ex)
        {
            deleteWorkArea.WorkAreaDataSource = new DataTable();
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
        }
    }

    /// <summary>
    /// Rename Workarea file
    /// </summary>
    /// <param name="newFileName"></param>
    /// <param name="oldFileName"></param>
    /// <param name="workAreaId"></param>
    /// <param name="userid"></param>
    /// <param name="filename"></param>
    /// <returns></returns>
    [WebMethod]
    public static int RenameWorkAreaDocument(string newFileName, string oldFileName, string workAreaId, string userid, string filename)
    {
        string errorMsg = string.Empty;
        //Manager_DeleteWorkArea deleteWorkArea = new Manager_DeleteWorkArea();
        RackSpaceFileUpload rackspacefileupload = new RackSpaceFileUpload();
        EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
        newFileName = Common_Tatva.SubStringFilename(newFileName);
        oldFileName = oldFileName.Replace("%22", "\"").Replace("%27", "'");
        oldFileName = oldFileName.Replace("WorkArea", "workArea");
        string oldObjectPath = oldFileName;
        oldFileName = oldObjectPath.Substring(oldObjectPath.LastIndexOf('/') + 1);
        if (oldFileName == newFileName)
            return 1;
        string newPath = rackspacefileupload.RenameObject(ref errorMsg, oldObjectPath, newFileName, Enum_Tatva.Folders.WorkArea, userid);
        if (!string.IsNullOrEmpty(newPath))
        {
            General_Class.UpdateWorkArea(Path.GetFileName(newPath), Convert.ToInt32(workAreaId), 0, Enum_Tatva.IsDelete.No.GetHashCode(), 1, 0, newPath);
            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Rename.GetHashCode(), 0, 0, 0, Convert.ToInt32(workAreaId));
        }
        else
        {
            return 0;
        }

        return 1;
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
            string pathname = drv.Row["PathName"].ToString();
            string workAreaId = drv.Row["WorkAreaId"].ToString();
            string filename = drv.Row["FileName"].ToString();

            TextBox txtDocumentName = (e.Row.FindControl("TextDocumentName") as TextBox);
            LinkButton lnkBtnUpdate = (e.Row.FindControl("lnkBtnUpdate") as LinkButton);
            if (lnkBtnUpdate != null)
            {
                lnkBtnUpdate.Attributes.Add("onclick", "OnSaveRename('" + txtDocumentName.ClientID + "','" + pathname + "','" + workAreaId + "','" + filename + "');");
            }
        }
    }

    public DataTable WorkAreaDataSource
    {
        get
        {
            return this.Session["DataSource_WorkArea"] as DataTable;
        }
        set
        {
            this.Session["DataSource_WorkArea"] = value;
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.WorkAreaDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;
        this.WorkAreaDataSource = Source.ToTable();
        GridView1.DataSource = Source.ToTable();
        GridView1.DataBind();
    }
    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    /// <summary>
    /// Delete selected folder
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnDeleteFolder_Click(object sender, EventArgs e)
    {
        if (this.WorkAreaDataSource.Rows.Count >= 0)
        {
            try
            {
                for (int i = 0; i < this.WorkAreaDataSource.Rows.Count; i++)
                {
                    eFileFolderJSONWS.DeleteWorkArea(this.WorkAreaDataSource.Rows[i]["PathName"].ToString(), hdnUserId.Value, Convert.ToInt32(this.WorkAreaDataSource.Rows[i]["WorkAreaId"]), this.WorkAreaDataSource.Rows[i]["FileName"].ToString());
                }
                int count = General_Class.UpdateWorkAreaTree(Convert.ToInt32(hdnFolderId.Value), Enum_Tatva.IsDelete.Yes.GetHashCode(), 0, null, null);
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteElasticLog(string.Empty, hdnFolderId.Value, string.Empty, ex);
            }
        }
        hdnFolderId.Value = null;
        GetData(hdnUserId.Value);
    }
}