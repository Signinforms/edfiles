﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DepartmentDetails.aspx.cs" Inherits="Manager_DepartmentDetails" MasterPageFile="~/MasterPageOld.master" %>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
    <style type="text/css">
        .Naslov_Protokalev1
        {
            font-size: 22px;
            color: #ff7e00;
            padding: 19px 0 0 30px;
            width: 400px;
        }

        .white_content
        {
            display: none;
            position: fixed;
            top: 20%;
            left: 20%;
            width: 60%;
            height: 50%;
            padding: 16px;
            background-color: white;
            z-index: 999999999;
            overflow: auto;
        }

        #popupclose
        {
            float: right;
            padding: 10px;
            cursor: pointer;
        }

        .popupcontent
        {
            padding: 10px;
        }

        .downloadFile
        {
            margin-left: -10px;
            margin-top: -2px;
            cursor: pointer;
        }

        .preview-popup
        {
            width: 100%;
            max-width: 1100px;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 10%;
            right: 0;
            margin: auto;
            background: #fff;
            height: 85%;
        }

        .ic-close
        {
            background: url(../../images/icon-close.png) no-repeat center center;
        }

        .ajax__fileupload_dropzone
        {
            font-size: 15px;
            line-height: 120px !important;
            height: 120px !important;
            font-family: "Open Sans", sans-serif;
        }

        #overlay
        {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }

        #floatingCirclesG
        {
            top: 50%;
            left: 50%;
            position: fixed;
        }

        .imgBtn
        {
            background: url(../images/plus.png) 7px 7px no-repeat;
            border-radius: 5px;
            border: none;
            cursor: pointer;
            float: right;
            background-color: lightgrey;
            height: 30px;
            /*margin-top: -20px;*/
            padding-left: 30px;
            font-size: 16px;
            background-origin: padding-box;
        }

        .createDepartment
        {
            display: none;
            height: 20px;
            padding-left: 10px;
            width: 200px;
        }
    </style>
    <script type="text/javascript">

        function pageLoad() {
            //$('.editDepartment').click(function (e) {
            //    e.preventDefault();
            //    removeAllEditable();
            //    showEdit($(this));
            //});

            var closePopup = document.getElementById("popupclose");
            closePopup.onclick = function () {
                var popup = document.getElementById("preview_popup");
                var overlay = document.getElementById("overlay");
                overlay.style.display = 'none';
                popup.style.display = 'none';
                $('#floatingCirclesG').css('display', 'block');
                enableScrollbar();
                $('#reviewContent').removeAttr('src');
            };

        }

        function OnEditClick(ele) {
            removeAllEditable();
            showEdit($(ele));
            return false;
        }

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
        }

        function cancleFileName(ele) {
            $('.saveDepartment,.cancelDepartment').hide();
            $('.deleteDepartment,.editDepartment').show();
            var $this = $(ele);
            var labelName = '';
            var FileName = '';
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                var len = 0;
                len = $(this).find('input[type=text]').length;
                if (len > 0) {
                    labelName = $(this).find('[name=DepartmentName]').html();
                    $(this).find('input[type = text]').val(labelName);
                    $(this).find('input[type = text]').hide();
                    $(this).find('[name=DepartmentName]').show();
                }
                else {
                }
            });
            return false;
        }

        function removeAllEditable() {
            $('.txtDepartmentName,.saveDepartment,.cancelDepartment').hide();
            $('.lblDepartmentName,.editDepartment, .deleteDepartment').show();
        }

        function showEdit(ele) {
            $('.saveDepartment,.cancelDepartment').hide();
            $('.deleteDepartment,.editDepartment,').show();
            var $this = $(ele);
            $this.css('display', 'none');
            $this.parent().find('[name=lnkUpdate]').show();
            $this.parent().find('[name=lnkcancel]').show();
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                $(this).find('input[type=text]').css('border', '1px solid #c4c4c4');
                $(this).find('input[type = text]').show();
                if ($(this).find('input[type = text]').val() == undefined || $(this).find('input[type = text]').val() == "")
                { }
                else
                {
                    oldName = $(this).find('input[type = text]').val();
                }
                $(this).find('[name=DepartmentName]').hide();
                $(this).closest('td').find('.deleteDepartment').hide();
            });
            return false;
        }

        function OnSaveRename(ele, eFileFlowId, fileName) {
            showLoader();
            var newName = $(ele).parent().parent().find("input.txtDepartmentName").val();
            var oldFileName = fileName;
            if (newName == undefined || newName == "") {
                $('.txtDepartmentName').css('border', '1px solid red');
                hideLoader();
                return false;
            }
            else if (!/^[a-z\d][a-z\d\()_\-.\s]+$/i.test(newName)) {
                alert("Department name can not contain special characters.");
                $('.txtDepartmentName').css('border', '1px solid red');
                hideLoader();
                return false;
            }
            else {
                $('.txtDepartmentName').css('border', '1px solid c4c4c4');
            }
            $.ajax(
            {
                type: "POST",
                url: '../EFileFolderJSONService.asmx/RenameDepartment',
                data: "{ newName:'" + newName + "',departmentId:'" + eFileFlowId + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (result.d == 0)
                        alert("Failed to rename department. Please try again after sometime.");
                    else if (result.d == 1)
                        alert("Department has been edited successfully.");
                    else if (result.d == 2)
                        alert("Department with same name already exists. Please enter new name.");
                    hideLoader();
                    __doPostBack('', 'RefreshGrid@');
                },
                error: function (result) {
                    hideLoader();
                }
            });
        }

        function ConfirmDelete() {
            var result = confirm("Are you sure you want to delete this department?");
            if (result)
                showLoader();
            else
                return false;
        }

        function disableScrollbar() {
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }

        function enableScrollbar() {
            $('html, body').css({
                'overflow': 'auto',
                'height': 'auto'
            });
        }

        function openBox() {
            event.preventDefault();
            $('.imgBtn').hide();
            $('.createDepartment').val('');
            $('.createDepartment').show();
            $('.cancelbox').show();
            $('.lblDepartment').show();
            $('.createbox').show();
            return false;
        }

        function closeBox() {
            event.preventDefault();
            $('.imgBtn').show();
            $('.createDepartment').val('');
            $('.createDepartment').hide();
            $('.cancelbox').hide();
            $('.createbox').hide();
            $('.lblDepartment').hide();
            return false;
        }

        function validateDepartment() {
            showLoader();
            var name = $('.createDepartment').val();
            if (name == undefined || name == "") {
                alert("Department name can not be blank.");
                hideLoader();
                return false;
            }
            else if (!/^[a-z\d][a-z\d\()_\-.\s]+$/i.test(name)) {
                alert("Department name can not contain special characters.");
                hideLoader();
                return false;
            }
            else {
                $('.createDepartment').css('border', '1px solid c4c4c4');
            }
        }
    </script>
    <div id="overlay" style="left: 0">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <div class="popup-mainbox preview-popup" id="preview_popup" style="display: none">
        <div class="popupcontrols" style="width: 100%; height: 5%;">
            <span id="popupclose" class="ic-icon ic-close"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 100%; box-sizing: border-box">
            <iframe id="reviewContent" style="width: 100%; height: 95%;"></iframe>
        </div>
    </div>
    <asp:Button ID="Button1ShowPopup2" runat="server" Style="display: none" />
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server"
        PopupControlID="pnlPopup2" DropShadow="true" BackgroundCssClass="modalBackground"
        Enabled="True" TargetControlID="Button1ShowPopup2" />

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 825px; height: 75px; margin-left: -30px;">
                <div class="Naslov_Protokalev1">
                    Department
                </div>
            </div>
            </div>
            <div style="float: right; margin-top: -30px;">
                <asp:Label ID="lblDepartment" runat="server" Text="Department Name :" CssClass="lblDepartment" Style="display: none; font-size: 15px;"></asp:Label>
                <asp:TextBox ID="textDepartment" CssClass="createDepartment" runat="server" Placeholder="Enter Department name here"></asp:TextBox>
                <br />
                <div style="float: right; margin-top: 5px; padding: 3px;">
                    <asp:Button ID="submitDepartment" runat="server" Text="Create" CssClass="createbox" Style="display: none; margin-right: 15px" OnClick="submitDepartment_Click" OnClientClick="return validateDepartment()" />
                    <asp:Button ID="cancelbox" runat="server" Text="Cancel" CssClass="cancelbox" Style="display: none" OnClientClick="return closeBox();" />
                </div>
                <asp:LinkButton ID="btnDepartment" runat="server" CssClass="imgBtn" ToolTip="Create New Department" OnClientClick="return openBox();" Text="" />
            </div>
            <table style="margin-top: 5%; width: 1000px">
                <tbody>
                    <tr>
                        <td>
                            <asp:GridView ID="GridView1" runat="server" CssClass="Empty"
                                DataKeyNames="DepartmentId" OnSorting="GridView1_Sorting"
                                AllowSorting="True" AutoGenerateColumns="false">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%" ItemStyle-Height="35px" ItemStyle-Wrap="false" SortExpression="Name">
                                        <HeaderTemplate>
                                            <asp:LinkButton runat="server" Text="Department Name" CommandName="Sort" CommandArgument="DepartmentName" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDepartmentName" runat="server" Text='<%#Eval("DepartmentName")%>' name="DepartmentName" CssClass="lblDepartmentName" />
                                            <asp:TextBox runat="server" MaxLength="100" Text='<%#Eval("DepartmentName")%>'
                                                ID="TextDepartmentName" Style="display: none; height: 30px; width: 400px; padding-left: 10px;" name="txtDepartmentName" CssClass="txtDepartmentName" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:BoundField DataField="DepartmentName" HeaderText="Department Date" SortExpression="DepartmentName" ItemStyle-Width="4%" />--%>
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" SortExpression="CreatedDate" ItemStyle-Width="4%" />
                                    <asp:TemplateField ShowHeader="true" ItemStyle-Width="3%" ItemStyle-CssClass="actionMinWidth">
                                        <HeaderTemplate>
                                            <asp:Label ID="Label8" runat="server" Text="Action" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton Style="padding: 3px;" ID="lnkBtnEdit" OnClientClick="return OnEditClick(this);" runat="server" CausesValidation="false" Text="Edit" CssClass="editDepartment"
                                                ToolTip="Rename File" />
                                            <asp:LinkButton Text="Update" ID="lnkBtnUpdate" runat="server" CausesValidation="True" AutoPostBack="False"
                                                ToolTip="Update" OnClientClick='<%# "return OnSaveRename(this, " + Eval("DepartmentId") + ")" %>' CssClass="saveDepartment" Style="display: none; padding: 3px;" name="lnkUpdate"></asp:LinkButton>
                                            <asp:LinkButton Text="Cancel" ID="lnkBtnCancel" runat="server" CausesValidation="False" AutoPostBack="False"
                                                OnClientClick="return cancleFileName(this)" ToolTip="Cancel" CssClass="cancelDepartment" Style="display: none; padding: 3px;" name="lnkcancel"></asp:LinkButton>
                                            <asp:LinkButton Style="padding: 3px;" ID="lnkBtnDelete" runat="server" CausesValidation="false" Text="Delete" CssClass="deleteDepartment" OnClientClick="return ConfirmDelete()"
                                                OnCommand="lnkBtnDelete_Command" CommandName='<%#string.Format("{0}",Eval("DepartmentId")) %>' CommandArgument='<%#string.Format("{0}",Eval("DepartmentName")) %>' />
                                            <asp:HiddenField runat="server" ID="hdnDepartmentId" Value='<%# Eval("DepartmentId")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label3" runat="server" CssClass="Empty" />There is no Departments.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="prevnext" bgcolor="#f8f8f5">
                            <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                PageSize="15" OnPageSizeChanged="WebPager1_PageSizeChanged" CssClass="prevnext"
                                PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1"></cc1:WebPager>
                            <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                        </td>
                    </tr>
                </tbody>
            </table>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

