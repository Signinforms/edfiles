﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Shinetech.DAL;

public partial class Manager_DepartmentDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
        else
        {
            var argument = Request.Form["__EVENTARGUMENT"];
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
            {
                GetData();
                BindGrid();
            }
        }
    }

    protected void GetData()
    {
        try
        {
            this.DepartmentSource = General_Class.GetDepartmentsForAdmin();
        }
        catch (Exception ex) { }
    }

    protected void BindGrid()
    {
        try
        {
            GridView1.DataSource = this.DepartmentSource;
            GridView1.DataBind();
            WebPager1.DataSource = this.DepartmentSource;
            WebPager1.DataBind();
        }
        catch (Exception ex) { }
    }

    protected DataTable DepartmentSource
    {
        get
        {
            return this.Session["Files_EdForms"] as DataTable;
        }
        set
        {
            this.Session["Files_EdForms"] = value;
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DepartmentSource);
        Source.Sort = e.SortExpression + " " + sortDirection;
        GridView1.DataSource = Source.ToTable();
        GridView1.DataBind();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void lnkBtnDelete_Command(object sender, CommandEventArgs e)
    {
        try
        {
            string name = e.CommandArgument.ToString();
            int departmentId = Convert.ToInt32(e.CommandName.ToString());
            int value = General_Class.UpdateDepartment(departmentId, name, true);
            if (value == 0)
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Unable to delete department at this time. Please try again later." + "\");", true);
            else if (value == 1)
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Department has been deleted successfully." + "\");", true);
        }
        catch (Exception ex) { }
        GetData();
        BindGrid();
    }

    protected void WebPager1_PageSizeChanged(object s, Shinetech.Framework.Controls.PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.DepartmentSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageIndexChanged(object sender, Shinetech.Framework.Controls.PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DepartmentSource;
        WebPager1.DataBind();
    }

    protected void submitDepartment_Click(object sender, EventArgs e)
    {
        try
        {
            string name = textDepartment.Text.Trim();
            int value = General_Class.CreateDepartment(name.Trim());
            if (value == 0)
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Unable to create department at this time. Please try again later." + "\");", true);
            else if (value == 1)
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Department has been created successfully." + "\");", true);
            else if (value == 2)
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Department with same name already exists. Please enter new department name." + "\");", true);
        }
        catch (Exception ex) { }
        GetData();
        BindGrid();
    }
}