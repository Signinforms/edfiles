﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DriveSizeLogs.aspx.cs" Inherits="Manager_DriveSizeLogs" MasterPageFile="~/MasterPageOld.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .Naslov_Protokalev {
            letter-spacing: 0px;
        }

        .divSize {
            float: right;
            padding: 10px;
            border: solid 1px lightgrey;
            border-radius: 25px;
            margin-right: -140px;
            margin-top: -20px;
        }
    </style>
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <div id="casboLogs">
                <div style="width: 825px; height: 75px;">
                    <div class="Naslov_Protokalev">
                        Drive Size Logs
                    </div>
                    <div class="divSize">
                        <label>Current Size :</label>&nbsp;&nbsp;<span style="font-weight: bold" id="sizeText" runat="server"></span>
                    </div>
                </div>
                <div class="table-scroll" style="margin-left: 30px;width:100%;">
                    <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                    <asp:GridView ID="GridView1" runat="server"
                        DataKeyNames="Id" OnSorting="GridView1_Sorting"
                        AllowSorting="True" AutoGenerateColumns="false" Style="margin-bottom: 20px;width:96%;">
                        <PagerSettings Visible="False" />
                        <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                        <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                        <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                        <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                        <Columns>
                            <asp:BoundField DataField="Size" HeaderText="Size (TB)" SortExpression="Size" />
                            <asp:BoundField DataField="OcrCount" HeaderText="File Count" SortExpression="OcrCount" />
                            <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" SortExpression="CreatedDate" />
                        </Columns>
                        <EmptyDataTemplate>
                            <asp:Label ID="Label1" runat="server" CssClass="Empty" />There is no record.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
