﻿using Shinetech.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_DriveSizeLogs : System.Web.UI.Page
{
    #region Propertis

    protected static string DriveName = ConfigurationManager.AppSettings["DriveName"];

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            float dblSByte = 0;
            foreach (DriveInfo drive in DriveInfo.GetDrives())
            {
                if (drive.IsReady && drive.Name == DriveName)
                {
                    var driveSize = drive.TotalSize;
                    var freeSize = drive.AvailableFreeSpace;

                    float currentSize = driveSize - freeSize;
                    string[] Suffix = { "B", "KB", "MB", "GB", "TB" };
                    int i;
                    dblSByte = currentSize;
                    for (i = 0; i < Suffix.Length && currentSize >= 1024; i++, currentSize /= 1024)
                    {
                        dblSByte = currentSize / 1024;
                    }
                }
            }
            sizeText.InnerText = Convert.ToString(dblSByte.ToString("0.##")) + " TB";
            GetData();
        }
    }

    protected void GetData()
    {
        this.DataSource = General_Class.GetDriveSizeLogs();
        GridView1.DataSource = this.DataSource;
        GridView1.DataBind();
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource_DriveSie"] as DataTable;
        }
        set
        {
            this.Session["DataSource_DriveSie"] = value;
        }
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();
        GridView1.DataSource = this.DataSource;
        GridView1.DataBind();
    }
}