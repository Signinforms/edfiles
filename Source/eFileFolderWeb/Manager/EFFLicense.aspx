<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    CodeFile="EFFLicense.aspx.cs" Inherits="EFFLicense" Title="" %>

<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.ComponentModel" %>
<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>

                        <td width="725" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                            style="background-repeat: repeat-x;">

                            <div style="background: url(images/inner_b.gif); background-position: right top; background-repeat: no-repeat; margin-top: 10px;">

                                <div style="width: 725px; height: 75px;">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="Naslov_Protokalev">Manage Licenses <%=UserName%></div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="DDListOfficeName" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">

                                    <ContentTemplate>

                                        <table width="725" border="0" cellpadding="0" cellspacing="1" bgcolor="#FFFFFF" style="margin: 0 20px 0 70px;">
                                            <tr>
                                                <td>
                                                    <div class="form_title_2" style="margin-top: 0px; margin-bottom: 2px">
                                                        Enter User Name:     
                                                        <asp:DropDownList ID="DDListOfficeName" runat="server" Width="255px" DropDownStyle="DropDownList"
                                                            AutoCompleteMode="SuggestAppend" CssClass="WindowsStyle" DataValueField="UID"
                                                            DataTextField="Name" AutoPostBack="true" CaseSensitive="false" EnableViewState="true"
                                                            OnSelectedIndexChanged="DropDownList1_OnSelectedIndexChanged" AppendDataBoundItems="false">
                                                        </asp:DropDownList>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form_title_2" style="margin-top: 0px; margin-bottom: 2px"><strong></strong></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="GridView3" BorderWidth="2" runat="server" Width="100%" AutoGenerateColumns="false"
                                                        AllowSorting="false" DataKeyNames="UID" OnRowDataBound="GridView1_RowDataBound">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                        <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                        <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                        <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px" ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label81" runat="server" Text="Action" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton11" runat="server" Text="Edit" CausesValidation="false" OnClick="LinkButton11_Click"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="Name" HeaderText="Name">
                                                                <HeaderStyle CssClass="form_title" Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FileFolders" HeaderText="Folder Number">
                                                                <HeaderStyle CssClass="form_title" Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="PersonalMonthPrice" HeaderText="Month Price" DataFormatString="{0:F2}$">
                                                                <HeaderStyle CssClass="form_title" Width="80px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="PerFileRequestFee" HeaderText="Per File Request Fee" DataFormatString="${0:F2}">
                                                                <HeaderStyle CssClass="form_title" Width="100px" />
                                                            </asp:BoundField>

                                                            <asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label10" Text="Total File Request" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton4" CommandArgument='<%#string.Format("{0}",Eval("UID")) %>' runat="server" CausesValidation="false" Text='<%# Bind("TotalFileRequest") %>'
                                                                        OnCommand="LinkButton4_Click" DataFormatString="{0:F0}"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:BoundField DataField="TrialDays" HeaderText="Trial Days" DataFormatString="{0} Days">
                                                                <HeaderStyle CssClass="form_title" Width="80px" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#F8F8F5" class="prevnext">
                                    </ContentTemplate>

                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="DDListOfficeName" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                        </td>

                    </tr>
                </table>
            </td>

        </tr>

    </table>

    <!-- Storage Editor Panel-->
    <asp:Button ID="btnShowPopup1" runat="server" CausesValidation="false" Style="display: none" />
    <ajaxToolkit:ModalPopupExtender ID="mdlPopup1" runat="server" TargetControlID="btnShowPopup1" PopupControlID="pnlPopup1"
        CancelControlID="btnClose1" DropShadow="false" BackgroundCssClass="modalBackground" />
    <!-- ModalPopup Panel-->
    <asp:Panel ID="pnlPopup1" runat="server" Width="350px" Style="display: none">
        <asp:UpdatePanel ID="updPnlLicenseDetail1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label ID="lblLicenseDetail1" runat="server" Font-Bold="true" Text="Storage Fee Details" BackColor="lightblue" Width="95%" />
                <asp:DetailsView ID="dvLicenseDetail1" OnDataBound="dvLicenseDetail1_DataBound" DataKeyNames="UID" AllowPaging="false" AutoGenerateRows="false" runat="server" DefaultMode="Edit" Width="95%" BackColor="white">
                    <Fields>
                        <asp:TemplateField ShowHeader="true" ItemStyle-Wrap="false" ItemStyle-Width="100px" ItemStyle-CssClass="table_tekst_edit">
                            <HeaderTemplate>
                                <asp:Label ID="Label008" runat="server" Text="Name" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="TextBox121" runat="server" Text='<%# Bind("Name") %>' maxlength="20" ReadOnly="true" Width="180px"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Enable Charge">
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkBoxEnableCharge" Checked='<%# Bind("EnableChargeFlag") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="License Kit">
                            <ItemTemplate>
                                <asp:DropDownList runat="server" ID="drpKits">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="true" ItemStyle-Wrap="false" ItemStyle-Width="150px" ItemStyle-CssClass="table_tekst_edit">
                            <HeaderTemplate>
                                <asp:Label ID="Label118" runat="server" Text="Folder Number" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox125" runat="server" Text='<%# Bind("FileFolders") %>' MaxLength="30" ReadOnly="false" Width="50px"></asp:TextBox>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="true" ItemStyle-Wrap="false" ItemStyle-Width="150px" ItemStyle-CssClass="table_tekst_edit">
                            <HeaderTemplate>
                                <asp:Label ID="Label191" runat="server" Text="Month Price" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox122" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PersonalMonthPrice", "{0:F2}") %>' Width="150px" MaxLength="8"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="true" ItemStyle-Wrap="false" ItemStyle-Width="150px" ItemStyle-CssClass="table_tekst_edit">
                            <HeaderTemplate>
                                <asp:Label ID="Label123" runat="server" Text="Per FileRequest Fee" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox1221" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "PerFileRequestFee", "{0:F2}") %>' Width="150px" MaxLength="8"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <%--<asp:TemplateField ShowHeader="true" ItemStyle-Wrap="false" ItemStyle-Width="150px" ItemStyle-CssClass="table_tekst_edit">
                            <HeaderTemplate>
                                <asp:Label ID="Label19" runat="server" Text="Total File Request" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox1222" runat="server" ReadOnly="true" Text='<%# DataBinder.Eval(Container.DataItem, "TotalFileRequest", "{0:F0}") %>' Width="150px" MaxLength="8"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>--%>

                        <asp:TemplateField ShowHeader="true" ItemStyle-Wrap="false" ItemStyle-Width="150px" ItemStyle-CssClass="table_tekst_edit">
                            <HeaderTemplate>
                                <asp:Label ID="Label110" runat="server" Text="Trial Days" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox123" runat="server" Text='<%# Bind("TrialDays") %>' Width="150px" MaxLength="30"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Fields>
                </asp:DetailsView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSave1" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <div align="right" style="width: 95%; background-color: lightblue">
            <asp:Button ID="btnSave1" runat="server" OnClick="btnSave1_Click" Text="Save" CausesValidation="false" Width="50px" />
            <asp:Button ID="btnClose1" runat="server" Text="Close" Width="50px" CausesValidation="false" />
        </div>
    </asp:Panel>

</asp:Content>
