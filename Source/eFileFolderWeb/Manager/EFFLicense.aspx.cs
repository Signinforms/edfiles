using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AjaxControlToolkit;
using Shinetech.DAL;
using Shinetech.Framework.Controls;

public partial class EFFLicense : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.UserName = "";
            this.UserUID = "";

            DDListOfficeName.DataSource = UserManagement.GetOfficeUsers();
            DDListOfficeName.SelectedIndex = 0;
            DDListOfficeName.DataBind();
        }
    }

    #region 数据绑定
   
    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData(string uid)
    {
        this.StorageData = LicenseManagement.GetStorageFee(uid);
        this.DataSource = LicenseManagement.GetLicenseWithTrial();
    }

    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        GridView3.DataSource = this.StorageData;
        GridView3.DataBind();
    }
    #endregion

    public DataTable DataSource
    {
        get
        {
            return this.Session["LicenseData"] as DataTable;
        }
        set
        {
            this.Session["LicenseData"] = value;
        }
    }

    public DataTable StorageData
    {
        get
        {
            return this.Session["StorageData"] as DataTable;
        }
        set
        {
            this.Session["StorageData"] = value;
        }
    }


    protected void LinkButton11_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;
        string uid = Convert.ToString(this.GridView3.DataKeys[row.RowIndex].Value);
        int iUID;
        bool bNumber = Int32.TryParse(uid, out iUID);
        DataView view = new DataView(this.StorageData as DataTable);

        if (bNumber) // from fee table
        {
            view.RowFilter = "UID=" + uid;
        }
        else
        {
            view.RowFilter = "UID='" + uid + "'";
        }

      

        dvLicenseDetail1.DataSource = view;
        dvLicenseDetail1.DataBind();
        view.RowStateFilter = DataViewRowState.ModifiedCurrent;
        updPnlLicenseDetail1.Update();
        this.mdlPopup1.Show();

    }

    protected void btnSave1_Click(object sender, EventArgs e)
    {
        string uid = this.dvLicenseDetail1.DataKey.Value.ToString();
        DataTable dataTable = this.StorageData;
       
        DataRow drView = dataTable.Rows[0];

        try
        {
            drView.BeginEdit();

            if (!string.IsNullOrEmpty(uid))
            {
                Account subfee = new Account(uid); ;
                TextBox textBox;

                textBox = dvLicenseDetail1.FindControl("TextBox125") as TextBox;
                if (textBox != null && !string.IsNullOrEmpty(textBox.Text.Trim()))
                {
                    drView["FileFolders"] = textBox.Text.Trim();
                    subfee.FileFolders.Value = Convert.ToInt32(textBox.Text.Trim());

                }

                textBox = dvLicenseDetail1.FindControl("TextBox122") as TextBox;
                if (textBox != null && !string.IsNullOrEmpty(textBox.Text.Trim()))
                {
                    drView["PersonalMonthPrice"] = textBox.Text.Trim();
                    subfee.PersonalMonthPrice.Value = Convert.ToDouble(textBox.Text.Trim());

                }

                textBox = dvLicenseDetail1.FindControl("TextBox1221") as TextBox;
               if (textBox != null && !string.IsNullOrEmpty(textBox.Text.Trim()))
                {
                    drView["PerFileRequestFee"] = textBox.Text.Trim();
                    subfee.PerFileRequestFee.Value = (float)Convert.ToDouble(textBox.Text.Trim());

                }


                textBox = dvLicenseDetail1.FindControl("TextBox123") as TextBox;
                if (textBox != null && !string.IsNullOrEmpty(textBox.Text.Trim()))
                {
                    drView["TrialDays"] = textBox.Text.Trim();
                    subfee.TrialDays.Value = Convert.ToInt32(textBox.Text.Trim());
                }

                DropDownList drpLicenseKits = dvLicenseDetail1.FindControl("drpKits") as DropDownList;
                if (drpLicenseKits != null)
                {
                    drView["LicenseID"] = Convert.ToInt32(drpLicenseKits.SelectedValue.Trim());
                    subfee.LicenseID.Value = Convert.ToInt32(drpLicenseKits.SelectedValue.Trim());
                }

                CheckBox chkBoxEnable = dvLicenseDetail1.FindControl("chkBoxEnableCharge") as CheckBox;
                if (chkBoxEnable != null)
                {
                    drView["EnableChargeFlag"] = chkBoxEnable.Checked;
                    subfee.EnableChargeFlag.Value = chkBoxEnable.Checked;
                }

                subfee.Update();
            }
    
            drView.EndEdit();
            dataTable.AcceptChanges();
        }
        catch (Exception ex)
        {
            drView.EndEdit();
            dataTable.RejectChanges();
        }
        finally
        {
            dataTable.Dispose();
        }

        this.GridView3.DataSource = this.StorageData;
        this.GridView3.DataBind();

        mdlPopup1.Hide();
        UpdatePanel1.Update();

    }

    protected string UserName
    {
        get
        {
            if (string.IsNullOrEmpty(this.Session["_StorageForUserName"] as string))
            {
                return "";
            }

            return "For "+this.Session["_StorageForUserName"] as string;
        }

        set
        {
            if(string.IsNullOrEmpty(value))
            {
                this.Session["_StorageForUserName"] = value;
            }
            else
            {
                this.Session["_StorageForUserName"] = value;
            }
            
        }
    }

    protected string UserUID
    {
        get
        {
            return this.Session["_StorageForUserID"] as string;
        }

        set
        {
            this.Session["_StorageForUserID"] = value;
        }
    }

    //搜索用户信息
    protected void ImageButton1_OnClick(object sender, EventArgs e)
    {
        string username = "";//txtBoxUID.Text.Trim();        

        if (string.IsNullOrEmpty(username))
        {
            UserName = "";
            UserUID = "";
        }
        else
        {
            MembershipUser user = Membership.GetUser(username);
            if(user!=null)
            {
                UserName = user.UserName;
                string uid = Membership.GetUser(username).ProviderUserKey.ToString();
                UserUID = uid;
                GetData(uid);
            }
            else
            {
                UserName = "";
                UserUID = "";
            }
        }

        BindGrid();
    }

    protected void dvLicenseDetail1_DataBound(object sender, EventArgs e)
    {
        DetailsView dsDetailsView = updPnlLicenseDetail1.FindControl("dvLicenseDetail1") as DetailsView;
        DropDownList drpLicenseKits = dvLicenseDetail1.FindControl("drpKits") as DropDownList;

        if (drpLicenseKits != null)
        {
            drpLicenseKits.DataSource = this.DataSource;
            drpLicenseKits.DataValueField = "LicenseKitID";
            drpLicenseKits.DataTextField = "KitName";
            drpLicenseKits.DataBind();

            DataView view = dvLicenseDetail1.DataSource as DataView;
            if (view!=null)
            {
                drpLicenseKits.SelectedValue = (view[0]["LicenseID"]).ToString();
            }
            
        }
    }

    protected void DropDownList1_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        string uid = DDListOfficeName.SelectedValue;
        UserUID = uid;
        GetData(uid);

        BindGrid();
    }

    protected void LinkButton4_Click(Object sender, CommandEventArgs e)
    {
        string url = string.Format("~/Office/FileRequestMonthly.aspx?id={0}", e.CommandArgument.ToString());

        Response.Redirect(url,true);
    }

  
    //获取某个用户的file request 总数 from box and filerequest
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var rv = e.Row.DataItem as DataRowView;
            var uid = rv.Row.ItemArray[0].ToString();
            rv.Row.ItemArray[8] = 100;

            var textBox = e.Row.Cells[5] as TableCell;
            if (textBox != null)
            {
               var button =  textBox.FindControl("LinkButton4") as LinkButton;
               if (button != null)
               {
                   int total = UserManagement.GetTotalFileRequestsNumber(uid);
                   button.Text = Convert.ToString(total);
               }
            }
        }
    }
}
