﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EstimatorPricing.aspx.cs" Inherits="Manager_EstimatorPricing" MasterPageFile="~/MasterPageOld.master" %>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style type="text/css">
        .Naslov_Protokalev1 {
            font-size: 22px;
            color: #ff7e00;
            padding: 19px 0 0 30px;
            width: 400px;
        }

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }

        #floatingCirclesG {
            top: 50%;
            left: 50%;
            position: fixed;
        }

        .Empty {
            width: 100%;
            height: 50px;
            text-align: center;
            font-size: 20px;
        }

        #comboBoxRecord_chosen
        {
            /*display: block;*/
            margin-top: 5px;
        }
    </style>
    <script type="text/javascript">
        var isValid;
        $(document).ready(function () {
            var timer;
            $('.txtPrice').keypress(function (evt) {
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode != 46 && charCode > 31
                    && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            });

            $('.txtPrice').keyup(function () {
                $(this).val($(this).val().replace(/[^0-9.]/g, ''));
                clearTimeout(timer);
                timer = setTimeout(() => {
                    if ($(this).val() != undefined && $(this).val() != '' && $(this).val().length < 8 && $(this).val().indexOf('..') == -1) {
                        var num = parseFloat($(this).val());
                        var cleanNum = num.toFixed(2);
                        $(this).val(cleanNum);
                        isValid = true;
                    }
                    else {
                        isValid = false;
                    }
                }, 800);

            });

        });

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
        }

        function ConfirmDelete() {
            var result = confirm("Are you sure you want to delete this content?");
            if (result)
                showLoader();
            else
                return false;
        }

        function disableScrollbar() {
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }

        function enableScrollbar() {
            $('html, body').css({
                'overflow': 'auto',
                'height': 'auto'
            });
        }

        function OnEditClick(ele) {
            removeAllEditable();
            showEdit($(ele));
            return false;
        }

        function removeAllEditable() {
            $('.txtPrice,.savePrice,.cancelPrice').hide();
            $('.lblPrice,.editPrice,.deletePrice').show();
        }

        function showEdit(ele) {
            $('.savePrice,.cancelPrice').hide();
            $('.deletePrice,.editPrice').show();
            var $this = $(ele);
            $this.css('display', 'none');
            $this.parent().find('[name=lnkUpdate]').show();
            $this.parent().find('[name=lnkcancel]').show();
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                $(this).find('input[type=text]').css('border', '1px solid #c4c4c4');
                $(this).find('input[type = text]').show();
                if ($(this).find('input[type = text]').val() == undefined || $(this).find('input[type = text]').val() == "") { }
                else {
                    oldName = $(this).find('input[type = text]').val();
                }
                $(this).find('[name=Price]').hide();
                $(this).closest('td').find('.deletePrice').hide();
            });
            return false;
        }

        function OnSave(ele, id) {
            showLoader();
            var newPrice = $(ele).parent().parent().find("input.txtPrice").val();
            if (newPrice == undefined || newPrice == "" || !isValid) {
                $('.txtPrice').css('border', '1px solid red');
                hideLoader();
                return false;
            }
            else {
                $('.textPrice').css('border', '1px solid c4c4c4');
            }
            $.ajax(
                {
                    type: "POST",
                    url: 'EstimatorPricing.aspx/UpdateEstimatorPrice',
                    data: "{ newPrice:'" + newPrice + "',id:'" + id + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result.d == 0)
                            alert("Failed to update price. Please try again after sometime.");
                        else if (result.d == 1)
                            alert("Price has been updated successfully.");
                        hideLoader();
                        __doPostBack('', 'RefreshGridEdit@');
                    },
                    error: function (result) {
                        hideLoader();
                    }
                });
        }

        function cancel(ele) {
            $('.savePrice,.cancelPrice').hide();
            $('.deletePrice,.editPrice').show();
            var price = '';
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                var len = 0;
                len = $(this).find('input[type=text]').length;
                if (len > 0) {
                    price = $(this).find('[name=Price]').html();
                    $(this).find('input[type = text]').val(price);
                    $(this).find('input[type = text]').hide();
                    $(this).find('[name=Price]').show();
                }
                else {
                }
            });
            return false;
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function pageLoad() {
            LoadRecords();
            $("#comboBoxRecord").change(function () {
                showLoader();
                $('#<%= hdnRecordTypeId.ClientID%>').val($("#comboBoxRecord").chosen().val());
                GetPricing();
            });
        }

        function LoadRecords() {
            showLoader();
            $("#comboBoxRecord").html('');
            $("#comboBoxRecord").chosen("destroy");
            $.ajax(
                {
                    type: "POST",
                    url: 'EstimatorPricing.aspx/GetRecordTypes',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result.d) {
                            $.each(result.d, function (i, text) {
                                $('<option />', { value: i, text: text }).appendTo($("#comboBoxRecord"));
                            });
                        }
                        $("#comboBoxRecord").chosen({ width: "270px" });
                        if ($("#" + '<%= hdnRecordTypeId.ClientID%>').val() != "") {
                            $("#comboBoxRecord").val($("#" + '<%= hdnRecordTypeId.ClientID%>').val()).trigger("chosen:updated");
                        }
                        $("#" + '<%= hdnRecordTypeId.ClientID%>').val($("#comboBoxRecord").val());
                        hideLoader();
                    },
                    fail: function () {
                        alert("Failed to get Records. Please try again!");
                        hideLoader();
                    },
                    error: function () {
                        alert("Failed to get Records. Please try again!");
                        hideLoader();
                    }
                });
        }

        function GetPricing() {
            var recordTypeId = $('#<%= hdnRecordTypeId.ClientID%>').val();
            $.ajax(
                {
                    type: "POST",
                    url: 'EstimatorPricing.aspx/LoadPricingDetails',
                    contentType: "application/json; charset=utf-8",
                    data: '{"recordTypeId":"' + recordTypeId + '"}',
                    dataType: "json",
                    success: function (result) {
                        __doPostBack('', 'refreshGrid@');
                    },
                    fail: function (data) {
                        alert("Failed to get Documents. Please try again!");
                        hideLoader();
                    }
                });
        }
    </script>
    <div id="overlay" style="left: 0">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 825px; height: 75px; margin-left: -30px;">
                <div class="Naslov_Protokalev1">
                    Estimator Pricing
                </div>
            </div>
            <fieldset>
                <asp:HiddenField ID="hdnRecordTypeId" runat="server" />
                <asp:Label ID="Label2" Style="font-size: 15px;" runat="server" Text="Select Record"></asp:Label>
                <br />
                <select id="comboBoxRecord" style="color: rgb(68,68,68); border-radius: 5px; background-color: #fff; border: 1px solid #aaa; box-shadow: 0 1px 0 #fff inset; background: linear-gradient(#fff 20%, #f6f6f6 50%, #eee 52%, #f4f4f4 100%)">
                </select>
            </fieldset>
            <table style="margin-top: 5%; width: 1000px">
                <tbody>
                    <tr>
                        <td>
                            <asp:GridView ID="GridView1" runat="server" CssClass="Empty"
                                DataKeyNames="Id" OnSorting="GridView1_Sorting"
                                AllowSorting="True" AutoGenerateColumns="false">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                <Columns>
                                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" ItemStyle-Width="4%" />
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%" ItemStyle-Height="35px" ItemStyle-Wrap="false" SortExpression="Price">
                                        <HeaderTemplate>
                                            <asp:LinkButton runat="server" Text="Price" CommandName="Sort" CommandArgument="Price" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblPricing" runat="server" Text='<%#Eval("Price")%>' name="Price" CssClass="lblPrice" />
                                            <asp:TextBox runat="server" MaxLength="100" Text='<%#Eval("Price")%>'
                                                ID="TextPrice" Style="display: none; height: 30px; width: 400px; padding-left: 10px;" name="txtPrice" CssClass="txtPrice" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" SortExpression="CreatedDate" ItemStyle-Width="4%" />
                                    <asp:TemplateField ShowHeader="true" ItemStyle-Width="3%" ItemStyle-CssClass="actionMinWidth">
                                        <HeaderTemplate>
                                            <asp:Label ID="Label8" runat="server" Text="Action" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton Style="padding: 3px;" ID="lnkBtnEdit" OnClientClick="return OnEditClick(this);" runat="server" CausesValidation="false" Text="Edit" CssClass="editPrice"
                                                ToolTip="Rename File" />
                                            <asp:LinkButton Text="Update" ID="lnkBtnUpdate" runat="server" CausesValidation="True" AutoPostBack="False"
                                                ToolTip="Update" OnClientClick='<%# "return OnSave(this, " + Eval("Id") + ")" %>' CssClass="savePrice" Style="display: none; padding: 3px;" name="lnkUpdate"></asp:LinkButton>
                                            <asp:LinkButton Text="Cancel" ID="lnkBtnCancel" runat="server" CausesValidation="False" AutoPostBack="False"
                                                OnClientClick="return cancel(this)" ToolTip="Cancel" CssClass="cancelPrice" Style="display: none; padding: 3px;" name="lnkcancel"></asp:LinkButton>
                                            <asp:LinkButton Style="padding: 3px;" ID="lnkBtnDelete" runat="server" CausesValidation="false" Text="Delete" CssClass="deletePrice" OnClientClick="return ConfirmDelete()"
                                                OnCommand="lnkBtnDelete_Command" CommandName='<%#String.Format("{0}", Eval("Id")) %>' CommandArgument='<%#String.Format("{0}", Eval("Price")) %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label3" runat="server" CssClass="Empty" />There is no Pricing details.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="prevnext" bgcolor="#f8f8f5">
                            <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                PageSize="15" OnPageSizeChanged="WebPager1_PageSizeChanged" CssClass="prevnext"
                                PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1"></cc1:WebPager>
                            <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                        </td>
                    </tr>
                </tbody>
            </table>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
