﻿using Shinetech.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_EstimatorPricing : System.Web.UI.Page
{
    /// <summary>
    /// The logger
    /// </summary>
    private readonly NLogLogger _logger = new NLogLogger();

    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
        else
        {
            var argument = Request.Form["__EVENTARGUMENT"];
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgridedit"))
            {
                GetData();
                BindGrid();
            }
            else if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
            {
                BindGrid();
            }
        }
    }

    /// <summary>
    /// Gets the data.
    /// </summary>
    protected void GetData()
    {
        try
        {
            this.PricingDataSource = General_Class.GetEstimatorPricing(string.IsNullOrEmpty(hdnRecordTypeId.Value) ? 0 : Convert.ToInt32(hdnRecordTypeId.Value));
        }
        catch (Exception ex) { }
    }

    /// <summary>
    /// Binds the grid.
    /// </summary>
    protected void BindGrid()
    {
        try
        {
            GridView1.DataSource = this.PricingDataSource;
            GridView1.DataBind();
            WebPager1.DataSource = this.PricingDataSource;
            WebPager1.DataBind();
        }
        catch (Exception ex) { }
    }

    /// <summary>
    /// Gets or sets the pricing data source.
    /// </summary>
    /// <value>
    /// The pricing data source.
    /// </value>
    protected DataTable PricingDataSource
    {
        get
        {
            return this.Session["Pricing"] as DataTable;
        }
        set
        {
            this.Session["Pricing"] = value;
        }
    }

    /// <summary>
    /// Handles the Sorting event of the GridView1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.PricingDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;
        GridView1.DataSource = Source.ToTable();
        GridView1.DataBind();
    }

    /// <summary>
    /// Handles the PageSizeChanged event of the WebPager1 control.
    /// </summary>
    /// <param name="s">The source of the event.</param>
    /// <param name="e">The <see cref="Shinetech.Framework.Controls.PageSizeChangeEventArgs"/> instance containing the event data.</param>
    protected void WebPager1_PageSizeChanged(object s, Shinetech.Framework.Controls.PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.PricingDataSource;
        WebPager1.DataBind();
    }

    /// <summary>
    /// Handles the PageIndexChanged event of the WebPager1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="Shinetech.Framework.Controls.PageChangedEventArgs"/> instance containing the event data.</param>
    protected void WebPager1_PageIndexChanged(object sender, Shinetech.Framework.Controls.PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.PricingDataSource;
        WebPager1.DataBind();
    }

    /// <summary>
    /// Gets or sets the sort direction.
    /// </summary>
    /// <value>
    /// The sort direction.
    /// </value>
    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    /// <summary>
    /// Handles the Command event of the lnkBtnDelete control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void lnkBtnDelete_Command(object sender, CommandEventArgs e)
    {
        try
        {
            string name = e.CommandArgument.ToString();
            int id = Convert.ToInt32(e.CommandName.ToString());
            int value = General_Class.UpdateDeleteEstimatorPrice(id, null, true);
            if (value == 0)
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Unable to delete price at this time. Please try again later." + "\");", true);
            else if (value == 1)
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Price has been deleted successfully." + "\");", true);
            GetData();
            BindGrid();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Loads the pricing details.
    /// </summary>
    /// <param name="recordTypeId">The record type identifier.</param>
    [WebMethod]
    public static void LoadPricingDetails(string recordTypeId = null)
    {
        Manager_EstimatorPricing manager = new Manager_EstimatorPricing();
        try
        {
            manager.PricingDataSource = General_Class.GetEstimatorPricing(string.IsNullOrEmpty(recordTypeId) ? 0 : Convert.ToInt32(recordTypeId));
        }
        catch (Exception ex)
        {
            manager._logger.Error(ex);
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
        }
    }
    #region Estimator

    /// <summary>
    /// Updates the price.
    /// </summary>
    /// <param name="newPrice">The new price.</param>
    /// <param name="id">The identifier.</param>
    /// <returns></returns>
    [WebMethod]
    public static int UpdateEstimatorPrice(decimal newPrice, string id)
    {
        try
        {
            return General_Class.UpdateDeleteEstimatorPrice(Convert.ToInt32(id), newPrice);
        }
        catch (Exception ex)
        {
            NLogLogger _logger = new NLogLogger();
            _logger.Info(ex.Message, ex);
            return 0;
        }
    }

    /// <summary>
    /// Gets the record types.
    /// </summary>
    /// <returns>
    /// Record Types
    /// </returns>
    [WebMethod]
    public static Dictionary<string, string> GetRecordTypes()
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        try
        {
            DataTable dtRecords = General_Class.GetRecordTypes();
            if (dtRecords != null)
            {
                dtRecords.AsEnumerable().ToList().ForEach(d =>
                {
                    dict.Add(Convert.ToString(d.Field<int>("Id")), d.Field<string>("Type"));
                });
            }
        }
        catch (Exception ex)
        {
            NLogLogger _logger = new NLogLogger();
            _logger.Info(ex.Message, ex);
        }
        return dict;
    }

    #endregion

}