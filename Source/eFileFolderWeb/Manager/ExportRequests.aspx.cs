using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Serialization;
//using ExportToExcel;
using Shinetech.Framework;
using Shinetech.Utility;
using Shinetech.DAL;
using DataQuicker2.Framework;

public partial class ExportRequests : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            string username = this.Request.QueryString["name"];

            XmlDataDocument xmldoc = GenerateXmlDocument(this.DataSource, this.BoxDataSource);
            //ExportToExcel.ExcelExport objExport = new ExcelExport();
            //try
            //{
            //    objExport.TempFolder = @"\Excel\Temp\";
            //    objExport.TemplateFolder = @"\Excel\Template\";
            //    objExport.XSLStyleSheetFolder = @"\Excel\XSLStyleSheet\";

            //    objExport.CleanUpTemporaryFilesWeb();

            //    string strExcelFile = objExport.TransformXMLDocumentToExcel3(xmldoc, "FileRequests.xsl");
            //    objExport.SendFileRequestToClient(strExcelFile + username);

            //}
            //catch (Exception Ex)
            //{

            //}
        }
    }

    private XmlDataDocument GenerateXmlDocument(DataTable table1, DataTable table2)
    {
        DataSet ds = new DataSet();
        ds.Tables.Add(table1);
        ds.Tables.Add(table2);

        XmlDataDocument doc = new XmlDataDocument(ds);
       
        return doc;
    }

    public DataTable BoxDataSource
    {
        get
        {
            DataTable table =  (this.Session["StoredPaperFiles"] as DataTable).Copy();
            table.TableName = "PagerFile";
            AddDateTimeField1(table);
            return table;
        }
    }

    public DataTable DataSource
    {
        get
        {
            DataTable table = (this.Session["FileFormSource"] as DataTable).Copy();
            table.TableName = "FileForm";
            AddDateTimeField(table);

            return table;
        }
        
    }

    //在这里添加根据SignatureId获取所有questions 和answers的字符串，然后连接在一起
    public void AddDateTimeField(DataTable table)
    {
        DataColumn columnRequestDate = table.Columns.Add("FormatRequestDate", typeof(string));
        DataColumn columnConvertedDat = table.Columns.Add("FormatConvertedDate", typeof(string));
       // DataColumn columnConvertedDat = table.Columns.Add("FormatRequestBy", typeof(string));

        foreach (DataRow row in table.Rows)
        {
            row["FormatRequestDate"] = row["RequestDate"] == DBNull.Value ? "" : ((DateTime)row["RequestDate"]).ToString("MM.dd.yyyy H:m tt");
            row["FormatConvertedDate"] = row["ConvertDate"] == DBNull.Value ? "" : ((DateTime)row["ConvertDate"]).ToString("MM.dd.yyyy H:m tt");

            //row["FormatAnswers"] = GetQuestionDataSource(Convert.ToInt32(row["signatureID"]), uid);
        }
    }

    //在这里添加根据SignatureId获取所有questions 和answers的字符串，然后连接在一起
    public void AddDateTimeField1(DataTable table)
    {
        DataColumn columnRequestDate = table.Columns.Add("FormatRequestDate", typeof(string));

        foreach (DataRow row in table.Rows)
        {
            row["FormatRequestDate"] = row["RequestDate"] == DBNull.Value ? "" : ((DateTime)row["RequestDate"]).ToString("MM.dd.yyyy H:m tt");
        }
    }

    


}
