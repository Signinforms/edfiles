using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Serialization;
//using ExportToExcel;
using Shinetech.Framework;
using Shinetech.Utility;

public partial class ExportUsers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            //XmlDataDocument xmldoc = GenerateXmlDocument(this.DataSource);
            //ExportToExcel.ExcelExport objExport = new ExcelExport();
            //try
            //{
            //    objExport.TempFolder = @"\Excel\Temp\";
            //    objExport.TemplateFolder = @"\Excel\Template\";
            //    objExport.XSLStyleSheetFolder = @"\Excel\XSLStyleSheet\";

            //    objExport.CleanUpTemporaryFilesWeb();

            //    string strExcelFile = objExport.TransformXMLDocumentToExcel(xmldoc, "UsersExcel.xsl");
            //    objExport.SendUsersToClient(strExcelFile);

            //}
            //catch (Exception Ex)
            //{
            //    throw new ApplicationException(Ex.Message);
            //}
        }
    }

    private XmlDataDocument GenerateXmlDocument(DataTable table)
    {
        DataSet ds = new DataSet();
        ds.Tables.Add(table);
        XmlDataDocument doc = new XmlDataDocument(ds);

        return doc;
    }

    public DataTable DataSource
    {
        get
        {
            return UserManagement.GetGiftUsers();
        }
        
    }

}
