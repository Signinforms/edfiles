﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LogForm.aspx.cs" Inherits="Manager_LogForm" MasterPageFile="~/MasterPageOld.master" %>

<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
     <link rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css">
    <script type='text/javascript' src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" defer='defer'></script>
    <style>
        #combobox_chosen {
            display: block;
            margin-top: 5px;
        }

        .chosen-results {
            max-height: 200px !important;
        }
    </style>
    <script type="text/javascript">
        function LoadUsers() {
            $.ajax(
              {
                  type: "POST",
                  url: '../FlexWebService.asmx/GetAllUsersForAdmin',
                  contentType: "application/json; charset=utf-8",
                  dataType: "json",
                  success: function (data) {
                      $.each(data.d, function (i, text) {
                          $('<option />', { value: i, text: text }).appendTo($("#combobox"));
                      });
                      $("#combobox").chosen({ width: "315px" });
                      var hdnValue = $("#" + '<%=hdnUserID.ClientID%>').val();
                      $("#combobox").val(hdnValue).trigger("chosen:updated");
                  },
                  fail: function (data) {
                  }
              });
        }

        function pageLoad() {
            LoadUsers();

            $("#combobox").change(function (event) {
                $("#" + '<%=hdnUserID.ClientID%>').val($("#combobox").chosen().val());
            });
        }
       
    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 825px; height: 75px;">
                                    <div class="Naslov_Protokalev">
                                        LogForm
                                    </div>
                                </div>
            <table style="margin: 0px 20px 0px 30px;">
                <tr>
                    <td>
                        <label>1. Select User to search</label>
                        <select id="combobox" class="combobox">
                        </select>
                      
                        <asp:HiddenField runat="server" ID="hdnUserID"/>
                    </td>
                    <td>
                          <label style="margin-left:5px;">Enter FolderName to search</label>
                        <asp:TextBox ID="txtFolderName" runat="server" style="display: table;padding-bottom: 5px;margin-top: 3px;margin-left: 5px;"></asp:TextBox>
                    </td>
                    <td>
                          <label style="margin-left:5px;">Enter FileName to search</label>
                        <asp:TextBox ID="txtFileName" runat="server" style="display: table;padding-bottom: 5px;margin-top: 3px;margin-left: 5px;"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button runat="server" ID="btnLogFormSearch" text="Search" OnClick="btnLogFormSearch_Click" style="margin-top: 22px;height: 25px;"/>
                    </td>
                </tr>
            </table>
            
            <table style="margin: 0px 20px 0px 30px; width: 925px">
                <tbody>
                    <tr>
                        <td>
                            <asp:GridView ID="GridView1" runat="server" DataKeyNames="ID" OnSorting="GridView1_Sorting"
                                AllowSorting="True" AutoGenerateColumns="false" Width="100%" BorderWidth="0">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                <Columns>
                                    <asp:BoundField DataField="FolderName" HeaderText="Folder Name" SortExpression="FolderName">
                                        <HeaderStyle CssClass="form_title" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FileName" HeaderText="File Name" SortExpression="FileName">
                                        <HeaderStyle CssClass="form_title" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" SortExpression="CreatedDate">
                                        <HeaderStyle CssClass="form_title" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Name" HeaderText="Created By" SortExpression="Name">
                                        <HeaderStyle CssClass="form_title" />
                                    </asp:BoundField>
                                    <%--<asp:BoundField DataField="ID" Visible="false" HeaderText=""></asp:BoundField>--%>
                                    <asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblDelete" Text="Delete" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkBtnDelete" runat="server" CausesValidation="false" Text="Delete"
                                                OnCommand="lnkBtnDelete_Command" CommandArgument='<%#string.Format("{0},{1}",Eval("ID"),Eval("FolderID")) %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                        <tr>
                                            <td align="left" class="podnaslov">
                                                <asp:Label ID="Label1" runat="server" />There is no File Form Request.
                                            </td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <!-- Tabs Editor Panel-->

                        </td>
                    </tr>
                    <tr>
                        <td class="prevnext" bgcolor="#f8f8f5">
                            <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                PageSize="15" OnPageSizeChanged="WebPager1_PageSizeChanged" CssClass="prevnext"
                                PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1"></cc1:WebPager>
                            <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                        </td>
                    </tr>
                </tbody>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
