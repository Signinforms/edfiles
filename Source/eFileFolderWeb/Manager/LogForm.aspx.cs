﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using AjaxControlToolkit;
using DataQuicker2.Framework;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;
using System.Web.Services;

public partial class Manager_LogForm : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["LogForm"] as DataTable;
        }
        set
        {
            this.Session["LogForm"] = value;
        }
    }

    private void GetData()
    {
        this.DataSource = FileFolderManagement.GetLogFormDocsAdmin();
    }

    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }
    protected void lnkBtnDelete_Command(object sender, CommandEventArgs e)
    {
        string folderID = string.Empty;
        try
        {
            string [] args = e.CommandArgument.ToString().Split(',');
            string logFormID = args[0];
            folderID = args[1];
            string path = (Server.MapPath("~/Volume/" + args[1] + "/LogForm"));
            if(!Directory.Exists(path))
                path = (Server.MapPath("~/Volume2/" + args[1] + "/LogForm"));
            string fileName =  FileFolderManagement.DeleteLogForm(int.Parse(logFormID));
            if (!string.IsNullOrEmpty(fileName) && Directory.Exists(path)) 
            {
                if (!Directory.Exists(path + "/DeletedLogForm")) 
                    Directory.CreateDirectory(path + "/DeletedLogForm");
                File.Move(path + "/" + fileName, path + "/DeletedLogForm/" + fileName);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert('Logform has been deleted successfully.')", true);
            }
            this.hdnUserID.Value = "";
            GetData();
            BindGrid();
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(folderID, string.Empty, string.Empty, ex);
        }
    }

    protected void btnLogFormSearch_Click(object sender, EventArgs e)
    {
        try
        {
            string userID = this.hdnUserID.Value;
            this.DataSource = FileFolderManagement.GetFilteredLogForms(userID, txtFileName.Text.Trim(), txtFolderName.Text.Trim());
            BindGrid();
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
        }
    }
}