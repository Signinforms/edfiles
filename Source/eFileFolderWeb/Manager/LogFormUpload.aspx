﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LogFormUpload.aspx.cs" Inherits="Manager_LogFormUpload" MasterPageFile="~/MasterPageOld.master"%>

<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
<link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style>
        #combobox_chosen {
            display: block;
            margin-top: 5px;
        }
        #comboFolder_chosen {
            display: block;
            margin-top: 5px;
        }
        .chosen-results {
            max-height: 200px !important;
        }

        label {
            font-size:14px;
        }

          #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
            left:0;
        }

            #folderList {
            width: 100%;
            display: none;
            border: 1px #dadada solid;
            position: absolute;
            left: 0;
            top: 100%;
            z-index: 9;
            background: #fff;
            max-height: 235px;
            overflow-y: auto;
        }

            #folderList label {
                display:block;
                padding:0 5px;
                width:auto;
                min-width:initial;
                cursor:pointer;
                margin-top: 3px;
            }

                #folderList label:hover {
                    background-color: #e5e5e5;
                }

                    #folderList input {
                        display:inline-block;
                    }   
                     .selectBox-outer {
            position:relative;
            display:inline-block;
        }
        
            .selectBox select {
                width: 100%;
                
            }
              .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            font-size:15px;
        }

              .logFormAddFile {
             background: url(../images/file-add.png) no-repeat center center;
             margin-top:6px;
             margin-left:-10px;
        }

        .logFormUploadFile {
             background: url(../images/file-upload.png) no-repeat center center;
             cursor:pointer;
             margin-top:6px;
             border:none;
        }
        .chosen-container {
        font-size:15px;
        }

    </style>
    <script type="text/javascript">
        var pubExpanded = false;
        function LoadUsers() {
            
            $.ajax(
              {
                  type: "POST",
                  url: '../FlexWebService.asmx/GetAllUsersForAdmin',
                  contentType: "application/json; charset=utf-8",
                  dataType: "json",
                  success: function (data) {
                      $.each(data.d, function (i, text) {
                          $('<option />', { value: i, text: text }).appendTo($("#combobox"));
                      });
                      $("#combobox").chosen({ width: "315px" });
                      
                      LoadFolders($("#combobox").val());
                  },
                  fail: function (data) {
                      alert("Failed to get Users. Please try again!");
                      hideLoader();
                  }
              });
        }

        function LoadFolders(userID) {
            $("#folderList").html('');
            $("#comboBoxFolder").html('');
            $("#comboBoxFolder").chosen("destroy");
            $.ajax(
             {
                 type: "POST",
                 url: '../FlexWebService.asmx/GetFolderDetailsById',
                 contentType: "application/json; charset=utf-8",
                 data: '{"officeId":"' + userID + '"}',
                 dataType: "json",
                 success: function (result) {
                     if (result.d) {
                         $('<label><input type="checkbox" name="select-all" id="select-all" onclick="toggle(this);"/>Select All</label>').appendTo($('#folderList'));
                         $.each(result.d, function (i, text) {
                             var container = $('#folderList');
                             $('<label for="cb' + i + '"><input type="checkbox" id="cb' + i + '" value=' + i + '> ' + text.trim() + '</label>').appendTo(container);
                         });
                         //var folders = JSON.parse(result.d);
                         //if (folders.length > 0) {
                         //    $('<label><input type="checkbox" name="select-all" id="select-all" onclick="toggle(this);"/>Select All</label>').appendTo($('#folderList'));
                         //    for (var i = 0; i < folders.length; i++) {
                         //        var container = $('#folderList');
                         //        $('<label for="cb' + folders[i].folderID + '"><input type="checkbox" id="cb' + folders[i].folderID + '" value=' + folders[i].folderID + '> ' + folders[i].FolderName.trim() + '</label>').appendTo(container);
                         //    }
                         //}
                     }
                     $("#comboBoxFolder").chosen({ width: "315px" });
                     hideLoader();
                     
                  },
                 fail: function (data) {
                     alert("Failed to get Folders. Please try again!");
                     hideLoader();
                  }
              });
        }

        function showLoader() {
            $('#overlay').show();
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
        }

        function pageLoad() {
            showLoader();
            LoadUsers();

            $("#combobox").change(function (event) {
                showLoader();
                $("#" + '<%=hdnUserID.ClientID%>').val($("#combobox").chosen().val());
                  LoadFolders($("#combobox").val());
              });
        }

        function showCheckboxesPublicHolidays() {
            var checkboxes = document.getElementById("folderList");
            if (!pubExpanded) {
                checkboxes.style.display = "block";
                pubExpanded = true;
            } else {
                checkboxes.style.display = "none";
                pubExpanded = false;
            }
            return false;
        }

        function toggle(source) {
            var checkboxes = $("#folderList").find("input[type=checkbox]");
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i] != source)
                    checkboxes[i].checked = source.checked;
            }
        }

        function OnBtnUploadclick() {

            if (document.getElementById("<%=FileUpload1.ClientID%>").files.length > 2)
            {
                alert("Maximum two files are allowed to upload in any folder!");
                return false;
            }
            
            var source = document.getElementById("<%=FileUpload1.ClientID%>").value;
            if (source.length > 0) {
                if (source.substring(source.lastIndexOf(".") + 1, source.length).toLowerCase() != "pdf") {
                    alert("Only pdf extension is allowed!");
                    return false;
                }

                var checkboxes = $("#folderList").find("input[type=checkbox]");
                var folders = "";
                if (checkboxes.length > 0) {
                    for (var i = 1; i < checkboxes.length; i++) {
                        if ($(checkboxes[i]).prop("checked"))
                            folders += $(checkboxes[i]).val().trim() + ", ";
                    }
                    folders = folders.substr(0, folders.lastIndexOf(','));
                }
                if (folders.length <= 0 || checkboxes.length <= 0) {
                    alert("Please select at least one folder to upload file");
                    return false;
                }
                $("#" + '<%=hdnFolderID.ClientID %>').val(folders);
            }
            else {
                alert("Please select at least one file to upload!");
                return false;
            }
        }

    </script>
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 825px; height: 75px;">
                                    <div class="Naslov_Protokalev">
                                        Upload LogForm
                                    </div>
                                </div>
            <table style="margin: 0px 20px 0px 30px;">
                <tr>
                    <td>
                        <label>Select User to upload LogForm</label>
                        <select id="combobox">
                        </select>
                      
                        <asp:HiddenField runat="server" ID="hdnUserID"/>
                    </td>
                    
                    
                </tr>
                <tr>
                    <td>
                        <br />
                        <div class="selectBox-outer" style="width:315px;display:block;margin-top:5px;">                                    
                                  <div class="selectBox " onclick="showCheckboxesPublicHolidays()">
                                                            <select id="comboBoxFolder" style="color:rgb(68,68,68);border-radius:5px;background-color: #fff;border: 1px solid #aaa;box-shadow: 0 1px 0 #fff inset;background: linear-gradient(#fff 20%, #f6f6f6 50%, #eee 52%, #f4f4f4 100%)" >
                                                                <option>Select Folder to upload LogForm</option>
                                                            </select>
                                                            <div class="overSelect"></div>
                                                        </div>
                                     <div id="folderList" style="display:none;margin-top: -1px;border-radius: 0 0 4px 4px;background-clip: padding-box"></div> 
                            <asp:HiddenField runat="server" ID="hdnFolderID"/>
                                    </div>
                      
                         
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--<span class="ic-icon logFormAddFile" id="spanFileUpload1" runat="server" >
                                <asp:FileUpload ID="FileUpload1" runat="server"  style="opacity:0;cursor:pointer; width:30px!important; margin-left:7px; overflow:hidden; height:28px !important" ToolTip="Add Log Form"/>
                                    </span>
                                <asp:Button ID="btnUpload" Text="Upload" runat="server" OnClick="btnUpload_Click"  CssClass="ic-icon logFormUploadFile" style="display:none;z-index:999; width:28px;"/>--%>
                        <br />
                        <br />
                        <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="true" style="margin-top:5px;"/>

                        <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" OnClientClick="return OnBtnUploadclick();" Text="Upload" style="margin-top:5px;margin-bottom:25%;float:right"/>
                        
                    </td>
                </tr>
            </table>

            </ContentTemplate>
            <Triggers>
                                        <asp:PostBackTrigger ControlID="btnUpload" />
                                    </Triggers>
    </asp:UpdatePanel>
    </asp:Content>
