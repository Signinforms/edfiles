﻿using Shinetech.DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_LogFormUpload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
   
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (FileUpload1.HasFiles)
        {
            try
            {
                foreach (HttpPostedFile postedFile in FileUpload1.PostedFiles)
                {
                    if (!string.IsNullOrEmpty(hdnFolderID.Value.Trim()))
                    {
                        string[] folderIds = hdnFolderID.Value.Trim().Split(',');
                        foreach (string folderid in folderIds)
                        {
                            try
                            {
                                if (postedFile.ContentType == "application/pdf")
                                {
                                    string filename = Path.GetFileName(postedFile.FileName);
                                    var path = HttpContext.Current.Server.MapPath("~/" + "Volume/" + folderid.Trim());
                                    if (!Directory.Exists(path))
                                    {
                                        path = HttpContext.Current.Server.MapPath("~/" + "Volume2/" + folderid.Trim() + "/LogForm/");
                                    }
                                    else
                                        path += "/LogForm/";

                                    if (!Directory.Exists(path))
                                    {
                                        Directory.CreateDirectory(path);
                                    }
                                    else
                                    {
                                        if (Directory.GetFiles(path).Length >= 2)
                                        {
                                            continue;
                                        }
                                    }
                                    filename = filename.Replace("'", "");
                                    postedFile.SaveAs(path + filename);
                                    General_Class.DocumentsInsertForLogForm(filename, Sessions.SwitchedSessionId, DateTime.Now, int.Parse(folderid.Trim()));
                                    //General_Class.DocumentsInsertForLogForm(filename, Sessions.UserId, DateTime.Now, int.Parse(folderid.Trim()));

                                }
                            }
                            catch (Exception ex)
                            {
                                Common_Tatva.WriteErrorLog(folderid.Trim(), string.Empty, postedFile.FileName, ex);
                            }
                            //else
                            //    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Only pdf type is allowed!');", true);
                        }
                    }
                }
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('LogForm(s) have been uploaded!');", true);
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Failed to upload LogForm. Please try again later.');", true);
            }
        }
    }
   
}