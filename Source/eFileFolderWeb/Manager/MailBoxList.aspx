﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MailBoxList.aspx.cs" Inherits="Manager_MailBoxList" MasterPageFile="~/MasterPageOld.master" %>

<%@ Import Namespace="System.Data" %>
<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <%--<link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/style.css" rel="stylesheet" type="text/css" />--%>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style>
        #combobox_chosen
        {
            display: block;
            margin-top: 5px;
        }

        body
        {
            font-family: 'Times New Roman';
        }

        h3
        {
            display: block;
            font-size: 20px;
            -webkit-margin-before: 1em;
            -webkit-margin-after: 1em;
            -webkit-margin-start: 0px;
            -webkit-margin-end: 0px;
            font-weight: bold;
        }

        .chosen-results
        {
            max-height: 200px !important;
        }

        label
        {
            font-size: 14px;
        }

        #overlay
        {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
            left: 0;
        }

        .selectBox-outer
        {
            position: relative;
            display: inline-block;
        }

        .selectBox select
        {
            width: 100%;
        }

        .overSelect
        {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            font-size: 15px;
        }

        .chosen-container
        {
            font-size: 15px;
        }

        .datatable
        {
            border: 1px solid #cccaca;
            width: 100%;
            border-spacing: 0;
            empty-cells: show;
            border-bottom: 0 none;
        }

            .datatable th
            {
                background: #ecebeb;
                color: #030303;
                font-weight: bold;
                font-size: 14px;
                padding: 6px;
                vertical-align: middle;
                text-align: center;
                text-decoration: none;
                border: 0 none;
                border-bottom: 1px solid #cccaca;
            }

                .datatable th a
                {
                    color: #030303;
                    font-weight: bold;
                    font-size: 14px;
                    padding: 6px;
                    vertical-align: middle;
                    text-align: center;
                    text-decoration: none;
                    border: 0 none;
                }

            .datatable td
            {
                border-bottom: 1px solid #e9e9e9;
                border-left: 1px solid #e9e9e9;
                border-right: 1px solid #e9e9e9;
                color: #595959;
                font-size: 12px;
                padding: 6px;
                font-family: 'open sans', sans-serif;
                vertical-align: middle;
                text-align: center;
            }

            .datatable tbody td, .datatable thead th
            {
                border-bottom: 1px solid #cccaca;
                border-left: 0 none;
                border-right: 1px solid #cccaca;
            }

                .datatable tbody td.last, .datatable thead th.last
                {
                    border-right: 0 none !important;
                }

            .datatable tbody tr.even
            {
                background: #fff;
            }

            .datatable tbody tr.odd
            {
                background: #f5f5f5;
            }

        .datatable-small th
        {
            background: #ecebeb;
            color: #030303;
            font-weight: bold;
            padding: 6px;
            border: 0 none;
            border-bottom: 1px solid #cccaca;
            font-size: 12px;
            text-align: center;
            text-decoration: none;
            vertical-align: middle;
        }

            .datatable-small th a
            {
                background: none;
                color: #030303;
                font-weight: bold;
                padding: 6px;
                border: 0 none;
                font-size: 12px;
                text-align: center;
                text-decoration: none;
                vertical-align: middle;
            }

        .listing-datatable a
        {
            color: #1f43a7;
            text-decoration: underline;
        }

        .emptyBox
        {
            height: 30px;
            width: 1%;
            font-size: 15px;
            text-align: center;
        }

        .Empty td
        {
            height: 30px;
            width: 1%;
            font-size: 15px;
            text-align: center;
        }

        .imgBtn
        {
            background: url(../images/plus.png) 7px 7px no-repeat;
            border-radius: 5px;
            border: none;
            cursor: pointer;
            float: right;
            background-color: lightgrey;
            height: 30px;
            padding-left: 25px;
            font-size: 16px;
            background-origin: padding-box;
        }

        /*th
        {
            cursor:pointer;
        }*/

        .hidden
        {
            display: none;
        }

        .Naslov_Protokalev1
        {
            font-size: 22px;
            color: #ff7e00;
            padding: 19px 0 0 30px;
            width: 400px;
        }

        .linkDelete
        {
            margin-left: 10px;
            cursor: pointer;
        }

        .linkEdit
        {
            margin-right: 10px;
            cursor: pointer;
        }
    </style>
    <script type="text/javascript">
        function showLoader() {
            $('#overlay').show();
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
        }


        function pageLoad() {
            //showLoader();
            //LoadUsers();
        }

        function EditMailBox(mailBoxId) {

            window.open('MailBoxDetails.aspx?id=' + mailBoxId + '', '_blank');
        }

        function CreateMailBox() {
            window.open('MailBoxDetails.aspx', '_blank');
          }
    </script>
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <div style="margin-left: -40px;">
        <div style="width: 825px; height: 75px; padding-left: 5px;">
            <div class="Naslov_Protokalev1">
                MailBox List
            </div>
        </div>
        <%--<table style="margin: 0px 20px 40px 30px;">
            <tr>
                <td>
                    <label>Select User to see Template</label>
                    <select id="combobox">
                    </select>

                </td>
            </tr>
        </table>--%>
        <%--<asp:Button runat="server" ID="btnTempData" Text="Search" OnClick="btnTempData_Click" CssClass="hidden" Style="margin-top: 22px; height: 25px;" />--%>
        <%--<asp:HiddenField runat="server" ID="hdnOfficeName" />
        <asp:HiddenField runat="server" ID="hdnOfficeId" />--%>
        <div id="mainContent" style="margin-bottom: 50%; margin-left: 30px;">

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Button ID="Button1" runat="server" CssClass="imgBtn" ToolTip="Create New MailBox" OnClientClick="javascript:CreateMailBox();" Text="" />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                    <asp:GridView ID="GridView1" runat="server" CssClass="Empty"
                        DataKeyNames="MailBoxId" OnSorting="GridView1_Sorting"
                        AllowSorting="True" AutoGenerateColumns="false">
                        <PagerSettings Visible="False" />
                        <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                        <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                        <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                        <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                        <Columns>
                            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" ItemStyle-Width="2%" />
                            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" ItemStyle-Width="2%" />
                            <asp:TemplateField ShowHeader="true" ItemStyle-Width="1%" ItemStyle-CssClass="actionMinWidth">
                                <HeaderTemplate>
                                    <asp:Label ID="Label8" runat="server" Text="Action" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton Text="Edit" CssClass="linkEdit" runat="server" ID="lnkEditTemplate"
                                        OnClientClick='<%#string.Format("javascript:EditMailBox({0})", Eval("MailBoxId"))%>'>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton2" CssClass="linkDelete" runat="server" Text="Delete" CausesValidation="false" CommandArgument='<%# Eval("MailBoxId")%>' OnCommand="LinkButton2_Command"></asp:LinkButton>
                                    <ajaxToolkit:ConfirmButtonExtender ID="ButtonDeleteUser" DisplayModalPopupID="lnkDelete_ModalPopupExtender" runat="server" ConfirmText="Are you sure to delete this template ?"
                                        TargetControlID="LinkButton2">
                                    </ajaxToolkit:ConfirmButtonExtender>
                                    <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="LinkButton2"
                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                    </ajaxToolkit:ModalPopupExtender>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                        <EmptyDataTemplate>
                            <asp:Label ID="Label1" runat="server" CssClass="Empty" />There is no mailbox.
                        </EmptyDataTemplate>
                    </asp:GridView>

                    <asp:Panel class="popupConfirmation" ID="DivDeleteConfirmation" Style="display: none"
                        runat="server">
                        <div class="popup_Container">
                            <div class="popup_Titlebar" id="PopupHeader">
                                <div class="TitlebarLeft">
                                    Delete Template
                                </div>
                                <div class="TitlebarRight" onclick="$get('ButtonDeleteCancel').click();">
                                </div>
                            </div>
                            <div class="popup_Body">
                                <p>
                                    Are you sure you want to delete this mailbox?
                                </p>
                                <p>
                                    Please enter password to delete mailbox:
                                                                    <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                                </p>
                            </div>
                            <div class="popup_Buttons">
                                <input id="ButtonDeleleOkay" type="button" value="Yes" style="margin-left: 80px; width: 65px" />
                                <input id="ButtonDeleteCancel" type="button" value="No" style="margin-left: 20px; width: 65px" />
                            </div>
                        </div>
                    </asp:Panel>

                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
