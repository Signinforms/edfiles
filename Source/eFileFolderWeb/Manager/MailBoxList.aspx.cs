﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using AjaxControlToolkit;
using DataQuicker2.Framework;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Shinetech.Utility;
using System.Linq;
using MailEnable.Administration;
using System.Net;
using System.Text;

public partial class Manager_MailBoxList : System.Web.UI.Page
{
    public string webServicePath = ConfigurationSettings.AppSettings["MailBoxService"].ToString();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
    }

    public void GetData()
    {
        this.DataSource = General_Class.GetMailBox(null);
        this.BackDataSource = this.DataSource;
    }

    public void BindGrid()
    {
        GridView1.DataSource = this.DataSource;
        GridView1.DataBind();
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["MailBox"] as DataTable;
        }
        set
        {
            this.Session["MailBox"] = value;
        }
    }

    public DataTable BackDataSource
    {
        get
        {
            return this.Session["Back_DataSource_MailBox"] as DataTable;
        }
        set
        {
            this.Session["Back_DataSource_MailBox"] = value;
        }
    }

    protected void LinkButton2_Command(object sender, CommandEventArgs e)
    {
        try
        {
            string password = textPassword.Text.Trim();
            string strPassword = PasswordGenerator.GetMD5(password);

            if (strPassword != PasswordGenerator.GetMD5(ConfigurationManager.AppSettings["ANOTHER_PASSWORD"]))
            {
                //show the password is wrong
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again later.');", true);

            }
            else
            {
                int boxId = int.Parse(e.CommandArgument.ToString());
                DataTable dt = Session["MailBox"] as DataTable;
                bool isMailBoxRemoved = true;
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataRow[] filteredRows = dt.Select("MailBoxId = '" + boxId + "'");
                    if (filteredRows != null && filteredRows.Count() > 0)
                    {
                        using (WebClient client = new WebClient())
                        {
                            var reqparm = new System.Collections.Specialized.NameValueCollection();
                            reqparm.Add("Email", Convert.ToString(filteredRows[0]["Email"]));
                            byte[] responsebytes = client.UploadValues(webServicePath + "RemoveMailBox", "POST", reqparm);
                            string responsebody = Encoding.UTF8.GetString(responsebytes);
                            try
                            {
                                if (!string.IsNullOrEmpty(responsebody))
                                    isMailBoxRemoved = Convert.ToBoolean(responsebody);
                            }
                            catch (Exception ex)
                            {
                                isMailBoxRemoved = false;
                            }
                        }
                    }
                }
                if (isMailBoxRemoved)
                {
                    bool isDeleted = General_Class.DeleteMailBox(boxId);
                    GetData();
                    BindGrid();
                    if (isDeleted)
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('The mailbox has been deleted successfully.');", true);
                    else
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Failed to delete mailbox, please try again later.');", true);
                }
                else
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Failed to delete mailbox, please try again later.');", true);
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog("Error occured in Catch of LinkButton2_Command ", " Ln : 115 ", "", ex);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Failed to delete mailbox, please try again later.');", true);
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    //protected void btnTempData_Click(object sender, EventArgs e)
    //{
    //    DataRow[] dr = this.BackDataSource.Select("OfficeId = '" + hdnOfficeId.Value + "'");
    //    if (dr.Length > 0)
    //        this.DataSource = dr.CopyToDataTable();
    //    else
    //        this.DataSource = new DataTable();
    //    BindGrid();
    //}

    public bool RemoveMailBox(string email)
    {
        string[] postOffice = email.Split('@');

        MailEnable.Administration.Mailbox mb = new MailEnable.Administration.Mailbox();
        mb.Postoffice = postOffice[1];
        mb.MailboxName = postOffice[0];
        mb.RedirectAddress = "";
        mb.RedirectStatus = -1;
        mb.Size = -1;
        mb.Limit = -1;
        mb.Status = -1;
        int ret_mb = mb.RemoveMailbox();
        if (ret_mb == 1)
        {
            MailEnable.Administration.AddressMap map = new MailEnable.Administration.AddressMap();
            map.Account = postOffice[1];
            map.DestinationAddress = "[SF:" + postOffice[1] + "/" + postOffice[0] + "]";
            map.SourceAddress = "";
            map.Scope = "";
            int ret_map = map.RemoveAddressMap();

            MailEnable.Administration.Login login = new MailEnable.Administration.Login();
            login.Account = postOffice[1];
            login.LastAttempt = -1;
            login.LastSuccessfulLogin = -1;
            login.LoginAttempts = -1;
            login.Password = "";
            login.Rights = "";
            login.Status = -1;
            login.UserName = email;
            int ret_login = login.RemoveLogin();
            if (ret_login != 1)
                return false;
        }
        else
            return false; ;
        return true;
    }
}