﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MailboxDetails.aspx.cs" Inherits="Manager_MailBoxDetails" MasterPageFile="~/MasterPageOld.master" %>

<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <%--<link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/style.css" rel="stylesheet" type="text/css" />--%>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style>
        #combobox_chosen
        {
            display: block;
            margin-top: 5px;
        }

        body
        {
            font-family: 'Times New Roman';
        }

        h3
        {
            display: block;
            font-size: 20px;
            -webkit-margin-before: 1em;
            -webkit-margin-after: 1em;
            -webkit-margin-start: 0px;
            -webkit-margin-end: 0px;
            font-weight: bold;
        }

        .chosen-results
        {
            max-height: 200px !important;
        }

        label
        {
            font-size: 14px;
        }

        #overlay
        {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
            left: 0;
        }

        .selectBox-outer
        {
            position: relative;
            display: inline-block;
        }

        .selectBox select
        {
            width: 100%;
        }

        .txtMail
        {
            margin-top: 9px;
            height: 21px;
            border: solid grey 1px;
            border-radius: 4px;
            padding-left: 10px;
        }

        .chosen-container
        {
            font-size: 15px;
        }

        table.mail td
        {
            padding: 10px;
        }

        /*th
        {
            cursor:pointer;
        }*/

        .hidden
        {
            display: none;
        }

        .Naslov_Protokalev1
        {
            font-size: 22px;
            color: #ff7e00;
            padding: 19px 0 0 30px;
            width: 400px;
        }

        .create-btn
        {
            color: #fff;
            cursor: pointer;
            font-size: 15px;
            text-align: center;
            width: auto;
        }

        .btn-small
        {
            border: 0 none;
            color: #fff;
            cursor: pointer;
            display: inline-block;
            font-family: "HelveticaNeueLT-Bold";
            font-size: 15px;
            height: 25px;
            line-height: 19px;
            text-align: center;
            transition: all 0.3s ease 0s;
            vertical-align: top;
            width: 60px;
            -moz-appearance: none;
            -ms-appearance: none;
            -o-appearance: none;
            -webkit-appearance: none;
            appearance: none;
            /*margin: 2px 0;*/
            /*margin-left:50px;*/
        }

            .btn.green, .btn-small.green
            {
                background-color: #83af33;
                font-family: "Open Sans",sans-serif;
                margin-left: 10px;
            }
    </style>

    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <div style="margin-left:-40px;">
        <div style="width: 825px; height: 50px; margin-top: 20px;padding-left:10px;">
            <asp:Label runat="server" ID="labelTemplate" CssClass="Naslov_Protokalev1"></asp:Label>
        </div>
        <%--<asp:HiddenField ID="hdnOfficeName" runat="server" />
        <asp:HiddenField ID="hdnOfficeId" runat="server" />--%>
        <asp:HiddenField ID="hdnUID" runat="server" />
        <asp:HiddenField ID="hdnEmail" runat="server" />
        <table class="mail" style="margin: 0px 20px 0px 30px;">
            <tr>
                <td>
                    <label>Select User</label>
                    <select id="combobox"></select>
                </td>
                <td>
                    <label>Email</label><br />
                    <asp:TextBox runat="server" ID="txtEmail" disabled CssClass="txtMail txtMailName" Text="@edfiles.com" Style="font-size: 18px; background-color: white; width: auto;"></asp:TextBox>
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server"></asp:UpdatePanel>
        <table class="mail" style="margin: 20px 20px 0px 30px;">
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblPassword" Style="font-size: 14px;">Password</asp:Label><br />
                    <asp:TextBox type="password" onfocusout="confirmPass(this)" runat="server" CssClass="txtMail txtPassword" ID="txtPassword" placeholder="Enter password"></asp:TextBox>
                </td>
                <td>
                    <label>Confirm Password</label><br />
                    <asp:TextBox type="password" onfocusout="confirmPass(this)" runat="server" CssClass="txtMail txtConfPassword" ID="txtConfPassword" placeholder="Enter password"></asp:TextBox><br />
                </td>
                <td>
                    <asp:Button CssClass="create-btn btn-small green" runat="server" ID="btnSubmit" Text="Submit" OnClick="btnSubmit_Click" Style="margin-top: 25px;" />
                </td>
            </tr>
            <tr>
                <td style="width:50px !important">
                    <span class="valPass" style="display: none; color: red; font-size: 15px;">Password must contain one number, one lowercase, one uppercase and atleast 8 characters</span>
                </td>
                <td style="width:50px !important">
                    <span class="valConfPass" style="display: none; color: red; font-size: 15px;">Password and Confirm Password must match</span>
                </td>
                <td></td>
            </tr>
        </table>
    </div>
    <script type="text/javascript">

        var isValid = true;
        var isPassFilled = true;
        var isPassValid = true;
        var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;

        //function submit() {
        //    if (($('.txtPassword').val() != '') && ($('.txtPassword').val() != $('.txtConfPassword').val())) {
        //        $('.txtPassword').css('border', 'solid red 1px');
        //        $('.txtConfPassword').css('border', 'solid red 1px');
        //        isValid = false;
        //    }
        //    if (isValid)
        //        return true;
        //    else
        //        return false;
        //}

        function showLoader() {
            $('#overlay').show();
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function confirmPass(e) {
            var txtPass = $('.txtPassword');
            var txtConfPass = $('.txtConfPassword');
            if ($(e).attr("id") == '<%= txtPassword.ClientID %>') {
                if (txtConfPass.val() != '') {
                    if (txtConfPass.val() != txtPass.val()) {
                        txtPass.css('border', 'solid red 1px');
                        txtConfPass.css('border', 'solid red 1px');
                        $('.valConfPass').show();
                    }
                    else {
                        txtPass.css('border', 'solid grey 1px');
                        txtConfPass.css('border', 'solid grey 1px');
                        $('.valConfPass').hide();
                    }
                }
                if (txtPass.val() != '' && !re.test(txtPass.val())) {
                    txtPass.css('border', 'solid red 1px');
                    $('.valPass').show();
                    isPassValid = false;
                }
                else
                {
                    txtPass.css('border', 'solid grey 1px');
                    $('.valPass').hide();
                    isPassFilled = true;
                }
            }
            else if ($(e).attr("id") == '<%= txtConfPassword.ClientID %>') {
                if (txtPass.val() != '') {
                    if (txtPass.val() != txtConfPass.val()) {
                        txtConfPass.css('border', 'solid red 1px');
                        $('.valConfPass').show();
                    }
                    else {
                        txtPass.css('border', 'solid grey 1px');
                        txtConfPass.css('border', 'solid grey 1px');
                        $('.valConfPass').hide();
                    }
                }
            }
        }

        function hideLoader() {
            $('#overlay').hide();
        }
        function LoadUsers() {
            $.ajax(
              {
                  type: "POST",
                  url: 'MailBoxDetails.aspx/GetOnlyUsers',
                  contentType: "application/json; charset=utf-8",
                  dataType: "json",
                  success: function (data) {
                      $.each(data.d, function (i, text) {
                          $('<option />', { value: i, text: text }).appendTo($("#combobox"));
                      });
                      $("#combobox").chosen({ width: "175px" });
                      if ($('#<%= hdnUID.ClientID%>').val() != '')
                          $("#combobox").val($("#" + '<%= hdnUID.ClientID%>').val()).trigger("chosen:updated");
                      $('.txtMailName').val($('.chosen-single').find('span').text().replace(/\s/g, "_") + "@edfiles.com");
                      $('#<%= hdnEmail.ClientID%>').val($('.txtMailName').val());
                      hideLoader();

                  },
                  fail: function (data) {
                      alert("Failed to get Users. Please try again!");
                      hideLoader();
                  }
              });
          }

          $(document).ready(function () {
              showLoader();
              $("#combobox").change(function (e) {
                  $('.txtMailName').val($('.chosen-single').find('span').text().replace(/\s/g, "_") + "@edfiles.com");
                  $('#<%= hdnUID.ClientID%>').val($("#combobox").chosen().val());
                  $('#<%= hdnEmail.ClientID%>').val($('.txtMailName').val());
              });
              $('#<%= btnSubmit.ClientID%>').click(function (e) {
                  if (($('.txtPassword').val() != '') && $('.txtConfPassword').val() != '') {
                      isPassFilled = true;
                  }
                  else {
                      $('.txtPassword').css('border', 'solid red 1px');
                      $('.txtConfPassword').css('border', 'solid red 1px');
                      isPassFilled = false;
                  }
                  if ($('.txtPassword').val() != $('.txtConfPassword').val()) {
                      $('.txtPassword').css('border', 'solid red 1px');
                      $('.txtConfPassword').css('border', 'solid red 1px');
                      isValid = false;
                  }
                  else {
                      isValid = true;
                  }

                  if (isValid && isPassFilled && isPassValid)
                      return true;
                  else
                      return false;
              });
              if ($('#<%= labelTemplate.ClientID%>').text() == 'Create MailBox')
                  LoadUsers();
              else {
                  $("#combobox").parent().remove();
                  $('.txtMailName').val($('#<%= hdnEmail.ClientID%>').val());
                  hideLoader();
              }
          });
    </script>
</asp:Content>
