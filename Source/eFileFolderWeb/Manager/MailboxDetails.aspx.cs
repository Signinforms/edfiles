﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DataQuicker2.Framework;
using Shinetech.Framework.Controls;
using Shinetech.Utility;
using Shinetech.DAL;
using Shinetech.Framework;
using System.IO;
using Account = Shinetech.DAL.Account;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Web.Services;
using System.Linq;
using MailEnable.Administration;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Net;

public partial class Manager_MailBoxDetails : System.Web.UI.Page
{
    public string hostName = ConfigurationSettings.AppSettings["MailboxHost"].ToString();
    public string domain = ConfigurationSettings.AppSettings["MailboxDomain"].ToString();
    public string rights = ConfigurationSettings.AppSettings["MailboxRight"].ToString();
    public string webServicePath = ConfigurationSettings.AppSettings["MailBoxService"].ToString();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            int boxId = Convert.ToInt32(Request.QueryString["id"]);
            //string uid = (Request.QueryString["uid"]).ToString();
            if (boxId != 0)
            {
                GetData(boxId);
            }
            else
            {
                labelTemplate.Text = "Create MailBox";
                btnSubmit.Text = "Create";
                //hdnUID.Value = Request.QueryString["officeId"].ToString();
                //hdnOfficeId.Value = Request.QueryString["officeId"].ToString();
                //hdnOfficeName.Value = Request.QueryString["officeName"].ToString();
            }
        }
    }

    public void GetData(int boxId)
    {
        DataTable dt = Session["MailBox"] as DataTable;
        if (dt != null && dt.Rows.Count > 0)
        {
            DataRow[] filteredRows = dt.Select("MailBoxId = '" + boxId + "'");
            labelTemplate.Text = "Edit MailBox";
            //txtConfPassword.Text = filteredRows[0]["Password"].ToString();
            //txtPassword.Text = filteredRows[0]["Password"].ToString();
            hdnUID.Value = filteredRows[0]["UID"].ToString();
            //txtEmail.Text = filteredRows[0]["Email"].ToString();
            hdnEmail.Value = filteredRows[0]["Email"].ToString();
            lblPassword.Text = "New Password";
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Session has been expired !!!');window.location.href = 'MailBoxList.aspx'", true);
        }
        btnSubmit.Text = "Edit";
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        string passWord = txtPassword.Text;
        string email = hdnEmail.Value;
        int boxId = Convert.ToInt32(Request.QueryString["id"]);
        bool isExist = General_Class.isMailBoxExist(hdnUID.Value, boxId);
        int created = 0;
        int updated = 0;
        if (isExist)
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Error: Mailbox with same user exists !!!')", true);
        else
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    var reqparm = new System.Collections.Specialized.NameValueCollection();
                    reqparm.Add("Email", email);
                    reqparm.Add("Password", passWord);

                    if (boxId == 0)
                    {
                        byte[] responsebytes = client.UploadValues(webServicePath + "AddMailBox", "POST", reqparm);
                        string responsebody = Encoding.UTF8.GetString(responsebytes);
                        try
                        {
                            if (!string.IsNullOrEmpty(responsebody))
                            {
                                created = Convert.ToInt32(responsebody);
                            }
                        }
                        catch (Exception ex) { }
                    }
                    else
                    {
                        byte[] responsebytes = client.UploadValues(webServicePath + "EditMailBox", "POST", reqparm);
                        string responsebody = Encoding.UTF8.GetString(responsebytes);
                        try
                        {
                            if (!string.IsNullOrEmpty(responsebody))
                            {
                                updated = Convert.ToInt32(responsebody);
                            }
                        }
                        catch (Exception ex) { }
                        if (updated == 2)
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Error: Old password is wrong');window.location.href = 'MailBoxList.aspx'", true);
                    }
                }
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteErrorLog("For MailBox Id = " + boxId.ToString(), "Email : " + email, "btnSubmit_Click on MailboxDetail.aspx.cs", ex);
            }
            int count = 0;
            if ((boxId > 0 && updated == 1) || (boxId == 0 && created == 1))
                count = General_Class.UpdateOrCreateMailBox(email, passWord, hdnUID.Value.ToLower(), boxId);
            if (count > 0)
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", boxId > 0 ? "alert('Mailbox has been updated successfully.');window.location.href = 'MailBoxList.aspx'" : "alert('Mailbox has been created successfully.');window.location.href = 'MailBoxList.aspx'", true);
            else
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", boxId > 0 ? "alert('Error occured while updating mailbox !!')" : "alert('Error occured while creating mailbox !!')", true);
        }
    }

    [WebMethod]
    public static Dictionary<string, string> GetOnlyUsers()
    {
        Manager_MailBoxDetails mb = new Manager_MailBoxDetails();
        Dictionary<string, string> dict = new Dictionary<string, string>();
        try
        {
            DataTable dtUsers = General_Class.GetAllUsers();
            IList<string> list = new List<string>();
            if (dtUsers != null)
            {
                dtUsers.AsEnumerable().ToList().ForEach(d =>
                {
                    dict.Add(Convert.ToString(d.Field<Guid>("UID")), d.Field<string>("Name"));

                });
            }
        }
        catch (Exception ex)
        { }
        return dict;
    }

    public int AddMailbox(string password, string Email)
    {
        MailEnable.Administration.Mailbox oMailbox = new MailEnable.Administration.Mailbox();
        MailEnable.Administration.Login oLogin = new MailEnable.Administration.Login();
        string sMailboxName = Email.Split('@')[0];
        oLogin.Account = domain;
        oLogin.LastAttempt = -1;
        oLogin.LastSuccessfulLogin = -1;
        oLogin.LoginAttempts = -1;
        oLogin.Password = "";
        oLogin.Rights = "";
        oLogin.Status = -1;
        oLogin.UserName = Email;
        //  If the login does not exist we need to create it
        if ((oLogin.GetLogin() == 0))
        {
            oLogin.Account = domain;
            oLogin.LastAttempt = 0;
            oLogin.LastSuccessfulLogin = 0;
            oLogin.LoginAttempts = 0;
            oLogin.Password = password;
            oLogin.Rights = rights;
            oLogin.Status = 1;
            //  0  Disabled, 1  Enabled
            oLogin.UserName = Email;
            if ((oLogin.AddLogin() != 1))
            {
                //  Error adding the Login
                return 0;
            }

        }
        //  Now we create the mailbox
        oMailbox.Postoffice = domain;
        oMailbox.MailboxName = sMailboxName;
        //  Set the Redirect Address if applicable
        // oMailbox.RedirectStatus = 1
        // oMailbox.RedirectAddress = sRedirectAddress
        oMailbox.Size = 0;
        oMailbox.Limit = -1;
        //  -1  Unlimited OR size value (in KB)
        oMailbox.Status = 1;
        if ((oMailbox.AddMailbox() != 1))
        {
            //  Failed to add mailbox
            return 0;
        }

        //  Now we need to add the Address Map entries for the Account
        MailEnable.Administration.AddressMap oAddressMap = new MailEnable.Administration.AddressMap();
        oAddressMap.Account = domain;
        oAddressMap.DestinationAddress = ("[SF:"
                    + (domain + ("/"
                    + (sMailboxName + "]"))));
        oAddressMap.SourceAddress = ("[SMTP:"
                    + Email + "]");
        oAddressMap.Scope = "0";
        if ((oAddressMap.AddAddressMap() != 1))
        {
            //  Failed to add Address Map for some reason!
            return 2;
        }
        return 1;
    }

    public int EditMailBox(string password, string Email)
    {
        MailEnable.Administration.Login login = new MailEnable.Administration.Login();
        //  set search fields of login
        login.Account = domain;
        login.UserName = Email;
        login.Description = string.Empty;
        login.Host = string.Empty;
        login.LastAttempt = -1;
        login.LastSuccessfulLogin = -1;
        login.LoginAttempts = -1;
        login.Password = string.Empty;
        login.Rights = rights;
        login.Status = -1;

        if (login.GetLogin() == 1)
        {
            if (login.Status == 1)
                login.EditLogin(Email, login.Status, password, domain, login.Description, -1, -1, -1, rights);
            else
                return 2;
        }
        return 1;
    }
}

public class MailBoxItem
{
    public string Email { get; set; }
    public string Password { get; set; }
}
