<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    CodeFile="MessageUsers.aspx.cs" Inherits="ManageMessageUsers" Title="" %>

<%@ Import Namespace="System.Data" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register Src="../Controls/UserFind.ascx" TagName="UserFind" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td width="725px" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                                        style="background-repeat: repeat-x;">
                                        <div style="background: url(images/inner_b.gif); background-position: right top;
                                            background-repeat: no-repeat; margin-top: 10px;">
                                            <div style="width: 725px; height: 60px;">
                                                <div class="Naslov_Zelen">
                                                    Message Users</div>
                                            </div>
                                            <div class="podnaslovUser" style="padding: 5px; background-color: #f6f5f2;">
                                                <table border="0" cellpadding="0" cellspacing="0" style="margin: 10px 0 20px 10px;">
                                                    <tr>
                                                        <td height="20" colspan="3" align="left" valign="top" class="tekstDef" style="padding-top: 0px;
                                                            padding-bottom: 0px;">
                                                            Enter search term: (Name, First name, Last name, Email)
                                                            <asp:TextBox ID="txtKey" runat="server" class="form_tekst_field" MaxLength="50" />
                                                            <asp:Button ID="btnSearch" runat="server" CausesValidation="false" Text="Search"
                                                                Width="60" Height="21" OnClick="btnSearch_Click" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div>
                                                <div background="../images/bg_main.gif" bgcolor="#f6f5f2">
                                                </div>
                                                <asp:GridView ID="GridView1" runat="server" DataKeyNames="UID" OnSorting="GridView1_Sorting"
                                                    AllowSorting="True" AutoGenerateColumns="false" Width="100%" BorderWidth="2">
                                                    <PagerSettings Visible="False" />
                                                    <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                    <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                    <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                    <Columns>
                                                        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name">
                                                            <HeaderStyle CssClass="form_title" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname">
                                                            <HeaderStyle CssClass="form_title" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname">
                                                            <HeaderStyle CssClass="form_title" />
                                                        </asp:BoundField>
                                                       <%-- <asp:BoundField DataField="DOB" HeaderText="DOB" DataFormatString="{0:MM.dd.yyyy}"
                                                            SortExpression="DOB" />--%>
                                                        <asp:BoundField DataField="IsApproved" HeaderText="IsActive" SortExpression="IsApproved" />
                                                        <asp:BoundField DataField="LastLoginDate" HeaderText="LastLoginDate" DataFormatString="{0:MM.dd.yyyy}"
                                                            SortExpression="LastLoginDate" />
                                                        <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="false" Text="Message"
                                                                    OnClick="LinkButton3_Click"></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                <asp:Button ID="Button1ShowPopup" runat="server" Style="display: none" />
                                                <ajaxToolkit:ModalPopupExtender ID="ModalPopup3" runat="server" PopupControlID="pnlPopup3"
                                                    CancelControlID="btnClose" DropShadow="true" BackgroundCssClass="modalBackground"
                                                    TargetControlID="Button1ShowPopup" />
                                                <!-- ModalPopup Panel-->
                                                <asp:Panel ID="pnlPopup3" runat="server" Width="450px" Style="display: none; background-color: white">
                                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <div style="width: 100%; background-color: lightblue">
                                                                <strong>Message Body</strong></div>
                                                            <div class="tekstDef_message">
                                                                <table width="100%" border="0" cellpadding="2" cellspacing="0">
                                                                    <tr>
                                                                        <td align="left" class="podnaslov">
                                                                            <asp:TextBox ID="txtMessageBody" runat="server" TextMode="MultiLine" Width="400px"
                                                                                Height="200" MaxLength="500" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div align="center" style="width: 100%; background-color: white">
                                                                <asp:HiddenField id="hdnUID" runat="server" />
                                                                <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" Width="50px" />
                                                                <asp:Button ID="btnClose" runat="server" Text="Close" Width="50px" />
                                                            </div>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click"></asp:AsyncPostBackTrigger>
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </asp:Panel>
                                                <!-- end Panel --->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="prevnext" bgcolor="#f8f8f5">
                                        <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                          OnPageSizeChanged="WebPager1_PageSizeChanged" PageSize="15" CssClass="prevnext" PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1">
                                        </cc1:WebPager>
                                        <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
            </td>
            <td width="10" align="center">
                <img src="../images/lin.gif" width="1" height="453" vspace="10" /></td>
        </tr>
    </table>
</asp:Content>
