using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DataQuicker2.Framework;
using Shinetech.Framework.Controls;
using Shinetech.Utility;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;
using Account=Shinetech.DAL.Account;

public partial class ManageMessageUsers : LicensePage
{
    private static readonly string PDFPATH = ConfigurationManager.AppSettings["PdfPath"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();//重新获取操作后的数据源
            BindGrid();//绑定GridView,为删除服务

        }
    }

    #region 分页
    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        if (this.DataSource!=null)
        {
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        if (this.DataSource != null)
        {
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource_MessageUsers"] as DataTable;
        }
        set
        {
            this.Session["DataSource_MessageUsers"] = value;
        }
    }


    #endregion

    #region 数据绑定
    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData()
    {
        if (User.IsInRole("Administrators"))
        {
            this.DataSource = UserManagement.GetOfficeUsers();
        }
        else
        {
            this.DataSource = UserManagement.GetWebUserList(Sessions.SwitchedSessionId);
            //this.DataSource = UserManagement.GetWebUserList(Membership.GetUser().ProviderUserKey.ToString());
        }
    }
    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }
    #endregion

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();//重新设置数据源，绑定
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

 
    //LinkButton3_Click for Message users
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;
        string uid = Convert.ToString(this.GridView1.DataKeys[row.RowIndex].Value);
        hdnUID.Value = uid;

        Account account = new Account(uid);
        if (account.IsExist)
        {
            txtMessageBody.Text = account.MessageBody.Value;
        }
        
        UpdatePanel3.Update();
        ModalPopup3.Show();
    }

   
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string uid = hdnUID.Value;

        Account account = new Account(uid);
        if (account.IsExist)
        {
            account.MessageBody.Value = txtMessageBody.Text.Trim();
            account.Update();
        }

        UpdatePanel1.Update();
    }



    public DataTable States
    {
        get
        {
            object obj = Application["States"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetStates();
                Application["States"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    public DataTable Countries
    {
        get
        {
            object obj = Application["Countries"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetCountries();
                Application["Countries"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    //search users by terms
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string strKey = this.txtKey.Text.Trim();
        DataTable table = UserManagement.SearchUsers(strKey);

        this.DataSource = table;

        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }
}
