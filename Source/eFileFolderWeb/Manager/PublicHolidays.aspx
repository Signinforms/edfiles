﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PublicHolidays.aspx.cs" Inherits="Manager_PublicHolidays"  MasterPageFile="~/MasterPageOld.master" %>

<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
      <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>extras/jquery-ui-1.8.22.custom.min.js"></script>
    <style>
        #combobox_chosen {
            display: block;
            margin-top: 5px;
        }

        .chosen-results {
            max-height: 200px !important;
        }

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }
        .form-group{
            padding:10px 0;
        }
        .form-group label {
            font-size:16px;
            line-height:20px;
        }
        .form-group input[type="text"], .form-group select {
            font-size:16px;
            line-height:20px;
            height:25px;
            box-sizing:border-box;
        }
    </style>
    <script type="text/javascript">
       

    </script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 825px; height: 75px;">
                                    <div class="Naslov_Protokalev">
                                        Public Holidays
                                    </div>
                                </div>
            <table style="margin: 0px 20px 0px 30px;">
                <tr>
                    <td class="form-group">
                        <label>Enter Public Holiday Name</label>
                        <asp:TextBox runat="server" ID="txtHolidayName"></asp:TextBox>
                        <asp:HiddenField runat="server" ID="hdnUserID"/>
                    </td>
                    <td>
                        <asp:Button runat="server" ID="btnSaveHoliday" text="Save" OnClick="btnSaveHoliday_Click" style="height: 25px;"/>
                    </td>
                </tr>
            </table>
            
            <table style="margin: 0px 20px 0px 30px; width: 925px">
                <tbody>
                    <tr>
                        <td>
                            <asp:GridView ID="GridView2" runat="server" DataKeyNames="HolidayID" OnSorting="GridView2_Sorting"
                                AllowSorting="True" AutoGenerateColumns="false" Width="100%" BorderWidth="0" OnRowDataBound="GridView2_RowDataBound">
                                <PagerSettings  />
                                <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                <Columns>
                                    
                                     <asp:TemplateField SortExpression="Name" HeaderText="Name" ShowHeader="true">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("Name")%>' name="DocumentName" CssClass="lblDocumentName" />
                                             <asp:TextBox runat="server" MaxLength="50" Text='<%#Eval("Name")%>'
                                                        ID="TextDocumentName" Style="display: none; height: 30px;" name="txtDocumentName" CssClass="txtDocumentName"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField ItemStyle-CssClass="table_tekst_edit">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblAction" runat="server" Text="Action" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkBtnDelete" runat="server" CausesValidation="false" Text="Delete" CssClass="deleteDocument"
                                                OnCommand="lnkBtnDelete_Command" CommandArgument='<%#string.Format("{0}",Eval("HolidayID")) %>' Style="margin-left:5px;" name="lnkBtnDelete"/>
                                            <asp:LinkButton ID="lnkBtnEdit" runat="server" CausesValidation="false" Text="Edit" CommandArgument='<%# Eval("HolidayID")%>' name="lnkBtnEdit"
                                                CssClass="editDocument" Style="margin-left:5px;"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkBtnUpdate" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                Text="Update" CssClass="saveDocument" Style="display: none" CommandArgument='<%# Eval("HolidayID")%>' name="lnkUpdate"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkBtnCancel" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                OnClientClick="cancleFileName(this)" Text="Cancel" CssClass="cancelDocument" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                </Columns>
                                <EmptyDataTemplate>
                                    <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                        <tr>
                                            <td align="left" class="podnaslov">
                                                <asp:Label ID="Label1" runat="server" />There is no public holiday.
                                            </td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <!-- Tabs Editor Panel-->

                        </td>
                    </tr>
                    <tr>
                        <td class="prevnext" bgcolor="#f8f8f5">
                            <cc1:WebPager ID="WebPager2" runat="server" OnPageIndexChanged="WebPager2_PageIndexChanged"
                                PageSize="15" OnPageSizeChanged="WebPager2_PageSizeChanged" CssClass="prevnext"
                                PagerStyle="OnlyNextPrev" ControlToPaginate="GridView2"></cc1:WebPager>
                            <asp:HiddenField ID="SortDirection2" runat="server" Value=""></asp:HiddenField>
                        </td>
                    </tr>

                    <tr>
                        <td class="form-group">
                            <label>Choose year to edit/search holiday.</label>
                            
                            <select id="holidayYear">
                                <option value="2017" selected="selected">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                            </select>
                             <asp:HiddenField runat="server" ID="hdnYear"/>
                            <asp:Button runat="server" ID="btnSearch" text="Search" OnClick="btnSearch_Click" style="margin-top: 22px;height: 25px;"/>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <asp:GridView ID="GridView1" runat="server" DataKeyNames="HolidayID" OnSorting="GridView1_Sorting"
                                AllowSorting="True" AutoGenerateColumns="false" Width="100%" BorderWidth="0" OnRowDataBound="GridView1_RowDataBound">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                <Columns>
                                    <asp:BoundField DataField="Name" HeaderText="Holiday Name" SortExpression="Name">
                                        <HeaderStyle CssClass="form_title" />
                                    </asp:BoundField>
                                    <asp:TemplateField ShowHeader="true" HeaderText="Holiday Date" SortExpression="DateOfHoliday">
                                        <ItemTemplate>
                                            <asp:Label ID="lblHolidayDate" runat="server" Text='<%#Eval("HolidayDate")%>' name="lblHolidayDate" CssClass="lblHolidayDate" />
                                            <asp:TextBox ID="textfield15" name="textfield15" runat="server" size="20" style="display:none" class="textfield15" />
                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" PopupPosition="BottomLeft" runat="server"
                                                TargetControlID="textfield15" Format="MM-dd-yyyy">
                                            </ajaxToolkit:CalendarExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-CssClass="table_tekst_edit">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblAction" runat="server" Text="Action" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                          <%--  <asp:LinkButton ID="lnkBtnDelete" runat="server" CausesValidation="false" Text="Delete" CssClass="deleteDocument"
                                                OnCommand="lnkBtnDelete_Command" CommandArgument='<%#string.Format("{0}",Eval("HolidayID")) %>' Style="margin-left:5px;" name="lnkBtnDelete"/>--%>
                                            <asp:LinkButton ID="lnkBtnEdit" runat="server" CausesValidation="false" Text="Edit" CommandArgument='<%# Eval("HolidayID")%>' name="lnkBtnEdit"
                                                CssClass="editDocumentDate" Style="margin-left:5px;"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkBtnUpdate" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                Text="Update" CssClass="saveDocument" Style="display: none" CommandArgument='<%# Eval("HolidayID")%>' name="lnkUpdate"></asp:LinkButton>
                                            <asp:LinkButton ID="lnkBtnCancel" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                OnClientClick="cancelEditDate(this)" Text="Cancel" CssClass="cancelDocument" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                        <tr>
                                            <td align="left" class="podnaslov">
                                                <asp:Label ID="Label1" runat="server" />There is no public holiday.
                                            </td>
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                            </asp:GridView>
                            <!-- Tabs Editor Panel-->

                        </td>
                    </tr>
                    <tr>
                        <td class="prevnext" bgcolor="#f8f8f5">
                            <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                PageSize="15" OnPageSizeChanged="WebPager1_PageSizeChanged" CssClass="prevnext"
                                PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1"></cc1:WebPager>
                            <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                        </td>
                    </tr>
                </tbody>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="WebPager2" EventName="PageIndexChanged" />
        </Triggers>
    </asp:UpdatePanel>
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <script type="text/jscript">

        function pageLoad() {
            $('.editDocument').click(function (e) {
                e.preventDefault();
                removeAllEditable();
                showEdit($(this));
            });

            $('.editDocumentDate').click(function (e) {
                e.preventDefault();
                removeAllEditable();
                showEditDate($(this));
            });

            if ($("#" + '<%= hdnYear.ClientID%>').val() != "" && $("#" + '<%= hdnYear.ClientID%>').val() != undefined)
                $("#holidayYear").val($("#" + '<%= hdnYear.ClientID%>').val());
        }

        $("#holidayYear").live('change', function () {
            $("#" + '<%= hdnYear.ClientID%>').val($("#holidayYear").val());
        });

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
        }

        function removeAllEditable() {
            $('.lblDocumentName,.editDocument, .deleteDocument, .editDocumentDate, .lblHolidayDate').show();
            $('.saveDocument, .cancelDocument, .txtDocumentName, .changeDate, .textfield15').hide();
        }

        function OnSaveRename(txtRenameObj, holidayID, fileName) {
            newname = $('#' + txtRenameObj)[0].value;
            oldFileName = fileName;
            if (newname == undefined || newname == "") {
                $('.txtDocumentName').css('border', '1px solid red');
                return false;
            }
            else if (!/^[a-z\d][a-z\d\()_\-.\s]+$/i.test(newname)) {
                alert("File name can not contain special characters.");
                $('.txtDocumentName').css('border', '1px solid red');
                return false;
            }
            else if (newname.indexOf('.') > 0 && newname.substring(newname.indexOf('.') + 1).toLowerCase() != 'pdf') {
                alert("Only File with type 'pdf' is allowed.");
                $('.txtDocumentName').css('border', '1px solid red');
                return false;
            }
            else {
                $('.txtDocumentName').css('border', '1px solid c4c4c4');
            }
            showLoader();
            $.ajax(
            {
                type: "POST",
                url: '../EFileFolderJSONService.asmx/RenamePublicHoliday',
                data: "{ rename:'" + newname + "', holidayId:'" + holidayID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (result.d.length > 0)
                        alert("Failed to rename holiday. Please try again after sometime.");
                    hideLoader();
                    window.location.reload();
                },
                error: function (result) {
                    hideLoader();
                }
            });
        }

        function OnSaveRenameDate(txtRenameObj, holidayID) {
            newname = $('#' + txtRenameObj)[0].value;
            showLoader();
            $.ajax(
            {
                type: "POST",
                url: '../EFileFolderJSONService.asmx/EditPublicHolidayDate',
                data: "{ holidayId:'" + holidayID + "', yearOfHoliday:'" + $("#holidayYear").val() + "', holidDayDate:'" + newname + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (result.d.length > 0)
                        alert("Failed to edit holiday. Please try again after sometime.");
                    hideLoader();
                    window.location.reload();
                },
                error: function (result) {
                    hideLoader();
                }
            });
        }

        function showEdit(ele) {
            $('.editDocument').show();
            var $this = $(ele);
            $this.css('display', 'none');
            $this.parent().find('[name=lnkUpdate]').show();
            $this.parent().find('[name=lnkcancel]').show();
            $this.parent().find('[name=lnkBtnDelete]').hide();
            
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                $(this).find('input[type=text]').css('border', '1px solid #c4c4c4');//1px solid #c4c4c4
                $(this).find('input[type = text]').show();
                if ($(this).find('input[type = text]').val() == undefined || $(this).find('input[type = text]').val() == "")
                { }
                else
                {
                    oldName = $(this).find('input[type = text]').val();
                }
                $(this).find('[name=DocumentName]').hide();
                $(this).find('[name=lblClass]').hide();
            });
            return false;
        }

        function showEditDate(ele) {
            $('.editDocumentDate').show();
            var $this = $(ele);
            $this.css('display', 'none');
            $this.parent().find('[name=lnkUpdate]').show();
            $this.parent().find('[name=lnkcancel]').show();
            $this.parent().find('[name=lnkBtnDelete]').hide();
            var calendar = ele.parent().parent().find("input[class=textfield15]");
            if (calendar) {
                $(calendar).val("");
                calendar.css("display", "inline-block");
            }
           
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                $(this).find('[name=lblHolidayDate]').hide();
            });
            return false;
        }

        function cancleFileName(ele) {
            var $this = $(ele);
            var LabelName = '';
            var FileName = '';
            $this.css('display', 'none');
            $this.parent().find('[name=lnkUpdate]').hide();
            $this.parent().find('[name=lnkBtnDelete]').show();
            $this.parent().find('[name=lnkBtnEdit]').show();
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                var len = 0;
                len = $(this).find('input[type=text]').length;
                if (len > 0) {
                    LabelName = $(this).find('[name=DocumentName]').html();
                    $(this).find('input[type = text]').val(LabelName);
                    $(this).find('input[type = text]').hide();
                    $(this).find('[name=DocumentName]').show();
                }
                else {
                }
            });
        }
        
        function cancelEditDate(ele) {
            var $this = $(ele);
            $this.css('display', 'none');
            $this.parent().find('[name=lnkUpdate]').hide();
            $this.parent().find('[name=lnkBtnDelete]').show();
            $this.parent().find('[name=lnkBtnEdit]').show();
            var calendar = $this.parent().parent().find("input[class=textfield15]");
            if (calendar)
                calendar.css("display", "none");
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                    $(this).find('[name=lblHolidayDate]').show();
            });
        }
    </script>
</asp:Content>
