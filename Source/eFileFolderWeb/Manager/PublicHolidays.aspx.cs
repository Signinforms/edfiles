﻿using Shinetech.DAL;
using Shinetech.Framework.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_PublicHolidays : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["PublicHolidays"] as DataTable;
        }
        set
        {
            this.Session["PublicHolidays"] = value;
        }
    }

    public DataTable DataSourceEdit
    {
        get
        {
            return this.Session["PublicHolidaysEdit"] as DataTable;
        }
        set
        {
            this.Session["PublicHolidaysEdit"] = value;
        }
    }

    private void GetData()
    {
        this.DataSource = FileFolderManagement.GetPublicHolidays();
        this.DataSourceEdit = FileFolderManagement.GetPublicHolidayDates(System.DateTime.Now.Year);
        AddColumn();   
    }

    private void AddColumn() {
        this.DataSourceEdit.Columns.Add("HolidayDate");
        foreach (DataRow row in this.DataSourceEdit.Rows)
        {
            foreach (DataColumn column in this.DataSourceEdit.Columns)
            {
                if (column.ColumnName == "HolidayDate" && !string.IsNullOrEmpty(row["DateOfHoliday"].ToString()))
                {
                    DateTime tmp = DateTime.Parse(row["DateOfHoliday"].ToString());
                    var st = tmp.ToString("dd/MM/yyyy");
                    row[column] = st;
                }

            }
        }
    }

    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSourceEdit;
        WebPager1.DataBind();
        WebPager2.DataSource = this.DataSource;
        WebPager2.DataBind();
    }

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSourceEdit;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.DataSourceEdit;
        WebPager1.DataBind();
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSourceEdit);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSourceEdit = Source.ToTable();
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    private SortDirection Sort_Direction_Edit
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection2.Value))
            {
                SortDirection2.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection2.Value);

        }
        set
        {
            this.SortDirection2.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void btnSaveHoliday_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtHolidayName.Text.Trim()))
            FileFolderManagement.InsertPublicHoliday(txtHolidayName.Text.Trim());
        else
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Holiday name can not be blank." + "\");", true);
        GetData();
        BindGrid();
    }

    protected void lnkBtnSave_Command(object sender, CommandEventArgs e)
    {

    }
    protected void lnkBtnDelete_Command(object sender, CommandEventArgs e)
    {
        try
        {
            LinkButton lnkbtnDelete = sender as LinkButton;
            GridViewRow gvrow = lnkbtnDelete.NamingContainer as GridViewRow;
            int holidayId = Convert.ToInt32(GridView2.DataKeys[gvrow.RowIndex].Value.ToString());

            General_Class.DeletePublicHoliday(holidayId);
            GetData();
            BindGrid();
        }
        catch (Exception ex)
        {
 
        }
        finally
        {
            ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
        }
    }
    
    protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction_Edit == SortDirection.Ascending)
        {
            this.Sort_Direction_Edit = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction_Edit = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();
        BindGrid();
    }
    protected void WebPager2_PageSizeChanged(object s, PageSizeChangeEventArgs e)
    {
        WebPager2.CurrentPageIndex = 0;
        WebPager2.DataSource = this.DataSource;
        WebPager2.DataBind();
    }
    protected void WebPager2_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager2.CurrentPageIndex = e.NewPageIndex;
        WebPager2.DataSource = this.DataSource;
        WebPager2.DataBind();
    }
   
    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
            string holidayId = drv.Row[0].ToString();
            string fileName = drv.Row[1].ToString();

            TextBox txtDocumentName = (e.Row.FindControl("TextDocumentName") as TextBox);

            LinkButton lnkBtnUpdate = (e.Row.FindControl("lnkBtnUpdate") as LinkButton);
            if (lnkBtnUpdate != null)
            {
                lnkBtnUpdate.Attributes.Add("onclick", "OnSaveRename('" + txtDocumentName.ClientID + "','" + holidayId + "','" + fileName + "');");
            }
            
        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
            string holidayId = drv.Row[0].ToString();
            string fileName = drv.Row[1].ToString();

            TextBox txtDocumentName = (e.Row.FindControl("textfield15") as TextBox);
            if (!string.IsNullOrEmpty(drv.Row[2].ToString()))
                txtDocumentName.Text = DateTime.Parse(drv.Row[2].ToString()).ToString("MM-dd-yyyy");
            
            LinkButton lnkBtnUpdate = (e.Row.FindControl("lnkBtnUpdate") as LinkButton);
            if (lnkBtnUpdate != null)
            {
                lnkBtnUpdate.Attributes.Add("onclick", "OnSaveRenameDate('" + txtDocumentName.ClientID + "','" + holidayId + "');");
            }
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(hdnYear.Value))
        {
            this.DataSourceEdit = FileFolderManagement.GetPublicHolidayDates(int.Parse(hdnYear.Value));
            AddColumn();
            BindGrid();
        }
    }

}