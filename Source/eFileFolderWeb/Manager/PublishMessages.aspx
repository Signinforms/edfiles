<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    CodeFile="PublishMessages.aspx.cs" Inherits="PublishMessages" Title="" %>

<%@ Import Namespace="System.Data" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register Src="../Controls/UserFind.ascx" TagName="UserFind" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="725px" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                            style="background-repeat: repeat-x;">
                            <div style="background: url(images/inner_b.gif); background-position: right top;
                                background-repeat: no-repeat; margin-top: 10px;">
                                <div style="width: 725px; height: 60px;">
                                    <div class="Naslov_Zelen">
                                        Publish Messages</div>
                                </div>
                                <div class="podnaslovUser" style="padding: 5px; background-color: #f6f5f2;">
                                    Below is a list of all messages published. you can publish messages and edit them here.
                                </div>
                                <div>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <contenttemplate>
                                        <table style="margin: 0px 20px 0px 35px" cellspacing="1" cellpadding="0" width="920"
                                            bgcolor="#ffffff" border="0">
                                            <tbody>
                                            <div tyle="margin-bottom:20px;"><asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OnSelectedIndexChaned_DropDownList1">
                                                <asp:ListItem Text="Welcome Message" Value="1" Selected="true"></asp:ListItem>
                                                <asp:ListItem Text="Left Side Message" Value="2"></asp:ListItem></asp:DropDownList>
                                                <asp:CheckBox id="chkBox1" Text="Enable/disable the selected message" runat="server" />
                                            </div>
                                            <div style="margin-bottom:20px; margin-top:20px;">
                                            <asp:TextBox id="TextBox1" runat="server"  Text="" TextMode="MultiLine" width="90%" height="300px" />
                                            </div>
                                            <div align="center"><asp:Button runat="server" name="btnUpdate" ID="btnUpdate" OnClick="btnUpdate_Click"
                                                Text="Submit"/></div>
                                            </tbody>
                                            </table>
                                        </contenttemplate>
                                        <triggers>
                                          <asp:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click" />
                                          <asp:AsyncPostBackTrigger ControlID="DropDownList1" EventName="SelectedIndexChanged" />
                                         </triggers>
                                    </asp:UpdatePanel>
                                 </div></div>
                        </td>
            <td width="10" align="center">
                <img src="../images/lin.gif" width="1" height="453" vspace="10" /></td>
        </tr>
    </table>
    </td> </tr> </table>
</asp:Content>
