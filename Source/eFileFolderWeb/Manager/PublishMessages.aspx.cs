using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Framework.Controls;
using Shinetech.Utility;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;
using Account=Shinetech.DAL.Account;

public partial class PublishMessages : BasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DropDownList1.SelectedIndex = 0;
            Message msg = new Message(1);
            if (msg.IsExist)
            {
                TextBox1.Text = msg.MainMessage.Value;
                chkBox1.Checked = msg.EnableFlag.Value;
            }
            else
            {
                msg.EnableFlag.Value = false;
                msg.SideMessage.Value = "";
                msg.MainMessage.Value="";
                msg.Create();
            }

        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            Message msg = new Message(1);
            if (msg.IsExist)
            {
                if (DropDownList1.SelectedIndex == 0)
                {

                    msg.MainMessage.Value = TextBox1.Text;
                    msg.EnableFlag.Value = chkBox1.Checked;
                }
                else
                {
                    msg.SideMessage.Value = TextBox1.Text;
                    msg.EnableFlag1.Value = chkBox1.Checked;
                }

                msg.Update();

                string okmsg = "Message : The update succeed.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + okmsg + "\");", true);
            }
        }
        catch(Exception exp)
        {
            string strWrongInfor = "Error : The Update failed." + exp.Message;
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
        }
        
    }

    protected void OnSelectedIndexChaned_DropDownList1(object sender, EventArgs e)
    {
        Message msg = new Message(1);
        if (msg.IsExist)
        {

            if (DropDownList1.SelectedIndex == 0)
            {

                TextBox1.Text = msg.MainMessage.Value;
                chkBox1.Checked = msg.EnableFlag.Value;
            }
            else
            {
                TextBox1.Text = msg.SideMessage.Value;
                chkBox1.Checked = msg.EnableFlag1.Value;
            }
        }
    }
}
