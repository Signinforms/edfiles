<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    EnableSessionState="True" EnableViewState="true" CodeFile="RequestDocument.aspx.cs"
    Inherits="RequestDocument" Title="" %>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<script language="javascript" type="text/javascript">
     function selectAll(obj)
    {
        var theTable  = obj.parentElement.parentElement.parentElement;
        var i;
        var j = obj.parentElement.cellIndex;
        
        for(i=0;i<theTable.rows.length;i++)
        {
            var objCheckBox = theTable.rows[i].cells[j].firstChild;
            if(objCheckBox.checked!=null)objCheckBox.checked = obj.checked;
        }
    }
</script>

    <tr>
        <td height="186" valign="top" style="background-repeat: no-repeat;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="730" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                        style="background-repeat: repeat-x;">
                        <div style="background: url(images/inner_b.gif); background-position: right top;
                            background-repeat: no-repeat; margin-top: 10px;">
                            <div style="height: 55px;">
                                <div class="Naslov_Sin_Wide">
                                    Create eFiles for <%=CurrentClientName%></div>
                            </div>
                            <div class="podnaslov" style="padding: 5px; background-color: #f6f5f2;">
                                </div>
                            <asp:Panel ID="panelMain" runat="server" Visible="true">
                                <div class="tekstDef_upload">
                                    
                                    <asp:UpdatePanel id="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <contenttemplate><asp:HiddenField ID="HiddenField1"  runat="server" />
           <asp:HiddenField ID="HiddenField2"  runat="server" /><table border="0" cellpadding="2" cellspacing="0" width="100%" style="margin:0px 0 10px 10px;">
            <tr>
              <td width="35%" valign="top"><table><tr><td><div class="form_title_2" style="margin-top:0px; margin-bottom:2px">1. Select an edFile</div>
              
              <asp:DropDownList ID="DropDownList1" AutoPostBack="true" width="100%" DataValueField="FolderID"  DataTextField="DisplayName" OnSelectedIndexChanged="DropDownList1_OnSelectedIndexChanged" runat="server">
              </asp:DropDownList></td></tr><tr><td height="100%" width="100%"><div class="form_title_2" style="margin-top:20px; margin-bottom:2px">2. Select a tab for the selected edFile</div>
            <asp:ListBox ID="ListBox1" width="100%" height="250"  DataValueField="DividerID" DataTextField="Name"  AutoPostBack="true" OnSelectedIndexChanged="ListBox1_OnSelectedIndexChanged" runat="server"></asp:ListBox>
              </td></tr></table>
               </td>
               <td width="65%" valign="top"><asp:Label ID="label12" runat="server"></asp:Label>
                <div class="form_title_2" style="margin-top:3px; margin-bottom:2px">3. Select the Requested PDF Files from the Below</div>
               <%-- upload files--%>
               <asp:GridView ID="GridView2" ShowFooter="true" BorderWidth="2" ShowHeader="true"  EmptyDataText="No Found Files"
                   runat="server" Width="100%" AutoGenerateColumns="false" DataKeyNames="FileID,FileName" OnRowDataBound="GridView2_RowDataBound">
                  <PagerSettings Visible="False" />
                  <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                  <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                  <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                  <HeaderStyle  CssClass="form_title" BackColor="#DBD9CF" />
                  <FooterStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                  <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" Checked="false" Enabled="true"/>
                        </ItemTemplate>
                        <HeaderTemplate><input id="CheckAll" type="checkbox" onclick="selectAll(this);" />
                        </HeaderTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" >
                        <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                    <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" >
                        <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                    <asp:BoundField DataField="FileNumber" HeaderText="File#" SortExpression="FileNumber" >
                        <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                    <asp:BoundField DataField="YearDate" HeaderText="Year" SortExpression="YearDate" >
                        <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                    <asp:BoundField DataField="UploadDate" HeaderText="Sync Date" SortExpression="UploadDate" DataFormatString="{0:MM.dd.yyyy}" NullDisplayText="" >
                        <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ConvertDate" HeaderText="Converted Date" SortExpression="ConvertDate" DataFormatString="{0:MM.dd.yyyy}" NullDisplayText="" >
                        <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                    
                  </Columns>
                  <EmptyDataTemplate >  <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                        <tr>
                                            <td align="left" class="podnaslov">
                                                <asp:Label ID="Label1" runat="server" />Found No the Requested Files. 
                                            </td>
                                        </tr>
                                    </table>
                  </EmptyDataTemplate>
              </asp:GridView>
              <cc1:WebPager id="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" PageSize="15" 
               OnPageSizeChanged="WebPager1_PageSizeChanged" CssClass="prevnext" PagerStyle="OnlyNextPrev" ControlToPaginate="GridView2"></cc1:WebPager> 
                  <asp:HiddenField id="SortDirection1" runat="server" Value=""></asp:HiddenField>
            </td>
            </tr>
            <tr><td width="100%" colspan="2"><div align="center" style="height: 30px;">
                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/create.gif"
                            AlternateText="Create" OnClick="ImageButton1_Click" />
                    </div></td></tr>
          </table></contenttemplate>
             </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
            </td>
        </tr>
        </table>
        </td>
    </tr>
    
 
</asp:Content>
