using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using Shinetech.DAL;
using Shinetech.Engines.Adapters;
using Shinetech.Framework.Controls;


public partial class RequestDocument : System.Web.UI.Page
{
    public string FolderId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            //string uid =(string) Session["FileForm_UID"];
            string fileID = (string)Session["FileForm_FID"];

            FormFile file = new FormFile(Convert.ToInt32(fileID));
            
            if (file.IsExist)
            {

                DataTable table = GetMyEFileFolders(file.UserID.Value);

                DropDownList1.DataSource = table.DefaultView;
                DropDownList1.DataBind();

                DataBindDividers();

                GetData(file.FileID.Value.ToString());
                BindGrid();

            }

        }
    }

    public string CurrentClientName
    {
        get
        {
            return (string)this.Session["CurrentClientName"];
        }

        set
        {
            this.Session["CurrentClientName"] = value;
        }
    }

    public DataTable GetMyEFileFolders(string uid)
    {
        Account account = new Account(uid);
        DataTable effTable = null;
        this.CurrentClientName = account.Name.Value;
        effTable = FileFolderManagement.GetMyFileFoldersWithDisplayName(uid);

        return effTable;
    }

    public DataTable GetDividersByEFF(int eff)
    {
        DataTable dividerTable = FileFolderManagement.GetDividers(eff);
        return dividerTable;
    }

    protected void DropDownList1_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DataBindDividers();
    }

    public void DataBindDividers()
    {
        int effid = Convert.ToInt32(DropDownList1.SelectedValue);
        this.EffID = DropDownList1.SelectedValue;

        ListBox1.DataSource = GetDividersByEFF(effid).DefaultView;
        ListBox1.DataBind();

        if (ListBox1.Items.Count > 0) ListBox1.SelectedIndex = 0;
      
        this.DividerID = ListBox1.SelectedValue;
    }

    protected void ListBox1_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        this.DividerID = ListBox1.SelectedValue;
    }

    public string DividerID
    {
        get
        {
            return (string)this.Session["DividerID_Request"] ;
        }
        set
        {
            Session["DividerID_Request"] = value;
        }
    }

    public string EffID
    {
        get
        {
            return (string)Session["EFFID_Request"];
        }
        set
        {
            Session["EFFID_Request"] = value;
           
        }
    }

    private DataTable MakeDataTable()
    {
        // Create new DataTable.
        DataTable table = new DataTable();

        // Declare DataColumn and DataRow variables.
        DataColumn column;

        // Create new DataColumn, set DataType, ColumnName
        // and add to DataTable.    
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FileUID";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "FilePath";
        table.Columns.Add(column);


        column = new DataColumn();
        column.DataType = Type.GetType("System.Double");
        column.ColumnName = "Size";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.DateTime");
        column.ColumnName = "DOB";
        table.Columns.Add(column);

        return table;
    }


    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["FormFileSource"] as DataTable;
        }
        set
        {
            this.Session["FormFileSource"] = value;
        }
    }

    private void GetData(string fileId)
    {
        this.DataSource = FileFolderManagement.GetFormFilesById(fileId);
    }

    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    protected void GridView2_RowDataBound(Object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = e.Row.DataItem as DataRowView;
            string fileName = drv["FileName"] == System.DBNull.Value ? "" :(string) drv["FileName"];

            CheckBox box = e.Row.Cells[0].FindControl("CheckBox1") as CheckBox;
            if (box == null) return;

            bool status = false;

            try
            {
                string fullPath = ConfigurationManager.AppSettings["UploadPath"];

                if (fileName.Trim() != "")
                {
                    fullPath = Path.Combine(fullPath, fileName);
                    if (File.Exists(fullPath))
                    {
                        status = true;                                        
                    }
                }
                else // if not existing, we search the server folder
                {
                    string firstName, lastName, uid;
                    firstName = drv["FirstName"] == System.DBNull.Value ? "" :(string) drv["FirstName"];
                    lastName = drv["LastName"] == System.DBNull.Value ? "" : (string)drv["LastName"];
                    uid = drv["UserID"] == System.DBNull.Value ? "": ((Guid)(drv["UserID"])).ToString("D");

                    DataTable table ;
                    table = FileFolderManagement.GetUploadFile(firstName, lastName, uid);

                    if(table.Rows.Count>0)
                    {
                        fileName = (string) table.Rows[0]["FileName"];
                        fullPath = Path.Combine(fullPath, fileName);

                        status = File.Exists(fullPath);

                        if(status)
                        {
                            drv.BeginEdit();
                            drv["FileName"] = fileName;
                            drv.EndEdit();
                        }
                    }
                    else
                    {
                        status = false;
                    }

                }
            }catch{}
            
            box.Enabled = status;
            box.Checked = status;
           
            
        }

    }


    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            foreach (GridViewRow item in GridView2.Rows)
            {
                CheckBox box = item.Cells[0].Controls[1] as CheckBox;
                if (box != null)
                {
                    int FileID = Convert.ToInt32(GridView2.DataKeys[item.RowIndex].Value);

                    object obj = GridView2.DataKeys[item.RowIndex].Values[1];


                    string file = (string) obj; //OfficeUser\request_form.pdf
                    string path = ConfigurationManager.AppSettings["UploadPath"];
                        //C:\Upload\OfficeUser\request_form.pdf
                    string vpath = Server.MapPath("~/Volume");
                    //string pathname = string.Format("{0}\\{1}\\{2}", vpath, this.EffID, this.DividerID);

                    string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, this.EffID,
                                                   Path.DirectorySeparatorChar, this.DividerID);

                    if (!Directory.Exists(dstpath))
                    {
                        Directory.CreateDirectory(dstpath);
                    }

                    string fullFileName = path = Path.Combine(path, file);
                    string fileName = Path.GetFileName(fullFileName); //request_form.pdf
                 
                    path = Path.GetDirectoryName(fullFileName);
                    string dstFile = Path.Combine(dstpath, fileName);

                    File.Copy(fullFileName, dstFile, true);

                    //file,this.EffID,this.DividerID, Session["FileForm_UID"],Session["FileForm_FID"],FileID
                    ImageAdapter.ProcessPostFile(dstFile, this.EffID, this.DividerID, (string) Session["FileForm_UID"],
                                                 dstpath, fileName, FileID);
                }
            }

            FileForm form = new FileForm(Convert.ToInt32((string)Session["FileForm_FID"]));
            if(form.IsExist)
            {
                form.ConvertDate.Value = DateTime.Now;
                form.Update();
            }
            Response.Redirect("~/Manager/ViewFileForms.aspx");
        }
        catch (Exception exp)
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + exp.Message + "\");", true);
        }
    }
}
