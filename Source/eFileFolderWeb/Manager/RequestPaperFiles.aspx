<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RequestPaperFiles.aspx.cs" Inherits="Manage_RequestPaperFiles" Title="" %>

<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.ComponentModel" %>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">
        function openwindow(id) {
            var url = 'ModifyBox.aspx?id=' + id;
            var name = 'ModifyBox';
            window.open(url, name, '');
        }

    </script>
    <table>
        <tr>
            <td valign="top" style="background-repeat: no-repeat;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="925" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                            style="background-repeat: repeat-x;">
                            <div style="background: url(images/inner_b.gif); background-position: right top; background-repeat: no-repeat; margin-top: 10px;">
                                <div style="width: 825px; height: 75px;">
                                    <div class="Naslov_Protokalev">
                                        File Requests
                                    </div>
                                </div>
                                <div class="Naslov_Plav" style="width: 750px;">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 0px 0 20px 25px;">
                                        <tr>
                                            <td height="20" align="left" valign="top" class="tekstDef" style="padding-top: 0px; padding-bottom: 0px;">First Name</td>
                                            <td align="left" class="podnaslov">
                                                <input name="textfieldFirstName" type="text" runat="server" class="form_tekst_field" id="textfieldFirstName" size="20" maxlength="50" /></td>
                                            <td height="20" align="left" valign="top" class="tekstDef" style="padding-top: 0px; padding-bottom: 0px;">Last Name</td>
                                            <td align="left" class="podnaslov">
                                                <input name="textfieldLastName" type="text" runat="server" class="form_tekst_field" id="textfieldLastName" size="20" maxlength="50" /></td>
                                            <td width="128"></td>
                                        </tr>
                                        <tr>
                                            <td height="20" align="left" valign="top" class="tekstDef" style="padding-top: 0px; padding-bottom: 0px;">File Name</td>
                                            <td align="left" class="podnaslov">
                                                <input name="textfieldFileName" type="text" runat="server" class="form_tekst_field" id="textfieldFileName" size="20" maxlength="50" /></td>
                                            <td height="20" align="left" valign="top" class="tekstDef" style="padding-top: 0px; padding-bottom: 0px;">File Number</td>
                                            <td align="left" class="podnaslov">
                                                <input name="textfieldFileNumber" type="text" runat="server" class="form_tekst_field" id="textfieldFileNumber" size="20" maxlength="50" /></td>
                                            <td width="128"></td>
                                        </tr>
                                        <tr>
                                            <td height="20" align="left" valign="top" class="tekstDef" style="padding-top: 0px; padding-bottom: 0px;">Box Name</td>
                                            <td align="left" class="podnaslov">
                                                <input name="textfieldBoxName" type="text" runat="server" class="form_tekst_field" id="textfieldBoxName" size="20" maxlength="50" /></td>
                                            <td height="20" align="left" valign="top" class="tekstDef" style="padding-top: 0px; padding-bottom: 0px;">Box Number</td>
                                            <td align="left" class="podnaslov">
                                                <input name="textfieldBoxNumber" type="text" runat="server" class="form_tekst_field" id="textfieldBoxNumber" size="20" maxlength="50" /></td>
                                            <td width="128">
                                                <asp:ImageButton ID="searchButton1" OnClick="OnSearchButton_Click" runat="server" ImageUrl="~/images/search_btn.gif" Width="60" Height="21" /></td>
                                        </tr>
                                    </table>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table style="margin: 0px 20px 0px 30px; width: 925px">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GridView1" runat="server" DataKeyNames="FileID" OnSorting="GridView1_Sorting" AllowSorting="True" AutoGenerateColumns="false" Width="100%" BorderWidth="0">
                                                            <PagerSettings Visible="False" />
                                                            <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                            <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                            <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                            <Columns>

                                                                <asp:BoundField DataField="FileID" HeaderText="FileID" SortExpression="FileID">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>

                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="30px"
                                                                    ItemStyle-CssClass="table_tekst_edit">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="Label34" Text="BoxName" runat="server" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton21" runat="server" Text='<%# Bind("BoxName") %>' CausesValidation="false"
                                                                            OnClientClick='<%#string.Format("javascript:openwindow({0})",Eval("BoxId"))%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:BoundField DataField="BoxNumber" HeaderText="BoxNumber" SortExpression="BoxNumber">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="Name" HeaderText="OfficeName" SortExpression="Name">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="FileName" HeaderText="FileName" SortExpression="FileName">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="FileNumber" HeaderText="FileNumber" SortExpression="FileNumber">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="RequestDate" HeaderText="RequestDate" SortExpression="RequestDate">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>

                                                                <asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="Label11" Text="Action" runat="server" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton5" runat="server" CausesValidation="false" Text="Create" OnCommand="LinkButton5_Click" CommandArgument='<%# Eval("FileID")%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                                    <tr>
                                                                        <td align="left" class="podnaslov">
                                                                            <asp:Label ID="Label1" runat="server" />There is no Stored Paper File. 
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </EmptyDataTemplate>

                                                        </asp:GridView>

                                                        </asp:Panel>
                                                        <!-- Tabs Editor Panel-->
                                                        <asp:Button Style="display: none" ID="Button1ShowPopup" runat="server"></asp:Button>
                                                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
                                                            CancelControlID="btnClose1" PopupControlID="Panel1Tabs" TargetControlID="Button1ShowPopup">
                                                        </ajaxToolkit:ModalPopupExtender>
                                                        <!-- ModalPopup Panel-->
                                                        <asp:Panel Style="display: none; z-index: 10001" ID="Panel1Tabs" runat="server" Width="600px" BackColor="lightblue">
                                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <div id="divTabNum">&nbsp;File Details</div>

                                                                </ContentTemplate>

                                                            </asp:UpdatePanel>
                                                            <div align="right" style="width: 95%; background-color: lightblue">
                                                                <asp:Button ID="btnClose1" runat="server" Text="Close" Width="50px" />
                                                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                                            </div>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="prevnext" bgcolor="#f8f8f5">
                                                        <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" PageSize="15"
                                                            OnPageSizeChanged="WebPager1_PageSizeChanged" CssClass="prevnext" PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1"></cc1:WebPager>
                                                        <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </td>

                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
