<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    CodeFile="RestoreRecycleBin.aspx.cs" Inherits="RestoreRecycleBin" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript">
        function openwindow(id) {
            var url = '../Office/ModifyBox.aspx?id=' + id;
            var name = 'ModifyBox';
            window.open(url, name, '');
        }

        function setConversionText() {
            var obj = document.getElementById("<%= lbsStatus.ClientID %>");
            obj.setAttribute("src", "../Images/wait24trans.gif");
            obj.setAttribute("Visible", "true");
            obj.innerHTML = " (Converting, Please Wait.)";
        }

        function GetArgsFromHref(sHref, sArgName) {
            var args = sHref.split("?");
            var retval = "";

            if (args[0] == sHref) /*参数为空*/ {
                return retval; /*无需做任何处理*/
            }
            var str = args[1];
            args = str.split("&");
            for (var i = 0; i < args.length; i++) {
                str = args[i];
                var arg = str.split("=");
                if (arg.length <= 1) continue;
                if (arg[0] == sArgName) retval = arg[1];
            }
            return retval;
        }

    </script>
    <%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
        TagPrefix="cc1" %>
    <table>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="725px" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                            style="background-repeat: repeat-x;">
                            <div style="background: url(images/inner_b.gif); background-position: right top; background-repeat: no-repeat; margin-top: 10px;">
                                <div style="width: 725px; height: 75px;">
                                    <div class="Naslov_Zelen">
                                        Search EdFiles
                                    </div>
                                </div>
                                <ajaxToolkit:TabContainer runat="server" ID="fileTabs" ActiveTabIndex="0"
                                    Width="100%">
                                    <ajaxToolkit:TabPanel runat="server" ID="Panel1" HeaderText="EdFiles">
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="UpdatePanelResult" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Panel ID="PanelInfor" runat="server" Visible="false">
                                                        <div class="tekstDef" style="">
                                                            <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                                <tr>
                                                                    <td align="left" class="podnaslov">
                                                                        <asp:Label ID="Label1" runat="server" />There is no edFile be found.
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:HiddenField ID="txtHasPrivilage" runat="server" />
                                                    <table width="725" border="0" cellpadding="0" cellspacing="1" bgcolor="#FFFFFF" style="margin: 0 20px 0 10px;">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="GridView1" BorderWidth="2" runat="server" Width="100%" AutoGenerateColumns="false"
                                                                    AllowSorting="True" OnSorting="GridView1_Sorting"
                                                                    DataKeyNames="FolderID" OnRowDeleting="GridView1_RowDeleting">
                                                                    <PagerSettings Visible="False" />
                                                                    <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                                    <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                                    <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                                    <Columns>
                                                                        <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                            ItemStyle-CssClass="table_tekst_edit">
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="Label8" runat="server" Text="Action" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="libDelete" runat="server" Text="Restore" CausesValidation="false"
                                                                                    CommandName="Delete"></asp:LinkButton>
                                                                                <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                                    CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                                    PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                                </ajaxToolkit:ModalPopupExtender>
                                                                                <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                                    ConfirmText="" TargetControlID="libDelete">
                                                                                </ajaxToolkit:ConfirmButtonExtender>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="FolderName" HeaderText="FolderName" SortExpression="FolderName">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="FileNumber" HeaderText="FileNumber" SortExpression="FileNumber">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="DeleteDate" HeaderText="DeleteDate" DataFormatString="{0:MM.dd.yyyy}"
                                                                            SortExpression="DeleteDate" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F8F8F5" class="prevnext">
                                                                <cc1:WebPager ID="WebPager1" ControlToPaginate="GridView1" PagerStyle="OnlyNextPrev"
                                                                    CssClass="prevnext" PageSize="25" OnPageSizeChanged="WebPager1_PageSizeChanged"
                                                                    OnPageIndexChanged="WebPager1_PageIndexChanged" runat="server" />
                                                                <asp:HiddenField ID="SortDirection1" runat="server" Value="" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                                <p></p>
                                <ajaxToolkit:TabContainer runat="server" ID="TabContainer1" ActiveTabIndex="0"
                                    Width="100%">
                                    <ajaxToolkit:TabPanel runat="server" ID="Panel2" HeaderText="Documents">
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table width="725" border="0" cellpadding="0" cellspacing="1" bgcolor="#FFFFFF" style="margin: 0 20px 0 10px;">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="GridView2" runat="server" DataKeyNames="DocumentID" OnSorting="GridView2_Sorting"
                                                                    AllowSorting="True" AutoGenerateColumns="false" Width="100%" BorderWidth="0" OnRowDeleting="GridView12_RowDeleting" >
                                                                    <PagerSettings Visible="False" />
                                                                    <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                                    <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                                    <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                                    <Columns>
                                                                        <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                            ItemStyle-CssClass="table_tekst_edit">
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="Label8" runat="server" Text="Action" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="libDelete" runat="server" Text="Restore" CausesValidation="false"
                                                                                    CommandName="delete"></asp:LinkButton>
                                                                                <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                                    CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                                    PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                                </ajaxToolkit:ModalPopupExtender>
                                                                                <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                                    ConfirmText="" TargetControlID="libDelete">
                                                                                </ajaxToolkit:ConfirmButtonExtender>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="FolderName" HeaderText="FolderName" SortExpression="FolderName">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                       
                                                                        <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="DeleteDate" HeaderText="DeleteDate" DataFormatString="{0:MM.dd.yyyy}"
                                                                            SortExpression="DeleteDate" />
                                                                    </Columns>
                                                                    <EmptyDataTemplate>
                                                                        <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                                            <tr>
                                                                                <td align="left" class="podnaslov">
                                                                                    <asp:Label ID="Label1" runat="server" />There is no Stored File.
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F8F8F5" class="prevnext">
                                                                <cc1:WebPager ID="WebPager2" runat="server" OnPageIndexChanged="WebPager2_PageIndexChanged"
                                                                    OnPageSizeChanged="WebPager2_PageSizeChanged" PageSize="25" CssClass="prevnext"
                                                                    PagerStyle="OnlyNextPrev" ControlToPaginate="GridView2"></cc1:WebPager>
                                                                <asp:HiddenField ID="SortDirection2" runat="server" Value=""></asp:HiddenField>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="WebPager2" EventName="PageIndexChanged" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>
                                <p></p>
                                <ajaxToolkit:TabContainer runat="server" ID="TabContainer2" ActiveTabIndex="0"
                                    Width="100%">
                                    <ajaxToolkit:TabPanel runat="server" ID="Panel3" HeaderText="Workarea">
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="UpdatePanel13" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Image runat="server" ImageUrl="../Images/loader_bg.gif" ID="lbsStatus" ImageAlign="Middle" />
                                                    <table width="725" border="0" cellpadding="0" cellspacing="1" bgcolor="#FFFFFF" style="margin: 0 20px 0 10px;">
                                                        <tr>
                                                            <td>
                                                                <asp:GridView ID="GridView3" runat="server" OnSorting="GridView3_Sorting" AllowSorting="True"
                                                                    AutoGenerateColumns="false" Width="100%" BorderWidth="2">
                                                                    <PagerSettings Visible="False" />
                                                                    <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                                    <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                                    <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                                    <Columns>
                                                                        <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="Label8" runat="server" Text="Action" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="LinkButton23" runat="server" Text="Restore" CommandArgument='<%# Eval("FilePath")%>'
                                                                                    OnCommand="LinkButton23_Click"></asp:LinkButton>
                                                                                <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                                    CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="LinkButton23"
                                                                                    PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                                </ajaxToolkit:ModalPopupExtender>
                                                                                <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                                    ConfirmText="" TargetControlID="LinkButton23">
                                                                                </ajaxToolkit:ConfirmButtonExtender>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="FileUID" HeaderText="FileName" SortExpression="FileUID">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Size" HeaderText="Size(Mb)" DataFormatString="{0:F2}"
                                                                            HtmlEncode="false" />
                                                                        <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="DeleteDate" HeaderText="DeleteDate" DataFormatString="{0:MM.dd.yyyy}"
                                                                            SortExpression="DOB" />
                                                                    </Columns>
                                                                    <EmptyDataTemplate>
                                                                        <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                                            <tr>
                                                                                <td align="left" class="podnaslov">
                                                                                    <asp:Label ID="Label120" runat="server" />There are no files. right now.
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor="#F8F8F5" class="prevnext">
                                                                <cc1:WebPager ID="WebPager3" runat="server" OnPageIndexChanged="WebPager3_PageIndexChanged"
                                                                    PageSize="25" CssClass="prevnext" PagerStyle="OnlyNextPrev" ControlToPaginate="GridView3"></cc1:WebPager>
                                                                <asp:HiddenField ID="SortDirection3" runat="server" Value=""></asp:HiddenField>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="WebPager3" EventName="PageIndexChanged" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                </ajaxToolkit:TabContainer>
                                <div class="tekstDef" style="">
                                </div>
                            </div>
                        </td>
                       
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <asp:Panel class="popupConfirmation" ID="DivDeleteConfirmation" Style="display: none"
        runat="server">
        <div class="popup_Container">
            <div class="popup_Titlebar" id="PopupHeader">
                <div class="TitlebarLeft">
                    Restore Folder
                </div>
                <div class="TitlebarRight" onclick=" $get('ButtonDeleteCancel').click(); ">
                </div>
            </div>
            <div class="popup_Body">
                <p>
                    Are you sure you want to restore this folder. Once you restore this file,
                    it will be restored for good
                </p>
                <p>
                    Confirm Password:
                    <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                </p>
            </div>
            <div class="popup_Buttons">
                <input id="ButtonDeleleOkay" type="button" value="Yes" style="margin-left: 80px; width: 65px" />
                <input id="ButtonDeleteCancel" type="button" value="No" style="margin-left: 20px; width: 65px" />
            </div>
        </div>
    </asp:Panel>



</asp:Content>
