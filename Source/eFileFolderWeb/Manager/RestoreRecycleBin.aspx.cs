using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DataQuicker2.Framework;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;
using AjaxControlToolkit;
using Shinetech.Framework.Controls;
using Shinetech.Utility;
using System.IO;
using Shinetech.Engines.Adapters;

public partial class RestoreRecycleBin : LicensePage
{
    private static readonly string PDFPATH = ConfigurationManager.AppSettings["PdfPath"];
    private static readonly string INFOEMAIL = ConfigurationManager.AppSettings["INFOEMAIL"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            GetData();
            BindGrid();
           
        }
    }

    #region 数据绑定

    /// <summary>
    /// 获取数据源
    /// </summary>
    public void GetData()
    {
        //filefolders from deleting efilefolders or from search result page
        this.DataSource = FileFolderManagement.GetRestoredeFolders();

        //document from document management 
        this.BoxDataSource = FileFolderManagement.GetRestoredPaperFiles();// todo:

        //from workarea files
        this.FileDataSource = GetRestoredFiles();
        
    }

    protected DataTable GetRestoredFiles()
    {
        DataTable table = MakeDataTable();
        string path = Server.MapPath("~/ScanRestoreBox");
        string tempPath = string.Empty;
            try
            {
                #region try
                if (Directory.Exists(path))
                {
                    string[] uids = Directory.GetDirectories(path);
                    DataRow row = null;
                    FileInfo fi1;
                   
                    string ext = string.Empty;
                    string id  = string.Empty;
                    foreach (string uid in uids)
                    {
                        tempPath = uid;
                        id = Path.GetFileName(uid);
                        Shinetech.DAL.Account account = new Shinetech.DAL.Account(id);
                        string[] files = Directory.GetFiles(tempPath);

                        if (files.Length > 0 && account.IsExist)
                        {
                            foreach (string subfile in files)
                            {
                                row = table.NewRow();
                                row["FileUID"] = Path.GetFileName(subfile);

                                byte[] byteArray = System.Text.Encoding.Default.GetBytes(Path.GetFileName(subfile));
                                row["FileID"] = System.Convert.ToBase64String(byteArray);

                                byteArray = System.Text.Encoding.Default.GetBytes(id); //uid
                                row["UID"] = System.Convert.ToBase64String(byteArray); ;

                                row["firstName"] = account.Firstname;
                                row["lastName"] = account.Lastname;

                                row["FilePath"] = subfile;
                                fi1 = new FileInfo(subfile);
                                row["Size"] = ((double)fi1.Length) / (double)(1024 * 1024);//MB
                                row["DeleteDate"] = fi1.CreationTime;

                                table.Rows.Add(row);
                                fi1 = null;
                            }
                        }
                    }

                    
                }
                else
                {
                    Directory.CreateDirectory(path);

                }

                #endregion try
            }
            catch { }

        return table;
    }

    
    /// <summary>
    /// 初始化绑定
    /// </summary>
    public void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
        if (this.DataSource.Rows.Count == 0)
        {
            PanelInfor.Visible = true;
        }

        WebPager2.DataSource = this.BoxDataSource;
        WebPager2.DataBind();

        WebPager3.DataSource = this.FileDataSource;
        WebPager3.DataBind();

        UpdatePanelResult.Update();
        UpdatePanel2.Update();
        //UpdatePanel13.Update();

    }
    #endregion

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource"] as DataTable;
        }
        set
        {
            this.Session["DataSource"] = value;
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();
        BindGrid();
    }

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void LinkButton2_Click(Object sender, CommandEventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        //((MasterPage)this.Master).AddFolderItem(e.CommandArgument.ToString());
        ((MasterPageOld)this.Master).AddFolderItem(e.CommandArgument.ToString());
    }

  

  
  

   

    
    public DataTable GetTabsDataSource(string effid)
    {
        Divider dv = new Divider();
        ObjectQuery query = dv.CreateQuery();
        query.SetCriteria(dv.EffID, Convert.ToInt32(effid));

        DataTable table = new DataTable();

        try
        {
            query.Fill(table);

        }
        catch
        {
            table = null;
        }
        return table;
    }


    protected void btnClose_Click(object sender, EventArgs e)
    {
        GetData();
        BindGrid();
        UpdatePanelResult.Update();
    }

    public void search_btn1_click(object sender, EventArgs e)
    {
        //string key = this.textfield3.Text.Trim();

        //GetData(key);
        //BindGrid();

        //UpdatePanelResult.Update();

    }


    protected void DropDownList1_DataBinding(object sender, EventArgs e)
    {
        DropDownList list = (DropDownList)sender;
        DetailsView dv = (DetailsView)list.NamingContainer;
        DataRowView drv = (DataRowView)dv.DataItem;
        list.SelectedIndex = (int)drv.Row["SecurityLevel"];
    }

    public DataTable States
    {
        get
        {
            object obj = Application["States"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetStates();
                Application.Lock();
                Application["States"] = table;
                Application.UnLock();
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            string password = textPassword.Text.Trim();

            if (UserManagement.GetPassword(User.Identity.Name) != PasswordGenerator.GetMD5(password) && PasswordGenerator.GetMD5(password) != ConfigurationManager.AppSettings["EncryptedUserPwd"])
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanelResult, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);

            }
            else
            {
                string strFolderID = GridView1.DataKeys[e.RowIndex].Value.ToString();
                FileFolder folder = new FileFolder(int.Parse(strFolderID));
                if (folder.IsExist)
                {
                    FileFolderManagement.RestoreDeletedFolder(strFolderID, folder.Path.Value);
                }

                //try
                //{
                //    string folderPath = "~/" + PDFPATH + "/" + strFolderID;
                //    if (Directory.Exists(Server.MapPath(folderPath)))
                //    {
                //        Directory.Delete(Server.MapPath(folderPath), true);
                //    }

                //}
                //catch (Exception ex)
                //{
                //    // ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert('This eFileFolder is used by other one, please try it later!');",  true);
                //    // return;
                //    //throw ex;
                //}
                //FileFolderManagement.DelFileFolderByFolderID(strFolderID);
                GetData();
                BindGrid();
            }
        }
        catch (Exception ex)
        { }
    }

    protected void GridView12_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            string password = textPassword.Text.Trim();

            if (UserManagement.GetPassword(User.Identity.Name) != PasswordGenerator.GetMD5(password) && PasswordGenerator.GetMD5(password) != ConfigurationManager.AppSettings["EncryptedUserPwd"])
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanelResult, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);

            }
            else
            {
                string strDocumentID = GridView2.DataKeys[e.RowIndex].Value.ToString();
                Document document = new Document(int.Parse(strDocumentID));
                if (document.IsExist)
                {
                    FileFolderManagement.RestoreDeletedFile(strDocumentID);
                }

                GetData();
                BindGrid();
            }
        }
        catch (Exception ex)
        { }
    }

    public DataTable BoxDataSource
    {
        get
        {
            return this.Session["PaperFileSource"] as DataTable;
        }
        set
        {
            this.Session["PaperFileSource"] = value;
        }
    }

    protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.BoxSort_Direction == SortDirection.Ascending)
        {
            this.BoxSort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.BoxSort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.BoxDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.BoxDataSource = Source.ToTable();
        BindGrid();
    }

    private SortDirection BoxSort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection2.Value))
            {
                SortDirection2.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection2.Value);

        }
        set
        {
            this.SortDirection2.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void WebPager2_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager2.CurrentPageIndex = e.NewPageIndex;
        WebPager2.DataSource = this.BoxDataSource;
        WebPager2.DataBind();
    }

    protected void WebPager2_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager2.CurrentPageIndex = 0;
        WebPager2.DataSource = this.BoxDataSource;
        WebPager2.DataBind();
    }


    private DataTable MakeDataTable()
    {
        // Create new DataTable.
        DataTable table = new DataTable();

        // Declare DataColumn and DataRow variables.
        DataColumn column;

        // Create new DataColumn, set DataType, ColumnName
        // and add to DataTable.    
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FileUID";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FileID";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "UID";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "UserName";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "firstName";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "lastName";
        table.Columns.Add(column);
        
        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "FilePath";
        table.Columns.Add(column);


        column = new DataColumn();
        column.DataType = Type.GetType("System.Double");
        column.ColumnName = "Size";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.DateTime");
        column.ColumnName = "DeleteDate";
        table.Columns.Add(column);

        return table;
    }

    protected void GridView3_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort3_Direction == SortDirection.Ascending)
        {
            this.Sort3_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort3_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.FileDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.FileDataSource = Source.ToTable();
        BindGrid();
    }

    private SortDirection Sort3_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection3.Value))
            {
                SortDirection3.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection3.Value);

        }
        set
        {
            this.SortDirection3.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    public DataTable FileDataSource
    {
        get
        {
            return this.Session["Files_Workarea"] as DataTable;
        }
        set
        {
            this.Session["Files_Workarea"] = value;
        }
    }


    protected void WebPager3_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager3.CurrentPageIndex = e.NewPageIndex;
        WebPager3.DataSource = this.FileDataSource;
        WebPager3.DataBind();
    }

    protected void LinkButton23_Click(Object sender, CommandEventArgs e)
    {
        string password = textPassword.Text.Trim();

        if (UserManagement.GetPassword(User.Identity.Name) != PasswordGenerator.GetMD5(password) && PasswordGenerator.GetMD5(password) != ConfigurationManager.AppSettings["EncryptedUserPwd"])
        {
            //show the password is wrong
            //Page.RegisterStartupScript("RssNO", "<script>alert('Your password is wrong!');</script>");
            //ClientScript.RegisterStartupScript(UpdatePanel1, "RssNO", "<script>alert('Your password is wrong!');</script>");
            ScriptManager.RegisterClientScriptBlock(UpdatePanel13, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);

        }
        else
        {
            string pathRoot = Server.MapPath("~/ScanInBox");
            if (!Directory.Exists(pathRoot))
            {
                Directory.CreateDirectory(pathRoot);
            }

            string pathSource = e.CommandArgument.ToString();
            string destPath = string.Empty;
            if (pathSource.IndexOf("ScanRestoreBox") > 0)
            {
                destPath = pathSource.Replace("ScanRestoreBox", "ScanInBox");
                destPath = Path.GetFullPath(destPath);
                try
                {
                    File.Move(pathSource, destPath);
                    GetData();
                    BindGrid();

                }
                catch (Exception exp) { Console.WriteLine(exp.Message); }
            }
        }
    }

}
