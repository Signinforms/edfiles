using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using AjaxControlToolkit;
using DataQuicker2.Framework;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;

public partial class Manage_StoredPaperFiles : BasePage
{
    private static readonly string PDFPATH = ConfigurationManager.AppSettings["PdfPath"];
    //public string IsShowInsert;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
    }

    #region 数据绑定
    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData()
    {
        this.DataSource = FileFolderManagement.GetPaperFiles();
    }
    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }
    #endregion

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["StoredPaperFiles"] as DataTable;
        }
        set
        {
            this.Session["StoredPaperFiles"] = value;
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

 
    //File Quantity
    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        //LinkButton btnLink = sender as LinkButton;
        //if (btnLink == null) return;
        //GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        //if (row == null) return;
        //string formId = Convert.ToString(this.GridView1.DataKeys[row.RowIndex].Value);
        //HiddenField1.Value = formId;

        //DataView view = new DataView(GetFilesDataSource(formId));
        //GridView2.DataSource = view;

        //GridView2.DataBind();
        //UpdatePanel3.Update();
        //ModalPopupExtender1.Show();

    }

    //Resolver File Request for clients
    protected void LinkButton5_Click(object sender, CommandEventArgs e)
    {
        string pid = e.CommandArgument.ToString();
        string uid = Membership.GetUser().ProviderUserKey.ToString();

        PaperFile file = new PaperFile(Convert.ToInt32(pid));

        if (file.IsExist)
        {
            Account account = new Account(file.UserID.Value);
            string userId = account.UID.Value;

            this.Response.Redirect(string.Format("~/Office/RequestPaperFile.aspx?id={0} &uid={1}", pid, userId));
        }
        
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        GetData();
        BindGrid();
        UpdatePanel1.Update();
    }


    protected void OnSearchButton_Click(object sender, ImageClickEventArgs e)
    {
        string firstName = this.textfieldFirstName.Value.Trim();
        string lastName = this.textfieldLastName.Value.Trim();

        string boxName = this.textfieldBoxName.Value.Trim();
        string boxNumber = this.textfieldBoxNumber.Value.Trim();

        string fileName = this.textfieldFileName.Value.Trim();
        string fileNumber = this.textfieldFileNumber.Value.Trim();

        string userId = Membership.GetUser().ProviderUserKey.ToString();

        this.DataSource = FileFolderManagement.GetPaperFiles(userId, firstName, lastName, boxName, boxNumber,fileName, fileNumber);

        this.BindGrid();
    }

   
}
