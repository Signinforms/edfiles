﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TemplateDetails.aspx.cs" Inherits="Manager_TemplateDetails" MasterPageFile="~/MasterPageOld.master" %>

<%@ Import Namespace="System.Data" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <%--<link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/style.css" rel="stylesheet" type="text/css" />--%>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style>
        .cp_button {
            background-image: url(../../images/cp_button.png);
            width: 16px !important;
            height: 16px;
            border: none !important;
            background-repeat: no-repeat;
            /* margin-top: 14px; */
            margin: 10px;
        }

        table {
            white-space: normal;
            line-height: normal;
            font-weight: normal;
            font-size: medium;
            font-variant: normal;
            font-style: normal;
            color: -internal-quirk-inherit;
            text-align: start;
        }

        .color-label {
            float: left;
            height: 30px;
            padding: 8px 10px 0px 5px;
            vertical-align: middle;
            width: auto !important;
        }

        .txLbl {
            font-size: 15px;
            font-weight: 600;
            margin-left: 10px;
        }

        .tabDropDown {
            height: 25px;
            width: 50px;
            padding-left: 10px;
            margin-left: 5px;
        }

        .tempBox {
            padding: 0 5px;
            height: 23px;
            width: 170px;
            margin-left: 5px;
        }

        .create-btn {
            color: #fff;
            cursor: pointer;
            font-size: 20px;
            min-width: 100px;
            text-align: center;
            width: auto;
        }

        img {
            margin-top: 5px !important;
        }

        .colorPckr {
            height: 30px;
        }

        .colorBox div {
            margin-top: 3px;
        }

        .btn-small {
            border: 0 none;
            color: #fff;
            height: 34px;
            cursor: pointer;
            display: inline-block;
            font-family: "HelveticaNeueLT-Bold";
            font-size: 15px;
            line-height: 28px;
            text-align: center;
            transition: all 0.3s ease 0s;
            vertical-align: top;
            width: auto;
            -moz-appearance: none;
            -ms-appearance: none;
            -o-appearance: none;
            -webkit-appearance: none;
            appearance: none;
            /*margin: 2px 0;*/
            /*margin-left:50px;*/
        }

            .btn.green, .btn-small.green {
                background-color: #83af33;
                font-family: "Open Sans",sans-serif;
                margin-left: 10px;
            }

            /*.btn:hover.green, .btn-small:hover.green
            {
                background-color: #01487f;
                color: #fff;
            }*/

            .btn.blue, .btn-small.blue {
                font-family: "Open Sans",sans-serif;
                background: #01487f;
            }

        /*.btn:hover.blue, .btn-small:hover.blue
            {
                background: #83af33;
                color: #fff;
            }*/

        .Naslov_Protokalev1 {
            font-size: 22px;
            color: #ff7e00;
            padding: 19px 0 0 30px;
            width: 400px;
        }

        input[type="checkbox"] {
            width: 20px;
            height: 20px;
            position: absolute;
        }
    </style>
    <%--<script type="text/javascript">
        $(document).ready(function () {
            $(".color-label").each(function () {
                $(this).parent().children().children().first().children().first().css("height", "25px");
            })
        });
    </script>--%>
    <div style="margin-top: 50px; width: 825px; height: 50px; margin-left: -20px; letter-spacing: 0px !important;">
        <asp:Label runat="server" ID="labelTemplate" CssClass="Naslov_Protokalev1"></asp:Label>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div class="left-content">
                    <asp:Panel ID="panelMain" runat="server" Visible="true">
                        <div id="mainContent" style="margin-bottom: 50%;">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Label ID="Label1" CssClass="txLbl" runat="server">Template Name:</asp:Label>
                                    <asp:TextBox runat="server" CssClass="tempBox" ID="templateTextBox"></asp:TextBox>
                                    <br />
                                    <br />
                                    <br />
                                    <asp:Label CssClass="txLbl" runat="server">Number Of Tabs:</asp:Label>
                                    <asp:DropDownList ID="DropDownList1" runat="server" CssClass="tabDropDown" AutoPostBack="true" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                                        <asp:ListItem Text="1" Value="1" Selected="true"></asp:ListItem>
                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:Label ID="Label2" CssClass="txLbl" runat="server">Lock:</asp:Label>
                                    <asp:CheckBox ID="isLocked" CssClass="lockChkBx" runat="server" />

                                    <asp:RequiredFieldValidator runat="server" ID="templateTextBox1" ControlToValidate="templateTextBox"
                                        Display="Dynamic" ErrorMessage="<br />Template name is required." />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
                                        TargetControlID="templateTextBox1" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    <br />
                                    <br />

                                    <div style="padding: 10px 0px">
                                        <asp:DataList ID="Repeater2" OnUpdateCommand="Repeater2_UpdateCommand" DataKeyField="Name"
                                            OnItemCreated="Repeater2_ItemCreated" runat="server">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <div>
                                                                <label style="padding: 0 5px">Name</label>
                                                                <asp:TextBox Style="padding: 0 5px; height: 23px" ID="textfield4" runat="server" Text='<%# Eval("Name")%>' />
                                                                <asp:RequiredFieldValidator Style="padding: 0 5px" runat="server" ID="textfield4Req" ControlToValidate="textfield4"
                                                                    Display="Dynamic" ErrorMessage="<br />Tab name is required." />
                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                                                    TargetControlID="textfield4Req" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div style="padding: 5px 0px" class="colorBox">
                                                                <div style="display: inline-flex">
                                                                    <label class="color-label">Color</label>
                                                                    <div style="border: solid 1px black; height: 28px; width: 70px">
                                                                        <asp:Label
                                                                            ID="lblSample"
                                                                            AutoCompleteType="None"
                                                                            runat="server"
                                                                            Style="display: block; height: 20px; width: 62px; margin: 3px;" />
                                                                    </div>
                                                                    <asp:TextBox
                                                                        ID="txtCardColor"
                                                                        AutoCompleteType="None"
                                                                        runat="server" Style="height: 0px; width: 0px; border: none" />
                                                                    <asp:Button
                                                                        CssClass="cp_button"
                                                                        ID="btnPickColor"
                                                                        runat="server" />
                                                                    <cc1:ColorPickerExtender
                                                                        ID="ColorPicker2"
                                                                        TargetControlID="txtCardColor"
                                                                        PopupButtonID="btnPickColor"
                                                                        PopupPosition="TopRight"
                                                                        SampleControlID="lblSample"
                                                                        SelectedColor='<%# Eval("Color")%>'
                                                                        OnClientColorSelectionChanged="Color_Changed"
                                                                        Enabled="True"
                                                                        runat="server"></cc1:ColorPickerExtender>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>


                                                </table>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <div>
                                                                <label style="padding: 0 5px">Name</label>
                                                                <asp:TextBox ID="textfield8" Style="padding: 0 5px; height: 23px" runat="server" Text='<%# Eval("Name")%>' />
                                                                <asp:RequiredFieldValidator runat="server" ID="textfield8Req" ControlToValidate="textfield8"
                                                                    Display="Dynamic" ErrorMessage="<br />Tab name is required." />
                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
                                                                    TargetControlID="textfield8Req" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div style="padding: 5px 0px" class="colorBox">
                                                                <div style="display: inline-flex">
                                                                    <label class="color-label">Color</label>
                                                                    <div style="border: solid 1px black; height: 32px; width: 70px">
                                                                        <asp:Label
                                                                            ID="lblSample1"
                                                                            AutoCompleteType="None"
                                                                            runat="server" Style="display: block; height: 22px; width: 62px; margin: 3px;" />
                                                                    </div>
                                                                    <asp:TextBox
                                                                        ID="txtCardColor1"
                                                                        AutoCompleteType="None"
                                                                        runat="server" Style="height: 0px; width: 0px; border: none" />
                                                                    <asp:Button
                                                                        CssClass="cp_button"
                                                                        ID="btnPickColor1"
                                                                        runat="server" />
                                                                    <cc1:ColorPickerExtender
                                                                        ID="ColorPicker3"
                                                                        TargetControlID="txtCardColor1"
                                                                        PopupButtonID="btnPickColor1"
                                                                        PopupPosition="TopRight"
                                                                        SampleControlID="lblSample1"
                                                                        SelectedColor='<%# Eval("Color")%>'
                                                                        OnClientColorSelectionChanged="Color_Changed"
                                                                        Enabled="True"
                                                                        runat="server"></cc1:ColorPickerExtender>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                </div>
                                           
                                            </AlternatingItemTemplate>
                                        </asp:DataList>
                                    </div>
                                    <asp:Button ID="btnSave" CssClass="create-btn btn-small green" runat="server" CausesValidation="false"
                                        Text="Update" OnClick="btnSave_Click" />
                                    <asp:Button ID="btnCancel" CssClass="create-btn btn-small blue" runat="server" CausesValidation="false"
                                        Text="Reset" OnClick="btnCancel_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        //function LoadColorPicker() {
        //This code must be placed below the ScriptManager, otherwise the "Sys" cannot be used because it is undefined.
        Sys.Application.add_init(function () {
            // Store the color validation Regex in a "static" object off of
            // AjaxControlToolkit.ColorPickerBehavior.  If this _colorRegex object hasn't been
            // created yet, initialize it for the first time.
            if (!Sys.Extended.UI.ColorPickerBehavior._colorRegex) {
                Sys.Extended.UI.ColorPickerBehavior._colorRegex = new RegExp('^[A-Fa-f0-9]{6}$');
            }
        });
        //}

        function Color_Changed(sender) {
            sender.get_element().value = "#" + sender.get_selectedColor();
        }
    </script>
</asp:Content>
