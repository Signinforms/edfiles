﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;
using TextBox = System.Web.UI.WebControls.TextBox;
using Shinetech.DAL;

public partial class Manager_TemplateDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            int tempId = Convert.ToInt32(Request.QueryString["id"]);
            //string uid = (Request.QueryString["uid"]).ToString();
            if (tempId != 0)
            {
                GetData(tempId);
            }
            else
            {
                GetDefaultData();
            }
        }
    }

    protected ArrayList Templates
    {
        get
        {
            int tempId = Convert.ToInt32(Request.QueryString["id"]);
            DataTable table = General_Class.GetFolderTemplateByTemplateID(tempId);

            ArrayList temples = new ArrayList();
            TemplateEntity template;
            table.Columns.Add("ColorWithoutHash");
            foreach (DataRow item in table.Rows)
            {
                item["Content"] = Convert.ToString(item["Content"]).Replace("#", "");
                template = new TemplateEntity(item["Name"] as string);
                template.TemplateID = (int)item["TemplateID"];
                template.Content = item["Content"] as string;
                template.IsLocked = string.IsNullOrEmpty(Convert.ToString(item["IsLocked"])) ? false : Convert.ToBoolean(item["IsLocked"]);
                temples.Add(template);
            }
            return temples;
        }
    }

    public void GetDefaultData()
    {
        try
        {
            Dividers.Clear();
            string name = "Tab_Name1";
            DividerEntity entity = new DividerEntity(name);
            entity.Color = Colors[0];
            Dividers.Add(entity);
            labelTemplate.Text = "Create Template";
            btnSave.Text = "Save";

            this.Repeater2.DataSource = this.Dividers;
            this.Repeater2.DataBind();
        }
        catch (Exception ex) { }
    }

    public void GetData(int tempId)
    {
        try
        {

            ArrayList list = this.Templates;

            foreach (TemplateEntity item in list)
            {
                if (item.TemplateID == tempId)
                {
                    Repeater2.DataSource = item.Dividers;
                    Repeater2.DataBind();

                    labelTemplate.Text = "Edit Template";
                    templateTextBox.Text = item.Name;
                    isLocked.Checked = item.IsLocked;
                    Dividers = item.Dividers;
                    DividersCount = item.Dividers.Count;
                    DropDownList1.SelectedValue = item.Dividers.Count.ToString();
                    UpdatePanel2.Update();

                    return;
                }
            }


            btnSave.Text = "Save";
        }
        catch (Exception ex) { }

    }

    public List<DividerEntity> Dividers
    {
        get
        {
            List<DividerEntity> obj = this.Session["Dividers"] as List<DividerEntity>;

            if (obj == null)
            {
                obj = new List<DividerEntity>();
                this.Session["Dividers"] = obj;
            }
            return obj;
        }

        set
        {
            this.Session["Dividers"] = value;
        }
    }

    public int DividersCount
    {
        get
        {
            var obj = this.Session["DividersCount"];

            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(this.Session["DividersCount"]);
            }
        }

        set
        {
            this.Session["DividersCount"] = value;
        }
    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int count = Convert.ToInt32(DropDownList1.SelectedValue.Trim());

            if (count > Dividers.Count)
            {
                int nCount = count - Dividers.Count;
                int number = Dividers.Count;
                for (int i = 0; i < nCount; i++)
                {
                    string name = "Tab_Name" + ++number;
                    DividerEntity entity = new DividerEntity(name);
                    entity.Color = Colors[number - 1];
                    Dividers.Add(entity);
                }
            }
            else if (count < Dividers.Count)
            {
                int nCount = Dividers.Count - count;
                Dividers.RemoveRange(count, nCount);
            }


            this.Repeater2.DataSource = this.Dividers;
            this.Repeater2.DataBind();
        }
        catch (Exception ex) { }
    }
    public Dictionary<int, string> Colors
    {
        get
        {
            object obj = this.Application["Colors"];
            if (obj == null)
            {
                Dictionary<int, string> colors = new Dictionary<int, string>();
                colors.Add(0, "FF0000");
                colors.Add(1, "FF9933");
                colors.Add(2, "CC0066");
                colors.Add(3, "CC9999");
                colors.Add(4, "9900CC");
                colors.Add(5, "9999FF");
                colors.Add(6, "663300");
                colors.Add(7, "66CC33");
                colors.Add(8, "333366");
                colors.Add(9, "33CC99");
                colors.Add(10, "0033CC");
                colors.Add(11, "00CCFF");

                obj = colors;
                this.Application["Colors"] = obj;
            }

            return obj as Dictionary<int, string>;
        }
        set
        {
            this.Application["Colors"] = value;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Dictionary<string, DividerEntity> list = new Dictionary<string, DividerEntity>();
            DataListItemCollection items = this.Repeater2.Items;
            if (templateTextBox.Text == "")
            {
                string strWrong = "Error : the template name is empty, please input.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, typeof(UpdatePanel), "alertok", "alert(\"" + strWrong + "\");", true);
                return;
            }
            if (items.Count == 0)
            {
                string strWrong = "Error : the tab number is empty, please input.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, typeof(UpdatePanel), "alertok", "alert(\"" + strWrong + "\");", true);
                return;
            }
            else
            {

                string color, tabname;
                TextBox box;
                TextBox boxColor;
                AjaxControlToolkit.ColorPickerExtender colorpicker;
                foreach (DataListItem item in items)
                {
                    box = item.Controls[1] as TextBox;
                    colorpicker = item.Controls[13] as AjaxControlToolkit.ColorPickerExtender;
                    boxColor = (TextBox)colorpicker.FindControl(colorpicker.TargetControlID);
                    tabname = box.Text.Trim();
                    color = boxColor.Text;

                    if (list.ContainsKey(tabname))
                    {
                        string strWrongInfor = string.Format("Error : the tab name {0} has existed, try another.", tabname);
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel2, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                        return;
                    }
                    else
                    {
                        DividerEntity newEntity = new DividerEntity(tabname);
                        newEntity.Color = color;

                        list.Add(tabname, newEntity);
                    }
                }
            }


            string content = "";

            foreach (DividerEntity item in list.Values)
            {
                content += item.Name + "|";
                content += item.Color + ";";
            }

            ArrayList listTemplates = this.Templates;

            int tid = Convert.ToInt32(Request.QueryString["id"]);
            string uid = string.Empty;
            if ((Request.QueryString["uid"]) != null)
            {
                uid = (Request.QueryString["uid"]).ToString();
            }

            bool isExist = FileFolderManagement.IsTemplateNameExist(uid, templateTextBox.Text, tid);
            if (!isExist)
            {

                if (tid != 0 && tid != null)
                {
                    DataTable newList = new DataTable();
                    newList.Columns.AddRange(new DataColumn[2] {new DataColumn("Name", typeof(string)),
                                                                new DataColumn("Color",typeof(string))
                                                              });
                    if (list.Count > DividersCount)
                    {
                        int number = list.Count - DividersCount;
                        int i = list.Count;
                        foreach (DividerEntity item in list.Values)
                        {
                            if (i < number + 1)
                            {
                                newList.Rows.Add(item.Name, item.Color);
                            }
                            i--;
                        }
                    }
                    DataTable fileFolders = General_Class.GetFoldersByTemplateId(tid);
                    if (fileFolders != null && fileFolders.Rows.Count > 0)
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.AddRange(new DataColumn[4] { new DataColumn("EFFID", typeof(int)),
                                                                new DataColumn("Name", typeof(string)),
                                                                new DataColumn("Color",typeof(string)),
                                                                new DataColumn("OfficeId",typeof(string))
                                                              });
                        foreach (DataRow dr in fileFolders.Rows)
                        {
                            foreach (DataRow drNew in newList.Rows)
                            {
                                dt.Rows.Add(Convert.ToInt32(dr["FolderID"].ToString()), drNew["Name"].ToString(), drNew["Color"].ToString(), dr["OfficeId"].ToString());
                            }
                        }
                        General_Class.AddDividers(dt);
                    }
                    FileFolderManagement.UpdateFolderTemplate(tid, templateTextBox.Text, content, isLocked.Checked);
                    UpdatePanel2.Update();
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel2, typeof(UpdatePanel), "RssNO", "alert('The template has been updated successfully');window.location.href = 'TemplateFolder.aspx'", true);
                    //Response.Redirect("TemplateFolder.aspx");
                }

                if (uid != null && uid != "" && uid != "0")
                {

                    TemplateEntity template = new TemplateEntity(templateTextBox.Text);
                    template.OfficeID = uid;
                    template.Content = content;
                    template.IsLocked = isLocked.Checked;
                    FileFolderManagement.SaveFolderTemplate(template);
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel2, typeof(UpdatePanel), "Redirect", "alert('The template has been created successfully');window.location.href = 'TemplateFolder.aspx'", true);
                    //Response.Redirect("TemplateFolder.aspx");
                }

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, typeof(UpdatePanel), "Redirect", "alert('Error : The template name already exists for this user.');", true);
            }
        }
        catch (Exception ex) { }
    }
    protected void Repeater2_UpdateCommand(object source, DataListCommandEventArgs e)
    {
        DataListItem item = e.Item;
    }
    protected void Repeater2_ItemCreated(object sender, DataListItemEventArgs e)
    {
        Control control;
        DataListItem item;
        switch (e.Item.ItemType)
        {
            case ListItemType.Item:
                item = e.Item;
                control = item.FindControl("ColorPicker2");
                control.ID = control.ID + item.ItemIndex;
                break;
            case ListItemType.AlternatingItem:
                item = e.Item;
                control = item.FindControl("ColorPicker3");
                control.ID = control.ID + item.ItemIndex;
                break;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        int tempId = Convert.ToInt32(Request.QueryString["id"]);
        GetData(tempId);
    }
}