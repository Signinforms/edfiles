﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TemplateFolder.aspx.cs" Inherits="Manager_TemplateFolder" MasterPageFile="~/MasterPageOld.master" %>

<%@ Import Namespace="System.Data" %>
<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <%--<link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/style.css" rel="stylesheet" type="text/css" />--%>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style>
        #combobox_chosen
        {
            display: block;
            margin-top: 5px;
        }

        body
        {
            font-family: 'Times New Roman';
        }

        #comboBoxFolder_chosen
        {
            display: block;
            margin-top: 5%;
        }

        #comboBoxSubUser_chosen
        {
            margin-left: 30%;
            display: block;
            margin-top: 5px;
        }

        h3
        {
            display: block;
            font-size: 20px;
            -webkit-margin-before: 1em;
            -webkit-margin-after: 1em;
            -webkit-margin-start: 0px;
            -webkit-margin-end: 0px;
            font-weight: bold;
        }

        .chosen-results
        {
            max-height: 200px !important;
        }

        label
        {
            font-size: 14px;
        }

        #overlay
        {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
            left: 0;
        }

        #folderList
        {
            width: 100%;
            display: none;
            border: 1px #dadada solid;
            position: absolute;
            left: 0;
            top: 100%;
            z-index: 9;
            background: #fff;
            max-height: 235px;
            overflow-y: auto;
        }

            #folderList label
            {
                display: block;
                padding: 0 5px;
                width: auto;
                min-width: initial;
                cursor: pointer;
                margin-top: 3px;
            }

                #folderList label:hover
                {
                    background-color: #e5e5e5;
                }

            #folderList input
            {
                display: inline-block;
            }

        .selectBox-outer
        {
            position: relative;
            display: inline-block;
        }

        .selectBox select
        {
            width: 100%;
        }

        .overSelect
        {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            font-size: 15px;
        }

        .logFormAddFile
        {
            background: url(../images/file-add.png) no-repeat center center;
            margin-top: 6px;
            margin-left: -10px;
        }

        .logFormUploadFile
        {
            background: url(../images/file-upload.png) no-repeat center center;
            cursor: pointer;
            margin-top: 6px;
            border: none;
        }

        .chosen-container
        {
            font-size: 15px;
        }

        .datatable
        {
            border: 1px solid #cccaca;
            width: 100%;
            border-spacing: 0;
            empty-cells: show;
            border-bottom: 0 none;
        }

            .datatable th
            {
                background: #ecebeb;
                color: #030303;
                font-weight: bold;
                font-size: 14px;
                padding: 6px;
                vertical-align: middle;
                text-align: center;
                text-decoration: none;
                border: 0 none;
                border-bottom: 1px solid #cccaca;
            }

                .datatable th a
                {
                    color: #030303;
                    font-weight: bold;
                    font-size: 14px;
                    padding: 6px;
                    vertical-align: middle;
                    text-align: center;
                    text-decoration: none;
                    border: 0 none;
                }

            .datatable td
            {
                border-bottom: 1px solid #e9e9e9;
                border-left: 1px solid #e9e9e9;
                border-right: 1px solid #e9e9e9;
                color: #595959;
                font-size: 12px;
                padding: 6px;
                font-family: 'open sans', sans-serif;
                vertical-align: middle;
                text-align: center;
            }

            .datatable tbody td, .datatable thead th
            {
                border-bottom: 1px solid #cccaca;
                border-left: 0 none;
                border-right: 1px solid #cccaca;
            }

                .datatable tbody td.last, .datatable thead th.last
                {
                    border-right: 0 none !important;
                }

            .datatable tbody tr.even
            {
                background: #fff;
            }

            .datatable tbody tr.odd
            {
                background: #f5f5f5;
            }

        .datatable-small th
        {
            background: #ecebeb;
            color: #030303;
            font-weight: bold;
            padding: 6px;
            border: 0 none;
            border-bottom: 1px solid #cccaca;
            font-size: 12px;
            text-align: center;
            text-decoration: none;
            vertical-align: middle;
        }

            .datatable-small th a
            {
                background: none;
                color: #030303;
                font-weight: bold;
                padding: 6px;
                border: 0 none;
                font-size: 12px;
                text-align: center;
                text-decoration: none;
                vertical-align: middle;
            }

        .listing-datatable .folder-name
        {
            min-width: 190px;
        }

        .listing-datatable .first-name
        {
            width: 140px;
        }

        .listing-datatable .last-name
        {
            width: 140px;
        }

        .listing-datatable .file-number
        {
            width: 105px;
        }

        .listing-datatable .tabs
        {
            width: 50px;
        }

        .listing-datatable .size
        {
            width: 52px;
        }

        .listing-datatable .date
        {
            width: 120px;
        }

        .listing-datatable .eff
        {
            width: 50px;
        }

        .listing-datatable .action
        {
            min-width: 210px;
        }

        .listing-datatable a
        {
            color: #1f43a7;
            text-decoration: underline;
        }

        .emptyBox
        {
            height: 30px;
            width: 1%;
            font-size: 15px;
            text-align: center;
        }

        .Empty td
        {
            height: 30px;
            width: 1%;
            font-size: 15px;
            text-align: center;
        }

        .imgBtn
        {
            background: url(../images/plus.png) 7px 7px no-repeat;
            border-radius: 5px;
            border: none;
            cursor: pointer;
            float: right;
            background-color: lightgrey;
            height: 30px;
            padding-left: 25px;
            font-size: 16px;
            background-origin: padding-box;
        }

        .lockBtn
        {
            margin-left: 30px;
            cursor: pointer;
            float: left;
            height: 30px;
            font-size: 16px;
        }

        /*th
        {
            cursor:pointer;
        }*/

        .hidden
        {
            display: none;
        }

        .Naslov_Protokalev1
        {
            font-size: 22px;
            color: #ff7e00;
            padding: 19px 0 0 30px;
            width: 400px;
        }

        .linkDelete
        {
            margin-left: 10px;
            cursor: pointer;
        }

        .linkEdit
        {
            margin-right: 10px;
            cursor: pointer;
        }
    </style>
    <script type="text/javascript">

        var checkArray = [];
        function showLoader() {
            $('#overlay').show();
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
        }
        function LoadUsers() {

            $.ajax(
              {
                  type: "POST",
                  url: '../Office/DocumentAndFolderLogs.aspx/GetAllUsers',
                  contentType: "application/json; charset=utf-8",
                  dataType: "json",
                  success: function (data) {
                      if ($("#" + '<%= hdnUserID.ClientID%>').val() == "") {
                          $('<option />', { value: 0, text: "Select" }).appendTo($("#combobox"));
                      }
                      $.each(data.d, function (i, text) {
                          $('<option />', { value: i, text: text }).appendTo($("#combobox"));
                      });
                      $("#combobox").chosen({ width: "315px" });
                      if ($("#" + '<%= hdnUserID.ClientID%>').val() != "") {
                          $("#combobox").val($("#" + '<%= hdnUserID.ClientID%>').val()).trigger("chosen:updated");
                      }
                      hideLoader();

                  },
                  fail: function (data) {
                      alert("Failed to get Users. Please try again!");
                      hideLoader();
                  }
              });
          }

          function pageLoad() {
              //showLoader();
              //LoadUsers();
          }

          function EditTemplate(folderId) {

              window.open('TemplateDetails.aspx?id=' + folderId + '', '_blank');
          }

          function CreateTemplate() {
              window.open('TemplateDetails.aspx?uid=' + $("#" + '<%=hdnUserID.ClientID%>').val() + '', '_blank');
          }

          function LockSelectedTemplates() {
              showLoader();
              $('#<%= locekdTemplateIds.ClientID %>').val(JSON.stringify(checkArray));
          }

          function updateArray(templateId, isLocked) {
              for (var i = 0; i < checkArray.length; i++) {
                  if (checkArray[i].TemplateID == templateId) {
                      checkArray[i].Locked = isLocked;
                      return true;
                  }
              }
              return false;
          }

          function pushArray(templateId, isLocked) {
              checkArray.push({ 'TemplateID': templateId, 'Locked': isLocked });
          }

          $(document).ready(function () {
              showLoader();
              LoadUsers();
              if ($("#" + '<%=hdnUserID.ClientID%>').val() == 0 || "") {
                  $('.imgBtn').hide();
                  $('.lockBtn').hide();
              }
              $("#combobox").change(function (event) {
                  showLoader();
                  $("#" + '<%=hdnUserID.ClientID%>').val($("#combobox").chosen().val());
                  $('.imgBtn').show();
                  $('.lockBtn').show();
                  $("#" + '<%= btnTempData.ClientID %>').click();
              });
              $('input:checkbox').not('.parentChk').click(function () {
                  var isChecked = $(this).prop('checked');
                  var templateId = $(this).parent().siblings().first().val();
                  if (!updateArray(templateId, isChecked))
                      pushArray(templateId, isChecked);
              })
              $('.parentChk').click(function () {
                  var isChecked = $(this).prop('checked');
                  $('input:checkbox').not('.parentChk').each(function () {
                      $(this).prop('checked', isChecked);
                      var templateId = $(this).parent().siblings().first().val();
                      if (!updateArray(templateId, isChecked))
                          pushArray(templateId, isChecked);
                  })
              })
          });
    </script>
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <div style="width: 825px; height: 75px;">
        <div class="Naslov_Protokalev1">
            Folder Templates
        </div>
    </div>
    <table style="margin: 0px 20px 40px 30px;">
        <tr>
            <td>
                <label>Select User to see Template</label>
                <select id="combobox">
                </select>

                <asp:HiddenField runat="server" ID="hdnUserID" />
            </td>
        </tr>
    </table>
    <asp:Button runat="server" ID="btnTempData" Text="Search" OnClick="btnTempData_Click" CssClass="hidden" Style="margin-top: 22px; height: 25px;" />
    <asp:Button ID="Button2" runat="server" CssClass="imgBtn" ToolTip="Create New Template" OnClientClick="javascript:CreateTemplate()" Text="" />
    <asp:Button ID="btnLock" runat="server" CssClass="lockBtn create-btn btn-small" ToolTip="Lock Selected Templates" OnClick="btnLock_Click" OnClientClick="javascript:LockSelectedTemplates()" Text="Lock" />
    <asp:HiddenField ID="locekdTemplateIds" runat="server" />
    <div id="mainContent" style="margin-bottom: 50%; margin-left: 30px; clear: both; margin-top: 10%;">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                <asp:GridView ID="GridView1" runat="server" CssClass="Empty"
                    DataKeyNames="TemplateID" OnSorting="GridView1_Sorting"
                    AllowSorting="True" AutoGenerateColumns="false">
                    <PagerSettings Visible="False" />
                    <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                    <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                    <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                    <Columns>
                        <asp:BoundField DataField="Name" HeaderText="Template" SortExpression="Name" ItemStyle-Width="2%" />
                        <asp:TemplateField ShowHeader="true" ItemStyle-Width="1%" ItemStyle-CssClass="actionMinWidth">
                            <HeaderTemplate>
                                <input type="checkbox" title="Check all templates" class="chckBox parentChk" />
                                <%--<asp:CheckBox runat="server" ToolTip="Check all templates" CssClass="chckBox parentChk" />--%>
                                <asp:Label ID="Label9" runat="server" Text="Lock" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="lockedChkBx" CssClass="chckBox" runat="server" Checked='<%#Eval("IsLocked") %>' />
                                <asp:HiddenField ID="hiddenTID" runat="server" Value='<%#Eval("TemplateID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="true" ItemStyle-Width="1%" ItemStyle-CssClass="actionMinWidth">
                            <HeaderTemplate>
                                <asp:Label ID="Label8" runat="server" Text="Action" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton Text="Edit" CssClass="linkEdit" runat="server" ID="lnkEditTemplate"
                                    OnClientClick='<%#string.Format("javascript:EditTemplate({0})", Eval("TemplateID"))%>'>
                                </asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" CssClass="linkDelete" runat="server" Text="Delete" CausesValidation="false" CommandArgument='<%# Eval("TemplateID")%>' OnCommand="LinkButton2_Command"></asp:LinkButton>
                                <ajaxToolkit:ConfirmButtonExtender ID="ButtonDeleteUser" DisplayModalPopupID="lnkDelete_ModalPopupExtender" runat="server" ConfirmText="Are you sure to delete this template ?"
                                    TargetControlID="LinkButton2">
                                </ajaxToolkit:ConfirmButtonExtender>
                                <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                    CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="LinkButton2"
                                    PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                </ajaxToolkit:ModalPopupExtender>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>

                    <EmptyDataTemplate>
                        <asp:Label ID="Label1" runat="server" CssClass="Empty" />There is no template for selected user.
                    </EmptyDataTemplate>
                </asp:GridView>

                <asp:Panel class="popupConfirmation" ID="DivDeleteConfirmation" Style="display: none"
                    runat="server">
                    <div class="popup_Container">
                        <div class="popup_Titlebar" id="PopupHeader">
                            <div class="TitlebarLeft">
                                Delete Template
                            </div>
                            <div class="TitlebarRight" onclick="$get('ButtonDeleteCancel').click();">
                            </div>
                        </div>
                        <div class="popup_Body">
                            <p>
                                Are you sure you want to delete this template?
                            </p>
                            <p>
                                Please enter password to delete template:
                                                                    <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                            </p>
                        </div>
                        <div class="popup_Buttons">
                            <input id="ButtonDeleleOkay" type="button" value="Yes" style="margin-left: 80px; width: 65px" />
                            <input id="ButtonDeleteCancel" type="button" value="No" style="margin-left: 20px; width: 65px" />
                        </div>
                    </div>
                </asp:Panel>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
