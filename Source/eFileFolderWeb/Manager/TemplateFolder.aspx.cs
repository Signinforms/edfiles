﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using AjaxControlToolkit;
using DataQuicker2.Framework;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Shinetech.Utility;
using System.Linq;

public partial class Manager_TemplateFolder : System.Web.UI.Page
{
    private static readonly string ConnectionString =
         ConfigurationManager.ConnectionStrings["Default"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
        }
    }

    protected void btnTempData_Click(object sender, EventArgs e)
    {
        string uid = this.hdnUserID.Value;
        DataTable templates = FileFolderManagement.GetTemplatesByUID(uid);
        foreach (DataRow drRow in templates.Rows)
        {
            if (string.IsNullOrEmpty(drRow["IsLocked"].ToString()))
                drRow["IsLocked"] = false;
        }
        this.DataSource = templates;
        BindGrid();
    }
    public void BindGrid()
    {
        GridView1.DataSource = this.DataSource.AsDataView();
        GridView1.DataBind();
    }
    public DataTable DataSource
    {
        get
        {
            return this.Session["Template"] as DataTable;
        }
        set
        {
            this.Session["Template"] = value;
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }


    protected void LinkButton2_Command(object sender, CommandEventArgs e)
    {
        try
        {
            string password = textPassword.Text.Trim();
            string strPassword = PasswordGenerator.GetMD5(password);

            if (strPassword != PasswordGenerator.GetMD5(ConfigurationManager.AppSettings["ANOTHER_PASSWORD"]))
            {
                //show the password is wrong
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again later.');", true);

            }
            else
            {
                int tempID = int.Parse(e.CommandArgument.ToString());
                Shinetech.DAL.FolderTemplate template = new Shinetech.DAL.FolderTemplate(tempID);

                if (template.IsExist)
                {
                    try
                    {
                        FileFolderManagement.DeleteFolderTemplate(tempID);

                    }
                    catch { }

                    string uid = this.hdnUserID.Value;
                    DataTable templates = FileFolderManagement.GetTemplatesByUID(uid);
                    this.DataSource = templates;
                    BindGrid();
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('The template has been deleted successfully.');", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Failed to delete template, please try again later.');", true);
        }
    }
    protected void btnLock_Click(object sender, EventArgs e)
    {
        DataTable dtLocked = new DataTable();
        dtLocked.Columns.Add("TemplateID", typeof(string));
        dtLocked.Columns.Add("Locked", typeof(int));
        dtLocked.Columns.Add("Index", typeof(int));
        LockedFolderTemplate[] lockedTemplates = JsonConvert.DeserializeObject<List<LockedFolderTemplate>>(locekdTemplateIds.Value).ToArray();
        int index = 1;
        foreach (var item in lockedTemplates)
        {
            if (!string.IsNullOrEmpty(item.TemplateID))
            {
                dtLocked.Rows.Add(new object[] { item.TemplateID, item.Locked, index });
                index++;
            }
        }
        General_Class.UpdateLockForSelectedTemplates(dtLocked);
        btnTempData_Click(new object(), new EventArgs());
    }
}