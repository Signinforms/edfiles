<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true" CodeFile="TrackView.aspx.cs" Inherits="TrackView" Title="" %>
<%@ Import Namespace="System.Data" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<table>
<tr>
  <td height="186" valign="top" style="background-repeat: no-repeat;"><asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <contenttemplate>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="725px" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                            style="background-repeat: repeat-x;"><div style="background: url(images/inner_b.gif); background-position: right top;
                                background-repeat: no-repeat; margin-top: 10px;"/>
          <div style="width: 725px; height: 60px;">
            <div class="Naslov_Zelen"> Track Analytics</div>
          </div>
          <div class="podnaslovUser" style="padding: 1px; background-color: #f6f5f2;">Below is a list of all tracks associated with all partners. You may select 
           a date to view track analytics.</div>
          <div>
            <table style="margin: 0px 10px 0px 10px" cellspacing="1" cellpadding="0" width="920"
                                            bgcolor="#ffffff" border="0"> 
              <tr><td> <div style="padding: 1px; background-color: #f6f5f2;"><label style="font-size:14px; font-weight:bold">Select Date:&nbsp;</label><asp:TextBox ID="TextBoxEndDate" runat="server" class="form_tekst_field" width="85px"/>
                     <ajaxToolkit:CalendarExtender ID="CalendarExtender1" PopupPosition="Right" runat="server" TargetControlID="TextBoxEndDate" Format="MM-dd-yyyy"  />
                     <asp:Button id="btnSearch" runat="server" CausesValidation="false" Text="Search" width="60" height="21"  OnClick="btnSearch_Click"/></div></td></tr>                             
              <tr>
                <td><asp:GridView ID="GridView1" runat="server" DataKeyNames="PartnerID" OnSorting="GridView1_Sorting"
                                                            AllowSorting="True" AutoGenerateColumns="false" Width="100%" BorderWidth="2">
                    <PagerSettings Visible="False" />
                    <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                    <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                    <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                    <Columns>
                    <asp:BoundField DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName">
                      <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ContactName" HeaderText="ContactName" SortExpression="ContactName">
                      <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ContactEmail" HeaderText="ContactEmail" SortExpression="ContactEmail">
                      <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CNT1" HeaderText="Visitor Users" SortExpression="CNT1">
                      <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CNT2" HeaderText="Trial Users" SortExpression="CNT2">
                      <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                    <asp:BoundField DataField="CNT3" HeaderText="Purchase Users" SortExpression="CNT3">
                      <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                   
                    <asp:BoundField DataField="CNT4" HeaderText="Purchase Amount" SortExpression="CNT4" DataFormatString="{0:F2}">
                      <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                    </Columns>
                  </asp:GridView>
                </td>
              </tr>
              <tr>
                <td class="prevnext" bgcolor="#f8f8f5">
                <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                 OnPageSizeChanged="WebPager1_PageSizeChanged" PageSize="15" CssClass="prevnext" PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1"> </cc1:WebPager>
                  <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                </td>
              </tr>
            </table>
            </contenttemplate>
            <triggers>
              <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged"></asp:AsyncPostBackTrigger>
            </triggers>
            </asp:UpdatePanel>
          </div></td>
        <td width="10" align="center"><img src="../images/lin.gif" width="1" height="453" vspace="10" /></td>
      </tr>
    </table>
</asp:Content>
