﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UploadArchive.aspx.cs" Inherits="Manager_UploadArchive" MasterPageFile="~/MasterPageOld.master" %>

<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style type="text/css">
        .Empty {
            text-align: center;
            font-size: 20px;
        }

        .yellow {
            background-color: yellow;
        }

        .alignCenter {
            text-align: center;
        }

        .EditFolderName {
            display: none;
            height: 30px;
            border: 1px solid rgb(196, 196, 196);
            margin-top: 5px;
        }

        #comboBoxFolder_chosen {
            /*display: block;*/
            margin-top: 5px;
        }

        .chosen-results {
            max-height: 100px !important;
        }

        .chosen-container-single .chosen-single {
            line-height: 33px !important;
        }

        .chosen-single {
            height: 30px !important;
        }

        #combobox2_chosen {
            margin-left: 5px;
            margin-bottom: 8px;
        }

        #combobox1_chosen {
            margin-bottom: 8px;
        }

        .left-content {
            width: 780px;
        }

        .searchedLbl {
            margin-bottom: 10px;
        }

        .ic-save {
            background: url(../../images/icon-save.png) no-repeat center center;
        }

        .ic-cancel {
            background: url(../../images/icon-cancel.png) no-repeat center center;
        }

        .ic-edit {
            background: url(../../images/icon-edit.png) no-repeat right center;
        }

        .ic-icon {
            display: inline-block;
            vertical-align: top;
            font-size: 0;
            width: 45px;
            height: 28px;
            /*margin-left: 10px;*/
            margin-top: 5px;
            border: none;
            cursor: pointer;
        }

        .ic-delete {
            background: url(../../images/icon-delete.png) no-repeat center center;
        }

        .collapseFolder {
            display: none;
            background-color: white;
            margin-top: -5px;
            position: absolute;
            z-index: 10000;
            left: 44%;
            border: none;
            border-bottom-right-radius: 100%;
            border-bottom-left-radius: 100%;
            background: url(../images/up-arrow-key.png) 2px -3px no-repeat white;
            height: 24px;
            width: 30px;
            border: solid 1px #94ba33;
            cursor: pointer;
            top: 100%;
        }

        .btnNav {
            border-radius: 4px;
            height: 25px;
            width: 25px;
            cursor: pointer;
            float: right;
            margin-top: 10px;
        }

        .lnkExpand {
            text-decoration: underline;
            font-size: 17px;
            color: #83af33;
            margin-left: 10px;
            margin-top: 10px;
            float: right;
            cursor: pointer;
        }

        .lnkCollapse {
            text-decoration: underline;
            font-size: 17px;
            color: #01487f;
            margin-left: 10px;
            margin-top: 10px;
            float: right;
            cursor: pointer;
            display: none;
        }


        label {
            font-size: 14px;
            display: inline-block;
        }

        .selectBox-outer {
            position: relative;
            display: inline-block;
        }

        .selectBox select {
            width: 100%;
        }

        .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            font-size: 15px;
        }

        .chosen-container {
            font-size: 15px;
        }

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
            left: 0;
        }

        #folderList {
            width: 100%;
            display: none;
            border: 1px #dadada solid;
            position: absolute;
            left: 0;
            top: 100%;
            z-index: 9;
            background: #fff;
            max-height: 235px;
            overflow-y: auto;
        }

            #folderList label {
                display: block;
                padding: 0 5px;
                width: auto;
                min-width: initial;
                cursor: pointer;
                margin-top: 3px;
            }

                #folderList label:hover {
                    background-color: #e5e5e5;
                }

            #folderList input {
                display: inline-block;
            }

        .textBox {
            /*margin-left: 5%;*/
            padding-left: 5px;
        }

        .span {
            font-weight: normal !important;
        }

        .datatable td {
            font-family: "Open Sans", sans-serif;
        }

        .register-container .form-containt .content-title {
            margin-bottom: 5px;
        }

        #btnCheckedAll {
            float: right;
            margin-right: 3px;
            margin-top: 9px;
            height: 18px;
            width: 18px;
        }

        .selectalltab {
            font-size: 18px;
            float: right;
            padding: 5px;
            font-family: 'Open Sans', sans-serif !important;
            color: #fff;
            font-weight: 700;
            margin-right: 5px;
        }

        .ajax__tab_body {
            height: auto!important;
        }

        .white_content {
            display: none;
            position: fixed;
            top: 20%;
            left: 20%;
            width: 60%;
            height: 50%;
            padding: 16px;
            background-color: white;
            z-index: 999999999;
            overflow: auto;
        }

        .btn_Search {
            visibility: hidden;
        }

        .preview {
            position: relative;
            left: -10px;
        }

        .actionMinWidth {
            min-width: 50px;
        }

        .chkpending, .chkinbox {
            margin-left: 10px;
        }

        .chkpendingAll, .chkinboxAll {
            margin-left: 10px;
        }

        .boxtitle {
            margin-top: 20px !important;
        }

        .file {
            background: url(../lib/TreeView/file_sprite.png) 0 0 no-repeat !important;
            width: 18px !important;
            height: 18px !important;
            margin-top: 4px!important;
            margin-left: 2px!important;
            margin-right: 5px!important;
        }


        /*16-09-2016*/
        .create-btn {
            background-image: url(../images/create-folder.png);
            cursor: pointer;
        }

        .rename-btn {
            background-image: url(../images/rename.png);
            cursor: pointer;
        }

        .delete-btn {
            background-image: url(../images/delete.png);
            cursor: pointer;
            opacity: 0.5;
        }

        .cancel-btn {
            background-image: url(../images/cancel-btn.png);
            cursor: pointer;
        }

        .send-btn {
            background-image: url(../images/send.png);
            cursor: pointer;
        }

        .preview-btn {
            cursor: pointer;
        }

        .register-container .form-containt .content-title {
            margin-bottom: 5px;
        }

        #popupclose,
        .popupclose,
        #popupclose_thumbnail {
            float: right;
            padding: 10px;
            cursor: pointer;
        }

        .popupcontent {
            padding: 10px;
        }

        .form-containt .content-box fieldset {
            font-size: initial;
        }

        .ajax__fileupload_dropzone {
            font-size: 15px;
            line-height: 120px !important;
            height: 120px !important;
        }

        .lnkbtnPreviewInbox,
        .lnkbtnPreviewPending {
            margin-left: -10px;
        }

        .downloadFile {
            margin-left: -10px;
            cursor: pointer;
        }

        #floatingCirclesG {
            top: 50%;
            left: 50%;
            position: fixed;
        }

        .listing-contant .quick-find {
            background: #fff;
            padding: 0px 0px 0px 0px;
            width: 235px;
            margin-top: 0px;
        }

        #uploadProgress {
            font-size: 14px;
        }

        .imgBtn {
            background: url(../images/plus.png) 7px 7px no-repeat;
            border-radius: 5px;
            border: none;
            cursor: pointer;
            float: right;
            background-color: lightgrey;
            height: 30px;
            /*margin-top: -20px;*/
            padding-left: 28px;
            font-size: 16px;
            background-origin: padding-box;
        }
    </style>
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <asp:HiddenField ID="hdnUserId" runat="server" />
    <asp:HiddenField ID="hdnSharePath" runat="server" />
    <asp:HiddenField ID="hdnSelectedPath" runat="server" />
    <asp:HiddenField ID="hdnSearchedId" runat="server" />
    <asp:HiddenField ID="hdnFolderId" runat="server" />
    <asp:HiddenField runat="server" ID="hdnFileName" />
    <asp:HiddenField runat="server" ID="hdnFileId" />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 825px; height: 75px;">
                <div class="Naslov_Protokalev">
                    Upload Archive
                </div>
            </div>
            <div>
                <label style="display: block">Select User to upload Archive</label>
                <select id="comboBox">
                </select>
            </div>

            <div style="float: right; margin-top: -30px; margin-right: 48px">
                <asp:TextBox ID="createFolderName" CssClass="createFolderName" runat="server" Style="display: none"></asp:TextBox>
                <asp:Button ID="createbox" runat="server" Text="Create" CssClass="createbox" Style="display: none" OnClick="createbox_Click" OnClientClick="return validateFolder()" />
                <asp:Button ID="cancelbox" runat="server" Text="Cancel" CssClass="cancelbox" Style="display: none" OnClientClick="return cancelFolderBox();" />

                <asp:LinkButton ID="createFolder" runat="server" CssClass="imgBtn" ToolTip="Create New Folder" OnClientClick="javascript:showtextbox()" Text="" AutoPostBack="False" />
            </div>
            <div>
                <label style="display: block; margin-top: 15px">Select Folder to upload Archive</label>
                <select id="comboBoxFolder" style="color: rgb(68,68,68); border-radius: 5px; background-color: #fff; border: 1px solid #aaa; box-shadow: 0 1px 0 #fff inset; background: linear-gradient(#fff 20%, #f6f6f6 50%, #eee 52%, #f4f4f4 100%)">
                </select>
                <asp:Button CssClass="ic-icon ic-edit editFolder" OnClientClick="return editFolder()" OnClick="btnEditFolder_Click" ID="btnEditFolder" ToolTip="Edit Selected Folder" runat="server" />
                <asp:Button CssClass="ic-icon ic-delete" OnClientClick="return ConfirmDeleteFolder()" OnClick="btnDeleteFolder_Click" ID="btnDeleteFolder" ToolTip="Delete Selected Folder" runat="server" />

            </div>
            <div style="float: right; margin-top: -24px; margin-right: 48px">
                <asp:FileUpload ID="FileUpload1" runat="server" AllowMultiple="true" />

                <asp:Button ID="btnUpload" runat="server" OnClientClick="return OnBtnUploadclick();" OnClick="btnUpload_Click" Text="Upload" Style="float: right;" />
            </div>
            <div style="display: inline-block">
                <asp:TextBox ID="EditFolderName" runat="server" CssClass="EditFolderName"></asp:TextBox>
                <asp:Button ID="lnkBtnEditFolder" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                    ToolTip="Update" CssClass="ic-icon ic-save lnkBtnEditFolder" Style="display: none" CommandArgument='<%#Eval("FileName")%>' name="lnkEdit"></asp:Button>
                <asp:Button ID="lnkBtnEditCancel" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                    OnClientClick="cancelEditFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel lnkBtnEditCancel" Style="display: none" name="lnkeditcancel"></asp:Button>

            </div>
            <table style="margin: 20px 20px 0px 0px; width: 925px">
                <tbody>
                    <tr>
                        <td>
                            <asp:GridView ID="GridView1" runat="server" OnSorting="GridView1_Sorting"
                                AllowSorting="True" AutoGenerateColumns="false" Width="100%" BorderWidth="0" OnRowDataBound="GridView1_RowDataBound" DataKeyNames="ArchiveId">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                <Columns>
                                    <%--<asp:BoundField HeaderText="FolderId" DataField="FolderId" />--%>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="400px" ItemStyle-Wrap="false" SortExpression="FileName">
                                        <HeaderTemplate>
                                            <%--<asp:Label ID="lblDocumentName" runat="server" Text="Document Name" />--%>
                                            <asp:LinkButton ID="lblDocumentName1" runat="server" Text="File Name" CommandName="Sort" CommandArgument="FileName" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("FileName")%>' name="DocumentName" CssClass="lblDocumentName" />
                                            <asp:TextBox runat="server" MaxLength="50" Text='<%#Eval("FileName")%>'
                                                ID="TextDocumentName" Style="display: none; height: 30px;" name="txtDocumentName" CssClass="txtDocumentName" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:BoundField HeaderText="ArchiveId" DataField="ArchiveId" />--%>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="200px" ItemStyle-Wrap="false" SortExpression="CreatedDate">
                                        <HeaderTemplate>
                                            <%--<asp:Label ID="lblDocumentName" runat="server" Text="Document Name" />--%>
                                            <asp:LinkButton ID="lblCreatedDate1" runat="server" Text="Created Date" CommandName="Sort" CommandArgument="CreatedDate" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("CreatedDate")%>' name="CreatedDate" CssClass="lblCreatedDate" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" ItemStyle-Wrap="false" SortExpression="Size">
                                        <HeaderTemplate>
                                            <%--<asp:Label ID="lblDocumentName" runat="server" Text="Document Name" />--%>
                                            <asp:LinkButton ID="lblSize1" runat="server" Text="Size" CommandName="Sort" CommandArgument="Size" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblSize" runat="server" Text='<%#Eval("Size")%>' name="CreatedDate" CssClass="lblSize" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:BoundField HeaderText="Searched Keyword" DataField="Data" />--%>
                                    <asp:TemplateField ShowHeader="true">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblMoveOrder" runat="server" Text="Edit|Delete" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkBtnEdit" runat="server" CausesValidation="false" Text="Edit" CssClass="editDocument"
                                                ToolTip="Rename File" CommandArgument='<%#string.Format("{0};{1}",Eval("ArchiveId"),Eval("PathName")) %>' />
                                            <asp:LinkButton Text="Update" ID="lnkBtnUpdate" runat="server" CausesValidation="True" AutoPostBack="False"
                                                ToolTip="Update" CssClass="saveDocument" Style="display: none" CommandArgument='<%# Eval("FileName")%>' name="lnkUpdate"></asp:LinkButton>
                                            <asp:LinkButton Text="Cancel" ID="lnkBtnCancel" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="cancelDocument" Style="display: none" name="lnkcancel"></asp:LinkButton>


                                            <asp:LinkButton ID="lnkBtnDelete" runat="server" CausesValidation="false" Text="Delete" CssClass="deleteDocument" OnClientClick="return ConfirmDelete()"
                                                OnCommand="lnkBtnDelete_Command" CommandArgument='<%#string.Format("{0};{1};{2}",Eval("ArchiveId"),Eval("PathName"),Eval("FileName")) %>' />
                                            <asp:HiddenField runat="server" ID="hdnArchiveId" Value='<%# Eval("ArchiveId")%>' />
                                            <asp:HiddenField runat="server" ID="hdnPathName" Value='<%# Eval("PathName")%>' />
                                            <asp:HiddenField runat="server" ID="hdnFileName" Value='<%#Eval("FileName")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label3" runat="server" CssClass="Empty" Text="There is no document for selected folder." />
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="prevnext" bgcolor="#f8f8f5">
                            <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                PageSize="15" OnPageSizeChanged="WebPager1_PageSizeChanged" CssClass="prevnext"
                                PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1"></cc1:WebPager>
                            <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                        </td>
                    </tr>
                </tbody>
            </table>
            <%--<asp:Button ID="btn_Search" CssClass="btn_Search" runat="server" OnClick="btn_Search_Click" />--%>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnUpload" />
        </Triggers>
    </asp:UpdatePanel>
    <script type="text/javascript">
        function pageLoad() {
            LoadUsers();
            $("#comboBox").change(function (event) {
                showLoader();
                $("#" + '<%=hdnUserId.ClientID%>').val($("#comboBox").chosen().val());
                LoadFolder($("#comboBox").val());
                SearchArchiveTree(0);
            });
            $("#comboBoxFolder").change(function () {
                showLoader();
                $('#<%= hdnFolderId.ClientID%>').val($("#comboBoxFolder").chosen().val());
                SearchArchiveTree();
                //$("#comboBoxFolder").chosen().val($('#<%= hdnFolderId.ClientID%>').val);
            });

            $('.editDocument').click(function (e) {
                e.preventDefault();
                removeAllEditable();
                showEdit($(this));
            });
            $('.lnkBtnEditFolder').click(function (e) {
                e.preventDefault();
                UpdateFolderName();
            });

            $('.lnkBtnEditFolder').click(function (e) {
                e.preventDefault();
                showLoader();
                if ($('.EditFolderName').val() == '') {
                    alert("Please enter valid Folder Name");
                    return false;
                }
                else {
                    var newfoldername = $(".EditFolderName").val();
                    var oldFoldername = $("#comboBoxFolder_chosen").find('a').find('span').html();
                    var archivetreeid = $('#<%= hdnFolderId.ClientID%>').val();
                    $.ajax(
                 {
                     type: "POST",
                     url: 'UploadArchive.aspx/RenameArchiveFolder',
                     contentType: "application/json; charset=utf-8",
                     data: '{"newFolderName":"' + newfoldername + '","oldFolderName":"' + oldFoldername + '","archiveTreeId":"' + archivetreeid + '"}',
                        dataType: "json",
                        success: function (result) {
                            if (result.d > 0)
                                alert("Folder has been renamed successfully");
                            SearchArchiveTree();
                            hideLoader();
                        },
                        error: function (result) {
                            //hideLoader();
                        }
                    });
                   }
            });

            $('.cancelDocument').click(function (e) {
                e.preventDefault();
                removeAllEditable();
            });
        }

        function LoadUsers() {
            showLoader();
            $("#comboBox").html('');
            $("#comboBox").chosen("destroy");
            $.ajax(
             {
                 type: "POST",
                 url: '../FlexWebService.asmx/GetOfficeUsersForAdmin',
                 contentType: "application/json; charset=utf-8",
                 data: '{"officeId":"' + '<%= Sessions.SwitchedRackspaceId %>' + '"}',
                 dataType: "json",
                    success: function (result) {
                     if (result.d) {
                         $.each(result.d, function (i, text) {
                             $('<option />', { value: i, text: text }).appendTo($("#comboBox"));
                         });
                     }

                     $("#comboBox").chosen({ width: "270px" });
                     if ($("#" + '<%= hdnUserId.ClientID%>').val() != "") {
                         $("#comboBox").val($("#" + '<%= hdnUserId.ClientID%>').val()).trigger("chosen:updated");
                     }
                     $("#" + '<%= hdnUserId.ClientID%>').val($("#comboBox").val())

                     LoadFolder($("#comboBox").val());
                     hideLoader();
                 },
                 fail: function (data) {
                     alert("Failed to get Folders. Please try again!");
                     hideLoader();
                 }
             });
         }

         function showtextbox() {
             event.preventDefault();
             $('.imgBtn').hide();
             $('.createFolderName').show();
             $('.cancelbox').show();
             $('.createbox').show();
             return false;
         }

         function showLoader() {
             $('#overlay').show();
             $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
         }

         function hideLoader() {
             $('#overlay').hide();
         }
         function LoadFolder(userId) {
             showLoader();
             $("#comboBoxFolder").html('');
             $("#comboBoxFolder").chosen("destroy");
             $.ajax(
              {
                  type: "POST",
                  url: '../FlexWebService.asmx/GetArchiveFolders',
                  contentType: "application/json; charset=utf-8",
                  data: '{"officeId":"' + userId + '"}',
                  dataType: "json",
                  success: function (result) {
                      if (result.d) {
                          $.each(result.d.Item1, function (i, text) {
                              $('<option />', { value: i, text: text }).appendTo($("#comboBoxFolder"));
                          });
                      }

                      $("#comboBoxFolder").chosen({ width: "270px" });
                      if ($("#" + '<%= hdnFolderId.ClientID%>').val() != "") {
                          $("#comboBoxFolder").val($("#" + '<%= hdnFolderId.ClientID%>').val()).trigger("chosen:updated");
                      }
                      $("#" + '<%= hdnFolderId.ClientID%>').val($("#comboBoxFolder").val());
                      $.each(result.d.Item2, function (i, text) {
                          if (i == $("#" + '<%= hdnFolderId.ClientID%>').val())
                              $("#" + '<%= hdnSharePath.ClientID%>').val(text);
                      });
                      //SearchArchiveTree();
                      hideLoader();
                  },
                  fail: function (data) {
                      alert("Failed to get Folders. Please try again!");
                      hideLoader();
                  }
              });
          }

          function SearchArchiveTree(folderId) {
              if (folderId != 0)
                  folderId = $('#<%= hdnFolderId.ClientID%>').val();
              $.ajax(
                   {
                       type: "POST",
                       url: 'UploadArchive.aspx/LoadArchives',
                       contentType: "application/json; charset=utf-8",
                       data: '{"folderId":"' + folderId + '","uid":"' + $('#<%= hdnUserId.ClientID%>').val() + '"}',
                       dataType: "json",
                       success: function (result) {
                           //hideLoader();
                           __doPostBack('', 'refreshGrid@');
                       },
                       fail: function (data) {
                           alert("Failed to get Documents. Please try again!");
                           hideLoader();
                       }
                   });
               }
               function removeAllEditable() {
                   $('.txtDocumentName,.saveDocument,.cancelDocument').hide();
                   $('.lblDocumentName,.editDocument, .deleteDocument').show();
               }

               function cancleFileName(ele) {
                   $('.saveDocument,.cancelDocument').hide();
                   $('.deleteDocument,.editDocument').show();
                   var $this = $(ele);
                   var labelName = '';
                   var FileName = '';
                   //$this.css('display', 'none');
                   //$this.parent().find('[name=lbkUpdate]').hide();
                   //$this.parent().find('[name=lnkEdit]').show();
                   //$this.parent().find('.deleteInboxfile,.lnkbtnPreview,.lnkBtnDownload, .lnkBtnShare, .lnkBtnAddToTab, .editdocs').show();
                   var row = $(ele).closest("tr");
                   $("td", row).each(function () {
                       var len = 0;
                       len = $(this).find('input[type=text]').length;
                       if (len > 0) {
                           labelName = $(this).find('[name=DocumentName]').html();
                           $(this).find('input[type = text]').val(labelName);
                           $(this).find('input[type = text]').hide();
                           $(this).find('[name=DocumentName]').show();
                       }
                       else {
                       }
                   });
               }

               function showEdit(ele) {
                   $('.saveDocument,.cancelDocument').hide();
                   $('.deleteDocument,.editDocument').show();
                   var $this = $(ele);
                   $this.css('display', 'none');
                   $this.parent().find('[name=lnkUpdate]').show();
                   $this.parent().find('[name=lnkcancel]').show();
                   var row = $(ele).closest("tr");
                   $("td", row).each(function () {
                       $(this).find('input[type=text]').css('border', '1px solid #c4c4c4');//1px solid #c4c4c4
                       $(this).find('input[type = text]').show();
                       if ($(this).find('input[type = text]').val() == undefined || $(this).find('input[type = text]').val() == "")
                       { }
                       else
                       {
                           oldName = $(this).find('input[type = text]').val();
                       }
                       $(this).find('[name=DocumentName]').hide();
                       $(this).closest('td').find('.deleteDocument').hide();
                   });
                   return false;
               }

               function OnSaveRename(txtRenameObj, pathName, archiveId, filename) {
                   showLoader();
                   var newname = $('#' + txtRenameObj)[0].value;
                   var oldFileName = pathName;
                   if (newname == undefined || newname == "") {
                       $('.txtDocumentName').css('border', '1px solid red');
                       return false;
                   }
                   else if (!/^[a-z\d][a-z\d\()_\-.\s]+$/i.test(newname)) {
                       alert("File name can not contain special characters.");
                       $('.txtDocumentName').css('border', '1px solid red');
                       return false;
                   }
                   else if (newname.indexOf('.') > 0 && newname.substring(newname.indexOf('.') + 1).toLowerCase() != 'pdf') {
                       alert("Only File with type 'pdf' is allowed.");
                       $('.txtDocumentName').css('border', '1px solid red');
                       return false;
                   }
                   else {
                       $('.txtDocumentName').css('border', '1px solid c4c4c4');
                   }
                   //showLoader();
                   $.ajax(
                   {
                       type: "POST",
                       url: 'UploadArchive.aspx/RenameArchiveDocument',
                       data: "{ newFileName:'" + newname + "',oldFileName:'" + oldFileName + "',archiveId:'" + archiveId + "',userid:'" + $("#" + '<%=hdnUserId.ClientID%>').val() + "',filename:'" + filename + "'}",
                     contentType: "application/json; charset=utf-8",
                     dataType: "json",
                     success: function (result) {
                         if (result.d > 0)
                             alert("File has been renamed successfully");
                         else
                             alert("Failed to rename file!! Please try again later");
                         //  hideLoader();
                         SearchArchiveTree();
                         hideLoader();
                     },
                     error: function (result) {
                         //hideLoader();
                     }
                 });
               }

               function UpdateFolderName() {
                   showLoader();
                   if ($('.EditFolderName').val() == '') {
                       alert("Please enter valid Folder Name");
                       return false;
                   }
                   else {
                       var newfoldername = $(".EditFolderName").val();
                       var oldFoldername = $("#comboBoxFolder_chosen").find('a').find('span').html();
                       var archivetreeid = $('#<%= hdnFolderId.ClientID%>').val();
                       $.ajax(
                    {
                      type: "POST",
                      url: 'UploadArchive.aspx/RenameArchiveFolder',
                      data: "{ newFolderName:'" + newfoldername + "',oldFileName:'" + oldFoldername + "',archiveTreeId:'" + archivetreeid + "'}",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      success: function (result) {
                          if (result.d > 0)
                              alert("Folder has been renamed successfully");
                          //else
                          //    alert("Failed to rename Folder!! Please try again later");
                          //  hideLoader();
                          SearchArchiveTree();
                          hideLoader();
                      },
                      error: function (result) {
                          //hideLoader();
                      }
                  });
             }
          }

             function OnBtnUploadclick() {
                 var source = document.getElementById("<%=FileUpload1.ClientID%>").value;
                   if (source.length > 0) {
                       if (source.substring(source.lastIndexOf(".") + 1, source.length).toLowerCase() != "pdf" && source.substring(source.lastIndexOf(".") + 1, source.length).toLowerCase() != "tiff" && source.substring(source.lastIndexOf(".") + 1, source.length).toLowerCase() != "tif") {
                           alert("Only pdf extension is allowed !!");
                           return false;
                       }

                       if ($("#" + '<%= hdnFolderId.ClientID%>').val() == "") {
                         alert("Please select at least one folder to upload file !!");
                         return false;
                     }
                     else {
                         showLoader();
                     }

                 }
                 else {
                     alert("Please select at least one file to upload !!");
                     return false;
                 }
             }

             function ConfirmDelete() {
                 var result = confirm("Are you sure you want to delete this file ?");
                 if (result)
                     showLoader();
                 else
                     return false;
             }

             function ConfirmDeleteFolder() {
                 if ($('#<%= hdnFolderId.ClientID %>').val() == "" || $('#<%= hdnFolderId.ClientID %>').val() == undefined) {
                alert("Please select one folder to delete !!!!");
                return false;
            }
            else {
                var result = confirm("Are you sure you want to delete folder : " + $("#comboBoxFolder_chosen").find('a').find('span').html() + " ?");
                if (result)
                    showLoader();
                else
                    return false;
                   }
               }

               function editFolder() {
                   event.preventDefault();
                   if ($('#<%= hdnFolderId.ClientID %>').val() == "" || $('#<%= hdnFolderId.ClientID %>').val() == undefined) {
                       alert("Please select one folder to Edit !!!!");
                       return false;
            }
                   else {
                       $(".editFolder").hide();
                       $(".lnkBtnEditFolder,.EditFolderName,.lnkBtnEditCancel").show();
                       $(".EditFolderName").val($("#comboBoxFolder_chosen").find('a').find('span').html());
                   }
        }

               function cancelEditFileName(e) {
                   e.preventDefault();
                   $(".lnkBtnEditFolder,.EditFolderName,.lnkBtnEditCancel").hide();
                   $(".editFolder").show();
               }
        function cancelFolderBox() {
            event.preventDefault();
            $('.imgBtn').show();
            $('.createFolderName').val('');
            $('.createFolderName').hide();
            $('.cancelbox').hide();
            $('.createbox').hide();
            return false;
        }
        function validateFolder() {
            if ($('.createFolderName').val() == '')
                alert("Please enter valid Folder Name");
            else {
                showLoader();
                return true;
            }

            return false;
        }
    </script>
</asp:Content>
