﻿using Elasticsearch.Net;
using Nest;
using Shinetech.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using PdfSharp.Drawing;

public partial class Manager_UploadArchive : System.Web.UI.Page
{
    protected static string hostString = ConfigurationManager.AppSettings.Get("ElasticHostString");
    protected static string elasticUserName = ConfigurationManager.AppSettings.Get("ElasticUserName");
    protected static string elasticUserSecret = ConfigurationManager.AppSettings.Get("ElasticUserSecret");
    protected static string elasticIndexName = ConfigurationManager.AppSettings.Get("ElasticIndices");
    protected static Uri[] hosts = hostString.Split(',').Select(x => new Uri(x)).ToArray();
    protected static StaticConnectionPool connectionPool = new StaticConnectionPool(hosts);
    protected static ConnectionSettings settings = new ConnectionSettings(connectionPool)
                                                                         .DefaultMappingFor<Archive>(i => i
                                                                         .IndexName(elasticIndexName)
                                                                         .TypeName("pdf"))
                                                                         .BasicAuthentication(elasticUserName, elasticUserSecret);
    //.DisableDirectStreaming(true);
    protected static ElasticClient highClient = new ElasticClient(settings);
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
    RackSpaceFileUpload rackspacefileupload = new RackSpaceFileUpload();

    protected void Page_Load(object sender, EventArgs e)
    {
        var argument = Request.Form["__EVENTARGUMENT"];
        if (!Page.IsPostBack)
        {
            GetData();
        }
        else
        {
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
            {
                BindDataGrid();
            }
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["LogForm"] as DataTable;
        }
        set
        {
            this.Session["LogForm"] = value;
        }
    }

    protected void WebPager1_PageIndexChanged(object sender, Shinetech.Framework.Controls.PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.ArchiveDataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object s, Shinetech.Framework.Controls.PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.ArchiveDataSource;
        WebPager1.DataBind();
    }

    protected void lnkBtnDelete_Command(object sender, CommandEventArgs e)
    {
        LinkButton btnLink = (LinkButton)sender;

        string[] commandArgs = btnLink.CommandArgument.ToString().Split(new char[] { ';' });
        string archiveId = commandArgs[0];
        string pathname = commandArgs[1];
        string filename = commandArgs[2];
        pathname = pathname.Substring(0, pathname.LastIndexOf('/') + 1);
        pathname = pathname + filename;
        int count = eFileFolderJSONWS.DeleteArchive(pathname, hdnUserId.Value, Convert.ToInt32(archiveId), hdnFolderId.Value);
        if (count > 0)
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "File has been deleted successfully." + "\");", true);
        else
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Failed to delete file. Please try again." + "\");", true);
        GetData();
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (FileUpload1.HasFiles)
        {
            try
            {
                foreach (HttpPostedFile postedFile in FileUpload1.PostedFiles)
                {
                    if (!string.IsNullOrEmpty(hdnFolderId.Value.Trim()))
                    {
                        int archiveId = 0;
                        try
                        {
                            archiveId = eFileFolderJSONWS.InsertDocumentArchive(postedFile.FileName, hdnSharePath.Value, hdnUserId.Value, postedFile.InputStream);
                            if (archiveId > 0)
                                GetData();
                        }
                        catch (Exception ex)
                        {
                            Common_Tatva.WriteElasticLog(archiveId.ToString(), hdnFolderId.Value, postedFile.FileName, ex);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteElasticLog(string.Empty, string.Empty, string.Empty, ex);
            }
        }
    }

    public void GetData(string uid = null)
    {
        this.ArchiveDataSource = General_Class.GetArchiveForAdmin(string.IsNullOrEmpty(hdnFolderId.Value) ? null : (int?)Convert.ToInt32(hdnFolderId.Value), uid);
        BindDataGrid();
    }

    private void BindDataGrid()
    {
        GridView1.DataSource = this.ArchiveDataSource.AsDataView();
        GridView1.DataBind();
        WebPager1.DataSource = this.ArchiveDataSource;
        WebPager1.DataBind();
    }

    [WebMethod]
    public static void LoadArchives(string folderId = null, string uid = null)
    {
        Manager_UploadArchive archives = new Manager_UploadArchive();
        try
        {
            archives.ArchiveDataSource = null;
            archives.ArchiveDataSource = General_Class.GetArchiveForAdmin(string.IsNullOrEmpty(folderId) ? null : (int?)Convert.ToInt32(folderId), uid);
        }
        catch (Exception ex)
        {
            archives.ArchiveDataSource = new DataTable();
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
        }
    }

    [WebMethod]
    public static int RenameArchiveDocument(string newFileName, string oldFileName, string archiveId, string userid, string filename)
    {
        string errorMsg = string.Empty;
        Manager_UploadArchive uploadarchive = new Manager_UploadArchive();
        string oldPath = oldFileName.Substring(0, oldFileName.LastIndexOf('/') + 1);
        oldFileName = oldPath + filename;
        newFileName = Common_Tatva.SubStringFilename(newFileName);
        oldPath = oldPath.Substring(0, oldPath.LastIndexOf('/'));
        string newObjectName = uploadarchive.rackspacefileupload.RenameObject(ref errorMsg, oldFileName, newFileName, Enum_Tatva.Folders.Archive, userid);
        if (newObjectName != null)
        {
            newFileName = newObjectName.Substring(newObjectName.LastIndexOf('/') + 1);
            int count = General_Class.UpdateArchive(Convert.ToInt32(archiveId), newFileName, newObjectName);
            EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
            bool isValid = eFileFolderJSONWS.UpdateArchiveInElastic(archiveId, newFileName, newObjectName, oldPath.Substring(oldPath.LastIndexOf('/') + 1));
            return count;
        }
        else
            return 0;
    }

    [WebMethod]
    public static int RenameArchiveFolder(string newFolderName, string oldFolderName, string archiveTreeId)
    {
        string errorMsg = string.Empty;
        newFolderName = Common_Tatva.SubStringFilename(newFolderName);
        if (oldFolderName == newFolderName)
            return 1;

        int count = General_Class.UpdateArchiveTree(Convert.ToInt32(archiveTreeId), newFolderName);
        return count;
    }


    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
            string pathname = drv.Row["PathName"].ToString();
            string archiveId = drv.Row["ArchiveId"].ToString();
            string filename = drv.Row["FileName"].ToString();

            TextBox txtDocumentName = (e.Row.FindControl("TextDocumentName") as TextBox);
            LinkButton lnkBtnUpdate = (e.Row.FindControl("lnkBtnUpdate") as LinkButton);
            if (lnkBtnUpdate != null)
            {
                lnkBtnUpdate.Attributes.Add("onclick", "OnSaveRename('" + txtDocumentName.ClientID + "','" + pathname + "','" + archiveId + "','" + filename + "');");
            }
        }
    }

    public DataTable ArchiveDataSource
    {
        get
        {
            return this.Session["DataSource_Archive"] as DataTable;
        }
        set
        {
            this.Session["DataSource_Archive"] = value;
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.ArchiveDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;
        GridView1.DataSource = Source.ToTable();
        GridView1.DataBind();
    }
    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void createbox_Click(object sender, EventArgs e)
    {
        int archiveTreeId = General_Class.DocumentInsertforArchiveTree(createFolderName.Text, 0, hdnUserId.Value, 1, string.Empty);
        if (archiveTreeId > 0)
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Folder has been created successfully" + "\");", true);
        else
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Something went wrong!! Please try again later." + "\");", true);
    }

    protected void btnDeleteFolder_Click(object sender, EventArgs e)
    {
        if (this.ArchiveDataSource.Rows.Count >= 0)
        {
            try
            {
                for (int i = 0; i < this.ArchiveDataSource.Rows.Count; i++)
                {
                    eFileFolderJSONWS.DeleteArchive(this.ArchiveDataSource.Rows[i]["PathName"].ToString(), hdnUserId.Value, Convert.ToInt32(this.ArchiveDataSource.Rows[i]["ArchiveId"]), hdnFolderId.Value);
                }
                int count = General_Class.DeleteArchiveTree(Convert.ToInt32(hdnFolderId.Value));
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteElasticLog(string.Empty, hdnFolderId.Value, string.Empty, ex);
            }
        }
        hdnFolderId.Value = null;
        GetData(hdnUserId.Value);
    }
    protected void btnEditFolder_Click(object sender, EventArgs e)
    {

    }
}