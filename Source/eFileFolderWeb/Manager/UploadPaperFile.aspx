<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    CodeFile="UploadPaperFile.aspx.cs" Inherits="UploadPaperFile" Title="" %>

<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript">

        function onClientUploadComplete(sender, e) {
            //onImageValidated("TRUE", e);
        }

        function onImageValidated(arg, context) {

            var test = document.getElementById("testuploaded");
            test.style.display = 'block';

            var fileList = document.getElementById("fileList");
            var item = document.createElement('div');
            item.style.padding = '4px';

            item.appendChild(createFileInfo(context));

            fileList.appendChild(item);
        }

        function createFileInfo(e) {
            var holder = document.createElement('div');
            holder.appendChild(document.createTextNode(e.get_fileName() + ' with size ' + e.get_fileSize() + ' bytes'));

            return holder;
        }



        function onClientUploadStart(sender, e) {
            document.getElementById('uploadCompleteInfo').innerHTML = 'Please wait while uploading ' + e.get_filesInQueue() + ' files...';
        }

        function onClientUploadCompleteAll(sender, e) {

            var args = JSON.parse(e.get_serverArguments()),
                unit = args.duration > 60 ? 'minutes' : 'seconds',
                duration = (args.duration / (args.duration > 60 ? 60 : 1)).toFixed(2);

            var info = 'At <b>' + args.time + '</b> server time <b>'
                + e.get_filesUploaded() + '</b> of <b>' + e.get_filesInQueue()
                + '</b> files were uploaded with status code <b>"' + e.get_reason()
                + '"</b> in <b>' + duration + ' ' + unit + '</b>';

            document.getElementById('uploadCompleteInfo').innerHTML = info;
        }


    </script>
    <table>
        <tr>
            <td valign="top" style="background-repeat: no-repeat;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="925" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                            style="background-repeat: repeat-x;">
                            <div style="background: url(images/inner_b.gif); background-position: right top;
                                background-repeat: no-repeat; margin-top: 10px;">
                                <div style="width: 825px; height: 75px;">
                                    <div class="Naslov_Protokalev">
                                        Upload Stored Paper Files
                                    </div>
                                </div>
                                <div class="form_title_2" style="width: 750px; margin: 0px 0 20px 25px;">
                                    <div>
                                        Click '<i>Select File</i>' for storage file uploading.
                                    </div>
                                    <br />
                                    <br />
                                    <div>
                                        <asp:Label runat="server" ID="myThrobber" Style="display: none;"><img align="absmiddle" alt="" src="uploading.gif"/></asp:Label>
                                        <ajaxToolkit:AjaxFileUpload ID="AjaxFileUpload1" runat="server" Padding-Bottom="4"
                                            Padding-Left="2" Padding-Right="1" Padding-Top="4" ThrobberID="myThrobber" OnClientUploadComplete="onClientUploadComplete"
                                            OnUploadComplete="AjaxFileUpload1_OnUploadComplete" MaximumNumberOfFiles="10"
                                            AllowedFileTypes="jpg,jpeg,pdf,doc,docx,xls" AzureContainerName="" OnClientUploadCompleteAll="onClientUploadCompleteAll"
                                            OnUploadCompleteAll="AjaxFileUpload1_UploadCompleteAll" OnUploadStart="AjaxFileUpload1_UploadStart"
                                            OnClientUploadStart="onClientUploadStart" />
                                        <div id="uploadCompleteInfo">
                                        </div>
                                    </div>
                                </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
