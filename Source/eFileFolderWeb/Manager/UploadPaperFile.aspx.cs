using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using AjaxControlToolkit;
using DataQuicker2.Framework;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;
using Shinetech.Utility;
using Account = Shinetech.DAL.Account;
using Shinetech.Engines.Adapters;
using AjaxControlToolkit;
using System.Web.Script.Serialization;

public partial class UploadPaperFile : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }

    protected void AjaxFileUpload1_OnUploadComplete(object sender, AjaxFileUploadEventArgs file)
    {
        string path = Server.MapPath("~/ScanInBox");
        string uid = this.Session["FileForm_UploadFID"].ToString();

        string fullPath = Path.Combine(path, uid);
        if (!Directory.Exists(fullPath))
        {
            Directory.CreateDirectory(fullPath);
        }

        if (AjaxFileUpload1.IsInFileUploadPostBack)
        {
            var fullname = Path.Combine(fullPath, file.FileName);
            AjaxFileUpload1.SaveAs(fullname);           
        }       
    }

    protected void AjaxFileUpload1_UploadCompleteAll(object sender, AjaxFileUploadCompleteAllEventArgs e)
    {
        var startedAt = (DateTime)Session["uploadTime"];
        var now = DateTime.Now;
        e.ServerArguments = new JavaScriptSerializer()
            .Serialize(new
            {
                duration = (now - startedAt).Seconds,
                time = DateTime.Now.ToShortTimeString()
            });
    }

    protected void AjaxFileUpload1_UploadStart(object sender, AjaxFileUploadStartEventArgs e)
    {
        var now = DateTime.Now;
        e.ServerArguments = now.ToShortTimeString();
        Session["uploadTime"] = now;
    }
}