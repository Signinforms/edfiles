<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    CodeFile="UserResourceDetail.aspx.cs" Inherits="UserResourceDetail" Title="" %>

<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.ComponentModel" %>
<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>

                        <td width="725" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                            style="background-repeat: repeat-x;">
                            <div style="background: url(images/inner_b.gif); background-position: right top; background-repeat: no-repeat; margin-top: 10px;">
                                <div style="width: 725px; height: 75px;">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="Naslov_Protokalev" style="width:100%">Resource Details of <%=UserName%></div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table width="725" border="0" cellpadding="0" cellspacing="1" bgcolor="#FFFFFF" style="margin: 0 20px 0 70px;">
                                            <tr>
                                                <td>
                                                    <div class="form_title_2" style="margin-top: 0px; margin-bottom: 2px">
                                                        Enter User Name:
                                                        <ajaxToolkit:ComboBox ID="DropDownList1" runat="server" Width="255px" DropDownStyle="DropDownList"
                                                            AutoCompleteMode="SuggestAppend" CssClass="WindowsStyle" DataValueField="UID"
                                                            DataTextField="FullName" AutoPostBack="true" CaseSensitive="false" EnableViewState="true"
                                                            OnSelectedIndexChanged="DropDownList1_OnSelectedIndexChanged" AppendDataBoundItems="false">
                                                        </ajaxToolkit:ComboBox>
                                                        &nbsp;&nbsp;
                                                            <asp:Button ID="ImageButton1" Text="Search" OnClick="ImageButton1_OnClick" runat="server" />
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form_title_2" style="margin-top: 0px; margin-bottom: 2px"></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="GridView3" BorderWidth="2" runat="server" Width="100%" AutoGenerateColumns="false"
                                                        AllowSorting="false" DataKeyNames="UID">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                        <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                        <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                        <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                        <Columns>
                                                            <asp:BoundField DataField="TotaleFile" HeaderText="Total number of EdFiles">
                                                                <HeaderStyle CssClass="form_title" Width="100px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TotalBox" HeaderText="Total number of boxes">
                                                                <HeaderStyle CssClass="form_title" Width="80px" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TotalScan" HeaderText="Total number of scans">
                                                                <HeaderStyle CssClass="form_title" Width="80px" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td bgcolor="#F8F8F5" class="prevnext"></td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
