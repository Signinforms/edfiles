using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AjaxControlToolkit;
using Shinetech.DAL;
using Shinetech.Framework.Controls;

public partial class UserResourceDetail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.UserName = "";
            this.UserUID = "";

            DropDownList1.DataSource = UserManagement.GetUsersList();
            DropDownList1.DataBind();
        }
    }

    #region 数据绑定
   
    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData(string uid)
    {
        //根据UID获取eff总数，box总数和scan总数
        DataTable table = this.StorageResourceData;
        table.Rows.Clear();

        DataRow row = table.NewRow();
        row["UID"] = uid;
        row["TotaleFile"] = UserManagement.GetTotalFolderNumberByUID(uid);
        row["TotalBox"] = UserManagement.GetTotalBoxNumberByUID(uid);
        row["TotalScan"] = GetScanFilesNumber(uid);
        table.Rows.Add(row);

    }

    protected void DropDownList1_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        
    }

    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        GridView3.DataSource = this.StorageResourceData;
        GridView3.DataBind();
    }
    #endregion

    protected Int32 GetScanFilesNumber(string uid)
    {
        Int32 scanNumber = 0;

        string path = Server.MapPath("~/ScanInBox");
        path = string.Concat(path, Path.DirectorySeparatorChar, uid);

        try
        {
            if (Directory.Exists(path))
            {
                string[] files = Directory.GetFiles(path);
                scanNumber += files.Length;
                string[] varpaths = Directory.GetDirectories(path);
                foreach (string item in varpaths)
                {
                    files = Directory.GetFiles(item);
                    scanNumber += files.Length;
                }
            }
           
        }
        catch { }


        return scanNumber;
    }

    private DataTable MakeDataTable()
    {
        // Create new DataTable.
        DataTable table = new DataTable();

        // Declare DataColumn and DataRow variables.
        DataColumn column;

        // Create new DataColumn, set DataType, ColumnName
        // and add to DataTable.    
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "UID";
        table.Columns.Add(column);
       
        column = new DataColumn();
        column.DataType = Type.GetType("System.Int32");
        column.ColumnName = "TotaleFile";
        table.Columns.Add(column);


        column = new DataColumn();
        column.DataType = Type.GetType("System.Int32");
        column.ColumnName = "TotalBox";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.Int32");
        column.ColumnName = "TotalScan";
        table.Columns.Add(column);

        return table;
    }


    public DataTable StorageResourceData
    {
        get
        {
            if (this.Session["StorageResourceData"] == null) this.Session["StorageResourceData"] = MakeDataTable();
            return this.Session["StorageResourceData"] as DataTable;
        }
        set
        {
            this.Session["StorageResourceData"] = value;
        }
    }

    

    protected string UserUID
    {
        get
        {
            return this.Session["_ResourceForUserID"] as string;
        }

        set
        {
            this.Session["_ResourceForUserID"] = value;
        }
    }

    protected void ImageButton1_OnClick(object sender, EventArgs e)
    {
        string uid = DropDownList1.SelectedValue.Trim();
        if (string.IsNullOrEmpty(uid))
        {
            UserName = "";
            UserUID = "";
        }
        else
        {
          
            UserName = DropDownList1.SelectedItem.Text;
            UserUID = uid;
            GetData(uid);
            
        }

        BindGrid();
    }

    protected string UserName
    {
        get
        {
            if (string.IsNullOrEmpty(this.Session["_ResourceForUserName"] as string))
            {
                return "";
            }

            return "For " + this.Session["_ResourceForUserName"] as string;
        }

        set
        {
            if (string.IsNullOrEmpty(value))
            {
                this.Session["_ResourceForUserName"] = value;
            }
            else
            {
                this.Session["_ResourceForUserName"] = value;
            }

        }
    }
}
