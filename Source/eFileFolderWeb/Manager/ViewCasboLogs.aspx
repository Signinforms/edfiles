﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewCasboLogs.aspx.cs" Inherits="Manager_ViewCasboLogs" MasterPageFile="~/MasterPageOld.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <style>
        .Naslov_Protokalev {
            letter-spacing:0px;
        }
    </style>
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <div id="casboLogs">
                <div style="width: 825px; height: 75px;">
                <div class="Naslov_Protokalev">
                    Casbo Logs
                </div>
            </div>
                <div class="table-scroll" style="margin-left:30px;">
                    <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                    <asp:GridView ID="GridView1" runat="server"
                        DataKeyNames="FileID" OnSorting="GridView1_Sorting"
                        AllowSorting="True" AutoGenerateColumns="false" style="margin-bottom:20px;">
                        <PagerSettings Visible="False" />
                        <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                        <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                        <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                        <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                        <Columns>
                            <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" ItemStyle-Width="25%" />
                            <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" ItemStyle-Width="25%" />
                            <asp:BoundField DataField="FileName" HeaderText="File Name" SortExpression="FileName" ItemStyle-Width="25%" />
                            <asp:BoundField DataField="CreatedDate" HeaderText="Action Date" SortExpression="CreatedDate" ItemStyle-Width="25%" />
                            <asp:BoundField DataField="ActionName" HeaderText="Action" SortExpression="ActionName" ItemStyle-Width="25%" />
                        </Columns>
                        <EmptyDataTemplate >
                            <asp:Label ID="Label1" runat="server" CssClass="Empty" />There is no record.
                        </EmptyDataTemplate>
                    </asp:GridView>
                    </div>
                </div>
                    </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Content>
