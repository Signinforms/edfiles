﻿using Shinetech.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Manager_ViewCasboLogs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
    }

    public DataTable CasboDataSource
    {
        get
        {
            return this.Session["DataSourceCasboLogs"] as DataTable;
        }
        set
        {
            this.Session["DataSourceCasboLogs"] = value;
        }
    }

    private SortDirection Casbo_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    private void GetData()
    {
        this.CasboDataSource = General_Class.GetCasboLogs();
        if (this.CasboDataSource != null)
        {
            foreach (DataRow dr in this.CasboDataSource.Rows)
            {
                dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
            }
            BindGrid();
        }
    }

    private void BindGrid()
    {
        GridView1.DataSource = this.CasboDataSource.AsDataView();
        GridView1.DataBind();
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Casbo_Sort_Direction == SortDirection.Ascending)
        {
            this.Casbo_Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Casbo_Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.CasboDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.CasboDataSource = Source.ToTable();
        BindGrid();

    }
}