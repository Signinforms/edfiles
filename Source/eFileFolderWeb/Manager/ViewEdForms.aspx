﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ViewEdForms.aspx.cs" Inherits="Manager_ViewEdForms" MasterPageFile="~/MasterPageOld.master" %>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style type="text/css">
        .Naslov_Protokalev1 {
            font-size: 22px;
            color: #ff7e00;
            padding: 19px 0 0 30px;
            width: 400px;
        }

        .white_content {
            display: none;
            position: fixed;
            top: 20%;
            left: 20%;
            width: 60%;
            height: 50%;
            padding: 16px;
            background-color: white;
            z-index: 999999999;
            overflow: auto;
        }

        #popupclose {
            float: right;
            padding: 10px;
            cursor: pointer;
        }

        .popupcontent {
            padding: 10px;
        }

        .downloadFile {
            margin-left: -10px;
            margin-top: -2px;
            cursor: pointer;
        }

        .preview-popup {
            width: 100%;
            max-width: 1300px;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 10%;
            right: 0;
            margin: auto;
            background: #fff;
            height: 85%;
        }

        .ic-close {
            background: url(../../images/icon-close.png) no-repeat center center;
        }

        .ajax__fileupload_dropzone {
            font-size: 15px;
            line-height: 120px !important;
            height: 120px !important;
            font-family: "Open Sans", sans-serif;
        }

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }

        #floatingCirclesG {
            top: 50%;
            left: 50%;
            position: fixed;
        }

        #comboBoxDepartment_chosen {
            /*display: block;*/
            margin-top: 5px;
        }

        #comboBoxUser_chosen {
            /*display: block;*/
            margin-top: 5px;
        }

        .chosen-results {
            max-height: 100px !important;
        }

        .chosen-container-single .chosen-single {
            line-height: 33px !important;
        }

        .chosen-single {
            height: 30px !important;
        }

        #combobox2_chosen {
            margin-left: 5px;
            margin-bottom: 8px;
        }

        #combobox1_chosen {
            margin-bottom: 8px;
        }
    </style>
    <script type="text/javascript">

        function pageLoad() {

            LoadUsers();

            $("#comboBoxUser").change(function () {
                $('#<%= hdnUserId.ClientID%>').val($("#comboBoxUser").chosen().val());
                GetDepartments($("#comboBoxUser").chosen().val());
                GetEdForms();
            });

            $("#comboBoxDepartment").change(function () {
                $('#<%= hdnDepartmentId.ClientID%>').val($("#comboBoxDepartment").chosen().val());
                GetEdForms();
            });

            $('.editDocument').click(function (e) {
                e.preventDefault();
                removeAllEditable();
                showEdit($(this));
            });

            var closePopup = document.getElementById("popupclose");
            closePopup.onclick = function () {
                var popup = document.getElementById("preview_popup");
                var overlay = document.getElementById("overlay");
                overlay.style.display = 'none';
                popup.style.display = 'none';
                $('#floatingCirclesG').css('display', 'block');
                enableScrollbar();
                $('#reviewContent').removeAttr('src');
            };
        }

        function LoadUsers() {
            showLoader();
            $("#comboBoxUser").html('');
            $("#comboBoxUser").chosen("destroy");
            $.ajax(
                {
                    type: "POST",
                    url: '../FlexWebService.asmx/GetOfficeUsersForAdmin',
                    contentType: "application/json; charset=utf-8",
                    data: '{"officeId":"' + '<%= Sessions.SwitchedRackspaceId %>' + '"}',
                    dataType: "json",
                    success: function (result) {
                        if (result.d) {
                            $.each(result.d, function (i, text) {
                                $('<option />', { value: i, text: text }).appendTo($("#comboBoxUser"));
                            });
                        }

                        $("#comboBoxUser").chosen({ width: "270px" });
                        if ($("#" + '<%= hdnUserId.ClientID%>').val() != "") {
                            $("#comboBoxUser").val($("#" + '<%= hdnUserId.ClientID%>').val()).trigger("chosen:updated");
                        }
                        $("#" + '<%= hdnUserId.ClientID%>').val($("#comboBoxUser").val())
                        GetDepartments($("#comboBoxUser").val());
                        hideLoader();
                    },
                    fail: function (data) {
                        alert("Failed to get departments. Please try again!");
                        hideLoader();
                    }
                });
        }

        function GetDepartments(uid) {
            showLoader();
            $("#comboBoxDepartment").html('');
            $("#comboBoxDepartment").chosen("destroy");
            $.ajax(
                {
                    type: "POST",
                    url: '../FlexWebService.asmx/GetAssignedDepartmentsForOffice',
                    contentType: "application/json; charset=utf-8",
                    data: '{"officeId":"' + uid + '"}',
                    dataType: "json",
                    async: false,
                    success: function (result) {
                        if (result.d.Item2) {
                            $.each(result.d.Item2, function (i, text) {
                                $('<option />', { value: i, text: text }).appendTo($("#comboBoxDepartment"));
                            });
                        }
                        $("#comboBoxDepartment").chosen({ width: "270px" });
                        if ($("#" + '<%= hdnDepartmentId.ClientID%>').val() != "") {
                            $("#comboBoxDepartment").val($("#" + '<%= hdnDepartmentId.ClientID%>').val()).trigger("chosen:updated");
                        }
                        $("#" + '<%= hdnDepartmentId.ClientID%>').val($("#comboBoxDepartment").val());
                        if ($("#" + '<%= hdnDepartmentId.ClientID%>').val() > 0) {
                            $("#ctl00_ContentPlaceHolder1_ajaxfileuploadeFileFlow").show();
                        }
                        else
                            $("#ctl00_ContentPlaceHolder1_ajaxfileuploadeFileFlow").hide();
                        //GetEdForms(uid, $("#comboBoxDepartment").val());
                        hideLoader();
                    },
                    fail: function (data) {
                        alert("Failed to get Folders. Please try again!");
                        hideLoader();
                    }
                });
        }

        function GetEdForms() {
            showLoader();
            $.ajax(
                {
                    type: "POST",
                    url: 'ViewEdForms.aspx/LoadEdForms',
                    contentType: "application/json; charset=utf-8",
                    data: '{officeId:"' + $("#" + '<%= hdnUserId.ClientID%>').val() + '",departmentId:' + ($("#" + '<%= hdnDepartmentId.ClientID%>').val() > 0 ? $("#" + '<%= hdnDepartmentId.ClientID%>').val() : null) + '}',
                    dataType: "json",
                    success: function (result) {
                        //hideLoader();
                        __doPostBack('', 'refreshGrid@');
                    },
                    fail: function (data) {
                        alert("Failed to get Documents. Please try again!");
                        hideLoader();
                    }
                });
        }

        function onClientUploadComplete(sender, e) {
            return false;
        }

        var myTimer;
        var fileIDDividerId = [];
        function onClientUploadStart(sender, e) {
            $.ajax(
                {
                    type: "POST",
                    url: 'ViewEdForms.aspx/SaveSelectedDepartment',
                    data: '{officeId:"' + $('#<%= hdnUserId.ClientID %>').val() + '",departmentId:' + $('#<%= hdnDepartmentId.ClientID %>').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (result) {
                        document.getElementById('uploadCompleteInfo').innerHTML = "please wait while the " + e.get_filesInQueue() + " file is uploaded..";
                        document.cookie = "Files=" + JSON.stringify(fileIDDividerId);
                        showLoader();

                        $('[id*=ajaxfileupload1_Html5DropZone]').addClass('hideControl').css('display', 'none');
                        $('[id*=ajaxfileupload1_SelectFileContainer]').addClass('hideControl').css('display', 'none');
                        $('[id*=FileItemDeleteButton]').addClass('hideControl').css('display', 'none');
                        $('[id*=    ]').addClass('hideControl').css('display', 'none');
                        $('.TabDropDown').prop('disabled', 'disabled');
                        $('#uploadProgress').html('');
                        $('#uploadProgress').html('').html($('#divUpload').html());

                        document.getElementById('uploadProgress').style.display = 'block';
                        myTimer = setInterval(function () {
                            $('#uploadProgress').html('').html($('#divUpload').html());
                        }, 1000);
                    },
                    error: function (result) {
                        hideLoader();
                    }
                });
        }

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
        }

        function onClientUploadCompleteAll(sender, e) {
            $('.hideControl').removeClass('hideControl');
            $('.uploadProgress').find('select').removeAttr('disabled');

            //$('#divUpload').clone().appendTo('#uploadFieldset');
            clearInterval(myTimer);
            document.getElementById('uploadProgress').style.display = 'none';
            hideLoader();
                 //__doPostBack('<%= UpdatePanel1.ClientID %>', '');

            setTimeout(function () {
                var vd = '<%= ConfigurationManager.AppSettings["VirtualDir"] %>';
                window.location.reload();
            }, 2000);
        }

        function onClientButtonClick() {
            $find('pnlPopup2').hide();

            setTimeout(function () {
            }, 1000);
            return false;
        }

        function cancleFileName(ele) {
            $('.saveDocument,.cancelDocument').hide();
            $('.deleteDocument,.editDocument,.viewDocument').show();
            var $this = $(ele);
            var labelName = '';
            var FileName = '';
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                var len = 0;
                len = $(this).find('input[type=text]').length;
                if (len > 0) {
                    labelName = $(this).find('[name=DocumentName]').html();
                    $(this).find('input[type = text]').val(labelName);
                    $(this).find('input[type = text]').hide();
                    $(this).find('[name=DocumentName]').show();
                }
                else {
                }
            });
            return false;
        }

        function removeAllEditable() {
            $('.txtDocumentName,.saveDocument,.cancelDocument').hide();
            $('.lblDocumentName,.editDocument, .deleteDocument,.viewDocument').show();
        }

        function showEdit(ele) {
            $('.saveDocument,.cancelDocument').hide();
            $('.deleteDocument,.editDocument,.viewDocument').show();
            var $this = $(ele);
            $this.css('display', 'none');
            $this.parent().find('[name=lnkUpdate]').show();
            $this.parent().find('[name=lnkcancel]').show();
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                $(this).find('input[type=text]').css('border', '1px solid #c4c4c4');//1px solid #c4c4c4
                $(this).find('input[type = text]').show();
                if ($(this).find('input[type = text]').val() == undefined || $(this).find('input[type = text]').val() == "") { }
                else {
                    oldName = $(this).find('input[type = text]').val();
                }
                $(this).find('[name=DocumentName]').hide();
                $(this).closest('td').find('.deleteDocument').hide();
                $(this).closest('td').find('.viewDocument').hide();
            });
            return false;
        }

        function OnSaveRename(ele, edFormsId, fileName) {
            showLoader();
            var newName = $(ele).parent().parent().find("input.txtDocumentName").val();
            var oldFileName = fileName;
            if (newName == undefined || newName == "") {
                alert("File name can not be blank.");
                $('.txtDocumentName').css('border', '1px solid red');
                hideLoader();
                return false;
            }
            else if (!/^[a-z\d][a-z\d\()_\-.\s]+$/i.test(newName)) {
                alert("File name can not contain special characters.");
                $('.txtDocumentName').css('border', '1px solid red');
                hideLoader();
                return false;
            }
            else if (newName.indexOf('.') > 0 && newName.substring(newName.indexOf('.') + 1).toLowerCase() != 'pdf') {
                alert("Only File with type 'pdf' is allowed.");
                $('.txtDocumentName').css('border', '1px solid red');
                hideLoader();
                return false;
            }
            else {
                $('.txtDocumentName').css('border', '1px solid c4c4c4');
            }
            $.ajax(
                {
                    type: "POST",
                    url: '../EFileFolderJSONService.asmx/RenameDocumentForEdForms',
                    data: "{ renameFile:'" + newName + "',edFormsId:'" + edFormsId + "',oldFileName:'" + oldFileName + "', departmentId:'" + $('#<%= hdnDepartmentId.ClientID %>').val() + "', edFormsUserId:''}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result.d.length > 0)
                            alert("Failed to rename document. Please try again after sometime.");
                        hideLoader();
                        __doPostBack('', 'RefreshGrid@');
                    },
                    error: function (result) {
                        hideLoader();
                    }
                });
        }

        function ShowPreviewForEFileFlow(eFileFlowId, fileName) {
            showLoader();
            var folderID = <%= (int)Enum_Tatva.Folders.EdForms%>
                $.ajax({
                    type: "POST",
                    url: 'ViewEdForms.aspx/GetPreviewURL',
                    contentType: "application/json; charset=utf-8",
                    data: "{ documentID:'" + eFileFlowId + "', folderID:'" + folderID + "', shareID:'" + fileName + "', departmentId:'" + $('#<%= hdnDepartmentId.ClientID %>').val() + "'}",
                    dataType: "json",
                    success: function (data) {
                        if (data.d == "") {
                            hideLoader();
                            disableScrollbar();
                            alert("Failed to get object from cloud");
                            return false;
                        }
                        else
                            showPreview(data.d);
                    },
                    error: function (result) {
                        console.log('Failed' + result.responseText);
                        hideLoader();
                    }
                });
            return false;
        }

        function showPreview(URL) {
            hideLoader();
            disableScrollbar();
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//"
                    + window.location.hostname
                    + (window.location.port ? ':' + window.location.port : '');
            }
            var popup = document.getElementById("preview_popup");
            var overlay = document.getElementById("overlay");
            $('#reviewContent').attr('src', '../Office/Preview.aspx?data=' + window.location.origin + URL.trim() + "?v=" + "<%= DateTime.Now.Ticks%>");
            //$('#reviewContent').attr('src', src.trim());
            overlay.style.display = 'block';
            popup.style.display = 'block';
            $('#floatingCirclesG').css('display', 'none');
            //$('#preview_popup').css('top', ($(window).scrollTop() + 50) + 'px');
            $('#preview_popup').focus();
            return false;
        }

        function ConfirmDelete() {
            var result = confirm("Are you sure you want to delete this file ?");
            if (result)
                showLoader();
            else
                return false;
        }

        function disableScrollbar() {
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }

        function enableScrollbar() {
            $('html, body').css({
                'overflow': 'auto',
                'height': 'auto'
            });
        }
    </script>
    <div id="overlay" style="left: 0">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <div class="popup-mainbox preview-popup" id="preview_popup" style="display: none">
        <div class="popupcontrols" style="width: 100%; height: 5%;">
            <span id="popupclose" class="ic-icon ic-close"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 100%; box-sizing: border-box">
            <iframe id="reviewContent" style="width: 100%; height: 95%;"></iframe>
        </div>
    </div>
    <asp:Button ID="Button1ShowPopup2" runat="server" Style="display: none" />
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server"
        PopupControlID="pnlPopup2" DropShadow="true" BackgroundCssClass="modalBackground"
        Enabled="True" TargetControlID="Button1ShowPopup2" />
    <asp:Panel ID="pnlPopup2" Style="display: none"
        runat="server" class="popup-mainbox">
        <div>
            <div class="popup-head" id="Div1">
                Message
            </div>
            <div class="popup-scroller">
                <p>
                    Your document has been uploaded successfully...It will be converting to the edFile format in our server over the next few minutes, depending on the size of the uploaded document. You can now select additional files to upload or navigate away from this web page. 
                </p>
            </div>
            <div class="popup-btn">
                <asp:Button ID="ShowPopup2Button2" runat="server" OnClientClick="return onClientButtonClick()"
                    Text="Close" class="btn green" />
                <%--<input id="btnuploadclose" type="button" value="Close"  />--%>
            </div>
        </div>
    </asp:Panel>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style="width: 825px; height: 75px; margin-left: -30px;">
                <div class="Naslov_Protokalev1">
                    EdForms
                </div>
            </div>
            <div>
                <label style="display: block; margin-top: 15px; font-size: 15px;">Select User to get Departments</label>
                <select id="comboBoxUser" style="color: rgb(68,68,68); border-radius: 5px; background-color: #fff; border: 1px solid #aaa; box-shadow: 0 1px 0 #fff inset; background: linear-gradient(#fff 20%, #f6f6f6 50%, #eee 52%, #f4f4f4 100%)">
                </select>
            </div>
            <div>
                <label style="display: block; margin-top: 15px; font-size: 15px;">Select Department to upload EdForms</label>
                <select id="comboBoxDepartment" style="color: rgb(68,68,68); border-radius: 5px; background-color: #fff; border: 1px solid #aaa; box-shadow: 0 1px 0 #fff inset; background: linear-gradient(#fff 20%, #f6f6f6 50%, #eee 52%, #f4f4f4 100%)">
                </select>
            </div>
            <div id="uploadProgress" class="white_content" style="font-size: 14px;"></div>
            <fieldset id="uploadFieldset" style="font-size: 20px; font-family: 'Segoe UI'; font-weight: normal">
                <div id="divUpload">
                    <div class='clearfix'></div>
                    <asp:Image runat="server" ID="Image12" ImageUrl="../Images/loader_bg.gif" ImageAlign="Middle" />
                    <asp:Label ID="label1" runat="server"></asp:Label>
                    <asp:Label runat="server" ID="Label2" Style="display: none;">
                                <img align="absmiddle" alt="" src="../images/uploading.gif"/></asp:Label>


                    <ajaxtoolkit:ajaxfileupload id="ajaxfileuploadeFileFlow" runat="server" padding-bottom="4"
                        padding-left="2" padding-right="1" padding-top="4" throbberid="mythrobber"
                        maximumnumberoffiles="10"
                        allowedfiletypes="pdf"
                        azurecontainername=""
                        onuploadcomplete="AjaxfileuploadeFileFlow_onuploadcomplete1"
                        onclientuploadcomplete="onClientUploadComplete"
                        onuploadcompleteall="AjaxfileuploadeFileFlow_uploadcompleteall1"
                        onuploadstart="AjaxfileuploadeFileFlow_uploadstart1"
                        onclientuploadstart="onClientUploadStart"
                        onclientuploadcompleteall="onClientUploadCompleteAll"
                        contextkeys="2" xmlns:ajaxtoolkit="ajaxcontroltoolkit"
                        cssclass="boxWidth" />
                    <div class='clearfix'></div>
                    <div id="uploadCompleteInfo">
                    </div>
                </div>
            </fieldset>
            <table style="margin-top: 5%; width: 1000px">
                <tbody>
                    <tr>
                        <td>
                            <asp:HiddenField ID="hdnUserId" runat="server" />
                            <asp:HiddenField ID="hdnDepartmentId" runat="server" />
                            <asp:GridView ID="GridView1" runat="server" CssClass="Empty"
                                DataKeyNames="EdFormsId" OnSorting="GridView1_Sorting"
                                AllowSorting="True" AutoGenerateColumns="false">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%" ItemStyle-Height="35px" ItemStyle-Wrap="false" SortExpression="Name">
                                        <HeaderTemplate>
                                            <%--<asp:Label ID="lblDocumentName" runat="server" Text="Document Name" />--%>
                                            <asp:LinkButton ID="lblDocumentName1" runat="server" Text="File Name" CommandName="Sort" CommandArgument="Name" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("Name")%>' name="DocumentName" CssClass="lblDocumentName" />
                                            <asp:TextBox runat="server" MaxLength="100" Text='<%#Eval("Name")%>'
                                                ID="TextDocumentName" Style="display: none; height: 30px; width: 400px; padding-left: 10px;" name="txtDocumentName" CssClass="txtDocumentName" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Upload Date" SortExpression="CreatedDate" ItemStyle-Width="4%" />
                                    <asp:TemplateField ShowHeader="true" ItemStyle-Width="3%" ItemStyle-CssClass="actionMinWidth">
                                        <HeaderTemplate>
                                            <asp:Label ID="Label8" runat="server" Text="Action" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton Style="padding: 3px;" ID="LinkButton1" runat="server" CausesValidation="false" Text="Preview" CssClass="viewDocument"
                                                ToolTip="View File" OnClientClick='<%# "return ShowPreviewForEFileFlow(" + Eval("EdFormsId") + ",\"" + Eval("Name") + "\")" %>' />
                                            <asp:LinkButton Style="padding: 3px;" ID="lnkBtnEdit" runat="server" CausesValidation="false" Text="Edit" CssClass="editDocument"
                                                ToolTip="Rename File" CommandArgument='<%#string.Format("{0}",Eval("EdFormsId")) %>' />
                                            <asp:LinkButton Text="Update" ID="lnkBtnUpdate" runat="server" CausesValidation="True" AutoPostBack="False"
                                                ToolTip="Update" OnClientClick='<%# "return OnSaveRename(this, " + Eval("EdFormsId") + ",\"" + Eval("Name") + "\")" %>' CssClass="saveDocument" Style="display: none; padding: 3px;" name="lnkUpdate"></asp:LinkButton>
                                            <asp:LinkButton Text="Cancel" ID="lnkBtnCancel" runat="server" CausesValidation="False" AutoPostBack="False"
                                                OnClientClick="return cancleFileName(this)" ToolTip="Cancel" CssClass="cancelDocument" Style="display: none; padding: 3px;" name="lnkcancel"></asp:LinkButton>
                                            <asp:LinkButton Style="padding: 3px;" ID="lnkBtnDelete" runat="server" CausesValidation="false" Text="Delete" CssClass="deleteDocument" OnClientClick="return ConfirmDelete()"
                                                OnCommand="lnkBtnDelete_Command" CommandArgument='<%#string.Format("{0};{1}",Eval("EdFormsId"),Eval("Name")) %>' />
                                            <asp:HiddenField runat="server" ID="hdnEdFormsId" Value='<%# Eval("EdFormsId")%>' />
                                            <asp:HiddenField runat="server" ID="hdnFileName" Value='<%#Eval("Name")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label1" runat="server" CssClass="Empty" />There is no EdForms.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td class="prevnext" bgcolor="#f8f8f5">
                            <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                PageSize="15" OnPageSizeChanged="WebPager1_PageSizeChanged" CssClass="prevnext"
                                PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1"></cc1:WebPager>
                            <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                        </td>
                    </tr>
                </tbody>
            </table>

            <asp:Panel class="popupConfirmation" ID="DivDeleteConfirmation" Style="display: none"
                runat="server">
                <div class="popup_Container">
                    <div class="popup_Titlebar" id="PopupHeader">
                        <div class="TitlebarLeft">
                            Delete EdForms
                        </div>
                        <div class="TitlebarRight" onclick="$get('ButtonDeleteCancel').click();">
                        </div>
                    </div>
                    <div class="popup_Body">
                        <p>
                            Are you sure you want to delete this EdForms?
                        </p>
                        <p>
                            Please enter password to delete EdForms:
                                                                    <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                        </p>
                    </div>
                    <div class="popup_Buttons">
                        <input id="ButtonDeleleOkay" type="button" value="Yes" style="margin-left: 80px; width: 65px" />
                        <input id="ButtonDeleteCancel" type="button" value="No" style="margin-left: 20px; width: 65px" />
                    </div>
                </div>
            </asp:Panel>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
