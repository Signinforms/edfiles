﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Shinetech.DAL;
using System.IO;
using System.Web.Script.Serialization;
using System.Data;
using System.Web.Services;

public partial class Manager_ViewEdForms : System.Web.UI.Page
{
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
    private int DepartmentId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
        else
        {
            var argument = Request.Form["__EVENTARGUMENT"];
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
            {
                GetData();
                BindGrid();
            }
        }
    }

    protected void GetData()
    {
        this.EdFormsSource = General_Class.GetEdFormsForAdmin(string.IsNullOrEmpty(hdnUserId.Value) ? null : hdnUserId.Value, string.IsNullOrEmpty(hdnDepartmentId.Value) ? null : (int?)Convert.ToInt32(hdnDepartmentId.Value));
    }

    protected void BindGrid()
    {
        GridView1.DataSource = this.EdFormsSource;
        GridView1.DataBind();
        WebPager1.DataSource = this.EdFormsSource;
        WebPager1.DataBind();
    }

    [WebMethod]
    public static Dictionary<string, string> GetDepartmentsForAdmin()
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        try
        {
            DataTable dtDepartments = General_Class.GetDepartmentsForAdmin();
            if (dtDepartments != null)
            {
                dtDepartments.AsEnumerable().ToList().ForEach(d =>
                {
                    dict.Add(Convert.ToString(d.Field<int>("DepartmentId")), d.Field<string>("DepartmentName"));
                });
            }
        }
        catch (Exception ex)
        { }
        return dict;
    }

    [WebMethod]
    public static void LoadEdForms(string officeId = null, string departmentId = null)
    {
        Manager_ViewEdForms edForms = new Manager_ViewEdForms();
        try
        {
            edForms.EdFormsSource = General_Class.GetEdFormsForAdmin(officeId, string.IsNullOrEmpty(departmentId) ? null : (int?)Convert.ToInt32(departmentId));
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
        }
    }

    [WebMethod]
    public static string GetPreviewURL(int documentID, int folderID, string shareID, string departmentId)
    {
        RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
        string errorMsg = string.Empty;
        string path = rackSpaceFileUpload.GeneratePath(shareID.ToLower(), Enum_Tatva.Folders.EdForms, departmentId);
        string newFileName1 = rackSpaceFileUpload.GetObject(ref errorMsg, path, null, Enum_Tatva.Folders.EdForms.GetHashCode());
        EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Preview.GetHashCode(), 0, 0, documentID);
        if (string.IsNullOrEmpty(errorMsg))
            return Common_Tatva.RackSpaceEdFormsDownload + "/" + Sessions.SwitchedRackspaceId + "/" + newFileName1;
        else
            return string.Empty;
    }

    [WebMethod]
    public static void SaveSelectedDepartment(string officeId, int departmentId)
    {
        try
        {
            Manager_ViewEdForms manager_ViewEdForms = new Manager_ViewEdForms();
            Sessions.DepartmentId = Convert.ToInt32(departmentId);
            Sessions.EdFormsOfficeId = officeId;
        }
        catch (Exception ex) { }
    }

    #region FileUpload
    protected void AjaxfileuploadeFileFlow_onuploadcomplete1(object sender, AjaxFileUploadEventArgs file)
    {
        try
        {
            string path = Server.MapPath("~/" + Common_Tatva.eFileFlow);
            string uid = Sessions.SwitchedRackspaceId;

            string fullPath = Path.Combine(path, Sessions.SwitchedRackspaceId);

            if (ajaxfileuploadeFileFlow.IsInFileUploadPostBack)
            {
                var fullname = Path.Combine(fullPath, file.FileName);
                string fileName = Path.GetFileName(fullname);
                decimal size;

                size = Math.Round((decimal)(file.FileSize / 1024) / 1024, 2);
                Guid guid = new Guid(Sessions.SwitchedRackspaceId);


                RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                string errorMsg = string.Empty;
                string newFileName1 = rackSpaceFileUpload.UploadObject(ref errorMsg, file.FileName, Enum_Tatva.Folders.EdForms, file.GetStreamContents(), Convert.ToString(Sessions.DepartmentId));
                if (string.IsNullOrEmpty(errorMsg))
                {
                    int edFormsId = General_Class.DocumentsInsertForEdFormsAdmin(newFileName1.Substring(newFileName1.LastIndexOf('/') + 1), Sessions.EdFormsOfficeId, Sessions.DepartmentId, size);
                    //eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Upload.GetHashCode(), 0, 0, Convert.ToInt32(edFormId));
                }
                else
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Failed to upload file. Please try again after sometime." + "\");", true);
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void AjaxfileuploadeFileFlow_uploadcompleteall1(object sender, AjaxFileUploadCompleteAllEventArgs e)
    {
        var startedAt = (DateTime)Session["uploadTime"];
        var now = DateTime.Now;
        e.ServerArguments = new JavaScriptSerializer()
            .Serialize(new
            {
                duration = (now - startedAt).Seconds,
                time = DateTime.Now.ToShortTimeString()
            });

    }

    protected void AjaxfileuploadeFileFlow_uploadstart1(object sender, AjaxFileUploadStartEventArgs e)
    {
        var now = DateTime.Now;
        e.ServerArguments = now.ToShortTimeString();
        Session["uploadTime"] = now;
    }

    #endregion

    protected DataTable EdFormsSource
    {
        get
        {
            return this.Session["EdForms_Admin"] as DataTable;
        }
        set
        {
            this.Session["EdForms_Admin"] = value;
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.EdFormsSource);
        Source.Sort = e.SortExpression + " " + sortDirection;
        GridView1.DataSource = Source.ToTable();
        GridView1.DataBind();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void WebPager1_PageSizeChanged(object s, Shinetech.Framework.Controls.PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.EdFormsSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageIndexChanged(object sender, Shinetech.Framework.Controls.PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.EdFormsSource;
        WebPager1.DataBind();
    }

    protected void lnkBtnDelete_Command(object sender, CommandEventArgs e)
    {
        try
        {
            LinkButton lnkbtnDelete = sender as LinkButton;
            GridViewRow gvrow = lnkbtnDelete.NamingContainer as GridViewRow;
            int edFormsId = Convert.ToInt32(e.CommandArgument.ToString().Split(';')[0]);
            string errorMsg = string.Empty;
            DataTable dtFiles = General_Class.GetEdFormsById(edFormsId);
            dtFiles.AsEnumerable().ToList().ForEach(d =>
            {
                var file = d.Field<string>("Name");
                int id = d.Field<int>("edFormsId");
                string pathName = Enum_Tatva.GetEnumDescription(Enum_Tatva.Folders.EdForms) + "/" + hdnDepartmentId.Value + "/" + file;
                EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
                eFileFolderJSONWS.DeleteEdForms(pathName, Sessions.SwitchedRackspaceId, file, ref errorMsg, id);
                if (string.IsNullOrEmpty(errorMsg))
                {
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "File " + file + " has been deleted successfully" + "\");", true);
                }
                else
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Failed to delete file: " + file + ". Please try again after sometime." + "\");", true);
                GetData();
                BindGrid();
            });
        }
        catch (Exception ex)
        { }
        finally
        {
            ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
        }
    }
}