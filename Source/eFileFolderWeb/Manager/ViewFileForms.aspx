<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    CodeFile="ViewFileForms.aspx.cs" Inherits="Manage_FileForms" Title="" %>

<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.ComponentModel" %>
<%--<%@ Register Assembly="ajax.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" charset="utf-8">
        function openwindow() {
            var url = 'ExportRequests.aspx';
            var name = 'ExportRequests';
            var iWidth = 250;
            var iHeight = 200;
            var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
            var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
            window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',toolbar=no,menubar=no,scrollbars=no,resizeable=no,location=no,status=no');
        }

        function openboxwindow(id) {
            var url = 'ModifyBox.aspx?id=' + id;
            var name = 'ModifyBox';
            window.open(url, name, '');
        }
    </script>
    <table>
        <tr>
            <td valign="top" style="background-repeat: no-repeat;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="925" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                            style="background-repeat: repeat-x;">
                            <div style="background: url(images/inner_b.gif); background-position: right top;
                                background-repeat: no-repeat; margin-top: 10px;">
                                <div style="width: 825px; height: 75px;">
                                    <div class="Naslov_Protokalev">
                                        View FileForm Requests
                                    </div>
                                </div>
                                <div class="Naslov_Plav" style="width: 750px;">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin: 0px 0 20px 25px;">
                                        <tr>
                                            <td height="20" colspan="2" align="left" valign="top" class="tekstDef" style="padding-top: 0px;
                                                padding-bottom: 0px;">
                                                Enter search term: (OfficeName, Lastname, Firstname..)
                                            </td>
                                            <td colspan="2" align="left" class="podnaslov">
                                                <input name="textfield3" type="text" runat="server" class="form_tekst_field" id="textfield3"
                                                    size="30" maxlength="50" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" colspan="2" align="left" valign="top" class="tekstDef" style="padding-top: 0px;
                                                padding-bottom: 0px;">
                                                Enter Start date:
                                            </td>
                                            <td colspan="2" align="left" class="podnaslov">
                                                <asp:TextBox name="textfielddob" runat="server" class="form_tekst_field" ID="textfielddob"
                                                    size="30" />
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" PopupPosition="Right" runat="server"
                                                    TargetControlID="textfielddob" Format="MM-dd-yyyy">
                                                </ajaxToolkit:CalendarExtender>
                                            </td>
                                            <td width="128">
                                                <asp:Button ID="searchButton1" OnClick="OnSearchButton_Click" runat="server" Text="Search"
                                                    Width="60" Height="21" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" colspan="2" align="left" valign="top" class="tekstDef" style="padding-top: 0px;
                                                padding-bottom: 0px;">
                                                End date:
                                            </td>
                                            <td colspan="2" align="left" class="podnaslov">
                                                <asp:TextBox name="textfielddob" runat="server" class="form_tekst_field" ID="textfieldedob"
                                                    size="30" />
                                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" PopupPosition="Right" runat="server"
                                                    TargetControlID="textfieldedob" Format="MM-dd-yyyy">
                                                </ajaxToolkit:CalendarExtender>
                                            </td>
                                            <td width="128">
                                                <input id="imgbuttn1" width="60px" type="button" height="21px" value="Export Excel"
                                                    title="Export Requests" onclick="openwindow()" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table style="margin: 0px 20px 0px 30px; width: 925px">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GridView1" runat="server" DataKeyNames="FileID,UID" OnSorting="GridView1_Sorting"
                                                            AllowSorting="True" AutoGenerateColumns="false" Width="100%" BorderWidth="0">
                                                            <PagerSettings Visible="False" />
                                                            <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                            <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                            <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                            <Columns>
                                                                <asp:BoundField DataField="FileID" HeaderText="Request ID" SortExpression="FileID">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="OfficeName" HeaderText="OfficeName" SortExpression="OfficeName">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>                                                                
                                                                <asp:BoundField DataField="FileNumber" HeaderText="FileNumber" SortExpression="FileNumber">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="YearDate" HeaderText="YearDate" SortExpression="YearDate">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>

                                                                <asp:BoundField DataField="PhoneNumber" HeaderText="PhoneNumber" SortExpression="PhoneNumber">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="RequestedBy" HeaderText="RequestedBy" SortExpression="RequestedBy">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="CreateDate" HeaderText="RequestDate" DataFormatString="{0:MM.dd.yyyy hh:mm}"
                                                                    SortExpression="CreateDate" />                                                                
                                                                <asp:BoundField DataField="Name" HeaderText="Creator" SortExpression="Name">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>                                                                
                                                                <asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="Label101" Text="Status" runat="server" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton41" runat="server" CausesValidation="false" Text='<%# Bind("Status") %>'
                                                                            OnClick="LinkButtonStatus_Click" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="Label11" Text="Action" runat="server" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton5" runat="server" CausesValidation="false" Text="Create"
                                                                           OnCommand="LinkButton5_Click" CommandArgument='<%# Eval("FileID")%>'  />
                                                                        <asp:LinkButton ID="LinkButton6" runat="server" CausesValidation="false" Text="Upload"
                                                                           OnCommand="LinkButton6_Click" CommandArgument='<%# Eval("UID")%>'  />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                                    <tr>
                                                                        <td align="left" class="podnaslov">
                                                                            <asp:Label ID="Label1" runat="server" />There is no File Form Request.
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>
                                                        <!-- Tabs Editor Panel-->
                                                        <asp:Button Style="display: none" ID="Button1ShowPopup" runat="server"></asp:Button>
                                                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
                                                            CancelControlID="btnClose1" PopupControlID="Panel1Tabs" TargetControlID="Button1ShowPopup">
                                                        </ajaxToolkit:ModalPopupExtender>
                                                        <!-- ModalPopup Panel-->
                                                        <asp:Panel Style="display: none; z-index: 10001" ID="Panel1Tabs" runat="server" Width="600px"
                                                            BackColor="lightblue">
                                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <div id="divTabNum">
                                                                        &nbsp;File Details</div>
                                                                    <ajaxToolkit:ColorPickerExtender ID="ColorPicker1" runat="server" />
                                                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                                        <ContentTemplate>
                                                                            <asp:GridView ID="GridView2" ShowFooter="true" BorderWidth="2" runat="server" Width="100%"
                                                                                AutoGenerateColumns="false" DataKeyNames="FileID">
                                                                                <PagerSettings Visible="False" />
                                                                                <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                                                <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                                                <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                                                <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                                                <FooterStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                                                <Columns>
                                                                                    <asp:BoundField DataField="FileID" HeaderText="File ID" SortExpression="FileID">
                                                                                        <HeaderStyle CssClass="form_title" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName">
                                                                                        <HeaderStyle CssClass="form_title" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName">
                                                                                        <HeaderStyle CssClass="form_title" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="FileNumber" HeaderText="File#" SortExpression="FileNumber">
                                                                                        <HeaderStyle CssClass="form_title" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="YearDate" HeaderText="Year" SortExpression="YearDate">
                                                                                        <HeaderStyle CssClass="form_title" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="UploadDate" HeaderText="Sync Date" SortExpression="UploadDate"
                                                                                        DataFormatString="{0:MM.dd.yyyy}" NullDisplayText="">
                                                                                        <HeaderStyle CssClass="form_title" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="ConvertDate" HeaderText="ConvertedDate" SortExpression="ConvertDate"
                                                                                        DataFormatString="{0:MM.dd.yyyy}" NullDisplayText="">
                                                                                        <HeaderStyle CssClass="form_title" />
                                                                                    </asp:BoundField>
                                                                                </Columns>
                                                                            </asp:GridView>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                            <div align="right" style="width: 95%; background-color: lightblue">
                                                                <asp:Button ID="btnClose1" runat="server" Text="Close" Width="50px" />
                                                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                                            </div>
                                                        </asp:Panel>
                                                        <!-- Tabs Editor Panel-->
                                                        <asp:Button Style="display: none" ID="Button2ShowPopup" runat="server"></asp:Button>
                                                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalBackground"
                                                            CancelControlID="btnClose2" PopupControlID="Panel1Status" TargetControlID="Button2ShowPopup">
                                                        </ajaxToolkit:ModalPopupExtender>
                                                        <!-- ModalPopup Panel-->
                                                        <asp:Panel Style="display: none; z-index: 10001" ID="Panel1Status" runat="server"
                                                            Width="200px" BackColor="lightblue">
                                                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <div id="div1">
                                                                        &nbsp;Request Status</div>
                                                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                                        <ContentTemplate>
                                                                            <div style="margin: 10px auto; background-color: #DFDFDF">
                                                                                <span>Status</span><asp:DropDownList ID="requestStatus" runat="server" Width="100px"
                                                                                    Style="margin-left: 10px">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="btnOK2" EventName="Click" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                            <div align="right" style="width: 95%; background-color: lightblue">
                                                                <asp:Button ID="btnClose2" runat="server" Text="Close" Width="50px" />
                                                                <asp:Button ID="btnOK2" runat="server" Text="OK" OnClick="btnOK2_click" Width="50px" />
                                                                <asp:HiddenField ID="HiddenField2" runat="server" />
                                                            </div>
                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="prevnext" bgcolor="#f8f8f5">
                                                        <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                                            PageSize="15" OnPageSizeChanged="WebPager1_PageSizeChanged" CssClass="prevnext"
                                                            PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1"></cc1:WebPager>
                                                        <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>

                                <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <table style="margin: 0px 20px 0px 30px; width: 925px">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GridView3" runat="server" DataKeyNames="FileID" OnSorting="GridView3_Sorting"
                                                            AllowSorting="True" AutoGenerateColumns="false" Width="100%" BorderWidth="0">
                                                            <PagerSettings Visible="False" />
                                                            <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                            <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                            <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                            <Columns>
                                                                <asp:BoundField DataField="FileID" HeaderText="FileID" SortExpression="FileID">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="30px"
                                                                    ItemStyle-CssClass="table_tekst_edit">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="Label34" Text="BoxName" runat="server" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton21" runat="server" Text='<%# Bind("BoxName") %>' CausesValidation="false"
                                                                            OnClientClick='<%#string.Format("javascript:openboxwindow({0})",Eval("BoxId"))%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--<asp:BoundField DataField="BoxNumber" HeaderText="BoxNumber" SortExpression="BoxNumber">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>--%>
                                                                <asp:BoundField DataField="Name" HeaderText="OfficeName" SortExpression="Name">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="FileName" HeaderText="FileName" SortExpression="FileName">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="FileNumber" HeaderText="FileNumber" SortExpression="FileNumber">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate" DataFormatString="{0:MM.dd.yyyy hh:mm}">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="RequestDate" HeaderText="RequestDate" SortExpression="RequestDate" DataFormatString="{0:MM.dd.yyyy hh:mm}">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="Label101" Text="Status" runat="server" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton41" runat="server" CausesValidation="false" Text='<%# Bind("Status") %>'
                                                                            OnClick="LinkButtonStatus3_Click" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="Label11" Text="Action" runat="server" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="false" Text="Create"
                                                                            OnCommand="LinkButton3_Click" CommandArgument='<%# Eval("FileID")%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <EmptyDataTemplate>
                                                                <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                                    <tr>
                                                                        <td align="left" class="podnaslov">
                                                                            <asp:Label ID="Label1" runat="server" />There is no Stored Paper File.
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>

                                                        <!-- Tabs Editor Panel-->
                                                        <asp:Button Style="display: none" ID="Button3ShowPopup" runat="server"></asp:Button>
                                                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender3" runat="server" BackgroundCssClass="modalBackground"
                                                            CancelControlID="ButtonClose2" PopupControlID="Panel3" TargetControlID="Button3ShowPopup">
                                                        </ajaxToolkit:ModalPopupExtender>
                                                        <!-- ModalPopup Panel-->
                                                        <asp:Panel Style="display: none; z-index: 10001" ID="Panel3" runat="server"
                                                            Width="200px" BackColor="lightblue">
                                                            <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <div id="div2">
                                                                        &nbsp;Request Status</div>
                                                                    <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                                                        <ContentTemplate>
                                                                            <div style="margin: 10px auto; background-color: #DFDFDF">
                                                                                <span>Status</span><asp:DropDownList ID="DropDownList3" runat="server" Width="100px"
                                                                                    Style="margin-left: 10px">
                                                                                </asp:DropDownList>
                                                                            </div>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="ButtonOK3" EventName="Click" />
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                            <div align="right" style="width: 95%; background-color: lightblue">
                                                                <asp:Button ID="ButtonClose2" runat="server" Text="Close" Width="50px" />
                                                                <asp:Button ID="ButtonOK3" runat="server" Text="OK" OnClick="btnOK3_click" Width="50px" />
                                                                <asp:HiddenField ID="HiddenField3" runat="server" />
                                                            </div>
                                                        </asp:Panel>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="prevnext" bgcolor="#f8f8f5">
                                                        <cc1:WebPager ID="WebPager3" runat="server" OnPageIndexChanged="WebPager3_PageIndexChanged"
                                                            PageSize="15" OnPageSizeChanged="WebPager3_PageSizeChanged" CssClass="prevnext"
                                                            PagerStyle="OnlyNextPrev" ControlToPaginate="GridView3"></cc1:WebPager>
                                                        <asp:HiddenField ID="SortDirection3" runat="server" Value=""></asp:HiddenField>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="WebPager3" EventName="PageIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
