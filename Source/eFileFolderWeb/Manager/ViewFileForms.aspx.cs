using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using AjaxControlToolkit;
using DataQuicker2.Framework;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;

public partial class Manage_FileForms : BasePage
{
    private static readonly string PDFPATH = ConfigurationManager.AppSettings["PdfPath"];
    //public string IsShowInsert;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
    }

    #region 数据绑定
    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData()
    {
        this.DataSource = FileFolderManagement.GetFileFormsWithStatus();

        this.BoxDataSource = FileFolderManagement.GetPaperFiles();
    }
    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();

        WebPager3.DataSource = this.BoxDataSource;
        WebPager3.DataBind();
    }
    #endregion

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["FileFormSource"] as DataTable;
        }
        set
        {
            this.Session["FileFormSource"] = value;
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

 
    //File Quantity
    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;
        string formId = Convert.ToString(this.GridView1.DataKeys[row.RowIndex].Value);
        HiddenField1.Value = formId;

        DataView view = new DataView(GetFilesDataSource(formId));
        GridView2.DataSource = view;

        GridView2.DataBind();
        UpdatePanel3.Update();
        ModalPopupExtender1.Show();

    }

    protected void LinkButtonStatus_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;
        string fileId = Convert.ToString(this.GridView1.DataKeys[row.RowIndex].Value);
        HiddenField2.Value = fileId;

        requestStatus.Items.Clear();
        requestStatus.Items.Add("JI");
        requestStatus.Items.Add("NR");
		requestStatus.Items.Add("IP");
        requestStatus.Items.Add("C");

        requestStatus.SelectedValue = btnLink.Text.Trim();
        UpdatePanel2.Update();
        ModalPopupExtender2.Show();

        //todo: change the status manually

    }

    protected void LinkButtonStatus3_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;
        string fileId = Convert.ToString(this.GridView3.DataKeys[row.RowIndex].Value);
        HiddenField3.Value = fileId;

        DropDownList3.Items.Clear();
        DropDownList3.Items.Add("JI");
        DropDownList3.Items.Add("NR");
		DropDownList3.Items.Add("IP");
        DropDownList3.Items.Add("C");

        DropDownList3.SelectedValue = btnLink.Text.Trim();
        UpdatePanel6.Update();
        ModalPopupExtender3.Show();

        //todo: change the status manually

    }

    protected void btnOK2_click(object sender, EventArgs e)
    {
        var fileId = HiddenField2.Value;
        var status = requestStatus.SelectedValue;

        var formfile = new FormFile(Convert.ToInt32(fileId));

        if (formfile.IsExist)
        {
            formfile.Status.Value = status;
            formfile.Update();

            if (string.IsNullOrEmpty(this.textfield3.Value.Trim()))
            {
                GetData();
                BindGrid();
            }
            else
            {
                OnSearchButton_Click(sender, e);
            }
            
        }

        UpdatePanel2.Update();
        ModalPopupExtender2.Hide();
    }

    protected void btnOK3_click(object sender, EventArgs e)
    {
        var formId = HiddenField3.Value;
        var status = DropDownList3.SelectedValue;

        var paperFile = new PaperFile(Convert.ToInt32(formId));

        if (paperFile.IsExist)
        {
            paperFile.Status.Value = status;
            paperFile.Update();

            if (string.IsNullOrEmpty(this.textfield3.Value.Trim()))
            {
                GetData();
                BindGrid();
            }
            else
            {
                OnSearchButton_Click(sender, e);
            }

        }

        UpdatePanel6.Update();
        ModalPopupExtender3.Hide();
    }

    //Resolver File Request for clients
    protected void LinkButton5_Click(object sender, CommandEventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;        

        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;

        string pid = e.CommandArgument.ToString();
        string uid = Convert.ToString(this.GridView1.DataKeys[row.RowIndex].Values[1]);

        this.Session["FileForm_UID"] = uid;
        this.Session["FileForm_FID"] = pid;

        this.Response.Redirect("~/Manager/RequestDocument.aspx");
    }

    protected void LinkButton6_Click(object sender, CommandEventArgs e)
    {
        var fid = e.CommandArgument.ToString();
        this.Session["FileForm_UploadFID"] = fid;
        this.Response.Redirect("~/Manager/UploadPaperFile.aspx");
    }

    public DataTable GetFilesDataSource(string formId)
    {
        FormFile dv = new FormFile();
        ObjectQuery query = dv.CreateQuery();
        query.SetCriteria(dv.FormId, Convert.ToInt32(formId));

        DataTable table = new DataTable();

        try
        {
            query.Fill(table);

        }
        catch
        {
            table = null;
        }
        return table;
    }

    public void FillGridView()
    {
        string formId = HiddenField1.Value;

        DataTable table = GetFilesDataSource(formId);
        DataView view = new DataView(table);
        GridView2.DataSource = view;
        GridView2.DataBind();

    }



    protected void btnClose_Click(object sender, EventArgs e)
    {
        GetData();
        BindGrid();
        UpdatePanel1.Update();
    }


    protected void OnSearchButton_Click(object sender, EventArgs e)
    {
        string keyword = this.textfield3.Value.Trim();
        if (!string.IsNullOrEmpty(textfielddob.Text.Trim()))
        {
            if (string.IsNullOrEmpty(textfieldedob.Text.Trim()))
            {
                this.DataSource = FileFolderManagement.GetAllFileFormsWithStatusByKey(keyword, textfielddob.Text.Trim());
                this.BoxDataSource = FileFolderManagement.GetAllPaperFilesByKey(keyword, textfielddob.Text.Trim());
            }
            else
            {
                this.DataSource = FileFolderManagement.GetAllFileFormsWithStatusByKey(keyword, textfielddob.Text.Trim(), textfieldedob.Text.Trim());
                this.BoxDataSource = FileFolderManagement.GetAllPaperFilesByKey(keyword, textfielddob.Text.Trim(), textfieldedob.Text.Trim());
            }
        }
        else
        {
            this.DataSource = FileFolderManagement.GetAllFileFormsWithStatusByKey(keyword);
            this.BoxDataSource = FileFolderManagement.GetAllPaperFilesByKey(keyword);
        }
        

        this.BindGrid();
    }

    protected void OnExportSumButton_Click(object sender, EventArgs e)
    {
    }

    protected void LinkButton3_Click(object sender, CommandEventArgs e)
    {
        string pid = e.CommandArgument.ToString();
        string uid = Membership.GetUser().ProviderUserKey.ToString();

        PaperFile file = new PaperFile(Convert.ToInt32(pid));

        if (file.IsExist)
        {
            Account account = new Account(file.UserID.Value);
            string userId = account.UID.Value;

            this.Response.Redirect(string.Format("~/Office/RequestPaperFile.aspx?id={0} &uid={1}", pid, userId));
        }

    }

    private SortDirection Sort_Direction3
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection3.Value))
            {
                SortDirection3.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection3.Value);

        }
        set
        {
            this.SortDirection3.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }


    protected void GridView3_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction3 == SortDirection.Ascending)
        {
            this.Sort_Direction3 = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction3 = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.BoxDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.BoxDataSource = Source.ToTable();
        BindGrid();
    }

    protected void WebPager3_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager3.CurrentPageIndex = e.NewPageIndex;
        WebPager3.DataSource = this.BoxDataSource;
        WebPager3.DataBind();
    }

    protected void WebPager3_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager3.CurrentPageIndex = 0;
        WebPager3.DataSource = this.BoxDataSource;
        WebPager3.DataBind();
    }

    public DataTable BoxDataSource
    {
        get
        {
            return this.Session["StoredPaperFiles"] as DataTable;
        }
        set
        {
            this.Session["StoredPaperFiles"] = value;
        }
    }
}
