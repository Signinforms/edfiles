<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true" CodeFile="ViewGiftNumber.aspx.cs" Inherits="ViewGiftNumber" Title="" %>
<%@ Import Namespace="System.Data" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script language="javascript" type="text/javascript">
function openwindow()
{
    var url='ExportGifts.aspx'; 
    var name='ExportGifts';
    var iWidth=250; 
    var iHeight=200; 
    var iTop = (window.screen.availHeight-30-iHeight)/2; 
    var iLeft = (window.screen.availWidth-10-iWidth)/2; 
    window.open(url,name,'height='+iHeight+',,innerHeight='+iHeight+',width='+iWidth+',innerWidth='+iWidth+',top='+iTop+',left='+iLeft+',toolbar=no,menubar=no,scrollbars=no,resizeable=no,location=no,status=no');
}

function openwindow1()
{
    var url='ExportUsers.aspx'; 
    var name='ExportUsers';
    var iWidth=250; 
    var iHeight=200; 
    var iTop = (window.screen.availHeight-30-iHeight)/2; 
    var iLeft = (window.screen.availWidth-10-iWidth)/2; 
    window.open(url,name,'height='+iHeight+',,innerHeight='+iHeight+',width='+iWidth+',innerWidth='+iWidth+',top='+iTop+',left='+iLeft+',toolbar=no,menubar=no,scrollbars=no,resizeable=no,location=no,status=no');
}


    </script>
<table>
<tr>
  <td height="186" valign="top" style="background-repeat: no-repeat;"><asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <contenttemplate>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="725px" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                            style="background-repeat: repeat-x;"><div style="background: url(images/inner_b.gif); background-position: right top;
                                background-repeat: no-repeat; margin-top: 10px;"/>
          <div style="width: 725px; height: 60px;">
            <div class="Naslov_Zelen"> View Gift Number</div>
          </div>
          <div class="podnaslovUser" style="padding: 1px; background-color: #f6f5f2;">Below is a list of gift numbers. You may select 
           a date to view gift numbers, or generate and export these gift numbers.</div>
          <div>
            <table style="margin: 0px 10px 0px 10px" cellspacing="1" cellpadding="0" width="920"
                                            bgcolor="#ffffff" border="0"> 
              <tr><td nowrap="nowrap"> <div style="padding: 1px; background-color: #f6f5f2;"><label style="font-size:14px; font-weight:bold">Select Start Date:&nbsp;</label><asp:TextBox ID="TextBoxStartDate" runat="server" class="form_tekst_field" width="85px"/>
                     <ajaxToolkit:CalendarExtender ID="CalendarExtender1" PopupPosition="Right" runat="server" TargetControlID="TextBoxStartDate" Format="MM-dd-yyyy"  />
                     <label style="font-size:14px; font-weight:bold">End Date:&nbsp;</label><asp:TextBox ID="TextBoxEndDate" runat="server" class="form_tekst_field" width="85px"/>
                     <ajaxToolkit:CalendarExtender ID="CalendarExtender2" PopupPosition="Right" runat="server" TargetControlID="TextBoxEndDate" Format="MM-dd-yyyy"  />
                     <asp:Button id="btnSearch" runat="server" CausesValidation="false" Text="Search" width="60px" height="21px"  OnClick="btnSearch_Click"/>
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img id="imgbuttn1" width="60px" height="21px" alt="Export" src="../images/export_btn.gif"  onclick="openwindow()"/>
                     
                     &nbsp;&nbsp;&nbsp;<img id="imgUsers" width="60px" height="21px" alt="Export Users" src="../images/export_users.gif"  onclick="openwindow1()"/></div>
                     </td></tr>                             
              <tr>
                <td><asp:GridView ID="GridView1" runat="server" DataKeyNames="GiftNumber" OnSorting="GridView1_Sorting"
                        AllowSorting="True" AutoGenerateColumns="false" Width="100%" BorderWidth="2">
                    <PagerSettings Visible="False" />
                    <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                    <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                    <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                    <Columns>
                    <asp:BoundField DataField="GiftNumber" HeaderText="Gift Number" SortExpression="GiftNumber">
                      <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                    <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName">
                      <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                    <asp:BoundField DataField="FolderCount" HeaderText="FolderCount" SortExpression="FolderCount">
                      <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                    <asp:BoundField DataField="UseFlag" HeaderText="Used" SortExpression="UseFlag">
                      <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                    <asp:BoundField DataField="InputDate" HeaderText="InputDate" SortExpression="InputDate">
                      <HeaderStyle CssClass="form_title" />
                    </asp:BoundField>
                    </Columns> <EmptyDataTemplate><table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                        <tr>
                                            <td align="left" class="podnaslov">
                                                <asp:Label ID="Label1" runat="server" />There is no gift numbers.
                                            </td>
                                        </tr>
                                    </table></EmptyDataTemplate>
                  </asp:GridView>
                </td>
              </tr>
              <tr>
                <td class="prevnext" bgcolor="#f8f8f5"><cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                PageSize="15" CssClass="prevnext" PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1"> </cc1:WebPager>
                  <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                </td>
              </tr>
              <tr><td><div style="padding: 1px; background-color: #f6f5f2;"><label style="font-size:14px; font-weight:bold">Gift Count:&nbsp;</label>
              <asp:TextBox id="txtBoxCount" runat="server" align="center" width="85px" Text="1000"/>
               <asp:RegularExpressionValidator ID="digitalV" ControlToValidate="txtBoxCount" runat="server"
                                        ErrorMessage="<b>Required Field Missing</b><br />It is not a digital format or the max number is 9999." ValidationExpression="^\d{1,4}"
                                        Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8"
                                        TargetControlID="digitalV" HighlightCssClass="validatorCalloutHighlight" />
                     <asp:Button id="btnGenerate" runat="server" CausesValidation="false" Text="Generate" width="60px" 
                     OnClick="btnGenerate_Click" align="center" valign="middle"/><ajaxToolkit:ConfirmButtonExtender ID="ButtonGGift" runat="server" ConfirmText="Are you sure to generate gift numbers ?"
                         TargetControlID="btnGenerate"></ajaxToolkit:ConfirmButtonExtender></div>
              </td>
              </tr>
            </table>
            </contenttemplate>
            <triggers>
              <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged"></asp:AsyncPostBackTrigger>
            </triggers>
            </asp:UpdatePanel>
          </div></td>
        <td width="10" align="center"><img src="../images/lin.gif" width="1" height="453" vspace="10" /></td>
      </tr>
    </table>
</asp:Content>
