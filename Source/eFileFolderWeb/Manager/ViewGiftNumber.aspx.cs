using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using Shinetech.DAL;
using Shinetech.Framework.Controls;
using Shinetech.Utility;

public partial class ViewGiftNumber : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CalendarExtender1.SelectedDate = DateTime.Now.AddDays(-1);
            CalendarExtender2.SelectedDate = DateTime.Now;
            GetData();//重新获取操作后的数据源
            BindGrid();//绑定GridView,为删除服务
        }
    }

    #region 分页
    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        if (this.DataSource != null)
        {
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource_Gifts"] as DataTable;
        }
        set
        {
            this.Session["DataSource_Gifts"] = value;
        }
    }


    #endregion

    #region 数据绑定
    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData()
    {
        if (User.IsInRole("Administrators"))
        {

            string dateStart = TextBoxStartDate.Text != "" ? TextBoxStartDate.Text : DateTime.Now.AddDays(-1).ToShortDateString();
            string dateEnd = TextBoxEndDate.Text != "" ? TextBoxEndDate.Text : DateTime.Now.ToShortDateString();
            CalendarExtender1.SelectedDate = DateTime.Parse(dateStart);
            CalendarExtender2.SelectedDate = DateTime.Parse(dateEnd);
            this.DataSource = FileFolderManagement.GetGiftNumbers(DateTime.Parse(dateStart), DateTime.Parse(dateEnd));
        }
        else
        {
            throw new ApplicationException("You havn't ther rights to view partners");
        }
    }
    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }
    #endregion

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();//重新设置数据源，绑定
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        GetData();//重新获取操作后的数据源
        BindGrid();//绑定GridView,为删除服务
    }

    protected void btnGenerate_Click(object sender, EventArgs e)
    {
        string txtCount = txtBoxCount.Text.Trim();
        GiftGenerator giftGen = new GiftGenerator();
        try
        {
            giftGen.Maximum = 15;
            giftGen.ConsecutiveCharacters = true ;
            giftGen.RepeatCharacters = true;
            giftGen.ExcludeSymbols = true;

            int count = Convert.ToInt32(txtCount);

            if(count>9999)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "The max number can't be more than 9999." + "\");", true);
            }
            string giftnumber = "";

            DataTable table = this.DataSource;
            table.Rows.Clear();

            DataRow row;

            for(int i=0;i<count;i++)
            {
                giftnumber = giftGen.Generate();
                giftnumber = giftnumber.Substring(0, 10);
                try
                {
                    Gift gift = new Gift();
                    gift.GiftNumber.Value = giftnumber;
                    gift.Create();

                    row = table.NewRow();
                    row["GiftNumber"] = giftnumber;
                    row["InputDate"] = DateTime.Now;
                    row["UseFlag"] = false;
                    row["FolderCount"] = 25;
                    table.Rows.Add(row);

                }catch
                {
                   
                }
            }

            BindGrid();
            this.UpdatePanel1.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Generate Gift Numbers Successfully" + "\");", true);
            return;
            
        }catch(Exception exp)
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + exp.Message+ "\");", true);

        }
    }

    protected void btnOutput_Click(object sender, ImageClickEventArgs e)
    {
        
    }

    private XmlDataDocument GenerateXmlDocument(DataTable table)
    {
        DataSet ds = new DataSet();
        ds.Tables.Add(table);
        XmlDataDocument doc = new XmlDataDocument(ds);

        return doc;
    }

    private DataTable MakeDataTable()
    {
        // Create new DataTable.
        DataTable table = new DataTable();

        // Declare DataColumn and DataRow variables.
        DataColumn column;

        // Create new DataColumn, set DataType, ColumnName
        // and add to DataTable.    
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "ID";
        table.Columns.Add(column);

        // Create second column.
        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "GiftNumber";
        table.Columns.Add(column);

        return table;
    }
}
