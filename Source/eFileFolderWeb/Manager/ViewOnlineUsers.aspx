﻿<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    CodeFile="ViewOnlineUsers.aspx.cs" Inherits="Manager_ViewOnlineUsers" %>

<%@ Import Namespace="System.Data" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <table>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td width="725px" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                                        style="background-repeat: repeat-x;">
                                        <div style="background: url(images/inner_b.gif); background-position: right top; background-repeat: no-repeat; margin-top: 10px;">
                                            <div style="width: 725px; height: 60px;">
                                                <div class="Naslov_Zelen">
                                                    View Online Users
                                                </div>
                                            </div>
                                            <div class="podnaslovUser" style="padding: 1px; background-color: #f6f5f2;">
                                                Below is a list of all the online users.
                                            </div>
                                        </div>
                                        <table style="margin: 0px 20px 0px 35px" cellspacing="1" cellpadding="0" width="920"
                                            bgcolor="#ffffff" border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="GridView1" runat="server" DataKeyNames="UID" OnSorting="GridView1_Sorting"
                                                            AllowSorting="True" AutoGenerateColumns="false" Width="100%" BorderWidth="2">
                                                            <PagerSettings Visible="False" />
                                                            <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                            <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                            <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                            <Columns>

                                                                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <%--                                                <asp:BoundField DataField="ParentName" HeaderText="ParentName" SortExpression="ParentName">
                                                    <HeaderStyle CssClass="form_title" />
                                                </asp:BoundField>--%>
                                                                <asp:BoundField DataField="LoginDate" HeaderText="LoginDate" DataFormatString="{0:MM.dd.yyyy}"
                                                                    SortExpression="LoginDate" />
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="prevnext" bgcolor="#f8f8f5">
                                                        <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                                            OnPageSizeChanged="WebPager1_PageSizeChanged" PageSize="15" CssClass="prevnext" PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1"></cc1:WebPager>
                                                        <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>

    </table>

</asp:Content>
