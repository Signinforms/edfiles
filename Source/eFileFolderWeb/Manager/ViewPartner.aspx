<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true" CodeFile="ViewPartner.aspx.cs" Inherits="ViewPartner"
 Title="" %>
 
 <%@ Import Namespace="System.Data" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls" TagPrefix="cc1" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
               <contenttemplate>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="725px" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                            style="background-repeat: repeat-x;">
                            <div style="background: url(images/inner_b.gif); background-position: right top;
                                background-repeat: no-repeat; margin-top: 10px;"/>
                                <div style="width: 725px; height: 60px;">
                                    <div class="Naslov_Zelen">
                                        View Partners</div>
                                </div>
                                <div class="podnaslovUser" style="padding: 1px; background-color: #f6f5f2;">Below is a list of all partners associated with this account. Please verify and update
                                    appropriate partner info.
                                </div>
                                <div>
                                       <table style="margin: 0px 10px 0px 10px" cellspacing="1" cellpadding="0" width="920"
                                            bgcolor="#ffffff" border="0"><tr><td>
                                            <asp:GridView ID="GridView1" runat="server" DataKeyNames="PartnerID" OnSorting="GridView1_Sorting"
                                                            AllowSorting="True" AutoGenerateColumns="false" Width="100%" BorderWidth="2">
                                                            <PagerSettings Visible="False" />
                                                            <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                            <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                            <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                            <Columns>
                                                                <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                                   <HeaderTemplate>
                                                                        <asp:Label ID="Label8" runat="server" Text="Action"/>
                                                                   </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandArgument='<%# Eval("PartnerID")%>' OnCommand="LinkButton1_Click"></asp:LinkButton>
                                                                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("PartnerID")%>' OnCommand="LinkButton2_Click"></asp:LinkButton>
                                                                        <ajaxToolkit:ConfirmButtonExtender ID="ButtonDeletePartner" runat="server" ConfirmText="Are you sure to delete this partner ?"
                                                                        TargetControlID="LinkButton2">
                                                                        </ajaxToolkit:ConfirmButtonExtender>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="CompanyName" HeaderText="CompanyName" SortExpression="CompanyName">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                
                                                                <asp:BoundField DataField="ContactName" HeaderText="ContactName" SortExpression="ContactName">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="ContactEmail" HeaderText="ContactEmail" SortExpression="ContactEmail">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="ContactName2" HeaderText="2ndContact" SortExpression="ContactName2">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="ContactEmail2" HeaderText="2ndEmail" SortExpression="ContactEmail2">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Tel" HeaderText="Tel" SortExpression="Tel">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Fax" HeaderText="Fax" SortExpression="Fax">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                 <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                                   <HeaderTemplate>
                                                                        <asp:Label ID="Label81" runat="server" Text="PartnerLink"/>
                                                                   </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton21" runat="server" Text='<%# Eval("PartnerID")%>' CommandArgument='<%# Eval("HashCode")%>' OnCommand="LinkButton3_Click"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                                   <HeaderTemplate>
                                                                        <asp:Label ID="Label82" runat="server" Text="WebSite"/>
                                                                   </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton22" runat="server" Text="..." CommandArgument='<%# Eval("WebSite")%>' OnCommand="LinkButton4_Click"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                
                                                            </Columns>
                                                        </asp:GridView>
                                                        
                                                        <asp:Button Style="display: none" ID="btnShowPopupLink2" runat="server"></asp:Button>
                                                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalBackground"
                                                            CancelControlID="btnClose" PopupControlID="pnlPopupLink2" TargetControlID="btnShowPopupLink2">
                                                        </ajaxToolkit:ModalPopupExtender>
                                                        <!-- partner website Panel-->
                                                        <asp:Panel Style="display: none" ID="pnlPopupLink2" runat="server" Width="500px">
                                                            <asp:Label ID="lblPartnerLinkTitle2" runat="server" Width="95%" Text="Partner Web Site"
                                                                        BackColor="lightblue" Font-Bold="true"></asp:Label>
                                                            <div><asp:Label ID="lblPartnerLink2" runat="server" Width="95%"
                                                                        BackColor="white" Font-Bold="true"></asp:Label></div>
                                                            <div style="width: 95%; background-color: lightblue" align="right">
                                                                <asp:Button ID="btnWebsiteClose" runat="server" Width="50px" Text="Close"></asp:Button>
                                                            </div>
                                                        </asp:Panel>
                                                        
                                                        <asp:Button Style="display: none" ID="btnShowPopupLink" runat="server"></asp:Button>
                                                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
                                                            CancelControlID="btnClose" PopupControlID="pnlPopupLink1" TargetControlID="btnShowPopupLink">
                                                        </ajaxToolkit:ModalPopupExtender>
                                                        <!-- pnlPopupLink1 Panel-->
                                                        <asp:Panel Style="display: none" ID="pnlPopupLink1" runat="server" Width="500px">
                                                            <asp:Label ID="lblPartnerLinkTitle" runat="server" Width="95%" Text="Partner Link"
                                                                        BackColor="lightblue" Font-Bold="true"></asp:Label>
                                                            <div><asp:Label ID="lblPartnerLink1" runat="server" Width="95%"
                                                                        BackColor="white" Font-Bold="true"></asp:Label></div>
                                                            <div style="width: 95%; background-color: lightblue" align="right">
                                                                <asp:Button ID="btnLinkClose" runat="server" Width="50px" Text="Close"></asp:Button>
                                                            </div>
                                                        </asp:Panel>
                                                        <asp:Button Style="display: none" ID="btnShowPopup" runat="server"></asp:Button>
                                                        <ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" BackgroundCssClass="modalBackground"
                                                            CancelControlID="btnClose" PopupControlID="pnlPopup" TargetControlID="btnShowPopup">
                                                        </ajaxToolkit:ModalPopupExtender>
                                                        <!-- ModalPopup Panel-->
                                                        <asp:Panel Style="display: none" ID="pnlPopup" runat="server" Width="500px">
                                                            <asp:UpdatePanel ID="updPnlCustomerDetail" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:Label ID="lblCustomerDetail" runat="server" Width="95%" Text="User Details"
                                                                        BackColor="lightblue" Font-Bold="true"></asp:Label>
                                                                    <asp:DetailsView ID="dvCustomerDetail" runat="server" DataKeyNames="PartnerID" Width="95%"
                                                                        BackColor="white" DefaultMode="Edit" AutoGenerateRows="false" AllowPaging="false"
                                                                        OnDataBound="dvCustomerDetail_DataBound">
                                                                        <Fields>
                                                                            <asp:BoundField DataField="PartnerID" ReadOnly="true" HeaderText="PartnerID" />
                                                                            <asp:TemplateField HeaderText="CompanyName" ShowHeader ="true">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox id="textBoxCompanyName" runat="server" MaxLength="20" Text='<%# DataBinder.Eval(Container.DataItem,"CompanyName")%>' />
                                                                                <asp:RequiredFieldValidator runat="server" ID="firstNameV" ControlToValidate="textBoxCompanyName"
                                                                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The company name is required!" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                                                                    TargetControlID="firstNameV" HighlightCssClass="validatorCalloutHighlight" />
                                                                            </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="ContactName" ShowHeader ="true">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox id="textBoxContactName" runat="server" MaxLength="20" Text='<%# DataBinder.Eval(Container.DataItem,"ContactName")%>' />
                                                                                <asp:RequiredFieldValidator runat="server" ID="contactNameV" ControlToValidate="textBoxContactName"
                                                                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The company name is required!" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
                                                                                    TargetControlID="contactNameV" HighlightCssClass="validatorCalloutHighlight" />
                                                                            </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="ContactEmail" ShowHeader ="true">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox id="textBoxEmail"  MaxLength="50"  runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ContactEmail")%>' />
                                                                                <asp:RequiredFieldValidator runat="server" ID="EmailReq" ControlToValidate="textBoxEmail"
                                                                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />An email is required." />
                                                                                <asp:RegularExpressionValidator ID="rev_Email" runat="server" ErrorMessage="<b>Required Field Format</b><br />It is not an email format."
                                                                                    ControlToValidate="textBoxEmail" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                                    Visible="true" Display="None"></asp:RegularExpressionValidator>
                                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="EmailReqE" TargetControlID="EmailReq"
                                                                                    HighlightCssClass="validatorCalloutHighlight" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
                                                                                    TargetControlID="rev_Email" HighlightCssClass="validatorCalloutHighlight" />
                                                                            </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="ContactName2" ShowHeader ="true">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox id="textBoxContactName2" runat="server" MaxLength="20" Text='<%# DataBinder.Eval(Container.DataItem,"ContactName2")%>' />
                                                                            </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="ContactEmail2" ShowHeader ="true">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox id="textBoxEmail2"  MaxLength="50"  runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ContactEmail2")%>' />
                                                                                <asp:RegularExpressionValidator ID="rev_Email2" runat="server" ErrorMessage="<b>Required Field Format</b><br />It is not an email format."
                                                                                    ControlToValidate="textBoxEmail2" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                                    Visible="true" Display="None"></asp:RegularExpressionValidator>
                                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                                                                    TargetControlID="rev_Email2" HighlightCssClass="validatorCalloutHighlight" />
                                                                            </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Tel" ShowHeader ="true">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox id="textBoxPhone" runat="server" MaxLength="23" Text='<%# DataBinder.Eval(Container.DataItem,"Tel")%>' />
                                                                                <asp:RegularExpressionValidator ID="TelV" ControlToValidate="textBoxPhone" runat="server"
                                                                                    ErrorMessage="<b>Required Field Format</b><br />It is not a telphone format."
                                                                                    ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                                                                    TargetControlID="TelV" HighlightCssClass="validatorCalloutHighlight" />
                                                                            </ItemTemplate></asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Fax" ShowHeader ="true">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox id="textBoxFax" MaxLength="23" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Fax")%>' />
                                                                                <asp:RegularExpressionValidator ID="faxV" ControlToValidate="textBoxFax" runat="server"
                                                                                    ErrorMessage="<b>Required Field Missing</b><br />It is not a FAX format." ValidationExpression="^\d{7,}"
                                                                                    Display="None"></asp:RegularExpressionValidator>
                                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8"
                                                                                    TargetControlID="faxV" HighlightCssClass="validatorCalloutHighlight" />
                                                                            </ItemTemplate></asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Address" ShowHeader ="true">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox id="textBoxAddress" MaxLength="200"  runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Address")%>' />
                                                                            </ItemTemplate></asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="WebSite" ShowHeader ="true">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox id="textBoxWebSite" MaxLength="200"  runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Website")%>' />
                                                                            </ItemTemplate></asp:TemplateField>
                                                                           
                                                                            <asp:BoundField DataField="CreateDate" HeaderText="CreateDate"  ReadOnly="true" SortExpression="CreatedDate" DataFormatString="{0:MM-dd-yyyy}"/>                                                                         
                                                                        </Fields>
                                                                    </asp:DetailsView>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click"></asp:AsyncPostBackTrigger>
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                            <div style="width: 95%; background-color: lightblue" align="right">
                                                                <asp:Button ID="btnSave" OnClick="btnSave_Click" runat="server" Width="50px" Text="Save">
                                                                </asp:Button>
                                                                <asp:Button ID="btnClose" runat="server" Width="50px" Text="Close"></asp:Button>
                                                            </div>
                                                        </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td class="prevnext" bgcolor="#f8f8f5">
                            <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                               OnPageSizeChanged="WebPager1_PageSizeChanged" PageSize="15" CssClass="prevnext" PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1">
                            </cc1:WebPager>
                            <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                        </td>
                    </tr>
                </table>
                </contenttemplate><triggers>
                   <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged"></asp:AsyncPostBackTrigger>
                 </triggers>
                </asp:UpdatePanel></div>
            </td><td width="10" align="center">
                <img src="../images/lin.gif" width="1" height="453" vspace="10" /></td>
            </tr>
    </table>
</asp:Content>

