using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;

public partial class ViewPartner : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();//重新获取操作后的数据源
            BindGrid();//绑定GridView,为删除服务

        }

    }

    #region 分页
    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        if (this.DataSource != null)
        {
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        if (this.DataSource != null)
        {
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
    }

        public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource_Partners"] as DataTable;
        }
        set
        {
            this.Session["DataSource_Partners"] = value;
        }
    }


    #endregion

    #region 数据绑定
    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData()
    {
        if (User.IsInRole("Administrators"))
        {
            this.DataSource = UserManagement.GetParterList();
        }
        else
        {
            throw new ApplicationException("You havn't ther rights to view partners");
        }
    }
    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }
    #endregion


    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();//重新设置数据源，绑定
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }


    protected void LinkButton1_Click(object sender, CommandEventArgs e)
    {
        string pid = e.CommandArgument.ToString();

        DataView view = new DataView(this.DataSource as DataTable);
        view.RowFilter = "PartnerID='" + pid + "'";
        dvCustomerDetail.DataSource = view;
        dvCustomerDetail.DataBind();
        view.RowStateFilter = DataViewRowState.CurrentRows;
        updPnlCustomerDetail.Update();
        this.mdlPopup.Show();
    }

    protected void LinkButton2_Click(Object sender, CommandEventArgs e)
    {
       string pid = e.CommandArgument.ToString();
       Partner partner = new Partner(Convert.ToInt32(pid));

       if (partner.IsExist)
       {
           try
           {
               partner.Delete();

           }
           catch
           {
           }

           GetData();
           BindGrid();
           UpdatePanel1.Update();
       }
    }

    protected void LinkButton3_Click(Object sender, CommandEventArgs e)
    {
        string pid = e.CommandArgument.ToString();
       
        lblPartnerLink1.Text = "&nbsp;&nbsp;&nbsp;&nbsp;http://www.edfiles.com/PartnerHome.aspx?pid=" + pid;
        ModalPopupExtender1.Show();
        
    }

    protected void LinkButton4_Click(Object sender, CommandEventArgs e)
    {
        string website = e.CommandArgument.ToString();
        lblPartnerLink2.Text = "&nbsp;&nbsp;&nbsp;&nbsp;" + website;
        ModalPopupExtender2.Show();
       

    }

    protected void dvCustomerDetail_DataBound(object sender, EventArgs e)
    {
       
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string pid = this.dvCustomerDetail.DataKey.Value.ToString();
        DataView dataTable = new DataView(this.DataSource);
        dataTable.RowFilter = "PartnerID='" + pid + "'";
        dataTable.Sort = "PartnerID";
        DataRowView drView = dataTable.FindRows(pid)[0];

        try
        {
            drView.BeginEdit();

            Shinetech.DAL.Partner partner = new Shinetech.DAL.Partner(Convert.ToInt32(pid));

            if (!partner.IsExist) return;

            foreach (DetailsViewRow item in dvCustomerDetail.Rows)
            {
                #region Edit DataRow
                TableCell cell = item.Controls[1] as TableCell;
                if (cell != null && cell.HasControls())
                {
                    TextBox textBox;
                    switch (item.RowIndex)
                    {
                        case 1: //CompanyName
                            textBox = cell.FindControl("textBoxCompanyName") as TextBox;
                            drView["CompanyName"] = textBox.Text.Trim();
                            partner.CompanyName.Value = textBox.Text.Trim();
                            break;
                        case 2: //ContactName
                            textBox = cell.FindControl("textBoxContactName") as TextBox;
                            drView["ContactName"] = textBox.Text.Trim();
                            partner.ContactName.Value = textBox.Text.Trim();
                            break;
                        case 3: //ContactEmail
                            textBox = cell.FindControl("textBoxEmail") as TextBox;
                            drView["ContactEmail"] = textBox.Text.Trim();
                            partner.ContactEmail.Value = textBox.Text.Trim();
                            break;
                        case 4: //ContactName2
                            textBox = cell.FindControl("textBoxContactName2") as TextBox;
                            drView["ContactName2"] = textBox.Text.Trim();
                            partner.ContactName2.Value = textBox.Text.Trim();
                            break;
                        case 5: //ContactEmail2
                            textBox = cell.FindControl("textBoxEmail2") as TextBox;
                            drView["ContactEmail2"] = textBox.Text.Trim();
                            partner.ContactEmail2.Value = textBox.Text.Trim();
                            break;
                        case 6: //Tel
                            textBox = cell.FindControl("textBoxPhone") as TextBox;
                            drView["Tel"] = textBox.Text.Trim();
                            partner.Tel.Value = textBox.Text.Trim();
                            break;
                        case 7: //Fax
                            textBox = cell.FindControl("textBoxFax") as TextBox;
                            drView["Fax"] = textBox.Text.Trim();
                            partner.Fax.Value = textBox.Text.Trim();
                            break;
                        case 8: //Address 
                            textBox = cell.FindControl("textBoxAddress") as TextBox;
                            drView["Address"] = textBox.Text.Trim();
                            partner.Address.Value = textBox.Text.Trim();
                            break;
                        case 9: //WebSite 
                            textBox = cell.FindControl("textBoxWebSite") as TextBox;
                            drView["WebSite"] = textBox.Text.Trim();
                            partner.Website.Value = textBox.Text.Trim();
                            break;

                        default:
                            break;
                    }

                }
                #endregion
            }

            //submit to database
            UserManagement.UpdatePartner(partner);
            drView.EndEdit();
            dataTable.Table.AcceptChanges();
        }
        catch (Exception ex)
        {
            drView.EndEdit();
            dataTable.Table.RejectChanges();
        }
        finally
        {
            dataTable.Dispose();
        }

        this.GridView1.DataSource = this.DataSource;
        this.GridView1.DataBind();
        UpdatePanel1.Update();
    }

    
}
