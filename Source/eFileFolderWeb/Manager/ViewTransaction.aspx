<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    CodeFile="ViewTransaction.aspx.cs" Inherits="Manager_ViewTransaction" Title="" %>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <%--<asp:ScriptManager ID="scriptManager" runat="server" />--%>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="100%" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                            style="background-repeat: repeat-x;">
                            <div style="background: url(images/inner_b.gif); background-position: right top;
                                background-repeat: no-repeat; margin-top: 10px;">
                                <div style="width: 725px; height: 60px;">
                                    <div class="Naslov_Protokalev">
                                        Transactions</div>
                                </div>
                            </div>
                            <div class="podnaslov" style="padding: 5px; background-color: #f6f5f2;">
                                Below is EFF payment transaction list, by which you can view all EFF payment transactions
                                detail happen. You can input started date and ended date to query the transactions
                                within the date time. For storage fee transactions, you can modify the credit card
                                information and recharge for the failed transactions.
                            </div>
                            <div class="form_title_1">
                                EFF&nbsp;Payment&nbsp;Transactions</div>
                            <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
                                <contenttemplate>
                    <div class="tekstDef" style="margin-right:0px;margin-left:30px;">
                    
                    <table>
                    <tr>
                    <td>
                     <label style="font-size:14px">Started&nbsp;Date&nbsp;</label><asp:TextBox ID="TextBoxStartDate" runat="server" class="form_tekst_field" width="85px"/>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" PopupPosition="Right" runat="server"  TargetControlID="TextBoxStartDate" Format="MM-dd-yyyy"/>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender6" PopupPosition="Right" runat="server"  TargetControlID="TextBoxStartDate" Format="MM-dd-yyyy" EnableViewState="False"/><asp:RegularExpressionValidator ID="startDateC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The Started Date is not a date"
                                            ControlToValidate="TextBoxStartDate" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                            Visible="true" Display="None"></asp:RegularExpressionValidator><ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6"
                                            TargetControlID="startDateC" HighlightCssClass="validatorCalloutHighlight" />
                    </td>
                    <td >
                    &nbsp;&nbsp;<label style="font-size:14px">Ended&nbsp;Date&nbsp;</label><asp:TextBox ID="TextBoxEndDate" runat="server" class="form_tekst_field" width="85px"/>
                     <ajaxToolkit:CalendarExtender ID="CalendarExtender1" PopupPosition="Right" runat="server"  TargetControlID="TextBoxEndDate" Format="MM-dd-yyyy"/>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender7" PopupPosition="Right" runat="server"  TargetControlID="TextBoxEndDate" Format="MM-dd-yyyy" EnableViewState="False"/><asp:RegularExpressionValidator ID="endDateC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The End Date is not a date"
                                            ControlToValidate="TextBoxEndDate" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                            Visible="true" Display="None"></asp:RegularExpressionValidator><ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                            TargetControlID="endDateC" HighlightCssClass="validatorCalloutHighlight" />
                    </td>
                    <td >
                     &nbsp;&nbsp;<label style="font-size:14px">First&nbsp;Name&nbsp;</label><asp:TextBox ID="TextBoxFirstName" runat="server" class="form_tekst_field" width="85px" MaxLength="20"/>
                    </td>
                     <td>
                    &nbsp;&nbsp;<label style="font-size:14px">Last&nbsp;Name&nbsp;</label><asp:TextBox ID="TextBoxLastName" runat="server" class="form_tekst_field" width="85px" MaxLength="20"/>
                    </td>
                    <td >
                    &nbsp;&nbsp;<asp:Button ID="ImageButton1" Text="Search" OnClick="ImageButton1_OnClick" runat="server" />
                    </td>
                    </tr>
                    </table>

                <table width="100%" style="margin-top:10px;">
                <tr>
                    <td>
                        <asp:GridView ID="GridView1" BorderWidth="0" runat="server" Width="100%" AutoGenerateColumns="false"
                            AllowSorting="True" OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound"
                            DataKeyNames="TransactionID">
                            <PagerSettings Visible="False" />
                            <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                            <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                            <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <Columns>
                                <asp:BoundField DataField="TransactionID" HeaderText="ID" SortExpression="TransactionID">
                                    <HeaderStyle CssClass="form_title" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FirstName" HeaderText="Firstname" SortExpression="Firstname">
                                    <HeaderStyle CssClass="form_title" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname">
                                    <HeaderStyle CssClass="form_title" />
                                </asp:BoundField>
                                <asp:BoundField DataField="MaximalUserCount" HeaderText="MaxUserCount" SortExpression="MaximalUserCount" />
                                <asp:BoundField DataField="MaxStorageVolume" HeaderText="MaxStorage" SortExpression="MaxStorageVolume" DataFormatString="${0:F2}"/>
                                <asp:BoundField DataField="MonthPricePerUser" HeaderText="MonthPrice" SortExpression="MonthPricePerUser" DataFormatString="${0:F2}" />
                                <asp:BoundField DataField="AdditionalStoragePrice" HeaderText="AddStoragePrice" SortExpression="AdditionalStoragePrice" DataFormatString="${0:F2}" />
                                <asp:BoundField DataField="AdditionalUserPrice" HeaderText="AddUserPrice" SortExpression="AdditionalUserPrice"  DataFormatString="${0:F2}"/>
                                <asp:BoundField DataField="CardNumber" HeaderText="CardNumber" SortExpression="CardNumber" />
                                <asp:BoundField DataField="CreatedDate" HeaderText="PaymentDate" SortExpression="CreatedDate"
                                    DataFormatString="{0:MM-dd-yyyy hh:mm}" />
                                <asp:BoundField DataField="ResponseCode" HeaderText="Status" />
                            </Columns>
                            <EmptyDataTemplate>
                                <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                        <tr>
                                            <td align="center" class="podnaslov">
                                                <asp:Label ID="Label411" runat="server" />There is no transaction.
                                            </td>
                                        </tr>
                                    </table>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#F8F8F5" class="prevnext">
                        <cc1:WebPager ID="WebPager1" ControlToPaginate="GridView1" PagerStyle="OnlyNextPrev"
                           OnPageSizeChanged="WebPager1_PageSizeChanged" CssClass="prevnext" PageSize="15" 
                           OnPageIndexChanged="WebPager1_PageIndexChanged" runat="server" />
                        <asp:HiddenField ID="SortDirection1" runat="server" Value="" />
                    </td>
                </tr>
    </table>
    </div> </contenttemplate>
                                <triggers><asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" /></triggers>
                            </asp:UpdatePanel>
                            <div class="form_title_3">
                                Storage&nbsp;Fee&nbsp;Transactions</div>
                            <asp:UpdatePanel ID="UpdatePanel3" UpdateMode="Conditional" runat="server">
                                <contenttemplate>
                    <div class="tekstDef" style="margin-right:0px;margin-left:30px;">
                    <table>
                    <tr>
                    <td>
                    <label style="font-size:14px">Started&nbsp;Date&nbsp;</label><asp:TextBox ID="TextBoxStartDate1" runat="server" class="form_tekst_field" width="85px"/>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" PopupPosition="Right" runat="server"  TargetControlID="TextBoxStartDate1" Format="MM-dd-yyyy"/>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender8" PopupPosition="Right" runat="server"  TargetControlID="TextBoxStartDate1" Format="MM-dd-yyyy" EnableViewState="False"/><asp:RegularExpressionValidator ID="startDateC1" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The Started Date is not a date"
                                            ControlToValidate="TextBoxStartDate1" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                            Visible="true" Display="None"></asp:RegularExpressionValidator><ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                            TargetControlID="startDateC1" HighlightCssClass="validatorCalloutHighlight" />
                    </td>
                    <td>
                    &nbsp;&nbsp;<label style="font-size:14px">Ended&nbsp;Date&nbsp;</label><asp:TextBox ID="TextBoxEndDate1" runat="server" class="form_tekst_field" width="85px"/>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender4" PopupPosition="Right" runat="server"  TargetControlID="TextBoxEndDate1" Format="MM-dd-yyyy"/>
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender9" PopupPosition="Right" runat="server"  TargetControlID="TextBoxEndDate1" Format="MM-dd-yyyy" EnableViewState="False"/><asp:RegularExpressionValidator ID="endDateC1" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The End Date is not a date"
                                            ControlToValidate="TextBoxEndDate1" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                            Visible="true" Display="None"></asp:RegularExpressionValidator><ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
                                            TargetControlID="endDateC1" HighlightCssClass="validatorCalloutHighlight" />
                    </td>
                    <td>
                    &nbsp;&nbsp;<label style="font-size:14px">First&nbsp;Name&nbsp;</label><asp:TextBox ID="TextBoxFirstName1" runat="server" class="form_tekst_field" width="85px" MaxLength="20"/>
                    </td>
                    <td>
                    &nbsp;&nbsp;<label style="font-size:14px">Last&nbsp;Name&nbsp;</label><asp:TextBox ID="TextBoxLastName1" runat="server" class="form_tekst_field" width="85px" MaxLength="20"/>
                    </td>
                    <td>
                      &nbsp;&nbsp;<label style="font-size:14px">Status&nbsp;</label><asp:DropDownList id="DropDownList2" runat="server" class="form_tekst_field" width="85px"></asp:DropDownList>
                    </td>
                    <td>
                    &nbsp;&nbsp;<asp:Button ID="ImageButton2" Text="Search" OnClick="ImageButton2_OnClick" runat="server" />
                    </td>
                    </tr>
                    </table>
                    <div>
                    <table width="100%" style="margin-top:10px;">
                <tr>
              <td><asp:GridView ID="GridView2" BorderWidth="0" runat="server" Width="100%" AutoGenerateColumns="false"
                    AllowSorting="True" OnSorting="GridView2_Sorting" ShowHeader="true" OnRowDataBound="GridView2_RowDataBound" DataKeyNames="StorageTransID" >
                  <PagerSettings Visible="False" />
                  <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                  <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                  <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                  <HeaderStyle  CssClass="form_title" BackColor="#DBD9CF" />
                  <EmptyDataRowStyle  HorizontalAlign="Center" />
                  <Columns>
                  <asp:BoundField DataField="StorageTransID" HeaderText="ID" SortExpression="StorageTransID" >
                    <HeaderStyle CssClass="form_title" />
                  </asp:BoundField>
                  <asp:BoundField DataField="FirstName" HeaderText="Firstname" SortExpression="Firstname" >
                    <HeaderStyle CssClass="form_title" />
                  </asp:BoundField>
                  <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname" >
                    <HeaderStyle CssClass="form_title" />
                  </asp:BoundField>
                  <asp:BoundField DataField="Amount" HeaderText="Amount" SortExpression="Amount" DataFormatString="${0:F2}">
                    <HeaderStyle CssClass="form_title" />
                  </asp:BoundField>
                  <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" DataFormatString="${0:F2}">
                    <HeaderStyle CssClass="form_title" />
                  </asp:BoundField>
                  <asp:BoundField DataField="Size" HeaderText="Size(Mb)" SortExpression="Size" DataFormatString="{0:F2}"/>
                 <asp:BoundField DataField="CardType" HeaderText="CardType" SortExpression="CardType"/>
                 <asp:BoundField DataField="CardNumber" HeaderText="CardNumber" SortExpression="CardNumber"/>
                 <asp:BoundField DataField="ExpiredDate" HeaderText="Exp.Date" SortExpression="ExpiredDate" DataFormatString="{0:MM-yyyy}"/>
                 <asp:BoundField DataField="ExchangeDate" HeaderText="ChargedDate" SortExpression="ExchangeDate" DataFormatString="{0:MM-dd-yyyy}"/>
                 <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true"  SortExpression="ResponseCode" ItemStyle-Width="30px" ItemStyle-CssClass="table_tekst_edit">
                    <HeaderTemplate>
                      <asp:Label ID="Label34" Text="Status" runat="server"/>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <asp:LinkButton ID="LinkButton21" runat="server" Text='<%# Eval("ResponseCode")%>' OnCommand="LinkButton21_Click" 
                      CommandArgument='<%# Eval("StorageTransID")%>'/>
                    </ItemTemplate>
                  </asp:TemplateField>  
                  <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true"  ItemStyle-Width="30px" ItemStyle-CssClass="table_tekst_edit">
                    <HeaderTemplate>
                      <asp:Label ID="Label4" Text="Action" runat="server"/>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <asp:LinkButton ID="LinkButton22" runat="server" Text='<%# Eval("ResponseCode")%>' OnCommand="LinkButton22_Click" 
                      CommandArgument='<%# Eval("StorageTransID")%>'/>
                    </ItemTemplate>
                  </asp:TemplateField>
                  </Columns>
                 <EmptyDataTemplate><table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                        <tr>
                                            <td align="center" class="podnaslov">
                                                <asp:Label ID="Label1" runat="server" />There is no transaction.
                                            </td>
                                        </tr>
                                    </table></EmptyDataTemplate>

                </asp:GridView>
                <!-- Transaction Status Panel-->
                <asp:Button id="btnShowPopup" runat="server" style="display:none" />
                <ajaxToolKit:ModalPopupExtender ID="mdlPopup" runat="server" TargetControlID="btnShowPopup" PopupControlID="pnlPopup" 
			         CancelControlID="btnClose" BackgroundCssClass="modalBackground" />
                <!-- ModalPopup Panel-->
                <asp:Panel ID="pnlPopup" runat="server" Width="450px" style="display:none">
                  <asp:UpdatePanel ID="updPnlStorageDetail" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                      <asp:Label ID="lblStorageDetail" runat="server" Font-Size="15px" Font-Bold="true" Text="Storage Transaction Details" BackColor="lightblue" Width="95%" />
                      <asp:DetailsView ID="dvStorageDetail" Font-Size="14px" DataKeyNames="StorageTransID" DefaultMode="Edit" AllowPaging="false" AutoGenerateRows="false" runat="server" Width="95%" BackColor="white" >
                        <Fields>
                          <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                            <HeaderTemplate>
                              <asp:Label ID="Label5" runat="server"  Text="CardType"/>
                            </HeaderTemplate>
                            <EditItemTemplate>
                              <asp:DropDownList ID="DropDownList1" runat="server" OnDataBinding="DropDownList1_DataBinding" DataValueField="CardType">
                                <asp:ListItem Text="Visa" Value="Visa"/>
                                <asp:ListItem Text="MasterCard" Value="MasterCard"/>
                                <asp:ListItem Text="American Express" Value="Amex"/>
                                <asp:ListItem Text="Discover" Value="Discover"/>
                              </asp:DropDownList>
                            </EditItemTemplate>
                          </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                            <HeaderTemplate>
                              <asp:Label ID="Label14" runat="server"  Text="CardNumber"/>
                            </HeaderTemplate>
                            <EditItemTemplate>
                              <asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CardNumber")%>' id="textfield6" maxlength = "20"/>
                                <asp:RequiredFieldValidator runat="server" ID="TF6" ControlToValidate="textfield6"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The CardNumber is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                    TargetControlID="TF6" HighlightCssClass="validatorCalloutHighlight" />
                            </EditItemTemplate>
                          </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                            <HeaderTemplate>
                              <asp:Label ID="Label2" runat="server"  Text="Exp.Date"/>
                            </HeaderTemplate>
                            <ItemTemplate>
                              <asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ExpiredDate")%>' id="textfield5"/>
                              <ajaxToolkit:CalendarExtender ID="CalendarExtender5" PopupPosition="Right" runat="server"  
                              SelectedDate='<%# DataBinder.Eval(Container.DataItem,"ExpiredDate")%>' TargetControlID="textfield5" 
                              Format="MM-dd-yyyy"> </ajaxToolkit:CalendarExtender>
                              <asp:RegularExpressionValidator ID="TF5" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The Expired Date is not a date"
                                    ControlToValidate="textfield5" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                    Visible="true" Display="None"></asp:RegularExpressionValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
                                    TargetControlID="TF5" HighlightCssClass="validatorCalloutHighlight" />
                                     <asp:RequiredFieldValidator runat="server" ID="TF5V" ControlToValidate="textfield5"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The Expired Date is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5"
                                    TargetControlID="TF5V" HighlightCssClass="validatorCalloutHighlight" />
                            </ItemTemplate>
                          </asp:TemplateField>
                            <asp:BoundField DataField="ExchangeDate" ReadOnly="true" HeaderText="ChargedDate" DataFormatString="{0:MM-dd-yyyy}"/>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left" ShowHeader="false">
                            <ItemTemplate>
                              <hr width="90%"/>
                            </ItemTemplate>
                          </asp:TemplateField>
                            <asp:BoundField DataField="ResponseCode" ReadOnly="true" HeaderText="ResponseCode" />
                            <asp:BoundField DataField="ResponseAuthCode" ReadOnly="true" HeaderText="ResponseAuthCode" />
                            <asp:BoundField DataField="ResponseAVSCode" ReadOnly="true" HeaderText="ResponseAVSCode" />
                            <asp:BoundField DataField="ResponseReferenceCode" ReadOnly="true" HeaderText="ResponseReferenceCode" />
                            <asp:BoundField DataField="ResponseDescription" ReadOnly="true" HeaderText="ResponseDescription" />
                        </Fields>
                      </asp:DetailsView>
                    </ContentTemplate><Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" /></Triggers>
                  </asp:UpdatePanel>
                  <div align="right" style="width:95%; background-color:lightblue">
                  <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" Width="50px" />
                    <asp:Button ID="btnClose" runat="server" Text="Close" Width="50px" />
                  </div>
                  </asp:Panel>
                  <!-- Recharge ModalPopup Panel-->
                  <asp:Button id="Button1ShowPopup" runat="server" style="display:none" />
                        <ajaxToolKit:ModalPopupExtender ID="mdlPopup1" runat="server" PopupControlID="pnlPopup1" 
			                 DropShadow="true" BackgroundCssClass="modalBackground" TargetControlID="Button1ShowPopup"/>
                        <!-- ModalPopup Panel-->
                        <asp:Panel ID="pnlPopup1" runat="server" Width="350px" style="display:none;background-color:white">
                        <div style="width:100%; background-color:lightblue; font-size:15px;"><strong>Recharge Details</strong></div>
                        <div class="tekstDef" style="margin-right:0px;margin-left:30px;">
                            <table width="100%" border="0" cellpadding="2" style="margin-top:10px" cellspacing="0">
                                <tr>
                                    <td width="100%" align="left" class="podnaslov_recharge">This recharge transaction is <strong><asp:Label ID="LabelResult" Text="" runat="server" /></strong>
                                    :&nbsp;&nbsp;<asp:Label ID="LabelTransInfo" Text="" runat="server" />
                                    </td>
                                </tr>
                                
                            </table>
                        </div>
                        <hr width="95%" align="center">
                        <div align="center" style="width:100%; background-color:white">
                        <asp:Button ID="btnClose1" runat="server" Text="Close" Width="50px" />
                        </div>
                        </asp:Panel>
              </td>
            </tr>
            <tr>
                <td bgcolor="#F8F8F5" class="prevnext"><cc1:WebPager ID="WebPager2" ControlToPaginate="GridView2" PagerStyle="OnlyNextPrev" CssClass="prevnext"  
                        PageSize="15" OnPageIndexChanged="WebPager2_PageIndexChanged" OnPageSizeChanged="WebPager2_PageSizeChanged" runat="server" />
                  <asp:HiddenField ID="SortDirection2" runat="server" Value=""/></td>
              </tr>
      </table>
                    </div>
                    </contenttemplate>
                                <triggers><asp:AsyncPostBackTrigger ControlID="ImageButton2" EventName="Click" /></triggers>
                            </asp:UpdatePanel>
                            <div class="form_title_3">
                                Gift&nbsp;Number&nbsp;Transactions</div>
                                <asp:UpdatePanel ID="UpdatePanel5" UpdateMode="Conditional" runat="server">
                                <contenttemplate><div class="tekstDef" style="margin-right:0px;margin-left:30px;"><table width="100%" style="margin-top:10px;">
                <tr>
                    <td>
                        <asp:GridView ID="GridView3" BorderWidth="0" runat="server" Width="100%" AutoGenerateColumns="false"
                            AllowSorting="True" OnSorting="GridView3_Sorting" DataKeyNames="GiftNumber">
                            <PagerSettings Visible="False" />
                            <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                            <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                            <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                            <EmptyDataRowStyle HorizontalAlign="Center" />
                            <Columns>
                                <asp:BoundField DataField="GiftNumber" HeaderText="Gift Number" SortExpression="GiftNumber">
                                    <HeaderStyle CssClass="form_title" />
                                </asp:BoundField>
                                <asp:BoundField DataField="UserName" HeaderText="User Name" SortExpression="UserName">
                                    <HeaderStyle CssClass="form_title" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FirstName" HeaderText="Firstname" SortExpression="Firstname">
                                    <HeaderStyle CssClass="form_title" />
                                </asp:BoundField>
                                <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname">
                                    <HeaderStyle CssClass="form_title" />
                                </asp:BoundField>
                                <asp:BoundField DataField="InputDate" HeaderText="Input Date" SortExpression="InputDate"
                                    DataFormatString="{0:MM-dd-yyyy}" />
                                
                            </Columns>
                            <EmptyDataTemplate>
                                <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                        <tr>
                                            <td align="center" class="podnaslov">
                                                <asp:Label ID="Label3" runat="server" />There is no transaction.
                                            </td>
                                        </tr>
                                    </table>
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#F8F8F5" class="prevnext">
                        <cc1:WebPager ID="WebPager3" ControlToPaginate="GridView3" PagerStyle="OnlyNextPrev"
                            CssClass="prevnext" PageSize="15" OnPageIndexChanged="WebPager3_PageIndexChanged"
                            OnPageSizeChanged="WebPager3_PageSizeChanged"
                            runat="server" />
                        <asp:HiddenField ID="SortDirection3" runat="server" Value="" />
                    </td>
                </tr>
    </table>
    </div> </contenttemplate>
                                
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
