using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AjaxControlToolkit;
using DataQuicker2.Framework;
using Shinetech.DAL;
using Shinetech.Framework.Controls;


public partial class Manager_ViewTransaction : BasePage
{ 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CalendarExtender2.SelectedDate = DateTime.Now.AddMonths(-1);
            TextBoxStartDate.Text = CalendarExtender2.SelectedDate.Value.ToString("MM-dd-yyy");
            CalendarExtender1.SelectedDate = DateTime.Now.AddDays(1);
            TextBoxEndDate.Text = CalendarExtender1.SelectedDate.Value.ToString("MM-dd-yyy");

            CalendarExtender3.SelectedDate = DateTime.Now.AddMonths(-1);
            TextBoxStartDate1.Text = CalendarExtender3.SelectedDate.Value.ToString("MM-dd-yyy");
            CalendarExtender4.SelectedDate = DateTime.Now.AddDays(1);
            TextBoxEndDate1.Text = CalendarExtender4.SelectedDate.Value.ToString("MM-dd-yyy");

            GetTransData1();
            GetTransData2();
            GetTransData3();
            BindGridView1();
            BindGridView2();
            BindGridView3();

            DropDownList2.Items.Add(new ListItem("All", "0"));
            DropDownList2.Items.Add(new ListItem("Success","1"));
            DropDownList2.Items.Add(new ListItem("Failed", "2"));
            DropDownList2.SelectedIndex = 0;
        }
    }

    #region 数据绑定
    /// <summary> 
    /// 获取数据源
    /// </summary>
    private void GetTransData1()
    {
        string uid = Membership.GetUser().ProviderUserKey.ToString();

        DateTime startDate = DateTime.ParseExact(this.TextBoxStartDate.Text, "mm-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
        DateTime endDate = DateTime.ParseExact(this.TextBoxEndDate.Text, "mm-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);

        //DateTime startDate = DateTime.Parse(this.TextBoxStartDate.Text);
        //DateTime endDate = DateTime.Parse(this.TextBoxEndDate.Text);
        CalendarExtender2.SelectedDate = startDate;
        CalendarExtender1.SelectedDate = endDate;
        if (endDate.AddMonths(-12) > startDate)
        {
            startDate = endDate.AddMonths(-12);
            CalendarExtender2.SelectedDate = startDate;
        }
        this.EFFDataSource = LicenseManagement.GetEFFTransactions(false, uid, startDate, endDate,
            this.TextBoxFirstName.Text.Trim(),this.TextBoxLastName.Text.Trim());
        
    }

    private void GetTransData2()
    {
        string uid = Membership.GetUser().ProviderUserKey.ToString();

        DateTime startDate = DateTime.ParseExact(this.TextBoxStartDate1.Text, "mm-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
        DateTime endDate = DateTime.ParseExact(this.TextBoxEndDate1.Text, "mm-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
        
        //DateTime startDate = DateTime.Parse(this.TextBoxStartDate1.Text);
        //DateTime endDate = DateTime.Parse(this.TextBoxEndDate1.Text);
        int status = DropDownList2.SelectedIndex;
        CalendarExtender3.SelectedDate = startDate;
        CalendarExtender4.SelectedDate = endDate;
        if (endDate.AddMonths(-12) > startDate)
        {
            startDate = endDate.AddMonths(-12);
            CalendarExtender3.SelectedDate = startDate;
        }
        this.StorageDataSource = LicenseManagement.GetStorageTransactions(false, uid, startDate,endDate,
            this.TextBoxFirstName1.Text.Trim(), this.TextBoxLastName1.Text.Trim(), status);
    }

    private void GetTransData3()
    {

        this.GiftNumberSource = LicenseManagement.GetGiftNumberTransactions();
    }


    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGridView1()
    {
        WebPager1.DataSource = this.EFFDataSource;
        if (this.EFFDataSource != null)
        {
            WebPager1.DataBind();
        }
    }

    private void BindGridView2()
    {
        WebPager2.DataSource = this.StorageDataSource;

        if (this.StorageDataSource != null)
        {
            WebPager2.DataBind();
        }
    }

    private void BindGridView3()
    {
        WebPager3.DataSource = this.GiftNumberSource;
        if (this.GiftNumberSource != null)
        {
            WebPager3.DataBind();
        }
    }

    #endregion

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.EFFDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.EFFDataSource = Source.ToTable();
        BindGridView1();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dataRowView = (DataRowView)e.Row.DataItem;
            char[] xxCardNo = e.Row.Cells[8].Text.ToCharArray();
            int cnt = xxCardNo.Length;
            if (cnt > 4)
                for (int i = 0; i < cnt - 4; i++)
                {
                    xxCardNo[i] = 'x';
                }

            e.Row.Cells[8].Text = new string(xxCardNo);

            string code = (string)dataRowView.Row["ResponseCode"];
            if(code.Trim()=="1")
            {
                e.Row.Cells[10].Text="Success";
            }
            else
            {
                e.Row.Cells[10].Text = "Failed";
            }
        }
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    private SortDirection Sort_Direction2
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection2.Value))
            {
                SortDirection2.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection2.Value);

        }
        set
        {
            this.SortDirection2.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    private SortDirection Sort_Direction3
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection3.Value))
            {
                SortDirection3.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection3.Value);

        }
        set
        {
            this.SortDirection3.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.EFFDataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.EFFDataSource;
        WebPager1.DataBind();
    }

    public DataTable EFFDataSource
    {
        get
        {
            return this.Session["EFFTransactions"] as DataTable;
        }
        set
        {
            this.Session["EFFTransactions"] = value;
        }
    }

    public DataTable StorageDataSource
    {
        get
        {
            return this.Session["StorageDataSource"] as DataTable;
        }
        set
        {
            this.Session["StorageDataSource"] = value;
        }
    }

    public DataTable GiftNumberSource
    {
        get
        {
            return this.Session["GiftNumberSource"] as DataTable;
        }
        set
        {
            this.Session["GiftNumberSource"] = value;
        }
    }

    protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction2 == SortDirection.Ascending)
        {
            this.Sort_Direction2 = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction2 = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.StorageDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.StorageDataSource = Source.ToTable();
        BindGridView2();
    }

    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dataRowView = (DataRowView)e.Row.DataItem;
            if(dataRowView.Row["CardNumber"]!=System.DBNull.Value)
            {
                string var = (string)dataRowView.Row["CardNumber"];
                char[] xxCardNo = var.ToCharArray();
                int cnt = xxCardNo.Length;
                if (cnt > 4)
                    for (int i = 0; i < cnt - 4; i++)
                    {
                        xxCardNo[i] = 'x';
                    }

                e.Row.Cells[7].Text = new string(xxCardNo);
            }
            else
            {
                e.Row.Cells[7].Text = "";
            }

            string code = (string)dataRowView.Row["ResponseCode"];
            LinkButton control = e.Row.Cells[10].FindControl("LinkButton21") as LinkButton;
            LinkButton control1 = e.Row.Cells[11].FindControl("LinkButton22") as LinkButton;
            if (code.Trim() == "1")
            {
                control.Text = "Success";
                e.Row.Cells[11].Text = "";
            }
            else
            {
                control.Text = "Failed";
                control1.Text = "Recharge";
            }
            
        }
    }

    protected void WebPager2_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager2.CurrentPageIndex = e.NewPageIndex;
        WebPager2.DataSource = this.StorageDataSource;
        WebPager2.DataBind();
    }

    protected void WebPager2_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager2.CurrentPageIndex = 0;
        WebPager2.DataSource = this.StorageDataSource;
        WebPager2.DataBind();
    }

    //search button
    protected void ImageButton1_OnClick(object sender, EventArgs e)
    {
        GetTransData1();
        BindGridView1();
    }

    protected void ImageButton2_OnClick(object sender, EventArgs e)
    {
        GetTransData2();
        BindGridView2();
    }


    protected void LinkButton21_Click(Object sender, CommandEventArgs e)
    {
        string transId = (string)e.CommandArgument;
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;

        DataView view = new DataView(this.StorageDataSource as DataTable);
        view.RowFilter = "StorageTransID=" + transId;
        dvStorageDetail.DataSource = view;
        dvStorageDetail.DataBind();
        updPnlStorageDetail.Update();
        mdlPopup.Show();
    }

    //Recharge
    protected void LinkButton22_Click(Object sender, CommandEventArgs e)
    {
        string transId = (string)e.CommandArgument;
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        StorageTransaction trans = new StorageTransaction(Convert.ToInt32(transId));
        if(trans.IsExist)
        {
            Account account = new Account(trans.OfficeID.Value);
            Hashtable vars = StorageTransManage.ReadHtmlPage(account, trans.Amount.Value);
            if (vars == null)
            {
                trans.ResponseCode.Value = "-1";
                trans.ResponseAuthCode.Value = "";
                trans.ResponseAVSCode.Value = "";
                trans.ResponseDescription.Value = "Error:Transaction Failed";
                trans.ResponseReferenceCode.Value = "";
            }
            else
            {
                trans.ResponseAuthCode.Value = (string)vars[4];
                trans.ResponseAVSCode.Value = (string)vars[5] + ":" + ParseAvsCode((string)vars[5]);
                trans.ResponseCode.Value = (string)vars[0];
                trans.ResponseDescription.Value = (string)vars[3];
                trans.ResponseReferenceCode.Value = (string)vars[6];
            }

            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();

            try
            {
                trans.Update(connection);
            }
            catch(Exception exp){ throw new ApplicationException(exp.Message);}

            GetTransData2();
            BindGridView2();
            LabelResult.Text = trans.ResponseCode.Value == "1" ? "successful" : "failed";
            LabelTransInfo.Text = trans.ResponseDescription.Value;
            UpdatePanel3.Update();
            mdlPopup1.Show();

            //runat another thread
            Thread emailThread = new Thread(new ParameterizedThreadStart(SendEmail));

            Hashtable paramVars = new Hashtable();
            paramVars.Add("trans", trans);
            paramVars.Add("account", account);

            emailThread.Start(paramVars);
        }

    }

    private void SendEmail(object paramVars)
    {
        Hashtable vars = paramVars as Hashtable;

        string path = Server.MapPath("./");
        StorageTransManage.SendEmail(vars["trans"] as StorageTransaction,
            vars["account"] as Account,path);
    }

    private static string ParseAvsCode(string code)
    {
        string result = "Uknown";

        switch (code.ToUpper())
        {
            case "A":
                result = " [AVS - Address (Street) matches, ZIP does not]";
                break;
            case "B":
                result = " [AVS - Address information not provided for AVS check]";
                break;
            case "E":
                result = " [AVS - Error]";
                break;
            case "G":
                result = " [AVS - Non-U.S. Card Issuing Bank]";
                break;
            case "N":
                result = " [AVS - No Match on Address (Street) or ZIP]";
                break;
            case "P":
                result = " [AVS - AVS not applicable for this transaction]";
                break;
            case "R":
                result = " [AVS - Retry – System unavailable or timed out]";
                break;
            case "S":
                result = " [AVS - Service not supported by issuer]";
                break;
            case "U":
                result = " [AVS - Address information is unavailable]";
                break;
            case "W":
                result = " [AVS - 9 digit ZIP matches, Address (Street) does not]";
                break;
            case "X":
                result = " [AVS - Address (Street) and 9 digit ZIP match]";
                break;
            case "Y":
                result = " [AVS - Address (Street) and 5 digit ZIP match]";
                break;
            case "Z":
                result = " [AVS - 5 digit ZIP matches, Address (Street) does not]";
                break;
        }

        return result;
    }

    

    protected void DropDownList1_DataBinding(object sender, EventArgs e)
    {
        DropDownList list = (DropDownList)sender;
        DetailsView dv = (DetailsView)list.NamingContainer;
        DataRowView drv = (DataRowView)dv.DataItem;
        if (drv.Row["CardType"] != System.DBNull.Value && drv.Row["CardType"] != null)
            list.SelectedValue = (string)drv.Row["CardType"];
        
    }

    //sava into account and transaction
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string transid = this.dvStorageDetail.DataKey.Value.ToString();
        DataView dataTable = new DataView(this.StorageDataSource);
        dataTable.RowFilter = "StorageTransID=" + transid;
        dataTable.Sort = "StorageTransID";
        DataRowView drView = dataTable.FindRows(transid)[0];

        
        IDbTransaction transaction=null;
        try
        {
            drView.BeginEdit();

            Shinetech.DAL.StorageTransaction trans = new Shinetech.DAL.StorageTransaction(Convert.ToInt32(transid));
            if (!trans.IsExist) return;

            foreach (DetailsViewRow item in dvStorageDetail.Rows)
            {
                if (item.RowIndex > 2) break;
                #region Edit DataRow
                if(item.RowIndex==0)
                {
                    DropDownList droplist = item.FindControl("DropDownList1") as DropDownList;
                    drView["CardType"] = droplist.SelectedValue;
                    trans.CardType.Value = droplist.SelectedValue;
                }
                else
                {
                    TextBox textBox ;
                    switch (item.RowIndex)
                    {
                        case 1: //CardNumber
                            textBox = item.FindControl("textfield6") as TextBox;
                            drView["CardNumber"] = textBox.Text.Trim();
                            trans.CardNumber.Value = textBox.Text.Trim();
                            break;
                        case 2: //DOB
                            textBox = item.FindControl("textfield5") as TextBox;
                            drView["ExpiredDate"] = textBox.Text.Trim();
                            trans.ExpiredDate.Value = DateTime.Parse(textBox.Text.Trim());
                            break;
                        default:
                            break;
                    }
                }
                #endregion
            }

            //submit to database
            Account account = new Account(trans.OfficeID.Value);
            account.CardType.Value = trans.CardType.Value;
            account.CreditCard.Value = trans.CardNumber.Value;
            account.ExpirationDate.Value = trans.ExpiredDate.Value;
            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            transaction = connection.BeginTransaction();

            trans.Update(transaction);
            account.Update(transaction);
            transaction.Commit();
            drView.EndEdit();
            dataTable.Table.AcceptChanges();
        }
        catch (Exception ex)
        {
            if (transaction!=null) transaction.Rollback();
            drView.EndEdit();
            dataTable.Table.RejectChanges();
        }
        finally
        {
            dataTable.Dispose();
        }

        WebPager2.DataSource = StorageDataSource;
        WebPager2.DataBind();
        UpdatePanel3.Update();
    }

    protected void WebPager3_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager3.CurrentPageIndex = e.NewPageIndex;
        WebPager3.DataSource = this.GiftNumberSource;
        WebPager3.DataBind();
    }

    protected void WebPager3_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager3.CurrentPageIndex = 0;
        WebPager3.DataSource = this.GiftNumberSource;
        WebPager3.DataBind();
    }

    protected void GridView3_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction3 == SortDirection.Ascending)
        {
            this.Sort_Direction3 = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction3 = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.GiftNumberSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.GiftNumberSource = Source.ToTable();
        BindGridView3();
    }
}
