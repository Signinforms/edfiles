<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    CodeFile="ViewUploadedFiles.aspx.cs" Inherits="Manage_UploadFiles" Title="" %>

<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.ComponentModel" %>
<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td valign="top" style="background-repeat: no-repeat;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="925" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                            style="background-repeat: repeat-x;">
                            <div style="background: url(images/inner_b.gif); background-position: right top;
                                background-repeat: no-repeat; margin-top: 10px;">
                                <div style="width: 825px; height: 75px;">
                                    <div class="Naslov_Protokalev">
                                        View Uploaded Files</div>
                                </div>
                                <div class="Naslov_Plav" style="width:750px;">
                                  <table width="100%" border="0" cellpadding="0" cellspacing="0" style="margin:0px 0 20px 25px;">
                                    <tr>
                                      <td height="20" colspan="3" align="left" valign="top" class="tekstDef" style="padding-top:0px;padding-bottom:0px;">Enter search term: (OfficeName, FolerName,Lastname, Firstname..)</td>
                                      <td colspan="2" align="left" class="podnaslov"><input name="textfield3" type="text" runat="server" class="form_tekst_field" id="textfield3" size="20" maxlength="50"/></td>
                                      <td width="128"><asp:ImageButton id="searchButton1" OnClick="OnSearchButton_Click" runat="server" ImageUrl="~/images/search_btn.gif" width="60" height="21" /></td>
                                      </tr>
                                  </table>
                                 </div>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <contenttemplate>
                <TABLE style="MARGIN: 0px 20px 0px 30px;width:925px"><TBODY><TR><TD>
                <asp:GridView id="GridView1" runat="server" DataKeyNames="FileID,UserID,FormFileID" OnSorting="GridView1_Sorting" 
                    AllowSorting="True" OnRowDataBound="GridView1_OnRowDataBound" AutoGenerateColumns="false" Width="100%" BorderWidth="0">
                  <PagerSettings Visible="False" />
                  <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                  <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                  <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                  <HeaderStyle  CssClass="form_title" BackColor="#DBD9CF" />
                  <Columns>
                  
                   <asp:BoundField DataField="FileID" HeaderText="File ID" SortExpression="FileID" >
                    <HeaderStyle CssClass="form_title" />
                  </asp:BoundField>
                  
                  <asp:BoundField DataField="Name" HeaderText="OfficeName" SortExpression="Name" >
                    <HeaderStyle CssClass="form_title" />
                  </asp:BoundField>
                  
                  <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname" >
                    <HeaderStyle CssClass="form_title" />
                  </asp:BoundField>
                  
                  <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname" >
                    <HeaderStyle CssClass="form_title" />
                  </asp:BoundField>
                  
                  <asp:BoundField DataField="FolderName" HeaderText="FolderName" SortExpression="FolderName" >
                    <HeaderStyle CssClass="form_title" />
                  </asp:BoundField>

                  <asp:BoundField DataField="UploadDate" HeaderText="UploadDate" dataformatstring="{0:MM.dd.yyyy}" SortExpression="UploadDate"/>
                  
                  <asp:BoundField DataField="ConvertedDate" HeaderText="ConvertedDate" SortExpression="ConvertedDate" DataFormatString="{0:MM.dd.yyyy}" NullDisplayText="" >
                        <HeaderStyle CssClass="form_title" />
                  </asp:BoundField>
                  
                  <asp:TemplateField ShowHeader="false"  ItemStyle-CssClass="table_tekst_edit">
                    <HeaderTemplate>
                      <asp:Label ID="Label10"   Text="FileForm" runat="server"/>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="false" Text='<%# Bind("FormFileID") %>' OnClick="LinkButton4_Click"/>
                    </ItemTemplate>
                  </asp:TemplateField>
                  
                  <asp:TemplateField ShowHeader="false"  ItemStyle-CssClass="table_tekst_edit">
                    <HeaderTemplate>
                      <asp:Label ID="Label11"   Text="Action" runat="server"/>
                    </HeaderTemplate>
                    <ItemTemplate>
                      <asp:LinkButton ID="LinkButton5" runat="server" CommandArgument='<%# Bind("FileID") %>' CausesValidation="false" Text="Convert" OnClick="LinkButton5_Click"/>
                    </ItemTemplate>
                  </asp:TemplateField>
                  
                  </Columns>
                  <EmptyDataTemplate >  <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                        <tr>
                                            <td align="left" class="podnaslov">
                                                <asp:Label ID="Label1" runat="server" />There is no Uploaded Files. 
                                            </td>
                                        </tr>
                                    </table>
                  </EmptyDataTemplate>

                </asp:GridView>
                  
                  </asp:Panel> <!-- Tabs Editor Panel-->
                  <asp:Button style="DISPLAY: none" id="Button1ShowPopup" runat="server"></asp:Button> 
                  <ajaxToolKit:ModalPopupExtender id="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground" 
                  CancelControlID="btnClose1" PopupControlID="Panel1Tabs" TargetControlID="Button1ShowPopup"></ajaxToolKit:ModalPopupExtender> <!-- ModalPopup Panel-->
                  <asp:Panel style="DISPLAY: none; Z-INDEX: 10001" id="Panel1Tabs" runat="server" Width="600px" BackColor="lightblue">
                  <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                    <contenttemplate><div id="divTabNum"> &nbsp;File Details</div>
                        <ajaxToolkit:ColorPickerExtender ID="ColorPicker1" runat="server"/>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" >
                          <contenttemplate>
                              <asp:GridView ID="GridView2" ShowFooter="true"  BorderWidth="2"
                               runat="server" Width="100%" AutoGenerateColumns="false" DataKeyNames="FileID">
                              <PagerSettings Visible="False" />
                              <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                              <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                              <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                              <HeaderStyle  CssClass="form_title" BackColor="#DBD9CF" />
                              <FooterStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                              <Columns>
                                <asp:BoundField DataField="FileID" HeaderText="File ID" SortExpression="FileID" >
                                    <HeaderStyle CssClass="form_title" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" >
                                    <HeaderStyle CssClass="form_title" />
                                </asp:BoundField>
                                <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" >
                                    <HeaderStyle CssClass="form_title" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FileNumber" HeaderText="File#" SortExpression="FileNumber" >
                                    <HeaderStyle CssClass="form_title" />
                                </asp:BoundField>
                                <asp:BoundField DataField="YearDate" HeaderText="Year" SortExpression="YearDate" >
                                    <HeaderStyle CssClass="form_title" />
                                </asp:BoundField>
                                
                                <asp:BoundField DataField="UploadDate" HeaderText="Sync Date" SortExpression="UploadDate" DataFormatString="{0:MM.dd.yyyy}" NullDisplayText="" >
                                    <HeaderStyle CssClass="form_title" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ConvertDate" HeaderText="ConvertedDate" SortExpression="ConvertDate" DataFormatString="{0:MM.dd.yyyy}" NullDisplayText="" >
                                    <HeaderStyle CssClass="form_title" />
                                </asp:BoundField>
                    
                              </Columns>
                              </asp:GridView>
                          </contenttemplate>
                        
                        </asp:UpdatePanel>
                    </contenttemplate>
                  
                  </asp:UpdatePanel>
                     <div align="right" style="width:95%; background-color:lightblue">
                    <asp:Button ID="btnClose1" runat="server"  Text="Close" Width="50px"/>
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                  </div>
                  </asp:Panel> </TD></TR><TR><TD class="prevnext" bgColor=#f8f8f5>
                  <cc1:WebPager id="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" PageSize="15" 
                  OnPageSizeChanged="WebPager1_PageSizeChanged" CssClass="prevnext" PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1"></cc1:WebPager> 
                  <asp:HiddenField id="SortDirection1" runat="server" Value=""></asp:HiddenField></TD></TR></TBODY></TABLE>
                </contenttemplate>
                <triggers>
                    <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged"/>
                </triggers>
                </asp:UpdatePanel>
                            </div>
                        </td>
                       
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
