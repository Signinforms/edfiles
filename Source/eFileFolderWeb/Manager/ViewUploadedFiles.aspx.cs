using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using AjaxControlToolkit;
using DataQuicker2.Framework;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;
using Shinetech.Engines.Adapters;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;

public partial class Manage_UploadFiles : BasePage
{
    private static readonly string PDFPATH = ConfigurationManager.AppSettings["PdfPath"];
    private static readonly string localRoot = ConfigurationManager.AppSettings["UploadPath"];
    private static readonly string serverVolume = ConfigurationManager.AppSettings["ServerVolume"];

    //public string IsShowInsert;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
    }

    #region 数据绑定
    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData()
    {
        this.DataSource = FileFolderManagement.GetUploadedFiles();
    }
    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }
    #endregion

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["UploadedFileSource"] as DataTable;
        }
        set
        {
            this.Session["UploadedFileSource"] = value;
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

 
    //View FileForm
    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;
        string formId = Convert.ToString(btnLink.Text.Trim());

        DataView view = new DataView(GetFilesDataSource(formId));
        GridView2.DataSource = view;

        GridView2.DataBind();
        UpdatePanel3.Update();
        ModalPopupExtender1.Show();

    }

    //Convert this pdf file for a user
    protected void LinkButton5_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;

        string fileId = btnLink.CommandArgument;
        ConvertFile(Convert.ToInt32(fileId));
    }

    protected void ConvertFile(int fileId)
    {
        UploadFile file = new UploadFile(fileId);
        if (!file.IsExist) return;

        string fileName = Path.GetFileName(file.FileName.Value);
        string fullPath = Path.Combine(localRoot, file.FileName.Value);

        string folderId = file.FolderId.ToString();
        string dividerId = file.DividerId.ToString();
        string uid = file.UserID.ToString();

        string serverPath = Path.Combine(serverVolume, folderId.ToString());
        serverPath = Path.Combine(serverPath, dividerId.ToString());

        if (!Directory.Exists(serverPath))
        {
            Directory.CreateDirectory(serverPath);
        }

        string encodeName = HttpUtility.UrlPathEncode(fileName);
        encodeName = encodeName.Replace("%", "$");

        serverPath = Path.Combine(serverPath, encodeName);

        File.Copy(fullPath, serverPath, true);

        ImageAdapter.ProcessPostFile(fullPath, folderId, dividerId, uid, Path.GetDirectoryName(serverPath),
            Path.GetFileName(fullPath), file.FormFileID.Value, file.FileID.Value);

        SearchFilesByField();
    }

    public DataTable GetFilesDataSource(string formId)
    {
        FormFile dv = new FormFile();
        ObjectQuery query = dv.CreateQuery();
        query.SetCriteria(dv.FormId, Convert.ToInt32(formId));

        DataTable table = new DataTable();

        try
        {
            query.Fill(table);

        }
        catch
        {
            table = null;
        }
        return table;
    }

    public void FillGridView()
    {
        string formId = HiddenField1.Value;

        DataTable table = GetFilesDataSource(formId);
        DataView view = new DataView(table);
        GridView2.DataSource = view;
        GridView2.DataBind();

    }



    protected void btnClose_Click(object sender, EventArgs e)
    {
        GetData();
        BindGrid();
        UpdatePanel1.Update();
    }


    protected void OnSearchButton_Click(object sender, ImageClickEventArgs e)
    {
        SearchFilesByField();
    }

    protected void SearchFilesByField()
    {
        String keyword = this.textfield3.Value.Trim();
        this.DataSource = FileFolderManagement.GetFileForms(keyword);

        this.BindGrid();
    }

    protected void GridView1_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if(e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView obj = e.Row.DataItem as DataRowView;
            if (Convert.ToInt32(obj.Row["FormFileID"]) == 0)
            {
                LinkButton button = e.Row.Cells[1].FindControl("LinkButton4") as LinkButton;
                if (button != null)
                {
                    button.Visible = false;
                }
            }

            if (Convert.ToString(obj.Row["Flag"]) == "y")
            {
                LinkButton button = e.Row.Cells[1].FindControl("LinkButton5") as LinkButton;
                if (button != null)
                {
                    button.Visible = false;
                }
            }
        }
    }
}
