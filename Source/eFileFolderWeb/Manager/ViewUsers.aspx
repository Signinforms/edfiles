<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    CodeFile="ViewUsers.aspx.cs" Inherits="ManageViewUsers" Title="" %>

<%@ Import Namespace="System.Data" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register Src="../Controls/UserFind.ascx" TagName="UserFind" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style>
        #comboBoxOffice_chosen
        {
            display: block;
            margin-top: 5px;
        }

        #comboBoxDepartments_chosen
        {
            display: block;
            margin-top: 5px;
        }

        .chosen-results
        {
            max-height: 200px !important;
        }

        .chosen-container
        {
            font-size: 15px;
        }

            .chosen-container .chosen-choices
            {
                max-height: 100px;
                overflow: auto;
            }
    </style>
    <script type="text/javascript">

        function OnBtnSaveClick() {
            var usersList = $('#comboBoxOffice_chosen .chosen-choices').children();
            var depsList = $('#comboBoxDepartments_chosen .chosen-choices').children();
            var users = [];
            var departments = [];

            if (usersList.length > 1) {
                for (var i = 0; i < usersList.length - 1; i++) {
                    if ($(usersList[i])) {
                        users.push({ 'UID': $($('#comboBoxOffice').find('option')[$(usersList[i]).find('a').attr('data-option-array-index')]).val() });
                    }
                }
            }

            if (depsList.length > 1) {
                for (var i = 0; i < depsList.length - 1; i++) {
                    if ($(depsList[i])) {
                        departments.push({ 'DepartmentId': $($('#comboBoxDepartments').find('option')[$(depsList[i]).find('a').attr('data-option-array-index')]).val() });
                    }
                }
            }

            $("#" + '<%=hdnMultiUserIds.ClientID %>').val(JSON.stringify(users));
            $("#" + '<%=hdnDepartmentIds.ClientID %>').val(JSON.stringify(departments));
        }

        function UnAssignOffice(ele, uid) {
            var assignedUID = $($('#comboBoxOffice').find('option')[ele.attr('data-option-array-index')]).val();
            var assignedName = $($('#comboBoxOffice').find('option')[ele.attr('data-option-array-index')]).html();
            $.ajax(
             {
                 type: "POST",
                 url: '../FlexWebService.asmx/UnAssignOffice',
                 contentType: "application/json; charset=utf-8",
                 data: '{"assignedUid":"' + assignedUID + '","assignedTo": "' + uid + '"}',
                 dataType: "json",
                 success: function (result) {
                     if (result.d.length > 0)
                         alert("Office " + assignedName + " has been unassigned successfully.");
                 },
                 fail: function (data) {
                 }
             });
        }

        function UnAssignDepartment(ele, uid) {
            var assignedDep = $($('#comboBoxDepartments').find('option')[ele.attr('data-option-array-index')]).html();
            //var isDelete = confirm("Are you sure you want to delete " + assignedDep + " ?");
            //if (isDelete) {
            var depId = $($('#comboBoxDepartments').find('option')[ele.attr('data-option-array-index')]).val();
            $.ajax(
             {
                 type: "POST",
                 url: '../FlexWebService.asmx/UnAssignDepartment',
                 contentType: "application/json; charset=utf-8",
                 data: '{"departmentId":"' + depId + '","uid": "' + uid + '"}',
                 dataType: "json",
                 success: function (result) {
                     if (result.d.length > 0)
                         alert("Department " + assignedDep + " has been unassigned successfully.");
                 },
                 fail: function (data) {
                 }
             });
            //}
            //else { return false; }
        }

        function LoadOffice(uid) {
            $("#comboBoxOffice").html('');
            $("#comboBoxOffice").chosen("destroy");
            $.ajax(
             {
                 type: "POST",
                 url: '../FlexWebService.asmx/GetAssignedOfficesForOffice',
                 contentType: "application/json; charset=utf-8",
                 data: '{"officeId":"' + uid + '"}',
                 dataType: "json",
                 success: function (result) {
                     var offices = [];
                     if (result.d) {
                         $.each(result.d.Item1, function (i, text) {
                             if (i != uid)
                                 $('<option />', { value: i, text: text }).appendTo($("#comboBoxOffice"));
                         });
                         $.each(result.d.Item2, function (i, text) {
                             offices.push(i);
                         });
                     }
                     $("#comboBoxOffice").chosen({ width: "315px" });
                     $("#comboBoxOffice").val(offices).trigger("chosen:updated");
                     $chosen = $("#comboBoxOffice").chosen();
                     var chosen = $chosen.data("chosen");
                     var _fn = chosen.result_select;
                     chosen.result_select = function (evt) {
                         evt["metaKey"] = true;
                         evt["ctrlKey"] = true;
                         chosen.result_highlight.addClass("result-selected");
                         return _fn.call(chosen, evt);
                     };
                     $('#comboBoxOffice_chosen .search-choice-close').click(function () {
                         UnAssignOffice($(this), uid);
                     });
                     //hideLoader();
                 },
                 fail: function (data) {
                     alert("Failed to get Offices. Please try again!");
                     //hideLoader();
                 }
             });
        }

        function LoadDepartments(uid) {
            $("#comboBoxDepartments").html('');
            $("#comboBoxDepartments").chosen("destroy");
            $.ajax(
             {
                 type: "POST",
                 url: '../FlexWebService.asmx/GetAssignedDepartmentsForOffice',
                 contentType: "application/json; charset=utf-8",
                 data: '{"officeId":"' + uid + '"}',
                 dataType: "json",
                 success: function (result) {
                     var offices = [];
                     if (result.d) {
                         $.each(result.d.Item1, function (i, text) {
                             if (i != uid)
                                 $('<option />', { value: i, text: text }).appendTo($("#comboBoxDepartments"));
                         });
                         $.each(result.d.Item2, function (i, text) {
                             offices.push(i);
                         });
                     }
                     $("#comboBoxDepartments").chosen({ width: "315px" });
                     $("#comboBoxDepartments").val(offices).trigger("chosen:updated");
                     $chosen = $("#comboBoxDepartments").chosen();
                     var chosen = $chosen.data("chosen");
                     var _fn = chosen.result_select;
                     chosen.result_select = function (evt) {
                         evt["metaKey"] = true;
                         evt["ctrlKey"] = true;
                         chosen.result_highlight.addClass("result-selected");
                         return _fn.call(chosen, evt);
                     };
                     $('#comboBoxDepartments_chosen .search-choice-close').click(function () {
                         UnAssignDepartment($(this), uid);
                     });
                 },
                 fail: function (data) {
                     alert("Failed to get Departments. Please try again!");
                 }
             });
        }
    </script>
    <table>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td width="725px" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                                        style="background-repeat: repeat-x;">
                                        <div style="background: url(images/inner_b.gif); background-position: right top; background-repeat: no-repeat; margin-top: 10px;">
                                            <div style="width: 725px; height: 60px;">
                                                <div class="Naslov_Zelen">
                                                    View Users
                                                </div>
                                            </div>
                                            <div class="podnaslovUser" style="padding: 5px; background-color: #f6f5f2;">
                                                Below is a list of all users associated with this account. Please verify and update
                                    appropriate user info.
                                            </div>
                                            <div>

                                                <table style="margin: 0px 20px 0px 35px" cellspacing="1" cellpadding="0" width="920"
                                                    bgcolor="#ffffff" border="0">
                                                    <tbody>
                                                        <div background="../images/bg_main.gif" bgcolor="#f6f5f2">
                                                            <asp:LoginView ID="LoginView2" runat="server">
                                                                <RoleGroups>
                                                                    <asp:RoleGroup Roles="Administrators">
                                                                        <ContentTemplate>
                                                                            <td valign="top">
                                                                                <uc1:UserFind ID="UserFind1" runat="server" />
                                                                        </ContentTemplate>
                                                                    </asp:RoleGroup>
                                                                </RoleGroups>
                                                            </asp:LoginView>
                                                        </div>
                                                        <div background="../images/bg_main.gif" bgcolor="#f6f5f2">
                                                            <asp:LoginView ID="LoginView3" runat="server">
                                                                <RoleGroups>
                                                                    <asp:RoleGroup Roles="offices">
                                                                        <ContentTemplate>
                                                                            <td valign="top">
                                                                                <uc1:UserFind ID="UserFind2" runat="server" />
                                                                        </ContentTemplate>
                                                                    </asp:RoleGroup>
                                                                </RoleGroups>
                                                            </asp:LoginView>
                                                        </div>
                                                        <asp:GridView ID="GridView1" runat="server" DataKeyNames="UID" OnSorting="GridView1_Sorting" OnRowCreated="GridView1_RowCreated"
                                                            AllowSorting="True" AutoGenerateColumns="false" Width="100%" BorderWidth="2">
                                                            <PagerSettings Visible="False" />
                                                            <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                            <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                            <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                            <Columns>
                                                                <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="Label8" runat="server" Text="Action" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CausesValidation="false" OnClick="LinkButton1_Click"></asp:LinkButton>
                                                                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CausesValidation="false" CommandArgument='<%# Eval("UID")%>' OnCommand="LinkButton2_Click"></asp:LinkButton>
                                                                        <%--<ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="LinkButton2"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                        <ajaxToolkit:ConfirmButtonExtender ID="ButtonDeleteUser" DisplayModalPopupID="lnkDelete_ModalPopupExtender"  runat="server" ConfirmText="Are you sure to delete this user ?"
                                                                        TargetControlID="LinkButton2">
                                                                        </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="ParentName" HeaderText="ParentName" SortExpression="ParentName">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <%--<asp:BoundField DataField="DOB" HeaderText="DOB" DataFormatString="{0:MM.dd.yyyy}"
                                                                    SortExpression="DOB" />--%>
                                                                <asp:BoundField DataField="IsApproved" HeaderText="IsActive" SortExpression="IsApproved" />
                                                                <asp:BoundField DataField="LastLoginDate" HeaderText="LastLoginDate" DataFormatString="{0:MM.dd.yyyy}"
                                                                    SortExpression="LastLoginDate" />

                                                                <asp:BoundField DataField="EnableChargeFlag" HeaderText="EnableStorage" SortExpression="EnableChargeFlag" />
                                                                <asp:BoundField DataField="FolderNO" HeaderText="FolderCount" SortExpression="FolderNO" DataFormatString="{0}" />
                                                                <asp:BoundField DataField="FolderSize" HeaderText="StorageSize" SortExpression="FolderSize" DataFormatString="{0:F2}" />
                                                                <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="false" Text="subscription" OnClick="LinkButton3_Click"></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>

                                                        <asp:Panel class="popupConfirmation" ID="DivDeleteConfirmation" Style="display: none"
                                                            runat="server">
                                                            <div class="popup_Container">
                                                                <div class="popup_Titlebar" id="PopupHeader">
                                                                    <div class="TitlebarLeft">
                                                                        Delete User
                                                                    </div>
                                                                    <div class="TitlebarRight" onclick="$get('ButtonDeleteCancel').click();">
                                                                    </div>
                                                                </div>
                                                                <div class="popup_Body">
                                                                    <p>
                                                                        Are you sure you want to delete this user?
                                                                    </p>
                                                                    <p>
                                                                        Please enter password to delete user:
                                                                    <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                                                                    </p>
                                                                </div>
                                                                <div class="popup_Buttons">
                                                                    <input id="ButtonDeleleOkay" type="button" value="Yes" style="margin-left: 80px; width: 65px" />
                                                                    <input id="ButtonDeleteCancel" type="button" value="No" style="margin-left: 20px; width: 65px" />
                                                                </div>
                                                            </div>
                                                        </asp:Panel>

                                                        <asp:Button Style="display: none" ID="btnShowPopup" CausesValidation="false" runat="server"></asp:Button>
                                                        <ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" BackgroundCssClass="modalBackground"
                                                            CancelControlID="btnClose" PopupControlID="pnlPopup" TargetControlID="btnShowPopup">
                                                        </ajaxToolkit:ModalPopupExtender>
                                                        <!-- ModalPopup Panel-->
                                                        <asp:Panel Style="display: none" ID="pnlPopup" runat="server" Width="500px">
                                                            <asp:UpdatePanel ID="updPnlCustomerDetail" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <asp:Label ID="lblCustomerDetail" runat="server" Width="95%" Text="User Details"
                                                                        BackColor="lightblue" Font-Bold="true"></asp:Label>
                                                                    <asp:DetailsView ID="dvCustomerDetail" runat="server" DataKeyNames="UID" Width="95%"
                                                                        BackColor="white" DefaultMode="Edit" AutoGenerateRows="false" AllowPaging="false"
                                                                        OnDataBound="dvCustomerDetail_DataBound">
                                                                        <Fields>
                                                                            <asp:BoundField DataField="Name" ReadOnly="true" HeaderText="Name" />
                                                                            <asp:TemplateField HeaderText="Firstname" ShowHeader="true">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="textBoxFirstname" runat="server" MaxLength="20" Text='<%# DataBinder.Eval(Container.DataItem,"Firstname")%>' />
                                                                                    <asp:RequiredFieldValidator runat="server" ID="firstNameV" ControlToValidate="textBoxFirstname"
                                                                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The first name is required!" />
                                                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                                                                        TargetControlID="firstNameV" HighlightCssClass="validatorCalloutHighlight" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Lastname" ShowHeader="true">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="textBoxLastname" MaxLength="20" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Lastname")%>' />
                                                                                    <asp:RequiredFieldValidator runat="server" ID="lastNameV" ControlToValidate="textBoxLastname"
                                                                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The last name is required!" />
                                                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10"
                                                                                        TargetControlID="lastNameV" HighlightCssClass="validatorCalloutHighlight" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <%-- <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                                                                <HeaderTemplate>
                                                                                    <asp:Label ID="Label2" runat="server" Text="DOB" />
                                                                                </HeaderTemplate>
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox runat="server" MaxLength ="50" Text='<%# DataBinder.Eval(Container.DataItem,"DOB")%>'
                                                                                        ID="textfield5" />
                                                                                         <asp:RequiredFieldValidator runat="server" ID="DobV" ControlToValidate="textfield5"
                                                                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The DOB is required!" />
                                                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5"
                                                                                        TargetControlID="DobV" HighlightCssClass="validatorCalloutHighlight" />
                                                                                    <asp:RegularExpressionValidator ID="DobC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The DOB is not a date"
                                                                                        ControlToValidate="textfield5" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                                                                        Visible="true" Display="None"></asp:RegularExpressionValidator>
                                                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6"
                                                                                        TargetControlID="DobC" HighlightCssClass="validatorCalloutHighlight" />
                                                                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" PopupPosition="Right" SelectedDate='<%# DataBinder.Eval(Container.DataItem,"DOB")%>'
                                                                                        runat="server" TargetControlID="textfield5" Format="MM-dd-yyyy">
                                                                                    </ajaxToolkit:CalendarExtender>
                                                                                    
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>--%>
                                                                            <asp:CheckBoxField DataField="IsApproved" HeaderText="IsActive" />
                                                                            <asp:CheckBoxField DataField="EnableChargeFlag" HeaderText="EnableStorage" />
                                                                            <asp:TemplateField HeaderText="Email" ShowHeader="true">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="textBoxEmail" MaxLength="50" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Email")%>' />
                                                                                    <asp:RequiredFieldValidator runat="server" ID="EmailReq" ControlToValidate="textBoxEmail"
                                                                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />An email is required." />
                                                                                    <asp:RegularExpressionValidator ID="rev_Email" runat="server" ErrorMessage="<b>Required Field Format</b><br />It is not an email format."
                                                                                        ControlToValidate="textBoxEmail" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                                        Visible="true" Display="None"></asp:RegularExpressionValidator>
                                                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="EmailReqE" TargetControlID="EmailReq"
                                                                                        HighlightCssClass="validatorCalloutHighlight" />
                                                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                                                                        TargetControlID="rev_Email" HighlightCssClass="validatorCalloutHighlight" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Telephone" ShowHeader="true">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="textBoxPhone" runat="server" MaxLength="23" Text='<%# DataBinder.Eval(Container.DataItem,"Telephone")%>' />
                                                                                    <asp:RegularExpressionValidator ID="TelV" ControlToValidate="textBoxPhone" runat="server"
                                                                                        ErrorMessage="<b>Required Field Format</b><br />It is not a telphone format."
                                                                                        ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                                                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                                                                        TargetControlID="TelV" HighlightCssClass="validatorCalloutHighlight" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="MobilePhone" ShowHeader="true">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="textBoxMobile" MaxLength="20" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"MobilePhone")%>' />
                                                                                    <asp:RegularExpressionValidator ID="mobileV" ControlToValidate="textBoxMobile" runat="server"
                                                                                        ErrorMessage="<b>Required Field Missing</b><br />It is not a Mobile format."
                                                                                        ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                                                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9"
                                                                                        TargetControlID="mobileV" HighlightCssClass="validatorCalloutHighlight" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="FaxNumber" ShowHeader="true">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="textBoxFax" MaxLength="23" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"FaxNumber")%>' />
                                                                                    <asp:RegularExpressionValidator ID="faxV" ControlToValidate="textBoxFax" runat="server"
                                                                                        ErrorMessage="<b>Required Field Missing</b><br />It is not a FAX format." ValidationExpression="^\d{7,}"
                                                                                        Display="None"></asp:RegularExpressionValidator>
                                                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8"
                                                                                        TargetControlID="faxV" HighlightCssClass="validatorCalloutHighlight" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="CompanyName" ShowHeader="true">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="textBoxCompanyName" MaxLength="50" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CompanyName")%>' />
                                                                                    <%--  <asp:RequiredFieldValidator runat="server" ID="companyNameN" ControlToValidate="textBoxCompanyName"
                                                                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The company name is required!" />
                                                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14"
                                                                                        TargetControlID="companyNameN" HighlightCssClass="validatorCalloutHighlight" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Assign Offices">
                                                                                <ItemTemplate>
                                                                                    <select id="comboBoxOffice" multiple style="color: rgb(68,68,68); border-radius: 5px; background-color: #fff; border: 1px solid #aaa; box-shadow: 0 1px 0 #fff inset; background: linear-gradient(#fff 20%, #f6f6f6 50%, #eee 52%, #f4f4f4 100%); max-height: 100px;">
                                                                                    </select>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Address" ShowHeader="true">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="textBoxAddress" MaxLength="200" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Address")%>' />
                                                                                    <%--<asp:RequiredFieldValidator runat="server" ID="addressN" ControlToValidate="textBoxAddress"
                                                                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The address is required!" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15"
                                                                                    TargetControlID="addressN" HighlightCssClass="validatorCalloutHighlight" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="City" ShowHeader="true">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="textBoxCity" MaxLength="30" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"City")%>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="State/Province">
                                                                                <ItemTemplate>
                                                                                    <asp:DropDownList runat="server" ID="drpState">
                                                                                    </asp:DropDownList>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="OtherState" ShowHeader="true">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="textBoxOtherState" MaxLength="30" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"OtherState")%>' />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Country">
                                                                                <ItemTemplate>
                                                                                    <asp:DropDownList runat="server" ID="drpCountry" Width="51%">
                                                                                    </asp:DropDownList>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Postal" ShowHeader="true">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="textBoxPostal" MaxLength="30" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Postal")%>' />
                                                                                    <%--  <asp:RequiredFieldValidator runat="server" ID="txtPostalN" ControlToValidate="textBoxPostal"
                                                                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The postal is required!" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17"
                                                                                    TargetControlID="txtPostalN" HighlightCssClass="validatorCalloutHighlight" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Assign Departments">
                                                                                <ItemTemplate>
                                                                                    <select id="comboBoxDepartments" multiple style="color: rgb(68,68,68); border-radius: 5px; background-color: #fff; border: 1px solid #aaa; box-shadow: 0 1px 0 #fff inset; background: linear-gradient(#fff 20%, #f6f6f6 50%, #eee 52%, #f4f4f4 100%); max-height: 100px;">
                                                                                    </select>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Card No." ShowHeader="true">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="textBoxCardNo" MaxLength="30" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CreditCard")%>' />
                                                                                    <%--  <asp:RequiredFieldValidator runat="server" ID="txtCardNo" ControlToValidate="textBoxCardNo"
                                                                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The card no is required!" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
                                                                                    TargetControlID="txtCardNo" HighlightCssClass="validatorCalloutHighlight" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Exp.Date" ShowHeader="true">
                                                                                <ItemTemplate>
                                                                                    <asp:TextBox ID="textBoxExpireDate" MaxLength="30" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ExpirationDate","{0:MM/yyyy}")%>' />
                                                                                    <label align="right">(Format: MM/yyyy)</label><%--<asp:RequiredFieldValidator runat="server" ID="CardExpDate" ControlToValidate="textBoxExpireDate"
                                                                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The Exp.Date is required!" /><ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="CardExpDateAjax"
                                                                                    TargetControlID="CardExpDate" HighlightCssClass="validatorCalloutHighlight" />
                                                                                    <asp:RegularExpressionValidator ID="CardExpDateFormat" ControlToValidate="textBoxExpireDate" runat="server"
                                                                                    ErrorMessage="<b>Required Field Format</b><br />It is not a Exp.Date format."
                                                                                    ValidationExpression="^\d{2}\/\d{4}" Display="None"></asp:RegularExpressionValidator>
                                                                                       <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="CardExpDateFormatV"
                                                                                    TargetControlID="CardExpDateFormat" HighlightCssClass="validatorCalloutHighlight" />--%>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:BoundField DataField="RoleID" ReadOnly="true" HeaderText="RoleName" />
                                                                            <asp:BoundField DataField="LastLoginDate" ReadOnly="true" HeaderText="LastLoginDate" DataFormatString="{0:MM/dd/yyy}" />
                                                                        </Fields>
                                                                    </asp:DetailsView>
                                                                </ContentTemplate>
                                                                <Triggers>
                                                                    <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click"></asp:AsyncPostBackTrigger>
                                                                </Triggers>
                                                            </asp:UpdatePanel>
                                                            <asp:HiddenField runat="server" ID="hdnDepartmentIds" />
                                                            <asp:HiddenField runat="server" ID="hdnMultiUserIds" />
                                                            <div style="width: 95%; background-color: lightblue" align="right">
                                                                <asp:Button ID="btnSave" OnClientClick="OnBtnSaveClick()" OnClick="btnSave_Click" runat="server" Width="50px" Text="Save"></asp:Button>
                                                                <asp:Button ID="btnClose" runat="server" CausesValidation="false" Width="50px" Text="Close"></asp:Button>
                                                            </div>
                                                        </asp:Panel>

                                                        <!-- oo --->

                                                        <asp:Button ID="Button1ShowPopup" runat="server" Style="display: none" />
                                                        <ajaxToolkit:ModalPopupExtender ID="ModalPopup3" runat="server" PopupControlID="pnlPopup3" CancelControlID="btnCloseTrans"
                                                            DropShadow="true" BackgroundCssClass="modalBackground" TargetControlID="Button1ShowPopup" />
                                                        <!-- ModalPopup Panel-->
                                                        <asp:Panel ID="pnlPopup3" runat="server" Width="350px" Style="display: none; background-color: white">
                                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                                <ContentTemplate>
                                                                    <div style="width: 100%; background-color: lightblue"><strong>Transaction Details</strong></div>
                                                                    <div class="tekstDef">
                                                                        <table width="100%" border="0" cellpadding="2" cellspacing="0">
                                                                            <tr>
                                                                                <td align="left" class="podnaslov"><strong>Hello,<asp:Label ID="LabelUserName" Text="" runat="server" />:</strong></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="100%" align="left" class="podnaslov">This transaction is <strong>
                                                                                    <asp:Label ID="LabelResult" Text="" runat="server" /></strong>
                                                                                    :&nbsp;&nbsp;<asp:Label ID="LabelTransInfo" Text="" runat="server" />
                                                                                </td>
                                                                            </tr>

                                                                        </table>
                                                                    </div>
                                                                    <div class="tekstDef" style="background-color: White">
                                                                        <table width="100%" border="0" cellpadding="2" cellspacing="0">
                                                                            <tr>
                                                                                <td align="left" class="podnaslov" colspan="3">
                                                                                    <strong><span>Card details:</span></strong>                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td width="15%" align="left" class="podnaslov">Card Type</td>
                                                                                <td width="1%">&nbsp;</td>
                                                                                <td width="40%">
                                                                                    <span class="podnaslov">
                                                                                        <asp:Label ID="LabelCardType" Text="" runat="server" /></span>                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" class="podnaslov">Card&nbsp;No.</td>
                                                                                <td>&nbsp;</td>
                                                                                <td>
                                                                                    <span class="podnaslov">
                                                                                        <asp:Label ID="LabelCardNo" Text="" runat="server" /></span>                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" class="podnaslov">Exp.&nbsp;Date</td>
                                                                                <td>&nbsp;</td>
                                                                                <td>
                                                                                    <span class="podnaslov">
                                                                                        <asp:Label ID="LabelExpDate" Text="" runat="server" /></span>                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" class="podnaslov">Price</td>
                                                                                <td>&nbsp;</td>
                                                                                <td>
                                                                                    <span class="podnaslov">
                                                                                        <asp:Label ID="LabelPrice" Text="" runat="server" /></span>                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="left" class="podnaslov">Amount</td>
                                                                                <td>&nbsp;</td>
                                                                                <td>
                                                                                    <span class="podnaslov">
                                                                                        <asp:Label ID="LabelAmount" Text="" runat="server" /></span>                </td>
                                                                            </tr>

                                                                        </table>
                                                                        <hr width="95%" align="center" />
                                                                    </div>
                                                                    <div align="center" style="width: 100%; background-color: white">
                                                                        <asp:Button ID="btnCloseTrans" runat="server" Text="Close" Width="50px" />
                                                                    </div>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </asp:Panel>
                                                        <!-- o end o --->
                                    </td>
                                </tr>
                                <tr>
                                    <td class="prevnext" bgcolor="#f8f8f5">
                                        <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                            OnPageSizeChanged="WebPager1_PageSizeChanged" PageSize="15" CssClass="prevnext" PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1"></cc1:WebPager>
                                        <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
            </td>
            <td width="10" align="center">
                <img src="../images/lin.gif" width="1" height="453" vspace="10" /></td>
        </tr>
    </table>
</asp:Content>
