using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DataQuicker2.Framework;
using Shinetech.Framework.Controls;
using Shinetech.Utility;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;
using Account=Shinetech.DAL.Account;
using Newtonsoft.Json;

public partial class ManageViewUsers : LicensePage
{
    private static readonly string PDFPATH = ConfigurationManager.AppSettings["PdfPath"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();//重新获取操作后的数据源
            BindGrid();//绑定GridView,为删除服务

        }
    }

    #region 分页
    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        if (this.DataSource!=null)
        {
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        if (this.DataSource != null)
        {
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource_Users_Admin"] as DataTable;
        }
        set
        {
            this.Session["DataSource_Users_Admin"] = value;
        }
    }


    #endregion

    #region 数据绑定
    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData()
    {
        if (User.IsInRole("Administrators"))
        {
            this.DataSource = UserManagement.GetOfficeSubUsersForAdmin();
        }
        else
        {
            this.DataSource = UserManagement.GetWebUserList(Membership.GetUser().ProviderUserKey.ToString());
        }
    }
    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }
    #endregion

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();//重新设置数据源，绑定
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;
        string uid = Convert.ToString(this.GridView1.DataKeys[row.RowIndex].Value);

        DataView view = new DataView(this.DataSource as DataTable);
        view.RowFilter = "UID='" + uid + "'";
        dvCustomerDetail.DataSource = view;
        dvCustomerDetail.DataBind();
        DetailsView dsDetailsView = updPnlCustomerDetail.FindControl("dvCustomerDetail") as DetailsView;
        DropDownList drpStateIN = dsDetailsView.FindControl("drpState") as DropDownList;
        DropDownList drpCountryIN = dsDetailsView.FindControl("drpCountry") as DropDownList;
        drpStateIN.SelectedValue = (view[0]["StateID"].ToString() != "" && view[0]["StateID"].ToString() != "  " && view[0]["StateID"].ToString() != null) ? view[0]["StateID"].ToString() : "CA";
        drpCountryIN.SelectedValue = (view[0]["CountryID"].ToString() != "" && view[0]["CountryID"].ToString() != "  " && view[0]["CountryID"].ToString() != null) ? view[0]["CountryID"].ToString() : "US";
        this.mdlPopup.Show();
        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "LoadOffice('" + uid + "');LoadDepartments('" + uid + "')", true);
    }

    //Subscription
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;
        string uid = Convert.ToString(this.GridView1.DataKeys[row.RowIndex].Value);

        StorageTransaction trans = new StorageTransaction(); //new transaction
        Account account = new Account(uid);
        if (account.IsExist)
        {
            float price = 9.95f;
          
            LicenseKit lit = new LicenseKit(account.LicenseID.Value);

            if (account.LicenseID.Value == -1)
            {
                string strWrongInfor = String.Format("Error : The user {0} is trial user now.", account.Name.Value);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel3, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return;
            }

            if (!account.PersonalMonthPrice.Value.Equals(DBNull.Value) && !account.PersonalMonthPrice.Value.Equals(0))
            {
                price =(float) account.PersonalMonthPrice.Value;
            }
            else
            {
                price = lit.MonthPricePerUser.Value;
            }

            StorageState storagest = new StorageState(account, lit.MonthPricePerUser.Value, lit);

            Hashtable vars = StorageTransManage.ReadHtmlPage(account, storagest.Amount);
            if (vars == null)
            {
                trans.ResponseCode.Value = "-1";
                trans.ResponseAuthCode.Value = "";
                trans.ResponseAVSCode.Value = "";
                trans.ResponseDescription.Value = "Error:Transaction Failed";
                trans.ResponseReferenceCode.Value = "";
            }
            else
            {
                trans.ResponseAuthCode.Value = (string)vars[4];
                trans.ResponseAVSCode.Value = (string)vars[5] + ":" + ParseAvsCode((string)vars[5]);
                trans.ResponseCode.Value = (string)vars[0];
                trans.ResponseDescription.Value = (string)vars[3];
                trans.ResponseReferenceCode.Value = (string)vars[6];
            }

            trans.UserName.Value = account.Name.Value;
            trans.EmailAddress.Value = account.Email.Value;
            trans.Amount.Value = storagest.Amount;
            trans.CardNumber.Value = account.CreditCard.Value;
            trans.CardType.Value = account.CardType.Value;
            trans.ExchangeDate.Value = DateTime.Now;
            trans.ExpiredDate.Value = account.ExpirationDate.Value;
            trans.FirstName.Value = account.Firstname.Value;
            trans.LastName.Value = account.Lastname.Value;
            trans.OfficeID.Value = account.UID.Value;
            trans.Price.Value = price;
            trans.Size.Value = storagest.Size;

            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();

            try
            {
                trans.Create(connection);
            }
            catch (Exception exp)
            {

                //throw new ApplicationException(exp.Message);
            }

            if (vars[0].Equals("1"))
            {
                //runat another thread
                Thread emailThread = new Thread(new ParameterizedThreadStart(SendEmail));

                Hashtable paramVars = new Hashtable();
                paramVars.Add("trans", trans);
                paramVars.Add("account", account);

                emailThread.Start(paramVars);
            }

            LabelUserName.Text = account.Name.Value;
            LabelResult.Text = (string)vars[0] == "1" ? "successful" : "failed";
            LabelCardType.Text = account.CardType.Value;
            LabelCardNo.Text = account.CreditCard.Value;
            DateTimeFormatInfo myDTFI = new CultureInfo("en-US", false).DateTimeFormat;
            LabelExpDate.Text = account.ExpirationDate.Value.ToString("MM/yyyy", myDTFI);
            LabelTransInfo.Text = (string)vars[3];
            LabelPrice.Text = string.Format("${0:F2}", price);
            LabelAmount.Text = string.Format("${0:F2}", storagest.Amount);
            UpdatePanel3.Update();
            ModalPopup3.Show();

        }
    }

    private void SendEmail(object paramVars)
    {
        Hashtable vars = paramVars as Hashtable;

        string path = Server.MapPath("./");
        StorageTransManage.SendEmail(vars["trans"] as StorageTransaction,
            vars["account"] as Account, path);
    }

    private static string ParseAvsCode(string code)
    {
        string result = "Uknown";

        switch (code.ToUpper())
        {
            case "A":
                result = " [AVS - Address (Street) matches, ZIP does not]";
                break;
            case "B":
                result = " [AVS - Address information not provided for AVS check]";
                break;
            case "E":
                result = " [AVS - Error]";
                break;
            case "G":
                result = " [AVS - Non-U.S. Card Issuing Bank]";
                break;
            case "N":
                result = " [AVS - No Match on Address (Street) or ZIP]";
                break;
            case "P":
                result = " [AVS - AVS not applicable for this transaction]";
                break;
            case "R":
                result = " [AVS - Retry – System unavailable or timed out]";
                break;
            case "S":
                result = " [AVS - Service not supported by issuer]";
                break;
            case "U":
                result = " [AVS - Address information is unavailable]";
                break;
            case "W":
                result = " [AVS - 9 digit ZIP matches, Address (Street) does not]";
                break;
            case "X":
                result = " [AVS - Address (Street) and 9 digit ZIP match]";
                break;
            case "Y":
                result = " [AVS - Address (Street) and 5 digit ZIP match]";
                break;
            case "Z":
                result = " [AVS - 5 digit ZIP matches, Address (Street) does not]";
                break;
        }

        return result;
    }

    private static string ParseSecurityCode(string code)
    {
        string result = "Uknown";
        switch (code.ToUpper())
        {
            case "M":
                result = " [CVV - Match]";
                break;
            case "N":
                result = " [CVV - No Match]";
                break;
            case "P":
                result = " [CVV - Not Processed]";
                break;
            case "S":
                result = " [CVV - Should have been present]";
                break;
            case "U":
                result = " [CVV - Issuer unable to process request]";
                break;
        }
        return result;
    }


    protected void LinkButton2_Click(Object sender, CommandEventArgs e)
    {
         string password = textPassword.Text.Trim();
        string strPassword = PasswordGenerator.GetMD5(password);

        if (strPassword != ConfigurationManager.AppSettings["DeleteUserPwd"])
        {
            //show the password is wrong
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);

        }
        else
        {
            string uid = e.CommandArgument.ToString();
            Account account = new Account(uid);

            if (account.IsExist)
            {
                try
                {
                    MembershipUser user = Membership.GetUser(account.Name.Value);
                    Membership.DeleteUser(account.Name.Value);
                    account.Delete();

                    DataTable table = FileFolderManagement.GetFileFoldersByUserID(uid);

                    foreach (DataRow item in table.Rows)
                    {
                        try
                        {
                            string folderPath = "~/" + PDFPATH + "/" + item["FolderID"];
                            if (Directory.Exists(Server.MapPath(folderPath)))
                            {
                                Directory.Delete(Server.MapPath(folderPath), true);
                            }

                            FileFolderManagement.DelFileFolderByFolderID(item["FolderID"].ToString());
                        }
                        catch (Exception ex)
                        { }
                    }

                }
                catch { }

                GetData();
                BindGrid();
                //UpdatePanel1.Update();
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string uid = this.dvCustomerDetail.DataKey.Value.ToString();
        DataView dataTable = new DataView(this.DataSource);
        dataTable.RowFilter = "UID='" + uid + "'";
        dataTable.Sort = "UID";
        DataRowView drView = dataTable.FindRows(uid)[0];

        try
        {
            drView.BeginEdit();

            Shinetech.DAL.Account account = new Shinetech.DAL.Account(uid);

            if (!account.IsExist) return;

            foreach (DetailsViewRow item in dvCustomerDetail.Rows)
            {
                #region Edit DataRow
                TableCell cell = item.Controls[1] as TableCell;
                if (cell != null && cell.HasControls())
                {
                    TextBox textBox;
                    DropDownList dropDownList;
                    switch (item.RowIndex)
                    {
                        case 1: //Firstname
                            textBox = cell.FindControl("textBoxFirstname") as TextBox;
                            drView["Firstname"] = textBox.Text.Trim();
                            account.Firstname.Value = textBox.Text.Trim();
                            break;
                        case 2: //Lastname
                            textBox = cell.FindControl("textBoxLastname") as TextBox;
                            drView["Lastname"] = textBox.Text.Trim();
                            account.Lastname.Value = textBox.Text.Trim();
                            break;
                        //case 3: //DOB 
                        //    textBox = cell.Controls[1] as TextBox;
                        //    textBox.Text = textBox.Text.Replace('-', '/');
                        //    drView["DOB"] = DateTime.ParseExact(textBox.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                        //    account.DOB.Value = DateTime.ParseExact(textBox.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                            //break;
                        case 3: //IsActive 
                            CheckBox checkBox = cell.Controls[0] as CheckBox;
                            bool isActive = (bool)drView["IsApproved"];
                            if (checkBox.Checked != isActive)
                            {
                                drView["IsApproved"] = checkBox.Checked;
                                MembershipUser user = Membership.GetUser(account.Name.Value);
                                if (user != null)
                                {
                                    if (checkBox.Checked)
                                    {
                                        user.UnlockUser();
                                    }
                                   
                                    user.IsApproved = checkBox.Checked;
                                    Membership.UpdateUser(user);
                                }
                            }
                            break;
                        case 4: //EnableStorage
                            CheckBox checkBox1 = cell.Controls[0] as CheckBox;
                            bool enableStorage = (bool)drView["EnableChargeFlag"];
                            if (checkBox1.Checked != enableStorage)
                            {
                                account.EnableChargeFlag.Value = checkBox1.Checked;
                                account.StoragePaymentDate.Value = DateTime.Now;
                            }

                            break;
                        case 5: //Email
                            textBox = cell.FindControl("textBoxEmail") as TextBox;
                            drView["Email"] = textBox.Text.Trim();
                            account.Email.Value = textBox.Text.Trim();
                            break;
                        case 6: //Telephone
                            textBox = cell.FindControl("textBoxPhone") as TextBox;
                            drView["Telephone"] = textBox.Text.Trim();
                            account.Telephone.Value = textBox.Text.Trim();
                            break;
                        case 7: //MobilePhone
                            textBox = cell.FindControl("textBoxMobile") as TextBox;
                            drView["MobilePhone"] = textBox.Text.Trim();
                            account.MobilePhone.Value = textBox.Text.Trim();
                            break;
                        case 8: //FaxNumber 
                            textBox = cell.FindControl("textBoxFax") as TextBox;
                            drView["FaxNumber"] = textBox.Text.Trim();
                            account.FaxNumber.Value = textBox.Text.Trim();
                            break;
                        case 9: //CompanyName
                            textBox = cell.FindControl("textBoxCompanyName") as TextBox;
                            drView["CompanyName"] = textBox.Text.Trim();
                            account.CompanyName.Value = textBox.Text.Trim();
                            break;
                        case 10: //Address 
                            textBox = cell.FindControl("textBoxAddress") as TextBox;
                            drView["Address"] = textBox.Text.Trim();
                            account.Address.Value = textBox.Text.Trim();
                            break;
                        case 11://City
                            textBox = cell.FindControl("textBoxCity") as TextBox;
                            drView["City"] = textBox.Text.Trim();
                            account.City.Value = textBox.Text.Trim();
                            break;
                        case 13://OtherState
                            textBox = cell.FindControl("textBoxOtherState") as TextBox;
                            drView["OtherState"] = textBox.Text.Trim();
                            account.OtherState.Value = textBox.Text.Trim();
                            break;
                        case 15://Postal
                            textBox = cell.FindControl("textBoxPostal") as TextBox;
                            drView["Postal"] = textBox.Text.Trim();
                            account.Postal.Value = textBox.Text.Trim();
                            break;
                        case 12://State
                            dropDownList = cell.FindControl("drpState") as DropDownList;
                            drView["StateID"] = dropDownList.SelectedValue;
                            account.StateID.Value = dropDownList.SelectedValue;
                            break;
                        case 14://Country
                            dropDownList = cell.FindControl("drpCountry") as DropDownList;
                            drView["CountryID"] = dropDownList.SelectedValue;
                            account.CountryID.Value = dropDownList.SelectedValue;
                            break;
                        case 17://Credit Card No.
                            textBox = cell.FindControl("textBoxCardNo") as TextBox;
                            drView["CreditCard"] = textBox.Text.Trim();
                            account.CreditCard.Value = textBox.Text.Trim();
                            break;
                        case 18://ExpirationDate
                            textBox = cell.FindControl("textBoxExpireDate") as TextBox;
                            drView["ExpirationDate"] = string.IsNullOrEmpty(textBox.Text.Trim()) ? "01/1990" : textBox.Text.Trim();
                            account.ExpirationDate.Value = string.IsNullOrEmpty(textBox.Text.Trim()) ? Convert.ToDateTime("01/1990") : Convert.ToDateTime(textBox.Text);
                            break;
                        default:
                            break;
                    }

                }
                #endregion
            }

            #region AssignUser

            if (!string.IsNullOrEmpty(this.hdnMultiUserIds.Value))
            {
                DataTable dtUsers = new DataTable();
                dtUsers.Columns.Add("Index", typeof(int));
                dtUsers.Columns.Add("UID", typeof(string));
                AssignedUser[] userData = JsonConvert.DeserializeObject<List<AssignedUser>>(hdnMultiUserIds.Value).ToArray();
                int index = 1;
                foreach (var i in userData)
                {
                    if (!string.IsNullOrEmpty(i.UID))
                    {
                        dtUsers.Rows.Add(new object[] { index, i.UID });
                        index++;
                    }
                }
                General_Class.AssignUser(uid, Membership.GetUser().ProviderUserKey.ToString(), dtUsers);
            }

            #endregion

            #region AssignDepartment

            try
            {
                if (!string.IsNullOrEmpty(this.hdnDepartmentIds.Value))
                {
                    DataTable dtDeps = new DataTable();
                    dtDeps.Columns.Add("Index", typeof(int));
                    dtDeps.Columns.Add("DepartmentId", typeof(int));
                    AssignedDepartment[] userData = JsonConvert.DeserializeObject<List<AssignedDepartment>>(hdnDepartmentIds.Value).ToArray();
                    int index = 1;
                    foreach (var i in userData)
                    {
                        if (i.DepartmentId > 0)
                        {
                            dtDeps.Rows.Add(new object[] { index, i.DepartmentId });
                            index++;
                        }
                    }
                    General_Class.InsertIntoUserDepartments(dtDeps, new Guid(uid));
                }
            }
            catch (Exception ex) { }

            #endregion

            //submit to database
            UserManagement.UpdateUser(account);
            drView.EndEdit();
            dataTable.Table.AcceptChanges();
        }
        catch (Exception ex)
        {
            drView.EndEdit();
            dataTable.Table.RejectChanges();
        }
        finally
        {
           // dataTable.Dispose();
        }

        this.GridView1.DataSource = this.DataSource;
        this.GridView1.DataBind();
        UpdatePanel1.Update();
    }



    public DataTable States
    {
        get
        {
            object obj = Application["States"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetStates();
                Application["States"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    public DataTable Countries
    {
        get
        {
            object obj = Application["Countries"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetCountries();
                Application["Countries"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

   
    protected void GridView1_DataBinding(object sender, EventArgs e)
    {


    }
    protected void dvCustomerDetail_DataBound(object sender, EventArgs e)
    {
        DetailsView dsDetailsView = updPnlCustomerDetail.FindControl("dvCustomerDetail") as DetailsView;
        DropDownList drpStateIN = dsDetailsView.FindControl("drpState") as DropDownList;
        drpStateIN.DataSource = States;
        drpStateIN.DataValueField = "StateID";
        drpStateIN.DataTextField = "StateName";
        drpStateIN.DataBind();

        DropDownList drpCountryIn = dsDetailsView.FindControl("drpCountry") as DropDownList;
        drpCountryIn.DataSource = Countries;
        drpCountryIn.DataValueField = "CountryID";
        drpCountryIn.DataTextField = "CountryName";
        drpCountryIn.DataBind();
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow) 
        {
            LinkButton linkBtn = e.Row.FindControl("LinkButton2") as LinkButton;
            if (linkBtn != null)
                linkBtn.Enabled = false;
        }
    }
}
