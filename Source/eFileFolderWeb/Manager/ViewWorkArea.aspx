<%@ Page Language="C#" MasterPageFile="~/MasterPage5.master" AutoEventWireup="true"
    CodeFile="ViewWorkArea.aspx.cs" Inherits="ManageViewWorkArea" Title="" %>

<%@ Import Namespace="System.Data" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register Src="../Controls/UserFind.ascx" TagName="UserFind" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--link rel="stylesheet" href="http://blueimp.github.com/cdn/css/bootstrap.min.css">
<!-- Generic page styles -->
<!-- Bootstrap styles for responsive website layout, supporting different screen sizes -->
<link rel="stylesheet" href="http://blueimp.github.com/cdn/css/bootstrap-responsive.min.css">
<!-- Bootstrap CSS fixes for IE6 -->
<!--[if lt IE 7]><link rel="stylesheet" href="http://blueimp.github.com/cdn/css/bootstrap-ie6.min.css"><![endif]-->
<!-- Bootstrap Image Gallery styles -->
<link rel="stylesheet" href="http://blueimp.github.com/Bootstrap-Image-Gallery/css/bootstrap-image-gallery.min.css">--%>
    <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
    <link rel="stylesheet" href="../css/jquery.fileupload-ui.css">
    <!-- CSS adjustments for browsers with JavaScript disabled -->
    <noscript>
        <link rel="stylesheet" href="../css/jquery.fileupload-ui-noscript.css">
    </noscript>
    <table>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody>
                                <tr>
                                    <td width="725px" valign="top" colspan="2" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                                        style="background-repeat: repeat-x;">
                                        <div style="background: url(images/inner_b.gif); background-position: right top;
                                            background-repeat: no-repeat; margin-top: 10px;">
                                            <div style="width: 725px; height: 60px;">
                                                <div class="Naslov_Zelen">
                                                    View WorkArea</div>
                                            </div>
                                            <div class="podnaslovUser" style="padding: 5px; background-color: #f6f5f2;">
                                                Below is a list of all users associated with their workarea. Please choose and upload
                                                files by drag and drop from desktop.
                                            </div>
                                            <div>
                                                <table style="margin: 0px 20px 0px 35px" cellspacing="1" cellpadding="0" width="920"
                                                    bgcolor="#ffffff" border="0">
                                                    <tbody>
                                                        <div background="../images/bg_main.gif" bgcolor="#f6f5f2">
                                                            <td valign="top">
                                                                User Name:
                                                                <ajaxToolkit:ComboBox ID="DropDownList1" runat="server" Width="255px" DropDownStyle="DropDownList"
                                                                    AutoCompleteMode="SuggestAppend" CssClass="WindowsStyle" DataValueField="UID"
                                                                    DataTextField="Name" AutoPostBack="true" CaseSensitive="false" EnableViewState="true"
                                                                    OnSelectedIndexChanged="DropDownList1_OnSelectedIndexChanged" AppendDataBoundItems="false">
                                                                </ajaxToolkit:ComboBox>
                                                        </div>
                                                    </tbody>
                                                </table>
                                            </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="GridView1" runat="server" OnSorting="GridView1_Sorting" AllowSorting="True"
                                            AutoGenerateColumns="false" Width="100%" BorderWidth="2">
                                            <PagerSettings Visible="False" />
                                            <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                            <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                            <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                            <Columns>
                                                <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label8" runat="server" Text="Action" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("FilePath")%>'
                                                            OnCommand="LinkButton2_Click"></asp:LinkButton>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="ButtonDelete" runat="server" ConfirmText="Are you sure to delete this file ?"
                                                            TargetControlID="LinkButton2">
                                                        </ajaxToolkit:ConfirmButtonExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="FileUID" HeaderText="FileName" SortExpression="FileUID">
                                                    <HeaderStyle CssClass="form_title" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Size" HeaderText="Size(Mb)" DataFormatString="{0:F2}"
                                                    HtmlEncode="false" />
                                                <asp:BoundField DataField="DOB" HeaderText="DOB" DataFormatString="{0:MM.dd.yyyy}"
                                                    SortExpression="DOB" />
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                    <tr>
                                                        <td align="left" class="podnaslov">
                                                            <asp:Label ID="Label120" runat="server" />There are no files. right now.
                                                        </td>
                                                    </tr>
                                                </table>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </td>
                                    <td>
                                        <div class="container">
                                            <!-- The file upload form used as target for the file upload widget -->
                                            <form enctype="multipart/form-data" method="POST" action="//localhost:8638/"
                                            id="fileupload">
                                            <!-- Redirect browsers with JavaScript disabled to the origin page -->
                                            <noscript>
                                                &lt;input type="hidden" name="redirect" value="http://localhost:8638/file.upload/"&gt;</noscript>
                                            <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                                            <div class="row fileupload-buttonbar">
                                                <div class="span7">
                                                    <!-- The fileinput-button span is used to style the file input field as button -->
                                                    <span class="btn btn-success fileinput-button"><i class="icon-plus icon-white"></i><span>
                                                        Add files...</span>
                                                        <input type="file" multiple="" name="files[]">
                                                    </span>
                                                    <button class="btn btn-primary start" type="submit">
                                                        <i class="icon-upload icon-white"></i><span>Start upload</span>
                                                    </button>
                                                    <button class="btn btn-warning cancel" type="reset">
                                                        <i class="icon-ban-circle icon-white"></i><span>Cancel upload</span>
                                                    </button>
                                                    <button class="btn btn-danger delete" type="button">
                                                        <i class="icon-trash icon-white"></i><span>Delete</span>
                                                    </button>
                                                    <input type="checkbox" class="toggle">
                                                </div>
                                                <!-- The global progress information -->
                                                <div class="span5 fileupload-progress fade">
                                                    <!-- The global progress bar -->
                                                    <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress progress-success progress-striped active">
                                                        <div style="width: 0%;" class="bar">
                                                        </div>
                                                    </div>
                                                    <!-- The extended global progress information -->
                                                    <div class="progress-extended">
                                                        &nbsp;</div>
                                                </div>
                                            </div>
                                            <!-- The loading indicator is shown during file processing -->
                                            <div class="fileupload-loading">
                                            </div>
                                            <br>
                                            <div id="dropzone" class="fade well">Drag and Drop Pdf file(s) here</div>
                                            <!-- The table listing the files available for upload/download -->
                                            <table class="table table-striped" role="presentation">
                                                <tbody data-target="#modal-gallery" data-toggle="modal-gallery" class="files">
                                                </tbody>
                                            </table>
                                            <span class="alert alert-error">Upload server currently unavailable - Thu Nov 22 2012
                                                04:50:29 GMT+0800</span></form>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="prevnext" bgcolor="#f8f8f5">
                                        <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                            OnPageSizeChanged="WebPager1_PageSizeChanged" PageSize="15" CssClass="prevnext"
                                            PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1"></cc1:WebPager>
                                        <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged"></asp:AsyncPostBackTrigger>
                    </Triggers>
                </asp:UpdatePanel>
            </td>
            <td width="10" align="center">
                <img src="../images/lin.gif" width="1" height="453" vspace="10" />
            </td>
        </tr>
    </table>
    <!-- The template to display files available for upload -->
    <script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td class="preview"><span class="fade"></span></td>
        <td class="name"><span>{%=file.name%}</span></td>
        <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
        {% if (file.error) { %}
            <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else if (o.files.valid && !i) { %}
            <td>
                <div class="progress progress-success progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="bar" style="width:0%;"></div></div>
            </td>
            <td class="start">{% if (!o.options.autoUpload) { %}
                <button class="btn btn-primary">
                    <i class="icon-upload icon-white"></i>
                    <span>Start</span>
                </button>
            {% } %}</td>
        {% } else { %}
            <td colspan="2"></td>
        {% } %}
        <td class="cancel">{% if (!i) { %}
            <button class="btn btn-warning">
                <i class="icon-ban-circle icon-white"></i>
                <span>Cancel</span>
            </button>
        {% } %}</td>
    </tr>
{% } %}
    </script>
    <!-- The template to display files available for download -->
    <script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        {% if (file.error) { %}
            <td></td>
            <td class="name"><span>{%=file.name%}</span></td>
            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
            <td class="error" colspan="2"><span class="label label-important">Error</span> {%=file.error%}</td>
        {% } else { %}
            <td class="preview">{% if (file.thumbnail_url) { %}
                <a href="{%=file.url%}" title="{%=file.name%}" rel="gallery" download="{%=file.name%}"><img src="{%=file.thumbnail_url%}"></a>
            {% } %}</td>
            <td class="name">
                <a href="{%=file.url%}" title="{%=file.name%}" rel="{%=file.thumbnail_url&&'gallery'%}" download="{%=file.name%}">{%=file.name%}</a>
            </td>
            <td class="size"><span>{%=o.formatFileSize(file.size)%}</span></td>
            <td colspan="2"></td>
        {% } %}
        <td class="delete">
            <button class="btn btn-danger" data-type="{%=file.delete_type%}" data-url="{%=file.delete_url%}"{% if (file.delete_with_credentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                <i class="icon-trash icon-white"></i>
                <span>Delete</span>
            </button>
            <input type="checkbox" name="delete" value="1">
        </td>
    </tr>
{% } %}
    </script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>
    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    <script src="../js/vendor/jquery.ui.widget.js" type="text/javascript"></script>
    <!-- The Templates plugin is included to render the upload/download listings -->
    <script src="http://blueimp.github.com/JavaScript-Templates/tmpl.min.js" type="text/javascript"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="http://blueimp.github.com/JavaScript-Load-Image/load-image.min.js" type="text/javascript"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="http://blueimp.github.com/JavaScript-Canvas-to-Blob/canvas-to-blob.min.js"
        type="text/javascript"></script>
    <!-- Bootstrap JS and Bootstrap Image Gallery are not required, but included for the demo -->
    <script src="http://blueimp.github.com/cdn/js/bootstrap.min.js"></script>
    <script src="http://blueimp.github.com/Bootstrap-Image-Gallery/js/bootstrap-image-gallery.min.js"
        type="text/javascript"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="../js/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="../js/jquery.fileupload.js"></script>
    <!-- The File Upload file processing plugin -->
    <script src="../js/jquery.fileupload-ip.js"></script>
    <!-- The File Upload user interface plugin -->
    <script src="../js/jquery.fileupload-ui.js"></script>
    <!-- The main application script -->
    <script src="../js/main-local.js"></script>
    <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE8+ -->
    <!--[if gte IE 8]><script src="js/cors/jquery.xdr-transport.js"></script><![endif]-->
</asp:Content>
