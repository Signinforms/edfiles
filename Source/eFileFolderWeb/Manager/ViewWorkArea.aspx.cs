using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DataQuicker2.Framework;
using Shinetech.Framework.Controls;
using Shinetech.Utility;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;
using Account=Shinetech.DAL.Account;

public partial class ManageViewWorkArea : LicensePage
{
    private static readonly string PDFPATH = ConfigurationManager.AppSettings["PdfPath"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();//重新获取操作后的数据源
            BindGrid();//绑定GridView,为删除服务

            GetWorkAreaData();
            BindWorkAreaGrid();
        }
    }

    #region 分页
    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        if (this.WorkAreaSource != null)
        {
            WebPager1.DataSource = this.WorkAreaSource;
            WebPager1.DataBind();
        }
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        if (this.WorkAreaSource != null)
        {
            WebPager1.DataSource = this.WorkAreaSource;
            WebPager1.DataBind();
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource_Users"] as DataTable;
        }
        set
        {
            this.Session["DataSource_Users"] = value;
        }
    }

    public DataTable WorkAreaSource
    {
        get
        {
            return this.Session["DataSource_WorkArea"] as DataTable;
        }
        set
        {
            this.Session["DataSource_WorkArea"] = value;
        }
    }


    #endregion

    #region 数据绑定
    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData()
    {
        if (User.IsInRole("Administrators"))
        {
            this.DataSource = UserManagement.GetOfficeUsers();
        }
        else
        {
            this.DataSource = UserManagement.GetWebUserList(Membership.GetUser().ProviderUserKey.ToString());
        }

    }

    private void GetWorkAreaData()
    {
        if (DropDownList1.SelectedIndex == -1) return;

        string uid = DropDownList1.SelectedValue;
        DataTable table = GetWorkareaFiles(uid);
        if (table != null)
        {
            this.WorkAreaSource = table;
        }
    }

    private DataTable MakeDataTable()
    {
        // Create new DataTable.
        DataTable table = new DataTable();

        // Declare DataColumn and DataRow variables.
        DataColumn column;

        // Create new DataColumn, set DataType, ColumnName
        // and add to DataTable.    
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FileUID";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "FilePath";
        table.Columns.Add(column);


        column = new DataColumn();
        column.DataType = Type.GetType("System.Double");
        column.ColumnName = "Size";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.DateTime");
        column.ColumnName = "DOB";
        table.Columns.Add(column);

        return table;
    }

    protected DataTable GetWorkareaFiles(string uid)
    {
        DataTable table = MakeDataTable();

        string path = Server.MapPath("~/ScanInBox");
        path = string.Concat(path, Path.DirectorySeparatorChar, uid);

        try
        {
            if (Directory.Exists(path))
            {
                string[] files = Directory.GetFiles(path);
                DataRow row = null;
                FileInfo fi1;
                foreach (string file in files)
                {
                    row = table.NewRow();
                    row["FileUID"] = Path.GetFileName(file);
                    row["FilePath"] = file;
                    fi1 = new FileInfo(file);
                    row["Size"] = ((double)fi1.Length) / (double)(1024 * 1024);//MB
                    row["DOB"] = fi1.CreationTime;
                    table.Rows.Add(row);
                }
            }
            else
            {
                Directory.CreateDirectory(path);

            }
        }
        catch { }


        return table;
    }
    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        DropDownList1.DataSource = this.DataSource;
        DropDownList1.DataBind();
    }

    private void BindWorkAreaGrid()
    {
        WebPager1.DataSource = this.WorkAreaSource;
        WebPager1.DataBind();
    }

    #endregion

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.WorkAreaSource = Source.ToTable();//重新设置数据源，绑定
        BindGrid();
    }

    protected void DropDownList1_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void LinkButton2_Click(Object sender, CommandEventArgs e)
    {
        string path = e.CommandArgument.ToString();
        if (File.Exists(path))
        {
            try
            {
                File.Delete(path);
                GetWorkAreaData();
                BindWorkAreaGrid();

            }
            catch { }
        }
    }

  
}
