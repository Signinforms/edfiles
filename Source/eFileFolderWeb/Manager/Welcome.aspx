<%@ Page Language="C#" MasterPageFile="~/MasterPageOld.master" AutoEventWireup="true"
    CodeFile="Welcome.aspx.cs" Inherits="Manager_Welcome" Title="" %>

<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <!--BEGINNING OF chatstat.com CODE-->
    <%-- <script type='text/javascript'>
    var chat = 1;
    var chat_div_id = "chat_div";
    </script>--%>
    <%--    <script type='text/javascript' src='https://adlink.chatstat.com/JSBuilder.aspx?csuid=55627' defer='defer'></script><div id="chat_div"></div>
    --%>   <%-- <noscript></noscript>--%>
    <!--END OF chatstat.com CODE-->

    <table>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="730" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                            style="background-repeat: repeat-x;">
                            <div style="background: url(images/inner_b.gif); background-position: right top; background-repeat: no-repeat; margin-top: 10px;">
                                <div style="width: 725px; height: 65px;">
                                    <div class="Naslov_Crven">
                                        Welcome <span style="color: #333333;"><a target="_self" href="../Account/MyAccount.aspx" class="user_url">
                                            <asp:LoginName ID="LoginName2" runat="server" />
                                        </a>
                                        </span>
                                        <% if (OnlineUsers == 0)
                                            { %>
                                        <span>(<%=OnlineUsers%> online)</span>
                                        <% } %>
                                        <% else
                                            { %>
                                        <%--<asp:HyperLink ID="HyperLink35" NavigateUrl="~/Manager/ViewOnlineUsers.aspx" runat="server"><span>(<%=OnlineUsers%> online)</span></asp:HyperLink>--%>
                                        <%--<a href="ViewOnlineUsers.aspx" style="text-decoration:none">(<%=OnlineUsers%> online)</a>--%>
                                        <% } %>
                                    </div>
                                </div>
                                <%--<div class="podnaslov">
                                </div>--%>
                                <table width="100%" border="0" cellspacing="1" cellpadding="0" style="margin-top: 30px;">
                                    <tr>
                                        <td align="right">
                                            <asp:HyperLink ID="HyperLink3" NavigateUrl="~/Manager/CreateOffice.aspx" runat="server">
                                                <asp:Image ID="imagebutton3" runat="server" ImageUrl="~/images/mub6.gif" Width="230"
                                                    Height="60" border="0" />
                                            </asp:HyperLink></td>
                                        <td>
                                            <asp:HyperLink ID="HyperLink4" NavigateUrl="~/Manager/ViewUsers.aspx" runat="server">
                                                <asp:Image ID="imagebutton4" runat="server" ImageUrl="~/images/mub2.gif" Width="230"
                                                    Height="60" border="0" />
                                            </asp:HyperLink></td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Account/MyAccount.aspx" runat="server">
                                                <asp:Image ID="image1" runat="server" ImageUrl="~/images/mub3.gif" Width="230" Height="60"
                                                    border="0" />
                                            </asp:HyperLink></td>
                                        <td>
                                            <asp:HyperLink ID="HyperLink2" NavigateUrl="~/Account/ChangePassword.aspx" runat="server">
                                                <asp:Image ID="image2" runat="server" ImageUrl="~/images/mub4.gif" Width="230" Height="60"
                                                    border="0" />
                                            </asp:HyperLink></td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:HyperLink ID="HyperLink5" NavigateUrl="~/Manager/EFFLicense.aspx" runat="server">
                                                <asp:Image ID="image3" runat="server" ImageUrl="~/images/mub5.gif" Width="230" Height="60"
                                                    border="0" />
                                            </asp:HyperLink></td>
                                        <td>
                                            <asp:HyperLink ID="HyperLink6" NavigateUrl="~/Manager/ViewTransaction.aspx" runat="server">
                                                <asp:Image ID="image4" runat="server" ImageUrl="~/images/mub15.gif" Width="230" Height="60"
                                                    border="0" />
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:HyperLink ID="HyperLink8" NavigateUrl="~/Manager/CreatePartner.aspx" runat="server">
                                                <asp:Image ID="image6" runat="server" ImageUrl="~/images/mub7.png" Width="230" Height="60"
                                                    border="0" />
                                            </asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="HyperLink7" NavigateUrl="~/Manager/ViewPartner.aspx" runat="server">
                                                <asp:Image ID="image5" runat="server" ImageUrl="~/images/mub11.gif" Width="230" Height="60"
                                                    border="0" />
                                            </asp:HyperLink></td>

                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:HyperLink ID="HyperLink9" NavigateUrl="~/Manager/TrackView.aspx" runat="server">
                                                <asp:Image ID="image7" runat="server" ImageUrl="~/images/mub8.png" Width="230" Height="60"
                                                    border="0" />
                                            </asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="HyperLink10" NavigateUrl="~/Manager/ViewGiftNumber.aspx" runat="server">
                                                <asp:Image ID="image8" runat="server" ImageUrl="~/images/mub12.gif" Width="230" Height="60"
                                                    border="0" />
                                            </asp:HyperLink>
                                        </td>

                                    </tr>

                                    <tr>
                                        <td align="right">
                                            <asp:HyperLink ID="HyperLink11" NavigateUrl="~/Manager/PublishMessages.aspx" runat="server">
                                                <asp:Image ID="image9" runat="server" ImageUrl="~/images/mub9.png" Width="230" Height="60"
                                                    border="0" />
                                            </asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="HyperLink12" NavigateUrl="~/Manager/ViewFileForms.aspx" runat="server">
                                                <asp:Image ID="image10" runat="server" ImageUrl="~/images/mub10.png" Width="230" Height="60"
                                                    border="0" />
                                            </asp:HyperLink>
                                        </td>

                                    </tr>

                                    <tr>
                                        <td align="right">
                                            <asp:HyperLink ID="HyperLink13" NavigateUrl="~/Manager/ViewUploadedFiles.aspx" runat="server">
                                                <asp:Image ID="image11" runat="server" ImageUrl="~/images/mub16.gif" Width="230" Height="60"
                                                    border="0" />
                                            </asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="HyperLink14" NavigateUrl="~/Manager/MessageUsers.aspx" runat="server">
                                                <asp:Image ID="image12" runat="server" ImageUrl="~/images/ub18.gif" Width="230" Height="60"
                                                    border="0" />
                                            </asp:HyperLink>
                                        </td>

                                    </tr>

                                    <tr>
                                        <td align="right">
                                            <asp:HyperLink ID="HyperLink15" NavigateUrl="~/Manager/StoredPaperFiles.aspx" runat="server" Visible="true">
                                                <asp:Image ID="image13" runat="server" ImageUrl="~/images/ub21.gif" Width="230px" Height="60px" border="0" />
                                            </asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="HyperLink16" NavigateUrl="~/Manager/CreateNewBox.aspx" runat="server" Visible="true">
                                                <asp:Image ID="image14" runat="server" ImageUrl="~/images/ub22.gif" Width="230" Height="60"
                                                    border="0" />
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:HyperLink ID="HyperLink17" NavigateUrl="~/Manager/UserResourceDetail.aspx" runat="server" Visible="true">
                                                <asp:Image ID="image15" runat="server" ImageUrl="~/images/ub24.png" Width="230px" Height="60px" border="0" />
                                            </asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="HyperLink18" NavigateUrl="~/Manager/CreateFileRequest.aspx" runat="server" Visible="true">
                                                <asp:Image ID="image16" runat="server" ImageUrl="~/images/ub17.gif" Width="230px" Height="60px" border="0" />
                                            </asp:HyperLink>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:HyperLink ID="HyperLink19" NavigateUrl="~/Manager/RestoreRecycleBin.aspx" runat="server" Visible="true">
                                                <asp:Image ID="image27" runat="server" ImageUrl="~/images/ub27.gif" Width="230px" Height="60px" border="0" />
                                            </asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="HyperLink20" NavigateUrl="~/Manager/CreateFileFolders.aspx" runat="server" Visible="true">
                                                <asp:Image ID="image17" runat="server" ImageUrl="~/images/ub28.gif" Width="230px" Height="60px" border="0" />
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:HyperLink ID="HyperLink21" NavigateUrl="~/Manager/LogForm.aspx" runat="server">
                                                <asp:Image ID="image18" runat="server" ImageUrl="~/images/delete-log-forms.png" Width="230px" Height="60" border="0" />
                                            </asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="HyperLink22" NavigateUrl="~/Manager/PublicHolidays.aspx" runat="server">
                                                <asp:Image ID="image19" runat="server" ImageUrl="~/images/mub10000.png" Width="230px" Height="60" border="0" />
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:HyperLink ID="HyperLink23" NavigateUrl="~/Manager/LogFormUpload.aspx" runat="server">
                                                <asp:Image ID="image20" runat="server" ImageUrl="~/images/mub_LogFormUpload.png" Width="230px" Height="60" border="0" />
                                            </asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="HyperLink24" NavigateUrl="~/Manager/TemplateFolder.aspx" runat="server">
                                                <asp:Image ID="image21" runat="server" ImageUrl="~/images/ub30.png" Width="230px" Height="60" border="0" />
                                            </asp:HyperLink>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:HyperLink ID="HyperLink26" NavigateUrl="~/Manager/CasboFileUpload.aspx" runat="server">
                                                <asp:Image ID="image23" runat="server" ImageUrl="~/images/casbo-library.png" Width="230px" Height="60" border="0" />
                                            </asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="HyperLink27" NavigateUrl="~/Manager/ViewCasboLogs.aspx" runat="server">
                                                <asp:Image ID="image24" runat="server" ImageUrl="~/images/casbo-logs.png" Width="230px" Height="60" border="0" />
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:HyperLink ID="HyperLink25" NavigateUrl="~/Manager/MailBoxList.aspx" runat="server">
                                                <asp:Image ID="image22" runat="server" ImageUrl="~/images/view-mailbox.png" Width="230px" Height="60" border="0" />
                                            </asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="HyperLink28" NavigateUrl="~/Manager/UploadArchive.aspx" runat="server">
                                                <asp:Image ID="image25" runat="server" ImageUrl="~/images/archive-upload.png" Width="230px" Height="60" border="0" />
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:HyperLink ID="HyperLink29" NavigateUrl="~/Manager/DeleteWorkArea.aspx" runat="server">
                                                <asp:Image ID="image26" runat="server" ImageUrl="~/images/workarea.png" Width="230px" Height="60" border="0" />
                                            </asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="HyperLink30" NavigateUrl="~/Manager/DepartmentDetails.aspx" runat="server">
                                                <asp:Image ID="image28" runat="server" ImageUrl="~/images/view-departments.png" Width="230px" Height="60" border="0" />
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:HyperLink ID="HyperLink31" NavigateUrl="~/Manager/ViewEdForms.aspx" runat="server">
                                                <asp:Image ID="image29" runat="server" ImageUrl="~/images/view-edforms.png" Width="230px" Height="60" border="0" />
                                            </asp:HyperLink>
                                        </td>
                                        <td>
                                            <asp:HyperLink ID="HyperLink32" NavigateUrl="~/Manager/EstimatorPricing.aspx" runat="server">
                                                <asp:Image ID="image30" runat="server" ImageUrl="~/images/estimator-pricing.png" Width="230px" Height="60" border="0" />
                                            </asp:HyperLink>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:HyperLink ID="HyperLink33" NavigateUrl="~/Manager/DriveSizeLogs.aspx" runat="server">
                                                <asp:Image ID="image31" runat="server" ImageUrl="~/images/Drive-Size-Logs.png" Width="230px" Height="60" border="0" />
                                            </asp:HyperLink>
                                        </td>
                                        <td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
