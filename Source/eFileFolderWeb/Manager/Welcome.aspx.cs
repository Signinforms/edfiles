using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using OnlineActiveUsers;
using Shinetech.DAL;

public partial class Manager_Welcome : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    public int  OnlineUsers
    {
        get
        {
            //return OnlineUsersInstance.OnlineUsers.UsersCount;
            return General_Class.GetOnlineUserCount();
        }
    }
}
