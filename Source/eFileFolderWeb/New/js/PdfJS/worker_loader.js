/* -*- Mode: Java; tab-width: 2; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/* vim: set shiftwidth=2 tabstop=2 autoindent cindent expandtab: */
/* Copyright 2012 Mozilla Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 /* globals PDFJS, Util */

'use strict';

// List of shared files to include;
var sharedFiles = [
  '../New/js/PdfJS/util.js'
];

// List of other files to include;
var otherFiles = [
  '../New/js/PdfJS/core/network.js',
  '../New/js/PdfJS/core/chunked_stream.js',
  '../New/js/PdfJS/core/pdf_manager.js',
  '../New/js/PdfJS/core/core.js',
  '../New/js/PdfJS/core/obj.js',
  '../New/js/PdfJS/core/charsets.js',
  '../New/js/PdfJS/core/annotation.js',
  '../New/js/PdfJS/core/function.js',
  '../New/js/PdfJS/core/colorspace.js',
  '../New/js/PdfJS/core/crypto.js',
  '../New/js/PdfJS/core/pattern.js',
  '../New/js/PdfJS/core/evaluator.js',
  '../New/js/PdfJS/core/cmap.js',
  '../New/js/PdfJS/core/fonts.js',
  '../New/js/PdfJS/core/font_renderer.js',
  '../New/js/PdfJS/core/glyphlist.js',
  '../New/js/PdfJS/core/image.js',
  '../New/js/PdfJS/core/metrics.js',
  '../New/js/PdfJS/core/parser.js',
  '../New/js/PdfJS/core/ps_parser.js',
  '../New/js/PdfJS/core/stream.js',
  '../New/js/PdfJS/core/worker.js',
  '../New/js/PdfJS/core/arithmetic_decoder.js',
  '../New/js/PdfJS/core/jpg.js',
  '../New/js/PdfJS/core/jpx.js',
  '../New/js/PdfJS/core/jbig2.js',
  '../New/js/PdfJS/core/bidi.js',
  '../New/js/PdfJS/core/murmurhash3.js'
];

function loadInOrder(index, path, files) {
  if (index >= files.length) {
    PDFJS.fakeWorkerFilesLoadedCapability.resolve();
    return;
  }
  PDFJS.Util.loadScript(path + files[index],
                  loadInOrder.bind(null, ++index, path, files));
}

// Load all the files.
if (typeof PDFJS === 'undefined' || !PDFJS.fakeWorkerFilesLoadedCapability) {
  var files = sharedFiles.concat(otherFiles);
  for (var i = 0; i < files.length; i++) {
    importScripts(files[i]);
  }
} else {
  var src = PDFJS.workerSrc;
  var path = src.substr(0, src.indexOf('worker_loader.js'));
  // If Util is available, we assume that shared files are already loaded. Can
  // happen that they are not if PDF.js is bundled inside a special namespace.
  var skipShared = typeof Util !== 'undefined';
  var files = skipShared ? otherFiles : sharedFiles.concat(otherFiles);
  loadInOrder(0, path, files);
}

function importScripts(url) {
    // Adding the script tag to the head as suggested before
    var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;

    // Then bind the event to the callback function.
    // There are several events for cross browser compatibility.
    //script.onreadystatechange = callback;
    //script.onload = callback;

    // Fire the loading
    head.appendChild(script);
}