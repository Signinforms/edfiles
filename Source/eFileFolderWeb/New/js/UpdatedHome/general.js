$(window).on("load", function() {
	//Prevent Page Reload on all # links
	$("a[href='#']").click(function(e) {
		e.preventDefault();
	});


	
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		$(".call-link").click(function(e) {
			e.stopImmediatePropagation();
			var $this = $(this);
			if(!$this.hasClass('clicked')){
				e.preventDefault();
				$this.addClass('clicked');				
			}
		});
		$("html, body").click(function(e) {
			$(".call-link").removeClass('clicked');
		});
		$("window").scroll(function() {
			$(".call-link").removeClass('clicked');
		});
	}else{
		$(".call-link-mobile").removeAttr("href", "#");		
	}

	// On scroll header small
	$(window).scroll(function(e) {
		if($(window).scrollTop() > 300)
			$(".wrapper").addClass('small-header');
		else
			$(".wrapper").removeClass('small-header');
	});


	// FooterAdj
	function footerAdj(){
		var mt = $("#footer").innerHeight();
		$("#footer").css({"margin-top": -mt});
		$(".wrapper").css({"padding-bottom": mt});
	};
	footerAdj();
	$(window).resize(function() {
		setTimeout(function(){
			footerAdj();
		}, 300);
	});


	// MainNavigation
	$(".nav-icon").click(function(e){
		$("#mainNavigation").stop(true, true).slideToggle(300);
	});


	
	//fast click for touch devices  
	FastClick.attach(document.body);
	
	
	// Owl Carousal
	
	$('.casbo-carousel').owlCarousel({
		loop:true,
		margin:0,		
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			991:{
				items:3
			},
			1200:{
				items:4
			}
		},
		nav:true,
		navText:["",""]
	});

	$(".casbo-carousel .owl-item .item").css("height", $(".casbo-carousel .owl-stage").innerHeight());
	$(window).resize(function(event) {		
		setTimeout(function(){
			$(".casbo-carousel .owl-item .item").css("height", "auto")
			$(".casbo-carousel .owl-item .item").css("height", $(".casbo-carousel .owl-stage").innerHeight());		
		}, 500);
	});


	$(".steps").owlCarousel({
		nav:true,
		pagination:false,		
		responsive:{
			0:{items:2},			
			479:{items:3},
			768:{items:5},
			991:{items:6},
			1200:{items:8}
			
		}		
	});

	 //multipal modal open
	$('.modal').on('show.bs.modal', function () {
		if($(window).outerWidth() != $("body").innerWidth())
			$(".header-inner").css({"padding-right":parseInt($(".header-inner").css("padding-right")) + ($(window).outerWidth() - $("body").innerWidth())});		
    });

	$('.modal').on('hidden.bs.modal', function () {
		$(".header-inner, body").removeAttr('style');
		if ($('.modal:visible').length) { 
			$(".header-inner, body").css({"padding-right":$(window).outerWidth() - $("body").innerWidth()});					
			$('body').addClass('modal-open');
		};
	});

});