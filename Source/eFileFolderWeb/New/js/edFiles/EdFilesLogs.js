﻿$(document).ready(function () {
    //called when key is pressed in textbox
    $("#expirationTime").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if ((e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) || $("#expirationTime").val().length > 2) {
            $("#errorMsg").html("Expiration time should be only in digits and length of 3.").show().delay(2000).fadeOut(500);
            $("#errorMsg").css("color", "red");
            return false;
        }
    });
});

//Maintains logs
var ActionEnum = {
    Share: 1,
    Edit: 2,
    View: 3,
    Preview: 4,
    Delete: 5,
    Rename: 6,
    DocIndex: 7,
    Download: 8,
    BookView: 9,
    Create: 10,
    EditReminder: 11,
    Upload: 12,
    Submit: 13,
    Split: 14
};
var SubActionEnum = {
    FileNotFound: 1
};

function CreateDocumentsAndFolderLogs(folderId, dividerId, docId, logFormId, folderLogId, action, subAction, eFileFlowId, workAreaId, eFileFlowShareId, inboxId, archiveId) {
    $.ajax({
        type: "POST",
        url: '../EFileFolderJSONService.asmx/InsertDocumentsAndFolderLogs',
        contentType: "application/json; charset=utf-8",
        data: "{ folderId: '" + (folderId ? folderId : 0) + "',dividerId:'" + (dividerId ? dividerId : 0) + "',docId: '" + (docId ? docId : 0) + "', logFormId:'" + (logFormId ? logFormId : 0) + "', folderLogId:'" + (folderLogId ? folderLogId : 0) + "', eFileFlowId:'" + (eFileFlowId ? eFileFlowId : 0) + "', workAreaId:'" + (workAreaId ? workAreaId : 0) + "', action: '" + action + "', subAction: '" + (subAction ? subAction : 0) + "',eFileFlowShareId: '" + (eFileFlowShareId ? eFileFlowShareId : '') + "',eFileFlowId: '" + (eFileFlowId ? eFileFlowId : 0) + "',inboxId: '" + (inboxId ? inboxId : 0) + "',workAreaId: '" + (workAreaId ? workAreaId : 0) + "', type: '0', archiveId: '" + (archiveId ? archiveId : 0) + "', officeId: '', edFormsId: '0', edFormsUserId: '0'}",
        dataType: "json",
        success: function (data) {
            
        },
        error: function (result) {
            console.log('Failed' + result.responseText);
        }
    });
}

function CreateCasboLogs(fileID, action, custId, firstName, lastName) {
    $.ajax({
        type: "POST",
        url: './EFileFolderJSONService.asmx/InsertCasboLogs',
        contentType: "application/json; charset=utf-8",
        data: "{ fileId: " + (fileID ? fileID : 0) + ", custId: '" + custId + "', firstName: '" + firstName + "', lastName: '" + lastName + "', action: " + action + "}",
        dataType: "json",
        success: function (data) {
            var test = "test";
        },
        error: function (result) {
            console.log('Failed' + result.responseText);
        }
    });
}

function UpdateExpirationTime() {
    var expirationTime = $("#expirationTime").val();
    if (expirationTime == "0")
    {
        alert("An Expiration Time should be greater than 0.");
        return;
    }
    showLoader();
    $.ajax(
        {
            type: "POST",
            url: '../EFileFolderJSONService.asmx/UpdateExpirationTime',
            data: "{ expirationTime:'" + expirationTime + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                var value = data.d;
                hideLoader();
                if (value = false) {
                    alert("Failed to update expiration time at this time. Please try again later !!!")
                }
                else {
                    //SetSessionValue(expirationTime);
                    alert("Expiration time has been updated successfully.")
                }
            },
            fail: function (data) {
                alert("Failed to update expiration time at this time. Please try again later !!!")
                hideLoader();
            }
        });
}

function GetExpirationTime() {
    $.ajax(
        {
            type: "POST",
            url: '../EFileFolderJSONService.asmx/GetExpirationTime',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                $('#dialog-share-book input[name="linkexpiration"]').val(data.d.expirationTime);
            },
            fail: function (data) {
            }
        });
}

function GetExpirationValidation(uid) {
    $.ajax(
        {
            type: "POST",
            url: '../EFileFolderJSONService.asmx/GetExpirationTimeForValidation',
            data: "{ uid:'" + uid + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false,
            success: function (data) {
                $("#hdnExpirationTime").val(data.d.expirationTime);
            },
            fail: function (data) {
            }
        });
}

function SetSessionValue(expirationTime) {
    $("#expirationTime").val(expirationTime);
    $.ajax(
        {
            type: "POST",
            url: '../EFileFolderJSONService.asmx/SetSessionValueForExpirationTime',
            data: "{ expirationTime:'" + expirationTime + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
            },
            fail: function (data) {
            }
        });
}