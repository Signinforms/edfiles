$(window).load(function() {
	//Prevent Page Reload on all # links
	$("a[href='#']").click(function(e) {
		e.preventDefault();
	});


	
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		$(".call-link").click(function(e) {
			e.stopImmediatePropagation();
			var $this = $(this);
			if(!$this.hasClass('clicked')){
				e.preventDefault();
				$this.addClass('clicked');				
			}
		});
		$("html, body").click(function(e) {
			$(".call-link").removeClass('clicked');
		});
		$("window").scroll(function() {
			$(".call-link").removeClass('clicked');
		});
	}else{
		$(".call-link-mobile").removeAttr("href", "#");		
	}

	// On scroll header small
	$(window).scroll(function(e) {
		if($(window).scrollTop() > 300)
			$(".wrapper").addClass('small-header');
		else
			$(".wrapper").removeClass('small-header');
	});


	//// FooterAdj
	//function footerAdj(){
	//	var mt = $("#footer").innerHeight();
	//	$("#footer").css({"margin-top": -mt});
	//	$(".wrapper").css({"padding-bottom": mt});
	//};
	//footerAdj();
	//$(window).resize(function() {
	//	setTimeout(function(){
	//		footerAdj();
	//	}, 300);
	//});


	// MainNavigation
	$(".nav-icon").click(function(e){
		$("#mainNavigation").stop(true, true).slideToggle(300);
	});


	
	//fast click for touch devices  
	FastClick.attach(document.body);

});