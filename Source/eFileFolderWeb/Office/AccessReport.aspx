<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="AccessReport.aspx.cs" Inherits="AccessReport" Title="" %>

<%@ Import Namespace="System.Data" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register Src="../Controls/UserFind.ascx" TagName="UserFind" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>User Access Report </h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div>   <%--class="left-content"--%>
                    <div class="content-box listing-view">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="prevnext" style="float: right" bgcolor="#f8f8f5">
                                    <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                        CssClass="prevnext custom_select" OnPageSizeChanged="WebPager1_PageSizeChanged" PageSize="15" PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>
                                    <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                </div>
                                <div class="work-area">
                                    <div class="overflow-auto">
                                        <asp:GridView ID="GridView1" runat="server" DataKeyNames="AccessID" OnSorting="GridView1_Sorting"
                                            AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable datatable-small listing-datatable">
                                            <PagerSettings Visible="False" />
                                            <AlternatingRowStyle CssClass="odd" />
                                            <RowStyle CssClass="even" />
                                            <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                            <Columns>
                                                <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundField>
                                                <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname"></asp:BoundField>
                                                <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname"></asp:BoundField>

                                                <asp:BoundField DataField="AddressIP" HeaderText="Address IP" SortExpression="AddressIP"></asp:BoundField>

                                                <asp:BoundField DataField="WeekDay" HeaderText="WeekDay" SortExpression="WeekDay"></asp:BoundField>

                                                <asp:BoundField DataField="AccessDate" HeaderText="Access Date" DataFormatString="{0:MM.dd.yyyy hh:mm:ss}"
                                                    SortExpression="AccessDate" />
                                                <asp:BoundField DataField="LogoutDate" HeaderText="Logout Date" DataFormatString="{0:MM.dd.yyyy hh:mm:ss}"
                                                    SortExpression="LogoutDate" />
                                            </Columns>
                                        </asp:GridView>

                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged"></asp:AsyncPostBackTrigger>
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <%--<div class="right-content">
                    <div class="quick-find">
                        <h3>Quick Find</h3>
                        <p>Enter search term: (Name, First name, Last name)</p>
                        <div class="find-inputbox">
                            <asp:TextBox ID="txtKey" runat="server" MaxLength="50" CssClass="find-input" />
                            <asp:Button ID="btnSearch" class="quick-find-btn btn green" runat="server" CausesValidation="false" Text="Search" OnClick="btnSearch_Click" />
                        </div>
                    </div>
                </div>--%>
            </div>
        </div>
    </div>
</asp:Content>
