﻿using Shinetech.DAL;
using Shinetech.Engines;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ionic.Zip;
using System.IO.Compression;

public partial class Office_ArchiveDownloader : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
            string id = this.Request.QueryString["id"];
            int boxid;
            if (!string.IsNullOrEmpty(id) && int.TryParse(id, out boxid))
            {
                Common_Tatva.WriteDocumentsAndFoldersLog(Convert.ToInt32(id.ToString()), 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), "Entered in function");
                //文件夹打包
                FileFolder folder = new FileFolder(boxid);
                if (folder.IsExist)
                {
                    Shinetech.DAL.Account user = new Shinetech.DAL.Account(Sessions.SwitchedSessionId);
                    string basePath = this.MapPath(this.ArchiveBox);
                    if (!Directory.Exists(basePath))
                    {
                        Directory.CreateDirectory(basePath);
                    }

                    string userPath = Path.Combine(basePath, Page.User.Identity.Name);
                    if (!Directory.Exists(userPath))
                    {
                        Directory.CreateDirectory(userPath);
                    }

                    string folderPath = Path.Combine(userPath, folder.FolderName.Value + ".zip");

                    if (File.Exists(folderPath)) //删除以前的folder及数据
                    {
                        File.Delete(folderPath);
                    }

                    //Directory.CreateDirectory(folderPath); //创建目标文件夹
                    StringBuilder folderComment = new StringBuilder();
                    folderComment.AppendFormat("Folder Name : {0}\r\n", folder.FolderName.Value);
                    folderComment.AppendFormat("File Number : {0}\r\n", folder.FileNumber.Value);
                    folderComment.AppendFormat("Email  : {0}   ", folder.Email.Value);
                    folderComment.AppendFormat("Tel.   : {0}\r\n", folder.Tel.Value);
                    folderComment.AppendFormat("Alert 1: {0}\r\n", folder.Alert1.Value);
                    folderComment.AppendFormat("Alert 2: {0}\r\n", folder.Alert2.Value);
                    folderComment.AppendFormat("Comments: {0}\r\n", folder.Comments.Value);


                    //ZipFile zipfile = new ZipFile();
                    ZipStorer zip = ZipStorer.Create(folderPath, folderComment.ToString());

                    try
                    {

                        zip.EncodeUTF8 = true;

                        MemoryStream readme = new MemoryStream(
                            System.Text.Encoding.UTF8.GetBytes(folderComment.ToString()));

                        // Stores a new file directly from the stream                     
                        zip.AddStream(ZipStorer.Compression.Store, "readme.txt", readme, DateTime.Now, "Please read");

                        readme.Close();


                        DataTable dividers = FileFolderManagement.GetDividerByEffid(Convert.ToInt32(id));
                        DataTable documents = FileFolderManagement.GetDocumentByFolderId(id);

                        if (dividers != null && documents != null)
                        {
                            foreach (DataRow item in dividers.Rows)
                            {
                                #region 数据文件拷贝
                                string dividerText = (string)item["Name"];

                                //获取该divider下的所有文档并从原路径中拷贝过来
                                //在archives目录下创建该用户登录名的folder
                                foreach (DataRow docRow in documents.Rows)
                                {
                                    if (docRow["DividerID"].Equals(item["DividerID"]))
                                    {

                                        string docName = (string)docRow["Name"];
                                        string docPath = (string)docRow["PathName"];

                                        docPath = Path.Combine("~/", docPath);
                                        string dcoFullPath = this.MapPath(docPath);

                                        //文档编码后的名称,即保存在磁盘上的文档名称
                                        string encodeName1 = HttpUtility.UrlPathEncode(docName);
                                        encodeName1 = encodeName1.Replace("%", "$");
                                        if ((int)docRow["FileExtention"] == 14)
                                        {
                                            encodeName1 = encodeName1 + Path.GetExtension((string)docRow["FullFIleName"]);
                                        }
                                        else
                                            encodeName1 = encodeName1 + ".pdf";
                                        string docEncodeFullName = Path.Combine(dcoFullPath, encodeName1); // 其他的格式现在不管

                                        if (File.Exists(docEncodeFullName))
                                        {
                                            //dividerText为zip中的子目录名称
                                            string targetFileNameInZip = Path.Combine(dividerText, docName + Path.GetExtension(docEncodeFullName));
                                            //添加该文件到压缩文件中
                                            zip.AddFile(ZipStorer.Compression.Store, docEncodeFullName, targetFileNameInZip, "");
                                        }
                                    }
                                }
                                #endregion 数据文件拷贝
                            }
                        }


                        zip.Close();
                        // zip.Dispose();
                        zip = null;

                        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(id), 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), 0, 0);

                        Common_Tatva.WriteDocumentsAndFoldersLog(Convert.ToInt32(id), 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), "Downloaded");

                        //压缩打包成一个rar文件后，输出为stream到客户端
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.Expires = 0;
                        Response.Buffer = true;

                        Response.AddHeader("Content-disposition", "attachment; filename=\"" + Path.GetFileName(folderPath) + "\"");
                        Response.ContentType = "application/zip";
                        Response.WriteFile(folderPath);
                        //byte[] buffer = File.ReadAllBytes(folderPath);
                        //Response.OutputStream.Write(buffer, 0, buffer.Length);
                        Response.Flush();
                        Response.Close();
                        //Response.End();
                        HttpContext.Current.ApplicationInstance.CompleteRequest();

                        //完成后删除该Folder,l放到垃圾箱
                        // FileFolderManagement.RecycleFolderByID(id);
                    }
                    catch (Exception exp)
                    {
                        Common_Tatva.WriteDocumentsAndFoldersLog(Convert.ToInt32(id), 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), "File Not Found");
                        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(id), 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0);
                        Console.WriteLine(exp.Message);
                    }
                    finally
                    {
                        if (zip != null)
                        {
                            zip.Close();
                            zip = null;
                        }
                    }

                }
            }
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource_FolderBox"] as DataTable;
        }
        set
        {
            this.Session["DataSource_FolderBox"] = value;
        }
    }

    public DataTable DividerSource
    {
        get
        {
            return this.Session["DividerSource_FolderBox"] as DataTable;
        }
        set
        {
            this.Session["DividerSource_FolderBox"] = value;
        }
    }

    public string ArchiveBox
    {
        get
        {
            return "~/ArchiveBox";
        }

    }

    protected string GetFileExtention(StreamType ext)
    {
        string extname = ".pdf";
        switch (ext)
        {
            case StreamType.PDF:
                extname = ".pdf";
                break;
            case StreamType.Word:
                extname = ".doc";
                break;
            case StreamType.Excel:
                extname = ".xls";
                break;
            case StreamType.PPT:
                extname = ".ppt";
                break;
            case StreamType.RTF:
                extname = ".rtf";
                break;
            case StreamType.JPG:
                extname = ".jpg";
                break;
            case StreamType.PNG:
                extname = ".png";
                break;
            default:
                break;
        }

        return extname;
    }
}