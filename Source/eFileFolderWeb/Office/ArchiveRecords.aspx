﻿<%@ Page MasterPageFile="~/MasterPage.master" Language="C#" AutoEventWireup="true" CodeFile="ArchiveRecords.aspx.cs" Inherits="Office_ArchiveRecords" %>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript" defer="defer"></script>
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/style.css" rel="Stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=2" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>extras/jquery-ui-1.8.22.custom.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jstree.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/edFiles/EdFilesLogs.js?v=4" type="text/javascript"></script>
    <style type="text/css">
        .fileNameField
        {
            font-size: 15px;
            height: 30px;
            width: 250px;
            line-height: 5px;
            margin-left: 15px;
            margin-top: 5px;
            padding-left: 5px;
        }

        .btnEdit
        {
            text-align: center !important;
            height: 30px;
            width: 95px;
            background-color: #94ba33;
            color: white;
            font-weight: bold;
            font-size: 21px;
            margin-top: 5px;
            border: none;
            cursor: pointer;
        }

            .btnEdit:hover
            {
                background-color: #01487f;
            }

        .yellow
        {
            background-color: yellow;
        }

        .alignCenter
        {
            text-align: center;
            max-height: 135px;
            overflow-y: auto;
        }

        #comboBoxFolder_chosen
        {
            /*display: block;*/
            width: 315px;
            /*margin-top: -10px;*/
            text-align: left !important;
        }

        .chosen-results
        {
            max-height: 200px !important;
        }

        .left-content
        {
            width: 780px;
        }

        .searchedLbl
        {
            margin-bottom: 10px;
        }

        .searchTab
        {
            width: 100%;
            /*margin-left: 20px;*/
            margin-top: 10px;
            height: 34px;
            padding-right: 5px;
            padding: 5px;
            border: 2px solid #d4d4d4;
        }

        .collapseFolder
        {
            display: none;
            background-color: white;
            margin-top: -5px;
            position: absolute;
            z-index: 10000;
            left: 44%;
            border: none;
            border-bottom-right-radius: 100%;
            border-bottom-left-radius: 100%;
            background: url(../images/up-arrow-key.png) 2px -3px no-repeat white;
            height: 24px;
            width: 30px;
            border: solid 1px #94ba33;
            cursor: pointer;
            top: 100%;
        }

        .btnNav
        {
            border-radius: 4px;
            height: 25px;
            width: 25px;
            cursor: pointer;
            float: right;
            margin-top: 10px;
        }

        .lnkExpand
        {
            text-decoration: underline;
            font-size: 17px;
            color: #83af33;
            margin-left: 10px;
            margin-top: 10px;
            float: right;
            cursor: pointer;
        }

        .lnkCollapse
        {
            text-decoration: underline;
            font-size: 17px;
            color: #01487f;
            margin-left: 10px;
            margin-top: 10px;
            float: right;
            cursor: pointer;
            display: none;
        }

        .btnPrevEnabled
        {
            background: url(../images/prev_green.png) 2px 3px no-repeat white;
            border: solid 1px #83af33;
        }

        .btnNextEnabled
        {
            margin-left: 10px;
            background: url(../images/next_green.png) 4px 3px no-repeat white;
            border: solid 1px #83af33;
        }

        .btnPrevDisabled
        {
            background: url(../images/prev_blue.png) 2px 3px no-repeat white;
            border: solid 1px #01487f;
            cursor: not-allowed;
            opacity: 0.2;
        }

        .btnNextDisabled
        {
            margin-left: 10px;
            background: url(../images/next_blue.png) 4px 3px no-repeat white;
            border: solid 1px #01487f;
            cursor: not-allowed;
            opacity: 0.2;
        }

        label
        {
            font-size: 14px;
            display: inline-block;
        }

        .selectBox-outer
        {
            position: relative;
            display: inline-block;
        }

        .selectBox select
        {
            width: 100%;
        }

        .overSelect
        {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            font-size: 15px;
        }

        .chosen-container
        {
            font-size: 15px;
        }

        #folderList
        {
            width: 100%;
            display: none;
            border: 1px #dadada solid;
            position: absolute;
            left: 0;
            top: 100%;
            z-index: 9;
            background: #fff;
            max-height: 235px;
            overflow-y: auto;
        }

            #folderList label
            {
                display: block;
                padding: 0 5px;
                width: auto;
                min-width: initial;
                cursor: pointer;
                margin-top: 3px;
            }

                #folderList label:hover
                {
                    background-color: #e5e5e5;
                }

            #folderList input
            {
                display: inline-block;
            }

        /*.chosen-container .chosen-choices
        {
            max-height: 100px;
            overflow: auto;
        }*/

        .textBox
        {
            /*margin-left: 5%;*/
            padding-left: 5px;
        }

        .span
        {
            font-weight: normal !important;
        }



        td
        {
            max-width: 300px;
        }



        .datatable td
        {
            font-family: "Open Sans", sans-serif;
        }

        .register-container .form-containt .content-title
        {
            margin-bottom: 5px;
        }

        #btnCheckedAll
        {
            float: right;
            margin-right: 3px;
            margin-top: 9px;
            height: 18px;
            width: 18px;
        }

        .selectalltab
        {
            font-size: 18px;
            float: right;
            padding: 5px;
            font-family: 'Open Sans', sans-serif !important;
            color: #fff;
            font-weight: 700;
            margin-right: 5px;
        }

        .ajax__tab_body
        {
            height: auto!important;
        }

        .white_content
        {
            display: none;
            position: fixed;
            top: 20%;
            left: 20%;
            width: 60%;
            height: 50%;
            padding: 16px;
            background-color: white;
            z-index: 999999999;
            overflow: auto;
        }

        .datatable td
        {
            font-family: "Open Sans", sans-serif;
        }

        #overlay
        {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
            position: fixed;
        }

        .preview
        {
            position: relative;
            left: -10px;
        }

        td span
        {
            -ms-word-break: break-all;
            word-break: break-all;
            word-break: break-word;
            -webkit-hyphens: auto;
            -moz-hyphens: auto;
            hyphens: auto;
            max-width: 300px;
            white-space: normal!important;
        }

        /*td {
            max-width: 200px;
        }*/

        .actionMinWidth
        {
            min-width: 50px;
        }

        .chkpending, .chkinbox
        {
            margin-left: 10px;
        }

        .chkpendingAll, .chkinboxAll
        {
            margin-left: 10px;
        }

        .boxtitle
        {
            margin-top: 20px !important;
        }

        .file
        {
            background: url(../lib/TreeView/file_sprite.png) 0 0 no-repeat !important;
            width: 18px !important;
            height: 18px !important;
            margin-top: 4px!important;
            margin-left: 2px!important;
            margin-right: 5px!important;
        }


        /*16-09-2016*/
        .create-btn
        {
            background-image: url(../images/create-folder.png);
            cursor: pointer;
        }

        .rename-btn
        {
            background-image: url(../images/rename.png);
            cursor: pointer;
        }

        .delete-btn
        {
            background-image: url(../images/delete.png);
            cursor: pointer;
            opacity: 0.5;
        }

        .cancel-btn
        {
            background-image: url(../images/cancel-btn.png);
            cursor: pointer;
        }

        .send-btn
        {
            background-image: url(../images/send.png);
            cursor: pointer;
        }

        .preview-btn
        {
            cursor: pointer;
        }

        .register-container .form-containt .content-title
        {
            margin-bottom: 5px;
        }

        #popupclose,
        .popupclose,
        #popupclose_thumbnail
        {
            float: right;
            padding: 10px;
            cursor: pointer;
        }

        .popupcontent
        {
            padding: 10px;
        }

        .form-containt .content-box fieldset
        {
            font-size: initial;
        }

        .ajax__fileupload_dropzone
        {
            font-size: 15px;
            line-height: 120px !important;
            height: 120px !important;
        }

        .lnkbtnPreviewInbox,
        .lnkbtnPreviewPending
        {
            margin-left: -10px;
        }

        .downloadFile
        {
            margin-left: -10px;
            cursor: pointer;
        }

        .preview-popup
        {
            width: 100%;
            max-width: 1100px;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 10%;
            right: 0;
            margin: auto;
            background: #fff;
            height: 85%;
        }

        .preview-popup-thumbnails
        {
            width: 100%;
            max-width: 1200px;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 10%;
            right: 0;
            margin: auto;
            background: #fff;
            height: 85%;
        }

        #floatingCirclesG
        {
            top: 50%;
            left: 50%;
            position: fixed;
        }

        .listing-contant .quick-find
        {
            background: #fff;
            padding: 0px 0px 0px 0px;
            width: 235px;
            margin-top: 0px;
        }

        .quick-find .find-inputbox .quick-find-btn
        {
            top: 3px;
            height: 30px;
            width: 30px;
        }

        .quick-find .find-inputbox .find-input
        {
            height: 35px;
            padding-right: 5px;
        }

        .shareDocument
        {
            cursor: pointer;
        }

        .ui-dialog
        {
            top: 15% !important;
            position: fixed;
        }

        .ic-save, .ic-share
        {
            cursor: pointer;
        }

        .selectedTreeNodeLabel
        {
            display: inline-block;
            font-size: 14px;
            margin-top: 25px;
        }

        #uploadProgress
        {
            font-size: 14px;
        }

        .workareatable
        {
            width: 80% !important;
            float: right;
            border-width: 1px !important;
        }

        .chosen-container-single .chosen-single
        {
            line-height: 33px !important;
        }

        .chosen-single
        {
            height: 35px !important;
        }

        .chkSearch
        {
            font-size: 14px;
            vertical-align: super;
        }

        .rename-block
        {
            float: inherit !important;
        }

        input[type="radio"]
        {
            height: 18px;
            width: 18px;
            vertical-align: middle;
        }

        .chkSearch > label
        {
            vertical-align: sub;
        }

        .btnSearch
        {
            /*width: 39px !important;
            height: 29px !important;
            font-size: 0;
            background-image: url(../images/quickfind-bg.png) !important;
            background-repeat: no-repeat !important;
            background-position: center center !important;
            border-radius: 0 !important;
            -webkit-border-radius: 0 !important;
            top: 7px !important;
            position: relative !important;
            border: none !important;
            background-color: transparent !important;
            */
            /* top: 3px; */
            cursor: pointer !important;
            text-align: center !important;
            height: 33px;
            width: 88px;
            background-color: #94ba33;
            color: white;
            font-weight: bold;
            font-size: 21px;
            margin-top: 5px;
        }

        .prevnext > table > tbody > tr > td > select
        {
            height: 25px;
            margin-top: 7px;
        }

        #circularG
        {
            position: absolute;
            top: 75px;
            left: 142px;
            width: 25px;
            height: 25px;
            margin: auto;
        }

        #comboboxDividerSplit_chosen
        {
            margin-top: 5px;
            display: block;
        }

        #comboboxSplit_chosen
        {
            display: block;
            margin-top: 5px;
        }

        .selectFolderlbl
        {
            font-weight: bold;
            /*margin-left: -17%;*/
        }

        .btnNextPreview
        {
            padding: 8px 10px 8px 35px;
            font-size: 15px;
            padding-right: 10px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 5px;
            background-image: url(../images/left-arrow.png);
            background-position: 7px center;
            background-repeat: no-repeat;
            float: left;
        }

        .btnPrevPreview
        {
            padding: 8px 30px 8px 10px;
            font-size: 15px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 5px;
            background-image: url(../images/right-arrow.png);
            background-position: 50px center;
            background-repeat: no-repeat;
            float: right;
        }
    </style>
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <asp:HiddenField ID="hdnSharePath" runat="server" />
    <asp:HiddenField ID="hdnSelectedPath" runat="server" />
    <asp:HiddenField ID="hdnSearchedId" runat="server" />
    <asp:HiddenField ID="hdnIsSearched" runat="server" />
    <asp:HiddenField ID="hdnFolderId" runat="server" />
    <asp:HiddenField runat="server" ID="hdnFileName" />
    <asp:HiddenField runat="server" ID="hdnFileNameOnly" />
    <asp:HiddenField runat="server" ID="hdnFileId" />
    <div class="popup-mainbox preview-popup" id="preview_popup" style="display: none">
        <div class="popupcontrols" style="width: 100%; height: 5%;">
            <span id="popupclose" class="ic-icon ic-close"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%;">
            <table style="width: 100%; height: 100%">
                <tr style="height: 5%">
                    <td style="text-align: center;">
                        <input type="button" id="btnPrevious" runat="server" value="PREVIOUS" onclick="ShowPrevious();" style="margin-bottom: 10px;" class="btn green btnNextPreview" title="PREVIOUS" />
                        <div style="display: inline-flex; margin-bottom: 20px;">
                            <asp:Label ID="editableFile" runat="server" Style="font-size: 20px; padding-top: 10px;">Filename:</asp:Label>
                            <asp:Label ID="lblFileName" runat="server" Style="font-size: 15px; margin: 14px; margin-bottom: 0;"></asp:Label>
                            <asp:TextBox ID="fileNameFld" CssClass="fileNameField" runat="server" Style="display: none;"></asp:TextBox>
                            <asp:Button ID="btnUpdate" CssClass="quick-find-btn btnEdit" OnClientClick="return makeEditable();" Style="display: inline-block" runat="server" Text="Rename" />
                            <asp:Button ID="btnSave" OnClientClick="return CheckFileValidation();" CssClass="quick-find-btn btnEdit" OnClick="btnSave_Click" Style="display: none; margin-left: 20px;" runat="server" Text="Save" />
                            <asp:Button ID="btnCancel" CssClass="quick-find-btn btnEdit" OnClientClick="return makeReadOnly();" Style="display: none; margin-left: 10px;" runat="server" Text="Cancel" />
                        </div>
                        <input type="button" id="btnNextClick" runat="server" value="NEXT" onclick="ShowNext();" style="margin-bottom: 10px; float: right" class="btn green btnPrevPreview" title="NEXT" />
                        <%--  <input type="button" id="btnSpltBtn" runat="server" value="Tab" onclick="ShowAddToTabPopup();" style="margin-bottom: 10px;" class="btn green btnAddtoTab" title="Add To Tab" />
                                        <input type="button" id="btnSplitAddToWA" runat="server" value="WorkArea" onclick="ShowAddtoWAPopup();" style="margin-bottom: 10px;" class="btn green btnAddtoWorkArea" title="Add To WorkArea" />
                        <div class="selecttabcheck"><span class="selectalltab">Select All</span><input type="checkbox" id="btnCheckedAll" value="Select All" onclick="checkAllThumbnail();" /></div>--%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <iframe id="reviewContent" style="width: 100%; height: 100%;"></iframe>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="dialog-share-book" title="Share this Inbox Document" style="display: none">
        <p class="validateTips">All form fields are required.</p>
        <br />
        <%-- <br />--%>
        <label for="email">From Email</label><br />
        <input type="email" name="email" id="email" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <%--<label for="myname" style="display:none;">My Name</label><br />
                    <input type="text" name="myname" id="myname" class="text ui-widget-content ui-corner-all" style="border: 1px solid black;display:none" /><br />--%>
        <label for="emailto">To Email</label><br />
        <input type="email" name="emailto" id="emailto" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <label for="nameto">To Name</label><br />
        <input type="text" name="nameto" id="nameto" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <label for="title">Subject</label><br />
        <input type="text" name="title" id="title" value="Archive share" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" readonly="readonly" /><br />
        <label for="content">Message</label><br />
        <textarea name="content" id="content" rows="2" cols="25" class="text ui-widget-content ui-corner-all" style="border: 1px solid black"></textarea><br />
        <label for="content">Link Expiration Time (In Hrs)</label><br />
        <input type="text" name="linkexpiration" id="expirationTime" class="text ui-widget-content ui-corner-all" style="border: 1px solid black; width: 80%;" />
        <input type="button" id="updateExpiration" onclick="UpdateExpirationTime()" class="btn-small green" style="float: right" value="UPDATE" /><br />
        <span id="errorMsg"></span>
    </div>
    <div id="dialog-message-all" title="" style="display: none;">
        <p id="dialog-message-content">
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span>
            <span id="spanText"></span>
        </p>
    </div>
    <%--End Share Document--%>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Archive</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <asp:UpdatePanel runat="server" ID="updatePanle1" DefaultButton="btnArchiveSearch">
                <ContentTemplate>
                    <div class="form-containt listing-contant modify-contant">
                        <div id="workareatreeview" class="content-box listing-view" style="padding-top: 3%; text-align: center;">
                            <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <label style="font-weight: 700">Select Any Folder</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="comboBoxFolder">
                                            <%--<option>Select Folder to upload LogForm</option>--%>
                                        </select>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnArchiveSearch" OnClick="btnArchiveSearch_Click" imagealign="AbsMiddle" CssClass="quick-find-btn btnSearch" Text="Search" runat="server" TabIndex="3" Style="float: right" />
                                    </td>
                                </tr>
                            </table>

                            <div class="table-scroll">
                                <asp:GridView ID="gridView" runat="server" OnSorting="gridView_Sorting"
                                    AllowSorting="True" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover datatable listing-datatable" Width="100%" Style="margin-top: 20px;">
                                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                    <AlternatingRowStyle CssClass="odd" />
                                    <RowStyle CssClass="even" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:BoundField DataField="FileName" HeaderText="File Name" SortExpression="FileName" ItemStyle-Width="40%" />
                                        <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" SortExpression="CreatedDate" ItemStyle-Width="25%" />
                                        <asp:BoundField DataField="Size" HeaderText="Size" SortExpression="Size" ItemStyle-Width="10%" />
                                        <asp:TemplateField ShowHeader="true">
                                            <HeaderTemplate>
                                                <asp:Label ID="lblMoveOrder" runat="server" Text="Preview|Download|Share" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-preview" CausesValidation="False" ToolTip="Preview"
                                                    CommandName="" OnClientClick="ShowPreviewForArchive(this, false);return false;"></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton21" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" ToolTip="Download"
                                                    OnClientClick="DownloadWADoc(this);" />
                                                <asp:Button ID="btnWorkAreaShare" runat="server" CssClass="icon-btn ic-icon ic-share lnkBtnShare" Style="border: none; background-color: white; margin-left: 0px; float: none" ToolTip="Share File" OnClientClick="OnShare(this)" />
                                                <asp:HiddenField runat="server" ID="hdnArchiveId" Value='<%# Eval("ArchiveId")%>' />
                                                <asp:HiddenField runat="server" ID="hdnPathName" Value='<%# Eval("PathName")%>' />
                                                <asp:HiddenField runat="server" ID="hdnFileName" Value='<%#Eval("FileName")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label3" runat="server" CssClass="Empty" />There is no record for searched keyword.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>

                            <div class="table-toolbar">
                                <%--<asp:Button OnClick="backFromLetters_Click" CommandName="Back" CssClass="backBtn" ToolTip="Back to previous page" runat="server" ID="backFromLetters"></asp:Button>--%>
                                <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" PageSize="10" CssClass="opt-select" PagerStyle="NumericPages" ControlToPaginate="gridView" Style="height: 50px;"></cc1:WebPager>

                            </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </div>
    <script type="text/javascript">
        var ids = "", selectedButton = "", selectedPath = "", selectedChildNode = "", modalOpenedFrom = "";
        var userId = '<%= Sessions.SwitchedRackspaceId%>';
        var min = 0;
        var max = 0; var i = 0;
        //function GetSearchDocs() {

        //    showLoader();
        //    return true;
        //}

        function makeEditable() {
            $("#<%= lblFileName.ClientID %>").hide();
            $("#<%= btnUpdate.ClientID %>").hide();
            $("#<%= fileNameFld.ClientID %>").show();
            $("#<%= btnSave.ClientID %>").show();
            $("#<%= btnCancel.ClientID %>").show();
            $("#<%= fileNameFld.ClientID %>").val($("#<%= hdnFileNameOnly.ClientID %>").val());
            return false;
        }

        function makeReadOnly() {
            $("#<%= lblFileName.ClientID %>").show();
            $("#<%= btnUpdate.ClientID %>").show();
            $("#<%= fileNameFld.ClientID %>").hide();
            $("#<%= btnSave.ClientID %>").hide();
            $("#<%= btnCancel.ClientID %>").hide();
            return false;
        }

        function CheckFileValidation() {
            if ($("#<%= fileNameFld.ClientID %>").val() == "" || $("#<%= fileNameFld.ClientID %>").val() == undefined) {
                alert("Please enter valid file name");
                return false;
            }
            showLoader();
            return true;
        }

        function pageLoad() {
            LoadFolders();
            $('.listing-datatable').find('tr').each(function () {

                $(this).attr('data-index', i);
                i++;
            })
            max = i;
            i = 0;
            var closePopup = document.getElementById("popupclose");
            closePopup.onclick = function () {
                var popup = document.getElementById("preview_popup");
                var overlay = document.getElementById("overlay");
                overlay.style.display = 'none';
                popup.style.display = 'none';
                enableScrollbar();
                $('#reviewContent').removeAttr('src');
            };

            $('#dialog-share-book').dialog({
                autoOpen: false,
                width: 450,
                //height: 540,
                top: 300,
                resizable: false,
                modal: true,
                buttons: {
                    "Close": function () {
                        $(this).dialog("close");
                    },
                    "Share Now": function () {
                        SaveMail();
                    }
                },
                close: function () {
                }
            });

            $('.ui-dialog-buttonset').find('button').each(function () {
                $(this).find('span').text($(this).attr('text'));
            })

            $("#dialog-message-all").dialog({
                autoOpen: false,
                resizable: false,
                modal: true,
                Ok: function () {
                    $(this).dialog("close");
                },
                close: function (event, ui) {
                    $(this).dialog("option", 'title', '');
                    $('#dialog-confirm-content').empty();
                }

            });
            window.CheckValidation = function CheckValidation(fieldObj) {
                if (fieldObj.val() == "") {
                    fieldObj.css('border', '1px solid red');
                }
                else {
                    fieldObj.css('border', '1px solid black');
                }
            }

            window.CheckEmailValidation = function CheckEmailValidation(fieldObj) {
                var x = fieldObj.val();
                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(x)) {
                    fieldObj.css('border', '1px solid black');
                    return true;
                }
                else {
                    fieldObj.css('border', '1px solid red');

                    return false;
                }
            }

            function SaveMail() {
                var email = $('#dialog-share-book input[name="email"]').val();
                //var name = $('#dialog-share-book input[name="myname"]').val();
                var emailto = $('#dialog-share-book input[name="emailto"]').val();
                var nameto = $('#dialog-share-book input[name="nameto"]').val();

                var title = $('#dialog-share-book input[name="title"]').val();
                var content = $('#dialog-share-book textarea[name="content"]').val();
                var Isvalid = false;

                //$('#dialog-share-book input[name="myname"]').keyup(function () { CheckValidation($('#dialog-share-book input[name="myname"]')) });
                //$('#dialog-share-book input[name="email"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="email"]')) });

                $('#dialog-share-book input[name="emailto"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="emailto"]')) });

                //if (email == "" || name == "" || emailto == "") {
                if (emailto == "") {
                    //CheckValidation($('#dialog-share-book input[name="email"]'));
                    //CheckValidation($('#dialog-share-book input[name="myname"]'));
                    CheckValidation($('#dialog-share-book input[name="emailto"]'));
                    //CheckEmailValidation($('#dialog-share-book input[name="email"]'));
                    CheckEmailValidation($('#dialog-share-book input[name="emailto"]'))
                    return false;
                }

                //if (!Isvalid) {
                //    Isvalid = CheckEmailValidation($('#dialog-share-book input[name="email"]'));
                //    if (!Isvalid)
                //        return Isvalid;
                //}

                Isvalid = CheckEmailValidation($('#dialog-share-book input[name="emailto"]'));
                if (!Isvalid)
                { return Isvalid; }

                var src = '';
                showLoader();

                var path1 = $("#" + '<%= hdnSelectedPath.ClientID%>').val();
                var archiveId = $("#" + '<%= hdnSearchedId.ClientID%>').val();
                //$('#hdnWorkArea').children().each(function () {
                //    if ($(this).val() == filename) {
                //        workAreaId = $(this).attr("id");
                //    }
                //});
                var URL = PrepareURLForArchive(null);
                $.ajax(
                   {

                       type: "POST",
                       url: '../EFileFolderJSONService.asmx/ShareMailForArchive',
                       //data: "{ strSendMail:'" + email + "', strToMail:'" + emailto + "', documentId:'" + documentId + "', strMailInfor:'" + content + "',SendName:'" + name + "', ToName:'" + nameto + "',MailTitle:'" + title + "',Path:'" + src + "'}",
                       data: "{strToMail:'" + emailto + "',MailTitle:'" + title + "',ToName:'" + nameto + "',SendName:'" + "" + "',strSendMail:'" + email + "',strMailInfor:'" + content + "',URL:'" + URL + "',archiveId: '" + (archiveId == undefined || 0 ? 0 : archiveId) + "'}",
                       contentType: "application/json; charset=utf-8",
                       dataType: "json",
                       success: function (data) {
                           var id = data.d;
                           var title, message;
                           if (id === 0) {
                               title = "Sharing Archive URL error";
                               message = "This URL has not been shared successfully."
                           }
                           else {
                               title = "Sharing Archive URL success";
                               message = "This URL has been shared successfully."
                           }

                           $('#dialog-message-all').dialog({
                               title: title,
                               open: function () {
                                   $("#spanText").text(message);
                               }
                           });

                           $('#dialog-message-all').dialog("open");

                           if (id != null || id != "")
                               $('#dialog-share-book').dialog("close");
                           hideLoader();
                           modalOpenedFrom = "";
                       },
                       fail: function (data) {
                           bookshelf.loaded.apply();

                           $('#dialog-message-all').dialog({
                               title: "Sharing WorkArea URL error",
                               open: function () {
                                   $("#spanText").text("This URL has not been shared successfully.");
                               }
                           });
                           $('#dialog-message-all').dialog("open");

                           hideLoader();
                           modalOpenedFrom = "";
                       }
                   });

            }

            $("#comboBoxFolder").change(function () {
                //$('.search-choice span').click(function () {
                //    $('.collapseFolder').css("display", "block");
                //})
                //$('.search-choice-close').click(function () {
                //    if ($('.chosen-choices').children().length > 2)
                //        $('.collapseFolder').css("display", "none");
                //})
                $('#<%= hdnFolderId.ClientID%>').val($("#comboBoxFolder").chosen().val());
                SearchArchiveTree($('#<%= hdnFolderId.ClientID%>').val());
                //$("#comboBoxFolder").chosen().val($('#<%= hdnFolderId.ClientID%>').val);
            })
        };

        function ShowPrevious() {
            //makeReadOnly();
            $("#overlay").css("z-index", "100002");
            var ci = $('.listing-datatable').find('tr.activetr').attr('data-index');
            ci = parseInt(ci);
            var ni = 0;
            if (ci < max & ci > min + 1) {
                ni = ci - 1;
            }
            else {
                ni = max - 1;
            }

            $('.listing-datatable').find('tr').each(function () {
                if ($(this).attr('data-index') == ni) {
                    $(this).find('a.ic-preview').click();
                }
            })
        }

        function ShowNext() {
            //makeReadOnly();
            $("#overlay").css("z-index", "100002");
            var ci = $('.listing-datatable').find('tr.activetr').attr('data-index');
            ci = parseInt(ci);
            var ni = 0;
            if (ci < max - 1 & ci > min) {
                ni = ci + 1;
            }
            else {
                ni = min + 1;
            }

            $('.listing-datatable').find('tr').each(function () {
                if ($(this).attr('data-index') == ni) {
                    $(this).find('a.ic-preview').click();
                }
            })
        }

        function collapseFolder() {
            pubExpanded = false;
            $('.chosen-drop').css("display", "none");
            $('.collapseFolder').css("display", "none");
        }

        function showLoader() {
            $('#overlay').show();
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
        }

        function navigatePrev(event) {
            var id = parseInt($(event).siblings().first().find('label:visible').attr("id"), 10);
            if ($(event).siblings().find('#' + parseInt(id - 1)).length > 0) {
                $(event).siblings().first().find('label:visible').hide();
                $(event).siblings().find('#' + parseInt(id - 1)).delay(500).show();
                if ($(event).siblings().find('#' + parseInt(id - 2)).length == 0) {
                    $(event).removeClass("btnPrevEnabled");
                    $(event).addClass("btnPrevDisabled");
                }
                else {
                    $(event).addClass("btnPrevEnabled");
                    $(event).removeClass("btnPrevDisabled");
                }
                if ($(event).siblings().find('#' + parseInt(id)).length > 0) {
                    $(event).siblings().parent().find('.btnNextDisabled').addClass("btnNextEnabled");
                    $(event).siblings().parent().find('.btnNextEnabled').removeClass("btnNextDisabled");
                }
            }
            return false;
        }

        function navigateNext(event) {
            var id = parseInt($(event).siblings().first().find('label:visible').attr("id"), 10);
            if ($(event).siblings().find('#' + parseInt(id + 1)).length > 0) {
                $(event).siblings().first().find('label:visible').hide();
                $(event).siblings().find('#' + parseInt(id + 1)).delay(500).show();
                if ($(event).siblings().find('#' + parseInt(id + 2)).length == 0) {
                    $(event).removeClass("btnNextEnabled");
                    $(event).addClass("btnNextDisabled");
                }
                else {
                    $(event).addClass("btnNextEnabled");
                    $(event).removeClass("btnNextDisabled");
                }
                if ($(event).siblings().find('#' + parseInt(id)).length > 0) {
                    $(event).siblings().parent().find('.btnPrevDisabled').addClass("btnPrevEnabled");
                    $(event).siblings().parent().find('.btnPrevEnabled').removeClass("btnPrevDisabled");
                }
            }
            return false;
        }

        function LoadFolders() {
            showLoader();
            $("#comboBoxFolder").html('');
            $("#comboBoxFolder").chosen("destroy");
            $.ajax(
             {
                 type: "POST",
                 url: '../FlexWebService.asmx/GetArchiveFolders',
                 contentType: "application/json; charset=utf-8",
                 data: '{"officeId":"' + userId + '"}',
                 dataType: "json",
                 success: function (result) {
                     if (result.d) {
                         $.each(result.d.Item1, function (i, text) {
                             $('<option />', { value: i, text: text }).appendTo($("#comboBoxFolder"));
                         });
                     }

                     $("#comboBoxFolder").chosen({ width: "270px" });
                     if ($("#" + '<%= hdnFolderId.ClientID%>').val() != "") {
                         $("#comboBoxFolder").val($("#" + '<%= hdnFolderId.ClientID%>').val()).trigger("chosen:updated");
                     }
                     $("#" + '<%= hdnFolderId.ClientID%>').val($("#comboBoxFolder").val());
                     $.each(result.d.Item2, function (i, text) {
                         if (i == $("#" + '<%= hdnFolderId.ClientID%>').val())
                             $("#" + '<%= hdnSharePath.ClientID%>').val(text);
                     });
                     //SearchArchiveTree();
                     hideLoader();
                 },
                 fail: function (data) {
                     alert("Failed to get Folders. Please try again!");
                     hideLoader();
                 }
             });
         }

         function SearchArchiveTree(folderId) {
             if (folderId != 0)
                 folderId = $('#<%= hdnFolderId.ClientID%>').val();
             $.ajax(
                  {
                      type: "POST",
                      url: 'ArchiveRecords.aspx/LoadArchives',
                      contentType: "application/json; charset=utf-8",
                      data: '{"folderId":"' + folderId + '","uid":"' + '<%= Sessions.SwitchedRackspaceId%>' + '"}',
                      dataType: "json",
                      success: function (result) {
                          //hideLoader();
                          __doPostBack('', 'refreshGrid@');
                      },
                      fail: function (data) {
                          alert("Failed to get Documents. Please try again!");
                          hideLoader();
                      }
                  });
              }

              function ShowPreviewForArchive(obj, isSearched) {
                  $("#<%= hdnIsSearched.ClientID %>").val(isSearched);
            if (obj != null) {
                $('.listing-datatable tr').removeClass('activetr');
                $(obj).closest('tr').addClass('activetr');
                $('#<%= hdnFileNameOnly.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnFileName]").val());
                      $('#<%= hdnSelectedPath.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnPathName]").val());
                      $('#<%= hdnSearchedId.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnArchiveId]").val());
                  }
                  showLoader();
                  $("#<%= lblFileName.ClientID %>").html($('#<%= hdnFileNameOnly.ClientID%>').val());
                   var folderID = <%= (int)Enum_Tatva.Folders.Archive %>

                            $.ajax({
                                type: "POST",
                                url: '../Office/ArchiveSearch.aspx/GetPreviewURL',
                                contentType: "application/json; charset=utf-8",
                                data: "{ archiveID:'" + $('#<%= hdnSearchedId.ClientID%>').val() + "', folderID:'" + folderID + "', shareID:'" + $('#<%= hdnSelectedPath.ClientID%>').val() + "', isSplit:'" + false + "'}",
                                dataType: "json",
                                success: function (data) {
                                    $("#overlay").css("z-index", "9999");
                                    if (data.d == "") {
                                        hideLoader();
                                        alert("Failed to get thumbnails!");
                                        return false;
                                    }
                                    else {
                                        if (data.d.length > 1) {
                                            $('.thumbnailsPart').show();
                                            $("#thumnailImages").html("");
                                            showPreview(data.d[0]);
                                            $("#" + '<%= hdnFileName.ClientID %>').val(data.d[0]);
                                     $("#" + '<%= hdnFileId.ClientID %>').val(data.d[2]);
                                     //$('.thumbnailsPart').hide();
                                     //$("#thumnailImages").html("");
                                     $('#preview_popup').css("max-width", "1300px");
                                 }
                             }
                             var ci = $(obj).closest('tr').attr('data-index');
                             if (ci == max - 1) {
                                 $(".btnPrevPreview").hide();
                             }
                             else {
                                 $(".btnPrevPreview").show();
                             }
                             if (ci == min + 1) {
                                 $(".btnNextPreview").hide();
                             }
                             else {
                                 $(".btnNextPreview").show();

                             }
                         },
                                error: function (result) {
                                    $("#overlay").css("z-index", "9999");
                                    console.log('Failed' + result.responseText);
                                    hideLoader();
                                }
                            });
                     return false;
                 }
                 function showPreview(URL) {
                     // URL = URL.substr(1);
                     hideLoader();
                     if (!window.location.origin) {
                         window.location.origin = window.location.protocol + "//"
                           + window.location.hostname
                           + (window.location.port ? ':' + window.location.port : '');
                     }


                     //var src = 'http://docs.google.com/gview?url=' + window.location.origin + '/' + URL.trim() + '&embedded=true';
                     //if (window.location.origin.indexOf('https://') > -1)
                     //    src = 'https://docs.google.com/gview?url=' + window.location.origin + '/' + URL.trim() + '&embedded=true';
                     //URL = URL.replace(" ", "%20");
                     src = './Preview.aspx?data=' + window.location.origin + URL.trim() + "?v=" + "<%= DateTime.Now.Ticks%>";
                     var popup = document.getElementById("preview_popup");
                     var overlay = document.getElementById("overlay");
                     $('#reviewContent').attr('src', src);
                     overlay.style.display = 'block';
                     popup.style.display = 'block';
                     $('#preview_popup').focus();
                     disableScrollbar();
                     return false;
                 }
                 function DownloadWADoc(path) {
                     window.open(PrepareURLForArchive(path), '_blank');
                 }

                 function DisplayAddToTabModal(e) {
                     e.preventDefault();
                     $('#AddToTab_popup,#overlay').show().focus();
                     $('#combobox').trigger('chosen:open');
                     $(<%= hdnSelectedPath.ClientID%>).val($(e.currentTarget).closest('tr').find("input[type='hidden'][id$=hdnPathName]").val());
                      e.stopPropagation();
                      disableScrollbar();
                  }

                  function PrepareURLForArchive(obj) {
                      if (!window.location.origin) {
                          window.location.origin = window.location.protocol + "//"
                            + window.location.hostname
                            + (window.location.port ? ':' + window.location.port : '');
                      }
                      var hdnId;
                      if (obj != null && obj != undefined) {
                          $('#<%= hdnSelectedPath.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnPathName]").val());
                          $('#<%= hdnSearchedId.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnArchiveId]").val());

                          //var hdnObj = $('#<%= hdnSelectedPath.ClientID%>').val();
                          hdnId = $('#<%= hdnSearchedId.ClientID%>').val();
                          path = $('#<%= hdnSelectedPath.ClientID%>').val();
                          var tempURL = "path=" + path + "&ID=" + hdnId + "<%= "&Data="+ QueryString.QueryStringEncode("ID=&folderID=" +(int)Enum_Tatva.Folders.Archive + "&sessionID=" +Sessions.SwitchedRackspaceId )%>";
                      }
                      else {
                          path = $("#" + '<%= hdnSelectedPath.ClientID%>').val();
                          hdnId = $("#" + '<%= hdnSearchedId.ClientID%>').val();
                          var tempURL = "path=" + path + "&ID=" + hdnId + "<%= "&Data="+ QueryString.QueryStringEncode("ID=&dateTime="+ DateTime.UtcNow.ToString("yyyyMMddHHmmssfff") +"&folderID=" +(int)Enum_Tatva.Folders.Archive + "&sessionID=" +Sessions.SwitchedRackspaceId )%>";
                      }<%--var url = window.location.origin + "<%= ConfigurationManager.AppSettings["VirtualDir"]%>" + "Office/PdfViewer.aspx?" + tempURL;--%>
                      var url = window.location.origin + "/Office/PdfViewer.aspx?" + tempURL;
                      url = encodeURI(url);
                      return url;
                  }



                  function disableScrollbar() {
                      $('html, body').css({
                          'overflow': 'hidden',
                          'height': '100%'
                      });
                  }

                  function enableScrollbar() {
                      $('html, body').css({
                          'overflow': 'auto',
                          'height': 'auto'
                      });
                  }
                  function OnShare(obj) {
                      $("#dialog-share-book").dialog({ title: "Share this Archive Document" });
                      $('#dialog-share-book').dialog("open");
                      $('#dialog-share-book input[name="email"]').val('<%=Sessions.SwitchedEmailId%>');
                      $('#dialog-share-book input[name="myname"]').val('');
                      $('#dialog-share-book input[name="emailto"]').val('');
                      $('#dialog-share-book input[name="nameto"]').val('');
                      //$('#dialog-share-book input[name="title"]').val('');
                      <%--if ('<%= User.IsInRole("Offices") %>' == 'True')
                          $("#expirationTime").attr("readonly", false);
                      else {
                          $("#updateExpiration").hide();
                          $("#expirationTime").css("width", "95%");
                      }--%>
                      $('#dialog-share-book input[name="linkexpiration"]').val('<%= Sessions.ExpirationTime %>');
                      //GetExpirationTime();
                      $('#dialog-share-book textarea[name="content"]').val('');

                      $('#dialog-share-book input[name="email"]').css('border', '1px solid black');
                      $('#dialog-share-book input[name="myname"]').css('border', '1px solid black');
                      $('#dialog-share-book input[name="emailto"]').css('border', '1px solid black');

                      $('#dialog-share-book input[name="email"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="email"]')) });

                      $('#dialog-share-book input[name="emailto"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="emailto"]')) });
                      $('#<%= hdnSelectedPath.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnPathName]").val());
                      $('#<%= hdnSearchedId.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnArchiveId]").val());
                  }

                  function RedirectToArchive() {
                      window.location = "ArchiveSearch.aspx";
                  }

    </script>
</asp:Content>
