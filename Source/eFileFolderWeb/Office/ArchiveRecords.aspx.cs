﻿using Elasticsearch.Net;
using Nest;
using Shinetech.Engines;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using PdfSharp.Drawing;
using Shinetech.DAL;

public partial class Office_ArchiveRecords : System.Web.UI.Page
{
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();

    protected void Page_Load(object sender, EventArgs e)
    {
        var argument = Request.Form["__EVENTARGUMENT"];

        if (!Page.IsPostBack)
        {
            hdnFolderId.Value = Convert.ToString(Session["DropDownValue"]);
            Session["DropDownValue"] = null;
            GetData();
        }
        else
        {
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
            {
                BindDataGrid();
            }
        }
    }

    protected void WebPager1_PageIndexChanged(object sender, Shinetech.Framework.Controls.PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.ArchiveDataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object s, Shinetech.Framework.Controls.PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.ArchiveDataSource;
        WebPager1.DataBind();
    }

    public void GetData()
    {
        this.ArchiveDataSource = General_Class.GetArchiveForUser(string.IsNullOrEmpty(hdnFolderId.Value) ? 0 : Convert.ToInt32(hdnFolderId.Value), Sessions.SwitchedRackspaceId);
        BindDataGrid();
    }

    private void BindDataGrid()
    {
        gridView.DataSource = this.ArchiveDataSource.AsDataView();
        gridView.DataBind();
        WebPager1.DataSource = this.ArchiveDataSource;
        WebPager1.DataBind();
    }

    [WebMethod]
    public static void LoadArchives(string folderId = null, string uid = null)
    {
        Office_ArchiveRecords archives = new Office_ArchiveRecords();
        try
        {
            archives.ArchiveDataSource = null;
            archives.ArchiveDataSource = General_Class.GetArchiveForUser(string.IsNullOrEmpty(folderId) ? 0 : Convert.ToInt32(folderId), uid);
        }
        catch (Exception ex)
        {
            archives.ArchiveDataSource = new DataTable();
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
        }
    }

    protected void gridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.ArchiveDataSource);

        Source.Sort = e.SortExpression + " " + sortDirection;
        this.ArchiveDataSource = Source.ToTable();
        BindDataGrid();
    }
    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    public DataTable ArchiveDataSource
    {
        get
        {
            return this.Session["DataSource_ArchiveRecord"] as DataTable;
        }
        set
        {
            this.Session["DataSource_ArchiveRecord"] = value;
        }
    }

    [WebMethod]
    public static Array GetPreviewURL(int archiveID, int folderID, string shareID, bool isSplit = false)
    {
        Office_ArchiveRecords archive = new Office_ArchiveRecords();
        return archive.GetURL(folderID, shareID, Sessions.SwitchedRackspaceId, archiveID, isSplit);
    }

    public Array GetURL(int folderID, string shareID, string sessionID, int archiveID, bool isSplit = false)
    {
        try
        {
            string type = Path.GetExtension(shareID).Split('.')[1];
            string filePath = string.Empty, fileName = string.Empty, physicalPath = string.Empty;
            long fileSize = 0;
            //int workAreaId = 0;
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            physicalPath = rackSpaceFileUpload.GetPath(shareID, Enum_Tatva.Folders.Archive, sessionID, ref fileSize);
            if (System.IO.Path.GetExtension(physicalPath).ToLower() == ".tiff" || System.IO.Path.GetExtension(physicalPath).ToLower() == ".tif")
            {
                Stream strm = PDFParser.ConvertTiffToPdf(Path.GetFileName(physicalPath), null, HttpContext.Current.Server.MapPath("~/" + physicalPath));
                physicalPath = physicalPath.Substring(0, physicalPath.LastIndexOf('.') + 1) + "pdf";
                var fileStream = File.Create(HttpContext.Current.Server.MapPath("~/" + physicalPath));
                strm.Seek(0, SeekOrigin.Begin);
                strm.CopyTo(fileStream);
                fileStream.Close();
            }
            if (type == StreamType.JPG.ToString().ToLower())
                type = "1";
            else if (type == StreamType.PDF.ToString().ToLower())
                type = "2";

            string[] outPut = new string[3];
            outPut[0] = physicalPath;
            outPut[1] = type;
            outPut[2] = archiveID.ToString();
            if (archiveID > 0)
                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Preview.GetHashCode(), 0, 0, 0, 0, null, 0, archiveID);
            return outPut;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteElasticLog(archiveID.ToString(), folderID.ToString(), shareID, ex);
            return null;
        }
    }

    protected void btnArchiveSearch_Click(object sender, EventArgs e)
    {
        Response.Redirect("ArchiveSearch.aspx");
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        RackSpaceFileUpload rackspacefileupload = new RackSpaceFileUpload();
        string errorMsg = string.Empty;
        string newFileName = fileNameFld.Text.Trim();
        newFileName.Replace("&", "");
        newFileName.Replace("#", "");
        newFileName = newFileName.IndexOf(".pdf") > 0 ? newFileName : newFileName + ".pdf";
        string oldFileName = hdnSelectedPath.Value;
        string oldPath = oldFileName.Substring(0, oldFileName.LastIndexOf('/') + 1);
        oldFileName = oldPath + hdnFileNameOnly.Value;
        newFileName = Common_Tatva.SubStringFilename(newFileName);
        oldPath = oldPath.Substring(0, oldPath.LastIndexOf('/'));
        string newObjectName = rackspacefileupload.RenameObject(ref errorMsg, oldFileName, newFileName, Enum_Tatva.Folders.Archive, Sessions.SwitchedRackspaceId);
        if (newObjectName != null)
        {
            newFileName = newObjectName.Substring(newObjectName.LastIndexOf('/') + 1);
            int count = General_Class.UpdateArchive(Convert.ToInt32(hdnFileId.Value), newFileName, newObjectName);
            EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
            bool isValid = eFileFolderJSONWS.UpdateArchiveInElastic(hdnFileId.Value, newFileName, newObjectName, oldPath.Substring(oldPath.LastIndexOf('/') + 1));
            if (Convert.ToBoolean(hdnIsSearched.Value)) 
            {
                GetData();
            }
            else
            {
                //GetSearchedData(Convert.ToInt32(pager.SelectedItem.Value));
            }
            hdnFileName.Value = newFileName;
            lblFileName.Text = newFileName;
            hdnSelectedPath.Value = newObjectName;
            ScriptManager.RegisterStartupScript(updatePanle1, typeof(UpdatePanel), "alertok",
                                                           "<script language=javascript>alert(\"" + "Document has been edited successfully." + "\");ShowPreviewForArchive(null);</script>", false);
        }
        else
        { }
    }
}