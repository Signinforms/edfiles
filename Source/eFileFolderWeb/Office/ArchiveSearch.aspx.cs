﻿using Elasticsearch.Net;
using Nest;
using Shinetech.Engines;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Shinetech.DAL;
using System.Threading;

public partial class Office_ArchiveSearch : System.Web.UI.Page
{
    protected static string hostString = ConfigurationManager.AppSettings.Get("ElasticHostString");
    protected static string elasticIndexName = ConfigurationManager.AppSettings.Get("ElasticIndices");
    protected static Uri[] hosts = hostString.Split(',').Select(x => new Uri(x)).ToArray();
    protected static StaticConnectionPool connectionPool = new StaticConnectionPool(hosts);
    protected static ConnectionSettings settings = new ConnectionSettings(connectionPool)
                                                                         .DefaultMappingFor<Archive>(i => i
                                                                         .IndexName(elasticIndexName)
                                                                         .TypeName("pdf"))
                                                                         .DisableDirectStreaming();
    protected static ElasticClient highClient = new ElasticClient(settings);
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
    protected NLogLogger _logger = new NLogLogger();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            pager.Visible = false;
        }
    }

    protected void btnArchiveSearch_Click(object sender, EventArgs e)
    {
        try
        {
            IsSearched = true;
            int totalHits = GetSearchedData();
            if (totalHits > 0)
            {
                pager.Items.Clear();
                int pageCount = ((totalHits % 10) == 0) ? ((totalHits / 10)) : ((totalHits / 10) + 1);
                for (int i = 0; i < pageCount; i++)
                {
                    pager.Items.Add(new ListItem((i + 1).ToString(), (i * 10).ToString()));
                }
                pager.Visible = true;
            }
            else
            {
                pager.Items.Clear();
                pager.Visible = false;
            }
        }
        catch (Exception ex)
        {
            _logger.Info(ex);
        }
    }

    protected void btnViewRecords_Click(object sender, EventArgs e)
    {
        try
        {
            IsSearched = false;
            int totalHits = GetArchiveData();
            if (totalHits > 0)
            {
                pager.Items.Clear();
                int pageCount = ((totalHits % 10) == 0) ? ((totalHits / 10)) : ((totalHits / 10) + 1);
                for (int i = 0; i < pageCount; i++)
                {
                    pager.Items.Add(new ListItem((i + 1).ToString(), (i * 10).ToString()));
                }
                pager.Visible = true;
            }
            else
            {
                pager.Items.Clear();
                pager.Visible = false;
            }
        }
        catch (Exception ex)
        {
            _logger.Info(ex);
        }
    }

    public int GetSearchedData(int page = 0)
    {
        var searchResponse = highClient
            .Search<Archive>(s => s
                .From(page)
                .Size(10)
                .Sort(so => so
                    .Field(f => f
                        .Field(p => p.FileName)
                        .Order(Sort_Direction)
                    )
                )
                .Query(q => q
                    .MultiMatch(qs => qs
                       .Fields(fs => fs
                           .Field(f => f.FolderId))
                       .Query(hdnFolderId.Value)
                    )
                )
                .Highlight(h => h
                   .PreTags("<span class='yellow'>")
                   .PostTags("</span>")
                   .Encoder(HighlighterEncoder.Html)
                   .Fields(fs => fs
                       .Field("jsonData.ngram")
                       .Type(HighlighterType.Plain)
                       .FragmentSize(50)
                       .ForceSource(true)
                       .Fragmenter(HighlighterFragmenter.Span)
                       .NumberOfFragments(150)
                       .NoMatchSize(50)
                       .PhraseLimit(100)
                       .BoundaryMaxScan(50)
                       .HighlightQuery(hl => hl
                           .Fuzzy(m => m
                               .Name("ngram")
                               .Boost(1.1)
                               .Field("jsonData.ngram")
                               .Value(searchKey.Text.ToLower())
                               .Rewrite(MultiTermQueryRewrite.ConstantScore)
                               .Transpositions(true)
                            )
                        )
                    )
                )
                .PostFilter(q => q
                    .Fuzzy(b => b
                        .Name("ngram")
                        .Boost(1.1)
                        .Field("jsonData.ngram")
                        .Value(searchKey.Text.ToLower())
                        .Rewrite(MultiTermQueryRewrite.ConstantScore)
                        .Transpositions(true)
                        )
                    )
                );

        DataTable dtNew = new DataTable();
        dtNew.Columns.Add("FolderId");
        dtNew.Columns.Add("FileName");
        dtNew.Columns.Add("ArchiveId");
        dtNew.Columns.Add("Data");
        dtNew.Columns.Add("PathName");
        dtNew.Columns.Add("Size");
        dtNew.Columns.Add("CreatedDate");

        for (int i = 0; i < searchResponse.Documents.Count; i++)
        {
            var dr = dtNew.NewRow();
            dr["FolderId"] = searchResponse.Documents.ToList()[i].FolderId;
            dr["FileName"] = searchResponse.Documents.ToList()[i].FileName;
            dr["ArchiveId"] = searchResponse.Documents.ToList()[i].ArchiveId;
            dr["PathName"] = searchResponse.Documents.ToList()[i].PathName;
            if (searchResponse.Hits.Count > 0)
            {
                if (searchResponse.Hits.ToList()[i].MatchedQueries.Count <= 0)
                    continue;
                foreach (var responses in searchResponse.Hits.ToList()[i].Highlights.ToList())
                {
                    foreach (var highLights in responses.Value.Highlights)
                    {
                        dr["Data"] += (highLights + "<span>EdSplit</span>");
                    }
                }
            }
            dtNew.Rows.Add(dr);
        }
        this.ArchiveDataSource = dtNew;
        gridView.DataSource = this.ArchiveDataSource;
        gridView.DataBind();
        if (searchResponse.HitsMetadata != null)
            return Convert.ToInt32(searchResponse.HitsMetadata.Total);
        else return 0;
    }


    protected int GetArchiveData(int pageNo = 0)
    {
        DataTable dtArchive = General_Class.GetArchiveForUser(string.IsNullOrEmpty(hdnFolderId.Value) ? null : (int?)Convert.ToInt32(hdnFolderId.Value), Sessions.SwitchedRackspaceId, string.IsNullOrEmpty(Sort_Expression) ? null : Sort_Expression, string.IsNullOrEmpty(Sort_Direction.ToString()) ? null : Sort_Direction == SortOrder.Ascending ? "ASC" : "DESC", pageNo);
        if (dtArchive != null && dtArchive.Rows.Count > 0)
        {
            int totalHits = Convert.ToInt32(dtArchive.Rows[0]["TotalRecords"].ToString());
            dtArchive.Columns.Add("Data");
            this.ArchiveDataSource = dtArchive;
            gridView.DataSource = this.ArchiveDataSource;
            gridView.DataBind();
            return totalHits;
        }
        else
        {
            gridView.DataSource = new DataTable();
            gridView.DataBind();
            return 0;
        }
    }

    protected void pager_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (IsSearched)
        {
            GetSearchedData(Convert.ToInt32(pager.SelectedItem.Value));
        }
        else
        {
            GetArchiveData(Convert.ToInt32(pager.SelectedItem.Value) / 10);
        }
    }

    protected void gridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        Sort_Expression = e.SortExpression;
        if (Sort_Direction == SortOrder.Ascending)
            Sort_Direction = SortOrder.Descending;
        else
            Sort_Direction = SortOrder.Ascending;
        if (IsSearched)
        {
            GetSearchedData(Convert.ToInt32(pager.SelectedItem.Value));
        }
        else
        {
            GetArchiveData(Convert.ToInt32(pager.SelectedItem.Value) / 10);
        }
    }


    protected void gridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                if (IsSearched)
                {
                    Label lbl = e.Row.FindControl("serchedData") as Label;
                    Label btnAll = e.Row.FindControl("lblAll") as Label;
                    List<string> results = new List<string>();
                    if (lbl != null)
                    {
                        results = lbl.Text.Split(new string[] { "<span>EdSplit</span>" }, StringSplitOptions.None).ToList();
                        lbl.Text = results[0];
                        //lbl.Visible = false;
                    }
                    var finalData = string.Empty;
                    for (int i = 0; i < results.Count - 1; i++)
                    {
                        if (i == 0)
                            finalData += "<label class='searchedLbl' id='" + i + "' style='display:inline-block'>" + results[i] + "<br /></label>";
                        else
                            finalData += "<label class='searchedLbl' id='" + i + "' style='display:none'>" + results[i] + "<br /></label>";
                    }
                    if (results.Count < 3)
                    {
                        Button btnNext = e.Row.FindControl("btnNext") as Button;
                        if (btnNext != null)
                            btnNext.Visible = false;
                        Button btnPrev = e.Row.FindControl("btnPrev") as Button;
                        if (btnPrev != null)
                            btnPrev.Visible = false;
                        if (btnAll != null)
                            btnAll.Visible = false;
                    }
                    lbl.Text = finalData;
                    gridView.Columns[1].Visible = true;
                    gridView.Columns[2].Visible = false;
                    gridView.Columns[3].Visible = false;
                }
                else
                {
                    gridView.Columns[1].Visible = false;
                    gridView.Columns[2].Visible = true;
                    gridView.Columns[3].Visible = true;
                }
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteElasticLog(string.Empty, hdnFolderId.Value, searchKey.Text, ex);
            }
        }
    }

    [WebMethod]
    public static void LoadArchive(int pgNo, string isSearched)
    {
        Office_ArchiveSearch archiveSearch = new Office_ArchiveSearch();
        if (Convert.ToBoolean(isSearched))
        {
            archiveSearch.GetSearchedData(pgNo);
        }
        else
        {
            archiveSearch.GetArchiveData(pgNo);
        }
    }

    [WebMethod]
    public static Array GetPreviewURL(int archiveID, int folderID, string shareID, bool isSplit = false)
    {
        Office_ArchiveSearch archive = new Office_ArchiveSearch();
        return archive.GetURL(folderID, shareID, Sessions.SwitchedRackspaceId, archiveID, isSplit);
    }

    public Array GetURL(int folderID, string shareID, string sessionID, int archiveID, bool isSplit = false)
    {
        try
        {
            string type = Path.GetExtension(shareID).Split('.')[1];
            string filePath = string.Empty, fileName = string.Empty, physicalPath = string.Empty;
            long fileSize = 0;
            //int workAreaId = 0;
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            physicalPath = rackSpaceFileUpload.GetPath(shareID, Enum_Tatva.Folders.Archive, sessionID, ref fileSize);
            if (System.IO.Path.GetExtension(physicalPath).ToLower() == ".tiff" || System.IO.Path.GetExtension(physicalPath).ToLower() == ".tif")
            {
                Stream strm = PDFParser.ConvertTiffToPdf(Path.GetFileName(physicalPath), null, HttpContext.Current.Server.MapPath("~/" + physicalPath));
                physicalPath = physicalPath.Substring(0, physicalPath.LastIndexOf('.') + 1) + "pdf";
                var fileStream = File.Create(HttpContext.Current.Server.MapPath("~/" + physicalPath));
                strm.Seek(0, SeekOrigin.Begin);
                strm.CopyTo(fileStream);
                fileStream.Close();
            }
            if (type == StreamType.JPG.ToString().ToLower())
                type = "1";
            else if (type == StreamType.PDF.ToString().ToLower())
                type = "2";

            string[] outPut = new string[3];
            outPut[0] = physicalPath;
            outPut[1] = type;
            outPut[2] = archiveID.ToString();
            if (archiveID > 0)
                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Preview.GetHashCode(), 0, 0, 0, 0, null, 0, archiveID);
            return outPut;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteElasticLog(archiveID.ToString(), folderID.ToString(), shareID, ex);
            return null;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            RackSpaceFileUpload rackspacefileupload = new RackSpaceFileUpload();
            string errorMsg = string.Empty;
            string newFileName = fileNameFld.Text.Trim();
            newFileName = newFileName.Replace("&", "").Replace("#", "");
            newFileName = newFileName.IndexOf(".pdf") > 0 ? newFileName : newFileName + ".pdf";
            string oldFileName = hdnSelectedPath.Value;
            string oldPath = oldFileName.Substring(0, oldFileName.LastIndexOf('/') + 1);
            oldFileName = oldPath + hdnFileNameOnly.Value;
            newFileName = Common_Tatva.SubStringFilename(newFileName);
            oldPath = oldPath.Substring(0, oldPath.LastIndexOf('/'));
            string newObjectName = rackspacefileupload.RenameObject(ref errorMsg, oldFileName, newFileName, Enum_Tatva.Folders.Archive, Sessions.SwitchedRackspaceId);
            if (string.IsNullOrEmpty(errorMsg) && !string.IsNullOrEmpty(newObjectName))
            {
                newFileName = newObjectName.Substring(newObjectName.LastIndexOf('/') + 1);
                int count = General_Class.UpdateArchive(Convert.ToInt32(hdnFileId.Value), newFileName, newObjectName);
                EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
                bool isValid = eFileFolderJSONWS.UpdateArchiveInElastic(hdnFileId.Value, newFileName, newObjectName, oldPath.Substring(oldPath.LastIndexOf('/') + 1));
                if (IsSearched)
                {
                    Thread.Sleep(1000);
                    GetSearchedData(Convert.ToInt32(pager.SelectedItem.Value));
                }
                else
                {
                    GetArchiveData(Convert.ToInt32(pager.SelectedItem.Value));
                }
                hdnFileNameOnly.Value = newFileName;
                lblFileName.Text = newFileName;
                hdnSelectedPath.Value = newObjectName;
                ScriptManager.RegisterStartupScript(updatePanle1, typeof(UpdatePanel), "alertok",
                                                               "<script language=javascript>alert(\"" + "Document has been edited successfully." + "\");ShowPreviewForArchive(" + hdnObjId.Value + ", true);</script>", false);
            }
            else
            {
                ScriptManager.RegisterStartupScript(updatePanle1, typeof(UpdatePanel), "alertok",
                                                               "<script language=javascript>alert(\"" + "Some error occurred. Please try again later !!" + "\");ShowPreviewForArchive(" + hdnObjId.Value + ", true);</script>", false);
            }
        }
        catch (Exception ex)
        {
            _logger.Info(ex);
            ScriptManager.RegisterStartupScript(updatePanle1, typeof(UpdatePanel), "alertok",
                                                               "<script language=javascript>alert(\"" + "Some error occurred. Please try again later !!" + "\");ShowPreviewForArchive(" + hdnObjId.Value + ", true);</script>", false);
        }
    }

    public string Sort_Expression
    {
        get
        {
            return SortExpression1.Value as string;
        }
        set
        {
            SortExpression1.Value = value;
        }
    }

    private SortOrder Sort_Direction
    {
        get
        {
            if (SortDirectionSearch == null)
            {
                return SortOrder.Descending;
            }
            if (SortDirectionSearch != null && string.IsNullOrEmpty(SortDirectionSearch.Value))
            {
                SortDirectionSearch.Value = Enum.GetName(typeof(SortOrder), SortOrder.Descending);
            }

            return (SortOrder)Enum.Parse(typeof(SortOrder), this.SortDirectionSearch.Value);

        }
        set
        {
            SortDirectionSearch.Value = Enum.GetName(typeof(SortOrder), value);
        }
    }

    public DataTable ArchiveDataSource
    {
        get
        {
            return this.Session["DataSource_ArchiveOffice"] as DataTable;
        }
        set
        {
            this.Session["DataSource_ArchiveOffice"] = value;
        }
    }

    public bool IsSearched
    {
        get
        {
            return Convert.ToBoolean(this.Session["IsSearched"]);
        }
        set
        {
            this.Session["IsSearched"] = value;
        }
    }

    //Archive archive = new Archive{
    //ArchiveId = 145
    //};
    //var test123 = highClient.Get<Archive>(archive, a => a.Index("archive").Type("pdf"));
    //var deleteResponse = highClient.DeleteIndex("archivetest");
    //var testRes = highClient.Map<Archive>(a => a
    //                                .Properties(t => t
    //.Number(i => i
    //    .Name("archiveId")
    //    )
    //                                    .Text(k => k
    //                                        .Fields(f => f
    //                                            .Text(w => w.Analyzer("lowercase_analyzer").Name("lowercase"))
    //                                            .Text(w => w.Index().Name("raw").Fielddata())
    //                                            )
    //                                        .Name("fileName")
    //                                        )
    //.Text(k => k
    //    .Fields(f => f
    //        .Text(w => w.Analyzer("lowercase_analyzer").Name("lowercase"))
    //        .Text(w => w.Index().Name("raw"))
    //        )
    //    .Name("pathName")
    //    )
    //.Number(i => i
    //    .Name("folderId")
    //    )
    //.Number(i => i
    //    .Name("size")
    //    )
    //.Text(k => k
    //    .Fields(f => f
    //        .Text(w => w.Analyzer("edge_ngram_analyzer").Name("edgengram"))
    //        .Text(w => w.Analyzer("ngram_analyzer").Name("ngram"))
    //        .Text(w => w.Index().Name("raw"))
    //        )
    //    .Name("jsonData")
    //    )
    //)
    //.Settings(s => s
    //   .Analysis(an => an
    //       .Analyzers(ana => ana
    //           .Custom("lowercase_analyzer", c => c
    //               .Tokenizer("keyword")
    //               .Filters("lowercase_token_filter")
    //            )
    //           .Custom("edge_ngram_analyzer", c => c
    //               .Tokenizer("edge_ngram_tokenizer")
    //               .Filters("lowercase")
    //            )
    //            .Custom("ngram_analyzer", c => c
    //               .Tokenizer("ngram_tokenizer")
    //               .Filters("lowercase")
    //            )
    //       )
    //       .TokenFilters(tf => tf
    //           .Lowercase("lowercase_token_filter")
    //           )
    //       .Tokenizers(t => t
    //          .EdgeNGram("edge_ngram_tokenizer", ef => ef.TokenChars(TokenChar.Letter, TokenChar.Digit).MinGram(2).MaxGram(30))
    //          .NGram("ngram_tokenizer", ef => ef.TokenChars(TokenChar.Letter, TokenChar.Digit).MinGram(4).MaxGram(10))
    //          )
    //            )
    //         )
    //);
    //var restoreResponse = highClient.Reindex<Archive>("archivelive", "archive");
    //highClient.Delete<Archive>(145);
    //var searchResponse = highClient.Search<Archive>(a => a
    //   .From(0)
    //   .Size(100)
    //   .Query(q => q
    //       .MultiMatch(qs => qs
    //           .Fields(fs => fs
    //               .Field(f => f.FolderId))
    //           .Query(hdnFolderId.Value)
    //       ))
    //       .Highlight(h => h
    //           .PreTags("<span class='yellow'>")
    //           .PostTags("</span>")
    //           .Encoder(HighlighterEncoder.Html)
    //           .Fields(fs => fs
    //               .Field(f => f.JsonData)
    //               .Type(HighlighterType.Plain)
    //               .FragmentSize(50)
    //               .ForceSource(true)
    //               .Fragmenter(HighlighterFragmenter.Span)
    //               .NumberOfFragments(150)
    //               .NoMatchSize(50)
    //               .PhraseLimit(100)
    //               .BoundaryMaxScan(50)
    //               .HighlightQuery(hl => hl
    //                   .Fuzzy(m => m
    //                       .Name("misspelled")
    //                       .Boost(1.1)
    //                       .Field(p => p.JsonData)
    //                       .Value(searchKey.Text.ToLower())
    //                       .Rewrite(MultiTermQueryRewrite.ConstantScore)
    //                       .Transpositions(true)
    //                       )
    //                   )
    //               )
    //            )
    //   .PostFilter(q => q
    //       .Fuzzy(qs => qs
    //           .Name("misspelled")
    //           .Boost(1.1)
    //           .Field(p => p.JsonData)
    //           .Value(searchKey.Text.ToLower())
    //           .Rewrite(MultiTermQueryRewrite.ConstantScore)
    //           .Transpositions(true)
    //       )
    //   )
    //    );



    //var result1 = highClient
    //    .CreateIndex(elasticIndexName, es => es
    //        .Settings(s => s
    //            .NumberOfReplicas(1)
    //            .Analysis(an => an
    //                .Analyzers(ana => ana
    //                    .Custom("lowercase_analyzer", c => c
    //                       .Tokenizer("keyword")
    //                       .Filters("lowercase_token_filter")
    //                    )
    //                   .Custom("edge_ngram_analyzer", c => c
    //                       .Tokenizer("edge_ngram_tokenizer")
    //                       .Filters("lowercase")
    //                    )
    //                    .Custom("ngram_analyzer", c => c
    //                       .Tokenizer("ngram_tokenizer")
    //                       .Filters("lowercase")
    //                    )
    //                )
    //               .TokenFilters(tf => tf
    //                   .Lowercase("lowercase_token_filter")
    //                )
    //               .Tokenizers(t => t
    //                  .EdgeNGram("edge_ngram_tokenizer", ef => ef.TokenChars(TokenChar.Letter, TokenChar.Digit).MinGram(2).MaxGram(30))
    //                  .NGram("ngram_tokenizer", ef => ef.TokenChars(TokenChar.Letter, TokenChar.Digit).MinGram(4).MaxGram(10))
    //                )
    //            )
    //        )
    //        .Mappings(m => m
    //            .Map<Archive>(a => a
    //                .Properties(t => t
    //                    .Number(i => i
    //                        .Name("archiveId")
    //                        )
    //                    .Text(k => k
    //                        .Fields(f => f
    //                            .Text(w => w.Analyzer("lowercase_analyzer").Name("lowercase"))
    //                            .Text(w => w.Index().Name("raw").Fielddata())
    //                            )
    //                        .Name("fileName")
    //                        )
    //                    .Text(k => k
    //                        .Fields(f => f
    //                            .Text(w => w.Analyzer("lowercase_analyzer").Name("lowercase"))
    //                            .Text(w => w.Index().Name("raw"))
    //                            )
    //                        .Name("pathName")
    //                        )
    //                    .Number(i => i
    //                        .Name("folderId")
    //                        )
    //                    .Number(i => i
    //                        .Name("size")
    //                        )
    //                    .Text(k => k
    //                        .Fields(f => f
    //                            .Text(w => w.Analyzer("edge_ngram_analyzer").Name("edgengram"))
    //                            .Text(w => w.Analyzer("ngram_analyzer").Name("ngram"))
    //                            .Text(w => w.Index().Name("raw"))
    //                            )
    //                        .Name("jsonData")
    //                    )
    //                )
    //            )
    //        )
    //);
    //var data = highClient.CatIndices();
    //var archive = new Archive()
    //{
    //    ArchiveId = 2,
    //    FolderId = 1,
    //    FileName = "test.pdf",
    //    PathName = "test",
    //    Size = 1,
    //    JsonData = "Hello!!!"
    //};
    //var createResponse = highClient.Index(archive, idx => idx.Index("archive_test").Id(archive.ArchiveId));
}
