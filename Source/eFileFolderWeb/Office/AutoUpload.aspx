﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AutoUpload.aspx.cs" Inherits="Office_AutoUpload" %>

<%@ Register Src="../Controls/FileRequestControl.ascx" TagName="FileRequestControl"
    TagPrefix="uc2" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css?v=1" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=3" rel="stylesheet" />

    <style type="text/css">
        .white_content
        {
            display: none;
            position: fixed;
            top: 20%;
            left: 20%;
            width: 60%;
            height: 50%;
            padding: 16px;
            background-color: white;
            z-index: 999999999;
            overflow: auto;
            font-size: large;
        }

        .hideControl
        {
            display: none;
        }

        .ajax__tab_xp .ajax__tab_body
        {
            font-family: "Open Sans", sans-serif;
        }

        .ajax__tab_header > span
        {
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
        }

            .ajax__tab_header > span > span > span > a > span
            {
                background: none !important;
            }

        .ajax__tab_body
        {
            height: auto!important;
        }

        .ajax__fileupload_uploadbutton,
        .removeButton
        {
            font-size: 10px;
        }

        .ajax__fileupload_dropzone
        {
            font-size: 15px;
            line-height: 120px !important;
            height: 120px !important;
        }

        .ajax__fileupload_fileItemInfo
        {
            font-size: 14px;
        }

        #overlay
        {
            display: none;
            position: fixed;
            top: 0;
            bottom: 0;
            background: rgba(0,0,0,.8);
            width: 100%;
            height: 100%;
            z-index: 99999;
        }
    </style>

    <script type="text/javascript">
        function pageLoad() {
            var dz = document.querySelector('.ajax__fileupload_dropzone');
            dz.addEventListener('dragenter', handleDragEnter, false);//Register Event dragenter    
            dz.addEventListener('dragover', handleDragOver, false);//Register Event dragover    
            dz.addEventListener('dragleave', handleDragLeave, false);//Register Event dragleave    
            dz.addEventListener('drop', handleDrop, false);//Register Event drop  
            $(".ajax__fileupload_dropzone").text("Drag and Drop Pdf file(s) here");

            $('input[type="file"]').change(function () {
                $('.ajax__fileupload_uploadbutton').click();
            });
        }

        function onClientUploadComplete(sender, e) {
            $('.uploadRename').remove();
            $('[id*=UploadOrCancelButton]').removeClass('uploadOnly');
            //onImageValidated("TRUE", e);
            return false;
        }

        function onClientUploadCompleteAll(sender, e) {
            $('.hideControl').removeClass('hideControl');
            $('.uploadProgress').find('select').removeAttr('disabled');
            $('#divUpload').clone().appendTo('#uploadFieldset');
            clearInterval(myTimer);
            document.getElementById('uploadProgress').style.display = 'none';
            __doPostBack('<%= UpdatePanel1.ClientID %>', '');
        }

        var myTimer;
        function onClientUploadStart(sender, e) {
            document.getElementById('uploadCompleteInfo').innerHTML = "please wait while the " + e.get_filesInQueue() + " file is uploaded and converted to eff...";
            showLoader();

            $('[id*=AjaxFileUpload1_Html5DropZone]').addClass('hideControl');
            $('[id*=AjaxFileUpload1_SelectFileContainer]').addClass('hideControl');
            $('[id*=FileItemDeleteButton]').addClass('hideControl').css('display', 'none');
            $('[id*=UploadOrCancelButton]').addClass('hideControl');

            $('.TabDropDown').prop('disabled', 'disabled');
            $('.ClassDropDown').prop('disabled', 'disabled');
            $('#uploadProgress').html('');
            $('#uploadProgress').html('').html($('#divUpload').html());
            document.getElementById('uploadProgress').style.display = 'block';
            myTimer = setInterval(function () {
                // $('#divUpload').clone(true).appendTo('#uploadProgress');
                $('#uploadProgress').html('').html($('#divUpload').html());
            }, 1000);
        }

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');;
        }

        function hideLoader() {
            $('#overlay').hide();
        }

        function handleDragOver(e) {
            if (e.preventDefault) {
                e.preventDefault(); // Necessary. Allows us to drop.    
            }
            this.classList.add('over');
            return false;
        }

        function handleDragEnter(e) {
            // If you have used the DropZone you must have noticed that when you drag an item into the browser the gray area(The DropZone)is hilighted by dotted line at its border    
            // well here is how I do it I just add border to the div...    
            // I Have created ta class called over which basically has the styles to hilight the div.     
            // but only when the you are draging any file on the browser    
            this.classList.add('over');
        }

        function handleDragLeave(e) {
            // while draging If you move the cursour out of the DropZone, then the hilighting must be removed    
            // so here I am removing the class over from the div    
            e.preventDefault();
            this.classList.remove('over');

        }

        //On Drop of file over the DropZone(I have Elaborated the process of What Happen on Drop in Points)    
        function handleDrop(e) {
            //1st thing to do is Stop its default event, If you won't then the browser will end up rendering the file    
            e.preventDefault();
            //2nd Checking if the the object e has any files in it.    
            //    actually the object named "dataTransfer" in the object e is the object that hold the data that is being dragged during a drag and drop operation    
            //    dataTransfer object can hold one or more files    
            if (e.dataTransfer.files.length == 0) {
                this.classList.remove('over');
                return;// if no files are found then there is no point executing the rest of the code so I return to the function.    
            }
            var files = e.dataTransfer.files;
            //3rd  Here I am using an object of FormData() to send the files to the server using AJAX    
            //     The FormData object lets you compile a set of key/value pairs to send using AJAX    
            //     Its primarily intended for use in sending form data    
            var data = new FormData();
            for (var i = 0, f; f = files[i]; i++) {
                data.append(files[i].name, files[i]);//Here I am appending the files from the dataTransfer object to FormData() object in Key(Name of File) Value(File) pair    
            }
            // The operation of Uploading the file consumes time till the time the browser almost freezes     
            this.classList.remove('over');
            $('.ajax__fileupload_uploadbutton').click();
        }

        function closePage() {
            window.location = "http://www.edfiles.com/Office/Welcome.aspx";
        }
    </script>
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <asp:Button ID="Button1ShowPopup2" runat="server" Style="display: none" />
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BehaviorID="pnlPopup2"
        PopupControlID="pnlPopup2" DropShadow="true" BackgroundCssClass="modalBackground"
        Enabled="True" TargetControlID="Button1ShowPopup2" />
    <asp:Panel ID="pnlPopup2" Style="display: none"
        runat="server" class="popup-mainbox">
        <div>
            <div class="popup-head" id="Div1">
                Message
            </div>
            <div class="popup-scroller">
                <p>
                    Your document has been uploaded successfully...It will be converting to the EdFile format in our server over the next few minutes, depending on the size of the uploaded document. You can now select additional files to upload or navigate away from this web page. 
                </p>
            </div>
            <div class="popup-btn">
                <asp:Button ID="ShowPopup2Button2" runat="server" OnClientClick="return onClientButtonClick()"
                    Text="Close" class="btn green" />
                <%--<input id="btnuploadclose" type="button" value="Close"  />--%>
            </div>
        </div>
    </asp:Panel>
    <div id="uploadProgress" class="white_content"></div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" OnPreRender="UpdatePanel1_PreRender">
        <ContentTemplate>
            <div>
                <div class="title-container">
                    <div class="inner-wrapper">
                        <h1>Auto Upload</h1>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="page-container register-container">
                    <div class="inner-wrapper">
                        <div class="form-containt listing-contant">
                            <div>
                                <%--class="left-content"--%>
                                <div class="">
                                    <div>
                                        <p>Just drag and drop files here, we will upload it to their respective folder.</p>
                                    </div>
                                    <div class="content-box listing-view" style="margin-top: 30px; margin-bottom: 10px" id="uploadDiv" runat="server">
                                        <fieldset id="uploadFieldset">
                                            <div id="divUpload">
                                                <div class='clearfix'></div>
                                                <asp:Image runat="server" ID="Imagelabel12" ImageUrl="../Images/loader_bg.gif" ImageAlign="Middle" /><asp:Label
                                                    ID="label12" runat="server"></asp:Label>
                                                <asp:Label runat="server" ID="myThrobber" Style="display: none;">
                                    <img align="absmiddle" alt="" src="uploading.gif"/></asp:Label>
                                                <ajaxToolkit:AjaxFileUpload ID="AjaxFileUpload1" runat="server" Padding-Bottom="4"
                                                    Padding-Left="2" Padding-Right="1" Padding-Top="4" ThrobberID="myThrobber" OnClientUploadComplete="onClientUploadComplete"
                                                    OnUploadComplete="AjaxFileUpload1_UploadComplete" MaximumNumberOfFiles="25"
                                                    AllowedFileTypes="jpg,jpeg,pdf" AzureContainerName="" OnClientUploadCompleteAll="onClientUploadCompleteAll"
                                                    OnUploadCompleteAll="AjaxFileUpload1_UploadCompleteAll" OnUploadStart="AjaxFileUpload1_UploadStart"
                                                    OnClientUploadStart="onClientUploadStart" ContextKeys="2" />
                                                <div class='clearfix'></div>
                                                <div id="uploadCompleteInfo">
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
