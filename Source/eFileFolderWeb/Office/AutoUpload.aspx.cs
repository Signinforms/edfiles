﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using iTextSharp.text.pdf;
using Shinetech.DAL;
using Shinetech.Engines;

public partial class Office_AutoUpload : System.Web.UI.Page
{
    #region Properties
    public NLogLogger _logger = new NLogLogger();
    public string newFileName = string.Empty;
    public string fullFilePath = string.Empty;
    public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    /// <summary>
    /// Handles the UploadComplete event of the AjaxFileUpload1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="file">The <see cref="AjaxFileUploadEventArgs"/> instance containing the event data.</param>
    protected void AjaxFileUpload1_UploadComplete(object sender, AjaxFileUploadEventArgs file)
    {
        int dividerIdForFile = 0, folderIdForFile = 0;
        string dividerName = string.Empty;
        string folderName = string.Empty;
        try
        {
            string[] splitedData = Path.GetFileNameWithoutExtension(file.FileName).Split('-');
            folderIdForFile = Convert.ToInt32(splitedData[0].Trim());
            dividerIdForFile = Convert.ToInt32(splitedData[1].Trim());

            DataTable dtFolderAndTabs = General_Class.GetFolderAndDividerDetails(folderIdForFile, dividerIdForFile, Sessions.SwitchedRackspaceId);
            if (dtFolderAndTabs != null && dtFolderAndTabs.Rows.Count > 0)
            {
                folderName = Convert.ToString(dtFolderAndTabs.Rows[0]["FolderName"]);
                dividerName = Convert.ToString(dtFolderAndTabs.Rows[0]["DividerName"]);
                byte[] fullfile = ProcessFile(AjaxFileUpload1, file, folderIdForFile, dividerIdForFile, dividerName, folderName);
                ProcessFile(file, fullfile, folderIdForFile, dividerIdForFile);

                if (Convert.ToInt32(this.DocumentID) > 0)
                {
                    Shinetech.DAL.General_Class.AuditLogByDocId(Convert.ToInt32(this.DocumentID), Sessions.SwitchedSessionId, Enum_Tatva.Action.Create.GetHashCode());
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(folderIdForFile, dividerIdForFile, Convert.ToInt32(this.DocumentID), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Upload.GetHashCode(), 0, 0);
                }
                else
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert('Failed to upload file. Please try again after sometime.');", true);
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
            Common_Tatva.WriteErrorLog(folderIdForFile.ToString(), dividerIdForFile.ToString(), newFileName, ex);
        }
    }

    /// <summary>
    /// Processes the file.
    /// </summary>
    /// <param name="AjaxFileUpload1">The ajax file upload1.</param>
    /// <param name="file">The <see cref="AjaxFileUploadEventArgs"/> instance containing the event data.</param>
    /// <param name="folderIdForFile">The folder identifier for file.</param>
    /// <param name="dividerIdForFile">The divider identifier for file.</param>
    /// <param name="dividerName">Name of the divider.</param>
    /// <param name="folderName">Name of the folder.</param>
    /// <returns></returns>
    public byte[] ProcessFile(AjaxFileUpload AjaxFileUpload1, AjaxFileUploadEventArgs file, int folderIdForFile, int dividerIdForFile, string dividerName, string folderName)
    {
        try
        {
            string basePath = this.MapPath(this.FolderName);

            string folderID = Convert.ToString(folderIdForFile);
            string dividerID = Convert.ToString(dividerIdForFile);
            string path = basePath + Path.DirectorySeparatorChar +
                        folderID + Path.DirectorySeparatorChar + dividerIdForFile;

            var fName = file.FileName;
            fName = string.Join(" ", folderName, dividerName) + Path.GetExtension(fName);
            fName = fName.Replace("#", "");
            fName = fName.Replace("&", "");
            fName = Common_Tatva.SubStringFilename(fName);
            string encodeName = HttpUtility.UrlPathEncode(fName);
            encodeName = encodeName.Replace("%", "$");
            string fileName = String.Concat(
                    path,
                    Path.DirectorySeparatorChar,
                    encodeName
                );
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            byte[] s = file.GetContents();

            fileName = String.Concat(path, Path.DirectorySeparatorChar, Common_Tatva.ChangeExtension(fileName));

            fileName = Common_Tatva.GetRenameFileName(fileName);

            newFileName = Path.GetFileName(fileName);
            newFileName = HttpUtility.UrlDecode(newFileName.Replace('$', '%').ToString());

            AjaxFileUpload1.SaveAs(fileName);
            fullFilePath = fileName;
            return s;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(Convert.ToString(folderIdForFile), dividerIdForFile.ToString(), newFileName, ex);
            return null;
        }
    }

    /// <summary>
    /// Gets the name of the folder.
    /// </summary>
    /// <value>
    /// The name of the folder.
    /// </value>
    public string FolderName
    {
        get
        {
            return string.Format("~/{0}", CurrentVolume);
        }

    }

    /// <summary>
    /// Gets the path URL.
    /// </summary>
    /// <value>
    /// The path URL.
    /// </value>
    public string PathUrl
    {
        get { return CurrentVolume; }
    }

    /// <summary>
    /// Processes the file.
    /// </summary>
    /// <param name="file">The <see cref="AjaxFileUploadEventArgs"/> instance containing the event data.</param>
    /// <param name="fileStream">The file stream.</param>
    /// <param name="folderIdForFile">The folder identifier for file.</param>
    /// <param name="dividerIdForFile">The divider identifier for file.</param>
    public void ProcessFile(AjaxFileUploadEventArgs file, byte[] fileStream, int folderIdForFile, int dividerIdForFile)
    {
        try
        {
            WorkItem item = new WorkItem();
            item.FolderID = Convert.ToInt32(folderIdForFile);
            item.DividerID = dividerIdForFile;
            string basePath = this.MapPath(this.FolderName);

            item.Path = this.PathUrl + Path.DirectorySeparatorChar +
                        item.FolderID + Path.DirectorySeparatorChar + dividerIdForFile;
            item.FileName = Path.GetFileNameWithoutExtension(newFileName);
            item.FullFileName = Path.GetFileName(newFileName);
            item.UID = Sessions.SwitchedSessionId;
            //item.UID = Membership.GetUser().ProviderUserKey.ToString();
            item.contentType = Common_Tatva.getFileExtention(Path.GetExtension(newFileName));
            item.Content = new MemoryStream(fileStream);
            int order = FlashViewer.GetDocumentOrder(Convert.ToInt32(dividerIdForFile));


            item.MachinePath = string.Concat(basePath, Path.DirectorySeparatorChar, item.FolderID,
            Path.DirectorySeparatorChar, dividerIdForFile);

            if (!Directory.Exists(item.MachinePath))
            {
                Directory.CreateDirectory(item.MachinePath);
            }

            float size = (float)Math.Round((decimal)(fileStream.Length / 1024) / 1024, 2); // IN MB.
            string encodeName = HttpUtility.UrlPathEncode(item.FullFileName);
            encodeName = encodeName.Replace("%", "$");
            string source = string.Concat(item.MachinePath, Path.DirectorySeparatorChar, encodeName);
            var pageCount = 1;
            if (item.contentType == StreamType.JPG)
            {
                Jpeg2Engine jpeg2Engine = new Jpeg2Engine(item);
                jpeg2Engine.generatePDFfromImage(source, source.Split('.')[0] + ".pdf");
            }
            else if (item.contentType == StreamType.PDF)
            {
                PdfReader pdfReader = new PdfReader(source);
                pageCount = pdfReader.NumberOfPages;
            }
            this.DocumentID = General_Class.DocumentsInsert(item.FileName, item.FolderID, item.UID, item.Path, item.DividerID, pageCount, size, item.FileName, (int)item.contentType, order + 1, Enum_Tatva.Class.Permanent.GetHashCode().ToString());
            this.DocumentName = item.FileName;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(Convert.ToString(folderIdForFile), Convert.ToString(dividerIdForFile), newFileName, ex);

            Dictionary<string, string> UploadErrorFileName1 = new Dictionary<string, string>();
            UploadErrorFileName1.Add(file.FileId, file.FileName);

            UploadErrorFileName = UploadErrorFileName1;

            if ((File.Exists(fullFilePath)))
                File.Delete(fullFilePath);
        }
    }

    /// <summary>
    /// Gets or sets the name of the upload error file.
    /// </summary>
    /// <value>
    /// The name of the upload error file.
    /// </value>
    public Dictionary<string, string> UploadErrorFileName
    {
        get
        {
            return (Session["UploadErrorFileName"] != null) ? (Dictionary<string, string>)Session["UploadErrorFileName"] : new Dictionary<string, string>();
        }
        set
        {
            Session["UploadErrorFileName"] = value;
        }
    }

    /// <summary>
    /// Gets or sets the document identifier.
    /// </summary>
    /// <value>
    /// The document identifier.
    /// </value>
    public string DocumentID
    {
        get
        {
            return (string)Session["DocumentID"];
        }
        set
        {
            Session["DocumentID"] = value;

        }
    }

    /// <summary>
    /// Gets or sets the name of the document.
    /// </summary>
    /// <value>
    /// The name of the document.
    /// </value>
    public string DocumentName
    {
        get
        {
            return (string)Session["DocumentName"];
        }
        set
        {
            Session["DocumentName"] = value;

        }
    }

    /// <summary>
    /// Handles the UploadStart event of the AjaxFileUpload1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="AjaxControlToolkit.AjaxFileUploadStartEventArgs"/> instance containing the event data.</param>
    protected void AjaxFileUpload1_UploadStart(object sender, AjaxControlToolkit.AjaxFileUploadStartEventArgs e)
    {
        var now = DateTime.Now;
        e.ServerArguments = now.ToShortTimeString();
        Session["uploadTime"] = now;
    }

    /// <summary>
    /// Handles the UploadCompleteAll event of the AjaxFileUpload1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="AjaxControlToolkit.AjaxFileUploadCompleteAllEventArgs"/> instance containing the event data.</param>
    protected void AjaxFileUpload1_UploadCompleteAll(object sender, AjaxControlToolkit.AjaxFileUploadCompleteAllEventArgs e)
    {
        var startedAt = (DateTime)Session["uploadTime"];
        var now = DateTime.Now;
        e.ServerArguments = new JavaScriptSerializer()
            .Serialize(new
            {
                duration = (now - startedAt).Seconds,
                time = DateTime.Now.ToShortTimeString()
            });
    }

    /// <summary>
    /// Handles the PreRender event of the UpdatePanel1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void UpdatePanel1_PreRender(object sender, EventArgs e)
    {
        if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"] != string.Empty)
        {
            // This code will only be executed if the partial postback                
            //  was raised by a __doPostBack('UpdatePanel1', '')                
            if (Request.Form["__EVENTTARGET"] == UpdatePanel1.ClientID)
            {
                ScriptManager.RegisterClientScriptBlock(this.UpdatePanel1, typeof(UpdatePanel), "Alert", "hideLoader();$find('pnlPopup2').show();", true);
            }
        }
    }
}