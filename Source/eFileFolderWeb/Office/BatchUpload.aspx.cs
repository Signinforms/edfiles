﻿using Newtonsoft.Json;
using Shinetech.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Office_BatchUpload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #region BatchUpload
    [WebMethod]
    public string CheckFolderIdExists(string folderName, string uid)
    {
        return JsonConvert.SerializeObject(General_Class.CheckFolderIdExists(folderName, uid));
    }
    #endregion
}