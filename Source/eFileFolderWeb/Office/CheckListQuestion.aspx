﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CheckListQuestion.aspx.cs" Inherits="Office_CheckListQuestion" %>

<link rel="shortcut icon" href="../Images/fav.ico" />
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../New/css/style.css" rel="stylesheet" />
    <style>
        table th {
            font-weight: bold;
        }
    </style>
</head>

<body style="background: none;">
    <script src="../New/js/jquery.min.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>extras/jquery-ui-1.8.22.custom.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js" type="text/javascript"></script>

    <%--     <script src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/js/menu/jquery-1.10.1.min.js" type="text/javascript"></script>
    <script src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/js/menu/menu3d.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/js/jquery.min.js"></script>--%>

    <form id="frmCheckListQuestion" runat="server">
        <div style="overflow: scroll; width: 100%; height: 100%">
            <div id="gvFolderLogDetails" class="iframe-checklist">
                <h2 style="text-align: center">Check List Question
                </h2>
                <asp:HiddenField ID="hdnData" runat="server" />
                <asp:HiddenField ID="hdnID" runat="server" />
                <asp:HiddenField ID="hdnV" runat="server" />
                <table>
                </table>
            </div>
            <div style="text-align: right; margin: 10px">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn green submit" />
                <asp:Button ID="btnSkip" runat="server" Text="Skip" CssClass="btn green skip" />
                <asp:Button ID="btnPrevious" runat="server" Text="Previous <<" CssClass="btn green previous" />
                <asp:Button ID="btnNext" runat="server" Text="Next >>" CssClass="btn green next" />
            </div>
        </div>

        <%--<asp:Repeater ID="rptCheckListQuestionAnswer" runat="server">
            <HeaderTemplate>
                <table width="100%" style="margin: 0px 0px 25px 7px;" class="datatable listing-datatable">
                    <thead>
                        <tr>
                            <th style="width: 30%; text-align: left">Question</th>
                            <th style="text-align: left">Answer</th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td style="text-align: left"><%# Eval("Question") %></td>
                    <td style="text-align: left"><%# Eval("Answer") %></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <asp:Repeater ID="rptCheckListQuestion" runat="server">
            <ItemTemplate>
                <div style="margin: 10px;">
                    <%# Eval("Question") %>
                    <div style="clear: both"></div>
                    <input type="text" class="itrame-text" id="tbxAnswer<%#Eval("ID") %>" name="tbxAnswer<%#Eval("ID") %>" data-id="<%# Eval("ID") %>" style="width: 100%" maxlength="2000"></input>
                </div>
            </ItemTemplate>
        </asp:Repeater>--%>
    </form>
</body>
</html>

<script type="text/javascript">

    var folderId = $('#' + '<%=hdnID.ClientID%>').val();
    var arrayFolderLogId = [];
    var arrayQue = [];
    var arrayQnsAns = [];
    var folderLogIndex = 0;
    var folderLogId;
    var questionDetails;
    var queAns;
    var ansDetails = [];
    var skip = 0;
    var previous = 0;
    var currentFolderLog = 0;
    var cntLastRecored = 0;

    $(document).ready(function () {
        GetFolderLogDetailById();
    });

    $('.next').click(function (e) {
        e.preventDefault();
        onNext(this);
    });

    $('.skip').click(function (e) {
        e.preventDefault();
        onSkip(this);
    });


    $('.previous').click(function (e) {
        e.preventDefault();
        onPrevious(this);
    });

    $('.submit').click(function (e) {
        e.preventDefault();
        onSubmit(this);
    });



    $('#frmCheckListQuestion').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    function onSubmit() {
        if ($('.AnsDetails').length > 0) {
            if (cntLastRecored == 1)
                cntLastRecored = 0;
            storeAnsDetails();
            var ansData = JSON.stringify(ansDetails);
            var folderId = $("#" + '<%= hdnID.ClientID %>').val();
            $.ajax({
                type: "POST",
                url: "CheckListQuestion.aspx/InsertAnsDetails",
                data: JSON.stringify({ ansDetails: ansData }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    if (result.d) {
                        if (result.d.length > 0) {
                            var success = result.d
                            if (success == "1") {
                                alert("Answer Saved Successfully");
                                var id = $('#' + '<%=hdnID.ClientID%>').val();
                                var data = $('#' + '<%=hdnData.ClientID%>').val();
                                window.top.close();
                                window.open("../EFileFolderV2.html?ID=" + id + "&" + "DATA=" + data + "&ISEDIT=" + true, '_blank', "toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no");
                            }
                            else if (success == "2") {
                                alert("You didn't enter any answer for question.");
                                var id = $('#' + '<%=hdnID.ClientID%>').val();
                                var data = $('#' + '<%=hdnData.ClientID%>').val();
                                window.top.close();
                                window.open("../EFileFolderV2.html?ID=" + id + "&" + "DATA=" + data + "&ISEDIT=" + true, '_blank', "toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no");
                            }
                            else {
                                alert("Fail");
                            }
                    }
                }
                }
            });
    }
}

function onPrevious() {
    storeAnsDetails();
    if (folderLogIndex == folderLogId.length - 1) {
        cntLastRecored = 1;
    }
    previous = 1;
    folderLogIndex = folderLogIndex - 1;
    clearFolderLogDetails();
    AddFolderLogDetails();
    if (folderLogIndex == 0) {
        $('#btnSubmit').hide();
        $('.previous').hide();
        $('.next').show();
        $('.skip').show();
    }
    if (folderLogIndex < folderLogId.length) {
        $('#btnSubmit').hide();
        $('.next').show();
        $('.skip').show();
    }


}

function onNext() {
    storeAnsDetails();
    previous = 1;
    folderLogIndex = folderLogIndex + 1;
    $('.previous').show();
    if (folderLogIndex < folderLogId.length) {
        if (skip == 0) {
            //storeAnsDetails();
        }
        clearFolderLogDetails();
        AddFolderLogDetails();
        skip = 0;
    }
    if (folderLogIndex + 1 == folderLogId.length) {
        $('#btnSubmit').show();
        $('.next').hide();
        $('.skip').show();
    }
}

function storeAnsDetails() {
    if ($('.AnsDetails').length > 0) {
        for (var i = 0; i < $('.AnsDetails').length; i++) {
            if (ansDetails.length > 0) {
                var isInsert = false;
                $.each(ansDetails, function (index, ele) {
                    if (ele.queId == $('.AnsDetails').eq(i).attr('queid')) {
                        ele.ans = $('.AnsDetails').eq(i).val();
                        isInsert = false;
                        return false;
                    }
                    else {
                        isInsert = true;
                    }
                });
                if (isInsert)
                    insertAnsDetails(i);
            }
            else {
                insertAnsDetails(i);
            }
        }
    }
}


function insertAnsDetails(CurrentIndex) {
    var questionId = $('.AnsDetails').eq(CurrentIndex).attr('queId');
    var answer = $('.AnsDetails').eq(CurrentIndex).val();
    var folderLogId = $('.AnsDetails').eq(CurrentIndex).attr('folderLogId');
    if (answer != "") {
        ansDetails.push({ queId: questionId, ans: answer, folderlogId: folderLogId });
    }
}


function clearFolderLogDetails() {
    if ($('.logDetails').length > 0) {
        $('.logDetails').remove();
    }
    if ($('.questionDetails').length > 0) {
        $('.questionDetails').remove();
    }
    if ($('.AnsDetails').length > 0) {
        $('.AnsDetails').remove();
    }
}

function onSkip() {
    skip = 1;
    //if (folderLogIndex + 1 < folderLogId.length) {
    //$('.skip').hide();
    //}
    //else {
    var id = $('#' + '<%=hdnID.ClientID%>').val();
    var data = $('#' + '<%=hdnData.ClientID%>').val();
    window.top.close();
    window.open("../EFileFolderV2.html?ID=" + id + "&" + "DATA=" + data, '_blank', "toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no");
    //}
}





function fillTextboxDetails(ansDetails, currentFolderLog) {
    for (var i = 0; i < ansDetails.length; i++) {
        if (ansDetails[i].folderlogId == currentFolderLog) {
            for (var j = 0; j <= $('.AnsDetails').length; j++) {
                if ($('.AnsDetails').eq(j).attr('queid') == ansDetails[i].queId) {
                    if (ansDetails[i].ans != "") {
                        $('.AnsDetails').eq(j).val(ansDetails[i].ans);
                    }
                }
            }

        }
    }
}


function searchFolderLogId(ansDetails, currentFolderLog) {
    for (var i = 0; i < ansDetails.length; i++) {
        if (ansDetails[i].folderlogId == currentFolderLog) {
            return ansDetails[i];
        }
    }
}


function AddFolderLogDetails() {
    if (folderLogId) {
        currentFolderLog = folderLogId[folderLogIndex].FolderLogID;
        for (var j = 0; j < queAns.length; j++) {
            if (queAns[j].FolderLogId == currentFolderLog) {
                if (queAns[j].Question != "" || queAns[j].Answer != "") {
                    $(".folderLogDetails").append("<tr class='logDetails'><td style='text-align: left'>" + queAns[j].Question + "</td><td style='text-align: left'>" + queAns[j].Answer + "</td></tr>");
                }
            }
        }
        for (var k = 0; k < questionDetails.length; k++) {
            if (questionDetails[k].FolderLogID == currentFolderLog) {
                if (questionDetails[k].Question != "") {
                    $(".folderLogDetails").append("<div style='margin: 10px;' class='questionDetails' >" + questionDetails[k].Question + "<div style='clear: both'></div><input type='text' class='itrame-text AnsDetails' id='tbxAnswer' folderLogId = " + currentFolderLog + "  queId= " + questionDetails[k].QuestionId + " name='tbxAnswer'  style='width: 100%' maxlength='2000'></input></div>");
                }
            }
        }
        if (previous == 1) {
            fillTextboxDetails(ansDetails, currentFolderLog);
            previous = 0;
        }
    }
}

function GetFolderLogDetailById() {
    var folderId = $('#' + '<%=hdnID.ClientID%>').val();
    $.ajax({
        type: "POST",
        url: "CheckListQuestion.aspx/GetFolderLogDetailById",
        data: '{"folderId":"' + folderId + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            if (data.d.length > 0) {
                folderLogId = JSON.parse(data.d[0]);
                questionDetails = JSON.parse(data.d[1]);
                queAns = JSON.parse(data.d[2]);
                $("#gvFolderLogDetails").append("<table width='100%' style='margin: 0px 0px 25px 7px;' class='datatable listing-datatable folderLogDetails'></table>");
                $(".folderLogDetails").append("<thead><tr><th style='width: 30%; text-align: left'>Question</th><th style='text-align: left'>Answer</th></tr></thead>");
                AddFolderLogDetails();
                if (folderLogId.length > 1) {
                    $('#btnSubmit').hide();
                    $('.previous').hide();
                    $('.skip').show();
                }
                else {
                    $('#btnSubmit').show();
                    $('.previous').hide();
                    $('.next').hide();
                    $('.skip').show();
                }
            }
            else {
                var id = $('#' + '<%=hdnID.ClientID%>').val();
                var data = $('#' + '<%=hdnData.ClientID%>').val();
                window.top.close();
                window.open("../EFileFolderV2.html?ID=" + id + "&" + "DATA=" + data, '_blank', "toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no");
            }
        }
    });
}


</script>
