﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Shinetech.DAL;
using System.Web.Security;
using System.Web.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;


public partial class Office_CheckListQuestion : System.Web.UI.Page
{
    string strID, data, v = string.Empty;

    public class KeyValue
    {
        public string queId;
        public string ans;
        public string folderLogId;
    }

    #region Page Load Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            strID = Request.QueryString["ID"];
            hdnID.Value = strID;
            data = Request.QueryString["DATA"];
            hdnData.Value = data;
            v = Request.QueryString["v"];
        }
    }
    #endregion

  

    #region GetFolderLogDetails
    [WebMethod]
    public static List<string> GetFolderLogDetailById(string folderId)
    {
        Office_CheckListQuestion objChecklistQuestion = new Office_CheckListQuestion();
        DataSet dsFolderLogDetails = new DataSet();
        dsFolderLogDetails = General_Class.GetFolderlogDetailsById(folderId);
        List<string> lstFolderLog = new List<string>();
        if (dsFolderLogDetails != null && dsFolderLogDetails.Tables.Count > 0 && dsFolderLogDetails.Tables[0] != null && dsFolderLogDetails.Tables[0].Rows.Count > 0)
        {
            string strDetails = string.Empty;
            for (int i = 0; i <= dsFolderLogDetails.Tables.Count - 1; i++)
            {
                strDetails = Newtonsoft.Json.JsonConvert.SerializeObject(dsFolderLogDetails.Tables[i]);
                lstFolderLog.Add(strDetails);
            }
        }
        return lstFolderLog;
    }


    [WebMethod]
    public static string InsertAnsDetails(string ansDetails)
    {
        string sucess = string.Empty;
        DataSet dsAnswer = null;
        DataTable dtAnswerDetails = new DataTable();
        dtAnswerDetails.Columns.Add("AnsId", typeof(int));
        dtAnswerDetails.Columns.Add("AnsValue", typeof(string));
        dtAnswerDetails.Columns.Add("QueId", typeof(int));
        dtAnswerDetails.Columns.Add("CreatedDate", typeof(DateTime));
        var ansList = JsonConvert.DeserializeObject<List<KeyValue>>(ansDetails);

        DateTime datCurrentDate = DateTime.Now;
        foreach (var item in ansList)
        {
            if (!string.IsNullOrEmpty(item.ans))
                dtAnswerDetails.Rows.Add(new object[] { "0", item.ans, item.queId, datCurrentDate });
        }
        if (dtAnswerDetails.Rows.Count > 0)
        {
            int intFolderLogId = 0;
            dsAnswer = General_Class.QnsInsetandUpdate(dtAnswerDetails, "1", DateTime.Now, new Guid(Sessions.SwitchedSessionId), "", intFolderLogId);
            //dsAnswer = General_Class.QnsInsetandUpdate(dtAnswerDetails, "1", DateTime.Now, new Guid(Sessions.UserId), "", intFolderLogId);
            if (dsAnswer != null && dsAnswer.Tables.Count > 0 && dsAnswer.Tables[0] != null && dsAnswer.Tables[0].Rows.Count > 0)
                sucess = "1";
            else
                sucess = "0";
        }
        else
            sucess = "2";

        return sucess;
    }

    //public string DataSetToJSON(DataSet ds)
    //{

    //    Dictionary<string, object> dict = new Dictionary<string, object>();
    //    foreach (DataTable dt in ds.Tables)
    //    {
    //        object[] arr = new object[dt.Rows.Count];

    //        for (int i = 0; i <= dt.Rows.Count - 1; i++)
    //        {
    //            arr[i] = dt.Rows[i].ItemArray;
    //        }

    //        dict.Add(dt.TableName, arr);
    //    }
    //    JavaScriptSerializer json = new JavaScriptSerializer();
    //    return json.Serialize(dict);
    //}
    #endregion
}