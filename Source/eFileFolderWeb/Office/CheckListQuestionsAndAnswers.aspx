﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CheckListQuestionsAndAnswers.aspx.cs" Inherits="Office_CheckListQuestionsAndAnswers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=2" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css?v=1" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>extras/jquery-ui-1.8.22.custom.min.js"></script>

    <%--<link href="../New/css/style.css" rel="stylesheet" />--%>

    <style>
        /*table th {
            font-weight: bold;
        }*/

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }

        .temp {
            min-width: 20% !important;
        }

        .lblfolderlog {
            margin-left: 5px;
        }

        td span {
            -ms-word-break: break-all;
            word-break: break-all;
            word-break: break-word;
            -webkit-hyphens: auto;
            -moz-hyphens: auto;
            hyphens: auto;
            max-width: 300px;
            white-space: normal!important;
        }

        td {
            max-width: 300px;
        }

        .datatable tbody td, .datatable thead th {
            border-left: 1px solid #cccaca;
            border-top: 1px solid #cccaca;
        }

        .datatable td {
            padding: 10px;
        }

        .setWidth {
            width: 20px !important;
        }

        .setWidthEdit {
            width: 34px !important;
        }

        .questionGrid {
            min-width: 1100px !important;
            max-width: none !important;
            overflow-x: scroll !important;
        }

        .exportToExcel {
            background: rgba(0, 0, 0, 0) url("../Images/export-excel.png") no-repeat scroll left center;
        }

        .exportToPDF {
            background: rgba(0, 0, 0, 0) url("../Images/export-pdf.png") no-repeat scroll left center;
        }

        .newddl {
            background: rgba(0, 0, 0, 0) url("../../images/select-icon.png") no-repeat scroll 53px 9px;
            color: #414a54;
            font-family: open sans;
            font-size: 16px;
            font-weight: 600;
            height: 30px;
            padding: 0 6px;
        }
    </style>


    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Folder Logs</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <%--<div class="iframe-checklist">--%>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant modify-contant">
                <asp:HiddenField ID="hdnID" runat="server" />
                <asp:HiddenField ID="hdnFolderLogId" runat="server" />
                <%--<asp:DataList ID="GridViewQNS" runat="server"  CssClass="datatable listing-datatable" Style="border:none">

                     <ItemTemplate>
                        <table cellspacing="0" rules="all" border="1" style="width:100%">
                            <tr>
                                <td scope="col" style="text-align:left;font-weight:600;">Name
                                </td>
                                 <td style="text-align:left;width:30%">
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>' />
                                </td>
                                <td scope="col" style="text-align:left;font-weight:600;">Created On
                                </td>
                                <td style="text-align:left">
                                    <asp:Label ID="lblCreatedOn" runat="server" Text='<%# Eval("CreatedOn") %>' />
                                </td>
                            </tr>
                            <tr>
                                <td scope="col" style="text-align:left;font-weight:600;" >Question
                                </td>
                                <td  colspan="3" style="text-align:left">
                                    <asp:Label ID="lblQuestion" runat="server" Text='<%# Eval("Question") %>' />
                                </td>
                            </tr>
                            <tr>
                                <td scope="col" style="text-align:left;font-weight:600;">Answer
                                </td>
                                <td  colspan="3" style="text-align:left">
                                   <asp:Label ID="lblAnswer" runat="server" Text='<%#Eval("Answer")%>' name="LabelAnswer" />
                               <asp:TextBox runat="server" Text='<%# Eval("Answer")%>' MaxLength="2000"
                                    ID="txtAnswer" TextMode="multiline" Style="display: none; height: auto; width: 365px;" name="txtAnswer" CssClass="txtAnswer" ClientIDMode="Static" />
                                <asp:LinkButton ID="lnkEdit" runat="server" Text="" AutoPostBack="false"
                                    CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Edit Answer" Style="float:right"  ></asp:LinkButton>
                                <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("ID"))) %>' />
                                <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" AutoPostBack="False"
                                    ToolTip="Update" CssClass="ic-icon ic-save newSave" Style="display: none;" name="lnkUpdate" ></asp:LinkButton>
                                <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" AutoPostBack="False" CommandName="CancelRow"
                                    ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel" Style="display: none;" name="lnkcancel"></asp:LinkButton>
                                    </td>
                            </tr>
                        </table>
                        </ItemTemplate>
                </asp:DataList>--%>

                <asp:Panel ID="Panel1" runat="server">
                    <div style="text-align: center; margin-top: -28px; margin-bottom: 19px;">
                        <asp:Label ID="lblFolderNameText" CssClass="srLabel" runat="server" Font-Bold="true" Font-Size="X-Large"  >Folder Name :</asp:Label>
                        <asp:Label ID="lblFolderName" CssClass="srLabel lblfolderlog " runat="server" Font-Bold="true" Font-Size="X-Large"></asp:Label>
                    </div>
                    <div class="sr-main">
                        <div class="folder-logs-input">
                            <div class="sr1">
                                <asp:Label ID="lblName" runat="server" CssClass="srLabel"><b>UserName:</b></asp:Label>
                                <asp:TextBox ID="txtName" name="txtName" runat="server" CssClass="clsName" size="12" MaxLength="23" Height="30" Placeholder="Name" Style="margin-left: 5px;" />
                            </div>
                            <div class="sr2">
                                <asp:Label ID="lblDate" CssClass="srLabel" runat="server"><b >Date:</b></asp:Label>
                                <asp:TextBox ID="txtFDate" runat="server" CssClass="clsFDate" size="12" Height="30" Placeholder="From Date" Style="margin-left: 8px;" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender7" PopupPosition="Right" runat="server"
                                    TargetControlID="txtFDate" Format="MM-dd-yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:TextBox ID="txtTDate" runat="server" size="12" CssClass="clsTDate" Height="30" Placeholder="To Date" Style="margin-left: 8px;" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender6" PopupPosition="Right" runat="server"
                                    TargetControlID="txtTDate" Format="MM-dd-yyyy">
                                </ajaxToolkit:CalendarExtender>
                            </div>
                        </div>

                        <div class="folder-logs">
                            <div class="srbtn">
                                <asp:Button runat="server" CssClass="create-btn btn-small green search" ID="btnSearch" Text="Search" OnClick="btnSearch_Click" />

                                <asp:Button runat="server" CssClass="create-btn btn-small green clear" ID="btnClear" Text="Clear" OnClick="btnClear_Click" />
                            </div>

                            <asp:DropDownList CssClass="opt-select select newddl" runat="server" ID="ddlPage" OnSelectedIndexChanged="ddlPage_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>

                            <div class="srLogobtn">
                                <asp:Button ID="btnCreate" runat="server" Text="Click to Fill Log" CssClass="create-btn btn green create" Style="float: right; margin-bottom: 20px; height: 30px !important; line-height: 0px !important; width: 155px !important; font-size: 17px !important" />
                                <asp:Button runat="server" CssClass="create-btn btn-small exportToExcel" ID="btnExcel" Style="float: right; width: 40px;" ToolTip="Export to Excel" OnClick="btnExcel_Click" />
                                <asp:Button runat="server" CssClass="create-btn btn-small exportToPDF" ID="btnPDF" Style="float: right; width: 40px;" ToolTip="Export to PDF" OnClick="btnPDF_Click" />
                            </div>


                        </div>

                    </div>
                    <div class="clear"></div>





                    <asp:HiddenField ID="hdnInsert" runat="server" />
                    <div class="grid-resp">



                        <asp:GridView ID="gvQns" runat="server" AutoGenerateColumns="false" EnableViewState="false" ShowHeaderWhenEmpty="True" EmptyDataText="No records found." CssClass="questionGrid">
                            <Columns>
                                <asp:TemplateField AccessibleHeaderText="button">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" runat="server" Text="edit" CommandName="Edit1"
                                            CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Edit" />
                                          <asp:LinkButton ID="btnDelete" runat="server" Text="Delete" CommandName="Delete1"
                                            CssClass="ic-icon ic-delete delete" name="lnkDelete" ToolTip="Delete" />
                                        <asp:LinkButton ID="lnkUpdate" runat="server" CausesValidation="True" CommandName="Update1" AutoPostBack="False"
                                            ToolTip="Update" CssClass="ic-icon ic-save Save setWidth" Style="display: none" name="lnkUpdate"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkCancel" runat="server" CausesValidation="False" CommandName="Cancel1" AutoPostBack="False"
                                            ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel setWidth" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                <asp:BoundField DataField="Createdon" HeaderText="Createdon" ItemStyle-CssClass="CreatedOn" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </asp:Panel>

            </div>
        </div>
    </div>
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>

    <script language="javascript" type="text/javascript">

        var create = 0;
        var quesIdColumnMapping = [];
        var createUpdateTR;

        $('.edit').click(function (e) {
            e.preventDefault();
            showEdit(this);
        });
        $('.Save').click(function (e) {
            e.preventDefault();
            onSave(this);
        });
        $('.cancel').click(function (e) {
            e.preventDefault();
            onCancel(this);
        });

        $('.create').click(function (e) {
            e.preventDefault();
            onCreate(this);
        });
        $('.delete').click(function (e) {
            e.preventDefault();
            showDelete(this);
        });

        //$('.exportToExcel').click(function (e) {
        //    e.preventDefault();
        //    exportToExcel(this);
        //});


        //$('.search').click(function (e) {
        //    e.preventDefault();
        //    onSearch(this);
        //});



        //function onSearch() {
        //    var userName, fromDate, toDate, folderId, folderLogId;
        //    userName = $('.clsName').val();
        //    fromDate = $('.clsFDate').val();
        //    toDate = $('.clsTDate').val();
        //    folderId = $('#' + '<%=hdnID.ClientID%>').val();
        //    folderLogId = $('#' + '<%=hdnFolderLogId.ClientID%>').val();
        //    var obj = {};
        //    obj.userName = $.trim(userName);
        //    obj.fromDate = $.trim(fromDate);
        //    obj.toDate = $.trim(toDate);
        //    obj.folderId = $.trim(folderId);
        //    obj.folderLogId = $.trim(folderLogId);
        //    $.ajax({
        //        type: "POST",
        //        url: "CheckListQuestionsAndAnswers.aspx/SearchChecklistAns",
        //        data: JSON.stringify(obj),
        //        contentType: "application/json; charset=utf-8",
        //        dataType: "json",
        //        success: function (Responce) {
        //
        //        }
        //    });
        //}

        function onCreate(ele) {

            if (create == 1)
                return false;
            var new_line = '';
            var isFirstRecordCreate = false;

            if ($(".datatable").find('tr').length == 2) {
                if ($(".datatable").find('tr').eq(1).find('td').attr('colspan')) {
                    isFirstRecordCreate = true;
                }
            }

            if (isFirstRecordCreate) {
                new_line = "<tr id='newAnswer'></tr>";
                $(".datatable").append(new_line);

                var newColumn = "<td><a id='btnEdit' title='Edit' class='ic-icon ic-edit edit' name='lnkEdit' style='display: none'></a><a id='btnDelete' title='Delete' class='ic-icon ic-delete delete' name='lnkDelete'  style='display: none;'>Delete</a><a id=lnkUpdate title='Update' class='ic-icon ic-save Save setWidth' name='lnkUpdate' style='cursor:pointer'; ></a><a id=lnkCancel title='Cancel' class='ic-icon ic-cancel cancel setWidth' name='lnkcancel'  style='cursor:pointer';></a></td><td><%= Sessions.UserName %></td><td class='CreatedOn'></td>";
                $(".datatable").find('tr').eq(2).append(newColumn);

                $.each($(".datatable").find('th'), function (i, e) {
                    if (i > 2) {
                        var width = $(this).width() - 9;
                        var column = "<td><textarea style='width:" + width + "px;'></textarea></td>"
                        $(".datatable").find('tr').eq(2).append(column);
                    }
                });

                $(".datatable").find('tr').eq(2).find('.Save').click(function (e) {
                    e.preventDefault();
                    onSave(this);
                });
                $(".datatable").find('tr').eq(2).find('.cancel').click(function (e) {
                    e.preventDefault();
                    onCancel(this);
                });

                $(".datatable").find('tr').eq(2).find('.edit').click(function (e) {
                    e.preventDefault();
                    showEdit(this);
                });

                $(".datatable").find('tr').eq(2).find('.delete').click(function (e) {
                    e.preventDefault();
                    showDelete(this);
                });

                $(".datatable").find('tr').eq(1).remove();
            }
            else {
                new_line = $(".datatable").find('tr:eq(1)').clone();
                $(new_line).attr("id", "newAnswer");
                $(new_line).find('a').eq(0).hide();
                $(new_line).find('a').eq(1).hide();
                $(new_line).find('a').eq(2).show();
                $(new_line).find('a').eq(3).show();
            }

            $(new_line).find('.Save').click(function (e) {
                e.preventDefault();
                onSave(this);
            });
            $(new_line).find('.cancel').click(function (e) {
                e.preventDefault();
                onCancel(this);
            });
            $(new_line).find('.edit').click(function (e) {
                e.preventDefault();
                showEdit(this);
            });
            $(new_line).find('.delete').click(function (e) {
                e.preventDefault();
                showDelete(this);
            });

            var count = $(new_line).children('td').length;
            var cnt = 0;

            if (!isFirstRecordCreate) {
                for (i = 0; i < count ; i++) {
                    cnt += 1;
                    if (cnt == 3) {
                        $(new_line).find('td').eq(3).prev().empty();
                    }
                    if (cnt > 3) {
                        var span = $(new_line).children('td').eq(i).find('span');

                        var width = $('table').find('tr').find('th').eq(i).width();


                        var $input = $("<textarea>", {
                            value: span.text(),
                        });
                        span.replaceWith($input);
                        $(new_line).children('td').eq(i).find("textarea").val('');
                        $(new_line).children('td').eq(i).find("textarea").css("width", width - 9);
                    }
                }
                $('table > tbody > tr:first').after(new_line);
            }
            create = 1;
        }


        function showEdit(ele) {
            var gridView = $("[id*=gvQns]");
            var th = gridView.find("th");
            var button = $(ele).parents('tr').children('td').eq(0).find("a");
            var Edit = button.eq(0);
            var Delete = button.eq(1);
            var Update = button.eq(2);
            var cancel = button.eq(3);
            Update.show();
            cancel.show();
            Delete.hide();
            Edit.hide();
            var count = $(ele).parents('tr').children('td').length;

            //count = count - 3;

            var cnt = 0;

            for (i = 0; i < count ; i++) {
                cnt += 1;

                var span = $(ele).parents('tr').children('td').eq(i).find('span.clsAns');
                var width = $('table').find('tr').find('th').eq(i).width();

                if (cnt > 3) {
                    var $input = $("<textarea>", {
                        value: span.text(),
                    });
                    span.replaceWith($input);
                    $(ele).parents('tr').children('td').eq(i).find("textarea").css("width", width - 9);
                }
            }
        }

        function onCancel(ele) {
            var button = $(ele).parents('tr').children('td').eq(0).find("a");
            var Edit = button.eq(0);
            var Delete = button.eq(1);
            var Update = button.eq(2);
            var cancel = button.eq(3);
            Update.hide();
            cancel.hide();
            Delete.show();
            Edit.show();
            var row = $(ele).parents("tr");
            var count = $(ele).parents('tr').children('td').length;
            var cnt = 0;
            for (i = 0; i < count ; i++) {
                cnt += 1;
                var textArea = $(ele).parents('tr').children('td').eq(i).find('textarea');
                if (cnt > 3) {
                    var $input = $("<span>", {
                        text: textArea.val(),
                    });
                    textArea.replaceWith($input);
                    $(ele).parents('tr').children('td').eq(i).find('span').eq(0).attr('class', 'clsAns');
                }
            }
            if (create == 1) {
                $(ele).closest('tr').last().remove();
            }

            create = 0;
        }

        var isCreate = 0;

        function onSave(ele) {

            $(".clsFDate").val('');
            $(".clsTDate").val('');
            $(".clsName").val('');

            showLoader();
            createUpdateTR = ele;
            var createdOn;
            var questionId = null;
            var IDs = [];
            var button = $(ele).parents('tr').children('td').eq(0).find("a");
            var Edit = button.eq(0);
            var Delete = button.eq(1);
            var Update = button.eq(2);
            var cancel = button.eq(3);
            Update.hide();
            cancel.hide();
            Delete.show();
            Edit.show();
            var row = $(ele).parents("tr");
            var count = $(ele).parents('tr').children('td').length;
            var cnt = 0;
            var header = $('.Header');
            for (i = 0; i < count ; i++) {
                cnt += 1;
                var textArea = $(ele).parents('tr').children('td').eq(i).find('textarea');
                var Ans = textArea.val();
                if (cnt > 3) {
                    var $input = $("<span>", {
                        text: textArea.val(),
                    });
                    textArea.replaceWith($input);
                    $(ele).parents('tr').children('td').eq(i).find('span').eq(0).attr('class', 'clsAns');

                    //var questionId = $('table').find('tr').find('th').eq(i).find('img').attr('src');
                    var questionId = $('table').find('tr').find('th').eq(i).attr('abbr');
                    //alert(questionId);

                    var AnsID = $(ele).parents('tr').children('td').eq(i).find('span.clsAnsId').text();

                    if (AnsID == 0 || AnsID == "" || AnsID == undefined || AnsID == null)
                        quesIdColumnMapping.push({ questionId: questionId, columnCount: i });

                    createdOn = $(ele).parents('tr').find('.CreatedOn').html();

                    if (create == 1) {
                        isCreate = 1;
                        IDs.push({ queId: questionId, ansId: AnsID, ans: Ans, createdOn: null });
                    }
                    else {
                        //var createdDate = new Date(createdOn);
                        isCreate = 0;
                        //var splitDate = createdOn.split("-");
                        //var splitTime = createdOn.split(":");
                        //var formatDate = new Date(splitDate[2].split(/\s+/)[0], splitDate[1] - 1, splitDate[0], splitTime[0].split(/\s+/)[1], splitTime[1], splitTime[2]);
                        //var tickDate = formatDate.getTime();

                        //IDs.push({ queId: questionId, ansId: AnsID, ans: Ans, createdOn: tickDate.toString() });
                        IDs.push({ queId: questionId, ansId: AnsID, ans: Ans, createdOn: createdOn });
                    }
                }
            }
            var data = JSON.stringify(IDs);
            var folderId = $("#" + '<%= hdnID.ClientID %>').val();
                var folderLogId = $("#" + '<%= hdnFolderLogId.ClientID %>').val();

                $.ajax({
                    type: "POST",
                    url: "CheckListQuestionsAndAnswers.aspx/InsertDetails",
                    data: JSON.stringify({ ansData: data, isCreate: isCreate, folderId: folderId, folderLogId: folderLogId }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (result) {

                        if (result.d) {
                            if (result.d.length > 0) {
                                for (var i = 0; i < result.d.length; i++) {

                                    $.each(quesIdColumnMapping, function (index, e) {
                                        if (e.questionId == result.d[i]["queId"]) {

                                            if (create == 1) {
                                                var spanCreate = $('<span />').attr('style', 'display:none;').html(result.d[i]["ansId"]);
                                                spanCreate.attr('class', 'clsAnsId');
                                                $($(createUpdateTR).parents('tr').children('td').eq(e.columnCount).find('span')).after(spanCreate);
                                                $(createUpdateTR).parents('tr').children('td').eq(e.columnCount).find('span').eq(0).attr('class', 'clsAns');
                                            }
                                            else {
                                                $(createUpdateTR).parents('tr').children('td').eq(e.columnCount).find('span').eq(1).text(result.d[i]["ansId"])
                                            }
                                            $(createUpdateTR).parents('tr').find('.CreatedOn').html(result.d[i]["createdOn"]);
                                        }
                                    });
                                }
                            }
                            if (create == 1) {
                                updatePaging();
                            }
                        }

                        $(createUpdateTR).parents('tr').find('.Save').hide();
                        $(createUpdateTR).parents('tr').find('.cancel').hide();
                        $(createUpdateTR).parents('tr').find('.edit').show();
                        //createUpdateTR

                        // new create row # append span
                        $(".datatable").find('#newAnswer').removeAttr("id");
                        quesIdColumnMapping = [];
                        create = 0;

                        hideLoader();
                    }
                });
            }

            function updatePaging() {
                __doPostBack("<%=ddlPage.UniqueID %>", "UpdatePageNum");
            //$.ajax({
            //    type: "POST",
            //    url: "CheckListQuestionsAndAnswers.aspx/GetAnswerRowCount",
            //    contentType: "application/json; charset=utf-8",
            //    dataType: "json",
            //    success: function (result) {
            //        if (result.d) {
            //
            //            var ddlCount = $(".newddl").children('option').length;
            //            $('.newddl').empty();
            //
            //            var answerCount = parseInt(result.d);
            //            if (parseInt(answerCount) > 0) {
            //                var pageDiv = parseInt(answerCount) / 10;
            //                var pageMod = parseInt(answerCount) % 10;
            //
            //                if (pageMod != 0)
            //                    pageDiv = pageDiv + 1;
            //
            //                pageDiv = parseInt(pageDiv);
            //
            //                for (var j = 1; j <= pageDiv; j++)
            //                    $('.newddl').append($('<option></option>').val(j.toString()).html(j.toString()));
            //
            //                if ($(".newddl").children('option').length > 0)
            //                    $(".newddl").css('display', 'block');
            //                else
            //                    $(".newddl").css('display', 'none');
            //
            //                alert($(".newddl").children('option').length);
            //            }
            //
            //            //$(".newddl").val(1);
            //
            //            $('.newddl option:selected').attr("selected", null);
            //
            //            __doPostBack("<%=ddlPage.UniqueID %>", "UpdatePageNum");
            //            //document.getElementById('<%= ddlPage.ClientID %>').onchange();
            //        }
            //    }
            //});
        }

        function showDelete(ele)
        {
            if (confirm("Do you really want to delete answers?")) {
                var ansID = [];
                var count = $(ele).parents('tr').children('td').length;
                for (i = 0; i < count ; i++) {
                    if (i > 2) {
                        var ansId = $(ele).parents('tr').children('td').eq(i).find('span.clsAnsId').text();
                        ansID.push({ ansId: ansId })
                    }
                }
                var data = JSON.stringify(ansID);
                var folderId = $("#" + '<%= hdnID.ClientID %>').val();
            var folderLogId = $("#" + '<%= hdnFolderLogId.ClientID %>').val();

                $.ajax({
                    type: "POST",
                    url: "CheckListQuestionsAndAnswers.aspx/deleteAnswerDetails",
                    data: JSON.stringify({ ansId: data, folderId: folderId, folderLogId: folderLogId }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (result) {
                        alert("Answers deleted successfully.");
                        $(ele).parents('tr').remove();
                        updatePaging();
                    }
                });
            }
            return false;
           
        }

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');;
        }
        function hideLoader() {
            $('#overlay').hide();
        }

        //function showLoader() {
        //    $('#overlay').show().height($(document).height());
        //    $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');;
        //}
        //function hideLoader() {
        //    $('#overlay').hide();
        //}

        $(document).ready(function () {

            var width = 0;
            $.each($(".datatable").find('th'), function (i, e) {
                var thWidth = 200;
                width = width + thWidth;
            });

            $(".datatable").css("width", width);

            $(window).scroll(function () {
                $('#overlay:visible') && $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
            });
        });
    </script>

</asp:Content>

