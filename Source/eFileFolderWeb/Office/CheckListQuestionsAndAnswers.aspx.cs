﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using Shinetech.DAL;
using System.Web.Security;
using System.Globalization;
using System.Web.UI.WebControls.WebParts;
using System.DirectoryServices;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


//using System.Collections.Generic;
using System.Text;
//using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Services.Protocols;
//using System.Data;
using System.Configuration;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
//using System.Data;
using DataQuicker2.Framework;
//using Shinetech.DAL;
using Shinetech.Engines;
using Shinetech.Utility;
using System.IO;
using Account = Shinetech.DAL.Account;
using DocPage = Shinetech.DAL.Page;
using Shinetech.Engines.Adapters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Aspose.Cells;
using iTextSharp.text.pdf;
using iTextSharp.text;
//using System.Windows.Forms;





public partial class Office_CheckListQuestionsAndAnswers : System.Web.UI.Page
{
    string strID = string.Empty;
    string folderLogId = string.Empty;
    //public static DataTable dtExport
    //{
    //    get
    //    {
    //        if (HttpContext.Current.Session["dtExport"] == null)
    //            return null;
    //        else
    //            return (DataTable)(HttpContext.Current.Session["dtExport"]);
    //    }
    //    set
    //    {
    //        HttpContext.Current.Session["dtExport"] = value;
    //    }
    //}

    public static DataTable dtAnswer
    {
        get
        {
            if (HttpContext.Current.Session["dtAnswer"] == null)
                return null;
            else
                return (DataTable)(HttpContext.Current.Session["dtAnswer"]);
        }
        set
        {
            HttpContext.Current.Session["dtAnswer"] = value;
        }
    }

    public static DataTable dtQuestion
    {
        get
        {
            if (HttpContext.Current.Session["dtQuestion"] == null)
                return null;
            else
                return (DataTable)(HttpContext.Current.Session["dtQuestion"]);
        }
        set
        {
            HttpContext.Current.Session["dtQuestion"] = value;
        }
    }

    public static int PageIndex
    {
        get
        {
            if (HttpContext.Current.Session["PageIndex"] == null)
                return 0;
            else
                return Convert.ToInt32((HttpContext.Current.Session["PageIndex"]));
        }
        set
        {
            HttpContext.Current.Session["PageIndex"] = value;
        }
    }

    DataTable dtExport = null;
    //DataTable dtAnswer = null;
    DataSet dsQns = null;
    //DataTable dtQuestion = new DataTable();

    public class KeyValue
    {
        public string queId;
        public string ansId;
        public string ans;
        public string createdOn;
    }

    public class ans
    {
        public string ansId;
    }

    class TextColumn : ITemplate
    {
        public void InstantiateIn(System.Web.UI.Control container)
        {
            System.Web.UI.WebControls.TextBox txt = new System.Web.UI.WebControls.TextBox();
            //txt.ID = "MyTextBox";
            container.Controls.Add(txt);

        }
    }

    class GridViewTemplate : ITemplate
    {
        ListItemType _templateType;
        string _columnName, _columnName2;

        public GridViewTemplate(ListItemType type, string colname, string columnName2)
        {
            _templateType = type;
            _columnName = colname;
            _columnName2 = columnName2;
            //_columnName3 = columnName3;
        }


        public void InstantiateIn(System.Web.UI.Control container)
        {
            switch (_templateType)
            {
                case ListItemType.Item:
                    Label lbl2 = new Label();
                    lbl2.DataBinding += lbl2_DataBinding;
                    container.Controls.Add(lbl2);

                    Label lblAnsId = new Label();
                    lblAnsId.DataBinding += lblAnsId_DataBinding;
                    container.Controls.Add(lblAnsId);

                    //Label lblQuesId = new Label();
                    //lblQuesId.DataBinding += lblQuesId_DataBinding;
                    //container.Controls.Add(lblQuesId);
                    break;

            }
        }

        void lbl2_DataBinding(object sender, EventArgs e)
        {
            Label lbldata = (Label)sender;
            GridViewRow container = (GridViewRow)lbldata.NamingContainer;
            lbldata.Enabled = true;
            lbldata.Attributes.Add("style", "border:0px; text-align:center;");
            lbldata.CssClass = "clsAns";
            //object dataValue = DataBinder.Eval(container.DataItem, _columnName);
            object dataValue = DataBinder.GetPropertyValue(container.DataItem, _columnName);
            if (dataValue != DBNull.Value)
            {
                lbldata.Text = dataValue.ToString();
            }
        }


        void lblAnsId_DataBinding(object sender, EventArgs e)
        {
            Label lbldata = (Label)sender;
            GridViewRow container = (GridViewRow)lbldata.NamingContainer;
            lbldata.Attributes.Add("style", "display:none;");
            lbldata.CssClass = "clsAnsId";
            object dataValue = DataBinder.GetPropertyValue(container.DataItem, _columnName2);
            if (dataValue != DBNull.Value)
            {
                lbldata.Text = dataValue.ToString();
            }
        }

        //void lblQuesId_DataBinding(object sender, EventArgs e)
        //{
        //    Label lbldata = (Label)sender;
        //    GridViewRow container = (GridViewRow)lbldata.NamingContainer;
        //    lbldata.Attributes.Add("style", "display:none;");
        //    object dataValue = DataBinder.Eval(container.DataItem, _columnName3);
        //    if (dataValue != DBNull.Value)
        //    {
        //        lbldata.Text = dataValue.ToString();
        //    }
        //}
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        txtFDate.Attributes.Add("readonly", "readonly");
        txtTDate.Attributes.Add("readonly", "readonly");

        strID = Request.QueryString["ID"];
        ViewState["ID"] = strID;

        folderLogId = Request.QueryString["FolderLogId"];
        ViewState["FolderLogId"] = folderLogId;

        var argument = Request.Form["__EVENTARGUMENT"];
        if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
        {
            BindCheckListQuestionsandAnswers(strID, null, null, null, Convert.ToInt32(folderLogId), false);
        }
        if (Request["__EVENTARGUMENT"] != null && Request["__EVENTARGUMENT"] != "" && Request["__EVENTARGUMENT"].ToString().Contains("UpdatePageNum"))
        {
            UpdatePageNumber(dtAnswer);
            BindCheckListQuestionsandAnswers(strID, null, null, null, Convert.ToInt32(folderLogId),true);
        }

        argument = "";
        if (!IsPostBack)
        {
            if (!string.IsNullOrEmpty(strID))
            {
                dtAnswer = null;
                dtQuestion = null;
                PageIndex = 1;
                BindCheckListQuestionsandAnswers(strID, null, null, null, Convert.ToInt32(folderLogId), false);
            }
        }
    }
    //public void BindCheckListQuestionsandAnswers(string folderId)
    //{
    //    hdnID.Value = folderId;
    //    try
    //    {
    //        TemplateField tempField = null;
    //        dsQns = General_Class.GetCheckListQuestionsWithAnswers(folderId);
    //        dtQuestion = dsQns.Tables[0];
    //        dtAnswer = dsQns.Tables[1];
    //        if (dtAnswer != null)
    //        {


    //            for (int j = 0; j < dtQuestion.Rows.Count; j++)
    //            {
    //                tempField = new TemplateField();
    //                int columnId = Convert.ToInt32(dtQuestion.Rows[j]["ID"]);
    //                dtAnswer.Columns[columnId + "_ans"].ColumnName = Convert.ToString(dtQuestion.Rows[j]["Question"]);
    //                tempField.HeaderText = Convert.ToString(dtQuestion.Rows[j]["Question"]);
    //                tempField.ItemTemplate = new GridViewTemplate(ListItemType.Item, dtAnswer.Columns[Convert.ToString(dtQuestion.Rows[j]["Question"])].ColumnName, dtAnswer.Columns[Convert.ToString(columnId)].ColumnName);
    //                tempField.HeaderImageUrl = dtQuestion.Rows[j]["ID"].ToString();
    //                gvQns.Columns.Add(tempField);
    //            }
    //            DataView dvAnswer = new DataView(dtAnswer);
    //            dvAnswer.Sort = "CreatedOn DESC";
    //            gvQns.DataSource = dvAnswer;
    //            gvQns.DataBind();
    //            gvQns.Width = 1200;
    //            gvQns.RowStyle.CssClass = "even";
    //            gvQns.AlternatingRowStyle.CssClass = "odd";
    //            gvQns.CssClass = "datatable listing-datatable";
    //        }
    //    }
    //    catch (Exception ex)
    //    {

    //    }
    //}

    public void AddDeleteColumnOnGrid(bool isSearchorPageIndexChange)
    {
        TemplateField tempField = null;

       


        for (int j = 0; j < dtQuestion.Rows.Count; j++)
        {
            int columnId = Convert.ToInt32(dtQuestion.Rows[j]["ID"]);
            if (isSearchorPageIndexChange)
            {
                for (int i = 0; i < gvQns.Columns.Count; i++)
                {
                    if (gvQns.Columns[i].AccessibleHeaderText == Convert.ToString(columnId))
                    {
                        gvQns.Columns.Remove(gvQns.Columns[i]);
                    }
                }
            }

            if (dtAnswer.Columns[columnId + "_ans"] != null)
                dtAnswer.Columns[columnId + "_ans"].ColumnName = Convert.ToString(dtQuestion.Rows[j]["Question"]);

            tempField = new TemplateField();
            tempField.HeaderText = Convert.ToString(dtQuestion.Rows[j]["Question"]);
            tempField.ItemTemplate = new GridViewTemplate(ListItemType.Item, dtAnswer.Columns[Convert.ToString(dtQuestion.Rows[j]["Question"])].ColumnName, dtAnswer.Columns[Convert.ToString(columnId)].ColumnName);
            tempField.AccessibleHeaderText = dtQuestion.Rows[j]["ID"].ToString();
            gvQns.Columns.Add(tempField);
        }
    }


    public void BindCheckListQuestionsandAnswers(string folderId, string userName = null, DateTime? FDate = null, DateTime? TDate = null, int folderLogId = 0, bool isSearch = false, bool isPageIndexChange = false)
    {
        hdnID.Value = folderId;
        hdnFolderLogId.Value = Convert.ToString(folderLogId);
        try
        {
            //TemplateField tempField = null;

            if (!isPageIndexChange)
            {
                dsQns = General_Class.SearchCheckListQuestionsWithAnswers(folderId, userName, FDate, TDate, folderLogId);
                if (dsQns != null && dsQns.Tables.Count > 0)
                {
                    if (dsQns.Tables[0] != null && dsQns.Tables[0].Rows.Count > 0)
                    {
                        lblFolderName.Text = Convert.ToString(dsQns.Tables[0].Rows[0]["FolderName"]);


                        if (dsQns.Tables[1] != null && dsQns.Tables[1].Rows.Count > 0 && !Sessions.HasViewPrivilegeOnly)
                        {
                            dtQuestion = dsQns.Tables[1];
                            btnCreate.Visible = true;
                            btnPDF.Visible = true;
                            btnExcel.Visible = true;
                        }
                        else
                        {
                            btnCreate.Visible = false;
                            btnPDF.Visible = false;
                            btnExcel.Visible = false;
                        }

                        if (dsQns.Tables[1] != null && dsQns.Tables[1].Rows.Count > 0)
                        {
                            if (dsQns.Tables[2] != null)
                            {
                                dtAnswer = dsQns.Tables[2];

                                UpdatePageNumber(dtAnswer);

                                if (dtAnswer.Rows.Count > 0)
                                    PageIndex = 1;
                            }
                        }
                    }
                }
            }

           


            if (dtAnswer != null && dtQuestion != null)
            {    
                AddDeleteColumnOnGrid(isSearch);

                string rowFilter = string.Empty;
                rowFilter = "row <=" + PageIndex * 10 + "and row >= " + (PageIndex * 10 - 9);

                DataView dvAnswer = new DataView(dtAnswer);
                dvAnswer.RowFilter = rowFilter;
                dvAnswer.Sort = "CreatedOn DESC";
                gvQns.DataSource = dvAnswer;
            }
            else
                gvQns.DataSource = new DataTable();

            gvQns.DataBind();
            gvQns.RowStyle.CssClass = "even";
            gvQns.AlternatingRowStyle.CssClass = "odd";
            gvQns.CssClass = "datatable listing-datatable";

            ShowHidePaging();
        }
        catch (Exception ex)
        {
            gvQns.DataSource = new DataTable();
            gvQns.DataBind();
            gvQns.RowStyle.CssClass = "even";
            gvQns.AlternatingRowStyle.CssClass = "odd";
            gvQns.CssClass = "datatable listing-datatable";

            ShowHidePaging();
        }
    }

    private void PrepareTableForExport()
    {
        dtExport = new System.Data.DataTable();
        dtExport.Columns.Add("Name", typeof(string));
        dtExport.Columns.Add("Createdon", typeof(DateTime));

        List<string> lstQuestion = new System.Collections.Generic.List<string>();
        lstQuestion.Add("Name");
        lstQuestion.Add("Createdon");

        if (dtQuestion != null && dtQuestion.Rows.Count > 0)
        {
            for (int i = 0; i < dtQuestion.Rows.Count; i++)
            {
                dtExport.Columns.Add(dtQuestion.Rows[i]["Question"].ToString(), typeof(string));
                lstQuestion.Add(dtQuestion.Rows[i]["Question"].ToString());
            }
        }
        if (dtAnswer != null)
            dtExport = dtAnswer.DefaultView.ToTable(false, lstQuestion.ToArray());

        //response = Response;
    }

    public void ShowHidePaging()
    {
        if (ddlPage.Items.Count > 0)
            ddlPage.Attributes.Add("style", "display:block;");
        else
            ddlPage.Attributes.Add("style", "display:none");
    }

    public void UpdatePageNumber(DataTable dtAnswer)
    {
        int ddlCount = ddlPage.Items.Count;
        for (int i = 1; i <= ddlCount; i++)
            ddlPage.Items.Remove(Convert.ToString(i));

        if (dtAnswer.Rows.Count > 0)
        {
            int pageDiv = dtAnswer.Rows.Count / 10;
            int pageMod = dtAnswer.Rows.Count % 10;

            if (pageMod != 0)
                pageDiv = pageDiv + 1;

            for (int i = 1; i <= pageDiv; i++)
                ddlPage.Items.Add(Convert.ToString(i));
        }
    }

    [WebMethod]
    //[SoapHeader("authenticationInfo")]
    public static List<KeyValue> InsertDetails(string ansData, string isCreate, string folderId, string folderLogId)
    {
        DataTable dtAnswer1 = new DataTable();
        dtAnswer1.Columns.Add("AnsId", typeof(int));
        dtAnswer1.Columns.Add("AnsValue", typeof(string));
        dtAnswer1.Columns.Add("QueId", typeof(int));
        dtAnswer1.Columns.Add("CreatedDate", typeof(DateTime));

        //Dictionary<string, string> values = JsonConvert.DeserializeObject<Dictionary<string, string>>(ansData);
        var list = JsonConvert.DeserializeObject<List<KeyValue>>(ansData);

        DateTime datCurrentDate = DateTime.Now;

        var createdOn = "";
        List<KeyValue> lstAns = new List<KeyValue>();
        foreach (var item in list)
        {
            if (item.ansId == "")
                item.ansId = "0";

            if (!string.IsNullOrEmpty(item.ans))
                dtAnswer1.Rows.Add(new object[] { item.ansId, item.ans, item.queId, !string.IsNullOrEmpty(item.createdOn) ? Convert.ToDateTime(Convert.ToDateTime(item.createdOn).ToString("yyyy-MM-dd HH:mm:ss.fff")) : datCurrentDate });
            else
            {
                KeyValue objKeyValue = new KeyValue();
                objKeyValue.ansId = "0";
                objKeyValue.queId = item.queId;
                objKeyValue.ans = "";
                objKeyValue.createdOn = item.createdOn;
                lstAns.Add(objKeyValue);
            }
        }

        DataSet dsAnswer = General_Class.QnsInsetandUpdate(dtAnswer1, isCreate, DateTime.Now, new Guid(Sessions.SwitchedSessionId), folderId, Convert.ToInt32(folderLogId));
        //DataSet dsAnswer = General_Class.QnsInsetandUpdate(dtAnswer1, isCreate, DateTime.Now, new Guid(Sessions.UserId), folderId, Convert.ToInt32(folderLogId));
        if (dsAnswer != null && dsAnswer.Tables.Count > 0 && dsAnswer.Tables[0] != null)
        {
            if (dsAnswer.Tables.Count > 2 && dsAnswer.Tables[0].Rows.Count > 0)
            {
                List<KeyValue> lstAnsInsert = new List<KeyValue>();
                lstAnsInsert = dsAnswer.Tables[0].AsEnumerable().Select(row => new KeyValue
                {
                    ansId = Convert.ToString(row.Field<Int32>("ansID")),
                    queId = Convert.ToString(row.Field<Int32>("quesId")),
                    ans = Convert.ToString(row.Field<string>("answer")),
                    //createdOn = row.Field<DateTime>("createdOn").ToString("dd-MM-yyyy HH:mm:ss.fff")
                    createdOn = row.Field<DateTime>("createdOn").ToString("yyyy-MM-dd HH:mm:ss.fff")
                }).ToList();
                lstAns.AddRange(lstAnsInsert);
            }

            if (dsAnswer.Tables[1] != null)
                dtQuestion = dsAnswer.Tables[1];

            if (dsAnswer.Tables[2] != null)
            {
                dtAnswer = new DataTable();
                dtAnswer = dsAnswer.Tables[2];

                if (dtAnswer.Rows.Count <= 10)
                    PageIndex = 1;
            }
            //for (int j = 0; j < dtQuestion.Rows.Count; j++)
            //{
            //    int columnId = Convert.ToInt32(dtQuestion.Rows[j]["ID"]);
            //    dtAnswer.Columns[columnId + "_ans"].ColumnName = Convert.ToString(dtQuestion.Rows[j]["Question"]);
            //}
        }

        return lstAns;
    }

    [WebMethod]
    public static string GetAnswerRowCount()
    {
        if (dtAnswer.Rows.Count > 0)
            return Convert.ToString(dtAnswer.Rows.Count);
        else
            return null;
    }

    public void BindGrid(string folderId)
    {
        hdnID.Value = folderId;
        try
        {
            //DataTable dtAnswer = CheckListQuestion.GetCheckListQuestionAnswer1(folderId);
            //dtAnswer = General_Class.GetCheckListQuestionsWithAnswers(folderId);
            //dtAnswer.Columns.Add(new DataColumn { ColumnName = "Edit" });            

            TemplateField tempField = null;

            //gvQns.Columns.Add(tempField);

            //for (int i = 0; i < dtAnswer.Columns.Count; i++)
            //{
            //    if (dtAnswer.Columns[i].ColumnName.ToLower() != "createddateunix")
            //    {
            //        tempField = new TemplateField();
            //        tempField.HeaderTemplate = new GridViewTemplate(ListItemType.Header, dtAnswer.Columns[i].ColumnName.ToString());
            //        tempField.ItemTemplate = new GridViewTemplate(ListItemType.Item, dtAnswer.Columns[i].ColumnName.ToString());
            //        gvQns.Columns.Add(tempField);
            //    }
            //}

            //GridView gvQns = null;
            if (dtAnswer != null)
            {
                DataView dvQns = new DataView(dtAnswer);
                dvQns.Sort = "CreatedOn DESC";
                gvQns.DataSource = dvQns;
                gvQns.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }



    //public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
    //{
    //     Unix timestamp is seconds past epoch
    //    System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
    //    dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
    //    return dtDateTime;
    //}

    [WebMethod]
    public static void deleteAnswerDetails(string ansId, string folderId, string folderLogId)
    {
        string strAnsId = string.Empty;
        string success = string.Empty;
        DataTable dtAnswer = new DataTable();
        dtAnswer.Columns.Add("Id", typeof(int));
        var lstAnsID = JsonConvert.DeserializeObject<List<ans>>(ansId);
        foreach (var item in lstAnsID)
        {
            if (!string.IsNullOrEmpty(item.ansId))
            {
                dtAnswer.Rows.Add(new object[] { item.ansId });
            }
        }
        if (dtAnswer != null && dtAnswer.Rows.Count > 0)
        {
           General_Class.deleteAnswer(dtAnswer);
        }
    }


    public static double DateTimeToUnixTimestamp(DateTime dateTime)
    {
        return (TimeZoneInfo.ConvertTimeToUtc(dateTime) -
               new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc)).TotalSeconds;
    }

    //where DATEADD(ms, -DATEPART(ms, CLA.CreatedOn), CLA.CreatedOn) = @DateToCompare
    //Declare @DateToCompare datetime
    //select @DateToCompare  = DATEADD(second,@createdOn, CAST('1970-01-01 00:00:00' AS datetime))

    //protected void gvQns_RowEditing(object sender, GridViewEditEventArgs e)
    //{
    //    gvQns.EditIndex = e.NewEditIndex;
    //    GridViewRow row = gvQns.Rows[e.NewEditIndex];
    //    row.Cells[1].Enabled = false;
    //    row.Cells[2].Enabled = false;
    //    BindGrid(Convert.ToString(ViewState["ID"]));

    //}
    //protected void gvQns_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    //{
    //    gvQns.EditIndex = -1;
    //    BindGrid(Convert.ToString(ViewState["ID"]));
    //}


    //protected void gvQns_RowUpdating(object sender, GridViewUpdateEventArgs e)
    //{
    //    var i = e.RowIndex;
    //    GridViewRow row = gvQns.Rows[e.RowIndex];
    //    string Date = ((TextBox)(row.Cells[2].Controls[0])).Text;
    //}


    //protected void gvQns_RowUpdating1(object sender, GridViewUpdateEventArgs e)
    //{


    //    string name = e.NewValues[0].ToString();
    //    DataSet dsDetails = new DataSet();
    //    DataTable dtAnsDetais = new DataTable();
    //    DataTable dtQnsDetais = new DataTable();
    //    DataTable dt = new DataTable();

    //    dsDetails = General_Class.GetQns(Convert.ToInt32(gvQns.DataKeys[e.RowIndex].Value), name, Convert.ToString(ViewState["ID"]));

    //    dtAnsDetais = dsDetails.Tables[0];
    //    dtQnsDetais = dsDetails.Tables[1];


    //    //if (gvQns.EditIndex == 0)
    //    //{
    //    if (hdnInsert.Value == "1")
    //    {
    //        dt.Columns.Add("UserID", typeof(Guid));
    //        dt.Columns.Add("FK_QuestionID", typeof(int));
    //        dt.Columns.Add("Answer", typeof(string));
    //        dt.Columns.Add("CreatedOn", typeof(DateTime));
    //        var datetime = DateTime.Now;

    //        for (int i = 0; i < dtQnsDetais.Rows.Count; i++)
    //        {
    //            for (int j = 0; j < dtQnsDetais.Rows.Count; j++)
    //            {
    //                if (gvQns.HeaderRow.Cells[j + 3].Text == dtQnsDetais.Rows[i]["Question"].ToString())
    //                {
    //                    DataRow dr = dt.NewRow();
    //                    dr["UserID"] = Membership.GetUser().ProviderUserKey.ToString();
    //                    dr["FK_QuestionID"] = dtQnsDetais.Rows[i]["ID"].ToString();
    //                    //dr["Answer"] = e.NewValues[j + 3].ToString();
    //                    dr["Answer"] = ((TextBox)(gvQns.Rows[e.RowIndex] as GridViewRow).Cells[j + 3].Controls[0]).Text;
    //                    dr["CreatedOn"] = datetime;
    //                    dt.Rows.Add(dr);
    //                }
    //            }
    //        }
    //        if (dt != null)
    //        {
    //            CheckListQuestion.AddAnswer(dt);
    //        }
    //        hdnInsert.Value = "0";
    //        gvQns.EditIndex = -1;
    //        BindGrid(Convert.ToString(ViewState["ID"]));
    //    }
    //    else
    //    {
    //        //string date = e.NewValues[1].ToString();

    //        //DateTime dtOldDate;
    //        //dtOldDate = DateTime.ParseExact(date, "dd-MM-yyyy HH:mm:ss",null);
    //        //date = dtOldDate.ToString("yyyy-MM-dd HH:mm:ss.fff");

    //        if (dtQnsDetais.Rows.Count == dtAnsDetais.Rows.Count)
    //        {
    //            if (dtAnsDetais != null && dtAnsDetais.Rows.Count > 0)
    //            {
    //                for (int i = 0; i < dtAnsDetais.Rows.Count; i++)
    //                {
    //                    string qnsId = Convert.ToString(dtAnsDetais.Rows[i]["ID"]);
    //                    for (int j = 0; j < dtAnsDetais.Rows.Count; j++)
    //                    {
    //                        if (gvQns.HeaderRow.Cells[j + 3].Text == dtAnsDetais.Rows[i]["Question"].ToString())
    //                        {
    //                            string ans = ((TextBox)(gvQns.Rows[e.RowIndex] as GridViewRow).Cells[j + 3].Controls[0]).Text;
    //                            General_Class.UpdateQns(ans, qnsId);
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //        else
    //        {
    //            for (int i = 0; i < dtQnsDetais.Rows.Count; i++)
    //            {
    //                for (int j = 0; j < dtAnsDetais.Rows.Count; j++)
    //                {
    //                    string qnsId = Convert.ToString(dtAnsDetais.Rows[j]["ID"]);
    //                    if (dtQnsDetais.Rows[i]["Question"].ToString() == dtAnsDetais.Rows[j]["Question"].ToString())
    //                    {
    //                        for (int k = 0; k < dtQnsDetais.Rows.Count; k++)
    //                        {

    //                            if (gvQns.HeaderRow.Cells[k + 3].Text == dtAnsDetais.Rows[j]["Question"].ToString())
    //                            {
    //                                string ans = ((TextBox)(gvQns.Rows[e.RowIndex] as GridViewRow).Cells[k + 3].Controls[0]).Text;
    //                                General_Class.UpdateQns(ans, qnsId);
    //                            }
    //                            else
    //                            {
    //                                string ans = ((TextBox)(gvQns.Rows[e.RowIndex] as GridViewRow).Cells[k + 3].Controls[0]).Text;
    //                                string que = gvQns.HeaderRow.Cells[k + 3].Text;
    //                                General_Class.InsertQns(ans, que, Convert.ToInt32(ViewState["ID"]), Sessions.UserId, Convert.ToInt32(gvQns.DataKeys[e.RowIndex].Value));
    //                                //General_Class.UpdateQns(ans, qnsId);
    //                            }
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //        gvQns.EditIndex = -1;
    //        BindGrid(Convert.ToString(ViewState["ID"]));
    //    }
    //}

    //protected void gvQns_RowCreated(object sender, GridViewRowEventArgs e)
    //{

    //    if (sender.ToString().Contains("create"))
    //    {
    //        gvQns.EditIndex = 0;
    //    }

    //    //string Index1 = Convert.ToString(e.Row.RowIndex);
    //    //string Index2 = Convert.ToString(gvQns.EditIndex);

    //    if (gvQns.EditIndex == e.Row.RowIndex)
    //    {
    //        //var firstCell = e.Row.Cells[0];
    //        //firstCell.Controls.Clear();
    //        //Button btn_Check = new Button();
    //        //btn_Check.ID = "Create";
    //        //btn_Check.Text = firstCell.Text;
    //        //btn_Check.Click += btn_Check_Click;
    //        //firstCell.Controls.Add(btn_Check);

    //        if (hdnInsert.Value == "1")
    //        {
    //            gvQns.EditIndex = 0;
    //        }

    //        //Loop through all Cells in the Row.
    //        int index = 0;
    //        foreach (TableCell cell in e.Row.Cells)
    //        {
    //            if (cell.Controls.Count > 0)
    //            {
    //                //Check whether Cell has TextBox.
    //                if (cell.Controls[0] is TextBox)
    //                {
    //                    TextBox textBox = cell.Controls[0] as TextBox;
    //                    if (index == 1)
    //                    {
    //                        textBox.Text = Sessions.UserName;
    //                    }
    //                    if (index > 2)
    //                        textBox.TextMode = TextBoxMode.MultiLine;
    //                    else
    //                    {
    //                        textBox.ReadOnly = true;
    //                        textBox.Attributes.Add("style", "border:0px; text-align:center;");
    //                    }

    //                }
    //            }
    //            index++;
    //        }
    //    }

    //if (gvQns.EditIndex == 0)
    //{
    //    int index = 0;
    //    foreach (TableCell cell in e.Row.Cells)
    //    {
    //        if (cell.Controls.Count > 0)
    //        {
    //            //Check whether Cell has TextBox.
    //            if (cell.Controls[0] is TextBox)
    //            {
    //                TextBox textBox = cell.Controls[0] as TextBox;
    //                if (index > 2)
    //                    textBox.TextMode = TextBoxMode.MultiLine;
    //                else
    //                {
    //                    if (index == 1)
    //                    {
    //                        textBox.Text = Sessions.UserName;
    //                    }
    //                    textBox.ReadOnly = true;
    //                    textBox.Attributes.Add("style", "border:0px; text-align:center;");
    //                }

    //            }
    //        }
    //        index++;
    //    }
    //}


    //}

    ////private void AddLinkButton(GridViewRow row)
    ////{
    ////    TableCell cellWithLink = new TableCell();
    ////    LinkButton lnk = new LinkButton();
    ////    lnk.Command += new CommandEventHandler(CheckOut);

    ////    if (row.RowType == DataControlRowType.DataRow)
    ////    {
    ////        String.Empty Then 
    ////        if (row.Cells[0].Text != "5")
    ////        {
    ////            lnk.Text = "Check In";
    ////            cellWithLink.Controls.Add(lnk);
    ////            row.Cells.Add(cellWithLink);
    ////        }
    ////    }
    ////}


    //protected void btnCreate_Click(object sender, EventArgs e)
    //{
    //    int pos = (gvQns.PageIndex * gvQns.PageSize);
    //    gvQns.EditIndex = pos;
    //    hdnInsert.Value = "1";
    //    DataRow dr;

    //    DataTable dtAnswer = General_Class.GetCheckListQuestionsWithAnswers(hdnID.Value);
    //        foreach (GridViewRow gvr in gvQns.Rows)
    //        {
    //            dr = dtAnswer.NewRow();

    //            dr[0] = Sessions.UserName;
    //            dtAnswer.Rows.Add(dr);
    //        }

    //    //TextBox newtext = new TextBox();
    //    //TemplateField tempField = new TemplateField();
    //    //tempField.HeaderText = "Test1234";
    //    //tempField.ItemTemplate = new TextColumn();
    //    //gvQns.Columns.Add(tempField);

    //    //tempField.ItemTemplate  = new 
    //    //.Add(tempField);
    //    //if (e.Row.RowType == DataControlRowType.DataRow)
    //    //{
    //    //    storid = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "stor_id").ToString());
    //    //}
    //}



    ////TextBox txtCountry = new TextBox();
    ////txtCountry.ID = "t";
    ////txtCountry.Text =
    ////e.Row.Cells[1].Controls.Add(txtCountry);

    ////GridViewRow grdRow = (GridViewRow)bt.Parent.Parent;
    ////gvQns.Controls[0].Controls.AddAt(0, HeaderGridRow);

    //protected void gvQns_RowDeleting(object sender, GridViewDeleteEventArgs e)
    //{
    //    int intRow = e.RowIndex;
    //    string createdon = ((TextBox)(gvQns.Rows[e.RowIndex] as GridViewRow).Cells[intRow + 1].Controls[0]).Text;

    //}
    //protected void gvQns_RowDataBound(object sender, GridViewRowEventArgs e)
    //{


    //    int intRow = e.Row.Cells.Count;
    //    string createdon = ((TextBox)(gvQns.Rows[e.Row] as GridViewRow).Cells[intRow + 1].Controls[0]).Text;

    //       if (e.Row.RowType == DataControlRowType.DataRow)
    //       {
    //           LinkButton lb = (LinkButton)e.Row.Cells[1].Controls[1];

    //           if (lb != null)
    //           {
    //               lb.Attributes.Add("onclick", "javascript:return " +
    //    "confirm('Are you sure you want to delete this record ')");
    //           }
    //       }
    //}
    //protected void gvQns_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    if (e.CommandName == "Edit")
    //    {
    //        Page.ClientScript.RegisterStartupScript(this.GetType(), "Call my function", "return confirm('Really wanna delete?');", true);
    //    }
    //    //if (e.CommandName = "Edit")
    //    //{

    //    //}
    //}
    //protected void gvQns_RowUpdating(object sender, GridViewUpdateEventArgs e)
    //{
    //    string name = e.NewValues[0].ToString();
    //    DataSet dsDetails = new DataSet();
    //    DataTable dtAnsDetais = new DataTable();
    //    DataTable dtQnsDetais = new DataTable();
    //    DataTable dt = new DataTable();

    //    dsDetails = General_Class.GetQns(Convert.ToInt32(gvQns.DataKeys[e.RowIndex].Value), name, Convert.ToString(ViewState["ID"]));

    //    dtAnsDetais = dsDetails.Tables[0];
    //    dtQnsDetais = dsDetails.Tables[1];


    //    if (gvQns.EditIndex == 0)
    //    {
    //    if (hdnInsert.Value == "1")
    //    {
    //        dt.Columns.Add("UserID", typeof(Guid));
    //        dt.Columns.Add("FK_QuestionID", typeof(int));
    //        dt.Columns.Add("Answer", typeof(string));
    //        dt.Columns.Add("CreatedOn", typeof(DateTime));
    //        var datetime = DateTime.Now;

    //        for (int i = 0; i < dtQnsDetais.Rows.Count; i++)
    //        {
    //            for (int j = 0; j < dtQnsDetais.Rows.Count; j++)
    //            {
    //                if (gvQns.HeaderRow.Cells[j + 3].Text == dtQnsDetais.Rows[i]["Question"].ToString())
    //                {
    //                    DataRow dr = dt.NewRow();
    //                    dr["UserID"] = Membership.GetUser().ProviderUserKey.ToString();
    //                    dr["FK_QuestionID"] = dtQnsDetais.Rows[i]["ID"].ToString();
    //                    dr["Answer"] = e.NewValues[j + 3].ToString();
    //                    dr["Answer"] = ((TextBox)(gvQns.Rows[e.RowIndex] as GridViewRow).Cells[j + 3].Controls[0]).Text;
    //                    dr["CreatedOn"] = datetime;
    //                    dt.Rows.Add(dr);
    //                }
    //            }
    //        }
    //        if (dt != null)
    //        {
    //            CheckListQuestion.AddAnswer(dt);
    //        }
    //        hdnInsert.Value = "0";
    //        gvQns.EditIndex = -1;
    //        BindGrid(Convert.ToString(ViewState["ID"]));
    //    }
    //}

    //protected void gvQns_RowCreated(object sender, GridViewRowEventArgs e)
    //{
    //    for (int i = 0; i < dtAnswer.Rows.Count; i++)
    //    {
    //        if(gvQns.ro)
    //    }
    //}
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        PageIndex = 1;
        DateTime? fDate;
        DateTime? tDate;
        if (!string.IsNullOrEmpty(txtFDate.Text))
            txtFDate.Text = txtFDate.Text.Replace('-', '/');
        if (!string.IsNullOrEmpty(txtTDate.Text))
            txtTDate.Text = txtTDate.Text.Replace('-', '/');
        fDate = (!string.IsNullOrEmpty(txtFDate.Text) ? DateTime.ParseExact(txtFDate.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture) : (DateTime?)null);
        tDate = (!string.IsNullOrEmpty(txtTDate.Text) ? DateTime.ParseExact(txtTDate.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture) : (DateTime?)null);
        BindCheckListQuestionsandAnswers(hdnID.Value, txtName.Text,
                                        (fDate),
                                        tDate,
                                        Convert.ToInt32(folderLogId), true);
    }
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        PrepareTableForExport();

        DataTable dtExcel = dtExport;
        StringBuilder csv = new StringBuilder();

        if (dtExport != null)
        {
            string columnName = string.Empty;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<table>");
            sb.AppendLine("<tr>");

            for (int i = 0; i < dtExport.Columns.Count; i++)
            {
                sb.AppendLine("<td><b>" + dtExport.Columns[i].ColumnName + "</b></td>");
            }

            sb.AppendLine("</tr>");

            for (int i = 0; i < dtExport.Rows.Count; i++)
            {
                sb.AppendLine("<tr>");
                for (int j = 0; j < dtExport.Columns.Count; j++)
                {
                    if (j == 1)
                    {
                        string questionDate = Convert.ToDateTime(dtExport.Rows[i][j]).ToString("dd MMM yyyy HH:mm:ss");
                        sb.AppendLine("<td>" + questionDate + "</td>");
                    }
                    else
                        sb.AppendLine("<td>" + dtExport.Rows[i][j].ToString() + "</td>");
                }
                sb.AppendLine("</tr>");
            }

            sb.AppendLine("</table>");

            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=FolderLogQuestion" + DateTime.Now.Ticks.ToString() + ".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            Response.Output.Write(sb.ToString());
            Response.Flush();
            Response.End();

        }
    }
    protected void btnPDF_Click(object sender, EventArgs e)
    {
        PrepareTableForExport();

        iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4, 88f, 88f, 10f, 10f);
        iTextSharp.text.Font NormalFont = FontFactory.GetFont("Arial", 12, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLACK);
        using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
        {
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
            document.Open();
            iTextSharp.text.Font font5 = iTextSharp.text.FontFactory.GetFont(FontFactory.HELVETICA, 5);

            PdfPTable table = new PdfPTable(dtExport.Columns.Count);
            PdfPRow row = null;
            //float[] widths = new float[] { 4f, 4f, 4f, 4f };

            List<float> newWidth = new List<float>();
            for (int i = 0; i < dtExport.Columns.Count; i++)
                newWidth.Add(4f);

            float[] widths = newWidth.ToArray();

            table.SetWidths(widths);

            table.WidthPercentage = 100;
            int iCol = 0;
            string colname = "";
            PdfPCell cell = new PdfPCell(new Phrase("Question-Answer"));

            cell.Colspan = dtExport.Columns.Count;

            foreach (DataColumn c in dtExport.Columns)
            {
                table.AddCell(new Phrase(c.ColumnName, font5));
            }

            foreach (DataRow r in dtExport.Rows)
            {
                if (dtExport.Rows.Count > 0)
                {
                    for (int i = 0; i < dtExport.Columns.Count; i++)
                    {
                        table.AddCell(new Phrase(r[i].ToString(), font5));
                    }
                    //table.AddCell(new Phrase(r[0].ToString(), font5));
                    //table.AddCell(new Phrase(r[1].ToString(), font5));
                    //table.AddCell(new Phrase(r[2].ToString(), font5));
                    //table.AddCell(new Phrase(r[3].ToString(), font5));
                }
            } document.Add(table);
            document.Close();

            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=FolderLogQuestion" + DateTime.Now.Ticks.ToString() + ".pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();
        }
    }
    //protected void gvQns_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
    //{
    //    gvQns.PageIndex = e.NewPageIndex;
    //    BindCheckListQuestionsandAnswers(hdnID.Value, null, null, null, Convert.ToInt32(folderLogId), true);
    //}
    protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        PageIndex = Convert.ToInt32(ddlPage.SelectedValue);
        BindCheckListQuestionsandAnswers(hdnID.Value, null, null, null, Convert.ToInt32(folderLogId), true, true);
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtFDate.Text = "";
        txtName.Text = "";
        txtTDate.Text = "";
        PageIndex = 1;
        BindCheckListQuestionsandAnswers(strID, null, null, null, Convert.ToInt32(folderLogId), false,true);
    }
}


