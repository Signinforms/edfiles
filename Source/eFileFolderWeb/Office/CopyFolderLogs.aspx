﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CopyFolderLogs.aspx.cs" Inherits="Office_CopyFolderLogs" MasterPageFile="~/MasterPage.master"%>

<%@ Import Namespace="System.Data" %>
<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function pageLoad()
        {
            $('.edFileLink').click(function () {
                ClearFolderIdSource();
            });
        }

        function ClearFolderIdSource() {
            $.ajax(
                    {
                        type: "POST",
                        url: '../EFileFolderJSONService.asmx/ClearFolderIdSource',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json"
                    });
        } 
    </script>
    <div id="overlay" style="display: none">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Folder Transfer Logs</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="inner-wrapper">
        <div class="page-container" style="margin-left:10%">
            <div class="dashboard-container">
                <div class="left-content">
                    <div>

                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table style="margin: 0px 20px 0px 30px; width: 925px">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GridView6" CssClass="datatable listing-datatable" runat="server"
                                                    OnSorting="GridView6_Sorting"
                                                    AllowSorting="True" AutoGenerateColumns="false">
                                                    <PagerSettings Visible="False" />
                                                    <AlternatingRowStyle CssClass="odd" />
                                                    <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                    <Columns>
                                                        <asp:BoundField DataField="FromUser" HeaderText="From" SortExpression="FromUser" ItemStyle-Width="5%" />
                                                        <asp:BoundField DataField="ToUser" HeaderText="To" SortExpression="ToUser" ItemStyle-Width="5%" />
                                                        <%--<asp:BoundField DataField="FolderName" HeaderText="Folder Name" SortExpression="FolderName" ItemStyle-Width="5%" />--%>
                                                        <asp:HyperLinkField DataTextField="FolderName" HeaderText="Folder Name" DataNavigateUrlFields="newFolderID" ItemStyle-Width="5%"
                                                            DataNavigateUrlFormatString="~/Office/FileFolderBox.aspx?id={0}" SortExpression="FolderName" ControlStyle-CssClass="edFileLink">
                                                            <HeaderStyle Width="190px" />
                                                        </asp:HyperLinkField>
                                                        <asp:BoundField DataField="CreatedDate" HeaderText="Transfer Date" SortExpression="CreatedDate" ItemStyle-Width="5%" />
                                                        <%--<asp:BoundField DataField="FromUser" HeaderText="Transfered By" SortExpression="FromUser" ItemStyle-Width="5%" />--%>
                                                    </Columns>
                                                    <EmptyDataTemplate>
                                                        <asp:Label ID="Label2" runat="server" CssClass="Empty" />There is no record.
                                                    </EmptyDataTemplate>
                                                </asp:GridView>
                                                <!-- Tabs Editor Panel-->

                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="prevnext" bgcolor="#f8f8f5">
                                                <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                                    PageSize="15" OnPageSizeChanged="WebPager1_PageSizeChanged" CssClass="prevnext"
                                                    PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1"></cc1:WebPager>
                                                <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
