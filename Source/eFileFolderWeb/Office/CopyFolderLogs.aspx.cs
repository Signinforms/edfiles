﻿using Shinetech.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Office_CopyFolderLogs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
    }

    public DataTable CopyFolderLogs
    {
        get
        {
            return this.Session["CopyFolderLogs"] as DataTable;
        }
        set
        {
            this.Session["CopyFolderLogs"] = value;
        }
    }

    private void GetData()
    {
        CopyFolderLogs = General_Class.GetCopyFolderLogs(Sessions.SwitchedSessionId);
        //CopyFolderLogs = General_Class.GetCopyFolderLogs(Membership.GetUser().ProviderUserKey.ToString());
    }

    private void BindGrid() 
    {
        GridView6.DataSource = this.CopyFolderLogs.AsDataView();
        GridView6.DataBind();
        WebPager1.DataSource = this.CopyFolderLogs;
        WebPager1.DataBind();
    }

    private SortDirection Folder_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void GridView6_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Folder_Sort_Direction == SortDirection.Ascending)
        {
            this.Folder_Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Folder_Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.CopyFolderLogs);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.CopyFolderLogs = Source.ToTable();
        BindGrid();
    }

    protected void WebPager1_PageIndexChanged(object sender, Shinetech.Framework.Controls.PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.CopyFolderLogs;
        WebPager1.DataBind();
    }
    protected void WebPager1_PageSizeChanged(object s, Shinetech.Framework.Controls.PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.CopyFolderLogs;
        WebPager1.DataBind();
    }
}