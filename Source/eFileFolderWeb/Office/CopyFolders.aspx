﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CopyFolders.aspx.cs" Inherits="Office_CopyFolders" MasterPageFile="~/MasterPage.master"%>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%--<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style>
        #combobox_chosen {
            display: block;
            margin-top: 5px;
        }

        #comboFolder_chosen {
            display: block;
            margin-top: 5px;
        }

        .chosen-results {
            max-height: 200px !important;
        }

        .left-content {
            width:780px;
        }

        .collapseFolder
        {
            display: none;
            background-color: white;
            margin-top: -5px;
            position: absolute;
            z-index: 10000;
            left: 44%;
            border: none;
            border-bottom-right-radius: 100%;
            border-bottom-left-radius: 100%;
            background: url(../images/up-arrow-key.png) 2px -3px no-repeat white;
            height: 24px;
            width: 30px;
            border: solid 1px #94ba33;
            cursor: pointer;
            top:100%;
        }

        label {
            font-size: 14px;
            display:inline-block;
        }

        .selectBox-outer {
            position: relative;
            display: inline-block;
        }

        .selectBox select {
            width: 100%;
        }

        .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            font-size: 15px;
        }

        .chosen-container {
            font-size: 15px;
        }

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
            left: 0;
        }

        #folderList {
            width: 100%;
            display: none;
            border: 1px #dadada solid;
            position: absolute;
            left: 0;
            top: 100%;
            z-index: 9;
            background: #fff;
            max-height: 235px;
            overflow-y: auto;
        }

            #folderList label {
                display: block;
                padding: 0 5px;
                width: auto;
                min-width: initial;
                cursor: pointer;
                margin-top: 3px;
            }

                #folderList label:hover {
                    background-color: #e5e5e5;
                }

            #folderList input {
                display: inline-block;
            }

        .chosen-container .chosen-choices
        {
            max-height:100px;
            overflow:auto;
        }
    </style>

    <script type="text/javascript">
        var pubExpanded = false;
        function pageLoad() {
            showLoader();
            LoadFolders();
            $('body').click(function (evt) {
                if (evt.target.className == "chosen-search-input" || evt.target.className == "chosen-search-input default" || evt.target.className == "chosen-results" || evt.target.className == "search-choice" || evt.target.className == "chosen-choices" || $(evt.target).parent().attr("class") == "search-choice" || evt.target.className == "result-selected") {
                    $('.chosen-drop').css("display", "block");
                    $('.collapseFolder').css("display", "block");
                    return;
                }
                else {
                    $('.chosen-drop').css("display", "none");
                    $('.collapseFolder').css("display", "none");
                }

            });
            $("#comboBoxFolder").change(function () {
                $('.search-choice span').click(function () {
                    $('.collapseFolder').css("display", "block");
                })
                $('.search-choice-close').click(function () {
                    if ($('.chosen-choices').children().length > 2)
                    $('.collapseFolder').css("display", "none");
                })
            })
            
        }

        function LoadFolders() {
            $("#folderList").html('');
            $("#comboBoxFolder").html('');
            $("#comboBoxFolder").chosen("destroy");
            $.ajax(
             {
                 type: "POST",
                 url: '../FlexWebService.asmx/GetFolderDetailsById',
                 contentType: "application/json; charset=utf-8",
                 data: '{"officeId":"' + '<%= Sessions.SwitchedSessionId %>' + '"}',
                 dataType: "json",
                 success: function (result) {

                     if (result.d) {
                         $.each(result.d, function (i, text) {
                             $('<option />', { value: i, text: text }).appendTo($("#comboBoxFolder"));
                         });
                         $('.chosen-toggle').each(function (index) {
                             $(this).on('click', function () {
                                 $(this).parent().parent().find('option').prop('selected', $(this).hasClass('select')).parent().trigger('chosen:updated');
                             });
                         });
                     }
                     $("#comboBoxFolder").chosen({ width: "315px" });
                     $chosen = $("#comboBoxFolder").chosen();

                     var chosen = $chosen.data("chosen");
                     var _fn = chosen.result_select;
                     chosen.result_select = function (evt) {
                         evt["metaKey"] = true;
                         evt["ctrlKey"] = true;
                         chosen.result_highlight.addClass("result-selected");
                         return _fn.call(chosen, evt);
                     };
                     $('.chosen-results').after('<asp:Button runat="server" ID="collapse" CssClass="collapseFolder" OnClientClick="collapseFolder();return false;"></asp:Button>');
                     
                     hideLoader();

                 },
                 fail: function (data) {
                     alert("Failed to get Folders. Please try again!");
                     hideLoader();
                 }
             });
         }

         function toggle(source) {
             var checkboxes = $("#folderList").find("input[type=checkbox]");
             for (var i = 0; i < checkboxes.length; i++) {
                 if (checkboxes[i] != source)
                     checkboxes[i].checked = source.checked;
             }
         }

         function collapseFolder() {
             pubExpanded = false;
             $('.chosen-drop').css("display", "none");
             $('.collapseFolder').css("display", "none");
         }

         function showLoader() {
             $('#overlay').show();
             $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
         }

         function hideLoader() {
             $('#overlay').hide();
         }

         function OnCopyFoldersClick() {
             showLoader();
             var children = $('.chosen-choices').children();
             var folders = "";

             var userName = $("#" + '<%= txtUserName.ClientID%>').val();
             if (userName == undefined || userName == '') {
                 hideLoader();
                 alert("Please enter UserName to transfer folder(s).");
                 return false;
             }

             if (children.length > 1) {
                 for (var i = 0; i < children.length - 1; i++) {
                     if ($(children[i]))
                     {
                         folders += $($('#comboBoxFolder').find('option')[$(children[i]).find('a').attr('data-option-array-index')]).val() + ", ";
                     }
                 }
                 folders = folders.substr(0, folders.lastIndexOf(','));
             }
             if (folders.length <= 0 || children.length < 2) {
                 hideLoader();
                 alert("Please select at least one folder to copy.");
                 return false;
             }
             $("#" + '<%=hdnFolderID.ClientID %>').val(folders);
        }

    </script>

    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Copy Folders To Another User</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="inner-wrapper">
        <div class="page-container">
            <div class="dashboard-container">
                <div class="left-content">
                    <div>
                        <asp:Panel ID="panelMain" runat="server" Visible="true">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div>
                                        <label style="vertical-align:middle;margin-top:-5%;">Select folders to transfer to another user</label>
                                        <div class="selectBox-outer" style="margin-left:10px;margin-top:-8px;">
                                            <div style="float:right;">                                                
                                            </div>
                                            <div class="selectBox" style="float:left;">
                                                <select id="comboBoxFolder" multiple style="color: rgb(68,68,68); border-radius: 5px; background-color: #fff; border: 1px solid #aaa; box-shadow: 0 1px 0 #fff inset; background: linear-gradient(#fff 20%, #f6f6f6 50%, #eee 52%, #f4f4f4 100%); max-height: 100px;">
                                                    <%--<option>Select Folder to upload LogForm</option>--%>
                                                </select>
                                                <button type="button" class="chosen-toggle select create-btn btn-small green" style="margin-left: 10px; position: absolute; bottom: 0; margin-left: 10px;height:30px;width:71px;">
                                                    Select all
                                                </button>
                                                <button type="button" class="chosen-toggle deselect create-btn btn-small green" style="margin-left: 90px;position: absolute; bottom: 0;height:30px;width:90px;">Deselect all</button>
                                            <%--<asp:Button runat="server" ID="collapse" CssClass="collapseFolder" OnClientClick="collapseFolder();return false;"></asp:Button>--%>
                                                <%--<div class="overSelect"></div>--%>
                                            </div>
                                            
                                            <div id="folderList" style="display: none; margin-top: -1px; border-radius: 0 0 4px 4px; background-clip: padding-box"></div>
                                            <asp:HiddenField runat="server" ID="hdnFolderID" />
                                        </div>
                                    </div>
                                    <div style="margin-top:15px;">
                                        <label>Enter user name to transfer folders</label>
                                        <asp:TextBox runat="server" ID="txtUserName" MaxLength="50" style="margin-left:10px;height:30px;width:235px;"/>
                                        <%--</div>
                                    <div>--%>
                                        <asp:Button class="create-btn btn-small green" style="margin-left:10px;" runat="server" ID="btnCopyFolders" Text="Copy Folders" OnClick="btnCopyFolders_Click" OnClientClick="return OnCopyFoldersClick();"/>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>