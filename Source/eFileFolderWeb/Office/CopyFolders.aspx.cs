﻿using iTextSharp.text.pdf;
using Shinetech.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Office_CopyFolders : System.Web.UI.Page
{
    public static string uid = Sessions.SwitchedSessionId;
    //public static string uid = Membership.GetUser().ProviderUserKey.ToString();
    public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();

    protected void Page_Load(object sender, EventArgs e)
    {
        uid = Sessions.SwitchedSessionId;
    }

    protected void btnCopyFolders_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = General_Class.GetUserIdFromUserName(txtUserName.Text.Trim());
            if (dt != null && dt.Rows.Count > 0 && dt.Rows[0].Field<Guid>("UID").ToString() == uid)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                                  "hideLoader();alert(\"" + "You can not transfer to your own account. Please enter another user." + "\");", true);
            }
            else if (dt != null && dt.Rows.Count > 0 && dt.Rows[0].Field<Guid>("UID").ToString() != uid)
            {
                DataSet ds = General_Class.GetSelectedFolderDetails(hdnFolderID.Value.Trim(), uid);
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0].Field<string>("FileNumber")))
                {
                    CreateFolder(ds, dt.Rows[0].Field<Guid>("UID").ToString());
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                       "hideLoader();alert(\"" + "Folder(s) have been copied successfully." + "\");", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                   "hideLoader();alert(\"" + "Only folders with File Numbers are eligible for transfer. Please make sure folders have a file number before transfering." + "\");", true);
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                   "hideLoader();alert(\"" + "Failed to move folders. Please try again." + "\");", true);
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                   "hideLoader();alert(\"" + "Failed to move folders. Please try again." + "\");", true);
        }
    }

    private string GetOfficeID(string newOfficeID)
    {
        try
        {
            Account account = new Account(newOfficeID);
            string officeID = string.Empty;
            if (account.IsSubUser.ToString() == "1")
                officeID = account.OfficeUID.ToString();
            else
                officeID = newOfficeID;
            return officeID;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
    }

    private bool SetFolderValues(FileFolder fileFolder, DataRow folder, string newOfficeUID)
    {
        try
        {
            fileFolder.FolderName.Value = folder.Field<string>("FolderName");
            fileFolder.DOB.Value = folder.Field<string>("DOB");
            fileFolder.CreatedDate.Value = DateTime.Now;
            fileFolder.OtherInfo.Value = folder.Field<int>("OtherInfo");
            fileFolder.OtherInfo2.Value = (float)folder.Field<double>("OtherInfo2");
            fileFolder.OfficeID.Value = newOfficeUID;
            fileFolder.OfficeID1.Value = GetOfficeID(newOfficeUID);
            fileFolder.LastDate.Value = DateTime.Now;
            fileFolder.FirstName.Value = folder.Field<string>("FirstName");
            fileFolder.Lastname.Value = folder.Field<string>("Lastname");

            fileFolder.Email.Value = folder.Field<string>("Email");
            fileFolder.Tel.Value = folder.Field<string>("Tel");
            fileFolder.FaxNumber.Value = folder.Field<string>("FaxNumber");
            fileFolder.Address.Value = folder.Field<string>("Address");
            fileFolder.City.Value = folder.Field<string>("City");
            fileFolder.Postal.Value = folder.Field<string>("Postal");
            fileFolder.FileNumber.Value = folder.Field<string>("FileNumber");
            fileFolder.SecurityLevel.Value = folder.Field<int>("SecurityLevel");
            fileFolder.Alert1.Value = folder.Field<string>("Alert1");
            fileFolder.Alert2.Value = folder.Field<string>("Alert2");
            fileFolder.Comments.Value = folder.Field<string>("Comments");
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    private bool CreateFolder(DataSet ds, string newOfficeID)
    {
        try
        {
            if (ds != null && ds.Tables.Count >= 2 && ds.Tables[0].Rows.Count > 0 && ds.Tables[1].Rows.Count > 0)
            {
                foreach (DataRow folder in ds.Tables[0].Rows)
                {
                    int folderID = folder.Field<int>("FolderID");
                    DataRow[] dr = ds.Tables[1].Select("EFFID = " + folderID);
                    Dictionary<string, Divider> list = new Dictionary<string, Divider>();

                    foreach (DataRow divider in dr)
                    {
                        Divider newEntity = new Divider();
                        newEntity.Name.Value = divider.Field<string>("Name");
                        newEntity.Color.Value = divider.Field<string>("Color");
                        newEntity.DOB.Value = DateTime.Now;
                        newEntity.OfficeID.Value = newOfficeID;

                        list.Add(newEntity.Name.Value, newEntity);
                    }
                    FileFolder fileFolder = new FileFolder();
                    if (SetFolderValues(fileFolder, folder, newOfficeID))
                    {
                        string folderName = folder.Field<string>("FolderName");
                        string fileNumber = folder.Field<string>("FileNumber");
                        DataTable dtNew = FileFolderManagement.IsFolderNameExist(newOfficeID, folderName, fileNumber);
                        if (dtNew != null && dtNew.Rows.Count > 0)
                        {
                            int index = 1;
                            while (true)
                            {
                                string renameFolderName = folderName + "_v" + index;
                                DataTable dtNewFolder = FileFolderManagement.IsFolderNameExist(newOfficeID, renameFolderName, fileNumber);
                                if (dtNewFolder != null && dtNewFolder.Rows.Count > 0)
                                    index++;
                                else
                                {
                                    fileFolder.FolderName.Value = renameFolderName;
                                    break;
                                }
                            }
                        }
                        int newfolderId = FileFolderManagement.CreatNewFileFolder(fileFolder, list);
                        if (CopyFiles(newfolderId, dr, folderID, newOfficeID))
                        {
                            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(folderID, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Copy.GetHashCode(), 0, 0);
                            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(newfolderId, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Create.GetHashCode(), 0, 0);
                            General_Class.InsertCopyFolderLogs(uid, newOfficeID, folder.Field<int>("FolderID"), newfolderId, Enum_Tatva.CopyFoldersFromOneUserToAnother.Move.GetHashCode());
                        }
                        else
                        {
                            General_Class.DeleteFileFolder(newfolderId);
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            return false;
        }
    }


    private bool CopyFiles(int newFolderID, DataRow[] dr, int oldFolderID, string newOfficeUID)
    {
        try
        {
            //Get divider details from database
            DataSet ds = General_Class.GetSelectedFolderDetails(newFolderID.ToString(), newOfficeUID);
            if (ds != null && ds.Tables.Count >= 2 && ds.Tables[0].Rows.Count > 0 && ds.Tables[1].Rows.Count > 0)
            {
                DataRow[] newDividers = ds.Tables[1].Select("EFFID = " + newFolderID);
                for (int i = 0; i < newDividers.Length; i++)
                {
                    string sourcePath = Server.MapPath("~/" + CurrentVolume + Path.DirectorySeparatorChar + oldFolderID + Path.DirectorySeparatorChar
                        + dr[i].Field<int>("DividerID"));

                    //Find path in current volume
                    if (!Directory.Exists(sourcePath))
                    {
                        //If not exists, find in previous volume
                        sourcePath = Server.MapPath("~/" + "Volume" + Path.DirectorySeparatorChar + oldFolderID + Path.DirectorySeparatorChar
                        + dr[i].Field<int>("DividerID"));
                    }

                    if (Directory.Exists(sourcePath))
                    {
                        string newPath = Server.MapPath("~/" + CurrentVolume + Path.DirectorySeparatorChar + newFolderID + Path.DirectorySeparatorChar
                            + newDividers[i].Field<int>("DividerID"));

                        if (!Directory.Exists(newPath))
                            Directory.CreateDirectory(newPath);

                        //Copy files from existing folders to new folders
                        foreach (string file in Directory.GetFiles(sourcePath))
                        {

                            string sourceExtension = Path.GetExtension(file);
                            if (sourceExtension == ".jpeg" || sourceExtension == ".jpg")
                            {
                                string fileToMove = Path.GetDirectoryName(file) + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(file) + ".pdf";
                                string newFile = newPath + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(file) + ".pdf";
                                if (!File.Exists(newFile))
                                    File.Copy(fileToMove, newFile);
                            }

                            string sourceFile = file.Substring(file.LastIndexOf("\\") + 1);
                            if (!File.Exists(newPath + Path.DirectorySeparatorChar + sourceFile))
                            {
                                File.Copy(file, newPath + Path.DirectorySeparatorChar + sourceFile);

                                int pageCount = 0;
                                try
                                {
                                    PdfReader pdfReader = new PdfReader(newPath + Path.DirectorySeparatorChar + sourceFile);
                                    pageCount = pdfReader.NumberOfPages;
                                }
                                catch (Exception ex)
                                { }

                                FileInfo fileInfo = new FileInfo(newPath + Path.DirectorySeparatorChar + sourceFile);
                                float size = (float)Math.Round((decimal)(fileInfo.Length / 1024) / 1024, 2); // IN MB.
                                string path = CurrentVolume + Path.DirectorySeparatorChar + newFolderID + Path.DirectorySeparatorChar +
                                              newDividers[i].Field<int>("DividerID");

                                string newFileName = HttpUtility.UrlDecode(sourceFile.Replace('$', '%').ToString());

                                string documentID = General_Class.DocumentsInsert(Path.GetFileNameWithoutExtension(newFileName), newFolderID, newOfficeUID,
                                                              path, newDividers[i].Field<int>("DividerID"), pageCount, size, Path.GetFileNameWithoutExtension(newFileName),
                                                              (int)Common_Tatva.getFileExtention(Path.GetExtension(newFileName)),
                                                              FlashViewer.GetDocumentOrder(Convert.ToInt32(newDividers[i].Field<int>("DividerID"))) + 1,
                                                              Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()));

                                if (Convert.ToInt32(documentID) > 0)
                                {
                                    Shinetech.DAL.General_Class.AuditLogByDocId(Convert.ToInt32(documentID), Sessions.SwitchedSessionId, Enum_Tatva.Action.Create.GetHashCode());
                                    //Shinetech.DAL.General_Class.AuditLogByDocId(Convert.ToInt32(documentID), Membership.GetUser().ProviderUserKey.ToString(), Enum_Tatva.Action.Create.GetHashCode());
                                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(newFolderID, newDividers[i].Field<int>("DividerID"), Convert.ToInt32(documentID), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.CopyFolderUpload.GetHashCode(), 0, 0);
                                }
                            }
                        }
                    }
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            return false;
        }
    }
}