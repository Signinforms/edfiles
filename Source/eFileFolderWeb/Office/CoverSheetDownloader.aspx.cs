﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BarcodeLib;
using Ionic.Zip;
using iTextSharp.text;
using iTextSharp.text.pdf;

public partial class Office_CoverSheetDownloader : System.Web.UI.Page
{
    #region properties

    public NLogLogger _logger = new NLogLogger();
    private string data = HttpContext.Current.Request.QueryString["data"];

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            string decode = QueryString.QueryStringDecode(data);
            string[] splitedData = decode.Split('&');
            int folderId = Convert.ToInt32(splitedData[0].Split('=')[1]);
            string fileNumber = Convert.ToString(splitedData[2].Split('=')[1]), folderName = Convert.ToString(splitedData[1].Split('=')[1]);

            ZipFile zip = new ZipFile();
            try
            {
                if (DividerSource != null && DividerSource.Rows.Count > 0)
                {
                    foreach (DataRow dr in DividerSource.Rows)
                    {
                        try
                        {
                            BarCodeStructure bcStrct = new BarCodeStructure()
                            {
                                FolderId = folderId,
                                TabId = Convert.ToInt32(dr["DividerId"])
                            };
                            string encodeData = "FolderId-" + folderId + "\\TabId-" + Convert.ToInt32(dr["DividerId"]);
                            string fileName = Common_Tatva.CoverSheetFolder + " - " + folderName + " - " + Convert.ToString(dr["Name"]) + ".pdf";
                            string path = Server.MapPath("~") + Common_Tatva.CoverSheetFolder + Path.DirectorySeparatorChar + folderId;
                            string fullPath = path + Path.DirectorySeparatorChar + fileName;
                            if (!Directory.Exists(path))
                                Directory.CreateDirectory(path);
                            if (!File.Exists(fullPath))
                            {
                                BarcodeLib.Barcode b = new BarcodeLib.Barcode();
                                b.IncludeLabel = true;
                                b.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                                b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;
                                BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE128;
                                System.Drawing.Image barCodeImg = b.Encode(type, encodeData, Color.Black, Color.White, 1200, 100);

                                iTextSharp.text.Document document = new iTextSharp.text.Document(PageSize.A4, 50, 50, 50, 50);
                                PdfWriter pdfWriter = PdfWriter.GetInstance(document, new FileStream(fullPath, FileMode.Create));
                                document.Open();

                                PdfContentByte pdfContentByte = pdfWriter.DirectContent;
                                PdfPTable table1 = new PdfPTable(1) { WidthPercentage = 100 };

                                iTextSharp.text.Font hFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.UNDEFINED, 34, iTextSharp.text.Font.NORMAL);
                                hFont.IsBold();
                                PdfPCell hCell = new PdfPCell(new Phrase("COVER SHEET", hFont));
                                hCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                                hCell.HorizontalAlignment = Element.ALIGN_CENTER;
                                hCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                table1.AddCell(hCell);

                                PdfPCell cellBlankRow = new PdfPCell(new Phrase(" "));
                                cellBlankRow.Border = iTextSharp.text.Rectangle.NO_BORDER;

                                table1.AddCell(cellBlankRow);
                                table1.AddCell(cellBlankRow);
                                table1.AddCell(cellBlankRow);
                                table1.AddCell(cellBlankRow);
                                table1.AddCell(cellBlankRow);

                                iTextSharp.text.Image myImage = iTextSharp.text.Image.GetInstance(barCodeImg, BaseColor.BLACK);
                                PdfPCell bCell = new PdfPCell(myImage, true);
                                bCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                                table1.AddCell(bCell);

                                table1.AddCell(cellBlankRow);
                                table1.AddCell(cellBlankRow);

                                document.Add(table1);

                                PdfPTable table2 = new PdfPTable(1) { WidthPercentage = 100 };
                                table2.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                                table2.DefaultCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                table2.DefaultCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                table2.DefaultCell.FixedHeight = 20;
                                table2.DefaultCell.PaddingLeft = 30;

                                iTextSharp.text.Font dFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.UNDEFINED, 16, iTextSharp.text.Font.NORMAL);
                                dFont.IsBold();
                                PdfPCell dCell = new PdfPCell(new Phrase("EdFile Details", dFont));
                                dCell.FixedHeight = 20;
                                dCell.PaddingLeft = 30;
                                dCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
                                dCell.HorizontalAlignment = Element.ALIGN_LEFT;
                                dCell.VerticalAlignment = Element.ALIGN_MIDDLE;
                                table2.AddCell(dCell);

                                table2.AddCell(cellBlankRow);

                                table2.AddCell("Folder Name : " + folderName);
                                table2.AddCell("Tab Name : " + Convert.ToString(dr["Name"]));
                                if (!string.IsNullOrEmpty(fileNumber))
                                    table2.AddCell("File Number : " + fileNumber);
                                document.Add(table2);

                                document.Close();
                            }
                            zip.AddFile(fullPath, "/");
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                        }
                    }
                    if (zip.Count == 0)
                        Response.Redirect("~/FileNotFound.aspx", true);
                    Response.Clear();
                    string zipFileName = "Cover Sheet - " + folderName + ".zip";
                    Response.AddHeader("Content-Disposition", "attachment; filename=CoverSheet.zip");
                    Response.ContentType = "application/zip";
                    zip.Save(Response.OutputStream);
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                zip.Dispose();
                _logger.Error(ex);
            }
        }
        catch (Exception ex) { }
    }

    public DataTable DividerSource
    {
        get
        {
            return this.Session["DividerSource_FolderBox"] as DataTable;
        }
        set
        {
            this.Session["DividerSource_FolderBox"] = value;
        }
    }

    public class BarCodeStructure
    {
        public int FolderId { get; set; }
        public int TabId { get; set; }
    }
}