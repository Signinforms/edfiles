<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CreateFileRequest.aspx.cs" Inherits="CreateFileRequest" Title="" %>

<%@ Register Src="../Controls/FileRequestControl.ascx" TagName="FileRequestControl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        .lblOfficeName
        {
            font-size:15px;
            font-weight:bold;
            min-width:200px;
        }
        .ajax__validatorcallout
        {
            top:-67px !important;
        }
    </style>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Create File Request</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant modify-contant">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="left-content">
                            <div class="content-box listing-view">
                                <fieldset>
                                    <label>User Name</label>
                                    <asp:TextBox ID="textUserName" name="textUserName" runat="server" size="50" MaxLength="20" />
                                    <asp:RequiredFieldValidator runat="server" ID="userNameV" ControlToValidate="textUserName"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The user name is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                        TargetControlID="userNameV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                </fieldset>
                                <fieldset id="fieldSet1">
                                    <label><asp:Label runat="server" CssClass="lblOfficeName" ID="lblOfficeName">Office Name</asp:Label></label>
                                    <asp:TextBox ID="textOfficeName" name="textOfficeName" runat="server" size="50" MaxLength="20" />
                                    <asp:RequiredFieldValidator runat="server" ID="officeNameV" ControlToValidate="textOfficeName"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The office name is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                        TargetControlID="officeNameV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                </fieldset>
                                <fieldset>
                                    <label>Telephone</label>
                                    <asp:TextBox ID="textPhone" name="textPhone" runat="server" size="50" MaxLength="23" />
                                    <asp:RequiredFieldValidator runat="server" ID="telN" ControlToValidate="textPhone"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The telephone is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11"
                                        TargetControlID="telN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    <asp:RegularExpressionValidator ID="TelV" ControlToValidate="textPhone" runat="server"
                                        ErrorMessage="<b>Required Field Missing</b><br />It is not a telphone format."
                                        ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                        TargetControlID="TelV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                </fieldset>
                                <fieldset>
                                    <label>Requested By</label>
                                    <asp:TextBox name="textRequestBy" runat="server" ID="textRequestBy" size="50" MaxLength="23" />
                                    <asp:RequiredFieldValidator runat="server" ID="textRequestByN" ControlToValidate="textRequestBy"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The Requested By is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14"
                                        TargetControlID="textRequestByN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                </fieldset>
                                <fieldset>
                                    <label>Request Date</label>
                                    <asp:TextBox ID="textRequestDate" name="textRequestDate" runat="server" size="50" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" PopupPosition="BottomLeft" runat="server" TargetControlID="textRequestDate" Format="MM-dd-yyyy">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RequiredFieldValidator runat="server" ID="DobV" ControlToValidate="textRequestDate"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The Request Date is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5"
                                        TargetControlID="DobV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    <asp:RegularExpressionValidator ID="DobC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The request date is not a date"
                                        ControlToValidate="textRequestDate" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                        Visible="true" Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6"
                                        TargetControlID="DobC" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                </fieldset>
                            </div>
                            <div class="content-box listing-view" style="margin-top: 15px; border: 0; padding: 0;">
                                <p><strong>Please provide complete File information (First Name, Last Name, File#, Year)</strong></p>
                                <div>&nbsp;</div>
                                <div class="multiple-fieldset">
                                    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                        <tbody>
                                            <tr class="even">
                                                <td width="6%" class="number">1</td>
                                                <td width="47%">
                                                    <fieldset>
                                                        <label>First Name </label>
                                                        <asp:TextBox runat="server" ID="textFirstName1" size="30" MaxLength="30" />
                                                        <label>Last Name</label>
                                                        <asp:TextBox runat="server" ID="textLastName1" size="30" MaxLength="30" />

                                                    </fieldset>
                                                </td>
                                                <td width="47%">
                                                    <fieldset>
                                                        <label>File #</label>
                                                        <asp:TextBox runat="server" ID="textFile1" size="30" MaxLength="30" />
                                                        <label>Year</label>
                                                        <asp:TextBox runat="server" ID="textYear1" size="30" MaxLength="30" />
                                                    </fieldset>
                                                </td>
                                            </tr>

                                            <tr class="odd">
                                                <td width="6%" class="number">2</td>
                                                <td width="47%">
                                                    <fieldset>
                                                        <label>First Name </label>
                                                        <asp:TextBox runat="server" ID="textFirstName2" size="30" MaxLength="30" />
                                                        <label>Last Name</label>
                                                        <asp:TextBox runat="server" ID="textLastName2" size="30" MaxLength="30" />
                                                    </fieldset>
                                                </td>
                                                <td width="47%">
                                                    <fieldset>
                                                        <label>File #</label>
                                                        <asp:TextBox runat="server" ID="textFile2" size="30" MaxLength="30" />
                                                        <label>Year</label>
                                                        <asp:TextBox runat="server" ID="textYear2" size="30" MaxLength="30" />
                                                    </fieldset>
                                                </td>
                                            </tr>

                                            <tr class="even">
                                                <td width="6%" class="number">3</td>
                                                <td width="47%">
                                                    <fieldset>
                                                        <label>First Name </label>
                                                        <asp:TextBox runat="server" ID="textFirstName3" size="30" MaxLength="30" />
                                                        <label>Last Name</label>
                                                        <asp:TextBox runat="server" ID="textLastName3" size="30" MaxLength="30" />
                                                    </fieldset>
                                                </td>
                                                <td width="47%">
                                                    <fieldset>
                                                        <label>File #</label>
                                                        <asp:TextBox runat="server" ID="textFile3" size="30" MaxLength="30" />
                                                        <label>Year</label>
                                                        <asp:TextBox runat="server" ID="textYear3" size="30" MaxLength="30" />
                                                    </fieldset>
                                                </td>
                                            </tr>

                                            <tr class="odd">
                                                <td width="6%" class="number">4</td>
                                                <td width="47%">
                                                    <fieldset>
                                                        <label>First Name </label>
                                                        <asp:TextBox runat="server" ID="textFirstName4" size="30" MaxLength="30" />
                                                        <label>Last Name</label>
                                                        <asp:TextBox runat="server" ID="textLastName4" size="30" MaxLength="30" />
                                                    </fieldset>
                                                </td>
                                                <td width="47%">
                                                    <fieldset>
                                                        <label>File #</label>
                                                        <asp:TextBox runat="server" ID="textFile4" size="30" MaxLength="30" />
                                                        <label>Year</label>
                                                        <asp:TextBox runat="server" ID="textYear4" size="30" MaxLength="30" />
                                                    </fieldset>
                                                </td>
                                            </tr>

                                            <tr class="even">
                                                <td width="6%" class="number">5</td>
                                                <td width="47%">
                                                    <fieldset>
                                                        <label>First Name </label>
                                                        <asp:TextBox runat="server" ID="textFirstName5" size="30" MaxLength="30" />
                                                        <label>Last Name</label>
                                                        <asp:TextBox runat="server" ID="textLastName5" size="30" MaxLength="30" />
                                                    </fieldset>
                                                </td>
                                                <td width="47%">
                                                    <fieldset>
                                                        <label>File #</label>
                                                        <asp:TextBox runat="server" ID="textFile5" size="30" MaxLength="30" />
                                                        <label>Year</label>
                                                        <asp:TextBox runat="server" ID="textYear5" size="30" MaxLength="30" />
                                                    </fieldset>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <fieldset style="padding: 0 15px;">
                                    <label>Additional Comments</label>
                                    <asp:TextBox runat="server" ID="textComment" size="72" MaxLength="200" />
                                </fieldset>
                                <fieldset>
                                    <label>&nbsp;</label>
                                    <div style="text-align: center">
                                        <asp:Button runat="server" name="imageField" ID="imageField" OnClick="imageField_Click" Text="Submit" CssClass="create-btn btn green" />
                                    </div>
                                </fieldset>
                            </div>
                            <div class="podnaslov">
                                <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                            </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="imageField" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div class="right-content">
                <%--<div class="quick-find">
                    <uc1:QuickFind ID="QuickFind1" runat="server" />
                </div>--%>
                <div class="quick-find" style="margin-top: 15px;">
                    <div class="find-inputbox">
                        <marquee scrollamount="1" id="marqueeside" runat="server" behavior="scroll" onmouseover="this.stop()" onmouseout="this.start()" scrolldelay="20" direction="up">
                                <%=MessageBody %></marquee>
                    </div>
                </div>
                <div class="quick-find" style="margin-top: 15px;">
                    <uc2:FileRequestControl ID="fileRequest1" runat="server" />
                </div>
            </div>
        </div>
        <div class="clearfix"></div>

    </div>
    </div>

    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Button1ShowPopup" runat="server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" PopupControlID="pnlPopup" OnOkScript="anotherRequest()" CancelControlID="btnClose" OnCancelScript="closePage()" OkControlID="btnRequset"
                DropShadow="true" BackgroundCssClass="modalBackground" TargetControlID="Button1ShowPopup" />
            <!-- ModalPopup Panel-->
            <asp:Panel ID="pnlPopup" runat="server" Style="display: none;" CssClass="popup-mainbox">
                <div id="div1" class="popup-head">File Request</div>
                <div style="background-color: White">
                    <div style="background-color: White">
                        <table width="100%" class="details-datatable">
                            <tr>
                                <td align="left" class="podnaslov"><strong>Hello&nbsp;<asp:Label ID="LabelUserName" Text="" runat="server" />,</strong></td>
                            </tr>
                            <tr>
                                <td width="100%" align="left" class="podnaslov">Your request: {<strong><asp:Label ID="LabelResult" Text="" runat="server" /></strong>
                                    }&nbsp;&nbsp;has been submitted.
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div align="center" style="width: 100%; background-color: white; padding: 20px 0;">
                    <asp:Button ID="btnRequset" runat="server" Text="Request Another" Style="width: auto; margin: 2px 0" CssClass="create-btn btn green" />
                    <asp:Button ID="btnClose" runat="server" Text="Close" Style="margin: 2px 0" CssClass="create-btn btn green" />
                </div>

                <div style="height: 10px">&nbsp;&nbsp;</div>

            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="imageField" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>

    <script language="javascript" type="text/javascript">
        function anotherRequest() {
            window.location = window.location;

        }
        function closePage() {
            window.location = "http://www.edfiles.com/Office/Welcome.aspx";
        }
        function pageLoad() {
            if ($("#fieldSet1").find('.lblOfficeName').length < 1) {
                $("#fieldSet1").css("margin", "0");
            }
        }
    </script>
</asp:Content>
