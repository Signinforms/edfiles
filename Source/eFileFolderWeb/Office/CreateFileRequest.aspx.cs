using System;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using DataQuicker2.Framework;
using Shinetech.DAL;
using Shinetech.Utility;
using Account = Shinetech.DAL.Account;

public partial class CreateFileRequest : LicensePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string uid = Sessions.SwitchedSessionId;
            //string uid = Membership.GetUser().ProviderUserKey.ToString();
            textUserName.Text = Sessions.SwitchedUserName;
            //textUserName.Text = Membership.GetUser().UserName;
            Account act = new Account(uid);
            if (act.IsExist)
            {
                MessageBody = act.MessageBody.Value ?? "";
                if (MessageBody == "") marqueeside.Attributes["height"] = "0";
            }

            if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users"))
            {
                lblOfficeName.Visible = true;
                DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(Sessions.SwitchedSessionId);
                textOfficeName.Text = dtOfficeUser.Rows[1]["Name"].ToString();
            }
            else
            {
                textOfficeName.Visible = false;
                textOfficeName.Text = Sessions.SwitchedUserName;
                //textOfficeName.Text = Membership.GetUser().UserName;
                lblOfficeName.Visible = false;
            }

            CalendarExtender2.SelectedDate = DateTime.Today;

        }

    }

    public string MessageBody
    {
        get
        {
            return this.Session["_Side_MessageBody"] as string;
        }

        set
        {
            this.Session["_Side_MessageBody"] = value;
        }
    }

    protected void imageField_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (!this.IsFileValid()) return;

            FileForm form = new FileForm();
            form.OfficeName.Value = textOfficeName.Text.Trim();
            form.PhoneNumber.Value = textPhone.Text.Trim();
            form.RequestedBy.Value = textRequestBy.Text.Trim();
            textRequestDate.Text = textRequestDate.Text.Replace('-', '/');
            form.RequestDate.Value = DateTime.ParseExact(textRequestDate.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
            //form.RequestDate.Value = DateTime.ParseExact(textRequestDate.Text.Trim(),"mm-dd-yyyy",System.Globalization.CultureInfo.InvariantCulture);
            form.UserName.Value = textUserName.Text.Trim();
            form.UserID.Value = Sessions.SwitchedSessionId;
            //form.UserID.Value = Membership.GetUser().ProviderUserKey.ToString();
            form.Comment.Value = textComment.Text.Trim();

            IDbConnection connection = DbFactory.CreateConnection();
            connection.Open();
            IDbTransaction transaction = connection.BeginTransaction();
            try
            {


                ArrayList items = new ArrayList();
                int fileQty = 0;
                //create requested file
                if (textFirstName1.Text.Trim() != "")
                {

                    FormFile file1 = new FormFile();
                    file1.FirstName.Value = textFirstName1.Text.Trim();
                    file1.LastName.Value = textLastName1.Text.Trim();
                    file1.FileNumber.Value = textFile1.Text.Trim();
                    file1.YearDate.Value = textYear1.Text.Trim();

                    file1.UserID.Value = form.UserID.Value;

                    fileQty++;
                    items.Add(file1);
                }

                if (textFirstName2.Text.Trim() != "")
                {
                    FormFile file2 = new FormFile();
                    file2.FirstName.Value = textFirstName2.Text.Trim();
                    file2.LastName.Value = textLastName2.Text.Trim();
                    file2.FileNumber.Value = textFile2.Text.Trim();
                    file2.YearDate.Value = textYear2.Text.Trim();

                    file2.UserID.Value = form.UserID.Value;


                    fileQty++;
                    items.Add(file2);
                }

                if (textFirstName3.Text.Trim() != "")
                {
                    FormFile file3 = new FormFile();
                    file3.FirstName.Value = textFirstName3.Text.Trim();
                    file3.LastName.Value = textLastName3.Text.Trim();
                    file3.FileNumber.Value = textFile3.Text.Trim();
                    file3.YearDate.Value = textYear3.Text.Trim();

                    file3.UserID.Value = form.UserID.Value;

                    fileQty++;
                    items.Add(file3);
                }

                if (textFirstName4.Text.Trim() != "")
                {
                    FormFile file4 = new FormFile();
                    file4.FirstName.Value = textFirstName4.Text.Trim();
                    file4.LastName.Value = textLastName4.Text.Trim();
                    file4.FileNumber.Value = textFile4.Text.Trim();
                    file4.YearDate.Value = textYear4.Text.Trim();

                    file4.UserID.Value = form.UserID.Value;


                    fileQty++;
                    items.Add(file4);
                }

                if (textFirstName5.Text.Trim() != "")
                {
                    FormFile file5 = new FormFile();
                    file5.FirstName.Value = textFirstName5.Text.Trim();
                    file5.LastName.Value = textLastName5.Text.Trim();
                    file5.FileNumber.Value = textFile5.Text.Trim();
                    file5.YearDate.Value = textYear5.Text.Trim();

                    file5.UserID.Value = form.UserID.Value;

                    fileQty++;
                    items.Add(file5);
                }

                form.FileQuantity.Value = fileQty;
                form.Create(transaction);

                foreach (FormFile file in items)
                {
                    file.FormId.Value = form.FormId.Value;
                    file.Create(transaction);
                }

                transaction.Commit();
                ArrayList recent = new ArrayList();
                DataTable table = FileFolderManagement.GetRequestFiles(Sessions.SwitchedSessionId);
                if (table != null)
                {
                    int n = table.Rows.Count;
                    RequestItem item;

                    for (int i = 0; i < n; i++)
                    {
                        item = new RequestItem(table.Rows[i]);
                        item.Number = i + 1;

                        recent.Add(item);
                    }

                    this.Session["Recent_RequestFiles_" + Sessions.SwitchedSessionId] = recent;

                }
                // string strWrongInfor = "Your request has been submited.";
                LabelUserName.Text = Sessions.SwitchedUserName;
                //LabelUserName.Text = Membership.GetUser().UserName;
                LabelResult.Text = form.FormId.ToString();
                mdlPopup.Show();

                sendeFileFormEmail(form, items);

                // ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
            }
            catch (Exception ex)
            {
                transaction.Rollback();

                string strWrongInfor = string.Format("Error : {0}", ex.Message);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
            }
        }

    }


    public bool IsFileValid()
    {
        if (textFirstName1.Text.Trim() == "" && textFirstName2.Text.Trim() == "" && textFirstName3.Text.Trim() == ""
                && textFirstName4.Text.Trim() == "" && textFirstName5.Text.Trim() == "")
        {
            string strWrongInfor = string.Format("Error : You have to insert one file at least.");
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
            return false;
        }

        if (textFirstName1.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName1.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 1th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }

        if (textFirstName2.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName2.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 2th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }

        if (textFirstName3.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName3.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 3th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }

        if (textFirstName4.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName3.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 4th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }

        if (textFirstName5.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName5.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 4th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }


        return true;
    }

    private void sendeFileFormEmail(FileForm form, ArrayList items)
    {
        string strMailTemplet = getFileFormMailTemplate();

        strMailTemplet = strMailTemplet.Replace("[textOfficeName]", form.OfficeName.Value);
        strMailTemplet = strMailTemplet.Replace("[textUserName]", Sessions.SwitchedUserName);
        //strMailTemplet = strMailTemplet.Replace("[textUserName]", Membership.GetUser().UserName);
        strMailTemplet = strMailTemplet.Replace("[textPhone]", form.PhoneNumber.Value);
        strMailTemplet = strMailTemplet.Replace("[textRequestBy]", form.RequestedBy.Value);
        strMailTemplet = strMailTemplet.Replace("[textRequestDate]", form.RequestDate.Value.ToShortDateString());
        strMailTemplet = strMailTemplet.Replace("[textOfficeName]", form.OfficeName.Value);

        strMailTemplet = strMailTemplet.Replace("[textComment]", form.Comment.Value);

        int count = items.Count;
        int delta = 5 - count;

        FormFile file;

        for (int i = 0; i < count; i++)
        {
            file = items[i] as FormFile;
            strMailTemplet = strMailTemplet.Replace("[textFirstName" + (i + 1) + "]", file.FirstName.Value);
            strMailTemplet = strMailTemplet.Replace("[textLastName" + (i + 1) + "]", file.LastName.Value);
            strMailTemplet = strMailTemplet.Replace("[textFile" + (i + 1) + "]", file.FileNumber.Value);
            strMailTemplet = strMailTemplet.Replace("[textYear" + (i + 1) + "]", file.YearDate.Value);

        }

        for (int i = 5; i > count; i--)
        {
            strMailTemplet = strMailTemplet.Replace("[textFirstName" + i + "]", "");
            strMailTemplet = strMailTemplet.Replace("[textLastName" + i + "]", "");
            strMailTemplet = strMailTemplet.Replace("[textFile" + i + "]", "");
            strMailTemplet = strMailTemplet.Replace("[textYear" + i + "]", "");

        }

        string strEmailSubject = "File Form Request";

        //Need to check.
        Email.SendFileRequestService(Membership.GetUser().Email, strEmailSubject, strMailTemplet);

    }


    private string getFileFormMailTemplate()
    {

        string strMailTemplet = string.Empty;
        string physicalPath = Request.PhysicalPath;
        int index = physicalPath.LastIndexOf("\\");
        string mailTempletUrl = physicalPath.Substring(0, index + 1) + "FileForm.html";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    protected void btnClose_OnClick(object sender, EventArgs e)
    {
        Page.Response.Redirect("~/Office/Welcome.aspx");
    }

    protected void anotherRequest_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect("~/Office/CreateFileRequest.aspx");
    }
}
