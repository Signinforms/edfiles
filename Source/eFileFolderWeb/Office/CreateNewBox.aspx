<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="CreateNewBox.aspx.cs" Inherits="CreateOfficeBox" Title="" %>

<%--<%@ Register Src="../Controls/FileRequestControl.ascx" TagName="FileRequestControl"
    TagPrefix="uc2" %>--%>
<%@ Register Src="../Controls/BoxRequestControl.ascx" TagName="BoxRequestControl"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .lblOfficeName
        {
            font-size:15px;
            font-weight:bold;
            min-width:200px;
        }
        .calenderTxtBox
        {
            background:url(../images/calendar-interface-symbol-tool.png) 100% 2px no-repeat;
        }
        .cancelBtn
        {
            margin-top:5px;
            cursor: pointer;
            margin-left: 10px;
            border: none;
            width: 20px;
            background: url(../images/cancel-Btn.png) -3px no-repeat;
            height: 20px;
            position: absolute;
        }
    </style>
    <script language="javascript" type="text/javascript">
        $("div#btnprint").click(function () {
            $("div#divBoxContent").printArea();
        });
    </script>
    <script type="text/javascript" language="javascript">

        function onCalendarShown(sender, args) {
            // Set new calendar position
            sender._popupDiv.parentElement.style.bottom = '0px';
            sender._popupDiv.parentElement.style.right = '40px';
            sender._popupDiv.parentElement.style.top = '';
            sender._popupDiv.parentElement.style.left = '';
        }

        function clearDate(a) {
            if ($(a).siblings("input:text").attr("id") == '<%=textScannedDate.ClientID %>') {
                $('#<%=textScannedDate.ClientID %>').val("Not Scanned");
            }
            else if ($(a).siblings("input:text").attr("id") == '<%=textPickupDate.ClientID %>') {
                $('#<%=textPickupDate.ClientID %>').val($('#<%=hdnPickupDate.ClientID %>').val());
            }
            else {
                $('#<%=textDestroyDate.ClientID %>').val("Never");
            }
        }

        function pageLoad() {
            if ($('#<%=textScannedDate.ClientID %>').val() == "") {
                $('#<%=textScannedDate.ClientID %>').val("Not Scanned");
            }
            if ($('#<%=textDestroyDate.ClientID %>').val() == "") {
                $('#<%=textDestroyDate.ClientID %>').val("Never");
            }
            if ($('#<%=textPickupDate.ClientID %>').val() == "") {
                $('#<%=textPickupDate.ClientID %>').val($('#<%=hdnPickupDate.ClientID %>').val());
            }
            if ($("#fieldSet1").children().find('.lblOfficeName').length < 1) {
                $("#fieldSet1").css("margin", "0");
            }
        }

    function checkfile(sender) {
        var validExts = new Array(".xlsx", ".xls");
        var fileExt = sender.value;
        fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
        if (validExts.indexOf(fileExt) < 0) {
            alert("Invalid file selected, valid files are of " +
                     validExts.toString() + " types.");
            return false;
        }
        else return true;
    }

    //$('.savebox').click(function (e) {
    //    e.preventDefault();
    //    validatePastDate();
    //});
    function validatePastDate() {
        var destroydate = $('#ctl00_ContentPlaceHolder1_textDestroyDate').val();
        var d = new Date();
        var today = (d.getMonth() + 1) + "-" + d.getDate() + "-" + d.getFullYear();
        if (new Date(destroydate) >= new Date(today) || destroydate == "Never") {
            return true;
        }
        else {
            alert("DestroyDate Must be greater then today.");
            return false;
            e.preventDefault();
        }
    }
    </script>


    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Create New Box</h1>
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant modify-contant">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="left-content">
                            <div class="content-box listing-view">

                                <fieldset>
                                    <label>User Name</label>
                                    <asp:TextBox ID="textUserName" name="textUserName" ReadOnly="true" runat="server"
                                        size="50" MaxLength="23" />
                                </fieldset>

                                <fieldset>
                                    <label>Department Name</label>
                                    <asp:TextBox ID="textDepartmentName" name="textDepartmentName" runat="server"
                                        size="50" MaxLength="23" />
                                </fieldset>

                                <fieldset>
                                    <label>Boxes Count</label>
                                    <asp:TextBox ID="textBoxCount" name="textBoxCount" runat="server" size="25" MaxLength="5" Text="1" AutoCompleteType="None" />
                                    <asp:RequiredFieldValidator runat="server" ID="RqBoxCount" ControlToValidate="textBoxCount"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The Box Count is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                        TargetControlID="RqBoxCount" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    <asp:RangeValidator ID="rangeValidator1" runat="server" ControlToValidate="textBoxCount" MaximumValue="99" MinimumValue="1"
                                        ForeColor="Red" ErrorMessage="The Box Count Out of Range(1-99)" Type="Integer" Display="None" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                        TargetControlID="rangeValidator1" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    <span class="print-btn">
                                        <asp:Button runat="server" name="buttonBoxCount" ID="buttonBoxCount" OnClick="buttonBoxCount_Click" CausesValidation="true" 
                                            Text="Change" CssClass="create-btn btn-small green" />
                                    </span>
                                </fieldset>

                                <fieldset id="fieldSet1" >
                                    <label><asp:Label ID="lblOfficeName" CssClass="lblOfficeName" runat="server">Office Name</asp:Label></label>
                                    <asp:TextBox ID="textOfficeName" name="textOfficeName" ReadOnly="true" runat="server"
                                        size="50" MaxLength="23" />
                                </fieldset>

                                <fieldset>
                                    <label>Box Name</label>
                                    <asp:TextBox ID="textBoxName" name="textBoxName" runat="server"
                                        size="50" MaxLength="23" />
                                    <asp:DropDownList ID="dropBoxNumber" runat="server" Width="65" Visible="false" AutoPostBack="true" DataTextField="Number" DataValueField="BarNumber"
                                        OnSelectedIndexChanged="dropBoxNumber_OnSelectedIndexChanged" />

                                    <asp:RequiredFieldValidator runat="server" ID="telN" ControlToValidate="textBoxName"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The Box Name is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                        TargetControlID="telN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    <span class="print-btn">
                                        <asp:Button ID="boxPrint" Text="Print" Width="50px" runat="server" OnClick="boxPrint_OnClick" CssClass="create-btn btn-small green" />
                                    </span>
                                </fieldset>

                                <fieldset>
                                    <label>Retention Class</label>
                                    <asp:DropDownList runat="server" ID="dropRetention">
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                    </asp:DropDownList>
                                </fieldset>

                                <fieldset>
                                    <label>Box Number</label>
                                    <asp:TextBox ID="textBoxNumber" name="textBoxNumber" runat="server"
                                        size="20" MaxLength="15" ReadOnly="true" />
                                    <span class="barcode">
                                        <asp:Image ID="image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="" />
                                    </span>
                                </fieldset>

                                <fieldset>
                                    <label>Warehouse</label>
                                    <asp:TextBox ID="textWarehouse" name="textWarehouse" ReadOnly="false" runat="server"
                                        size="50" MaxLength="23" />
                                </fieldset>

                                <fieldset>
                                    <label>Section</label>
                                    <asp:TextBox ID="textSection" name="textSection" ReadOnly="false" runat="server"
                                        size="50" MaxLength="23" />
                                </fieldset>

                                <fieldset>
                                    <label>Row</label>
                                    <asp:TextBox ID="textRow" name="textRow" ReadOnly="false" runat="server"
                                        size="50" MaxLength="23" />
                                </fieldset>

                                <fieldset>
                                    <label>Space</label>
                                    <asp:TextBox ID="textSpace" name="textSpace" ReadOnly="false" runat="server"
                                        size="50" MaxLength="50" />
                                </fieldset>

                                <fieldset>
                                    <label>Other</label>

                                    <asp:TextBox ID="textOther" name="textOther" ReadOnly="false" runat="server"
                                        size="50" MaxLength="50" />
                                </fieldset>

                                <fieldset>
                                    <label>Scanned Date</label>
                                    <asp:TextBox ID="textScannedDate" CssClass="calenderTxtBox" name="textScannedDate" runat="server"
                                        size="50" />
                                    <asp:Button runat="server" CssClass="cancelBtn" OnClientClick="clearDate(this);return false;" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                        TargetControlID="textScannedDate" Format="MM-dd-yyyy" PopupButtonID="textScannedDate" OnClientShown="onCalendarShown">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RegularExpressionValidator ID="SodC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The scanned date is not a date"
                                        ControlToValidate="textScannedDate" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))|Not Scanned"
                                        Visible="true" Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
                                        TargetControlID="SodC" HighlightCssClass="validatorCalloutHighlight" />
                                </fieldset>

                                <fieldset>
                                    <label>Destroy Date</label>
                                    <asp:TextBox ID="textDestroyDate" CssClass="calenderTxtBox" name="textDestroyDate" runat="server"
                                        size="50" />
                                    <asp:Button runat="server" CssClass="cancelBtn" OnClientClick="clearDate(this);return false;" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                        TargetControlID="textDestroyDate" Format="MM-dd-yyyy" OnClientShown="onCalendarShown">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RegularExpressionValidator ID="DobC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The destroy date is not a date"
                                        ControlToValidate="textDestroyDate" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))|Never"
                                        Visible="true" Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6"
                                        TargetControlID="DobC" HighlightCssClass="validatorCalloutHighlight" />
                                </fieldset>

                                 <fieldset>
                                    <label>Box Pick-up Date</label>
                                     <asp:HiddenField runat="server" ID="hdnPickupDate" />
                                    <asp:TextBox ID="textPickupDate" CssClass="calenderTxtBox" name="textPickupDate" runat="server"
                                        size="50" />
                                    <asp:Button runat="server" CssClass="cancelBtn" OnClientClick="clearDate(this);return false;" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                        TargetControlID="textPickupDate" Format="MM-dd-yyyy" OnClientShown="onCalendarShown">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RegularExpressionValidator ID="BodC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The box pick-up date is not a date"
                                        ControlToValidate="textPickupDate" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                        Visible="true" Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
                                        TargetControlID="BodC" HighlightCssClass="validatorCalloutHighlight" />
                                </fieldset>
                            </div>
                            <div class="content-box listing-view upload-area" style="margin-top: 15px;">
                                <p>1. Please provide Excel Sheet File with Columns(First Name, Last Name, File#, File Class)</p>
                               <%-- <div class="input-file">--%>
                                    <asp:FileUpload ID="fileExlt1" runat="server" CssClass="input-file" ></asp:FileUpload>
                                <%--</div>--%>
                                <asp:Button runat="server" name="buttonUpload1" ID="buttonUpload1" OnClick="imageField_Click"
                                    Text="Save" CssClass="create-btn btn-small blue radius-none savebox" OnClientClick="return validatePastDate();" />
                                <%-- <asp:Button runat="server" name="buttonUpload1" ID="buttonUpload1" OnClick="imageField_Click" CssClass="create-btn btn-small blue radius-none"
                                    Text="Save" />--%>
                            </div>
                            <div class="content-box listing-view" style="margin-top: 15px; border: 0; padding: 0;">
                                <p>
                                    <strong>2. Please provide complete File information (First Name, Last Name, File#, File Class)</strong>
                                </p>
                                <div>&nbsp;</div>
                                <p>
                                    <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                                </p>

                                <ajaxToolkit:TabContainer runat="server" ID="fileTabs" ActiveTabIndex="0">

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel1" HeaderText="File01-10">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName1" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName1" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile1" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName1" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName2" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName2" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile2" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName2" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName3" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName3" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile3" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName3" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName4" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName4" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile4" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName4" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName5" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName5" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile5" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName5" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName6" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName6" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile6" size="30" MaxLength="30" />
                                                                    <label>File Class </label>
                                                                    <asp:TextBox runat="server" ID="textFileName6" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName7" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName7" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile7" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName7" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName8" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName8" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile8" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName8" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName9" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName9" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile9" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName9" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName10" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName10" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile10" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName10" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel2" HeaderText="File11-20">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName11" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName11" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile11" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName11" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName12" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName12" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile12" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName12" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName13" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName13" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile13" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName13" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName14" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName14" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile14" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName14" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName15" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName15" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile15" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName15" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName16" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName16" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile16" size="30" MaxLength="30" />
                                                                    <label>File Class </label>
                                                                    <asp:TextBox runat="server" ID="textFileName16" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName17" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName17" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile17" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName17" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName18" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName18" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile18" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName18" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName19" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName19" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile19" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName19" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName20" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName20" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile20" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName20" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel3" HeaderText="File21-30">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName21" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName21" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile21" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName21" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName22" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName22" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile22" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName22" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName23" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName23" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile23" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName23" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName24" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName24" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile24" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName24" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName25" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName25" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile25" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName25" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName26" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName26" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile26" size="30" MaxLength="30" />
                                                                    <label>File Class </label>
                                                                    <asp:TextBox runat="server" ID="textFileName26" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName27" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName27" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile27" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName27" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName28" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName28" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile28" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName28" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName29" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName29" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile29" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName29" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName30" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName30" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile30" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName30" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel4" HeaderText="File31-40">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName31" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName31" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile31" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName31" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName32" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName32" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile32" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName32" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName33" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName33" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile33" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName33" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName34" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName34" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile34" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName34" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName35" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName35" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile35" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName35" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName36" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName36" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile36" size="30" MaxLength="30" />
                                                                    <label>File Class </label>
                                                                    <asp:TextBox runat="server" ID="textFileName36" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName37" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName37" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile37" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName37" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName38" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName38" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile38" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName38" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName39" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName39" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile39" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName39" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName40" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName40" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile40" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName40" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel5" HeaderText="File41-50">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName41" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName41" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile41" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName41" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName42" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName42" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile42" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName42" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName43" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName43" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile43" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName43" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName44" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName44" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile44" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName44" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName45" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName45" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile45" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName45" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName46" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName46" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile46" size="30" MaxLength="30" />
                                                                    <label>File Class </label>
                                                                    <asp:TextBox runat="server" ID="textFileName46" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName47" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName47" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile47" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName47" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName48" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName48" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile48" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName48" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName49" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName49" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile49" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName49" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName50" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName50" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile50" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName50" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel6" HeaderText="File51-60">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName51" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName51" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile51" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName51" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName52" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName52" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile52" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName52" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName53" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName53" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile53" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName53" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName54" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName54" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile54" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName54" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName55" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName55" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile55" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName55" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName56" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName56" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile56" size="30" MaxLength="30" />
                                                                    <label>File Class </label>
                                                                    <asp:TextBox runat="server" ID="textFileName56" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName57" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName57" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile57" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName57" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName58" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName58" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile58" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName58" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName59" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName59" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile59" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName59" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName60" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName60" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile60" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName60" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel7" HeaderText="File61-70">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName61" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName61" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile61" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName61" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName62" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName62" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile62" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName62" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName63" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName63" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile63" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName63" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName64" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName64" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile64" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName64" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName65" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName65" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile65" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName65" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName66" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName66" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile66" size="30" MaxLength="30" />
                                                                    <label>File Class </label>
                                                                    <asp:TextBox runat="server" ID="textFileName66" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName67" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName67" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile67" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName67" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName68" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName68" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile68" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName68" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName69" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName69" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile69" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName69" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName70" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName70" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile70" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName70" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel8" HeaderText="File71-80">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName71" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName71" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile71" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName71" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName72" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName72" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile72" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName72" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName73" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName73" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile73" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName73" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName74" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName74" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile74" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName74" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName75" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName75" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile75" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName75" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName76" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName76" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile76" size="30" MaxLength="30" />
                                                                    <label>File Class </label>
                                                                    <asp:TextBox runat="server" ID="textFileName76" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName77" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName77" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile77" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName77" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName78" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName78" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile78" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName78" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName79" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName79" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile79" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName79" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName80" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName80" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile80" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName80" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel9" HeaderText="File81-90">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName81" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName81" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile81" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName81" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName82" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName82" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile82" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName82" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName83" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName83" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile83" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName83" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName84" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName84" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile84" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName84" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName85" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName85" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile85" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName85" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName86" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName86" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile86" size="30" MaxLength="30" />
                                                                    <label>File Class </label>
                                                                    <asp:TextBox runat="server" ID="textFileName86" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName87" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName87" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile87" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName87" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName88" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName88" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile88" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName88" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName89" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName89" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile89" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName89" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName90" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName90" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile90" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName90" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel10" HeaderText="File91-100">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName91" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName91" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile91" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName91" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName92" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName92" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile92" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName92" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName93" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName93" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile93" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName93" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName94" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName94" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile94" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName94" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName95" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName95" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile95" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName95" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName96" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName96" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile96" size="30" MaxLength="30" />
                                                                    <label>File Class </label>
                                                                    <asp:TextBox runat="server" ID="textFileName96" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName97" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName97" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile97" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName97" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName98" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName98" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile98" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName98" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName99" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName99" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile99" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName99" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>

                                                        <tr class="even">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name </label>
                                                                    <asp:TextBox runat="server" ID="textFirstName100" size="30"
                                                                        MaxLength="30" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName100" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile100" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileName100" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>
                                </ajaxToolkit:TabContainer>

                                <fieldset>
                                    <label>&nbsp;</label>
                                    <div style="text-align: center">
                                        <asp:Button runat="server" name="imageField" ID="imageField" OnClick="imageField_Click"
                                            Text="Save" CssClass="create-btn btn green" />
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="right-content">
                            <%--<div class="quick-find">
                                <uc1:QuickFind ID="QuickFind1" runat="server" />
                            </div>--%>
                            <div class="quick-find" style="margin-top: 15px;">
                                <div class="find-inputbox">
                                    <marquee scrollamount="1" id="marqueeside" runat="server"
                                    behavior="scroll" onmouseover="this.stop()" onmouseout="this.start()" scrolldelay="20"
                                    direction="up">
                                <%=MessageBody%></marquee>
                                </div>
                            </div>
                            <%-- <div class="quick-find" style="margin-top: 15px;">
                                <uc2:FileRequestControl ID="fileRequest1" runat="server" />
                            </div>--%>
                            <div class="quick-find" style="margin-top: 15px;">
                                <uc2:BoxRequestControl ID="fileRequest1" runat="server" />
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="imageField" EventName="Click" />
                        <asp:PostBackTrigger ControlID="buttonUpload1" />
                    </Triggers>
                </asp:UpdatePanel>

            </div>
        </div>
    </div>

    <asp:Button ID="Button1ShowPopup" runat="server" Style="display: none" />
    <ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" PopupControlID="pnlPopup"
        OnOkScript="anotherRequest()" CancelControlID="btnClose" OnCancelScript="closePage()"
        OkControlID="btnRequset" DropShadow="true" BackgroundCssClass="modalBackground"
        TargetControlID="Button1ShowPopup" />
    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <!-- ModalPopup Panel-->
            <asp:Panel ID="pnlPopup" runat="server" tyle="display: none;" CssClass="popup-mainbox">
                <div id="div1" class="popup-head">File Request</div>
                <div style="background-color: White">
                    <div style="background-color: White">
                        <table width="100%" class="details-datatable">
                            <tr>
                                <td>
                                    <strong>Hello&nbsp;<asp:Label ID="LabelUserName" Text="" runat="server" />,</strong>
                                </td>
                            </tr>
                            <tr>
                                <td>Your request: {<strong><asp:Label ID="LabelResult" Text="" runat="server" /></strong>
                                    }&nbsp;&nbsp;has been submitted.
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <div align="center" style="width: 100%; background-color: white; padding: 20px 0;">
                    <asp:Button ID="btnRequset" runat="server" Text="Request Another" Style="width: auto; margin: 2px 0" CssClass="create-btn btn green" />
                    <asp:Button ID="btnClose" runat="server" Text="Close" Style="margin: 2px 0" CssClass="create-btn btn green" />
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="imageField" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <script language="javascript" type="text/javascript">
        function anotherRequest() {
            window.location = window.location;

        }

        function closePage() {
            window.location = "http://www.edfiles.com/Office/Welcome.aspx";
        }
    </script>
    <!-- Preview Panel-->
    <asp:Button Style="display: none" ID="Button1Preview" runat="server"></asp:Button>
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="btnClose2" PopupControlID="Panel1Preview" TargetControlID="Button1Preview">
    </ajaxToolkit:ModalPopupExtender>
    <!-- ModalPopup Panel-->
    <asp:Panel Style="display: none; z-index: 10001" ID="Panel1Preview" runat="server"
        CssClass="popup-mainbox">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divContainer" class="popup-head">Box Details</div>
                <asp:HiddenField ID="HiddenField51" runat="server" />
                <div id="divContentContainer" style="background-color: White">
                    <div id="divBoxContent" style="background-color: White">
                        <table width="100%" class="details-datatable">
                            <tr style="border-width: 2px;">
                                <td>User Name
                                </td>
                                <td>
                                    <asp:TextBox ID="textUserName2" name="textUserName2" ReadOnly="true" runat="server"
                                        size="18" MaxLength="20" />
                                </td>
                            </tr>
                            <tr style="border-width: 2px;">
                                <td>Department Name
                                </td>
                                <td>
                                    <asp:TextBox ID="textDepartmentName2" name="textDepartmentName2" ReadOnly="true" runat="server"
                                        size="18" MaxLength="20" />
                                </td>
                            </tr>
                            <tr style="border-width: 2px;">
                                <td><asp:Label runat="server" ID="lblOfficeName2">Office Name</asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="textOfficeName2" name="textOfficeName2" ReadOnly="true" runat="server"
                                        size="18" MaxLength="20" />
                                </td>
                            </tr>
                            <tr>
                                <td>Box Name
                                </td>
                                <td>
                                    <asp:TextBox ID="textBoxName2" name="textBoxName2" runat="server"
                                        size="18" MaxLength="20" />
                                </td>
                            </tr>
                            <tr>
                                <td>Retention Class
                                </td>
                                <td>
                                    <asp:TextBox ID="textRetentionClass2" name="textRetentionClass2" runat="server"
                                        size="18" MaxLength="20" />
                                </td>
                            </tr>
                            <tr>
                                <td>Box Number
                                </td>
                                <td>
                                    <asp:TextBox ID="textBoxNumber2" name="textBoxNumber2" runat="server"
                                        size="18" MaxLength="18" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Image ID="imageBarcode" runat="server" ImageAlign="AbsMiddle" ImageUrl="" Style="padding-bottom: 10px; width: 95%;" />
                                </td>
                            </tr>
                        </table>
                    </div>

                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div align="center" style="width: 100%; background-color: white; padding: 20px 0;">
            <input id="btnprint" type="button" onclick='$("#divBoxContent").printArea();' value="Print"
                class="create-btn btn green" />
            <asp:Button ID="btnClose2" runat="server" Text="Close" CssClass="create-btn btn green" />
        </div>
    </asp:Panel>

    <asp:Button ID="Button1" runat="server" Style="display: none" />
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel67"
        DropShadow="true" BackgroundCssClass="modalBackground" TargetControlID="Button1" CancelControlID="anotherBox" />
    <!-- ModalPopup Panel-->

    <asp:Panel ID="Panel67" runat="server" Style="display: none;" CssClass="popup-mainbox">
        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="popup-head">Message</div>
                <div style="background-color: White">
                    <div style="background-color: White">
                        <table width="100%" class="details-datatable">
                            <tr>
                                <td>
                                    <strong>Box Name "<asp:LinkButton ID="viewBox" OnClick="viewBox_OnClick" runat="server"></asp:LinkButton>" has been created in your stored files." </strong>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:HiddenField runat="server" ID="hidBoxId" />
                                    <asp:LinkButton ID="anotherBox" OnClick="newBox_OnClick" runat="server">Create New Box</asp:LinkButton>
                                    <asp:LinkButton ID="searchforHome" OnClick="searchBox_OnClick" runat="server">Search for Home</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div align="center" style="width: 100%; background-color: white;">
                        <asp:Button ID="Button2" runat="server" Style="margin: 10px 0" Text="Ok" OnClick="btnOk_OnClick" CssClass="create-btn btn green" />
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Button2" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="viewBox" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="anotherBox" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="searchforHome" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>


</asp:Content>
