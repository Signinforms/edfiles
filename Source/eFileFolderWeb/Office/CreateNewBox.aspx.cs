using System;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Collections;

using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;

using DataQuicker2.Framework;
using ExcelDataReader;
using Shinetech.DAL;
using Shinetech.Utility;
using Account = Shinetech.DAL.Account;
using BarcodeLib;

public partial class CreateOfficeBox : LicensePage
{


    protected void Page_Load(object sender, EventArgs e)
    {
        // enctype="multipart/form-data"
        this.Form.Enctype = "multipart/form-data";

        if (!Page.IsPostBack)
        {
            textBoxCount.Text = "1";
            dropBoxNumber.Visible = false;

            //CalendarExtender2.SelectedDate = DateTime.Now.AddYears(1);
            //CalendarExtender2.SelectedDate =Convert.ToDateTime(CalendarExtender2.SelectedDate.Value.ToString("MM-dd-yyy"));

            BarcodeLib.Barcode b = new BarcodeLib.Barcode();
            b.IncludeLabel = false;

            b.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
            b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;
            BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39Extended;
            textBoxNumber.Text = Guid.NewGuid().ToString("N").Substring(0, 15).ToUpper();
            b.Encode(type, textBoxNumber.Text,
                Color.Black, Color.Bisque, 400, 25);

            image1.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(b.GetImageData(SaveTypes.JPG));

            textUserName.Text = Sessions.SwitchedUserName;
            //textUserName.Text = Membership.GetUser().UserName;
            //textScannedDate.Attributes["style"] = "background: url(../images/calendar-interface-symbol-tool.png) 100% 2px no-repeat;";
            //textDestroyDate.Attributes["style"] = "background: url(../images/calendar-interface-symbol-tool.png) 100% 2px no-repeat;";
            //textPickupDate.Attributes["style"] = "background: url(../images/calendar-interface-symbol-tool.png) 100% 2px no-repeat;";
            //textPickupDate.Text = DateTime.Now.ToString("MM-dd-yyyy");
            hdnPickupDate.Value = DateTime.Now.ToString("MM-dd-yyyy");
            if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users"))
            {
                lblOfficeName.Visible = true;
                DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(Sessions.SwitchedSessionId);
                textOfficeName.Text = dtOfficeUser.Rows[1]["Name"].ToString();
            }
            else
            {
                textOfficeName.Visible = false;
                lblOfficeName.Visible = false;
            }
        }
    }


    public string MessageBody
    {
        get
        {
            return this.Session["_Side_MessageBody"] as string;
        }

        set
        {
            this.Session["_Side_MessageBody"] = value;
        }
    }

    protected void imageField_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            if (!fileExlt1.HasFile)
            {
                if (dropBoxNumber.Visible && dropBoxNumber.Items.Count > 0)
                {
                    foreach (Barcode item in BoxSource)
                    {
                        #region Box from input tabs, crete many box at this place

                        Box box = new Box();
                        box.BoxName.Value = string.Format("{0}-{1}", textBoxName.Text.Trim(), item.Number);
                        box.BoxNumber.Value = item.BarNumber;
                        box.UserId.Value = Sessions.SwitchedSessionId;
                        //box.UserId.Value = Membership.GetUser().ProviderUserKey.ToString();

                        box.Warehouse.Value = textWarehouse.Text.Trim();
                        box.Section.Value = textSection.Text.Trim();
                        box.Row.Value = textRow.Text.Trim();
                        box.Space.Value = textSpace.Text.Trim();
                        box.Other.Value = textOther.Text.Trim();
                        box.BoxPickupDate.Value = Convert.ToDateTime(textPickupDate.Text);
                        box.DepartmentName.Value = textDepartmentName.Text.Trim();
                        box.RetentionClass.Value = Convert.ToInt16(dropRetention.SelectedItem.Value);
                        if (textScannedDate.Text != null && textScannedDate.Text != "Not Scanned" && textScannedDate.Text != "")
                            box.ScannedDate.Value = Convert.ToDateTime(textScannedDate.Text);
                        //if (CalendarExtender2.SelectedDate != null)                       
                        //box.DestroyDate.Value = (DateTime)CalendarExtender2.SelectedDate;
                        //if (Convert.ToDateTime(textDestroyDate.Text) >= DateTime.Now)
                        if (textDestroyDate.Text != null && textDestroyDate.Text != "Never" && textDestroyDate.Text != "")
                            box.DestroyDate.Value = Convert.ToDateTime(textDestroyDate.Text);

                        box.CreateDate.Value = DateTime.Now;

                        if (ExistBox(box.BoxName.Value, box.UserId.Value))
                        {
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                                        "alert(\"" + "This Box Name '" + box.BoxName.Value + "' existed already." + "\");",
                                                                        true);
                            return;
                        }

                        IDbConnection connection = DbFactory.CreateConnection();
                        connection.Open();
                        IDbTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            ArrayList items = new ArrayList();
                            int tabIndex = 0;
                            for (int i = 1; i <= 100; i++)  // 50 - 100
                            {
                                tabIndex = ((int)Math.Ceiling(i / 10f)) - 1;
                                //create paper file
                                PaperFile file1 = new PaperFile();
                                file1.FirstName.Value = ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFirstName" + i)).Text.Trim();
                                file1.LastName.Value = ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textLastName" + i)).Text.Trim();
                                file1.FileNumber.Value = ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFile" + i)).Text.Trim();
                                file1.FileName.Value = ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFileName" + i)).Text.Trim();
                                file1.UserID.Value = box.UserId.Value;
                                file1.FileIndex.Value = i;

                                items.Add(file1);
                            }

                            box.Create(transaction);

                            foreach (PaperFile file in items)
                            {
                                file.BoxId.Value = box.BoxId.Value;
                                file.Create(transaction);
                            }

                            transaction.Commit();

                            hidBoxId.Value = box.BoxId.ToString();
                            viewBox.Text = box.BoxName.Value;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();

                            string strWrongInfor = string.Format("Error : {0}", ex.Message);
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                        }

                        #endregion
                    }

                    UpdatePanel4.Update();
                    ModalPopupExtender1.Show();

                }
                else
                {
                    #region Box from input tabs, crete many box at this place

                    Box box = new Box();
                    box.BoxName.Value = textBoxName.Text.Trim();
                    box.BoxNumber.Value = textBoxNumber.Text.Trim();
                    box.UserId.Value = Sessions.SwitchedSessionId;
                    //box.UserId.Value = Membership.GetUser().ProviderUserKey.ToString();

                    box.Warehouse.Value = textWarehouse.Text.Trim();
                    box.Section.Value = textSection.Text.Trim();
                    box.Row.Value = textRow.Text.Trim();
                    box.Space.Value = textSpace.Text.Trim();
                    box.Other.Value = textOther.Text.Trim();
                    box.BoxPickupDate.Value = Convert.ToDateTime(textPickupDate.Text);
                    box.DepartmentName.Value = textDepartmentName.Text.Trim();
                    box.RetentionClass.Value = Convert.ToInt16(dropRetention.SelectedItem.Value);
                    if (textScannedDate.Text != null && textScannedDate.Text != "Not Scanned" && textScannedDate.Text != "")
                        box.ScannedDate.Value = Convert.ToDateTime(textScannedDate.Text);
                    //if (CalendarExtender2.SelectedDate != null)                       
                    //box.DestroyDate.Value = (DateTime)CalendarExtender2.SelectedDate;
                    //if (Convert.ToDateTime(textDestroyDate.Text) >= DateTime.Now)
                    if (textDestroyDate.Text != null && textDestroyDate.Text != "Never" && textDestroyDate.Text != "")
                        box.DestroyDate.Value = Convert.ToDateTime(textDestroyDate.Text);

                    box.CreateDate.Value = DateTime.Now;

                    if (ExistBox(box.BoxName.Value, box.UserId.Value))
                    {
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                                    "alert(\"" + "This Box Name '" + box.BoxName.Value + "' existed already." + "\");",
                                                                    true);
                        return;
                    }

                    IDbConnection connection = DbFactory.CreateConnection();
                    connection.Open();
                    IDbTransaction transaction = connection.BeginTransaction();
                    try
                    {
                        ArrayList items = new ArrayList();
                        int tabIndex = 0;
                        for (int i = 1; i <= 100; i++)  // 50 - 100
                        {
                            tabIndex = ((int)Math.Ceiling(i / 10f)) - 1;
                            //create paper file
                            PaperFile file1 = new PaperFile();
                            file1.FirstName.Value = ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFirstName" + i)).Text.Trim();
                            file1.LastName.Value = ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textLastName" + i)).Text.Trim();
                            file1.FileNumber.Value = ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFile" + i)).Text.Trim();
                            file1.FileName.Value = ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFileName" + i)).Text.Trim();
                            file1.UserID.Value = box.UserId.Value;
                            file1.FileIndex.Value = i;

                            items.Add(file1);
                        }

                        box.Create(transaction);

                        foreach (PaperFile file in items)
                        {
                            file.BoxId.Value = box.BoxId.Value;
                            file.Create(transaction);
                        }

                        transaction.Commit();

                        hidBoxId.Value = box.BoxId.ToString();
                        viewBox.Text = box.BoxName.Value;
                        UpdatePanel4.Update();
                        ModalPopupExtender1.Show();

                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();

                        string strWrongInfor = string.Format("Error : {0}", ex.Message);
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                    }

                    #endregion
                }


            }
            else
            {
                IExcelDataReader reader = null;

                try
                {
                    if (Path.GetExtension(fileExlt1.FileName).ToLower().Equals(".xlsx"))
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(fileExlt1.PostedFile.InputStream);
                    }
                    else if (Path.GetExtension(fileExlt1.FileName).ToLower().Equals(".xls"))
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(fileExlt1.PostedFile.InputStream);
                    }
                    else
                    {
                        throw new ApplicationException("Please Provide Excel File with .xlsx or .xls extension");
                    }

                    DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration
                    {
                        ConfigureDataTable = (_) => new ExcelDataTableConfiguration
                        {
                            UseHeaderRow = true
                        }
                    });

                    if (dropBoxNumber.Visible && dropBoxNumber.Items.Count > 0)
                    {
                        foreach (Barcode item in BoxSource)
                        {
                            #region Box from fileupload, crete many box at this place

                            Box box = new Box();
                            box.BoxName.Value = string.Format("{0}-{1}", textBoxName.Text.Trim(), item.Number);
                            box.BoxNumber.Value = item.BarNumber;
                            box.UserId.Value = Sessions.SwitchedSessionId;
                            //box.UserId.Value = Membership.GetUser().ProviderUserKey.ToString();

                            box.Warehouse.Value = textWarehouse.Text.Trim();
                            box.Section.Value = textSection.Text.Trim();
                            box.Row.Value = textRow.Text.Trim();
                            box.Space.Value = textSpace.Text.Trim();
                            box.Other.Value = textOther.Text.Trim();
                            box.BoxPickupDate.Value = Convert.ToDateTime(textPickupDate.Text);
                            box.DepartmentName.Value = textDepartmentName.Text.Trim();
                            box.RetentionClass.Value = Convert.ToInt16(dropRetention.SelectedItem.Value);
                            if (textScannedDate.Text != null && textScannedDate.Text != "Not Scanned" && textScannedDate.Text != "")
                                box.ScannedDate.Value = Convert.ToDateTime(textScannedDate.Text);
                            //if (CalendarExtender2.SelectedDate != null)                       
                            //box.DestroyDate.Value = (DateTime)CalendarExtender2.SelectedDate;
                            //if (Convert.ToDateTime(textDestroyDate.Text) >= DateTime.Now)
                            if (textDestroyDate.Text != null && textDestroyDate.Text != "Never" && textDestroyDate.Text != "")
                                box.DestroyDate.Value = Convert.ToDateTime(textDestroyDate.Text);

                            box.CreateDate.Value = DateTime.Now;

                            if (ExistBox(box.BoxName.Value, box.UserId.Value))
                            {
                                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                                        "alert(\"" + "This Box Name '" + box.BoxName.Value + "' existed already." + "\");",
                                                                        true);
                                return;
                            }

                            IDbConnection connection = DbFactory.CreateConnection();
                            connection.Open();
                            IDbTransaction transaction = connection.BeginTransaction();
                            try
                            {
                                ArrayList items = new ArrayList();

                                for (int i = 0; i < 100; i++) // 50 - 100
                                {
                                    if (result.Tables[0].Rows.Count > i)
                                    {
                                        DataRow row = result.Tables[0].Rows[i];
                                        //create paper file
                                        PaperFile file1 = new PaperFile();
                                        file1.FirstName.Value = row[0].ToString().Trim();
                                        file1.LastName.Value = row[1].ToString().Trim();
                                        file1.FileNumber.Value = row[2].ToString().Trim();
                                        file1.FileName.Value = row[3].ToString().Trim();
                                        file1.UserID.Value = box.UserId.Value;
                                        file1.FileIndex.Value = i + 1;

                                        items.Add(file1);
                                    }
                                    else
                                    {
                                        //create paper file
                                        PaperFile file1 = new PaperFile();
                                        file1.FirstName.Value = string.Empty;
                                        file1.LastName.Value = string.Empty;
                                        file1.FileNumber.Value = string.Empty;
                                        file1.FileName.Value = string.Empty;
                                        file1.UserID.Value = box.UserId.Value;
                                        file1.FileIndex.Value = i + 1;

                                        items.Add(file1);
                                    }

                                }

                                box.Create(transaction);

                                foreach (PaperFile file in items)
                                {
                                    file.BoxId.Value = box.BoxId.Value;
                                    file.Create(transaction);
                                }

                                transaction.Commit();

                                hidBoxId.Value = box.BoxId.ToString();
                                viewBox.Text = box.BoxName.Value;
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();

                                string strWrongInfor = string.Format("Error : {0}", ex.Message);
                                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                                        "alert(\"" + strWrongInfor + "\");", true);
                            }

                            #endregion
                        }

                        UpdatePanel4.Update();
                        ModalPopupExtender1.Show();
                    }
                    else
                    {
                        #region Box from fileupload, crete many box at this place

                        Box box = new Box();
                        box.BoxName.Value = textBoxName.Text.Trim();
                        box.BoxNumber.Value = textBoxNumber.Text.Trim();
                        box.UserId.Value = Sessions.SwitchedSessionId;
                        //box.UserId.Value = Membership.GetUser().ProviderUserKey.ToString();

                        box.Warehouse.Value = textWarehouse.Text.Trim();
                        box.Section.Value = textSection.Text.Trim();
                        box.Row.Value = textRow.Text.Trim();
                        box.Space.Value = textSpace.Text.Trim();
                        box.Other.Value = textOther.Text.Trim();
                        box.BoxPickupDate.Value = Convert.ToDateTime(textPickupDate.Text);
                        box.DepartmentName.Value = textDepartmentName.Text.Trim();
                        box.RetentionClass.Value = Convert.ToInt16(dropRetention.SelectedItem.Value);
                        if (textScannedDate.Text != null && textScannedDate.Text != "Not Scanned" && textScannedDate.Text != "")
                            box.ScannedDate.Value = Convert.ToDateTime(textScannedDate.Text);
                        //if (CalendarExtender2.SelectedDate != null)                       
                        //box.DestroyDate.Value = (DateTime)CalendarExtender2.SelectedDate;
                        //if (Convert.ToDateTime(textDestroyDate.Text) >= DateTime.Now)
                        if (textDestroyDate.Text != null && textDestroyDate.Text != "Never" && textDestroyDate.Text != "")
                            box.DestroyDate.Value = Convert.ToDateTime(textDestroyDate.Text);

                        box.CreateDate.Value = DateTime.Now;

                        if (ExistBox(box.BoxName.Value, box.UserId.Value))
                        {
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                                    "alert(\"" + "This Box Name '" + box.BoxName.Value + "' existed already." + "\");",
                                                                    true);
                            return;
                        }

                        IDbConnection connection = DbFactory.CreateConnection();
                        connection.Open();
                        IDbTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            ArrayList items = new ArrayList();

                            for (int i = 0; i < 100; i++) // 50 - 100
                            {
                                if (result.Tables[0].Rows.Count > i)
                                {
                                    DataRow row = result.Tables[0].Rows[i];
                                    //create paper file
                                    PaperFile file1 = new PaperFile();
                                    file1.FirstName.Value = row[0].ToString().Trim();
                                    file1.LastName.Value = row[1].ToString().Trim();
                                    file1.FileNumber.Value = row[2].ToString().Trim();
                                    file1.FileName.Value = row[3].ToString().Trim();
                                    file1.UserID.Value = box.UserId.Value;
                                    file1.FileIndex.Value = i + 1;

                                    items.Add(file1);
                                }
                                else
                                {
                                    //create paper file
                                    PaperFile file1 = new PaperFile();
                                    file1.FirstName.Value = string.Empty;
                                    file1.LastName.Value = string.Empty;
                                    file1.FileNumber.Value = string.Empty;
                                    file1.FileName.Value = string.Empty;
                                    file1.UserID.Value = box.UserId.Value;
                                    file1.FileIndex.Value = i + 1;

                                    items.Add(file1);
                                }

                            }

                            box.Create(transaction);

                            foreach (PaperFile file in items)
                            {
                                file.BoxId.Value = box.BoxId.Value;
                                file.Create(transaction);
                            }

                            transaction.Commit();

                            hidBoxId.Value = box.BoxId.ToString();
                            viewBox.Text = box.BoxName.Value;
                            UpdatePanel4.Update();
                            ModalPopupExtender1.Show();

                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();

                            string strWrongInfor = string.Format("Error : {0}", ex.Message);
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                                    "alert(\"" + strWrongInfor + "\");", true);
                        }

                        #endregion
                    }


                }
                catch (Exception ex)
                {
                    string strWrongInfor = string.Format("Error : {0}", ex.Message);
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                            "alert(\"" + strWrongInfor + "\");", true);
                }
                finally
                {
                    if (reader != null)
                    {
                        reader.Close();
                    }
                }
            }
        }

    }

    public bool ExistBox(string name, string uid)
    {
        return FileFolderManagement.ExistBox(name, uid);
    }


    public bool IsFileValid()
    {
        if (textFirstName1.Text.Trim() == "" && textFirstName2.Text.Trim() == "" && textFirstName3.Text.Trim() == ""
                && textFirstName4.Text.Trim() == "" && textFirstName5.Text.Trim() == "")
        {
            string strWrongInfor = string.Format("Error : You have to insert one file at least.");
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
            return false;
        }

        if (textFirstName1.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName1.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 1th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }

        if (textFirstName2.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName2.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 2th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }

        if (textFirstName3.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName3.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 3th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }

        if (textFirstName4.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName3.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 4th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }

        if (textFirstName5.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName5.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 4th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }


        return true;
    }

    protected void DropDownList1_OnSelectedIndexChanged(object sender, EventArgs e)
    {

    }

    private void sendeFileFormEmail(FileForm form, ArrayList items)
    {
        string strMailTemplet = getFileFormMailTemplate();

        strMailTemplet = strMailTemplet.Replace("[textOfficeName]", form.OfficeName.Value);
        strMailTemplet = strMailTemplet.Replace("[textUserName]", Sessions.SwitchedUserName);
        //strMailTemplet = strMailTemplet.Replace("[textUserName]", Membership.GetUser().UserName);
        strMailTemplet = strMailTemplet.Replace("[textPhone]", form.PhoneNumber.Value);
        strMailTemplet = strMailTemplet.Replace("[textRequestBy]", form.RequestedBy.Value);
        strMailTemplet = strMailTemplet.Replace("[textRequestDate]", form.RequestDate.Value.ToShortDateString());
        strMailTemplet = strMailTemplet.Replace("[textOfficeName]", form.OfficeName.Value);

        strMailTemplet = strMailTemplet.Replace("[textComment]", form.Comment.Value);

        int count = items.Count;
        int delta = 5 - count;

        FormFile file;

        for (int i = 0; i < count; i++)
        {
            file = items[i] as FormFile;
            strMailTemplet = strMailTemplet.Replace("[textFirstName" + (i + 1) + "]", file.FirstName.Value);
            strMailTemplet = strMailTemplet.Replace("[textLastName" + (i + 1) + "]", file.LastName.Value);
            strMailTemplet = strMailTemplet.Replace("[textFile" + (i + 1) + "]", file.FileNumber.Value);
            strMailTemplet = strMailTemplet.Replace("[textYear" + (i + 1) + "]", file.YearDate.Value);

        }

        for (int i = 5; i > count; i--)
        {
            strMailTemplet = strMailTemplet.Replace("[textFirstName" + i + "]", "");
            strMailTemplet = strMailTemplet.Replace("[textLastName" + i + "]", "");
            strMailTemplet = strMailTemplet.Replace("[textFile" + i + "]", "");
            strMailTemplet = strMailTemplet.Replace("[textYear" + i + "]", "");

        }

        string strEmailSubject = "File Form Request";

        //Need To Check.
        Email.SendFileRequestService(Membership.GetUser().Email, strEmailSubject, strMailTemplet);

    }


    private string getFileFormMailTemplate()
    {

        string strMailTemplet = string.Empty;
        string physicalPath = Request.PhysicalPath;
        int index = physicalPath.LastIndexOf("\\");
        string mailTempletUrl = physicalPath.Substring(0, index + 1) + "FileForm.html";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    protected void btnClose_OnClick(object sender, EventArgs e)
    {
        Page.Response.Redirect("~/Office/Welcome.aspx");
    }

    protected void anotherRequest_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect("~/Office/CreateFileRequest.aspx");
    }

    protected void boxPrint_OnClick(object sender, EventArgs e)
    {
        BarcodeLib.Barcode b = new BarcodeLib.Barcode();
        b.IncludeLabel = false;

        b.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
        b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;
        BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39Extended;

        textUserName2.Text = textUserName.Text;
        textRetentionClass2.Text = dropRetention.SelectedItem.Value;
        if (this.Page.User.IsInRole("Offices"))
        {
            textOfficeName2.Visible = false;
            lblOfficeName2.Visible = false;
        }
        else
        {
            lblOfficeName2.Visible = true;
            textOfficeName2.Visible = true;
            textOfficeName2.Text = textOfficeName.Text;
        }
        textOfficeName2.Text = textOfficeName.Text;
        textDepartmentName2.Text = textDepartmentName.Text;
        textBoxName2.Text = textBoxName.Text;
        if (dropBoxNumber.Visible)
        {
            textBoxName2.Text = string.Format("{0}-{1}", textBoxName.Text, dropBoxNumber.SelectedItem.Text);
        }

        textBoxNumber2.Text = textBoxNumber.Text;

        b.Encode(type, textBoxNumber.Text,
                Color.Black, Color.Bisque, 400, 40);
        imageBarcode.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(b.GetImageData(SaveTypes.JPG));

        UpdatePanel2.Update();
        ModalPopupExtender2.Show();
    }

    protected void btnOk_OnClick(object sender, EventArgs e)
    {
        this.Response.Redirect("~/Office/Welcome.aspx", true);
    }

    protected void viewBox_OnClick(object sender, EventArgs e)
    {
        this.Response.Redirect("~/Office/ModifyBox.aspx?id=" + hidBoxId.Value, true);
    }

    protected void newBox_OnClick(object sender, EventArgs e)
    {
        textBoxName.Text = string.Empty;

        CalendarExtender2.SelectedDate = DateTime.Now.AddYears(1);
        BarcodeLib.Barcode b = new BarcodeLib.Barcode();
        b.IncludeLabel = false;

        b.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
        b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;
        BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39Extended;
        textBoxNumber.Text = Guid.NewGuid().ToString("N").Substring(0, 15).ToUpper();
        b.Encode(type, textBoxNumber.Text,
            Color.Black, Color.Bisque, 400, 25);

        image1.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(b.GetImageData(SaveTypes.JPG));

        UpdatePanel4.Update();
        ModalPopupExtender1.Hide();
    }

    protected void searchBox_OnClick(object sender, EventArgs e)
    {
        this.Response.Redirect("~/Office/StoredPaperFiles.aspx", true);
    }

    protected void buttonBoxCount_Click(object sender, EventArgs e)
    {
        if (int.Parse(textBoxCount.Text.Trim()) > 1)
        {
            dropBoxNumber.Visible = true;

            int count = int.Parse(textBoxCount.Text.Trim());

            ArrayList list = new ArrayList();
            for (int i = 1; i <= count; i++)
            {
                list.Add(new Barcode(i, Guid.NewGuid().ToString("N").Substring(0, 15).ToUpper()));
            }

            this.BoxSource = list;

            dropBoxNumber.DataSource = this.BoxSource;
            dropBoxNumber.DataBind();
            dropBoxNumber.SelectedIndex = 0;
        }
        else
        {
            dropBoxNumber.Visible = false;
        }
    }

    protected void dropBoxNumber_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Barcode code = (this.BoxSource[dropBoxNumber.SelectedIndex]) as Barcode;
        textBoxNumber.Text = code.BarNumber;

        BarcodeLib.Barcode b = new BarcodeLib.Barcode();
        b.IncludeLabel = false;
        BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39Extended;

        b.Encode(type, textBoxNumber.Text,
                Color.Black, Color.Bisque, 400, 25);

        image1.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(b.GetImageData(SaveTypes.JPG));
    }


    public ArrayList BoxSource
    {
        get
        {
            return this.Session["_Box_Source"] as ArrayList;
        }

        set
        {
            this.Session["_Box_Source"] = value;
        }
    }

}

