<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="DefineQuestions.aspx.cs" Inherits="DefineQuestions" Title="" %>

<%@ Import Namespace="System.Reflection" %>
<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Define Doctor's Questions</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">

            <div class="form-containt listing-contant">
                <div class="left-content">
                    <p>Please Fill in the question field to define your questions for a patient.</p>
                    <div class="content-box listing-view define-questions-table">
                        <asp:Panel ID="panelMain" runat="server" Visible="true">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>

                                    <fieldset>
                                        <asp:DataList ID="Repeater2" DataKeyField="QuestionID" OnItemCreated="Repeater2_OnItemCreated"
                                            runat="server" Width="100%">
                                            <HeaderTemplate>
                                                <fieldset>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <label>
                                                    Question<asp:Label ID="lableItem1" runat="server">
                                                    </asp:Label>
                                                    <asp:HiddenField ID="HiddenField2" runat="server" Value='<%# Eval("QuestionID")%>' />
                                                </label>
                                                <asp:TextBox ID="textfield4" runat="server" Text='<%# Eval("QuestionTitle")%>' />
                                                <asp:CheckBox ID="checkBox1" runat="server" Checked='<%# Eval("IsTextBox")%>' Text="IsTextBox" />
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <label>
                                                    Question<asp:Label ID="lableItem1" runat="server">
                                                    </asp:Label>
                                                    <asp:HiddenField ID="HiddenField1" runat="server" Value='<%# Eval("QuestionID")%>' />
                                                </label>
                                                <asp:TextBox ID="textfield8" runat="server" Text='<%# Eval("QuestionTitle")%>' />
                                                <asp:CheckBox ID="checkBox2" runat="server" Checked='<%# Eval("IsTextBox")%>' Text="IsTextBox" />
                                            </AlternatingItemTemplate>
                                            <FooterTemplate>
                                                </fieldset>
                                            </FooterTemplate>
                                        </asp:DataList>
                                    </fieldset>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="form_title_1">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                            <div align="center" style="height: 30px;">
                                <%--<asp:ImageButton runat="server" ID="ImageButton1" OnClick="ImageButton1_Click" ImageUrl="~/images/submit_btn.gif" />--%>
                                <asp:Button runat="server" ID="ImageButton1" OnClick="ImageButton1_Click" Text="Submit" CssClass="create-btn btn green" />
                                <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                            </div>
                        </asp:Panel>
                    </div>
                </div>
                <div class="right-content">
               <%--     <div class="quick-find">
                        <uc1:QuickFind ID="QuickFind1" runat="server" />
                    </div>--%>
                    <div class="quick-find" style="margin-top: 15px;">
                        <div class="find-inputbox">
                            <marquee scrollamount="1" id="marqueeside" runat="server"
                                behavior="scroll" onmouseover="this.stop()" onmouseout="this.start()" scrolldelay="20"
                                direction="up">
                                <%=SideMessage %>
                            </marquee>
                            <%--<uc2:ReminderControl ID="ReminderControl1" runat="server" />--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
