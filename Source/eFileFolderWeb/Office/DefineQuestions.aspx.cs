using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;

public partial class DefineQuestions : OverLicensePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //List<QuestionEntity> items = new List<QuestionEntity>();
            QuestionEntities.Clear();
            //QuestionTextBoxs.Clear();

            DataTable table = UserManagement.GetQuestionDefines();

            int i = 1;
            foreach (DataRow dataRow in table.Rows)
            {
                QuestionEntity entity = new QuestionEntity();
                entity.QuestionOrder = i++;
                entity.QuestionID = Convert.ToInt32(dataRow["QuestionID"]);
                entity.QuestionTitle = Convert.ToString(dataRow["QuestionTitle"]);
                entity.IsTextBox = Convert.ToBoolean(dataRow["IsTextBox"]);

                QuestionEntities.Add(entity);
            }

            string doctorId = Sessions.SwitchedSessionId;
            //string doctorId = Membership.GetUser().ProviderUserKey.ToString();
            DataTable doctorTable = UserManagement.GetDoctorQuestions(doctorId);

            foreach (DataRow dataRow in doctorTable.Rows)
            {
                QuestionEntity entity = new QuestionEntity();
                entity.QuestionID = Convert.ToInt32(dataRow["QuestionID"]);
                entity.QuestionTitle = Convert.ToString(dataRow["QuestionTitle"]);
                entity.OfficeID = doctorId;
                entity.IsTextBox = Convert.ToBoolean(dataRow["IsTextBox"]);

                QuestionEntity qEntity = FindQuestion(entity.QuestionID, QuestionEntities);
                qEntity.IsTextBox = entity.IsTextBox;

                if(qEntity !=null)
                {
                    qEntity.QuestionTitle = entity.QuestionTitle;
                    qEntity.IsTextBox = entity.IsTextBox;
                }
            }

            this.Repeater2.DataSource = this.QuestionEntities;
            this.Repeater2.DataBind();
        }
    }

    public string SideMessage
    {
        get
        {
            return this.Session["_Side_Message"] as string;
        }

        set
        {
            this.Session["_Side_Message"] = value;
        }
    }

    public bool HasQuestion(int qid, List<QuestionEntity> questions)
    {
        foreach(QuestionEntity item in questions)
        {
            if(item.QuestionID ==qid)
            {
                return true;
            }
        }

        return false;
    }

    public QuestionEntity FindQuestion(int qid, List<QuestionEntity> questions)
    {
        foreach (QuestionEntity item in questions)
        {
            if (item.QuestionID == qid)
            {
                return item;
            }
        }

        return null;
    }

    protected void ImageButton1_Click(object sender, EventArgs e)
    {
        DataListItemCollection items = this.Repeater2.Items;
        List<DoctorQuestion> list = new List<DoctorQuestion>();

        string id, qid, qname;
        TextBox box;
        CheckBox checkBox;

        string doctorId = Sessions.SwitchedSessionId;
        //string doctorId = Membership.GetUser().ProviderUserKey.ToString();
        foreach (DataListItem item in items)
        {
            qid = (item.Controls[3] as HiddenField).Value.Trim();
            box = item.Controls[5] as TextBox;
            qname = box.Text.Trim();
            checkBox = item.Controls[7] as CheckBox;
            
            DoctorQuestion newEntity = new DoctorQuestion();
            newEntity.QuestionID.Value = Convert.ToInt32(qid);
            newEntity.QuestionTitle.Value = qname;
            newEntity.DoctorID.Value = doctorId;
            newEntity.IsTextBox.Value = checkBox.Checked;

            if (!string.IsNullOrEmpty(qname))
            {
                list.Add(newEntity);
            }
            
        }

        try
        {
            UserManagement.DefineDoctorQuestions(doctorId,list);
        }
        catch (Exception exp)
        {
            string strWrongInfor = "Error : Failed to define the question titles, try again.";
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);

            return;
        }
    }

    public List<QuestionEntity> QuestionEntities
    {
        get
        {
            List<QuestionEntity> obj = this.Session["QuestionEntities"] as List<QuestionEntity>;

            if (obj == null)
            {
                obj = new List<QuestionEntity>();
                this.Session["QuestionEntities"] = obj;
            }
            return obj;
        }

        set { this.Session["QuestionEntities"] = value; }
    }

    public List<QuestionEntity> QuestionTextBoxs
    {
        get
        {
            List<QuestionEntity> obj = this.Session["QuestionTextBoxs"] as List<QuestionEntity>;

            if (obj == null)
            {
                obj = new List<QuestionEntity>();
                this.Session["QuestionTextBoxs"] = obj;
            }
            return obj;
        }

        set { this.Session["QuestionTextBoxs"] = value; }
    }


    protected void Repeater2_OnItemCreated(object sender, DataListItemEventArgs e)
    {
        Control control;
        Label label;
        DataListItem item;

        switch (e.Item.ItemType)
        {
            case ListItemType.Item:
                item = e.Item;
                control = item.FindControl("textfield4");
                control.ID = control.ID + item.ItemIndex;
                label = item.FindControl("lableItem1") as Label;
                if (label!=null)
                {
                    label.Text = (item.ItemIndex+1).ToString();
                }
                
                break;
            case ListItemType.AlternatingItem:
                item = e.Item;
                control = item.FindControl("textfield8");
                control.ID = control.ID + item.ItemIndex;
                label = item.FindControl("lableItem2") as Label;
                if (label != null)
                {
                    label.Text = (item.ItemIndex + 1).ToString();
                }
                break;
        }
    }
}