﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DocumentAndFolderLogs.aspx.cs" Inherits="Office_DocumentAndFolderLogs" MasterPageFile="~/MasterPage.master" %>

<%@ Import Namespace="System.Data" %>
<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>




<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/edFiles/EdFilesLogs.js?v=4"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <%--<link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/style.css" rel="stylesheet" type="text/css" />--%>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style>
        /*#combobox_chosen
        {
            display: block;
            margin-top: 5px;
        }*/



        #comboBoxFolder_chosen {
            display: block;
            /*margin-top: 3.5%;*/
            /*margin-left: 18%;*/
        }

        #comboBoxSubUser_chosen {
            display: block;
            /*margin-top: 3.5%;*/
            margin-left: 0%;
        }

        #comboBoxUser_chosen {
            display: block;
            margin-top: 3.5%;
        }

        .imgbuttn1 {
            color: white;
            background-color: #0047b3;
            border: none;
            /*width: 110px;*/
            min-height: 30px;
            border-radius: 5px;
            margin: 0 0 0 10px;
            cursor: pointer;
        }

        .imgbuttn2 {
            color: white;
            background-color: #0047b3;
            border: none;
            width: 160px;
            height: 30px;
            border-radius: 5px;
            margin: 0 0 0 10px;
            cursor: pointer;
        }

        h3 {
            display: block;
            font-size: 20px;
            -webkit-margin-before: 1em;
            -webkit-margin-after: 1em;
            -webkit-margin-start: 0px;
            -webkit-margin-end: 0px;
            font-weight: bold;
        }

        .chosen-results {
            max-height: 200px !important;
        }

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
            left: 0;
        }
    </style>
    <script type="text/javascript">
        var pubExpanded = false;
        var isFolderSelected = "true";

        function disableScrollbar() {
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }

        function enableScrollbar() {
            $('html, body').css({
                'overflow': 'auto',
                'height': 'auto'
            });
        }

        function LoadSubUser() {

            $("#comboBoxSubUser").html('');
            $("#comboBoxSubUser").chosen("destroy");
            $.ajax(
                {
                    type: "POST",
                    url: 'DocumentAndFolderLogs.aspx/GetAllSubUsers',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        $('<option />', { value: 0, text: "Select a Sub User to see logs" }).appendTo($("#comboBoxSubUser"));
                        $.each(data.d, function (i, text) {
                            $('<option />', { value: i, text: text }).appendTo($("#comboBoxSubUser"));
                        });

                        if ($("#comboBoxSubUser").val() == null) {
                            $('<option />', { value: 0, text: "There is no subuser for this user." }).appendTo($("#comboBoxSubUser"));
                        }
                        $("#comboBoxSubUser").chosen({ width: "235px" });
                        if ($("#" + '<%= hdnSubUserID.ClientID%>').val() != "") {
                            $("#comboBoxSubUser").val($("#" + '<%= hdnSubUserID.ClientID%>').val()).trigger("chosen:updated");
                        }
                        //hideLoader();
                    },
                    fail: function (data) {
                        alert("Failed to get SubUsers. Please try again!");
                        hideLoader();
                    }
                });
        }

        function LoadFolders(userID) {

            var currFolderId = $("#comboBoxFolder").val();
            $("#folderList").html('');
            $("#comboBoxFolder").html('');
            $("#comboBoxFolder").chosen("destroy");
            $.ajax(
                {
                    type: "POST",
                    url: 'DocumentAndFolderLogs.aspx/GetFolderDetailsById',
                    contentType: "application/json; charset=utf-8",
                    data: '{"officeUID":"' + userID + '"}',
                    dataType: "json",
                    success: function (result) {
                        var folders = JSON.parse(result.d);

                        $.each(folders, function (i, text) {
                            if (text.FolderName != "")
                                $('<option />', { value: text.FolderID, text: text.FolderName }).appendTo($("#comboBoxFolder"));
                        });
                        $("#comboBoxFolder").chosen({ width: "235px" });
                        if ($("#" + '<%= hdnFolderID.ClientID%>').val() != "") {
                            $("#comboBoxFolder").val($("#" + '<%= hdnFolderID.ClientID%>').val()).trigger("chosen:updated");
                        }
                        hideLoader();
                    },
                    fail: function (data) {
                        alert("Failed to get Folders. Please try again!");
                        hideLoader();
                    }
                });
        }

        function LoadUserForShareLogs() {

            $("#comboBoxUser").html('');
            $("#comboBoxUser").chosen("destroy");
            $.ajax(
                {
                    type: "POST",
                    url: 'DocumentAndFolderLogs.aspx/GetAllUsersWithOffice',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $.each(data.d, function (i, text) {
                            $('<option />', { value: i, text: text }).appendTo($("#comboBoxUser"));
                        });
                        $("#comboBoxUser").chosen({ width: "235px" });
                        if ($("#" + '<%= hdnUserId.ClientID%>').val() != "") {
                            $("#comboBoxUser").val($("#" + '<%= hdnUserId.ClientID%>').val()).trigger("chosen:updated");
                        }
                        hideLoader();
                    },
                    fail: function (data) {
                        alert("Failed to get SubUsers. Please try again!");
                        hideLoader();
                    }
                });
        }

        function LoadSelect() {
            $("#comboBoxSelect").html('');
            $("#comboBoxSelect").chosen("destroy");
            $('<option />', { value: 1, text: "View Documents And Folder Logs" }).appendTo($("#comboBoxSelect"));
            $('<option />', { value: 2, text: "View Share Logs" }).appendTo($("#comboBoxSelect"));
            if ($("#" + '<%= hdnSelect.ClientID%>').val() != "") {
                $("#comboBoxSelect").val($("#" + '<%= hdnSelect.ClientID%>').val()).trigger("chosen:updated");
            }
            $("#comboBoxSelect").chosen({ width: "235px" });
            if ($("#comboBoxSelect").val() == "1") {
                $("#logsForView").show();
                LoadSubUser();
                LoadFolders('<%= Sessions.SwitchedSessionId%>');
            }
            else if ($("#comboBoxSelect").val() == "2") {
                $("#logsForShare").show();
                LoadUserForShareLogs();
            }
        }

        function showLoader() {
            $('#overlay').show();
            disableScrollbar();
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
            enableScrollbar();
        }

        function LoadLogsTable(uid, subUserId, folderId) {
            $.ajax(
                {
                    type: "POST",
                    url: 'DocumentAndFolderLogs.aspx/LoadFolderTable',
                    contentType: "application/json; charset=utf-8",
                    data: '{"uid":"' + uid + '","subUserId":"' + subUserId + '", "folderId":"' + folderId + '"}',
                    dataType: "json",
                    success: function (result) {
                        hideLoader();
                        __doPostBack('', 'refreshGrid@');
                    },
                    fail: function (data) {
                        alert("Failed to get Folders. Please try again!");
                        hideLoader();
                    }
                });
        }

        function LoadShareLogsTable(uid) {
            $.ajax(
                {
                    type: "POST",
                    url: 'DocumentAndFolderLogs.aspx/LoadShareLogTable',
                    contentType: "application/json; charset=utf-8",
                    data: '{"uid":"' + uid + '"}',
                    dataType: "json",
                    success: function (result) {
                        hideLoader();
                        __doPostBack('', 'refreshGrid1@');
                    },
                    fail: function (data) {
                        alert("Failed to get Logs. Please try again!");
                        hideLoader();
                    }
                });
        }

        function openwindow() {
            var url = 'ExportDocumentsAndFolderLogs.aspx?id=1';
            var name = 'ExportDocumentsAndFolderLogs';
            var iWidth = 250;
            var iHeight = 200;
            var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
            var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
            window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',toolbar=no,menubar=no,scrollbars=no,resizeable=no,location=no,status=no');
        }

        function openwindow1() {
            var url = 'ExportDocumentsAndFolderLogs.aspx?id=2';
            var name = 'ExportAllLogs';
            var iWidth = 250;
            var iHeight = 200;
            var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
            var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
            window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',toolbar=no,menubar=no,scrollbars=no,resizeable=no,location=no,status=no');
        }

        function openwindowForWorkArea() {
            var url = 'WorkareaExporter.aspx';
            var name = 'WorkareaExporter';
            var iWidth = 250;
            var iHeight = 200;
            var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
            var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
            window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',toolbar=no,menubar=no,scrollbars=no,resizeable=no,location=no,status=no');
        }

        function openwindowForEdFiles() {
            var url = '../Account/FolderExporter.aspx';
            var name = 'FolderExporter';
            var iWidth = 250;
            var iHeight = 200;
            var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
            var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
            window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',toolbar=no,menubar=no,scrollbars=no,resizeable=no,location=no,status=no');
        }

        function pageLoad() {
            $("#logsForView").hide();
            $("#logsForShare").hide();
            showLoader();
            LoadSelect();
            if ($("#" + '<%=CheckFolderChanged.ClientID%>').val() != isFolderSelected) {
                $("#folderLogs").css("margin-bottom", "14%");
            }
            $("#alertSubUser").hide();

            $("#comboBoxSubUser").change(function (event) {
                showLoader();
                $("#" + '<%=hdnSubUserID.ClientID%>').val($("#comboBoxSubUser").chosen().val());
                LoadLogsTable('<%=Sessions.SwitchedSessionId%>', $("#comboBoxSubUser").val(), $("#comboBoxFolder").val());
                     //$("#" + '<%=CheckFolderChanged.ClientID%>').val("");
                     //$("#" + '<%=hdnFolderID.ClientID%>').val("");
            });

            $("#comboBoxFolder").change(function (event) {

                showLoader();
                if ($("#comboBoxFolder").val() == "0") {
                    $("#comboBoxFolder").val($("#" + '<%=hdnFolderID.ClientID%>').val()).trigger("chosen:updated");
                    hideLoader();
                    return false;
                }
                $("#" + '<%=hdnFolderID.ClientID%>').val($("#comboBoxFolder").chosen().val());
                $("#" + '<%=CheckFolderChanged.ClientID%>').val(isFolderSelected);
                LoadLogsTable('<%=Sessions.SwitchedSessionId%>', $("#comboBoxSubUser").val(), $("#comboBoxFolder").val());
            });

            $("#comboBoxUser").change(function (event) {
                showLoader();
                if ($("#comboBoxUser").val() == "0") {
                    $("#comboBoxUser").val($("#" + '<%=hdnUserId.ClientID%>').val()).trigger("chosen:updated");
                    hideLoader();
                    return false;
                }
                $("#" + '<%=hdnUserId.ClientID%>').val($("#comboBoxUser").chosen().val());
                LoadShareLogsTable($("#comboBoxUser").val());
            });

            $("#comboBoxSelect").change(function (event) {
                showLoader();
                if ($("#comboBoxSelect").val() == "0") {
                    $("#comboBoxSelect").val($("#" + '<%=hdnSelect.ClientID%>').val()).trigger("chosen:updated");
                    hideLoader();
                    return false;
                }
                if ($("#comboBoxSelect").val() == "1") {
                    $("#logsForView").show();
                    $("#logsForShare").hide();
                    LoadSubUser();
                    LoadFolders('<%= Sessions.SwitchedSessionId%>');
                }
                else {
                    $("#logsForShare").show();
                    $("#logsForView").hide();
                    LoadUserForShareLogs();
                }
                $("#" + '<%=hdnSelect.ClientID%>').val($("#comboBoxSelect").chosen().val());
            });
        }



    </script>
    <div id="overlay" style="display: none">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <%--<div style="width: 825px; height: 75px;">
        <div class="Naslov_Protokalev">
            Documents & Folders Logs
        </div>
    </div>--%>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Documents & Folders Logs</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container">
        <div class="inner-wrapper">
            <table style="float: right">
                <tr>
                    <td>
                        <button class="imgbuttn1" onclick="openwindow()">Export to Excel</button>
                    </td>
                    <td>
                        <button class="imgbuttn1" onclick="openwindow1()">Export All Logs to Excel</button>
                    </td>
                    <td>
                        <button class="imgbuttn1" onclick="openwindowForEdFiles()">Export EdFiles Report</button></td>
                    <td>
                        <button class="imgbuttn1" onclick="openwindowForWorkArea()">Export Workarea Report</button>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td>
                        <select data-placeholder="Select an option to view logs" id="comboBoxSelect" style="color: rgb(68,68,68); border-radius: 5px; background-color: #fff; border: 1px solid #aaa; box-shadow: 0 1px 0 #fff inset; background: linear-gradient(#fff 20%, #f6f6f6 50%, #eee 52%, #f4f4f4 100%)">
                        </select>
                        <asp:HiddenField runat="server" ID="hdnSelect" />
                    </td>
                </tr>
            </table>
            <div id="logsForView" style="clear: both">
                <table cellspacing="10" style="margin-left: -10px">
                    <tr>
                        <td style="width: 120px; vertical-align: middle">
                            <label style="font-size: 14px; font-weight: 700">Select Subuser:</label>
                        </td>
                        <td style="width: 235px">
                            <br />
                            <select id="comboBoxSubUser" style="color: rgb(68,68,68); border-radius: 5px; background-color: #fff; border: 1px solid #aaa; box-shadow: 0 1px 0 #fff inset; background: linear-gradient(#fff 20%, #f6f6f6 50%, #eee 52%, #f4f4f4 100%)">
                            </select>
                            <asp:HiddenField runat="server" ID="hdnSubUserID" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="width: 120px; vertical-align: middle">
                            <label style="font-size: 14px; font-weight: 700">Select Folder:</label></td>
                        <td style="width: 235px">
                            <select id="comboBoxFolder" style="color: rgb(68,68,68); border-radius: 5px; background-color: #fff; border: 1px solid #aaa; box-shadow: 0 1px 0 #fff inset; background: linear-gradient(#fff 20%, #f6f6f6 50%, #eee 52%, #f4f4f4 100%)">
                            </select>
                            <asp:HiddenField runat="server" ID="hdnFolderID" />
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <br />
                <asp:HiddenField ID="CheckFolderChanged" runat="server" Value=""></asp:HiddenField>

                <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                    <ContentTemplate>
                        <h3>FileFolder Logs:</h3>
                        <div class="table-scroll">
                            <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                            <asp:GridView ID="GridView1" runat="server"
                                DataKeyNames="FolderID" CssClass="datatable listing-datatable" OnRowDataBound="GridView1_RowDataBound" OnSorting="GridView1_Sorting"
                                AllowSorting="True" AutoGenerateColumns="false">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="odd" />
                                <RowStyle CssClass="even" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:BoundField DataField="User" HeaderText="User" SortExpression="User" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="FolderName" HeaderText="Folder Name" SortExpression="FolderName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Action Date" SortExpression="CreatedDate" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="ActionName" HeaderText="Action" SortExpression="ActionName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="IP" HeaderText="IP Address" SortExpression="IP" ItemStyle-Width="5%" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label1" runat="server" CssClass="Empty" />There is no record for Folder.
                               
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <br />
                        <div class="table-toolbar">
                            <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" OnPageSizeChanged="WebPager1_PageSizeChanged"
                                PageSize="10" CssClass="opt-select" Style="" PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>
                            <br />
                        </div>

                        <h3>Tab Logs:</h3>

                        <div class="table-scroll">
                            <asp:HiddenField ID="SortDirection2" runat="server" Value=""></asp:HiddenField>
                            <asp:GridView ID="GridView2" CssClass="datatable listing-datatable" runat="server"
                                DataKeyNames="DividerID" OnRowDataBound="GridView2_RowDataBound" OnSorting="GridView2_Sorting"
                                AllowSorting="True" AutoGenerateColumns="false">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="odd" />
                                <RowStyle CssClass="even" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:BoundField DataField="User" HeaderText="User" SortExpression="User" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="FolderName" HeaderText="Folder Name" SortExpression="FolderName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="DividerName" HeaderText="Tab Name" SortExpression="DividerName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Action Date" SortExpression="CreatedDate" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="ActionName" HeaderText="Action" SortExpression="ActionName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="IP" HeaderText="IP Address" SortExpression="IP" ItemStyle-Width="5%" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label2" runat="server" CssClass="Empty" />There is no record for Tab.
                               
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <br />
                        <div class="table-toolbar">
                            <cc1:WebPager ID="WebPager2" runat="server" OnPageIndexChanged="WebPager2_PageIndexChanged" OnPageSizeChanged="WebPager2_PageSizeChanged"
                                PageSize="10" CssClass="opt-select" Style="" PagerStyle="NumericPages" ControlToPaginate="GridView2"></cc1:WebPager>
                        </div>

                        <h3>Document Logs:</h3>

                        <div class="table-scroll">
                            <asp:HiddenField ID="SortDirection3" runat="server" Value=""></asp:HiddenField>
                            <asp:GridView ID="GridView3" CssClass="datatable listing-datatable" runat="server"
                                DataKeyNames="DocumentID" OnRowDataBound="GridView3_RowDataBound" OnSorting="GridView3_Sorting"
                                AllowSorting="True" AutoGenerateColumns="false">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="odd" />
                                <RowStyle CssClass="even" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:BoundField DataField="User" HeaderText="User" SortExpression="User" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="FolderName" HeaderText="Folder Name" SortExpression="FolderName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="DividerName" HeaderText="Tab Name" SortExpression="DividerName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="DocumentName" HeaderText="Document Name" SortExpression="DocumentName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Action Date" SortExpression="CreatedDate" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="ActionName" HeaderText="Action" SortExpression="ActionName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="IP" HeaderText="IP Address" SortExpression="IP" ItemStyle-Width="5%" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label3" runat="server" CssClass="Empty" />There is no record for Document.
                               
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <br />
                        <div class="table-toolbar">
                            <cc1:WebPager ID="WebPager3" runat="server" OnPageIndexChanged="WebPager3_PageIndexChanged" OnPageSizeChanged="WebPager3_PageSizeChanged"
                                PageSize="10" CssClass="opt-select" Style="" PagerStyle="NumericPages" ControlToPaginate="GridView3"></cc1:WebPager>
                        </div>

                        <h3>FolderLog Logs:</h3>

                        <div class="table-scroll">
                            <asp:HiddenField ID="SortDirection4" runat="server" Value=""></asp:HiddenField>
                            <asp:GridView ID="GridView4" CssClass="datatable listing-datatable" runat="server"
                                DataKeyNames="FolderLogId" OnRowDataBound="GridView4_RowDataBound" OnSorting="GridView4_Sorting"
                                AllowSorting="True" AutoGenerateColumns="false">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="odd" />
                                <RowStyle CssClass="even" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:BoundField DataField="User" HeaderText="User" SortExpression="User" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="FolderName" HeaderText="Folder Name" SortExpression="FolderName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="LogName" HeaderText="Log Name" SortExpression="LogName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Action Date" SortExpression="CreatedDate" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="ActionName" HeaderText="Action" SortExpression="ActionName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="IP" HeaderText="IP Address" SortExpression="IP" ItemStyle-Width="5%" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label4" runat="server" CssClass="Empty" />There is no record for Folder Log.
                               
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <br />
                        <div class="table-toolbar">
                            <cc1:WebPager ID="WebPager4" runat="server" OnPageIndexChanged="WebPager4_PageIndexChanged" OnPageSizeChanged="WebPager4_PageSizeChanged"
                                PageSize="10" CssClass="opt-select" Style="" PagerStyle="NumericPages" ControlToPaginate="GridView4"></cc1:WebPager>
                        </div>

                        <h3>LogForm Logs:</h3>

                        <div class="table-scroll">
                            <asp:HiddenField ID="SortDirection5" runat="server" Value=""></asp:HiddenField>
                            <asp:GridView ID="GridView5" CssClass="datatable listing-datatable" runat="server" OnRowDataBound="GridView5_RowDataBound" OnSorting="GridView5_Sorting"
                                AllowSorting="True" AutoGenerateColumns="false">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="odd" />
                                <RowStyle CssClass="even" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:BoundField DataField="User" HeaderText="User" SortExpression="User" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="FolderName" HeaderText="Folder Name" SortExpression="FolderName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="FileName" HeaderText="File Name" SortExpression="FileName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Action Date" SortExpression="CreatedDate" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="ActionName" HeaderText="Action" SortExpression="ActionName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="IP" HeaderText="IP Address" SortExpression="IP" ItemStyle-Width="5%" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label5" runat="server" CssClass="Empty" />There is no record for Log Form.
                               
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <br />
                        <div class="table-toolbar">
                            <cc1:WebPager ID="WebPager5" runat="server" OnPageIndexChanged="WebPager5_PageIndexChanged" OnPageSizeChanged="WebPager5_PageSizeChanged"
                                PageSize="10" CssClass="opt-select" Style="" PagerStyle="NumericPages" ControlToPaginate="GridView5"></cc1:WebPager>
                        </div>

                        <h3>WorkArea Logs:</h3>

                        <div class="table-scroll">
                            <asp:HiddenField ID="SortDirection10" runat="server" Value=""></asp:HiddenField>
                            <asp:GridView ID="GridView10" CssClass="datatable listing-datatable" runat="server" OnRowDataBound="GridView10_RowDataBound" OnSorting="GridView10_Sorting"
                                AllowSorting="True" AutoGenerateColumns="false">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="odd" />
                                <RowStyle CssClass="even" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:BoundField DataField="User" HeaderText="User" SortExpression="User" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="FileName" HeaderText="File Name" SortExpression="FileName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Action Date" SortExpression="CreatedDate" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="ActionName" HeaderText="Action" SortExpression="ActionName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="IP" HeaderText="IP Address" SortExpression="IP" ItemStyle-Width="5%" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label10" runat="server" CssClass="Empty" />There is no record for Work Area.
                               
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <br />
                        <div class="table-toolbar">
                            <cc1:WebPager ID="WebPager10" runat="server" OnPageIndexChanged="WebPager10_PageIndexChanged" OnPageSizeChanged="WebPager10_PageSizeChanged"
                                PageSize="10" CssClass="opt-select" Style="" PagerStyle="NumericPages" ControlToPaginate="GridView10"></cc1:WebPager>
                        </div>

                        <h3>EdForms Logs:</h3>

                        <div class="table-scroll">
                            <asp:HiddenField ID="SortDirection11" runat="server" Value=""></asp:HiddenField>
                            <asp:GridView ID="GridView11" CssClass="datatable listing-datatable" runat="server" OnRowDataBound="GridView11_RowDataBound" OnSorting="GridView11_Sorting"
                                AllowSorting="True" AutoGenerateColumns="false">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="odd" />
                                <RowStyle CssClass="even" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:BoundField DataField="User" HeaderText="User" SortExpression="User" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="FileName" HeaderText="File Name" SortExpression="FileName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Action Date" SortExpression="CreatedDate" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="ActionName" HeaderText="Action" SortExpression="ActionName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="IP" HeaderText="IP Address" SortExpression="IP" ItemStyle-Width="5%" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label11" runat="server" CssClass="Empty" />There is no record for EdForms.
                               
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <br />
                        <div class="table-toolbar">
                            <cc1:WebPager ID="WebPager11" runat="server" OnPageIndexChanged="WebPager11_PageIndexChanged" OnPageSizeChanged="WebPager11_PageSizeChanged"
                                PageSize="10" CssClass="opt-select" Style="" PagerStyle="NumericPages" ControlToPaginate="GridView11"></cc1:WebPager>
                        </div>

                        <%--<h3>Inbox & Pending Logs:</h3>--%>

                        <div class="table-scroll">
                            <asp:HiddenField ID="SortDirection12" runat="server" Value=""></asp:HiddenField>
                            <asp:GridView ID="GridView12" CssClass="datatable listing-datatable" runat="server" OnRowDataBound="GridView12_RowDataBound" OnSorting="GridView12_Sorting"
                                AllowSorting="True" AutoGenerateColumns="false">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="odd" />
                                <RowStyle CssClass="even" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:BoundField DataField="User" HeaderText="User" SortExpression="User" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="FileName" HeaderText="File Name" SortExpression="FileName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Action Date" SortExpression="CreatedDate" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="ActionName" HeaderText="Action" SortExpression="ActionName" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="typename" HeaderText="Type" SortExpression="typename" ItemStyle-Width="5%" />
                                    <asp:BoundField DataField="IP" HeaderText="IP Address" SortExpression="IP" ItemStyle-Width="5%" />
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label12" runat="server" CssClass="Empty" />There is no record for Inbox & Pendings.
                               
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <br />
                        <div class="table-toolbar">
                            <cc1:WebPager ID="WebPager12" runat="server" OnPageIndexChanged="WebPager12_PageIndexChanged" OnPageSizeChanged="WebPager12_PageSizeChanged"
                                PageSize="10" CssClass="opt-select" Style="" PagerStyle="NumericPages" ControlToPaginate="GridView12"></cc1:WebPager>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div id="logsForShare" style="clear: both">
                <table>
                    <tr>
                        <td style="width: 120px; vertical-align: middle">
                            <label style="font-size: 14px; font-weight: 700">Select User:</label></td>
                        <td style="padding-left: 8px">
                            <select id="comboBoxUser" style="color: rgb(68,68,68); border-radius: 5px; background-color: #fff; border: 1px solid #aaa; box-shadow: 0 1px 0 #fff inset; background: linear-gradient(#fff 20%, #f6f6f6 50%, #eee 52%, #f4f4f4 100%)">
                            </select>
                            <asp:HiddenField runat="server" ID="hdnUserId" />
                        </td>
                    </tr>
                </table>
                <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="UpdatePanel2">
                    <ContentTemplate>
                        <div id="shareLogs">
                            <h3>Folder Share Logs:</h3>

                            <div class="table-scroll">
                                <asp:HiddenField ID="Srt_Folder_Dir" runat="server" Value=""></asp:HiddenField>
                                <asp:GridView ID="GridView6" CssClass="datatable listing-datatable" runat="server"
                                    OnSorting="GridView6_Sorting"
                                    AllowSorting="True" AutoGenerateColumns="false">
                                    <PagerSettings Visible="False" />
                                    <AlternatingRowStyle CssClass="odd" />
                                    <RowStyle CssClass="even" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:BoundField DataField="User" HeaderText="Shared By" SortExpression="User" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="FolderName" HeaderText="Folder Name" SortExpression="FolderName" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="ReceiverName" HeaderText="Receiver Name" SortExpression="ReceiverName" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="Message" HeaderText="Message" SortExpression="Message" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="DOB" HeaderText="Action Date" SortExpression="DOB" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="Email" HeaderText="Receiver Email" SortExpression="Email" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="IP" HeaderText="IP Address" SortExpression="IP" ItemStyle-Width="5%" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label2" runat="server" CssClass="Empty" />There is no record for Folder Share.
                                   
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                            <br />
                            <div class="table-toolbar">
                                <cc1:WebPager ID="WebPager6" runat="server" OnPageIndexChanged="WebPager6_PageIndexChanged" OnPageSizeChanged="WebPager6_PageSizeChanged"
                                    PageSize="10" CssClass="opt-select" Style="" PagerStyle="NumericPages" ControlToPaginate="GridView6"></cc1:WebPager>
                            </div>
                            <br />
                            <br />
                            <h3>Document Share Logs:</h3>

                            <div class="table-scroll">
                                <asp:HiddenField ID="Srt_Doc_Dir" runat="server" Value=""></asp:HiddenField>
                                <asp:GridView ID="GridView7" CssClass="datatable listing-datatable" runat="server"
                                    OnSorting="GridView7_Sorting"
                                    AllowSorting="True" AutoGenerateColumns="false">
                                    <PagerSettings Visible="False" />
                                    <AlternatingRowStyle CssClass="odd" />
                                    <RowStyle CssClass="even" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:BoundField DataField="User" HeaderText="Shared By" SortExpression="User" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="Name" HeaderText="Document Name" SortExpression="Name" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="ReceiverName" HeaderText="Receiver Name" SortExpression="ReceiverName" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="Message" HeaderText="Message" SortExpression="Message" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="DOB" HeaderText="Action Date" SortExpression="DOB" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="Email" HeaderText="Receiver Email" SortExpression="Email" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="IP" HeaderText="IP Address" SortExpression="IP" ItemStyle-Width="5%" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label3" runat="server" CssClass="Empty" />There is no record for Document Share.
                                   
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                            <br />
                            <div class="table-toolbar">
                                <cc1:WebPager ID="WebPager7" runat="server" OnPageIndexChanged="WebPager7_PageIndexChanged" OnPageSizeChanged="WebPager7_PageSizeChanged"
                                    PageSize="10" CssClass="opt-select" Style="" PagerStyle="NumericPages" ControlToPaginate="GridView7"></cc1:WebPager>
                            </div>
                            <br />
                            <h3>Workarea Share Logs:</h3>

                            <div class="table-scroll">
                                <asp:HiddenField ID="Srt_WorkArea_Dir" runat="server" Value=""></asp:HiddenField>
                                <asp:GridView ID="GridView8" CssClass="datatable listing-datatable" runat="server"
                                    OnSorting="GridView8_Sorting"
                                    AllowSorting="True" AutoGenerateColumns="false">
                                    <PagerSettings Visible="False" />
                                    <AlternatingRowStyle CssClass="odd" />
                                    <RowStyle CssClass="even" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:BoundField DataField="User" HeaderText="Shared By" SortExpression="User" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="FileName" HeaderText="File Name" SortExpression="FileName" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="ReceiverName" HeaderText="Receiver Name" SortExpression="ReceiverName" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="Message" HeaderText="Message" SortExpression="Message" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="DOB" HeaderText="Action Date" SortExpression="DOB" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="Email" HeaderText="Receiver Email" SortExpression="Email" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="IP" HeaderText="IP Address" SortExpression="IP" ItemStyle-Width="5%" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label4" runat="server" CssClass="Empty" />There is no record for WorkArea Share.
                                   
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                            <br />
                            <div class="table-toolbar">
                                <cc1:WebPager ID="WebPager8" runat="server" OnPageIndexChanged="WebPager8_PageIndexChanged" OnPageSizeChanged="WebPager8_PageSizeChanged"
                                    PageSize="10" CssClass="opt-select" Style="" PagerStyle="NumericPages" ControlToPaginate="GridView8"></cc1:WebPager>
                            </div>
                            <%--<br />
                            <h3>EFileFlow Share Logs:</h3>

                            <div class="table-scroll">
                                <asp:HiddenField ID="Srt_EdFileFlow_Dir" runat="server" Value=""></asp:HiddenField>
                                <asp:GridView ID="GridView9" CssClass="datatable listing-datatable" runat="server"
                                    OnSorting="GridView9_Sorting"
                                    AllowSorting="True" AutoGenerateColumns="false">
                                    <PagerSettings Visible="False" />
                                    <AlternatingRowStyle CssClass="odd" />
                                    <RowStyle CssClass="even" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:BoundField DataField="User" HeaderText="Shared By" SortExpression="User" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="Name" HeaderText="File Name" SortExpression="Name" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="ReceiverName" HeaderText="Receiver Name" SortExpression="ReceiverName" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="Message" HeaderText="Message" SortExpression="Message" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="DOB" HeaderText="Action Date" SortExpression="DOB" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="Email" HeaderText="Receiver Email" SortExpression="Email" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="IP" HeaderText="IP Address" SortExpression="IP" ItemStyle-Width="5%" />
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label5" runat="server" CssClass="Empty" />There is no record for EFileFlow Share.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                            <br />
                            <div class="table-toolbar">
                                <cc1:WebPager ID="WebPager9" runat="server" OnPageIndexChanged="WebPager9_PageIndexChanged" OnPageSizeChanged="WebPager9_PageSizeChanged"
                                    PageSize="10" CssClass="opt-select" Style="" PagerStyle="NumericPages" ControlToPaginate="GridView9"></cc1:WebPager>
                            </div>--%>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
