﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using AjaxControlToolkit;
using DataQuicker2.Framework;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Shinetech.Utility;
using System.Linq;

public partial class Office_DocumentAndFolderLogs : System.Web.UI.Page
{
    private readonly NLogLogger _logger = new NLogLogger();
    private static readonly string ConnectionString =
         ConfigurationManager.ConnectionStrings["Default"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        var argument1 = Request.Form["__EVENTTARGET"];
        var argument = Request.Form["__EVENTARGUMENT"];
        if (!Page.IsPostBack)
        {
            GetData();
            GetShareData();
        }
        else
        {
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
            {
                BindDataGrid();
            }
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid1"))
            {
                BindShareDataGrid();
            }
        }
        argument = "";
    }

    public void GetData()
    {
        try
        {
            this.FolderDataSource = null;
            this.DividerDataSource = null;
            this.DocumentDataSource = null;
            this.FolderLogDataSource = null;
            this.LogFormDataSource = null;
            this.WorkAreaDataSource = null;
            this.EdFormsDataSource = null;
            //this.EFileFlowShareDataSource = null;
            string officeUID = Sessions.SwitchedSessionId;
            DataSet docFolderLogs = General_Class.GetAllDocumentAndFolderLogs(officeUID, null, null);
            if (docFolderLogs != null && docFolderLogs.Tables.Count > 0 && docFolderLogs.Tables[0].Rows != null)
            {
                foreach (DataRow dr in docFolderLogs.Tables[0].Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                }
                foreach (DataRow dr in docFolderLogs.Tables[1].Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                }
                foreach (DataRow dr in docFolderLogs.Tables[2].Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                }
                foreach (DataRow dr in docFolderLogs.Tables[3].Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                }
                foreach (DataRow dr in docFolderLogs.Tables[4].Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                }
                foreach (DataRow dr in docFolderLogs.Tables[5].Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                }
                foreach (DataRow dr in docFolderLogs.Tables[6].Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                }
                //foreach (DataRow dr in docFolderLogs.Tables[7].Rows)
                //{
                //    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                //}
                foreach (DataRow dr in docFolderLogs.Tables[6].Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                }
                this.FolderDataSource = docFolderLogs.Tables[0];
                this.DividerDataSource = docFolderLogs.Tables[1];
                this.DocumentDataSource = docFolderLogs.Tables[2];
                this.FolderLogDataSource = docFolderLogs.Tables[3];
                this.LogFormDataSource = docFolderLogs.Tables[4];
                this.WorkAreaDataSource = docFolderLogs.Tables[5];
                this.EdFormsDataSource = docFolderLogs.Tables[6];
                //this.EFileFlowShareDataSource = docFolderLogs.Tables[7];
                BindDataGrid();
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    public void GetShareData()
    {
        try
        {
            FolderShareLogSource = null;
            DocumentShareLogSource = null;
            WorkAreaShareLogSource = null;
            EdFileFlowLogSource = null;
            string defaultUID = Sessions.SwitchedSessionId;
            //string defaultUID = Membership.GetUser().ProviderUserKey.ToString();
            DataSet shareLogs = General_Class.GetAllShareLinkLogs(defaultUID);
            FolderShareLogSource = shareLogs.Tables[0];
            DocumentShareLogSource = shareLogs.Tables[1];
            WorkAreaShareLogSource = shareLogs.Tables[2];
            EdFileFlowLogSource = shareLogs.Tables[3];
            BindShareDataGrid();
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog("", string.Empty, string.Empty, ex);
        }
    }

    [WebMethod]
    public static Dictionary<string, string> GetAllUsers()
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        try
        {
            DataTable dtUsers = General_Class.GetAllUsers();
            IList<string> list = new List<string>();
            if (dtUsers != null)
            {
                dtUsers.AsEnumerable().ToList().ForEach(d =>
                {
                    dict.Add(Convert.ToString(d.Field<Guid>("UID")), d.Field<string>("Name"));

                });
            }
        }
        catch (Exception ex)
        { }
        return dict;
    }

    [WebMethod]
    public static Dictionary<string, string> GetAllUsersWithOffice()
    {
        string officeUID = Sessions.SwitchedSessionId;
        Dictionary<string, string> dict = new Dictionary<string, string>();
        try
        {
            DataTable dtUsers = General_Class.GetAllSubUsers(officeUID);
            dict.Add(officeUID, Sessions.SwitchedUserName);
            //dict.Add(officeUID, Membership.GetUser().UserName.ToString());
            IList<string> list = new List<string>();
            if (dtUsers != null)
            {
                dtUsers.AsEnumerable().ToList().ForEach(d =>
                {
                    dict.Add(Convert.ToString(d.Field<Guid>("UID")), d.Field<string>("Name"));

                });
            }
        }
        catch (Exception ex)
        { }
        return dict;
    }

    [WebMethod]
    public static Dictionary<string, string> GetAllSubUsers()
    {
        string officeUID = Sessions.SwitchedSessionId;
        //string officeUID = Membership.GetUser().ProviderUserKey.ToString();
        Dictionary<string, string> dict = new Dictionary<string, string>();
        try
        {
            DataTable dtUsers = General_Class.GetAllSubUsers(officeUID);
            IList<string> list = new List<string>();
            if (dtUsers != null)
            {
                dtUsers.AsEnumerable().ToList().ForEach(d =>
                {
                    dict.Add(Convert.ToString(d.Field<Guid>("UID")), d.Field<string>("Name"));

                });
            }
        }
        catch (Exception ex)
        { }
        return dict;
    }

    [WebMethod]
    public static string GetFolderDetailsById(string officeUID)
    {
        Office_DocumentAndFolderLogs docs = new Office_DocumentAndFolderLogs();
        var folders = FileFolderManagement.GetFileFoldersInfor(officeUID);

        var uid = Sessions.SwitchedSessionId;
        var act = new Shinetech.DAL.Account(uid);
        string[] groups = act.Groups.Value != null ? act.Groups.Value.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries) : new string[] { };

        int count = folders.Rows.Count;

        for (int i = 0; i < count; i++)
        {
            DataRow row = folders.Rows[i];
            if (!uid.Equals(row["OfficeID"].ToString()))
            {
                if (row["SecurityLevel"].ToString().Equals("0") && !docs.BelongToGroup(row["Groups"].ToString(), groups))
                {
                    row.Delete();
                }
            }
        }

        folders.AcceptChanges();
        return JsonConvert.SerializeObject(folders);
    }

    public bool BelongToGroup(string p, string[] groups)
    {
        foreach (string group in groups)
        {
            if (p.Contains(group))
            {
                return true;
            }
        }

        return false;
    }

    [WebMethod]
    public static string LoadFolderTable(string uid, string subUserId, string folderId)
    {
        try
        {
            Office_DocumentAndFolderLogs docs = new Office_DocumentAndFolderLogs();

            docs.FolderDataSource = null;
            docs.DividerDataSource = null;
            docs.DocumentDataSource = null;
            docs.FolderLogDataSource = null;
            docs.LogFormDataSource = null;
            docs.WorkAreaDataSource = null;
            docs.EdFormsDataSource = null;
            DataSet docLogs = General_Class.GetAllDocumentAndFolderLogs(uid, (subUserId == "null" || subUserId == "0") ? null : subUserId, folderId == "null" ? 0 : Convert.ToInt32(folderId));
            if (docLogs != null && docLogs.Tables.Count > 6)
            {
                foreach (DataRow dr in docLogs.Tables[0].Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                }
                foreach (DataRow dr in docLogs.Tables[1].Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                }
                foreach (DataRow dr in docLogs.Tables[2].Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                }
                foreach (DataRow dr in docLogs.Tables[3].Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                }
                foreach (DataRow dr in docLogs.Tables[4].Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                }
                foreach (DataRow dr in docLogs.Tables[5].Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                }
                foreach (DataRow dr in docLogs.Tables[6].Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                }
                docs.FolderDataSource = docLogs.Tables[0];
                docs.DividerDataSource = docLogs.Tables[1];
                docs.DocumentDataSource = docLogs.Tables[2];
                docs.FolderLogDataSource = docLogs.Tables[3];
                docs.LogFormDataSource = docLogs.Tables[4];
                docs.WorkAreaDataSource = docLogs.Tables[5];
                docs.EdFormsDataSource = docLogs.Tables[6];
            }
            return JsonConvert.SerializeObject(docLogs);
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(folderId, string.Empty, string.Empty, ex);
            return string.Empty;
        }
    }



    private void BindDataGrid()
    {
        try
        {
            GridView1.DataSource = this.FolderDataSource.AsDataView();
            GridView1.DataBind();
            WebPager1.DataSource = this.FolderDataSource;
            WebPager1.DataBind();
            GridView2.DataSource = this.DividerDataSource.AsDataView();
            GridView2.DataBind();
            WebPager2.DataSource = this.DividerDataSource;
            WebPager2.DataBind();
            GridView3.DataSource = this.DocumentDataSource.AsDataView();
            GridView3.DataBind();
            WebPager3.DataSource = this.DocumentDataSource;
            WebPager3.DataBind();
            GridView4.DataSource = this.FolderLogDataSource.AsDataView();
            GridView4.DataBind();
            WebPager4.DataSource = this.FolderLogDataSource;
            WebPager4.DataBind();
            GridView5.DataSource = this.LogFormDataSource.AsDataView();
            GridView5.DataBind();
            WebPager5.DataSource = this.LogFormDataSource;
            WebPager5.DataBind();
            GridView10.DataSource = this.WorkAreaDataSource.AsDataView();
            GridView10.DataBind();
            WebPager10.DataSource = this.WorkAreaDataSource;
            WebPager10.DataBind();
            GridView11.DataSource = this.EdFormsDataSource.AsDataView();
            GridView11.DataBind();
            WebPager11.DataSource = this.EdFormsDataSource;
            WebPager11.DataBind();
            GridView12.DataSource = this.EFileFlowShareDataSource.AsDataView();
            GridView12.DataBind();
            WebPager12.DataSource = this.EFileFlowShareDataSource;
            WebPager12.DataBind();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    private void BindShareDataGrid()
    {
        try
        {
            GridView6.DataSource = this.FolderShareLogSource.AsDataView();
            GridView6.DataBind();
            WebPager6.DataSource = this.FolderShareLogSource;
            WebPager6.DataBind();
            GridView7.DataSource = this.DocumentShareLogSource.AsDataView();
            GridView7.DataBind();
            WebPager7.DataSource = this.DocumentShareLogSource;
            WebPager7.DataBind();
            GridView8.DataSource = this.WorkAreaShareLogSource.AsDataView();
            GridView8.DataBind();
            WebPager8.DataSource = this.WorkAreaShareLogSource;
            WebPager8.DataBind();
            //GridView9.DataSource = this.EdFileFlowLogSource.AsDataView();
            //GridView9.DataBind();
            //WebPager9.DataSource = this.EdFileFlowLogSource;
            //WebPager9.DataBind();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    private void BindGrid()
    {
        try
        {
            GridView1.DataSource = this.FolderDataSource.AsDataView();
            GridView1.DataBind();
            WebPager1.DataSource = this.FolderDataSource;
            WebPager1.DataBind();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    public DataTable FolderDataSource
    {
        get
        {
            return this.Session["DataSource_Folder"] as DataTable;
        }
        set
        {
            this.Session["DataSource_Folder"] = value;
        }
    }

    public DataTable DividerDataSource
    {
        get
        {
            return this.Session["DataSource_Divider"] as DataTable;
        }
        set
        {
            this.Session["DataSource_Divider"] = value;
        }
    }

    public DataTable DocumentDataSource
    {
        get
        {
            return this.Session["DataSource_Document"] as DataTable;
        }
        set
        {
            this.Session["DataSource_Document"] = value;
        }
    }

    public DataTable FolderLogDataSource
    {
        get
        {
            return this.Session["DataSource_FolderLog"] as DataTable;
        }
        set
        {
            this.Session["DataSource_FolderLog"] = value;
        }
    }

    public DataTable LogFormDataSource
    {
        get
        {
            return this.Session["DataSource_LogForm"] as DataTable;
        }
        set
        {
            this.Session["DataSource_LogForm"] = value;
        }
    }

    public DataTable WorkAreaDataSource
    {
        get
        {
            return this.Session["DataSource_WorkArea"] as DataTable;
        }
        set
        {
            this.Session["DataSource_WorkArea"] = value;
        }
    }

    public DataTable EFileFlowDataSource
    {
        get
        {
            return this.Session["DataSource_EFileFlow"] as DataTable;
        }
        set
        {
            this.Session["DataSource_EFileFlow"] = value;
        }
    }

    public DataTable EdFormsDataSource
    {
        get
        {
            return this.Session["DataSource_EdForms"] as DataTable;
        }
        set
        {
            this.Session["DataSource_EdForms"] = value;
        }
    }

    public DataTable EFileFlowShareDataSource
    {
        get
        {
            return this.Session["DataSource_EFileFlowShare"] as DataTable;
        }
        set
        {
            this.Session["DataSource_EFileFlowShare"] = value;
        }
    }

    public DataTable FolderShareLogSource
    {
        get
        {
            return this.Session["DataSource_FolderShare"] as DataTable;
        }
        set
        {
            this.Session["DataSource_FolderShare"] = value;
        }
    }
    public DataTable DocumentShareLogSource
    {
        get
        {
            return this.Session["DataSource_DocumentShare"] as DataTable;
        }
        set
        {
            this.Session["DataSource_DocumentShare"] = value;
        }
    }
    public DataTable WorkAreaShareLogSource
    {
        get
        {
            return this.Session["DataSource_WorkAreaShare"] as DataTable;
        }
        set
        {
            this.Session["DataSource_WorkAreaShare"] = value;
        }
    }
    public DataTable EdFileFlowLogSource
    {
        get
        {
            return this.Session["DataSource_EdFileFlowShare"] as DataTable;
        }
        set
        {
            this.Session["DataSource_EdFileFlowShare"] = value;
        }
    }


    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drvnew = e.Row.DataItem as DataRowView;
            string FileFolderID = drvnew.Row["FolderID"].ToString();

        }

    }

    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drvnew = e.Row.DataItem as DataRowView;
            string DividerID = drvnew.Row["DividerID"].ToString();

        }

    }

    protected void GridView3_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drvnew = e.Row.DataItem as DataRowView;
            string DocumentID = drvnew.Row["DocumentID"].ToString();

        }

    }

    protected void GridView4_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drvnew = e.Row.DataItem as DataRowView;
            string FolderLogId = drvnew.Row["FolderLogId"].ToString();

        }

    }

    protected void GridView5_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drvnew = e.Row.DataItem as DataRowView;
            string LogFormID = drvnew.Row["LogFormID"].ToString();

        }

    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Folder_Sort_Direction == SortDirection.Ascending)
        {
            this.Folder_Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Folder_Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.FolderDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.FolderDataSource = Source.ToTable();
        BindDataGrid();
    }

    protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Divider_Sort_Direction == SortDirection.Ascending)
        {
            this.Divider_Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Divider_Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DividerDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DividerDataSource = Source.ToTable();
        BindDataGrid();
    }

    protected void GridView3_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Document_Sort_Direction == SortDirection.Ascending)
        {
            this.Document_Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Document_Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DocumentDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DocumentDataSource = Source.ToTable();
        BindDataGrid();
    }

    protected void GridView4_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.FolderLog_Sort_Direction == SortDirection.Ascending)
        {
            this.FolderLog_Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.FolderLog_Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.FolderLogDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.FolderLogDataSource = Source.ToTable();
        BindDataGrid();
    }

    protected void GridView5_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.LogForm_Sort_Direction == SortDirection.Ascending)
        {
            this.LogForm_Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.LogForm_Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.LogFormDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.LogFormDataSource = Source.ToTable();
        BindDataGrid();
    }

    private SortDirection Folder_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    private SortDirection Divider_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection2.Value))
            {
                SortDirection2.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection2.Value);

        }
        set
        {
            this.SortDirection2.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    private SortDirection Document_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection3.Value))
            {
                SortDirection3.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection3.Value);

        }
        set
        {
            this.SortDirection3.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    private SortDirection FolderLog_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection4.Value))
            {
                SortDirection4.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection4.Value);

        }
        set
        {
            this.SortDirection4.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    private SortDirection LogForm_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection5.Value))
            {
                SortDirection5.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection5.Value);

        }
        set
        {
            this.SortDirection5.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    private SortDirection WorkArea_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection10.Value))
            {
                SortDirection10.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection10.Value);

        }
        set
        {
            this.SortDirection10.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    private SortDirection EFileFlow_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection11.Value))
            {
                SortDirection11.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection11.Value);

        }
        set
        {
            this.SortDirection11.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    private SortDirection EFileFlowShare_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection12.Value))
            {
                SortDirection12.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection12.Value);

        }
        set
        {
            this.SortDirection12.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void WebPager5_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager5.CurrentPageIndex = e.NewPageIndex;
        WebPager5.DataSource = this.LogFormDataSource;
        WebPager5.DataBind();
    }
    protected void WebPager5_PageSizeChanged(object s, PageSizeChangeEventArgs e)
    {
        WebPager5.CurrentPageIndex = 0;
        WebPager5.DataSource = this.LogFormDataSource;
        WebPager5.DataBind();
    }
    protected void WebPager4_PageSizeChanged(object s, PageSizeChangeEventArgs e)
    {
        WebPager4.CurrentPageIndex = 0;
        WebPager4.DataSource = this.FolderLogDataSource;
        WebPager4.DataBind();
    }
    protected void WebPager4_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager4.CurrentPageIndex = e.NewPageIndex;
        WebPager4.DataSource = this.FolderLogDataSource;
        WebPager4.DataBind();
    }
    protected void WebPager3_PageSizeChanged(object s, PageSizeChangeEventArgs e)
    {
        WebPager3.CurrentPageIndex = 0;
        WebPager3.DataSource = this.DocumentDataSource;
        WebPager3.DataBind();
    }
    protected void WebPager3_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager3.CurrentPageIndex = e.NewPageIndex;
        WebPager3.DataSource = this.DocumentDataSource;
        WebPager3.DataBind();
    }
    protected void WebPager2_PageSizeChanged(object s, PageSizeChangeEventArgs e)
    {
        WebPager2.CurrentPageIndex = 0;
        WebPager2.DataSource = this.DividerDataSource;
        WebPager2.DataBind();
    }
    protected void WebPager2_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager2.CurrentPageIndex = e.NewPageIndex;
        WebPager2.DataSource = this.DividerDataSource;
        WebPager2.DataBind();
    }
    protected void WebPager1_PageSizeChanged(object s, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.FolderDataSource;
        WebPager1.DataBind();
    }
    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.FolderDataSource;
        WebPager1.DataBind();
    }

    [WebMethod]
    public static void LoadShareLogTable(string uid)
    {
        try
        {
            Office_DocumentAndFolderLogs docs = new Office_DocumentAndFolderLogs();

            docs.FolderShareLogSource = null;
            docs.DocumentShareLogSource = null;
            docs.WorkAreaShareLogSource = null;
            docs.EdFileFlowLogSource = null;
            string defaultUID = Sessions.SwitchedSessionId;
            //string defaultUID = Membership.GetUser().ProviderUserKey.ToString();
            DataSet shareLogs = General_Class.GetAllShareLinkLogs((uid == "" || uid == null) ? defaultUID : uid);
            docs.FolderShareLogSource = shareLogs.Tables[0];
            docs.DocumentShareLogSource = shareLogs.Tables[1];
            docs.WorkAreaShareLogSource = shareLogs.Tables[2];
            docs.EdFileFlowLogSource = shareLogs.Tables[3];
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(uid, string.Empty, string.Empty, ex);
        }
    }
    protected void GridView6_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.FolderShareLog_Sort_Direction == SortDirection.Ascending)
        {
            this.FolderShareLog_Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.FolderShareLog_Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.FolderShareLogSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.FolderShareLogSource = Source.ToTable();
        BindShareDataGrid();
    }
    protected void GridView7_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.DocumentShareLog_Sort_Direction == SortDirection.Ascending)
        {
            this.DocumentShareLog_Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.DocumentShareLog_Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DocumentShareLogSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DocumentShareLogSource = Source.ToTable();
        BindShareDataGrid();
    }
    protected void GridView8_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.WorkAreaShareLog_Sort_Direction == SortDirection.Ascending)
        {
            this.WorkAreaShareLog_Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.WorkAreaShareLog_Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.WorkAreaShareLogSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.WorkAreaShareLogSource = Source.ToTable();
        BindShareDataGrid();
    }
    //protected void GridView9_Sorting(object sender, GridViewSortEventArgs e)
    //{
    //    string sortDirection = "";
    //    string sortExpression = e.SortExpression;
    //    if (this.EdFileFlowShareLog_Sort_Direction == SortDirection.Ascending)
    //    {
    //        this.EdFileFlowShareLog_Sort_Direction = SortDirection.Descending;
    //        sortDirection = "DESC";
    //    }
    //    else
    //    {
    //        this.EdFileFlowShareLog_Sort_Direction = SortDirection.Ascending;
    //        sortDirection = "ASC";
    //    }
    //    DataView Source = new DataView(this.EdFileFlowLogSource);
    //    Source.Sort = e.SortExpression + " " + sortDirection;

    //    this.EdFileFlowLogSource = Source.ToTable();
    //    BindShareDataGrid();
    //}

    private SortDirection FolderShareLog_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(Srt_Folder_Dir.Value))
            {
                Srt_Folder_Dir.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.Srt_Folder_Dir.Value);

        }
        set
        {
            this.Srt_Folder_Dir.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    private SortDirection DocumentShareLog_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(Srt_Doc_Dir.Value))
            {
                Srt_Doc_Dir.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.Srt_Doc_Dir.Value);

        }
        set
        {
            this.Srt_Doc_Dir.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    private SortDirection WorkAreaShareLog_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(Srt_WorkArea_Dir.Value))
            {
                Srt_WorkArea_Dir.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.Srt_WorkArea_Dir.Value);

        }
        set
        {
            this.Srt_WorkArea_Dir.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    //private SortDirection EdFileFlowShareLog_Sort_Direction
    //{
    //    get
    //    {
    //        if (string.IsNullOrEmpty(Srt_EdFileFlow_Dir.Value))
    //        {
    //            Srt_EdFileFlow_Dir.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
    //        }

    //        return (SortDirection)Enum.Parse(typeof(SortDirection), this.Srt_EdFileFlow_Dir.Value);

    //    }
    //    set
    //    {
    //        this.Srt_EdFileFlow_Dir.Value = Enum.GetName(typeof(SortDirection), value);
    //    }
    //}

    protected void WebPager6_PageSizeChanged(object s, PageSizeChangeEventArgs e)
    {
        WebPager6.CurrentPageIndex = 0;
        WebPager6.DataSource = this.FolderShareLogSource;
        WebPager6.DataBind();
    }
    protected void WebPager6_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager6.CurrentPageIndex = e.NewPageIndex;
        WebPager6.DataSource = this.FolderShareLogSource;
        WebPager6.DataBind();
    }

    protected void WebPager7_PageSizeChanged(object s, PageSizeChangeEventArgs e)
    {
        WebPager7.CurrentPageIndex = 0;
        WebPager7.DataSource = this.DocumentShareLogSource;
        WebPager7.DataBind();
    }
    protected void WebPager7_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager7.CurrentPageIndex = e.NewPageIndex;
        WebPager7.DataSource = this.DocumentShareLogSource;
        WebPager7.DataBind();

    }

    protected void WebPager8_PageSizeChanged(object s, PageSizeChangeEventArgs e)
    {
        WebPager8.CurrentPageIndex = 0;
        WebPager8.DataSource = this.WorkAreaShareLogSource;
        WebPager8.DataBind();
    }
    protected void WebPager8_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager8.CurrentPageIndex = e.NewPageIndex;
        WebPager8.DataSource = this.WorkAreaShareLogSource;
        WebPager8.DataBind();
    }

    //protected void WebPager9_PageSizeChanged(object s, PageSizeChangeEventArgs e)
    //{
    //    WebPager9.CurrentPageIndex = 0;
    //    WebPager9.DataSource = this.EdFileFlowLogSource;
    //    WebPager9.DataBind();
    //}
    //protected void WebPager9_PageIndexChanged(object sender, PageChangedEventArgs e)
    //{
    //    WebPager9.CurrentPageIndex = e.NewPageIndex;
    //    WebPager9.DataSource = this.EdFileFlowLogSource;
    //    WebPager9.DataBind();
    //}

    protected void GridView10_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drvnew = e.Row.DataItem as DataRowView;
            string WorkAreaId = drvnew.Row["WorkAreaId"].ToString();

        }
    }
    protected void GridView10_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.WorkArea_Sort_Direction == SortDirection.Ascending)
        {
            this.WorkArea_Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.WorkArea_Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.WorkAreaDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.WorkAreaDataSource = Source.ToTable();
        BindDataGrid();
    }
    protected void WebPager10_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager10.CurrentPageIndex = e.NewPageIndex;
        WebPager10.DataSource = this.WorkAreaDataSource;
        WebPager10.DataBind();
    }
    protected void WebPager10_PageSizeChanged(object s, PageSizeChangeEventArgs e)
    {
        WebPager10.CurrentPageIndex = 0;
        WebPager10.DataSource = this.WorkAreaDataSource;
        WebPager10.DataBind();
    }
    protected void GridView11_RowDataBound(object sender, GridViewRowEventArgs e)
    {
    }
    protected void GridView11_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.EFileFlow_Sort_Direction == SortDirection.Ascending)
        {
            this.EFileFlow_Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.EFileFlow_Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.EdFormsDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.EdFormsDataSource = Source.ToTable();
        BindDataGrid();
    }
    protected void WebPager11_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager11.CurrentPageIndex = e.NewPageIndex;
        WebPager11.DataSource = this.EdFormsDataSource;
        WebPager11.DataBind();
    }
    protected void WebPager11_PageSizeChanged(object s, PageSizeChangeEventArgs e)
    {
        WebPager11.CurrentPageIndex = 0;
        WebPager11.DataSource = this.EdFormsDataSource;
        WebPager11.DataBind();
    }
    protected void GridView12_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drvnew = e.Row.DataItem as DataRowView;
            string EFileFlowShareId = drvnew.Row["ID"].ToString();

        }
    }
    protected void GridView12_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.EFileFlowShare_Sort_Direction == SortDirection.Ascending)
        {
            this.EFileFlowShare_Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.EFileFlowShare_Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.EFileFlowShareDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.EFileFlowShareDataSource = Source.ToTable();
        BindDataGrid();
    }
    protected void WebPager12_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager12.CurrentPageIndex = e.NewPageIndex;
        WebPager12.DataSource = this.EFileFlowShareDataSource;
        WebPager12.DataBind();
    }
    protected void WebPager12_PageSizeChanged(object s, PageSizeChangeEventArgs e)
    {
        WebPager12.CurrentPageIndex = 0;
        WebPager12.DataSource = this.EFileFlowShareDataSource;
        WebPager12.DataBind();
    }
}