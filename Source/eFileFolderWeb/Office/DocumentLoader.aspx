<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    EnableSessionState="True" EnableViewState="true" CodeFile="DocumentLoader.aspx.cs"
    Inherits="DocumentLoader" Title="" %>

<%@ Register TagPrefix="fjx" Namespace="com.flajaxian" Assembly="com.flajaxian.FileUploader" %>
<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register TagPrefix="cust" Namespace="Shinetech.Engines.Adapters" Assembly="FormatEngine" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript">

        function FileStateChanged(uploader, file, httpStatus, isLast) {
            Flajaxian.fileStateChanged(uploader, file, httpStatus, isLast);
            if (file.state == 3 && isLast) {
                var obj = document.getElementById("<%= label12.ClientID %>");
                obj.innerHTML = "Converting, Please Wait.";
                obj = document.getElementById("<%= Imagelabel12.ClientID %>");
                obj.setAttribute("src", "../Images/wait24trans.gif");
                obj.setAttribute("Visible", "true");

                window.location = "http://www.edfiles.com/Office/DocumentLoader.aspx?View=1";

            }
            else if (file.state == 5 && isLast) {
                //alert("Failed to upload documents.");
            }

        }

        function setConversionText() {
            var obj = document.getElementById("<%= lbsStatus.ClientID %>");
            obj.setAttribute("src", "../Images/wait24trans.gif");
            obj.setAttribute("Visible", "true");
            //obj.innerHTML = " (Converting, Please Wait.)";
        }

        function GetArgsFromHref(sHref, sArgName) {
            var args = sHref.split("?");
            var retval = "";

            if (args[0] == sHref) /*参数为空*/ {
                return retval; /*无需做任何处理*/
            }
            var str = args[1];
            args = str.split("&");
            for (var i = 0; i < args.length; i++) {
                str = args[i];
                var arg = str.split("=");
                if (arg.length <= 1) continue;
                if (arg[0] == sArgName) retval = arg[1];
            }
            return retval;
        }

    </script>
    <tr>
        <td height="186" valign="top" style="background-repeat: no-repeat;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="730" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                        style="background-repeat: repeat-x;">
                        <div style="background: url(images/inner_b.gif); background-position: right top; background-repeat: no-repeat; margin-top: 10px;" />
                        <div style="height: 75px;">
                            <div class="Naslov_Sin">
                                Document Loader
                            </div>
                        </div>
                        <div class="podnaslov" style="padding: 5px; background-color: #f6f5f2;">
                            In order to add documents and files to your EdFiles, please select the folder
                                from the drop down menu below, then select the tab in which you would like to place
                                the document in and then select the file/document you would like to place in that
                                tab of the EdFiles. It is that simple. If you need assistance, please feel free
                                to chat, email or call customer service.
                        </div>
                        <asp:Panel ID="panelMain" runat="server" Visible="true">
                            <div class="tekstDef_upload">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                        <asp:HiddenField ID="HiddenField2" runat="server" />
                                        <table border="0" cellpadding="2" cellspacing="0" width="100%" style="margin: 0px 0 10px 10px;">
                                            <tr>
                                                <td width="55%" valign="top">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <div class="form_title_2" style="margin-top: 0px; margin-bottom: 2px">
                                                                    1. Select an edFile
                                                                </div>
                                                                <ajaxToolkit:ComboBox ID="DropDownList1" runat="server" Width="255px" DropDownStyle="DropDownList"
                                                                    AutoCompleteMode="SuggestAppend" CssClass="WindowsStyle" DataValueField="FolderID"
                                                                    DataTextField="DisplayName" AutoPostBack="true" CaseSensitive="false" EnableViewState="true"
                                                                    OnSelectedIndexChanged="DropDownList1_OnSelectedIndexChanged" AppendDataBoundItems="false">
                                                                </ajaxToolkit:ComboBox>
                                                                <%--<asp:DropDownList ID="DropDownList1" AutoPostBack="true" Width="100%" DataValueField="FolderID"
                                                                        DataTextField="DisplayName" OnSelectedIndexChanged="DropDownList1_OnSelectedIndexChanged"
                                                                        runat="server">
                                                                    </asp:DropDownList>--%>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="100%" width="100%">
                                                                <div class="form_title_2" style="margin-top: 20px; margin-bottom: 2px">
                                                                    2. Select a tab for the selected eFileFoldor
                                                                </div>
                                                                <asp:ListBox ID="ListBox1" Width="100%" Height="150" DataValueField="DividerID" DataTextField="Name"
                                                                    AutoPostBack="true" OnSelectedIndexChanged="ListBox1_OnSelectedIndexChanged"
                                                                    runat="server"></asp:ListBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="45%" valign="top">
                                                    <div>
                                                        <asp:Image runat="server" ID="Imagelabel12" ImageUrl="../Images/loader_bg.gif" ImageAlign="Middle" /><asp:Label
                                                            ID="label12" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="form_title_2" style="margin-top: 3px; margin-bottom: 2px">
                                                        3. Click the Browse button to upload document
                                                    </div>
                                                    <fjx:FileUploader ID="FileUploader1" MaxNumberFiles="10" MaxFileSize="50MB" MaxFileSizeReachedMessage="No files bigger than {0} are allowed"
                                                        AllowedFileTypes="All Files(*.*):*.pdf;*.doc;*.xls;*.ppt;*.odt;*.ods;*.odp;*.rtf;*.jpeg;*.jpg|Adobe PDF Files(*.pdf):*.pdf|Microsoft Office Documents(*.doc; *.xls; *.ppt):*.doc;*.xls;*.ppt|OpenOffice.org Documents(*.odt; *.ods; *.odp): *.odt;*.ods;*.odp|Rich Text Format(*.rtf):*.rtf|Image Files(*.jpeg):*.jpeg;*.jpg"
                                                        UseInsideUpdatePanel="true" JsFunc_FileStateChanged="FileStateChanged" RequestAsPostBack="true"
                                                        OnFileReceived="OnFileReceived" runat="server">
                                                        <Adapters>
                                                            <cust:FileSaverAdapterEx Runat="server" FolderName="../Volume" />
                                                            <cust:ImageAdapter FolderName="../Volume" PathUrl="Volume" Runat="server" />
                                                        </Adapters>
                                                    </fjx:FileUploader>
                                                </td>
                                            </tr>
                                            <%-- workarea files--%>
                                            <tr>
                                                <td colspan="2">
                                                    <div class="form_title_2">
                                                        4. Files in Work Area...have not been assigned to any folder<asp:Image runat="server"
                                                            ImageUrl="../Images/loader_bg.gif" ID="lbsStatus" ImageAlign="Middle" />
                                                    </div>
                                                    <asp:GridView ID="GridView1" runat="server" OnSorting="GridView1_Sorting" AllowSorting="True"
                                                        AutoGenerateColumns="false" Width="100%" BorderWidth="2" OnRowDataBound="GridView1_RowDataBound">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                        <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                        <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                        <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Action" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButtonEdit" runat="server" Text="Edit" CommandArgument='<%# Eval("FilePath")%>'
                                                                        OnCommand="LinkButtonEdit_Click"></asp:LinkButton>

                                                                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("FilePath")%>'
                                                                        OnCommand="LinkButton2_Click"></asp:LinkButton>
                                                                    <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="LinkButton2"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="LinkButton2">
                                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                          
                                                            <asp:HyperLinkField DataTextField="FileUID" HeaderText="FileName" SortExpression="FileUID"
                                                                DataNavigateUrlFormatString="~/Office/SharePDF.aspx?id={0}&uid={1}" Target="_blank" DataNavigateUrlFields="FileID,UID">
                                                                <HeaderStyle CssClass="form_title" />
                                                            </asp:HyperLinkField>
                                                            <asp:BoundField DataField="Amount" HeaderText="Amount" HtmlEncode="false" />
                                                            <asp:BoundField DataField="CheckNo" HeaderText="Check/Wire No." HtmlEncode="false" />
                                                            <asp:BoundField DataField="Size" HeaderText="Size(Mb)" DataFormatString="{0:F2}"
                                                                HtmlEncode="false" />
                                                            <asp:BoundField DataField="DOB" HeaderText="Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" />
                                                            <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Add to Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton3" runat="server" Text="Add" CommandArgument='<%# Eval("FilePath")%>'
                                                                        OnCommand="LinkButton3_Click" OnClientClick="setConversionText();"></asp:LinkButton>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ButtonAdd" runat="server" ConfirmText="Are you sure you want to add this document to the selected tab?"
                                                                        TargetControlID="LinkButton3">
                                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label9" runat="server" Text="Add to EFF" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton4" runat="server" Text="Create" CommandArgument='<%# Eval("FilePath")%>'
                                                                        OnCommand="LinkButton4_Click" OnClientClick="setConversionText();"></asp:LinkButton>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ButtonAddEFF" runat="server" ConfirmText="Are you sure you want to add this document to new eff?"
                                                                        TargetControlID="LinkButton4">
                                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="Label22" runat="server" Text="Status" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="checkBox1" runat="server" AutoPostBack="false" OnCheckedChanged="CheckBox1_CheckedChanged">
                                                                        </asp:CheckBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                                <tr>
                                                                    <td align="left" class="podnaslov">
                                                                        <asp:Label ID="Label120" runat="server" />There are no files. right now.
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="prevnext" bgcolor="#f8f8f5" colspan="2">
                                                    <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                                        PageSize="5" CssClass="prevnext" PagerStyle="NextPrev" ControlToPaginate="GridView1"></cc1:WebPager>
                                                    <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="prevnext" bgcolor="#f8f8f5" colspan="2" align="center">
                                                    <asp:TextBox ID="textfield4" name="textfield4" runat="server" class="form_tekst_field" MaxLength="50"
                                                        autocomplete="off" />
                                                    <ajaxToolkit:AutoCompleteExtender runat="server" UseContextKey="true" BehaviorID="AutoCompleteEx1"
                                                        ID="autoComplete2" TargetControlID="textfield4" ServicePath="~/FlexWebService.asmx"
                                                        ServiceMethod="GetCompletionFileNameList" MinimumPrefixLength="1" CompletionInterval="1000"
                                                        EnableCaching="true" CompletionSetCount="10" CompletionListCssClass="autocomplete_completionListElement"
                                                        CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                        DelimiterCharacters=";, :" ShowOnlyCurrentWordInCompletionListItem="true">
                                                        <Animations>
                                                                <OnShow>
                                                                    <Sequence>
                                                                        <%-- Make the completion list transparent and then show it --%>
                                                                        <OpacityAction Opacity="0" />
                                                                        <HideAction Visible="true" />
                            
                                                                        <%--Cache the original size of the completion list the first time
                                                                            the animation is played and then set it to zero --%>
                                                                        <ScriptAction Script="
                                                                            // Cache the size and setup the initial size
                                                                            var behavior = $find('AutoCompleteEx1');
                                                                            if (!behavior._height) {
                                                                                var target = behavior.get_completionList();
                                                                                behavior._height = target.offsetHeight - 2;
                                                                                target.style.height = '0px';
                                                                            }" />
                            
                                                                        <%-- Expand from 0px to the appropriate size while fading in --%>
                                                                        <Parallel Duration=".4">
                                                                            <FadeIn />
                                                                            <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                                                                        </Parallel>
                                                                    </Sequence>
                                                                </OnShow>
                                                                <OnHide>
                                                                    <%-- Collapse down to 0pxc and fade out --%>
                                                                    <Parallel Duration=".4">
                                                                        <FadeOut />
                                                                        <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                                                                    </Parallel>
                                                                </OnHide>
                                                        </Animations>
                                                    </ajaxToolkit:AutoCompleteExtender>
                                                    <%--<asp:ImageButton ID="searchButton2" name="searchButton2" ImageAlign="AbsMiddle" runat="server" CausesValidation="false"
                                                            ImageUrl="~/images/search_btn.gif" Width="60" Height="21" OnClick="search_btn2_click" />--%>
                                                    <asp:Button ID="searchButton2" name="searchButton2" runat="server" CausesValidation="false"
                                                        Width="60" Height="21" OnClick="search_btn2_click" Text="Search" />
                                                </td>
                                                <%--  <td class="prevnext" bgcolor="#f8f8f5" colspan="2" align="center">
                                                        <asp:Button ID="btnConvert" runat="server" Text="Add to tab" OnClick="btnConvert_Clicked"
                                                            OnClientClick="setConversionText();" />
                                                    </td>--%>
                                            </tr>
                                            <tr>
                                                <td class="prevnext" bgcolor="#f8f8f5" colspan="2" align="center">
                                                    <br />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="DropDownList1" EventName="SelectedIndexChanged" />
                                        <asp:AsyncPostBackTrigger ControlID="ListBox1" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </asp:Panel>
                        <asp:Panel class="popupConfirmation_1" ID="DivDeleteConfirmation" Style="display: none"
                            runat="server">
                            <div class="popup_Container">
                                <div class="popup_Titlebar" id="PopupHeader">
                                    <div class="TitlebarLeft">
                                        Delete File
                                    </div>
                                    <div class="TitlebarRight" onclick="$get('ButtonDeleteCancel').click();">
                                    </div>
                                </div>
                                <div class="popup_Body_1">
                                    <p>
                                        Caution! Are you sure you want to delete this file?
                                    </p>
                                    <p>
                                        Confirm Password:
                                            <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                                    </p>
                                </div>
                                <div class="popup_Buttons">
                                    <input id="ButtonDeleleOkay" type="button" value="Yes" style="margin-left: 80px; width: 65px" />
                                    <input id="ButtonDeleteCancel" type="button" value="No" style="margin-left: 20px; width: 65px" />
                                </div>
                            </div>
                        </asp:Panel>

                        <asp:Button Style="display: none" ID="btnShowPopup" runat="server"></asp:Button>
                        <ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" BackgroundCssClass="modalBackground" 
                            CancelControlID="ButtonEditCancel1" OkControlID="ButtonEditOk" PopupControlID="DivEditConfirmation" TargetControlID="btnShowPopup">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel class="popupConfirmation_1" ID="DivEditConfirmation" Style="display: none" runat="server">
                            <asp:UpdatePanel ID="updPnlEditDetail" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="popup_Container">
                                        <div class="popup_Titlebar" id="PopupHeaderEdit">
                                            <div class="TitlebarLeft">
                                                Edit File
                                            </div>

                                        </div>
                                        <div class="popup_Body_1">
                                            <table>
                                                <tr>
                                                    <td>File Name</td>
                                                    <td>
                                                        <asp:HiddenField runat="server" ID="hidFullName"/>
                                                        <asp:TextBox ID="TextBoxFileName" runat="server" Width="200px" MaxLength="20" />

                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>First Name</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxFirstName" runat="server" Width="200px" MaxLength="20" /></td>
                                                </tr>
                                                <tr>
                                                    <td>Last Name</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxLastName" runat="server" Width="200px" MaxLength="20" /></td>
                                                </tr>
                                                <tr>
                                                    <td>Company Name</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxCompanyName" runat="server" Width="200px" MaxLength="20" /></td>
                                                </tr>
                                                <tr>
                                                    <td>Amount</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxAmount" runat="server" Width="200px" MaxLength="20" /></td>
                                                </tr>
                                                <tr>
                                                    <td>Check/Wire No.</td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxCheckNo" runat="server" Width="200px" MaxLength="20" /></td>
                                                </tr>

                                            </table>
                                        </div>
                                        <div class="popup_Buttons">
                                            <asp:Button ID="ButtonEditOk" runat="server" OnClick="ButtonEditOk_Click" Text="Save" Width="65px" Style="margin-left: 80px; width: 65px" />
                                            <asp:Button ID="ButtonEditCancel1" runat="server" OnClick="ButtonEditCancel1_Click"  Text="Cancel" Width="65px" Style="margin-left: 20px; width: 65px" />
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonEditOk" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="ButtonEditCancel1" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </asp:Panel>

                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Panel ID="panelConfirm" runat="server" Visible="false">
                                    <div class="form_title_1">
                                        Document was uploaded successfully. Next Step:
                                    </div>
                                    <div class="tekstDef" style="">
                                        <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                            <tr>
                                                <td align="left" class="podnaslov">
                                                    <asp:Label ID="lbConfirm" runat="server" />
                                                    Do you want to
                                                        <asp:HyperLink ID="btnUpload" runat="server" Text="Upload More Documents" NavigateUrl="~/Office/DocumentLoader.aspx"></asp:HyperLink>
                                                    or <a href="#" onclick="window.open('WebFlashViewer.aspx?ID=<%=FolderId %>', 'eBook', 'toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no'); return false;">View Your EdFile Now</a> ?
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="PanelInfor" runat="server" Visible="false">
                                    <div class="tekstDef" style="">
                                        <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                            <tr>
                                                <td align="left" class="podnaslov">
                                                    <asp:Label ID="Label1" runat="server" />There is no folders. please check the link
                                                        to
                                                        <asp:HyperLink ID="HyperLink1" runat="server" Text="Create New EdFile" NavigateUrl="~/Office/NeweFileFolder.aspx"></asp:HyperLink>
                                                    right now.
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div style="padding-left: 30px;"></div>
                    </td>
                    <td width="10" align="center">
                        <img src="../images/lin.gif" width="1" height="453" alt="" vspace="10" />
                    </td>
                    <td valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2" style="background-repeat: repeat-x;">
                        <%--<uc1:QuickFind ID="QuickFind1" runat="server" />--%>
                        <%--<uc2:ReminderControl ID="ReminderControl1" runat="server" />--%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</asp:Content>
