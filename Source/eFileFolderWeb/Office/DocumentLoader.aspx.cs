using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Aspose.Pdf;
using com.flajaxian;
using Shinetech.DAL;
using Shinetech.Engines.Adapters;
using Shinetech.Framework.Controls;

using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using PdfSharp.Pdf.Security;
using Shinetech.Utility;


public partial class DocumentLoader : StoragePage
{
    public string FolderId;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["View"] != null && Request.QueryString["View"] == "1")
        {
            Page.Response.Redirect("~/Office/DocumentLoading.aspx", false);
            return;
            /*if (!string.IsNullOrEmpty(EffID))
            {
                string folderId = EffID;
                panelMain.Visible = false;
                PanelInfor.Visible = false;
                panelConfirm.Visible = true;
                btnUpload.NavigateUrl = btnUpload.NavigateUrl;
                FolderId = folderId;
            }*/
        }
        else
        {
            if (!Page.IsPostBack)
            {
                autoComplete2.ContextKey = Sessions.SwitchedSessionId;
                //autoComplete2.ContextKey = Membership.GetUser().ProviderUserKey.ToString();

                Session["LastFile"] = false;
                Session["FileName"] = "";

                FileUploader1.MaxFileSize = WebConfigurationManager.AppSettings["MaxFileSize"];
                DataTable dtFolders = GetEFileFolders();
                if (dtFolders.Rows.Count == 0)
                {
                    PanelInfor.Visible = true;
                    panelMain.Visible = false;
                    panelConfirm.Visible = false;
                }
                else
                {
                    BindEFileFolders();

                    DataBindDividers();


                    //列出所有的workarea文件
                    GetData();
                    BindGrid();
                }
            }
            else
            {
                if (FileUploader1.FileIsPosted)//提交文件成功后调用
                {
                    //如果是pdf文件则要判断是否可用可复制
                    //Request.Files[0].InputStream
                    //Timer1.Enabled = true;
                    FileUploader1.Enabled = false;

                    FileUploader1.State["VPath"] = WebConfigurationManager.AppSettings["PdfPath"];
                    FileUploader1.State["EffID"] = this.EffID;
                    FileUploader1.State["DividerID"] = this.DividerID;
                    FileUploader1.State["UID"] = Sessions.SwitchedSessionId;
                    //FileUploader1.State["UID"] = Membership.GetUser().ProviderUserKey.ToString();
                }
                else //第一次加载页面
                {
                    Session["LastFile"] = false;
                    Session["FileName"] = "";
                }

            }
        }
    }

    private void BindEFileFolders()
    {
        DataTable table = GetEFileFolders();
        //ComboBox1.DataSource = table;
        //ComboBox1.DataBind();

        DropDownList1.DataSource = table.DefaultView;
        DropDownList1.DataBind();

        //ComboBoxFolder.DataSource = table.DefaultView;
        //ComboBoxFolder.DataBind();

        if (!string.IsNullOrEmpty(EffID))
        {
            string folderId = EffID;

            DropDownList1.SelectedValue = folderId;
        }
        else
        {
            DropDownList1.SelectedIndex = 0;
        }

        if (!string.IsNullOrEmpty(Page.Request.QueryString["FolderId"]))
        {
            string folderId = Page.Request.QueryString["FolderId"];
            DropDownList1.SelectedValue = folderId;
        }
    }

    private void GetData()
    {
        //获取当前用户的workarea文件
        //todo: 要改为可以看到offices and users的文件 2013-06-28
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        DataTable table = GetWorkareaFiles(uid);
        if (table != null)
        {
            this.DataSource = table;
        }
    }

    private void GetData(string keyword)
    {
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        DataTable table = GetWorkareaFiles(uid, keyword);
        if (table != null)
        {
            this.DataSource = table;
        }
    }

    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["Files_Workarea"] as DataTable;
        }
        set
        {
            this.Session["Files_Workarea"] = value;
        }
    }


    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }


    public DataTable GetEFileFolders()
    {
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        Shinetech.DAL.Account account = new Shinetech.DAL.Account(uid);
        string officeId;
        DataTable effTable = null;
        if (account.IsExist)
        {
            if (account.IsSubUser.Value.Equals("1"))
            {
                officeId = account.OfficeUID.ToString();
            }
            else
            {
                officeId = uid;
            }

            effTable = FileFolderManagement.GetFileFoldersWithDisplayName(uid, officeId);
        }

        return effTable;
    }

    public DataTable GetDividersByEFF(int eff)
    {
        DataTable dividerTable = FileFolderManagement.GetDividers(eff);
        return dividerTable;
    }


    protected void ComboBoxFolder_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataBindDividers();
    }

    protected void DropDownList1_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DataBindDividers();
    }

    public void DataBindDividers()
    {
        if (DropDownList1.Items.Count == 0 || DropDownList1.SelectedValue == "")
        {
            FileUploader1.Enabled = (ListBox1.Items.Count != 0);
            return;
        }
        int effid = Convert.ToInt32(DropDownList1.SelectedValue);
        this.EffID = DropDownList1.SelectedValue;

        ListBox1.DataSource = GetDividersByEFF(effid).DefaultView;
        ListBox1.DataBind();

        if (ListBox1.Items.Count > 0) ListBox1.SelectedIndex = 0;
        FileUploader1.Enabled = (ListBox1.Items.Count != 0);
        this.DividerID = ListBox1.SelectedValue;

        FileUploader1.State["EffID"] = DropDownList1.SelectedValue;
    }

    protected void ListBox1_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        this.DividerID = ListBox1.SelectedValue;
    }

    public string DividerID
    {
        get
        {
            return (string)this.Session["DividerID"];
        }
        set
        {
            Session["DividerID"] = value;
        }
    }

    public string EffID
    {
        get
        {
            return (string)Session["EFFID"];
        }
        set
        {
            Session["EFFID"] = value;

        }
    }

    protected void OnFileReceived(object sender, FileReceivedEventArgs e)
    {
        if (e.isLast)
        {
            Session["LastFile"] = true;
            Session["FileName"] = "";
        }
    }


    protected void Timer1_Tick(object sender, EventArgs e)
    {
        if (Session["FileName"] != null && (string)Session["FileName"] != "")
        {
            label12.Text = string.Format("Converting {0}, Please waiting for ...", Session["FileName"]);
        }

        if (Session["LastFile"] != null && (bool)Session["LastFile"])
        {
            Session["LastFile"] = false;
            Session["FileName"] = "";
            Response.Redirect(Page.Request.Url + "?View=1");
        }
    }

    protected void LinkButton2_Click(Object sender, CommandEventArgs e)
    {
        string password = textPassword.Text.Trim();
        string strPassword = PasswordGenerator.GetMD5(password);

        if (UserManagement.GetPassword(User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
        {
            //show the password is wrong
            //Page.RegisterStartupScript("RssNO", "<script>alert('Your password is wrong!');</script>");
            //ClientScript.RegisterStartupScript(UpdatePanel1, "RssNO", "<script>alert('Your password is wrong!');</script>");
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);

        }
        else
        {
            string pathRoot = Server.MapPath("~/ScanRestoreBox");
            if (!Directory.Exists(pathRoot))
            {
                Directory.CreateDirectory(pathRoot);
            }

            string pathSource = e.CommandArgument.ToString();
            string destPath = string.Empty;
            if (pathSource.IndexOf("ScanInBox") > 0)
            {
                destPath = pathSource.Replace("ScanInBox", "ScanRestoreBox");
                string destDirectory = Path.GetDirectoryName(destPath);
                try
                {
                    if (!Directory.Exists(destDirectory))
                    {
                        Directory.CreateDirectory(destDirectory);
                    }

                    File.Move(pathSource, destPath);
                    GetData();
                    BindGrid();

                }
                catch (Exception exp) { Console.WriteLine(exp.Message); }
            }
        }
    }

    protected void LinkButton4_Click(Object sender, CommandEventArgs e)
    {
        string filePath = e.CommandArgument.ToString();
        if (File.Exists(filePath))
        {
            try
            {
                string uid = Sessions.SwitchedSessionId;
                //string uid = Membership.GetUser().ProviderUserKey.ToString();
                Shinetech.DAL.Account account = new Shinetech.DAL.Account(uid);

                string strFolderName = Path.GetFileNameWithoutExtension(filePath);
                char[] chars = { ',', ' ' };
                string[] tokens = strFolderName.Split(chars, StringSplitOptions.RemoveEmptyEntries);

                FileFolder folder = new FileFolder();
                folder.FolderName.Value = strFolderName;

                if (tokens.Length == 1)
                {
                    folder.FirstName.Value = tokens[0];
                    folder.Lastname.Value = string.Empty;
                }
                else if (tokens.Length == 2)
                {
                    folder.FirstName.Value = tokens[0];
                    folder.Lastname.Value = tokens[1];
                }
                else
                {
                    folder.FirstName.Value = tokens[0];
                    folder.Lastname.Value = tokens[tokens.Length - 1];
                }

                folder.DOB.Value = null;
                folder.CreatedDate.Value = DateTime.Now;
                folder.SecurityLevel.Value = 1;
                folder.StateID.Value = "CA";

                folder.OfficeID.Value = uid;
                folder.OfficeID1.Value = Sessions.SwitchedRackspaceId;

                //if (this.Page.User.IsInRole("Offices"))
                //{
                //    folder.OfficeID1.Value = uid;
                //}

                //else if (this.Page.User.IsInRole("Users"))
                //{
                //    DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(uid);
                //    folder.OfficeID1.Value = dtOfficeUser.Rows[0]["OfficeUID"].ToString();
                //}

                Divider newEntity = new Divider();
                newEntity.Name.Value = strFolderName;
                newEntity.Color.Value = "#e78302";

                int newfolderId = FileFolderManagement.CreatNewFileFolder(folder, newEntity);

                //New Field for Folder in 2012-10-28          
                string fid = newfolderId.ToString();
                string tid = newEntity.DividerID.Value.ToString();

                string vpath = Server.MapPath("~/Volume");
                string pathname = string.Format("{0}\\{1}\\{2}", vpath, fid, tid);

                string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, fid, Path.DirectorySeparatorChar, tid); //转换目标路径
                if (!Directory.Exists(dstpath))
                {
                    Directory.CreateDirectory(dstpath);
                }

                string filename = Path.GetFileName(filePath);
                string encodeName = HttpUtility.UrlPathEncode(filename);
                encodeName = encodeName.Replace("%", "$");
                dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);

                if (File.Exists(filePath))
                {
                    //拷贝文件并转换格式
                    try
                    {
                        File.Copy(filePath, dstpath, true);
                        ImageAdapter.ProcessPostFile(dstpath, fid, tid, uid, pathname, filePath);

                        File.Delete(filePath); //添加后删除原文

                        //列出所有的workarea文件
                        this.EffID = fid;
                        this.DividerID = tid;

                        //GetData();
                        //BindGrid();
                    }
                    catch (Exception exp)
                    {
                        lbsStatus.ImageUrl = "";
                        string strWrongInfor = string.Format("Error : Creating failed -{0}.", exp.Message);
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                        return;
                    }
                }

                //BindEFileFolders();

                // DataBindDividers();

            }
            catch (Exception exp)
            {
                lbsStatus.ImageUrl = "";
                string strWrongInfor = string.Format("Error : Converting failed -{0}.", exp.Message);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return;
            }

            lbsStatus.ImageUrl = "";

            Server.Transfer("~/Office/DocumentLoader.aspx?View=1");
        }

    }
    //Add to Tab for each pdf
    protected void LinkButton3_Click(Object sender, CommandEventArgs e)
    {

        string filePath = e.CommandArgument.ToString();
        if (File.Exists(filePath))
        {
            string uid = Sessions.SwitchedSessionId;
            //string uid = Membership.GetUser().ProviderUserKey.ToString();

            if (DropDownList1.Items.Count == 0) return;
            string fid = DropDownList1.SelectedValue;

            if (ListBox1.Items.Count == 0) return;
            string tid = ListBox1.SelectedValue;

            string vpath = Server.MapPath("~/Volume");
            string pathname = string.Format("{0}\\{1}\\{2}", vpath, fid, tid);

            string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, fid, Path.DirectorySeparatorChar, tid); //转换目标路径
            if (!Directory.Exists(dstpath))
            {
                Directory.CreateDirectory(dstpath);
            }

            string filename = Path.GetFileName(filePath);
            string encodeName = HttpUtility.UrlPathEncode(filename);
            encodeName = encodeName.Replace("%", "$");
            dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);
            bool existFile = false;

            if (File.Exists(filePath))
            {
                //拷贝文件并转换格式
                try
                {
                    existFile = true;

                    File.Copy(filePath, dstpath, true);
                    ImageAdapter.ProcessPostFile(dstpath, fid, tid, uid, pathname, filePath);

                    File.Delete(filePath); //添加后删除原文

                    //列出所有的workarea文件
                    //GetData();
                    //BindGrid();
                }
                catch { }
            }

            if (existFile)
            {
                Server.Transfer("~/Office/DocumentLoader.aspx?View=1");
            }
            else
            {
                lbsStatus.ImageUrl = "";
            }
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected DataTable GetWorkareaFiles(string uid)
    {
        DataTable table = MakeDataTable();

        ArrayList uids = new ArrayList();
        uids.Add(uid);

        string path = Server.MapPath("~/ScanInBox");
        if (this.Page.User.IsInRole("Users") && !Common_Tatva.IsUserSwitched())
        {
            DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(uid);
            var officeID1 = dtOfficeUser.Rows[0]["OfficeUID"].ToString();
            uids.Add(officeID1);
        }
        else
        {
            uids.AddRange(UserManagement.GetSubUserList(uid));
        }

        string tempPath = string.Empty;
        foreach (string id in uids)
        {
            tempPath = string.Concat(path, Path.DirectorySeparatorChar, id);

            try
            {
                if (Directory.Exists(tempPath))
                {
                    string[] files = Directory.GetFiles(tempPath);
                    DataRow row = null;
                    FileInfo fi1;

                    string ext = string.Empty;

                    foreach (string file in files)
                    {
                        row = table.NewRow();
                        row["FileUID"] = Path.GetFileName(file);

                        byte[] byteArray = System.Text.Encoding.Default.GetBytes(Path.GetFileName(file));
                        row["FileID"] = System.Convert.ToBase64String(byteArray);

                        byteArray = System.Text.Encoding.Default.GetBytes(id); //uid
                        row["UID"] = System.Convert.ToBase64String(byteArray); ;

                        row["FilePath"] = file;
                        fi1 = new FileInfo(file);
                        row["Size"] = ((double)fi1.Length) / (double)(1024 * 1024);//MB
                        row["DOB"] = fi1.CreationTime;

                        ext = Path.GetExtension(file);

                        if (ext.ToLower().Equals(".pdf"))
                        {
                            #region PDF Document

                            Aspose.Pdf.Document docPDF = new Aspose.Pdf.Document(file);
                            var docInfo = new Aspose.Pdf.DocumentInfo(docPDF);

                            if (docInfo.ContainsKey("CheckNo")) row["CheckNo"] = docInfo["CheckNo"].ToString();
                            if (docInfo.ContainsKey("Amount")) row["Amount"] = docInfo["Amount"].ToString();

                            #endregion
                        }
                        else if (ext.ToLower().Equals(".doc") || ext.ToLower().Equals(".docx"))
                        {
                            #region docx Document
                            Aspose.Words.Document docWord = new Aspose.Words.Document(file);

                            if (docWord.CustomDocumentProperties.Contains("CheckNo")) row["CheckNo"] = docWord.CustomDocumentProperties["CheckNo"].ToString();
                            if (docWord.CustomDocumentProperties.Contains("Amount")) row["Amount"] = docWord.CustomDocumentProperties["Amount"].ToString();

                            #endregion
                        }

                        table.Rows.Add(row);
                        fi1 = null;
                    }

                    string[] varpaths = Directory.GetDirectories(tempPath);
                    DirectoryInfo info;

                    foreach (string item in varpaths)
                    {
                        info = new DirectoryInfo(item);
                        files = Directory.GetFiles(item);
                        if (files.Length > 0)
                        {
                            foreach (string subfile in files)
                            {
                                row = table.NewRow();
                                row["FileUID"] = Path.GetFileName(subfile);

                                string subfileName = subfile.Substring(tempPath.Length);
                                byte[] byteArray = System.Text.Encoding.Default.GetBytes(subfileName);
                                row["FileID"] = System.Convert.ToBase64String(byteArray);

                                byteArray = System.Text.Encoding.Default.GetBytes(id); //uid
                                row["UID"] = System.Convert.ToBase64String(byteArray); ;

                                row["FilePath"] = subfile;
                                fi1 = new FileInfo(subfile);
                                row["Size"] = ((double)fi1.Length) / (double)(1024 * 1024);//MB
                                row["DOB"] = fi1.CreationTime;

                                ext = Path.GetExtension(subfile);

                                if (ext.ToLower().Equals(".pdf"))
                                {
                                    #region PDF Document

                                    Aspose.Pdf.Document docPDF = new Aspose.Pdf.Document(subfile);
                                    var docInfo = new Aspose.Pdf.DocumentInfo(docPDF);

                                    if (docInfo.ContainsKey("CheckNo")) row["CheckNo"] = docInfo["CheckNo"].ToString();
                                    if (docInfo.ContainsKey("Amount")) row["Amount"] = docInfo["Amount"].ToString();

                                    #endregion
                                }
                                else if (ext.ToLower().Equals(".doc") || ext.ToLower().Equals(".docx"))
                                {
                                    #region docx Document
                                    Aspose.Words.Document docWord = new Aspose.Words.Document(subfile);

                                    if (docWord.CustomDocumentProperties.Contains("CheckNo")) row["CheckNo"] = docWord.CustomDocumentProperties["CheckNo"].ToString();
                                    if (docWord.CustomDocumentProperties.Contains("Amount")) row["Amount"] = docWord.CustomDocumentProperties["Amount"].ToString();

                                    #endregion
                                }

                                table.Rows.Add(row);
                                fi1 = null;
                            }
                        }

                    }
                }
                else
                {
                    Directory.CreateDirectory(tempPath);

                }
            }
            catch { }
        }

        DataView dv = table.DefaultView;
        dv.Sort = "DOB desc";
        table = dv.ToTable();

        return table;
    }

    protected DataTable GetWorkareaFiles(string uid, string keyword)
    {
        DataTable table = MakeDataTable();

        string path = Server.MapPath("~/ScanInBox");
        path = string.Concat(path, Path.DirectorySeparatorChar, uid);

        try
        {
            if (Directory.Exists(path))
            {
                string[] files = Directory.GetFiles(path);
                DataRow row = null;
                FileInfo fi1;
                string fileName = string.Empty;
                string ext = string.Empty;

                foreach (string file in files)
                {
                    fileName = Path.GetFileName(file);
                    if (fileName.IndexOf(keyword, StringComparison.CurrentCultureIgnoreCase) >= 0)
                    {
                        row = table.NewRow();
                        row["FileUID"] = Path.GetFileName(file);

                        byte[] byteArray = System.Text.Encoding.Default.GetBytes(Path.GetFileName(file));
                        row["FileID"] = System.Convert.ToBase64String(byteArray);

                        byteArray = System.Text.Encoding.Default.GetBytes(uid);
                        row["UID"] = System.Convert.ToBase64String(byteArray); ;

                        row["FilePath"] = file;
                        fi1 = new FileInfo(file);
                        row["Size"] = ((double)fi1.Length) / (double)(1024 * 1024);//MB
                        row["DOB"] = fi1.CreationTime;

                        ext = Path.GetExtension(file);

                        if (ext.ToLower().Equals(".pdf"))
                        {
                            #region PDF Document

                            Aspose.Pdf.Document docPDF = new Aspose.Pdf.Document(file);
                            var docInfo = new Aspose.Pdf.DocumentInfo(docPDF);

                            if (docInfo.ContainsKey("CheckNo")) row["CheckNo"] = docInfo["CheckNo"].ToString();
                            if (docInfo.ContainsKey("Amount")) row["Amount"] = docInfo["Amount"].ToString();

                            #endregion
                        }
                        else if (ext.ToLower().Equals(".doc") || ext.ToLower().Equals(".docx"))
                        {
                            #region docx Document
                            Aspose.Words.Document docWord = new Aspose.Words.Document(file);

                            if (docWord.CustomDocumentProperties.Contains("CheckNo")) row["CheckNo"] = docWord.CustomDocumentProperties["CheckNo"].ToString();
                            if (docWord.CustomDocumentProperties.Contains("Amount")) row["Amount"] = docWord.CustomDocumentProperties["Amount"].ToString();

                            #endregion
                        }
                        table.Rows.Add(row);
                        fi1 = null;
                    }


                }

                string[] varpaths = Directory.GetDirectories(path);
                DirectoryInfo info;

                foreach (string item in varpaths)
                {
                    files = Directory.GetFiles(item);
                    if (files.Length > 0)
                    {
                        double sizetotal = 0;
                        string details = string.Empty;
                        foreach (string subfile in files)
                        {
                            fileName = Path.GetFileName(subfile);
                            if (fileName.IndexOf(keyword, StringComparison.CurrentCultureIgnoreCase) >= 0)
                            {
                                row = table.NewRow();
                                row["FileUID"] = Path.GetFileName(subfile);

                                string subfileName = subfile.Substring(path.Length);
                                byte[] byteArray = System.Text.Encoding.Default.GetBytes(subfileName);
                                row["FileID"] = System.Convert.ToBase64String(byteArray);

                                byteArray = System.Text.Encoding.Default.GetBytes(uid); //uid
                                row["UID"] = System.Convert.ToBase64String(byteArray);
                                ;

                                row["FilePath"] = subfile;
                                fi1 = new FileInfo(subfile);
                                row["Size"] = ((double)fi1.Length) / (double)(1024 * 1024); //MB
                                row["DOB"] = fi1.CreationTime;

                                ext = Path.GetExtension(subfile);

                                if (ext.ToLower().Equals(".pdf"))
                                {
                                    #region PDF Document

                                    Aspose.Pdf.Document docPDF = new Aspose.Pdf.Document(subfile);
                                    var docInfo = new Aspose.Pdf.DocumentInfo(docPDF);

                                    if (docInfo.ContainsKey("CheckNo")) row["CheckNo"] = docInfo["CheckNo"].ToString();
                                    if (docInfo.ContainsKey("Amount")) row["Amount"] = docInfo["Amount"].ToString();

                                    #endregion
                                }
                                else if (ext.ToLower().Equals(".doc") || ext.ToLower().Equals(".docx"))
                                {
                                    #region docx Document
                                    Aspose.Words.Document docWord = new Aspose.Words.Document(subfile);

                                    if (docWord.CustomDocumentProperties.Contains("CheckNo")) row["CheckNo"] = docWord.CustomDocumentProperties["CheckNo"].ToString();
                                    if (docWord.CustomDocumentProperties.Contains("Amount")) row["Amount"] = docWord.CustomDocumentProperties["Amount"].ToString();

                                    #endregion
                                }

                                table.Rows.Add(row);
                                fi1 = null;
                            }
                        }
                    }

                }
            }
            else
            {
                Directory.CreateDirectory(path);

            }
        }
        catch { }

        DataView dv = table.DefaultView;
        dv.Sort = "DOB desc";
        table = dv.ToTable();
        return table;
    }

    private DataTable MakeDataTable()
    {
        // Create new DataTable.
        DataTable table = new DataTable();

        // Declare DataColumn and DataRow variables.
        DataColumn column;

        // Create new DataColumn, set DataType, ColumnName
        // and add to DataTable.    
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FileUID";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FileID";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "UID";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "FilePath";
        table.Columns.Add(column);


        column = new DataColumn();
        column.DataType = Type.GetType("System.Double");
        column.ColumnName = "Size";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.DateTime");
        column.ColumnName = "DOB";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.Boolean");
        column.ColumnName = "IsFolder";
        column.DefaultValue = false;
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "FolderItems";
        column.DefaultValue = "";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "CheckNo";
        column.DefaultValue = "";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "Amount";
        column.DefaultValue = "";

        table.Columns.Add(column);

        return table;
    }

    protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {
        Console.WriteLine(sender.ToString());

    }

    protected void btnConvert_Clicked(object sender, EventArgs e)
    {
        string filename = "";
        string filePath = "";
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        string path = Server.MapPath("~/ScanInBox");
        string fid = DropDownList1.SelectedValue;
        string tid = ListBox1.SelectedValue;

        string vpath = Server.MapPath("~/Volume");
        string pathname = string.Format("{0}\\{1}\\{2}", vpath, fid, tid);

        string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, fid, Path.DirectorySeparatorChar, tid);
        if (!Directory.Exists(dstpath))
        {
            Directory.CreateDirectory(dstpath);
        }

        path = string.Concat(path, Path.DirectorySeparatorChar, uid);

        bool existFile = false;
        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            if (((CheckBox)GridView1.Rows[i].FindControl("checkBox1")).Checked)
            {
                Console.WriteLine(sender.ToString());

                filename = ((DataControlFieldCell)GridView1.Rows[i].Cells[1]).Text.Trim();
                filePath = string.Concat(path, Path.DirectorySeparatorChar, filename);

                string encodeName = HttpUtility.UrlPathEncode(filename);
                encodeName = encodeName.Replace("%", "$");
                dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);
                if (File.Exists(filePath))
                {
                    //拷贝文件并转换格式

                    try
                    {
                        existFile = true;
                        File.Copy(filePath, dstpath, true);
                        ImageAdapter.ProcessPostFile(dstpath, fid, tid, uid, pathname, filePath);

                        File.Delete(filePath); //添加后删除原文
                    }
                    catch { }

                }

            }

            if (existFile)
            {
                Server.Transfer("~/Office/DocumentLoader.aspx?View=1");
            }
            else
            {
                lbsStatus.ImageUrl = "";
            }

        }

    }

    protected void ComboBox1_OnSelectedIndexChanged(object sender, EventArgs e)
    {

    }

    public void search_btn2_click(object sender, EventArgs e)
    {
        string prefxWord = textfield4.Text.Trim();

        GetData(prefxWord);
        BindGrid();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (this.Page.User.IsInRole("Users"))
            {
                LinkButton libDel = e.Row.FindControl("LinkButton2") as LinkButton;
                libDel.Visible = false;
            }

        }
    }

    protected void LinkButtonEdit_Click(object sender, CommandEventArgs e)
    {
        string pdfPath = e.CommandArgument.ToString();
        hidFullName.Value = pdfPath;

        string ext = Path.GetExtension(pdfPath);
        string fileName = Path.GetFileNameWithoutExtension(pdfPath);
        TextBoxFileName.Text = fileName;

        if (ext.ToLower().Equals(".pdf"))
        {
            #region PDF Document

            Aspose.Pdf.Document docPDF = new Aspose.Pdf.Document(pdfPath);
            var docInfo = new Aspose.Pdf.DocumentInfo(docPDF);

            if (docInfo.ContainsKey("FirstName")) TextBoxFirstName.Text = docInfo["FirstName"].ToString();
            if (docInfo.ContainsKey("LastName")) TextBoxLastName.Text = docInfo["LastName"].ToString();

            if (docInfo.ContainsKey("CompanyName")) TextBoxCompanyName.Text = docInfo["CompanyName"].ToString();
            if (docInfo.ContainsKey("CheckNo")) TextBoxCheckNo.Text = docInfo["CheckNo"].ToString();
            if (docInfo.ContainsKey("Amount")) TextBoxAmount.Text = docInfo["Amount"].ToString();

            #endregion
        }
        else if (ext.ToLower().Equals(".doc") || ext.ToLower().Equals(".docx"))
        {
            #region docx Document
            Aspose.Words.Document docWord = new Aspose.Words.Document(pdfPath);

            if (docWord.CustomDocumentProperties.Contains("FirstName")) TextBoxFirstName.Text = docWord.CustomDocumentProperties["FirstName"].ToString();
            if (docWord.CustomDocumentProperties.Contains("LastName")) TextBoxLastName.Text = docWord.CustomDocumentProperties["LastName"].ToString();
            if (docWord.CustomDocumentProperties.Contains("CompanyName")) TextBoxCompanyName.Text = docWord.CustomDocumentProperties["CompanyName"].ToString();
            if (docWord.CustomDocumentProperties.Contains("CheckNo")) TextBoxCheckNo.Text = docWord.CustomDocumentProperties["CheckNo"].ToString();
            if (docWord.CustomDocumentProperties.Contains("Amount")) TextBoxAmount.Text = docWord.CustomDocumentProperties["Amount"].ToString();

            #endregion
        }
        else
        {
            return;
        }

        updPnlEditDetail.Update();
        this.mdlPopup.Show();
    }

    protected void ButtonEditOk_Click(object sender, EventArgs e)
    {
        string fullPath = hidFullName.Value;

        string fileName = Path.GetFileName(fullPath);
        string ext = Path.GetExtension(fileName);

        var newPath = Path.GetDirectoryName(fullPath);
        var newFullName = Path.Combine(newPath, TextBoxFileName.Text.Trim() + ext);

        if (ext.ToLower().Equals(".pdf"))
        {
            #region PDF Document

            Aspose.Pdf.Document docPDF = new Aspose.Pdf.Document(fullPath);
            var docInfo = new Aspose.Pdf.DocumentInfo(docPDF);

            if (docInfo.ContainsKey("FirstName"))
            {
                docInfo["FirstName"] = TextBoxFirstName.Text.Trim();
            }
            else
            {
                docInfo.Add("FirstName", TextBoxFirstName.Text.Trim());
            }

            if (docInfo.ContainsKey("LastName"))
            {
                docInfo["LastName"] = TextBoxLastName.Text.Trim();
            }
            else
            {
                docInfo.Add("LastName", TextBoxLastName.Text.Trim());
            }

            if (docInfo.ContainsKey("CompanyName"))
            {
                docInfo["CompanyName"] = TextBoxCompanyName.Text.Trim();
            }
            else
            {
                docInfo.Add("CompanyName", TextBoxCompanyName.Text.Trim());
            }

            if (docInfo.ContainsKey("CheckNo"))
            {
                docInfo["CheckNo"] = TextBoxCheckNo.Text.Trim();
            }
            else
            {
                docInfo.Add("CheckNo", TextBoxCheckNo.Text.Trim());
            }

            if (docInfo.ContainsKey("Amount"))
            {
                docInfo["Amount"] = TextBoxAmount.Text.Trim();
            }
            else
            {
                docInfo.Add("Amount", TextBoxAmount.Text.Trim());
            }

            docInfo.ModDate = DateTime.Now;

            docPDF.Save(newFullName);

            #endregion
        }
        else if (ext.ToLower().Equals(".doc") || ext.ToLower().Equals(".docx"))
        {
            #region docx Document

            Aspose.Words.Document docWord = new Aspose.Words.Document(fullPath);

            if (docWord.CustomDocumentProperties.Contains("FirstName"))
            {
                docWord.CustomDocumentProperties["FirstName"].Value = TextBoxFirstName.Text.Trim();
            }
            else
            {
                docWord.CustomDocumentProperties.Add("FirstName", TextBoxFirstName.Text.Trim());
            }

            if (docWord.CustomDocumentProperties.Contains("LastName"))
            {
                docWord.CustomDocumentProperties["LastName"].Value = TextBoxLastName.Text.Trim();
            }
            else
            {
                docWord.CustomDocumentProperties.Add("LastName", TextBoxLastName.Text.Trim());
            }

            if (docWord.CustomDocumentProperties.Contains("CompanyName"))
            {
                docWord.CustomDocumentProperties["CompanyName"].Value = TextBoxCompanyName.Text.Trim();
            }
            else
            {
                docWord.CustomDocumentProperties.Add("CompanyName", TextBoxCompanyName.Text.Trim());
            }

            if (docWord.CustomDocumentProperties.Contains("CheckNo"))
            {
                docWord.CustomDocumentProperties["CheckNo"].Value = TextBoxCheckNo.Text.Trim();
            }
            else
            {
                docWord.CustomDocumentProperties.Add("CheckNo", TextBoxCheckNo.Text.Trim());
            }

            if (docWord.CustomDocumentProperties.Contains("Amount"))
            {
                docWord.CustomDocumentProperties["Amount"].Value = TextBoxAmount.Text.Trim();
            }
            else
            {
                docWord.CustomDocumentProperties.Add("Amount", TextBoxAmount.Text.Trim());
            }

            docWord.Save(newFullName);

            #endregion
        }
        else
        {
            this.mdlPopup.Hide();
            return;
        }

        try
        {
            FileInfo fileInfo = new FileInfo(fullPath);
            if (fileInfo.Exists && !fileName.Equals(Path.GetFileName(newFullName)))
            {
                fileInfo.Delete();
            }

            hidFullName.Value = newFullName;

        }
        catch (Exception)
        { }

        //列出所有的workarea文件

        string prefxWord = textfield4.Text.Trim();
        if (prefxWord.Length > 0)
            GetData(prefxWord);
        else
            GetData();

        BindGrid();

        updPnlEditDetail.Update();
        UpdatePanel1.Update();
        this.mdlPopup.Hide();


    }

    protected void ButtonEditCancel1_Click(object sender, EventArgs e)
    {
        this.mdlPopup.Hide();
    }
}
