<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    EnableSessionState="True" EnableViewState="true" CodeFile="DocumentLoader1.aspx.cs"
    Inherits="DocumentLoader1" Title="" ValidateRequest="false" %>

<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%--<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>--%>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Button ID="btnRedirect" runat="server" OnClick="btnRedirect_Click" Style="display: none;" />
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js"></script>

    <script language="javascript">
        $(function () {
            $('#<%= tagsXmlContent.ClientID %>').hide();
        });

        $(function () {
            var dWidth = window.screen.width;
            var dHeight = window.screen.height;
            var aWidth = Math.floor((80 * window.screen.width) / 100);
            var aHeight = Math.floor((57 * aWidth) / 100);

            $(".iframe").colorbox({
                iframe: true, width: "80%", height: "95%", onComplete: function () {
                    //iframe: true, width: aWidth, height: aHeight, onComplete: function () {
                    $('iframe').live('load', function () {
                        $('iframe').contents().find("head")
                          .append($("<style type='text/css'>  .splash{overflow:visible;}  </style>"));
                    });
                }
            });
        });

        function ScanDocFile() {
            $('.iframe').attr('href', 'UploadScanDoc.aspx?page=documentloader').trigger('click');
        }

        $('form').submit(function () {
            funEncodeTags();
        });
        function onClientUploadComplete(sender, e) {
            //onImageValidated("TRUE", e);
        }

        function onClientUploadComplete2(sender, e) {
            //onImageValidated("TRUE", e);
        }

        //$(document).ready(function () {
        //    $(".ajax__fileupload_dropzone").text("Drop files here");
        //});

        //function onChangeListBox1() {
        //    $(".ajax__fileupload_dropzone").text("Drop files here");
        //}

        function onImageValidated(arg, context) {

            var test = document.getElementById("testuploaded");
            test.style.display = 'block';

            var fileList = document.getElementById("fileList");
            var item = document.createElement('div');
            item.style.padding = '4px';

            item.appendChild(createFileInfo(context));

            fileList.appendChild(item);
        }

        function createFileInfo(e) {
            var holder = document.createElement('div');
            holder.appendChild(document.createTextNode(e.get_fileName() + ' with size ' + e.get_fileSize() + ' bytes'));

            return holder;
        }

        function onClientUploadStart(sender, e) {
            document.getElementById('uploadCompleteInfo').innerHTML = "please wait while the " + e.get_filesInQueue() + " file is uploaded and converted to eff...";
        }

        function onClientUploadStart2(sender, e) {
            document.getElementById('uploadCompleteInfo2').innerHTML = 'Please wait while uploading ' + e.get_filesInQueue() + ' files...';
        }

        function onClientUploadCompleteAll(sender, e) {
            var args = JSON.parse(e.get_serverArguments()),
               unit = args.duration > 60 ? 'minutes' : 'seconds',
               duration = (args.duration / (args.duration > 60 ? 60 : 1)).toFixed(2);

            var info = 'At <b>' + args.time + '</b> server time <b>'
                + e.get_filesUploaded() + '</b> of <b>' + e.get_filesInQueue()
                + '</b> files were uploaded with status code <b>"' + e.get_reason()
                + '"</b> in <b>' + duration + ' ' + unit + '</b>';

            document.getElementById('uploadCompleteInfo').innerHTML = info;


            // $('#<%=btnRedirect.ClientID%>').trigger('click');

            setTimeout(function () {
                var vd = '<%= ConfigurationManager.AppSettings["VirtualDir"] %>';
                window.location.reload();
            }, 2000);
        }

        function onClientUploadCompleteAll2(sender, e) {
            var args = JSON.parse(e.get_serverArguments()),
                unit = args.duration > 60 ? 'minutes' : 'seconds',
                duration = (args.duration / (args.duration > 60 ? 60 : 1)).toFixed(2);

            var info = 'At <b>' + args.time + '</b> server time <b>'
                + e.get_filesUploaded() + '</b> of <b>' + e.get_filesInQueue()
                + '</b> files were uploaded with status code <b>"' + e.get_reason()
                + '"</b> in <b>' + duration + ' ' + unit + '</b>';

            document.getElementById('uploadCompleteInfo2').innerHTML = info;
        }

        function setConversionText() {
            var obj = document.getElementById("<%= lbsStatus.ClientID %>");
                obj.setAttribute("src", "../Images/wait24trans.gif");
                obj.setAttribute("Visible", "true");
                funEncodeTags();
                //obj.innerHTML = " (Converting, Please Wait.)";
            }



    </script>

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Document Loader</h1>
            <p style="font-size: 20px;">
                In order to add documents and files to your EdFiles, please select the folder
                from the drop down menu below, then select the tab in which you would like to place
                the document in and then select the file/document you would like to place in that
                tab of the edFile. It is that simple. If you need assistance, please feel free
                to chat, email or call customer service.
            </p>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <%--<div class="content-title">Login Details</div>--%>
                <div class="left-content">
                    <div class="content-box listing-view">

                        <asp:Panel ID="panelMain" runat="server" Visible="true">
                            <%--<div class="tekstDef_upload">--%>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <fieldset>
                                        1. Select an edFile
                                        <div class='clearfix'></div>
                                        <%-- <editable:EditableDropDownList ID="EditableDropDownList1" runat="server" Sorted="true" Width="225px"
                                            OnOnClick="EditableDropDownList_OnClick" OnClick="javascript:funEncodeTags();">
                                        </editable:EditableDropDownList>--%>

                                        <asp:TextBox ID="FileFolderTextBoxId" runat="server" class="find-input" MaxLength="50" autocomplete="off" placeholder="Enter Folder Name" AutoPostBack="false" />

                                        <ajaxToolkit:AutoCompleteExtender
                                            runat="server" UseContextKey="true"
                                            BehaviorID="AutoCompleteEx2"
                                            ID="autoComplete1"
                                            TargetControlID="FileFolderTextBoxId" ServicePath="~/FlexWebService.asmx"
                                            ServiceMethod="GetCompletionFolderList"
                                            MinimumPrefixLength="1"
                                            CompletionInterval="1000"
                                            EnableCaching="true"
                                            CompletionSetCount="20"
                                            CompletionListCssClass="autocomplete_completionListElement"
                                            CompletionListItemCssClass="autocomplete_listItem"
                                            CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                            DelimiterCharacters="; :"
                                            ShowOnlyCurrentWordInCompletionListItem="true" OnClientItemSelected="autoCompleteEx_FolderItemSelected">

                                            <Animations>

                                                <OnShow>
                                                    <Sequence>
                                                        <%-- Make the completion list transparent and then show it --%>
                                                        <OpacityAction Opacity="0" />
                                                        <HideAction Visible="true" />
                            
                                                        <%--Cache the original size of the completion list the first time
                                                            the animation is played and then set it to zero --%>
                                                        <ScriptAction Script="
                                                            // Cache the size and setup the initial size
                                                            var behavior = $find('AutoCompleteEx2');
                                                            if (!behavior._height) {
                                                                var target = behavior.get_completionList();
                                                                behavior._height = target.offsetHeight - 2;
                                                                target.style.height = '0px';
                                                            }" />
                            
                                                        <%-- Expand from 0px to the appropriate size while fading in --%>
                                                        <Parallel Duration=".4">
                                                            <FadeIn />
                                                            <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx2')._height" />
                                                        </Parallel>
                                                    </Sequence>
                                                </OnShow>
                                                <OnHide>
                                                    <%-- Collapse down to 0pxc and fade out --%>
                                                    <Parallel Duration=".4">
                                                        <FadeOut />
                                                        <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx2')._height" EndValue="0" />
                                                    </Parallel>
                                                </OnHide>
                                            </Animations>
                                        </ajaxToolkit:AutoCompleteExtender>
                                        <asp:HiddenField ID="FolderID" runat="server" />
                                    </fieldset>
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                    <asp:HiddenField ID="HiddenField2" runat="server" />


                                    <fieldset>
                                        2. Select a tab for the selected edFile
                                        <div class='clearfix'></div>
                                        <asp:ListBox ID="ListBox1" Width="100%" Height="150" DataValueField="DividerID" DataTextField="Name"
                                            AutoPostBack="true" OnSelectedIndexChanged="ListBox1_OnSelectedIndexChanged"
                                            runat="server"></asp:ListBox>
                                    </fieldset>

                                    <fieldset>
                                        3. Click the Browse button to upload document
                                        <div class='clearfix'></div>
                                        <asp:Image runat="server" ID="Imagelabel12" ImageUrl="../Images/loader_bg.gif" ImageAlign="Middle" /><asp:Label
                                            ID="label12" runat="server"></asp:Label>
                                        <asp:TextBox ID="tagsXmlContent" runat="server" Visible="true" Value="<Tags><document><Id>0</Id><FileName>Name of File</FileName><TagKey></TagKey><TagValue></TagValue></document></Tags>" />
                                        <asp:Label runat="server" ID="myThrobber" Style="display: none;"><img align="absmiddle" alt="" src="uploading.gif"/></asp:Label>
                                        <ajaxToolkit:AjaxFileUpload ID="AjaxFileUpload1" runat="server" Padding-Bottom="4"
                                            Padding-Left="2" Padding-Right="1" Padding-Top="4" ThrobberID="myThrobber" OnClientUploadComplete="onClientUploadComplete"
                                            OnUploadComplete="AjaxFileUpload1_OnUploadComplete" MaximumNumberOfFiles="10"
                                            AllowedFileTypes="jpg,jpeg,pdf,doc,docx,xls" AzureContainerName="" OnClientUploadCompleteAll="onClientUploadCompleteAll"
                                            OnUploadCompleteAll="AjaxFileUpload1_UploadCompleteAll" OnUploadStart="AjaxFileUpload1_UploadStart"
                                            OnClientUploadStart="onClientUploadStart" ContextKeys="2" />
                                        <div class='clearfix'></div>
                                        <div id="uploadCompleteInfo">
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <input type="button" class="create-btn btn-small green" id="btnScandoc" name="btnScandoc" runat="server" causesvalidation="false" value="Scan Document" onclick="ScanDocFile();" />
                                    </fieldset>
                                    <%-- <fieldset>
                                    <a href="javascript:;" style="font-family: Trebuchet MS;" onclick="AddNewTag()">Add New Custom field</a>
                                    <div class='clearfix'></div>
                                    <div id="taggrid"></div>
                                </fieldset>--%>


                                    <div class="work-area">
                                        <%-- 4. Upload scans to Work Area, please <a href="./UploadWorkFile.aspx" style="color:blue;">click here</a>
                                        . Scans in the work are not assigned or filed in any folder.--%><asp:Image runat="server"
                                            ImageUrl="../Images/loader_bg.gif" ID="lbsStatus" ImageAlign="Middle" />
                                        <div class='clearfix'></div>
                                        <div class="prevnext" style="float: right" bgcolor="#f8f8f5">
                                            <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                                PageSize="5" CssClass="prevnext" PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>
                                            <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                        </div>
                                        <div class='clearfix'></div>
                                        <div class="overflow-auto">
                                            <asp:GridView ID="GridView1" runat="server" OnSorting="GridView1_Sorting" AllowSorting="True"
                                                AutoGenerateColumns="false" OnRowDataBound="GridView1_RowDataBound" CssClass="datatable listing-datatable">
                                                <PagerSettings Visible="False" />
                                                <AlternatingRowStyle CssClass="odd" />
                                                <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                <%--
                                            <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                <Columns>
                                                    <asp:TemplateField ShowHeader="true">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="Label8" runat="server" Text="Action" />
                                                        </HeaderTemplate>
                                                        <HeaderStyle Width="180px" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButtonEdit" runat="server" Text="" CommandArgument='<%# Eval("FilePath")%>'
                                                                OnCommand="LinkButtonEdit_Click" CssClass="ic-icon ic-edit" OnClientClick="javascript:funEncodeTags();"></asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton2" runat="server" Text="" CommandArgument='<%# Eval("FilePath")%>'
                                                                OnCommand="LinkButton2_Click" CssClass="ic-icon ic-delete"></asp:LinkButton>
                                                            <%--CommandArgument='<%#string.Format("{0},{1}",Eval("FilePath"),Eval("DocumentId")) %>'--%>
                                                            <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="LinkButton2"
                                                                PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                            </ajaxToolkit:ModalPopupExtender>
                                                            <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                ConfirmText="" TargetControlID="LinkButton2">
                                                            </ajaxToolkit:ConfirmButtonExtender>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%-- <asp:HyperLinkField DataTextField="FileName" HeaderText="FileName" SortExpression="FileName"
                                                        DataNavigateUrlFormatString="~/Office/SharePDF.aspx?id={0}&uid={1}" Target="_blank"
                                                        DataNavigateUrlFields="ScanInBoxId,UID">
                                                        <HeaderStyle CssClass="form_title" />--%>
                                                    <asp:HyperLinkField DataTextField="FileUID" HeaderText="FileName" SortExpression="FileUID"
                                                        DataNavigateUrlFormatString="~/Office/SharePDF.aspx?id={0}&uid={1}" Target="_blank"
                                                        DataNavigateUrlFields="FileID,UID">
                                                        <HeaderStyle CssClass="form_title" />
                                                    </asp:HyperLinkField>
                                                    <%--  <asp:BoundField DataField="Amount" HeaderText="Amount" HtmlEncode="false" />
                                                            <asp:BoundField DataField="CheckNo" HeaderText="Check/Wire No." HtmlEncode="false" />--%>
                                                    <asp:BoundField DataField="Size" HeaderText="Size(Mb)" DataFormatString="{0:F2}"
                                                        HtmlEncode="false" />
                                                    <asp:BoundField DataField="DOB" HeaderText="Date" DataFormatString="{0:MM.dd.yyyy}"
                                                        SortExpression="DOB" />
                                                    <%-- <asp:BoundField DataField="CreatedDate" HeaderText="Date" DataFormatString="{0:MM.dd.yyyy}"
                                                        SortExpression="CreatedDate" />--%>
                                                    <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="Label8" runat="server" Text="Add to Tab" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton3" runat="server" Text="Add" CommandArgument='<%# Eval("FilePath")%>'
                                                                OnCommand="LinkButton3_Click" OnClientClick="setConversionText();"></asp:LinkButton>
                                                            <ajaxToolkit:ConfirmButtonExtender ID="ButtonAdd" runat="server" ConfirmText="Are you sure you want to add this document to the selected tab?"
                                                                TargetControlID="LinkButton3">
                                                            </ajaxToolkit:ConfirmButtonExtender>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="Label9" runat="server" Text="Add to EFF" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton4" runat="server" Text="Create" CommandArgument='<%# Eval("FilePath")%>'
                                                                OnCommand="LinkButton4_Click" OnClientClick="setConversionText();"></asp:LinkButton>
                                                            <ajaxToolkit:ConfirmButtonExtender ID="ButtonAddEFF" runat="server" ConfirmText="Are you sure you want to add this document to new eff?"
                                                                TargetControlID="LinkButton4">
                                                            </ajaxToolkit:ConfirmButtonExtender>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <p>
                                                        <asp:Label ID="Label120" runat="server" />There are no files. right now.
                                                    </p>
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </div>

                                    </div>

                                    <fieldset>
                                        <div style="text-align: center; margin-top: 15px;">
                                            <%--margin: 90px 0 0 -683px !important--%>
                                            <asp:TextBox ID="textfield4" name="textfield4" runat="server"
                                                MaxLength="50" autocomplete="off" />
                                            <ajaxToolkit:AutoCompleteExtender runat="server" UseContextKey="true" BehaviorID="AutoCompleteEx1"
                                                ID="autoComplete2" TargetControlID="textfield4" ServicePath="~/FlexWebService.asmx"
                                                ServiceMethod="GetCompletionFileNameList" MinimumPrefixLength="1" CompletionInterval="1000"
                                                EnableCaching="true" CompletionSetCount="10" CompletionListCssClass="autocomplete_completionListElement"
                                                CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                DelimiterCharacters=";, :" ShowOnlyCurrentWordInCompletionListItem="true">
                                                <Animations>
                                                                <OnShow>
                                                                    <Sequence>
                                                                        <%-- Make the completion list transparent and then show it --%>
                                                                        <OpacityAction Opacity="0" />
                                                                        <HideAction Visible="true" />
                            
                                                                        <%--Cache the original size of the completion list the first time
                                                                            the animation is played and then set it to zero --%>
                                                                        <ScriptAction Script="
                                                                            // Cache the size and setup the initial size
                                                                            var behavior = $find('AutoCompleteEx1');
                                                                            if (!behavior._height) {
                                                                                var target = behavior.get_completionList();
                                                                                behavior._height = target.offsetHeight - 2;
                                                                                target.style.height = '0px';
                                                                            }" />
                            
                                                                        <%-- Expand from 0px to the appropriate size while fading in --%>
                                                                        <Parallel Duration=".4">
                                                                            <FadeIn />
                                                                            <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                                                                        </Parallel>
                                                                    </Sequence>
                                                                </OnShow>
                                                                <OnHide>
                                                                    <%-- Collapse down to 0pxc and fade out --%>
                                                                    <Parallel Duration=".4">
                                                                        <FadeOut />
                                                                        <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                                                                    </Parallel>
                                                                </OnHide>
                                                </Animations>
                                            </ajaxToolkit:AutoCompleteExtender>
                                            <asp:Button CssClass="create-btn btn-small green" ID="searchButton2" name="searchButton2" runat="server" CausesValidation="false" OnClick="search_btn2_click" Text="Search" />
                                        </div>
                                    </fieldset>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ListBox1" EventName="SelectedIndexChanged" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <%--</div>--%>
                        </asp:Panel>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Panel ID="panelConfirm" runat="server" Visible="false">
                                    <div class="form_title_1">
                                        Document was uploaded successfully. Next Step:
                                    </div>
                                    <div class="tekstDef" style="">
                                        <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                            <tr>
                                                <td align="left" class="podnaslov">
                                                    <asp:Label ID="lbConfirm" runat="server" />
                                                    Do you want to
                                                    <asp:HyperLink ID="btnUpload" runat="server" Text="Upload More Documents" NavigateUrl="~/Office/DocumentLoader.aspx"></asp:HyperLink>
                                                    or <a href="#" onclick="window.open('WebFlashViewer.aspx?ID=<%=FolderId %>', 'eBook', 'toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no'); return false;">View Your EdFile Now</a> ?
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="PanelInfor" runat="server" Visible="false">
                                    <div class="tekstDef" style="">
                                        <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                            <tr>
                                                <td align="left" class="podnaslov">
                                                    <asp:Label ID="Label1" runat="server" />There is no folders. please check the link
                                                    to
                                                    <asp:HyperLink ID="HyperLink1" runat="server" Text="Create New edFile" NavigateUrl="~/Office/NeweFileFolder.aspx"></asp:HyperLink>
                                                    right now.
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="right-content">
                    <%-- <div class="quick-find">
                        <uc1:QuickFind ID="QuickFind1" runat="server" />
                    </div>--%>
                    <div class="quick-find" style="margin-top: 15px;">
                        <div class="find-inputbox">
                            <marquee scrollamount="1" id="marqueeside" runat="server"
                                behavior="scroll" onmouseover="this.stop()" onmouseout="this.start()" scrolldelay="20"
                                direction="up">
                                <%=SideMessage %></marquee>
                            <%--<uc2:ReminderControl ID="ReminderControl1" runat="server" />--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:Panel class="popup-mainbox" ID="DivDeleteConfirmation" Style="display: none"
        runat="server">
        <div>
            <div class="popup-head" id="PopupHeader">
                Delete File
            </div>
            <div class="popup-scroller">
                <p>
                    Caution! Are you sure you want to delete this file?
                </p>
                <p>
                    Confirm Password:
                                        <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                </p>
            </div>
            <div class="popup-btn">
                <input id="ButtonDeleleOkay" type="button" value="Yes" class="btn green" />
                <input id="ButtonDeleteCancel" type="button" value="No" class="btn green" />
            </div>
        </div>
    </asp:Panel>
    <asp:Button Style="display: none" ID="btnShowPopup" runat="server"></asp:Button>
    <ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="ButtonEditCancel1" OkControlID="ButtonEditOk" PopupControlID="DivEditConfirmation"
        TargetControlID="btnShowPopup">
    </ajaxToolkit:ModalPopupExtender>

    <asp:Panel class="popup-mainbox" ID="DivEditConfirmation" Style="display: none"
        runat="server">
        <asp:UpdatePanel ID="updPnlEditDetail" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div>
                    <div class="popup-head" id="PopupHeaderEdit">
                        Edit File
                    </div>
                    <div class="popup-scroller">
                        <table class="details-datatable" width="100%">
                            <tr>
                                <td id="lblFileName" runat="server">File Name
                                </td>
                                <td>
                                    <asp:HiddenField runat="server" ID="hidFullName" />
                                    <asp:HiddenField runat="server" ID="hidWorkareaId" />
                                    <asp:TextBox ID="TextBoxFileName" runat="server" MaxLength="20" />
                                </td>
                            </tr>
                            <tr>
                                <td id="lblFirstName" runat="server">First Name
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxFirstName" runat="server" MaxLength="20" />
                                </td>
                            </tr>
                            <tr>
                                <td id="lblLastName" runat="server">Last Name
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxLastName" runat="server" MaxLength="20" />
                                </td>
                            </tr>
                            <tr>
                                <td id="lblCompanyName" runat="server">Company Name
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxCompanyName" runat="server" MaxLength="20" />
                                </td>
                            </tr>
                            <tr>
                                <td id="lblAmount" runat="server">Amount
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxAmount" runat="server" MaxLength="20" />
                                </td>
                            </tr>
                            <tr>
                                <td id="lblCheckNo" runat="server">Check/Wire No.
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBoxCheckNo" runat="server" MaxLength="20" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="popup-btn">
                        <asp:Button ID="ButtonEditOk" runat="server" OnClick="ButtonEditOk_Click" Text="Save" CssClass="btn green" />
                        <asp:Button ID="ButtonEditCancel1" runat="server" OnClick="ButtonEditCancel1_Click"
                            Text="Cancel" CssClass="btn green" />
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonEditOk" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEditCancel1" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>

    <%--Forimageandxls--%>
    <asp:Button Style="display: none" ID="btnforimageandxls" runat="server"></asp:Button>
     <ajaxToolkit:ModalPopupExtender ID="mdlPopupforimageandxls" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="ButtonEditCancel1" OkControlID="ButtonEditOk" PopupControlID="DivEditConfirmationforimageandxls"
        TargetControlID="btnforimageandxls">
    </ajaxToolkit:ModalPopupExtender>

    <asp:Panel class="popup-mainbox" ID="DivEditConfirmationforimageandxls" Style="display: none"
        runat="server">
        <asp:UpdatePanel ID="UpdatePanelforimageandxls" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div>
                    <div class="popup-head" id="Div1">
                        Edit File
                    </div>
                    <div class="popup-scroller">
                        <table class="details-datatable" width="100%">
                            <tr>
                                <td id="Td1" runat="server">File Name
                                </td>
                                <td>
                                    <asp:HiddenField runat="server" ID="hdnforimageandxlspath" />
                                    <asp:HiddenField runat="server" ID="hdnforimageandxlsWorkareaId" />
                                    <asp:TextBox ID="TextBoxforimageandxls" runat="server" MaxLength="20" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="popup-btn">
                        <asp:Button ID="Button1" runat="server" OnClick="ButtonEditOk_Click" Text="Save" CssClass="btn green" />
                        <asp:Button ID="Button2" runat="server" OnClick="ButtonEditCancel1_Click"
                            Text="Cancel" CssClass="btn green" />
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="ButtonEditOk" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="ButtonEditCancel1" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>


   
    <script type="text/javascript">

        var hidTagsXml = document.getElementById("<%=tagsXmlContent.ClientID%>");

        var TemplateXml = hidTagsXml.value; //"<Tags><document><Id>0</Id><FileName>Name of File</FileName><TagKey>TagKey</TagKey><TagValue>SomeName</TagValue></document></Tags>";

        function getDocument() {
            var xmlDoc = null;
            // TemplateXml = hidTagsXml.value;

            if (window.DOMParser) {
                parser = new DOMParser();
                xmlDoc = parser.parseFromString(TemplateXml, "text/xml");
            }
            else {
                xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async = false;
                xmlDoc.loadXML(TemplateXml);
            }
            return xmlDoc;
        }


        function GenerateGrid() {
            var xmlDoc = getDocument();
            //var strHTML = '<table id="tbltags" style="border-width:2px;border-style:solid;width:100%;border-collapse:collapse;"><tr class="form_title" style="background-color:#DBD9CF;text-align:left;" ><th style="display:none;">File Name</th><th>Custom Field Label</th><th>Custom Field Value</th><th>Action</th></tr>';
            var strHTML = '<table id="tbltags" class="datatable listing-datatable">' +
                '<tr>' +
                    '<th style="display:none;">File Name</th>' +
                    '<th>Custom Field Label</th>' +
                    '<th>Custom Field Value</th>' +
                    '<th style="width:110px;">Action</th>' +
                '</tr>';
            if (xmlDoc != null) {
                var ListOfElements = xmlDoc.getElementsByTagName('document');
                if (ListOfElements != null) {
                    for (var i = 0; i < ListOfElements.length; i++) {
                        strHTML += '<tr id="TRE-' + ListOfElements[i].childNodes[0].innerHTML + '"><td style="display:none;">' + ListOfElements[i].childNodes[1].innerHTML + '</td><td>' + ListOfElements[i].childNodes[2].innerHTML + '</td><td>' + ListOfElements[i].childNodes[3].innerHTML + '</td><td><a onclick="Edit(\'TRE-' + ListOfElements[i].childNodes[0].innerHTML + '\')" href="javascript:;" class="ic-icon ic-edit">Edit</a>&nbsp;<a href="javascript:;" onclick="RemoveTag(\'TRE-' + ListOfElements[i].childNodes[0].innerHTML + '\')" class="ic-icon ic-delete">Delete</a></td></tr>';
                    }
                }
            }
            strHTML += '</table>';
            if (document.getElementById('taggrid')) {
                document.getElementById('taggrid').innerHTML = strHTML;
            }
        }

        function Edit(id) {
            try {
                id = id.split('-')[1];
                id = parseInt(id) + 1;

                if (id != null) {
                    var fileFileName = "";
                    var TagKey = "";
                    var TagValue = "";

                    var action = document.getElementById("tbltags").rows[id].cells[3].firstChild.innerHTML;
                    if (action == "Edit") {

                        fileFileName = document.getElementById("tbltags").rows[id].cells[0].innerHTML;
                        TagKey = document.getElementById("tbltags").rows[id].cells[1].innerHTML;
                        TagValue = document.getElementById("tbltags").rows[id].cells[2].innerHTML;

                        var tagKeyElement = '<input type="text" id="editTheTagKey" value="' + TagKey + '" >';
                        var tagValueElement = '<input type="text" id="editTheTagValue" value="' + TagValue + '" >';

                        document.getElementById("tbltags").rows[id].cells[1].innerHTML = tagKeyElement;
                        document.getElementById("tbltags").rows[id].cells[2].innerHTML = tagValueElement;
                        document.getElementById("tbltags").rows[id].cells[3].firstChild.innerHTML = "Update";

                    }
                    else {
                        fileFileName = document.getElementById("tbltags").rows[id].cells[0].innerHTML;
                        TagKey = document.getElementById("editTheTagKey").value;
                        TagValue = document.getElementById("editTheTagValue").value;

                        document.getElementById("tbltags").rows[id].cells[1].innerHTML = TagKey;
                        document.getElementById("tbltags").rows[id].cells[2].innerHTML = TagValue;
                        document.getElementById("tbltags").rows[id].cells[3].firstChild.innerHTML = "Edit";
                        UpdateXmlDocumentForTags(id, fileFileName, TagKey, TagValue);
                    }
                }
            }
            catch (ex) {
                //  alert(ex.message);
            }
        }

        function UpdateXmlDocumentForTags(id, fileName, tagKey, tagValue) {
            id = parseInt(id) - 1;
            var xmlDoc = getDocument();
            if (xmlDoc != null) {
                var ListOfElements = xmlDoc.getElementsByTagName('document');
                if (ListOfElements != null) {
                    for (var i = 0; i < ListOfElements.length; i++) {
                        if (i == id) {
                            ListOfElements[i].childNodes[1].innerHTML = fileName;
                            ListOfElements[i].childNodes[2].innerHTML = tagKey;
                            ListOfElements[i].childNodes[3].innerHTML = tagValue;
                        }
                    }
                }
                var xmlString = (new XMLSerializer()).serializeToString(xmlDoc);
                hidTagsXml.value = xmlString;
                TemplateXml = xmlString;
            }
        }

        function AddNewTag() {
            var rows = document.getElementById("tbltags").rows;
            var id = rows.length - 1;
            var xmlDoc = getDocument();
            if (xmlDoc != null) {
                var documentNode = xmlDoc.createElement('document');
                var InnerNode = '<Id>' + id.toString() + '</Id><FileName></FileName><TagKey></TagKey><TagValue></TagValue>';
                documentNode.innerHTML = InnerNode;
                var MasterTags = xmlDoc.getElementsByTagName('Tags');
                if (MasterTags != null) {
                    MasterTags[0].appendChild(documentNode);
                    var xmlString = (new XMLSerializer()).serializeToString(xmlDoc);
                    TemplateXml = xmlString;
                    GenerateGrid();
                    Edit('TRE-' + id.toString());

                }
            }
        }

        function RemoveTag(id) {
            id = id.split('-')[1];
            //id = parseInt(id) + 1;            
            var xmlDoc = getDocument();
            if (xmlDoc != null) {
                var ListOfElements = xmlDoc.getElementsByTagName('document');
                if (ListOfElements != null) {
                    for (var i = 0; i < ListOfElements.length; i++) {
                        if (i == parseInt(id)) {
                            xmlDoc.documentElement.removeChild(ListOfElements[i]);
                            break;
                        }
                    }
                }
                var xmlString = (new XMLSerializer()).serializeToString(xmlDoc);
                hidTagsXml.value = xmlString;
                TemplateXml = xmlString;
                GenerateGrid();
            }
        }

        GenerateGrid();
        $(function () {
            $('#ctl00_ContentPlaceHolder1_EditableDropDownList1_list_button').click(function () {
                funEncodeTags();
            });
        });

        function funEncodeTags() {
            var mytext = $('#<%= tagsXmlContent.ClientID %>').val();
                var encode = $('<div />').text(mytext).html();
                $('#<%= tagsXmlContent.ClientID %>').val(encode);
            }
        function autoCompleteEx_FolderItemSelected(sender, e) {
            funEncodeTags();
                var folderId = $get('<%= FolderID.ClientID %>');
                folderId.value = e.get_value();
                __doPostBack(sender.get_element().name, 'RefreshTabs');
            }

    </script>
    <a class='iframe' href="javascript:;" style="display: none;">popup</a>
</asp:Content>
