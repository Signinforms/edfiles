﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AjaxControlToolkit;
using Aspose.Pdf;
using com.flajaxian;
using Shinetech.DAL;
using Shinetech.Engines.Adapters;
using Shinetech.Framework.Controls;

using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using PdfSharp.Pdf.Security;
using Shinetech.Utility;
using Shinetech.Engines;
using System.Xml;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using DataQuicker2.Framework;
using System.Collections.Generic;

public partial class DocumentLoader1 : StoragePage
{
    public string FolderId;
    string WorkareaID;

    //卢远宗 2015-11-02
    public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    public string virtualDir = ConfigurationManager.AppSettings["VirtualDir"];
    public string newFileName = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            AjaxFileUpload1.Enabled = true;
            autoComplete2.ContextKey = Sessions.SwitchedSessionId;
            //autoComplete2.ContextKey = Membership.GetUser().ProviderUserKey.ToString();
            autoComplete1.ContextKey = Sessions.SwitchedSessionId;
            //autoComplete1.ContextKey = Membership.GetUser().ProviderUserKey.ToString();
            DataTable dtFolders = GetEFileFolders();
            if (dtFolders.Rows.Count == 0)
            {
                PanelInfor.Visible = true;
                panelMain.Visible = false;
                panelConfirm.Visible = false;
            }
            else
            {
                if (!AjaxFileUpload1.IsInFileUploadPostBack)
                {
                    BindEFileFolders();
                    DataBindDividers();
                    //列出所有的workarea文件
                    GetData();
                    BindGrid();
                }
            }
        }
        else
        {
            if (AjaxFileUpload1.IsInFileUploadPostBack)
            {
                // do for ajax file upload partial postback request
            }
            if (Request.Params.Get("__EVENTARGUMENT") != null && Convert.ToString(Request.Params.Get("__EVENTARGUMENT")) == "RefreshTabs")
                DataBindDividers();
            //if (AjaxFileUpload2.IsInFileUploadPostBack)
            //{
            // do for ajax file upload partial postback request
            //}

        }

    }

    public string SideMessage
    {
        get
        {
            return this.Session["_Side_Message"] as string;
        }

        set
        {
            this.Session["_Side_Message"] = value;
        }
    }

    public ArrayList FilesuploadedRecently
    {
        get
        {
            if (this.Session["FilesuploadedRecently"] != null)
            {
                return this.Session["FilesuploadedRecently"] as ArrayList;
            }
            else
            {
                this.Session["FilesuploadedRecently"] = new ArrayList();
                return this.Session["FilesuploadedRecently"] as ArrayList;
            }

        }
        set
        {
            this.Session["FilesuploadedRecently"] = value;
        }
    }

    protected void AjaxFileUpload1_OnUploadComplete(object sender, AjaxFileUploadEventArgs file)
    {
        try
        {
            //byte[] fullfile = ProcessFile(AjaxFileUpload1, file);
            //ProcessFile(file, fullfile);            

            //if (Convert.ToInt32(this.DocumentID) > 0)
            //{
            //    Common_Tatva.AuditLogByDocId(Convert.ToInt32(this.DocumentID), Membership.GetUser().ProviderUserKey.ToString(), Enum_Tatva.Action.Create.GetHashCode());
            //}

            string path = Server.MapPath("~/ScanInBox");
            string uid = Sessions.SwitchedSessionId;
            //string uid = Membership.GetUser().ProviderUserKey.ToString();

            string fullPath = Path.Combine(path, uid);
            decimal size;
            size = file.FileSize / (1024 * 1024);
            //string fullPath = HttpContext.Current.Server.MapPath(Path);
            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath);
            }

            if (AjaxFileUpload1.IsInFileUploadPostBack)
            {
                var fullname = Path.Combine(fullPath, file.FileName);
                AjaxFileUpload1.SaveAs(fullname);
            }
            //string fileName = Path.GetFileNameWithoutExtension(file.FileName);
            string fileName = file.FileName;
            string pathName = file.FileName;
            // General_Class.DocumentsInsertForWorkArea(fileName, uid, size, Enum_Tatva.IsDelete.No.GetHashCode(), pathName);
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void AjaxFileUpload2_OnUploadComplete(object sender, AjaxFileUploadEventArgs file)
    {
        try
        {
            ProcessFileForWorkarea(AjaxFileUpload1, file);

        }
        catch (Exception)
        {

            throw;
        }


    }

    public string FolderName
    {
        get
        {
            return string.Format("~/{0}", CurrentVolume);
        }

    }

    public string WorkAreaName
    {
        get
        {
            return "~/ScanInBox";
        }

    }

    public void ProcessFileForWorkarea(AjaxFileUpload AjaxFileUpload1, AjaxFileUploadEventArgs file)
    {

        if (!String.IsNullOrEmpty(this.WorkAreaName))
        {
            string basePath = this.MapPath(this.WorkAreaName);

            string path = basePath + Sessions.SwitchedSessionId;
            //string path = basePath + Membership.GetUser().ProviderUserKey.ToString();

            string encodeName = HttpUtility.UrlPathEncode(file.FileName);
            encodeName = encodeName.Replace("%", "$");

            string fileName = String.Concat(
                    path,
                    Path.DirectorySeparatorChar,
                    encodeName
                );
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            AjaxFileUpload1.SaveAs(fileName);
        }
    }

    public byte[] ProcessFile(AjaxFileUpload AjaxFileUpload1, AjaxFileUploadEventArgs file)
    {

        if (!String.IsNullOrEmpty(this.FolderName))
        {
            string basePath = this.MapPath(this.FolderName);

            string folderID = this.EffID;
            string dividerID = this.DividerID;
            string path = basePath + Path.DirectorySeparatorChar +
                        folderID + Path.DirectorySeparatorChar + dividerID;

            string encodeName = HttpUtility.UrlPathEncode(file.FileName);
            encodeName = encodeName.Replace("%", "$");

            string fileName = String.Concat(
                    path,
                    Path.DirectorySeparatorChar,
                    encodeName
                );
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            byte[] s = file.GetContents();

            fileName = String.Concat(path, Path.DirectorySeparatorChar, Common_Tatva.ChangeExtension(fileName));

            fileName = Common_Tatva.GetRenameFileName(fileName);

            newFileName = Path.GetFileName(fileName);
            newFileName = HttpUtility.UrlDecode(newFileName.Replace('$', '%').ToString());

            AjaxFileUpload1.SaveAs(fileName);

            return s;
        }

        return null;
    }

    protected static StreamType getFileExtention(string ext)
    {
        StreamType streamType = StreamType.PDF;

        switch (ext.ToLower())
        {
            case ".pdf":
                streamType = StreamType.PDF;
                break;
            case ".docx":
            case ".doc":
                streamType = StreamType.Word;
                break;
            case ".xls":
                streamType = StreamType.Excel;
                break;
            case ".ppt":
                streamType = StreamType.PPT;
                break;
            case ".rtf":
                streamType = StreamType.RTF;
                break;
            case ".odt":
                streamType = StreamType.ODT;
                break;
            case ".ods":
                streamType = StreamType.ODS;
                break;
            case ".odp":
                streamType = StreamType.ODP;
                break;
            case ".jpeg":
            case ".jpg":
                streamType = StreamType.JPG;
                break;
            case ".png":
                streamType = StreamType.PNG;
                break;
            default:
                break;
        }

        return streamType;
    }

    public string PathUrl
    {
        get { return CurrentVolume; }
    }

    //要求返还错误信息
    public void ProcessFile(AjaxFileUploadEventArgs file, byte[] fileStream)
    {
        WorkItem item = new WorkItem();
        item.FolderID = Convert.ToInt32(this.EffID);
        item.DividerID = Convert.ToInt32(this.DividerID);
        string basePath = this.MapPath(this.FolderName);

        item.Path = this.PathUrl + Path.DirectorySeparatorChar +
                    item.FolderID + Path.DirectorySeparatorChar + Convert.ToInt32(this.DividerID);
        item.FileName = Path.GetFileNameWithoutExtension(newFileName);
        item.FullFileName = Path.GetFileName(newFileName);
        item.UID = Sessions.SwitchedSessionId;
        //item.UID = Membership.GetUser().ProviderUserKey.ToString();
        item.contentType = getFileExtention(Path.GetExtension(newFileName));
        item.Content = new MemoryStream(fileStream);
        int order = FlashViewer.GetDocumentOrder(Convert.ToInt32(this.DividerID));


        item.MachinePath = string.Concat(basePath, Path.DirectorySeparatorChar, item.FolderID,
        Path.DirectorySeparatorChar, item.DividerID);

        if (!Directory.Exists(item.MachinePath))
        {
            Directory.CreateDirectory(item.MachinePath);
        }
        try
        {
            float size = fileStream.Length / (1024 * 1024); // IN MB.
            string encodeName = HttpUtility.UrlPathEncode(item.FullFileName);
            encodeName = encodeName.Replace("%", "$");
            string source = string.Concat(item.MachinePath, Path.DirectorySeparatorChar, encodeName);
            var pageCount = 1;
            if (item.contentType == StreamType.JPG)
            {
                Jpeg2Engine jpeg2Engine = new Jpeg2Engine(item);
                jpeg2Engine.generatePDFfromImage(source, source.Split('.')[0] + ".pdf");
            }
            else if (item.contentType == StreamType.PDF)
            {
                iTextSharp.text.pdf.PdfReader pdfReader = new iTextSharp.text.pdf.PdfReader(source);
                pageCount = pdfReader.NumberOfPages;
            }
            this.DocumentID = General_Class.DocumentsInsert(item.FileName, item.FolderID, item.UID, item.Path, item.DividerID, pageCount, size, item.FileName, (int)item.contentType, order + 1, Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()));
            //Common_Tatva.DocumentsInsert(item, pageCount, size, (int)item.contentType, order + 1, Convert.ToString(Enum_Tatva.Class.Class1.GetHashCode()));
            this.DocumentName = item.FileName;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(Convert.ToString(this.EffID), Convert.ToString(this.DividerID), newFileName, ex);
            //General_Class.WriteErrorLog(Convert.ToString(this.EffID), Convert.ToString(this.DividerID), newFileName, ex);
        }

        //WorkItem item = new WorkItem();
        //item.FolderID = Convert.ToInt32(this.EffID);
        //item.DividerID = Convert.ToInt32(this.DividerID);
        //string basePath = this.MapPath(this.FolderName);

        //item.Path = this.PathUrl + Path.DirectorySeparatorChar +
        //            item.FolderID + Path.DirectorySeparatorChar + item.DividerID;

        //item.MachinePath = string.Concat(basePath, Path.DirectorySeparatorChar, item.FolderID,
        //Path.DirectorySeparatorChar, item.DividerID);

        //if (!Directory.Exists(item.MachinePath))
        //{
        //    Directory.CreateDirectory(item.MachinePath);
        //}

        //item.FileName = Path.GetFileNameWithoutExtension(newFileName);
        //item.FullFileName = Path.GetFileName(newFileName);
        //item.UID = Membership.GetUser().ProviderUserKey.ToString();
        //item.contentType = getFileExtention(Path.GetExtension(newFileName));
        //item.Content = new MemoryStream(fileStream);
        //try
        //{
        //    IConvert task = EngineProvider.GetEngine(item);
        //    PdfDesc result = task.Execute();
        //    int order = FlashViewer.GetDocumentOrder(Convert.ToInt32(this.DividerID));
        //    result.DocumentOrder = order + 1;
        //    if (result != null)
        //    {
        //        task.callbackExecute(result);
        //        this.DocumentID = Convert.ToString(result.DocumentID);
        //        this.DocumentName = result.Name;
        //    }
        //}
        //catch (Exception ex)
        //{
        //    Common_Tatva.WriteErrorLog(Convert.ToString(this.EffID), Convert.ToString(this.DividerID), newFileName, ex);
        //    throw new ApplicationException(ex.Message);
        //}
    }

    protected void AjaxFileUpload1_UploadCompleteAll(object sender, AjaxFileUploadCompleteAllEventArgs e)
    {
        var startedAt = (DateTime)Session["uploadTime"];
        var now = DateTime.Now;
        e.ServerArguments = new JavaScriptSerializer()
            .Serialize(new
            {
                duration = (now - startedAt).Seconds,
                time = DateTime.Now.ToShortTimeString()
            });

    }

    protected void AjaxFileUpload2_UploadCompleteAll(object sender, AjaxFileUploadCompleteAllEventArgs e)
    {
        var startedAt = (DateTime)Session["uploadTime2"];
        var now = DateTime.Now;
        e.ServerArguments = new JavaScriptSerializer()
            .Serialize(new
            {
                duration = (now - startedAt).Seconds,
                time = DateTime.Now.ToShortTimeString()
            });
    }

    protected void AjaxFileUpload1_UploadStart(object sender, AjaxFileUploadStartEventArgs e)
    {
        var now = DateTime.Now;
        e.ServerArguments = now.ToShortTimeString();
        Session["uploadTime"] = now;
    }

    protected void AjaxFileUpload2_UploadStart(object sender, AjaxFileUploadStartEventArgs e)
    {
        var now = DateTime.Now;
        e.ServerArguments = now.ToShortTimeString();
        Session["uploadTime2"] = now;
    }

    private void BindEFileFolders()
    {
        DataTable table = GetEFileFolders();

        if (!string.IsNullOrEmpty(EffID))
        {
            string folderId = EffID;
            FolderID.Value = folderId;
        }
        else
        {
            FolderID.Value = "0";

        }

        if (!string.IsNullOrEmpty(Page.Request.QueryString["FolderId"]))
        {
            string folderId = Page.Request.QueryString["FolderId"];
            FolderID.Value = folderId;
        }
    }

    private void GetData()
    {
        //Note:- GetData From Database
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        //string searchkey = string.Empty;
        //this.DataSource = General_Class.GetWorkAreaDocuments(uid, searchkey);
        //this.DataSource.Columns.Add("FilePath", typeof(string));
        //if (this.DataSource != null && this.DataSource.Rows.Count > 0)
        //{
        //    foreach (DataRow row in this.DataSource.Rows)
        //    {
        //        string path = Server.MapPath("~/" + Common_Tatva.ScanInBox);
        //        string tempPath = string.Concat(path, Path.DirectorySeparatorChar, uid);
        //        if (Directory.Exists(tempPath))
        //        {
        //            string[] files = Directory.GetFiles(tempPath);
        //            foreach (string file in files)
        //            {
        //                if (file.Contains(row["FileName"].ToString()))
        //                    row["FilePath"] = file;
        //            }
        //        }
        //    }
        //}
        //DataView dvSource = this.DataSource.AsDataView();
        //dvSource.Sort = "WorkareaId DESC";
        //this.DataSource = dvSource.ToTable(); 

        //Note:-GetData From Folder 
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        DataTable table = GetWorkareaFiles(uid);
        if (table != null)
        {
            this.DataSource = table;
        }
    }

    private void GetDataFoeSearch(string prefxWord)
    {
        //Note:- GetDataFoeSearch From DataBase
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        //this.DataSource = General_Class.GetWorkAreaDocuments(uid, prefxWord);
        //this.DataSource.Columns.Add("FilePath", typeof(string));
        //if (this.DataSource != null && this.DataSource.Rows.Count > 0)
        //{
        //    foreach (DataRow row in this.DataSource.Rows)
        //    {
        //        string path = Server.MapPath("~/" + Common_Tatva.ScanInBox);
        //        string tempPath = string.Concat(path, Path.DirectorySeparatorChar, uid);
        //        if (Directory.Exists(tempPath))
        //        {
        //            string[] files = Directory.GetFiles(tempPath);
        //            foreach (string file in files)
        //            {
        //                if (file.Contains(row["FileName"].ToString()))
        //                    row["FilePath"] = file;
        //            }
        //        }
        //    }
        //}
        //DataView dvSource = this.DataSource.AsDataView();
        //dvSource.Sort = "WorkareaId,FileName DESC";
        //this.DataSource = dvSource.ToTable();

        //Not:- GetDataFoeSearch From Folder
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        DataTable table = GetWorkareaFiles(uid, prefxWord);
        if (table != null)
        {
            this.DataSource = table;
        }
    }

    //private void GetData(string keyword)
    //{
    //    string uid = Membership.GetUser().ProviderUserKey.ToString();
    //    DataTable table = GetWorkareaFiles(uid, keyword);
    //    if (table != null)
    //    {
    //        this.DataSource = table;
    //    }
    //}

    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
        UpdatePanel1.Update();
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["Files_Workarea"] as DataTable;
        }
        set
        {
            this.Session["Files_Workarea"] = value;
        }
    }


    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }


    public DataTable GetEFileFolders()
    {
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        Shinetech.DAL.Account account = new Shinetech.DAL.Account(uid);
        string officeId;
        DataTable effTable = null;
        if (account.IsExist)
        {
            if (account.IsSubUser.Value.Equals("1"))
            {
                officeId = account.OfficeUID.ToString();
            }
            else
            {
                officeId = uid;
            }

            effTable = FileFolderManagement.GetFileFoldersWithDisplayName(uid, officeId);
        }

        return effTable;
    }

    public DataTable GetDividersByEFF(int eff)
    {
        DataTable dividerTable = FileFolderManagement.GetDividers(eff);
        return dividerTable;
    }


    protected void ComboBoxFolder_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataBindDividers();
    }


    public void DataBindDividers()
    {
        if (FileFolderTextBoxId.Text.Trim() == "")
        {
            // AjaxFileUpload1.Enabled = (ListBox1.Items.Count != 0);
            return;
        }
        int effid = Convert.ToInt32(FolderID.Value);
        this.EffID = FolderID.Value;

        ListBox1.DataSource = GetDividersByEFF(effid).DefaultView;
        ListBox1.DataBind();

        if (ListBox1.Items.Count > 0) ListBox1.SelectedIndex = 0;
        //  AjaxFileUpload1.Enabled = (ListBox1.Items.Count != 0);
        this.DividerID = ListBox1.SelectedValue;
    }

    protected void ListBox1_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        this.DividerID = ListBox1.SelectedValue;
    }

    public string DividerID
    {
        get
        {
            return (string)this.Session["DividerID"];
        }
        set
        {
            Session["DividerID"] = value;
        }
    }

    public string EffID
    {
        get
        {
            return (string)Session["EFFID"];
        }
        set
        {
            Session["EFFID"] = value;

        }
    }

    public string DocumentID
    {
        get
        {
            return (string)Session["DocumentID"];
        }
        set
        {
            Session["DocumentID"] = value;

        }
    }

    public string DocumentName
    {
        get
        {
            return (string)Session["DocumentName"];
        }
        set
        {
            Session["DocumentName"] = value;

        }
    }

    protected void LinkButton2_Click(Object sender, CommandEventArgs e)
    {
        string password = textPassword.Text.Trim();
        string strPassword = PasswordGenerator.GetMD5(password);
        string filePath = e.CommandArgument.ToString();

        //Note:- Update ScanInbox
        //string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        //string filePath = commandArgs[0];
        //WorkareaID = commandArgs[1];
        //string newfileName = Path.GetFileName(filePath);
        //int WorkareaId = Convert.ToInt32(WorkareaID);

        if (UserManagement.GetPassword(User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
        {
            //show the password is wrong        
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);
        }
        else
        {
            string pathRoot = Server.MapPath("~/ScanRestoreBox");
            if (!Directory.Exists(pathRoot))
            {
                Directory.CreateDirectory(pathRoot);
            }

            //string pathSource = e.CommandArgument.ToString();
            string destPath = string.Empty;

            if (filePath.IndexOf("ScanInBox") > 0)
            {
                //General_Class.UpdateWorkArea(newfileName, WorkareaId, 0, Enum_Tatva.IsDelete.Yes.GetHashCode(), 0, 0);
                destPath = filePath.Replace("ScanInBox", "ScanRestoreBox");
                string destDirectory = Path.GetDirectoryName(destPath);
                try
                {
                    if (!Directory.Exists(destDirectory))
                    {
                        Directory.CreateDirectory(destDirectory);
                    }

                    if (File.Exists(destPath))
                    {
                        File.Delete(destPath);
                    }

                    File.Move(filePath, destPath);
                    GetData();
                    BindGrid();
                }
                catch (Exception exp) { Console.WriteLine(exp.Message); }
            }
        }
    }

    protected void LinkButton4_Click(Object sender, CommandEventArgs e)
    {
        string filePath = e.CommandArgument.ToString();

        //Note:- Update ScanInbox
        //string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        //string filePath = commandArgs[0];
        //string ext = Path.GetExtension(filePath).ToLower();
        //WorkareaID = commandArgs[1];
        //string newfileName = Path.GetFileName(filePath);
        //int WorkareaId = Convert.ToInt32(WorkareaID);

        if (File.Exists(filePath))
        {
            try
            {
                string uid = Sessions.SwitchedSessionId;
                //string uid = Membership.GetUser().ProviderUserKey.ToString();
                Shinetech.DAL.Account account = new Shinetech.DAL.Account(uid);

                string strFolderName = Path.GetFileNameWithoutExtension(filePath);
                char[] chars = { ',', ' ' };
                string[] tokens = strFolderName.Split(chars, StringSplitOptions.RemoveEmptyEntries);

                FileFolder folder = new FileFolder();
                folder.FolderName.Value = strFolderName;

                if (tokens.Length == 1)
                {
                    folder.FirstName.Value = tokens[0];
                    folder.Lastname.Value = string.Empty;
                }
                else if (tokens.Length == 2)
                {
                    folder.FirstName.Value = tokens[0];
                    folder.Lastname.Value = tokens[1];
                }
                else
                {
                    folder.FirstName.Value = tokens[0];
                    folder.Lastname.Value = tokens[tokens.Length - 1];
                }

                folder.DOB.Value = null;
                folder.CreatedDate.Value = DateTime.Now;
                folder.SecurityLevel.Value = 1;
                folder.StateID.Value = "CA";


                folder.OfficeID.Value = uid;

                if (this.Page.User.IsInRole("Offices"))
                {
                    folder.OfficeID1.Value = uid;
                }

                else if (this.Page.User.IsInRole("Users"))
                {
                    DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(uid);
                    folder.OfficeID1.Value = dtOfficeUser.Rows[0]["OfficeUID"].ToString();
                }

                Divider newEntity = new Divider();
                newEntity.Name.Value = strFolderName;
                newEntity.Color.Value = "#e78302";

                int newfolderId = FileFolderManagement.CreatNewFileFolder(folder, newEntity);

                //New Field for Folder in 2012-10-28          
                string fid = newfolderId.ToString();
                string tid = newEntity.DividerID.Value.ToString();

                //卢远宗 2015-11-01
                string vpath = Server.MapPath(string.Format("~/{0}", CurrentVolume));
                string pathname = string.Format("{0}\\{1}\\{2}", vpath, fid, tid);

                string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, fid, Path.DirectorySeparatorChar, tid); //转换目标路径
                if (!Directory.Exists(dstpath))
                {
                    Directory.CreateDirectory(dstpath);
                }

                string filename = Path.GetFileName(filePath);
                string encodeName = HttpUtility.UrlPathEncode(filename);
                encodeName = encodeName.Replace("%", "$");
                dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);
                bool existFile = false;

                if (File.Exists(filePath))
                {
                    //拷贝文件并转换格式
                    try
                    {
                        WorkItem item = new WorkItem();
                        item.FolderID = Convert.ToInt32(fid);
                        item.DividerID = Convert.ToInt32(tid);
                        string basePath = this.MapPath(this.FolderName);

                        item.Path = this.PathUrl + Path.DirectorySeparatorChar +
                                    item.FolderID + Path.DirectorySeparatorChar + Convert.ToInt32(tid);
                        item.FileName = Path.GetFileNameWithoutExtension(filePath);
                        item.FullFileName = Path.GetFileName(filePath);
                        item.UID = Sessions.SwitchedSessionId;
                        //item.UID = Membership.GetUser().ProviderUserKey.ToString();
                        item.contentType = getFileExtention(Path.GetExtension(filename));
                        int order = FlashViewer.GetDocumentOrder(Convert.ToInt32(Convert.ToInt32(tid)));

                        item.MachinePath = string.Concat(basePath, Path.DirectorySeparatorChar, item.FolderID,
                        Path.DirectorySeparatorChar, Convert.ToInt32(tid));

                        if (!Directory.Exists(item.MachinePath))
                        {
                            Directory.CreateDirectory(item.MachinePath);
                        }


                        existFile = true;

                        File.Copy(filePath, dstpath, true);
                        var pageCount = 1;
                        float size;
                        using (FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                        {
                            size = stream.Length / (1024 * 1024); // IN MB.
                            string encodeName1 = HttpUtility.UrlPathEncode(item.FullFileName);
                            encodeName = encodeName.Replace("%", "$");
                            string source = string.Concat(item.MachinePath, Path.DirectorySeparatorChar, encodeName);
                            //pageCount = 1;
                            if (item.contentType == StreamType.JPG)
                            {
                                Jpeg2Engine jpeg2Engine = new Jpeg2Engine(item);
                                jpeg2Engine.generatePDFfromImage(source, source.Split('.')[0] + ".pdf");
                            }
                            else if (item.contentType == StreamType.PDF)
                            {
                                iTextSharp.text.pdf.PdfReader pdfReader = new iTextSharp.text.pdf.PdfReader(source);
                                pageCount = pdfReader.NumberOfPages;
                            }
                        }

                        string documentID = General_Class.DocumentsInsert(item.FileName, item.FolderID, item.UID, item.Path, item.DividerID, pageCount, size, item.FileName, (int)item.contentType, order + 1, Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()));
                        //ImageAdapter.ProcessPostFile(dstpath, fid, tid, uid, pathname, filePath);

                        if (Convert.ToInt32(tid) > 0)
                        {
                            General_Class.AuditLogByDocId(Convert.ToInt32(documentID), Membership.GetUser().ProviderUserKey.ToString(), Enum_Tatva.Action.Create.GetHashCode());
                            EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
                            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(item.FolderID, item.DividerID, Convert.ToInt32(documentID), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Create.GetHashCode(), 0, 0);
                        }

                        //General_Class.UpdateWorkArea(item.FileName, WorkareaId, Convert.ToInt32(documentID), Enum_Tatva.IsDelete.No.GetHashCode(), 0, 1);

                        File.Delete(filePath); //添加后删除原文

                        //列出所有的workarea文件
                        //GetData();
                        //BindGrid();
                        //File.Copy(filePath, dstpath, true);
                        //ImageAdapter.ProcessPostFile(dstpath, fid, tid, uid, pathname, filePath);
                        //File.Delete(filePath); //添加后删除原文
                        //列出所有的workarea文件
                        //this.EffID = fid;
                        //this.DividerID = tid;

                        //GetData();
                        //BindGrid();
                    }
                    catch (Exception exp)
                    {
                        lbsStatus.ImageUrl = "";
                        string strWrongInfor = string.Format("Error : Creating failed -{0}.", exp.Message);
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                        return;
                    }
                }
                //BindEFileFolders();
                // DataBindDividers();
            }
            catch (Exception exp)
            {
                lbsStatus.ImageUrl = "";
                string strWrongInfor = string.Format("Error : Converting failed -{0}.", exp.Message);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                return;
            }
            // lbsStatus.ImageUrl = "";
            //Server.Transfer("~/Office/DocumentLoader.aspx?View=1");
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
    }

    //Add to Tab for each pdf
    protected void LinkButton3_Click(Object sender, CommandEventArgs e)
    {
        string[] allowdExtensions = { ".pdf", ".jpeg", ".jpg" };
        string filePath = e.CommandArgument.ToString();
        string ext = Path.GetExtension(filePath).ToLower();

        //Note:- Update ScanInbox
        //string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        //string filePath = commandArgs[0];
        //string ext = Path.GetExtension(filePath).ToLower();
        //WorkareaID = commandArgs[1];
        //string newfileName = Path.GetFileName(filePath);
        //int WorkareaId = Convert.ToInt32(WorkareaID);

        if (!allowdExtensions.Contains(ext))
        {
            Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "alert('You can't move this file.')", true);
            return;
        }

        if (File.Exists(filePath))
        {
            string uid = Sessions.SwitchedSessionId;
            //string uid = Membership.GetUser().ProviderUserKey.ToString();

            //if (EditableDropDownList1.Items.Count == 0) return;
            //string fid = EditableDropDownList1.SelectedValue;

            string fid = FolderID.Value;

            if (ListBox1.Items.Count == 0) return;
            string tid = ListBox1.SelectedValue;

            //卢远宗 2015-11-01
            string vpath = Server.MapPath(string.Format("~/{0}", CurrentVolume));
            string pathname = string.Format("{0}\\{1}\\{2}", vpath, fid, tid);

            string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, fid, Path.DirectorySeparatorChar, tid); //转换目标路径
            if (!Directory.Exists(dstpath))
            {
                Directory.CreateDirectory(dstpath);
            }

            string filename = Path.GetFileName(filePath);
            string encodeName = HttpUtility.UrlPathEncode(filename);
            encodeName = encodeName.Replace("%", "$");
            dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);
            bool existFile = false;


            if (File.Exists(filePath))
            {
                //拷贝文件并转换格式
                try
                {
                    WorkItem item = new WorkItem();
                    item.FolderID = Convert.ToInt32(fid);
                    item.DividerID = Convert.ToInt32(tid);
                    string basePath = this.MapPath(this.FolderName);

                    item.Path = this.PathUrl + Path.DirectorySeparatorChar +
                                item.FolderID + Path.DirectorySeparatorChar + Convert.ToInt32(tid);
                    item.FileName = Path.GetFileNameWithoutExtension(filePath);
                    item.FullFileName = Path.GetFileName(filePath);
                    item.UID = Sessions.SwitchedSessionId;
                    //item.UID = Membership.GetUser().ProviderUserKey.ToString();
                    item.contentType = getFileExtention(Path.GetExtension(filename));
                    int order = FlashViewer.GetDocumentOrder(Convert.ToInt32(Convert.ToInt32(tid)));


                    item.MachinePath = string.Concat(basePath, Path.DirectorySeparatorChar, item.FolderID,
                    Path.DirectorySeparatorChar, Convert.ToInt32(tid));

                    if (!Directory.Exists(item.MachinePath))
                    {
                        Directory.CreateDirectory(item.MachinePath);
                    }


                    existFile = true;

                    File.Copy(filePath, dstpath, true);
                    var pageCount = 1;
                    float size;
                    using (FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                    {
                        size = stream.Length / (1024 * 1024); // IN MB.
                        string encodeName1 = HttpUtility.UrlPathEncode(item.FullFileName);
                        encodeName = encodeName.Replace("%", "$");
                        string source = string.Concat(item.MachinePath, Path.DirectorySeparatorChar, encodeName);
                        //pageCount = 1;
                        if (item.contentType == StreamType.JPG)
                        {
                            Jpeg2Engine jpeg2Engine = new Jpeg2Engine(item);
                            jpeg2Engine.generatePDFfromImage(source, source.Split('.')[0] + ".pdf");
                        }
                        else if (item.contentType == StreamType.PDF)
                        {
                            iTextSharp.text.pdf.PdfReader pdfReader = new iTextSharp.text.pdf.PdfReader(source);
                            pageCount = pdfReader.NumberOfPages;
                        }
                    }

                    string documentID = General_Class.DocumentsInsert(item.FileName, item.FolderID, item.UID, item.Path, item.DividerID, pageCount, size, item.FileName, (int)item.contentType, order + 1, Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()));
                    //ImageAdapter.ProcessPostFile(dstpath, fid, tid, uid, pathname, filePath);

                    if (Convert.ToInt32(tid) > 0)
                    {
                        General_Class.AuditLogByDocId(Convert.ToInt32(documentID), Membership.GetUser().ProviderUserKey.ToString(), Enum_Tatva.Action.Create.GetHashCode());
                    }

                    //General_Class.UpdateWorkArea(item.FileName, WorkareaId, Convert.ToInt32(documentID), Enum_Tatva.IsDelete.No.GetHashCode(), 0, 1);

                    File.Delete(filePath); //添加后删除原文

                    //列出所有的workarea文件
                    //GetData();
                    //BindGrid();
                }
                catch
                {
                    //Common_Tatva.WriteErrorLog(fid, tid, filename, ex);

                    //FilesuploadedRecently_FFB.Add(75);
                    //FilesuploadedRecently_FFB.Add(83);
                    //FilesuploadedRecently_FFB.Add(84);
                    //FilesuploadedRecently_FFB.Add(85);
                    //FilesuploadedRecently_FFB.Add(86);

                    //Dictionary<string, string> UploadErrorFileName1 = new Dictionary<string, string>();
                    //UploadErrorFileName1.Add(file.FileId, filename);

                    //UploadErrorFileName = UploadErrorFileName1;

                    //if ((File.Exists(fullFilePath)))
                    //    File.Delete(fullFilePath);
                }
            }

            if (existFile)
            {
                // Server.Transfer("~/Office/DocumentLoader.aspx?View=1");
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
            }
            else
            {
                lbsStatus.ImageUrl = "";
            }
        }
    }

    public ArrayList FilesuploadedRecently_FFB
    {
        get
        {
            if (this.Session["FilesuploadedRecently_FFB"] != null)
            {
                return this.Session["FilesuploadedRecently_FFB"] as ArrayList;
            }
            else
            {
                this.Session["FilesuploadedRecently_FFB"] = new ArrayList();
                return this.Session["FilesuploadedRecently_FFB"] as ArrayList;
            }

        }
        set
        {
            this.Session["FilesuploadedRecently_FFB"] = value;
        }
    }
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected DataTable GetWorkareaFiles(string uid)
    {
        DataTable table = MakeDataTable();

        ArrayList uids = new ArrayList();
        uids.Add(uid);

        string path = Server.MapPath("~/ScanInBox");
        if (this.Page.User.IsInRole("Offices"))
        {
            uids.AddRange(UserManagement.GetSubUserList(uid));
        }
        else if (this.Page.User.IsInRole("Users"))
        {
            DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(uid);
            var officeID1 = dtOfficeUser.Rows[0]["OfficeUID"].ToString();
            uids.Add(officeID1);

            IList subuses = UserManagement.GetSubUserList(officeID1);

            foreach (string sub in subuses)
            {
                if (sub.ToLower() != uid.ToLower())
                {
                    uids.Add(sub);
                }
            }
        }

        string tempPath = string.Empty;
        foreach (string id in uids)
        {
            tempPath = string.Concat(path, Path.DirectorySeparatorChar, id);

            try
            {
                if (Directory.Exists(tempPath))
                {
                    string[] files = Directory.GetFiles(tempPath);
                    DataRow row = null;
                    FileInfo fi1;

                    string ext = string.Empty;

                    foreach (string file in files)
                    {
                        row = table.NewRow();
                        row["FileUID"] = Path.GetFileName(file);

                        byte[] byteArray = System.Text.Encoding.Default.GetBytes(Path.GetFileName(file));
                        row["FileID"] = System.Convert.ToBase64String(byteArray);

                        byteArray = System.Text.Encoding.Default.GetBytes(id); //uid
                        row["UID"] = System.Convert.ToBase64String(byteArray); ;

                        row["FilePath"] = file;
                        fi1 = new FileInfo(file);
                        row["Size"] = ((double)fi1.Length) / (double)(1024 * 1024);//MB
                        row["DOB"] = fi1.CreationTime;

                        ext = Path.GetExtension(file);

                        /*
                        if (ext.ToLower().Equals(".pdf"))
                        {
                            #region PDF Document
                            try
                            {
                                Aspose.Pdf.Document docPDF = new Aspose.Pdf.Document(file);
                                var docInfo = new Aspose.Pdf.DocumentInfo(docPDF);

                                if (docInfo.ContainsKey("CheckNo")) row["CheckNo"] = docInfo["CheckNo"].ToString();
                                if (docInfo.ContainsKey("Amount")) row["Amount"] = docInfo["Amount"].ToString();

                            }
                            catch (Exception)
                            {
                                continue;
                            }

                            #endregion
                        }
                        else if (ext.ToLower().Equals(".doc") || ext.ToLower().Equals(".docx"))
                        {
                            #region docx Document
                            try
                            {
                                Aspose.Words.Document docWord = new Aspose.Words.Document(file);

                                if (docWord.CustomDocumentProperties.Contains("CheckNo")) row["CheckNo"] = docWord.CustomDocumentProperties["CheckNo"].ToString();
                                if (docWord.CustomDocumentProperties.Contains("Amount")) row["Amount"] = docWord.CustomDocumentProperties["Amount"].ToString();

                            }
                            catch (Exception)
                            {
                                continue;
                            }

                            #endregion
                        }
                        */
                        table.Rows.Add(row);
                        fi1 = null;
                    }

                    string[] varpaths = Directory.GetDirectories(tempPath);
                    DirectoryInfo info;

                    foreach (string item in varpaths)
                    {
                        info = new DirectoryInfo(item);
                        files = Directory.GetFiles(item);
                        if (files.Length > 0)
                        {
                            foreach (string subfile in files)
                            {
                                row = table.NewRow();
                                row["FileUID"] = Path.GetFileName(subfile);

                                string subfileName = subfile.Substring(tempPath.Length);
                                byte[] byteArray = System.Text.Encoding.Default.GetBytes(subfileName);
                                row["FileID"] = System.Convert.ToBase64String(byteArray);

                                byteArray = System.Text.Encoding.Default.GetBytes(id); //uid
                                row["UID"] = System.Convert.ToBase64String(byteArray); ;

                                row["FilePath"] = subfile;
                                fi1 = new FileInfo(subfile);
                                row["Size"] = ((double)fi1.Length) / (double)(1024 * 1024);//MB
                                row["DOB"] = fi1.CreationTime;

                                ext = Path.GetExtension(subfile);

                                /*
                                if (ext.ToLower().Equals(".pdf"))
                                {
                                    #region PDF Document

                                    try
                                    {
                                    Aspose.Pdf.Document docPDF = new Aspose.Pdf.Document(subfile);
                                    var docInfo = new Aspose.Pdf.DocumentInfo(docPDF);

                                    if (docInfo.ContainsKey("CheckNo")) row["CheckNo"] = docInfo["CheckNo"].ToString();
                                    if (docInfo.ContainsKey("Amount")) row["Amount"] = docInfo["Amount"].ToString();

                                    }
                                    catch (Exception)
                                    {

                                        continue;
                                    }

                                    #endregion
                                }
                                else if (ext.ToLower().Equals(".doc") || ext.ToLower().Equals(".docx"))
                                {
                                    #region docx Document
                                    try
                                    {
                                        Aspose.Words.Document docWord = new Aspose.Words.Document(subfile);

                                        if (docWord.CustomDocumentProperties.Contains("CheckNo")) row["CheckNo"] = docWord.CustomDocumentProperties["CheckNo"].ToString();
                                        if (docWord.CustomDocumentProperties.Contains("Amount")) row["Amount"] = docWord.CustomDocumentProperties["Amount"].ToString();

                                    }
                                    catch (Exception)
                                    {

                                        continue;
                                    }

                                    #endregion
                                }
                                */
                                table.Rows.Add(row);
                                fi1 = null;
                            }
                        }

                    }
                }
                else
                {
                    Directory.CreateDirectory(tempPath);

                }
            }
            catch (Exception exp)
            {

            }
        }

        DataView dv = table.DefaultView;
        dv.Sort = "DOB desc";
        table = dv.ToTable();

        return table;
    }

    protected DataTable GetWorkareaFiles(string uid, string keyword)
    {
        DataTable table = MakeDataTable();

        ArrayList uids = new ArrayList();
        uids.Add(uid);

        string path = Server.MapPath("~/ScanInBox");
        if (this.Page.User.IsInRole("Offices"))
        {
            uids.AddRange(UserManagement.GetSubUserList(uid));
        }
        else if (this.Page.User.IsInRole("Users"))
        {
            DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(uid);
            var officeID1 = dtOfficeUser.Rows[0]["OfficeUID"].ToString();
            uids.Add(officeID1);

            IList subuses = UserManagement.GetSubUserList(officeID1);

            foreach (string sub in subuses)
            {
                if (sub.ToLower() != uid.ToLower())
                {
                    uids.Add(sub);
                }
            }
        }

        string tempPath = string.Empty;

        foreach (string id in uids)
        {
            tempPath = string.Concat(path, Path.DirectorySeparatorChar, id);
            try
            {
                if (Directory.Exists(tempPath))
                {
                    string[] files = Directory.GetFiles(tempPath);
                    DataRow row = null;
                    FileInfo fi1;
                    string fileName = string.Empty;
                    string ext = string.Empty;

                    foreach (string file in files)
                    {
                        fileName = Path.GetFileName(file);
                        if (fileName.IndexOf(keyword, StringComparison.CurrentCultureIgnoreCase) >= 0)
                        {
                            row = table.NewRow();
                            row["FileUID"] = Path.GetFileName(file);

                            byte[] byteArray = System.Text.Encoding.Default.GetBytes(Path.GetFileName(file));
                            row["FileID"] = System.Convert.ToBase64String(byteArray);

                            byteArray = System.Text.Encoding.Default.GetBytes(uid);
                            row["UID"] = System.Convert.ToBase64String(byteArray); ;

                            row["FilePath"] = file;
                            fi1 = new FileInfo(file);
                            row["Size"] = ((double)fi1.Length) / (double)(1024 * 1024);//MB
                            row["DOB"] = fi1.CreationTime;

                            ext = Path.GetExtension(file);

                            table.Rows.Add(row);
                            fi1 = null;
                        }


                    }

                    string[] varpaths = Directory.GetDirectories(tempPath);
                    DirectoryInfo info;

                    foreach (string item in varpaths)
                    {
                        files = Directory.GetFiles(item);
                        if (files.Length > 0)
                        {
                            double sizetotal = 0;
                            string details = string.Empty;
                            foreach (string subfile in files)
                            {
                                fileName = Path.GetFileName(subfile);
                                if (fileName.IndexOf(keyword, StringComparison.CurrentCultureIgnoreCase) >= 0)
                                {
                                    row = table.NewRow();
                                    row["FileUID"] = Path.GetFileName(subfile);

                                    string subfileName = subfile.Substring(tempPath.Length);
                                    byte[] byteArray = System.Text.Encoding.Default.GetBytes(subfileName);
                                    row["FileID"] = System.Convert.ToBase64String(byteArray);

                                    byteArray = System.Text.Encoding.Default.GetBytes(uid); //uid
                                    row["UID"] = System.Convert.ToBase64String(byteArray);
                                    ;

                                    row["FilePath"] = subfile;
                                    fi1 = new FileInfo(subfile);
                                    row["Size"] = ((double)fi1.Length) / (double)(1024 * 1024); //MB
                                    row["DOB"] = fi1.CreationTime;

                                    ext = Path.GetExtension(subfile);

                                    table.Rows.Add(row);
                                    fi1 = null;
                                }
                            }
                        }

                    }
                }
                else
                {
                    Directory.CreateDirectory(tempPath);

                }
            }
            catch (Exception exp)
            {

            }
        }

        DataView dv = table.DefaultView;
        dv.Sort = "DOB desc";
        table = dv.ToTable();

        return table;
    }

    private DataTable MakeDataTable()
    {
        // Create new DataTable.
        DataTable table = new DataTable();

        // Declare DataColumn and DataRow variables.
        DataColumn column;

        // Create new DataColumn, set DataType, ColumnName
        // and add to DataTable.    
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FileUID";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FileID";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "UID";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "FilePath";
        table.Columns.Add(column);


        column = new DataColumn();
        column.DataType = Type.GetType("System.Double");
        column.ColumnName = "Size";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.DateTime");
        column.ColumnName = "DOB";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.Boolean");
        column.ColumnName = "IsFolder";
        column.DefaultValue = false;
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "FolderItems";
        column.DefaultValue = "";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "CheckNo";
        column.DefaultValue = "";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "Amount";
        column.DefaultValue = "";

        table.Columns.Add(column);

        return table;
    }


    protected void btnConvert_Clicked(object sender, EventArgs e)
    {
        string filename = "";
        string filePath = "";
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        string path = Server.MapPath("~/ScanInBox");
        string fid = FolderID.Value;
        string tid = ListBox1.SelectedValue;

        string vpath = Server.MapPath(string.Format("~/{0}", CurrentVolume));
        string pathname = string.Format("{0}\\{1}\\{2}", vpath, fid, tid);

        string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, fid, Path.DirectorySeparatorChar, tid);
        if (!Directory.Exists(dstpath))
        {
            Directory.CreateDirectory(dstpath);
        }

        path = string.Concat(path, Path.DirectorySeparatorChar, uid);

        bool existFile = false;
        for (int i = 0; i < GridView1.Rows.Count; i++)
        {
            if (((CheckBox)GridView1.Rows[i].FindControl("checkBox1")).Checked)
            {
                Console.WriteLine(sender.ToString());

                filename = ((DataControlFieldCell)GridView1.Rows[i].Cells[1]).Text.Trim();
                filePath = string.Concat(path, Path.DirectorySeparatorChar, filename);

                string encodeName = HttpUtility.UrlPathEncode(filename);
                encodeName = encodeName.Replace("%", "$");
                dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);
                if (File.Exists(filePath))
                {
                    //拷贝文件并转换格式

                    try
                    {
                        existFile = true;
                        File.Copy(filePath, dstpath, true);
                        ImageAdapter.ProcessPostFile(dstpath, fid, tid, uid, pathname, filePath);

                        File.Delete(filePath); //添加后删除原文
                    }
                    catch { }
                }
            }

            if (existFile)
            {
                Server.Transfer("~/Office/DocumentLoader.aspx?View=1");
            }
            else
            {
                lbsStatus.ImageUrl = "";
            }
        }
    }

    protected void ComboBox1_OnSelectedIndexChanged(object sender, EventArgs e)
    {

    }

    public void search_btn2_click(object sender, EventArgs e)
    {
        string prefxWord = textfield4.Text.Trim();

        GetDataFoeSearch(prefxWord);
        BindGrid();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drvnew = e.Row.DataItem as DataRowView;
            //string pathnew = drvnew.Row[8].ToString();
            string pathnew = drvnew.Row[3].ToString();
            if (!pathnew.ToLower().Contains(".pdf"))
            {
                LinkButton libAdd = e.Row.FindControl("LinkButton3") as LinkButton;
                LinkButton libCreate = e.Row.FindControl("LinkButton4") as LinkButton;
                libAdd.Visible = false;
                libCreate.Visible = false;
            }
            if (this.Page.User.IsInRole("Users"))
            {
                System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
                string path = drv.Row[3].ToString();

                if (!path.Contains(Sessions.SwitchedSessionId))
                //if (!path.Contains(Membership.GetUser().ProviderUserKey.ToString()))
                {
                    LinkButton libDel = e.Row.FindControl("LinkButton2") as LinkButton;
                    libDel.Visible = false;
                }
            }
        }
    }

    protected void LinkButtonEdit_Click(object sender, CommandEventArgs e)
    {
        //Note:- Update ScanInbox
        //string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
        //string pdfPath = commandArgs[0];
        // WorkareaID = commandArgs[1];

        string pdfPath = e.CommandArgument.ToString();
        hidFullName.Value = pdfPath;
        hidWorkareaId.Value = WorkareaID;
        hdnforimageandxlspath.Value = pdfPath;
        hdnforimageandxlsWorkareaId.Value = WorkareaID;

        string ext = Path.GetExtension(pdfPath);
        string fileName = Path.GetFileNameWithoutExtension(pdfPath);
        TextBoxFileName.Text = fileName;
        TextBoxforimageandxls.Text = fileName;
        if (ext.ToLower().Equals(".pdf"))
        {
            #region PDF Document
            Aspose.Pdf.Document docPDF = new Aspose.Pdf.Document(pdfPath);
            var docInfo = new Aspose.Pdf.DocumentInfo(docPDF);

            if (docInfo.ContainsKey("FirstName")) TextBoxFirstName.Text = docInfo["FirstName"].ToString();
            if (docInfo.ContainsKey("LastName")) TextBoxLastName.Text = docInfo["LastName"].ToString();

            if (docInfo.ContainsKey("CompanyName")) TextBoxCompanyName.Text = docInfo["CompanyName"].ToString();
            if (docInfo.ContainsKey("CheckNo")) TextBoxCheckNo.Text = docInfo["CheckNo"].ToString();
            if (docInfo.ContainsKey("Amount")) TextBoxAmount.Text = docInfo["Amount"].ToString();

            updPnlEditDetail.Update();
            this.mdlPopup.Show();

            docPDF.Dispose();

            #endregion
        }
        else if (ext.ToLower().Equals(".doc") || ext.ToLower().Equals(".docx"))
        {
            #region docx Document
            Aspose.Words.Document docWord = new Aspose.Words.Document(pdfPath);

            if (docWord.CustomDocumentProperties.Contains("FirstName")) TextBoxFirstName.Text = docWord.CustomDocumentProperties["FirstName"].ToString();
            if (docWord.CustomDocumentProperties.Contains("LastName")) TextBoxLastName.Text = docWord.CustomDocumentProperties["LastName"].ToString();
            if (docWord.CustomDocumentProperties.Contains("CompanyName")) TextBoxCompanyName.Text = docWord.CustomDocumentProperties["CompanyName"].ToString();
            if (docWord.CustomDocumentProperties.Contains("CheckNo")) TextBoxCheckNo.Text = docWord.CustomDocumentProperties["CheckNo"].ToString();
            if (docWord.CustomDocumentProperties.Contains("Amount")) TextBoxAmount.Text = docWord.CustomDocumentProperties["Amount"].ToString();

            updPnlEditDetail.Update();
            this.mdlPopup.Show();
            #endregion
        }
        else
        {

            UpdatePanelforimageandxls.Update();
            this.mdlPopupforimageandxls.Show();
            //return;
        }
        //updPnlEditDetail.Update();
        //this.mdlPopup.Show();
    }

    protected void ButtonEditOk_Click(object sender, EventArgs e)
    {
        string fullPath, ext, fileName;
        var newPath = ""; var newFullName = ""; var newFullNameforimageandxlspath = "";
        if (hidFullName.Value == null || hidFullName.Value == "")
        {
            fullPath = hdnforimageandxlspath.Value;
            //WorkareaId = Convert.ToInt32(hdnforimageandxlsWorkareaId.Value);
            fileName = Path.GetFileName(fullPath);
            ext = Path.GetExtension(fileName);
            newPath = Path.GetDirectoryName(fullPath);
            newFullNameforimageandxlspath = Path.Combine(newPath, TextBoxforimageandxls.Text.Trim() + ext);
        }
        else
        {
            fullPath = hidFullName.Value;
            //WorkareaId = Convert.ToInt32(hidWorkareaId.Value);
            fileName = Path.GetFileName(fullPath);
            ext = Path.GetExtension(fileName);
            newPath = Path.GetDirectoryName(fullPath);
            newFullName = Path.Combine(newPath, TextBoxFileName.Text.Trim() + ext);
        }


        //string fileName = Path.GetFileName(fullPath);
        //string ext = Path.GetExtension(fileName);

        //var newPath = Path.GetDirectoryName(fullPath);
        //var newFullName = Path.Combine(newPath, TextBoxFileName.Text.Trim() + ext);
        //int WorkareaId = Convert.ToInt32(hidWorkareaId.Value);

        if (ext.ToLower().Equals(".pdf"))
        {
            #region PDF Document

            Aspose.Pdf.Document docPDF = new Aspose.Pdf.Document(fullPath);
            var docInfo = new Aspose.Pdf.DocumentInfo(docPDF);

            if (docInfo.ContainsKey("FirstName"))
            {
                docInfo["FirstName"] = TextBoxFirstName.Text.Trim();
            }
            else
            {
                docInfo.Add("FirstName", TextBoxFirstName.Text.Trim());
            }

            if (docInfo.ContainsKey("LastName"))
            {
                docInfo["LastName"] = TextBoxLastName.Text.Trim();
            }
            else
            {
                docInfo.Add("LastName", TextBoxLastName.Text.Trim());
            }

            if (docInfo.ContainsKey("CompanyName"))
            {
                docInfo["CompanyName"] = TextBoxCompanyName.Text.Trim();
            }
            else
            {
                docInfo.Add("CompanyName", TextBoxCompanyName.Text.Trim());
            }

            if (docInfo.ContainsKey("CheckNo"))
            {
                docInfo["CheckNo"] = TextBoxCheckNo.Text.Trim();
            }
            else
            {
                docInfo.Add("CheckNo", TextBoxCheckNo.Text.Trim());
            }

            if (docInfo.ContainsKey("Amount"))
            {
                docInfo["Amount"] = TextBoxAmount.Text.Trim();
            }
            else
            {
                docInfo.Add("Amount", TextBoxAmount.Text.Trim());
            }

            docInfo.ModDate = DateTime.Now;

            docPDF.Save(newFullName);

            #endregion
        }
        else if (ext.ToLower().Equals(".doc") || ext.ToLower().Equals(".docx"))
        {
            #region docx Document

            Aspose.Words.Document docWord = new Aspose.Words.Document(fullPath);

            if (docWord.CustomDocumentProperties.Contains("FirstName"))
            {
                docWord.CustomDocumentProperties["FirstName"].Value = TextBoxFirstName.Text.Trim();
            }
            else
            {
                docWord.CustomDocumentProperties.Add("FirstName", TextBoxFirstName.Text.Trim());
            }

            if (docWord.CustomDocumentProperties.Contains("LastName"))
            {
                docWord.CustomDocumentProperties["LastName"].Value = TextBoxLastName.Text.Trim();
            }
            else
            {
                docWord.CustomDocumentProperties.Add("LastName", TextBoxLastName.Text.Trim());
            }

            if (docWord.CustomDocumentProperties.Contains("CompanyName"))
            {
                docWord.CustomDocumentProperties["CompanyName"].Value = TextBoxCompanyName.Text.Trim();
            }
            else
            {
                docWord.CustomDocumentProperties.Add("CompanyName", TextBoxCompanyName.Text.Trim());
            }

            if (docWord.CustomDocumentProperties.Contains("CheckNo"))
            {
                docWord.CustomDocumentProperties["CheckNo"].Value = TextBoxCheckNo.Text.Trim();
            }
            else
            {
                docWord.CustomDocumentProperties.Add("CheckNo", TextBoxCheckNo.Text.Trim());
            }

            if (docWord.CustomDocumentProperties.Contains("Amount"))
            {
                docWord.CustomDocumentProperties["Amount"].Value = TextBoxAmount.Text.Trim();
            }
            else
            {
                docWord.CustomDocumentProperties.Add("Amount", TextBoxAmount.Text.Trim());
            }

            docWord.Save(newFullName);

            #endregion
        }
        else
        {
            System.IO.File.Move(fullPath, newFullNameforimageandxlspath);
            //this.mdlPopup.Hide();
            //this.mdlPopupforimageandxls.Hide();
            //return;
        }

        try
        {
            FileInfo fileInfo = new FileInfo(fullPath);
            // string newfileName;
            //if (hidFullName.Value == null || hidFullName.Value == "")
            //{
            //newfileName = Path.GetFileName(newFullName);
            //}
            //else
            //{
            // newfileName = Path.GetFileName(newFullNameforimageandxlspath);
            //}
            //General_Class.UpdateWorkArea(newfileName, WorkareaId, 0, Enum_Tatva.IsDelete.No.GetHashCode(), 1, 0);

            if (hidFullName.Value != null && hidFullName.Value != "")
            {
                if (fileInfo.Exists && !fileName.Equals(Path.GetFileName(newFullName)))
                {
                    fileInfo.Delete();
                }
            }
            else
            {
            }

            hidFullName.Value = newFullName;
            hdnforimageandxlspath.Value = newFullNameforimageandxlspath;
        }
        catch (Exception)
        { }
        //列出所有的workarea文件

        string prefxWord = textfield4.Text.Trim();
        if (prefxWord.Length > 0)
            GetDataFoeSearch(prefxWord);
        else
            GetData();
        BindGrid();

        if (hidFullName.Value != null && hidFullName.Value != "")
        {
            updPnlEditDetail.Update();
            UpdatePanel1.Update();
            this.mdlPopup.Hide();
        }
        else
        {
            UpdatePanelforimageandxls.Update();
            UpdatePanel1.Update();
            this.mdlPopupforimageandxls.Hide();
        }
    }

    protected void ButtonEditCancel1_Click(object sender, EventArgs e)
    {
        if (hidFullName.Value != null && hidFullName.Value != "")
        {
            this.mdlPopup.Hide();
        }
        else
        {
            this.mdlPopupforimageandxls.Hide();
        }
    }

    protected void EditableDropDownList_OnClick(object sender, EventArgs e)
    {
        AjaxControlToolkit.DropDownExtender editableList = sender as AjaxControlToolkit.DropDownExtender;
        if (editableList != null)
        {
            DataBindDividers();
        }
    }

    protected void btnRedirect_Click(object sender, EventArgs e)
    {
        //string XS = this.tagsXmlContent.Text;
        string XS = HttpUtility.HtmlDecode(this.tagsXmlContent.Text);

        if (FilesuploadedRecently != null && FilesuploadedRecently.Count > 0)
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(XS);

            if (xml != null)
            {
                foreach (var item in FilesuploadedRecently)
                {
                    XmlNodeList xnList = xml.SelectNodes("/Tags/document");
                    foreach (XmlNode xn in xnList)
                    {
                        string TagKey = xn["TagKey"].InnerText;
                        string TagValue = xn["TagValue"].InnerText;
                        string uid = Sessions.SwitchedSessionId;
                        //string uid = Membership.GetUser().ProviderUserKey.ToString();
                        FileFolderManagement.InsertDocumentTags(uid, TagKey, TagValue, (int)item);
                    }
                }
            }

            FilesuploadedRecently.Clear();
            Response.Redirect(virtualDir + "Office/DocumentLoading.aspx");
        }
    }

}
