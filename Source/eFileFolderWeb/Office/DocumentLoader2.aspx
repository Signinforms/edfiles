<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    EnableSessionState="True" EnableViewState="true" CodeFile="DocumentLoader2.aspx.cs"
    Inherits="DocumentLoader1" Title="" %>

<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%--<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">    
    <asp:Button id="btnRedirect" runat="server" OnClick="btnRedirect_Click" />
    
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script language="javascript">

        function onClientUploadComplete(sender, e) {
            //onImageValidated("TRUE", e);
        }

        function onClientUploadComplete2(sender, e) {
            //onImageValidated("TRUE", e);
        }

        function onImageValidated(arg, context) {

            var test = document.getElementById("testuploaded");
            test.style.display = 'block';

            var fileList = document.getElementById("fileList");
            var item = document.createElement('div');
            item.style.padding = '4px';

            item.appendChild(createFileInfo(context));

            fileList.appendChild(item);
        }

        function createFileInfo(e) {
            var holder = document.createElement('div');
            holder.appendChild(document.createTextNode(e.get_fileName() + ' with size ' + e.get_fileSize() + ' bytes'));

            return holder;
        }



        function onClientUploadStart(sender, e) {
            document.getElementById('uploadCompleteInfo').innerHTML = "please wait while the " + e.get_filesInQueue() + " file is uploaded and converted to eff...";            
        }

        function onClientUploadStart2(sender, e) {
            document.getElementById('uploadCompleteInfo2').innerHTML = 'Please wait while uploading ' + e.get_filesInQueue() + ' files...';            
        }

        function onClientUploadCompleteAll(sender, e) {

            var args = JSON.parse(e.get_serverArguments()),
                unit = args.duration > 60 ? 'minutes' : 'seconds',
                duration = (args.duration / (args.duration > 60 ? 60 : 1)).toFixed(2);

            var info = 'At <b>' + args.time + '</b> server time <b>'
                + e.get_filesUploaded() + '</b> of <b>' + e.get_filesInQueue()
                + '</b> files were uploaded with status code <b>"' + e.get_reason()
                + '"</b> in <b>' + duration + ' ' + unit + '</b>';

            document.getElementById('uploadCompleteInfo').innerHTML = info;

            document.getElementById('<%=btnRedirect.ClientID%>').click();
            setTimeout(function () {
                window.location = "../Office/DocumentLoading.aspx";
            }, 3000);
        }

        function onClientUploadCompleteAll2(sender, e) {

            var args = JSON.parse(e.get_serverArguments()),
                unit = args.duration > 60 ? 'minutes' : 'seconds',
                duration = (args.duration / (args.duration > 60 ? 60 : 1)).toFixed(2);

            var info = 'At <b>' + args.time + '</b> server time <b>'
                + e.get_filesUploaded() + '</b> of <b>' + e.get_filesInQueue()
                + '</b> files were uploaded with status code <b>"' + e.get_reason()
                + '"</b> in <b>' + duration + ' ' + unit + '</b>';

            document.getElementById('uploadCompleteInfo2').innerHTML = info;
        }

        function setConversionText() {
            var obj = document.getElementById("<%= lbsStatus.ClientID %>");
            obj.setAttribute("src", "../Images/wait24trans.gif");
            obj.setAttribute("Visible", "true");
            //obj.innerHTML = " (Converting, Please Wait.)";
        }
          

        
    


  
    </script>
    <tr>
        <td height="186" valign="top" style="background-repeat: no-repeat;">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="730" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                        style="background-repeat: repeat-x;">
                        <div style="background: url(images/inner_b.gif); background-position: right top;
                            background-repeat: no-repeat; margin-top: 10px;" />
                        <div style="height: 75px;">
                            <div class="Naslov_Sin">
                                Document Loader
                            </div>
                        </div>
                        <div class="podnaslov" style="padding: 5px; background-color: #f6f5f2;">
                            In order to add documents and files to your EdFiles, please select the folder
                            from the drop down menu below, then select the tab in which you would like to place
                            the document in and then select the file/document you would like to place in that
                            tab of the edFile. It is that simple. If you need assistance, please feel free
                            to chat, email or call customer service.
                        </div>
                        <asp:Panel ID="panelMain" runat="server" Visible="true">
                            <div class="tekstDef_upload">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:HiddenField ID="HiddenField1" runat="server" />
                                        <asp:HiddenField ID="HiddenField2" runat="server" />                                                                                
                                        <table border="0" cellpadding="2" cellspacing="0" width="100%" style="margin: 0px 0 10px 10px;">
                                            <tr>
                                                <td width="45%" valign="top">
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <div class="form_title_2" style="margin-top: 0px; margin-bottom: 2px">
                                                                    1. Select an edFile
                                                                </div>
                                                                <asp:DropDownList ID="EditableDropDownList1" runat="server" Sorted="true" Width="265px"
                                                                    OnOnClick="EditableDropDownList_OnClick">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="100%" width="100%">
                                                                <div class="form_title_2" style="margin-top: 20px; margin-bottom: 2px">
                                                                    2. Select a tab for the selected edFile
                                                                </div>
                                                                <asp:ListBox ID="ListBox1" Width="100%" Height="150" DataValueField="DividerID" DataTextField="Name"
                                                                    AutoPostBack="true" OnSelectedIndexChanged="ListBox1_OnSelectedIndexChanged"
                                                                    runat="server"></asp:ListBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="55%" valign="top">
                                                    <div>
                                                        <asp:Image runat="server" ID="Imagelabel12" ImageUrl="../Images/loader_bg.gif" ImageAlign="Middle" /><asp:Label
                                                            ID="label12" runat="server"></asp:Label>
                                                    </div>
                                                    <div class="form_title_2" style="margin-top: 3px; margin-bottom: 2px">
                                                        3. Click the Browse button to upload document
                                                    </div>
                                                    <div><asp:HiddenField ID="tagsXmlContent" runat="server" Visible="true"  Value="<Tags><document><Id>0</Id><FileName>Name of File</FileName><TagKey></TagKey><TagValue></TagValue></document></Tags>"  />
                                                        <asp:Label runat="server" ID="myThrobber" Style="display: none;"><img align="absmiddle" alt="" src="uploading.gif"/></asp:Label>
                                                        <ajaxToolkit:AjaxFileUpload ID="AjaxFileUpload1" runat="server" Padding-Bottom="4"
                                                            Padding-Left="2" Padding-Right="1" Padding-Top="4" ThrobberID="myThrobber" OnClientUploadComplete="onClientUploadComplete"
                                                            OnUploadComplete="AjaxFileUpload1_OnUploadComplete" MaximumNumberOfFiles="10"
                                                            AllowedFileTypes="jpg,jpeg,pdf,doc,docx,xls" AzureContainerName="" OnClientUploadCompleteAll="onClientUploadCompleteAll"
                                                            OnUploadCompleteAll="AjaxFileUpload1_UploadCompleteAll" OnUploadStart="AjaxFileUpload1_UploadStart"
                                                            OnClientUploadStart="onClientUploadStart" ContextKeys="2" />
                                                        <div id="uploadCompleteInfo">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <a href="#"  style="font-family: Trebuchet MS;" onclick="AddNewTag()" >Add New Custom field</a>
                                                    <div id="taggrid" ></div>                                                    
                                                </td>
                                            </tr>
                                            <%-- workarea files--%>
                                            <tr>
                                                <td colspan="2">
                                                    <div class="form_title_2">
                                                        4. Upload scans to Work Area, please click <a href="./UploadWorkFile.aspx">here</a>
                                                        . Scans in the work are not assigned or filed in any folder.<asp:Image runat="server"
                                                            ImageUrl="../Images/loader_bg.gif" ID="lbsStatus" ImageAlign="Middle" />
                                                    </div>
                                                    <%-- workarea files loader--%>
                                                    <%-- <div>
                                                        <asp:Label runat="server" ID="myThrobber2" Style="display: none;"><img align="absmiddle" alt="" src="uploading.gif"/></asp:Label>
                                                        <ajaxToolkit:AjaxFileUpload ID="AjaxFileUpload2" runat="server" Padding-Bottom="4"
                                                            Padding-Left="2" Padding-Right="1" Padding-Top="4" ThrobberID="myThrobber2" OnClientUploadComplete="onClientUploadComplete2"
                                                            OnUploadComplete="AjaxFileUpload2_OnUploadComplete" MaximumNumberOfFiles="10"
                                                            AllowedFileTypes="pdf,doc,docx,xls" AzureContainerName="" OnClientUploadCompleteAll="onClientUploadCompleteAll2"
                                                            OnUploadCompleteAll="AjaxFileUpload2_UploadCompleteAll" OnUploadStart="AjaxFileUpload2_UploadStart"
                                                            OnClientUploadStart="onClientUploadStart2" ContextKeys="3"/>
                                                        <div id="uploadCompleteInfo2">
                                                        </div>
                                                    </div>--%>
                                                    <asp:GridView ID="GridView1" runat="server" OnSorting="GridView1_Sorting" AllowSorting="True"
                                                        AutoGenerateColumns="false" Width="100%" BorderWidth="2" OnRowDataBound="GridView1_RowDataBound">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                        <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                        <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                        <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Action" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButtonEdit" runat="server" Text="Edit" CommandArgument='<%# Eval("FilePath")%>'
                                                                        OnCommand="LinkButtonEdit_Click"></asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Delete" CommandArgument='<%# Eval("FilePath")%>'
                                                                        OnCommand="LinkButton2_Click"></asp:LinkButton>
                                                                    <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="LinkButton2"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="LinkButton2">
                                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:HyperLinkField DataTextField="FileUID" HeaderText="FileName" SortExpression="FileUID"
                                                                DataNavigateUrlFormatString="~/Office/SharePDF.aspx?id={0}&uid={1}" Target="_blank"
                                                                DataNavigateUrlFields="FileID,UID">
                                                                <HeaderStyle CssClass="form_title" />
                                                            </asp:HyperLinkField>
                                                            <%--  <asp:BoundField DataField="Amount" HeaderText="Amount" HtmlEncode="false" />
                                                            <asp:BoundField DataField="CheckNo" HeaderText="Check/Wire No." HtmlEncode="false" />--%>
                                                            <asp:BoundField DataField="Size" HeaderText="Size(Mb)" DataFormatString="{0:F2}"
                                                                HtmlEncode="false" />
                                                            <asp:BoundField DataField="DOB" HeaderText="Date" 
                                                                SortExpression="DOB" />
                                                            <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Add to Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton3" runat="server" Text="Add" CommandArgument='<%# Eval("FilePath")%>'
                                                                        OnCommand="LinkButton3_Click" OnClientClick="setConversionText();"></asp:LinkButton>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ButtonAdd" runat="server" ConfirmText="Are you sure you want to add this document to the selected tab?"
                                                                        TargetControlID="LinkButton3">
                                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label9" runat="server" Text="Add to EFF" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton4" runat="server" Text="Create" CommandArgument='<%# Eval("FilePath")%>'
                                                                        OnCommand="LinkButton4_Click" OnClientClick="setConversionText();"></asp:LinkButton>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ButtonAddEFF" runat="server" ConfirmText="Are you sure you want to add this document to new eff?"
                                                                        TargetControlID="LinkButton4">
                                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                                <tr>
                                                                    <td align="left" class="podnaslov">
                                                                        <asp:Label ID="Label120" runat="server" />There are no files. right now.
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="prevnext" bgcolor="#f8f8f5" colspan="2">
                                                    <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                                        PageSize="5" CssClass="prevnext" PagerStyle="NextPrev" ControlToPaginate="GridView1">
                                                    </cc1:WebPager>
                                                    <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="prevnext" bgcolor="#f8f8f5" colspan="2" align="center">
                                                    <asp:TextBox ID="textfield4" name="textfield4" runat="server" class="form_tekst_field"
                                                        MaxLength="50" autocomplete="off" />
                                                    <ajaxToolkit:AutoCompleteExtender runat="server" UseContextKey="true" BehaviorID="AutoCompleteEx1"
                                                        ID="autoComplete2" TargetControlID="textfield4" ServicePath="~/FlexWebService.asmx"
                                                        ServiceMethod="GetCompletionFileNameList" MinimumPrefixLength="1" CompletionInterval="1000"
                                                        EnableCaching="true" CompletionSetCount="10" CompletionListCssClass="autocomplete_completionListElement"
                                                        CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                                        DelimiterCharacters=";, :" ShowOnlyCurrentWordInCompletionListItem="true">
                                                        <Animations>
                                                                <OnShow>
                                                                    <Sequence>
                                                                        <%-- Make the completion list transparent and then show it --%>
                                                                        <OpacityAction Opacity="0" />
                                                                        <HideAction Visible="true" />
                            
                                                                        <%--Cache the original size of the completion list the first time
                                                                            the animation is played and then set it to zero --%>
                                                                        <ScriptAction Script="
                                                                            // Cache the size and setup the initial size
                                                                            var behavior = $find('AutoCompleteEx1');
                                                                            if (!behavior._height) {
                                                                                var target = behavior.get_completionList();
                                                                                behavior._height = target.offsetHeight - 2;
                                                                                target.style.height = '0px';
                                                                            }" />
                            
                                                                        <%-- Expand from 0px to the appropriate size while fading in --%>
                                                                        <Parallel Duration=".4">
                                                                            <FadeIn />
                                                                            <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx1')._height" />
                                                                        </Parallel>
                                                                    </Sequence>
                                                                </OnShow>
                                                                <OnHide>
                                                                    <%-- Collapse down to 0pxc and fade out --%>
                                                                    <Parallel Duration=".4">
                                                                        <FadeOut />
                                                                        <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx1')._height" EndValue="0" />
                                                                    </Parallel>
                                                                </OnHide>
                                                        </Animations>
                                                    </ajaxToolkit:AutoCompleteExtender>
                                                    <asp:Button ID="searchButton2" name="searchButton2" runat="server" CausesValidation="false"
                                                        Width="60" Height="21" OnClick="search_btn2_click" Text="Search" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="prevnext" bgcolor="#f8f8f5" colspan="2" align="center">
                                                    <br />
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                    <Triggers>    
                                        <asp:AsyncPostBackTrigger ControlID="ListBox1" EventName="SelectedIndexChanged" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </asp:Panel>
                        <asp:Panel class="popupConfirmation_1" ID="DivDeleteConfirmation" Style="display: none"
                            runat="server">
                            <div class="popup_Container">
                                <div class="popup_Titlebar" id="PopupHeader">
                                    <div class="TitlebarLeft">
                                        Delete File
                                    </div>
                                    <div class="TitlebarRight" onclick="$get('ButtonDeleteCancel').click();">
                                    </div>
                                </div>
                                <div class="popup_Body_1">
                                    <p>
                                        Caution! Are you sure you want to delete this file?
                                    </p>
                                    <p>
                                        Confirm Password:
                                        <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                                    </p>
                                </div>
                                <div class="popup_Buttons">
                                    <input id="ButtonDeleleOkay" type="button" value="Yes" style="margin-left: 80px;
                                        width: 65px" />
                                    <input id="ButtonDeleteCancel" type="button" value="No" style="margin-left: 20px;
                                        width: 65px" />
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Button Style="display: none" ID="btnShowPopup" runat="server"></asp:Button>
                        <ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" BackgroundCssClass="modalBackground"
                            CancelControlID="ButtonEditCancel1" OkControlID="ButtonEditOk" PopupControlID="DivEditConfirmation"
                            TargetControlID="btnShowPopup">
                        </ajaxToolkit:ModalPopupExtender>
                        <asp:Panel class="popupConfirmation_1" ID="DivEditConfirmation" Style="display: none"
                            runat="server">
                            <asp:UpdatePanel ID="updPnlEditDetail" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="popup_Container">
                                        <div class="popup_Titlebar" id="PopupHeaderEdit">
                                            <div class="TitlebarLeft">
                                                Edit File
                                            </div>
                                        </div>
                                        <div class="popup_Body_1">
                                            <table>
                                                <tr>
                                                    <td>
                                                        File Name
                                                    </td>
                                                    <td>
                                                        <asp:HiddenField runat="server" ID="hidFullName" />
                                                        <asp:TextBox ID="TextBoxFileName" runat="server" Width="200px" MaxLength="20" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        First Name
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxFirstName" runat="server" Width="200px" MaxLength="20" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Last Name
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxLastName" runat="server" Width="200px" MaxLength="20" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Company Name
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxCompanyName" runat="server" Width="200px" MaxLength="20" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Amount
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxAmount" runat="server" Width="200px" MaxLength="20" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        Check/Wire No.
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="TextBoxCheckNo" runat="server" Width="200px" MaxLength="20" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="popup_Buttons">
                                            <asp:Button ID="ButtonEditOk" runat="server" OnClick="ButtonEditOk_Click" Text="Save"
                                                Width="65px" Style="margin-left: 80px; width: 65px" />
                                            <asp:Button ID="ButtonEditCancel1" runat="server" OnClick="ButtonEditCancel1_Click"
                                                Text="Cancel" Width="65px" Style="margin-left: 20px; width: 65px" />
                                        </div>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="ButtonEditOk" EventName="Click" />
                                    <asp:AsyncPostBackTrigger ControlID="ButtonEditCancel1" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </asp:Panel>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>                                
                                <asp:Panel ID="panelConfirm" runat="server" Visible="false">
                                    <div class="form_title_1">
                                        Document was uploaded successfully. Next Step:
                                    </div>
                                    <div class="tekstDef" style="">
                                        <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                            <tr>
                                                <td align="left" class="podnaslov">
                                                    <asp:Label ID="lbConfirm" runat="server" />
                                                    Do you want to
                                                    <asp:HyperLink ID="btnUpload" runat="server" Text="Upload More Documents" NavigateUrl="~/Office/DocumentLoader.aspx"></asp:HyperLink>
                                                    or <a href="#" onclick="window.open('WebFlashViewer.aspx?ID=<%=FolderId %>', 'eBook', 'toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no'); return false;">
                                                        View Your EdFiles Now</a> ?
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="PanelInfor" runat="server" Visible="false">
                                    <div class="tekstDef" style="">
                                        <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                            <tr>
                                                <td align="left" class="podnaslov">
                                                    <asp:Label ID="Label1" runat="server" />There is no folders. please check the link
                                                    to
                                                    <asp:HyperLink ID="HyperLink1" runat="server" Text="Create New edFile" NavigateUrl="~/Office/NeweFileFolder.aspx"></asp:HyperLink>
                                                    right now.
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </asp:Panel>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div style="padding-left: 30px;">
                        </div>
                    </td>
                    <td width="10" align="center">
                        <img src="../images/lin.gif" width="1" height="453" alt="" vspace="10" />
                    </td>
                    <td valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2" style="background-repeat: repeat-x;">
                        <%--<uc1:QuickFind ID="QuickFind1" runat="server" />--%>
                         <marquee scrollamount="1" id="marqueeside"  runat="server"
                                behavior="scroll" onmouseover="this.stop()" onmouseout="this.start()" scrolldelay="20"
                                direction="up" >
                                <%=SideMessage %></marquee>
                        <%--<uc2:ReminderControl ID="ReminderControl1" runat="server" />--%>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    
    <script type="text/javascript">
        var hidTagsXml = document.getElementById("<%=tagsXmlContent.ClientID%>");

        var TemplateXml = hidTagsXml.value; //"<Tags><document><Id>0</Id><FileName>Name of File</FileName><TagKey>TagKey</TagKey><TagValue>SomeName</TagValue></document></Tags>";

        function getDocument() {
            var xmlDoc = null;
            // TemplateXml = hidTagsXml.value;

            if (window.DOMParser) {
                parser = new DOMParser();
                xmlDoc = parser.parseFromString(TemplateXml, "text/xml");
            }
            else {
                xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async = false;
                xmlDoc.loadXML(TemplateXml);
            }
            return xmlDoc;
        }


        function GenerateGrid()
        {

            var xmlDoc = getDocument();
            var strHTML = '<table id="tbltags" style="border-width:2px;border-style:solid;width:100%;border-collapse:collapse;"><tr class="form_title" style="background-color:#DBD9CF;text-align:left;" ><th style="display:none;">File Name</th><th>Custom Field Label</th><th>Custom Field Value</th><th>Action</th></tr>';
            if (xmlDoc != null) {
                var ListOfElements = xmlDoc.getElementsByTagName('document');
                if (ListOfElements != null)
                {
                    for (var i = 0; i < ListOfElements.length; i++)
                    {
                        strHTML += '<tr id="TRE-' + ListOfElements[i].childNodes[0].innerHTML + '"><td style="display:none;">' + ListOfElements[i].childNodes[1].innerHTML + '</td><td>' + ListOfElements[i].childNodes[2].innerHTML + '</td><td>' + ListOfElements[i].childNodes[3].innerHTML + '</td><td><a onclick="Edit(\'TRE-' + ListOfElements[i].childNodes[0].innerHTML + '\')" href="#">Edit</a>&nbsp;<a href="#" onclick="RemoveTag(\'TRE-' + ListOfElements[i].childNodes[0].innerHTML + '\')">Delete</a></td></tr>';
                    }
                }
            }
            strHTML += '</table>';
            document.getElementById('taggrid').innerHTML = strHTML;

        }

        function Edit(id)
        {
            try
            {
                id = id.split('-')[1];
                id = parseInt(id) + 1;

                if (id != null)
                {
                    var fileFileName = "";
                    var TagKey = "";
                    var TagValue = "";

                    var action = document.getElementById("tbltags").rows[id].cells[3].firstChild.innerHTML;
                    if (action == "Edit") {

                        fileFileName = document.getElementById("tbltags").rows[id].cells[0].innerHTML;
                        TagKey = document.getElementById("tbltags").rows[id].cells[1].innerHTML;
                        TagValue = document.getElementById("tbltags").rows[id].cells[2].innerHTML;

                        var tagKeyElement = '<input type="text" id="editTheTagKey" value="' + TagKey + '" >';
                        var tagValueElement = '<input type="text" id="editTheTagValue" value="' + TagValue + '" >';

                        document.getElementById("tbltags").rows[id].cells[1].innerHTML = tagKeyElement;
                        document.getElementById("tbltags").rows[id].cells[2].innerHTML = tagValueElement;
                        document.getElementById("tbltags").rows[id].cells[3].firstChild.innerHTML = "Update";

                    }
                    else
                    {
                        fileFileName = document.getElementById("tbltags").rows[id].cells[0].innerHTML;
                        TagKey = document.getElementById("editTheTagKey").value;
                        TagValue = document.getElementById("editTheTagValue").value;

                        document.getElementById("tbltags").rows[id].cells[1].innerHTML = TagKey;
                        document.getElementById("tbltags").rows[id].cells[2].innerHTML = TagValue;
                        document.getElementById("tbltags").rows[id].cells[3].firstChild.innerHTML = "Edit";
                        UpdateXmlDocumentForTags(id, fileFileName, TagKey, TagValue);
                    }
                }
            }
            catch (ex) {
              //  alert(ex.message);
            }
        }

        function UpdateXmlDocumentForTags(id, fileName, tagKey, tagValue)
        {
            id = parseInt(id) - 1;
            var xmlDoc = getDocument();
            if (xmlDoc != null) {
                var ListOfElements = xmlDoc.getElementsByTagName('document');
                if (ListOfElements != null)
                {
                    for (var i = 0; i < ListOfElements.length; i++)
                    {
                        if (i == id)
                        {
                            ListOfElements[i].childNodes[1].innerHTML = fileName;
                            ListOfElements[i].childNodes[2].innerHTML = tagKey;
                            ListOfElements[i].childNodes[3].innerHTML = tagValue;
                        }
                    }
                }
                var xmlString = (new XMLSerializer()).serializeToString(xmlDoc);
                hidTagsXml.value = xmlString;
                TemplateXml = xmlString;
            }
        }

        function AddNewTag()
        {
            var rows = document.getElementById("tbltags").rows;
            var id = rows.length - 1;
            var xmlDoc = getDocument();
            if (xmlDoc != null)
            {
                var documentNode = xmlDoc.createElement('document');
                var InnerNode = '<Id>' + id.toString() + '</Id><FileName></FileName><TagKey></TagKey><TagValue></TagValue>';
                documentNode.innerHTML = InnerNode;
                var MasterTags = xmlDoc.getElementsByTagName('Tags');
                if (MasterTags != null)
                {
                    MasterTags[0].appendChild(documentNode);
                    var xmlString = (new XMLSerializer()).serializeToString(xmlDoc);
                    TemplateXml = xmlString;
                    GenerateGrid();                    
                    Edit('TRE-' + id.toString());

                }
            }
        }

        function RemoveTag(id)
        {           
            
            id = id.split('-')[1];
            //id = parseInt(id) + 1;            
         
            var xmlDoc = getDocument();
            if (xmlDoc != null) {
                var ListOfElements = xmlDoc.getElementsByTagName('document');
                if (ListOfElements != null) {
                    for (var i = 0; i < ListOfElements.length; i++) {
                        if (i == parseInt(id))
                        {
                            xmlDoc.documentElement.removeChild(ListOfElements[i]);
                            break;                            
                        }
                    }
                }
                var xmlString = (new XMLSerializer()).serializeToString(xmlDoc);
                hidTagsXml.value = xmlString;
                TemplateXml = xmlString;
                GenerateGrid();
            }

        }

        GenerateGrid();

    </script>
</asp:Content>
