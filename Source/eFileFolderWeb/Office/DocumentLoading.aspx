<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    EnableSessionState="True" EnableViewState="true" CodeFile="DocumentLoading.aspx.cs"
    Inherits="DocumentLoading" Title="" %>

<%@ Register TagPrefix="fjx" Namespace="com.flajaxian" Assembly="com.flajaxian.FileUploader" %>
<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register TagPrefix="cust" Namespace="Shinetech.Engines.Adapters" Assembly="FormatEngine" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Document Loader</h1>
            <p>
                In order to add documents and files to your EdFiles, please select the folder
                from the drop down menu below, then select the tab in which you would like to place
                the document in and then select the file/document you would like to place in that
                tab of the edFile. It is that simple. If you need assistance, please feel free
                to chat, email or call customer service.
            </p>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div class="content-box listing-view">
                    <asp:Panel ID="panelMain" runat="server" Visible="true">
                        <div class="tekstDef">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Timer runat="server" ID="Timer1" Interval="4000" OnTick="Timer1_Tick"></asp:Timer>
                                    <asp:Label ID="label12" runat="server"></asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                                </Triggers>
                            </asp:UpdatePanel>

                        </div>
                    </asp:Panel>
                    <asp:Panel ID="wpanel" runat="server" Visible="false">
                        <div>The following PDF files can't convert to swf because of security setfings, please check them.</div>
                        <div class="tekstDef">
                            <asp:Label runat="server" ID="wrongpdfs">
                            </asp:Label>
                        </div>
                    </asp:Panel>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:Panel ID="panelConfirm" runat="server" Visible="false">
                                <fieldset>
                                    Document was uploaded successfully.
                                </fieldset>
                                <fieldset>
                                    <asp:Label ID="lbConfirm" runat="server" />
                                     Next Step: Do you want to
                                                <asp:HyperLink ID="btnUpload" runat="server" Text="Upload More Documents" NavigateUrl="~/Office/DocumentLoader1.aspx"></asp:HyperLink>
                                    or <a href="#" onclick="window.open('WebFlashViewer.aspx?V=2&ID=<%=FolderId %>', 'eBook', 'toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no'); return false;">View Your edFile Now</a> ?
                                </fieldset>
                            </asp:Panel>
                            <asp:Panel ID="PanelInfor" runat="server" Visible="false">
                                <fieldset>
                                    <asp:Label ID="Label1" runat="server" />There is no folder. please check the link to 
                                                <asp:HyperLink ID="HyperLink1" runat="server" Text="Create New edFile" NavigateUrl="~/Office/NeweFileFolder.aspx"></asp:HyperLink>
                                    right now.
                                </fieldset>
                            </asp:Panel>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <%--<div class="quick-find">
                    <uc1:QuickFind ID="QuickFind1" runat="server" />
                    <uc2:ReminderControl ID="ReminderControl1" runat="server" />
                </div>--%>
            </div>
        </div>
    </div>
</asp:Content>
