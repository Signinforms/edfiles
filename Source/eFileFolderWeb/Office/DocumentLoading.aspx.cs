using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using com.flajaxian;
using Shinetech.DAL;

public partial class DocumentLoading : LicensePage
{
    public string FolderId;

    protected void Page_Load(object sender, EventArgs e)
    {
        Session["LastFile"] = false;
        if (!string.IsNullOrEmpty(EffID))
        {
            if(IgnorePDF.Count>0)
            {
                wpanel.Visible = true;
                wrongpdfs.Text = IgnorePDF.ToString();
            }
            else
            {
                wpanel.Visible = false;
                wrongpdfs.Text = "";
            }
            string folderId = EffID;
            panelMain.Visible = true;
            PanelInfor.Visible = false;
            panelConfirm.Visible = true;
            btnUpload.NavigateUrl = btnUpload.NavigateUrl;
            FolderId = folderId;
        }

        if(!Page.IsPostBack)
        {
            Timer1.Enabled = true;
            if (Session["FileName"] != null && (string)Session["FileName"] != "")
            {
                label12.Text = string.Format("Converting {0}, Please waiting for ...", Session["FileName"]);
            }
        }
    }

    public ArrayList IgnorePDF
    {
        get
        {
            Object ars = HttpContext.Current.Session["_IgnoredPDFs"];
            if (ars == null)
            {
                ars = new ArrayList();
                HttpContext.Current.Session["_IgnoredPDFs"] = ars;
            }
            return ars as ArrayList;
        }
    }



    public DataTable GetEFileFolders()
    {
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        Account account = new Account(uid);
        string officeId;
        DataTable effTable = null;
        if (account.IsExist)
        {
            if(account.IsSubUser.Value.Equals("1"))
            {
                officeId = account.OfficeUID.ToString();
            }
            else
            {
                officeId = uid;
            }

            effTable = FileFolderManagement.GetFileFoldersWithDisplayName(uid, officeId);
        }
        
        return effTable;
    }

    public DataTable GetDividersByEFF(int eff)
    {
        DataTable dividerTable = FileFolderManagement.GetDividers(eff);
        return dividerTable;
    }



    public string DividerID
    {
        get
        {
            return (string)this.Session["DividerID"] ;
        }
        set
        {
            Session["DividerID"] = value;
        }
    }

    public string EffID
    {
        get
        {
            return (string)Session["EFFID"];
        }
        set
        {
            Session["EFFID"] = value;
           
        }
    }

    protected void Timer1_Tick(object sender, EventArgs e)
    {
        if (Session["FileName"] != null && (string)Session["FileName"]!="")
        {
            label12.Text = string.Format("Converting {0}, Please waiting for ...", Session["FileName"]);
        }

        if (Session["LastFile"] != null &&(bool) Session["LastFile"])
        {
            Session["LastFile"] = false;
            Session["FileName"] = "";
        }
    }
}
