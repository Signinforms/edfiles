using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Security.AccessControl;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Engines.Adapters;

public partial class Office_DocumentUploader : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            string path = Server.MapPath("~/Volume");

            string folderId = Request.Form["FolderID"];
            string tabId = Request.Form["TabID"];

            if(String.IsNullOrEmpty(folderId)||folderId =="0")
            {
                Response.Write("Folder value is invalid");
                Response.End();
            }

            if (String.IsNullOrEmpty(tabId) || folderId == "0")
            {
                Response.Write("Tab value is invalid");
                Response.End();
            }

            string userName = Request.Form["UserName"];
            string uid = UserManagement.ExistUserName(userName);
            if (uid == null)
            {
                Response.Write("The user doesn't exist");
                Response.End();
            }

            HttpFileCollection files = Request.Files;
            if (files.Count == 0)
            {   
                Response.End();
            }

            HttpPostedFile file = files[0];

            if (file != null && file.ContentLength > 0)
            {
                // flash 会自动发送文件名到 Request.Form["fileName"]
               
                string fileName = Request.Form["fileName"];
                string encodeName = HttpUtility.UrlPathEncode(file.FileName);
                encodeName = encodeName.Replace("%", "$");

                string pathname = string.Format("{0}\\{1}\\{2}", path, folderId, tabId);

                string savePath = string.Format("{0}\\{1}\\{2}\\{3}", path, folderId, tabId, encodeName);
                try
                {
                    if (!Directory.Exists(pathname))
                    {
                        try
                        {
                            Directory.CreateDirectory(pathname);
                        }
                        catch(Exception exp)
                        {
                            Response.Write(exp.Message);
                            Response.End();
                        }
                        
                    }
                    file.SaveAs(savePath);
                    ImageAdapter.ProcessPostFile(file, folderId, tabId, uid, pathname, fileName);
                }
                catch (Exception exp)
                {
                    Response.Write(exp.Message);
                    Response.End();
                }
               
            }

        }
    }
}
