﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Serialization;
using Shinetech.Framework;
using Ionic.Zip;
using System.IO.Compression;
using Shinetech.DAL;
using Shinetech.Engines;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;

public partial class EFFDownloader : System.Web.UI.Page
{
    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    string effid = Request.QueryString["ID"];
    //    DataTable dtFolderInfor = FileFolderManagement.GetFolderInforByEFFID(effid);

    //    XmlEFFolder objEFF = new XmlEFFolder();
    //    objEFF.head = new Head();
    //    objEFF.folders[0] = new Folder(Convert.ToInt32(effid));
    //    XmlSerializer xmls = new XmlSerializer(typeof(XmlEFFolder));
    //    MemoryStream ms = new MemoryStream();
    //    xmls.Serialize(ms, objEFF);
    //    byte[] buffer = new byte[ms.Length];
    //    ms.Read(buffer, 0,(int) ms.Length);
    //    Page.Response.Clear();

    //   // Response.AddHeader("Content-disposition", "attachment; filename=\"" + dtFolderInfor.Rows[0]["FolderName"].ToString() + ".eff\"");
    //    Response.AddHeader("Content-disposition", "attachment; filename=" + dtFolderInfor.Rows[0]["FolderName"].ToString() + ".eff");
    //    Response.ContentType = "text/eff";//"application/text-eff";
    //    //Response.Write(FileResponse);
    //    Response.OutputStream.Write(ms.GetBuffer(), 0, (int)ms.Length);
    //    //Response.Write(FileResponse);
    //    Response.End();   
    //    ms.Dispose();

    //}

    //protected void Page_Load(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        string effid = Request.QueryString["ID"];
    //        DataTable dtRecords = Common_Tatva.getDocumentByDeviderId(effid);
    //        ZipFile zip = new ZipFile();
    //        foreach (DataRow row in dtRecords.Rows)
    //        {
    //            string pathName = Convert.ToString(row["PathName"]);
    //            string file = Convert.ToString(row["Name"]);
    //            string ext = null;
    //            ext = Common_Tatva.GetExtension(Convert.ToInt16(row["FileExtention"]));
    //            var fileWithPath = Server.MapPath("~/" + pathName + "\\" + file + ext);

    //            if (File.Exists(fileWithPath) && !zip.ContainsEntry(row["DividerName"] + "/" + file + ext))
    //            {
    //                zip.AddFile(fileWithPath, Convert.ToString(row["DividerName"]));
    //            }
    //        }
    //        Response.Clear();
    //        Response.AddHeader("Content-Disposition", "attachment; filename=eff.zip");
    //        Response.ContentType = "application/zip";
    //        zip.Save(Response.OutputStream);
    //        Response.End();
    //    }
    //    catch (Exception ex)
    //    {

    //    }

    //}

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string id = this.Request.QueryString["id"];
            int boxid;
            if (!string.IsNullOrEmpty(id) && int.TryParse(id, out boxid))
            {

                FileFolder folder = new FileFolder(boxid);
                if (folder.IsExist)
                {
                    Shinetech.DAL.Account user = new Shinetech.DAL.Account(Sessions.SwitchedSessionId);
                    //Shinetech.DAL.Account user = new Shinetech.DAL.Account(Membership.GetUser().ProviderUserKey.ToString());
                    string basePath = this.MapPath(this.ArchiveBox);
                    if (!Directory.Exists(basePath))
                    {
                        Directory.CreateDirectory(basePath);
                    }

                    string userPath = Path.Combine(basePath, Page.User.Identity.Name);
                    if (!Directory.Exists(userPath))
                    {
                        Directory.CreateDirectory(userPath);
                    }

                    string folderPath = Path.Combine(userPath, folder.FolderName.Value + ".zip");

                    if (File.Exists(folderPath))
                    {
                        File.Delete(folderPath);
                    }

                    //Directory.CreateDirectory(folderPath); 
                    StringBuilder folderComment = new StringBuilder();
                    folderComment.AppendFormat("Folder Name : {0}\r\n", folder.FolderName.Value);
                    folderComment.AppendFormat("File Number : {0}\r\n", folder.FileNumber.Value);
                    folderComment.AppendFormat("Email  : {0}   ", folder.Email.Value);
                    folderComment.AppendFormat("Tel.   : {0}\r\n", folder.Tel.Value);
                    folderComment.AppendFormat("Alert 1: {0}\r\n", folder.Alert1.Value);
                    folderComment.AppendFormat("Alert 2: {0}\r\n", folder.Alert2.Value);
                    folderComment.AppendFormat("Comments: {0}\r\n", folder.Comments.Value);


                    //ZipFile zipfile = new ZipFile();
                    ZipStorer zip = ZipStorer.Create(folderPath, folderComment.ToString());

                    try
                    {

                        zip.EncodeUTF8 = true;

                        MemoryStream readme = new MemoryStream(
                            System.Text.Encoding.UTF8.GetBytes(folderComment.ToString()));

                        // Stores a new file directly from the stream                     
                        zip.AddStream(ZipStorer.Compression.Store, "readme.txt", readme, DateTime.Now, "Please read");

                        readme.Close();


                        DataTable dividers = this.DividerSource;
                        DataTable documents = this.DataSource;

                        foreach (DataRow item in dividers.Rows)
                        {
                            #region 数据文件拷贝
                            string dividerText = (string)item["Name"];

                            //获取该divider下的所有文档并从原路径中拷贝过来
                            //在archives目录下创建该用户登录名的folder
                            foreach (DataRow docRow in documents.Rows)
                            {
                                if (docRow["DividerID"].Equals(item["DividerID"]))
                                {

                                    string docName = (string)docRow["Name"];
                                    string docPath = (string)docRow["PathName"];

                                    docPath = Path.Combine("~/", docPath);
                                    string dcoFullPath = this.MapPath(docPath);

                                    //文档编码后的名称,即保存在磁盘上的文档名称
                                    string encodeName1 = HttpUtility.UrlPathEncode(docName);
                                    encodeName1 = encodeName1.Replace("%", "$");
                                    string docEncodeFullName = Path.Combine(dcoFullPath, encodeName1 + ".pdf"); // 其他的格式现在不管

                                    if (File.Exists(docEncodeFullName))
                                    {
                                        //dividerText为zip中的子目录名称
                                        string targetFileNameInZip = Path.Combine(dividerText, docName + Path.GetExtension(docEncodeFullName));
                                        //添加该文件到压缩文件中
                                        zip.AddFile(ZipStorer.Compression.Store, docEncodeFullName, targetFileNameInZip, "");
                                    }
                                }
                            }
                            #endregion 数据文件拷贝
                        }


                        zip.Close();
                        // zip.Dispose();
                        zip = null;



                        //压缩打包成一个rar文件后，输出为stream到客户端
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.Expires = 0;
                        Response.Buffer = true;

                        Response.AddHeader("Content-disposition", "attachment; filename=\"" + Path.GetFileName(folderPath) + "\"");
                        Response.ContentType = "application/zip";
                        Response.WriteFile(folderPath);
                        //byte[] buffer = File.ReadAllBytes(folderPath);
                        //Response.OutputStream.Write(buffer, 0, buffer.Length);
                        Response.Flush();
                        Response.End();

                        //完成后删除该Folder,l放到垃圾箱
                        // FileFolderManagement.RecycleFolderByID(id);
                    }
                    catch (Exception exp)
                    {
                        Console.WriteLine(exp.Message);
                    }
                    finally
                    {
                        if (zip != null)
                        {
                            zip.Close();
                            zip = null;
                        }
                    }

                }
            }
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource_FolderBox"] as DataTable;
        }
        set
        {
            this.Session["DataSource_FolderBox"] = value;
        }
    }

    public DataTable DividerSource
    {
        get
        {
            return this.Session["DividerSource_FolderBox"] as DataTable;
        }
        set
        {
            this.Session["DividerSource_FolderBox"] = value;
        }
    }

    public string ArchiveBox
    {
        get
        {
            return "~/ArchiveBox";
        }

    }

    protected string GetFileExtention(StreamType ext)
    {
        string extname = ".pdf";
        switch (ext)
        {
            case StreamType.PDF:
                extname = ".pdf";
                break;
            case StreamType.Word:
                extname = ".doc";
                break;
            case StreamType.Excel:
                extname = ".xls";
                break;
            case StreamType.PPT:
                extname = ".ppt";
                break;
            case StreamType.RTF:
                extname = ".rtf";
                break;
            case StreamType.JPG:
                extname = ".jpg";
                break;
            case StreamType.PNG:
                extname = ".png";
                break;
            default:
                break;
        }

        return extname;
    }
}
