﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EdForms.aspx.cs" Inherits="Office_EdForms" %>

<%@ Register Src="~/Office/UserControls/ucEdForms.ascx" TagPrefix="uc3" TagName="ucEdForms" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/style.css?v=3" rel="Stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/colorbox.css" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>css/main.css?v=2" rel="stylesheet" />
    <link rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>lib/TreeView/style.min.css" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/js/edFiles/EdFilesLogs.js?v=4" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/js/jquery.colorbox-min.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>extras/jquery-ui-1.8.22.custom.min.js"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style type="text/css">
        .form-containt .content-box fieldset {
            font-size: initial;
        }

        .pagerOffice > select {
            height: 25px;
            margin-right: -8px;
        }

        .pagerAdmin {
            margin-right: -3px;
        }

        .lblEdFormsName {
            color: blue;
            cursor: pointer;
            text-decoration: underline;
        }

        .popupcontent {
            padding: 10px;
        }

        .preview-popup {
            width: 100%;
            max-width: 1300px;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 10%;
            right: 0;
            margin: auto;
            background: #fff;
            height: 85%;
        }

        .preview-popup-field {
            width: 15%;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 40%;
            right: 0;
            margin: auto;
            background: #fff;
            height: auto;
        }

        .chosen-container-single .chosen-single {
            line-height: 33px !important;
        }

        .chosen-single {
            height: 30px !important;
        }

        .ui-dialog {
            top: 200px !important;
        }

        .imgBtn {
            background: url(../images/plus.png) 90px 5px no-repeat;
            border-radius: 5px;
            border: none;
            cursor: pointer;
            border: solid 1px;
            background-color: white;
            height: 30px;
            /* margin-top: -20px; */
            /* padding-left: 8px; */
            font-size: 16px;
            background-origin: padding-box;
            padding-top: 1px;
            padding-left: 10px;
            padding-right: 30px;
            padding-bottom: 3px;
        }

        .ic-close {
            float: right;
            padding: 10px;
            cursor: pointer;
        }

        #drpGroups {
            float: right;
            margin-right: 5px;
        }

        .txtEmails {
            float: right;
            height: 30px;
            margin-right: 5px;
            padding-left: 5px;
        }

        #groupList {
            width: 100%;
            display: none;
            border: 1px #dadada solid;
            position: absolute;
            left: 0;
            top: 100%;
            z-index: 9;
            background: #fff;
            max-height: 235px;
            overflow-y: auto;
            font-size: 12px;
        }

            #groupList label {
                display: block;
                padding: 0 5px;
                width: auto;
                min-width: initial;
                cursor: pointer;
                margin-top: 3px;
            }

                #groupList label:hover {
                    background-color: #e5e5e5;
                }

            #groupList input {
                display: inline-block;
                vertical-align: middle;
            }

        .selectBox-outer {
            position: relative;
            /*display: inline-block;*/
            margin-right: 5px;
            font-family: 'open sans', sans-serif;
        }

        .selectBox-outermail {
            position: relative;
            font-family: 'open sans', sans-serif;
        }

        .selectBox select {
            width: 100%;
        }

        .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            font-size: 15px;
        }

        #drpGroups_chosen {
            display: block;
            margin-top: 5px;
        }

        #drpGroups_chosen {
            display: block;
            margin-top: 5px;
        }

        #mailGroups_chosen {
            display: block;
        }

        #mailGroups_chosen {
            display: block;
        }

        .btn-sm {
            width: 60px;
            height: 30px;
            line-height: 30px;
            margin: 10px;
        }

        .btnSignature {
            width: auto !important;
        }

        .lblInformation, .lblTextField, .lblSignatureField {
            font-size: 20px;
            right: 0px;
            margin-left: 15%;
            line-height: 35px;
            color: black;
        }

        .chosen-container-multi .chosen-choices {
            overflow-y: auto !important;
            max-height: 200px;
        }

        .btnFormField {
            background: url(../images/plus-icon.png) 5px 10px no-repeat;
            padding-left: 30px;
            width: auto;
        }

        .btnActive {
            background-color: #01487f !important;
        }

        .extraLbl {
            font-size: 15px;
            float: right;
            line-height: 25px;
            padding-right: 10px;
        }
    </style>

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>EdForms</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <uc3:ucEdForms ID="ucEdForms" runat="server" />
    <div class="popup-mainbox preview-popup" id="preview_popup_edForms" style="display: none">
        <div class="popupcontrols" style="width: 100%; height: 5%;">
            <span id="popupclose_edForms" class="ic-icon ic-close"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%;">
            <table style="width: 100%; height: 100%">
                <tr style="height: 5%">
                    <td style="padding-bottom: 10px;">
                        <input type="button" id="startSignature" class="btn green btnSignature" value="Add Fields" onclick="StartSignature();" />
                        <input type="button" id="stopSignature" class="btn green btnSignature" value="Discard Changes" onclick="StopSignature();" style="display: none" />
                        <asp:Button runat="server" ID="btnSaveSignature" CssClass="btn green btnSignature" Text="Save" OnClick="btnSaveSignature_Click" OnClientClick="return OnAddingSignature();" ToolTip="Save Signature" Style="display: none; margin-left: 10px;" />
                        <asp:Label ID="lblTextField" Style="display: none;" CssClass="lblTextField" runat="server">Please click on any part of the pdf to add a Text Field.</asp:Label>
                        <asp:Label ID="lblSignatureField" Style="display: none;" CssClass="lblSignatureField" runat="server">Please click on any part of the pdf to add a Signature Field.</asp:Label>
                        <asp:Label ID="lblInformation" Style="display: none;" CssClass="lblInformation" runat="server">Please click Form Field button and select type of Field you want to add.</asp:Label>
                        <asp:Button runat="server" ID="btnFormField" CssClass="btn green btnFormField" Text="Form Field" OnClientClick="return OnAddingFormField();" ToolTip="Add Form Field" Style="display: none; float: right" />
                        <asp:Button ID="btnShareFile" Text="Tab" CssClass="ic-icon ic-share ic-sign-share" runat="server" OnClientClick="return OnSignShare();" Style="float: right; background-size: 30px; border: none; margin-top: 10px; cursor: pointer" ToolTip="Share EdForm"></asp:Button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <iframe id="reviewContent_edForms" style="width: 100%; height: 100%;"></iframe>
                        <%--<radPdf:PdfWebControl id="PdfWebControl1" runat="server" height="600px" width="100%" />--%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="popup-mainbox preview-popup-field" id="form_field_select" style="display: none">
        <div class="popupcontent">
            <table style="width: 100%; height: 100%">
                <tr style="height: 5%">
                    <td width="30%">
                        <label class="extraLbl">Type:</label></td>
                    <td style="padding-bottom: 10px; width: 50%">
                        <select id="drpType" style="height: 30px; width: 95%;" class="text ui-widget-content ui-corner-all" onchange="OnFieldTypeChange();">
                            <option value="1">Text</option>
                            <option value="2">Signature</option>
                        </select>
                    </td>
                </tr>
                <tr id="extraName">
                    <td width="30%">
                        <label class="extraLbl">Field Name:</label></td>
                    <td width="50%">
                        <input id="extraFieldName" class="text ui-widget-content ui-corner-all" type="text" placeholder="field name" /></td>
                </tr>
                <tr>
                    <td style="padding-top: 10px; padding-right: 10px; width: 50%">
                        <asp:Button runat="server" ID="btnSelectField" CssClass="btn-small green btnSelectField" Text="Select" OnClientClick="return OnSelectingField();" ToolTip="Select" Style="float: right" />
                    </td>
                    <td style="padding-top: 10px; padding-right: 10px; width: 50%">
                        <asp:Button runat="server" ID="btnCancelField" CssClass="btn-small green btnCancelField" Text="Cancel" OnClientClick="return OnCancelingField();" ToolTip="Cancel" Style="float: left" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <asp:HiddenField ID="hdnEdFormsIds" runat="server" />
    <asp:HiddenField ID="hdnPath" runat="server" />
    <asp:HiddenField ID="hdnCords" runat="server" />
    <asp:HiddenField ID="hdnFileId" runat="server" />
    <asp:HiddenField ID="hdnFileName" runat="server" />
    <asp:HiddenField ID="hdnIsSignature" runat="server" />
    <div class="popup-mainbox preview-popup" id="preview_edForms" style="display: none; height: auto !important;">
        <div class="popupcontrols" style="width: 100%; height: 5%;">
            <asp:LinkButton ID="btnAdd" CssClass="btn green btn-sm" ToolTip="Add To EdForms" runat="server" Text="ADD" OnClick="btnAdd_Click" OnClientClick="return CheckSelected(this);"></asp:LinkButton>
            <span id="popupclose1" class="ic-icon ic-close"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: auto">
            <asp:GridView runat="server" ID="EdFormsAdminGrid" CssClass="datatable listing-datatable boxWidth edFormsAdminGrid"
                AllowSorting="true" AutoGenerateColumns="false" BorderWidth="2" OnSorting="EdFormsAdminGrid_Sorting"
                OnPageIndexChanging="EdFormsAdminGrid_PageIndexChanging" PageSize="5" DataKeyNames="EdFormsId">
                <PagerSettings Visible="False" />
                <AlternatingRowStyle CssClass="odd" />
                <RowStyle CssClass="even" HorizontalAlign="Center" />
                <Columns>
                    <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                        ItemStyle-CssClass="table_tekst_edit">
                        <HeaderTemplate>
                            <asp:CheckBox Text="" ID="chkSelectAll" CssClass="parentCheck" runat="server" onclick="checkAll(this);" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                            <asp:HiddenField runat="server" ID="hdnEdFormsId" Value='<%#  Convert.ToInt32(Eval("EdFormsId")) %>' />
                            <asp:HiddenField runat="server" ID="hdnFileName" Value='<%#  Convert.ToString(Eval("Name"))%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" ItemStyle-Wrap="false" SortExpression="Name">
                        <HeaderTemplate>
                            <asp:LinkButton ID="lblDocumentName1" runat="server" Text="Form Name" CommandName="Sort" CommandArgument="Name" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("Name")%>' name="DocumentName" CssClass="lblDocumentName" />
                            <asp:TextBox runat="server" MaxLength="50" Text='<%#Eval("Name")%>'
                                ID="TextDocumentName" Style="display: none; height: 30px;" name="txtDocumentName" CssClass="txtDocumentName" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Size" HeaderText="Size(Mb)" DataFormatString="{0:F2}" ItemStyle-Width="15%"
                        HtmlEncode="false" SortExpression="Size" />
                    <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" DataFormatString="{0:MM.dd.yyyy}" ItemStyle-Width="15%"
                        SortExpression="CreatedDate" />
                </Columns>
                <EmptyDataTemplate>
                    <asp:Label ID="Label1" runat="server" CssClass="Empty" />There is no EdForms to be added for selected department.
               
                </EmptyDataTemplate>
            </asp:GridView>
            <cc1:WebPager ID="WebPager2" runat="server" Style="float: right" OnPageIndexChanged="WebPager2_PageIndexChanged" OnPageSizeChanged="WebPager2_PageSizeChanged"
                PageSize="5" CssClass="paging_position pagerAdmin" PagerStyle="NumericPages" ControlToPaginate="EdFormsAdminGrid"></cc1:WebPager>
            <asp:HiddenField ID="SortDirectionAdmin" runat="server" />
        </div>
    </div>
    <div class="inner-wrapper">
        <div class="page-container form-containt">
            <div class="dashboard-container">
                <asp:Panel ID="panelMain" runat="server" Visible="true">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <fieldset style="font-size: 20px; font-family: 'Segoe UI'; font-weight: normal">
                                <asp:HiddenField ID="hdnDepartmentId" runat="server" />
                                <asp:Label ID="Label2" Style="font-size: 15px;" runat="server" Text="Select Department"></asp:Label>
                                <br />
                                <select id="comboBoxDepartment" style="color: rgb(68,68,68); border-radius: 5px; background-color: #fff; border: 1px solid #aaa; box-shadow: 0 1px 0 #fff inset; background: linear-gradient(#fff 20%, #f6f6f6 50%, #eee 52%, #f4f4f4 100%)">
                                </select>
                                <asp:LinkButton Text="New Form" runat="server" CssClass="imgBtn" ToolTip="Add New EdForms" ID="btnAddNew" OnClientClick="return OpenModalPopup();" />
                                <asp:LinkButton ID="lnkBtnShare" OnClientClick="return OnShowShare(false);" runat="server" CssClass="ic-icon ic-share shareDocument" CausesValidation="False" Height="28px" Width="28px" ToolTip="Share File" Style="float: right; margin-top: 2px;">
                                </asp:LinkButton>

                                <div id="boxToMail" runat="server" class="selectBox-outer" style="width: 315px; display: inline-block; float: right">
                                    <div class="selectBox " onclick="showCheckboxesPublicHolidays()">
                                        <select id="drpGroups" data-placeholder="Select Group to send Mail">
                                            <option>Select Group to send Mail</option>
                                        </select>
                                        <div class="overSelect"></div>
                                    </div>
                                    <div id="groupList" style="display: none;"></div>
                                </div>
                            </fieldset>
                            <div class="content-box">
                                <div id="uploadProgress" class="white_content" style="font-size: 14px;"></div>
                                <fieldset id="uploadFieldset">
                                    <div id="divUpload">
                                        <div class='clearfix'></div>
                                        <asp:Image runat="server" ID="Imagelabel12" ImageUrl="../Images/loader_bg.gif" ImageAlign="Middle" />
                                        <asp:Label ID="label12" runat="server"></asp:Label>
                                        <asp:Label runat="server" ID="myThrobber" Style="display: none;"><img align="absmiddle" alt="" src="uploading.gif"/></asp:Label>
                                        <ajaxToolkit:AjaxFileUpload ID="AjaxFileUpload1" runat="server" Padding-Bottom="4"
                                            Padding-Left="2" Padding-Right="1" Padding-Top="4"
                                            ThrobberID="myThrobber"
                                            ContextKeys="2"
                                            MaximumNumberOfFiles="10"
                                            AzureContainerName=""
                                            AllowedFileTypes="jpg,jpeg,pdf,doc,docx,xls,avi"
                                            OnUploadComplete="AjaxFileUpload1_UploadComplete"
                                            OnClientUploadComplete="onClientUploadComplete"
                                            OnUploadCompleteAll="AjaxFileUpload1_UploadCompleteAll"
                                            OnClientUploadCompleteAll="onClientUploadCompleteAll"
                                            OnUploadStart="AjaxFileUpload1_UploadStart"
                                            OnClientUploadStart="onClientUploadStart" />

                                        <%--<asp:Button ID="btnRefreshGrid" runat="server" Style="display: none" OnClick="btnRefreshGrid_Click" />--%>
                                        <div class='clearfix'></div>
                                        <div id="uploadCompleteInfo">
                                        </div>
                                </fieldset>
                            </div>
                            <asp:GridView Style="margin-top: 15px;" ID="EdFormsGrid" runat="server" AllowSorting="True" DataKeyNames="EdFormsUserId"
                                AutoGenerateColumns="false" BorderWidth="2" OnRowDataBound="EdFormsGrid_RowDataBound"
                                CssClass="datatable listing-datatable boxWidth edFormsGrid" OnSorting="EdFormsGrid_Sorting">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="odd" />
                                <RowStyle CssClass="even" HorizontalAlign="Center" />
                                <Columns>

                                    <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                        ItemStyle-CssClass="table_tekst_edit">
                                        <HeaderTemplate>
                                            <asp:CheckBox Text="" ID="chkSelectAll" runat="server" CssClass="parentChk" onclick="checkAll(this);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%--convert label into textbox--%>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" ItemStyle-Wrap="false" SortExpression="EdFormsUserId">
                                        <HeaderTemplate>
                                            <%--<asp:Label ID="lblDocumentName" runat="server" Text="Document Name" />--%>
                                            <asp:LinkButton ID="lblDocumentName1" runat="server" Text="Form Name" CommandName="Sort" CommandArgument="EdFormsUserId" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("Name")%>' name="DocumentName" CssClass="lblEdFormsName" />
                                            <asp:TextBox runat="server" MaxLength="50" Text='<%#Eval("Name")%>'
                                                ID="TextDocumentName" Style="display: none; height: 30px; width: 400px; padding-left: 10px;" name="txtDocumentName" CssClass="txtDocumentName" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--end--%>

                                    <asp:BoundField DataField="Size" HeaderText="Size(Mb)" DataFormatString="{0:F2}" ItemStyle-Width="15%"
                                        HtmlEncode="false" SortExpression="Size" />
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" DataFormatString="{0:MM.dd.yyyy}" ItemStyle-Width="15%"
                                        SortExpression="CreatedDate" />

                                    <%--rename start--%>
                                    <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="20%"
                                        ItemStyle-CssClass="table_tekst_edit actionMinWidth">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblAction" runat="server" Text="Action" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton Style="padding: 3px;" ID="lnkBtnEdit" runat="server" CausesValidation="false" Text="" CssClass="ic-icon ic-edit editDocument"
                                                ToolTip="Rename File" CommandArgument='<%#string.Format("{0}", Eval("EdFormsUserId"))%>' />
                                            <asp:LinkButton Text="" ID="lnkBtnUpdate" runat="server" CausesValidation="True" AutoPostBack="False"
                                                ToolTip="Update" OnClientClick='<%# "return OnSaveRename(this, " + Eval("EdFormsUserId") + ",\"" + Eval("Name") + "\")"%>' CssClass="ic-icon ic-save saveDocument" Style="display: none; padding: 3px;" name="lnkUpdate"></asp:LinkButton>
                                            <asp:LinkButton Text="" ID="lnkBtnCancel" runat="server" CausesValidation="False" AutoPostBack="False"
                                                OnClientClick="return cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancelDocument" Style="display: none; padding: 3px;" name="lnkcancel"></asp:LinkButton>
                                            <asp:LinkButton Style="padding: 3px;" ID="lnkBtnDelete" runat="server" ToolTip="Delete File" CausesValidation="false" Text="" CssClass="ic-icon ic-delete delete" OnClientClick="return ConfirmDelete()"
                                                OnCommand="lnkBtnDelete_Command" CommandArgument='<%#string.Format("{0};{1}", Eval("EdFormsUserId"), Eval("Name"))%>' />
                                            <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                CancelControlID="EdDeleteCancel" OkControlID="ButtonDeleleEdFormOkay" TargetControlID="lnkBtnDelete"
                                                PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                            </ajaxToolkit:ModalPopupExtender>
                                            <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                ConfirmText="" TargetControlID="lnkBtnDelete"></ajaxToolkit:ConfirmButtonExtender>
                                            <asp:LinkButton ID="lnkBtnShare" OnClientClick='<%# "return OnSingleShare(" + Eval("EdFormsUserId") + ",\"" + Eval("Name") + "\")"%>' runat="server" CssClass="ic-icon ic-share shareDocument" CausesValidation="False" Height="28px" Width="28px" ToolTip="Share File">
                                            </asp:LinkButton>
                                            <asp:LinkButton Style="padding: 3px;" ID="LinkButton1" runat="server" CausesValidation="false" Text="" CssClass="ic-icon ic-preview viewDocument"
                                                ToolTip="View File" OnClientClick='<%# "return ShowPreviewForEdForms(" + Eval("EdFormsUserId") + ",\"" + Eval("Name") + "\")"%>' />
                                            <asp:HyperLink ID="lnkBtnDownload" ToolTip="Download File" runat="server" CausesValidation="false" CssClass="ic-icon ic-download downloadFile" Target="_blank" />
                                            <asp:HiddenField runat="server" ID="hdnEdFormsUserId" Value='<%# Eval("EdFormsUserId")%>' />
                                            <asp:HiddenField runat="server" ID="hdnFileName" Value='<%#Eval("Name")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--rename end--%>
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label1" runat="server" CssClass="Empty" />There is no EdForms.
                               
                                </EmptyDataTemplate>
                            </asp:GridView>

                            <div class="prevnext" bgcolor="#f8f8f5" style="float: right; padding: 5px;">
                                <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                    PageSize="10" CssClass="paging_position pagerOffice" PagerStyle="NumericPages" ControlToPaginate="EdFormsGrid"></cc1:WebPager>
                                <asp:HiddenField ID="FolderID" runat="server" />
                                <asp:HiddenField ID="SortDirectionOffice" runat="server" />
                            </div>
                            <br />
                            <br />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
        </div>
    </div>

    <%--Delete File--%>
    <asp:Panel class="popup-mainbox" ID="DivDeleteConfirmation" Style="display: none"
        runat="server">
        <div>
            <div class="popup-head" id="PopupHeader">
                Delete File
           
            </div>
            <div class="popup-scroller">
                <p>
                    Caution! Are you sure you want to delete this file. Once you delete this file, it
                                                        will be deleted for good
               
                </p>
                <p>
                    Confirm Password:
                                      
                    <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                </p>
            </div>
            <div class="popup-btn">
                <input id="ButtonDeleleEdFormOkay" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />
                <input id="EdDeleteCancel" type="button" value="No" class="btn green" />
            </div>
        </div>
    </asp:Panel>
    <%--end Delete--%>

    <%--Share Document--%>
    <div id="dialog-share-book-edForm" title="Share this edForm" style="display: none; overflow: hidden !important">
        <p class="validateTips">All form fields are required.</p>
        <br />
        <label for="email">From Email</label><br />
        <input type="email" name="email" id="email" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <label for="emailto">To Email</label><br />
        <input type="email" name="emailto" id="emailsto" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" placeholder="Enter Email separated with';'" /><br />
        <label for="emailto">To Email Group</label><br />
        <div class="selectBox-outer ui-widget-content ui-corner-all folderList" style="width: 55%; margin-bottom: 12px; display: inline-block;">
            <div class="selectBox">
                <select id="comboBoxFolder" multiple class="text ui-widget-content ui-corner-all" style="border: 1px solid black;">
                </select>
            </div>
            <div id="folderList" style="display: none; margin-top: -1px; border-radius: 0 0 4px 4px; background-clip: padding-box"></div>
            <asp:HiddenField runat="server" ID="hdnFolderID" />
        </div>
        <button type="button" class="chosen-toggle select btn-small green" style="bottom: 0; height: 30px; width: 65px;">
            Select all
       
        </button>
        <button type="button" class="chosen-toggle deselect btn-small green" style="bottom: 0; height: 30px; width: 81px;">Deselect all</button><br />
        <label for="nameto">To Name</label><br />
        <input type="text" name="nameto" id="namesto" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" placeholder="Enter Name separated with';'" /><br />
        <label for="subject">Subject</label><br />
        <input type="text" name="title" id="title" value="EdForm Share" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" readonly="readonly" /><br />
        <label for="content">Message</label><br />
        <textarea name="content" id="content" rows="2" cols="25" class="text ui-widget-content ui-corner-all" style="border: 1px solid black"></textarea><br />
        <label for="users">Send Received Alert</label><br />
        <div class="selectBox-outer ui-widget-content ui-corner-all" style="width: 95%; margin-bottom: 12px">
            <div class="selectBox">
                <select id="comboBoxUser" multiple class="text ui-widget-content ui-corner-all" style="border: 1px solid black">
                </select>
            </div>
        </div>
        <br />
        <br />
        <label for="content">Link Expiration Time (In Hrs)</label><br />
        <input type="text" name="linkexpiration" id="expirationTime" class="text ui-widget-content ui-corner-all" style="border: 1px solid black; width: 80%;" />
        <input type="button" id="updateExpiration" onclick="UpdateExpirationTime()" class="btn-small green" style="float: right" value="UPDATE" /><br />
        <span id="errorMsg"></span>
    </div>
    <div id="dialog-message-all-edForm" title="" style="display: none;">
        <p id="dialog-message-content">
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span>
            <span id="spanTextEdForm"></span>
        </p>
    </div>
    <%--End Share Document--%>

    <script type="text/javascript">
        var efileName, pathForSharemail;
        var edFormsUsers = [];
        var isSingleShare = false;
        var pubExpanded = false;
        $(document).ready(function (e) {
            //$('#dialog-share-book-edForm input[name="title"]').css('background-color', 'black');
            $('#title').css("background-color", "blue")
            $('#dialog-share-book-edForm').dialog({
                autoOpen: false,
                width: 450,
                resizable: false,
                modal: true,
                buttons: {
                    "Share Now": function () {
                        SaveMail();
                    },
                    "Close": function () {
                        $(this).dialog("close");
                    }
                },
                close: function () {
                }
            });

            $('.ui-dialog-buttonset').find('button').each(function () {
                $(this).find('span').text($(this).attr('text'));
            })

            $("#dialog-message-all-edForm").dialog({
                autoOpen: false,
                resizable: false,
                modal: true,
                Ok: function () {
                    $(this).dialog("close");
                },
                close: function (event, ui) {
                    $(this).dialog("option", 'title', '');
                    $('#dialog-confirm-content').empty();
                }
            });
        });

        function CheckValidation(fieldObj) {
            if (fieldObj.val() == "") {
                fieldObj.css('border', '1px solid red');
            }
            else {
                fieldObj.css('border', '1px solid black');
            }
        }

        function CheckEmailValidation(fieldObj) {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(fieldObj)) {
                return true;
            }
            else {
                return false;
            }
        }

        function OnSignShare() {
            edFormsUsers = [];
            var popup = document.getElementById("preview_popup_edForms");
            edFormsUsers.push({ "EdFormsUserId": $("#<%= hdnFileId.ClientID %>").val(), "FileName": $("#<%= hdnFileName.ClientID %>").val() });
            popup.style.display = "none";
            hideLoader();
            enableScrollbar();
            return OnShowShare(true);
        }

        function OnSingleShare(edFormsUserId, fileName) {
            edFormsUsers = [];
            edFormsUsers.push({ "EdFormsUserId": edFormsUserId, "FileName": fileName });
            return OnShowShare(true);
        }

        function OnShowShare(singleShare) {
            if (edFormsUsers.length == 0 && $('.edFormsGrid input[type="checkbox"]:checked').length <= 0) {
                alert("Please Select Atleast one document.");
                hideLoader();
                return false;
            }
            isSingleShare = singleShare;
            $('#dialog-share-book-edForm').dialog("open");
            $('#dialog-share-book-edForm input[name="emailto"]').val('');
            $("#comboBoxFolder").val('').trigger('chosen:updated');
            $("#comboBoxUser").val('').trigger('chosen:updated');
            $('#dialog-share-book-edForm input[name="nameto"]').val('');
            $('#dialog-share-book-edForm input[name="email"]').val('<%=Sessions.SwitchedEmailId%>');
            $('#dialog-share-book-edForm input[name="linkexpiration"]').val('<%= Sessions.ExpirationTime%>');
            //$('#dialog-share-book-edForm input[name="title"]').val('')
            $('#dialog-share-book-edForm textarea[name="content"]').val('');
            $('#dialog-share-book-edForm input[name="emailto"]').css('border', '1px solid black');
            return false;
        }

        function SaveMail() {
            showLoader();
            ids = "";
            var selectedEmails = $('#comboBoxFolder_chosen .chosen-choices').children();
            var selectedUsers = $('#comboBoxUser_chosen .chosen-choices').children();
            var folders = "";
            var users = [];
            if (isSingleShare) { }
            else if ($('.edFormsGrid input[type="checkbox"]:checked').length > 0) {
                edFormsUsers = [];
                $('.edFormsGrid input[type="checkbox"]:checked').each(function () {
                    if ($(this).closest('tr').find("input[type='hidden'][id$=hdnEdFormsUserId]").val() != undefined) {
                        var id = $(this).closest('tr').find("input[type='hidden'][id$=hdnEdFormsUserId]").val();
                        var fileName = $(this).closest('tr').find("input[type='hidden'][id$=hdnFileName]").val();
                        edFormsUsers.push({ "EdFormsUserId": id, "FileName": fileName });
                    }
                });
            }
            else {
                alert("Please Select Atleast one document.");
                hideLoader();
                return false;
            }

            if (selectedEmails.length > 1) {
                for (var i = 0; i < selectedEmails.length - 1; i++) {
                    if ($(selectedEmails[i])) {
                        folders += $($('#comboBoxFolder').find('option')[$(selectedEmails[i]).find('a').attr('data-option-array-index')]).val() + ", ";
                    }
                }
                folders = folders.substr(0, folders.lastIndexOf(','));
            }

            if (selectedUsers.length > 1) {
                for (var i = 0; i < selectedUsers.length - 1; i++) {
                    if ($(selectedUsers[i])) {
                        var uid = $($('#comboBoxUser').find('option')[$(selectedUsers[i]).find('a').attr('data-option-array-index')]).val();
                        users.push({ "UID": uid });
                    }
                }
            }

            $("#" + '<%=hdnFolderID.ClientID%>').val(folders);
            var nameto = $('#dialog-share-book-edForm input[name="nameto"]').val();
            var emails = $('#dialog-share-book-edForm input[name="emailto"]').val();
            var emailgroups = folders;
            var email = $('#dialog-share-book-edForm input[name="email"]').val();
            var title = $('#dialog-share-book-edForm input[name="title"]').val();
            var emailList = "", emailsList = "";
            if (emails.length > 0) {
                emailsList = emails.split(';');
                for (var i = 0; i < emailsList.length; i++) {
                    var test = "";
                    if (!CheckEmailValidation(emailsList[i])) {
                        alert("Please enter valid Email separated by ';' !")
                        $('#dialog-share-book-edForm input[name="emailto"]').css('border', '1px solid red');
                        hideLoader();
                        return false;
                    }
                }
            }

            if (emailgroups.length == 0 && emails.length == 0) {
                alert("Please select any type of Email");
                hideLoader();
                return false;
            }



            $('#dialog-share-book-edForm input[name="emailto"]').css('border', '1px solid black');

            var nameToList = "";
            if (nameto.length > 0) {
                var isValid = /^[a-zA-Z\;\s]+$/.test(nameto);
                if (isValid)
                    nameToList = nameto.split(';');
                else {
                    alert("Please enter valid ToName separated by ';' !")
                    $('#dialog-share-book-edForm input[name="nameto"]').css('border', '1px solid red');
                    hideLoader();
                    return false;
                }
            }
            if (emailsList.length != nameToList.length && emailsList.length > 0) {
                alert("Number of Emails should match with number of ToNames!");
                $('#dialog-share-book-edForm input[name="nameto"]').css('border', '1px solid red');
                $('#dialog-share-book-edForm input[name="emailto"]').css('border', '1px solid red');
                hideLoader();
                return false;
            }

            if (emails.length > 0 && emailgroups.length > 0) {
                emailgroups = emails.split(';') + "," + emailgroups;
                emailList = emailgroups.split(',');
            }
            else if (emailgroups.length == 0 && emails.length > 0) {
                emailgroups = emails.split(';');
                emailList = emailgroups;
            }
            else
                emailList = emailgroups.split(',');
            $('#dialog-share-book-edForm input[name="emailto"]').css('border', '1px solid black');
            $('#dialog-share-book-edForm input[name="nameto"]').css('border', '1px solid black');

            var content = $('#dialog-share-book-edForm textarea[name="content"]').val();
            var Isvalid = false;
            var checkboxes = $("#groupList").find("input[type=checkbox]");
            var groups = "";
            if (checkboxes.length > 0) {
                for (var i = 1; i < checkboxes.length; i++) {
                    if ($(checkboxes[i]).prop("checked"))
                        groups += $(checkboxes[i]).val().trim() + ", ";
                }
                groups = groups.substr(0, groups.lastIndexOf(','));
            }

            var emailto = [];
            if (groups.length > 0) {

                $.ajax(
                    {
                        type: "POST",
                        url: '../FlexWebService.asmx/GetEmails',
                        contentType: "application/json; charset=utf-8",
                        data: '{"GroupID":"' + groups + '"}',
                        dataType: "json",
                        success: function (result) {

                            if (result.d.length > 0) {
                                var emails = JSON.parse(result.d);
                                if (emails.length > 0) {
                                    var toEmail = JSON.parse(emails);
                                    for (var i = 0; i < toEmail.length; i++) {
                                        emailto.push({ "Email": toEmail[i].Email, "ToName": toEmail[i].ToName });
                                    }
                                    SendMail(emailList, edFormsUsers, emailto, nameto, content, pathForSharemail, nameToList, email, title, users);
                                }
                            }
                        },
                        fail: function (data) {
                            alert("Failed to share document. Please try again!");
                            hideLoader();
                        }
                    });
            }
            else {
                if (emailList.length <= 0) {
                    alert("Please select Email Group or enter Email to share this document!");
                    hideLoader();
                    return false;
                }
                SendMail(emailList, edFormsUsers, emailto, nameto, content, pathForSharemail, nameToList, email, title, users);
            }
        }

        function SendMail(emailList, edFormsUsers, emailto, nameto, content, pathForSharemail, nameToList, email, title, users) {
            for (var i = 0; i < emailList.length; i++) {
                emailto.push({ "Email": emailList[i], "ToName": nameToList[i] });
            }
            var src = '';
            $.ajax(
                {
                    type: "POST",
                    url: '../EFileFolderJSONService.asmx/ShareMailForEdForms',
                    data: "{edFormsUserList:'" + JSON.stringify(edFormsUsers) + "',strToMail:'" + JSON.stringify(emailto) + "',MailTitle:'" + title + "',ToName:'" + nameto + "',SendName:'" + "" + "',strSendMail:'" + email + "',strMailInfor:'" + content + "',pathName:'" + pathForSharemail + "',departmentId:'" + $('#<%= hdnDepartmentId.ClientID%>').val() + "', usersList:'" + JSON.stringify(users) + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var errorMsg = data.d;
                        var title, message;
                        if (errorMsg != "") {
                            title = "Sharing EdForms document error";
                            message = errorMsg;
                        }
                        else {
                            title = "Sharing EdForms document success";
                            message = "This document has been shared successfully."
                        }

                        $('#dialog-message-all-edForm').dialog({
                            title: title,
                            open: function () {
                                $("#spanTextEdForm").text(message);
                            }
                        });

                        $('#dialog-message-all-edForm').dialog("open");

                        if (errorMsg == null || errorMsg == "")
                            $('#dialog-share-book-edForm').dialog("close");
                        hideLoader();
                    },
                    fail: function (data) {
                        bookshelf.loaded.apply();

                        $('#dialog-message-all-edForm').dialog({
                            title: "Sharing EdForms document error",
                            open: function () {
                                $("#spanTextEdForm").text("This document has not been shared successfully.");
                            }
                        });
                        $('#dialog-message-all-edForm').dialog("open");

                        hideLoader();
                    }
                });
        }

        function pageLoad() {
            LoadUsers();
            LoadDepartments();
            GetEmailGroups();
            GetEmailToSend();

            $("#comboBoxDepartment").change(function () {
                showLoader();
                $('#<%= hdnDepartmentId.ClientID%>').val($("#comboBoxDepartment").chosen().val());
                GetEdFormsUser();
            });

            $('.editDocument').click(function (e) {
                e.preventDefault();
                removeAllEditable();
                showEdit($(this));
            });

            $('.cancelDocument').click(function (e) {
                e.preventDefault();
                removeAllEditable();
            });

            $('.lblEdFormsName').click(function (e) {
                var edFormsUserId = $(this).parent().parent().find("input[type=hidden][id$=hdnEdFormsUserId]").val()
                $.ajax({
                    type: "POST",
                    url: 'EdForms.aspx/SetSessionForEdFormsUser',
                    contentType: "application/json; charset=utf-8",
                    data: "{ edFormsUserId:'" + edFormsUserId + "'}",
                    dataType: "json",
                    success: function () {
                        window.open("EdFormsLogs.aspx", "_blank");
                        //window.location.href = "EdFormsLogs.aspx";
                    },
                    error: function () {
                        alert("Some error occurred. Please try again later !!")
                    }
                });
            });

            $(".ajax__fileupload_dropzone").text("Drag and Drop Pdf file(s) here");

            var closePopup = document.getElementById("popupclose_edForms");
            closePopup.onclick = function () {
                var popup = document.getElementById("preview_popup_edForms");
                var overlay = document.getElementById("overlay");
                overlay.style.display = 'none';
                popup.style.display = 'none';
                $('#floatingCirclesG').css('display', 'block');
                enableScrollbar();
                $('#reviewContent_edForms').removeAttr('src');
                EnableAfterSignature();
            };

            var closePopup1 = document.getElementById("popupclose1");
            closePopup1.onclick = function () {
                var popup = document.getElementById("preview_edForms");
                var overlay = document.getElementById("overlay");
                overlay.style.display = 'none';
                popup.style.display = 'none';
                $('#floatingCirclesG').css('display', 'block');
                enableScrollbar();
                $(this).parent().parent().find("input[type=checkbox]").prop("checked", false);
            };
        }

        function Check_Click(objRef) {
            var row = objRef.parentNode.parentNode.parentNode;
            var GridView = row.parentNode;
            var inputList = GridView.getElementsByTagName("input");

            for (var i = 0; i < inputList.length; i++) {
                var headerCheckBox = inputList[0];
                var checked = true;
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                        break;
                    }
                }
            }
            headerCheckBox.checked = checked;
        }

        function checkAll(objRef) {
            var inboxGridView = objRef.parentNode.parentNode.parentNode.parentNode;
            var inboxinputList = inboxGridView.getElementsByTagName("input");

            for (var i = 0; i < inboxinputList.length; i++) {
                var row = inboxinputList[i].parentNode.parentNode;
                if (inboxinputList[i].type == "checkbox" && objRef != inboxinputList[i]) {
                    if (objRef.checked) {
                        inboxinputList[i].checked = true;
                    }
                    else {
                        inboxinputList[i].checked = false;
                    }
                }
            }
        }

        function checkAllPending(objPendingRef) {
            var pendingGridView = objPendingRef.parentNode.parentNode.parentNode.parentNode;
            var pendinginputList = pendingGridView.getElementsByTagName("input");

            for (var i = 0; i < pendinginputList.length; i++) {
                var pendingrow = pendinginputList[i].parentNode.parentNode;
                if (pendinginputList[i].type == "checkbox" && objPendingRef != pendinginputList[i]) {
                    if (objPendingRef.checked) {
                        pendinginputList[i].checked = true;
                    }
                    else {
                        pendinginputList[i].checked = false;
                    }
                }
            }
        }

        function Check_ClickPending(objPendingRef) {
            var pendingrow = objPendingRef.parentNode.parentNode.parentNode;
            var pendingGridView = pendingrow.parentNode;
            var pendinginputList = pendingGridView.getElementsByTagName("input");

            for (var i = 0; i < pendinginputList.length; i++) {
                var pendingheaderCheckBox = pendinginputList[0];
                var checked = true;
                if (pendinginputList[i].type == "checkbox" && pendinginputList[i] != pendingheaderCheckBox) {
                    if (!pendinginputList[i].checked) {
                        checked = false;
                        break;
                    }
                }
            }
            pendingheaderCheckBox.checked = checked;
        }

        function showPreviewUrl(URL) {
            hideLoader();
            disableScrollbar();
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//"
                    + window.location.hostname
                    + (window.location.port ? ':' + window.location.port : '');
            }
            var popup = document.getElementById("preview_popup_edForms");
            var overlay = document.getElementById("overlay");
            $('#reviewContent_edForms').attr('src', './Preview.aspx?data=' + window.location.origin + URL.trim() + "?v=" + "<%= DateTime.Now.Ticks%>");
            //$('#reviewContent').attr('src', src.trim());
            overlay.style.display = 'block';
            popup.style.display = 'block';
            $('#floatingCirclesG').css('display', 'none');
            //$('#preview_popup').css('top', ($(window).scrollTop() + 50) + 'px');
            $('#preview_popup_edForms').focus();
            EnableAfterSignature();
            return false;
        }

        function ShowPreviewForEdForms(edFormsUserId, fileName) {
            showLoader();
            $("#<%= hdnFileName.ClientID %>").val(fileName);
            $("#<%= hdnFileId.ClientID %>").val(edFormsUserId);
            var folderID = <%= (int)Enum_Tatva.Folders.EdForms%>
                $.ajax({
                    type: "POST",
                    url: 'EdForms.aspx/GetPreviewURL',
                    contentType: "application/json; charset=utf-8",
                    data: "{ documentID:'" + edFormsUserId + "', folderID:'" + folderID + "', shareID:'" + fileName + "', departmentId:'" + $('#<%= hdnDepartmentId.ClientID%>').val() + "'}",
                    dataType: "json",
                    success: function (data) {
                        if (data.d.path == "") {
                            hideLoader();
                            disableScrollbar();
                            alert("Failed to get object from cloud");
                            return false;
                        }
                        else {
                            SetPathAndSignature(data.d.path, data.d.isSignature)
                            showPreviewUrl(data.d.path);
                        }
                    },
                    error: function (result) {
                        console.log('Failed' + result.responseText);
                        hideLoader();
                    }
                });
            return false;
        }

        function SetPathAndSignature(path, signature) {
            setTimeout(function () {
                var overlay = document.getElementById("overlay");
                overlay.style.display = 'block';
            }, 1000);
            $("#<%= hdnPath.ClientID %>").val(path);
            $("#<%= hdnIsSignature.ClientID %>").val(signature.toLowerCase());
        }

        function removeAllEditable() {
            $('.txtDocumentName,.saveDocument,.cancelDocument').hide();
            $('.lblEdFormsName,.editDocument, .viewDocument').show();
        }

        function showEdit(ele) {
            $('.saveDocument,.cancelDocument').hide();
            $('.delete,.shareDocument,.editDocument, .lnkbtnPreview, .downloadFile, .viewDocument').show();
            var $this = $(ele);
            $this.css('display', 'none');
            $this.parent().find('[name=lnkUpdate]').show();
            $this.parent().find('[name=lnkcancel]').show();
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                $(this).find('input[type=text]').css('border', '1px solid #c4c4c4');//1px solid #c4c4c4
                $(this).find('input[type = text]').show();
                if ($(this).find('input[type = text]').val() == undefined || $(this).find('input[type = text]').val() == "") { }
                else {
                    oldName = $(this).find('input[type = text]').val();
                }
                $(this).find('[name=DocumentName]').hide();
                $(this).find('[name=lblClass]').hide();
                $(this).closest('td').find('.delete,.shareDocument, .lnkbtnPreview, .downloadFile, .viewDocument').hide();
            });
            return false;
        }

        function cancleFileName(ele) {
            var $this = $(ele);
            var LabelName = '';
            var FileName = '';
            $this.css('display', 'none');
            $this.parent().find('[name=lnkUpdate]').hide();
            $this.parent().find('[name=lnkBtnEdit]').show();
            $this.parent().find('.delete,.shareDocument, .lnkbtnPreview, .downloadFile, .viewDocument').show();
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                var len = 0;
                len = $(this).find('input[type=text]').length;
                if (len > 0) {
                    LabelName = $(this).find('[name=DocumentName]').html();
                    $(this).find('input[type = text]').val(LabelName);
                    $(this).find('input[type = text]').hide();
                    $(this).find('[name=DocumentName]').show();
                }
                else {
                }
            });
        }

        var oldName, oldFileName;
        function OnSaveRename(ele, edFormsUserId, fileName) {
            var newName = $(ele).parent().parent().find("input.txtDocumentName").val();
            oldFileName = fileName;
            if (newName == undefined || newName == "") {
                alert("File name can not be blank.");
                $('.txtDocumentName').css('border', '1px solid red');
                return false;
            }
            else if (!/^[a-z\d][a-z\d\()_\-.\s]+$/i.test(newName)) {
                alert("File name can not contain special characters.");
                $('.txtDocumentName').css('border', '1px solid red');
                return false;
            }
            else if (newName.indexOf('.') > 0 && newName.substring(newName.indexOf('.') + 1).toLowerCase() != 'pdf') {
                alert("Only File with type 'pdf' is allowed.");
                $('.txtDocumentName').css('border', '1px solid red');
                return false;
            }
            else {
                $('.txtDocumentName').css('border', '1px solid c4c4c4');
            }
            showLoader();
            $.ajax(
                {
                    type: "POST",
                    url: '../EFileFolderJSONService.asmx/RenameDocumentForEdForms',
                    data: "{ renameFile:'" + newName + "',edFormsId:'',oldFileName:'" + oldFileName + "', departmentId:'" + $('#<%= hdnDepartmentId.ClientID%>').val() + "', edFormsUserId:'" + edFormsUserId + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result.d.length > 0)
                            alert("Failed to rename document. Please try again after sometime.");
                        else
                            alert("File has been modified successfully.");
                        hideLoader();
                        __doPostBack('', 'refreshgridedit@');
                    },
                    error: function (result) {
                        hideLoader();
                    }
                });
        }

        function GetEmailToSend() {
            $("#folderList").html('');
            $("#comboBoxFolder").html('');
            $("#comboBoxFolder").chosen("destroy");
            $.ajax(
                {
                    type: "POST",
                    url: 'EdForms.aspx/GetMailToSend',
                    contentType: "application/json; charset=utf-8",
                    data: '{"officeId":"' + '<%= Sessions.SwitchedRackspaceId%>' + '"}',
                    dataType: "json",
                    success: function (result) {
                        if (result.d) {
                            var emailGroups = JSON.parse(result.d);
                            if (emailGroups.length > 0) {
                                $('<option value="All Folders">All Folders</option>').appendTo($('#comboBoxFolder'));
                                //$('<option value="Email Groups">Email Groups</option>').appendTo($('#comboBoxFolder'));
                                for (var i = 0; i < emailGroups.length; i++) {
                                    if (emailGroups[i].Type == 1)
                                        $('<option />', { value: emailGroups[i].Alert + "(Alert)", text: emailGroups[i].Alert.trim() + "(Alert)" }).appendTo($("#comboBoxFolder"));
                                    else if (emailGroups[i].Type == 2)
                                        $('<option />', { value: emailGroups[i].Alert + "(Status)", text: emailGroups[i].Alert.trim() + "(Status)" }).appendTo($("#comboBoxFolder"));
                                };
                                $('.chosen-toggle').each(function (index) {
                                    $(this).on('click', function () {
                                        $(this).parent().find('.folderList').find('option').prop('selected', $(this).hasClass('select')).parent().trigger('chosen:updated');
                                    });
                                });
                            }
                            $("#comboBoxFolder").chosen({ width: "100%" });
                            $chosen = $("#comboBoxFolder").chosen();

                            var chosen = $chosen.data("chosen");
                            var _fn = chosen.result_select;
                            chosen.result_select = function (evt) {
                                evt["metaKey"] = true;
                                evt["ctrlKey"] = true;
                                chosen.result_highlight.addClass("result-selected");
                                return _fn.call(chosen, evt);
                            };
                        }
                    },
                    fail: function (data) {
                        alert("Failed to get Folders. Please try again!");
                        hideLoader();
                    }
                });
        }

        function GetEmailGroups() {
            $("#groupList").html('');
            $("#drpGroups").html('');
            $("#drpGroups").chosen("destroy");
            $.ajax(
                {
                    type: "POST",
                    url: '../FlexWebService.asmx/GetEmailGroups',
                    contentType: "application/json; charset=utf-8",
                    data: '{"officeId":"' + '<%= Sessions.SwitchedSessionId%>' + '"}',
                    dataType: "json",
                    success: function (result) {
                        if (result.d.length > 0) {
                            var emailGroups = JSON.parse(result.d);
                            if (emailGroups.length > 0) {
                                $('<label><input type="checkbox" name="select-all" id="select-all" onclick="toggle(this);"/>Select All</label>').appendTo($('#groupList'));
                                for (var i = 0; i < emailGroups.length; i++) {
                                    var container = $('#groupList');
                                    $('<label for="cb' + emailGroups[i].GroupId + '"><input type="checkbox" id="cb' + emailGroups[i].GroupId + '" value=' + emailGroups[i].GroupId + '> ' + emailGroups[i].GroupName.trim() + '</label>').appendTo(container);
                                }
                            }
                        }
                        $("#drpGroups").chosen({ width: "315px" });
                        // hideLoader();

                    },
                    fail: function (data) {
                        alert("Failed to get Folders. Please try again!");
                        hideLoader();
                    }
                });
        }

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
        }

        function disableScrollbar() {
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }

        function enableScrollbar() {
            $('html, body').css({
                'overflow': 'auto',
                'height': 'auto'
            });
        }

        function LoadUsers() {
            showLoader();
            $("#comboBoxUser").html('');
            $("#comboBoxUser").chosen("destroy");
            $.ajax(
                {
                    type: "POST",
                    url: 'EdForms.aspx/GetAllUsersWithOffice',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result.d) {
                            $.each(result.d, function (i, text) {
                                $('<option />', { value: i, text: text }).appendTo($("#comboBoxUser"));
                            });
                        }
                        $("#comboBoxUser").chosen({ width: "394px" });
                        $chosen = $("#comboBoxUser").chosen();
                        var chosen = $chosen.data("chosen");
                        var _fn = chosen.result_select;
                        chosen.result_select = function (evt) {
                            evt["metaKey"] = true;
                            evt["ctrlKey"] = true;
                            chosen.result_highlight.addClass("result-selected");
                            return _fn.call(chosen, evt);
                        };
                    },
                    fail: function (data) {
                        alert("Failed to get users. Please try again!");
                    }
                });
        }

        function LoadDepartments() {
            showLoader();
            $("#comboBoxDepartment").html('');
            $("#comboBoxDepartment").chosen("destroy");
            $.ajax(
                {
                    type: "POST",
                    url: 'EdForms.aspx/GetDepartmentsForAdmin',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result.d) {
                            $.each(result.d, function (i, text) {
                                $('<option />', { value: i, text: text }).appendTo($("#comboBoxDepartment"));
                            });
                        }
                        $("#comboBoxDepartment").chosen({ width: "270px" });
                        if ($("#" + '<%= hdnDepartmentId.ClientID%>').val() != "") {
                            $("#comboBoxDepartment").val($("#" + '<%= hdnDepartmentId.ClientID%>').val()).trigger("chosen:updated");
                        }
                        $("#" + '<%= hdnDepartmentId.ClientID%>').val($("#comboBoxDepartment").val());
                        if ($("#preview_edForms").css("display") != "block") {
                            hideLoader();
                        }
                    },
                    fail: function (data) {
                        alert("Failed to get Folders. Please try again!");
                        if ($("#preview_edForms").css("display") != "block") {
                            hideLoader();
                        }
                    }
                });
        }

        function CheckSelected(ele) {
            var hdnEdForms = [];
            $(ele).parent().siblings('.popupcontent').children().find('.chkList').each(function (e) {
                if ($(this).find("input[type=checkbox]").prop("checked"))
                    hdnEdForms.push({ "edFormsId": $(this).siblings("input[type=hidden][id$=hdnEdFormsId]").val() });
            });
            if (hdnEdForms.length == 0) {
                alert("Please select atleast one form to add !!!");
                return false;
            }
            $('#<%= hdnEdFormsIds.ClientID%>').val(JSON.stringify(hdnEdForms));
        }

        function GetEdFormsAdmin() {
            var departmentId = $('#<%= hdnDepartmentId.ClientID%>').val();
            $.ajax(
                {
                    type: "POST",
                    url: 'EdForms.aspx/LoadEdFormsAdmin',
                    contentType: "application/json; charset=utf-8",
                    data: '{"departmentId":"' + departmentId + '"}',
                    dataType: "json",
                    success: function (result) {
                        __doPostBack('', 'refreshGrid@');
                    },
                    fail: function (data) {
                        alert("Failed to get Documents. Please try again!");
                        hideLoader();
                    }
                });
        }

        function GetEdFormsUser() {
            var departmentId = $('#<%= hdnDepartmentId.ClientID%>').val();
            $.ajax(
                {
                    type: "POST",
                    url: 'EdForms.aspx/LoadEdFormsUser',
                    contentType: "application/json; charset=utf-8",
                    data: '{"departmentId":"' + departmentId + '"}',
                    dataType: "json",
                    success: function (result) {
                        GetEdFormsAdmin();
                    },
                    fail: function (data) {
                        alert("Failed to get Documents. Please try again!");
                        hideLoader();
                    }
                });
        }

        function OpenModalPopup() {
            var popup = document.getElementById("preview_edForms");
            var overlay = document.getElementById("overlay");
            overlay.style.display = 'block';
            popup.style.display = 'block';
            $('#floatingCirclesG').css('display', 'none');
            $('#preview_edForms').focus();
            disableScrollbar();
            return false;
        }

        function showCheckboxesPublicHolidays() {
            var checkboxes = document.getElementById("groupList");
            if (!pubExpanded) {
                checkboxes.style.display = "block";
                pubExpanded = true;
            } else {
                checkboxes.style.display = "none";
                pubExpanded = false;
            }
            return false;
        }
        var signIndex = 1;
        var cords = [];

        function StartSignature() {
            DisableWhileSignature();
            cords = [];
        }

        function StopSignature() {
            if (confirm("Are you sure want to discard the changes ?")) {
                EnableAfterSignature();
            }
        }

        function DisableWhileSignature() {
            $("#startSignature").hide();
            $("#stopSignature").show();
            $(".btnFormField").show();
            $("#<%= btnSaveSignature.ClientID %>").show();
            $('.lblInformation').show();
            $('.ic-sign-share').hide();
        }

        function EnableAfterSignature() {
            $("#startSignature").show();
            $("#stopSignature").hide();
            $("#<%= btnSaveSignature.ClientID %>").hide();
            $(".btnFormField").hide();
            var iframe = document.getElementById("reviewContent_edForms");
            DisablePageClick(iframe);
            $(iframe.contentWindow.document).children().find('.mydiv').remove();
            BackToBtnField();
            $('.lblInformation').hide();
            $('.ic-sign-share').show();
        }

        function BackToBtnField() {
            $('.lblInformation').show();
            $('.lblTextField').hide();
            $('.lblSignatureField').hide();
            $('.btnFormField').removeAttr("disabled");
            $('.btnFormField').removeClass("btnActive");
            $('.btnFormField').css("cursor", "pointer");
        }

        function OnAddingSignature() {
            var iframe = document.getElementById("reviewContent_edForms");
            var divs = $(iframe.contentWindow.document).children().find('.mydiv');
            if (divs.length > 0) {
                var popup = document.getElementById("preview_popup");
                popup.style.display = 'none';
                var pageHeight = $(iframe.contentWindow.document).children().find('.page').css("height").split('px')[0];
                var pageWidth = $(iframe.contentWindow.document).children().find('.page').css("width").split('px')[0];
                for (var i = 0; i < divs.length; i++) {
                    var height = $(divs[i]).css("height").split('px')[0];
                    var width = $(divs[i]).css("width").split('px')[0];
                    for (var j = 0; j < cords.length; j++) {
                        if (cords[j].id == $(divs[i]).attr("id")) {
                            cords[j].height = height;
                            cords[j].width = width;
                            cords[j].pageHeight = pageHeight;
                            cords[j].pageWidth = pageWidth;
                        }
                    }
                }
                $('#<%= hdnCords.ClientID %>').val(JSON.stringify(cords));
                var popupToHide = document.getElementById("preview_popup_edForms");
                popupToHide.style.display = 'none';
                $('#floatingCirclesG').css('display', 'block');
                return true;
            }
            else {
                alert("Please add at least one signature or text field!!");
                return false;
            }
        }

        function onClientUploadComplete(sender, e) {
            return false;
        }

        function onClientUploadCompleteAll(sender, e) {
            $('.hideControl').removeClass('hideControl');
            $('.uploadProgress').find('select').removeAttr('disabled');

            $('#divUpload').clone().appendTo('#uploadFieldset');
            hideLoader();
            clearInterval(myTimer);
            document.getElementById('uploadProgress').style.display = 'none';
            __doPostBack('', 'RefreshGrid@');

            setTimeout(function () {
                var vd = '<%= ConfigurationManager.AppSettings["VirtualDir"] %>';
                window.location.reload();
            }, 2000);
        }

        var myTimer;
        var fileIDDividerId = [];
        function onClientUploadStart(sender, e) {
            $.ajax(
                {
                    type: "POST",
                    url: '../Office/EdForms.aspx/SaveDepartmentId',
                    data: "{ departmentId:'" + $('#<%= hdnDepartmentId.ClientID %>').val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (result) {
                        document.getElementById('uploadCompleteInfo').innerHTML = "please wait while the " + e.get_filesInQueue() + " file is uploaded..";
                        document.cookie = "Files=" + JSON.stringify(fileIDDividerId);
                        showLoader();
                        $('[id*=ajaxfileupload1_Html5DropZone]').addClass('hideControl').css('display', 'none');
                        $('[id*=ajaxfileupload1_SelectFileContainer]').addClass('hideControl').css('display', 'none');
                        $('[id*=FileItemDeleteButton]').addClass('hideControl').css('display', 'none');
                        $('[id*=UploadOrCancelButton]').addClass('hideControl').css('display', 'none');
                        $('.TabDropDown').prop('disabled', 'disabled');
                        $('#uploadProgress').html('');
                        $('#uploadProgress').html('').html($('#divUpload').html());

                        document.getElementById('uploadProgress').style.display = 'block';
                        myTimer = setInterval(function () {
                            $('#uploadProgress').html('').html($('#divUpload').html());
                        }, 1000);
                    },
                    error: function (result) {
                        hideLoader();
                    }
                });
        }

        function OnAddingFormField() {
            if ($('.btnFormField').hasClass('btnActive')) {
                BackToBtnField();
            }
            else {
                $('.btnFormField').addClass('btnActive');
                //$('.btnFormField').attr("disabled", "disabled");
                //$('.btnFormField').css("cursor", "not-allowed");
                $("#drpType").val(1);
                $("#extraName").show();
                $("#extraFieldName").val("");
                var popupToHide = document.getElementById("preview_popup_edForms");
                var popupToShow = document.getElementById("form_field_select");
                popupToShow.style.display = 'block';
                popupToHide.style.display = 'none';
            }
            return false;
        }

        function EnablePageClick() {
            var iframe = document.getElementById("reviewContent_edForms");
            $(iframe.contentWindow.document).children().find('#pageRotateCw').css("cursor", "not-allowed").attr("disabled", "disabled");
            $(iframe.contentWindow.document).children().find('#pageRotateCcw').css("cursor", "not-allowed").attr("disabled", "disabled");
            $(iframe.contentWindow.document).find("#scaleSelect").change(function () {
                var scaleValue = $(this).val();
                if (scaleValue == 'auto' || scaleValue == 'page-fit' || scaleValue == 'page-width') {
                    scaleValue = 0.68;
                }
                else if (scaleValue == 'page-actual') {
                    scaleValue = 1;
                }

                var divs = $(this).parents().find("#mainContainer").find('.mydiv');
                var pageWidth = parseInt($(this).parents().find("#mainContainer").find('.page').first().css("width").split('px')[0]);
                var viewerWidth = parseInt($(this).parents().find("#mainContainer").find('#viewer').first().css("width").split('px')[0]);
                var diff = '';
                if (pageWidth < viewerWidth) {
                    diff = (viewerWidth - pageWidth) / 2;
                }
                for (var i = 0; i < divs.length; i++) {
                    var height = parseInt($(divs[i]).css("height").split('px')[0]);
                    var width = parseInt($(divs[i]).css("width").split('px')[0]);
                    var prevScale, prevDiff;
                    for (var j = 0; j < cords.length; j++) {
                        if (cords[j].id == $(divs[i]).attr("id")) {
                            prevScale = cords[j].prevScale;
                            prevDiff = cords[j].prevDiff;
                            cords[j].prevDiff = diff;
                            cords[j].prevScale = scaleValue;
                        }
                    }
                    height = (height / prevScale) * scaleValue;
                    width = (width / prevScale) * scaleValue;
                    $(divs[i]).css("height", height + "px");
                    $(divs[i]).css("width", width + "px");
                    var left = parseInt($(divs[i]).css("left").split('px')[0]);
                    var top = parseInt($(divs[i]).css("top").split('px')[0]);
                    if (prevDiff != '') {
                        if (prevDiff > left)
                            left = prevDiff - left;
                        else
                            left = left - prevDiff;
                    }
                    if (prevScale != '') {
                        left = left / prevScale;
                        top = top / prevScale;
                    }
                    left = scaleValue * left;
                    top = scaleValue * top;
                    left = left + diff;
                    $(divs[i]).css("left", left + "px");
                    $(divs[i]).css("top", top + "px");
                }
            });
            $(iframe.contentWindow.document).children().find('.page').click(function (e) {
                var pageIndex = $(this).attr("data-page-number");
                var scaleValue = $(this).parent().parent().parent().find("#scaleSelect").val();
                var divHeight = '25';
                var divWidth = '160';
                if (scaleValue == 'auto' || scaleValue == 'page-fit' || scaleValue == 'page-width') {
                    scaleValue = 0.68;
                }
                else if (scaleValue == 'page-actual') {
                    scaleValue = 1;
                }
                divHeight = scaleValue * divHeight;
                divWidth = scaleValue * divWidth;
                var top = e.offsetY;
                var left = e.offsetX;
                if (e.target.className != 'textLayer') {
                    top = parseInt($(e.target).css("top").split('px')[0]) + e.offsetY;
                    left = parseInt($(e.target).css("left").split('px')[0]) + e.offsetX;
                }
                if (pageIndex > 1) {
                    top = ((pageIndex - 1) * parseInt($(this).css("height").split('px')[0])) + top + ((pageIndex - 1) * 10);
                }
                var thisWidth = parseInt($(this).css("width").split('px')[0]);
                var parentWidth = parseInt($(this).parent().css("width").split('px')[0]);
                var diff = '';
                if (thisWidth < parentWidth) { diff = ((parentWidth - thisWidth) / 2); }
                left = left + diff;
                var className = $("#drpType").val() == "1" ? "myText" : "mySign";
                var fieldName = $("#drpType").val() == "1" ? $("#extraFieldName").val() : "signature";
                cords.push({
                    top: top,
                    left: left,
                    id: 'signatureDiv' + signIndex,
                    height: divHeight,
                    width: divWidth,
                    prevDiff: diff,
                    prevScale: scaleValue,
                    pageNumber: pageIndex,
                    pageWidth: 0,
                    pageHeight: 0,
                    //fieldType : $("#drpType").val(),
                    fieldName: fieldName
                });
                var redDiv = '<div name="' + fieldName + '" class="mydiv ' + className + '" id="signatureDiv' + signIndex + '" style="background-color:rgb(212, 251, 212);position:absolute;resize:auto;overflow:auto;border:dashed 1px black;z-index:1;height:' + divHeight + 'px;width:' + divWidth + 'px;top:' + top + 'px;left:' + left + 'px">' +
                    '<span onclick="removeSignatureDiv(this);" id="signatureClose' + signIndex + '" class="ic-icon signature-close" style="background: url(../../images/icon-close.png) no-repeat right top;float: left;padding: 6px;cursor: pointer;background-size: 12px;opacity:1 !important;"></span></div>';
                $(redDiv).insertAfter($(this));
                signIndex++;
                DisablePageClick(iframe);
                BackToBtnField();
            });
        }

        function DisablePageClick(elem) {
            $(elem.contentWindow.document).children().find('.page').unbind("click");
            $(elem.contentWindow.document).children().find('#pageRotateCw').css("cursor", "default").removeAttr("disabled");
            $(elem.contentWindow.document).children().find('#pageRotateCcw').css("cursor", "default").removeAttr("disabled");
        }

        function OnFieldTypeChange() {
            if ($("#drpType").val() == "1") {
                $("#extraName").show();
            }
            else {
                $("#extraName").hide();
            }
        }

        function OnSelectingField() {
            var iframe = document.getElementById("reviewContent_edForms");
            var divs = $(iframe.contentWindow.document).children().find('.mySign');
            var textField = $(iframe.contentWindow.document).children().find('div[name="' + $("#extraFieldName").val() + '"]');
            if ($("#drpType").val() == "1" &&
                ($("#extraFieldName").val() == ""
                    || $("#extraFieldName").val() == undefined
                    || $("#extraFieldName").val() == null)) {
                alert("Please enter valid field name!!");
            }
            else if ($("#drpType").val() == "1" && textField.length > 0) {
                alert("This field name already exists. Please enter another name!!");
            }
            else if ($("#drpType").val() == "2" && (divs.length > 0 || $("#<%= hdnIsSignature.ClientID %>").val() == "true")) {
                alert("PDF already contains one signature field. You can only add text fields.");
            }
            else {
                $('.lblInformation').hide();
                if ($("#drpType").val() == "1")
                    $('.lblTextField').show();
                else
                    $('.lblSignatureField').show();
                var popupToShow = document.getElementById("preview_popup_edForms");
                var popupToHide = document.getElementById("form_field_select");
                popupToShow.style.display = 'block';
                popupToHide.style.display = 'none';
                EnablePageClick();
                ShowEdFormsPopUp();
            }
            return false;
        }

        function OnCancelingField() {
            BackToBtnField();
            ShowEdFormsPopUp();
            return false;
        }

        function ShowEdFormsPopUp() {
            var popupToShow = document.getElementById("preview_popup_edForms");
            var popupToHide = document.getElementById("form_field_select");
            popupToShow.style.display = 'block';
            popupToHide.style.display = 'none';
        }
    </script>
</asp:Content>
