﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Shinetech.DAL;
using System.Web.Services;
using Newtonsoft.Json;
using System.Web.Script.Services;
using Shinetech.Utility;
using iTextSharp.text.pdf;
using iTextSharp.text;

public partial class Office_EdForms : System.Web.UI.Page
{
    public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    public static string EdFormsFolder = System.Configuration.ConfigurationManager.AppSettings["RackSpaceServerEdFormsFolder"];
    public string virtualDir = ConfigurationManager.AppSettings["VirtualDir"];
    public string SortType = "DESC";
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
    private readonly NLogLogger _logger = new NLogLogger();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //byte[] pdfData = System.IO.File.ReadAllBytes(@"C:\Users\pca65\Downloads\alameda_emergency1549338692 (1).pdf");

            ////Load PDF byte array into RAD PDF
            //this.PdfWebControl1.CreateDocument("Document Name", pdfData);

            if (Sessions.HasViewPrivilegeOnly)
            {
                lnkBtnShare.Visible = false;
                boxToMail.Style.Add("display", "none");
            }
            GetData();
            BindGrid();
            GetAdminEdForms();
            BindAdminGrid();
        }
        else
        {
            var argument = Request.Form["__EVENTARGUMENT"];
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower() == "refreshgrid@")
            {
                BindGrid();
                BindAdminGrid();
            }
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower() == "refreshgridedit@")
            {
                GetData();
                BindGrid();
            }
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower() == "refreshgridadmin@")
            {
                BindAdminGrid();
            }
        }
    }

    private void GetAdminEdForms()
    {
        this.AdminDataSource = General_Class.GetEdFormsForAdmin(Sessions.SwitchedRackspaceId, string.IsNullOrEmpty(hdnDepartmentId.Value) ? null : (int?)Convert.ToInt32(hdnDepartmentId.Value), true);
    }

    private void BindAdminGrid()
    {
        EdFormsAdminGrid.DataSource = this.AdminDataSource;
        EdFormsAdminGrid.PageIndex = 1;
        EdFormsAdminGrid.DataBind();
        WebPager2.DataSource = this.AdminDataSource;
        WebPager2.DataBind();
    }

    private void GetData()
    {
        string uid = Sessions.SwitchedRackspaceId;
        this.OfficeDataSource = General_Class.GetEdFormsForUser(string.IsNullOrEmpty(hdnDepartmentId.Value) ? 0 : Convert.ToInt32(hdnDepartmentId.Value), Sessions.SwitchedRackspaceId);
    }

    /// <summary>
    /// Set EdFormsUserId for Logs
    /// </summary>
    /// <param name="edFormsUserId"></param>
    [WebMethod(EnableSession = true)]
    public static void SetSessionForEdFormsUser(int edFormsUserId)
    {
        Sessions.EdFormsUserId = edFormsUserId;
    }

    private void BindGrid()
    {
        EdFormsGrid.DataSource = this.OfficeDataSource;
        EdFormsGrid.DataBind();
        WebPager1.DataSource = this.OfficeDataSource;
        WebPager1.DataBind();
    }

    /// <summary>
    /// Get all the subusers including the office users
    /// </summary>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public static Dictionary<string, string> GetAllUsersWithOffice()
    {
        string officeUID = Sessions.SwitchedRackspaceId;
        Dictionary<string, string> dict = new Dictionary<string, string>();
        try
        {
            DataTable dtUsers = General_Class.GetAllSubUsers(officeUID);
            dict.Add(officeUID, Sessions.SwitchedRackspaceUserName);
            IList<string> list = new List<string>();
            if (dtUsers != null)
            {
                dtUsers.AsEnumerable().ToList().ForEach(d =>
                {
                    dict.Add(Convert.ToString(d.Field<Guid>("UID")), d.Field<string>("Name"));

                });
            }
        }
        catch (Exception ex)
        {
            Office_EdForms edForms = new Office_EdForms();
            edForms._logger.Error(ex);
        }
        return dict;
    }

    [WebMethod(EnableSession = true)]
    public static Dictionary<string, string> GetDepartmentsForAdmin()
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        try
        {
            Guid uid = new Guid(Sessions.SwitchedRackspaceId);
            DataSet dtDepartments = General_Class.GetAssignedDepartment(uid);
            if (dtDepartments != null && dtDepartments.Tables.Count > 1)
            {
                dtDepartments.Tables[1].AsEnumerable().ToList().ForEach(d =>
                {
                    dict.Add(Convert.ToString(d.Field<int>("DepartmentId")), d.Field<string>("DepartmentName"));
                });
            }
        }
        catch (Exception ex)
        {
            Office_EdForms edForms = new Office_EdForms();
            edForms._logger.Error(ex);
        }
        return dict;
    }

    /// <summary>
    /// Get all mail Groups
    /// </summary>
    /// <param name="officeId"></param>
    /// <returns></returns>
    [WebMethod]
    public static string GetMailToSend(string officeId)
    {
        try
        {
            return JsonConvert.SerializeObject(General_Class.GetEmailGroupEfileflow(officeId));
        }
        catch (Exception ex)
        {
            Office_EdForms edForms = new Office_EdForms();
            edForms._logger.Error(ex);
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            return string.Empty;
        }
    }

    public DataTable OfficeDataSource
    {
        get
        {
            return this.Session["EdForms_Users"] as DataTable;
        }
        set
        {
            this.Session["EdForms_Users"] = value;
        }
    }

    public DataTable AdminDataSource
    {
        get
        {
            return this.Session["EdFormsAdmin_Users"] as DataTable;
        }
        set
        {
            this.Session["EdFormsAdmin_Users"] = value;
        }
    }

    private SortDirection Sort_Direction_Office
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirectionOffice.Value))
            {
                SortDirectionOffice.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirectionOffice.Value);

        }
        set
        {
            this.SortDirectionOffice.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    [WebMethod]
    public static Dictionary<string, string> GetPreviewURL(int documentID, int folderID, string shareID, string departmentId)
    {
        try
        {
            Dictionary<string, string> keyValues = new Dictionary<string, string>();
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            string errorMsg = string.Empty;
            string path = rackSpaceFileUpload.GeneratePath(shareID.ToLower(), Enum_Tatva.Folders.EdForms, departmentId);
            string newFileName1 = rackSpaceFileUpload.GetObject(ref errorMsg, path, null, Enum_Tatva.Folders.EdForms.GetHashCode());
            EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Preview.GetHashCode(), 0, 0, 0, 0, null, 0, 0, null, 0, documentID);
            if (string.IsNullOrEmpty(errorMsg))
            {
                string pathName = Common_Tatva.RackSpaceEdFormsDownload + "/" + Sessions.SwitchedRackspaceId + "/" + newFileName1;
                PdfReader pdfReader = new PdfReader(HttpContext.Current.Server.MapPath("~/" + pathName));
                var fields = pdfReader.AcroFields.Fields;
                keyValues.Add("path", pathName);
                keyValues.Add("isSignature", Convert.ToString(fields.Any(a => a.Key.ToLower() == "signature")));
                return keyValues;
            }
            else
                return null;
        }
        catch (Exception ex)
        {
            Office_EdForms edForms = new Office_EdForms();
            edForms._logger.Error(ex);
            return null;
        }
    }

    [WebMethod]
    public static string GetPreviewUrlForEdFormsShare(string documentID, string shareID, string edFormsShareId)
    {
        try
        {
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            string errorMsg = string.Empty;
            string path = rackSpaceFileUpload.GeneratePath(shareID.ToLower(), Enum_Tatva.Folders.EdFormsShare, edFormsShareId.ToLower());
            string newFileName1 = rackSpaceFileUpload.GetObject(ref errorMsg, path, null, Enum_Tatva.Folders.EdFormsShare.GetHashCode());
            EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
            //eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Preview.GetHashCode(), 0, 0, 0, 0, null, 0, 0, null, 0, documentID);
            if (string.IsNullOrEmpty(errorMsg))
                return Common_Tatva.RackSpaceEdFormsDownload + "/" + Sessions.SwitchedRackspaceId + "/" + newFileName1;
            else
                return string.Empty;
        }
        catch (Exception ex)
        {
            Office_EdForms edForms = new Office_EdForms();
            edForms._logger.Error(ex);
            return string.Empty;
        }
    }

    [WebMethod]
    public static void LoadEdFormsAdmin(string departmentId = null)
    {
        Office_EdForms edFormsUsers = new Office_EdForms();
        try
        {
            edFormsUsers.AdminDataSource = General_Class.GetEdFormsForAdmin(Sessions.SwitchedRackspaceId, string.IsNullOrEmpty(departmentId) ? null : (int?)Convert.ToInt32(departmentId), true);
        }
        catch (Exception ex)
        {
            Office_EdForms edForms = new Office_EdForms();
            edForms._logger.Error(ex);
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
        }
    }

    [WebMethod]
    public static void LoadEdFormsUser(string departmentId = null)
    {
        Office_EdForms edFormsUsers = new Office_EdForms();
        try
        {
            edFormsUsers.OfficeDataSource = General_Class.GetEdFormsForUser(string.IsNullOrEmpty(departmentId) ? 0 : Convert.ToInt32(departmentId), Sessions.SwitchedRackspaceId);
        }
        catch (Exception ex)
        {
            Office_EdForms edForms = new Office_EdForms();
            edForms._logger.Error(ex);
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
        }
    }

    protected void WebPager1_PageIndexChanged(object sender, Shinetech.Framework.Controls.PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.OfficeDataSource;
        WebPager1.DataBind();
    }

    protected void EdFormsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
                string edFormsUserId = drv.Row["EdFormsUserId"].ToString();
                string fileName = drv.Row["Name"].ToString();
                string departmentId = drv.Row["DepartmentId"].ToString();
                fileName = departmentId + "/" + fileName;
                HyperLink lnkBtnDownload = (e.Row.FindControl("lnkBtnDownload") as HyperLink);
                if (lnkBtnDownload != null)
                {
                    lnkBtnDownload.Attributes.Add("href", "PdfViewer.aspx?ID=" + edFormsUserId + "&data=" + QueryString.QueryStringEncode("ID=" + fileName + "&folderID=" + (int)Enum_Tatva.Folders.EdForms + "&sessionID=" + Sessions.SwitchedRackspaceId));
                }
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    protected void EdFormsGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortDirection = "";
            string sortExpression = e.SortExpression;
            if (this.Sort_Direction_Office == SortDirection.Ascending)
            {
                this.Sort_Direction_Office = SortDirection.Descending;
                sortDirection = "DESC";
            }
            else
            {
                this.Sort_Direction_Office = SortDirection.Ascending;
                sortDirection = "ASC";
            }
            DataView Source = new DataView(this.OfficeDataSource);
            Source.Sort = e.SortExpression + " " + sortDirection;

            this.OfficeDataSource = Source.ToTable();
            BindGrid();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    protected void lnkBtnDelete_Command(object sender, CommandEventArgs e)
    {
        try
        {
            string password = textPassword.Text.Trim();
            string strPassword = PasswordGenerator.GetMD5(password);
            string errorMsg = string.Empty;

            if (UserManagement.GetPassword(User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
            {
                //show the password is wrong        
                ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssYes", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);
            }
            else
            {
                LinkButton lnkbtnDelete = sender as LinkButton;
                GridViewRow gvrow = lnkbtnDelete.NamingContainer as GridViewRow;
                int edFormsUserId = Convert.ToInt32(e.CommandArgument.ToString().Split(';')[0]);
                DataTable dtFiles = General_Class.GetEdFormsUserById(edFormsUserId);
                dtFiles.AsEnumerable().ToList().ForEach(d =>
                {
                    var file = d.Field<string>("Name");
                    int id = d.Field<int>("edFormsUserId");
                    string pathName = Enum_Tatva.GetEnumDescription(Enum_Tatva.Folders.EdForms) + "/" + hdnDepartmentId.Value + "/" + file;
                    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
                    eFileFolderJSONWS.DeleteEdForms(pathName, Sessions.SwitchedRackspaceId, file, ref errorMsg, 0, id);
                    if (string.IsNullOrEmpty(errorMsg))
                    {
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "File " + file + " has been deleted successfully" + "\");", true);
                    }
                    else
                    { ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Failed to delete file: " + file + ". Please try again after sometime." + "\");", true); }
                });
                GetData();
                BindGrid();
                GetAdminEdForms();
                BindAdminGrid();
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
        finally
        {
            ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
        }
    }

    protected void WebPager2_PageIndexChanged(object sender, Shinetech.Framework.Controls.PageChangedEventArgs e)
    {
        WebPager2.CurrentPageIndex = e.NewPageIndex;
        WebPager2.DataSource = this.AdminDataSource;
        WebPager2.DataBind();
        ClientScript.RegisterStartupScript(this.GetType(), "Popup", "OpenModalPopup();", true);
    }
    protected void WebPager2_PageSizeChanged(object s, Shinetech.Framework.Controls.PageSizeChangeEventArgs e)
    {
        WebPager2.CurrentPageIndex = 0;
        WebPager2.DataSource = this.AdminDataSource;
        WebPager2.DataBind();
        ClientScript.RegisterStartupScript(this.GetType(), "Popup", "OpenModalPopup();", true);
    }

    protected void EdFormsAdminGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortDirection = "";
            string sortExpression = e.SortExpression;
            if (this.Sort_Direction_Admin == SortDirection.Ascending)
            {
                this.Sort_Direction_Admin = SortDirection.Descending;
                sortDirection = "DESC";
            }
            else
            {
                this.Sort_Direction_Admin = SortDirection.Ascending;
                sortDirection = "ASC";
            }
            DataView Source = new DataView(this.AdminDataSource);
            Source.Sort = e.SortExpression + " " + sortDirection;
            EdFormsAdminGrid.DataSource = Source.ToTable();
            EdFormsAdminGrid.DataBind();
            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "OpenModalPopup();", true);
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    protected void EdFormsAdminGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

    }

    private SortDirection Sort_Direction_Admin
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirectionAdmin.Value))
            {
                SortDirectionAdmin.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirectionAdmin.Value);

        }
        set
        {
            this.SortDirectionAdmin.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {
            EdFormsTemplateForAdmin[] edFormsId = JsonConvert.DeserializeObject<List<EdFormsTemplateForAdmin>>(hdnEdFormsIds.Value).ToArray();
            DataTable dtEdFormsId = new DataTable();
            dtEdFormsId.Columns.Add("EdFormsId", typeof(int));
            dtEdFormsId.Columns.Add("Index", typeof(int));
            int index = 1;
            foreach (var item in edFormsId)
            {
                if (item.EdFormsId > 0)
                {
                    dtEdFormsId.Rows.Add(new object[] { item.EdFormsId, index });
                    index++;
                }
            }
            Guid guid = new Guid(Sessions.SwitchedRackspaceId);
            DataTable dtFiles = General_Class.DocumentsInsertForEdFormsUser(guid, dtEdFormsId);
            bool success = true;
            if (dtFiles != null && dtFiles.Rows.Count > 0)
            {
                foreach (DataRow file in dtFiles.Rows)
                {
                    try
                    {
                        string fileName = Convert.ToString(file["FileName"]);
                        string uid = Convert.ToString(file["UID"]).ToLower();
                        int edFormsUserId = Convert.ToInt32(file["EdFormsUserId"]);
                        if (!Convert.ToBoolean(file["Success"]) || string.IsNullOrEmpty(fileName))
                        {
                            success = false;
                            continue;
                        }
                        else
                        {
                            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                            string errorMsg = string.Empty;
                            string newPath = rackSpaceFileUpload.CopyObject(ref errorMsg, fileName, Enum_Tatva.Folders.EdForms, uid, null, hdnDepartmentId.Value);
                            if (string.IsNullOrEmpty(errorMsg))
                            {
                                General_Class.UpdateEdFormsForUser(Path.GetFileName(newPath), edFormsUserId, true);
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Create.GetHashCode(), 0, 0, 0, 0, null, 0, 0, null, 0, edFormsUserId);
                            }
                            else
                            {
                                General_Class.UpdateEdFormsForUser(fileName, edFormsUserId, false, true);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                    }
                }
                if (success)
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('All the selected forms have been addedd.');", true);
                else
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Some EdForms have already been uploaded !!');", true);
            }
            else
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Failed to add forms at this time, please try again later.');", true);
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Failed to add forms at this time, please try again later.');", true);
        }
        GetData();
        BindGrid();
        GetAdminEdForms();
        BindAdminGrid();
    }

    protected void btnSaveSignature_Click(object sender, EventArgs e)
    {
        List<DivOption> divOptions = JsonConvert.DeserializeObject<List<DivOption>>(hdnCords.Value);
        string pathName = HttpContext.Current.Server.MapPath("~/" + hdnPath.Value);

        string newPathName = RedactPages(divOptions, pathName);
        if (!string.IsNullOrEmpty(newPathName))
        {
            string fileName = hdnFileName.Value;
            string depId = hdnDepartmentId.Value;
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            string errorMsg = string.Empty;
            string oldObjectPath = rackSpaceFileUpload.GeneratePath(fileName, Enum_Tatva.Folders.EdForms, depId);
            string newObjectPath = rackSpaceFileUpload.GeneratePath(fileName, Enum_Tatva.Folders.EdForms, depId, true);
            rackSpaceFileUpload.MoveObject(ref errorMsg, oldObjectPath, newObjectPath, Enum_Tatva.Folders.EdForms);
            if (string.IsNullOrEmpty(errorMsg))
            {
                string errMsg = string.Empty;
                rackSpaceFileUpload.UploadObject(ref errMsg, fileName, Enum_Tatva.Folders.EdForms, new MemoryStream(File.ReadAllBytes(newPathName)), depId);
                if (string.IsNullOrEmpty(errMsg))
                {
                    hdnCords.Value = "";
                    string newFileName = rackSpaceFileUpload.GetObject(ref errorMsg, oldObjectPath, null, Enum_Tatva.Folders.EdForms.GetHashCode());
                    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
                    string tmpPathName = Common_Tatva.RackSpaceEdFormsDownload + "/" + Sessions.SwitchedRackspaceId + "/" + newFileName;
                    PdfReader pdfReader = new PdfReader(HttpContext.Current.Server.MapPath("~/" + tmpPathName));
                    var fields = pdfReader.AcroFields.Fields;
                    ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                       "alert(\"" + "Field(s) have been added successfully." + "\");" +
                                                       "SetPathAndSignature(\"" + tmpPathName + "\", \"" + fields.Any(a => a.Key.ToLower() == "signature") + "\");" +
                                                       "showPreviewUrl(\"" + tmpPathName + "\");", true);
                    return;
                }
            }
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                            "alert(\"" + "Failed to add signature at this time. Please try again later !!" + "\");", true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                "alert(\"" + "Failed to add signature at this time. Please try again later !!" + "\");", true);
        }
    }

    public string RedactPages(List<DivOption> divOptions, string pathName)
    {
        try
        {
            string fileNameOnly = Path.GetFileName(pathName);
            string newPathName = HttpContext.Current.Server.MapPath(string.Format("~/{0}", EdFormsFolder) + Path.DirectorySeparatorChar + Sessions.SwitchedRackspaceId);
            if (!Directory.Exists(newPathName))
                Directory.CreateDirectory(newPathName);
            newPathName = newPathName + Path.DirectorySeparatorChar + DateTime.Now.ToString("mmddyyyyMM_") + fileNameOnly;
            PdfReader reader = new PdfReader(pathName);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newPathName, FileMode.Create));
            try
            {
                for (int i = 0; i < divOptions.Count; i++)
                {
                    int pageIndex = divOptions[i].pageNumber;
                    float scale = divOptions[i].prevScale;
                    float x = Math.Abs((divOptions[i].left - (divOptions[i].prevDiff == null ? 0 : (float)divOptions[i].prevDiff)) / scale);
                    float y = Math.Abs((divOptions[i].top) / scale);
                    float width = (float)divOptions[i].width / scale;
                    float height = (float)divOptions[i].height / scale;
                    float pageWidth = divOptions[i].pageWidth / scale;
                    float pageHeight = divOptions[i].pageHeight / scale;
                    string fieldName = divOptions[i].fieldName;
                    if (pageIndex > 1)
                        y = Math.Abs(((pageIndex - 1) * pageHeight) + (((pageIndex - 1) * 12) / scale) - y);
                    else
                        y = Math.Abs((10 / scale) - y);

                    iTextSharp.text.Rectangle rect = reader.GetPageSize(divOptions[i].pageNumber);
                    float ratio = rect.Height / pageHeight;
                    x = x * ratio;
                    y = y * ratio;
                    width = width * ratio;
                    height = height * ratio;
                    y = Math.Abs(rect.Height - y);
                    y = y - height;

                    TextField field = new TextField(stamper.Writer, new iTextSharp.text.Rectangle(x, y, x + width, y + height), fieldName);
                    // add the field here, the second param is the page you want it on         
                    stamper.AddAnnotation(field.GetTextField(), pageIndex);

                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            //stamper.FormFlattening = true; // lock fields and prevent further edits.
            stamper.Close();
            reader.Close();
            //File.Delete(pathName);
            //File.Move(newPathName, pathName);
            return newPathName;
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
            return null;
        }
    }

    protected void AjaxFileUpload1_UploadComplete(object sender, AjaxControlToolkit.AjaxFileUploadEventArgs e)
    {
        try
        {
            string path = Server.MapPath("~/" + Common_Tatva.eFileFlow);
            string uid = Sessions.SwitchedRackspaceId;

            string fullPath = Path.Combine(path, Sessions.SwitchedRackspaceId);

            if (AjaxFileUpload1.IsInFileUploadPostBack)
            {
                var fullname = Path.Combine(fullPath, e.FileName);
                string fileName = Path.GetFileName(fullname);
                decimal? size;

                size = Math.Round((decimal)(e.FileSize / 1024) / 1024, 2);
                Guid guid = new Guid(Sessions.SwitchedRackspaceId);

                DataTable dtEdFormsId = new DataTable();
                dtEdFormsId.Columns.Add("EdFormsId", typeof(int));
                dtEdFormsId.Columns.Add("Index", typeof(int));
                RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                string errorMsg = string.Empty;
                string newFileName = rackSpaceFileUpload.UploadObject(ref errorMsg, e.FileName, Enum_Tatva.Folders.EdForms, e.GetStreamContents(), Convert.ToString(Sessions.DepartmentId));
                if (string.IsNullOrEmpty(errorMsg))
                {
                    DataTable newEdForms = General_Class.DocumentsInsertForEdFormsUser(new Guid(uid), dtEdFormsId, newFileName.Substring(newFileName.LastIndexOf('/') + 1), size, Sessions.DepartmentId);
                    //eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Upload.GetHashCode(), 0, 0, Convert.ToInt32(edFormId));
                }
                else
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Failed to upload file. Please try again after sometime." + "\");", true);
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void AjaxFileUpload1_UploadCompleteAll(object sender, AjaxControlToolkit.AjaxFileUploadCompleteAllEventArgs e)
    {
        GetData();
        BindGrid();
    }

    protected void AjaxFileUpload1_UploadStart(object sender, AjaxControlToolkit.AjaxFileUploadStartEventArgs e)
    {
        string depId = hdnDepartmentId.Value;
    }

    [WebMethod]
    public static void SaveDepartmentId(string departmentId)
    {
        Sessions.DepartmentId = Convert.ToInt32(departmentId);
    }
}
public class DivOption
{
    public float top { get; set; }
    public float left { get; set; }
    public string id { get; set; }
    public float? prevDiff { get; set; }
    public float prevScale { get; set; }
    public double height { get; set; }
    public double width { get; set; }
    public int pageNumber { get; set; }
    public float pageHeight { get; set; }
    public float pageWidth { get; set; }
    //public int fieldType { get; set; }
    public string fieldName { get; set; }
}
