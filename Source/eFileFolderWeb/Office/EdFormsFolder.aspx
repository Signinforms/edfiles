﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EdFormsFolder.aspx.cs" Inherits="Office_EdFormsFolder" %>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register Src="~/Office/UserControls/ucAddToTabDefault.ascx" TagPrefix="uc1" TagName="ucAddToTab" %>
<%--<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>--%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/style.css?v=3" rel="Stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=2" rel="stylesheet" />
    <link rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/style.min.css" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/edFiles/EdFilesLogs.js?v=4" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>extras/jquery-ui-1.8.22.custom.min.js"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style type="text/css">
        .singleAddToTab {
            cursor: pointer;
            background-image: url(../images/AddToTabWorkArea.png);
            margin-left: 0px;
            float: none;
            background-color: white;
            border: 0px;
            margin-top: -4px;
        }

        .btnAddtoTab {
            padding: 8px 10px 8px 35px;
            font-size: 15px;
            padding-right: 10px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 5px;
            background-image: url(../images/plus-icon.png);
            background-position: 7px center;
            background-repeat: no-repeat;
        }

        .popupcontent {
            padding: 10px;
        }

        #floatingCirclesG {
            top: 50%;
            left: 50%;
            position: fixed;
        }

        .preview-popup {
            width: 100%;
            max-width: 1300px;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 10%;
            right: 0;
            margin: auto;
            background: #fff;
            height: 85%;
        }

        .chosen-results {
            max-height: 100px !important;
        }

        .chosen-container-single .chosen-single {
            line-height: 33px !important;
        }

        .chosen-single {
            height: 30px !important;
        }

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }

        .imgBtn {
            background: url(../images/plus.png) 90px 5px no-repeat;
            border-radius: 5px;
            border: none;
            cursor: pointer;
            border: solid 1px;
            background-color: white;
            height: 30px;
            /* margin-top: -20px; */
            /* padding-left: 8px; */
            font-size: 16px;
            background-origin: padding-box;
            padding-top: 1px;
            padding-left: 10px;
            padding-right: 30px;
            padding-bottom: 3px;
        }

        .ic-close {
            float: right;
            padding: 10px;
            cursor: pointer;
        }

        #drpGroups_chosen {
            display: block;
            margin-top: 5px;
        }

        #drpGroups_chosen {
            display: block;
            margin-top: 5px;
        }

        #mailGroups_chosen {
            display: block;
        }

        #mailGroups_chosen {
            display: block;
        }

        .btn-sm {
            width: 60px;
            height: 30px;
            line-height: 30px;
            margin: 10px;
        }
    </style>
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>

    <div class="popup-mainbox preview-popup" id="AddToTab_popup" style="display: none; height: 460px; width: 395px; max-width: 395px; max-height: 460px; margin-top: 20px;">
        <div class="popupcontrols popup-head" style="width: 100%; height: 60px;">
            Select Folder
        <span id="Span2" class="ic-icon ic-close popupclose"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%; background: #eaeaea; border-top: 2px solid darkgray;">
            <div class="popup-scroller" style="height: 75%; background: #eaeaea;">
                <p style="margin: 20px">
                    <uc1:ucAddToTab ID="ucAddToTab" runat="server" />
                </p>
            </div>
            <div class="popup-btn" style="background: #eaeaea;">
                <asp:Button ID="btnSelectAddToTab" Text="Save" runat="server" CssClass="btn green" OnClick="btnSelectAddToTab_Click" Style="margin-right: 5px;" />
                <input id="btnCancelAddToTab" type="button" value="Cancel" class="btn green" />
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnFolderId" runat="server" />
    <asp:HiddenField ID="hdnShareIds" runat="server" />
    <asp:HiddenField ID="hdnSelectedFolderID" runat="server" />
    <asp:HiddenField ID="hdnSelectedDividerID" runat="server" />
    <asp:HiddenField ID="hdnSelectedFolderText" runat="server" />
    <asp:HiddenField runat="server" ID="hdnFileName" />
    <asp:HiddenField runat="server" ID="hdnFileId" />
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>EdForms Folders</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="popup-mainbox preview-popup" id="preview_popup" style="display: none">
        <div class="popupcontrols" style="width: 100%; height: 6%;">
            <table style="width: 100%; height: 100%">
                <tr style="height: 6%">
                    <td>
                        <input type="button" id="btnSpltBtn" runat="server" value="Tab" onclick="ShowAddToTabPopup();" style="margin: 10px;" class="btn green btnAddtoTab" title="Add To Tab" />
                        <span id="popupclose" class="ic-icon ic-close"></span>
                    </td>
                </tr>
            </table>
        </div>
        <div class="popupcontent" style="width: 100%; height: 94%;">
            <iframe id="reviewContent" style="width: 100%; height: 100%;"></iframe>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <asp:UpdatePanel runat="server" ID="UpdatePanel1" DefaultButton="btnArchiveSearch">
                <ContentTemplate>
                    <div class="form-containt listing-contant modify-contant">
                        <%--<div id="workareatreeview" class="content-box listing-view" style="padding-top: 3%; text-align: center;">--%>
                        <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                        <div style="float: left;">
                            <table>
                                <tr>
                                    <td>
                                        <label style="font-weight: 700; font-size: 15px;">Select Any Folder</label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <select id="comboBoxFolder">
                                            <%--<option>Select Folder to upload LogForm</option>--%>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="float: right; margin-bottom: 15px">
                            <asp:Button ID="btnAddtoTab" Text="Tab" CssClass="btn green btnAddtoTab" runat="server" ToolTip="Add To Tab"></asp:Button>
                        </div>
                        <%--Notify--%>
                            <asp:Panel class="popup-mainbox" ID="DivNotifyConfirmation" Style="display: none"
                                runat="server">
                                <div>
                                    <div class="popup-head" id="PopupHeaderNotify">
                                        Resend Form
                                    </div>
                                    <div class="popup-scroller">
                                        <label style="margin: 20px; font-size: 15px;">
                                            Name :
                                        </label>
                                        <p style="margin-top: 5px">
                                            <asp:TextBox ID="textName" TextMode="SingleLine" runat="server" Width="100%" />
                                        </p>
                                    </div>
                                    <div class="popup-scroller">
                                        <label style="margin: 20px; font-size: 15px;">
                                            Message :
                                        </label>
                                        <p style="margin-top: 5px">
                                            <asp:TextBox ID="textMessage" TextMode="MultiLine" runat="server" Width="100%" />
                                        </p>
                                    </div>
                                    <div class="popup-btn">
                                        <input id="ButtonNotifyOkay" type="button" value="Send" class="btn green" style="margin-right: 15px" />
                                        <input id="ButtonNotifyCancel" onclick="RemoveMessage();" type="button" value="Cancel" class="btn green" />
                                    </div>
                                </div>
                            </asp:Panel>
                            <%--Notify--%>
                        <div class="table-scroll">
                            <asp:GridView ID="gridView" runat="server" OnSorting="gridView_Sorting" OnRowDataBound="gridView_RowDataBound"
                                AllowSorting="True" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover datatable listing-datatable" Width="100%" Style="margin-top: 20px;">
                                <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                <AlternatingRowStyle CssClass="odd" />
                                <RowStyle CssClass="even" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="3%"
                                        ItemStyle-CssClass="table_tekst_edit">
                                        <HeaderTemplate>
                                            <asp:CheckBox Text="" ID="chkSelectAll" runat="server" onclick="checkAll(this);" CssClass="chkpendingAll chkinboxAll" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList chkinbox" onclick="Check_Click(this);" />
                                            <asp:HiddenField Value='<%#Eval("EdFormsUserShareId")%>' runat="server" ID="hdnEdFormsShareId" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FileName" HeaderText="File Name" SortExpression="FileName" ItemStyle-Width="25%" />
                                    <asp:BoundField DataField="ReceivedDate" HeaderText="Received Date" DataFormatString="{0:MM-dd-yyyy HH:mm:ss}" SortExpression="ReceivedDate" ItemStyle-Width="15%" />
                                    <asp:BoundField DataField="StatusName" HeaderText="Status" SortExpression="StatusName" ItemStyle-Width="10%" />
                                    <asp:BoundField DataField="ToEmail" HeaderText="Submitted By" SortExpression="ToEmail" ItemStyle-Width="15%" />
                                    <asp:BoundField DataField="SharedDate" HeaderText="Shared Date" DataFormatString="{0:MM-dd-yyyy HH:mm:ss}" SortExpression="SharedDate" ItemStyle-Width="15%" />
                                    <asp:TemplateField ShowHeader="true" ItemStyle-Width="20%">
                                        <HeaderTemplate>
                                            <asp:Label ID="lblMoveOrder" runat="server" Text="Action" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-preview" CausesValidation="False" ToolTip="Preview"
                                                CommandName="" OnClientClick='<%# "return ShowPreviewForEdForms(\"" + Eval("EdFormsUserShareId") + "\",\"" + Eval("FileName") + "\")" %>'></asp:LinkButton>
                                            <asp:HyperLink ID="lnkBtnDownload" runat="server" CausesValidation="false" CssClass="ic-icon ic-download downloadFile lnkBtnDownload" Target="_blank" />
                                            <asp:Button ID="btnAddToTab" runat="server" CssClass="icon-btn singleAddToTab" ToolTip="Add to tab" OnClientClick='<%# "return ShowPopForAddToTab(\"" + Eval("EdFormsUserShareId") + "\")" %>' />
                                            <%--Notify--%>
                                            <asp:LinkButton ID="lnkBtnNotify" ToolTip="Resend Form" runat="server" CssClass="ic-icon ic-notify" OnCommand="lnkBtnNotify_Command" CommandArgument='<%#string.Format("{0}{1}{2}", Eval("EdFormsUserShareId"), ",", Eval("ToEmail"))%>' CommandName='<%#string.Format("{0}", Eval("FileName"))%>'></asp:LinkButton>
                                            <ajaxToolkit:ModalPopupExtender ID="lnkNotify_ModalPopupExtender" runat="server"
                                                CancelControlID="ButtonNotifyCancel" OkControlID="ButtonNotifyOkay" TargetControlID="lnkBtnNotify"
                                                PopupControlID="DivNotifyConfirmation" BackgroundCssClass="ModalPopupBG">
                                            </ajaxToolkit:ModalPopupExtender>
                                            <ajaxToolkit:ConfirmButtonExtender ID="cbeNotify" runat="server" DisplayModalPopupID="lnkNotify_ModalPopupExtender"
                                                ConfirmText="" TargetControlID="lnkBtnNotify">
                                            </ajaxToolkit:ConfirmButtonExtender>
                                            <%--Notify--%>
                                            <asp:HiddenField runat="server" ID="hdnEdFormsUserId" Value='<%# Eval("EdFormsUserShareId")%>' />
                                            <asp:HiddenField runat="server" ID="hdnFileName" Value='<%#Eval("FileName")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label3" runat="server" CssClass="Empty" />There is no record for selected folder.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                        <div class="table-toolbar">
                            <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" PageSize="5" CssClass="opt-select" PagerStyle="NumericPages" ControlToPaginate="gridView" Style="height: 50px;"></cc1:WebPager>
                        </div>
                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
    </div>
    <script type="text/javascript">
        function pageLoad() {
            //if (DisplayDivider)
            //    DisplayDivider();

            if ('<%=gridView.Rows.Count %>' == 0) {
                $('#<%=btnAddtoTab.ClientID%>').prop("disabled", true);
                $('#<%=btnAddtoTab.ClientID%>').css("opacity", "0.65");
            }
            else {
                $('#<%=btnAddtoTab.ClientID%>').prop("disabled", false);
                $('#<%=btnAddtoTab.ClientID%>').css("opacity", "1");
            }

            LoadSubFolders();
            $("#comboBoxFolder").change(function () {
                showLoader();
                $('#<%= hdnFolderId.ClientID%>').val($("#comboBoxFolder").chosen().val());
                LoadEdFormsLogs();
            });

            $('#<%= btnSelectAddToTab.ClientID%>').click(function (e) {
                var selectedFolder = $("#combobox").chosen().val();
                var selectedDivider = $("#comboboxDivider").chosen().val();
                if (selectedFolder == "0" && selectedDivider == "0") {
                    alert("Please select at least one EdFile & one Tab");
                    e.preventDefault();
                    return false;
                }
                else if (selectedFolder == "0") {
                    alert("Please select at least one EdFile");
                    e.preventDefault();
                    return false;
                }
                else if (selectedDivider == "0") {
                    alert("Please select at least one Tab");
                    e.preventDefault();
                    return false;
                }
                var folderValue = $('.chosen-single').text();
                //if (selectedFolder == undefined || selectedFolder == null || selectedFolder.length <= 0) {
                //    alert('Please select folder to move request files.');
                //    e.preventDefault();
                //    return false;
                //}
                $('#overlay').css('z-index', '100010');
                $('#<%= hdnSelectedDividerID.ClientID%>').val(selectedDivider);
                $('#<%= hdnSelectedFolderText.ClientID%>').val(folderValue);
                return true;
            });

            var closePopup = document.getElementById("popupclose");
            closePopup.onclick = function () {
                var popup = document.getElementById("preview_popup");
                var overlay = document.getElementById("overlay");
                overlay.style.display = 'none';
                popup.style.display = 'none';
                $('#floatingCirclesG').css('display', 'block');
                enableScrollbar();
                $(this).parent().parent().find("input[type=checkbox]").prop("checked", false);
            };

            $('#<%=btnAddtoTab.ClientID%>').click(function (e) {
                DisplayDivider();
                e.preventDefault();
                if (checkInbox()) {
                    $('#AddToTab_popup,#overlay').show().focus();
                    $('#combobox').trigger('chosen:open');
                    e.stopPropagation();
                    disableScrollbar();
                }
            });

            $('.popupclose').click(function () {
                $('.popup-mainbox').hide();
                $('#overlay').hide();
                enableScrollbar();
            });

            $("#btnCancelAddToTab").click(function () {
                $(this).closest('.popup-mainbox').hide();
                var popup = document.getElementById("preview_popup");
                if (popup.style.display == "none") {
                    $('#overlay').hide();
                    enableScrollbar();
                }
            });
        }

        function ShowAddToTabPopup() {
            DisplayDivider();
            var popup = document.getElementById("preview_popup");
            popup.style.display = 'none';
            $('#AddToTab_popup,#overlay').show().focus();
            $('#combobox').trigger('chosen:open');
            disableScrollbar();
            return false;
        }

        function ShowPopForAddToTab(edFormsShareId) {
            DisplayDivider();
            $('#<%= hdnShareIds.ClientID %>').val(edFormsShareId); // set hidden field value
            $('#AddToTab_popup,#overlay').show().focus();
            $('#combobox').trigger('chosen:open');
            disableScrollbar();
            return false;
        }

        function checkInbox() {
            showLoader();
            var shareIds = "";
            if ($('table input[type="checkbox"]:checked').length > 0) {
                $('table input[type="checkbox"]:checked').each(function () {
                    if ($(this).parent().parent().find("input[type='hidden'][id$=hdnEdFormsShareId]") && $(this).parent().parent().find("input[type='hidden'][id$=hdnEdFormsShareId]").val())
                        shareIds += $(this).parent().parent().find("input[type='hidden'][id$=hdnEdFormsShareId]").val() + ",";
                });
                shareIds = shareIds.substring(0, shareIds.length - 1);
                $('#<%= hdnShareIds.ClientID %>').val(shareIds); // set hidden field value
                return true;
            }
            else {
                hideLoader();
                alert("Please Select atleast one file.");
                return false;
            }
        }

        function Check_Click(objRef) {
            var row = objRef.parentNode.parentNode.parentNode;
            var GridView = row.parentNode;
            var inputList = GridView.getElementsByTagName("input");

            for (var i = 0; i < inputList.length; i++) {
                var headerCheckBox = inputList[0];
                var checked = true;
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                        break;
                    }
                }
            }
            headerCheckBox.checked = checked;
        }

        function checkAll(objRef) {
            var inboxGridView = objRef.parentNode.parentNode.parentNode.parentNode;
            var inboxinputList = inboxGridView.getElementsByTagName("input");

            for (var i = 0; i < inboxinputList.length; i++) {
                var row = inboxinputList[i].parentNode.parentNode;
                if (inboxinputList[i].type == "checkbox" && objRef != inboxinputList[i]) {
                    if (objRef.checked) {
                        inboxinputList[i].checked = true;
                    }
                    else {
                        inboxinputList[i].checked = false;
                    }
                }
            }
        }

        function ShowPreviewForEdForms(edFormsUserShareId, fileName) {
            showLoader();
            $('#<%= hdnShareIds.ClientID %>').val(edFormsUserShareId);
            $.ajax({
                type: "POST",
                url: 'EdFormsFolder.aspx/GetPreviewURL',
                contentType: "application/json; charset=utf-8",
                data: "{ edFormsUserShareId:'" + edFormsUserShareId + "', fileName:'" + fileName + "'}",
                dataType: "json",
                success: function (data) {
                    if (data.d == "") {
                        hideLoader();
                        disableScrollbar();
                        alert("Failed to get object from cloud");
                        return false;
                    }
                    else
                        showPreviewUrl(data.d);
                },
                error: function (result) {
                    console.log('Failed' + result.responseText);
                    hideLoader();
                }
            });
            return false;
        }

        function showPreviewUrl(URL) {
            hideLoader();
            disableScrollbar();
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//"
                    + window.location.hostname
                    + (window.location.port ? ':' + window.location.port : '');
            }
            var popup = document.getElementById("preview_popup");
            var overlay = document.getElementById("overlay");
            $('#reviewContent').attr('src', './Preview.aspx?data=' + window.location.origin + URL.trim() + "?v=" + "<%= DateTime.Now.Ticks%>");
            //$('#reviewContent').attr('src', src.trim());
            overlay.style.display = 'block';
            popup.style.display = 'block';
            $('#floatingCirclesG').css('display', 'none');
            //$('#preview_popup').css('top', ($(window).scrollTop() + 50) + 'px');
            $('#preview_popup').focus();
            return false;
        }

        function showLoader() {
            $('#overlay').show().height($(document).height());
        }

        function hideLoader() {
            $('#overlay').hide();
        }

        function disableScrollbar() {
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }

        function enableScrollbar() {
            $('html, body').css({
                'overflow': 'auto',
                'height': 'auto'
            });
        }

        function LoadEdFormsLogs() {
            showLoader();
            $.ajax(
                {
                    type: "POST",
                    url: 'EdFormsFolder.aspx/LoadEdFormsUserFolder',
                    contentType: "application/json; charset=utf-8",
                    data: '{"folderId":"' + $('#<%= hdnFolderId.ClientID %>').val() + '"}',
                    dataType: "json",
                    success: function (result) {
                        hideLoader();
                        __doPostBack('', 'RefreshGrid@');
                    },
                    fail: function (data) {
                        alert("Failed to get Folders. Please try again!");
                        hideLoader();
                    }
                });
        }

        function LoadSubFolders() {
            showLoader();
            $("#folderList").html('');
            $("#comboBoxFolder").html('');
            $("#comboBoxFolder").chosen("destroy");
            $.ajax(
                {
                    type: "POST",
                    url: '../FlexWebService.asmx/GetFolderDetailsById',
                    contentType: "application/json; charset=utf-8",
                    data: '{"officeId":"' + '<%= Sessions.SwitchedSessionId %>' + '"}',
                     dataType: "json",
                     success: function (result) {

                         if (result.d) {
                             $.each(result.d, function (i, text) {
                                 $('<option />', { value: i, text: text }).appendTo($("#comboBoxFolder"));
                             });
                             if ($('#<%= hdnFolderId.ClientID %>').val() != "") {
                              $("#comboBoxFolder").val($("#" + '<%= hdnFolderId.ClientID%>').val()).trigger("chosen:updated");
                            }
                        }
                        $("#comboBoxFolder").chosen({ width: "315px" });
                        $chosen = $("#comboBoxFolder").chosen();
                        hideLoader();

                    },
                    fail: function (data) {
                        alert("Failed to get Folders. Please try again!");
                        hideLoader();
                    }
                });
        }

        function RemoveMessage() {
        $("#<%= textName.ClientID %>").val("");
        $("#<%= textMessage.ClientID %>").val("");
    }
    </script>
</asp:Content>
