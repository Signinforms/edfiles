﻿using Newtonsoft.Json;
using Shinetech.DAL;
using Shinetech.Engines;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Office_EdFormsFolder : System.Web.UI.Page
{
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
    public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    private readonly NLogLogger _logger = new NLogLogger();

    protected void Page_Load(object sender, EventArgs e)
    {
        var argument = Request.Form["__EVENTARGUMENT"];

        if (!Page.IsPostBack)
        {
            hdnFolderId.Value = Convert.ToString(Sessions.FolderId);
            GetData();
            BindGrid();
        }
        else
        {
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
            {
                BindGrid();
            }
        }
    }

    public void GetData()
    {
        DataTable dtEdForms = General_Class.GetEdFormsForUserFolder((int)Enum_Tatva.EdFormsStatus.Submit, (int)Enum_Tatva.EdFormsLog.Submit, Sessions.SwitchedRackspaceId, Sessions.FolderId > 0 ? Sessions.FolderId : 0);
        if (dtEdForms != null && dtEdForms.Rows.Count > 0)
        {
            dtEdForms.Columns.Add("StatusName");
            foreach (DataRow dr in dtEdForms.Rows)
            {
                dr["StatusName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.EdFormsStatus)Convert.ToInt32(dr["Status"].ToString()));
            }
        }
        this.EdFormsDataSource = dtEdForms;
    }

    private void BindGrid()
    {
        gridView.DataSource = this.EdFormsDataSource.AsDataView();
        gridView.DataBind();
        WebPager1.DataSource = this.EdFormsDataSource;
        WebPager1.DataBind();
    }

    [WebMethod]
    public static string GetPreviewURL(string edFormsUserShareId, string fileName)
    {
        try
        {
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            string errorMsg = string.Empty;
            string path = rackSpaceFileUpload.GeneratePath(fileName.ToLower(), Enum_Tatva.Folders.EdFormsShare, edFormsUserShareId.ToLower());
            string newFileName = rackSpaceFileUpload.GetObject(ref errorMsg, path, null, Enum_Tatva.Folders.EdFormsShare.GetHashCode());
            EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
            if (string.IsNullOrEmpty(errorMsg))
                return Common_Tatva.RackSpaceEdFormsDownload + "/" + Sessions.SwitchedRackspaceId + "/" + newFileName;
            else
                return string.Empty;
        }
        catch (Exception ex)
        {
            Office_EdFormsFolder edFormsFolder = new Office_EdFormsFolder();
            edFormsFolder._logger.Error(ex);
            return string.Empty;
        }
    }

    public DataTable EdFormsDataSource
    {
        get
        {
            return this.Session["DataSource_EdFormsUserFolder"] as DataTable;
        }
        set
        {
            this.Session["DataSource_EdFormsUserFolder"] = value;
        }
    }

    protected void WebPager1_PageIndexChanged(object sender, Shinetech.Framework.Controls.PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.EdFormsDataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object s, Shinetech.Framework.Controls.PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.EdFormsDataSource;
        WebPager1.DataBind();
    }

    protected void gridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.EdFormsDataSource);

        Source.Sort = e.SortExpression + " " + sortDirection;
        this.EdFormsDataSource = Source.ToTable();
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    [WebMethod(EnableSession = true)]
    public static void LoadEdFormsUserFolder(string folderId = null)
    {
        Office_EdFormsFolder edFormsFolder = new Office_EdFormsFolder();
        try
        {
            Sessions.FolderId = Convert.ToInt32(folderId);
            DataTable dtEdForms = General_Class.GetEdFormsForUserFolder((int)Enum_Tatva.EdFormsStatus.Submit, (int)Enum_Tatva.EdFormsLog.Submit, Sessions.SwitchedRackspaceId, string.IsNullOrEmpty(folderId) ? 0 : Convert.ToInt32(folderId));
            if (dtEdForms != null && dtEdForms.Rows.Count > 0)
            {
                dtEdForms.Columns.Add("StatusName");
                foreach (DataRow dr in dtEdForms.Rows)
                {
                    dr["StatusName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.EdFormsStatus)Convert.ToInt32(dr["Status"].ToString()));
                }
            }
            edFormsFolder.EdFormsDataSource = dtEdForms;
        }
        catch (Exception ex)
        {
            edFormsFolder._logger.Error(ex);
            edFormsFolder.EdFormsDataSource = new DataTable();
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
        }
    }

    protected void gridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
                string edFormsUserShareId = drv.Row["EdFormsUserShareId"].ToString();
                string fileName = drv.Row["FileName"].ToString();
                HyperLink lnkBtnDownload = (e.Row.FindControl("lnkBtnDownload") as HyperLink);
                if (lnkBtnDownload != null)
                {
                    lnkBtnDownload.Attributes.Add("href", "PdfViewer.aspx?ID=" + edFormsUserShareId + "&data=" + QueryString.QueryStringEncode("ID=" + fileName + "&folderID=" + (int)Enum_Tatva.Folders.EdFormsShare + "&sessionID=" + Sessions.SwitchedRackspaceId));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
    }

    protected void btnSelectAddToTab_Click(object sender, EventArgs e)
    {
        try
        {
            var shareIds = hdnShareIds.Value;
            string folderId = Convert.ToString(Sessions.FolderId);
            int dividerId = Convert.ToInt32(hdnSelectedDividerID.Value);
            string errorMsg = string.Empty;
            if (!string.IsNullOrEmpty(shareIds))
            {
                DataTable dtFiles = General_Class.GetEdFormsUserShareById(shareIds);
                dtFiles.AsEnumerable().ToList().ForEach(d =>
                {
                    try
                    {
                        var file = d.Field<string>("FileName");
                        string edFormsUserShareId = d.Field<string>("EdFormsUserShareId").ToString();
                        RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                        string path = rackSpaceFileUpload.GeneratePath(file.ToLower(), Enum_Tatva.Folders.EdFormsShare, edFormsUserShareId.ToLower());
                        string dir = Server.MapPath("~/" + Common_Tatva.RackSpaceEdFormsDownload + Path.DirectorySeparatorChar + Sessions.SwitchedRackspaceId);
                        if (!Directory.Exists(dir))
                        {
                            Directory.CreateDirectory(dir);
                        }
                        string fileName = rackSpaceFileUpload.GetObject(ref errorMsg, path, Sessions.SwitchedRackspaceId, (int)Enum_Tatva.Folders.EdFormsShare);
                        if (string.IsNullOrEmpty(errorMsg))
                        {
                            var filePath = dir + "\\" + fileName;

                            string encodeName = HttpUtility.UrlPathEncode(file).Replace("%", "$");
                            encodeName = encodeName.Replace(encodeName, encodeName.Replace("$20", " "));
                            encodeName = Common_Tatva.SubStringFilename(encodeName);
                            encodeName = encodeName.Replace(encodeName, encodeName.Replace(" ", "$20"));
                            string vpath = Server.MapPath(string.Format("~/{0}", CurrentVolume));
                            string pathName = CurrentVolume + Path.DirectorySeparatorChar + folderId + Path.DirectorySeparatorChar + dividerId + Path.DirectorySeparatorChar + encodeName;

                            int documentOrder = FlashViewer.GetDocumentOrder(Convert.ToInt32(dividerId));
                            string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, folderId, Path.DirectorySeparatorChar, dividerId);
                            if (!Directory.Exists(dstpath))
                            {
                                Directory.CreateDirectory(dstpath);
                            }

                            dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);
                            if (File.Exists(dstpath))
                            {
                                dstpath = Common_Tatva.GetRenameFileName(dstpath);
                            }

                            File.Copy(filePath, dstpath, true);
                            string newFilename = Path.GetFileNameWithoutExtension(dstpath);
                            string displayName = HttpUtility.UrlDecode(newFilename.Replace('$', '%').ToString());
                            string documentIDOrder = Common_Tatva.InsertDocument(dividerId, pathName, documentOrder, displayName, Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()));
                            if (documentIDOrder == "0")
                            {
                                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to move file. There is a problem with the file format." + "\");hideLoader();", true);
                            }
                            else
                            {
                                General_Class.AuditLogByDocId(Convert.ToInt32(documentIDOrder.Split('#')[0]), Sessions.RackSpaceUserID, Enum_Tatva.Action.Create.GetHashCode());
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(folderId), Convert.ToInt32(dividerId), Convert.ToInt32(documentIDOrder.Split('#')[0]), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.InboxUpload.GetHashCode(), 0, 1);
                                General_Class.MoveEdFormsUserShare(edFormsUserShareId, Convert.ToInt32(folderId), Convert.ToInt32(dividerId), Sessions.UserId); ;
                                General_Class.ChangeEdFormsUserShareStatus(edFormsUserShareId, (int)Enum_Tatva.EdFormsStatus.Move);
                                string IP = string.Empty;
                                try
                                {
                                    IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                                }
                                catch (Exception ex)
                                {
                                    _logger.Info(ex);
                                }
                                General_Class.InsertIntoEdFormsUserLogs(edFormsUserShareId, (byte)Enum_Tatva.EdFormsStatus.Move, IP);
                                File.Delete(filePath);
                                rackSpaceFileUpload.DeleteObject(ref errorMsg, path);
                                //ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "File has been moved." + "\");hideLoader();", true);
                            }
                        }
                        else
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to get object from cloud." + "\");hideLoader();", true);
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                        Common_Tatva.WriteErrorLog("", "", "", ex);
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to move file." + "\");hideLoader();", true);
                    }

                });
            }
            string message = "File(s) have been moved successfully.";
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + message + "\");hideLoader();", true);
            GetData();
            BindGrid();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
        finally
        {
            ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
        }
    }
    protected void lnkBtnNotify_Command(object sender, CommandEventArgs e)
    {
        EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
        string[] details = Convert.ToString(e.CommandArgument).Split(',');
        bool isSent = eFileFolderJSONWS.NotifyBackForEdForms(details[0], e.CommandName, details[1], textName.Text, textMessage.Text, Sessions.SwitchedEmailId);
        if (isSent)
        {
            GetData();
            BindGrid();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert('An EdForm has been resent successfully.');", true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert('An error occured while resending an EdForm. Please try again later!!');", true);
        }
    }

}