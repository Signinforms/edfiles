﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EdFormsLogs.aspx.cs" Inherits="Office_EdFormsLogs" %>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/style.css?v=2" rel="Stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=2" rel="stylesheet" />
    <link rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/style.min.css" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/edFiles/EdFilesLogs.js?v=4" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>extras/jquery-ui-1.8.22.custom.min.js"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style type="text/css">
        .mainBox {
            display: inline-flex;
            width: 100%;
        }

        .selectStatus {
            margin-left: 1%;
        }

        .drpStatus {
            height: 30px;
            width: 100px;
            margin-top: 10px;
        }

        #comboBoxUser_chosen {
            top: 10px;
        }

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }

        #floatingCirclesG {
            top: 50%;
            left: 50%;
            position: fixed;
        }

        .chosen-results {
            max-height: 100px !important;
        }

        .chosen-container-single .chosen-single {
            line-height: 33px !important;
        }

        .chosen-single {
            height: 30px !important;
        }

        .selectBox-outer {
            position: relative;
            /*display: inline-block;*/
            margin-right: 5px;
            font-family: 'open sans', sans-serif;
        }

        .selectBox-outermail {
            position: relative;
            font-family: 'open sans', sans-serif;
        }

        .selectBox select {
            width: 100%;
        }

        .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            font-size: 15px;
        }
    </style>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>EdForms Logs</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnUserId" />
    <div class="inner-wrapper">
        <div class="page-container">
            <div class="dashboard-container">
                <asp:Panel ID="panelMain" runat="server" Visible="true">
                    <div class="mainBox">
                        <div class="selectUser">
                            <asp:Label Style="font-size: 15px; font-weight: bold" runat="server" Text="Select User"></asp:Label>
                            <br />
                            <select id="comboBoxUser" class="text ui-widget-content ui-corner-all" style="border: 1px solid black;">
                            </select>
                        </div>
                        <div class="selectStatus">
                            <asp:Label ID="Label2" Style="font-size: 15px; font-weight: bold" runat="server" Text="Select Status"></asp:Label>
                            <br />
                            <asp:DropDownList CssClass="text ui-widget-content ui-corner-all drpStatus" ID="drpStatus" AutoPostBack="true" runat="server" OnSelectedIndexChanged="drpStatus_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:GridView Style="margin-top: 15px;" ID="EdFormsLogsGrid" runat="server" AllowSorting="True" DataKeyNames="EdFormsUserId"
                                AutoGenerateColumns="false" BorderWidth="2" OnRowDataBound="EdFormsLogsGrid_RowDataBound"
                                CssClass="datatable listing-datatable boxWidth" OnSorting="EdFormsLogsGrid_Sorting">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="odd" />
                                <RowStyle CssClass="even" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:BoundField DataField="Filename" HeaderText="Filename" SortExpression="Filename" ItemStyle-Width="12%" />
                                    <asp:BoundField DataField="ActionName" HeaderText="Action" SortExpression="ActionName" ItemStyle-Width="11%" />
                                    <asp:BoundField DataField="CreatedDate" HeaderText="Action Date" DataFormatString="{0:MM-dd-yyyy HH:mm:ss}" ItemStyle-Width="12%"
                                        SortExpression="CreatedDate" />
                                    <asp:BoundField DataField="ToEmail" HeaderText="To Email" SortExpression="ToEmail" ItemStyle-Width="15%" />
                                    <asp:BoundField DataField="SharedDate" HeaderText="Shared Date" DataFormatString="{0:MM-dd-yyyy HH:mm:ss}" ItemStyle-Width="12%"
                                        SortExpression="SharedDate" />
                                    <asp:BoundField DataField="StatusName" HeaderText="Current Status" SortExpression="StatusName" ItemStyle-Width="10%" />
                                    <asp:TemplateField SortExpression="FolderName" HeaderText="Folder Name" ItemStyle-Width="10%">
                                        <%--<HeaderTemplate>
                                            <asp:Label runat="server" Text="Folder Name"></asp:Label>
                                        </HeaderTemplate>--%>
                                        <ItemTemplate>
                                            <asp:HyperLink CssClass="edFileLink" ID="navigateFolder" runat="server"></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="IP" HeaderText="IP Address" ItemStyle-Width="13%"
                                        HtmlEncode="false" SortExpression="IP" />
                                    <asp:TemplateField ShowHeader="true">
                                        <HeaderTemplate>
                                            <asp:Label ID="Label8" runat="server" Text="Share Logs" />
                                        </HeaderTemplate>
                                        <HeaderStyle Width="230" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="" OnClientClick="EdFormsUserShareLogView(this)" CssClass="ic-icon ic-edfLogs"></asp:LinkButton>
                                            <asp:HiddenField ID="hdnEdFomsUserShareId" runat="server" Value='<%# Eval("EdFormsUserShareId")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <asp:Label ID="Label1" runat="server" CssClass="Empty" />There is no logs for this user.
                                </EmptyDataTemplate>
                            </asp:GridView>

                            <div class="prevnext" bgcolor="#f8f8f5" style="float: right; padding: 5px;">
                                <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                    PageSize="10" CssClass="paging_position " PagerStyle="NumericPages" ControlToPaginate="EdFormsLogsGrid"></cc1:WebPager>
                                <asp:HiddenField ID="FolderID" runat="server" />
                                <asp:HiddenField ID="SortDirectionLog" runat="server" />
                            </div>
                            <br />
                            <br />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function pageLoad() {
            LoadUsers();
            $("#comboBoxUser").change(function () {
                showLoader();
                $('#<%= hdnUserId.ClientID%>').val($("#comboBoxUser").chosen().val());
                LoadEdFormsLogs();
            });

            $('.edFileLink').click(function () {
                ClearFolderIdSource();
            });
        }

        function ClearFolderIdSource() {
            $.ajax(
                {
                    type: "POST",
                    url: '../EFileFolderJSONService.asmx/ClearFolderIdSource',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json"
                });
        }

        function EdFormsUserShareLogView(ele) {
            var shareId = $(ele).closest("td").find('[name*=hdnEdFomsUserShareId]').val();
            window.open("EdFormsUserShareLogs.aspx?id=" + shareId);
        }

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
        }

        function LoadEdFormsLogs() {
            showLoader();
            $.ajax(
                {
                    type: "POST",
                    url: 'EdFormsLogs.aspx/LoadEdFormsLog',
                    contentType: "application/json; charset=utf-8",
                    data: '{"uid":"' + $('#<%= hdnUserId.ClientID %>').val() + '", "status":"' + <%= drpStatus.SelectedItem.Value%> + '"}',
                    dataType: "json",
                    success: function (result) {
                        hideLoader();
                        __doPostBack('', 'RefreshGrid@');
                    },
                    fail: function (data) {
                        alert("Failed to get Folders. Please try again!");
                        hideLoader();
                    }
                });
        }

        function LoadUsers() {
            showLoader();
            $("#comboBoxUser").html('');
            $("#comboBoxUser").chosen("destroy");
            $.ajax(
                {
                    type: "POST",
                    url: 'EdForms.aspx/GetAllUsersWithOffice',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result.d) {
                            $.each(result.d, function (i, text) {
                                $('<option />', { value: i, text: text }).appendTo($("#comboBoxUser"));
                            });
                        }
                        if ($('#<%= hdnUserId.ClientID %>').val() != "") {
                            $("#comboBoxUser").val($("#" + '<%= hdnUserId.ClientID%>').val()).trigger("chosen:updated");
                        }
                        $("#comboBoxUser").chosen({ width: "313px" });
                        $chosen = $("#comboBoxUser").chosen();
                        hideLoader();
                    },
                    fail: function (data) {
                        alert("Failed to get users. Please try again!");
                    }
                });
        }
    </script>
</asp:Content>

