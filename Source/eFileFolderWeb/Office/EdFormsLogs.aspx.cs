﻿using Shinetech.DAL;
using Shinetech.Engines;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Office_EdFormsLogs : System.Web.UI.Page
{
    private readonly NLogLogger _logger = new NLogLogger();
    private static string UID = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        var argument = Request.Form["__EVENTARGUMENT"];
        if (!Page.IsPostBack)
        {
            hdnUserId.Value = Sessions.SwitchedRackspaceId;
            UID = hdnUserId.Value; 
            var item = new Dictionary<int, string>();
            item.Add(0, "Select");
            foreach (int value in Enum.GetValues(typeof(Enum_Tatva.EdFormsFilterStatus)))
            {
                var name = Enum.GetName(typeof(Enum_Tatva.EdFormsFilterStatus), value);
                item.Add(value, name);
            }
            drpStatus.DataSource = item;
            drpStatus.DataTextField = "Value";
            drpStatus.DataValueField = "Key";
            drpStatus.DataBind();
            drpStatus.SelectedIndex = 3;
            GetData();
            BindGrid();
        }
        else
        {
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
            {
                BindGrid();
            }
        }
    }

    private void GetData()
    {
        try
        {
            DataTable dtEdForms = General_Class.GetEdFormsUserLogs(Sessions.EdFormsUserId, Sessions.SwitchedRackspaceId, Enum_Tatva.EdFormsStatus.Submit.GetHashCode());
            if (dtEdForms != null && dtEdForms.Rows.Count > 0)
            {
                dtEdForms.Columns.Add("ActionName");
                dtEdForms.Columns.Add("StatusName");
                foreach (DataRow dr in dtEdForms.Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.EdFormsLog)Convert.ToInt32(dr["Action"].ToString()));
                    dr["StatusName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.EdFormsStatus)Convert.ToInt32(dr["Status"].ToString()));
                }
            }
            this.DataSource = dtEdForms;
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    private void BindGrid()
    {
        try
        {
            EdFormsLogsGrid.DataSource = this.DataSource;
            EdFormsLogsGrid.DataBind();
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["EdFormsUserLogs"] as DataTable;
        }
        set
        {
            this.Session["EdFormsUserLogs"] = value;
        }
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirectionLog.Value))
            {
                this.SortDirectionLog.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirectionLog.Value);

        }
        set
        {
            this.SortDirectionLog.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void EdFormsLogsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                System.Data.DataRowView drvnew = e.Row.DataItem as DataRowView;
                string FileFolderID = drvnew.Row["FolderId"].ToString();
                string FolderName = drvnew.Row["FolderName"].ToString();
                int status = Convert.ToInt32(drvnew.Row["Status"].ToString());
                HyperLink hl = e.Row.FindControl("navigateFolder") as HyperLink;
                if ((status == (int)Enum_Tatva.EdFormsStatus.Submit || status == (int)Enum_Tatva.EdFormsStatus.Move) && !string.IsNullOrEmpty(FileFolderID))
                {
                    hl.NavigateUrl = "~/Office/FileFolderBox.aspx?id=" + FileFolderID;
                    hl.Text = FolderName;
                }
                else
                {
                    hl.Text = "-";
                    hl.Style.Add("text-decoration", "none");
                    hl.Style.Add("color", "#444");
                }
            }
            catch (Exception ex)
            {
                _logger.Info(ex);
            }
        }
    }

    protected void EdFormsLogsGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortDirection = "";
            string sortExpression = e.SortExpression;
            if (this.Sort_Direction == SortDirection.Ascending)
            {
                this.Sort_Direction = SortDirection.Descending;
                sortDirection = "DESC";
            }
            else
            {
                this.Sort_Direction = SortDirection.Ascending;
                sortDirection = "ASC";
            }
            DataView Source = new DataView(this.DataSource);
            Source.Sort = e.SortExpression + " " + sortDirection;
            this.DataSource = Source.ToTable();
            EdFormsLogsGrid.DataSource = this.DataSource;
            EdFormsLogsGrid.DataBind();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    protected void WebPager1_PageIndexChanged(object sender, Shinetech.Framework.Controls.PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    [WebMethod(EnableSession = true)]
    public static void LoadEdFormsLog(string uid, int? status)
    {
        try
        {
            Office_EdFormsLogs edFormsLogs = new Office_EdFormsLogs();
            UID = uid;
            DataTable dtEdForms = General_Class.GetEdFormsUserLogs(Sessions.EdFormsUserId, UID, status > 0 ? status : null);
            if (dtEdForms != null && dtEdForms.Rows.Count > 0)
            {
                dtEdForms.Columns.Add("ActionName");
                dtEdForms.Columns.Add("StatusName");
                foreach (DataRow dr in dtEdForms.Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.EdFormsLog)Convert.ToInt32(dr["Action"].ToString()));
                    dr["StatusName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.EdFormsStatus)Convert.ToInt32(dr["Status"].ToString()));
                }
            }
            edFormsLogs.DataSource = dtEdForms;
        }
        catch (Exception ex)
        {
            Office_EdFormsLogs edForms = new Office_EdFormsLogs();
            edForms._logger.Error(ex);
        }
    }

    protected void drpStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DataTable dtEdForms = General_Class.GetEdFormsUserLogs(Sessions.EdFormsUserId, string.IsNullOrEmpty(UID) ? Sessions.SwitchedRackspaceId : UID, Convert.ToInt32(drpStatus.SelectedItem.Value) > 0 ? (int?)Convert.ToInt32(drpStatus.SelectedItem.Value) : null);
            if (dtEdForms != null && dtEdForms.Rows.Count > 0)
            {
                dtEdForms.Columns.Add("ActionName");
                dtEdForms.Columns.Add("StatusName");
                foreach (DataRow dr in dtEdForms.Rows)
                {
                    dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.EdFormsLog)Convert.ToInt32(dr["Action"].ToString()));
                    dr["StatusName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.EdFormsStatus)Convert.ToInt32(dr["Status"].ToString()));
                }
            }
            this.DataSource = dtEdForms;
            BindGrid();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }
}