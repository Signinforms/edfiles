﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EdFormsUserShareLogs.aspx.cs" Inherits="Office_EdFormsUserShareLogs" %>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/style.css" rel="Stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/colorbox.css" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>css/main.css?v=2" rel="stylesheet" />
    <link rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>lib/TreeView/style.min.css" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/js/edFiles/EdFilesLogs.js?v=4" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/js/jquery.colorbox-min.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>extras/jquery-ui-1.8.22.custom.min.js"></script>
    <style>
        .imgbuttn1 {
            color: white;
            background-color: #0047b3;
            border: none;
            min-height: 30px;
            border-radius: 5px;
            margin: 0 0 0 10px;
            cursor: pointer;
            float: right;
        }

        .pagerAdmin select {
            margin-top: 10px;
            height: 25px;
            width: 40px;
            font-size: 15px;
        }

        .drpDate {
            height: 30px !important;
            border-radius: 10px;
            margin-top: 10px;
            padding-left: 10px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            <%--$("#" + '<%= modifiedDate.ClientID%>').change(function () {
                var selectedTime = $("#" + '<%= modifiedDate.ClientID%>').val();
                selectedTime = selectedTime == "All Logs" ? "" : selectedTime;
                $.ajax(
                    {
                        type: "POST",
                        url: 'EdFormsUserShareLogs.aspx/GetEdFormsUserShareLogData',
                        data: "{ edFormsUserShareId: '" + $("#" + '<%= hdnedFormsUserShareId.ClientID %>').val() + "' , datetime: '" + selectedTime + "' }",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            __doPostBack('', 'refreshgrid@');
                        },
                        error: function (result) {
                            console.log('Failed' + result.responseText);
                        }
                    });
            });--%>
        });

        function openwindow() {
            var id = $("#" + '<%= hdnedFormsUserShareId.ClientID %>').val();
            var url = 'ExportEdFormsUserShareLogs.aspx?id=' + id + '&datetime=' + $("#" + '<%= DropDate.ClientID%>').val();
            var name = 'ExportEdFormsUserShareLogs';
            var iWidth = 250;
            var iHeight = 200;
            var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
            var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
            window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',toolbar=no,menubar=no,scrollbars=no,resizeable=no,location=no,status=no');
        }

    </script>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1 style="margin-bottom: 5px; margin-top: 5px;">EdForms User Share Logs</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="inner-wrapper">
        <div class="page-container form-containt">
            <div class="dashboard-container">
                <asp:Panel ID="panelMain" runat="server" Visible="true">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:HiddenField runat="server" ID="hdnedFormsUserShareId" />
                            <asp:HiddenField ID="SortDirectionOffice" runat="server" />
                            <lable style="font-size: 15px; font-weight: bold;">Select Date</lable>
                            <br />
                            <asp:DropDownList ID="DropDate" AutoPostBack="true" runat="server" CssClass="drpDate" OnSelectedIndexChanged="DropDate_SelectedIndexChanged"></asp:DropDownList>
                            <%--<select id="modifiedDate" runat="server" class="drpDate"></select>--%>
                            <button class="imgbuttn1" onclick="openwindow();">Export to Excel</button>
                            <div style="overflow: auto">
                                <asp:GridView ID="ShareLogs" runat="server" OnRowDataBound="ShareLogs_RowDataBound" CssClass="datatable listing-datatable boxWidth"
                                    AllowSorting="true" AutoGenerateColumns="false" BorderWidth="2" OnSorting="shareLogs_Sorting" Style="margin-top: 20px">
                                    <Columns>
                                        <asp:BoundField DataField="ModifiedDate" SortExpression="ModifiedDate" HeaderText="Modified Date" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <cc1:WebPager ID="WebPager1" runat="server" Style="float: right" OnPageIndexChanged="WebPager1_PageIndexChanged" OnPageSizeChanged="WebPager1_PageSizeChanged"
                                PageSize="10" CssClass="paging_position pagerAdmin" PagerStyle="NumericPages" ControlToPaginate="ShareLogs"></cc1:WebPager>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
        </div>
    </div>
</asp:Content>
