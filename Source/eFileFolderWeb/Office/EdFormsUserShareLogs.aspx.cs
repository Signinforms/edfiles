﻿using Newtonsoft.Json;
using Shinetech.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Office_EdFormsUserShareLogs : System.Web.UI.Page
{
    private readonly NLogLogger _logger = new NLogLogger();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            try
            {
                string edFormsUserShareId = Request.QueryString["id"];
                if (!string.IsNullOrEmpty(edFormsUserShareId))
                {
                    hdnedFormsUserShareId.Value = edFormsUserShareId;
                    GetData();
                    BindGrid();
                    DropDate.Items.Add(new ListItem("All Logs", null));
                    if (this.ShareDataSource != null && this.ShareDataSource.Rows.Count > 0)
                    {
                        foreach (DataRow row in this.ShareDataSource.Rows)
                        {
                            DropDate.Items.Add(new ListItem(row.Field<DateTime>("ModifiedDate").ToString(), row.Field<DateTime>("ModifiedDate").ToString()));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            }
        }
        else
        {
            var argument = Request.Form["__EVENTARGUMENT"];
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower() == "refreshgrid@")
            {
                BindGrid();
            }
        }
    }

    public void GetData()
    {
        DataTable dt = General_Class.GetEdFormsUserShareLogs(hdnedFormsUserShareId.Value, (DropDate.SelectedValue == "All Logs" || string.IsNullOrEmpty(DropDate.SelectedValue)) ? null : Convert.ToDateTime(DropDate.SelectedValue).ToString("yyyy-MM-dd HH:mm:ss"));
        if (dt != null && dt.Rows.Count > 0)
        {
            foreach (DataRow dr in dt.Rows)
            {
                var data = JsonConvert.DeserializeObject<List<LogKeyValuePairNew>>(dr["SavedValue"].ToString());
                foreach (var item in data)
                {
                    if (!dt.Columns.Contains(item.Key))
                        dt.Columns.Add(item.Key);
                    dr[item.Key] = item.Value;
                }
            }
            this.ShareDataSource = dt;
        }
    }

    private void BindGrid()
    {
        try
        {
            if (ShareLogs.Columns.Count > 1)
            {
                int count = ShareLogs.Columns.Count;
                for (int i = 1; i < count; i++)
                {
                    ShareLogs.Columns.RemoveAt(1);
                }
            }
            ShareLogs.DataSource = this.ShareDataSource;
            ShareLogs.DataBind();
            WebPager1.DataSource = this.ShareDataSource;
            WebPager1.DataBind();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    protected void shareLogs_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortDirection = "";
            string sortExpression = e.SortExpression;
            if (this.Sort_Direction_Office == SortDirection.Ascending)
            {
                this.Sort_Direction_Office = SortDirection.Descending;
                sortDirection = "DESC";
            }
            else
            {
                this.Sort_Direction_Office = SortDirection.Ascending;
                sortDirection = "ASC";
            }
            DataView Source = new DataView(this.ShareDataSource);
            Source.Sort = e.SortExpression + " " + sortDirection;

            this.ShareDataSource = Source.ToTable();
            BindGrid();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    public DataTable ShareDataSource
    {
        get
        {
            return this.Session["EdFormsUserShareLogs_Users"] as DataTable;
        }
        set
        {
            this.Session["EdFormsUserShareLogs_Users"] = value;
        }
    }

    private SortDirection Sort_Direction_Office
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirectionOffice.Value))
            {
                SortDirectionOffice.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirectionOffice.Value);

        }
        set
        {
            this.SortDirectionOffice.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void WebPager1_PageIndexChanged(object sender, Shinetech.Framework.Controls.PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.ShareDataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object s, Shinetech.Framework.Controls.PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.ShareDataSource;
        WebPager1.DataBind();
    }

    protected void ShareLogs_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
                string SavedValues = drv.Row["SavedValue"].ToString();
                var data = JsonConvert.DeserializeObject<List<LogKeyValuePairNew>>(SavedValues);
                foreach (var item in data)
                {
                    bool isFound = false;
                    foreach (DataControlField col in ShareLogs.Columns)
                    {
                        if (col.HeaderText == item.Key)
                        {
                            isFound = true;
                            break;
                        }
                    }
                    if (!isFound)
                    {
                        BoundField column = new BoundField();
                        column.DataField = item.Key;
                        column.HeaderText = item.Key;
                        ShareLogs.Columns.Add(column);
                    }
                }

            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    protected void DropDate_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetData();
        BindGrid();
    }

    ///// <summary>
    ///// Get EdFormsUserShareLogs
    ///// </summary>
    ///// <param name="edFormsUserShareId"></param>
    ///// <param name="datetime"></param>
    ///// <returns></returns>
    //[WebMethod]
    //public static void GetEdFormsUserShareLogData(string edFormsUserShareId, string datetime)
    //{
    //    try
    //    {
    //        Office_EdFormsUserShareLogs shareLogs = new Office_EdFormsUserShareLogs();
    //        DataTable dt = General_Class.GetEdFormsUserShareLogs(edFormsUserShareId, string.IsNullOrEmpty(datetime) ? null : Convert.ToDateTime(datetime).ToString("yyyy-MM-dd HH:mm:ss"));
    //        if (dt != null && dt.Rows.Count > 0)
    //        {
    //            foreach (DataRow dr in dt.Rows)
    //            {
    //                var data = JsonConvert.DeserializeObject<List<LogKeyValuePairNew>>(dr["SavedValue"].ToString());
    //                foreach (var item in data)
    //                {
    //                    if (!dt.Columns.Contains(item.Key))
    //                        dt.Columns.Add(item.Key);
    //                    dr[item.Key] = item.Value;
    //                }
    //            }
    //            shareLogs.ShareDataSource = dt;
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
    //    }
    //}
}