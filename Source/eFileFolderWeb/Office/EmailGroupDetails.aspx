﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmailGroupDetails.aspx.cs" Inherits="Office_EmailGroupDetails" MasterPageFile="~/MasterPage.master"%>

<%@ Register Src="../Controls/FileRequestControl.ascx" TagName="FileRequestControl"
    TagPrefix="uc2" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

     <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
    <style>
         #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }

        .left-content img {
            margin-bottom:0px;
        }

        .form-containt .content-box fieldset input[type="text"], .form-containt .content-box fieldset input[type="password"] {
            max-width:250px !important;
        }
        
        #btnAddDetails, #btnRemoveDetails {
            margin-top:30px;
            height:30px;
        }

        .left-content {
            float:none;
            margin:0 auto;
        }
    </style>

    <script type="text/javascript">

        function CheckEmailValidation(fieldObj) {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(fieldObj)) {
                return true;
            }
            else {
                return false;
            }
        }

        $(document).ready(function () {
            var groupId = window.location.search.split("?")[1].split('&')[0].split("=")[1];
            GetGroupDetails(groupId);

            $("#btnDeleteIndexCancel").click(function () {
                $('#DivDeleteConfirmIndex').hide();
                $('#overlay').hide();
                $("#floatingCirclesG").css('display', 'none');
            });

            $("#btnIndexSave").click(function (e) {

                if ($("#txtGroupName").val().length <= 0) {
                    alert("Please enter Group Name!");
                    return false;
                }

                var arrIndex = [];
                for (i = 0; i < $('table#tblIndex tr').length; i++) {
                    var $item = $($('table#tblIndex tr')[i]);
                    if (($item.find('#txtToName').val() != null && $item.find('#txtToName').val() != "") &&
                        ($item.find('#txtEmail').val() != null && $item.find('#txtEmail').val() != "")) {
                        if (!CheckEmailValidation($item.find('#txtEmail').val()))
                        {
                            alert("Please enter valid Email!")
                            $item.find('#txtEmail').css('border', '1px solid red');
                            return false;
                        }
                        else if (!/^[a-zA-Z\s]+$/.test($item.find('#txtToName').val())) {
                            alert("Special characters are not allowed in ToName!")
                            $item.find('#txtToName').css('border', '1px solid red');
                            return false;
                        }
                        $item.find('#txtToName').css('border', 'solid 1px #c4c4c4');
                        $item.find('#txtEmail').css('border', 'solid 1px #c4c4c4');
                        arrIndex.push({ ToName: $item.find('#txtToName').val(), Email: $item.find('#txtEmail').val(), EmailID: $($item).find('input.hdnIndexId').val() });
                    }
                    else
                        deleteIndexData = deleteIndexData + $($item).find('input.hdnIndexId').val() + ",";
                }
                deleteIndexData = deleteIndexData.substr(0, deleteIndexData.lastIndexOf(','));
               
                var data = { "groupId": window.location.search.split("?")[1].split('&')[0].split("=")[1], "indexData": JSON.stringify(arrIndex), "deleteIndexData": deleteIndexData, "groupName": $("#txtGroupName").val() };
                $.ajax({
                    type: "POST",
                    url: "EmailGroupDetails.aspx/SaveEmails",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        deleteIndexData = "";
                        if (result.d == true) {
                            alert("Data has been saved successfully.");
                            $('#overlay').hide();
                            $("#floatingCirclesG").css('display', 'none');
                            window.open(window.location.origin  + "/Office/EmailGroupGrid.aspx", "toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no");
                        }
                        else
                            alert("Data has not been saved. Please try again later.");

                    }
                });
            });

            $("#btnIndexCancel").click(function (e) {
                e.preventDefault();
                $('#overlay').hide();
                $("#floatingCirclesG").css('display', 'none');
                $("#tblIndex").find("tr:gt(0)").remove();
                var row = $(".indexRow");
                //$("#txtGroupName").val("");
                $(row).find("#txtToName").val("");
                $(row).find("#txtEmail").val("");
                $(row).find("#btnAddDetails").parent().show();
                $(row).find("#btnRemoveDetails").parent().hide();
                e.stopPropagation();
            });
        });

        function GetGroupDetails(groupId) {
            var data = { "groupId": groupId }
            $.ajax({
                type: "POST",
                url: "EmailGroupDetails.aspx/GetEmails",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {

                    if (result.d.length > 0) {

                        if (JSON.parse(result.d).length == 1 && (JSON.parse(result.d)[0].EmailID == null || JSON.parse(result.d)[0].EmailID == undefined) && JSON.parse(result.d)[0].groupname != undefined && JSON.parse(result.d)[0].groupname.length > 0) {
                            $("#txtGroupName").val(JSON.parse(result.d)[0].groupname);
                        }
                        else {
                            var index = 0;
                            var arrIndex = [];
                            $.each($.parseJSON(result.d), function (i, e) {
                                arrIndex.push(e);
                            });
                            $("#tblIndex").find("tr:gt(0)").remove();
                            var row = $(".indexRow");
                            $(row).find("#txtToName").val("");
                            $(row).find("#txtEmail").val("");
                            $(row).find("#btnAddDetails").parent().show();
                            $(row).find("#btnRemoveDetails").parent().hide();
                            if (arrIndex.length > 0) {
                                $(row).find(".hdnIndexId").val(arrIndex[0].EmailID);
                                $("#txtGroupName").val(arrIndex[0].GroupName);
                            }
                            else
                                $(row).find(".hdnIndexId").val(0);
                            for (var i = 0; i < arrIndex.length; i++) {
                                if (i == 0) {
                                    SetIndexValues(arrIndex[i], true);
                                }
                                else
                                    AppendIndexDetails(arrIndex[i], i);
                            }
                        }
                    }
                }
            });
        }
        function AppendIndexDetails(indexPair, index) {
            CreateDynamicIndexTextBox(indexPair.EmailID, true);
            SetIndexValues(indexPair, false);
        }

        function CreateDynamicIndexTextBox(index, isRender) {
            var tr = document.createElement('tr');
            tr.innerHTML = GetDynamicTextBox(index);

            if (!isRender)
                tr.setAttribute("id", "indexRow" + (index = index + 1));
            else
                tr.setAttribute("id", "indexRow" + index);
            // var body = $(parentElement).parent();
            $('table#tblIndex tr:last').after(tr);
            SetIndexButtonVisibility();

            //Allow maximum of 5 key,value pairs
            if ($('table#tblIndex tr').length == 50) {
                $(tr).find('#btnAddDetails').parent().hide();
            }
        }

        function SetIndexValues(indexPair, isRender) {
            var row = "";
            if (!isRender)
                row = document.getElementById("indexRow" + indexPair.EmailID);
            else
                row = $(".indexRow");
            $(row).find("#txtToName").val(indexPair.ToName);
            $(row).find("#txtEmail").val(indexPair.Email);
            $(row).find(".hdnIndexId").val(indexPair.EmailID);
        }

            function GetDynamicTextBox(value) {
                var test = '<td><input type="hidden" value ="0" class="hdnIndexId"></td>' +
                            '<td><label>ToName:</label> <input type="text" ID = "txtToName" Width="90%" MaxLength="50" placeholder="ToName" /></td>' +
                            '<td><label>Email:</label><input type="text" ID= "txtEmail" Width="90%" MaxLength="255" placeholder="Email" /></td>' +
                            '<td><button id="btnAddDetails" type="button" Width="90%"  OnClick="return AddIndexTextBoxes(this)"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/add-icon.png" title="Add"/> </button> </td>' +
                            '<td><button id="btnRemoveDetails"  type="button" Width="90%"  OnClick="return DisplayConfirmBox(this)"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" /> </button>'

                return test;
            }

        function SetIndexButtonVisibility() {
                var rowCount = $('table#tblIndex tr').length;
                for (i = 0; i < rowCount; i++) {
                    var $item = $($('table#tblIndex tr')[i]);

                    if (i == (rowCount - 1)) {
                        $item.find('#btnRemoveDetails').parent().show();
                        $item.find('#btnAddDetails').parent().show();
                        if (i == 0) {
                            $item.find('#btnRemoveDetails').parent().hide();
                        }
                    }
                    else if (i == 0) {
                        $item.find('#btnRemoveDetails').parent().show();
                        $item.find('#btnAddDetails').parent().hide();
                    }
                    else {
                        $item.find('#btnRemoveDetails').parent().show();
                        $item.find('#btnAddDetails').parent().hide();
                    }
                }
            }

       

            var indexRemoveEle = "";
            function DisplayConfirmBox(ele) {
                $('#DivDeleteConfirmIndex,#overlay').show().focus();
                indexRemoveEle = ele;
                //  return false;
            }

            var deleteIndexData = "";
            function RemoveIndexTextBoxes() {
                $('#DivDeleteConfirmIndex').hide();
                $('#overlay').show().focus();

                if ($(indexRemoveEle).closest('tr').find('input.hdnIndexId').val() != 0) {
                    var hdnId = $(indexRemoveEle).closest('tr').find('input.hdnIndexId').val();
                    var test = deleteIndexData.indexOf(hdnId);
                    if (test <= 0)
                        deleteIndexData = deleteIndexData + hdnId + ",";
                }

                $(indexRemoveEle).closest('tr').remove();
                SetIndexButtonVisibility();
                $('#overlay').hide();
                indexRemoveEle = "";
                return false;
            }

            function AddIndexTextBoxes(ele) {
                var index = parseInt($($(ele).parent().parent().find('[class*=hdnIndexId]')).val());
                if ($(ele).closest('tr').find('#txtEmail').val() == "") {
                    alert("Please enter Email.");
                    return false;
                }
                else if ($(ele).closest('tr').find('#txtToName').val() == "") {
                    alert("Please enter ToName.");
                    return false;
                }
                else if (!CheckEmailValidation($(ele).closest('tr').find('#txtEmail').val()))
                {
                    alert("Please enter valid Email.");
                    return false;
                }
                else if (!/^[a-zA-Z\s]+$/.test($(ele).closest('tr').find('#txtToName').val())) {
                    alert("Special characters are not allowed in ToName!")
                    return false;
                }
                CreateDynamicIndexTextBox(index, false);
                return false;
            }
    </script>

    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Email Group Details</h1>
            <div class="clearfix"></div>
        </div>
    </div>

        <div class="popup-mainbox" ID="DivDeleteConfirmIndex" Style="display: none;position: fixed;z-index: 999999;margin-left: 30%;margin-top: 5%;" >
        <div>
            <div class="popup-head" id="Div5">
                Delete Email
            </div>
            <div class="popup-scroller">
                <p>
                    Caution! Are you sure you want to delete this Email?
                </p>
            </div>
            <div class="popup-btn">
                <input id="btnDeleteIndexOkay" OnClick="return RemoveIndexTextBoxes()" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />
                <input id="btnDeleteIndexCancel" type="button" value="No" class="btn green"/>
            </div>
        </div>
    </div>

    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant ">
                <div class="left-content">
                    <asp:Panel ID="panelMain" runat="server" Visible="true">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="content-title">Group Details</div>
                                <div class="content-box listing-view">
                                    <fieldset>
                                        <label style="font-size: 16px;display:inline;margin-right:10px;">Group Name:</label>
                                        <input type="text" id="txtGroupName" placeholder="Group Name" />
                                    </fieldset>
                                    <fieldset>
                                        <div class="popup-scroller-index">
                                            <div class="popup-row">
                                                <input type="hidden" id="hndDividerIdIndex" />
                                                <div class="popup-left">
                                                    <table width="490" id="tblIndex">
                                                        <tbody>
                                                            <tr id="indexRow0" class="indexRow">
                                                                <td>
                                                                    <input type="hidden" value="0" class="hdnIndexId"></td>
                                                                <td>
                                                                    <label>ToName:</label>
                                                                    <input type="text" id="txtToName" width="90%" maxlength="50" placeholder="ToName" /></td>
                                                                <td>
                                                                    <label>Email:</label>
                                                                    <input type="text" id="txtEmail" width="90%" maxlength="255" placeholder="Email" /></td>
                                                                <td>
                                                                    <button id="btnAddDetails" type="button" width="90%" onclick="return AddIndexTextBoxes(this)">
                                                                        <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/add-icon.png" title="Add" /></button></td>
                                                                <td>
                                                                    <button id="btnRemoveDetails" type="button" width="90%" onclick="return DisplayConfirmBox(this)">
                                                                        <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" />
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <div class="popup-btn" style="margin-top:20px;width:75%;">
                                                        <input id="btnIndexSave" type="button" value="Save" class="btn green" style="margin-right: 5px;" />
                                                        <input id="btnIndexCancel" type="button" value="Cancel" class="btn green" />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>

                </div>
            </div>
        </div>
    </div>
    </asp:Content>