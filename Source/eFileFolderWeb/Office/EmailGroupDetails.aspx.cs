﻿using Newtonsoft.Json;
using Shinetech.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Office_EmailGroupDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    [WebMethod]
    public static string GetEmails(string groupId)
    {
        try
        {
            return JsonConvert.SerializeObject(General_Class.GetEmailsFromGroup(int.Parse(groupId.Trim())));
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            return string.Empty;
        }
    }

    [WebMethod(EnableSession = true)]
    public static bool SaveEmails(string groupId, string indexData, string deleteIndexData, string groupName)
    {
        try
        {
            Emails[] indexKeyValue = JsonConvert.DeserializeObject<List<Emails>>(indexData).ToArray();
            DataTable dtIndex = new DataTable();
            dtIndex.Columns.Add("EmailID", typeof(int));
            dtIndex.Columns.Add("GroupID", typeof(int));
            dtIndex.Columns.Add("CreatedBy", typeof(string));
            dtIndex.Columns.Add("Email", typeof(string));
            dtIndex.Columns.Add("ToName", typeof(string));
            foreach (Emails item in indexKeyValue)
            {
                dtIndex.Rows.Add(new object[] { item.EmailID, groupId, Sessions.SwitchedRackspaceId, item.Email, item.ToName });
                //dtIndex.Rows.Add(new object[] { item.EmailID, groupId, Sessions.RackSpaceUserID, item.Email, item.ToName });
            }
            if (int.Parse(groupId) > 0)
            {

                General_Class.InsertEmails(dtIndex, deleteIndexData, groupName, groupId);
            }
            else
            {
                //Here we need to pass loggedin user id
                General_Class.InsertEmailGroupAndEmails(dtIndex, groupName, Sessions.UserId);
            }
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }
}

public class Emails
{
    public string Email { get; set; }
    public string ToName { get; set; }
    public int EmailID { get; set; }
}