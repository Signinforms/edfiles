﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmailGroupGrid.aspx.cs" Inherits="Office_EmailGroupGrid" MasterPageFile="~/MasterPage.master"%>

<%--<%@ Register Src="../Controls/FolderReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>--%>
<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%--<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
    <style>
         #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }
         .left-content {
            float:none;
            margin:0 auto;
        }
    </style>

    <script type="text/javascript">

        $(document).ready(function () {
            $("#ButtonDeleleOkay").live({
                click: function () {
                    showLoader();
                }
            });
        });

        function RedirectToDetailsPage(ele)
        {
            var groupID = $(ele).closest('tr').find("input[type='hidden'][id$=hdnGroupId]").val();
            if (groupID == undefined || groupID == null || groupID == "")
                groupID = 0;
            window.open(window.location.origin  + "/Office/EmailGroupDetails.aspx?GroupID=" + groupID, "toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no");
            
        }

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
        }

    </script>

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Email Groups</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>

    <div class="inner-wrapper">
        <div class="page-container">
            <div class="dashboard-container">
                <div class="left-content">
                    <div>
                        <asp:Panel ID="panelMain" runat="server" Visible="true">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>

                                    <%--GridView--%>
                                    <%--<div class="form_title_2">
                                        <asp:Image runat="server"
                                            ImageUrl="../Images/loader_bg.gif" ID="lbsStatus" ImageAlign="Middle" />
                                    </div>--%>
                                   <asp:LinkButton ID="lnkAddEmailGrp" runat="server" CausesValidation="False" OnClientClick="RedirectToDetailsPage(this)"
                                        ToolTip="Add Email Group" CssClass="ic-icon ic-add" style="float:right;"></asp:LinkButton>
                                    <div class='clearfix'></div>
                                    <asp:GridView ID="grdEmailGroups" runat="server" AllowSorting="True" DataKeyNames="GroupId"
                                        AutoGenerateColumns="false"  OnRowDataBound="grdEmailGroups_RowDataBound"
                                        CssClass="datatable listing-datatable boxWidth" OnSorting="grdEmailGroups_Sorting">
                                        <PagerSettings Visible="False" />
                                        <AlternatingRowStyle CssClass="odd" />
                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                        <Columns>
                                          

                                            <%--convert label into textbox--%>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" ItemStyle-Wrap="false" SortExpression="GroupName">
                                                <HeaderTemplate>
                                                    <asp:LinkButton ID="lblDocumentName1" runat="server" Text="Form Name" CommandName="Sort" CommandArgument="GroupName" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("GroupName")%>' name="GroupName" CssClass="lblDocumentName" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--end--%>

                                            

                                            <%--rename start--%>
                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="20%"
                                                ItemStyle-CssClass="table_tekst_edit actionMinWidth">
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblAction" runat="server" Text="Action" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkBtnEdit" runat="server" Text=""   OnClientClick="RedirectToDetailsPage(this)"
                                                        CssClass="ic-icon ic-edit editDocument" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                    <asp:HiddenField value='<%# Eval("GroupId")%>' runat="server"  ID="hdnGroupId"/>

                                                    <asp:LinkButton ID="lnkBtnDelete" runat="server" CssClass="ic-icon ic-delete delete" OnCommand="lnkBtnDelete_Command" ToolTip="Delete Group"
                                                        CommandArgument='<%# Eval("GroupId")%>' ></asp:LinkButton>
                                                    <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="lnkBtnDelete"
                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                    </ajaxToolkit:ModalPopupExtender>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                        ConfirmText="" TargetControlID="lnkBtnDelete">
                                                    </ajaxToolkit:ConfirmButtonExtender>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--rename end--%>
                                        </Columns>
                                         <EmptyDataTemplate>
                                            <table border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;display:inline-block;">
                                                <tr>
                                                    <td  class="podnaslov">
                                                        <asp:Label ID="Label120" runat="server" />There are no groups right now.
                                                    </td>
                                                </tr>
                                            </table>
                                        </EmptyDataTemplate>
                                    </asp:GridView>

                                     <div class="prevnext" bgcolor="#f8f8f5" style="float: right;padding: 5px;">
                                        <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                            PageSize="5" CssClass="paging_position " PagerStyle="NumericPages" ControlToPaginate="grdEmailGroups"></cc1:WebPager>
                                    </div>
                                    <br />
                                    <br />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>

                        <%--Delete File--%>
                        <asp:Panel class="popup-mainbox" ID="DivDeleteConfirmation" Style="display: none"
                            runat="server">
                            <div>
                                <div class="popup-head" id="PopupHeader">
                                    Delete Email Group
                                </div>
                                <div class="popup-scroller">
                                    <p>
                                        Caution! Are you sure you want to delete this group?
                                    </p>
                                    <p>
                                        Confirm Password:
                                       <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                                    </p>
                                </div>
                                <div class="popup-btn">
                                    <input id="ButtonDeleleOkay" type="button" value="Yes" class="btn green" style="margin-right:5px;"/>
                                    <input id="ButtonDeleteCancel" type="button" value="No" class="btn green" />
                                </div>
                            </div>
                        </asp:Panel>
                        <%--end Delete--%>

                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>