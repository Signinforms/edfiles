﻿using Shinetech.DAL;
using Shinetech.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Office_EmailGroupGrid : System.Web.UI.Page
{
    public string SortType = "DESC";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["EmailGroupList"] as DataTable;
        }
        set
        {
            this.Session["EmailGroupList"] = value;
        }
    }

    private void GetData()
    {
        try
        {
            this.DataSource = General_Class.GetEmailGroups(Sessions.SwitchedRackspaceId);
            
            DataView dvSource = this.DataSource.AsDataView();
            dvSource.Sort = "CreatedDate" + " " + SortType;
            this.DataSource = dvSource.ToTable();
            
            ViewState["SortEmailGroupDirection"] = SortType;
            ViewState["SortEmailGroupExpression"] = "CreatedDate";
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
        }
    }

    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
        UpdatePanel1.Update();
    }

    protected void grdEmailGroups_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void grdEmailGroups_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (this.DataSource != null)
        {
            //Sort the data.
            this.DataSource.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            BindGrid();
        }
    }

    private string GetSortDirection(string column)
    {

        // Retrieve the last column that was sorted.
        string sortExpression = ViewState["SortEmailGroupExpression"] as string;

        if (sortExpression != null)
        {
            // Check if the same column is being sorted.
            // Otherwise, the default value can be returned.
            if (sortExpression == column)
            {
                string lastDirection = ViewState["SortEmailGroupDirection"] as string;
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    SortType = "DESC";
                }
                else if ((lastDirection != null) && (lastDirection == "DESC"))
                {
                    SortType = "ASC";
                }
            }
        }
        // Save new values in ViewState.
        ViewState["SortEmailGroupDirection"] = SortType;
        ViewState["SortEmailGroupExpression"] = column;

        return SortType;
    }
    protected void WebPager1_PageIndexChanged(object sender, Shinetech.Framework.Controls.PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }
    protected void lnkBtnDelete_Command(object sender, CommandEventArgs e)
    {
        try
        {
            string password = textPassword.Text.Trim();
            string strPassword = PasswordGenerator.GetMD5(password);
            string errorMsg = string.Empty;

            if (UserManagement.GetPassword(User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
            {
                //show the password is wrong        
                ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssYes", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);
            }
            else
            {
                try
                {
                    LinkButton lnkbtnDelete = sender as LinkButton;
                    GridViewRow gvrow = lnkbtnDelete.NamingContainer as GridViewRow;
                    int emailGroupId = Convert.ToInt32(grdEmailGroups.DataKeys[gvrow.RowIndex].Value.ToString());
                    if (General_Class.DeleteEmailGroup(Sessions.SwitchedRackspaceId, emailGroupId))
                    {
                        ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
                        GetData();
                        BindGrid();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssYes", "alert('Failed to delete Email Group. Please try agian later!');", true);
                    }
                }
                catch (Exception ex)
                {
                    Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
                    ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssYes", "alert('Failed to delete Email Group. Please try agian later!');", true);
                }
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssYes", "alert('Failed to delete Email Group. Please try agian later!');", true);
        }
    }
}