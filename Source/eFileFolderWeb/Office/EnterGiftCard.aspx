<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="EnterGiftCard.aspx.cs" Inherits="EnterGiftCard" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div class="content-box">
                    <fieldset>
                        <label>Please Enter Gift Card No.</label>
                    </fieldset>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
                            <fieldset>
                                <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                            </fieldset>
                             <fieldset>
                            <asp:TextBox ID="txtBoxCarNo" Width="190px" runat="server" MaxLength="10" />
                            <%--<asp:ImageButton ID="LoginButton" runat="server" OnClick="OnClick_ImageButton" ImageUrl="~/images/continue_btn.gif" />--%>
                            <asp:Button runat="server" ID="LoginButton" OnClick="OnClick_ImageButton"
                                Text="Continue" CssClass="create-btn btn-small green" />
                             </fieldset>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>  
</asp:Content>
