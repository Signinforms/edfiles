using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.DAL;
using Shinetech.Utility;
using Account = Shinetech.DAL.Account;

public partial class EnterGiftCard : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ValidGiftNumber = "";
        }
    }

    protected void OnClick_ImageButton(object sender, EventArgs e)
    {
        if (this.txtBoxCarNo.Text.Trim() == "")
        {
            this.lblMessage.Text = "The Gif Card No. is required.";
            return;
        }

        Gift gift = new Gift(this.txtBoxCarNo.Text.Trim());
        if(gift.IsExist)
        {
            if(gift.UseFlag.Value)
            {
                this.lblMessage.Text = "This Card No. you entered has already been used.  To purchase additional eFileFolders visit\r\n"+
                                        "http://www.edfiles.com/Office/PurchasePage.aspx or Call 800-579-9190 or email info@edfiles.com";
                return;
            }
            ValidGiftNumber = gift.GiftNumber.Value;
            Response.Redirect("~/Office/PaymentPage.aspx", true);
        }
        else
        {
            this.lblMessage.Text = "Invalid Card No., please try again.  If the message presists, call 800-579-9190.";
        }
    }



    public string ValidGiftNumber
    {
        get
        {
            if (this.Session["_ValidGiftNumber"] == null)
            {
                return "";
            }
            return this.Session["_ValidGiftNumber"] as string;
        }
        set
        {
            this.Session["_ValidGiftNumber"] = value;
        }
    }
}
