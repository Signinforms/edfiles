﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;
using Shinetech.DAL;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Serialization;
//using ExportToExcel;
using Shinetech.Framework;
using Shinetech.Utility;
using System.Xml.Xsl;
using System.Xml.XPath;

public partial class Office_ExportDocumentsAndFolderLogs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            int type = int.Parse(Request.QueryString["id"]);
            StreamReader xml;
            XmlDocument objXmlDocument = new XmlDocument();
            XPathDocument objXPathDocument;
            XslCompiledTransform objXslTransform = new XslCompiledTransform();
            FileStream fs;
            string strXMLFilePath = string.Empty;
            string strXSLTPath = string.Empty;

            DataSet docFolderLogs = new DataSet();

            if (type == 1)
            {
                DataTable dtCopy = new DataTable();
                dtCopy = this.FolderDataSource.Copy();
                docFolderLogs.Tables.Add(dtCopy);
                if (this.DividerDataSource != null)
                {
                    dtCopy = this.DividerDataSource.Copy();
                    docFolderLogs.Tables.Add(dtCopy);
                }
                if (this.DocumentDataSource != null)
                {
                    dtCopy = this.DocumentDataSource.Copy();
                    docFolderLogs.Tables.Add(dtCopy);
                }
                if (this.FolderLogDataSource != null)
                {
                    dtCopy = this.FolderLogDataSource.Copy();
                    docFolderLogs.Tables.Add(dtCopy);
                }
                if (this.LogFormDataSource != null)
                {
                    dtCopy = this.LogFormDataSource.Copy();
                    docFolderLogs.Tables.Add(dtCopy);
                }
                if (this.WorkAreaDataSource != null)
                {
                    dtCopy = this.WorkAreaDataSource.Copy();
                    docFolderLogs.Tables.Add(dtCopy);
                }
                if (this.EFileFlowDataSource != null)
                {
                    dtCopy = this.EFileFlowDataSource.Copy();
                    docFolderLogs.Tables.Add(dtCopy);
                }
                if (this.EFileFlowShareDataSource != null)
                {
                    dtCopy = this.EFileFlowShareDataSource.Copy();
                    docFolderLogs.Tables.Add(dtCopy);
                }
            }
            else
            {
                docFolderLogs = General_Class.GetAllDocAndFolderLogsForExcel(Sessions.SwitchedSessionId);
                if (docFolderLogs != null)
                {
                    foreach (DataRow dr in docFolderLogs.Tables[0].Rows)
                    {
                        dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                    }
                    foreach (DataRow dr in docFolderLogs.Tables[1].Rows)
                    {
                        dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                    }
                    foreach (DataRow dr in docFolderLogs.Tables[2].Rows)
                    {
                        dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                    }
                    foreach (DataRow dr in docFolderLogs.Tables[3].Rows)
                    {
                        dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                    }
                    foreach (DataRow dr in docFolderLogs.Tables[4].Rows)
                    {
                        dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                    }
                    foreach (DataRow dr in docFolderLogs.Tables[5].Rows)
                    {
                        dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                    }
                    foreach (DataRow dr in docFolderLogs.Tables[6].Rows)
                    {
                        dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                    }
                    foreach (DataRow dr in docFolderLogs.Tables[7].Rows)
                    {
                        dr["ActionName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.DocumentsAndFoldersLogs)Convert.ToInt32(dr["Action"].ToString()));
                    }
                }
            }


            string filePath = Server.MapPath("~\\Excel\\Temp");
            FileInfo objFileInfo;

            //Get Xml from DataSet
            objXmlDocument.LoadXml(docFolderLogs.GetXml());
            strXMLFilePath = filePath + @"\" + "ExcelFileOutstdBill.xml";

            //Delete old Xml file
            if (!strXMLFilePath.Equals(string.Empty))
            {
                objFileInfo = new FileInfo(strXMLFilePath);
                if (objFileInfo.Exists)
                {
                    objFileInfo.Delete();
                }
            }
            objXmlDocument.Save(strXMLFilePath);

            //Load XML document
            xml = new StreamReader(strXMLFilePath);
            objXPathDocument = new XPathDocument(xml);

            //Load Xslt
            string tempPath = Server.MapPath("~\\Excel\\Temp");
            string excelPath = Server.MapPath("~\\Excel\\XSLStyleSheet");

            //Load Xslt
            strXSLTPath = excelPath + @"\" + "LogsExcel.xslt";
            objXmlDocument.Load(strXSLTPath);



            string strExcelFileOut = tempPath + @"\" + "FolderLogDetails" + DateTime.UtcNow.ToString("MMddyyyyhhmmss") + ".xls";

            //Create output stream
            fs = new FileStream(strExcelFileOut, FileMode.Create);

            //Do actual transform of Xml
            objXslTransform.Load(objXmlDocument);
            objXslTransform.Transform(objXPathDocument, null, fs);

            xml.Close();
            xml.Dispose();
            fs.Close();
            fs.Dispose();

            Response.ClearHeaders();
            Response.Clear();
            Response.Expires = 0;
            Response.Buffer = true;
            Response.AddHeader("Content-disposition", "attachment; filename=\"" + Path.GetFileName(strExcelFileOut) + "\"");
            Response.ContentType = "application/vnd.ms-excel";
            Response.WriteFile(strExcelFileOut);
            Response.Flush();
            Response.Close();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            objFileInfo = new FileInfo(strXMLFilePath);
            if (objFileInfo.Exists)
            {
                objFileInfo.Delete();
            }

            objFileInfo = new FileInfo(strExcelFileOut);
            if (objFileInfo.Exists)
            {
                objFileInfo.Delete();
            }

        }
        catch (Exception ex)
        {
            Console.Write("", "");
        }

    }

    private XmlDataDocument GenerateXmlDocument(DataSet ds)
    {
        //DataSet ds = new DataSet();
        //DataTable dtCopy = new DataTable();
        //dtCopy = table.Copy();
        ////ds.Tables.Remove(table);
        //ds.Tables.Add(dtCopy);
        XmlDataDocument doc = new XmlDataDocument(ds);

        return doc;
    }

    public DataTable FolderDataSource
    {
        get
        {
            return this.Session["DataSource_Folder"] as DataTable;
        }
        set
        {
            this.Session["DataSource_Folder"] = value;
        }
    }

    public DataTable DividerDataSource
    {
        get
        {
            return this.Session["DataSource_Divider"] as DataTable;
        }
        set
        {
            this.Session["DataSource_Divider"] = value;
        }
    }

    public DataTable DocumentDataSource
    {
        get
        {
            return this.Session["DataSource_Document"] as DataTable;
        }
        set
        {
            this.Session["DataSource_Document"] = value;
        }
    }

    public DataTable FolderLogDataSource
    {
        get
        {
            return this.Session["DataSource_FolderLog"] as DataTable;
        }
        set
        {
            this.Session["DataSource_FolderLog"] = value;
        }
    }

    public DataTable LogFormDataSource
    {
        get
        {
            return this.Session["DataSource_LogForm"] as DataTable;
        }
        set
        {
            this.Session["DataSource_LogForm"] = value;
        }
    }

    public DataTable WorkAreaDataSource
    {
        get
        {
            return this.Session["DataSource_WorkArea"] as DataTable;
        }
        set
        {
            this.Session["DataSource_WorkArea"] = value;
        }
    }

    public DataTable EFileFlowDataSource
    {
        get
        {
            return this.Session["DataSource_EFileFlow"] as DataTable;
        }
        set
        {
            this.Session["DataSource_EFileFlow"] = value;
        }
    }

    public DataTable EFileFlowShareDataSource
    {
        get
        {
            return this.Session["DataSource_EFileFlowShare"] as DataTable;
        }
        set
        {
            this.Session["DataSource_EFileFlowShare"] = value;
        }
    }
}