﻿using Newtonsoft.Json;
using Shinetech.DAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class Office_ExportEdFormsUserShareLogs : System.Web.UI.Page
{

    string edFormsUserShareId = string.Empty;
    string datetime = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (!string.IsNullOrEmpty(this.Request.QueryString["id"]) && !string.IsNullOrEmpty(this.Request.QueryString["datetime"]))
            {
                edFormsUserShareId = this.Request.QueryString["id"];
                datetime = this.Request.QueryString["datetime"];
                datetime = datetime == "All Logs" ? null : Convert.ToDateTime(datetime).ToString("yyyy-MM-dd HH:mm:ss");
                MemoryStream stream = GetCSV();

                var filename = "EdFormsUserShareLog" + DateTime.Now.ToString("_MM_dd_yyyy_HH_mm_ss") + ".csv";
                var contenttype = "text/csv";
                Response.Clear();
                Response.ContentType = contenttype;
                Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
        }
    }

    public MemoryStream GetCSV()
    {
        DataTable data = General_Class.GetEdFormsUserShareLogs(edFormsUserShareId, datetime);
        ArrayList fieldsToExpose = new ArrayList();
        DataTable dtnew = new DataTable();
        if (data != null && data.Rows.Count > 0)
        {
            DataTable edFormUser = General_Class.GetEdFormsUserById(data.Rows[0].Field<int>("EdFormsUserId"));
            DataTable dept = edFormUser != null ? General_Class.GetDepartmentbyEdFormUserId(data.Rows[0].Field<int?>("EdFormsUserId"), edFormUser.Rows[0].Field<int?>("EdFormsId")) : null;
            string userName = Membership.GetUser().UserName.ToString();

            dtnew.Columns.AddRange(new DataColumn[5] { new DataColumn("Username", typeof(string)),
                            new DataColumn("Department Name", typeof(string)),
                            new DataColumn("EdForm Name",typeof(string)),
                            new DataColumn("Modified Date",typeof(DateTime)),
                            new DataColumn("To Email", typeof(string))});
            for (int j = 0; j < data.Rows.Count; j++)
            {
                dtnew.Rows.Add(userName, dept != null ? dept.Rows[0].Field<string>("DepartmentName") : "", data.Rows[j].Field<string>("FileName"), data.Rows[j].Field<DateTime>("ModifiedDate"),
                    data.Rows[j].Field<string>("ToEmail"));

                if (!string.IsNullOrEmpty(data.Rows[j].Field<string>("SavedValue")))
                {
                    var savedValue = JsonConvert.DeserializeObject<List<KeyValuePair<string, string>>>(data.Rows[j].Field<string>("SavedValue"));
                    foreach (var item in savedValue)
                    {
                        DataColumnCollection columns = dtnew.Columns;
                        if (!columns.Contains(item.Key))
                        {
                            dtnew.Columns.Add(item.Key);
                        }
                        dtnew.Rows[j][item.Key] = item.Value;
                    }
                }
                for (int i = 0; i < dtnew.Columns.Count; i++)
                {
                    if (!fieldsToExpose.Contains(dtnew.Columns[i].ColumnName))
                        fieldsToExpose.Add(dtnew.Columns[i].ColumnName);
                }
            }
        }
        return GetCSV(fieldsToExpose, dtnew);
    }

    /// <summary>
    /// Gets the CSV.
    /// </summary>
    /// <param name="fieldsToExpose">The fields to expose.</param>
    /// <param name="data">The data.</param>
    /// <returns></returns>
    public static MemoryStream GetCSV(ArrayList fieldsToExpose, DataTable data)
    {
        MemoryStream stream = new MemoryStream();
        using (var writer = new StreamWriter(stream))
        {
            for (int i = 0; i < fieldsToExpose.Count; i++)
            {
                if (i != 0) { writer.Write(","); }
                writer.Write("\"");
                writer.Write(((string)fieldsToExpose[i]).Replace("\"", "\"\""));
                writer.Write("\"");
            }
            writer.Write("\n");
            writer.Write("\n");

            foreach (DataRow row in data.Rows)
            {
                for (int i = 0; i < fieldsToExpose.Count; i++)
                {
                    if (i != 0) { writer.Write(","); }
                    writer.Write("\"");
                    writer.Write(row[(string)fieldsToExpose[i]].ToString()
                        .Replace("\"", "\"\""));
                    writer.Write("\"");
                }
                writer.Write("\n");
            }
        }
        return stream;
    }
}