using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.Xml.Serialization;
//using ExportToExcel;
using Shinetech.Framework;
using Shinetech.Utility;
using Shinetech.DAL;
using DataQuicker2.Framework;

public partial class ExportSignatures : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            if (string.IsNullOrEmpty(this.Request.QueryString["action"]))
            {
                XmlDataDocument xmldoc = GenerateXmlDocument(this.DataSource);
                //ExportToExcel.ExcelExport objExport = new ExcelExport();
                //try
                //{
                //    objExport.TempFolder = @"\Excel\Temp\";
                //    objExport.TemplateFolder = @"\Excel\Template\";
                //    objExport.XSLStyleSheetFolder = @"\Excel\XSLStyleSheet\";

                //    objExport.CleanUpTemporaryFilesWeb();

                //    string strExcelFile = objExport.TransformXMLDocumentToExcel(xmldoc, "SignInExcel.xsl", true);
                //    objExport.SendSinatureToClient(strExcelFile);

                //}
                //catch (Exception Ex)
                //{

                //}
            }
            else
            {
                MemoryStream stream = GetCSV(this.DataSource);

                var filename = "SignInExcel.csv";
                var contenttype = "text/csv";
                Response.Clear();
                Response.ContentType = contenttype;
                Response.AddHeader("content-disposition", "attachment;filename=" + filename);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            
        }
    }

    public static MemoryStream GetCSV(DataTable data)
    {
        ArrayList fieldsToExpose = new ArrayList();
        for (int i = 0; i < data.Columns.Count; i++)
        {
            if (data.Columns[i].ColumnName != "SignatureID"&&
                data.Columns[i].ColumnName != "PatientID" &&
                data.Columns[i].ColumnName != "SignatureData" &&
                data.Columns[i].ColumnName != "DoctorID" &&
                data.Columns[i].ColumnName != "WaitTime"&&
                 data.Columns[i].ColumnName != "FormatDateTime" &&
                 data.Columns[i].ColumnName != "FormatCheckInDate")

            fieldsToExpose.Add(data.Columns[i].ColumnName);
        }

        return GetCSV(fieldsToExpose, data);
    }

    public static MemoryStream GetCSV(ArrayList fieldsToExpose, DataTable data)
    {
        MemoryStream stream = new MemoryStream();
        using (var writer = new StreamWriter(stream))
        {
            for (int i = 0; i < fieldsToExpose.Count; i++)
            {
                if (i != 0) { writer.Write(","); }
                writer.Write("\"");
                writer.Write(((string)fieldsToExpose[i]).Replace("\"", "\"\""));
                writer.Write("\"");
            }
            writer.Write("\n");

            foreach (DataRow row in data.Rows)
            {
                for (int i = 0; i < fieldsToExpose.Count; i++)
                {
                    if (i != 0) { writer.Write(","); }
                    writer.Write("\"");
                    writer.Write(row[(string)fieldsToExpose[i]].ToString()
                        .Replace("\"", "\"\""));
                    writer.Write("\"");
                }

                writer.Write("\n");
            }
        }

        return stream;
    }

    private XmlDataDocument GenerateXmlDocument(DataTable table)
    {
        DataSet ds = new DataSet();
        ds.Tables.Add(table);
        XmlDataDocument doc = new XmlDataDocument(ds);

        return doc;
    }

    public DataTable DataSource
    {
        get
        {
            DataTable table = (this.Session["SignIn_DataSource"] as DataTable).Copy();

            AddDateTimeField(table);

            return table;
        }
        
    }

    //在这里添加根据SignatureId获取所有questions 和answers的字符串，然后连接在一起
    public void AddDateTimeField(DataTable table)
    {
        DataColumn columnSignIn = table.Columns.Add("FormatDateTime", typeof(string));
        DataColumn columnCheckIn = table.Columns.Add("FormatCheckInDate", typeof(string));
        DataColumn columnAnswer = table.Columns.Add("FormatAnswers", typeof(string));

        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();

        foreach (DataRow row in table.Rows)
        {
            DateTime date = row["SignInDate"] == DBNull.Value ? DateTime.Now : (DateTime)row["SignInDate"];
            row["FormatDateTime"] = row["SignInDate"] == DBNull.Value ? "" : date.ToString("MM.dd.yyyy H:m tt");

            date = row["CheckInDate"] == DBNull.Value ? DateTime.Now : (DateTime)row["CheckInDate"];
            row["FormatCheckInDate"] = row["CheckInDate"] == DBNull.Value ? "" : date.ToString("MM.dd.yyyy H:m tt");

            row["FormatAnswers"] = GetQuestionDataSource(Convert.ToInt32(row["signatureID"]), uid);
        }
    }

    public string GetQuestionDataSource(int sid, string did)
    {
        PatientQuestion dv = new PatientQuestion();
        ObjectQuery query = dv.CreateQuery();
        query.SetCriteria(dv.SignatureID, sid);
        query.SetCriteria(dv.DoctorID, did);
        query.SetOrderBy(dv.QuestionID, false);

        DataTable table = new DataTable();
        string strAnswers = string.Empty;
        try
        {
            query.Fill(table);

            foreach (DataRow row in table.Rows)
            {
                strAnswers += string.Format("{0}: {1}", row["QuestionTitle"], row["AnswerContent"]);
            }
        }
        catch
        {
            table = null;
        }


        return strAnswers;
    }


}
