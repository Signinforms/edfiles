<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="FileFolderBox.aspx.cs" Inherits="Office_FileFolderBox" Title="" %>

<%@ Register Src="../Controls/FileRequestControl.ascx" TagName="FileRequestControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/Office/UserControls/ucDocSearch.ascx" TagName="ucDocSearch"
    TagPrefix="us" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register Src="~/Office/UserControls/ucAddToTab.ascx" TagPrefix="uc1" TagName="ucAddToTab" %>

<%--<%@ Register Src="~/Office/UserControls/ucFolderEditablePDF.ascx" TagPrefix="uc3" TagName="ucFolderEditablePDF" %>
<%@ Register Src="~/Office/UserControls/ucTest.ascx" TagPrefix="uc4" TagName="ucTest" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css?v=1" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=3" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/edFiles/EdFilesLogs.js?v=4" type="text/javascript"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>extras/jquery-ui-1.8.22.custom.min.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>extras/jquery-ui-1.8.22.custom.min.js"></script>

    <style type="text/css">
        .ajax__tab {
            height: 45px !important;
        }

        .checkSelect {
            margin-right: 100px !important;
            margin-top: 5px !important;
            float: right;
        }

        .sharefolder {
            background: url(../../images/shareFolder.png) no-repeat center center;
            margin-top: -7px;
            height: 40px;
        }

        .headerstyle {
            font-size: 15px;
            font-weight: bold;
        }

        .dvGrid {
            overflow-x: hidden;
        }

        .tdIndex {
            padding-top: 10px;
        }

        .btnCoverSheet {
            width: auto;
            background-position: 5px !important;
            padding-right: 5px;
            padding-left: 40px;
            float: right;
            background: url(../../images/barcode-small.png) no-repeat center center;
            line-height: 30px;
            height: 30px;
            font-size: 15px;
        }

        .btnIndex {
            height: 35px;
            width: 35px;
            margin-top: 28px;
        }

        .txtIndex {
            width: 85%;
            height: 25px;
            padding-left: 20px;
        }

        #tblIndexPreview {
            margin-top: 10px;
        }

        .indexHeader {
            text-align: center;
            color: #83af33;
            font-size: 25px;
        }

        .docIndex {
            height: 93%;
            border: solid 1px lightgray;
            padding: 25px;
        }

        #btnSpltBtn {
            float: right;
            margin-bottom: 10px;
        }

        .leftPreview {
            height: 100%;
            float: left;
            width: 75%;
        }

        .rightPreview {
            height: 100%;
            float: right;
            width: 25%;
        }

        .uploadRename {
            background-color: darkturquoise;
            float: right;
            margin-left: 10px;
            visibility: visible;
            width: auto !important;
            padding-left: 10px;
            padding-right: 10px;
            color: black;
        }

        .uploadOnly {
            float: right;
        }

        .labelVisible {
            display: none !important;
        }

        .classLabel {
            float: right;
            margin-right: -70px;
            top: -85px;
            position: relative;
        }

        .tabLabel {
            float: right;
            margin-right: 90px;
            top: -85px;
            position: relative;
        }

        .ic-edForms {
            background: url(../../images/ic-edF.png) no-repeat center center;
            margin-top: 6px;
            margin-left: -10px;
        }

        .btnCopyFile {
            padding: 7px 10px 8px 35px;
            padding-right: 10px;
            width: auto;
            height: 30px;
            line-height: 2px;
            margin-right: 5px;
            background-image: url(../images/plus-icon.png);
            background-position: 7px center;
            background-repeat: no-repeat;
        }

        .selecttabcheck {
            /*margin-top: -45px;
    margin-left: 89%;*/
            height: 37px;
            background-color: #83af33;
            float: left;
            display: inline-block;
            margin-right: 10px;
            max-width: 200px;
            width: auto;
        }

        .btnAddtoTab, .btnAddtoWorkArea {
            padding: 8px 10px 8px 35px;
            font-size: 15px;
            padding-right: 10px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 5px;
            background-image: url(../images/plus-icon.png);
            background-position: 7px center;
            background-repeat: no-repeat;
        }

        .btnSplitShare {
            padding: 8px 10px 8px 42px;
            font-size: 15px;
            padding-right: 10px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 5px;
            background-image: url(../images/ic-mail.png);
            background-position: 7px center;
            background-repeat: no-repeat;
        }

        .btnAddtoFolderLog {
            padding: 8px 10px 8px 35px;
            font-size: 15px;
            padding-right: 10px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 5px;
            background-image: url(../images/add-file.png);
            background-position: 7px center;
            background-repeat: no-repeat;
        }

        .btnRedact {
            padding: 8px;
            font-size: 15px;
            padding-right: 10px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 10px;
        }

        #btnCheckedAll {
            float: right;
            margin-right: 3px;
            margin-top: 9px;
            height: 18px;
            width: 18px;
        }

        .selectalltab {
            font-size: 18px;
            float: right;
            padding: 5px;
            font-family: 'Open Sans', sans-serif !important;
            color: #fff;
            font-weight: 700;
            margin-right: 5px;
        }

        .addtotab-filefolderbox {
            cursor: pointer;
            background-image: url(../images/AddToTabWorkArea.png);
            margin-left: 0px;
            float: none;
            background-color: white;
            border: 0px;
            margin-top: -4px;
        }

        body {
            font-family: "Open Sans", sans-serif;
        }

        .title-container h1 {
            font-family: "Open Sans", sans-serif;
        }

        .datatable td {
            font-family: "Open Sans", sans-serif;
        }

        .ajax__tab_xp .ajax__tab_body {
            font-family: "Open Sans", sans-serif;
        }

        .ajax__tab_header > span {
            border-top-left-radius: 15px;
            border-top-right-radius: 15px;
        }

            .ajax__tab_header > span > span > span > a > span {
                background: none !important;
            }

        /*.ajax__tab_hover
        {
  -webkit-transform: scale(1.1);
          transform: scale(1.1);
          z-index:1000;
        }*/

        .ic-AddTab {
            display: inline-block;
            vertical-align: top;
            font-size: 0;
            width: 45px;
            height: 28px;
            margin-top: 6px;
        }

        /*.ajax__tab_header span.ajax__tab_active
        {
            border:solid;
        }*/

        #file-request-inner label {
            font-weight: normal !important;
        }

        .ajax__tab_body {
            height: auto !important;
        }

        .popupclose1 {
            float: right;
            padding: 10px;
            cursor: pointer;
        }

        #overlay {
            display: none;
            position: fixed;
            top: 0;
            bottom: 0;
            background: rgba(0,0,0,.8);
            width: 100%;
            height: 100%;
            z-index: 99999;
        }

        .white_content {
            display: none;
            position: fixed;
            top: 20%;
            left: 20%;
            width: 60%;
            height: 50%;
            padding: 16px;
            background-color: white;
            z-index: 999999999;
            overflow: auto;
            font-size: large;
        }

        .hideControl {
            display: none;
        }

        #popupclose {
            float: right;
            padding: 10px;
            cursor: pointer;
        }

        .popupcontent {
            padding: 10px;
        }

        td span {
            -ms-word-break: break-all;
            word-break: break-all;
            word-break: break-word;
            -webkit-hyphens: auto;
            -moz-hyphens: auto;
            hyphens: auto;
            max-width: 300px;
            white-space: normal !important;
        }

        td {
            max-width: 300px;
        }

        .preview-popup {
            width: 100%;
            max-width: 1400px;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 10%;
            right: 0;
            margin: auto;
            background: #fff;
            height: 85%;
        }

        .ajax__fileupload_uploadbutton,
        .removeButton {
            font-size: 10px;
        }

        .ajax__fileupload_dropzone {
            font-size: 15px;
            line-height: 120px !important;
            height: 120px !important;
        }

        .ajax__fileupload_fileItemInfo {
            font-size: 14px;
        }

        .firstLog {
            display: none;
        }

        .aFolderLog {
            cursor: pointer;
        }

        .logFormPreveiw {
            background: url(../images/file-view.png) no-repeat center center;
            cursor: pointer;
            margin-top: 6px;
            width: 28px !important;
        }

        .logFormAddFile {
            background: url(../images/file-add.png) no-repeat center center;
            margin-top: 6px;
            margin-left: -10px;
        }

        .logFormUploadFile {
            background: url(../images/file-upload.png) no-repeat center center;
            cursor: pointer;
            margin-top: 6px;
            border: none;
        }

        .popup-row {
            padding: 20px;
            overflow: hidden;
            position: relative;
        }

            .popup-row:after {
                content: "";
                height: 1px;
                width: 100%;
                clear: both;
            }

        .popup-left {
            text-align: center;
            width: 100%;
            float: none;
        }

        .popup-right {
            width: 100%;
            float: none;
        }

        .popup-scroller-index {
            /*max-height: 675px;*/
            overflow: auto;
            background-color: #fff;
        }

        #thumnailImages .thumnail-outer {
            margin-bottom: 20px;
        }

            #thumnailImages .thumnail-outer span {
                float: left;
            }

            #thumnailImages .thumnail-outer .check {
                float: right;
            }

            #thumnailImages .thumnail-outer > img {
                display: block;
            }

        #comboboxDividerSplit_chosen {
            margin-top: 5px;
            display: block;
        }

        #comboboxSplit_chosen {
            display: block;
            margin-top: 5px;
        }

        /*#text-layer {
            position: absolute;
            left: 0;
            top: 0;
            right: 0;
            bottom: 0;
            overflow: hidden;
            opacity: 0.2;
            line-height: 1.0;
        }

            #text-layer > div {
                color: transparent;
                position: absolute;
                white-space: pre;
                cursor: text;
                transform-origin: 0% 0%;
            }*/
        .lblRedact {
            /* float: right; */
            font-size: 20px;
            /* width: auto; */
            right: 0px;
            margin-left: 15%;
            line-height: 35px;
            color: black;
            /* text-align: center; */
        }

        .redact-close {
            background: url(../../images/icon-close.png) no-repeat right bottom;
            float: right;
            padding: 10px;
            cursor: pointer;
        }

        .pdfViewer .mydiv {
            position: absolute;
            z-index: 9999999;
            background-color: black;
            border: 1px solid black;
            text-align: center;
            height: 20px;
            width: 150px;
        }

        .txtKey {
            margin-bottom: 5px;
        }

        .tdIndex {
            padding-top: 10px;
        }

        .btnCoverSheet {
            width: auto;
            background-position: 5px !important;
            padding-right: 5px;
            padding-left: 40px;
            float: right;
            background: url(../../images/barcode-small.png) no-repeat center center;
        }

        .btnIndex {
            height: 35px;
            width: 35px;
            margin-top: 28px;
        }

        .txtIndex {
            width: 85%;
            height: 25px;
            padding-left: 20px;
        }

        #tblIndexPreview {
            margin-top: 10px;
        }

        .indexHeader {
            text-align: center;
            color: #83af33;
            font-size: 25px;
        }

        .docIndex {
            height: 93%;
            border: solid 1px lightgray;
            padding: 25px;
        }

        #btnSpltBtn {
            float: right;
            margin-bottom: 10px;
        }

        .leftPreview {
            height: 100%;
            float: left;
            width: 75%;
        }

        .rightPreview {
            height: 100%;
            float: right;
            width: 25%;
        }

        .uploadRename {
            background-color: darkturquoise;
            float: right;
            margin-left: 10px;
            visibility: visible;
            width: auto !important;
            padding-left: 10px;
            padding-right: 10px;
        }

        .uploadOnly {
            float: right;
        }

        .labelVisible {
            display: none !important;
        }

        .classLabel {
            float: right;
            margin-right: -70px;
            top: -85px;
            position: relative;
        }

        .tabLabel {
            float: right;
            margin-right: 90px;
            top: -85px;
            position: relative;
        }

        .TabDropDown {
            float: right;
            margin-right: 75px !important;
            padding: 0px !important;
            height: 20px !important;
            border: solid 1px #c4c4c4;
            line-height: 30px;
            max-width: 200px !important;
            width: 100%;
        }

        .ClassDropDown {
            float: right;
            margin-right: 10px !important;
            padding: 0px !important;
            height: 20px !important;
            max-width: 110px !important;
        }

        .btnScandoc {
            margin-left: 10px;
            float: right;
            font-weight: bold;
        }

        .btnScandocBottom {
            float: left;
            font-weight: bold;
            margin-top: 10px;
        }

        .btnNavigate {
            font-size: 15px;
            margin-bottom: 10px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 5px;
            background-repeat: no-repeat;
            width: auto;
        }

        .btnPrevious {
            padding: 8px 10px 8px 35px;
            background-image: url(../images/left-arrow.png);
            background-position: 7px center;
        }

        .btnNext {
            padding: 8px 30px 8px 10px;
            background-image: url(../images/right-arrow.png);
            background-position: 50px center;
            float: right;
        }

        .btnConvertOcr {
            float: right;
            margin-right: 5px;
            width: 50px;
            height: 30px;
            line-height: 1px;
        }

        .ajax__tab_xp .ajax__tab_header {
            font-family: verdana,tahoma,helvetica;
            font-size: 11px;
            background: none;
        }

        .ajax__tab_xp .ajax__tab_body {
            font-family: verdana,tahoma,helvetica;
            font-size: 10pt;
            border: none !important;
            padding: 0px !important;
            background-color: #ffffff;
        }

        .yellow {
            background-color: yellow;
        }

        label {
            font-size: 14px;
            display: inline-block;
        }

        .scrollable {
            max-height: 200px !important;
            overflow: hidden;
            display: inline-block;
            max-width: 100%;
            overflow: auto;
        }

        .btnNav {
            border-radius: 4px;
            height: 25px;
            width: 25px;
            cursor: pointer;
            float: right;
            margin-top: 10px;
        }

        .lnkExpand {
            text-decoration: underline;
            font-size: 17px;
            color: #83af33;
            margin-left: 10px;
            margin-top: 10px;
            float: right;
            cursor: pointer;
        }

        .lnkCollapse {
            text-decoration: underline;
            font-size: 17px;
            color: #01487f;
            margin-left: 10px;
            margin-top: 10px;
            float: right;
            cursor: pointer;
            display: none;
        }

        .btnPrevEnabled {
            background: url(../images/prev_green.png) 2px 3px no-repeat white;
            border: solid 1px #83af33;
        }

        .btnNextEnabled {
            margin-left: 10px;
            background: url(../images/next_green.png) 4px 3px no-repeat white;
            border: solid 1px #83af33;
        }

        .btnPrevDisabled {
            background: url(../images/prev_blue.png) 2px 3px no-repeat white;
            border: solid 1px #01487f;
            cursor: not-allowed;
            opacity: 0.2;
        }

        .btnNextDisabled {
            margin-left: 10px;
            background: url(../images/next_blue.png) 4px 3px no-repeat white;
            border: solid 1px #01487f;
            cursor: not-allowed;
            opacity: 0.2;
        }

        .pager_drop {
            float: right;
            margin-top: 10px;
            height: 30px;
            width: 50px;
            font-size: 20px;
        }

        .btnEdit {
            text-align: center !important;
            height: 30px;
            width: 94px;
            background-color: #94ba33;
            color: white;
            font-weight: bold;
            font-size: 21px;
            margin-top: 5px;
            border: none;
            cursor: pointer;
        }

            .btnEdit:hover {
                background-color: #01487f;
            }

        .btnNextPreview {
            padding: 8px 10px 8px 35px;
            font-size: 15px;
            padding-right: 10px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 5px;
            background-image: url(../images/left-arrow.png);
            background-position: 7px center;
            background-repeat: no-repeat;
            float: left;
        }

        .btnPrevPreview {
            padding: 8px 30px 8px 10px;
            font-size: 15px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 5px;
            background-image: url(../images/right-arrow.png);
            background-position: 50px center;
            background-repeat: no-repeat;
            float: right;
        }

        #popupdocclose {
            float: right;
            padding: 10px;
            cursor: pointer;
        }

        .filename {
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            display: inline-block;
            width: 63%;
        }

        .ajax__fileupload_fileItemInfo .filetype, .ajax__fileupload_fileItemInfo .filesize, .ajax__fileupload_fileItemInfo .uploadstatus {
            vertical-align: top;
        }

        .ajax__fileupload_fileItemInfo .pendingState {
            width: 60%;
        }
    </style>
    <asp:HiddenField ID="hdnActiveTabIndex" runat="server" />

    <div class="title-container">
        <div class="inner-wrapper">
            <h1 style="font-size: 30px;">
                <asp:Label runat="server" ID="lblFolderDetails" Text=""></asp:Label>
            </h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <asp:UpdatePanel ID="upanel1" runat="server">
        <ContentTemplate>
            <asp:Button ID="hdnSplitBtn" Style="display: none" runat="server" OnClick="hdnSplitBtn_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant modify-contant">
                <asp:HiddenField ID="hdnIsRedaction" runat="server" />
                <asp:HiddenField ID="hdnIsRenamed" runat="server" />
                <asp:HiddenField ID="hdnUserIdentity" runat="server" />
                <asp:HiddenField ID="hdnUploadValue" runat="server" />
                <asp:HiddenField ID="hdnFolderId" runat="server" />
                <asp:HiddenField ID="hdnUploadFiles" runat="server" />
                <asp:HiddenField ID="hdnOfficeId" runat="server" />
                <asp:HiddenField ID="hdnToEmail" runat="server" />
                <asp:HiddenField ID="hdnMailTitle" runat="server" />
                <asp:HiddenField ID="hdnMailContent" runat="server" />
                <asp:HiddenField ID="hdnNewPdfName" runat="server" />
                <asp:HiddenField ID="hdnToName" runat="server" />
                <asp:HiddenField ID="hdnFromEmail" runat="server" />
                <asp:HiddenField ID="hdnExtension" runat="server" />
                <asp:HiddenField ID="hdnIsScrollable" runat="server" />
                <asp:HiddenField ID="hdnPageIndex" runat="server" />

                <asp:HiddenField ID="hdnFolderName" runat="server" />
                <asp:HiddenField ID="hdnCords" runat="server" />

                <asp:HiddenField ID="hdnNextId" runat="server" />
                <asp:HiddenField ID="hdnPreviousId" runat="server" />

                <asp:HiddenField ID="hdnFileNameOnly" runat="server" />
                <asp:HiddenField ID="hdnSelectedPath" runat="server" />
                <asp:HiddenField ID="hdnObjId" runat="server" />

                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" OnPreRender="UpdatePanel1_PreRender">
                    <ContentTemplate>
                        <div>
                            <div>
                                <input type="button" id="btnPrevious" runat="server" value="PREVIOUS" onclick="ShowPrevious();" class="btn green btnPrevious btnNavigate" title="PREVIOUS" />
                                <input type="button" id="btnNext" runat="server" value="NEXT" onclick="ShowNext();" class="btn green btnNext btnNavigate" title="NEXT" />
                            </div>
                            <div class="clearfix"></div>
                            <%--class="left-content"--%>
                            <div class="content-title" style="border: solid 1px #b8b8b8;">
                                <div style="padding: 10px; background-color: #e9e9e9;">
                                    EdFile Name:&nbsp;
                               
                                    <%--<label id="lblFolderName" runat="server" style="color:blue"></label>--%>
                                    <asp:LinkButton ID="lblFolderName" OnClientClick="InsertViewEdFilesAsBookLog();" Text="" runat="server" Style="color: blue; cursor: pointer; text-decoration: underline;" />
                                    &nbsp;&nbsp
                               
                                </div>
                                <div style="padding: 10px;">
                                    <asp:LinkButton ID="lnkFlashViewer" runat="server" Text="" CssClass="ic-icon ic-view" CausesValidation="false"
                                        OnCommand="LinkButton2_Click" Style="margin-top: 5px; display: none;" />
                                    <asp:LinkButton ID="lnkHtmlViewer" runat="server" Text="" CssClass="ic-icon ic-document htmlViewerFile" CausesValidation="false"
                                        OnCommand="LinkButton2_Click" OnClientClick="InsertViewEdFilesAsBookLog();" Style="margin-top: 8px;" />
                                    <asp:LinkButton ID="lnkFolderLogs" runat="server" Text="" CssClass="ic-icon ic-logfile addLog firstLog" CausesValidation="false" ToolTip="Folder logs "
                                        Style="margin-top: 5px" />
                                    <%--  <asp:LinkButton ID="lnkAddFolderLog" runat="server" Text="" CssClass="ic-icon ic-logfile" CausesValidation="false" ToolTip="Folder logs "
                                      Style="margin-top: 5px" />--%>
                                    <asp:LinkButton ID="btnAddtoTab" CssClass="ic-AddTab btnAddtoFolderLog" runat="server" ToolTip="Add To FolderLog" Style="padding-left: 19px; width: 35px;"></asp:LinkButton>
                                    <span class="ic-icon logFormAddFile" id="spanFileUpload1" runat="server">
                                        <asp:FileUpload ID="FileUpload1" runat="server" Style="opacity: 0; cursor: pointer; width: 30px !important; margin-left: 7px; overflow: hidden; height: 28px !important"
                                            ToolTip="Add Log Form" />
                                    </span>
                                    <asp:Button ID="btnUpload" Text="Upload" runat="server" OnClick="btnUpload_Click" CssClass="ic-icon logFormUploadFile" Style="display: none; z-index: 999; width: 28px;" />
                                    <asp:LinkButton ID="btnPreview1" runat="server" CssClass="ic-icon logFormPreveiw"></asp:LinkButton>
                                    <asp:LinkButton ID="btnLog1" runat="server" CssClass="ic-icon ic-logfile aFolderLog" Style="display: none"></asp:LinkButton>
                                    <asp:LinkButton ID="btnPreview2" runat="server" CssClass="ic-icon logFormPreveiw"></asp:LinkButton>
                                    <asp:LinkButton ID="btnLog2" runat="server" CssClass="ic-icon ic-logfile aFolderLog" Style="display: none"></asp:LinkButton>
                                    <asp:LinkButton ID="btnPreview3" runat="server" CssClass="ic-icon logFormPreveiw"></asp:LinkButton>
                                    <asp:LinkButton ID="btnLog3" runat="server" CssClass="ic-icon ic-logfile aFolderLog" Style="display: none"></asp:LinkButton>
                                    <asp:LinkButton ID="btnPreview4" runat="server" CssClass="ic-icon logFormPreveiw"></asp:LinkButton>
                                    <asp:LinkButton ID="btnLog4" runat="server" CssClass="ic-icon ic-logfile aFolderLog" Style="display: none"></asp:LinkButton>
                                    <asp:LinkButton ID="btnPreview5" runat="server" CssClass="ic-icon logFormPreveiw"></asp:LinkButton>
                                    <asp:LinkButton ID="btnLog5" runat="server" CssClass="ic-icon ic-logfile aFolderLog" Style="display: none"></asp:LinkButton>

                                    <asp:LinkButton ID="btnEdFormFolder" runat="server" CssClass="ic-icon ic-edForms aFolderLog" ToolTip="Show EdForms" OnClientClick="return SetFolderAndRedirect();"></asp:LinkButton>
                                    <asp:Button ID="btnShareFolder" Text="Tab" CssClass="ic-icon ic-share sharefolder" runat="server" OnClientClick="return ShareFolder(false);" Style="float: right; background-size: 36px; border: none; margin-right: 5px; cursor: pointer; margin-left: 5px;"
                                        ToolTip="Folder Share"></asp:Button>
                                    <input type="button" class="create-btn btn-small green btnScandoc" id="btnScandoc" name="btnScandoc" runat="server" causesvalidation="false" value="Scan" onclick="ScanDocFile();" />
                                    <asp:LinkButton ID="addCoverSheet" runat="server" CssClass="btn green btnCoverSheet" OnClick="addCoverSheet_Click" ToolTip="Generate Cover Sheet">Cover Sheet</asp:LinkButton>
                                </div>
                            </div>
                            <div class="content-box listing-view" style="margin-top: 30px; margin-bottom: 10px" id="Div6" runat="server">
                                <fieldset id="uploadFieldset">
                                    <div id="divUpload">
                                        <div class='clearfix'></div>
                                        <asp:Image runat="server" ID="Image1" ImageUrl="../Images/loader_bg.gif" ImageAlign="Middle" /><asp:Label
                                            ID="label2" runat="server"></asp:Label>
                                        <asp:Label runat="server" ID="Label4" Style="display: none;">
                                    <img align="absmiddle" alt="" src="uploading.gif"/></asp:Label>
                                        <ajaxToolkit:AjaxFileUpload ID="AjaxFileUpload2" runat="server" Padding-Bottom="4"
                                            Padding-Left="2" Padding-Right="1" Padding-Top="4" ThrobberID="myThrobber" OnClientUploadComplete="onClientUploadComplete"
                                            OnUploadComplete="AjaxFileUpload1_OnUploadComplete" MaximumNumberOfFiles="10"
                                            AzureContainerName="" OnClientUploadCompleteAll="onClientUploadCompleteAll"
                                            OnUploadCompleteAll="AjaxFileUpload1_UploadCompleteAll" OnUploadStart="AjaxFileUpload1_UploadStart"
                                            OnClientUploadStart="onClientUploadStart" ContextKeys="2" />
                                        <div class='clearfix'></div>
                                        <div id="uploadCompleteInfo">
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="content-box listing-view" id="docDetails" runat="server">
                                <fieldset>
                                    <%--<asp:Button class="create-btn btn-small green" Style="margin: 0 0 10px;" runat="server" name="download" ID="downloadall" Text="Download All PDF" OnClientClick="return check();" OnClick="downloadall_Click" />--%>
                                    <div style="float: right; display: flex; margin-bottom: 15px;"
                                        id="tabSearch">
                                        <%--<label>Search</label>--%>
                                        <asp:DropDownList ID="drpSearch" runat="server" Width="100px" CssClass="ddlDropClass" Height="30" Style="margin-right: 5px;">
                                            <%--   <asp:ListItem Value="0">Select</asp:ListItem>--%>
                                            <asp:ListItem Value="1" Selected="True">Name</asp:ListItem>
                                            <asp:ListItem Value="2">Class</asp:ListItem>
                                            <asp:ListItem Value="3">Date</asp:ListItem>
                                            <asp:ListItem Value="4">Index</asp:ListItem>
                                            <asp:ListItem Value="5">Keyword</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox ID="txtName" name="txtName" runat="server" size="20" MaxLength="23" Height="30" Placeholder="Name" />
                                        <asp:TextBox ID="txtClass" runat="server" size="20" MaxLength="20" Height="30px" Placeholder="Class" />
                                        <asp:TextBox ID="txtFDate" runat="server" size="12" Height="30" Placeholder="From Date" Style="margin-right: 3px;" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender7" PopupPosition="Right" runat="server"
                                            TargetControlID="txtFDate" Format="MM-dd-yyyy"></ajaxToolkit:CalendarExtender>
                                        <asp:TextBox ID="txtTDate" runat="server" size="12" Height="30" Placeholder="To Date" />
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender6" PopupPosition="Right" runat="server"
                                            TargetControlID="txtTDate" Format="MM-dd-yyyy"></ajaxToolkit:CalendarExtender>
                                        <asp:TextBox ID="txtIndex" runat="server" size="20" MaxLength="20" Height="30px" Placeholder="Index" />
                                        <asp:TextBox ID="txtKeyword" runat="server" size="20" MaxLength="20" Height="30px" Placeholder="Keyword" />
                                        <asp:Button runat="server" CssClass="create-btn btn-small green" ID="btnSearch" OnClientClick="SetPageIndex();return SearchDocument();" Text="Search" OnClick="btnSearch_Click" Style="margin-left: 5px;" />
                                    </div>
                                    <asp:Button ID="btnConvertOcr" CssClass="btn green btnConvertOcr" OnClientClick="return ConvertPdfValidation();" OnClick="btnConvertOcr_Click" runat="server" Text="OCR" ToolTip="Convert document(s) to searchable document(s)" />
                                    <asp:Button ID="btnCopyFile" Text="Tab" CssClass="btn green btnCopyFile" runat="server" Style="float: right" ToolTip="Transfer document(s) to another folder"></asp:Button>
                                    <asp:Button ID="btnShareFile" Text="Tab" CssClass="ic-icon ic-share" runat="server" OnClientClick="return OnMultipleShare();" Style="float: right; background-size: 40px; border: none; margin-right: 5px; cursor: pointer"
                                        ToolTip="Select Checkboxes to Share Multiple Files"></asp:Button>
                                    <ajaxToolkit:TabContainer runat="server" ID="fileTabs" ActiveTabIndex="0" OnActiveTabChanged="fileTabs_ActiveTabChanged" AutoPostBack="true">

                                        <ajaxToolkit:TabPanel runat="server" ID="Panel0" HeaderText="Divider-1" Visible="true">

                                            <ContentTemplate>
                                                <div class="table-scroll dvGrid" style="max-height: 450px; overflow-y: auto;">
                                                    <asp:GridView ID="GridView0" runat="server" OnRowDataBound="GridView0_RowDataBound"
                                                        DataKeyNames="DocumentID" AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />



                                                        <%--<SelectedRowStyle CssClass="odd" />
                                                 <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox Text="" ID="chkSelectAll" CssClass="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <%--<asp:Label ID="Label34" Text="File|Preview" runat="server" />--%>
                                                                    <asp:Label ID="Label34" Text="File|Indexing|Share" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="LinkButton21" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                                                        NavigateUrl='<%#string.Format("PDFDownloader.aspx?ID={0}", Eval("DocumentID"))%>' />


                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-index" CausesValidation="False"
                                                                        CommandName="" OnClientClick="OpenIndexPopup(this);return false;"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />

                                                                    <asp:LinkButton ID="lnkShare" runat="server" CssClass="ic-icon ic-share" CausesValidation="False" Height="28px" Width="28px">
                                                                    </asp:LinkButton>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libDelete" runat="server" OnClientClick="return OpenDeleteModal(this);" CssClass="ic-icon ic-delete" CausesValidation="False"></asp:LinkButton>
                                                                    <%-- <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" ConfirmText="Are you sure to delete this document?"
                                                                                        TargetControlID="libDelete">
                                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                    <%--<ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="libDelete">
                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="10px"
                                                            ItemStyle-CssClass="table_tekst_preview">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblPreview" runat="server" Text="Preview" />
                                                            </HeaderTemplate>
                                                            
                                                        </asp:TemplateField>--%>
                                                            <%-- Start Move/order--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="50px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Move|Order|Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <a href="javascript:void(0);" id="showMove" class="ic-icon ic-move editdocs" title="Move" runat="server"></a>
                                                                    <a href="javascript:void(0);" id="showOrder" class="ic-icon ic-order deletedocs" title="Order" runat="server"></a>
                                                                    <asp:Button ID="AddToTab" runat="server" CssClass="icon-btn addtotab-filefolderbox lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event)" />
                                                                    <asp:DropDownList ID="ddlDrpMove" runat="server" Width="150px" CssClass="ddlDropMove" Style="display: none"></asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlDropOrder" runat="server" Width="150px" CssClass="ddlDropOrder" Style="display: none"></asp:DropDownList>
                                                                    <asp:LinkButton ID="lbkSave1" runat="server" CausesValidation="True" CommandName="Save" ToolTip="Save" AutoPostBack="False" CssClass="ic-icon ic-save saveDoc" Style="display: none" OnClientClick="SaveOrder(this);"></asp:LinkButton>
                                                                    <a href="javascript:void(0);" id="cancelOrderMove" class="ic-icon ic-cancel cancelDoc" title="Cancel" style="display: none" runat="server"></a>
                                                                    <asp:HiddenField ID="hdndocId" runat="server" Value='<%# Eval("DocumentId")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <%-- END Move/order--%>
                                                            <asp:BoundField DataField="DocumentID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentId" />
                                                            <asp:BoundField DataField="DocumentOrder" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentOrder" />
                                                            <asp:BoundField DataField="DividerID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn dividerId" />
                                                            <asp:BoundField DataField="Name" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn name" />
                                                            <asp:BoundField DataField="PathName" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn pathName" />
                                                            <asp:BoundField DataField="FileExtention" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn fileExtention" />



                                                            <%-- Start Rename--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Rename" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" CommandName="Edit" runat="server" Text=""
                                                                        OnClientClick="showEdit(this);" CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("DocumentId")), Convert.ToInt32(Eval("DocumentOrder")))%>' />
                                                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                                        ToolTip="Update" CssClass="ic-icon ic-save newSave" Style="display: none" name="lnkUpdate"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>

                                                            <%-- Start Rename to convert label into textbox--%>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="DocumentName" runat="server" Text="Preview" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DisplayName")%>' name="DocumentName" Style="cursor: pointer; text-decoration: underline; color: blue"
                                                                        OnClick="showPreview(this);" />
                                                                    <asp:TextBox runat="server" Text='<%# Eval("DisplayName")%>' MaxLength="50"
                                                                        ID="txtDocumentName" Style="display: none; height: 30px;"
                                                                        name="txtDocumentName" CssClass="txtDocumentName" />
                                                                    <asp:HiddenField ID="hdnFileExtension" runat="server" Value='<%# Eval("FileExtention") %>' />
                                                                    <asp:HiddenField ID="hdnFullFileName" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("FullFileName") + "")%>' />
                                                                    <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>
                                                            <asp:BoundField DataField="DOB" HeaderText="Upload Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" ItemStyle-CssClass="dob" ItemStyle-Width="2px" />
                                                            <%-- Start class--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Class" runat="server" Text="Class" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClass" runat="server" CssClass="lblClass" Text='<%#Eval("Class")%>' name="lblClass" />
                                                                    <asp:DropDownList ID="ddlDropClass" name="ddlDropClass" runat="server" Width="60px" CssClass="ddlDropClass" Style="display: none"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END class--%>
                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                            ItemStyle-CssClass="table_tekst_edit">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Index" runat="server" Text="Index" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                  <asp:HyperLink ID="lblIndex" runat="server" CausesValidation="false"  Text='<%#Eval("DisplayName")%>' OnClick="OpenIndexPopup(this);return false;" style="cursor:pointer;" />
                                                                   <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") +".pdf") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />
                                                                There are no documents.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>

                                        <ajaxToolkit:TabPanel runat="server" ID="Panel1" HeaderText="Divider-1" Visible="false">

                                            <ContentTemplate>
                                                <div class="table-scroll dvGrid" style="max-height: 450px; overflow-y: auto;">
                                                    <asp:GridView ID="GridView1" runat="server" OnRowDataBound="GridView0_RowDataBound" CssClass="datatable listing-datatable"
                                                        DataKeyNames="DocumentID " AllowSorting="True" AutoGenerateColumns="false">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%--<SelectedRowStyle CssClass="odd" />
                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox Text="" ID="chkSelectAll" CssClass="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label34" Text="File|Indexing|Share" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="LinkButton21" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                                                        NavigateUrl='<%#string.Format("PDFDownloader.aspx?ID={0}", Eval("DocumentID"))%>' />


                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-index" CausesValidation="False"
                                                                        CommandName="" OnClientClick="OpenIndexPopup(this);return false;"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                    <asp:LinkButton ID="lnkShare" runat="server" CssClass="ic-icon ic-share" CausesValidation="False" Height="28px" Width="28px">
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libDelete" runat="server" OnClientClick="return OpenDeleteModal(this);" CssClass="ic-icon ic-delete" CausesValidation="False"></asp:LinkButton>
                                                                    <%-- <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" ConfirmText="Are you sure to delete this document?"
                                                                                        TargetControlID="libDelete">
                                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                    <%--<ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="libDelete">
                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="10px"
                                                            ItemStyle-CssClass="table_tekst_preview">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblPreview" runat="server" Text="Preview" />
                                                            </HeaderTemplate>
                                                            
                                                        </asp:TemplateField>--%>
                                                            <%-- Start Move/order--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="50px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Move|Order|Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <a href="javascript:void(0);" id="showMove" class="ic-icon ic-move editdocs" title="Move" runat="server"></a>
                                                                    <a href="javascript:void(0);" id="showOrder" class="ic-icon ic-order deletedocs" title="Order" runat="server"></a>
                                                                    <asp:Button ID="AddToTab" runat="server" CssClass="icon-btn addtotab-filefolderbox lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event)" />
                                                                    <asp:DropDownList ID="ddlDrpMove" runat="server" Width="150px" CssClass="ddlDropMove" Style="display: none"></asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlDropOrder" runat="server" Width="150px" CssClass="ddlDropOrder" Style="display: none"></asp:DropDownList>
                                                                    <asp:LinkButton ID="lbkSave1" runat="server" CausesValidation="True" CommandName="Save" ToolTip="Save" AutoPostBack="False" CssClass="ic-icon ic-save saveDoc" Style="display: none" OnClientClick="SaveOrder(this);"></asp:LinkButton>
                                                                    <a href="javascript:void(0);" id="cancelOrderMove" class="ic-icon ic-cancel cancelDoc" title="Cancel" style="display: none" runat="server"></a>
                                                                    <asp:HiddenField ID="hdndocId" runat="server" Value='<%# Eval("DocumentId")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <%-- END Move/order--%>
                                                            <asp:BoundField DataField="DocumentID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentId" />
                                                            <asp:BoundField DataField="DocumentOrder" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentOrder" />
                                                            <asp:BoundField DataField="DividerID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn dividerId" />
                                                            <asp:BoundField DataField="Name" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn name" />
                                                            <asp:BoundField DataField="PathName" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn pathName" />
                                                            <asp:BoundField DataField="FileExtention" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn fileExtention" />



                                                            <%-- Start Rename--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Rename" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" CommandName="Edit" runat="server" Text=""
                                                                        OnClientClick="showEdit(this);" CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("DocumentId")), Convert.ToInt32(Eval("DocumentOrder")))%>' />
                                                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                                        ToolTip="Update" CssClass="ic-icon ic-save newSave" Style="display: none" name="lnkUpdate"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>

                                                            <%-- Start Rename to convert label into textbox--%>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="DocumentName" runat="server" Text="Preview" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DisplayName")%>' name="DocumentName" Style="cursor: pointer; text-decoration: underline; color: blue"
                                                                        OnClick="showPreview(this);" />
                                                                    <asp:TextBox runat="server" Text='<%# Eval("DisplayName")%>' MaxLength="50"
                                                                        ID="txtDocumentName" Style="display: none; height: 30px;"
                                                                        name="txtDocumentName" CssClass="txtDocumentName" />
                                                                    <asp:HiddenField ID="hdnFileExtension" runat="server" Value='<%# Eval("FileExtention") %>' />
                                                                    <asp:HiddenField ID="hdnFullFileName" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("FullFileName") + "")%>' />
                                                                    <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>
                                                            <asp:BoundField DataField="DOB" HeaderText="Upload Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" ItemStyle-CssClass="dob" ItemStyle-Width="2px" />
                                                            <%-- Start class--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Class" runat="server" Text="Class" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClass" runat="server" CssClass="lblClass" Text='<%#Eval("Class")%>' name="lblClass" />
                                                                    <asp:DropDownList ID="ddlDropClass" name="ddlDropClass" runat="server" Width="60px" CssClass="ddlDropClass" Style="display: none"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END class--%>
                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                            ItemStyle-CssClass="table_tekst_edit">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Index" runat="server" Text="Index" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                  <asp:HyperLink ID="lblIndex" runat="server" CausesValidation="false"  Text='<%#Eval("DisplayName")%>' OnClick="OpenIndexPopup(this);return false;" style="cursor:pointer;"/>
                                                                   <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") +".pdf") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />
                                                                There are no documents.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>

                                        <ajaxToolkit:TabPanel runat="server" ID="Panel2" HeaderText="Divider-1" Visible="false">
                                            <ContentTemplate>
                                                <div class="table-scroll dvGrid" style="max-height: 450px; overflow-y: auto;">
                                                    <asp:GridView ID="GridView2" runat="server" OnRowDataBound="GridView0_RowDataBound"
                                                        DataKeyNames="DocumentID " AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%--<SelectedRowStyle CssClass="odd" />
                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox Text="" ID="chkSelectAll" CssClass="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label34" Text="File|Indexing|Share" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="LinkButton21" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                                                        NavigateUrl='<%#string.Format("PDFDownloader.aspx?ID={0}", Eval("DocumentID"))%>' />


                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-index" CausesValidation="False"
                                                                        CommandName="" OnClientClick="OpenIndexPopup(this);return false;"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                    <asp:LinkButton ID="lnkShare" runat="server" CssClass="ic-icon ic-share" CausesValidation="False" Height="28px" Width="28px">
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libDelete" runat="server" OnClientClick="return OpenDeleteModal(this);" CssClass="ic-icon ic-delete" CausesValidation="False"></asp:LinkButton>
                                                                    <%-- <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" ConfirmText="Are you sure to delete this document?"
                                                                                        TargetControlID="libDelete">
                                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                    <%--<ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="libDelete">
                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>



                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="10px"
                                                            ItemStyle-CssClass="table_tekst_preview">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblPreview" runat="server" Text="Preview" />
                                                            </HeaderTemplate>
                                                            
                                                        </asp:TemplateField>--%>
                                                            <%-- Start Move/order--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="50px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Move|Order|Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <a href="javascript:void(0);" id="showMove" class="ic-icon ic-move editdocs" title="Move" runat="server"></a>
                                                                    <a href="javascript:void(0);" id="showOrder" class="ic-icon ic-order deletedocs" title="Order" runat="server"></a>
                                                                    <asp:Button ID="AddToTab" runat="server" CssClass="icon-btn addtotab-filefolderbox lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event)" />
                                                                    <asp:DropDownList ID="ddlDrpMove" runat="server" Width="150px" CssClass="ddlDropMove" Style="display: none"></asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlDropOrder" runat="server" Width="150px" CssClass="ddlDropOrder" Style="display: none"></asp:DropDownList>
                                                                    <asp:LinkButton ID="lbkSave1" runat="server" CausesValidation="True" CommandName="Save" ToolTip="Save" AutoPostBack="False" CssClass="ic-icon ic-save saveDoc" Style="display: none" OnClientClick="SaveOrder(this);"></asp:LinkButton>
                                                                    <a href="javascript:void(0);" id="cancelOrderMove" class="ic-icon ic-cancel cancelDoc" title="Cancel" style="display: none" runat="server"></a>
                                                                    <asp:HiddenField ID="hdndocId" runat="server" Value='<%# Eval("DocumentId")%>' />
                                                                </ItemTemplate>


                                                            </asp:TemplateField>

                                                            <%-- END Move/order--%>
                                                            <asp:BoundField DataField="DocumentID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentId" />
                                                            <asp:BoundField DataField="DocumentOrder" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentOrder" />
                                                            <asp:BoundField DataField="DividerID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn dividerId" />
                                                            <asp:BoundField DataField="Name" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn name" />
                                                            <asp:BoundField DataField="PathName" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn pathName" />
                                                            <asp:BoundField DataField="FileExtention" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn fileExtention" />


                                                            <%-- Start Rename--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Rename" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" CommandName="Edit" runat="server" Text=""
                                                                        OnClientClick="showEdit(this);" CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("DocumentId")), Convert.ToInt32(Eval("DocumentOrder")))%>' />
                                                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                                        ToolTip="Update" CssClass="ic-icon ic-save newSave" Style="display: none" name="lnkUpdate"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>

                                                            <%-- Start Rename to convert label into textbox--%>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="DocumentName" runat="server" Text="Preview" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DisplayName")%>' name="DocumentName" Style="cursor: pointer; text-decoration: underline; color: blue"
                                                                        OnClick="showPreview(this);" />
                                                                    <asp:TextBox runat="server" Text='<%# Eval("DisplayName")%>' MaxLength="50"
                                                                        ID="txtDocumentName" Style="display: none; height: 30px;"
                                                                        name="txtDocumentName" CssClass="txtDocumentName" />
                                                                    <asp:HiddenField ID="hdnFileExtension" runat="server" Value='<%# Eval("FileExtention") %>' />
                                                                    <asp:HiddenField ID="hdnFullFileName" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("FullFileName") + "")%>' />
                                                                    <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>
                                                            <asp:BoundField DataField="DOB" HeaderText="Upload Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" ItemStyle-CssClass="dob" ItemStyle-Width="2px" />
                                                            <%-- Start class--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Class" runat="server" Text="Class" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClass" runat="server" CssClass="lblClass" Text='<%#Eval("Class")%>' name="lblClass" />
                                                                    <asp:DropDownList ID="ddlDropClass" name="ddlDropClass" runat="server" Width="60px" CssClass="ddlDropClass" Style="display: none"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END class--%>
                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                            ItemStyle-CssClass="table_tekst_edit">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Index" runat="server" Text="Index" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                              
                                                                  <asp:HyperLink ID="lblIndex" runat="server" CausesValidation="false"  Text='<%#Eval("DisplayName")%>' OnClick="OpenIndexPopup(this);return false;" style="cursor:pointer;"/>
                                                                   <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") +".pdf") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />
                                                                There are no documents.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>

                                        <ajaxToolkit:TabPanel runat="server" ID="Panel3" HeaderText="Divider-1" Visible="false">
                                            <ContentTemplate>
                                                <div class="table-scroll dvGrid" style="max-height: 450px; overflow-y: auto;">
                                                    <asp:GridView ID="GridView3" runat="server" OnRowDataBound="GridView0_RowDataBound"
                                                        DataKeyNames="DocumentID " AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%--<SelectedRowStyle CssClass="odd" />
                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox Text="" ID="chkSelectAll" CssClass="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label34" Text="File|Indexing|Share" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="LinkButton21" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                                                        NavigateUrl='<%#string.Format("PDFDownloader.aspx?ID={0}", Eval("DocumentID"))%>' />


                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-index" CausesValidation="False"
                                                                        CommandName="" OnClientClick="OpenIndexPopup(this);return false;"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                    <asp:LinkButton ID="lnkShare" runat="server" CssClass="ic-icon ic-share" CausesValidation="False" Height="28px" Width="28px">
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libDelete" runat="server" OnClientClick="return OpenDeleteModal(this);" CssClass="ic-icon ic-delete" CausesValidation="False"></asp:LinkButton>
                                                                    <%-- <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" ConfirmText="Are you sure to delete this document?"
                                                                                        TargetControlID="libDelete">
                                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                    <%--<ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="libDelete">
                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="10px"
                                                            ItemStyle-CssClass="table_tekst_preview">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblPreview" runat="server" Text="Preview" />
                                                            </HeaderTemplate>
                                                            
                                                        </asp:TemplateField>--%>
                                                            <%-- Start Move/order--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="50px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Move|Order|Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <a href="javascript:void(0);" id="showMove" class="ic-icon ic-move editdocs" title="Move" runat="server"></a>
                                                                    <a href="javascript:void(0);" id="showOrder" class="ic-icon ic-order deletedocs" title="Order" runat="server"></a>
                                                                    <asp:Button ID="AddToTab" runat="server" CssClass="icon-btn addtotab-filefolderbox lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event)" />

                                                                    <asp:DropDownList ID="ddlDrpMove" runat="server" Width="150px" CssClass="ddlDropMove" Style="display: none"></asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlDropOrder" runat="server" Width="150px" CssClass="ddlDropOrder" Style="display: none"></asp:DropDownList>
                                                                    <asp:LinkButton ID="lbkSave1" runat="server" CausesValidation="True" CommandName="Save" ToolTip="Save" AutoPostBack="False" CssClass="ic-icon ic-save saveDoc" Style="display: none" OnClientClick="SaveOrder(this);"></asp:LinkButton>
                                                                    <a href="javascript:void(0);" id="cancelOrderMove" class="ic-icon ic-cancel cancelDoc" title="Cancel" style="display: none" runat="server"></a>
                                                                    <asp:HiddenField ID="hdndocId" runat="server" Value='<%# Eval("DocumentId")%>' />
                                                                </ItemTemplate>


                                                            </asp:TemplateField>

                                                            <%-- END Move/order--%>
                                                            <asp:BoundField DataField="DocumentID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentId" />
                                                            <asp:BoundField DataField="DocumentOrder" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentOrder" />
                                                            <asp:BoundField DataField="DividerID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn dividerId" />
                                                            <asp:BoundField DataField="Name" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn name" />
                                                            <asp:BoundField DataField="PathName" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn pathName" />
                                                            <asp:BoundField DataField="FileExtention" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn fileExtention" />


                                                            <%-- Start Rename--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Rename" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" CommandName="Edit" runat="server" Text=""
                                                                        OnClientClick="showEdit(this);" CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("DocumentId")), Convert.ToInt32(Eval("DocumentOrder")))%>' />
                                                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                                        ToolTip="Update" CssClass="ic-icon ic-save newSave" Style="display: none" name="lnkUpdate"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>

                                                            <%-- Start Rename to convert label into textbox--%>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="DocumentName" runat="server" Text="Preview" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DisplayName")%>' name="DocumentName" Style="cursor: pointer; text-decoration: underline; color: blue"
                                                                        OnClick="showPreview(this);"></asp:Label>
                                                                    <asp:TextBox runat="server" Text='<%# Eval("DisplayName")%>' MaxLength="50"
                                                                        ID="txtDocumentName" Style="display: none; height: 30px;"
                                                                        name="txtDocumentName" CssClass="txtDocumentName" />
                                                                    <asp:HiddenField ID="hdnFileExtension" runat="server" Value='<%# Eval("FileExtention") %>' />
                                                                    <asp:HiddenField ID="hdnFullFileName" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("FullFileName") + "")%>' />
                                                                    <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>
                                                            <asp:BoundField DataField="DOB" HeaderText="Upload Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" ItemStyle-CssClass="dob" ItemStyle-Width="2px" />
                                                            <%-- Start class--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Class" runat="server" Text="Class" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClass" runat="server" CssClass="lblClass" Text='<%#Eval("Class")%>' name="lblClass" />
                                                                    <asp:DropDownList ID="ddlDropClass" name="ddlDropClass" runat="server" Width="60px" CssClass="ddlDropClass" Style="display: none"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END class--%>
                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                            ItemStyle-CssClass="table_tekst_edit">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Index" runat="server" Text="Index" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                            
                                                                  <asp:HyperLink ID="lblIndex" runat="server" CausesValidation="false"  Text='<%#Eval("DisplayName")%>' OnClick="OpenIndexPopup(this);return false;" style="cursor:pointer;"/>
                                                                   <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") +".pdf") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />
                                                                There are no documents.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>

                                        <ajaxToolkit:TabPanel runat="server" ID="Panel4" HeaderText="Divider-1" Visible="false">
                                            <ContentTemplate>
                                                <div class="table-scroll dvGrid" style="max-height: 450px; overflow-y: auto;">
                                                    <asp:GridView ID="GridView4" runat="server" OnRowDataBound="GridView0_RowDataBound"
                                                        DataKeyNames="DocumentID " AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%--<SelectedRowStyle CssClass="odd" />
                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox Text="" ID="chkSelectAll" CssClass="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label34" Text="File|Indexing|Share" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="LinkButton21" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                                                        NavigateUrl='<%#string.Format("PDFDownloader.aspx?ID={0}", Eval("DocumentID"))%>' />


                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-index" CausesValidation="False"
                                                                        CommandName="" OnClientClick="OpenIndexPopup(this);return false;"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                    <asp:LinkButton ID="lnkShare" runat="server" CssClass="ic-icon ic-share" CausesValidation="False" Height="28px" Width="28px">
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libDelete" runat="server" OnClientClick="return OpenDeleteModal(this);" CssClass="ic-icon ic-delete" CausesValidation="False"></asp:LinkButton>
                                                                    <%-- <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" ConfirmText="Are you sure to delete this document?"
                                                                                        TargetControlID="libDelete">
                                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                    <%--<ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="libDelete">
                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="10px"
                                                            ItemStyle-CssClass="table_tekst_preview">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblPreview" runat="server" Text="Preview" />
                                                            </HeaderTemplate>
                                                            
                                                        </asp:TemplateField>--%>
                                                            <%-- Start Move/order--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="50px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Move|Order|Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <a href="javascript:void(0);" id="showMove" class="ic-icon ic-move editdocs" title="Move" runat="server"></a>
                                                                    <a href="javascript:void(0);" id="showOrder" class="ic-icon ic-order deletedocs" title="Order" runat="server"></a>
                                                                    <asp:Button ID="AddToTab" runat="server" CssClass="icon-btn addtotab-filefolderbox lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event)" />
                                                                    <asp:DropDownList ID="ddlDrpMove" runat="server" Width="150px" CssClass="ddlDropMove" Style="display: none"></asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlDropOrder" runat="server" Width="150px" CssClass="ddlDropOrder" Style="display: none"></asp:DropDownList>
                                                                    <asp:LinkButton ID="lbkSave1" runat="server" CausesValidation="True" CommandName="Save" ToolTip="Save" AutoPostBack="False" CssClass="ic-icon ic-save saveDoc" Style="display: none" OnClientClick="SaveOrder(this);"></asp:LinkButton>
                                                                    <a href="javascript:void(0);" id="cancelOrderMove" class="ic-icon ic-cancel cancelDoc" title="Cancel" style="display: none" runat="server"></a>
                                                                    <asp:HiddenField ID="hdndocId" runat="server" Value='<%# Eval("DocumentId")%>' />
                                                                </ItemTemplate>


                                                            </asp:TemplateField>

                                                            <%-- END Move/order--%>
                                                            <asp:BoundField DataField="DocumentID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentId" />
                                                            <asp:BoundField DataField="DocumentOrder" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentOrder" />
                                                            <asp:BoundField DataField="DividerID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn dividerId" />
                                                            <asp:BoundField DataField="Name" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn name" />
                                                            <asp:BoundField DataField="PathName" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn pathName" />
                                                            <asp:BoundField DataField="FileExtention" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn fileExtention" />



                                                            <%-- Start Rename--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Rename" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" CommandName="Edit" runat="server" Text=""
                                                                        OnClientClick="showEdit(this);" CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("DocumentId")), Convert.ToInt32(Eval("DocumentOrder")))%>' />
                                                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                                        ToolTip="Update" CssClass="ic-icon ic-save newSave" Style="display: none" name="lnkUpdate"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>

                                                            <%-- Start Rename to convert label into textbox--%>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="DocumentName" runat="server" Text="Preview" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DisplayName")%>' name="DocumentName" Style="cursor: pointer; text-decoration: underline; color: blue"
                                                                        OnClick="showPreview(this);" />
                                                                    <asp:TextBox runat="server" Text='<%# Eval("DisplayName")%>' MaxLength="50"
                                                                        ID="txtDocumentName" Style="display: none; height: 30px;"
                                                                        name="txtDocumentName" CssClass="txtDocumentName" />
                                                                    <asp:HiddenField ID="hdnFileExtension" runat="server" Value='<%# Eval("FileExtention") %>' />
                                                                    <asp:HiddenField ID="hdnFullFileName" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("FullFileName") + "")%>' />
                                                                    <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>
                                                            <asp:BoundField DataField="DOB" HeaderText="Upload Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" ItemStyle-CssClass="dob" ItemStyle-Width="2px" />
                                                            <%-- Start class--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Class" runat="server" Text="Class" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClass" runat="server" CssClass="lblClass" Text='<%#Eval("Class")%>' name="lblClass" />
                                                                    <asp:DropDownList ID="ddlDropClass" name="ddlDropClass" runat="server" Width="60px" CssClass="ddlDropClass" Style="display: none"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END class--%>
                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                            ItemStyle-CssClass="table_tekst_edit">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Index" runat="server" Text="Index" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                               
                                                                  <asp:HyperLink ID="lblIndex" runat="server" CausesValidation="false"  Text='<%#Eval("DisplayName")%>' OnClick="OpenIndexPopup(this);return false;" style="cursor:pointer;"/>
                                                                   <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") +".pdf") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />
                                                                There are no documents.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>

                                        <ajaxToolkit:TabPanel runat="server" ID="Panel5" HeaderText="Divider-1" Visible="false">
                                            <ContentTemplate>
                                                <div class="table-scroll dvGrid" style="max-height: 450px; overflow-y: auto;">
                                                    <asp:GridView ID="GridView5" runat="server" OnRowDataBound="GridView0_RowDataBound"
                                                        DataKeyNames="DocumentID " AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%--<SelectedRowStyle CssClass="odd" />
                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox Text="" ID="chkSelectAll" CssClass="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label34" Text="File|Indexing|Share" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="LinkButton21" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                                                        NavigateUrl='<%#string.Format("PDFDownloader.aspx?ID={0}", Eval("DocumentID"))%>' />


                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-index" CausesValidation="False"
                                                                        CommandName="" OnClientClick="OpenIndexPopup(this);return false;"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                    <asp:LinkButton ID="lnkShare" runat="server" CssClass="ic-icon ic-share" CausesValidation="False" Height="28px" Width="28px">
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libDelete" runat="server" OnClientClick="return OpenDeleteModal(this);" CssClass="ic-icon ic-delete" CausesValidation="False"></asp:LinkButton>
                                                                    <%-- <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" ConfirmText="Are you sure to delete this document?"
                                                                                        TargetControlID="libDelete">
                                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                    <%--<ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="libDelete">
                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="10px"
                                                            ItemStyle-CssClass="table_tekst_preview">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblPreview" runat="server" Text="Preview" />
                                                            </HeaderTemplate>
                                                            
                                                        </asp:TemplateField>--%>
                                                            <%-- Start Move/order--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="50px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Move|Order|Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <a href="javascript:void(0);" id="showMove" class="ic-icon ic-move editdocs" title="Move" runat="server"></a>
                                                                    <a href="javascript:void(0);" id="showOrder" class="ic-icon ic-order deletedocs" title="Order" runat="server"></a>
                                                                    <asp:Button ID="AddToTab" runat="server" CssClass="icon-btn addtotab-filefolderbox lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event)" />
                                                                    <asp:DropDownList ID="ddlDrpMove" runat="server" Width="150px" CssClass="ddlDropMove" Style="display: none"></asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlDropOrder" runat="server" Width="150px" CssClass="ddlDropOrder" Style="display: none"></asp:DropDownList>
                                                                    <asp:LinkButton ID="lbkSave1" runat="server" CausesValidation="True" CommandName="Save" ToolTip="Save" AutoPostBack="False" CssClass="ic-icon ic-save saveDoc" Style="display: none" OnClientClick="SaveOrder(this);"></asp:LinkButton>
                                                                    <a href="javascript:void(0);" id="cancelOrderMove" class="ic-icon ic-cancel cancelDoc" title="Cancel" style="display: none" runat="server"></a>
                                                                    <asp:HiddenField ID="hdndocId" runat="server" Value='<%# Eval("DocumentId")%>' />
                                                                </ItemTemplate>


                                                            </asp:TemplateField>

                                                            <%-- END Move/order--%>
                                                            <asp:BoundField DataField="DocumentID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentId" />
                                                            <asp:BoundField DataField="DocumentOrder" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentOrder" />
                                                            <asp:BoundField DataField="DividerID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn dividerId" />
                                                            <asp:BoundField DataField="Name" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn name" />
                                                            <asp:BoundField DataField="PathName" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn pathName" />
                                                            <asp:BoundField DataField="FileExtention" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn fileExtention" />



                                                            <%-- Start Rename--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Rename" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" CommandName="Edit" runat="server" Text=""
                                                                        OnClientClick="showEdit(this);" CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("DocumentId")), Convert.ToInt32(Eval("DocumentOrder")))%>' />
                                                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                                        ToolTip="Update" CssClass="ic-icon ic-save newSave" Style="display: none" name="lnkUpdate"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>

                                                            <%-- Start Rename to convert label into textbox--%>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="DocumentName" runat="server" Text="Preview" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DisplayName")%>' name="DocumentName" Style="cursor: pointer; text-decoration: underline; color: blue"
                                                                        OnClick="showPreview(this);" />
                                                                    <asp:TextBox runat="server" Text='<%# Eval("DisplayName")%>' MaxLength="50"
                                                                        ID="txtDocumentName" Style="display: none; height: 30px;"
                                                                        name="txtDocumentName" CssClass="txtDocumentName" />
                                                                    <asp:HiddenField ID="hdnFileExtension" runat="server" Value='<%# Eval("FileExtention") %>' />
                                                                    <asp:HiddenField ID="hdnFullFileName" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("FullFileName") + "")%>' />
                                                                    <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>
                                                            <asp:BoundField DataField="DOB" HeaderText="Upload Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" ItemStyle-CssClass="dob" ItemStyle-Width="2px" />
                                                            <%-- Start class--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Class" runat="server" Text="Class" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClass" runat="server" CssClass="lblClass" Text='<%#Eval("Class")%>' name="lblClass" />
                                                                    <asp:DropDownList ID="ddlDropClass" name="ddlDropClass" runat="server" Width="60px" CssClass="ddlDropClass" Style="display: none"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END class--%>
                                                            <%--   <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                            ItemStyle-CssClass="table_tekst_edit">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Index" runat="server" Text="Index" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                              
                                                                  <asp:HyperLink ID="lblIndex" runat="server" CausesValidation="false"  Text='<%#Eval("DisplayName")%>' OnClick="OpenIndexPopup(this);return false;" style="cursor:pointer;"/>
                                                                   <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") +".pdf") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />
                                                                There are no documents.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>

                                        <ajaxToolkit:TabPanel runat="server" ID="Panel6" HeaderText="Divider-1" Visible="false">
                                            <ContentTemplate>
                                                <div class="table-scroll dvGrid" style="max-height: 450px; overflow-y: auto;">
                                                    <asp:GridView ID="GridView6" runat="server" OnRowDataBound="GridView0_RowDataBound"
                                                        DataKeyNames="DocumentID " AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%--<SelectedRowStyle CssClass="odd" />
                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox Text="" ID="chkSelectAll" CssClass="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label34" Text="File|Indexing|Share" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="LinkButton21" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                                                        NavigateUrl='<%#string.Format("PDFDownloader.aspx?ID={0}", Eval("DocumentID"))%>' />


                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-index" CausesValidation="False"
                                                                        CommandName="" OnClientClick="OpenIndexPopup(this);return false;"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                    <asp:LinkButton ID="lnkShare" runat="server" CssClass="ic-icon ic-share" CausesValidation="False" Height="28px" Width="28px">
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libDelete" runat="server" OnClientClick="return OpenDeleteModal(this);" CssClass="ic-icon ic-delete" CausesValidation="False"></asp:LinkButton>
                                                                    <%-- <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" ConfirmText="Are you sure to delete this document?"
                                                                                        TargetControlID="libDelete">
                                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                    <%--<ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="libDelete">
                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="10px"
                                                            ItemStyle-CssClass="table_tekst_preview">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblPreview" runat="server" Text="Preview" />
                                                            </HeaderTemplate>
                                                            
                                                        </asp:TemplateField>--%>
                                                            <%-- Start Move/order--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="50px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Move|Order|Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <a href="javascript:void(0);" id="showMove" class="ic-icon ic-move editdocs" title="Move" runat="server"></a>
                                                                    <a href="javascript:void(0);" id="showOrder" class="ic-icon ic-order deletedocs" title="Order" runat="server"></a>
                                                                    <asp:Button ID="AddToTab" runat="server" CssClass="icon-btn addtotab-filefolderbox lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event)" />
                                                                    <asp:DropDownList ID="ddlDrpMove" runat="server" Width="150px" CssClass="ddlDropMove" Style="display: none"></asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlDropOrder" runat="server" Width="150px" CssClass="ddlDropOrder" Style="display: none"></asp:DropDownList>
                                                                    <asp:LinkButton ID="lbkSave1" runat="server" CausesValidation="True" CommandName="Save" ToolTip="Save" AutoPostBack="False" CssClass="ic-icon ic-save saveDoc" Style="display: none" OnClientClick="SaveOrder(this);"></asp:LinkButton>
                                                                    <a href="javascript:void(0);" id="cancelOrderMove" class="ic-icon ic-cancel cancelDoc" title="Cancel" style="display: none" runat="server"></a>
                                                                    <asp:HiddenField ID="hdndocId" runat="server" Value='<%# Eval("DocumentId")%>' />
                                                                </ItemTemplate>


                                                            </asp:TemplateField>

                                                            <%-- END Move/order--%>
                                                            <asp:BoundField DataField="DocumentID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentId" />
                                                            <asp:BoundField DataField="DocumentOrder" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentOrder" />
                                                            <asp:BoundField DataField="DividerID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn dividerId" />
                                                            <asp:BoundField DataField="Name" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn name" />
                                                            <asp:BoundField DataField="PathName" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn pathName" />
                                                            <asp:BoundField DataField="FileExtention" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn fileExtention" />



                                                            <%-- Start Rename--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Rename" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" CommandName="Edit" runat="server" Text=""
                                                                        OnClientClick="showEdit(this);" CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("DocumentId")), Convert.ToInt32(Eval("DocumentOrder")))%>' />
                                                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                                        ToolTip="Update" CssClass="ic-icon ic-save newSave" Style="display: none" name="lnkUpdate"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>

                                                            <%-- Start Rename to convert label into textbox--%>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="DocumentName" runat="server" Text="Preview" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DisplayName")%>' name="DocumentName" Style="cursor: pointer; text-decoration: underline; color: blue"
                                                                        OnClick="showPreview(this);" />
                                                                    <asp:TextBox runat="server" Text='<%# Eval("DisplayName")%>' MaxLength="50"
                                                                        ID="txtDocumentName" Style="display: none; height: 30px;"
                                                                        name="txtDocumentName" CssClass="txtDocumentName" />
                                                                    <asp:HiddenField ID="hdnFileExtension" runat="server" Value='<%# Eval("FileExtention") %>' />
                                                                    <asp:HiddenField ID="hdnFullFileName" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("FullFileName") + "")%>' />
                                                                    <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>
                                                            <asp:BoundField DataField="DOB" HeaderText="Upload Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" ItemStyle-CssClass="dob" ItemStyle-Width="2px" />
                                                            <%-- Start class--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Class" runat="server" Text="Class" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClass" runat="server" CssClass="lblClass" Text='<%#Eval("Class")%>' name="lblClass" />
                                                                    <asp:DropDownList ID="ddlDropClass" name="ddlDropClass" runat="server" Width="60px" CssClass="ddlDropClass" Style="display: none"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END class--%>
                                                            <%--<asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                            ItemStyle-CssClass="table_tekst_edit">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Index" runat="server" Text="Index" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                          
                                                                  <asp:HyperLink ID="lblIndex" runat="server" CausesValidation="false"  Text='<%#Eval("DisplayName")%>' OnClick="OpenIndexPopup(this);return false;" style="cursor:pointer;"/>
                                                                   <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") +".pdf") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />
                                                                There are no documents.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>

                                        <ajaxToolkit:TabPanel runat="server" ID="Panel7" HeaderText="Divider-1" Visible="false">
                                            <ContentTemplate>
                                                <div class="table-scroll dvGrid" style="max-height: 450px; overflow-y: auto;">
                                                    <asp:GridView ID="GridView7" runat="server" OnRowDataBound="GridView0_RowDataBound"
                                                        DataKeyNames="DocumentID " AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%--<SelectedRowStyle CssClass="odd" />
                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox Text="" ID="chkSelectAll" CssClass="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label34" Text="File|Indexing|Share" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="LinkButton21" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                                                        NavigateUrl='<%#string.Format("PDFDownloader.aspx?ID={0}", Eval("DocumentID"))%>' />


                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-index" CausesValidation="False"
                                                                        CommandName="" OnClientClick="OpenIndexPopup(this);return false;"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                    <asp:LinkButton ID="lnkShare" runat="server" CssClass="ic-icon ic-share" CausesValidation="False" Height="28px" Width="28px">
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libDelete" runat="server" OnClientClick="return OpenDeleteModal(this);" CssClass="ic-icon ic-delete" CausesValidation="False"></asp:LinkButton>
                                                                    <%-- <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" ConfirmText="Are you sure to delete this document?"
                                                                                        TargetControlID="libDelete">
                                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                    <%--<ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="libDelete">
                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="10px"
                                                            ItemStyle-CssClass="table_tekst_preview">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblPreview" runat="server" Text="Preview" />
                                                            </HeaderTemplate>
                                                            
                                                        </asp:TemplateField>--%>
                                                            <%-- Start Move/order--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="50px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Move|Order|Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <a href="javascript:void(0);" id="showMove" class="ic-icon ic-move editdocs" title="Move" runat="server"></a>
                                                                    <a href="javascript:void(0);" id="showOrder" class="ic-icon ic-order deletedocs" title="Order" runat="server"></a>
                                                                    <asp:Button ID="AddToTab" runat="server" CssClass="icon-btn addtotab-filefolderbox lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event)" />
                                                                    <asp:DropDownList ID="ddlDrpMove" runat="server" Width="150px" CssClass="ddlDropMove" Style="display: none"></asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlDropOrder" runat="server" Width="150px" CssClass="ddlDropOrder" Style="display: none"></asp:DropDownList>
                                                                    <asp:LinkButton ID="lbkSave1" runat="server" CausesValidation="True" CommandName="Save" ToolTip="Save" AutoPostBack="False" CssClass="ic-icon ic-save saveDoc" Style="display: none" OnClientClick="SaveOrder(this);"></asp:LinkButton>
                                                                    <a href="javascript:void(0);" id="cancelOrderMove" class="ic-icon ic-cancel cancelDoc" title="Cancel" style="display: none" runat="server"></a>
                                                                    <asp:HiddenField ID="hdndocId" runat="server" Value='<%# Eval("DocumentId")%>' />
                                                                </ItemTemplate>


                                                            </asp:TemplateField>

                                                            <%-- END Move/order--%>
                                                            <asp:BoundField DataField="DocumentID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentId" />
                                                            <asp:BoundField DataField="DocumentOrder" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentOrder" />
                                                            <asp:BoundField DataField="DividerID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn dividerId" />
                                                            <asp:BoundField DataField="Name" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn name" />
                                                            <asp:BoundField DataField="PathName" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn pathName" />
                                                            <asp:BoundField DataField="FileExtention" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn fileExtention" />




                                                            <%-- Start Rename--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Rename" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" CommandName="Edit" runat="server" Text=""
                                                                        OnClientClick="showEdit(this);" CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("DocumentId")), Convert.ToInt32(Eval("DocumentOrder")))%>' />
                                                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                                        ToolTip="Update" CssClass="ic-icon ic-save newSave" Style="display: none" name="lnkUpdate"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>

                                                            <%-- Start Rename to convert label into textbox--%>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="DocumentName" runat="server" Text="Preview" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DisplayName")%>' name="DocumentName" Style="cursor: pointer; text-decoration: underline; color: blue"
                                                                        OnClick="showPreview(this);" />
                                                                    <asp:TextBox runat="server" Text='<%# Eval("DisplayName")%>' MaxLength="50"
                                                                        ID="txtDocumentName" Style="display: none; height: 30px;"
                                                                        name="txtDocumentName" CssClass="txtDocumentName" />
                                                                    <asp:HiddenField ID="hdnFileExtension" runat="server" Value='<%# Eval("FileExtention") %>' />
                                                                    <asp:HiddenField ID="hdnFullFileName" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("FullFileName") + "")%>' />
                                                                    <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>
                                                            <asp:BoundField DataField="DOB" HeaderText="Upload Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" ItemStyle-CssClass="dob" ItemStyle-Width="2px" />
                                                            <%-- Start class--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Class" runat="server" Text="Class" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClass" runat="server" CssClass="lblClass" Text='<%#Eval("Class")%>' name="lblClass" />
                                                                    <asp:DropDownList ID="ddlDropClass" name="ddlDropClass" runat="server" Width="60px" CssClass="ddlDropClass" Style="display: none"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END class--%>
                                                            <%--<asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                            ItemStyle-CssClass="table_tekst_edit">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Index" runat="server" Text="Index" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                            
                                                                  <asp:HyperLink ID="lblIndex" runat="server" CausesValidation="false"  Text='<%#Eval("DisplayName")%>' OnClick="OpenIndexPopup(this);return false;" style="cursor:pointer;"/>
                                                                   <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") +".pdf") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />
                                                                There are no documents.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>

                                        <ajaxToolkit:TabPanel runat="server" ID="Panel8" HeaderText="Divider-1" Visible="false">
                                            <ContentTemplate>
                                                <div class="table-scroll dvGrid" style="max-height: 450px; overflow-y: auto;">
                                                    <asp:GridView ID="GridView8" runat="server" OnRowDataBound="GridView0_RowDataBound"
                                                        DataKeyNames="DocumentID " AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%--<SelectedRowStyle CssClass="odd" />
                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox Text="" ID="chkSelectAll" CssClass="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label34" Text="File|Indexing|Share" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="LinkButton21" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                                                        NavigateUrl='<%#string.Format("PDFDownloader.aspx?ID={0}", Eval("DocumentID"))%>' />


                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-index" CausesValidation="False"
                                                                        CommandName="" OnClientClick="OpenIndexPopup(this);return false;"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                    <asp:LinkButton ID="lnkShare" runat="server" CssClass="ic-icon ic-share" CausesValidation="False" Height="28px" Width="28px">
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libDelete" runat="server" OnClientClick="return OpenDeleteModal(this);" CssClass="ic-icon ic-delete" CausesValidation="False"></asp:LinkButton>
                                                                    <%-- <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" ConfirmText="Are you sure to delete this document?"
                                                                                        TargetControlID="libDelete">
                                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                    <%--<ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="libDelete">
                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="10px"
                                                            ItemStyle-CssClass="table_tekst_preview">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblPreview" runat="server" Text="Preview" />
                                                            </HeaderTemplate>
                                                            
                                                        </asp:TemplateField>--%>
                                                            <%-- Start Move/order--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="50px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Move|Order|Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <a href="javascript:void(0);" id="showMove" class="ic-icon ic-move editdocs" title="Move" runat="server"></a>
                                                                    <a href="javascript:void(0);" id="showOrder" class="ic-icon ic-order deletedocs" title="Order" runat="server"></a>
                                                                    <asp:Button ID="AddToTab" runat="server" CssClass="icon-btn addtotab-filefolderbox lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event)" />
                                                                    <asp:DropDownList ID="ddlDrpMove" runat="server" Width="150px" CssClass="ddlDropMove" Style="display: none"></asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlDropOrder" runat="server" Width="150px" CssClass="ddlDropOrder" Style="display: none"></asp:DropDownList>
                                                                    <asp:LinkButton ID="lbkSave1" runat="server" CausesValidation="True" CommandName="Save" ToolTip="Save" AutoPostBack="False" CssClass="ic-icon ic-save saveDoc" Style="display: none" OnClientClick="SaveOrder(this);"></asp:LinkButton>
                                                                    <a href="javascript:void(0);" id="cancelOrderMove" class="ic-icon ic-cancel cancelDoc" title="Cancel" style="display: none" runat="server"></a>
                                                                    <asp:HiddenField ID="hdndocId" runat="server" Value='<%# Eval("DocumentId")%>' />
                                                                </ItemTemplate>


                                                            </asp:TemplateField>

                                                            <%-- END Move/order--%>
                                                            <asp:BoundField DataField="DocumentID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentId" />
                                                            <asp:BoundField DataField="DocumentOrder" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentOrder" />
                                                            <asp:BoundField DataField="DividerID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn dividerId" />
                                                            <asp:BoundField DataField="Name" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn name" />
                                                            <asp:BoundField DataField="PathName" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn pathName" />
                                                            <asp:BoundField DataField="FileExtention" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn fileExtention" />




                                                            <%-- Start Rename--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Rename" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" CommandName="Edit" runat="server" Text=""
                                                                        OnClientClick="showEdit(this);" CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("DocumentId")), Convert.ToInt32(Eval("DocumentOrder")))%>' />
                                                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                                        ToolTip="Update" CssClass="ic-icon ic-save newSave" Style="display: none" name="lnkUpdate"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>

                                                            <%-- Start Rename to convert label into textbox--%>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="DocumentName" runat="server" Text="Preview" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DisplayName")%>' name="DocumentName" Style="cursor: pointer; text-decoration: underline; color: blue"
                                                                        OnClick="showPreview(this);" />
                                                                    <asp:TextBox runat="server" Text='<%# Eval("DisplayName")%>' MaxLength="50"
                                                                        ID="txtDocumentName" Style="display: none; height: 30px;"
                                                                        name="txtDocumentName" CssClass="txtDocumentName" />
                                                                    <asp:HiddenField ID="hdnFileExtension" runat="server" Value='<%# Eval("FileExtention") %>' />
                                                                    <asp:HiddenField ID="hdnFullFileName" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("FullFileName") + "")%>' />
                                                                    <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>
                                                            <asp:BoundField DataField="DOB" HeaderText="Upload Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" ItemStyle-CssClass="dob" ItemStyle-Width="2px" />
                                                            <%-- Start class--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Class" runat="server" Text="Class" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClass" runat="server" CssClass="lblClass" Text='<%#Eval("Class")%>' name="lblClass" />
                                                                    <asp:DropDownList ID="ddlDropClass" name="ddlDropClass" runat="server" Width="60px" CssClass="ddlDropClass" Style="display: none"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END class--%>
                                                            <%--<asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                            ItemStyle-CssClass="table_tekst_edit">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Index" runat="server" Text="Index" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                              
                                                                  <asp:HyperLink ID="lblIndex" runat="server" CausesValidation="false"  Text='<%#Eval("DisplayName")%>' OnClick="OpenIndexPopup(this);return false;" style="cursor:pointer;" />
                                                                   <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") +".pdf") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />
                                                                There are no documents.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>

                                        <ajaxToolkit:TabPanel runat="server" ID="Panel9" HeaderText="Divider-1" Visible="false">
                                            <ContentTemplate>
                                                <div class="table-scroll dvGrid" style="max-height: 450px; overflow-y: auto;">
                                                    <asp:GridView ID="GridView9" runat="server" OnRowDataBound="GridView0_RowDataBound"
                                                        DataKeyNames="DocumentID " AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%--<SelectedRowStyle CssClass="odd" />
                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox Text="" ID="chkSelectAll" CssClass="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label34" Text="File|Indexing|Share" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="LinkButton21" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                                                        NavigateUrl='<%#string.Format("PDFDownloader.aspx?ID={0}", Eval("DocumentID"))%>' />


                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-index" CausesValidation="False"
                                                                        CommandName="" OnClientClick="OpenIndexPopup(this);return false;"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                    <asp:LinkButton ID="lnkShare" runat="server" CssClass="ic-icon ic-share" CausesValidation="False" Height="28px" Width="28px">
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libDelete" runat="server" OnClientClick="return OpenDeleteModal(this);" CssClass="ic-icon ic-delete" CausesValidation="False"></asp:LinkButton>
                                                                    <%-- <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" ConfirmText="Are you sure to delete this document?"
                                                                                        TargetControlID="libDelete">
                                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                    <%--<ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="libDelete">
                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="10px"
                                                            ItemStyle-CssClass="table_tekst_preview">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblPreview" runat="server" Text="Preview" />
                                                            </HeaderTemplate>
                                                            
                                                        </asp:TemplateField>--%>
                                                            <%-- Start Move/order--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="50px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Move|Order|Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <a href="javascript:void(0);" id="showMove" class="ic-icon ic-move editdocs" title="Move" runat="server"></a>
                                                                    <a href="javascript:void(0);" id="showOrder" class="ic-icon ic-order deletedocs" title="Order" runat="server"></a>
                                                                    <asp:Button ID="AddToTab" runat="server" CssClass="icon-btn addtotab-filefolderbox lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event)" />
                                                                    <asp:DropDownList ID="ddlDrpMove" runat="server" Width="150px" CssClass="ddlDropMove" Style="display: none"></asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlDropOrder" runat="server" Width="150px" CssClass="ddlDropOrder" Style="display: none"></asp:DropDownList>
                                                                    <asp:LinkButton ID="lbkSave1" runat="server" CausesValidation="True" CommandName="Save" ToolTip="Save" AutoPostBack="False" CssClass="ic-icon ic-save saveDoc" Style="display: none" OnClientClick="SaveOrder(this);"></asp:LinkButton>
                                                                    <a href="javascript:void(0);" id="cancelOrderMove" class="ic-icon ic-cancel cancelDoc" title="Cancel" style="display: none" runat="server"></a>
                                                                    <asp:HiddenField ID="hdndocId" runat="server" Value='<%# Eval("DocumentId")%>' />
                                                                </ItemTemplate>


                                                            </asp:TemplateField>

                                                            <%-- END Move/order--%>
                                                            <asp:BoundField DataField="DocumentID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentId" />
                                                            <asp:BoundField DataField="DocumentOrder" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentOrder" />
                                                            <asp:BoundField DataField="DividerID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn dividerId" />
                                                            <asp:BoundField DataField="Name" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn name" />
                                                            <asp:BoundField DataField="PathName" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn pathName" />
                                                            <asp:BoundField DataField="FileExtention" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn fileExtention" />




                                                            <%-- Start Rename--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Rename" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" CommandName="Edit" runat="server" Text=""
                                                                        OnClientClick="showEdit(this);" CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("DocumentId")), Convert.ToInt32(Eval("DocumentOrder")))%>' />
                                                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                                        ToolTip="Update" CssClass="ic-icon ic-save newSave" Style="display: none" name="lnkUpdate"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>

                                                            <%-- Start Rename to convert label into textbox--%>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="DocumentName" runat="server" Text="Preview" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DisplayName")%>' name="DocumentName" Style="cursor: pointer; text-decoration: underline; color: blue"
                                                                        OnClick="showPreview(this);" />
                                                                    <asp:TextBox runat="server" Text='<%# Eval("DisplayName")%>' MaxLength="50"
                                                                        ID="txtDocumentName" Style="display: none; height: 30px;"
                                                                        name="txtDocumentName" CssClass="txtDocumentName" />
                                                                    <asp:HiddenField ID="hdnFileExtension" runat="server" Value='<%# Eval("FileExtention") %>' />
                                                                    <asp:HiddenField ID="hdnFullFileName" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("FullFileName") + "")%>' />
                                                                    <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>
                                                            <asp:BoundField DataField="DOB" HeaderText="Upload Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" ItemStyle-CssClass="dob" ItemStyle-Width="2px" />
                                                            <%-- Start class--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Class" runat="server" Text="Class" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClass" runat="server" CssClass="lblClass" Text='<%#Eval("Class")%>' name="lblClass" />
                                                                    <asp:DropDownList ID="ddlDropClass" name="ddlDropClass" runat="server" Width="60px" CssClass="ddlDropClass" Style="display: none"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END class--%>
                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                            ItemStyle-CssClass="table_tekst_edit">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Index" runat="server" Text="Index" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                               
                                                                  <asp:HyperLink ID="lblIndex" runat="server" CausesValidation="false"  Text='<%#Eval("DisplayName")%>' OnClick="OpenIndexPopup(this);return false;" style="cursor:pointer;"/>
                                                                   <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") +".pdf") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />
                                                                There are no documents.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>

                                        <ajaxToolkit:TabPanel runat="server" ID="Panel10" HeaderText="Divider-1" Visible="false">
                                            <ContentTemplate>
                                                <div class="table-scroll dvGrid" style="max-height: 450px; overflow-y: auto;">
                                                    <asp:GridView ID="GridView10" runat="server" OnRowDataBound="GridView0_RowDataBound"
                                                        DataKeyNames="DocumentID " AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%--  <SelectedRowStyle CssClass="odd" />
                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox Text="" ID="chkSelectAll" CssClass="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label34" Text="File|Indexing|Share" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="LinkButton21" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                                                        NavigateUrl='<%#string.Format("PDFDownloader.aspx?ID={0}", Eval("DocumentID"))%>' />


                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-index" CausesValidation="False"
                                                                        CommandName="" OnClientClick="OpenIndexPopup(this);return false;"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                    <asp:LinkButton ID="lnkShare" runat="server" CssClass="ic-icon ic-share" CausesValidation="False" Height="28px" Width="28px">
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libDelete" runat="server" OnClientClick="return OpenDeleteModal(this);" CssClass="ic-icon ic-delete" CausesValidation="False"></asp:LinkButton>
                                                                    <%-- <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" ConfirmText="Are you sure to delete this document?"
                                                                                        TargetControlID="libDelete">
                                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                    <%--<ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="libDelete">
                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="10px"
                                                            ItemStyle-CssClass="table_tekst_preview">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblPreview" runat="server" Text="Preview" />
                                                            </HeaderTemplate>
                                                            
                                                        </asp:TemplateField>--%>
                                                            <%-- Start Move/order--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="50px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Move|Order|Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <a href="javascript:void(0);" id="showMove" class="ic-icon ic-move editdocs" title="Move" runat="server"></a>
                                                                    <a href="javascript:void(0);" id="showOrder" class="ic-icon ic-order deletedocs" title="Order" runat="server"></a>
                                                                    <asp:Button ID="AddToTab" runat="server" CssClass="icon-btn addtotab-filefolderbox lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event)" />
                                                                    <asp:DropDownList ID="ddlDrpMove" runat="server" Width="150px" CssClass="ddlDropMove" Style="display: none"></asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlDropOrder" runat="server" Width="150px" CssClass="ddlDropOrder" Style="display: none"></asp:DropDownList>
                                                                    <asp:LinkButton ID="lbkSave1" runat="server" CausesValidation="True" CommandName="Save" ToolTip="Save" AutoPostBack="False" CssClass="ic-icon ic-save saveDoc" Style="display: none" OnClientClick="SaveOrder(this);"></asp:LinkButton>
                                                                    <a href="javascript:void(0);" id="cancelOrderMove" class="ic-icon ic-cancel cancelDoc" title="Cancel" style="display: none" runat="server"></a>
                                                                    <asp:HiddenField ID="hdndocId" runat="server" Value='<%# Eval("DocumentId")%>' />
                                                                </ItemTemplate>


                                                            </asp:TemplateField>

                                                            <%-- END Move/order--%>
                                                            <asp:BoundField DataField="DocumentID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentId" />
                                                            <asp:BoundField DataField="DocumentOrder" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentOrder" />
                                                            <asp:BoundField DataField="DividerID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn dividerId" />
                                                            <asp:BoundField DataField="Name" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn name" />
                                                            <asp:BoundField DataField="PathName" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn pathName" />
                                                            <asp:BoundField DataField="FileExtention" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn fileExtention" />




                                                            <%-- Start Rename--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Rename" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" CommandName="Edit" runat="server" Text=""
                                                                        OnClientClick="showEdit(this);" CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("DocumentId")), Convert.ToInt32(Eval("DocumentOrder")))%>' />
                                                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                                        ToolTip="Update" CssClass="ic-icon ic-save newSave" Style="display: none" name="lnkUpdate"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>

                                                            <%-- Start Rename to convert label into textbox--%>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="DocumentName" runat="server" Text="Document Indexing" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DisplayName")%>' name="DocumentName" Style="cursor: pointer; text-decoration: underline; color: blue"
                                                                        OnClick="showPreview(this);" />
                                                                    <asp:TextBox runat="server" Text='<%# Eval("DisplayName")%>' MaxLength="50"
                                                                        ID="txtDocumentName" Style="display: none; height: 30px;"
                                                                        name="txtDocumentName" CssClass="txtDocumentName" />
                                                                    <asp:HiddenField ID="hdnFileExtension" runat="server" Value='<%# Eval("FileExtention") %>' />
                                                                    <asp:HiddenField ID="hdnFullFileName" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("FullFileName") + "")%>' />
                                                                    <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>
                                                            <asp:BoundField DataField="DOB" HeaderText="Upload Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" ItemStyle-CssClass="dob" ItemStyle-Width="2px" />
                                                            <%-- Start class--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Class" runat="server" Text="Class" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClass" runat="server" CssClass="lblClass" Text='<%#Eval("Class")%>' name="lblClass" />
                                                                    <asp:DropDownList ID="ddlDropClass" name="ddlDropClass" runat="server" Width="60px" CssClass="ddlDropClass" Style="display: none"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END class--%>
                                                            <%--  <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                            ItemStyle-CssClass="table_tekst_edit">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Index" runat="server" Text="Index" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                              
                                                                  <asp:HyperLink ID="lblIndex" runat="server" CausesValidation="false"  Text='<%#Eval("DisplayName")%>' OnClick="OpenIndexPopup(this);return false;" style="cursor:pointer;"/>
                                                                   <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") +".pdf") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />
                                                                There are no documents.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>

                                        <ajaxToolkit:TabPanel runat="server" ID="Panel11" HeaderText="Divider-1" Visible="false">
                                            <ContentTemplate>
                                                <div class="table-scroll dvGrid" style="max-height: 450px; overflow-y: auto;">
                                                    <asp:GridView ID="GridView11" runat="server" OnRowDataBound="GridView0_RowDataBound"
                                                        DataKeyNames="DocumentID " AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%-- <SelectedRowStyle CssClass="odd" />
                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox Text="" ID="chkSelectAll" CssClass="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label34" Text="File|Indexing|Share" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="LinkButton21" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                                                        NavigateUrl='<%#string.Format("PDFDownloader.aspx?ID={0}", Eval("DocumentID"))%>' />


                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-index" CausesValidation="False"
                                                                        CommandName="" OnClientClick="OpenIndexPopup(this);return false;"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                    <asp:LinkButton ID="lnkShare" runat="server" CssClass="ic-icon ic-share" CausesValidation="False" Height="28px" Width="28px">
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libDelete" runat="server" OnClientClick="return OpenDeleteModal(this);" CssClass="ic-icon ic-delete" CausesValidation="False"></asp:LinkButton>
                                                                    <%-- <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" ConfirmText="Are you sure to delete this document?"
                                                                                        TargetControlID="libDelete">
                                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                    <%--<ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="libDelete">
                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="10px"
                                                            ItemStyle-CssClass="table_tekst_preview">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblPreview" runat="server" Text="Preview" />
                                                            </HeaderTemplate>
                                                            
                                                        </asp:TemplateField>--%>
                                                            <%-- Start Move/order--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="50px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Move|Order|Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <a href="javascript:void(0);" id="showMove" class="ic-icon ic-move editdocs" title="Move" runat="server"></a>
                                                                    <a href="javascript:void(0);" id="showOrder" class="ic-icon ic-order deletedocs" title="Order" runat="server"></a>
                                                                    <asp:Button ID="AddToTab" runat="server" CssClass="icon-btn addtotab-filefolderbox lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event)" />
                                                                    <asp:DropDownList ID="ddlDrpMove" runat="server" Width="150px" CssClass="ddlDropMove" Style="display: none"></asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlDropOrder" runat="server" Width="150px" CssClass="ddlDropOrder" Style="display: none"></asp:DropDownList>
                                                                    <asp:LinkButton ID="lbkSave1" runat="server" CausesValidation="True" CommandName="Save" ToolTip="Save" AutoPostBack="False" CssClass="ic-icon ic-save saveDoc" Style="display: none" OnClientClick="SaveOrder(this);"></asp:LinkButton>
                                                                    <a href="javascript:void(0);" id="cancelOrderMove" class="ic-icon ic-cancel cancelDoc" title="Cancel" style="display: none" runat="server"></a>
                                                                    <asp:HiddenField ID="hdndocId" runat="server" Value='<%# Eval("DocumentId")%>' />
                                                                </ItemTemplate>


                                                            </asp:TemplateField>

                                                            <%-- END Move/order--%>
                                                            <asp:BoundField DataField="DocumentID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentId" />
                                                            <asp:BoundField DataField="DocumentOrder" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentOrder" />
                                                            <asp:BoundField DataField="DividerID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn dividerId" />
                                                            <asp:BoundField DataField="Name" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn name" />
                                                            <asp:BoundField DataField="PathName" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn pathName" />
                                                            <asp:BoundField DataField="FileExtention" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn fileExtention" />




                                                            <%-- Start Rename--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Rename" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" CommandName="Edit" runat="server" Text=""
                                                                        OnClientClick="showEdit(this);" CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("DocumentId")), Convert.ToInt32(Eval("DocumentOrder")))%>' />
                                                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                                        ToolTip="Update" CssClass="ic-icon ic-save newSave" Style="display: none" name="lnkUpdate"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>

                                                            <%-- Start Rename to convert label into textbox--%>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="DocumentName" runat="server" Text="Document Indexing" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DisplayName")%>' name="DocumentName" Style="cursor: pointer; text-decoration: underline; color: blue"
                                                                        OnClick="showPreview(this);" />
                                                                    <asp:TextBox runat="server" Text='<%# Eval("DisplayName")%>' MaxLength="50"
                                                                        ID="txtDocumentName" Style="display: none; height: 30px;"
                                                                        name="txtDocumentName" CssClass="txtDocumentName" />
                                                                    <asp:HiddenField ID="hdnFileExtension" runat="server" Value='<%# Eval("FileExtention") %>' />
                                                                    <asp:HiddenField ID="hdnFullFileName" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("FullFileName") + "")%>' />
                                                                    <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>
                                                            <asp:BoundField DataField="DOB" HeaderText="Upload Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" ItemStyle-CssClass="dob" ItemStyle-Width="2px" />
                                                            <%-- Start class--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Class" runat="server" Text="Class" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClass" runat="server" CssClass="lblClass" Text='<%#Eval("Class")%>' name="lblClass" />
                                                                    <asp:DropDownList ID="ddlDropClass" name="ddlDropClass" runat="server" Width="60px" CssClass="ddlDropClass" Style="display: none"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END class--%>
                                                            <%--  <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                            ItemStyle-CssClass="table_tekst_edit">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Index" runat="server" Text="Index" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                              
                                                                  <asp:HyperLink ID="lblIndex" runat="server" CausesValidation="false"  Text='<%#Eval("DisplayName")%>' OnClick="OpenIndexPopup(this);return false;" style="cursor:pointer;"/>
                                                                   <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") +".pdf") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />
                                                                There are no documents.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>

                                        <ajaxToolkit:TabPanel runat="server" ID="Panel12" HeaderText="Divider-1" Visible="false">
                                            <ContentTemplate>
                                                <div class="table-scroll dvGrid" style="max-height: 450px; overflow-y: auto;">
                                                    <asp:GridView ID="GridView12" runat="server" OnRowDataBound="GridView0_RowDataBound"
                                                        DataKeyNames="DocumentID " AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%--<SelectedRowStyle CssClass="odd" />
                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox Text="" ID="chkSelectAll" CssClass="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label34" Text="File|Indexing|Share" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="LinkButton21" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                                                        NavigateUrl='<%#string.Format("PDFDownloader.aspx?ID={0}", Eval("DocumentID"))%>' />


                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-index" CausesValidation="False"
                                                                        CommandName="" OnClientClick="OpenIndexPopup(this);return false;"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                    <asp:LinkButton ID="lnkShare" runat="server" CssClass="ic-icon ic-share" CausesValidation="False" Height="28px" Width="28px">
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libDelete" runat="server" OnClientClick="return OpenDeleteModal(this);" CssClass="ic-icon ic-delete" CausesValidation="False"></asp:LinkButton>
                                                                    <%-- <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" ConfirmText="Are you sure to delete this document?"
                                                                                        TargetControlID="libDelete">
                                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                    <%--<ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="libDelete">
                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="10px"
                                                            ItemStyle-CssClass="table_tekst_preview">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblPreview" runat="server" Text="Preview" />
                                                            </HeaderTemplate>
                                                            
                                                        </asp:TemplateField>--%>
                                                            <%-- Start Move/order--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="50px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Move|Order|Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <a href="javascript:void(0);" id="showMove" class="ic-icon ic-move editdocs" title="Move" runat="server"></a>
                                                                    <a href="javascript:void(0);" id="showOrder" class="ic-icon ic-order deletedocs" title="Order" runat="server"></a>
                                                                    <asp:Button ID="AddToTab" runat="server" CssClass="icon-btn addtotab-filefolderbox lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event)" />
                                                                    <asp:DropDownList ID="ddlDrpMove" runat="server" Width="150px" CssClass="ddlDropMove" Style="display: none"></asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlDropOrder" runat="server" Width="150px" CssClass="ddlDropOrder" Style="display: none"></asp:DropDownList>
                                                                    <asp:LinkButton ID="lbkSave1" runat="server" CausesValidation="True" CommandName="Save" ToolTip="Save" AutoPostBack="False" CssClass="ic-icon ic-save saveDoc" Style="display: none" OnClientClick="SaveOrder(this);"></asp:LinkButton>
                                                                    <a href="javascript:void(0);" id="cancelOrderMove" class="ic-icon ic-cancel cancelDoc" title="Cancel" style="display: none" runat="server"></a>
                                                                    <asp:HiddenField ID="hdndocId" runat="server" Value='<%# Eval("DocumentId")%>' />
                                                                </ItemTemplate>


                                                            </asp:TemplateField>

                                                            <%-- END Move/order--%>
                                                            <asp:BoundField DataField="DocumentID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentId" />
                                                            <asp:BoundField DataField="DocumentOrder" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentOrder" />
                                                            <asp:BoundField DataField="DividerID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn dividerId" />
                                                            <asp:BoundField DataField="Name" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn name" />
                                                            <asp:BoundField DataField="PathName" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn pathName" />
                                                            <asp:BoundField DataField="FileExtention" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn fileExtention" />




                                                            <%-- Start Rename--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Rename" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" CommandName="Edit" runat="server" Text=""
                                                                        OnClientClick="showEdit(this);" CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("DocumentId")), Convert.ToInt32(Eval("DocumentOrder")))%>' />
                                                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                                        ToolTip="Update" CssClass="ic-icon ic-save newSave" Style="display: none" name="lnkUpdate"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>

                                                            <%-- Start Rename to convert label into textbox--%>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="DocumentName" runat="server" Text="Document Indexing" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DisplayName")%>' name="DocumentName" Style="cursor: pointer; text-decoration: underline; color: blue"
                                                                        OnClick="showPreview(this);" />
                                                                    <asp:TextBox runat="server" Text='<%# Eval("DisplayName")%>' MaxLength="50"
                                                                        ID="txtDocumentName" Style="display: none; height: 30px;"
                                                                        name="txtDocumentName" CssClass="txtDocumentName" />
                                                                    <asp:HiddenField ID="hdnFileExtension" runat="server" Value='<%# Eval("FileExtention") %>' />
                                                                    <asp:HiddenField ID="hdnFullFileName" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("FullFileName") + "")%>' />
                                                                    <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>
                                                            <asp:BoundField DataField="DOB" HeaderText="Upload Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" ItemStyle-CssClass="dob" ItemStyle-Width="2px" />
                                                            <%-- Start class--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Class" runat="server" Text="Class" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClass" runat="server" CssClass="lblClass" Text='<%#Eval("Class")%>' name="lblClass" />
                                                                    <asp:DropDownList ID="ddlDropClass" name="ddlDropClass" runat="server" Width="60px" CssClass="ddlDropClass" Style="display: none"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END class--%>
                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                            ItemStyle-CssClass="table_tekst_edit">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Index" runat="server" Text="Index" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                              
                                                                  <asp:HyperLink ID="lblIndex" runat="server" CausesValidation="false"  Text='<%#Eval("DisplayName")%>' OnClick="OpenIndexPopup(this);return false;" style="cursor:pointer;"/>
                                                                   <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") +".pdf") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />
                                                                There are no documents.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>

                                        <ajaxToolkit:TabPanel runat="server" ID="Panel13" HeaderText="Divider-1" Visible="false">
                                            <ContentTemplate>
                                                <div class="table-scroll dvGrid" style="max-height: 450px; overflow-y: auto;">
                                                    <asp:GridView ID="GridView13" runat="server" OnRowDataBound="GridView0_RowDataBound"
                                                        DataKeyNames="DocumentID " AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%--<SelectedRowStyle CssClass="odd" />
                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox Text="" ID="chkSelectAll" CssClass="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label34" Text="File|Indexing|Share" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="LinkButton21" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                                                        NavigateUrl='<%#string.Format("PDFDownloader.aspx?ID={0}", Eval("DocumentID"))%>' />


                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-index" CausesValidation="False"
                                                                        CommandName="" OnClientClick="OpenIndexPopup(this);return false;"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                    <asp:LinkButton ID="lnkShare" runat="server" CssClass="ic-icon ic-share" CausesValidation="False" Height="28px" Width="28px">
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libDelete" runat="server" OnClientClick="return OpenDeleteModal(this);" CssClass="ic-icon ic-delete" CausesValidation="False"></asp:LinkButton>
                                                                    <%-- <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" ConfirmText="Are you sure to delete this document?"
                                                                                        TargetControlID="libDelete">
                                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                    <%--<ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="libDelete">
                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="10px"
                                                            ItemStyle-CssClass="table_tekst_preview">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblPreview" runat="server" Text="Preview" />
                                                            </HeaderTemplate>
                                                            
                                                        </asp:TemplateField>--%>
                                                            <%-- Start Move/order--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="50px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Move|Order|Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <a href="javascript:void(0);" id="showMove" class="ic-icon ic-move editdocs" title="Move" runat="server"></a>
                                                                    <a href="javascript:void(0);" id="showOrder" class="ic-icon ic-order deletedocs" title="Order" runat="server"></a>
                                                                    <asp:Button ID="AddToTab" runat="server" CssClass="icon-btn addtotab-filefolderbox lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event)" />
                                                                    <asp:DropDownList ID="ddlDrpMove" runat="server" Width="150px" CssClass="ddlDropMove" Style="display: none"></asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlDropOrder" runat="server" Width="150px" CssClass="ddlDropOrder" Style="display: none"></asp:DropDownList>
                                                                    <asp:LinkButton ID="lbkSave1" runat="server" CausesValidation="True" CommandName="Save" ToolTip="Save" AutoPostBack="False" CssClass="ic-icon ic-save saveDoc" Style="display: none" OnClientClick="SaveOrder(this);"></asp:LinkButton>
                                                                    <a href="javascript:void(0);" id="cancelOrderMove" class="ic-icon ic-cancel cancelDoc" title="Cancel" style="display: none" runat="server"></a>
                                                                    <asp:HiddenField ID="hdndocId" runat="server" Value='<%# Eval("DocumentId")%>' />
                                                                </ItemTemplate>


                                                            </asp:TemplateField>

                                                            <%-- END Move/order--%>
                                                            <asp:BoundField DataField="DocumentID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentId" />
                                                            <asp:BoundField DataField="DocumentOrder" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentOrder" />
                                                            <asp:BoundField DataField="DividerID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn dividerId" />
                                                            <asp:BoundField DataField="Name" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn name" />
                                                            <asp:BoundField DataField="PathName" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn pathName" />
                                                            <asp:BoundField DataField="FileExtention" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn fileExtention" />




                                                            <%-- Start Rename--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Rename" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" CommandName="Edit" runat="server" Text=""
                                                                        OnClientClick="showEdit(this);" CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("DocumentId")), Convert.ToInt32(Eval("DocumentOrder")))%>' />
                                                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                                        ToolTip="Update" CssClass="ic-icon ic-save newSave" Style="display: none" name="lnkUpdate"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>

                                                            <%-- Start Rename to convert label into textbox--%>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="DocumentName" runat="server" Text="Document Indexing" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DisplayName")%>' name="DocumentName" Style="cursor: pointer; text-decoration: underline; color: blue"
                                                                        OnClick="showPreview(this);" />
                                                                    <asp:TextBox runat="server" Text='<%# Eval("DisplayName")%>' MaxLength="50"
                                                                        ID="txtDocumentName" Style="display: none; height: 30px;"
                                                                        name="txtDocumentName" CssClass="txtDocumentName" />
                                                                    <asp:HiddenField ID="hdnFileExtension" runat="server" Value='<%# Eval("FileExtention") %>' />
                                                                    <asp:HiddenField ID="hdnFullFileName" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("FullFileName") + "")%>' />
                                                                    <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>
                                                            <asp:BoundField DataField="DOB" HeaderText="Upload Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" ItemStyle-CssClass="dob" ItemStyle-Width="2px" />
                                                            <%-- Start class--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Class" runat="server" Text="Class" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClass" runat="server" CssClass="lblClass" Text='<%#Eval("Class")%>' name="lblClass" />
                                                                    <asp:DropDownList ID="ddlDropClass" name="ddlDropClass" runat="server" Width="60px" CssClass="ddlDropClass" Style="display: none"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END class--%>
                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                            ItemStyle-CssClass="table_tekst_edit">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Index" runat="server" Text="Index" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                         
                                                                  <asp:HyperLink ID="lblIndex" runat="server" CausesValidation="false"  Text='<%#Eval("DisplayName")%>' OnClick="OpenIndexPopup(this);return false;" style="cursor:pointer;"/>
                                                                   <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") +".pdf") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />
                                                                There are no documents.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>

                                        <ajaxToolkit:TabPanel runat="server" ID="Panel14" HeaderText="Divider-1" Visible="false">
                                            <ContentTemplate>
                                                <div class="table-scroll dvGrid" style="max-height: 450px; overflow-y: auto;">
                                                    <asp:GridView ID="GridView14" runat="server" OnRowDataBound="GridView0_RowDataBound"
                                                        DataKeyNames="DocumentID " AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%--<SelectedRowStyle CssClass="odd" />
                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox Text="" ID="chkSelectAll" CssClass="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label34" Text="File|Indexing|Share" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="LinkButton21" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                                                        NavigateUrl='<%#string.Format("PDFDownloader.aspx?ID={0}", Eval("DocumentID"))%>' />


                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-index" CausesValidation="False"
                                                                        CommandName="" OnClientClick="OpenIndexPopup(this);return false;"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                    <asp:LinkButton ID="lnkShare" runat="server" CssClass="ic-icon ic-share" CausesValidation="False" Height="28px" Width="28px">
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libDelete" runat="server" OnClientClick="return OpenDeleteModal(this);" CssClass="ic-icon ic-delete" CausesValidation="False"></asp:LinkButton>
                                                                    <%-- <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" ConfirmText="Are you sure to delete this document?"
                                                                                        TargetControlID="libDelete">
                                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                    <%--<ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="libDelete">
                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="10px"
                                                            ItemStyle-CssClass="table_tekst_preview">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblPreview" runat="server" Text="Preview" />
                                                            </HeaderTemplate>
                                                            
                                                        </asp:TemplateField>--%>
                                                            <%-- Start Move/order--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="50px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Move|Order|Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <a href="javascript:void(0);" id="showMove" class="ic-icon ic-move editdocs" title="Move" runat="server"></a>
                                                                    <a href="javascript:void(0);" id="showOrder" class="ic-icon ic-order deletedocs" title="Order" runat="server"></a>
                                                                    <asp:Button ID="AddToTab" runat="server" CssClass="icon-btn addtotab-filefolderbox lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event)" />
                                                                    <asp:DropDownList ID="ddlDrpMove" runat="server" Width="150px" CssClass="ddlDropMove" Style="display: none"></asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlDropOrder" runat="server" Width="150px" CssClass="ddlDropOrder" Style="display: none"></asp:DropDownList>
                                                                    <asp:LinkButton ID="lbkSave1" runat="server" CausesValidation="True" CommandName="Save" ToolTip="Save" AutoPostBack="False" CssClass="ic-icon ic-save saveDoc" Style="display: none" OnClientClick="SaveOrder(this);"></asp:LinkButton>
                                                                    <a href="javascript:void(0);" id="cancelOrderMove" class="ic-icon ic-cancel cancelDoc" title="Cancel" style="display: none" runat="server"></a>
                                                                    <asp:HiddenField ID="hdndocId" runat="server" Value='<%# Eval("DocumentId")%>' />
                                                                </ItemTemplate>


                                                            </asp:TemplateField>

                                                            <%-- END Move/order--%>
                                                            <asp:BoundField DataField="DocumentID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentId" />
                                                            <asp:BoundField DataField="DocumentOrder" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentOrder" />
                                                            <asp:BoundField DataField="DividerID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn dividerId" />
                                                            <asp:BoundField DataField="Name" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn name" />
                                                            <asp:BoundField DataField="PathName" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn pathName" />
                                                            <asp:BoundField DataField="FileExtention" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn fileExtention" />




                                                            <%-- Start Rename--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Rename" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" CommandName="Edit" runat="server" Text=""
                                                                        OnClientClick="showEdit(this);" CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("DocumentId")), Convert.ToInt32(Eval("DocumentOrder")))%>' />
                                                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                                        ToolTip="Update" CssClass="ic-icon ic-save newSave" Style="display: none" name="lnkUpdate"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>

                                                            <%-- Start Rename to convert label into textbox--%>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="DocumentName" runat="server" Text="Preview" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DisplayName")%>' name="DocumentName" Style="cursor: pointer; text-decoration: underline; color: blue"
                                                                        OnClick="showPreview(this);" />
                                                                    <asp:TextBox runat="server" Text='<%# Eval("DisplayName")%>' MaxLength="50"
                                                                        ID="txtDocumentName" Style="display: none; height: 30px;"
                                                                        name="txtDocumentName" CssClass="txtDocumentName" />
                                                                    <asp:HiddenField ID="hdnFileExtension" runat="server" Value='<%# Eval("FileExtention") %>' />
                                                                    <asp:HiddenField ID="hdnFullFileName" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("FullFileName") + "")%>' />
                                                                    <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>
                                                            <asp:BoundField DataField="DOB" HeaderText="Upload Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" ItemStyle-CssClass="dob" ItemStyle-Width="2px" />
                                                            <%-- Start class--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Class" runat="server" Text="Class" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClass" runat="server" CssClass="lblClass" Text='<%#Eval("Class")%>' name="lblClass" />
                                                                    <asp:DropDownList ID="ddlDropClass" name="ddlDropClass" runat="server" Width="60px" CssClass="ddlDropClass" Style="display: none"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END class--%>
                                                            <%--<asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                            ItemStyle-CssClass="table_tekst_edit">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Index" runat="server" Text="Index" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                             
                                                                  <asp:HyperLink ID="lblIndex" runat="server" CausesValidation="false"  Text='<%#Eval("DisplayName")%>' OnClick="OpenIndexPopup(this);return false;" style="cursor:pointer;"/>
                                                                   <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") +".pdf") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />
                                                                There are no documents.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>

                                        <ajaxToolkit:TabPanel runat="server" ID="Panel15" HeaderText="Divider-1" Visible="false">
                                            <ContentTemplate>
                                                <div class="table-scroll dvGrid" style="max-height: 450px; overflow-y: auto;">
                                                    <asp:GridView ID="GridView15" runat="server" OnRowDataBound="GridView0_RowDataBound"
                                                        DataKeyNames="DocumentID " AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%--<SelectedRowStyle CssClass="odd" />
                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:CheckBox Text="" ID="chkSelectAll" CssClass="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label34" Text="File|Indexing|Share" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="LinkButton21" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                                                        NavigateUrl='<%#string.Format("PDFDownloader.aspx?ID={0}", Eval("DocumentID"))%>' />


                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-index" CausesValidation="False"
                                                                        CommandName="" OnClientClick="OpenIndexPopup(this);return false;"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                    <asp:LinkButton ID="lnkShare" runat="server" CssClass="ic-icon ic-share" CausesValidation="False" Height="28px" Width="28px">
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libDelete" runat="server" OnClientClick="return OpenDeleteModal(this);" CssClass="ic-icon ic-delete" CausesValidation="False"></asp:LinkButton>
                                                                    <%-- <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" ConfirmText="Are you sure to delete this document?"
                                                                                        TargetControlID="libDelete">
                                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                    <%--<ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="libDelete">
                                                                    </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="10px"
                                                            ItemStyle-CssClass="table_tekst_preview">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="lblPreview" runat="server" Text="Preview" />
                                                            </HeaderTemplate>
                                                            
                                                        </asp:TemplateField>--%>
                                                            <%-- Start Move/order--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="50px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Move|Order|Tab" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <a href="javascript:void(0);" id="showMove" class="ic-icon ic-move editdocs" title="Move" runat="server"></a>
                                                                    <a href="javascript:void(0);" id="showOrder" class="ic-icon ic-order deletedocs" title="Order" runat="server"></a>
                                                                    <asp:Button ID="AddToTab" runat="server" class="icon-btn addtotab-filefolderbox lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event)" />
                                                                    <asp:DropDownList ID="ddlDrpMove" runat="server" Width="150px" CssClass="ddlDropMove" Style="display: none"></asp:DropDownList>
                                                                    <asp:DropDownList ID="ddlDropOrder" runat="server" Width="150px" CssClass="ddlDropOrder" Style="display: none"></asp:DropDownList>
                                                                    <asp:LinkButton ID="lbkSave1" runat="server" CausesValidation="True" CommandName="Save" ToolTip="Save" AutoPostBack="False" CssClass="ic-icon ic-save saveDoc" Style="display: none" OnClientClick="SaveOrder(this);"></asp:LinkButton>
                                                                    <a href="javascript:void(0);" id="cancelOrderMove" class="ic-icon ic-cancel cancelDoc" title="Cancel" style="display: none" runat="server"></a>
                                                                    <asp:HiddenField ID="hdndocId" runat="server" Value='<%# Eval("DocumentId")%>' />
                                                                </ItemTemplate>


                                                            </asp:TemplateField>

                                                            <%-- END Move/order--%>
                                                            <asp:BoundField DataField="DocumentID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentId" />
                                                            <asp:BoundField DataField="DocumentOrder" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn documentOrder" />
                                                            <asp:BoundField DataField="DividerID" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn dividerId" />
                                                            <asp:BoundField DataField="Name" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn name" />
                                                            <asp:BoundField DataField="PathName" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn pathName" />
                                                            <asp:BoundField DataField="FileExtention" HeaderText="ID" HeaderStyle-CssClass="hideGridColumn" ItemStyle-CssClass="hideGridColumn fileExtention" />




                                                            <%-- Start Rename--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Rename" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkEdit" CommandName="Edit" runat="server" Text=""
                                                                        OnClientClick="showEdit(this);" CssClass="ic-icon ic-edit edit" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("DocumentId")), Convert.ToInt32(Eval("DocumentOrder")))%>' />
                                                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                                        ToolTip="Update" CssClass="ic-icon ic-save newSave" Style="display: none" name="lnkUpdate"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancel" Style="display: none" name="lnkcancel"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>

                                                            <%-- Start Rename to convert label into textbox--%>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="DocumentName" runat="server" Text="Preview" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DisplayName")%>' name="DocumentName" Style="cursor: pointer; text-decoration: underline; color: blue"
                                                                        OnClick="showPreview(this);" />
                                                                    <asp:TextBox runat="server" Text='<%# Eval("DisplayName")%>' MaxLength="50"
                                                                        ID="txtDocumentName" Style="display: none; height: 30px;"
                                                                        name="txtDocumentName" CssClass="txtDocumentName" />
                                                                    <asp:HiddenField ID="hdnFileExtension" runat="server" Value='<%# Eval("FileExtention") %>' />
                                                                    <asp:HiddenField ID="hdnFullFileName" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("FullFileName") + "")%>' />
                                                                    <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") + ".pdf")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END Rename--%>
                                                            <asp:BoundField DataField="DOB" HeaderText="Upload Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" ItemStyle-CssClass="dob" ItemStyle-Width="2px" />
                                                            <%-- Start class--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Class" runat="server" Text="Class" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblClass" runat="server" CssClass="lblClass" Text='<%#Eval("Class")%>' name="lblClass" />
                                                                    <asp:DropDownList ID="ddlDropClass" name="ddlDropClass" runat="server" Width="60px" CssClass="ddlDropClass" Style="display: none"></asp:DropDownList>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%-- END class--%>
                                                            <%-- <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5px" ItemStyle-Wrap="false"
                                                            ItemStyle-CssClass="table_tekst_edit">
                                                            <HeaderTemplate>
                                                                <asp:Label ID="Index" runat="server" Text="Index" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                              
                                                                  <asp:HyperLink ID="lblIndex" runat="server" CausesValidation="false"  Text='<%#Eval("DisplayName")%>' OnClick="OpenIndexPopup(this);return false;" style="cursor:pointer;"/>
                                                                   <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("Name") +".pdf") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />
                                                                There are no documents.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                        </ajaxToolkit:TabPanel>

                                    </ajaxToolkit:TabContainer>
                                    <asp:GridView ID="gridViewOcr" Visible="false" runat="server" OnRowDataBound="gridViewOcr_RowDataBound" OnSorting="gridViewOcr_Sorting"
                                        AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable docTable">
                                        <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                        <AlternatingRowStyle CssClass="odd" />
                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                ItemStyle-CssClass="table_tekst_edit">
                                                <HeaderTemplate>
                                                    <asp:CheckBox Text="" ID="chkSelectAll" CssClass="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemStyle CssClass="alignCenter" />
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblSearched" runat="server" Text="File Name" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="serchedName" Text='<%# Eval("Name")%>'></asp:Label>
                                                    <br />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemStyle CssClass="alignCenter" />
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblRecords" runat="server" Text="Records" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="serchedData" CssClass="scrollable" Text='<%# Eval("JsonData")%>'></asp:Label>
                                                    <br />
                                                    <asp:Label ID="lblAll" CssClass="lnkExpand" runat="server">Show All</asp:Label>
                                                    <asp:Label ID="Label1" CssClass="lnkCollapse" runat="server">Show Less</asp:Label>
                                                    <asp:Button runat="server" ID="btnNext" OnClientClick="return navigateNext(this);" CssClass="btnNav btnNextEnabled" />
                                                    <asp:Button runat="server" ID="btnPrev" OnClientClick="return navigatePrev(this);" CssClass="btnNav btnPrevDisabled" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Size" HeaderText="Size" ItemStyle-Width="10%" />
                                            <asp:TemplateField ShowHeader="true">
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Preview|Download|Share" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-preview" CausesValidation="False" ToolTip="Preview"
                                                        CommandName="" OnClientClick="ShowPreviewForDocument(this, false);return false;"></asp:LinkButton>
                                                    <asp:HyperLink ID="HyperLink1" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                                        NavigateUrl='<%#string.Format("PDFDownloader.aspx?ID={0}", Eval("DocumentId"))%>' />
                                                    <asp:LinkButton ID="lnkShare" runat="server" CssClass="icon-btn ic-icon ic-share" Style="border: none; background-color: white; margin-left: 0px; float: none"
                                                        ToolTip="Share File" />
                                                    <asp:HiddenField runat="server" ID="hdnFolderId" Value='<%# Eval("FolderId")%>' />
                                                    <asp:HiddenField runat="server" ID="hdnDividerId" Value='<%# Eval("DividerId")%>' />
                                                    <asp:HiddenField runat="server" ID="hdnDocId" Value='<%# Eval("DocumentId")%>' />
                                                    <asp:HiddenField runat="server" ID="hdnPathName" Value='<%# Eval("Path") + "/" + GetFileName(Eval("DisplayName") + ".pdf")%>' />
                                                    <asp:HiddenField runat="server" ID="hdnFileName" Value='<%#Eval("DisplayName")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <asp:Label ID="Label3" runat="server" CssClass="Empty" />There is no record for searched keyword.
                                       
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    <input type="button" class="create-btn btn-small green btnScandocBottom" id="btnScanDocBottom" name="btnScandoc" runat="server" causesvalidation="false" value="Scan" onclick="ScanDocFile();" />

                                    <asp:Button class="create-btn btn-small green" Style="margin: 10px 0 10px; float: right; width: 110px; font-size: 12px;"
                                        runat="server" name="download" ID="downloadall" Text="Download All PDF" OnClick="downloadall_Click" />

                                    <asp:Button ID="Button2" Text="Tab" CssClass="ic-icon ic-share sharefolder" runat="server" OnClientClick="return ShareFolder(true);" Style="float: right; background-size: 33px; border: none; margin: 4px 0px 10px; cursor: pointer;"
                                        ToolTip="Tab Share"></asp:Button>
                                    <asp:Button class="create-btn btn green form-submit-btn" Style="margin-left: 42%; width: 15%"
                                        runat="server" name="imageField" ID="imageField" OnClick="imageField_Click" Visible="false" Text="Submit" />
                                    <div>
                                        <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                                    </div>
                            </div>

                            <div class="listing-view content-box" style="margin-top: 20px; margin-bottom: 10px; border: solid 1px #b8b8b8;">
                                <%--<fieldset>--%><div style="padding: 10px; background-color: #e9e9e9; margin-top: -40px; margin-left: -30px; margin-right: -30px;">
                                    <label for="Folder Log" style="margin: 0 0 10px; font-weight: bolder; font-size: 20px; margin-top: -25px; display: initial">
                                        Folder Log</label>
                                    <%--  <asp:Button class="create-btn btn-small green" Style="margin: 0 0 10px;" runat="server" name="download" ID="Button1" Text="Folder Log" OnClientClick="return check();" OnClick="downloadall_Click" />--%>
                                    <%--</fieldset>--%>
                                </div>
                                <div id="gvFolderLog" style="margin-top: 20px;"></div>
                            </div>

                            <%--<div class="content-title" style="margin-top: 30px; padding-top: 30px;">Folder Details</div>--%>
                            <div class="content-box listing-view" id="file-request-inner" style="margin-top: 20px;">
                                <div style="padding: 10px; background-color: #e9e9e9; margin-top: -40px; margin-left: -30px; margin-right: -30px;">
                                    <label for="Folder Details" style="margin: 0 0 10px; font-weight: bold !important; font-size: 20px;">
                                        Folder Details</label>
                                </div>
                                <div style="margin-top: 20px;">
                                    <fieldset style="">
                                        <table>
                                            <tr>
                                                <td align="left">
                                                    <label>
                                                        Folder Name</label>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="textFoldeName" name="textFoldeName" ReadOnly="true" runat="server" size="40" MaxLength="30" Style="margin-left: -97px;" />
                                                </td>

                                                <td style="padding-left: 15px;">
                                                    <asp:LinkButton ID="LinkButton" runat="server" Text="" CssClass="ic-icon ic-view" CausesValidation="false"
                                                        OnCommand="LinkButton2_Click" Style="margin-top: 5px; display: none;" />
                                                </td>

                                                <td style="padding-left: 15px;">
                                                    <asp:LinkButton ID="LinkButton2" runat="server" Text="" CssClass="ic-icon ic-view" CausesValidation="false"
                                                        Style="margin-top: 5px; display: none;" />
                                                </td>


                                                <td>
                                                    <asp:LinkButton ID="LinkButton12" runat="server" Text="" CssClass="ic-icon ic-document htmlViewerFileNew" OnClientClick="return false;" CausesValidation="false"
                                                        OnCommand="LinkButton2_Click" />
                                                </td>

                                                <td>
                                                    <asp:Button ID="folderEditButton" runat="server" CssClass="btn green" Text="Edit" OnClick="folderEditButton_OnClick"
                                                        Style="margin-left: 15px; width: 80px !important; height: 40px !important;" />
                                                </td>
                                                <td>
                                                    <asp:Button ID="folderArchiveButton" runat="server" CssClass="btn green" Text="Archive" OnClick="folderkArchiveButton_OnClick"
                                                        Style="margin-left: 15px; width: 80px !important; height: 40px !important;" />
                                                </td>

                                                <ajaxToolkit:ModalPopupExtender ID="lnkArchive_ModalPopupExtender" runat="server"
                                                    CancelControlID="Button2ArchiveClose" OkControlID="Button1ArchiveOK" TargetControlID="folderArchiveButton"
                                                    PopupControlID="DivArchiveConfirmation" BackgroundCssClass="ModalPopupBG">
                                                </ajaxToolkit:ModalPopupExtender>
                                                <ajaxToolkit:ConfirmButtonExtender ID="cbeArchiveEFileFolder" runat="server" DisplayModalPopupID="lnkArchive_ModalPopupExtender"
                                                    ConfirmText="" TargetControlID="folderArchiveButton"></ajaxToolkit:ConfirmButtonExtender>
                                            </tr>

                                        </table>
                                    </fieldset>


                                    <div class="multiple-fieldset">
                                        <table class="fieldset-datatable reminder_table" width="100%" border="0" cellpadding="2" cellspacing="0">
                                            <tr>
                                                <td align="left">
                                                    <fieldset>
                                                        <table>
                                                            <tr style="font-size: 15px;">
                                                                <td align="left">
                                                                    <label>First Name</label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textFirstName" name="textFirstName" ReadOnly="true" runat="server" size="30" MaxLength="50" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                                <td>
                                                    <fieldset>
                                                        <table>
                                                            <tr style="font-size: 15px;">
                                                                <td>
                                                                    <label>Last Name</label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textLastName" name="textLastName" ReadOnly="true" runat="server" size="30" MaxLength="50" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <fieldset>
                                                        <table>
                                                            <tr style="font-size: 15px;">
                                                                <td align="left">
                                                                    <label>File Number</label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textBoxFileNumber" name="textBoxFileNumber" ReadOnly="true" runat="server"
                                                                        size="50" MaxLength="20" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                                <td>
                                                    <fieldset>
                                                        <table>
                                                            <tr style="font-size: 15px;">
                                                                <td>
                                                                    <label>Email</label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textBoxEmail" name="textBoxEmail" ReadOnly="true" runat="server"
                                                                        size="50" MaxLength="50" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <fieldset>
                                                        <table>
                                                            <tr style="font-size: 15px;">
                                                                <td align="left">
                                                                    <label>Telephone</label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textBoxTel" name="textBoxTel" ReadOnly="true" runat="server"
                                                                        size="50" MaxLength="50" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                                <td>
                                                    <fieldset>
                                                        <table>
                                                            <tr style="font-size: 15px;">
                                                                <td>
                                                                    <label>SSN</label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textBoxFax" name="textBoxFax" ReadOnly="true" runat="server"
                                                                        size="50" MaxLength="50" />
                                                                    <asp:RegularExpressionValidator ID="faxV" ControlToValidate="textBoxFax" runat="server"
                                                                        ErrorMessage="It is not a FAX format." ValidationExpression="^(\d{3}-?\d{2}-?\d{4}|XXX-XX-XXXX)$" Display="None"></asp:RegularExpressionValidator>
                                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8"
                                                                        TargetControlID="faxV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <fieldset>
                                                        <table>
                                                            <tr style="font-size: 15px;">
                                                                <td align="left">
                                                                    <label>Alert1</label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textBoxAlert1" name="textBoxAlert1" ReadOnly="true" runat="server"
                                                                        size="50" MaxLength="50" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                                <td>
                                                    <fieldset>
                                                        <table>
                                                            <tr style="font-size: 15px;">
                                                                <td>
                                                                    <label>Status</label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textBoxAlert2" name="textBoxAlert2" ReadOnly="true" runat="server"
                                                                        size="50" MaxLength="50" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <fieldset>
                                                        <table>
                                                            <tr style="font-size: 15px;">
                                                                <td>
                                                                    <label>Address</label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textBoxAddress" name="textBoxAddress" ReadOnly="true" runat="server"
                                                                        size="50" MaxLength="250" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                                <td>
                                                    <fieldset>
                                                        <table>
                                                            <tr style="font-size: 15px;">
                                                                <td align="left">
                                                                    <label>
                                                                        City
                                                                   
                                                                    </label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textBoxCity" name="textBoxCity" ReadOnly="true" runat="server"
                                                                        size="50" MaxLength="50" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <fieldset>
                                                        <table>
                                                            <tr style="font-size: 15px;">
                                                                <td align="left">
                                                                    <label>State</label>
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="drpState" runat="server" Enabled="False" Style="width: 180px;" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                                <td>
                                                    <fieldset>
                                                        <table>
                                                            <tr style="font-size: 15px;">
                                                                <td align="left">
                                                                    <label>Zip/Postal</label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textBoxZipCode" name="textBoxZipCode" ReadOnly="true" runat="server"
                                                                        size="30" MaxLength="10" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                                <%--<td>
                                                    <fieldset>
                                                        <table>
                                                            <tr style="font-size: 15px;">
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>--%>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <fieldset>
                                                        <table>
                                                            <tr style="font-size: 15px;">
                                                                <td align="left">
                                                                    <label>Site</label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox name="textBoxSite" ReadOnly="true" runat="server" ID="textBoxSite"
                                                                        size="15" MaxLength="50" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                                <td>
                                                    <fieldset>
                                                        <table>
                                                            <tr style="font-size: 15px;">
                                                                <td align="left">
                                                                    <label>Comments</label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="textBoxComments" name="textBoxComments" ReadOnly="true" runat="server"
                                                                        size="123" MaxLength="50" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    <fieldset>
                                                        <table>
                                                            <tr style="font-size: 15px;">
                                                                <td align="left">
                                                                    <label>DOB</label>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox name="textfield5" ReadOnly="true" runat="server" ID="textfield5" Placeholder="mm-dd-yyyy"
                                                                        size="15" MaxLength="50" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </fieldset>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                            </div>



                            <%--<div class="content-title" style="margin-top: 15px;">Reminders </div>--%>
                            <div class="content-box listing-view" style="margin-top: 30px;">
                                <div style="padding: 10px; background-color: #e9e9e9; margin-top: -40px; margin-left: -30px; margin-right: -30px;">
                                    <label for="Reminders" style="margin: 0 0 10px; font-weight: bolder; font-size: 20px;">
                                        Reminders</label>
                                </div>
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <fieldset>
                                            <%--<div class='clearfix'></div>--%>

                                            <div class="multiple-fieldset" style="margin-top: 20px;">
                                                <table class="fieldset-datatable reminder_table" width="100%" border="0" cellpadding="2" cellspacing="0">
                                                    <tr>
                                                        <td class=" " colspan="4">Below, you can create upto 5 date reminders for each folder.  Name the reminder in the space and add the date you would like to be reminded.
                                                                            
                                                        </td>
                                                    </tr>
                                                    <tr style="font-size: 15px;">
                                                        <td width="15%" align="left" class=" ">Reminder 1
                                                        </td>
                                                        <td width="35%">
                                                            <asp:TextBox ID="textfieldName1" runat="server" />
                                                        </td>
                                                        <td align="left" class=" " width="15%" style="padding-left: 5px">Remind Date
                                                        </td>
                                                        <td class=" ">
                                                            <asp:TextBox ID="textRequestDate1" name="textRequestDate1"
                                                                runat="server" size="35" />
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" PopupPosition="Right" runat="server"
                                                                TargetControlID="textRequestDate1" Format="MM-dd-yyyy"></ajaxToolkit:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                    <tr style="font-size: 15px;">
                                                        <td width="15%" align="left" class=" ">Reminder 2
                                                        </td>
                                                        <td width="35%">
                                                            <asp:TextBox ID="textfieldName2" runat="server" />
                                                        </td>
                                                        <td align="left" class=" " width="15%" style="padding-left: 5px">Remind Date
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="textRequestDate2" name="textRequestDate2"
                                                                runat="server" size="35" />
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" PopupPosition="Right" runat="server"
                                                                TargetControlID="textRequestDate2" Format="MM-dd-yyyy"></ajaxToolkit:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                    <tr style="font-size: 15px;">
                                                        <td width="15%" align="left" class=" ">Reminder 3
                                                        </td>
                                                        <td width="35%">
                                                            <asp:TextBox ID="textfieldName3" runat="server" />
                                                        </td>
                                                        <td align="left" class=" " width="15%" style="padding-left: 5px">Remind Date
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="textRequestDate3" name="textRequestDate3"
                                                                runat="server" size="35" />
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender3" PopupPosition="Right" runat="server"
                                                                TargetControlID="textRequestDate3" Format="MM-dd-yyyy"></ajaxToolkit:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                    <tr style="font-size: 15px;">
                                                        <td width="15%" align="left" class=" ">Reminder 4
                                                        </td>
                                                        <td width="35%">
                                                            <asp:TextBox ID="textfieldName4" runat="server" />
                                                        </td>
                                                        <td align="left" class=" " width="15%" style="padding-left: 5px">Remind Date
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="textRequestDate4" name="textRequestDate4"
                                                                runat="server" size="35" />
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender4" PopupPosition="Right" runat="server"
                                                                TargetControlID="textRequestDate4" Format="MM-dd-yyyy"></ajaxToolkit:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                    <tr style="font-size: 15px;">
                                                        <td width="15%" align="left" class=" ">Reminder 5
                                                        </td>
                                                        <td width="35%">
                                                            <asp:TextBox ID="textfieldName5" runat="server" />
                                                        </td>
                                                        <td align="left" class=" " width="15%" style="padding-left: 5px">Remind Date
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="textRequestDate5" name="textRequestDate5"
                                                                runat="server" size="35" />
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender5" PopupPosition="Right" runat="server"
                                                                TargetControlID="textRequestDate5" Format="MM-dd-yyyy"></ajaxToolkit:CalendarExtender>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class=" " colspan="4" style="text-align: center;">
                                                            <asp:Button ID="saveReminderButton" class="create-btn btn-small green" runat="server" Width="80" Text="Save" OnClick="saveReminderButton_OnClick" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>

                                        </fieldset>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="saveReminderButton" EventName="Click" />
                                        <asp:PostBackTrigger ControlID="btnUpload" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <%--<div class="right-content">
                            <div class="quick-find">
                                <uc1:QuickFind ID="QuickFind1" runat="server" />
                            </div>
                             <div class="quick-find" style="margin-top: 15px;">
                                <div class="find-inputbox">
                                    <uc2:FileRequestControl ID="fileRequest1" runat="server" />
                                </div>
                            </div>
                        </div>--%>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="imageField" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="downloadall" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>


    <div class="popup-mainbox preview-popup" id="Div2" style="display: none">
        <div class="popupcontrols" style="width: 100%; height: 5%;">
            <span id="Span2" class="ic-icon ic-close popupclose"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%;">
            <iframe id="Iframe1" style="width: 100%; height: 100%;"></iframe>
        </div>
    </div>

    <div class="popup-mainbox preview-popup" id="AddToFolderLog_popup" style="display: none">
        <div class="popupcontrols popup-head" style="width: 100%; height: 60px;">
            Add Folder Log
       
            <span id="Span1" class="ic-icon ic-close popupclose1"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%;">
            <div class="popup-scroller">

                <div>
                    <table border="0" width="100%" id="Table1" style="margin: 0">
                        <tr>
                            <td>
                                <asp:Label ID="lblFailFolderLog" runat="server" Style="color: red; font-weight: bolder; margin-left: 320px; display: none;"></asp:Label>
                            </td>
                        </tr>

                    </table>
                </div>

                <p style="margin: 35px;">
                    <asp:Label ID="Label3" runat="server" Style="font-size: 15px; font-weight: 500; color: black; margin-left: 63px;">Name:   </asp:Label>
                    <asp:TextBox ID="TextBox1" CssClass="Log" runat="server" Style="width: 200px;"></asp:TextBox>
                    <%--  <uc1:ucaddtotab ID="ucAddToTab" runat="server" />--%>
                </p>

                <div style="margin-left: 37px;">
                    <asp:Label ID="lblFolderName1" runat="server" Style="font-size: 15px; font-weight: 500; color: black; margin-left: 63px;"
                        CssClass="FolderName">Folder: </asp:Label>
                    <asp:DropDownList ID="ddlFolderName" runat="server" Style="overflow: scroll; width: 200px; height: 30px;"
                        CssClass="FolderName">
                    </asp:DropDownList>

                    <asp:Label ID="lblFolderLogName" runat="server" Style="font-size: 15px; font-weight: 500; color: black; margin-left: 63px; display: none;"
                        CssClass="FolderLogName">Folder Log: </asp:Label>
                    <asp:DropDownList ID="ddlFolderLog" runat="server" Style="overflow: scroll; width: 200px; height: 30px; display: none;"
                        CssClass="FolderLogName">
                    </asp:DropDownList>
                </div>

                <div style="padding: 10px 0px;" id="divQuestion" runat="server">
                    <table border="0" width="100%" id="tblBRD" style="margin: 0;">
                        <tr class="tblBRD" style="margin-left: 10px;">
                            <td width="5%" style="padding-top: 5px; padding-left: 80px;">Question</td>
                            <td>
                                <%--  <asp:HiddenField ID="hdnQuestion" runat="server" />--%>
                                <input type="text" id="tbxQuestion1" name="tbxQuestion1" class="question" style="padding: 0 5px; height: 30px; width: 100%" />
                            </td>
                            <td style="padding-top: 7px">
                                <a href="javascript:;" id="btnADDBRD">
                                    <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/add-icon.png" class="btnAdd"
                                        alt="Add" title="Add More" /></a>
                            </td>
                        </tr>
                    </table>
                </div>

            </div>
            <div class="popup-btn">
                <%--<input  runat="server"  type="button" value="Save"  />--%>
                <asp:Button ID="btnAddFolderlog" Text="Save" runat="server" CssClass="btn green FolderLog" Style="margin-right: 5px;" />
                <input id="btnCancelAddToTab" type="button" value="Cancel" class="btn green CancelAddToTab" />
            </div>
        </div>
    </div>
    <div class="popup-mainbox preview-popup" id="preview_popup" style="display: none">
        <div class="popupcontrols" style="width: 100%; height: 5%;">
            <span id="popupclose" class="ic-icon ic-close"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%;">
            <div class="leftPreview">
                <table style="width: 100%; height: 100%">
                    <tr style="height: 5%">
                        <td style="padding-bottom: 10px;">
                            <input type="button" value="Redacting" class="btn green btnStartRedact btnRedact" onclick="startRedacting()" id="startRedact" />
                            <input type="button" value="Discard Changes" class="btn green btnStopRedact btnRedact" onclick="stopRedacting()" style="display: none" id="stopRedact" />
                            <div class="selecttabcheck"><span class="selectalltab">Select All</span><input type="checkbox" id="btnCheckedAll" value="Select All" onclick="checkAllThumbnail();" /></div>
                            <input type="button" id="btnRedact" runat="server" style="display: none;" value="Redact" onclick="OnRedacting();" class="btn green btnRedactShare btnSplitShare" title="Share Redacted File" />
                            <input type="button" id="btnSpltShare" runat="server" value="Share" onclick="OnSplitShare()" class="btn green btnSplitShare" title="Share Splitted File" />
                            <asp:Label ID="lblRedact" Style="display: none" CssClass="lblRedact" runat="server">Please click on any part of the pdf to redact the text</asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <iframe id="reviewContent" style="width: 100%; height: 100%;"></iframe>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="rightPreview">
                <div style="height: 7%">
                    <input type="button" id="btnSpltBtn" value="Tab" onclick="ShowAddToTabPopup();" class="btn green btnAddtoTab" title="Add To Tab" />
                </div>
                <div class="docIndex">
                    <h2 class="indexHeader">Indexing</h2>
                    <table width="100%" id="tblIndexPreview">
                        <tbody>
                            <tr id="trIndex" class="indexPreviewRow">
                                <td>
                                    <input type="hidden" value="0" class="hdnIndexId" /></td>
                                <td class="tdIndex">
                                    <input type="text" id="txtIndexKey" class="txtIndex txtKey" maxlength="20" placeholder="Key" />
                                    <input type="text" id="txtIndexValue" class="txtIndex" maxlength="20" placeholder="Value" /></td>
                                <td>
                                    <button id="btnAddIndex" class="btnIndex" type="button" onclick="return AddIndexPreviewingTextBoxes(this)">
                                        <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/add-icon.png" alt="Add" title="Add" /></button></td>
                                <td>
                                    <button id="btnRemoveIndex" class="btnIndex" type="button" onclick="return DisplayConfirmBox(this, true)">
                                        <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" />
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="popup-btn" style="margin-top: 50px;">
                        <input id="btnIndexPreviewingSave" type="button" value="Save" class="btn green" style="margin-right: 5px;" />
                        <input id="btnIndexPreviewingCancel" type="button" value="Cancel" class="btn green" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="popup-mainbox preview-popup" id="preview_doc_popup" style="display: none">
        <div class="popupcontrols" style="width: 100%; height: 5%;">
            <span id="popupdocclose" class="ic-icon ic-close"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%;">
            <table style="width: 100%; height: 100%">
                <tr style="height: 5%">
                    <td style="text-align: center;">
                        <input type="button" id="Button1" runat="server" value="PREVIOUS" onclick="ShowPreviousDoc();" style="margin-bottom: 10px;" class="btn green btnNextPreview" title="PREVIOUS" />
                        <div style="display: inline-flex; margin-bottom: 20px;">
                            <asp:Label ID="editableFile" runat="server" Style="font-size: 20px; padding-top: 10px;">Filename:</asp:Label>
                            <asp:Label ID="lblFileName" runat="server" Style="font-size: 15px; margin: 14px; margin-bottom: 0;"></asp:Label>
                            <asp:TextBox ID="fileNameFld" CssClass="fileNameField" runat="server" Style="display: none;"></asp:TextBox>
                            <asp:Button ID="btnUpdate" CssClass="quick-find-btn btnEdit" OnClientClick="return makeEditable();" Style="display: inline-block" runat="server" Text="Rename" />
                            <asp:Button ID="btnSave" OnClientClick="return CheckFileValidation();" CssClass="quick-find-btn btnEdit" OnClick="btnSave_Click" Style="display: none; margin-left: 20px;"
                                runat="server" Text="Save" />
                            <asp:Button ID="btnCancel" CssClass="quick-find-btn btnEdit" OnClientClick="return makeReadOnly();" Style="display: none; margin-left: 10px;"
                                runat="server" Text="Cancel" />
                        </div>
                        <input type="button" id="btnNextClick" runat="server" value="NEXT" onclick="ShowNextDoc();" style="margin-bottom: 10px; float: right"
                            class="btn green btnPrevPreview" title="NEXT" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <iframe id="reviewDocContent" style="width: 100%; height: 100%;"></iframe>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="popup-mainbox preview-popup" id="preview_popup_split" style="display: none; height: 460px; width: 395px; max-width: 395px; max-height: 460px; margin-top: 20px;">
        <div class="popupcontrols popup-head" style="width: 100%; height: 60px;">
            Split PDF Details
       
            <span id="popupclose_split" class="ic-icon ic-close popupclose"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%; background: #eaeaea; border-top: 2px solid darkgray;">
            <div class="popup-scroller" style="height: 75%; background: #eaeaea;">
                <p style="margin: 20px">
                    <label>New split Pdf name</label>
                    <asp:TextBox runat="server" ID="txtNewPdfName" Style="margin-bottom: 5px;"></asp:TextBox>
                    <br />
                    <label>1. Select an edFile from list</label>
                    <select id="comboboxSplit">
                    </select>

                    <label style="display: block; margin-top: 15px">
                        2. Select a tab for the selected edFile</label>
                    <select id="comboboxDividerSplit" style="display: none;">
                    </select>
                </p>

            </div>
            <div class="popup-btn" style="background: #eaeaea;">

                <asp:Button ID="btnSaveSplitPdf" runat="server" Text="Save" CssClass="btn green" OnClick="btnSaveSplitPdf_Click" Style="margin-right: 5px;" />
                <input id="btnCancelAddToTabSplit" type="button" value="Cancel" class="btn green" />
            </div>
        </div>
    </div>
    <div class="popup-mainbox preview-popup" id="PanelIndex" style="display: none;">
        <div>
            <div class="popup-head" id="Div4">
                Document Indexing
               
                <span id="closeIndex" class="ic-icon ic-close" style="float: right; cursor: pointer"></span>
            </div>
            <div class="popup-scroller-index">
                <div class="popup-row">
                    <input type="hidden" id="hndDividerIdIndex" />
                    <div class="popup-left">
                        <table width="100%" id="tblIndex">
                            <tbody>
                                <tr id="Tr1" class="indexRow">
                                    <td>
                                        <input type="hidden" value="0" class="hdnIndexId" /></td>
                                    <td>
                                        <input type="text" id="txtKey" maxlength="20" placeholder="Key" style="width: 85%; height: 120%; padding-left: 20px;" /></td>
                                    <td>
                                        <input type="text" id="txtValue" width="90%" maxlength="20" placeholder="Value" style="width: 85%; height: 120%; padding-left: 20px;" /></td>
                                    <td>
                                        <button id="btnAddIndexPair" type="button" width="90%" onclick="return AddIndexTextBoxes(this)" style="height: 35px; width: 35px;">
                                            <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/add-icon.png" title="Add" /></button></td>
                                    <td>
                                        <button id="btnRemoveIndexPair" type="button" width="90%" onclick="return DisplayConfirmBox(this)" style="height: 35px; width: 35px;">
                                            <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" />
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="popup-btn">
                            <input id="btnIndexSave" type="button" value="Save" class="btn green" style="margin-right: 5px;" />
                            <input id="btnIndexCancel" type="button" value="Cancel" class="btn green" />
                        </div>

                    </div>
                    <div class="popup-right">
                        <div id="indexPreview" class="popupcontent" style="width: 100%; height: 100%;">
                            <iframe id="iframeIndex" style="width: 100%; height: 630px;"></iframe>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <div class="popup-mainbox" id="DivDeleteConfirmIndex" style="display: none; position: fixed; z-index: 999999; margin-left: 30%; margin-top: 5%;">
        <div>
            <div class="popup-head" id="Div5">
                Delete Index
           
            </div>
            <div class="popup-scroller">
                <p>
                    Caution! Are you sure you want to delete this index?
               
                </p>
            </div>
            <div class="popup-btn">
                <input id="btnDeleteIndexOkay" onclick="return RemoveIndexTextBoxes()" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />
                <input id="btnDeleteIndexCancel" type="button" value="No" class="btn green" />
            </div>
        </div>
    </div>

    <div class="popup-mainbox" id="DivDeleteConfirm" style="display: none; position: fixed; z-index: 999999; margin-left: 35%; margin-top: 10%;">
        <div>
            <div class="popup-head" id="divDelete">
                Delete File
           
            </div>
            <div class="popup-scroller">
                <p>
                    Caution! Are you sure you want to delete this file. Once you delete this file, it
                                                        will be deleted for good
               
                </p>
                <p>
                    Confirm Password:
                   
                    <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                </p>
            </div>
            <div class="popup-btn">
                <asp:Button Text="Yes" CssClass="btn green" OnClientClick="HideDeleteModal();" OnClick="btnDeleteOkay_Click" runat="server" ID="btnDeleteOkay" Style="margin-right: 5px;" />
                <%--<input id="ButtonDeleleOkay" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />--%>
                <input id="btnDeleteCancel" onclick="CloseDeleteModal();" type="button" value="No" class="btn green" />
                <asp:HiddenField ID="hdnDeleteId" runat="server" />
            </div>
        </div>
    </div>
    <div class="popup-mainbox" id="DivDeleteConfirmIndexPreview" style="display: none; position: fixed; z-index: 999999; margin-left: 30%; margin-top: 5%;">
        <div>
            <div class="popup-head" id="Div7">
                Delete Index
           
            </div>
            <div class="popup-scroller">
                <p>
                    Caution! Are you sure you want to delete this index?
               
                </p>
            </div>
            <div class="popup-btn">
                <input id="btnDeleteIndexPreviewOkay" onclick="return RemoveIndexPreviewTextBoxes()" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />
                <input id="btnDeleteIndexPreviewCancel" type="button" value="No" class="btn green" />
            </div>
        </div>
    </div>

    <asp:Panel class="popup-mainbox" ID="DivDeleteConfirmation" Style="display: none"
        runat="server">
        <div>
            <div class="popup-head" id="PopupHeader">
                Delete File
           
            </div>
            <div class="popup-scroller">
                <p>
                    Caution! Are you sure you want to delete this file. Once you delete this file, it
                                                        will be deleted for good
               
                </p>
                <p>
                    Confirm Password:
                   
                    <asp:TextBox ID="textPassword1" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                </p>
            </div>
            <div class="popup-btn">
                <%--<asp:Button Text="Yes" CssClass="btn green" OnClick="btnDeleteOkay_Click" runat="server" ID="btnDeleteOkay" Style="margin-right: 5px;" />--%>
                <input id="ButtonDeleleOkay" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />
                <input id="ButtonDeleteCancel" onclick="CloseDeleteModal();" type="button" value="No" class="btn green" />
            </div>
        </div>
    </asp:Panel>

    <%-- ByPratik --%>

    <asp:Panel class="popup-mainbox" ID="DivArchiveConfirmation" Style="display: none" runat="server">
        <div>
            <div class="popup-head" id="Div3">
                Archive Folder
           
            </div>
            <div class="popup-scroller">
                <p>
                    Are you sure you want to archive this EdFile?  If you archive, the EdFile will be removed from the list the of active EdFiles.
               
                </p>
                <p>
                    Confirm Password:
                   
                    <asp:TextBox ID="txtBoxArchivePwd" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                </p>
            </div>
            <div class="popup-btn">
                <input id="Button1ArchiveOK" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />
                <input id="Button2ArchiveClose" type="button" value="No" class="btn green" />
            </div>
        </div>
    </asp:Panel>



    <asp:Button ID="Button1ShowPopup2" runat="server" Style="display: none" />
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BehaviorID="pnlPopup2"
        PopupControlID="pnlPopup2" DropShadow="true" BackgroundCssClass="modalBackground"
        Enabled="True" TargetControlID="Button1ShowPopup2" />
    <asp:Panel ID="pnlPopup2" Style="display: none"
        runat="server" class="popup-mainbox">
        <div>
            <div class="popup-head" id="Div1">
                Message
           
            </div>
            <div class="popup-scroller">
                <p>
                    Your document has been uploaded successfully...It will be converting to the EdFile format in our server over the next few minutes, depending on the size of the uploaded document. You can now select additional files to upload or navigate away from this web page. 
               
                </p>
            </div>
            <div class="popup-btn">
                <asp:Button ID="ShowPopup2Button2" runat="server" OnClientClick="return onClientButtonClick()"
                    Text="Close" class="btn green" />
                <%--<input id="btnuploadclose" type="button" value="Close"  />--%>
            </div>
        </div>
    </asp:Panel>
    <%-- ByPratik --%>

    <div class="popup-mainbox preview-popup" id="AddToTab_popup" style="display: none; height: 460px; width: 395px; max-width: 395px; max-height: 460px; margin-top: 20px;">
        <div class="popupcontrols popup-head" style="width: 100%; height: 60px;">
            Select Folder
       
            <span id="copyFileClose" class="ic-icon ic-close popupclose" style="float: right; cursor: pointer"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%; background: #eaeaea; border-top: 2px solid darkgray;">
            <div class="popup-scroller" style="height: 75%; background: #eaeaea;">
                <p style="margin: 20px">
                    <uc1:ucAddToTab ID="ucAddToTab1" runat="server" />
                </p>

            </div>
            <div class="popup-btn" style="background: #eaeaea;">
                <%--<input  runat="server"  type="button" value="Save"  />--%>
                <asp:Button ID="btnSelectAddToTab" Text="Save" runat="server" CssClass="btn green" OnClick="btnSelectAddToTab_Click" Style="margin-right: 5px;" />
                <input id="btnCancelAddToTabCopyFile" type="button" value="Cancel" class="btn green" />
            </div>
        </div>
    </div>

    <asp:HiddenField ID="DocumentIDs" runat="server" />

    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>


    <%--<asp:UpdatePanel runat="server" ID="mailPanel">
        <ContentTemplate>--%>
    <div id="dialog-share-book" title="Share this EdFile" style="display: none">
        <p class="validateTips">All form fields are required.</p>
        <br />
        <label for="email">From Email</label><br />
        <input type="email" name="email" id="email" runat="server" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <%--<label for="myname">My Name</label><br />
                    <input type="text" name="myname" id="myname" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />--%>
        <label for="emailto">To Email</label><br />
        <input type="email" name="emailto" id="emailto" runat="server" placeholder="Enter Email separated with';'" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <label for="nameto">To Name</label><br />
        <input type="text" name="nameto" id="nameto" runat="server" placeholder="Enter name separated with';'" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <label for="title">Subject</label><br />
        <input type="text" name="title" id="title" runat="server" value="EdFile share" readonly="readonly" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <label for="content">Message</label><br />
        <textarea name="content" id="content" rows="2" runat="server" cols="25" class="text ui-widget-content ui-corner-all" style="border: 1px solid black"></textarea><br />
        <label for="content">Link Expiration Time (In Hrs)</label><br />
        <input type="text" name="linkexpiration" id="expirationTime" class="text ui-widget-content ui-corner-all" style="border: 1px solid black; width: 80%;" />
        <input type="button" id="updateExpiration" onclick="UpdateExpirationTime()" class="btn-small green" style="float: right" value="UPDATE" /><br />
        <label for="pdfName" style="display: none" id="pdfLbl">New PDF Name</label><br />
        <asp:TextBox runat="server" ID="pdfName" Text="" placeholder="Enter new pdf name here" AutoPostBack="false" CssClass="text ui-widget-content ui-corner-all" Style="border: 1px solid black; display: none" />
        <%--<input type="text" name="pdfName" placeholder="Enter new pdf name here" id="pdfName" runat="server" class=""  />--%><br />
        <span id="errorMsg"></span>
    </div>
    <%--</ContentTemplate>
    </asp:UpdatePanel>--%>
    <div id="dialog-message-all" title="" style="display: none;">
        <p id="dialog-message-content">
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span>
            <span id="spanText"></span>
            <input type="button" id="closedialog" class="btn-small green" style="text-align: center; margin: 5%;" value="Close" /><br />
        </p>
    </div>
    <div id="dialog-share-book1" title="Share this EdFile" style="display: none">
        <p class="validateTips">All form fields are required.</p>
        <br />
        <label for="email">From Email</label><br />
        <input type="email" name="email1" id="email1" runat="server" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <label for="emailto">To Email</label><br />
        <input type="email" name="emailto1" id="emailto1" runat="server" placeholder="Enter Email separated with';'" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <label for="nameto">To Name</label><br />
        <input type="text" name="nameto" id="nameto1" runat="server" placeholder="Enter name separated with';'" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <label for="title">Subject</label><br />
        <input type="text" name="title" id="title1" runat="server" value="EdFile share" readonly="readonly" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <label for="content">Message</label><br />
        <textarea name="content" id="content1" rows="2" runat="server" cols="25" class="text ui-widget-content ui-corner-all" style="border: 1px solid black"></textarea><br />
        <label for="content">Link Expiration Time (In Hrs)</label><br />
        <input type="text" name="linkexpiration" id="expirationTime1" class="text ui-widget-content ui-corner-all" style="border: 1px solid black; width: 80%;" />
        <input type="button" id="updateExpiration1" onclick="UpdateExpirationTime()" class="btn-small green" style="float: right" value="UPDATE" /><br />
        <input type="hidden" id="isDivider" runat="server" />
        <span id="errorMsg"></span>
    </div>
    <div id="dialog-message-all1" title="" style="display: none;">
        <p id="dialog-message-content1">
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span>
            <span id="spanText1"></span>
            <input type="button" id="closedialog1" class="btn-small green" style="text-align: center; margin: 5%;" value="Close" /><br />
        </p>
    </div>
    <div id="uploadProgress" class="white_content"></div>

    <div class="popup-mainbox preview-popup" id="LogForm_popup" style="display: none">
        <div class="popupcontrols popup-head" style="width: 100%; height: 60px;">
            LogForm
       
            <span id="Span3" class="ic-icon ic-close popupclose"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%; font-size: 16px;">
            <div class="popup-scroller">
                <%--<p style="margin: 20px">--%>
                <asp:Panel ID="pnlEditablePDF" runat="server" class="popup-mainbox">
                    <%-- <div>
            <div class="popup-head" id="Div4">
                Message
            </div>
            <div class="popup-scroller">
               
            </div>
            <div class="popup-btn">
                
                <%--<input id="btnuploadclose" type="button" value="Close"  />--%>
                    <%--<uc3:ucFolderEditablePDF ID="ucFolderEditablePDF" runat="server" />--%>

                    <%-- <uc4:ucTest runat="server" id="ucTest"/>--%>
                </asp:Panel>
            </div>
        </div>
        <%--</p>--%>

        <%--    </div>
            <div class="popup-btn">--%>
        <%--<input  runat="server"  type="button" value="Save"  />--%>
        <%-- <asp:Button id="btnSelectWA" Text="Save" runat="server" CssClass="btn green" OnClick="btnSelectWA_Click" style="margin-right:5px;"/>
            <input id="btnCancelWA" type="button" value="Cancel" class="btn green" />--%>
        <%-- </div>
        </div> --%>
    </div>

    <asp:HiddenField ID="hdnSelectedFolderID" runat="server" />
    <asp:HiddenField ID="hdnSelectedDividerID" runat="server" />
    <asp:HiddenField ID="hdnSelectedFolderText" runat="server" />
    <asp:HiddenField ID="hdnSelectedDocumentID" runat="server" />
    <asp:HiddenField ID="hdnFileNumber" runat="server" />
    <asp:HiddenField ID="hdndivId" runat="server" />
    <asp:HiddenField runat="server" ID="hdnFileName" />
    <asp:HiddenField runat="server" ID="hdnFileId" />
    <asp:HiddenField runat="server" ID="hdnPageNumbers" />
    <asp:HiddenField runat="server" ID="hdnPath" />

    <script language="javascript" type="text/javascript">
        var tabName = "Panel0_GridView0";
        var ids = "";
        var fileid = '<%=Request.QueryString["id"]%>';
        var userId = "<%= Sessions.SwitchedRackspaceId%>";
        var selectedButton = "";
        var redactIndex = 1;
        var cords = [];

        function anotherRequest() {
            window.location = window.location;
        }

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');;
        }
        function hideLoader() {
            $('#overlay').hide();
        }
        function CloseModal(e) {
            debugger
            $(this).dialog("close");
        }
        $(document).ready(function () {
            $("[id*=ddlFolderName]").empty();
            $("[id*=ddlFolderLog]").empty();

            $('.FolderName').show();
            $('.FolderLogName').hide();

            //AddFolderLog();

            GetFolderNameDetails();

            $(".iframe").colorbox({
                iframe: true, width: "80%", height: "95%", onComplete: function () {
                    //iframe: true, width: aWidth, height: aHeight, onComplete: function () {
                    $('iframe').live('load', function () {
                        $('iframe').contents().find("head")
                            .append($("<style type='text/css'>  .splash{overflow:visible;}  </style>"));
                    });
                }
            });
            //var dz = document.querySelector('.ajax__fileupload_dropzone');
            //dz.addEventListener('dragenter', handleDragEnter, false);//Register Event dragenter    
            //dz.addEventListener('dragover', handleDragOver, false);//Register Event dragover    
            //dz.addEventListener('dragleave', handleDragLeave, false);//Register Event dragleave    
            //dz.addEventListener('drop', handleDrop, false);//Register Event drop    

            $(window).scroll(function () {
                $('#overlay:visible') && $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
            });

            $('#dialog-share-book').dialog({
                autoOpen: false,
                width: 450,
                //height: 540,
                resizable: false,
                modal: true,
                buttons: {
                    "Share Now": function () {
                        //bookshelf.mailTo($(this));
                        SaveMail();

                        SaveMailLog(documentId);
                    },
                    "Close": function () {
                        $(this).dialog("close");
                    }
                },
                close: function () {

                }
            });

            $('#dialog-share-book1').dialog({
                autoOpen: false,
                width: 450,
                //height: 540,
                resizable: false,
                modal: true,
                buttons: {
                    "Share Now": function () {
                        //bookshelf.mailTo($(this));
                        SaveMailFolder();

                        //SaveMailLog(documentId);
                    },
                    "Close": function () {
                        $(this).dialog("close");
                    }
                },
                close: function () {

                }
            });

            $('.ui-dialog-buttonset').find('button').each(function () {
                $(this).find('span').text($(this).attr('text'));
            })

            $('#closedialog').click(function () {
                $('#dialog-message-all').dialog("close");
            });

            $('#closedialog1').click(function () {
                $('#dialog-message-all1').dialog("close");
            });

            $("#dialog-message-all").dialog({
                autoOpen: false,
                resizable: false,
                modal: true,
                Ok: function () {
                    $(this).dialog("close");
                },
                close: function (event, ui) {
                    $(this).dialog("option", 'title', '');
                    $('#dialog-confirm-content').empty();
                }

            });

            $("#dialog-message-all1").dialog({
                autoOpen: false,
                resizable: false,
                modal: true,
                buttons: {
                    "Close": function () {
                        $(this).dialog("close");
                    }
                },
                Ok: function () {
                    $(this).dialog("close");
                },
                close: function (event, ui) {
                    $(this).dialog("option", 'title', '');
                    $('#dialog-confirm-content1').empty();
                }

            });

            var closePopup = document.getElementById("popupclose");
            closePopup.onclick = function () {
                var popup = document.getElementById("preview_popup");
                var overlay = document.getElementById("overlay");
                overlay.style.display = 'none';
                popup.style.display = 'none';
                //popup.style.maxWidth = '900px;'
                enableScrollbar();
                EnableAfterRedacting();
                $('#reviewContent').removeAttr('src');
            };



            var closePopupDoc = document.getElementById("popupdocclose");
            closePopupDoc.onclick = function () {
                var popup = document.getElementById("preview_doc_popup");
                var overlay = document.getElementById("overlay");
                overlay.style.display = 'none';
                popup.style.display = 'none';
                enableScrollbar();
                makeReadOnly();
                $('#reviewDocContent').removeAttr('src');
            };

            $("#" + '<%= txtKeyword.ClientID%>').keypress(function (event) {
                if (event.keyCode == 13) {
                    $("#" + '<%=btnSearch.ClientID %>').click();
                    return false;
                }
            });

        //$('#dialog-share-book input[name="linkexpiration"]').val('<%= Sessions.ExpirationTime %>');

        });

        $(document).on('click', '#<%= btnSelectAddToTab.ClientID%>', function (e) {
            var selectedFolder = $("#combobox").chosen().val();
            var selectedDivider = $("#comboboxDivider").chosen().val();
            if (selectedFolder == "0" && selectedDivider == "0") {
                alert("Please select at least one EdFile & one Tab");
                e.preventDefault();
                return false;
            }
            else if (selectedFolder == "0") {
                alert("Please select at least one EdFile");
                e.preventDefault();
                return false;
            }
            else if (selectedDivider == "0") {
                alert("Please select at least one Tab");
                e.preventDefault();
                return false;
            }
            var folderValue = $('.chosen-single').text();
            if (selectedFolder == undefined || selectedFolder == null || selectedFolder.length <= 0) {
                alert('Please select folder to move request files.');
                e.preventDefault();
                return false;
            }
            SetPageIndex($('#<%= hdnActiveTabIndex.ClientID%>').val());
            $('#overlay').css('z-index', '100010');
            $('#<%= hdnSelectedFolderID.ClientID%>').val(selectedFolder);
            $('#<%= hdnSelectedDividerID.ClientID%>').val(selectedDivider);
            $('#<%= hdnSelectedFolderText.ClientID%>').val(folderValue);
            $('#<%= hdnFolderId.ClientID%>').val(selectedFolder);
            $('#<%= hdndivId.ClientID%>').val(selectedDivider);
            return true;
        });

        $(document).on('click', '#<%= btnCopyFile.ClientID%>', function (e) {
            e.preventDefault();
            if (GetSelectedFiles()) {
                $('#AddToTab_popup,#overlay').show().focus();
                $("#floatingCirclesG").hide();
                $('#combobox').trigger('chosen:open');
                e.stopPropagation();
                disableScrollbar();
            }
        });


        function DisplayAddToTabModal(e) {
            e.preventDefault();

            $('#AddToTab_popup,#overlay').show().focus();
            $("#floatingCirclesG").hide();
            $('#combobox').trigger('chosen:open');
            //if ($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]") && $(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val() && JSON.parse($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId)
            //    ids= JSON.parse($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId ;

            $('#<%= hdnSelectedDocumentID.ClientID%>').val(JSON.parse($(e.currentTarget).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId);

            e.stopPropagation();
            disableScrollbar();

        }
        function ShowPreviewForRequestedFiles(id) {

            var folderid = $('#' + '<%=hdnFolderId.ClientID%>').val();
            window.open(window.location.origin + "/ViewerTemp.aspx?id=" + id, '_blank');
            CreateDocumentsAndFolderLogs(folderid, null, null, id, null, ActionEnum.Preview, null);
            return false;
        }

        function InsertTabAction(dividerId) {
            CreateDocumentsAndFolderLogs(newFolderID, dividerId, null, null, null, ActionEnum.View, null);
        }

        function InsertViewEdFilesAsBookLog() {

            var folderId = $('#' + '<%=hdnFolderId.ClientID%>').val();
            CreateDocumentsAndFolderLogs(folderId, null, null, null, null, ActionEnum.BookView, null);
        }
        function ShowLogFormLogs(id) {
            var folderid = $('#' + '<%=hdnFolderId.ClientID%>').val();
            window.open(window.location.origin + "/LogFormLogs.aspx?id=" + id, '_blank');
            CreateDocumentsAndFolderLogs(folderid, null, null, id, null, ActionEnum.View, null);
        }

        $(document).delegate("#" + '<%= downloadall.ClientID%>', 'click', function () {
            ids = "";
            if ($("#ctl00_ContentPlaceHolder1_gridViewOcr:visible").find('input[type="checkbox"]:checked').length > 0) {
                $("#ctl00_ContentPlaceHolder1_gridViewOcr:visible").find('input[type="checkbox"]:checked').each(function () {
                    if ($(this).closest('tr').find("input[type='hidden'][id$=hdnDocId]") && $(this).closest('tr').find("input[type='hidden'][id$=hdnDocId]").val())
                        ids += $(this).closest('tr').find("input[type='hidden'][id$=hdnDocId]").val() + ",";
                });
                ids = ids.substring(0, ids.length - 1);
                $('#<%= DocumentIDs.ClientID %>').attr("value", ids); // set hidden field value
            }
            else if ($('.ajax__tab_panel:visible').find('table input[type="checkbox"]:checked').length > 0 && !($("#ctl00_ContentPlaceHolder1_gridViewOcr:visible").length > 0)) {
                $('.ajax__tab_panel:visible').find('table input[type="checkbox"]:checked').each(function () {

                    $('.ajax__tab_panel:visible').find('table input[type="hidden"][id$=hdnValues]').val();
                    if ($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]") && $(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val() && JSON.parse($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId)
                        ids += JSON.parse($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId + ",";
                });
                ids = ids.substring(0, ids.length - 1);
                $('#<%= DocumentIDs.ClientID %>').attr("value", ids); // set hidden field value
            }
            else {
                alert("Please select at least one document.");
                return false;
            }
            return true;
        });

        function SetFolderAndRedirect() {
            $.ajax(
                {
                    type: "POST",
                    url: 'FileFolderBox.aspx/SetFolderId',
                    contentType: "application/json; charset=utf-8",
                    data: '{"folderId":"' + newFolderID + '"}',
                    dataType: "json",
                    success: function (data) {
                        window.location.href = window.location.origin + '/Office/EdFormsFolder.aspx';
                    },
                    error: function (result) {
                        console.log('Failed' + result.responseText);
                        hideLoader();
                    }
                });
            return false;
        }

        function check() {
            ids = "";
            if ($('.ajax__tab_panel:visible').find('table input[type="checkbox"]:checked').length > 0) {
                $('.ajax__tab_panel:visible').find('table input[type="checkbox"]:checked').each(function () {
                    if ($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]") && $(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val() && JSON.parse($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId)
                        ids += JSON.parse($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId + ",";
                });
                ids = ids.substring(0, ids.length - 1);
                $('#<%= DocumentIDs.ClientID %>').attr("value", ids); // set hidden field value
                //     return true;
            }
            else {
                alert("Please Select Atleast one document.");
                //       return false;
            }
            return true;
        }

        function ShowHideUploadBtn() {
            var Upload_Image = document.getElementById('<%= FileUpload1.ClientID %>');
            if (Upload_Image.value != '') {
                document.getElementById('<%= btnUpload.ClientID %>').style.display = 'inline-block';
                document.getElementById('<%= spanFileUpload1.ClientID %>').style.display = 'none';
            }
            else {
                document.getElementById('<%= btnUpload.ClientID %>').style.display = 'none';
                document.getElementById('<%= spanFileUpload1.ClientID %>').style.display = 'inline-block';
            }

        }

        function handleDragOver(e) {
            if (e.preventDefault) {
                e.preventDefault(); // Necessary. Allows us to drop.    
            }
            this.classList.add('over');
            return false;
        }

        function handleDragEnter(e) {
            // If you have used the DropZone you must have noticed that when you drag an item into the browser the gray area(The DropZone)is hilighted by dotted line at its border    
            // well here is how I do it I just add border to the div...    
            // I Have created ta class called over which basically has the styles to hilight the div.     
            // but only when the you are draging any file on the browser    
            this.classList.add('over');
        }

        function handleDragLeave(e) {
            // while draging If you move the cursour out of the DropZone, then the hilighting must be removed    
            // so here I am removing the class over from the div    
            e.preventDefault();
            this.classList.remove('over');

        }

        //On Drop of file over the DropZone(I have Elaborated the process of What Happen on Drop in Points)    
        function handleDrop(e) {
            //1st thing to do is Stop its default event, If you won't then the browser will end up rendering the file    
            e.preventDefault();
            //2nd Checking if the the object e has any files in it.    
            //    actually the object named "dataTransfer" in the object e is the object that hold the data that is being dragged during a drag and drop operation    
            //    dataTransfer object can hold one or more files    
            if (e.dataTransfer.files.length == 0) {
                this.classList.remove('over');
                return;// if no files are found then there is no point executing the rest of the code so I return to the function.    
            }
            var files = e.dataTransfer.files;
            //3rd  Here I am using an object of FormData() to send the files to the server using AJAX    
            //     The FormData object lets you compile a set of key/value pairs to send using AJAX    
            //     Its primarily intended for use in sending form data    
            var data = new FormData();
            for (var i = 0, f; f = files[i]; i++) {
                data.append(files[i].name, files[i]);//Here I am appending the files from the dataTransfer object to FormData() object in Key(Name of File) Value(File) pair    
            }
            // The operation of Uploading the file consumes time till the time the browser almost freezes     
            this.classList.remove('over');
            BindDropDownOnSelectAndDrop();
        }

        function closePage() {
            window.location = "http://www.edfiles.com/Office/Welcome.aspx";
        }

        function setTab(dividerId, str) {
            tabName = str;
            InsertTabAction(dividerId);
        }
        var myTimer;
        function onClientUploadStart(sender, e) {
            $('.uploadRename').remove();
            $("#uploadHeader").remove();
            document.getElementById('uploadCompleteInfo').innerHTML = "please wait while the " + e.get_filesInQueue() + " file is uploaded and converted to eff...";
        //$('#<%= hdnUploadFiles.ClientID %>').val(JSON.stringify(fileIDDividerId));
            var contextKey = [];
            contextKey.push({ folderName: $('#<%= hdnFolderName.ClientID %>').val(), isRenamed: $('#<%= hdnIsRenamed.ClientID %>').val() });
            //sender._contextKey = { first: "1", second: "2" };
            document.cookie = "Files=" + JSON.stringify(fileIDDividerId);
            document.cookie = "Class=" + JSON.stringify(fileIDClassId);
            document.cookie = "OCR=" + JSON.stringify(fileIDCheckId);
            document.cookie = "IsRenamed=" + $('#<%= hdnIsRenamed.ClientID %>').val();
            document.cookie = "FolderName=" + $('#<%= hdnFolderName.ClientID %>').val();
            showLoader();

            $('[id*=AjaxFileUpload1_Html5DropZone]').addClass('hideControl');
            $('[id*=AjaxFileUpload1_SelectFileContainer]').addClass('hideControl');
            $('[id*=FileItemDeleteButton]').addClass('hideControl').css('display', 'none');
            $('[id*=UploadOrCancelButton]').addClass('hideControl');

            $('.TabDropDown').prop('disabled', 'disabled');
            $('.ClassDropDown').prop('disabled', 'disabled');
            $('#uploadProgress').html('');
            $('#uploadProgress').html('').html($('#divUpload').html());
            document.getElementById('uploadProgress').style.display = 'block';
            myTimer = setInterval(function () {
                // $('#divUpload').clone(true).appendTo('#uploadProgress');
                $('#uploadProgress').html('').html($('#divUpload').html());
            }, 1000);
        }

        function onClientUploadComplete(sender, e) {
            $('.uploadRename').remove();
            $('[id*=UploadOrCancelButton]').removeClass('uploadOnly');
            //onImageValidated("TRUE", e);
            return false;
        }

        function onImageValidated(arg, context) {
            var fileList = document.getElementById("fileList");
            var item = document.createElement('div');
            item.style.padding = '4px';

            item.appendChild(createFileInfo(context));

            fileList.appendChild(item);
        }

        function createFileInfo(e) {
            var holder = document.createElement('div');
            holder.appendChild(document.createTextNode(e.get_fileName() + ' with size ' + e.get_fileSize() + ' bytes'));

            return holder;
        }

        function onClientUploadCompleteAll(sender, e) {
            $('.hideControl').removeClass('hideControl');
            $('.uploadProgress').find('select').removeAttr('disabled');
            $('#divUpload').clone().appendTo('#uploadFieldset');
            clearInterval(myTimer);
            document.getElementById('uploadProgress').style.display = 'none';
            $('#<%= hdnIsRenamed.ClientID %>').val('');
            __doPostBack('<%= UpdatePanel1.ClientID %>', '');
        }

        function Check_Click(objRef) {
            //Get the Row based on checkbox            
            var row = objRef.parentNode.parentNode.parentNode;
            //Get the reference of GridView

            var GridView = row.parentNode;

            //Get all input elements in Gridview

            var inputList = GridView.getElementsByTagName("input");

            for (var i = 0; i < inputList.length; i++) {

                //The First element is the Header Checkbox

                var headerCheckBox = inputList[0];

                //Based on all or none checkboxes

                //are checked check/uncheck Header Checkbox

                var checked = true;

                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {

                    if (!inputList[i].checked) {

                        checked = false;
                        break;
                    }
                }
            }
            headerCheckBox.checked = checked;
        }

        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode.parentNode;

            var inputList = GridView.getElementsByTagName("input");

            for (var i = 0; i < inputList.length; i++) {

                //Get the Cell To find out ColumnIndex

                var row = inputList[i].parentNode.parentNode;

                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {

                    if (objRef.checked) {
                        inputList[i].checked = true;
                    }

                    else {
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function onClientButtonClick() {
            $find('pnlPopup2').hide();

            setTimeout(function () {
                //window.location.reload(true);
            }, 1000);
            return false;
        }

        function ScanDocFile() {
            $('.iframe').attr('href', 'UploadScanDocNew.aspx?id=' + fileid + '&page=filefolderbox&tabIndex=' + $('#<%= hdnActiveTabIndex.ClientID %>').val()).trigger('click');
        }

        $(function () {
            var dWidth = window.screen.width;
            var dHeight = window.screen.height;
            var aWidth = Math.floor((80 * window.screen.width) / 100);
            var aHeight = Math.floor((57 * aWidth) / 100);
        });

        var dividerId;
        var classId;
        var setDividerId;
        var newFolderID = '<%= this.EffID_FFB %>';
        var fileIDDividerId = [];
        var fileIDClassId = [];
        var fileIDCheckId = [];
        function pageLoad() {
            UpdateDocumentSearch();
            ShowPreviousNext();
            pageIndex = 1;
            $(".dvGrid").on("scroll", function (e) {
                var $o = $(e.currentTarget);
                if ($o[0].scrollHeight - $o.scrollTop() <= $o.outerHeight()) {
                    if ($('#<%= txtName.ClientID %>').val() != "" || $('#<%= txtClass.ClientID %>').val() != "" || $('#<%= txtTDate.ClientID %>').val() != "" || $('#<%= txtFDate.ClientID %>').val() != "" || $('#<%= txtIndex.ClientID %>').val() != "") {
                        GetFilteredRecords();
                    }
                    else {
                        GetRecords();
                    }
                }
            });

            if ($("#<%= hdnIsScrollable.ClientID %>").val() == "true") {
                var activeIndex = $("#<%= hdnActiveTabIndex.ClientID %>").val();
                $("[id$=GridView" + activeIndex + "]").parent().parent().css("max-height", "300px");
            }
            fileIDDividerId = [];
            fileIDClassId = [];
            fileIDCheckId = [];
            changeSearch();
            if (LoadFolders)
                LoadFolders();

            AddFolderLog();
            try {
                if (setDividerId)
                    dividerId = setDividerId;
                else
                    dividerId = '<%= this.DividerID_FFB %>';
            }
            catch (exception) {
            }
            init(dividerId);

            var fileTabsChildren = $("#" + '<%=fileTabs.ClientID%>').children();
            if (fileTabsChildren != undefined && fileTabsChildren != null) {
                var children = fileTabsChildren.children().find('.ajax__tab_disabled');
                if (children != undefined && children != null && children.parent().parent().parent() != undefined && children.parent().parent().parent() != null)
                    children.parent().parent().parent().css("display", "none");
            }

            $('.htmlViewerFile').click(function (e) {
                var folderId = $('#' + '<%=hdnFolderId.ClientID%>').val();
                e.preventDefault();
                window.open("WebFlashViewer.aspx?V=2&ID=" + folderId, '_blank', "toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no");
            });

            $('#' + '<%=lblFolderName.ClientID%>').click(function (e) {
                var folderId = $('#' + '<%=hdnFolderId.ClientID%>').val();
                e.preventDefault();
                window.open("WebFlashViewer.aspx?V=2&ID=" + folderId, '_blank', "toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no");
            });

            $('#<%=btnAddtoTab.ClientID%>').click(function (e) {
                e.preventDefault();
                update = 0;
                $(".FolderLogName").hide();
                $(".FolderName").show();
                $('#AddToFolderLog_popup,#overlay').show().focus();
                $('#combobox').trigger('chosen:open');
                $("#floatingCirclesG").css('display', 'none');
                //$('#floatingCirclesG').addClass(hideControl);
                e.stopPropagation();
            });

            var dz = document.querySelector('.ajax__fileupload_dropzone');
            dz.addEventListener('dragenter', handleDragEnter, false);//Register Event dragenter    
            dz.addEventListener('dragover', handleDragOver, false);//Register Event dragover    
            dz.addEventListener('dragleave', handleDragLeave, false);//Register Event dragleave    
            dz.addEventListener('drop', handleDrop, false);//Register Event drop  
            $(".ajax__fileupload_dropzone").text("Drag and Drop Pdf file(s) here");

            var abc = $('#<%= fileTabs.ClientID %>').children().last().children().each(function () {
                showLoader();
                $('#' + $(this).attr("id") + '_tab').css("background", $(this).css("background-color"));
                $('#' + $(this).attr("id") + '_tab').css("border", "solid 2px black");
                $('#' + $(this).attr("id") + '_tab').find('span').last().css("color", $(this).css("color"));
                $('#' + $(this).attr("id") + '_tab').css("margin-bottom", "10px");
                $('#' + $(this).attr("id") + '_tab').find('span').last().attr('title', $('#' + $(this).attr("id") + '_tab').find('span').last().text());
                if ($(this).css("color") == "rgb(0, 0, 0)") {
                    $('#' + $(this).attr("id") + '_tab').append('<span class="arrow" style="border-top-color:black"></span>');
                }
                else {
                    $('#' + $(this).attr("id") + '_tab').append('<span class="arrow" style="border-top-color:' + $(this).css("background-color") + '"></span>');
                }
                hideLoader();
            });


            $('#copyFileClose, #btnCancelAddToTabCopyFile').click(function (e) {
                e.preventDefault();
                $(this).closest('.popup-mainbox').hide();
                $('#overlay').hide();
                enableScrollbar();
            });

            $('.lnkExpand').click(function () {
                $(this).siblings().parent().find('label').show(800);
                $(this).siblings().parent().find('.btnNav').hide();
                $(this).siblings().parent().find('.lnkCollapse').show();
                $(this).hide();
            });

            $('.lnkCollapse').click(function () {
                $(this).siblings().parent().find('label').not('#1').hide(800);
                $(this).siblings().parent().find('.btnNav').show();
                $(this).siblings().parent().find('.lnkExpand').show();
                $(this).hide();
            });

        }

        function SetDividerId(dividerID) {

            if (dividerID)
                setDividerId = dividerID;
            //dividerId = dividerID;
        }


        function init() {
            $('input[type="file"]').change(function () {
                if ($("div.ajax__fileupload_fileItemInfo").length > 0 && $("#uploadHeader").length == 0) {
                    AddUploadHeader();
                }
                BindDropDownOnSelectAndDrop();
            });
        }

        function AddUploadHeader() {
            var div = document.createElement('div');
            $(div).attr("id", "uploadHeader");
            var table = document.createElement('table');
            $(table).css("float", "right");
            var tr = document.createElement('tr');
            var th1 = '<th class="headerstyle" style="padding-right:75px">Class<th>';
            var th2 = '<th class="headerstyle" style="padding-right:120px">Select Tab<th>';
            //var th3 = '<th class="headerstyle">OCR(Searchable)<th>';
            $(th1).appendTo($(tr));
            $(th2).appendTo($(tr));
            //$(th3).appendTo($(tr));
            $(tr).appendTo($(table));
            $(table).appendTo($(div));
            $(div).insertAfter($('[id*=AjaxFileUpload1_SelectFileContainer]'));
        }

        function BindDropDownOnSelectAndDrop() {
            $.each($("div.ajax__fileupload_fileItemInfo"), function (i, e) {
                if ($('.uploadRename').length == 0) {
                    var renameDiv = '<div id="uploadRename" title="File will be uploaded and renamed automatically." class="ajax__fileupload_uploadbutton uploadRename">Upload & Rename</div>';
                    $(renameDiv).insertBefore('[id*=UploadOrCancelButton]');
                    $('[id*=UploadOrCancelButton]').addClass('uploadOnly');
                    $('#uploadRename').click(function () {
                        $('#<%= hdnIsRenamed.ClientID %>').val("true");
                        $('[id*=UploadOrCancelButton]').click();
                    });
                }
                if ($(e).find('.TabDropDown').length == 0) {
                    var fileId = $(e).attr('id').split('FileItemContainer_').length > 1 ? $(e).attr('id').split('FileItemContainer_')[1] : "";
                    //BindCheckBox(e, fileId);
                    BindTabDropDownOnUpload(e, dividerId, fileId);
                    BindClassDropDownOnUpload(e, "class" + fileId);

                    $(e).find("div.removeButton").click(function () {
                        if ($("div.ajax__fileupload_fileItemInfo").length == 0) {
                            $("#uploadHeader").remove();
                        }
                        RemoveFileIdFromArray(fileId);
                    });
                }
            });
        }

        function BindCheckBox(obj, fileId) {
            var check = $('<input class="checkSelect" type="checkbox" id="check' + fileId + '"/>');
            check.appendTo(obj);
            fileIDCheckId.push({ FileId: fileId, CheckId: false });
            $("#check" + fileId).change(function () {
                SetfileCheckArray(fileId, $(this).prop("checked"));
            });
        }

        function SetfileCheckArray(fileId, value) {
            $.each(fileIDCheckId, function (i, e) {
                if (fileId && e.FileId && e.FileId == fileId) {
                    e.CheckId = value;
                }
            });
        }

        function BindTabDropDownOnUpload(obj, dividerId, fileId) {
            var data = JSON.parse('<%=GetDividerForDropDown%>');
            var s = $('<select class="TabDropDown" id="' + fileId + '"/>');
            $.each(data, function (i, e) {
                $('<option />', { value: e.DividerID, text: e.Name, selected: parseInt(e.DividerID) == parseInt(dividerId) }).appendTo(s);
            });
            s.appendTo(obj);
            $("#" + fileId).change(function () {
                SetfileDividerArray(fileId, $(this).val(), $(this)[0].selectedOptions[0].text, $(this));
            });
            if (dividerId && fileId != "")
                fileIDDividerId.push({ FileId: fileId, DividerID: parseInt(dividerId), DividerName: $("#" + fileId)[0].selectedOptions[0].text });
        }

        function SetfileDividerArray(fileId, selectedDividerId, selectedDividerName, objDropDown) {
            $.each(fileIDDividerId, function (i, e) {
                if (fileId && e.FileId && e.FileId == fileId) {
                    e.DividerID = parseInt(selectedDividerId);
                    e.DividerName = selectedDividerName;
                    $.each(objDropDown.find('option'), function (index, ele) {
                        if ($(this).attr('selected') && $(this).attr('selected') == "selected") {
                            $(this).removeAttr('selected');
                        }
                    });
                    $('#' + $(objDropDown).attr("id")).find('option[value="' + e.DividerID + '"]').attr("selected", true);
                }
            });
        }

        function BindClassDropDownOnUpload(obj, fileId) {
            var data = JSON.parse('<%=GetClassForDropDown%>');
            var s = $('<select class="ClassDropDown" id="' + fileId + '"/>');
            $.each(data, function (i, e) {
                $('<option />', { value: e.ClassID, text: e.Name }).appendTo(s);
            });
            s.appendTo(obj);

            $("#" + fileId).change(function () {
                SetfileClassArray(fileId, $(this).val(), $(this));
            });
            fileIDClassId.push({ FileId: fileId, ClassID: 1 });
        }

        function SetfileClassArray(fileId, selectedClassId, objDropDown) {
            $.each(fileIDClassId, function (i, e) {
                if (fileId && e.FileId && e.FileId == fileId) {
                    e.ClassID = parseInt(selectedClassId);
                    $.each(objDropDown.find('option'), function (index, ele) {
                        if ($(this).attr('selected') && $(this).attr('selected') == "selected") {
                            $(this).removeAttr('selected');
                        }
                    });
                    $('#' + $(objDropDown).attr("id")).find('option[value="' + e.ClassID + '"]').attr("selected", true);
                }
            });
        }

        function RemoveFileIdFromArray(fId) {
            fileIDDividerId = $.grep(fileIDDividerId, function (e) {
                return !(fId && e.FileId && fId == e.FileId);
            });
            fileIDClassId = $.grep(fileIDClassId, function (e) {
                return !(fId && e.FileId && fId == e.FileId.replace('class', ''));
            });
            fileIDCheckId = $.grep(fileIDCheckId, function (e) {
                return !(fId && e.FileId && fId == e.FileId.replace('check', ''));
            });
            if (fileIDDividerId.length == 0 || fileIDClassId.length == 0 || fileIDCheckId.length == 0) {
                $('.uploadRename').remove();
                $('[id*=UploadOrCancelButton]').removeClass('uploadOnly');
            }
        }

        var closePopup = document.getElementById("popupclose");
        var closePopupIndex = document.getElementById("PanelIndex");
        var overlay = document.getElementById("overlay");
        var popup = document.getElementById("preview_popup");
        var button = document.getElementById("button");
        // Close Popup Event
        closePopup.onclick = function () {
            overlay.style.display = 'none';
            popup.style.display = 'none';
            $("#floatingCirclesG").css('display', 'block');
            enableScrollbar();
            $('#reviewContent').removeAttr('src');
        };

        closePopupIndex.onclick = function () {
            overlay.style.display = 'block';
            $("#floatingCirclesG").css('display', 'block');
        };

        function DocIndex() {
            CreateDocumentsAndFolderLogs(newFolderID, null, documentId, null, null, ActionEnum.View, null);
        }

        function disableScrollbar() {
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }

        function enableScrollbar() {
            $('html, body').css({
                'overflow': 'auto',
                'height': 'auto'
            });
        }

        //done
        function showOrder(documentOrder, dropOrderObj, dropMoveObj, dividerId) {
            $('#' + dropOrderObj)[0].value = documentOrder;
            $('#' + dropMoveObj)[0].value = dividerId;
            $('.cancelDoc, .saveDoc ,.ddlDropOrder,.ddlDropMove').hide();
            $('.deletedocs ,.editdocs ,.lnkBtnAddToTab').show();
            $('#' + dropOrderObj).closest('td').find('.deletedocs ,.editdocs,.lnkBtnAddToTab').hide();
            $('#' + dropOrderObj).closest('td').find('.cancelDoc, .saveDoc ,.ddlDropOrder').show();
            return false;
        }
        function showMove(documentOrder, dropOrderObj, dropMoveObj, dividerId) {

            $('#' + dropOrderObj)[0].value = documentOrder;
            $('#' + dropMoveObj)[0].value = dividerId;
            $('.ddlDropMove,.saveDoc,.cancelDoc,.ddlDropOrder').hide();
            $('.deletedocs ,.editdocs').show();

            $('#' + dropMoveObj).closest('td').find('.deletedocs ,.editdocs,.lnkBtnAddToTab').hide();
            $('#' + dropMoveObj).closest('td').find('.cancelDoc, .saveDoc ,.ddlDropMove').show();


            return false;
        }


        var sourceOrder, targetOrder, dividerId;
        var newDividerId, oldDividerId, documentId, oldPath, newname, documentOrder;
        var targetClass, classname;
        //done
        function OnchangeOrder(documentOrder, dropDownObj, dividerid, className) {
            sourceOrder = documentOrder;
            //targetOrder = $('#' + dropDownObj)[0].value;
            targetOrder = dropDownObj.value;
            dividerId = dividerid;
            classname = className;
        }
        function OnchangeClass(class1, dropDownObj, dividerid, documentid, oldpath, documentorder) {
            targetClass = dropDownObj.value;
            documentId = documentid;
            newDividerId = dividerid;
            oldDividerId = dividerid;
            oldPath = oldpath;
            documentOrder = documentorder;
        }

        function OnMoveOrder(documentid, dropDownObj, dividerid, oldpath, className) {
            oldPath = oldpath;
            documentId = documentid;
            //newDividerId = $('#' + dropDownObj).value;
            newDividerId = $('#' + dropDownObj)[0].value;
            oldDividerId = dividerid;
            classname = className;
        }

        function CancelOrderMove(documentOrder, dropDownObj, dropMoveObj, dividerId) {
            $('#' + dropDownObj)[0].value = documentOrder;
            $('#' + dropMoveObj)[0].value = dividerId;
            $('#' + dropDownObj).closest('td').find('.editdocs ,.deletedocs,.lnkBtnAddToTab').show();
            $('#' + dropDownObj).closest('td').find('.cancelDoc, .saveDoc ,.ddlDropMove,.ddlDropOrder').hide();
        }

        function SaveOrder(obj) {
            pageIndex = 1;
            if ($(".ddlDropOrder").is(":visible")) {
                if (targetOrder != "" && targetOrder != undefined && targetOrder != sourceOrder) {
                    showLoader();
                    $.ajax(
                        {
                            type: "POST",
                            url: '../EFileFolderJSONService.asmx/UpdateDocumentOrder1',
                            data: "{ sourceId: 0 , targetId: 0 , dividerId:'" + dividerId + "', SourceDocOrder:'" + sourceOrder + "', TargetDocOrder:'" + targetOrder + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                var docObj = JSON.parse(data.d);
                                if (docObj !== undefined && docObj.length > 0) {
                                    alert("Your file has been reordered successfully.");
                                    hideLoader();
                                    clearSaerchTextbox();
                                    SetPageIndex($("#<%= hdnActiveTabIndex.ClientID %>").val());
                                    __doPostBack('', 'RefreshGrid@' + dividerId);
                                }
                            },
                            error: function (result) {
                                console.log('Failed' + result.responseText);
                                hideLoader();
                            }
                        });
                }

            }

            if ($(".ddlDropMove").is(":visible")) {

                if (newDividerId != "" && newDividerId != undefined && newDividerId != oldDividerId) {

                    oldPath = (oldPath);
                    var isMove = 1;
                    var documentOrder = 0;
                    var renameFile = "komal";
                    showLoader();

                    $.ajax(
                        {
                            type: "POST",
                            url: '../EFileFolderJSONService.asmx/MoveRenameFile',
                            data: "{ newDividerId:'" + newDividerId + "', oldPath:'" + oldPath + "', oldDividerId:'" + oldDividerId + "', documentId:'" + documentId + "', documentOrder:'" + 0 + "', isMove:'" + 1 + "', renameFile:'" + "" + "',className:'" + classname + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (result) {
                                alert("Your file has been moved successfully.");
                                hideLoader();
                                clearSaerchTextbox();
                                SetPageIndex($("#<%= hdnActiveTabIndex.ClientID %>").val());
                                __doPostBack('', 'RefreshGrid@' + newDividerId + "@" + oldDividerId);
                            },
                            error: function (result) {
                                hideLoader();
                            }
                        });

                }
            }
        }
        //changes of Rename
        var lastEditable, lastdropdownvalue;
        function showEdit(ele) {
            $('.newSave,.cancel').hide();
            $('.edit').show();
            var $this = $(ele);
            $('.txtDocumentName').hide();
            $('.ddlDropClass').hide();
            var label = $(ele).closest("tr").find('[name=DocumentName]');
            var dropdownvalue = $(ele).closest("tr").find('[name=lblClass]');
            if ((lastEditable == null && lastdropdownvalue == null) || (lastEditable == undefined && lastdropdownvalue == undefined)) {
                lastEditable = label;
                lastdropdownvalue = dropdownvalue;
            }
            else {
                if (lastEditable != label) {
                    lastEditable.css('display', 'inline');
                    lastdropdownvalue.css('display', 'inline');
                }
                lastEditable = label;
                lastdropdownvalue = dropdownvalue;
            }

            $this.css('display', 'none');
            $this.parent().find('[name=lnkUpdate]').css('display', '');
            $this.parent().find('[name=lnkcancel]').css('display', '');
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                $(this).find('input[type=text]').css('border', '1px solid #c4c4c4');//1px solid #c4c4c4
                $(this).find('input[type = text]').show();
                if ($(this).find('input[type = text]').val() == undefined || $(this).find('input[type = text]').val() == "") { }
                else {
                    oldName = $(this).find('input[type = text]').val();
                }
                $(this).find('[name=DocumentName]').hide();
                classNAME = $(this).find('[name=lblClass]').html();
                $(this).find('[name=lblClass]').hide();
                //$('.Cancelclass,.saveClass,.ddlDropClass').hide();
                //$('.deletedocs1 ,.editdocs').show();
                //$(this).closest('td').find('.deletedocs1 ,.editdocs').hide();
                $(this).closest('td').find('.Cancelclass, .saveClass ,.ddlDropClass').show();
            });
        }

        var pageIndex = 1;
        var pageCount;
        var index = 1;

        function SetPageIndex(activeIndexx) {
            $("#<%= hdnActiveTabIndex.ClientID %>").val(activeIndexx);
            pageIndex = 1;
        }

        //Function to make AJAX call to the Web Method
        function GetRecords() {
            pageIndex++;
            if (pageIndex == 2 || pageIndex <= pageCount) {
                var activeIndex = $("#<%= hdnActiveTabIndex.ClientID %>").val();
                //Show Loader
                if ($("[id$=GridView" + activeIndex + "] .loader").length == 0) {
                    var row = $("[id$=GridView" + activeIndex + "] tr").eq(1).clone(true);
                    row.addClass("loader");
                    row.children().remove();
                    row.append('<td colspan = "999" style = "background-color:white"><img height="50px" id="loader" alt="" src="../images/Ellipsis-1.7s-200px.gif" /></td>');
                    $("[id$=GridView" + activeIndex + "]").append(row);
                }
                $.ajax({
                    type: "POST",
                    url: "FileFolderBox.aspx/GetDocumentsOnScroll",
                    data: '{folderId:' + newFolderID + ', dividerId:' + dividerId + ', pageIndex: ' + pageIndex + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnSuccess,
                    failure: function (response) {
                        alert(response.d);
                    },
                    error: function (response) {
                        alert(response.d);
                    }
                });
            }
        }

        function GetFilteredRecords() {
            pageIndex++;
            if (pageIndex == 2 || pageIndex <= pageCount) {
                var activeIndex = $("#<%= hdnActiveTabIndex.ClientID %>").val();
                //Show Loader
                if ($("[id$=GridView" + activeIndex + "] .loader").length == 0) {
                    var row = $("[id$=GridView" + activeIndex + "] tr").eq(1).clone(true);
                    row.addClass("loader");
                    row.children().remove();
                    row.append('<td colspan = "999" style = "background-color:white"><img height="50px" id="loader" alt="" src="Ellipsis-1.7s-200px.gif" /></td>');
                    $("[id$=GridView" + activeIndex + "]").append(row);
                }
                $.ajax({
                    type: "POST",
                    url: "FileFolderBox.aspx/GetFilteredDocumentsOnScroll",
                    data: '{folderId:' + newFolderID + ', dividerId:' + dividerId + ', name:"' + $('#<%= txtName.ClientID %>').val() + '", className:"' + $('#<%= txtClass.ClientID %>').val() + '", fromDate: "' + $('#<%= txtFDate.ClientID %>').val() + '", toDate:"' + $('#<%= txtTDate.ClientID %>').val() + '", index:"' + $('#<%= txtIndex.ClientID %>').val() + '", pageIndex: ' + pageIndex + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnSuccess,
                    failure: function (response) {
                        alert(response.d);
                    },
                    error: function (response) {
                        alert(response.d);
                    }
                });
            }
        }

        //Function to recieve XML response append rows to GridView
        function OnSuccess(response) {
            var activeIndex = $("#<%= hdnActiveTabIndex.ClientID %>").val();
            var xmlDoc = $.parseXML(response.d);
            var xml = $(xmlDoc);
            pageCount = parseInt(xml.find("PageCount").eq(0).find("PageCount").text());
            var documents = xml.find("Documents");
            $("[id$=GridView" + activeIndex + "] .loader").remove();
            documents.each(function () {
                var document = $(this);
                var row = $("[id$=GridView" + activeIndex + "] tr").eq(1).clone(true);
                var docId = document.find("DocumentID").text();
                var docOrder = document.find("DocumentOrder").text();
                var pathName = document.find("PathName").text();
                var extn = document.find("FileExtention").text();
            //$("#<%= hdnExtension.ClientID %>").val(extn);
                var date = new Date(document.find("DOB").text());
                var newDate = ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getDate() + '.' + date.getFullYear();
                var stringifyData = "{\"DocId\":" + parseInt(docId) + ",\"DocOrder\":" + parseInt(docOrder) + "}";
                $(".name", row).html(document.find("Name").text());
                $(".documentId", row).html(document.find("DocumentID").text());
                $(".documentOrder", row).html(document.find("DocumentOrder").text());
                $(".dividerId", row).html(document.find("DividerId").text());
                $(".pathName", row).html(document.find("PathName").text());
                $(".fileExtention", row).html(document.find("FileExtention").text());
                $(".dob", row).html(newDate);
                $(".lblClass", row).html(document.find("Class").text());
                var name = document.find("Name").text();
                var displayName = document.find("DisplayName").text();
                var className = document.find("Class").text();
                var fullPathName = pathName + "\\" + getFileName(name + ".pdf");
                var href = $(row).find(".ic-download").attr("href").split("=")[0];
                var oldPath = pathName + "\\" + getFileName(name) + ".pdf";
                oldPath = oldPath.replace(/\\/g, "/");
                $(row).find(".ic-download").attr("href", href + "=" + docId);
                $(row).find("[id$=hdnPath]").val(fullPathName);
                $(row).find("[id$=hdndocId]").val(docId);
                $(row).find("[id$=hdnValues]").val(stringifyData);
                $(row).find("[id$=hdnPathIndex]").val(fullPathName);
                $(row).find("[id$=lblDocumentName]").text(displayName);
                $(row).find("[id$=txtDocumentName]").val(displayName)
                var addToTabId = "addToTabId" + index;
                var txtDocumentNameId = "txtDocumentName" + index;
                var ddlDropOrderId = "ddlDropOrder" + index;
                var ddlDropMoveId = "ddlDrpMove" + index;
                var ddlDropClassId = "ddlDropClass" + index;
                var chkStatusId = "chkStatus" + index;
                var isChecked = $('.chkSelectAll').children().first().prop("checked");
                var lnkEditId = "lnkEdit" + index;
                var confirmButtonExtenderId = "ConfirmButtonExtender" + index;
                $(row).find("[id$=ConfirmButtonExtender2_CBE_MPE_Placeholder]").removeAttr("id").attr("id", confirmButtonExtenderId);
                $(row).find(".txtDocumentName").removeAttr("id").attr("id", txtDocumentNameId);
                $(row).find(".chkList").children().first().removeAttr("id").attr("id", chkStatusId);
                $(row).find("#" + chkStatusId).prop("checked", isChecked);
                $(row).find(".lnkBtnAddToTab").removeAttr("id").attr("id", addToTabId);
                $(row).find(".ddlDropOrder").removeAttr("id").attr("id", ddlDropOrderId);
                $(row).find(".ddlDropMove").removeAttr("id").attr("id", ddlDropMoveId);
                $(row).find(".ddlDropClass").removeAttr("id").attr("id", ddlDropClassId);
                $(row).find(".ddlDropClass").val(className);
                $(row).find(".ic-edit").removeAttr("id").attr("id", lnkEditId);
                $(row).find("#" + confirmButtonExtenderId).attr("onclick", "javascript:WebForm_DoPostBackWithOptions(new WebForm_PostBackOptions('" + confirmButtonExtenderId + "', '', true, '', '', false, false))");
                $(row).find(".ic-move").attr("onclick", "showMove(" + docOrder + ", '" + ddlDropOrderId + "','" + ddlDropMoveId + "'," + dividerId + ")");
                $(row).find(".ic-order").attr("onclick", "showOrder(" + docOrder + ", '" + ddlDropOrderId + "','" + ddlDropMoveId + "'," + dividerId + ")");
                $(row).find(".ddlDropOrder").attr("onchange", "OnchangeOrder(" + docOrder + ", $('#" + ddlDropOrderId + "')[0]," + dividerId + ", " + className + ")");
                $(row).find(".ddlDropClass").attr("onchange", "OnchangeClass(" + className + ", $('#" + ddlDropClassId + "')[0]," + dividerId + ", " + docId + ",'" + oldPath + "'," + docOrder + ")");
                $(row).find(".ddlDropMove").attr("onchange", "OnMoveOrder(" + docId + ", '" + ddlDropMoveId + "'," + dividerId + ", '" + oldPath + "'," + className + ")");
                $(row).find(".ic-share").attr("onclick", "return OnShowShare(" + docId + ",'" + oldPath + "');")
                $(row).find(".cancelDoc").attr("onclick", "CancelOrderMove(" + docOrder + ", '" + ddlDropOrderId + "','" + ddlDropMoveId + "'," + dividerId + ");")
                $(row).find(".newSave").attr("onclick", "OnSaveRename(" + docId + ", " + dividerId + ",'" + oldPath + "','" + txtDocumentNameId + "'," + docOrder + "," + className + ");")
                $("[id$=GridView" + activeIndex + "]").append(row);
                index++;
            });

            if ($("#<%= hdnIsScrollable.ClientID %>").val() == "true") {
                var length = $("[id$=GridView" + activeIndex + "]").find("tr").length - 1;
                if (length < 7 && pageCount >= pageIndex) { $("[id$=GridView" + activeIndex + "]").parent().parent().css("max-height", "300px") }
                else if (length < 9 && pageCount >= pageIndex) { $("[id$=GridView" + activeIndex + "]").parent().parent().css("max-height", "400px") }
                else if (length < 11 && pageCount >= pageIndex) { $("[id$=GridView" + activeIndex + "]").parent().parent().css("max-height", "450px") }
                else if (length > 10) { $("[id$=GridView" + activeIndex + "]").parent().parent().css("max-height", "450px") }
            }
        }

        function getFileName(fileName) {
            var encodeName = encodeURI(fileName);
            encodeName = encodeName.replace(/%/g, "$").replace(/"?"/g, "$c3$b1");
            return encodeName;
        }

        function OpenDeleteModal(ele) {
            showLoader();
            disableScrollbar();
            $("#<%= hdnDeleteId.ClientID %>").val($(ele).parent().siblings().find("[id$=hdndocId]").val());
            $("#DivDeleteConfirm").show();
            return false;
        }

        function HideDeleteModal() {
            $("#DivDeleteConfirm").hide();
        }

        function CloseDeleteModal() {
            hideLoader();
            enableScrollbar();
            $("#<%= textPassword.ClientID %>").val("");
            $("#<%= hdnDeleteId.ClientID %>").val("");
            $("#DivDeleteConfirm").hide();
        }

        function cancleFileName(ele) {
            var $this = $(ele);
            var LabelName = '';
            var FileName = '';
            var test = '';
            $this.css('display', 'none');
            $this.parent().find('[name=lnkUpdate]').hide();
            $this.parent().find('[name=lnkEdit]').show();
            $this.closest('tr').find('.ddlDropClass').hide();
            //$(this).find('[name=lblClass]').text(targetClass);

            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                var len = 0;
                len = $(this).find('input[type=text]').length;
                if (len > 0) {
                    LabelName = $(this).find('[name=DocumentName]').html();
                    $(this).find('input[type = text]').val(LabelName);
                    $(this).find('input[type = text]').hide();
                    $(this).find('[name=DocumentName]').show();
                }
                else {
                    $(this).find('[name=lblClass]').show();
                    $(this).find('[name=lblClass]').text(classNAME);
                }
            });
        }

        function OnSaveRename(documentid, dividerid, oldpath, txtRenameObj, documentorder, className) {
            oldPath = oldpath;
            documentId = documentid;
            newDividerId = dividerid;
            oldDividerId = dividerid;
            documentOrder = documentorder;
            if (targetClass == undefined || targetClass == "") {
                classname1 = className;
            }
            else {
                classname1 = targetClass;
            }
            newname = $('#' + txtRenameObj)[0].value;

            if (newname == undefined || newname == "") {

                $('.txtDocumentName').css('border', '1px solid red');
                return false;
            }
            else {
                $('.txtDocumentName').css('border', '1px solid c4c4c4');
            }
            showLoader();
            $.ajax(
                {
                    type: "POST",
                    url: '../EFileFolderJSONService.asmx/MoveRenameFile',
                    data: "{ newDividerId:'" + newDividerId + "', oldPath:'" + oldPath + "', oldDividerId:'" + oldDividerId + "', documentId:'" + documentId + "', documentOrder:'" + documentOrder + "', isMove:'" + 0 + "', renameFile:'" + newname + "',className:'" + classname1 + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        hideLoader();
                        clearSaerchTextbox();
                        SetPageIndex($("#<%= hdnActiveTabIndex.ClientID %>").val()); __doPostBack('', 'RefreshGrid@' + oldDividerId);
                    },
                    error: function (result) {
                        hideLoader();
                    }
                });
        }

        function OnSplitShare() {
            if (GetSplitPdfValue()) {
                enableScrollbar();
                $("#overlay").hide();
                var popup = document.getElementById("preview_popup");
                popup.style.display = 'none';
                $("#pdfLbl").show();
                $('#<%= pdfName.ClientID %>').show();
                $('#dialog-share-book').dialog("open");
                $('#<%= email.ClientID %>').val('<%=Sessions.SwitchedEmailId%>');
                $('#<%= emailto.ClientID %>').val('');
                $('#<%= nameto.ClientID %>').val('');
                $('#<%= title.ClientID %>').val('');
                $('#<%= pdfName.ClientID %>').val('');
                $('#<%= content.ClientID %>').val('');
                $('#dialog-share-book input[name="linkexpiration"]').val('<%= Sessions.ExpirationTime %>');
                $('#<%= emailto.ClientID %>').css('border', '1px solid black');
            }
        }

        function OnMultipleShare() {
            if (GetSelectedFiles()) {
                OnShowShare(0, null, true);
            }
            return false;
        }

        function OnShowShare(documentid, path, isMultiple) {
            if (!isMultiple) {
                $('#<%= DocumentIDs.ClientID %>').val(documentid);
                oldPath = path;
            }
            $('#dialog-share-book').dialog("open");
            $("#pdfLbl").hide();
            $('#<%= pdfName.ClientID %>').hide();
            $('#<%= email.ClientID %>').val('<%=Sessions.SwitchedEmailId%>');
            $('#<%= emailto.ClientID %>').val('');
            $('#<%= nameto.ClientID %>').val('');
            <%-- $('#<%= title.ClientID %>').val('');--%>
            $('#<%= pdfName.ClientID %>').val('');
            $('#<%= content.ClientID %>').val('');
            $('#dialog-share-book input[name="linkexpiration"]').val('<%= Sessions.ExpirationTime %>');
            $('#<%= emailto.ClientID %>').css('border', '1px solid black');
            return false;
        }

        function ShareFolder(isDivider) {
            $('#dialog-share-book1').dialog("open");
            $('#<%= email1.ClientID %>').val('<%=Sessions.SwitchedEmailId%>');
            $('#<%= emailto1.ClientID %>').val('');
            $('#<%= nameto1.ClientID %>').val('');
            $('#<%= content1.ClientID %>').val('');
            $('#<%= isDivider.ClientID %>').val(isDivider);
            $('#dialog-share-book1 input[name="linkexpiration"]').val('<%= Sessions.ExpirationTime %>');
            $('#<%= emailto1.ClientID %>').css('border', '1px solid black');
            return false;
        }

        function SaveMailFolder() {
            var email = $('#<%= email1.ClientID %>').val();
                       var emailsto = $('#<%= emailto1.ClientID %>').val();
                       var nameto = $('#<%= nameto1.ClientID %>').val();
                       var title = $('#<%= title1.ClientID %>').val();
                       var isDivider = $('#<%= isDivider.ClientID %>').val();
                       var emailList = "";
                       if (emailsto.length > 0) {
                           emailList = emailsto.split(';');
                           for (var i = 0; i < emailList.length; i++) {
                               var test = "";
                               if (!CheckEmailValidation(emailList[i])) {
                                   alert("Please enter valid Email separated by ';' !")
                                   $('#<%= emailto.ClientID %>').css('border', '1px solid red');
                                   hideLoader();
                                   return false;
                               }
                           }
                       }
                       $('#<%= emailto1.ClientID %>').css('border', '1px solid black');
                       var content = $('#<%= content1.ClientID %>').val();
                       var Isvalid = false;
                       if (emailsto == "") {
                           CheckValidation($('#<%= emailto1.ClientID %>'));
                  CheckEmailValidation($('#<%= emailto1.ClientID %>'))
                           return false;
                       }

                       var nameToList = "";
                       if (nameto.length > 0) {
                           var isValid = /^[a-zA-Z\;\s]+$/.test(nameto)
                           if (isValid)
                               nameToList = nameto.split(';');
                           else {
                               alert("Please enter valid ToName separated by ';' !")
                               $('#<%= nameto1.ClientID %>').css('border', '1px solid red');
                               hideLoader();
                               return false;
                           }
                       }
                       if (emailList.length != nameToList.length) {
                           alert("Number of Emails should match with number of ToNames!");
                           $('#<%= emailto1.ClientID %>').css('border', '1px solid red');
                  $('#<%= nameto1.ClientID %>').css('border', '1px solid red');
                           hideLoader();
                           return false;
                       }

                       var emailTo = [];

                       for (var i = 0; i < emailList.length; i++) {
                           emailTo.push({ "Email": emailList[i], "ToName": nameToList[i] });
                       }

                       showLoader();
                       $('#floatingCirclesG').show();
                       $.ajax(
                           {
                               type: "POST",
                               url: '../EFileFolderJSONService.asmx/ShareMailForFileFolderBox',
                               data: "{ strSendMail:'" + email + "', strToMail:'" + JSON.stringify(emailTo) + "', folderId:'" + $('#<%= hdnFolderId.ClientID %>').val() + "', strMailInfor:'" + content + "',SendName:'" + "" + "', ToName:'" + nameto + "',MailTitle:'" + title + "',dividerId:'" + dividerId + "',isDivider:'" + isDivider + "'}",
                      contentType: "application/json; charset=utf-8",
                      dataType: "json",
                      success: function (data) {
                          var id = data.d;
                          var title, message;
                          if (id === false) {
                              title = "Sharing EdFile document error";
                              message = "This document has not been shared successfully."
                          }
                          else {
                              title = "Sharing EdFile document success";
                              message = "This document has been shared successfully."
                          }

                          $('#dialog-message-all1').dialog({
                              title: title,
                              open: function () {
                                  $("#spanText1").text(message);
                              }
                          });

                          $('#dialog-message-all1').dialog("open");

                          if (id > 0)
                              $('#dialog-share-book1').dialog("close");
                          hideLoader();
                      },
                      fail: function (data) {

                          bookshelf.loaded.apply();

                          $('#dialog-message-all1').dialog({
                              title: "Sharing EdFile document error",
                              open: function () {
                                  $("#spanText1").text("This document has not been shared successfully.");
                              }
                          });
                          $('#dialog-message-all1').dialog("open");

                          hideLoader();
                      }
                  });
        }

        function SaveMailLog(documentId) {
            if (documentId > 0)
                CreateDocumentsAndFolderLogs(newFolderID, dividerId, documentId, null, null, ActionEnum.Share, null);
        }
        function SaveMail() {
            var email = $('#<%= email.ClientID %>').val();
            //var name = $('#dialog-share-book input[name="myname"]').val();
            var emailsto = $('#<%= emailto.ClientID %>').val();
            var nameto = $('#<%= nameto.ClientID %>').val();
            var pdfName = $('#<%= pdfName.ClientID %>').val();
            var title = $('#<%= title.ClientID %>').val();
            var emailList = "";
            if (emailsto.length > 0) {
                emailList = emailsto.split(';');
                for (var i = 0; i < emailList.length; i++) {
                    var test = "";
                    if (!CheckEmailValidation(emailList[i])) {
                        alert("Please enter valid Email separated by ';' !")
                        $('#<%= emailto.ClientID %>').css('border', '1px solid red');
                        hideLoader();
                        return false;
                    }
                }
            }
            $('#<%= emailto.ClientID %>').css('border', '1px solid black');
            var content = $('#<%= content.ClientID %>').val();
            var Isvalid = false;

            //$('#dialog-share-book input[name="myname"]').keyup(function () { CheckValidation($('#dialog-share-book input[name="myname"]')) });
            //$('#dialog-share-book input[name="email"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="email"]')) });

            //$('#dialog-share-book input[name="emailto"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="emailto"]')) });

            //if (email == "" || name == "" || emailto == "") {
            if (emailsto == "") {
                //CheckValidation($('#dialog-share-book input[name="email"]'));
                //CheckValidation($('#dialog-share-book input[name="myname"]'));
                CheckValidation($('#<%= emailto.ClientID %>'));
                //CheckEmailValidation($('#dialog-share-book input[name="email"]'));
                CheckEmailValidation($('#<%= emailto.ClientID %>'))
                return false;
            }

            var nameToList = "";
            if (nameto.length > 0) {
                //var isValid = /^[a-zA-Z0-9!@#\$%\^\&*\,\)\(;+=._-]+$/.test(nameto);
                var isValid = /^[a-zA-Z\;\s]+$/.test(nameto)
                if (isValid)
                    nameToList = nameto.split(';');
                else {
                    alert("Please enter valid ToName separated by ';' !")
                    $('#<%= nameto.ClientID %>').css('border', '1px solid red');
                    hideLoader();
                    return false;
                }
            }
            if (emailList.length != nameToList.length) {
                alert("Number of Emails should match with number of ToNames!");
                $('#<%= emailto.ClientID %>').css('border', '1px solid red');
                $('#<%= nameto.ClientID %>').css('border', '1px solid red');
                hideLoader();
                return false;
            }

            var emailTo = [];

            for (var i = 0; i < emailList.length; i++) {
                emailTo.push({ "Email": emailList[i], "ToName": nameToList[i] });
            }

            var url = "<%= ConfigurationManager.AppSettings["VirtualDir"] %>" + oldPath;
            url = url.replace(/\\/g, "/");

            //var src = 'http://docs.google.com/gview?url=' + window.location.origin + url + '&embedded=true';

            var src = '';
            showLoader();
            $('#floatingCirclesG').show();
            if ($("#pdfLbl").is(":visible")) {
                if (pdfName == "" || pdfName == undefined) {
                    alert("Please enter new document name");
                    $("#pdfName").focus();
                    hideLoader();
                    return false;
                }
                else if (!/^[a-z\d][a-z\d\()_\-\s]+$/i.test(pdfName)) {
                    alert("Document name can not contain special characters");
                    $("#pdfName").focus();
                    hideLoader();
                    return false;
                }
                $('#dialog-share-book').dialog("close");
                $("#<%= hdnNewPdfName.ClientID %>").val(pdfName);
                $("#<%= hdnFromEmail.ClientID %>").val(email);
                $("#<%= hdnToEmail.ClientID %>").val(JSON.stringify(emailTo));
                $("#<%= hdnToName.ClientID %>").val(nameto);
                $("#<%= hdnMailContent.ClientID %>").val(content);
                $("#<%= hdnMailTitle.ClientID %>").val(title);
                $("#<%= hdnSplitBtn.ClientID %>").click();
            }
            else {
                $.ajax(
                    {
                        type: "POST",
                        url: '../EFileFolderJSONService.asmx/ShareMail',
                        data: "{ strSendMail:'" + email + "', strToMail:'" + JSON.stringify(emailTo) + "', documentIds:'" + $('#<%= DocumentIDs.ClientID %>').val() + "', strMailInfor:'" + content + "',SendName:'" + "" + "', ToName:'" + nameto + "',MailTitle:'" + title + "',Path:'" + src + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var id = data.d;
                            var title, message;
                            if (id === false) {
                                title = "Sharing EdFile document error";
                                message = "This document has not been shared successfully."
                            }
                            else {
                                title = "Sharing EdFile document success";
                                message = "This document has been shared successfully."
                            }

                            $('#dialog-message-all').dialog({
                                title: title,
                                open: function () {
                                    $("#spanText").text(message);
                                }
                            });

                            $('#dialog-message-all').dialog("open");

                            if (id > 0)
                                $('#dialog-share-book').dialog("close");
                            hideLoader();
                        },
                        fail: function (data) {

                            bookshelf.loaded.apply();

                            $('#dialog-message-all').dialog({
                                title: "Sharing EdFile document error",
                                open: function () {
                                    $("#spanText").text("This document has not been shared successfully.");
                                }
                            });
                            $('#dialog-message-all').dialog("open");

                            hideLoader();
                        }
                    });
            }
        }
        function CheckValidation(fieldObj) {
            if (fieldObj.val() == "") {
                fieldObj.css('border', '1px solid red');
            }
            else {
                fieldObj.css('border', '1px solid black');
            }
        }

        function CheckEmailValidation(value) {

            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)) {
                return true;
            }
            else {
                return false;
            }
        }

        //function CheckEmailValidation(fieldObj) {
        //    var x = fieldObj.val();

        //    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(x)) {
        //        fieldObj.css('border', '1px solid black');
        //        return true;
        //    }
        //    else {
        //        fieldObj.css('border', '1px solid red');

        //        return false;
        //    }
        //}

        function clearSaerchTextbox() {
            $('#<%= txtName.ClientID %>').val("");
            $('#<%= txtClass.ClientID %>').val("");
            $('#<%= txtTDate.ClientID %>').val("");
            $('#<%= txtFDate.ClientID %>').val("");
            $('#<%= txtIndex.ClientID %>').val("");
        }


        //$('select[id$=ctl00_ContentPlaceHolder1_drpSearch]').change(function () {
        function changeSearch() {
            setVisibility();
            $('select[id=<%=drpSearch.ClientID%>]').change(function () {
                clearSaerchTextbox();
                setVisibility();
            });
        }

        function setVisibility() {
            if ($('#<%= drpSearch.ClientID %>').val() == 1) {
                $('#<%= txtName.ClientID %>').show();
                $('#<%= txtClass.ClientID %>').val('').hide();
                $('#<%= txtTDate.ClientID %>').val('').hide();
                $('#<%= txtFDate.ClientID %>').val('').hide();
                $('#<%= txtIndex.ClientID %>').val('').hide();
                $('#<%= txtKeyword.ClientID %>').val('').hide();
            }
            else if ($('#<%= drpSearch.ClientID %>').val() == 2) {
                $('#<%= txtName.ClientID %>').val('').hide();
                $('#<%= txtIndex.ClientID %>').val('').hide();
                $('#<%= txtClass.ClientID %>').show();
                $('#<%= txtTDate.ClientID %>').val('').hide();
                $('#<%= txtFDate.ClientID %>').val('').hide();
                $('#<%= txtKeyword.ClientID %>').val('').hide();
            }
            else if ($('#<%= drpSearch.ClientID %>').val() == 3) {
                $('#<%= txtName.ClientID %>').val('').hide();
                $('#<%= txtClass.ClientID %>').val('').hide();
                $('#<%= txtIndex.ClientID %>').val('').hide();
                $('#<%= txtTDate.ClientID %>').show();
                $('#<%= txtFDate.ClientID %>').show();
                $('#<%= txtKeyword.ClientID %>').val('').hide();
            }
            else if ($('#<%= drpSearch.ClientID %>').val() == 4) {
                $('#<%= txtName.ClientID %>').val('').hide();
                $('#<%= txtClass.ClientID %>').val('').hide();
                $('#<%= txtTDate.ClientID %>').val('').hide();
                $('#<%= txtFDate.ClientID %>').val('').hide();
                $('#<%= txtIndex.ClientID %>').show();
                $('#<%= txtKeyword.ClientID %>').val('').hide();
            }
            else if ($('#<%= drpSearch.ClientID %>').val() == 5) {
                $('#<%= txtName.ClientID %>').val('').hide();
                $('#<%= txtClass.ClientID %>').val('').hide();
                $('#<%= txtTDate.ClientID %>').val('').hide();
                $('#<%= txtFDate.ClientID %>').val('').hide();
                $('#<%= txtIndex.ClientID %>').val('').hide();
                $('#<%= txtKeyword.ClientID %>').show();
            }

        }

        function showPreview(ele) {
            var id = JSON.parse($(ele).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId;
            var fileExtension = $(ele).closest('tr').find("input[type='hidden'][id$=hdnFileExtension]").val();
            if (fileExtension == 14) {
                window.open("PDFDownloader.aspx?ID=" + id, "_blank");
            }
            else {
                var data = { "documentId": JSON.parse($(ele).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId }
                $.ajax({
                    type: "POST",
                    url: "FileFolderBox.aspx/GetIndexValues",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result.d.length > 0) {
                            $("#hndDividerIdIndex").val(JSON.parse($(ele).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId);
                            var $this = $(ele);
                            var path = '/' + $($this).parent().find('[name*=hdnPath]').val();
                            path = path.replace(/\\/g, "/");
                            var newpath = $this.parent().find('[name*=hdnPath]').val();
                            $('#<%= hdnPath.ClientID %>').val(newpath);
                            var docId = JSON.parse($(ele).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId;
                            $("#<%= hdnFileId.ClientID %>").val(docId);
                            var fileName = $(ele)[0].innerHTML + ".pdf";
                            $('#<%= hdnFileName.ClientID %>').val(fileName);
                            if (!window.location.origin) {
                                window.location.origin = window.location.protocol + "//"
                                    + window.location.hostname
                                    + (window.location.port ? ':' + window.location.port : '');
                            }
                            var src = './Preview.aspx?data=' + window.location.origin + path + "?v=" + "<%= DateTime.Now.Ticks%>";
                            if (window.location.origin.indexOf('https://') > -1)
                                src = './Preview.aspx?data=' + window.location.origin + path + "?v=" + "<%= DateTime.Now.Ticks%>";
                            overlay.style.display = 'block';
                            popup.style.display = 'block';
                            $('#reviewContent').attr('src', src);
                            $('#preview_popup').focus();
                            disableScrollbar();
                            var index = 0;
                            var arrIndex = [];
                            $.each($.parseJSON(result.d), function (i, e) {
                                arrIndex.push(e);
                            });
                            $("#tblIndexPreview").find("tr:gt(0)").remove();
                            var row = $(".indexPreviewRow");
                            $(row).find("#txtIndexKey").val("");
                            $(row).find("#txtIndexValue").val("");
                            $(row).find("#btnAddIndex").show();
                            $(row).find("#btnRemoveIndex").hide();
                            if (arrIndex.length > 0)
                                $(row).find(".hdnIndexId").val(arrIndex[0].IndexId);
                            else
                                $(row).find(".hdnIndexId").val(0);
                            for (var i = 0; i < arrIndex.length; i++) {
                                if (i == 0) {
                                    SetIndexPreviewingValues(arrIndex[i], true);
                                }
                                else
                                    AppendIndexPreviewingDetails(arrIndex[i], i);
                            }
                            CreateDocumentsAndFolderLogs(newFolderID, dividerId, id, null, null, ActionEnum.Preview, null);
                            return false;
                        }
                    }
                });
            }
        }

        function checkAllThumbnail() {
            var selectAllValue = $("#btnCheckedAll").is(":checked");

            $('iFrame').contents().find('.check').each(function (k, v) {
                if (selectAllValue)
                    $(v).attr('checked', true);
                else
                    $(v).attr('checked', false);
            });
        }

        function ShowAddToTabPopup() {
            if (GetSplitPdfValue()) {
                $.ajax(
                    {
                        type: "POST",
                        url: '../FlexWebService.asmx/GetFoldersData',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $('<option />', { value: 0, text: "Select a file" }).appendTo($("#comboboxSplit")); $.each(data.d, function (i, text) {
                                $('<option />', { value: i, text: text }).appendTo($("#comboboxSplit"));
                            });
                            $("#comboboxSplit").chosen({ width: "315px" });

                            ChangeDivider();
                        },
                        fail: function (data) {
                        }
                    });
                var popup = document.getElementById("preview_popup");
                popup.style.display = 'none';

                var popup = document.getElementById("preview_popup_split");
                popup.style.display = 'block';
            }
        }

        function GetSplitPdfValue() {
            var pageNumber = '';
            if ($('iFrame').contents().find('.check').length > 0) {
                $('iFrame').contents().find('.check').each(function (k, v) {
                    var val = $(v).is(":checked");
                    if (val) {
                        if (pageNumber == '')
                            pageNumber += $(v).attr('data-value') + ',';
                        else
                            pageNumber += $(v).attr('data-value') + ',';
                    }

                });
                pageNumber = pageNumber.substring(0, pageNumber.length - 1);

                if (pageNumber.length <= 0) {
                    alert("Please select at least one page.");
                    return false;
                }

                $("#" + '<%= hdnPageNumbers.ClientID %>').val(pageNumber);
            }
            else {
                alert('Please select thumbnail');
                return false;
            }
            return true;
        }


        function ChangeDivider() {
            $("#comboboxDividerSplit").html('');
            $("#comboboxDividerSplit").chosen("destroy");
            var selectedFolder = $("#comboboxSplit").chosen().val();
            $.ajax(
                {
                    type: "POST",
                    url: '../FlexWebService.asmx/GetDividersByEFF',
                    data: "{eff:" + parseInt(selectedFolder) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $('<option />', { value: 0, text: "Select a tab" }).appendTo($("#comboboxDividerSplit"));
                        $.each(data.d, function (i, text) {
                            $('<option />', { value: i, text: text }).appendTo($("#comboboxDividerSplit"));
                        });
                        $("#comboboxDividerSplit").chosen({ width: "315px" });
                    },
                    fail: function (data) {
                    }
                });
        }

        function ShowConfirm(message) {
            if (confirm(message)) {
                window.location.href = window.location.href;
            }

            else {
                window.location.href = window.location.origin + '/Office/FileFolderBox.aspx?id=' + $('#<%= hdnSelectedFolderID.ClientID%>').val();
            }
        }

        $("#<%= btnSaveSplitPdf.ClientID%>").click(function () {
            var fileName = $("#" + '<%= txtNewPdfName.ClientID%>').val();
            if (fileName == '' || fileName == undefined) {
                alert("Please enter file name!");
                return false;
            }
            var selectedFolder = $("#comboboxSplit").chosen().val();
            var selectedDivider = $("#comboboxDividerSplit").chosen().val();
            if (selectedFolder == "" || selectedFolder == undefined || selectedFolder == "0") {
                alert("Please select folder");
                return false;
            }
            else if (selectedDivider == "" || selectedDivider == undefined || selectedDivider == "0") {
                alert("Please select divider");
                return false;
            }
            var folderValue = $('.chosen-single').text();
            $('#<%= hdnSelectedFolderID.ClientID%>').val(selectedFolder);
            $('#<%= hdnSelectedDividerID.ClientID%>').val(selectedDivider);
            $('#<%= hdnSelectedFolderText.ClientID%>').val(folderValue);

            return true;
        });


        $('#popupclose_split, #btnCancelAddToTabSplit, #popupclose_splitWA, #btnCancelAddToWA').click(function (e) {
            e.preventDefault();
            $(this).closest('.popup-mainbox').hide();
            $('#overlay').hide();
            enableScrollbar();
        });

        $("#comboboxSplit").change(function (event) {
            ChangeDivider();
        });

        function OpenIndexPopup(ele) {

            var id = JSON.parse($(ele).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId;
            var fileExtension = $(ele).closest('tr').find("input[type='hidden'][id$=hdnFileExtension]").val();
            // $(ele).parent().find('[name*=hndDividerIdIndex]').val(JSON.parse($(ele).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId);
            var data = { "documentId": JSON.parse($(ele).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId }
            $.ajax({
                type: "POST",
                url: "FileFolderBox.aspx/GetIndexValues",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (result.d.length > 0) {
                        $("#hndDividerIdIndex").val(JSON.parse($(ele).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId);
                        var $this = $(ele);
                        var path = '/' + $($this).parent().find('[name*=hdnPath]').val();
                        path = path.replace(/\\/g, "/");

                        if (!window.location.origin) {
                            window.location.origin = window.location.protocol + "//"
                                + window.location.hostname
                                + (window.location.port ? ':' + window.location.port : '');
                        }
                        var src = './Preview.aspx?data=' + window.location.origin + path + "?v=" + "<%= DateTime.Now.Ticks%>";
                        if (window.location.origin.indexOf('https://') > -1)
                            src = './Preview.aspx?data=' + window.location.origin + path + "?v=" + "<%= DateTime.Now.Ticks%>";

                        $('#iframeIndex').attr('src', src);
                        if (fileExtension == 14) {
                            $('#iframeIndex').css("display", "none");
                        }
                        else {
                            $('#iframeIndex').css("display", "block");

                        }
                        $('#PanelIndex,#overlay').show().focus();
                        disableScrollbar();

                        //$('#PanelIndex').show().focus();
                        //$("#floatingCirclesG").css('display', 'none');
                        var index = 0;
                        var arrIndex = [];
                        $.each($.parseJSON(result.d), function (i, e) {
                            arrIndex.push(e);
                        });
                        $("#tblIndex").find("tr:gt(0)").remove();
                        var row = $(".indexRow");
                        $(row).find("#txtKey").val("");
                        $(row).find("#txtValue").val("");
                        $(row).find("#btnAddIndexPair").show();
                        $(row).find("#btnRemoveIndexPair").hide();
                        if (arrIndex.length > 0)
                            $(row).find(".hdnIndexId").val(arrIndex[0].IndexId);
                        else
                            $(row).find(".hdnIndexId").val(0);
                        for (var i = 0; i < arrIndex.length; i++) {
                            if (i == 0) {
                                SetIndexValues(arrIndex[i], true);
                            }
                            else
                                AppendIndexDetails(arrIndex[i], i);
                        }
                        CreateDocumentsAndFolderLogs(newFolderID, dividerId, id, null, null, ActionEnum.DocIndex, null);
                        return false;
                    }
                }
            });
        }

        function AppendIndexDetails(indexPair, index) {
            CreateDynamicIndexTextBox(indexPair.IndexId, true);
            SetIndexValues(indexPair, false);
        }

        function SetIndexValues(indexPair, isRender) {
            var row = "";
            if (!isRender)
                row = document.getElementById("indexRow" + indexPair.IndexId);
            else
                row = $(".indexRow");
            $(row).find("#txtKey").val(indexPair.IndexKey);
            $(row).find("#txtValue").val(indexPair.IndexValue);
            $(row).find(".hdnIndexId").val(indexPair.IndexId);
        }

        function GetDynamicTextBox(value) {
            var test = '<td><input type="hidden" value ="0" class="hdnIndexId"></td>' +
                '<td><input type="text" ID = "txtKey" style="width:85%;height:120%;padding-left:20px;" MaxLength="20" placeholder="Key" /></td>' +
                '<td><input type="text" ID= "txtValue" style="width:85%;height:120%;padding-left:20px;" MaxLength="20" placeholder="Value" /></td>' +
            '<td><button id="btnAddIndexPair" type="button" style="height:35px;width:35px;"  OnClick="return AddIndexTextBoxes(this)"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/add-icon.png" title="Add"/> </button> </td>' +
            '<td><button id="btnRemoveIndexPair"  type="button" style="height:35px;width:35px;"  OnClick="return DisplayConfirmBox(this)"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" /> </button>'

            return test;
        }

        var indexRemoveEle = "";
        function DisplayConfirmBox(ele, isPreview) {
            if (!isPreview) {
                $("#PanelIndex").hide();
                $('#DivDeleteConfirmIndex,#overlay').show().focus();
                indexRemoveEle = ele;
            }
            else {
                $("#preview_popup").hide();
                $('#DivDeleteConfirmIndexPreview,#overlay').show().focus();
                indexRemoveEle = ele;
            }
            //  return false;
        }

        function AddIndexTextBoxes(ele) {
            var index = parseInt($($(ele).parent().parent().find('[class*=hdnIndexId]')).val());
            if ($(ele).closest('tr').find('#txtKey').val() == "") {
                alert("Please enter Key Value.");
                return false;
            }
            CreateDynamicIndexTextBox(index, false);
            return false;
        }

        function CreateDynamicIndexTextBox(index, isRender) {
            var tr = document.createElement('tr');
            tr.innerHTML = GetDynamicTextBox(index);

            if (!isRender)
                tr.setAttribute("id", "indexRow" + (index = index + 1));
            else
                tr.setAttribute("id", "indexRow" + index);
            // var body = $(parentElement).parent();
            $('table#tblIndex tr:last').after(tr);
            SetIndexButtonVisibility();

            //Allow maximum of 5 key,value pairs
            if ($('table#tblIndex tr').length == 5) {
                $(tr).find('#btnAddIndexPair').hide();
            }
        }

        var deleteIndexData = "";
        var deleteIndexPreviewData = "";
        function RemoveIndexTextBoxes() {
            $('#DivDeleteConfirmIndex').hide();
            $('#PanelIndex,#overlay').show().focus();
            if ($(indexRemoveEle).closest('tr').find('input.hdnIndexId').val() != 0) {
                var hdnId = $(indexRemoveEle).closest('tr').find('input.hdnIndexId').val();
                var test = deleteIndexData.indexOf(hdnId);
                if (test <= 0)
                    deleteIndexData = deleteIndexData + hdnId + ",";
            }
            $(indexRemoveEle).closest('tr').remove();
            SetIndexButtonVisibility();
            indexRemoveEle = "";
            disableScrollbar();
            return false;
        }

        $("#btnDeleteIndexCancel").click(function () {
            disableScrollbar();
            $('#DivDeleteConfirmIndex').hide();
            $('#PanelIndex,#overlay').show().focus();
        });

        $("#btnDeleteIndexPreviewCancel").click(function () {
            disableScrollbar();
            $('#DivDeleteConfirmIndexPreview').hide();
            $('#preview_popup,#overlay').show().focus();
        });

        function SetIndexButtonVisibility() {
            var rowCount = $('table#tblIndex tr').length;
            for (i = 0; i < rowCount; i++) {
                var $item = $($('table#tblIndex tr')[i]);

                if (i == (rowCount - 1)) {
                    $item.find('#btnRemoveIndexPair').show();
                    $item.find('#btnAddIndexPair').show();
                    if (i == 0) {
                        $item.find('#btnRemoveIndexPair').hide();
                    }
                }
                else if (i == 0) {
                    $item.find('#btnRemoveIndexPair').show();
                    $item.find('#btnAddIndexPair').hide();
                }
                else {
                    $item.find('#btnRemoveIndexPair').show();
                    $item.find('#btnAddIndexPair').hide();
                }
            }
        }

        $("#btnIndexCancel, #closeIndex").click(function (e) {
            e.preventDefault();
            $('#overlay').hide();
            $('#PanelIndex').hide().focus();
            $("#floatingCirclesG").css('display', 'none');
            $('#floatingCirclesG').addClass('hideControl');
            enableScrollbar();
            e.stopPropagation();
        });

        function disableScrollbar() {
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }

        var tempFolderLogId = 0;

        function folderLogEdit(folderLogId) {

            update = 1;
            tempFolderLogId = folderLogId;
            var folderId = newFolderID;
            obj = {};
            obj.folderId = folderId;
            obj.folderLogId = folderLogId;
            $.ajax({
                type: "POST",
                url: "FileFolderBox.aspx/GetFolderLogDetailById",
                data: JSON.stringify(obj),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (result.d.length > 0) {
                        $('#overlay').show();
                        $('#AddToFolderLog_popup').show().focus();
                        $("#floatingCirclesG").css('display', 'none');
                        var logName = result.d[0];
                        $(".Log").val(logName);
                        var questionList = $.parseJSON(result.d[1]);
                        if ($('#tblBRD tbody tr:last').find('input').attr('rank') == 1) {
                            $('#tblBRD tbody tr:last').find('input').attr("rank", 0);
                            $('#tblBRD tbody tr:last').find('input').removeAttr('rank');
                        }
                        $.each(questionList, function (i, e) {
                            AppendQuestionDetails(e, i)

                        });
                    }
                }
            });
        }

        function checkQuestion(currentQuestion) {
            var queCnt = $('.question').length - 1;
            if (queCnt == 0)
                return true;
            for (var i = 0; i < queCnt; i++) {
                var que = $('.question').eq(i).val();

                if (currentQuestion == que) {
                    alert("Last question is exist.Please enter another question.");
                    return false;
                }
            }
            return true;
        }


        $('#btnADDBRD').die().live('click', function () {
            var $newRow = $('#tblBRD tbody tr:last').clone(true);
            var question = $(this).closest('tr').find('.question').val();
            if (question == "") {
                alert("Please enter valid question.");
                return false;
            }
            var check = checkQuestion(question);
            if (check) {
                $('#tblBRD tbody tr:last').after($newRow);
                var addedRow = $('#tblBRD tbody tr:last').find('input');
                //$('#tblBRD tbody tr:last td:eq(0)').text('Question ' + ($('#tblBRD tbody tr').size()))
                $(addedRow).each(function (i) {
                    var name = '';
                    switch (i) {
                        case 0: name = 'tbxQuestion' + Math.random();
                    }
                    $(this).val('').prop('id', name).prop('name', name).attr("QuesId", 0);
                });
                $(this).after('<a href="javascript:;" id="btnDeleteBRD"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" /></a>');
                $(this).remove();
            }
        });



        $('#btnDeleteBRD').die().live('click', function () {
            var tblTRCount = $('#tblBRD tbody tr').size();
            if (tblTRCount != 1)
                $(this).parents().eq(1).remove();
        });




        function SetQuestions(items) {
            if (items) {
                var obj = JSON.parse(items);
                for (var i = 0; i < obj.length - 1; i++) {
                    $('#btnADDBRD').trigger('click');
                }
                $('#tblBRD tbody tr').each(function (i) {
                    $(this).find('input[type="text"]').val(obj[i]);
                });
            }
        }

        function clearTextbox() {
            $(".Log").val('');
            $.each($('.tblBRD'), function () {
                if ($('.tblBRD').length == 1)
                    $(this).find('.question').val('');
                else
                    $(this).remove();
            });
            var queCnt = $('.tblBRD').length;
            for (i = 0; i < queCnt - 1; i++) {
                if (i < queCnt) {
                    var Que = $('.tblBRD').eq(i).remove();
                }
            }
        }


        var check = 1;
        var update = 0;

        function checkfolderlogs() {
            var cnt = 0;
            if ($('.Log').val() != "") {
                var queCnt = $('.question').length;
                for (i = 0; i < queCnt; i++) {
                    var Que = $('.question').eq(i).val();
                    if (Que == "") {
                        cnt += 1;
                    }
                }
                if (cnt == queCnt && cnt > 0) {
                    check = 0;
                    alert("Please enter at least one question");
                }
            }
            else {
                cnt += 1;
                check = 0;
                alert("Please Enter Valid Folder Log Name.");
            }
            if (cnt == 0) {
                check = 1;
            }
        }



        $('.FolderLog').click(function (e) {

            var question = $('#tblBRD tbody tr:last').find('.question').val();
            var isValid = checkQuestion(question);
            if (!isValid) {
                return false;
            }
            var cnt = 0;
            e.preventDefault();
            e.stopPropagation();
            $('#<%= lblFailFolderLog.ClientID %>').hide();
            checkfolderlogs();
            if (check == 1) {
                if (update == 0) {
                    Savefolderlog(this);
                }
                else {
                    var que = [];
                    var queCnt = $('.question').length;
                    var folderLogName = $('.Log').val();
                    for (i = 0; i < queCnt; i++) {
                        var QuestionId = $('.question').eq(i).attr("QuesId");
                        var Que = $('.question').eq(i).val();
                        if (Que != "") {
                            que.push({ id: QuestionId, que: Que });
                        }
                    }
                    var data = JSON.stringify(que);
                    var folderId = newFolderID;
                //var folderLogId = $('#' + '<%=hdnFolderId.ClientID%>').val();
                    var folderLogId = tempFolderLogId;
                    $.ajax({
                        type: "POST",
                        url: "FileFolderBox.aspx/UpdateFolderLogDetails",
                        data: JSON.stringify({ FolderDetails: data, folderlogId: tempFolderLogId, folderId: folderId, folderLogName: folderLogName }),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (result) {

                            if (result.d == 0) {
                                var cntlog = $('.logName').length;
                                for (var i = 0; i < cntlog; i++) {
                                    if ($('.logName').eq(i - 1).attr('logid') == tempFolderLogId) {
                                        $('.logName').eq(i - 1).html(folderLogName);
                                    }
                                }
                                $('#overlay').hide();
                                $('#AddToFolderLog_popup').hide().focus();
                                $("#floatingCirclesG").css('display', 'none');
                                alert("Folder Log Updated Suceesfully");
                                clearTextbox();
                            }
                            else {
                                $('#<%= lblFailFolderLog.ClientID %>').show();
                                $('#<%= lblFailFolderLog.ClientID %>').html("Folder log name already exist.");
                            }
                        }
                    });
                }

            }
            else {
                return false;
            }

        });


        function Savefolderlog() {
            var logs = [];
            var folderId = '<%= this.EffID_FFB %>';
            var folderLogName = $('.Log').val();
            var queCnt = $('.question').length;
            for (i = 0; i < queCnt; i++) {
                var Que = $('.question').eq(i).val();
                logs.push({ que: Que });
            }
            var data = JSON.stringify(logs);
            $.ajax({
                type: "POST",
                url: "FileFolderBox.aspx/insertFolderLog",
                data: JSON.stringify({ Question: data, FolderLogName: folderLogName, FolderId: folderId }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (result.d) {
                        if (result.d == "-1") {
                            $('#<%= lblFailFolderLog.ClientID %>').show();
                            $('#<%= lblFailFolderLog.ClientID %>').html("Folder log name already exist.");
                        }
                        else {

                            if (result.d > 0) {

                                var addLogCount = $('.folderLogTable').find('tr').length;
                                addLogCount++;
                                //var addLogCount = $('.addLog').length;
                                //if ($('.firstLog').length > 0) {
                                //    $('.addLog').removeClass('firstLog');
                                //    $('.addLog').attr('value', result.d);
                                //    $('.addLog').click(function (e) {
                                //        e.preventDefault();
                                //        window.open("CheckListQuestionsAndAnswers.aspx?ID=" + folderId + "&FolderLogId=" + $(this).attr('value'), "toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no");
                                //    });
                                //}
                                //else {
                                //    var object = $('.addLog').eq(addLogCount - 1).clone();
                                //    object.attr('value', result.d);
                                //    $('.addLog').eq(addLogCount - 1).after(object);
                                //    $(object).click(function (e) {
                                //        e.preventDefault();
                                //        window.open("CheckListQuestionsAndAnswers.aspx?ID=" + folderId + "&FolderLogId=" + $(this).attr('value'), "toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no");
                                //    });
                                //}

                                var trClsName;
                                if (addLogCount % 2 == 0)
                                    trClsName = "even";
                                else
                                    trClsName = "odd";

                                addFolderLogTrToGrid(result.d, $(".Log").val(), trClsName)

                                clearTextbox()
                                $("#floatingCirclesG").css('display', 'none');
                                $('#<%= lblFailFolderLog.ClientID %>').hide();
                                $('#overlay').hide();
                                $('#AddToFolderLog_popup').hide().focus();
                                alert("Folder log added successfully");
                            }
                        }
                    }
                }
            });
        }

        $('.CancelAddToTab').click(function (e) {
            e.preventDefault();
            $("[id*=ddlFolderName]").val($("#ddlFolderName option:first").val());
            $("[id*=ddlFolderLog]").empty();
            $(".FolderLogName").hide();
            $(".FolderName").show();
            $('#overlay').hide();
            $('.Log').val('');
            clearTextbox();
            $('#<%= lblFailFolderLog.ClientID %>').hide();
            $('#AddToFolderLog_popup').hide().focus();
            $("#floatingCirclesG").css('display', 'none');
            $('#floatingCirclesG').addClass('hideControl');
            e.stopPropagation();
        });

        $('.popupclose1').click(function () {
            //$("[id*=ddlFolderName]").empty();
            $("[id*=ddlFolderName]").val($("#ddlFolderName option:first").val());
            $("[id*=ddlFolderLog]").empty();
            $(".FolderLogName").hide();
            $(".FolderName").show();
            $('.popup-mainbox').hide();
            clearTextbox();
            $("#floatingCirclesG").css('display', 'none');
            $('#overlay').hide();
            $('#<%= lblFailFolderLog.ClientID %>').hide();
        });


        //$('.question').keypress(function (event) {
        //    var keycode = (event.keyCode ? event.keyCode : event.which);
        //    //alert(keycode);
        //    if (keycode == 46 || keycode == 190) {
        //        event.preventDefault();
        //    }
        //});

        function addFolderLogTrToGrid(folderLogId, folderLogName, trClsName) {
            if (folderLogId > 0) {
                $(".folderLogTable").append("<tr class=" + trClsName + "><td class=table_tekst_edit align=center style=width:25%;><a onclick=folderLogEdit(" + folderLogId
                    + ");" + ('<%= Sessions.HasViewPrivilegeOnly%>' == 'True' ? "title=Edit class='ic-icon ic-edit aFolderLog'  style='display:none;'></a>" : "title=Edit class='ic-icon ic-edit aFolderLog'></a>") + "<a onclick=folderLogView(" + folderLogId + "); logId =" + folderLogId + " id=a" + folderLogId
                    + " title=View class='ic-icon ic-logfile aFolderLog' ></a></td> " + "<td align='center' logId =" + folderLogId + "  class='logName' style='width:75%;white-space:nowrap;'>" + folderLogName + "</td>" + "</tr>");

            }
            if ($(".folderLogTable").find('tr').length == 6) {
                $('.btnAddtoTab').hide();
            }
        }

        function folderLogView(folderLogId) {
            var folderId = $('#' + '<%=hdnFolderId.ClientID%>').val();
            window.open("CheckListQuestionsAndAnswers.aspx?ID=" + folderId + "&FolderLogId=" + folderLogId, "toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no");
            CreateDocumentsAndFolderLogs(folderId, null, null, null, folderLogId, ActionEnum.View, null);
        }

        function PreventPostBack() {
            return false;
        }

        function GetFolderNameDetails() {
            var obj = {};
            var cntQue = 0;
            var officeId = $('#' + '<%=hdnOfficeId.ClientID%>').val();
            $.ajax({
                type: "POST",
                url: "FileFolderBox.aspx/GetFolderDetailsById",
                data: '{"officeId":"' + officeId + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var array = [];
                    if (result.d) {
                        var ddlFolder = $("[id*=ddlFolderName]");
                        ddlFolder.empty().append('<option selected="selected" value="0">Please select</option>');
                        $.each(result.d, function () {
                            ddlFolder.append($("<option></option>").val(this['folderId']).html(this['folderName']));
                        });
                    }
                }
            });
        }


        $("[id*=ddlFolderName]").change(function () {
            $('.FolderLogName').show();
            var folderId = $(this).val();
            $.ajax({
                type: "POST",
                url: "FileFolderBox.aspx/GetFolderLogDetailsById",
                data: '{"folderId":"' + folderId + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var array = [];
                    if (result.d) {
                        var ddlFolderLog = $("[id*=ddlFolderLog]");
                        ddlFolderLog.empty().append('<option selected="selected" value="0">Please select</option>');
                        $.each(result.d, function () {
                            ddlFolderLog.append($("<option></option>").val(this['folderLogId']).html(this['folderLogName']));
                        });
                    }
                }
            });
            return false;
        })


        $("[id*=ddlFolderLog]").change(function () {
            var folderLogId = $(this).val();
            $.ajax({
                type: "POST",
                url: "FileFolderBox.aspx/GetQuestionByLogID",
                data: '{"folderLogId":"' + folderLogId + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var array = [];

                    if (result.d) {
                        $.each(result.d, function () {
                            var isInsert = false;
                            var queCnt = $('.question').length;
                            if (queCnt > 0) {
                                for (i = 0; i < queCnt; i++) {
                                    var que = $('.question').eq(i).val().trim();
                                    if (que.toLowerCase().trim() != this['que'].toLowerCase().trim()) {
                                        isInsert = true;
                                    }
                                    else {
                                        isInsert = false;
                                        break;
                                    }
                                }
                                if (isInsert) {
                                    if (que == "") {
                                        $('#tblBRD tbody tr:last').find('input').attr("rank", 0);
                                        $('#tblBRD tbody tr:last').find('input').removeAttr('rank');
                                    }
                                    AppendQuestionDetails(this['que'], 0);
                                }
                            }
                            else {
                                if ($('#tblBRD tbody tr:last').find('input').attr('rank') == 1) {
                                    $('#tblBRD tbody tr:last').find('input').attr("rank", 0);
                                    $('#tblBRD tbody tr:last').find('input').removeAttr('rank');
                                }
                                AppendQuestionDetails(this['que'], 0);
                            }
                        });

                    }
                }
            });

        });

        $('.logFormAddFile').change(function (e) {
            showLoader();
            $("#" + '<%= btnUpload.ClientID %>').click();
        });

        function AppendQuestionDetails(que, queId) {
            if ($('#tblBRD tbody tr:last').find('input').attr('rank') == 1) {
                $('#tblBRD tbody tr:last').find('input').removeAttr('rank');
                var $newRow = $('#tblBRD tbody tr:last').clone(true);
                $($newRow).find('input').val(que);
                $($newRow).find('input').attr("QuesId", queId);
                $($newRow).find('input').attr("rank", 1);
                $('#tblBRD tbody tr:last').find('td:last').append('<a id="btnDeleteBRD"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" /></a>');
                $('#tblBRD tbody tr:last').find('td:last').find('#btnADDBRD').remove();
                $('#tblBRD tbody tr:last').find('input').removeAttr('rank');
                $('#tblBRD tbody tr:last').after($newRow);
            }
            else {
                $('#tblBRD tbody tr:last').find('input').val(que);
                $('#tblBRD tbody tr:last').find('input').attr("QuesId", queId);
                $('#tblBRD tbody tr:last').find('input').attr("rank", 1);
            }
        }

        function AddFolderLog() {
            var obj = {};
            var cntQue = 0;
            var folderId = newFolderID;
            //var folderId = newFolderID;
            $.ajax({
                type: "POST",
                url: "FileFolderBox.aspx/GetFolderLogDetails",
                data: '{"FolderId":"' + folderId + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var array = [];
                    if (result.d) {
                        var data = $.parseJSON(result.d);
                        if (data.Table) {
                            if (data.Table[0]) {
                                array = data.Table[0].toString().split(',');
                            }
                        }

                        if (data.Table1) {
                            $("#gvFolderLog").append("<table class='datatable listing-datatable folderLogTable' style='border-collapse:collapse;'></table>");
                            if ('<%= Sessions.HasViewPrivilegeOnly%>' == 'True')
                                $(".folderLogTable").append("<tr><th scope=col><span>View</span></th><th scope=col><span>Folder Log Name</span></th></tr>");
                            else
                                $(".folderLogTable").append("<tr><th scope=col><span>Edit | View</span></th><th scope=col><span>Folder Log Name</span></th></tr>");
                            $.each(data.Table1, function (i, e) {
                                var trClsName;
                                if (i % 2 == 0)
                                    trClsName = "even";
                                else
                                    trClsName = "odd";
                                addFolderLogTrToGrid($(this)[0], $(this)[1], trClsName);
                            });

                        }
                    }
                }
            });
        }

        function GetSelectedFiles() {
            ids = "";
            if ($("#ctl00_ContentPlaceHolder1_gridViewOcr:visible").find('input[type="checkbox"]:checked').length > 0) {
                $("#ctl00_ContentPlaceHolder1_gridViewOcr:visible").find('input[type="checkbox"]:checked').each(function () {
                    if ($(this).closest('tr').find("input[type='hidden'][id$=hdnDocId]") && $(this).closest('tr').find("input[type='hidden'][id$=hdnDocId]").val())
                        ids += $(this).closest('tr').find("input[type='hidden'][id$=hdnDocId]").val() + ",";
                });
                ids = ids.substring(0, ids.length - 1);
                $('#<%= DocumentIDs.ClientID %>').attr("value", ids); // set hidden field value
            }
            else if ($('.ajax__tab_panel:visible').find('table input[type="checkbox"]:checked').length > 0 && !($("#ctl00_ContentPlaceHolder1_gridViewOcr:visible").length > 0)) {
                $('.ajax__tab_panel:visible').find('table input[type="checkbox"]:checked').each(function () {

                    $('.ajax__tab_panel:visible').find('table input[type="hidden"][id$=hdnValues]').val();
                    if ($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]") && $(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val() && JSON.parse($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId)
                        ids += JSON.parse($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId + ",";
                });
                ids = ids.substring(0, ids.length - 1);
                $('#<%= DocumentIDs.ClientID %>').attr("value", ids); // set hidden field value
            }
            else {
                alert("Please select at least one document.");
                return false;
            }
            return true;
        }

        function DisableWhileRedacting(elem) {
            $('.selecttabcheck').hide();
            $('.btnSplitShare').hide();
            $('.btnRedactShare').show();
            $('.btnStartRedact').hide();
            $('.btnStopRedact').show();
            $('.lblRedact').show();
            $("#btnSpltBtn").hide();
            $(elem.contentWindow.document).children().find('#pageRotateCw').css("cursor", "not-allowed").attr("disabled", "disabled");
            $(elem.contentWindow.document).children().find('#pageRotateCcw').css("cursor", "not-allowed").attr("disabled", "disabled");
        }

        function EnableAfterRedacting() {
            $('.selecttabcheck').show();
            $('.btnSplitShare').show();
            $('.btnStartRedact').show();
            $('.btnStopRedact').hide();
            $('.btnRedactShare').hide();
            $('.lblRedact').hide();
            $("#btnSpltBtn").show();
            var iframe = document.getElementById("reviewContent");
            $(iframe.contentWindow.document).children().find('.page').unbind("click");
            $(iframe.contentWindow.document).children().find('#pageRotateCw').css("cursor", "default").removeAttr("disabled");
            $(iframe.contentWindow.document).children().find('#pageRotateCcw').css("cursor", "default").removeAttr("disabled");
            $(iframe.contentWindow.document).children().find('.mydiv').remove();
        }

        function startRedacting() {
            var iframe = document.getElementById("reviewContent");
            DisableWhileRedacting(iframe);
            cords = [];
            $(iframe.contentWindow.document).find("#scaleSelect").change(function () {
                var scaleValue = $(this).val();
                if (scaleValue == 'auto' || scaleValue == 'page-fit' || scaleValue == 'page-width') {
                    scaleValue = 0.68;
                }
                else if (scaleValue == 'page-actual') {
                    scaleValue = 1;
                }

                var divs = $(this).parents().find("#mainContainer").find('.mydiv');
                var pageWidth = parseInt($(this).parents().find("#mainContainer").find('.page').first().css("width").split('px')[0]);
                var viewerWidth = parseInt($(this).parents().find("#mainContainer").find('#viewer').first().css("width").split('px')[0]);
                var diff = '';
                if (pageWidth < viewerWidth) {
                    diff = (viewerWidth - pageWidth) / 2;
                }
                for (var i = 0; i < divs.length; i++) {
                    var height = parseInt($(divs[i]).css("height").split('px')[0]);
                    var width = parseInt($(divs[i]).css("width").split('px')[0]);
                    var prevScale, prevDiff;
                    for (var j = 0; j < cords.length; j++) {
                        if (cords[j].id == $(divs[i]).attr("id")) {
                            prevScale = cords[j].prevScale;
                            prevDiff = cords[j].prevDiff;
                            cords[j].prevDiff = diff;
                            cords[j].prevScale = scaleValue;
                        }
                    }
                    height = (height / prevScale) * scaleValue;
                    width = (width / prevScale) * scaleValue;
                    $(divs[i]).css("height", height + "px");
                    $(divs[i]).css("width", width + "px");
                    var left = parseInt($(divs[i]).css("left").split('px')[0]);
                    var top = parseInt($(divs[i]).css("top").split('px')[0]);
                    //var prevDiff = cords.find(f => f.id == $(divs[i]).attr("id")).prevDiff;
                    if (prevDiff != '') {
                        if (prevDiff > left)
                            left = prevDiff - left;
                        else
                            left = left - prevDiff;
                    }
                    if (prevScale != '') {
                        left = left / prevScale;
                        top = top / prevScale;
                    }
                    left = scaleValue * left;
                    top = scaleValue * top;
                    left = left + diff;
                    //cords.find(f => f.id == $(divs[i]).attr("id")).prevDiff = diff;
                    //cords.find(f => f.id == $(divs[i]).attr("id")).prevScale = scaleValue;
                    $(divs[i]).css("left", left + "px");
                    $(divs[i]).css("top", top + "px");
                }
            });
            $(iframe.contentWindow.document).children().find('.page').click(function (e) {
                var pageIndex = $(this).attr("data-page-number");
                var scaleValue = $(this).parent().parent().parent().find("#scaleSelect").val();
                var divHeight = '25';
                var divWidth = '160';
                if (scaleValue == 'auto' || scaleValue == 'page-fit' || scaleValue == 'page-width') {
                    scaleValue = 0.68;
                }
                else if (scaleValue == 'page-actual') {
                    scaleValue = 1;
                }
                divHeight = scaleValue * divHeight;
                divWidth = scaleValue * divWidth;
                var top = e.offsetY;
                var left = e.offsetX;
                if (e.target.className != 'textLayer') {
                    top = parseInt($(e.target).css("top").split('px')[0]) + e.offsetY;
                    left = parseInt($(e.target).css("left").split('px')[0]) + e.offsetX;
                }
                if (pageIndex > 1) {
                    top = ((pageIndex - 1) * parseInt($(this).css("height").split('px')[0])) + top + ((pageIndex - 1) * 10);
                }
                var thisWidth = parseInt($(this).css("width").split('px')[0]);
                var parentWidth = parseInt($(this).parent().css("width").split('px')[0]);
                var diff = '';
                if (thisWidth < parentWidth) { diff = ((parentWidth - thisWidth) / 2); }
                left = left + diff;
                cords.push({ top: top, left: left, id: 'redactDiv' + redactIndex, height: divHeight, width: divWidth, prevDiff: diff, prevScale: scaleValue, pageNumber: pageIndex, pageWidth: 0, pageHeight: 0 });
                var redDiv = '<div class="mydiv" id="redactDiv' + redactIndex + '" style="background-color:black;position:absolute;resize:auto;overflow:auto;border:solid 1px black;z-index:1;height:' + divHeight + 'px;width:' + divWidth + 'px;top:' + top + 'px;left:' + left + 'px">' +
                    '<span onclick="removeRedactDiv(this);" id="redactClose' + redactIndex + '" class="ic-icon redact-close" style="background: url(../../images/icon-close.png) no-repeat right top;float: left;padding: 6px;cursor: pointer;background-size: 12px;"></span></div>';
                $(redDiv).insertAfter($(this));
                redactIndex++;
            });
        }

        function stopRedacting() {
            if (confirm("Are you sure want to discard the changes ?")) {
                EnableAfterRedacting();
            }
        }

        function OnRedacting() {
            var iframe = document.getElementById("reviewContent");
            var divs = $(iframe.contentWindow.document).children().find('.mydiv');
            if (divs.length > 0) {
                EnableAfterRedacting();
                enableScrollbar();
                $("#overlay").hide();
                var popup = document.getElementById("preview_popup");
                popup.style.display = 'none';
                $("#pdfLbl").show();
                $('#<%= pdfName.ClientID %>').show();
                $('#dialog-share-book').dialog("open");
                $('#<%= email.ClientID %>').val('<%=Sessions.SwitchedEmailId%>');
                $('#<%= emailto.ClientID %>').val('');
                $('#<%= nameto.ClientID %>').val('');
                $('#<%= title.ClientID %>').val('');
                $('#<%= pdfName.ClientID %>').val('');
                $('#<%= content.ClientID %>').val('');
                $('#dialog-share-book input[name="linkexpiration"]').val('<%= Sessions.ExpirationTime %>');
                $('#<%= emailto.ClientID %>').css('border', '1px solid black');
                var pageHeight = $(iframe.contentWindow.document).children().find('.page').css("height").split('px')[0];
                var pageWidth = $(iframe.contentWindow.document).children().find('.page').css("width").split('px')[0];
                for (var i = 0; i < divs.length; i++) {
                    var height = $(divs[i]).css("height").split('px')[0];
                    var width = $(divs[i]).css("width").split('px')[0];
                    for (var j = 0; j < cords.length; j++) {
                        if (cords[j].id == $(divs[i]).attr("id")) {
                            cords[j].height = height;
                            cords[j].width = width;
                            cords[j].pageHeight = pageHeight;
                            cords[j].pageWidth = pageWidth;
                        }
                    }
                }
                $("#<%= hdnIsRedaction.ClientID %>").val(true);
                $('#<%= hdnCords.ClientID %>').val(JSON.stringify(cords));
            }
            else {
                alert("Please add redaction !!!");
            }
        }

        function AppendIndexPreviewingDetails(indexPair, index) {
            CreateDynamicIndexPreviewingTextBox(indexPair.IndexId, true);
            SetIndexPreviewingValues(indexPair, false);
        }

        function SetIndexPreviewingValues(indexPair, isRender) {
            var row = "";
            if (!isRender)
                row = document.getElementById("indexPreviewRow" + indexPair.IndexId);
            else
                row = $(".indexPreviewRow");
            $(row).find("#txtIndexKey").val(indexPair.IndexKey);
            $(row).find("#txtIndexValue").val(indexPair.IndexValue);
            $(row).find(".hdnIndexId").val(indexPair.IndexId);
        }

        function GetDynamicPreviewingTextBox(value) {
            var test = '<td><input type="hidden" value ="0" class="hdnIndexId"></td>' +
                '<td class="tdIndex"><input type="text" id="txtIndexKey" class="txtIndex txtKey" maxlength="20" placeholder="Key" /><input type="text" id="txtIndexValue" class="txtIndex" maxlength="20" placeholder="Value" /></td></td>' +
            '<td><button id="btnAddIndex" class="btnIndex" type="button" onclick="return AddIndexPreviewingTextBoxes(this)"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/add-icon.png" title="Add"/> </button> </td>' +
            '<td><button id="btnRemoveIndex" class="btnIndex" type="button" onclick="return DisplayConfirmBox(this, true)"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" /> </button>'

            return test;
        }

        function AddIndexPreviewingTextBoxes(ele) {
            var index = parseInt($($(ele).parent().parent().find('[class*=hdnIndexId]')).val());
            if ($(ele).closest('tr').find('#txtIndexKey').val() == "") {
                alert("Please enter Key Value.");
                return false;
            }
            CreateDynamicIndexPreviewingTextBox(index, false);
            return false;
        }

        function CreateDynamicIndexPreviewingTextBox(index, isRender) {
            var tr = document.createElement('tr');
            tr.innerHTML = GetDynamicPreviewingTextBox(index);

            if (!isRender)
                tr.setAttribute("id", "indexPreviewRow" + (index = index + 1));
            else
                tr.setAttribute("id", "indexPreviewRow" + index);
            // var body = $(parentElement).parent();
            $('table#tblIndexPreview tr:last').after(tr);
            SetIndexPreviewingButtonVisibility();

            //Allow maximum of 5 key,value pairs
            if ($('table#tblIndexPreview tr').length == 5) {
                $(tr).find('#btnAddIndex').hide();
            }
        }

        function RemoveIndexPreviewTextBoxes() {
            $('#DivDeleteConfirmIndexPreview').hide();
            $('#preview_popup,#overlay').show().focus();
            if ($(indexRemoveEle).closest('tr').find('input.hdnIndexId').val() != 0) {
                var hdnId = $(indexRemoveEle).closest('tr').find('input.hdnIndexId').val();
                var test = deleteIndexData.indexOf(hdnId);
                if (test <= 0)
                    deleteIndexData = deleteIndexData + hdnId + ",";
            }
            $(indexRemoveEle).closest('tr').remove();
            SetIndexPreviewingButtonVisibility();
            indexRemoveEle = "";
            disableScrollbar();
            return false;
        }

        function SetIndexPreviewingButtonVisibility() {
            var rowCount = $('table#tblIndexPreview tr').length;
            for (i = 0; i < rowCount; i++) {
                var $item = $($('table#tblIndexPreview tr')[i]);

                if (i == (rowCount - 1)) {
                    $item.find('#btnRemoveIndex').show();
                    $item.find('#btnAddIndex').show();
                    if (i == 0) {
                        $item.find('#btnRemoveIndex').hide();
                    }
                }
                else if (i == 0) {
                    $item.find('#btnRemoveIndex').show();
                    $item.find('#btnAddIndex').hide();
                }
                else {
                    $item.find('#btnRemoveIndex').show();
                    $item.find('#btnAddIndex').hide();
                }
            }
        }

        $("#btnIndexPreviewingSave").click(function (e) {
            var arrIndex = [];
            for (i = 0; i < $('table#tblIndexPreview tr').length; i++) {
                var $item = $($('table#tblIndexPreview tr')[i]);
                if (($item.find('#txtIndexKey').val() != null && $item.find('#txtIndexKey').val() != ""))
                    arrIndex.push({ IndexKey: $item.find('#txtIndexKey').val(), IndexValue: $item.find('#txtIndexValue').val(), IndexId: $($item).find('input.hdnIndexId').val() });
                else
                    deleteIndexData = deleteIndexData + $($item).find('input.hdnIndexId').val() + ",";
            }
            var folderId = $('#' + '<%=hdnFolderId.ClientID%>').val();
            var folderId = $('#' + '<%=hdnFolderId.ClientID%>').val();
            var test = JSON.stringify(arrIndex);
            deleteIndexData = deleteIndexData.substr(0, deleteIndexData.lastIndexOf(','));
            var data = { "folderId": folderId, "dividerId": dividerId, "documentId": $("#hndDividerIdIndex").val(), "indexData": test, "deleteIndexData": deleteIndexData };
            $.ajax({
                type: "POST",
                url: "FileFolderBox.aspx/SaveIndexValues",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    deleteIndexData = "";
                    if (result.d == true) {
                        alert("Data has been saved successfully.");
                        //$('#overlay').hide();
                        //$('#preview_popup').hide().focus();
                        //$("#floatingCirclesG").css('display', 'none');
                        //$('#floatingCirclesG').addClass('hideControl');
                        //enableScrollbar();
                    }
                    else
                        alert("Data has not been saved. Please try again later.");

                }
            });
        });

        $("#btnIndexPreviewingCancel, #closeIndexPreview").click(function (e) {
            e.preventDefault();
            $('#overlay').hide();
            $('#preview_popup').hide().focus();
            $("#floatingCirclesG").css('display', 'none');
            $('#floatingCirclesG').addClass('hideControl');
            enableScrollbar();
            e.stopPropagation();
        });

        function ShowPrevious() {
            var id = $('#<%= hdnPreviousId.ClientID%>').val();
            window.open(window.location.origin + "/Office/FileFolderBox.aspx?id=" + id, '_self');
        }

        function ShowNext() {
            var id = $('#<%= hdnNextId.ClientID%>').val();
            window.open(window.location.origin + "/Office/FileFolderBox.aspx?id=" + id, '_self');
        }

        function ShowPreviousNext() {
            if ('<%= GetFolderIds() %>' != null) {
                var folderId = '<%= GetFolderIds() %>';
                var folderItem = folderId.split(',');
                for (i = 0; i < folderItem.length; i++) {
                    if (folderItem[i].trim() == newFolderID) {
                        if (i != 0)
                            $('#<%= hdnPreviousId.ClientID%>').val(folderItem[i - 1].trim());
                        $('#<%= hdnNextId.ClientID%>').val(folderItem[i + 1].trim());
                        break;
                    }
                }
                if ($('#<%= hdnPreviousId.ClientID%>').val() == "")
                    $(".btnPrevious").hide();
                else
                    $(".btnPrevious").show();
                if ($('#<%= hdnNextId.ClientID%>').val() == "")
                    $(".btnNext").hide();
                else
                    $(".btnNext").show();

            }
        }

        function ConvertPdfValidation() {
            if (GetSelectedFiles()) {
                showLoader();
                return true;
            }
            else return false;
        }

        function SearchDocument() {
            if ($("#<%= drpSearch.ClientID %>").val() == 5) {
                if ($("#<%= txtKeyword.ClientID %>").val() == ""
                    || $("#<%= txtKeyword.ClientID %>").val() == undefined
                    || $("#<%= txtKeyword.ClientID %>").val().length < 3) {
                    alert("A Keyword must have at least 3 characters to search.");
                    return false;
                }
                else {
                    UpdateDocumentSearch();
                    return true;
                }
            }
            else {
                $('.btnConvertOcr').show();
                return true;
            }
        }

        var min = 0;
        var max = 0; var i = 0;

        function UpdateDocumentSearch() {
            $('.docTable').find('tr').each(function () {
                $(this).attr('data-index', i);
                i++;
            });

            max = i;
            i = 0;
        }

        function CheckFileValidation() {
            if ($("#<%= fileNameFld.ClientID %>").val() == "" || $("#<%= fileNameFld.ClientID %>").val() == undefined) {
                alert("Please enter valid file name");
                return false;
            }
            var popup = document.getElementById("preview_doc_popup");
            popup.style.display = 'none';
            return true;
        }

        function navigatePrev(event) {
            var id = parseInt($(event).siblings().first().find('label:visible').attr("id"), 10);
            if ($(event).siblings().find('#' + parseInt(id - 1)).length > 0) {
                $(event).siblings().first().find('label:visible').hide();
                $(event).siblings().find('#' + parseInt(id - 1)).delay(500).show();
                if ($(event).siblings().find('#' + parseInt(id - 2)).length == 0) {
                    $(event).removeClass("btnPrevEnabled");
                    $(event).addClass("btnPrevDisabled");
                }
                else {
                    $(event).addClass("btnPrevEnabled");
                    $(event).removeClass("btnPrevDisabled");
                }
                if ($(event).siblings().find('#' + parseInt(id)).length > 0) {
                    $(event).siblings().parent().find('.btnNextDisabled').addClass("btnNextEnabled");
                    $(event).siblings().parent().find('.btnNextEnabled').removeClass("btnNextDisabled");
                }
            }
            return false;
        }

        function navigateNext(event) {
            var id = parseInt($(event).siblings().first().find('label:visible').attr("id"), 10);
            if ($(event).siblings().find('#' + parseInt(id + 1)).length > 0) {
                $(event).siblings().first().find('label:visible').hide();
                $(event).siblings().find('#' + parseInt(id + 1)).delay(500).show();
                if ($(event).siblings().find('#' + parseInt(id + 2)).length == 0) {
                    $(event).removeClass("btnNextEnabled");
                    $(event).addClass("btnNextDisabled");
                }
                else {
                    $(event).addClass("btnNextEnabled");
                    $(event).removeClass("btnNextDisabled");
                }
                if ($(event).siblings().find('#' + parseInt(id)).length > 0) {
                    $(event).siblings().parent().find('.btnPrevDisabled').addClass("btnPrevEnabled");
                    $(event).siblings().parent().find('.btnPrevEnabled').removeClass("btnPrevDisabled");
                }
            }
            return false;
        }

        function ShowPreviewForDocument(obj, isContinue) {
            $('.docTable tr').removeClass('activetr');
            $(obj).closest('tr').addClass('activetr');
            if (!isContinue) {
                $('#<%= hdnFileNameOnly.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnFileName]").val());
                $('#<%= hdnSelectedPath.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnPathName]").val());
                $('#<%= hdnFileId.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnDocId]").val());
                $('#<%= hdnFolderId.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnFolderId]").val());
            }
            $('#<%= hdnObjId.ClientID%>').val(obj.id);
            $("#<%= lblFileName.ClientID %>").html($('#<%= hdnFileNameOnly.ClientID%>').val());

            $("#overlay").css("z-index", "9999");
            $("#" + '<%= hdnFileId.ClientID %>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnDocId]").val());
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//"
                    + window.location.hostname
                    + (window.location.port ? ':' + window.location.port : '');
            }
            src = './Preview.aspx?data=' + window.location.origin + "/" + $('#<%= hdnSelectedPath.ClientID%>').val() + "?v=" + "<%= DateTime.Now.Ticks%>";
            var popup = document.getElementById("preview_doc_popup");
            var overlay = document.getElementById("overlay");
            $('#reviewDocContent').attr('src', src);
            setTimeout(function () {
                overlay.style.display = 'block';
            }, 500);
            popup.style.display = 'block';
            $('#preview_doc_popup').focus();
            $('#preview_doc_popup').css("max-width", "1300px");
            var ci = $(obj).closest('tr').attr('data-index');
            if (ci == max - 1) {
                $(".btnPrevPreview").hide();
            }
            else {
                $(".btnPrevPreview").show();
            }
            if (ci == min + 1) {
                $(".btnNextPreview").hide();
            }
            else {
                $(".btnNextPreview").show();

            }
            return false;
        }

        function makeEditable() {
            $("#<%= lblFileName.ClientID %>").hide();
            $("#<%= btnUpdate.ClientID %>").hide();
            $("#<%= fileNameFld.ClientID %>").show();
            $("#<%= btnSave.ClientID %>").show();
            $("#<%= btnCancel.ClientID %>").show();
            $("#<%= fileNameFld.ClientID %>").val($("#<%= hdnFileNameOnly.ClientID %>").val());
            return false;
        }

        function makeReadOnly() {
            $("#<%= lblFileName.ClientID %>").show();
            $("#<%= btnUpdate.ClientID %>").show();
            $("#<%= fileNameFld.ClientID %>").hide();
            $("#<%= btnSave.ClientID %>").hide();
            $("#<%= btnCancel.ClientID %>").hide();
            return false;
        }

        function ShowPreviousDoc() {
            makeReadOnly();
            $("#overlay").css("z-index", "100002");
            var ci = $('.docTable').find('tr.activetr').attr('data-index');
            ci = parseInt(ci);
            var ni = 0;
            if (ci < max & ci > min + 1) {
                ni = ci - 1;
            }
            else {
                ni = max - 1;
            }

            $('.docTable').find('tr').each(function () {
                if ($(this).attr('data-index') == ni) {
                    $(this).find('a.ic-preview').click();
                }
            })
        }

        function ShowNextDoc() {
            makeReadOnly();
            $("#overlay").css("z-index", "100002");
            var ci = $('.docTable').find('tr.activetr').attr('data-index');
            ci = parseInt(ci);
            var ni = 0;
            if (ci < max - 1 & ci > min) {
                ni = ci + 1;
            }
            else {
                ni = min + 1;
            }

            $('.docTable').find('tr').each(function () {
                if ($(this).attr('data-index') == ni) {
                    $(this).find('a.ic-preview').click();
                }
            })
        }

    </script>
    <a class='iframe' href="javascript:;" style="display: none;">popup</a>
</asp:Content>

