﻿using AjaxControlToolkit;
using Elasticsearch.Net;
using iTextSharp.text;
using iTextSharp.text.pdf;
using Nest;
using Newtonsoft.Json;
using Shinetech.DAL;
using Shinetech.Engines;
using Shinetech.Utility;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Rectangle = iTextSharp.text.Rectangle;


public partial class Office_FileFolderBox : LicensePage
{
    public string virtualDir = ConfigurationManager.AppSettings["VirtualDir"];
    //¬Զؚ 2015-11-01
    public static string EdFileShare = System.Configuration.ConfigurationManager.AppSettings["EdFileShare"];
    public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    public static string splitPdfTepmPath = System.Configuration.ConfigurationManager.AppSettings["SplitPdf"];
    public static bool enablePhysicalMapping = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["EnablePhysicalMapping"]);
    public static NLogLogger _logger = new NLogLogger();
    public string newFileName = string.Empty;
    public string fullFilePath = string.Empty;
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
    public static Stopwatch stopWatch = new Stopwatch();
    int dividerid;

    #region Elastic

    protected static string hostString = ConfigurationManager.AppSettings.Get("ElasticHostString");
    protected static string elasticIndexName = ConfigurationManager.AppSettings.Get("ElasticDocIndices");
    protected static Uri[] hosts = hostString.Split(',').Select(x => new Uri(x)).ToArray();
    protected static StaticConnectionPool connectionPool = new StaticConnectionPool(hosts);
    protected static ConnectionSettings settings = new ConnectionSettings(connectionPool)
                                                                         .DefaultMappingFor<DocumentIndex>(i => i
                                                                         .IndexName(elasticIndexName)
                                                                         .TypeName("pdf"))
                                                                         .DisableDirectStreaming();
    protected static ElasticClient highClient = new ElasticClient(settings);

    #endregion

    //public List<KeyValuePair<string,string>> UploadErrorFileName
    public Dictionary<string, string> UploadErrorFileName
    {
        get
        {
            return (Session["UploadErrorFileName"] != null) ? (Dictionary<string, string>)Session["UploadErrorFileName"] : new Dictionary<string, string>();
        }
        set
        {
            Session["UploadErrorFileName"] = value;
        }
    }

    public class FolderDetails
    {
        public string folderId;
        public string folderName;
    }

    public class FolderLogDetails
    {
        public string folderLogId;
        public string folderLogName;
    }

    public class question
    {

        public string que;

    }

    public class questionList
    {
        public int Id;
        public string que;
    }

    protected void InitTextBox()
    {
        this.drpState.DataSource = States;
        this.drpState.DataValueField = "StateID";
        this.drpState.DataTextField = "StateName";
        this.drpState.DataBind();
        this.drpState.SelectedValue = "CA";
    }

    public DataTable States
    {
        get
        {
            object obj = Application["States"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetStates();
                Application["States"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    DataTable dtOrder = new DataTable();
    DataTable dtDividerMove = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Common_Tatva.CreateIndex();
        stopWatch.Start();
        AwsUpload aws = new AwsUpload();
        aws.GetUrl();
        var argument = Request.Form["__EVENTARGUMENT"];
        if (Sessions.HasViewPrivilegeOnly)
        {
            FileUpload1.Visible = false;
            btnAddFolderlog.Visible = false;
            btnCopyFile.Visible = false;
            btnAddtoTab.Visible = false;
            downloadall.Visible = false;
            folderEditButton.Visible = false;
            folderArchiveButton.Visible = false;
            uploadDiv.Style.Add("display", "none");
            saveReminderButton.Visible = false;
            spanFileUpload1.Visible = false;
        }
        if (!Common_Tatva.IsUserSwitched() && (this.Page.User.IsInRole("Users") && !Sessions.HasPrevilegeToDownload))
            downloadall.Visible = false;
        if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
        {
            string id = this.Request.QueryString["id"];
            hdnFolderId.Value = id;
            //hdnOfficeId.Value = Convert.ToString(Sessions.UserId);
            SetOfficeId();



            string[] dividerIds = Convert.ToString(argument).Split('@');
            if (dividerIds.Length > 1)
            {
                GetData(id);
                GetDividerData(id);
                for (int i = 1; i < dividerIds.Length; i++)
                {
                    BindAllGrid(Convert.ToInt32(dividerIds[i]));
                }
                BindDividers();
            }
        }
        argument = "";
        if (!Page.IsPostBack && !AjaxFileUpload1.IsInFileUploadPostBack)
        {
            UploadErrorFileName = null;

            string id = this.Request.QueryString["id"];
            string tabIndex = this.Request.QueryString["tabIndex"];
            int folderId;
            hdnFolderId.Value = id;
            //hdnOfficeId.Value = Convert.ToString(Sessions.UserId);
            SetOfficeId();
            if (!string.IsNullOrEmpty(id) && int.TryParse(id, out folderId))
            {
                this.EffID_FFB = id;
                FileFolder folder = new FileFolder(Convert.ToInt32(id));
                if (string.IsNullOrEmpty(tabIndex))
                {
                    this.ActiveTabIndex = 0;
                    this.ActiveTabIndex1 = 0;
                    hdnActiveTabIndex.Value = "0";
                }
                else
                {
                    this.ActiveTabIndex = Convert.ToInt32(tabIndex);
                    this.ActiveTabIndex1 = Convert.ToInt32(tabIndex);
                    hdnActiveTabIndex.Value = tabIndex;
                }
                DataTable edFormsFolders = General_Class.GetEdFormsForUserFolder((int)Enum_Tatva.EdFormsStatus.Submit, (int)Enum_Tatva.EdFormsLog.Submit, Sessions.SwitchedRackspaceId, Convert.ToInt32(id));
                if (edFormsFolders != null && edFormsFolders.Rows.Count > 0)
                    btnEdFormFolder.Visible = true;
                else
                    btnEdFormFolder.Visible = false;
                if (folder.IsExist)
                {
                    Shinetech.DAL.Account user = new Shinetech.DAL.Account(Sessions.SwitchedSessionId);
                    //Shinetech.DAL.Account user = new Shinetech.DAL.Account(Membership.GetUser().ProviderUserKey.ToString());
                    bool hasPrivilage = user.HasPrivilegeDelete.Value;

                    //if (!Common_Tatva.IsUserSwitched() && User.IsInRole("   ") && !hasPrivilage)
                    //{
                    //    folderEditButton.Visible = false;
                    //    folderArchiveButton.Visible = false;
                    //}

                    if (user.UID.Value.Equals(folder.OfficeID.Value, StringComparison.CurrentCultureIgnoreCase))
                    {
                        folderEditButton.Visible = true;
                        folderArchiveButton.Visible = true;
                    }


                    InitTextBox();

                    textFoldeName.Text = folder.FolderName.Value;

                    textFirstName.Text = folder.FirstName.Value;
                    textLastName.Text = folder.Lastname.Value;

                    textBoxFileNumber.Text = folder.FileNumber.Value;
                    textBoxEmail.Text = folder.Email.Value;

                    textBoxTel.Text = folder.Tel.Value;
                    textBoxFax.Text = folder.FaxNumber.Value;

                    textBoxAlert1.Text = folder.Alert1.Value;
                    textBoxAlert2.Text = folder.Alert2.Value;
                    textBoxSite.Text = folder.Site1.Value;
                    textBoxCity.Text = folder.City.Value;
                    textBoxAddress.Text = folder.Address.Value;
                    textBoxZipCode.Text = folder.Postal.Value;
                    textfield5.Text = folder.DOB.Value;
                    //CalendarExtender8.SelectedDate = folder.DOB.Value;
                    textBoxComments.Text = folder.Comments.Value;
                    this.drpState.SelectedValue = folder.StateID.Value;

                    LinkButton lblFolderName = this.UpdatePanel1.FindControl("lblFolderName") as LinkButton;
                    lblFolderName.Text = folder.FolderName.Value + (string.IsNullOrEmpty(folder.FileNumber.Value) ? "" : (" (" + folder.FileNumber.Value + ")"));
                    hdnFolderName.Value = folder.FolderName.Value;
                    hdnFileNumber.Value = folder.FileNumber.Value;
                    lblFolderName.CommandArgument = string.Format("{0},{1},{2}", folder.FolderID.Value, folder.FirstName.Value,
                        folder.Lastname.Value);

                    Label lblFolderDetails = this.UpdatePanel1.FindControl("lblFolderDetails") as Label;
                    lblFolderDetails.Text = folder.FolderName.Value;

                    LinkButton2.CommandArgument = string.Format("{0},{1},{2}", folder.FolderID.Value, folder.FirstName.Value,
                        folder.Lastname.Value);

                    LinkButton12.CommandArgument = string.Format("{0},{1},{2}", folder.FolderID.Value, folder.FirstName.Value,
                        folder.Lastname.Value);

                    LinkButton lnkFlashViewer = this.UpdatePanel1.FindControl("lnkFlashViewer") as LinkButton;
                    lnkFlashViewer.CommandArgument = string.Format("{0},{1},{2}", folder.FolderID.Value, folder.FirstName.Value,
                        folder.Lastname.Value);

                    LinkButton lnkHtmlViewer = this.UpdatePanel1.FindControl("lnkHtmlViewer") as LinkButton;
                    lnkHtmlViewer.CommandArgument = string.Format("{0},{1},{2}", folder.FolderID.Value, folder.FirstName.Value,
                        folder.Lastname.Value);

                    LinkButton lnkFolderLogs = this.UpdatePanel1.FindControl("lnkFolderLogs") as LinkButton;
                    lnkFolderLogs.CommandArgument = string.Format("{0},{1},{2}", folder.FolderID.Value, folder.FirstName.Value,
                        folder.Lastname.Value);

                    //LinkButton2.OnClientClick = string.Format("window.open(\"WebFlashViewer.aspx?ID={0}\",\"\",\"toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no\")", folder.FolderID.Value);
                    LinkButton12.OnClientClick = string.Format("window.open(\"WebFlashViewer.aspx?V=2&ID={0}\",\"\",\"toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no\")", folder.FolderID.Value);


                    lnkFlashViewer.OnClientClick = string.Format("window.open(\"WebFlashViewer.aspx?V=2&ID={0}\",\"\",\"toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no\")", folder.FolderID.Value);
                    //lnkHtmlViewer.OnClientClick = string.Format("window.open(\"WebFlashViewer.aspx?V=2&ID={0}\",\"\",\"toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no\")", folder.FolderID.Value);
                    //lnkFolderLogs.OnClientClick = string.Format("window.open(\"CheckListQuestionsAndAnswers.aspx?ID={0}\",\"\",\"toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no\")", folder.FolderID.Value);

                    SetLogFormView(id);

                    this.GetReminderData(id);


                    GetDividerData(id);
                    try
                    {
                        this.DividerID_FFB = this.DividerSource.Rows[ActiveTabIndex]["DividerID"].ToString();
                    }
                    catch (Exception ex)
                    {
                        _logger.Error(ex);
                    }
                    GetData(id, string.IsNullOrEmpty(this.DividerID_FFB) ? 0 : Convert.ToInt32(this.DividerID_FFB));
                    BindDividers();


                    _logger.Debug("PageLoad : " + stopWatch.Elapsed.ToString());
                }
            }
        }
    }

    private void SetLogFormView(string id)
    {
        DataTable dataLogForm = General_Class.GetLogFormDocs(Sessions.SwitchedRackspaceId, int.Parse(id));
        //DataTable dataLogForm = General_Class.GetLogFormDocs(Sessions.RackSpaceUserID, int.Parse(id));
        LinkButton lnkBtnPreveiw1 = this.UpdatePanel1.FindControl("btnPreview1") as LinkButton;
        LinkButton lnkBtnPreveiw2 = this.UpdatePanel1.FindControl("btnPreview2") as LinkButton;
        LinkButton lnkBtnPreveiw3 = this.UpdatePanel1.FindControl("btnPreview3") as LinkButton;
        LinkButton lnkBtnPreveiw4 = this.UpdatePanel1.FindControl("btnPreview4") as LinkButton;
        LinkButton lnkBtnPreveiw5 = this.UpdatePanel1.FindControl("btnPreview5") as LinkButton;

        LinkButton lnkBtnLog1 = this.UpdatePanel1.FindControl("btnLog1") as LinkButton;
        LinkButton lnkBtnLog2 = this.UpdatePanel1.FindControl("btnLog2") as LinkButton;
        LinkButton lnkBtnLog3 = this.UpdatePanel1.FindControl("btnLog3") as LinkButton;
        LinkButton lnkBtnLog4 = this.UpdatePanel1.FindControl("btnLog4") as LinkButton;
        LinkButton lnkBtnLog5 = this.UpdatePanel1.FindControl("btnLog5") as LinkButton;

        if (dataLogForm != null && dataLogForm.Rows.Count > 0)
        {
            //foreach (DataRow row in dataLogForm.Rows)
            //{
            if (lnkBtnPreveiw1 != null)
            {
                lnkBtnPreveiw1.Attributes.Add("onclick", "ShowPreviewForRequestedFiles('" + dataLogForm.Rows[0]["ID"] + "');");
                lnkBtnPreveiw1.Attributes.Add("style", "display:inline-block;z-index:999;padding-left: 7px; margin-top:6px;");
                lnkBtnPreveiw1.ToolTip = dataLogForm.Rows[0]["FileName"].ToString();
            }
            if (lnkBtnLog1 != null)
            {
                lnkBtnLog1.Attributes.Add("onclick", "ShowLogFormLogs('" + dataLogForm.Rows[0]["ID"] + "');");
                lnkBtnLog1.Attributes.Add("style", "display:inline-block; margin-top:6px;");
                lnkBtnLog1.ToolTip = "View Logs of " + dataLogForm.Rows[0]["FileName"].ToString();
            }

            if (dataLogForm.Rows.Count > 1 && lnkBtnPreveiw2 != null)
            {
                lnkBtnPreveiw2.Attributes.Add("onclick", "ShowPreviewForRequestedFiles('" + dataLogForm.Rows[1]["ID"] + "');");
                lnkBtnPreveiw2.Attributes.Add("style", "display:inline-block;padding-left: 7px; margin-top:6px;");
                lnkBtnPreveiw2.ToolTip = dataLogForm.Rows[1]["FileName"].ToString();
                if (lnkBtnLog2 != null)
                {
                    lnkBtnLog2.Attributes.Add("onclick", "ShowLogFormLogs('" + dataLogForm.Rows[1]["ID"] + "');");
                    lnkBtnLog2.Attributes.Add("style", "display:inline-block; margin-top:6px;");
                    lnkBtnLog2.ToolTip = "View Logs of " + dataLogForm.Rows[1]["FileName"].ToString();
                }
            }
            else
            {
                lnkBtnPreveiw2.Attributes.Add("style", "display:none");
                lnkBtnLog2.Attributes.Add("style", "display:none");
            }
            if (dataLogForm.Rows.Count > 2 && lnkBtnPreveiw3 != null)
            {
                lnkBtnPreveiw3.Attributes.Add("onclick", "ShowPreviewForRequestedFiles('" + dataLogForm.Rows[2]["ID"] + "');");
                lnkBtnPreveiw3.Attributes.Add("style", "display:inline-block;padding-left: 7px; margin-top:6px;");
                lnkBtnPreveiw3.ToolTip = dataLogForm.Rows[2]["FileName"].ToString();
                if (lnkBtnLog3 != null)
                {
                    lnkBtnLog3.Attributes.Add("onclick", "ShowLogFormLogs('" + dataLogForm.Rows[2]["ID"] + "');");
                    lnkBtnLog3.Attributes.Add("style", "display:inline-block; margin-top:6px;");
                    lnkBtnLog3.ToolTip = "View Logs of " + dataLogForm.Rows[2]["FileName"].ToString();
                }
            }
            else
            {
                lnkBtnPreveiw3.Attributes.Add("style", "display:none");
                lnkBtnLog3.Attributes.Add("style", "display:none");
            }
            if (dataLogForm.Rows.Count > 3 && lnkBtnPreveiw4 != null)
            {
                lnkBtnPreveiw4.Attributes.Add("onclick", "ShowPreviewForRequestedFiles('" + dataLogForm.Rows[3]["ID"] + "');");
                lnkBtnPreveiw4.Attributes.Add("style", "display:inline-block;padding-left: 7px; margin-top:6px;");
                lnkBtnPreveiw4.ToolTip = dataLogForm.Rows[3]["FileName"].ToString();
                if (lnkBtnLog4 != null)
                {
                    lnkBtnLog4.Attributes.Add("onclick", "ShowLogFormLogs('" + dataLogForm.Rows[3]["ID"] + "');");
                    lnkBtnLog4.Attributes.Add("style", "display:inline-block; margin-top:6px;");
                    lnkBtnLog4.ToolTip = "View Logs of " + dataLogForm.Rows[3]["FileName"].ToString();
                }
            }
            else
            {
                lnkBtnPreveiw4.Attributes.Add("style", "display:none");
                lnkBtnLog4.Attributes.Add("style", "display:none");
            }
            if (dataLogForm.Rows.Count > 4 && lnkBtnPreveiw5 != null)
            {
                lnkBtnPreveiw5.Attributes.Add("onclick", "ShowPreviewForRequestedFiles('" + dataLogForm.Rows[4]["ID"] + "');");
                lnkBtnPreveiw5.Attributes.Add("style", "display:inline-block;padding-left: 7px; margin-top:6px;");
                lnkBtnPreveiw5.ToolTip = dataLogForm.Rows[4]["FileName"].ToString();
                if (lnkBtnLog5 != null)
                {
                    lnkBtnLog5.Attributes.Add("onclick", "ShowLogFormLogs('" + dataLogForm.Rows[4]["ID"] + "');");
                    lnkBtnLog5.Attributes.Add("style", "display:inline-block; margin-top:6px;");
                    lnkBtnLog5.ToolTip = "View Logs of " + dataLogForm.Rows[4]["FileName"].ToString();
                }
                spanFileUpload1.Visible = false;
                btnUpload.Visible = false;
            }
            else
            {
                lnkBtnPreveiw5.Attributes.Add("style", "display:none");
                lnkBtnLog5.Attributes.Add("style", "display:none");
            }

            // }
        }
        else
        {
            lnkBtnPreveiw1.Attributes.Add("style", "display:none;");
            lnkBtnPreveiw2.Attributes.Add("style", "display:none;");
            lnkBtnPreveiw3.Attributes.Add("style", "display:none;");
            lnkBtnPreveiw4.Attributes.Add("style", "display:none;");
            lnkBtnPreveiw5.Attributes.Add("style", "display:none;");

            lnkBtnLog1.Attributes.Add("style", "display:none");
            lnkBtnLog2.Attributes.Add("style", "display:none");
            lnkBtnLog3.Attributes.Add("style", "display:none");
            lnkBtnLog4.Attributes.Add("style", "display:none");
            lnkBtnLog5.Attributes.Add("style", "display:none");
        }
    }

    private void SetOfficeId()
    {
        hdnOfficeId.Value = Sessions.SwitchedRackspaceId;
        //if (this.Page.User.IsInRole("Offices"))
        //{
        //    hdnOfficeId.Value = Membership.GetUser().ProviderUserKey.ToString();
        //}

        //else if (this.Page.User.IsInRole("Users"))
        //{
        //    DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(Membership.GetUser().ProviderUserKey.ToString());
        //    hdnOfficeId.Value = dtOfficeUser.Rows[0]["OfficeUID"].ToString();
        //}
    }

    [WebMethod(EnableSession = true)]
    public static void SetFolderId(string folderId)
    {
        try
        {
            Sessions.FolderId = Convert.ToInt32(folderId);
        }
        catch (Exception ex) { }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource_FolderBox"] as DataTable;
        }
        set
        {
            this.Session["DataSource_FolderBox"] = value;
        }
    }

    public DataTable OcrDataSource
    {
        get
        {
            return this.Session["OcrDataSource_FolderBox"] as DataTable;
        }
        set
        {
            this.Session["OcrDataSource_FolderBox"] = value;
        }
    }

    public DataTable DocumentOrderSource
    {
        get
        {
            return this.Session["DocumentOrderSource_FolderBox"] as DataTable;
        }
        set
        {
            this.Session["DocumentOrderSource_FolderBox"] = value;
        }
    }

    public DataTable DividerSource
    {
        get
        {
            return this.Session["DividerSource_FolderBox"] as DataTable;
        }
        set
        {
            this.Session["DividerSource_FolderBox"] = value;
        }
    }

    public string ArchiveBox
    {
        get
        {
            return "~/ArchiveBox";
        }

    }

    public string GetDividerForDropDown;
    public string GetClassForDropDown;


    private void GetDividerData(string folderId)
    {
        this.DividerSource = FileFolderManagement.GetDividerByEffid(Convert.ToInt32(folderId));
        Array dtClass = System.Enum.GetValues(typeof(Enum_Tatva.Class));
        GetDividerForDropDown = string.Empty;
        GetClassForDropDown = string.Empty;
        for (int i = 0; i < 3; i++)
        {
            GetClassForDropDown += (!string.IsNullOrEmpty(GetClassForDropDown) ? ",{" : "{") +
                                                  "\"Name\"" + ":\"" + dtClass.GetValue(i) + " - " + (i + 1) + "\"," +
                                                  "\"ClassID\"" + ":\"" + (i + 1) + "\"}";
        }
        Shinetech.DAL.Account user = new Shinetech.DAL.Account(Sessions.SwitchedSessionId);
        if (user.IsSubUser.ToString() == "1")
        {
            List<DataRow> temp = this.DividerSource.AsEnumerable().ToList();
            foreach (DataRow row in temp)
            {
                if (row.Field<bool>("Locked") == true)
                    continue;
                else
                {
                    GetDividerForDropDown += (!string.IsNullOrEmpty(GetDividerForDropDown) ? ",{" : "{") +
                                                  "\"Name\"" + ":\"" + Convert.ToString(row.Field<string>("Name")).Replace("'", "\\'") + "\"," +
                                                  "\"DividerID\"" + ":\"" + Convert.ToString(row.Field<int>("DividerID")).Replace("'", "\\'") + "\"," +
                                                  "\"Color\"" + ":\"" + Convert.ToString(row.Field<string>("Color")).Replace("'", "\\'") + "\"}";
                }
            }
        }
        else
        {
            this.DividerSource.AsEnumerable().ToList().ForEach(d => GetDividerForDropDown += (!string.IsNullOrEmpty(GetDividerForDropDown) ? ",{" : "{") +
                                                  "\"Name\"" + ":\"" + Convert.ToString(d.Field<string>("Name")).Replace("'", "\\'") + "\"," +
                                                  "\"DividerID\"" + ":\"" + Convert.ToString(d.Field<int>("DividerID")).Replace("'", "\\'") + "\"," +
                                                  "\"Color\"" + ":\"" + Convert.ToString(d.Field<string>("Color")).Replace("'", "\\'") + "\"}");
        }
        GetClassForDropDown = !string.IsNullOrEmpty(GetClassForDropDown) ? "[" + GetClassForDropDown + "]" : "";
        GetDividerForDropDown = !string.IsNullOrEmpty(GetDividerForDropDown) ? "[" + GetDividerForDropDown + "]" : "";
        _logger.Debug("GetDividerData : " + stopWatch.Elapsed.ToString());
    }


    /// <summary>
    /// Gets the System.Drawing.Color object from hex string.
    /// </summary>
    /// <param name="hexString">The hex string.</param>
    /// <returns></returns>
    private System.Drawing.Color GetSystemDrawingColorFromHexString(string hexString)
    {
        if (!System.Text.RegularExpressions.Regex.IsMatch(hexString, @"[#]([0-9]|[a-f]|[A-F]){6}\b"))
            throw new ArgumentException();
        int red = int.Parse(hexString.Substring(1, 2), NumberStyles.HexNumber);
        int green = int.Parse(hexString.Substring(3, 2), NumberStyles.HexNumber);
        int blue = int.Parse(hexString.Substring(5, 2), NumberStyles.HexNumber);
        return Color.FromArgb(red, green, blue);
    }


    private void GetReminderData(string folderId)
    {
        DataTable table = FileFolderManagement.GetFolderReminders(folderId);

        var updatePanel2 = this.UpdatePanel1.FindControl("UpdatePanel2") as UpdatePanel;
        int count = 0;
        foreach (DataRow row in table.Rows)
        {
            count++;
            string name = row["Name"].ToString();

            TextBox box = updatePanel2.FindControl("textfieldName" + count) as TextBox;

            if (box != null)
            {
                box.Text = name;
            }

            DateTime? dt = row["ReminderDate"] as DateTime?;

            CalendarExtender textRequestDate = updatePanel2.FindControl("CalendarExtender" + count) as CalendarExtender;

            if (textRequestDate != null)
            {
                textRequestDate.SelectedDate = dt;

            }
        }
    }

    private void GetData(string folderId, int? dividerId = null)
    {
        try
        {
            DataSet ds = FileFolderManagement.GetDocumentsOnDemand(Convert.ToInt32(folderId), dividerId == 0 ? (int)dividerId : (string.IsNullOrEmpty(this.DividerID_FFB) ? 0 : Convert.ToInt32(this.DividerID_FFB)));
            if (ds != null && ds.Tables.Count > 0)
            {
                this.DataSource = ds.Tables[0];
                this.DocumentOrderSource = ds.Tables[1];
                string fileName = string.Empty;
                if (this.DataSource != null && this.DataSource.Rows.Count > 0)
                {
                    var pathList = new List<string>();
                    foreach (DataRow row in this.DataSource.Rows)
                    {
                        if (string.IsNullOrEmpty(row["DisplayName"].ToString()))
                        {
                            row["DisplayName"] = row["Name"].ToString();
                        }

                        int FileExtention = string.IsNullOrEmpty(Convert.ToString(row["FileExtention"])) ? 1 : Convert.ToInt32(row["FileExtention"].ToString());
                        if (FileExtention == 14)
                        {
                            fileName = (row["FullFileName"].ToString()).Replace("%", "$");
                            
                        }
                        else
                        {
                            fileName = System.Web.HttpUtility.UrlPathEncode(row["Name"].ToString()).Replace("%", "$") + "." + Enum.GetName(typeof(Shinetech.Engines.StreamType), FileExtention);
                        }
                        string pathName = System.Web.HttpContext.Current.Server.MapPath("~/" + row["PathName"].ToString() + Path.DirectorySeparatorChar + fileName);
                        try
                        {
                            if (enablePhysicalMapping && !File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/" + row["PathName"].ToString() + Path.DirectorySeparatorChar + fileName.Replace(" ","$20"))))
                            {
                                row.Delete();
                            }
                            if (pathList.Contains(pathName))
                            {
                                row.Delete();
                            }
                            pathList.Add(pathName);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    DataView dvSource = this.DataSource.AsDataView();
                    dvSource.Sort = "DividerID ASC,DocumentOrder DESC";
                    this.DataSource = dvSource.ToTable();
                    if (Convert.ToInt32(ds.Tables[2].Rows[0]["PageCount"]) > 1 && this.DataSource.Rows.Count < 10)
                    {
                        hdnIsScrollable.Value = "true";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    [WebMethod]
    public static string GetDocumentsOnScroll(int folderId, int dividerId, int pageIndex)
    {
        try
        {
            //System.Threading.Thread.Sleep(2000);
            Office_FileFolderBox fileFolderBox = new Office_FileFolderBox();
            DataSet ds = FileFolderManagement.GetDocumentsOnDemand(folderId, dividerId, pageIndex, 10);
            string fileName = string.Empty;
            if (ds != null && ds.Tables.Count > 0)
            {
                DataTable dtNew = ds.Tables[0];
                if (dtNew != null && dtNew.Rows.Count > 0)
                {
                    var pathList = new List<string>();
                    foreach (DataRow row in dtNew.Rows)
                    {
                        if (string.IsNullOrEmpty(row["DisplayName"].ToString()))
                        {
                            row["DisplayName"] = row["Name"].ToString();
                        }

                        int FileExtention = string.IsNullOrEmpty(Convert.ToString(row["FileExtention"])) ? 1 : Convert.ToInt32(row["FileExtention"].ToString());
                        if (FileExtention == 14)
                        {
                            fileName = (row["FullFileName"].ToString()).Replace("%", "$");
                        }
                        else
                        {
                            fileName = HttpUtility.UrlPathEncode(row["Name"].ToString()).Replace("%", "$") + "." + Enum.GetName(typeof(Shinetech.Engines.StreamType), FileExtention);
                        }
                        string pathName = HttpContext.Current.Server.MapPath("~/" + row["PathName"].ToString() + Path.DirectorySeparatorChar + fileName);
                        try
                        {
                            if (enablePhysicalMapping && !File.Exists(HttpContext.Current.Server.MapPath("~/" + row["PathName"].ToString() + Path.DirectorySeparatorChar + fileName.Replace(" ", "$20"))))
                            {
                                row.Delete();
                            }
                            if (pathList.Contains(pathName))
                            {
                                row.Delete();
                            }
                            pathList.Add(pathName);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    DataView dvSource = dtNew.AsDataView();
                    dvSource.Sort = "DividerID ASC,DocumentOrder DESC";
                    dtNew = dvSource.ToTable();
                    //if (Convert.ToInt32(ds.Tables[1].Rows[0]["PageCount"]) > 1 && fileFolderBox.DataSource.Rows.Count < 10)
                    //{
                    //    hdnIsScrollable.Value = "true";
                    //}
                    dtNew.TableName = "Documents";
                    ds.Tables.Remove("Documents");
                    ds.Tables.Add(dtNew);
                }
            }

            return ds.GetXml();
        }
        catch (Exception ex)
        {
            return null;
        }
        //fileFolderBox.GetData("117069", pageIndex, 10);
    }

    public void GetFilteredDocuments()
    {
        try
        {
            TextBox txtClass1 = (TextBox)UpdatePanel1.FindControl("txtClass");
            TextBox txtName1 = (TextBox)UpdatePanel1.FindControl("txtName");
            DateTime? fromDate = null;
            TextBox txtFDate1 = (TextBox)UpdatePanel1.FindControl("txtFDate");
            if (txtFDate1 != null && !string.IsNullOrEmpty(txtFDate1.Text))
            {
                fromDate = ChangeDateFormat(txtFDate1.Text);
                if (fromDate == DateTime.MinValue)
                {
                    fromDate = null;
                }
            }
            DateTime? ToDate = null;
            TextBox txtTDate1 = (TextBox)UpdatePanel1.FindControl("txtTDate");
            if (txtTDate1 != null && !string.IsNullOrEmpty(txtTDate1.Text))
            {
                ToDate = ChangeDateFormat(txtTDate1.Text);
                if (ToDate == DateTime.MinValue)
                {
                    ToDate = null;
                }
            }

            TextBox txtIndex1 = (TextBox)UpdatePanel1.FindControl("txtIndex");

            DataSet ds = FileFolderManagement.GetFilteredDocsOnDemand(Convert.ToInt32(this.Request.QueryString["id"]), (string.IsNullOrEmpty(this.DividerID_FFB) ? 0 : Convert.ToInt32(this.DividerID_FFB)), txtName1.Text.Trim(), txtClass1.Text.Trim(), fromDate, ToDate, txtIndex1.Text.Trim());
            if (ds != null && ds.Tables.Count > 0)
            {
                this.DataSource = ds.Tables[0];
                if (this.DataSource != null && this.DataSource.Rows.Count > 0)
                {
                    foreach (DataRow row in this.DataSource.Rows)
                    {
                        if (string.IsNullOrEmpty(row["DisplayName"].ToString()))
                        {
                            row["DisplayName"] = row["Name"].ToString();
                        }
                    }
                }
            }
            BindDividers();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    public void GetOcrDocuments()
    {
        try
        {
            var searchResponse = highClient
            .Search<DocumentIndex>(s => s
                //.From(page)
                //.Size(10)
                .Query(q => q
                    .Bool(b => b
                        .Must(mu => mu
                            .MultiMatch(mm => mm
                                .Fields(fs => fs
                                    .Field(f => f.FolderId))
                                .Query(this.EffID_FFB)) &&
                            q.MultiMatch(mm => mm
                                .Fields(fs => fs
                                    .Field(f => f.DividerId))
                                .Query(this.DividerID_FFB)) &&
                            q.MultiMatch(mm => mm
                                .Fields(fs => fs
                                    .Field(f => f.IsDeleted))
                                .Query("false"))
                        )
                    )
                )
                .Highlight(h => h
                    .PreTags("<span class='yellow'>")
                    .PostTags("</span>")
                    .Encoder(HighlighterEncoder.Html)
                    .Fields(
                        fs => fs
                            .Field("jsonData.ngram")
                            .Type(HighlighterType.Plain)
                            .FragmentSize(50)
                            .ForceSource(true)
                            .Fragmenter(HighlighterFragmenter.Span)
                            .NumberOfFragments(150)
                            .NoMatchSize(50)
                            .PhraseLimit(100)
                            .BoundaryMaxScan(50)
                            .HighlightQuery(hl => hl
                                .Fuzzy(m => m
                                    .Name("ngram")
                                    .Boost(1.1)
                                    .Field("jsonData.ngram")
                                    .Value(txtKeyword.Text.ToLower())
                                    .Rewrite(MultiTermQueryRewrite.ConstantScore)
                                    .Transpositions(true)
                                 )
                            ),
                        fs => fs
                            .Field("name.ngram")
                            .Type(HighlighterType.Plain)
                            .FragmentSize(50)
                            .ForceSource(true)
                            .Fragmenter(HighlighterFragmenter.Span)
                            .NumberOfFragments(150)
                            .NoMatchSize(50)
                            .PhraseLimit(100)
                            .BoundaryMaxScan(50)
                            .HighlightQuery(hl => hl
                                .Fuzzy(m => m
                                    .Name("ngram.Name")
                                    .Boost(1.1)
                                    .Field("name.ngram")
                                    .Value(txtKeyword.Text.ToLower())
                                    .Rewrite(MultiTermQueryRewrite.ConstantScore)
                                    .Transpositions(true)
                                 )
                            )
                    )
                )
                .PostFilter(q => q
                    .MultiMatch(mm => mm
                        .Fields(fs => fs
                            .Field("jsonData.ngram")
                            .Field("name.ngram")
                        )
                        .Query(txtKeyword.Text.ToLower())
                        .Analyzer("standard")
                        .Boost(1.1)
                        .Slop(2)
                        .Fuzziness(Fuzziness.Auto)
                        .PrefixLength(2)
                        .MaxExpansions(2)
                        .Operator(Operator.Or)
                        .MinimumShouldMatch(1)
                        .FuzzyRewrite(MultiTermQueryRewrite.ConstantScore)
                        .TieBreaker(1.1)
                        .CutoffFrequency(0.001)
                        .Lenient()
                        .ZeroTermsQuery(ZeroTermsQuery.All)
                        .Name("ngram")
                        .AutoGenerateSynonymsPhraseQuery(false)
                    )
                )
            );

            DataTable dtNew = new DataTable();
            DataTable dtFinal = new DataTable();
            PropertyInfo[] props = typeof(DocumentIndex).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var prop in props)
            {
                dtNew.Columns.Add(prop.Name, prop.PropertyType);
                dtFinal.Columns.Add(prop.Name, prop.PropertyType);
            }

            if (searchResponse.Documents.Count > 0)
            {
                for (int i = 0; i < searchResponse.Documents.Count; i++)
                {
                    var dr = dtNew.NewRow();
                    dr["FolderId"] = searchResponse.Documents.ToList()[i].FolderId;
                    dr["Name"] = searchResponse.Documents.ToList()[i].Name;
                    dr["DocumentId"] = searchResponse.Documents.ToList()[i].DocumentId;
                    dr["DividerId"] = searchResponse.Documents.ToList()[i].DividerId;
                    dr["Path"] = searchResponse.Documents.ToList()[i].Path;
                    dr["Size"] = searchResponse.Documents.ToList()[i].Size;
                    dr["DisplayName"] = searchResponse.Documents.ToList()[i].Name;
                    if (searchResponse.Hits.Count > 0)
                    {
                        if (searchResponse.Hits.ToList()[i].MatchedQueries.Count <= 0)
                            continue;
                        foreach (var responses in searchResponse.Hits.ToList()[i].Highlights.ToList())
                        {
                            {
                                foreach (var highLights in responses.Value.Highlights)
                                {
                                    if (responses.Key == "jsonData.ngram")
                                        dr["JsonData"] += (highLights + "<span>EdSplit</span>");
                                    else dr["Name"] = highLights;
                                }
                            }
                        }
                    }
                    dtNew.Rows.Add(dr);
                }
                DataTable dtTabs = this.DividerSource;
                var results = from records in dtNew.AsEnumerable()
                              join tabs in dtTabs.AsEnumerable() on Convert.ToInt32(records["DividerId"]) equals Convert.ToInt32(tabs["DividerID"])
                              select new DocumentIndex
                              {
                                  DocumentId = Convert.ToInt32(records["DocumentId"]),
                                  FolderId = Convert.ToInt32(records["FolderId"]),
                                  Name = Convert.ToString(records["Name"]),
                                  DividerId = Convert.ToInt32(records["DividerId"]),
                                  Path = Convert.ToString(records["Path"]).Replace("\\", "/"),
                                  JsonData = Convert.ToString(records["JsonData"]),
                                  Size = (float)records["Size"],
                                  DisplayName = Convert.ToString(records["DisplayName"]),
                                  DividerName = Convert.ToString(tabs["Name"]),
                              };
                //dtNew.Clear();
                foreach (var item in results.ToList())
                {
                    var values = new object[props.Length];
                    for (var i = 0; i < props.Length; i++)
                    {
                        values[i] = props[i].GetValue(item, null);
                    }
                    dtFinal.Rows.Add(values);
                }
            }
            this.OcrDataSource = dtFinal;
            gridViewOcr.DataSource = this.OcrDataSource;
            gridViewOcr.DataBind();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    [WebMethod]
    public static string GetFilteredDocumentsOnScroll(int folderId, int dividerId, string name, string className, DateTime? fromDate, DateTime? toDate, string index, int pageIndex)
    {
        //Added to similate delay so that we see the loader working
        //Must be removed when moving to production
        //System.Threading.Thread.Sleep(2000);
        try
        {
            Office_FileFolderBox fileFolderBox = new Office_FileFolderBox();
            string fileName = string.Empty;
            DataSet ds = FileFolderManagement.GetFilteredDocsOnDemand(folderId, dividerId, name, className, fromDate, toDate, index, pageIndex, 10);
            if (ds != null && ds.Tables.Count > 0)
            {
                DataTable dtNew = ds.Tables[0];
                if (dtNew != null && dtNew.Rows.Count > 0)
                {
                    var pathList = new List<string>();
                    foreach (DataRow row in dtNew.Rows)
                    {
                        if (string.IsNullOrEmpty(row["DisplayName"].ToString()))
                        {
                            row["DisplayName"] = row["Name"].ToString();
                        }

                        int FileExtention = string.IsNullOrEmpty(Convert.ToString(row["FileExtention"])) ? 1 : Convert.ToInt32(row["FileExtention"].ToString());
                        if (FileExtention == 14)
                        {
                            fileName = (row["FullFileName"].ToString()).Replace("%", "$");
                        }
                        else
                        {
                            fileName = HttpUtility.UrlPathEncode(row["Name"].ToString()).Replace("%", "$") + "." + Enum.GetName(typeof(Shinetech.Engines.StreamType), FileExtention);
                        }
                        string pathName = HttpContext.Current.Server.MapPath("~/" + row["PathName"].ToString() + Path.DirectorySeparatorChar + fileName);
                    }
                    DataView dvSource = dtNew.AsDataView();
                    dvSource.Sort = "DividerID ASC,DocumentOrder DESC";
                    dtNew = dvSource.ToTable();
                    dtNew.TableName = "Documents";
                    ds.Tables.Remove("Documents");
                    ds.Tables.Add(dtNew);
                }
            }

            return ds.GetXml();
        }
        catch (Exception ex)
        {
            return null;
        }
        //fileFolderBox.GetData("117069", pageIndex, 10);
    }

    /// <summary>
    /// Եʼۯѳ֨
    /// </summary>
    private void BindGrid()
    {
        GridView1.DataSource = this.DataSource;
        GridView1.DataBind();
    }

    private void CopyColumns(DataTable source, DataTable dest, bool isMoveDividers, params string[] columns)
    {
        foreach (string colname in columns)
        {
            dest.Columns.Add(colname);
        }

        foreach (DataRow sourcerow in source.Rows)
        {
            Shinetech.DAL.Account user = new Shinetech.DAL.Account(Sessions.SwitchedSessionId);
            //Shinetech.DAL.Account user = new Shinetech.DAL.Account(Membership.GetUser().ProviderUserKey.ToString());
            if (isMoveDividers && user.IsSubUser.ToString() == "1" && sourcerow.Field<bool>("Locked") == true)
            {
                continue;
            }
            DataRow destRow = dest.NewRow();
            foreach (string colname in columns)
            {
                destRow[colname] = sourcerow[colname];
            }
            dest.Rows.Add(destRow);
        }
    }

    private DataTable SortDataTable(DataTable dtSource, string sortColumn)
    {
        DataView dataview = dtSource.DefaultView;
        dataview.Sort = sortColumn;
        return dataview.ToTable();
    }

    private void BindDividers()
    {
        try
        {
            int count = this.DividerSource.Rows.Count;

            CopyColumns(this.DividerSource, dtDividerMove, true, "DividerID", "Name", "Color");
            dtDividerMove = SortDataTable(dtDividerMove, "DividerID");

            Shinetech.DAL.Account user = new Shinetech.DAL.Account(Sessions.SwitchedSessionId);
            //Shinetech.DAL.Account user = new Shinetech.DAL.Account(Membership.GetUser().ProviderUserKey.ToString());
            DataTable dt = null;
            if (user.IsSubUser.ToString() == "1")
                dt = General_Class.GetTabRestForSubUser(Sessions.SwitchedSessionId);
            //dt = General_Class.GetTabRestForSubUser(Membership.GetUser().ProviderUserKey.ToString());

            int lockedTabs = 0, unLockedTabs = 0;
            for (int i = 0; i < count; i++)
            {
                dtOrder = this.DocumentOrderSource;

                DataRow item = this.DividerSource.Rows[i];

                TabPanel panel = fileTabs.Tabs[i];
                panel.HeaderText = (string)item["Name"];
                dividerid = Convert.ToInt32(this.DividerSource.Rows[i]["DividerID"]);

                panel.OnClientClick = "function (){setTab(" + dividerid + ", 'Panel" + i + "_GridView" + i + "');}";
                string color = Convert.ToString(this.DividerSource.Rows[i]["Color"]);
                panel.Style.Add("background-color", color);
                //panel.Style.Add("height", "20px");
                Color color1 = GetSystemDrawingColorFromHexString(color);
                Color color2 = ContrastColor(color1);
                string fontColor = color2.Name;
                fontColor = ReplaceFirst(fontColor, "ff", "#");
                panel.Style.Add("color", fontColor);

                short s;
                short.TryParse(i.ToString(), out s);
                panel.TabIndex = s;


                if (dt != null && dt.Rows.Count > 0 && dt.Rows[0]["IsHideLockedTabs"].ToString().ToLower() == "true" && item.Field<bool>("Locked") == true)
                {
                    panel.Enabled = false;
                    lockedTabs++;
                }
                else
                {
                    panel.Enabled = true;
                    GridView gridV = fileTabs.Controls[i].FindControl("GridView" + i) as GridView;

                    DataView dvSource = this.DataSource.AsDataView();
                    dvSource.Sort = "DividerID ASC,DocumentOrder DESC";
                    this.DataSource = dvSource.ToTable();

                    DataView view = new DataView(this.DataSource);
                    view.RowFilter = string.Format("DividerID={0}", item["DividerID"]);

                    //CopyColumns(view.ToTable(), dtOrder, false, "DocumentOrder");
                    if (gridV != null)
                    {
                        gridV.DataSource = view;
                        gridV.DataBind();
                    }
                    //  unLockedTabs++;
                }
                panel.Visible = true;

                // }
            }
            fileTabs.ActiveTabIndex = this.ActiveTabIndex;
            if (lockedTabs == count)
            {
                docDetails.Visible = false;
                uploadDiv.Visible = false;
            }
            else
            {
                docDetails.Visible = true;
                uploadDiv.Visible = true;
            }
            _logger.Debug("BindDividers : " + stopWatch.Elapsed.ToString());
        }
        catch (Exception ex)
        { }
    }

    string ReplaceFirst(string text, string search, string replace)
    {
        int pos = text.IndexOf(search);
        if (pos < 0)
        {
            return text;
        }
        return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);
    }

    Color ContrastColor(Color color)
    {
        int d = 0;

        // Counting the perceptive luminance - human eye favors green color... 
        double a = 1 - (0.299 * color.R + 0.587 * color.G + 0.114 * color.B) / 255;

        if (a < 0.5)
            d = 0; // bright colors - black font
        else
            d = 255; // dark colors - white font

        return Color.FromArgb(d, d, d);
    }

    protected void imageField_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string id = this.Request.QueryString["id"];
            int boxid;
            if (!string.IsNullOrEmpty(id) && int.TryParse(id, out boxid))
            {

                try
                {
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                    "alert(\"" + "Update Box Successfully" + "\");", true);

                }
                catch (Exception ex)
                {


                    string strWrongInfor = string.Format("Error : {0}", ex.Message);
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                            "alert(\"" + strWrongInfor + "\");", true);
                }
            }

        }
    }

    //protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    //{
    //    WebPager1.CurrentPageIndex = e.NewPageIndex;
    //    if (this.DataSource != null)
    //    {
    //        WebPager1.DataSource = this.DataSource;
    //        WebPager1.DataBind();
    //    }
    //}

    //protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    //{
    //    WebPager1.CurrentPageIndex = 0;
    //    if (this.DataSource != null)
    //    {
    //        WebPager1.DataSource = this.DataSource;
    //        WebPager1.DataBind();
    //    }
    //}

    protected string GetFileExtention(StreamType ext)
    {
        string extname = ".pdf";
        switch (ext)
        {
            case StreamType.PDF:
                extname = ".pdf";
                break;
            case StreamType.Word:
                extname = ".doc";
                break;
            case StreamType.Excel:
                extname = ".xls";
                break;
            case StreamType.PPT:
                extname = ".ppt";
                break;
            case StreamType.RTF:
                extname = ".rtf";
                break;
            case StreamType.ODP:
                extname = ".odp";
                break;
            case StreamType.ODS:
                extname = ".ods";
                break;
            case StreamType.ODT:
                extname = ".odt";
                break;
            case StreamType.PNG:
                extname = ".png";
                break;
            case StreamType.JPG:
                extname = ".jpg";
                break;
            default:
                break;
        }

        return extname;
    }

    public bool ExistBox(string name, string uid)
    {
        return FileFolderManagement.ExistBox(name, uid);
    }

    public string MessageBody
    {
        get { return this.Session["_Side_MessageBody"] as string; }

        set { this.Session["_Side_MessageBody"] = value; }
    }

    public bool IsFileValid()
    {
        return true;
    }

    protected void DropDownList1_OnSelectedIndexChanged(object sender, EventArgs e)
    {
    }

    protected void btnClose_OnClick(object sender, EventArgs e)
    {
        Page.Response.Redirect("~/Office/Welcome.aspx");
    }

    protected void anotherRequest_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect("~/Office/CreateFileRequest.aspx");
    }

    protected void LinkButton2_Click(Object sender, CommandEventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        var folderId = Convert.ToString(e.CommandArgument).Split(',')[0];
        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(folderId), 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.BookView.GetHashCode(), 0, 0);
        ((MasterPage)this.Master).AddFolderItem(e.CommandArgument.ToString());
    }

    protected void downloadall_Click(object sender, EventArgs e)
    {
        string id = DocumentIDs.Value;
        Response.Redirect("PDFDownloader.aspx?ID=" + id);
    }

    protected void ShowPopup2Button2_Click(Object sender, EventArgs e)
    {
        this.GetDividerData(this.EffID_FFB);
        this.BindDividers();
    }

    protected void AjaxFileUpload1_OnUploadComplete(object sender, AjaxFileUploadEventArgs file)
    {
        bool isChecked = false;
        int dividerIdForFile = 0;
        int classIdForFile = 1;
        bool isRename = false;
        string dividerName = string.Empty;
        string folderName = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(Request.Cookies["IsRenamed"].Value.ToString()))
                isRename = true;
            if (!string.IsNullOrEmpty(Request.Cookies["FolderName"].Value.ToString()))
            {
                folderName = Request.Cookies["FolderName"].Value.ToString();
            }
            if (!string.IsNullOrEmpty(Request.Cookies["Files"].Value.ToString()))
            {
                dividerIdForFile = GetDividerIdForUploadFile(Request.Cookies["Files"].Value.ToString(), file);
                dividerName = GetDividerNameForUploadFile(Request.Cookies["Files"].Value.ToString(), file);
            }
            if (!string.IsNullOrEmpty(Request.Cookies["Class"].Value.ToString()))
                classIdForFile = GetClassIdForUploadFile(Request.Cookies["Class"].Value.ToString(), file);
            if (!string.IsNullOrEmpty(Request.Cookies["OCR"].Value.ToString()))
                isChecked = GetCheckIdForUploadFile(Request.Cookies["OCR"].Value.ToString(), file);

            byte[] fullfile = ProcessFile(AjaxFileUpload1, file, dividerIdForFile, isRename, dividerName, folderName);
            AwsUpload awsUpload = new AwsUpload();
            awsUpload.UploadObjectAsync(fullfile);
            ProcessFile(file, fullfile, dividerIdForFile, classIdForFile, isChecked);

            if (Convert.ToInt32(this.DocumentID) > 0)
            {
                Shinetech.DAL.General_Class.AuditLogByDocId(Convert.ToInt32(this.DocumentID), Sessions.SwitchedSessionId, Enum_Tatva.Action.Create.GetHashCode());
                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(this.EffID_FFB), dividerIdForFile, Convert.ToInt32(this.DocumentID), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Upload.GetHashCode(), 0, 0);
            }
            else
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert('Failed to upload file. Please try again after sometime.');", true);
            this.GetDividerData(this.EffID_FFB);
            this.GetData(this.EffID_FFB);
            this.BindActiveDivider(this.EffID_FFB);
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(Convert.ToString(this.EffID_FFB), dividerIdForFile.ToString(), newFileName, ex);
        }
    }

    private int GetDividerIdForUploadFile(string fileCookieData, AjaxFileUploadEventArgs file)
    {
        int dividerIdForFile = 0;
        List<UploadFile> objUploadFile = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UploadFile>>(fileCookieData);
        if (objUploadFile.Count > 0)
            dividerIdForFile = Convert.ToInt32(objUploadFile.Where(d => d.FileID == file.FileId).Select(d => d.dividerID).FirstOrDefault());
        return dividerIdForFile;
    }

    private string GetDividerNameForUploadFile(string fileCookieData, AjaxFileUploadEventArgs file)
    {
        string dividerNameForFile = string.Empty;
        List<UploadFile> objUploadFile = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UploadFile>>(fileCookieData);
        if (objUploadFile.Count > 0)
            dividerNameForFile = Convert.ToString(objUploadFile.Where(d => d.FileID == file.FileId).Select(d => d.dividerName).FirstOrDefault());
        return dividerNameForFile;
    }

    private int GetClassIdForUploadFile(string fileCookieData, AjaxFileUploadEventArgs file)
    {
        int classIdForFile = 0;
        List<UploadFile> objUploadFile = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UploadFile>>(fileCookieData);
        if (objUploadFile.Count > 0)
            classIdForFile = Convert.ToInt32(objUploadFile.Where(d => d.FileID.Replace("class", "") == file.FileId).Select(d => d.classId).FirstOrDefault());
        return classIdForFile;
    }

    private bool GetCheckIdForUploadFile(string fileCookieData, AjaxFileUploadEventArgs file)
    {
        bool checkIdForFile = false;
        List<UploadFile> objUploadFile = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UploadFile>>(fileCookieData);
        if (objUploadFile.Count > 0)
            checkIdForFile = Convert.ToBoolean(objUploadFile.Where(d => d.FileID.Replace("check", "") == file.FileId).Select(d => d.CheckId).FirstOrDefault());
        return checkIdForFile;
    }

    public void ProcessFile(AjaxFileUploadEventArgs file, byte[] fileStream, int dividerIdForFile, int classIdForFile, bool isChecked)
    {
        try
        {
            WorkItem item = new WorkItem();
            item.FolderID = Convert.ToInt32(this.EffID_FFB);
            item.DividerID = dividerIdForFile;
            string basePath = this.MapPath(this.FolderName);
            string fulFileName = string.Empty;
            item.Path = this.PathUrl + Path.DirectorySeparatorChar +
                        item.FolderID + Path.DirectorySeparatorChar + dividerIdForFile;
            item.FileName = Path.GetFileNameWithoutExtension(newFileName);
            item.FullFileName = Path.GetFileName(newFileName);
            item.UID = Sessions.SwitchedSessionId;
            //item.UID = Membership.GetUser().ProviderUserKey.ToString();
            item.contentType = Common_Tatva.getFileExtention(Path.GetExtension(newFileName));
            item.Content = new MemoryStream(fileStream);
            int order = FlashViewer.GetDocumentOrder(Convert.ToInt32(dividerIdForFile));


            item.MachinePath = string.Concat(basePath, Path.DirectorySeparatorChar, item.FolderID,
            Path.DirectorySeparatorChar, dividerIdForFile);

            if (!Directory.Exists(item.MachinePath))
            {
                Directory.CreateDirectory(item.MachinePath);
            }

            float size = (float)Math.Round((decimal)(fileStream.Length / 1024) / 1024, 2); // IN MB.
            string encodeName = HttpUtility.UrlPathEncode(item.FullFileName);
            encodeName = encodeName.Replace("%", "$");
            string source = string.Concat(item.MachinePath, Path.DirectorySeparatorChar, encodeName);
            var pageCount = 1;
            if (item.contentType == StreamType.JPG)
            {
                Jpeg2Engine jpeg2Engine = new Jpeg2Engine(item);
                jpeg2Engine.generatePDFfromImage(source, source.Split('.')[0] + ".pdf");
                jpeg2Engine.Dispose();
            }
            else if (item.contentType == StreamType.PDF)
            {
                PdfReader pdfReader = new PdfReader(source);
                pageCount = pdfReader.NumberOfPages;
                pdfReader.Close();
                pdfReader.Dispose();
            }
            else if ((item.contentType != StreamType.JPG) || (item.contentType != StreamType.PDF))
            {
                item.contentType = StreamType.OTHERS;
                fulFileName = item.FullFileName;
            }
            this.DocumentID = General_Class.DocumentsInsert(item.FileName, item.FolderID, item.UID, item.Path, item.DividerID, pageCount, size, item.FileName, (int)item.contentType, order + 1, Convert.ToString(classIdForFile), fulFileName);
            if (isChecked && Convert.ToInt32(this.DocumentID) > 0)
            {
                //ConvertPdfToSearchable(new MemoryStream(fileStream));
                string pdfText = PDFParser.ExtractTextFromPdfImages(item.FileName, new MemoryStream(fileStream));
                var document = new DocumentIndex()
                {
                    DocumentId = Convert.ToInt32(this.DocumentID),
                    FolderId = Convert.ToInt32(this.EffID_FFB),
                    DividerId = dividerIdForFile,
                    Name = item.FileName,
                    Path = item.Path,
                    Size = size,
                    JsonData = pdfText,
                    IsDeleted = false,
                    DisplayName = item.FileName
                };
                //Upload doc to elastic server
                bool isValid = Common_Tatva.UploadDocument(document);
                DataTable dtDoc = new DataTable();
                dtDoc.Columns.Add("Index");
                dtDoc.Columns.Add("DocumentId");
                dtDoc.Columns.Add("OcrStatus");
                int ocrStatus = Enum_Tatva.OcrStatus.NoOcr.GetHashCode();

                if (isValid)
                    ocrStatus = Enum_Tatva.OcrStatus.OcrDone.GetHashCode();
                else
                    ocrStatus = Enum_Tatva.OcrStatus.OcrRequested.GetHashCode();

                dtDoc.Rows.Add(1, document.DocumentId, ocrStatus);
                General_Class.UpdateOcrStatus(dtDoc);
            }
            this.DocumentName = item.FileName;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(Convert.ToString(this.EffID_FFB), Convert.ToString(dividerIdForFile), newFileName, ex);

            FilesuploadedRecently_FFB.Add(75);
            FilesuploadedRecently_FFB.Add(83);
            FilesuploadedRecently_FFB.Add(84);
            FilesuploadedRecently_FFB.Add(85);
            FilesuploadedRecently_FFB.Add(86);

            Dictionary<string, string> UploadErrorFileName1 = new Dictionary<string, string>();
            UploadErrorFileName1.Add(file.FileId, file.FileName);

            UploadErrorFileName = UploadErrorFileName1;

            if ((File.Exists(fullFilePath)))
                File.Delete(fullFilePath);
        }
    }
    
    public ArrayList FilesuploadedRecently_FFB
    {
        get
        {
            if (this.Session["FilesuploadedRecently_FFB"] != null)
            {
                return this.Session["FilesuploadedRecently_FFB"] as ArrayList;
            }
            else
            {
                this.Session["FilesuploadedRecently_FFB"] = new ArrayList();
                return this.Session["FilesuploadedRecently_FFB"] as ArrayList;
            }

        }
        set
        {
            this.Session["FilesuploadedRecently_FFB"] = value;
        }
    }

    public string FolderName
    {
        get
        {
            return string.Format("~/{0}", CurrentVolume);
        }

    }

    public string WorkAreaName
    {
        get
        {
            return "~/ScanInBox";
        }

    }

    public string PathUrl
    {
        get { return CurrentVolume; }
    }


    public string DividerID_FFB
    {
        get
        {
            return (string)this.Session["DividerID_FFB"];
        }
        set
        {
            Session["DividerID_FFB"] = value;
        }
    }

    public string EffID_FFB
    {
        get
        {
            return (string)Session["EffID_FFB"];
        }
        set
        {
            Session["EffID_FFB"] = value;

        }
    }

    public string DocumentID
    {
        get
        {
            return (string)Session["DocumentID"];
        }
        set
        {
            Session["DocumentID"] = value;

        }
    }

    public string DocumentName
    {
        get
        {
            return (string)Session["DocumentName"];
        }
        set
        {
            Session["DocumentName"] = value;

        }
    }

    public string SideMessage
    {
        get
        {
            return this.Session["_Side_Message_FFB"] as string;
        }
        set
        {
            this.Session["_Side_Message_FFB"] = value;
        }
    }

    public byte[] ProcessFile(AjaxFileUpload AjaxFileUpload1, AjaxFileUploadEventArgs file, int dividerIdForFile, bool isRename, string dividerName, string folderName)
    {
        try
        {
            if (!String.IsNullOrEmpty(this.FolderName))
            {
                string basePath = this.MapPath(this.FolderName);

                string folderID = this.EffID_FFB;
                string dividerID = Convert.ToString(dividerIdForFile);
                string path = basePath + Path.DirectorySeparatorChar +
                            folderID + Path.DirectorySeparatorChar + dividerIdForFile;

                var fName = file.FileName;
                if (isRename)
                {
                    fName = string.Join(" ", folderName, dividerName) + Path.GetExtension(fName);
                }
                fName = fName.Replace("#", "");
                fName = fName.Replace("&", "");
                fName = Common_Tatva.SubStringFilename(fName);
                string encodeName = HttpUtility.UrlPathEncode(fName);
                encodeName = encodeName.Replace("%", "$");
                string fileName = String.Concat(
                        path,
                        Path.DirectorySeparatorChar,
                        encodeName
                    );
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                byte[] s = file.GetContents();

                fileName = String.Concat(path, Path.DirectorySeparatorChar, Common_Tatva.ChangeExtension(fileName));

                fileName = Common_Tatva.GetRenameFileName(fileName);
                newFileName = Path.GetFileName(fileName);
                newFileName = HttpUtility.UrlDecode(newFileName.Replace('$', '%').ToString());

                AjaxFileUpload1.SaveAs(fileName);
                fullFilePath = fileName;
                return s;
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(Convert.ToString(this.EffID_FFB), dividerIdForFile.ToString(), newFileName, ex);
            return null;
        }
        return null;
    }

    protected void AjaxFileUpload1_UploadCompleteAll(object sender, AjaxFileUploadCompleteAllEventArgs e)
    {
        hdnIsRenamed.Value = "";
        var startedAt = (DateTime)Session["uploadTime"];
        var now = DateTime.Now;
        e.ServerArguments = new JavaScriptSerializer()
            .Serialize(new
            {
                duration = (now - startedAt).Seconds,
                time = DateTime.Now.ToShortTimeString()
            });
    }

    protected void AjaxFileUpload1_UploadStart(object sender, AjaxFileUploadStartEventArgs e)
    {
        this.ActiveTabIndex1 = this.ActiveTabIndex;
        var now = DateTime.Now;
        e.ServerArguments = now.ToShortTimeString();
        Session["uploadTime"] = now;
    }

    protected void fileTabs_ActiveTabChanged(object sender, EventArgs e)
    {
        this.ActiveTabIndex = fileTabs.ActiveTabIndex;
        //hdnActiveTabIndex.Value = fileTabs.ActiveTabIndex.ToString();
        string id = this.Request.QueryString["id"];
        this.DividerID_FFB = this.DividerSource.Rows[ActiveTabIndex]["DividerID"].ToString();
        var panel = fileTabs.FindControl("Panel" + this.ActiveTabIndex);
        var grid = new Control();
        if (panel != null)
        {
            grid = panel.FindControl("GridView" + this.ActiveTabIndex);
        }
        if (grid != null)
            grid.Visible = true;
        gridViewOcr.Visible = false;
        if (drpSearch.SelectedValue == "5")
        {
            if (!string.IsNullOrEmpty(txtKeyword.Text))
            {
                GetOcrDocuments();
                if (grid != null)
                    grid.Visible = false;
                gridViewOcr.Visible = true;
                btnConvertOcr.Visible = false;
            }
            else
            {
                btnConvertOcr.Visible = true;
                this.GetData(id);
            }
        }
        else
        {
            btnConvertOcr.Visible = true;
            if (!string.IsNullOrEmpty(txtClass.Text)
                || !string.IsNullOrEmpty(txtName.Text)
                || !string.IsNullOrEmpty(txtFDate.Text)
                || !string.IsNullOrEmpty(txtTDate.Text)
                || !string.IsNullOrEmpty(txtIndex.Text))
            {
                GetFilteredDocuments();
            }
            else
            {
                this.GetData(id);
            }
        }
        GetDividerData(id);
        ScriptManager.RegisterStartupScript(this, GetType(), "myFunction", "SetDividerId('" + this.DividerID_FFB + "');SetPageIndex(" + this.ActiveTabIndex + ");", true);
        BindDividers();
    }
    public int ActiveTabIndex
    {
        get
        {
            return Convert.ToInt32(this.Session["DFileFolderBox_ActiveTabIndex"]);
        }
        set
        {
            this.Session["DFileFolderBox_ActiveTabIndex"] = value;
        }
    }

    public int ActiveTabIndex1
    {
        get
        {
            return Convert.ToInt32(this.Session["DFileFolderBox_ActiveTabIndex1"]);
        }
        set
        {
            this.Session["DFileFolderBox_ActiveTabIndex1"] = value;
        }
    }

    private void BindActiveDivider(string folderId = "")
    {
        string id = string.IsNullOrEmpty(folderId) ? HttpContext.Current.Request.QueryString["id"] : folderId;
        GetDividerData(id);
        int i = fileTabs.ActiveTabIndex;

        DataRow item = this.DividerSource.Rows[i];
        TabPanel panel = fileTabs.Tabs[i];
        panel.Visible = true;

        GridView gridV = fileTabs.Controls[i].FindControl("GridView" + i) as GridView;
        DataView view = new DataView(this.DataSource);
        view.RowFilter = string.Format("DividerID={0}", item["DividerID"]);

        gridV.DataSource = view;
        gridV.DataBind();
    }

    private string BindAllDivider(string uploadFileData)
    {
        try
        {
            string id = HttpContext.Current.Request.QueryString["id"];
            GetDividerData(id);
            string errorFileName = string.Empty;
            int i = fileTabs.ActiveTabIndex;
            DataRow item = this.DividerSource.Rows[i];
            TabPanel panel = fileTabs.Tabs[i];
            panel.Visible = true;
            Exception ex = new Exception();

            List<UploadFile> objUploadFile = Newtonsoft.Json.JsonConvert.DeserializeObject<List<UploadFile>>(uploadFileData);
            if (objUploadFile.Count > 0)
            {
                for (int j = 0; j < objUploadFile.Count; j++)
                {
                    if (!UploadErrorFileName.ContainsKey(objUploadFile[j].FileID))
                    {
                        //Common_Tatva.WriteErrorLog(id, (objUploadFile[j].dividerID).ToString(), "Function :   BindAllDivider-----" + "Action :   Before going to BindAllGrid" + "-----" + "Total Rows: " + DividerSource.Rows.Count, ex);
                        BindAllGrid(Convert.ToInt32(objUploadFile[j].dividerID));
                    }
                    else
                    {
                        string fileName = UploadErrorFileName[objUploadFile[j].FileID];
                        errorFileName = !(string.IsNullOrEmpty(errorFileName)) ? ", " + fileName : fileName;
                        UploadErrorFileName.Remove(objUploadFile[j].FileID);
                    }
                }
            }
            BindDividers();
            _logger.Debug("BindAllDivider : " + stopWatch.Elapsed.ToString());
            return errorFileName;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(HttpContext.Current.Request.QueryString["id"], "", "BindAllDivider Exception", ex);
            return string.Empty;
        }
    }

    private void BindAllGrid(int dividerId)
    {
        try
        {
            Exception ex = new Exception();
            string id = HttpContext.Current.Request.QueryString["id"];
            GetDividerData(id);
            int index = DividerSource.AsEnumerable().ToList().FindIndex(d => d.Field<int>("DividerID") == dividerId);
            GridView gridV = fileTabs.Controls[index].FindControl("GridView" + index) as GridView;
            DataView view = new DataView(this.DataSource);
            view.RowFilter = string.Format("DividerID={0}", dividerId);
            gridV.DataSource = view;
            gridV.DataBind();
            _logger.Debug("BindAllGrid : " + stopWatch.Elapsed.ToString());
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(HttpContext.Current.Request.QueryString["id"], dividerId.ToString(), "BindAllGrid Exception", ex);
        }
    }

    protected void folderEditButton_OnClick(object sender, EventArgs e)
    {
        var s = folderEditButton.Text;

        if (s.Equals("Edit", StringComparison.CurrentCultureIgnoreCase))
        {
            textFoldeName.ReadOnly = false;
            textFirstName.ReadOnly = false;
            textLastName.ReadOnly = false;
            textBoxFileNumber.ReadOnly = false;
            textBoxEmail.ReadOnly = false;
            textBoxTel.ReadOnly = false;
            textBoxFax.ReadOnly = false;

            textBoxAlert1.ReadOnly = false;
            textBoxAlert2.ReadOnly = false;
            textBoxCity.ReadOnly = false;
            textBoxAddress.ReadOnly = false;
            textBoxZipCode.ReadOnly = false;
            textBoxComments.ReadOnly = false;
            textfield5.ReadOnly = false;
            drpState.Enabled = true;
            textBoxSite.ReadOnly = false;
            folderEditButton.Text = "Save";
        }
        else
        {
            textFoldeName.ReadOnly = true;
            textFirstName.ReadOnly = true;
            textLastName.ReadOnly = true;
            textBoxFileNumber.ReadOnly = true;
            textBoxEmail.ReadOnly = true;
            textBoxTel.ReadOnly = true;
            textBoxFax.ReadOnly = true;

            textBoxAlert1.ReadOnly = true;
            textBoxAlert2.ReadOnly = true;
            textBoxCity.ReadOnly = true;
            textBoxAddress.ReadOnly = true;
            textBoxZipCode.ReadOnly = true;
            textBoxComments.ReadOnly = true;
            drpState.Enabled = false;
            textfield5.ReadOnly = true;
            textBoxSite.ReadOnly = true;


            string id = this.Request.QueryString["id"];
            int boxid;
            if (!string.IsNullOrEmpty(id) && int.TryParse(id, out boxid))
            {
                try
                {
                    FileFolder folder = new FileFolder(boxid);

                    folder.FolderName.Value = textFoldeName.Text.Trim();
                    folder.FirstName.Value = textFirstName.Text.Trim();
                    folder.Lastname.Value = textLastName.Text.Trim();
                    folder.FileNumber.Value = textBoxFileNumber.Text.Trim();
                    folder.Email.Value = textBoxEmail.Text.Trim();
                    folder.Tel.Value = textBoxTel.Text.Trim();
                    folder.Postal.Value = textBoxFax.Text.Trim();
                    folder.Alert1.Value = textBoxAlert1.Text.Trim();
                    folder.Alert2.Value = textBoxAlert2.Text.Trim();
                    folder.City.Value = textBoxCity.Text.Trim();
                    folder.Address.Value = textBoxAddress.Text.Trim();
                    folder.Postal.Value = textBoxZipCode.Text.Trim();
                    folder.Comments.Value = textBoxComments.Text.Trim();
                    folder.StateID.Value = this.drpState.SelectedValue;
                    folder.Site1.Value = textBoxSite.Text.Trim();
                    folder.DOB.Value = textfield5.Text.Trim();
                    DataTable isExist = FileFolderManagement.IsFolderNameExist(folder.OfficeID1.Value, folder.FolderName.Value, folder.FileNumber.Value, folder.FolderID.Value);
                    if (isExist == null || isExist.Rows.Count == 0)
                    {
                        folder.Update();
                        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(id), 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Edit.GetHashCode(), 0, 0);
                    }
                    else
                    {
                        string strWrongInfor = string.Format("This EdFile cannot be edited, the File Number you entered already exsists in your account.");
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");hideLoader();", true);
                    }
                }
                catch (Exception)
                {

                }
            }
            folderEditButton.Text = "Edit";
        }
    }

    protected void folderkArchiveButton_OnClick(object sender, EventArgs e)
    {
        string password = txtBoxArchivePwd.Text.Trim();
        string strPassword = PasswordGenerator.GetMD5(password);

        if (UserManagement.GetPassword(User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);
        }
        else
        {
            string id = this.Request.QueryString["id"];
            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(id), 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Archive.GetHashCode(), 0, 0);
            Response.Redirect(string.Format("~/Office/ArchiveDownloader.aspx?ID={0}", id), true);
            return;
        }

    }

    protected void saveReminderButton_OnClick(object sender, EventArgs e)
    {
        string id = this.Request.QueryString["id"];
        int folderId;

        CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;

        IFormatProvider ifp = cultureInfo.DateTimeFormat;

        if (!string.IsNullOrEmpty(id) && int.TryParse(id, out folderId))
        {
            List<FolderReminder> reminders = new List<FolderReminder>();

            for (int i = 1; i <= 5; i++)
            {
                var reminder = new FolderReminder();

                TextBox box = this.UpdatePanel1.FindControl("textfieldName" + i) as TextBox;

                if (box != null && !string.IsNullOrEmpty(box.Text.Trim()))
                {
                    reminder.Name.Value = box.Text.Trim();

                    TextBox textRequestDate = this.UpdatePanel1.FindControl("textRequestDate" + i) as TextBox;

                    if (textRequestDate != null)
                    {
                        DateTime dt;

                        if (DateTime.TryParseExact(textRequestDate.Text.Trim(), "MM-dd-yyyy", ifp, DateTimeStyles.None, out dt))
                        {
                            reminder.ReminderDate.Value = dt;

                            reminder.FolderId.Value = folderId;
                            reminder.UID.Value = Sessions.SwitchedSessionId;
                            //reminder.UID.Value = Membership.GetUser().ProviderUserKey.ToString();

                            reminders.Add(reminder);
                        }
                    }
                }
            }


            FileFolderManagement.AddFolderReminders(reminders, folderId);

            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(folderId, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.EditReminder.GetHashCode(), 0, 0);
            string strWrongInfor = "Reminders are Stored Successfully.";
            ScriptManager.RegisterClientScriptBlock(this.UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
        }

    }

    public List<FolderReminder> Reminders
    {
        get
        {
            List<FolderReminder> obj = this.Session["Folder_Reminders"] as List<FolderReminder>;

            if (obj == null)
            {
                obj = new List<FolderReminder>();
                this.Session["Folder_Reminders"] = obj;
            }
            return obj;
        }

        set
        {
            this.Session["Folder_Reminders"] = value;
        }
    }


    // ByPratik

    protected void UpdatePanel1_PreRender(object sender, EventArgs e)
    {
        if (Request.Form["__EVENTTARGET"] != null && Request.Form["__EVENTTARGET"] != string.Empty)
        {
            // This code will only be executed if the partial postback                
            //  was raised by a __doPostBack('UpdatePanel1', '')                
            if (Request.Form["__EVENTTARGET"] == UpdatePanel1.ClientID)
            {
                // Use this to Update the GridView after AjaxFileUpload1 

                if (!string.IsNullOrEmpty(Request.Cookies["Files"].Value.ToString()))
                {
                    //string errorFileName = BindAllDivider(hdnUploadFiles.Value);
                    string errorFileName = BindAllDivider(Request.Cookies["Files"].Value.ToString());
                    if (!string.IsNullOrEmpty(errorFileName))
                    {

                        string alertMsg = errorFileName + (errorFileName.Contains(", ") ? " have " : " has ") + "not been uploaded successfully";
                        ScriptManager.RegisterClientScriptBlock(this.UpdatePanel1, typeof(UpdatePanel), "Alert", "hideLoader();alert('" + alertMsg + "');", true);
                    }
                    else
                        ScriptManager.RegisterClientScriptBlock(this.UpdatePanel1, typeof(UpdatePanel), "Alert", "hideLoader();$find('pnlPopup2').show();", true);
                }
                else
                    this.BindActiveDivider();
            }
            _logger.Debug("UpdatePanel1_PreRender", stopWatch.Elapsed.ToString());
        }
    }

    protected void GridView0_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            if (Sessions.HasViewPrivilegeOnly)
            {
                for (int i = 0; i < 11; i++)
                {
                    e.Row.Cells[i].Visible = false;
                }
            }
            if (!Common_Tatva.IsUserSwitched() && (this.Page.User.IsInRole("Users") && !Sessions.HasPrevilegeToDownload))
            {
                e.Row.Cells[1].Text = "Indexing|Share";
            }
        }
        //int FileFolderID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "FileFolderID"));
        string FileFolderID = this.Request.QueryString["id"];
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (Sessions.HasViewPrivilegeOnly)
            {
                for (int i = 0; i < 11; i++)
                {
                    e.Row.Cells[i].Visible = false;
                }
                //HyperLink libDownload = e.Row.FindControl("LinkButton21") as HyperLink;
                //libDownload.Visible = false;
                //LinkButton libDel = e.Row.FindControl("libDelete") as LinkButton;
                //libDel.Visible = false;
                //LinkButton libEdit = e.Row.FindControl("lnkEdit") as LinkButton;
                //libEdit.Visible = false;
                //LinkButton libShare = e.Row.FindControl("lnkShare") as LinkButton;
                //libShare.Visible = false;
            }

            if (!Common_Tatva.IsUserSwitched() && (this.Page.User.IsInRole("Users") && !Sessions.HasPrevilegeToDownload))
            {
                HyperLink libDownload = e.Row.FindControl("LinkButton21") as HyperLink;
                libDownload.Visible = false;
            }
            int DividerID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "DividerID"));
            int DocumentOrder = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "DocumentOrder"));
            int DocumentID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "DocumentID"));
            string Name = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Name"));
            string PathName = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "PathName"));
            //int FileExtention = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "FileExtention"));
            int FileExtention = string.IsNullOrEmpty(DataBinder.Eval(e.Row.DataItem, "FileExtention").ToString()) ? 0 : Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "FileExtention"));
            string ClassName = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Class"));


            string extname = FileExtention == 14 ? Path.GetExtension(Convert.ToString(DataBinder.Eval(e.Row.DataItem, "FullFileName"))).Replace(".", "") : Enum.GetName(typeof(Shinetech.Engines.StreamType), FileExtention);
            string oldPath = PathName + Path.DirectorySeparatorChar + GetFileName(Name) + "." + extname;
            DropDownList ddlDropOrder = (e.Row.FindControl("ddlDropOrder") as DropDownList);
            ddlDropOrder.DataSource = dtOrder;
            ddlDropOrder.DataTextField = "DocumentOrder";
            ddlDropOrder.DataValueField = "DocumentOrder";
            ddlDropOrder.DataBind();

            oldPath = oldPath.Replace(@"\", "/");

            ddlDropOrder.SelectedIndex = ddlDropOrder.Items.IndexOf(ddlDropOrder.Items.FindByValue(DocumentOrder.ToString()));
            if (ddlDropOrder != null)
            {
                ddlDropOrder.Attributes.Add("onchange", "OnchangeOrder(" + DocumentOrder + "," + ddlDropOrder.ClientID + " , " + DividerID + ",'" + ClassName + "');");
            }
            DropDownList ddlDropClass = (e.Row.FindControl("ddlDropClass") as DropDownList);
            if (ddlDropClass != null)
            {
                //ddlDropClass.DataSource = Enum.GetNames(typeof(Enum_Tatva.Class));                
                Array enumClass = Enum.GetValues(typeof(Enum_Tatva.Class));
                foreach (var item in enumClass)
                {
                    ddlDropClass.Items.Add(new System.Web.UI.WebControls.ListItem(((int)item).ToString(), ((int)item).ToString()));
                }

                ddlDropClass.DataTextField = "ClassName";
                ddlDropClass.DataValueField = "ClassName";
                ddlDropClass.DataBind();
                if (ClassName != "")
                {
                    ddlDropClass.SelectedIndex = ddlDropClass.Items.IndexOf(ddlDropClass.Items.FindByText(ClassName));
                    //ddlDropClass.Items.FindByValue(ClassName).Selected = true;
                }
            }

            if (ddlDropClass != null)
            {
                ddlDropClass.Attributes.Add("onchange", "OnchangeClass('" + ClassName + "'," + ddlDropClass.ClientID + " , " + DividerID + "," + DocumentID + ",'" + oldPath + "','" + DocumentOrder + "');");
            }




            DropDownList ddlDrpMove = (e.Row.FindControl("ddlDrpMove") as DropDownList);
            ddlDrpMove.DataSource = dtDividerMove;
            ddlDrpMove.DataTextField = "Name";
            ddlDrpMove.DataValueField = "DividerID";
            ddlDrpMove.DataBind();

            ddlDrpMove.SelectedIndex = ddlDrpMove.Items.IndexOf(ddlDrpMove.Items.FindByValue(DividerID.ToString()));
            //string encodeOldPath = HttpUtility.UrlEncode(oldPath);
            if (ddlDrpMove != null)
            {
                ddlDrpMove.Attributes.Add("onchange", "OnMoveOrder(" + DocumentID + ",'" + ddlDrpMove.ClientID + "' , " + DividerID + ", '" + oldPath + "','" + ClassName + "');");
            }

            //cancel class
            HtmlAnchor aCancelclass = (e.Row.FindControl("cancelClass") as HtmlAnchor);
            if (aCancelclass != null)
            {
                aCancelclass.Attributes.Add("onclick", "cancelClass(" + DocumentOrder + ",'" + ddlDropClass.ClientID + "','" + ddlDrpMove.ClientID + "'," + DividerID + ");");
            }
            // cancel
            HtmlAnchor aCancel = (e.Row.FindControl("cancelOrderMove") as HtmlAnchor);
            if (aCancel != null)
            {
                aCancel.Attributes.Add("onclick", "CancelOrderMove(" + DocumentOrder + ",'" + ddlDropOrder.ClientID + "','" + ddlDrpMove.ClientID + "'," + DividerID + ");");
            }
            //showclass
            HtmlAnchor ashowclass = (e.Row.FindControl("showclass") as HtmlAnchor);
            if (ashowclass != null)
            {
                ashowclass.Attributes.Add("onclick", "showclass(" + DocumentOrder + ",'" + ddlDropClass.ClientID + "','" + ddlDrpMove.ClientID + "'," + DividerID + ");");
            }
            //showMove
            HtmlAnchor ashowMove = (e.Row.FindControl("showMove") as HtmlAnchor);
            if (ashowMove != null)
            {
                ashowMove.Attributes.Add("onclick", "showMove(" + DocumentOrder + ",'" + ddlDropOrder.ClientID + "','" + ddlDrpMove.ClientID + "'," + DividerID + ");");
            }
            //showOrder

            HtmlAnchor ashowOrder = (e.Row.FindControl("showOrder") as HtmlAnchor);
            if (ashowOrder != null)
            {
                ashowOrder.Attributes.Add("onclick", "showOrder(" + DocumentOrder + ",'" + ddlDropOrder.ClientID + "','" + ddlDrpMove.ClientID + "'," + DividerID + ");");
            }
            //document Rename

            TextBox txtDocumentName = (e.Row.FindControl("txtDocumentName") as TextBox);

            LinkButton lnkShare = (e.Row.FindControl("lnkShare") as LinkButton);
            if (lnkShare != null)
            {
                lnkShare.Attributes.Add("onclick", "return OnShowShare(" + DocumentID + ", '" + oldPath + "')");

            }

            LinkButton lbkUpdate1 = (e.Row.FindControl("lbkUpdate1") as LinkButton);
            if (lbkUpdate1 != null)
            {
                lbkUpdate1.Attributes.Add("onclick", "OnSaveRename(" + DocumentID + " , " + DividerID + ", '" + oldPath + "','" + txtDocumentName.ClientID + "', " + DocumentOrder + ",'" + ClassName + "');");
            }

            //LinkButton lnkCancel1 = (e.Row.FindControl("lnkCancel1") as LinkButton);
            //if (lbkUpdate1 != null)
            //{
            //    lbkUpdate1.Attributes.Add("onclick", "cancleFileName(" + DocumentID + " , '" + ClassName + "', '" + oldPath + "','" + txtDocumentName.ClientID + "', " + DocumentOrder + ",'" + ClassName + "');");
            //}
        }
        bool check = General_Class.CheckFolderLogExists(Convert.ToInt32(FileFolderID));
        if (!check)
        {
            LinkButton libFolderLog = this.UpdatePanel1.FindControl("lnkFolderLogs") as LinkButton;
            //libFolderLog.Visible = false;
        }
        _logger.Debug("GridView0_RowDataBound" + stopWatch.Elapsed.ToString());
    }

    public string GetFileName(string fileName)
    {
        string encodeName = HttpUtility.UrlPathEncode(fileName);
        return encodeName.Replace("%", "$").Replace("?", "$c3$b1");
    }
    //protected void SaveMoveOrder_Click(object sender, EventArgs e)
    //{
    //    BindDividers();
    //    BindGrid();

    //}

    public string PrepareDocs(int docId, int docOrder)
    {
        //this.DividerID_FFB = this.DividerSource.Rows[ActiveTabIndex]["DividerID"].ToString();
        return "{\"DocId\":" + Convert.ToString(docId) + ",\"DocOrder\":" + Convert.ToString(docOrder) + "}";
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        var panel = fileTabs.FindControl("Panel" + this.ActiveTabIndex);
        var grid = new Control();
        if (panel != null)
        {
            grid = panel.FindControl("GridView" + this.ActiveTabIndex);
        }
        if (grid != null)
            grid.Visible = true;
        if (drpSearch.SelectedValue == "5")
        {
            GetOcrDocuments();
            if (grid != null)
                grid.Visible = false;
            gridViewOcr.Visible = true;
            btnConvertOcr.Visible = false;
        }
        else
        {
            gridViewOcr.Visible = false;
            btnConvertOcr.Visible = true;
            GetFilteredDocuments();
        }
        BindDividers();
        ScriptManager.RegisterStartupScript(this, GetType(), "myFunction", "SetPageIndex(" + this.ActiveTabIndex + ");", true);
    }

    private DateTime ChangeDateFormat(string dateTimeVal)
    {
        string[] splitedDate = dateTimeVal.Split('-');
        return new DateTime(int.Parse(splitedDate[2]), int.Parse(splitedDate[0]), int.Parse(splitedDate[1]));
    }

    [WebMethod]
    public static string insertFolderLog(string Question, string FolderLogName, string FolderId)
    {
        string FolderLogId = string.Empty;
        string cntFolderLog = string.Empty;
        DataTable dtChkQuestion = new DataTable();
        dtChkQuestion.Columns.Add("FolderId", typeof(string));
        dtChkQuestion.Columns.Add("FolderLogId", typeof(int));
        dtChkQuestion.Columns.Add("Question", typeof(string));
        dtChkQuestion.Columns.Add("createdon", typeof(DateTime));

        var createdOn = DateTime.Now;
        var cnt = General_Class.InsertFolderlog(FolderId, FolderLogName);
        if (cnt == "0")
        {
            //lblFailFolderLog.text = "FolderLogName Exist.Please enter another name.";
        }
        else
        {
            FolderLogId = Convert.ToString(cnt);
            var list = JsonConvert.DeserializeObject<List<question>>(Question);
            foreach (var item in list)
            {
                if (!string.IsNullOrEmpty(item.que))
                {
                    dtChkQuestion.Rows.Add(new object[] { FolderId, FolderLogId, item.que, createdOn });
                }
            }
            //Insert Question
            cntFolderLog = General_Class.InsertQuestion(dtChkQuestion, FolderId);
            //Insert Log
            EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(FolderId), 0, 0, 0, Convert.ToInt32(FolderLogId), Enum_Tatva.DocumentsAndFoldersLogs.Create.GetHashCode(), 0, 0);
        }
        return FolderLogId;
    }




    [WebMethod]
    public static string GetFolderLogDetails(string FolderId)
    {
        //string strFolderLogID = General_Class.GetFolderlog(FolderId);
        //return strFolderLogID;

        DataSet dsFolderLog = General_Class.GetFolderlog(FolderId);

        if (dsFolderLog != null)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            foreach (DataTable dt in dsFolderLog.Tables)
            {
                object[] arr = new object[dt.Rows.Count + 1];
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    arr[i] = dt.Rows[i].ItemArray;
                }
                dict.Add(dt.TableName, arr);
            }
            JavaScriptSerializer json = new JavaScriptSerializer();
            return json.Serialize(dict);
        }
        else
            return null;
    }

    [WebMethod]
    public static string UpdateFolderLogDetails(string FolderDetails, string folderlogId, string folderId, string folderLogName)
    {
        DataTable dtQuestion = new DataTable();
        dtQuestion.Columns.Add("ID", typeof(int));
        dtQuestion.Columns.Add("Question", typeof(string));

        var list = JsonConvert.DeserializeObject<List<questionList>>(FolderDetails);

        foreach (var item in list)
        {
            if (!string.IsNullOrEmpty(item.que))
            {
                dtQuestion.Rows.Add(new object[] { item.Id, item.que });
            }
        }

        string strCount = General_Class.UpdateFolderLog(dtQuestion, DateTime.Now, folderId, Convert.ToInt32(folderlogId), folderLogName.Trim());
        EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(folderId), 0, 0, 0, Convert.ToInt32(folderlogId), Enum_Tatva.DocumentsAndFoldersLogs.Edit.GetHashCode(), 0, 0);
        return strCount;
    }
    [WebMethod]
    public static List<string> GetFolderLogDetailById(string folderId, string folderLogId)
    {
        Dictionary<string, string> dicQuestion = new Dictionary<string, string>();
        DataSet dsFolderLog = new DataSet();
        DataTable dtFolderLog = new DataTable();
        DataTable dtQuestion = new DataTable();
        string folderLogName = string.Empty;
        string question = null;
        string strReturn = string.Empty;
        string questionId = string.Empty;

        dsFolderLog = General_Class.GetFolderLogDetails(Convert.ToInt32(folderLogId), Convert.ToInt32(folderId));
        dtFolderLog = dsFolderLog.Tables[0];
        dtQuestion = dsFolderLog.Tables[1];

        if (dtFolderLog.Rows.Count > 0)
        {
            folderLogName = dtFolderLog.Rows[0]["LogName"].ToString();
        }
        if (dtQuestion.Rows.Count > 0)
        {
            for (int j = 0; j < dtQuestion.Rows.Count; j++)
            {
                dicQuestion.Add(Convert.ToString(dtQuestion.Rows[j]["ID"]), Convert.ToString(dtQuestion.Rows[j]["Question"]));



                //if (string.IsNullOrEmpty(questionId))
                //    questionId = Convert.ToString(dtQuestion.Rows[j]["ID"]);
                //else
                //    questionId = questionId + "," + Convert.ToString(dtQuestion.Rows[j]["ID"]);

                //if (string.IsNullOrEmpty(question))
                //    question = Convert.ToString(dtQuestion.Rows[j]["Question"]);
                //else
                //    question = question + "," + Convert.ToString(dtQuestion.Rows[j]["Question"]);
            }
        }

        string json = JsonConvert.SerializeObject(dicQuestion, Newtonsoft.Json.Formatting.Indented);

        //Dictionary<string, string> dict = new Dictionary<string, string>();

        List<string> lstData = new List<string>();
        lstData.Add(folderLogName);
        lstData.Add(json);

        //strReturn = "FolderLogName=" + folderLogName + "&" + "QuestionCount=" + Convert.ToString(dtQuestion.Rows.Count) + "&" + "QuestionId=" + questionId + "&" + "Question=" + question;
        //strReturn = folderLogName + "&@$!" + json;
        return lstData;
    }

    [WebMethod]
    public static List<FolderDetails> GetFolderDetailsById(string officeId)
    {
        DataTable dtFolderDetails = new DataTable();
        List<FolderDetails> folderDetails = new List<FolderDetails>();
        dtFolderDetails = General_Class.GetFolderIdByOfficeID(officeId);
        if (dtFolderDetails != null && dtFolderDetails.Rows.Count > 0)
        {
            folderDetails = (from DataRow dr in dtFolderDetails.Rows
                             select new FolderDetails()
                             {
                                 folderName = dr["FolderName"].ToString(),
                                 folderId = dr["folderId"].ToString()
                             }).ToList();
        }
        _logger.Debug("GetFolderDetailsById" + stopWatch.Elapsed.ToString());
        return folderDetails;
    }


    [WebMethod]
    public static List<FolderLogDetails> GetFolderLogDetailsById(string folderId)
    {
        DataSet dsFolderLogDetails = new DataSet();
        DataTable dtFolderLogDetails = new DataTable();
        DataTable dtQuestionDetails = new DataTable();
        List<FolderLogDetails> folderLogDetails = new List<FolderLogDetails>();
        dsFolderLogDetails = General_Class.GetFolderLogDetailsByFolderId(folderId);
        if (dsFolderLogDetails != null && dsFolderLogDetails.Tables.Count > 0 && dsFolderLogDetails.Tables[0].Rows.Count > 0 && dsFolderLogDetails.Tables[0] != null)
        {
            dtFolderLogDetails = dsFolderLogDetails.Tables[0];
            if (dtFolderLogDetails != null && dtFolderLogDetails.Rows.Count > 0)
            {
                folderLogDetails = (from DataRow dr in dtFolderLogDetails.Rows
                                    select new FolderLogDetails()
                                    {
                                        folderLogName = dr["LogName"].ToString(),
                                        folderLogId = dr["FolderLogID"].ToString()
                                    }).ToList();
            }
        }
        return folderLogDetails;
    }

    [WebMethod]
    public static List<question> GetQuestionByLogID(string folderLogId)
    {
        DataTable dtQuestionDetails = new DataTable();
        List<question> questionDetails = new List<question>();
        dtQuestionDetails = General_Class.GetQuestionByFolderLogID(Convert.ToInt32(folderLogId));
        if (dtQuestionDetails != null && dtQuestionDetails.Rows.Count > 0)
        {
            questionDetails = (from DataRow dr in dtQuestionDetails.Rows
                               select new question()
                               {
                                   que = dr["Question"].ToString()
                               }).ToList();
        }
        return questionDetails;
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        if (FileUpload1.HasFile)
        {
            try
            {
                if (FileUpload1.PostedFile.ContentType == "application/pdf")
                {
                    //if (FileUpload1.PostedFile.ContentLength < 102400)
                    //{
                    //string filename = String.Concat(FileUpload1.FileName);

                    //filename = Common_Tatva.GetRenameFileName(filename);
                    string filename = Path.GetFileName(FileUpload1.FileName);
                    var path = HttpContext.Current.Server.MapPath("~/" + "Volume/" + hdnFolderId.Value.Trim());
                    if (!Directory.Exists(path))
                    {
                        path = HttpContext.Current.Server.MapPath("~/" + "Volume2/" + hdnFolderId.Value.Trim() + "/LogForm/");
                    }
                    else
                        path += "/LogForm/";

                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                    else
                    {
                        if (Directory.GetFiles(path).Length >= 5)
                        {
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('You can not upload more than five Log Forms!');", true);
                            return;
                        }
                    }
                    //filename = String.Concat(path, Path.DirectorySeparatorChar, Common_Tatva.ChangeExtension(filename));

                    filename = filename.Replace("'", "");
                    filename = Path.GetFileName(Common_Tatva.GetRenameFileName(path + filename));
                    FileUpload1.SaveAs(path + filename);
                    string logFormId = General_Class.DocumentsInsertForLogForm(filename, Sessions.SwitchedSessionId, DateTime.Now, int.Parse(hdnFolderId.Value));
                    //string logFormId = General_Class.DocumentsInsertForLogForm(filename, Sessions.UserId, DateTime.Now, int.Parse(hdnFolderId.Value));
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('LogForm has been uploaded!');", true);
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(int.Parse(hdnFolderId.Value), 0, 0, int.Parse(logFormId), 0, Enum_Tatva.DocumentsAndFoldersLogs.Create.GetHashCode(), 0, 0);
                    SetLogFormView(hdnFolderId.Value);
                    //   StatusLabel.Text = "Upload status: File uploaded!";
                    //}
                    //  else
                    //  StatusLabel.Text = "Upload status: The file has to be less than 100 kb!";
                }
                else
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Only pdf type is allowed!');", true);
                // else
                //  StatusLabel.Text = "Upload status: Only JPEG files are accepted!";
            }
            catch (Exception ex)
            {
                //StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Failed to upload LogForm. Please try again later.');", true);
            }
        }
    }

    [WebMethod]
    public static bool SaveIndexValues(string folderId, string dividerId, string documentId, string indexData, string deleteIndexData)
    {
        try
        {
            IndexKeyValue[] indexKeyValue = JsonConvert.DeserializeObject<List<IndexKeyValue>>(indexData).ToArray();
            DataTable dtIndex = new DataTable();
            dtIndex.Columns.Add("IndexId", typeof(int));
            dtIndex.Columns.Add("FolderId", typeof(int));
            dtIndex.Columns.Add("DividerId", typeof(int));
            dtIndex.Columns.Add("DocumentId", typeof(int));
            dtIndex.Columns.Add("IndexKey", typeof(string));
            dtIndex.Columns.Add("IndexValue", typeof(string));
            foreach (IndexKeyValue item in indexKeyValue)
            {
                dtIndex.Rows.Add(new object[] { item.IndexId, folderId, dividerId, documentId, item.IndexKey, item.IndexValue });
            }
            General_Class.InsertIndexValues(dtIndex, deleteIndexData);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    [WebMethod]
    public static string GetIndexValues(string documentId)
    {
        try
        {
            Dictionary<string, IndexKeyValue> dicIndexValues = new Dictionary<string, IndexKeyValue>();
            DataTable dtIndexValues = General_Class.GetIndexValues(Int32.Parse(documentId));
            if (dtIndexValues.Rows.Count > 0)
            {
                for (int j = 0; j < dtIndexValues.Rows.Count; j++)
                {
                    IndexKeyValue indexKeyValue = new IndexKeyValue();
                    indexKeyValue.IndexId = Int32.Parse(dtIndexValues.Rows[j]["IndexId"].ToString());
                    indexKeyValue.IndexValue = Convert.ToString(dtIndexValues.Rows[j]["IndexValue"]);
                    indexKeyValue.IndexKey = Convert.ToString(dtIndexValues.Rows[j]["IndexKey"]);
                    dicIndexValues.Add(Convert.ToString(dtIndexValues.Rows[j]["IndexId"]), indexKeyValue);
                }
            }

            return JsonConvert.SerializeObject(dicIndexValues, Newtonsoft.Json.Formatting.Indented);
        }
        catch (Exception ex)
        {
            return "";
        }
    }
    protected void btnSelectAddToTab_Click(object sender, EventArgs e)
    {
        try
        {
            int folderId = Convert.ToInt32(hdnSelectedFolderID.Value);
            int dividerId = Convert.ToInt32(hdnSelectedDividerID.Value);
            string errorMsg = string.Empty;
            List<string> docIDs = DocumentIDs.Value.Split(',').ToList();
            if (string.IsNullOrEmpty(DocumentIDs.Value))
            {
                docIDs.RemoveAt(0);
                docIDs.Add(Convert.ToString(hdnSelectedDocumentID.Value));
            }
            for (int i = 0; i < docIDs.Count; i++)
            {
                try
                {
                    DataTable dtFiles = General_Class.GetDocumentFromDocumentID(Convert.ToInt32(docIDs[i]));
                    dtFiles.AsEnumerable().ToList().ForEach(d =>
                    {
                        try
                        {
                            int documentId = d.Field<int>("DocumentID");
                            int newDocumentId = Common_Tatva.AddDocumentToTab(documentId, folderId, dividerId, d);
                            if (newDocumentId > 0)
                            {
                                General_Class.AuditLogByDocId(documentId, Sessions.SwitchedSessionId, Enum_Tatva.Action.Move.GetHashCode());
                                General_Class.AuditLogByDocId(newDocumentId, Sessions.SwitchedSessionId, Enum_Tatva.Action.Create.GetHashCode());
                                General_Class.UpdateDocument(documentId, Sessions.UserId);
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(d.Field<int>("FileFolderID"), d.Field<int>("DividerID"), documentId, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode(), 0, 0);
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(folderId, dividerId, newDocumentId, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Upload.GetHashCode(), 0, 0);
                                Thread.Sleep(1000);
                                GetOcrDocuments();
                                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "File has been moved successfully." + "\");", true);
                            }
                            else
                                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to move file. Please try again." + "\");", true);
                        }
                        catch (Exception ex)
                        {
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to move file. Please try again." + "\");", true);
                        }
                    });
                }
                catch (Exception ex)
                {
                    Common_Tatva.WriteErrorLog(Convert.ToString(folderId), Convert.ToString(dividerId), string.Empty, ex);
                }
            }
            GetData(this.EffID_FFB, string.IsNullOrEmpty(this.DividerID_FFB) ? 0 : Convert.ToInt32(this.DividerID_FFB));
            GetDividerData(this.EffID_FFB);
            BindDividers();
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to move file. Please try again." + "\");", true);
        }
    }

    public void ExtractAndUploadPDF(string pageNumbers, string fileName, string newFName, string fileID, ref string newEncodeName, ref string outputPdfPath, string folderID, string dividerID)
    {
        PdfReader reader = null;
        PdfCopy pdfCopyProvider = null;
        PdfCopy pdfRemaining = null;
        PdfImportedPage importedPage = null;
        PdfImportedPage importedPage1 = null;
        iTextSharp.text.Document sourceDocument = null;

        try
        {
            string[] extractThesePages = pageNumbers.Split(',');

            string sourcePdfPath;
            string encodeName = HttpUtility.UrlPathEncode(fileName);
            encodeName = encodeName.Replace("%", "$");

            string uid = Sessions.SwitchedSessionId;
            //string uid = Membership.GetUser().ProviderUserKey.ToString();
            string path = HttpContext.Current.Server.MapPath(string.Format("~/"));

            sourcePdfPath = path + hdnPath.Value;

            string newpath = HttpContext.Current.Server.MapPath(string.Format("~/{0}", Common_Tatva.NewSplitEdFilesDownload));
            outputPdfPath = newpath + Path.DirectorySeparatorChar + folderID + Path.DirectorySeparatorChar + dividerID;

            if (!Directory.Exists(outputPdfPath))
            {
                Directory.CreateDirectory(outputPdfPath);
            }

            if (newFName.IndexOf(".pdf") < 0)
                newFName = newFName + ".pdf";

            newFName = newFName.Replace("#", "");
            newFName = newFName.Replace("&", "");

            newEncodeName = HttpUtility.UrlPathEncode(newFName);
            newEncodeName = newEncodeName.Replace("%", "$");
            outputPdfPath = outputPdfPath + Path.DirectorySeparatorChar + newEncodeName;


            string remainingPath = newpath + Path.DirectorySeparatorChar + folderID + Path.DirectorySeparatorChar + dividerID + Path.DirectorySeparatorChar + encodeName;

            reader = new PdfReader(sourcePdfPath);

            // For simplicity, I am assuming all the pages share the same size
            // and rotation as the first page:
            sourceDocument = new iTextSharp.text.Document(reader.GetPageSizeWithRotation(int.Parse(extractThesePages[0])));

            // Initialize an instance of the PdfCopyClass with the source 
            // document and an output file stream:
            pdfCopyProvider = new PdfCopy(sourceDocument, new System.IO.FileStream(outputPdfPath, System.IO.FileMode.Create));
            if (reader.NumberOfPages > extractThesePages.Length)
                pdfRemaining = new PdfCopy(sourceDocument, new System.IO.FileStream(remainingPath, System.IO.FileMode.Create));
            sourceDocument.Open();

            // Walk the array and add the page copies to the output file:
            foreach (string pageNumber in extractThesePages)
            {
                importedPage = pdfCopyProvider.GetImportedPage(reader, int.Parse(pageNumber));
                pdfCopyProvider.AddPage(importedPage);
            }
            for (int pageNumber = 1; pageNumber <= reader.NumberOfPages; pageNumber++)
            {
                if (extractThesePages.Contains(pageNumber.ToString()))
                    continue;
                importedPage1 = pdfRemaining.GetImportedPage(reader, pageNumber);
                pdfRemaining.AddPage(importedPage1);
            }
            if (sourceDocument != null && sourceDocument.PageNumber > 0)
                sourceDocument.Close();
            if (reader != null)
                reader.Close();
            try
            {
                if (pdfCopyProvider != null)
                {
                    pdfCopyProvider.PageEmpty = false;
                    pdfCopyProvider.Close();
                }
                if (pdfRemaining != null)
                {
                    pdfRemaining.PageEmpty = false;
                    pdfRemaining.Close();
                }
            }
            catch (Exception ex)
            {
            }
            if (File.Exists(remainingPath))
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                //File.Copy(remainingPath, sourcePdfPath, true);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                File.Delete(remainingPath);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                //FileInfo fileInfo = new FileInfo(sourcePdfPath);
                //decimal fileSize = Math.Round((decimal)(fileInfo.Length / 1024) / 1024, 2);
                //General_Class.UpdateFileSplitSize(int.Parse(fileID), (long)fileSize, Sessions.UserId);
            }
            //else
            //{
            //    General_Class.UpdateFileSplit(fileName, int.Parse(fileID), Enum_Tatva.IsDelete.No.GetHashCode(), 0, 1, Sessions.UserId);
            //}
        }
        catch (Exception ex)
        {
            if (sourceDocument != null)
                sourceDocument.Close();
            if (reader != null)
                reader.Close();
            pdfCopyProvider.Close();
            pdfRemaining.Close();
        }

    }
    protected void btnSaveSplitPdf_Click(object sender, EventArgs e)
    {
        try
        {
            string pageNumbers = hdnPageNumbers.Value;

            if (!string.IsNullOrEmpty(pageNumbers))
            {
                var outValue = ExtractPages(pageNumbers, hdnFileName.Value, txtNewPdfName.Text.Trim(), hdnFileId.Value, hdnSelectedFolderID.Value, hdnSelectedDividerID.Value, hdnSelectedFolderText.Value);
                if (outValue != null && outValue.Item3)
                {
                    string message = "The selected pages have been added to tab.  Would you like to split additional pages?  If so, please select OK, otherewise, select Cancel";
                    ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "alert", "<script language=javascript>ShowConfirm('" + message + "');</script>", false);
                }
            }

        }
        catch (Exception ex)
        { }
        finally
        {
            //Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
    }

    [WebMethod]
    public static string SplitPdf(string FileName, Int32 Id, string PageNumber, string NewFileName)
    {
        return PageNumber;
    }
    public Tuple<decimal, string, bool, string> ExtractPages(string pageNumbers, string fileName, string newFName, string fileID = null, string folderID = null, string dividerID = null, string folderName = null, bool isSplitShare = false)
    {
        try
        {
            string newEncodeName = string.Empty;
            string outputPdfPath = string.Empty;
            string pathName = string.Empty;
            ExtractAndUploadPDF(pageNumbers, fileName, newFName, fileID, ref newEncodeName, ref outputPdfPath, folderID, dividerID);
            string edFileShareId = Guid.NewGuid().ToString();

            if (isSplitShare)
            {
                pathName = HttpContext.Current.Server.MapPath(string.Format("~/{0}", EdFileShare) + Path.DirectorySeparatorChar + edFileShareId);
                newEncodeName = pathName + Path.DirectorySeparatorChar + newEncodeName;
            }
            else
            {
                pathName = HttpContext.Current.Server.MapPath(string.Format("~/{0}", CurrentVolume) + Path.DirectorySeparatorChar + folderID + Path.DirectorySeparatorChar
                    + dividerID);
                newEncodeName = Common_Tatva.GetRenameFileName(pathName + Path.DirectorySeparatorChar + newEncodeName);
            }


            if (!Directory.Exists(pathName))
            {
                Directory.CreateDirectory(pathName);
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
            File.Copy(outputPdfPath, newEncodeName);

            FileInfo fileInfo = new FileInfo(newEncodeName);
            decimal fileSize = Math.Round((decimal)(fileInfo.Length / 1024) / 1024, 2);

            string temp = newEncodeName.Substring(newEncodeName.LastIndexOf("\\") + 1);
            newFileName = HttpUtility.UrlDecode(temp.Replace('$', '%').ToString());

            if (!isSplitShare)
                ProcessFileAndSaveInDB((float)fileSize, pathName, int.Parse(dividerID), int.Parse(folderID), true);

            GC.Collect();
            GC.WaitForPendingFinalizers();
            File.SetAttributes(outputPdfPath, FileAttributes.Normal);
            File.Delete(outputPdfPath);
            return Tuple.Create(fileSize, edFileShareId, true, newEncodeName);
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(folderID, dividerID, fileName, ex);
            return null;
        }
    }

    private void ProcessFileAndSaveInDB(float size, string path, int dividerIdForFile = 0, int folderID = 0, bool isMovedToFolder = false)
    {
        try
        {
            string fulFileName = string.Empty;
            WorkItem item = new WorkItem();
            item.FolderID = folderID;
            item.DividerID = dividerIdForFile;
            string basePath = this.MapPath(this.splitPdfFolderName);

            item.Path = path;

            item.FileName = Path.GetFileNameWithoutExtension(newFileName);

            item.FullFileName = Path.GetFileName(newFileName);

            item.UID = Sessions.SwitchedSessionId;
            //item.UID = Membership.GetUser().ProviderUserKey.ToString();
            item.contentType = Common_Tatva.getFileExtention(Path.GetExtension(newFileName));

            int order = FlashViewer.GetDocumentOrder(Convert.ToInt32(dividerIdForFile));


            item.MachinePath = path;

            if (!Directory.Exists(item.MachinePath))
            {
                Directory.CreateDirectory(item.MachinePath);
            }

            string encodeName = HttpUtility.UrlPathEncode(item.FullFileName);
            encodeName = encodeName.Replace("%", "$");
            string source = string.Concat(path, Path.DirectorySeparatorChar, encodeName);
            var pageCount = 1;
            if (item.contentType == StreamType.JPG)
            {
                Jpeg2Engine jpeg2Engine = new Jpeg2Engine(item);
                jpeg2Engine.generatePDFfromImage(source, source.Split('.')[0] + ".pdf");
            }
            else if (item.contentType == StreamType.PDF)
            {
                PdfReader pdfReader = new PdfReader(source);
                pageCount = pdfReader.NumberOfPages;
            }
            else if (item.contentType == StreamType.OTHERS)
            {
                fulFileName = item.FullFileName;
            }
            if (isMovedToFolder)
            {
                string relativePath = CurrentVolume + Path.DirectorySeparatorChar + folderID + Path.DirectorySeparatorChar + dividerIdForFile;
                this.DocumentID = General_Class.DocumentsInsert(item.FileName, item.FolderID, item.UID, relativePath, item.DividerID, pageCount, (float)size, item.FileName,
                    (int)item.contentType, order + 1, Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()), fulFileName);
                General_Class.AuditLogByDocId(Convert.ToInt32(this.DocumentID), Sessions.SwitchedRackspaceId, Enum_Tatva.Action.Create.GetHashCode());
                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(item.FolderID), Convert.ToInt32(item.DividerID), int.Parse(this.DocumentID), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.FileSplitUpload.GetHashCode(), 0, 0);
            }
            this.DocumentName = item.FileName;
        }
        catch (Exception ex)
        {
        }
    }

    public string splitPdfFolderName
    {
        get
        {
            return string.Format("~/{0}", splitPdfTepmPath);
        }

    }

    protected void hdnSplitBtn_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(hdnIsRedaction.Value) && Convert.ToBoolean(hdnIsRedaction.Value))
            {
                List<RedactOption> redactOption = JsonConvert.DeserializeObject<List<RedactOption>>(hdnCords.Value);
                string edFileShareId = Guid.NewGuid().ToString();

                var outValue = RedactPages(redactOption, hdnPath.Value, hdnNewPdfName.Value.Trim(), edFileShareId);
                if (outValue != null && outValue.Item3)
                {
                    var pdfName = Path.GetFileName(outValue.Item4);
                    pdfName = HttpUtility.UrlDecode(pdfName.Replace('$', '%').ToString());
                    Decimal size = Convert.ToDecimal(outValue.Item1);
                    bool isShared = eFileFolderJSONWS.ShareMailForEdFileShare(hdnToEmail.Value, hdnFromEmail.Value, Convert.ToInt32(hdnFileId.Value), hdnMailTitle.Value, hdnMailContent.Value, hdnToName.Value, pdfName, size, new Guid(edFileShareId));
                    if (isShared)
                    {
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                           "alert(\"" + "Document has been redacted and shared successfully." + "\");SetPageIndex(" + this.ActiveTabIndex + ");", true);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                        "alert(\"" + "Failed to share document at this time. Please try again later !!" + "\");SetPageIndex(" + this.ActiveTabIndex + ");", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                    "alert(\"" + "Failed to share document at this time. Please try again later !!" + "\");SetPageIndex(" + this.ActiveTabIndex + ");", true);
                }
                hdnIsRedaction.Value = "";
                hdnCords.Value = "";
            }
            else
            {
                string pageNumbers = hdnPageNumbers.Value;

                if (!string.IsNullOrEmpty(pageNumbers))
                {
                    var outValue = ExtractPages(pageNumbers, hdnFileName.Value, hdnNewPdfName.Value.Trim(), hdnFileId.Value, hdnSelectedFolderID.Value, hdnSelectedDividerID.Value, hdnSelectedFolderText.Value, true);
                    if (outValue != null && outValue.Item3)
                    {
                        var pdfName = Path.GetFileName(outValue.Item4);
                        pdfName = HttpUtility.UrlDecode(pdfName.Replace('$', '%').ToString());
                        Decimal size = Convert.ToDecimal(outValue.Item1);
                        Guid edFileShareId = new Guid(outValue.Item2);
                        bool isShared = eFileFolderJSONWS.ShareMailForEdFileShare(hdnToEmail.Value, hdnFromEmail.Value, Convert.ToInt32(hdnFileId.Value), hdnMailTitle.Value, hdnMailContent.Value, hdnToName.Value, pdfName, size, edFileShareId);
                        if (isShared)
                        {
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                               "alert(\"" + "Document has been splitted and shared successfully." + "\");", true);
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                            "alert(\"" + "Failed to share document at this time. Please try again later !!" + "\");", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                        "alert(\"" + "Failed to share document at this time. Please try again later !!" + "\");", true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, hdnFileId.Value, ex);
        }
    }

    public Tuple<decimal, string, bool, string> RedactPages(List<RedactOption> redactOption, string fileName, string newFileName, string edFileShareId)
    {
        try
        {
            string pathName = HttpContext.Current.Server.MapPath("~/" + hdnPath.Value);
            if (newFileName.IndexOf(".pdf") < 0)
                newFileName = newFileName + ".pdf";
            string newPathName = HttpContext.Current.Server.MapPath(string.Format("~/{0}", EdFileShare) + Path.DirectorySeparatorChar + edFileShareId);
            if (!Directory.Exists(newPathName))
                Directory.CreateDirectory(newPathName);
            newPathName = newPathName + Path.DirectorySeparatorChar + newFileName;
            PdfReader reader = new PdfReader(pathName);
            PdfStamper stamper = new PdfStamper(reader, new FileStream(newPathName, FileMode.Create));
            try
            {
                for (int i = 0; i < redactOption.Count; i++)
                {
                    int pageIndex = redactOption[i].pageNumber;
                    PdfContentByte content = stamper.GetOverContent(pageIndex);
                    content.SetColorFill(BaseColor.BLACK);
                    float scale = redactOption[i].prevScale;
                    float x = Math.Abs((redactOption[i].left - (redactOption[i].prevDiff == null ? 0 : (float)redactOption[i].prevDiff)) / scale);
                    float y = Math.Abs((redactOption[i].top) / scale);
                    float width = (float)redactOption[i].width / scale;
                    float height = (float)redactOption[i].height / scale;
                    float pageWidth = redactOption[i].pageWidth / scale;
                    float pageHeight = redactOption[i].pageHeight / scale;
                    if (pageIndex > 1)
                        y = Math.Abs(((pageIndex - 1) * pageHeight) + (((pageIndex - 1) * 12) / scale) - y);
                    else
                        y = Math.Abs((10 / scale) - y);
                    Rectangle rect = reader.GetPageSize(redactOption[i].pageNumber);
                    float ratio = rect.Height / pageHeight;
                    x = x * ratio;
                    y = y * ratio;
                    width = width * ratio;
                    height = height * ratio;
                    y = Math.Abs(rect.Height - y);
                    y = y - height;
                    content.Rectangle(x, y, width, height);
                    content.Fill();
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
            stamper.Close();
            reader.Close();
            FileInfo fileInfo = new FileInfo(newPathName);
            decimal fileSize = Math.Round((decimal)(fileInfo.Length / 1024) / 1024, 2);

            string temp = newPathName.Substring(newPathName.LastIndexOf("\\") + 1);
            newFileName = HttpUtility.UrlDecode(temp.Replace('$', '%').ToString());
            return Tuple.Create(fileSize, edFileShareId, true, newPathName);
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
            return null;
        }
    }

    /// <summary>
    /// Handles the Click event of the addCoverSheet control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void addCoverSheet_Click(object sender, EventArgs e)
    {
        //BarcodeLib.Barcode b = new BarcodeLib.Barcode();
        //b.IncludeLabel = true;
        //b.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
        //b.Encode(TYPE.CODE93, "1234", Color.Black, Color.White, 400, 40);

        //Bytescout.BarCodeReader.Reader rdr = new Reader();
        //var imageData = b.GetImageData(SaveTypes.JPG);
        //Bitmap bmp;
        //using (var ms = new MemoryStream(imageData))
        //{
        //    bmp = new Bitmap(ms);
        //}
        //rdr.BarcodeTypesToFind.Code93 = true;
        //FoundBarcode[] data = rdr.ReadFrom(bmp);
        Response.Redirect("CoverSheetDownloader.aspx?data=" + QueryString.QueryStringEncode("folderId=" + hdnFolderId.Value + "&folderName=" + hdnFolderName.Value + "&fileNumber=" + hdnFileNumber.Value));
    }

    /// <summary>
    /// Handles the Click event of the btnDeleteOkay control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnDeleteOkay_Click(object sender, EventArgs e)
    {
        string password = textPassword.Text.Trim();

        if (UserManagement.GetPassword(User.Identity.Name) != PasswordGenerator.GetMD5(password) && PasswordGenerator.GetMD5(password) != (ConfigurationManager.AppSettings["EncryptedUserPwd"]))
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);
            return;
        }
        else
        {
            try
            {
                if (!Directory.Exists(Server.MapPath(ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles")))
                    Directory.CreateDirectory(Server.MapPath(ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles"));

                Shinetech.DAL.Account user = new Shinetech.DAL.Account(Sessions.SwitchedSessionId);

                if (!Common_Tatva.IsUserSwitched() && User.IsInRole("Users") && !user.HasPrivilegeDelete.Value)
                {
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Please contact your office admin, your credentials do not permit file deletion.');", true);
                }
                else
                {
                    #region Start Delete file

                    GridView gridv = this.fileTabs.ActiveTab.FindControl("GridView" + fileTabs.ActiveTabIndex) as GridView;
                    int documentId = Convert.ToInt32(hdnDeleteId.Value);
                    string strPDFName = string.Empty;
                    string strFileName = string.Empty;
                    if (documentId > 0)
                    {
                        Shinetech.DAL.Document doc = new Shinetech.DAL.Document(documentId);

                        string ext = ".pdf";
                        StreamType extValue = StreamType.PDF;
                        try
                        {
                            extValue = (StreamType)doc.FileExtention.Value;
                        }
                        catch (Exception ex)
                        {
                            _logger.Error(ex);
                        }

                        if (extValue == StreamType.OTHERS)
                        {
                            strPDFName = strFileName = doc.FullFileName.Value;
                        }
                        else
                        {
                            strPDFName = doc.Name.Value + ext;
                            strFileName = doc.Name.Value + this.GetFileExtention(extValue);
                        }
                        //string strPDFName = doc.Name.Value + ext;
                        //string strFileName = doc.Name.Value + this.GetFileExtention(extValue);

                        strPDFName = GetFileName(strPDFName);
                        string strPDFPath = "~/" + doc.PathName.Value + "/" + strPDFName;
                        string destPDFPath = ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles/" + doc.PathName.Value + "/" + strPDFName;
                        if (File.Exists(Server.MapPath(strPDFPath)))
                        {
                            try
                            {
                                if (!Directory.Exists(Server.MapPath(destPDFPath.Substring(0, destPDFPath.LastIndexOf('/')))))
                                    Directory.CreateDirectory(Server.MapPath(destPDFPath.Substring(0, destPDFPath.LastIndexOf('/'))));
                                File.Move(Server.MapPath(strPDFPath), Server.MapPath(destPDFPath));
                            }
                            catch { }

                        }

                        DataTable dtPage = FileFolderManagement.GetDocumentByDocumentID(documentId.ToString());
                        for (int i = 0; i < dtPage.Rows.Count; i++)
                        {
                            string strPageID = dtPage.Rows[i]["PageID"].ToString();

                            //delete quick note
                            FileFolderManagement.DelQucikNoteByPageID(strPageID);

                            string strImagePath;
                            string strImagePath1;
                            string strImagePath2;
                            string strImagePath3;

                            #region delete image in local
                            if (dtPage.Rows[i]["ImagePath"].ToString() != "")
                            {
                                strImagePath = "~/" + dtPage.Rows[i]["ImagePath"].ToString();
                                if (File.Exists(Server.MapPath(strImagePath)))
                                {
                                    string destPath = ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles/" + strImagePath.Substring(2);
                                    if (!Directory.Exists(Server.MapPath(destPath.Substring(0, destPath.LastIndexOf("\\")))))
                                        Directory.CreateDirectory(Server.MapPath(destPath.Substring(0, destPath.LastIndexOf("\\"))));
                                    File.Move(Server.MapPath(strImagePath), Server.MapPath(destPath));
                                }

                            }
                            if (dtPage.Rows[i]["ImagePath2"].ToString() != "")
                            {
                                strImagePath1 = "~/" + dtPage.Rows[i]["ImagePath2"].ToString();
                                if (File.Exists(Server.MapPath(strImagePath1)))
                                {
                                    string destPath = ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles/" + strImagePath1.Substring(2);
                                    if (!Directory.Exists(Server.MapPath(destPath.Substring(0, destPath.LastIndexOf("\\")))))
                                        Directory.CreateDirectory(Server.MapPath(destPath.Substring(0, destPath.LastIndexOf("\\"))));
                                    File.Move(Server.MapPath(strImagePath1), Server.MapPath(destPath));
                                }
                            }
                            if (dtPage.Rows[i]["ImagePath3"].ToString() != "")
                            {
                                strImagePath2 = "~/" + dtPage.Rows[i]["ImagePath3"].ToString();
                                if (File.Exists(Server.MapPath(strImagePath2)))
                                {
                                    string destPath = ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles/" + strImagePath2.Substring(2);
                                    if (!Directory.Exists(Server.MapPath(destPath.Substring(0, destPath.LastIndexOf("\\")))))
                                        Directory.CreateDirectory(Server.MapPath(destPath.Substring(0, destPath.LastIndexOf("\\"))));
                                    File.Move(Server.MapPath(strImagePath2), Server.MapPath(destPath));
                                }
                            }
                            if (dtPage.Rows[i]["ImagePath4"].ToString() != "")
                            {
                                strImagePath3 = "~/" + dtPage.Rows[i]["ImagePath4"].ToString();
                                if (File.Exists(Server.MapPath(strImagePath3)))
                                {
                                    string destPath = ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles/" + strImagePath3.Substring(2);
                                    if (!Directory.Exists(Server.MapPath(destPath.Substring(0, destPath.LastIndexOf("\\")))))
                                        Directory.CreateDirectory(Server.MapPath(destPath.Substring(0, destPath.LastIndexOf("\\"))));
                                    File.Move(Server.MapPath(strImagePath3), Server.MapPath(destPath));
                                }
                            }

                            #endregion
                        }

                        string id = this.Request.QueryString["id"];

                        try
                        {
                            DataTable dtFileFolder = FileFolderManagement.GetFileFolderSizeByDocumentID(documentId.ToString());

                            int dividerId = doc.DividerID.Value;
                            string strFolderSize = dtFileFolder.Rows[0]["Otherinfo2"].ToString();
                            string strFolderID = dtFileFolder.Rows[0]["FolderID"].ToString();

                            DataTable dtDocInfor = FileFolderManagement.GetDocInforByDocumentID(documentId.ToString());
                            string strDocSize = dtDocInfor.Rows[0]["size"].ToString();

                            Double newFolderSize = Convert.ToDouble(strFolderSize) - Convert.ToDouble(strDocSize);
                            FileFolderManagement.UpdateFolderSize(strFolderID, newFolderSize);

                            //delete Document
                            if (documentId > 0)
                            {
                                //Need to Ask
                                Shinetech.DAL.General_Class.AuditLogByDocId(documentId, Sessions.SwitchedSessionId, Enum_Tatva.Action.Delete.GetHashCode());
                            }
                            FlashViewer.DeleteDocumentAndPages(documentId.ToString(), 0, Membership.GetUser().ProviderUserKey.ToString());

                            //Set IsDeleted in elastic server
                            Common_Tatva.DeleteDocument(documentId);

                            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(id), dividerId, documentId, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Delete.GetHashCode(), 0, 0);

                        }
                        catch { }


                        this.GetData(id);
                        this.GetDividerData(id);
                        BindDividers();
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('The document has been deleted successfully.');", true);

                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            }
        }
    }

    public string GetFolderIds()
    {
        string officeID1 = Sessions.SwitchedRackspaceId;
        if (string.IsNullOrEmpty(FolderIdSource))
        {
            try
            {
                DataTable dtNew = new DataTable();
                if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users")) // 
                {
                    var folders = FileFolderManagement.GetFileFoldersInfor(officeID1);

                    //Èç¹ûÊÇSub user Ôò½øÐÐÉ¸Ñ¡groups
                    var uid = Membership.GetUser().ProviderUserKey.ToString();
                    var act = new Shinetech.DAL.Account(uid);
                    string[] groups = act.Groups.Value != null ? act.Groups.Value.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries) : new string[] { };
                    bool? hasProtectedSecurity = (act.HasProtectedSecurity.IsNull || Convert.ToBoolean(act.HasProtectedSecurity.ToString())) ? true : false;
                    bool? hasPublicSecurity = (act.HasPublicSecurity.IsNull || Convert.ToBoolean(act.HasPublicSecurity.ToString())) ? true : false;
                    int count = folders.Rows.Count;

                    for (int i = 0; i < count; i++)
                    {
                        DataRow row = folders.Rows[i];
                        //µ±¸ÃFolder²»ÊÇÕâ¸öÓÃ»§´´½¨µÄfolder£¬ÇÒµ±¸ÃfolderÊÇË½ÓÐµÄ£¬²¢ÇÒ¸ÃFolder²¢²»ÔÚ¸ÃÓÃ»§ËùÊôµÄGroupsÖÐ
                        if (!uid.Equals(row["OfficeID"].ToString()))
                        {
                            string security = row["SecurityLevel"].ToString();
                            if ((security.Equals("0") && !BelongToGroup(row["Groups"].ToString(), groups)) || (security.Equals("1") && !hasProtectedSecurity.Value) || (security.Equals("2") && !hasPublicSecurity.Value))
                            {
                                row.Delete();
                            }
                        }
                    }

                    folders.AcceptChanges();

                    dtNew = folders;
                }
                else
                {
                    dtNew = FileFolderManagement.GetFileFoldersInfor(officeID1);
                }
                if (dtNew != null)
                {
                    foreach (DataRow drRow in dtNew.Rows)
                    {
                        FolderIdSource += (drRow["FolderID"]) + ", ";
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
        return FolderIdSource;
    }

    private bool BelongToGroup(string p, string[] groups)
    {
        foreach (string group in groups)
        {
            if (p.Contains(group))
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Gets or sets the folder identifier source.
    /// </summary>
    /// <value>
    /// The folder identifier source.
    /// </value>
    public string FolderIdSource
    {
        get
        {
            return this.Session["FolderIdSource"] as string;
        }
        set
        {
            this.Session["FolderIdSource"] = value;
        }
    }

    protected void btnConvertOcr_Click(object sender, EventArgs e)
    {
        try
        {
            string id = DocumentIDs.Value;
            string[] docs = DocumentIDs.Value.Split(',');
            DataTable dtDoc = new DataTable();
            dtDoc.Columns.Add("Index");
            dtDoc.Columns.Add("DocumentId");
            dtDoc.Columns.Add("OcrStatus");

            for (int i = 0; i < docs.Length; i++)
            {
                dtDoc.Rows.Add(i + 1, docs[i].Trim(), Enum_Tatva.OcrStatus.OcrRequested.GetHashCode());
            }

            General_Class.UpdateOcrStatus(dtDoc);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertOk", "alert('Your pdf(s) will be converted to searchable soon.');hideLoader();", true);
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertOk", "alert('Something went wrong. Please try again later.');hideLoader();", true);
        }
    }

    protected void gridViewOcr_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                int DocumentID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "DocumentID"));
                string Name = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "DisplayName"));
                string PathName = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Path"));

                string oldPath = PathName + Path.DirectorySeparatorChar + GetFileName(Name) + ".pdf";

                oldPath = oldPath.Replace(@"\", "/");

                Label lbl = e.Row.FindControl("serchedData") as Label;
                Label btnAll = e.Row.FindControl("lblAll") as Label;
                List<string> results = new List<string>();
                if (lbl != null)
                {
                    results = lbl.Text.Split(new string[] { "<span>EdSplit</span>" }, StringSplitOptions.None).ToList();
                    lbl.Text = results[0];
                    //lbl.Visible = false;
                }
                var finalData = string.Empty;
                for (int i = 0; i < results.Count - 1; i++)
                {
                    if (i == 0)
                        finalData += "<label class='searchedLbl' id='" + i + "' style='display:inline-block'>" + results[i] + "<br /></label>";
                    else
                        finalData += "<label class='searchedLbl' id='" + i + "' style='display:none'>" + results[i] + "<br /></label>";
                }
                if (results.Count < 3)
                {
                    Button btnNext = e.Row.FindControl("btnNext") as Button;
                    if (btnNext != null)
                        btnNext.Visible = false;
                    Button btnPrev = e.Row.FindControl("btnPrev") as Button;
                    if (btnPrev != null)
                        btnPrev.Visible = false;
                    if (btnAll != null)
                        btnAll.Visible = false;
                }
                lbl.Text = finalData;
                oldPath = oldPath.Replace(@"\", "/");

                LinkButton lnkShare = (e.Row.FindControl("lnkShare") as LinkButton);
                if (lnkShare != null)
                {
                    lnkShare.Attributes.Add("onclick", "return OnShowShare(" + DocumentID + ", '" + oldPath + "')");

                }
            }
            catch (Exception ex)
            {
                //Common_Tatva.WriteElasticLog(string.Empty, hdnFolderId.Value, searchKey.Text, ex);
            }
        }
    }

    protected void gridViewOcr_Sorting(object sender, GridViewSortEventArgs e)
    {

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
            int folderId = Convert.ToInt32(this.EffID_FFB);
            int dividerId = Convert.ToInt32(this.DividerID_FFB);
            int documentId = Convert.ToInt32(hdnFileId.Value);
            int newDocumentId = 0;
            int documentOrderId = 0;
            DataSet page = new DataSet();

            string newFileName = fileNameFld.Text;
            string oldFilePath = hdnSelectedPath.Value;

            newFileName = Common_Tatva.SubStringFilename(newFileName);
            newFileName = HttpUtility.UrlPathEncode(newFileName).Replace("%", "$").Replace("&", "").Replace("#", "");

            string oldFolderPath = oldFilePath.Substring(0, oldFilePath.LastIndexOf('/') + 1);
            oldFilePath = oldFolderPath + HttpUtility.UrlPathEncode(hdnFileNameOnly.Value).Replace("%", "$").Replace("&", "").Replace("#", "");
            oldFolderPath = oldFolderPath.Substring(0, oldFolderPath.LastIndexOf('/'));

            var newFilePath = Common_Tatva.GetNewPathOnly(oldFolderPath, dividerId, newFileName);
            if (newFilePath == oldFilePath)
            {
                ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                                "alert(\"" + "Document has been edited successfully." + "\");ShowPreviewForDocument(\"" + hdnObjId.Value + "\", true);", true);
                return;
            }

            if (newFilePath.IndexOf(".pdf") == -1)
                newFilePath = newFilePath + ".pdf";

            if (oldFilePath.IndexOf(".pdf") == -1)
                oldFilePath = oldFilePath + ".pdf";

            newFileName = newFileName.Replace("$20", " ");

            DataTable dtDoc = General_Class.GetDocumentFromDocumentID(documentId);
            if (dtDoc != null && dtDoc.Rows.Count > 0)
            {
                int documentOrder = Convert.ToInt32(dtDoc.Rows[0]["DocumentOrder"]);
                string className = Convert.ToString(dtDoc.Rows[0]["Class"]);
                // move document to new folder
                int moveRename = Common_Tatva.MoveFilesToAnotherFolder(newFilePath, oldFilePath);

                //insert file in database and folder
                if (moveRename == Convert.ToInt32(Enum_Tatva.FileMove.FileMoveSuccess))
                {
                    string documentIDOrder = Common_Tatva.InsertDocument(dividerId, newFilePath, documentOrder, newFileName, className, Membership.GetUser().ProviderUserKey.ToString());
                    if (documentIDOrder.Contains("#"))
                    {
                        newDocumentId = Convert.ToInt32(documentIDOrder.Split('#')[0]);
                        documentOrderId = Convert.ToInt32(documentIDOrder.Split('#')[1]);
                    }
                    else
                        newDocumentId = Convert.ToInt32(documentIDOrder);
                }

                #region Elastic Update

                if (newDocumentId > 0)
                    Common_Tatva.UpdateDocument(Convert.ToInt32(documentId), newDocumentId, dividerId, newFileName, newFilePath.Substring(0, newFilePath.LastIndexOf("/")));

                #endregion

                // delete document and pages from database // No need to pass switch user
                if (newDocumentId > 0)
                    page = FlashViewer.DeleteDocumentAndPages(documentId.ToString(), documentOrderId, Membership.GetUser().ProviderUserKey.ToString());

                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(folderId, Convert.ToInt32(dividerId), documentId, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Rename.GetHashCode(), 0, 0);
                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(folderId, Convert.ToInt32(dividerId), Convert.ToInt32(newDocumentId), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Create.GetHashCode(), 0, 0);
                string tempPath = newFilePath.Split(new[] { "/" + Path.GetFileName(newFilePath) }, StringSplitOptions.RemoveEmptyEntries)[0];
                tempPath = tempPath.Replace("/", @"\");


                if (newDocumentId > 0)
                {
                    Thread.Sleep(1000);
                    GetOcrDocuments();
                    hdnFileNameOnly.Value = newFileName;
                    lblFileName.Text = newFileName;
                    hdnSelectedPath.Value = newFilePath;
                    BindDividers();
                    ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                                "alert(\"" + "Document has been edited successfully." + "\");ShowPreviewForDocument(\"" + hdnObjId.Value + "\", true);", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                                   "alert(\"" + "Some error occurred. Please try again later !!" + "\");ShowPreviewForDocument(\"" + hdnObjId.Value + "\", true);", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                                   "alert(\"" + "Some error occurred. Please try again later !!" + "\");ShowPreviewForDocument(\"" + hdnObjId.Value + "\", true);", true);
            }
        }
        catch (Exception ex)
        {
            _logger.Info(ex);
            ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                                   "alert(\"" + "Some error occurred. Please try again later !!" + "\");ShowPreviewForDocument(\"" + hdnObjId.Value + "\", true);", true);
        }
    }

}

public class RedactOption
{
    public float top { get; set; }
    public float left { get; set; }
    public string id { get; set; }
    public float? prevDiff { get; set; }
    public float prevScale { get; set; }
    public double height { get; set; }
    public double width { get; set; }
    public int pageNumber { get; set; }
    public float pageHeight { get; set; }
    public float pageWidth { get; set; }
}

public class UploadFile
{
    public int dividerID { get; set; }
    public string FileID { get; set; }
    public int classId { get; set; }
    public string dividerName { get; set; }
    public bool CheckId { get; set; }
}

public class IndexKeyValue
{
    public string IndexKey { get; set; }
    public string IndexValue { get; set; }
    public int IndexId { get; set; }
}
