<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="FileRequestMonthly.aspx.cs" Inherits="FileRequestMonthly" Title="" %>

<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.ComponentModel" %>
<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>File Requests</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div class="content-title">File Requests Information Monthly About : &nbsp;<asp:Label ID="labUserName" runat="server" Text="" CssClass="text-green"></asp:Label></div>
                <div class="content-box listing-view">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="work-area" style="margin: 0">
                                <div class="overflow-auto">
                                    <asp:GridView ID="GridView3" runat="server" AutoGenerateColumns="false"
                                        AllowSorting="false" DataKeyNames="Yearly1,Monthly1" OnRowDataBound="GridView1_RowDataBound"
                                        CssClass="datatable listing-datatable">
                                        <PagerSettings Visible="False" />
                                        <AlternatingRowStyle CssClass="odd" />
                                        <RowStyle CssClass="even" />
                                        <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                        <Columns>
                                            <asp:BoundField DataField="Monthly1" HeaderText="Month">
                                                <%--<HeaderStyle CssClass="form_title" Width="80px" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Yearly1" HeaderText="Year">
                                                <%--<HeaderStyle CssClass="form_title" Width="100px" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CNT" HeaderText="Total File Request">
                                                <%--<HeaderStyle CssClass="form_title" Width="100px" />--%>
                                            </asp:BoundField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <p>
                                                <asp:Label ID="Label120" runat="server" />There are no file requests. right now.
                                            </p>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
               <%-- <div class="quick-find">
                    <uc1:QuickFind ID="QuickFind1" runat="server" />
                </div>--%>
                <div class="quick-find" style="margin-top: 15px;">
                    <div class="find-inputbox">
                        <uc2:ReminderControl ID="ReminderControl1" runat="server" />
                    </div>
                </div>
            </div>
        </div>
    </div>


</asp:Content>
