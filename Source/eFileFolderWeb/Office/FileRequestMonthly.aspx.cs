using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AjaxControlToolkit;
using Shinetech.DAL;
using Shinetech.Framework.Controls;

public partial class FileRequestMonthly : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string uid = "";
            if(string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                uid = Sessions.SwitchedSessionId;
                //uid = Membership.GetUser().ProviderUserKey.ToString();
            }
            else
            {
                uid = Request.QueryString["id"];
            }

            Account user = UserManagement.GetWebUser(uid);
            if (user == null) return;
            labUserName.Text = user.Name.Value;

            GetData(uid);
            BindGrid();

        }
    }

    #region 数据绑定
   
    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData(string uid)
    {
        this.StorageData = UserManagement.GetFileRequestMonthly(uid);

    }

    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        GridView3.DataSource = this.StorageData;
        GridView3.DataBind();
    }
    #endregion


    public DataTable StorageData
    {
        get
        {
            return this.Session["FileRequest_Monthly"] as DataTable;
        }
        set
        {
            this.Session["FileRequest_Monthly"] = value;
        }
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var rv = e.Row.DataItem as DataRowView;
            var uid = rv.Row.ItemArray[0].ToString();
            
        }
    }
}
