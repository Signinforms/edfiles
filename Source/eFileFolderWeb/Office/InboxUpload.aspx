﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="InboxUpload.aspx.cs" Inherits="Office_InboxUpload" %>

<%--<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>--%>


<%--<%@ Register Src="../Controls/FolderReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register Src="../Controls/QuickFind.ascx" TagName="QuickFind" TagPrefix="uc1" %>--%>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%--<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>--%>

<asp:Content ID="Contentinbox" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%
        var basePath = String.Format("{0}{1}", Request.ApplicationPath, string.IsNullOrEmpty(Request.ApplicationPath) || !Request.ApplicationPath.EndsWith("/") ? "/" : "");
    %>
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%=basePath%>css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js"></script>

    <%--<link href="<%=basePath%>Styles/Default.css" type="text/css" rel="Stylesheet" media="screen" />--%>
    <%--<link href="../App_Themes/White/style.css" rel="stylesheet" />--%>

    <style type="text/css">
        .white_content
        {
            display: none;
            position: fixed;
            top: 20%;
            left: 20%;
            width: 60%;
            height: 50%;
            padding: 16px;
            background-color: white;
            z-index: 999999999;
            overflow: auto;
        }

        .ajax__fileupload_dropzone
        {
            font-size: 15px;
            line-height: 120px !important;
            height: 120px !important;
            font-weight: bold;
                font-family: "Open Sans", sans-serif;
        }

        #overlay
        {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }
    </style>
    <script language="JavaScript" type="text/jscript">

        function getOs() {
            var OsObject = "";
            if (navigator.userAgent.indexOf("MSIE") > 0) {
                return "MSIE";
            }
            if (isFirefox = navigator.userAgent.indexOf("Firefox") > 0) {
                return "Firefox";
            }
            if (isSafari = navigator.userAgent.indexOf("Safari") > 0) {
                return "Safari";
            }
            if (isCamino = navigator.userAgent.indexOf("Camino") > 0) {
                return "Camino";
            }
            if (isMozilla = navigator.userAgent.indexOf("Gecko/") > 0) {
                return "Gecko";
            }

        }
        var os = getOs();

        function onClientUploadComplete(sender, e) {
            return false;
        }

        function onClientUploadCompleteAll(sender, e) {
            $('.hideControl').removeClass('hideControl');
            $('.uploadProgress').find('select').removeAttr('disabled');

            $('#divUpload').clone().appendTo('#uploadFieldset');
            clearInterval(myTimer);
            document.getElementById('uploadProgress').style.display = 'none';

            setTimeout(function () {
                var vd = '<%= ConfigurationManager.AppSettings["VirtualDir"] %>';
                window.location.reload();
            }, 2000);
        }

        $(document).ready(function () {
            $(".ajax__fileupload_dropzone").text("Drag and Drop Pdf file(s) here");
        });

        var myTimer;
        var fileIDDividerId = [];
        function onClientUploadStart(sender, e) {
            document.getElementById('uploadCompleteInfo').innerHTML = "please wait while the " + e.get_filesInQueue() + " file is uploaded and converted to eff...";
            document.cookie = "Files=" + JSON.stringify(fileIDDividerId);
            showLoader();

            $('[id*=AjaxFileUpload1_Html5DropZone]').addClass('hideControl').css('display', 'none');
            //$('[id*=AjaxFileUpload1_SelectFileContainer]').addClass('hideControl').css('display', 'none');
            $('[id*=FileItemDeleteButton]').addClass('hideControl').css('display', 'none');
            $('[id*=UploadOrCancelButton]').addClass('hideControl').css('display', 'none');
            $('.TabDropDown').prop('disabled', 'disabled');
            $('#uploadProgress').html('');
            $('#uploadProgress').html('').html($('#divUpload').html());

            document.getElementById('uploadProgress').style.display = 'block';

            //myTimer = setInterval(function () {
            //    $('#uploadProgress').html('').html($('#divUpload').html());
            //}, 1000);
        }

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');;
        }

        function hideLoader() {
            $('#overlay').hide();
        }

        function onClientButtonClick() {
            $find('pnlPopup2').hide();
            setTimeout(function () {
            }, 1000);
            return false;
        }

    </script>

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Inbox Upload File</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <asp:Button ID="Button1ShowPopup2" runat="server" Style="display: none" />
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BehaviorID="pnlPopup2"
        PopupControlID="pnlPopup2" DropShadow="true" BackgroundCssClass="modalBackground"
        Enabled="True" TargetControlID="Button1ShowPopup2" />
    <asp:Panel ID="pnlPopup2" Style="display: none"
        runat="server" class="popup-mainbox">
        <div>
            <div class="popup-head" id="Div1">
                Message
            </div>
            <div class="popup-scroller">
                <p>
                    Your document has been uploaded successfully...It will be converting to the edFile format in our server over the next few minutes, depending on the size of the uploaded document. You can now select additional files to upload or navigate away from this web page. 
                </p>
            </div>
            <div class="popup-btn">
                <asp:Button ID="ShowPopup2Button2" runat="server" OnClientClick="return onClientButtonClick()"
                    Text="Close" class="btn green" />
            </div>
        </div>
    </asp:Panel>
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <div class="inner-wrapper">
        <div class="page-container">
            <div class="dashboard-container">
                <div class="left-content">
                    <div style="overflow: hidden;">

                        <asp:Panel ID="panelMain" runat="server" Visible="true">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>

                                    <fieldset id="uploadFieldset" style="width: 650px; font: 20px bold">
                                        Inbox
                            <div id="divUpload">
                                <div class='clearfix'></div>
                                <asp:Image runat="server" ID="Image12" ImageUrl="../Images/loader_bg.gif" ImageAlign="Middle" />
                                <asp:Label ID="label1" runat="server"></asp:Label>
                                <asp:Label runat="server" ID="Label2" Style="display: none;">
                                <img align="absmiddle" alt="" src="uploading.gif"/></asp:Label>

                                <div id="uploadProgress" class="white_content"></div>

                                <ajaxToolkit:AjaxFileUpload ID="AjaxFileUpload1" runat="server" Padding-Bottom="4" Width="730"
                                    Padding-Left="2" Padding-Right="10" Padding-Top="4" ThrobberID="myThrobber" OnClientUploadComplete="onClientUploadComplete"
                                    OnUploadComplete="AjaxFileUpload1_OnUploadComplete" MaximumNumberOfFiles="10"
                                    AllowedFileTypes="jpg,jpeg,pdf" AzureContainerName="" OnClientUploadCompleteAll="onClientUploadCompleteAll"
                                    OnUploadCompleteAll="AjaxFileUpload1_UploadCompleteAll" OnUploadStart="AjaxFileUpload1_UploadStart"
                                    ContextKeys="2" />
                                <div class='clearfix'></div>
                                <div id="uploadCompleteInfo">
                                </div>
                            </div>
                                    </fieldset>
                                    <br />
                                    <br />

                                    <%--GridView--%>
                                    <div class="form_title_2">

                                        <asp:Image runat="server" ImageUrl="../Images/loader_bg.gif" ID="lbsStatus" ImageAlign="Middle" />
                                    </div>
                                    <div class='clearfix'></div>
                                    <div class="prevnext" style="float: right" bgcolor="#f8f8f5">
                                        <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                            PageSize="5" CssClass="prevnext" PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>
                                        <asp:HiddenField ID="FolderID" runat="server" />
                                    </div>
                                    <asp:GridView ID="GridView1" runat="server" AllowSorting="True"
                                        AutoGenerateColumns="false" BorderWidth="2" OnRowDataBound="GridView1_RowDataBound"
                                        CssClass="datatable listing-datatable">
                                        <PagerSettings Visible="False" />
                                        <AlternatingRowStyle CssClass="odd" />
                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                        <Columns>




                                            <%--convert label into textbox--%>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                <HeaderTemplate>
                                                    <asp:Label ID="FileUID" runat="server" Text="Document Name" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("FileUID")%>' name="DocumentName" CssClass="lblDocumentName" />
                                                    <asp:TextBox runat="server" MaxLength="50" Text='<%#Eval("FileUID")%>'
                                                        ID="TextDocumentName" Style="display: none; height: 30px;" name="txtDocumentName" CssClass="txtDocumentName" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--end--%>

                                            <asp:BoundField DataField="Size" HeaderText="Size(Mb)" DataFormatString="{0:F2}" ItemStyle-Width="5px"
                                                HtmlEncode="false" />
                                            <asp:BoundField DataField="DOB" HeaderText="Create Date" DataFormatString="{0:MM.dd.yyyy}" ItemStyle-Width="5px"
                                                SortExpression="DOB" />
                                        </Columns>
                                    </asp:GridView>

                                    <br />
                                    <br />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>

                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

