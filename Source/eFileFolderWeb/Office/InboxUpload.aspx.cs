﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;

//public partial class Office_InboxUpload : System.Web.UI.Page
//{
//    protected void Page_Load(object sender, EventArgs e)
//    {

//    }
//}

using System;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;

using System.Linq;
using DataQuicker2.Framework;
using Shinetech.DAL;
using Shinetech.Utility;
using BarcodeLib;
using Shinetech.Framework.Controls;
using AjaxControlToolkit;
using Shinetech.Engines;
using System.Web.Script.Serialization;
using System.Xml;
using System.Security.AccessControl;
using System.IO.Compression;
using System.Text;
using System.Collections.Generic;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Protocols;
//using Aspose.Pdf;
using Account = Shinetech.DAL.Account;

using System.Xml;
using Shinetech.Utility;
using System.Web.Script.Services;

//public partial class Office_Welcome : System.Web.UI.Page
public partial class Office_InboxUpload : StoragePage
{
    public string FolderId;
    public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    public string virtualDir = ConfigurationManager.AppSettings["VirtualDir"];
    public string newFileName = string.Empty;
    //DataTable dtOrder = new DataTable();
    string Uid = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Convert.ToString(Request.QueryString["UserId"]) != null)
        //{
        //    Uid = Convert.ToString(Request.QueryString["UserId"]);
        //}
        if (!Page.IsPostBack)
        {
            //string uid = Membership.GetUser().ProviderUserKey.ToString();
            var argument = Request.Form["__EVENTARGUMENT"];
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
            {
                GetData();
                BindGrid();
            }
            argument = "";
            AjaxFileUpload1.Enabled = true;
            if (!Page.IsPostBack)
            {
                string uid = Sessions.SwitchedSessionId;
                //string uid = Membership.GetUser().ProviderUserKey.ToString();
                GetData();
                BindGrid();
            }
            if (!AjaxFileUpload1.IsInFileUploadPostBack)
            {
                GetData();
                BindGrid();
            }
        }
        //}
        else
        {
            if (AjaxFileUpload1.IsInFileUploadPostBack)
            {
                // do for ajax file upload partial postback request
            }
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["Files_Workarea"] as DataTable;
        }
        set
        {
            this.Session["Files_Workarea"] = value;
        }
    }


    //public string DocumentID
    //{
    //    get
    //    {
    //        return (string)Session["DocumentID"];
    //    }
    //    set
    //    {
    //        Session["DocumentID"] = value;

    //    }
    //}

    //public string DocumentName
    //{
    //    get
    //    {
    //        return (string)Session["DocumentName"];
    //    }
    //    set
    //    {
    //        Session["DocumentName"] = value;

    //    }
    //}

    //public DataTable DividerSource
    //{
    //    get
    //    {
    //        return this.Session["DividerSource_FolderBox"] as DataTable;
    //    }
    //    set
    //    {
    //        this.Session["DividerSource_FolderBox"] = value;
    //    }
    //}

    public string DateTimeInfo
    {
        get
        {
            DateTimeFormatInfo myDTFI = new CultureInfo("en-US", false).DateTimeFormat;
            DateTime dt = DateTime.Now;
            return dt.ToString("MM/dd/yyy", myDTFI);

        }
    }


    #region FileUpload
    protected void AjaxFileUpload1_OnUploadComplete(object sender, AjaxFileUploadEventArgs file)
    {
        try
        {
            string path = Server.MapPath("~/" + Common_Tatva.Inbox);
            string uid = Sessions.SwitchedSessionId;
            //string uid = Membership.GetUser().ProviderUserKey.ToString();
            //string uid = Uid;

            string fullPath = Path.Combine(path, uid);

            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath);
            }

            if (AjaxFileUpload1.IsInFileUploadPostBack)
            {
                var fullname = Path.Combine(fullPath, file.FileName);
                AjaxFileUpload1.SaveAs(fullname);
                GetData();
                BindGrid();
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void AjaxFileUpload1_UploadCompleteAll(object sender, AjaxFileUploadCompleteAllEventArgs e)
    {
        var startedAt = (DateTime)Session["uploadTime"];
        var now = DateTime.Now;
        e.ServerArguments = new JavaScriptSerializer()
            .Serialize(new
            {
                duration = (now - startedAt).Seconds,
                time = DateTime.Now.ToShortTimeString()
            });

    }

    protected void AjaxFileUpload1_UploadStart(object sender, AjaxFileUploadStartEventArgs e)
    {
        var now = DateTime.Now;
        e.ServerArguments = now.ToShortTimeString();
        Session["uploadTime"] = now;
    }

    #endregion
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
            string path = drv.Row[3].ToString();
            path = path.Replace(@"\", "/");
        }
    }


    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
        //UpdatePanel1.Update();
    }

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }
    private void GetData()
    {
        //if (Uid != null && Uid != "")
        //{
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        //string uid = Uid;
        DataTable table = GetWorkareaFiles(uid);
        if (table != null)
        {
            this.DataSource = table;
        }
        //}
    }


    protected DataTable GetWorkareaFiles(string uid)
    {
        DataTable table = MakeDataTable();

        ArrayList uids = new ArrayList();
        uids.Add(uid);

        string path = Server.MapPath("~/" + Common_Tatva.Inbox);
        if (this.Page.User.IsInRole("Offices"))
        {
            uids.AddRange(UserManagement.GetSubUserList(uid));
        }
        else if (this.Page.User.IsInRole("Users"))
        {
            DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(uid);
            var officeID1 = dtOfficeUser.Rows[0]["OfficeUID"].ToString();
            uids.Add(officeID1);

            IList subuses = UserManagement.GetSubUserList(officeID1);

            foreach (string sub in subuses)
            {
                if (sub.ToLower() != uid.ToLower())
                {
                    uids.Add(sub);
                }
            }

        }

        string tempPath = string.Empty;
        foreach (string id in uids)
        {
            tempPath = string.Concat(path, Path.DirectorySeparatorChar, id);

            try
            {
                if (Directory.Exists(tempPath))
                {
                    string[] files = Directory.GetFiles(tempPath);
                    DataRow row = null;
                    FileInfo fi1;

                    string ext = string.Empty;

                    foreach (string file in files)
                    {
                        row = table.NewRow();
                        row["FileUID"] = Path.GetFileNameWithoutExtension(file);

                        byte[] byteArray = System.Text.Encoding.Default.GetBytes(Path.GetFileName(file));
                        row["FileID"] = System.Convert.ToBase64String(byteArray);

                        byteArray = System.Text.Encoding.Default.GetBytes(id); //uid
                        row["UID"] = System.Convert.ToBase64String(byteArray); ;

                        row["FilePath"] = file;
                        fi1 = new FileInfo(file);
                        row["Size"] = ((double)fi1.Length) / (double)(1024 * 1024);//MB
                        row["DOB"] = fi1.CreationTime;

                        ext = Path.GetExtension(file);
                        table.Rows.Add(row);
                        fi1 = null;
                    }

                    string[] varpaths = Directory.GetDirectories(tempPath);
                    DirectoryInfo info;

                    foreach (string item in varpaths)
                    {
                        info = new DirectoryInfo(item);
                        files = Directory.GetFiles(item);
                        if (files.Length > 0)
                        {
                            foreach (string subfile in files)
                            {
                                row = table.NewRow();
                                row["FileUID"] = Path.GetFileName(subfile);

                                string subfileName = subfile.Substring(tempPath.Length);
                                byte[] byteArray = System.Text.Encoding.Default.GetBytes(subfileName);
                                row["FileID"] = System.Convert.ToBase64String(byteArray);

                                byteArray = System.Text.Encoding.Default.GetBytes(id); //uid
                                row["UID"] = System.Convert.ToBase64String(byteArray); ;

                                row["FilePath"] = subfile;
                                fi1 = new FileInfo(subfile);
                                row["Size"] = ((double)fi1.Length) / (double)(1024 * 1024);//MB
                                row["DOB"] = fi1.CreationTime;

                                ext = Path.GetExtension(subfile);
                                table.Rows.Add(row);
                                fi1 = null;
                            }
                        }
                    }
                }
                else
                {
                    if (id == uid)
                    {
                        Directory.CreateDirectory(tempPath);
                    }
                }
            }
            catch (Exception exp)
            {

            }
        }
        DataView dv = table.DefaultView;
        dv.Sort = "DOB desc";
        table = dv.ToTable();
        return table;
    }

    //protected DataTable GetWorkareaFiles(string uid, string keyword)
    //{
    //    DataTable table = MakeDataTable();
    //    string path = Server.MapPath("~/" + Common_Tatva.Inbox);
    //    path = string.Concat(path, Path.DirectorySeparatorChar, uid);

    //    try
    //    {
    //        if (Directory.Exists(path))
    //        {
    //            string[] files = Directory.GetFiles(path);
    //            DataRow row = null;
    //            FileInfo fi1;
    //            string fileName = string.Empty;
    //            //string ext = string.Empty;

    //            foreach (string file in files)
    //            {
    //                fileName = Path.GetFileName(file);
    //                if (fileName.IndexOf(keyword, StringComparison.CurrentCultureIgnoreCase) >= 0)
    //                {
    //                    row = table.NewRow();
    //                    row["FileUID"] = Path.GetFileName(file);

    //                    byte[] byteArray = System.Text.Encoding.Default.GetBytes(Path.GetFileName(file));
    //                    row["FileID"] = System.Convert.ToBase64String(byteArray);

    //                    byteArray = System.Text.Encoding.Default.GetBytes(uid);
    //                    row["UID"] = System.Convert.ToBase64String(byteArray); ;

    //                    row["FilePath"] = file;
    //                    fi1 = new FileInfo(file);
    //                    row["Size"] = ((double)fi1.Length) / (double)(1024 * 1024);//MB
    //                    row["DOB"] = fi1.CreationTime;

    //                    table.Rows.Add(row);
    //                    fi1 = null;
    //                }
    //            }

    //            string[] varpaths = Directory.GetDirectories(path);
    //            DirectoryInfo info;

    //            foreach (string item in varpaths)
    //            {
    //                files = Directory.GetFiles(item);
    //                if (files.Length > 0)
    //                {
    //                    // double sizetotal = 0;
    //                    string details = string.Empty;
    //                    foreach (string subfile in files)
    //                    {
    //                        fileName = Path.GetFileName(subfile);
    //                        if (fileName.IndexOf(keyword, StringComparison.CurrentCultureIgnoreCase) >= 0)
    //                        {
    //                            row = table.NewRow();
    //                            row["FileUID"] = Path.GetFileName(subfile);

    //                            string subfileName = subfile.Substring(path.Length);
    //                            byte[] byteArray = System.Text.Encoding.Default.GetBytes(subfileName);
    //                            row["FileID"] = System.Convert.ToBase64String(byteArray);

    //                            byteArray = System.Text.Encoding.Default.GetBytes(uid); //uid
    //                            row["UID"] = System.Convert.ToBase64String(byteArray);
    //                            ;

    //                            row["FilePath"] = subfile;
    //                            fi1 = new FileInfo(subfile);
    //                            row["Size"] = ((double)fi1.Length) / (double)(1024 * 1024); //MB
    //                            row["DOB"] = fi1.CreationTime;

    //                            table.Rows.Add(row);
    //                            fi1 = null;
    //                        }
    //                    }
    //                }

    //            }
    //        }
    //        else
    //        {
    //            Directory.CreateDirectory(path);

    //        }
    //    }
    //    catch { }

    //    DataView dv = table.DefaultView;
    //    dv.Sort = "DOB desc";
    //    table = dv.ToTable();
    //    return table;
    //}

    private DataTable MakeDataTable()
    {
        // Create new DataTable.
        DataTable table = new DataTable();

        // Declare DataColumn and DataRow variables.
        DataColumn column;

        // Create new DataColumn, set DataType, ColumnName
        // and add to DataTable.    
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FileUID";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FileID";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "UID";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "FilePath";
        table.Columns.Add(column);


        column = new DataColumn();
        column.DataType = Type.GetType("System.Double");
        column.ColumnName = "Size";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.DateTime");
        column.ColumnName = "DOB";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "FolderItems";
        column.DefaultValue = "";
        table.Columns.Add(column);

        return table;
    }


    //protected void LinkButton2_Click(Object sender, CommandEventArgs e)
    //{
    //    string password = textPassword.Text.Trim();
    //    string strPassword = PasswordGenerator.GetMD5(password);

    //    if (UserManagement.GetPassword(User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
    //    {
    //        //show the password is wrong        
    //        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('Your password is wrong,Please check it!');", true);

    //    }
    //    else
    //    {
    //        string pathRoot = Server.MapPath("~/" + Common_Tatva.Inbox);
    //        if (!Directory.Exists(pathRoot))
    //        {
    //            Directory.CreateDirectory(pathRoot);
    //        }

    //        string pathSource = e.CommandArgument.ToString();
    //        string destPath = string.Empty;
    //        if (pathSource.IndexOf("WorkArea") > 0)
    //        {
    //            destPath = pathSource.Replace("WorkArea", "WorkAreaPath");
    //            string destDirectory = Path.GetDirectoryName(destPath);
    //            try
    //            {
    //                if (!Directory.Exists(destDirectory))
    //                {
    //                    Directory.CreateDirectory(destDirectory);
    //                }

    //                if (File.Exists(destPath))
    //                {
    //                    File.Delete(destPath);
    //                }

    //                File.Move(pathSource, destPath);
    //                GetData();
    //                BindGrid();


    //            }
    //            catch (Exception exp) { Console.WriteLine(exp.Message); }
    //        }
    //    }
    //}
}

