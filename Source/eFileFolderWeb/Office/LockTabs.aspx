﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="LockTabs.aspx.cs" Inherits="Office_LockTabs" Title="" %>

<%@ Import Namespace="System.Data" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register Src="../Controls/UserFind.ascx" TagName="UserFind" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
        <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/style.css" rel="Stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/edFiles/EdFilesLogs.js?v=4"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <style type="text/css">
        input[type='checkbox']
        {
            height: 18px;
            width: 18px;
            cursor: pointer;
        }

        .chckBox
        {
            float: right;
        }
        .find .tab {
            margin-left:15px;
        }
        

        .find .find-input
        {
            height: 43px;
            padding: 5px;
            /*width: 100%;*/
            float: left;
            border: 2px solid #d4d4d4;
        }

        .find .quick-find-btn
        {
            width: 39px;
            height: 29px;
            font-size: 0;
            background-image: url(../images/quickfind-bg.png);
            background-repeat: no-repeat;
            background-position: center center;
            border-radius: 0;
            -webkit-border-radius: 0;
            position: absolute;
            border: none;
            margin-left: -45px;
            margin-top: 6px;
            background-color: transparent;
            cursor: pointer;
        }

        #overlay
        {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
            left: 0;
        }

        #floatingCirclesG
        {
            top: 50%;
            left: 50%;
            position: fixed;
        }
    </style>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Lock Tabs</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div style="float: left">
                <div class="find">
                    <asp:TextBox ID="searchBox" runat="server" class="find-input" MaxLength="50" OnTextChanged="searchBox_TextChanged" autocomplete="off" placeholder="Enter folder name :" AutoPostBack="true" />
                    <asp:Button ID="searchButton1" runat="server" ImageAlign="AbsMiddle" CausesValidation="false" OnClick="searchBox_TextChanged" Text="Search" class="quick-find-btn" />
                    <asp:TextBox ID="searchBoxTab" runat="server" class="find-input tab" MaxLength="50" OnTextChanged="searchBox_TextChanged" autocomplete="off" placeholder="Enter Tab name :" AutoPostBack="true"  />
                    <asp:Button ID="searchButton2" runat="server" ImageAlign="AbsMiddle" CausesValidation="false" OnClick="searchBox_TextChanged" Text="Search" class="quick-find-btn searchtab" />

                </div>
            </div>
            <div style="float: right; margin-top: 15px;">
                <asp:HiddenField ID="checkBoxData" runat="server" />
                <%--<input type="checkbox" onchange="selectAllCheckBoxes();" id="cbSelectAll"
                    class="JchkAll" style="height: 20px; width: 20px;" /><span style="font-size: 15px; font-weight: bold; margin-left: 5px; margin-right: 10px;">Select/Deselect All</span>--%>
                <asp:Button runat="server" ID="locksearchtab" ToolTip="Locked Selected Tab(s)" Style="margin-right:15px" OnClientClick="CollectData()" OnClick="locksearchtab_Click" CssClass="create-btn btn-small green checkQuestion" Text="Lock Search Tabs" />
                <asp:Button runat="server" ID="saveLockedTabData" ToolTip="Hide Locked Tabs for Selected User(s)" OnClientClick="CollectData()" OnClick="saveLockedTabData_Click" CssClass="create-btn btn-small green checkQuestion" Text="Lock" />
            </div>
            <div class="form-containt listing-contant">

                <div class="overflow-auto">
                    <asp:GridView ID="GridView1" runat="server" OnRowDataBound="GridView_RowDataBound" OnRowCreated="GridView_RowCreated"
                        OnSorting="GridView1_Sorting" AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                        <PagerSettings Visible="False" />
                        <AlternatingRowStyle CssClass="odd" />
                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                        <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                        <Columns>
                            <%--<asp:TemplateField ShowHeader="true">
                                <ItemTemplate>
                                    <input type="checkbox" class="parentChk" style="float: left" onchange="parentChecked(this)" />
                                    <asp:HiddenField ID="hiddenField1" runat="server" Value='<%#Eval("FolderId") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:BoundField DataField="FolderName" SortExpression="FolderName" HeaderText="FolderName" />
                            <asp:TemplateField ShowHeader="true">
                                <HeaderTemplate>
                                    <asp:Label ID="Label8" runat="server" Text="Tab" />
                                </HeaderTemplate>
                                <HeaderStyle Width="150" />
                                <ItemTemplate>
                                    <asp:Label ID="TabName1" runat="server" Text='<%#Eval("TabName1") %>'></asp:Label>
                                    <asp:CheckBox ID="TabLocked1" CssClass="chckBox" runat="server" Checked='<%#Eval("TabLocked1") %>' />
                                    <asp:HiddenField ID="hiddenDID1" runat="server" Value='<%#Eval("TabId1") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="100" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="true">
                                <HeaderTemplate>
                                    <asp:Label ID="Label8" runat="server" Text="Tab" />
                                </HeaderTemplate>
                                <HeaderStyle Width="150" />
                                <ItemTemplate>
                                    <asp:Label ID="TabName2" runat="server" Text='<%#Eval("TabName2") %>'></asp:Label>
                                    <asp:CheckBox ID="TabLocked2" CssClass="chckBox" runat="server" Checked='<%#Eval("TabLocked2") %>' />
                                    <asp:HiddenField ID="hiddenDID2" runat="server" Value='<%#Eval("TabId2") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="100" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="true">
                                <HeaderTemplate>
                                    <asp:Label ID="Label8" runat="server" Text="Tab" />
                                </HeaderTemplate>
                                <HeaderStyle Width="150" />
                                <ItemTemplate>
                                    <asp:Label ID="TabName3" runat="server" Text='<%#Eval("TabName3") %>'></asp:Label>
                                    <asp:CheckBox ID="TabLocked3" CssClass="chckBox" runat="server" Checked='<%#Eval("TabLocked3") %>' />
                                    <asp:HiddenField ID="hiddenDID3" runat="server" Value='<%#Eval("TabId3") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="100" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="true">
                                <HeaderTemplate>
                                    <asp:Label ID="Label8" runat="server" Text="Tab" />
                                </HeaderTemplate>
                                <HeaderStyle Width="150" />
                                <ItemTemplate>
                                    <asp:Label ID="TabName4" runat="server" Text='<%#Eval("TabName4") %>'></asp:Label>
                                    <asp:CheckBox ID="TabLocked4" CssClass="chckBox" runat="server" Checked='<%#Eval("TabLocked4") %>' />
                                    <asp:HiddenField ID="hiddenDID4" runat="server" Value='<%#Eval("TabId4") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="100" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="true">
                                <HeaderTemplate>
                                    <asp:Label ID="Label8" runat="server" Text="Tab" />
                                </HeaderTemplate>
                                <HeaderStyle Width="150" />
                                <ItemTemplate>
                                    <asp:Label ID="TabName5" runat="server" Text='<%#Eval("TabName5") %>'></asp:Label>
                                    <asp:CheckBox ID="TabLocked5" CssClass="chckBox" runat="server" Checked='<%#Eval("TabLocked5") %>' />
                                    <asp:HiddenField ID="hiddenDID5" runat="server" Value='<%#Eval("TabId5") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="100" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="true">
                                <HeaderTemplate>
                                    <asp:Label ID="Label8" runat="server" Text="Tab" />
                                </HeaderTemplate>
                                <HeaderStyle Width="150" />
                                <ItemTemplate>
                                    <asp:Label ID="TabName6" runat="server" Text='<%#Eval("TabName6") %>'></asp:Label>
                                    <asp:CheckBox ID="TabLocked6" CssClass="chckBox" runat="server" Checked='<%#Eval("TabLocked6") %>' />
                                    <asp:HiddenField ID="hiddenDID6" runat="server" Value='<%#Eval("TabId6") %>' />
                                </ItemTemplate>
                                <ItemStyle Width="100" />
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <asp:Label ID="Label2" runat="server" CssClass="Empty" />There is no record for searched folder name.
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <div style="float: left; margin-top: 15px;">
                    <asp:Button runat="server" ID="Button2" ToolTip="Locked Selected Tab(s)" Style="margin-right:15px" OnClientClick="CollectData()" OnClick="locksearchtab_Click" CssClass="create-btn btn-small green checkQuestion" Text="Lock Search Tabs" />
                    <asp:Button runat="server" ID="Button1" ToolTip="Hide Locked Tabs for Selected User(s)" OnClientClick="CollectData()" OnClick="saveLockedTabData_Click" CssClass="create-btn btn-small green checkQuestion" Text="Lock" />
                </div>
                <div class="prevnext custom_select" style="float: right; margin-top: 10px;">
                    <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" OnPageSizeChanged="WebPager1_PageSizeChanged"
                        PageSize="50" CssClass="prevnext" PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>
                    <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                </div>
            </div>
        </div>
    </div>

    <div id="overlay" style="display: none;">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>

    <script type="text/javascript">

        var checkArray = [];

        $(document).ready(function () {
            $('input:checkbox').not('.parentChk').not('.JchkAll').click(function () {
                var isChecked = $(this).prop('checked');
                //var $parent = $(this).parent().parent().parent();
                $(this).prop("checked", isChecked);
                tabId = $(this).parent().siblings().last().val();
                if (!updateArray(tabId, isChecked))
                    pushArray(tabId, isChecked);
                //if ($parent.find('input:checkbox').not('.parentChk').length != $parent.find('input:checkbox:checked').not('.parentChk').length)
                //    $parent.find('.parentChk').prop("checked", false);
                //else if ($parent.find('input:checkbox').not('.parentChk').length == $parent.find('input:checkbox:checked').not('.parentChk').length)
                //    $parent.find('.parentChk').prop("checked", true);
            })
        });

        function disableScrollbar() {
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }

        function enableScrollbar() {
            $('html, body').css({
                'overflow': 'auto',
                'height': 'auto'
            });
        }

        function showLoader() {
            $('#overlay').show();
            disableScrollbar();
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
            enableScrollbar();
        }

        function updateArray(tabId, isLocked) {
            for (var i = 0; i < checkArray.length; i++) {
                if (checkArray[i].DividerId == tabId) {
                    checkArray[i].Locked = isLocked;
                    return true;
                }
            }
            return false;
        }

        function pushArray(tabId, isLocked) {
            checkArray.push({ 'DividerId': tabId, 'Locked': isLocked });
        }

        //function parentChecked(e) {
        //    var tabId;
        //    $this = $(e).parent().parent();
        //    $next = $(e).parent().parent().next();
        //    var isChecked = $this.find('.parentChk').prop('checked');
        //    $this.find('input:checkbox').not('.parentChk').each(function () {
        //        $(this).prop("checked", isChecked);
        //        tabId = $(this).parent().siblings().last().val();
        //        if (!updateArray(tabId, isChecked))
        //            pushArray(tabId, isChecked);
        //    });
        //    if ($this.attr("class") == $next.attr("class")) {
        //        $next.find('input:checkbox').not('.parentChk').each(function () {
        //            $(this).prop("checked", isChecked);
        //            tabId = $(this).parent().siblings().last().val();
        //            if (!updateArray(tabId, isChecked))
        //                pushArray(tabId, isChecked);
        //        });
        //    }
        //}

        //function pageLoad() {
        //    showLoader();
        //    $('tbody > tr:visible').not(':first').not(':last').each(function () {
        //        if ($(this).attr('class') == $(this).prev().attr('class'))
        //            $(this).find('.parentChk').hide();
        //    });
        //    $('.parentChk').each(function () {
        //        if ($(this).parent().siblings().find('input:checkbox').length == $(this).parent().siblings().find('input:checkbox:checked').length)
        //            $(this).prop("checked", true);
        //    });
        //    hideLoader();
        //}

        //function selectAllCheckBoxes() {
        //    $('tbody > tr:visible').not(':first').not(':last').find('input:checkbox').each(function () {
        //        $(this).prop("checked", $('.JchkAll').is(':checked'));
        //        tabId = $(this).parent().siblings().last().val();
        //        if (!updateArray(tabId, $(this).prop("checked")))
        //            pushArray(tabId, $(this).prop("checked"));
        //    });
        //}

        function CollectData() {
            showLoader();
            $('#<%= checkBoxData.ClientID %>').val(JSON.stringify(checkArray));
        }

        //function CollapseOrExpand(a) {
        //    if ($(a).attr("class") == "Expand") {
        //        $(a).attr("class", "Collapse");
        //        $(a).attr("src", "../images/minus.gif");
        //        $(a).attr("title", "Collapse");
        //        $('.' + $(a).parent().parent().attr("class")).not($(a).parent().parent()).css("display", "");
        //    }
        //    else {
        //        $(a).attr("class", "Expand");
        //        $(a).attr("src", "../images/plus.gif");
        //        $(a).attr("title", "Expand");
        //        $('.' + $(a).parent().parent().attr("class")).not($(a).parent().parent()).css("display", "none");
        //    }
        //}
    </script>
</asp:Content>
