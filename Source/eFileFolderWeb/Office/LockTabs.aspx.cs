﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DataQuicker2.Framework;
using Shinetech.Framework.Controls;
using Shinetech.Utility;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;
using Account = Shinetech.DAL.Account;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Web.Services;

public partial class Office_LockTabs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
    }

    public void GetData()
    {
        DataTable dtTabs = General_Class.GetTabsByOfficeId(Sessions.SwitchedSessionId);
        //DataTable dtTabs = General_Class.GetTabsByOfficeId(Membership.GetUser().ProviderUserKey.ToString());
        DataTable dtCloned = dtTabs.Clone();
        
        for (int i = 1; i < 7; i++)
        {
            dtCloned.Columns["TabLocked"+ i].DataType = typeof(Boolean);
        }
        foreach (DataRow row in dtTabs.Rows)
        {
            dtCloned.ImportRow(row);
        }
        this.DataSource = dtCloned;
        this.BackDataSource = dtCloned;
        if (!string.IsNullOrEmpty(searchBox.Text) || !string.IsNullOrEmpty(searchBoxTab.Text)) 
        {
            DataRow[] dr = this.BackDataSource.Select("(FolderName Like '%" + searchBox.Text + "%') AND ( TabName1 Like '%" + searchBoxTab.Text + "%'OR TabName2 Like '%" + searchBoxTab.Text + "%'OR TabName3 Like '%" + searchBoxTab.Text + "%'OR TabName4 Like '%" + searchBoxTab.Text + "%'OR TabName5 Like '%" + searchBoxTab.Text + "%'OR TabName6 Like '%" + searchBoxTab.Text + "%')");
            if (dr.Length > 0)
                this.DataSource = dr.CopyToDataTable();
            else
                this.DataSource = new DataTable();
        }
    }
    public void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        if (this.DataSource != null)
        {
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        if (this.DataSource != null)
        {
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource_Tabs"] as DataTable;
        }
        set
        {
            this.Session["DataSource_Tabs"] = value;
        }
    }

    public DataTable BackDataSource
    {
        get
        {
            return this.Session["Back_DataSource_Tabs"] as DataTable;
        }
        set
        {
            this.Session["Back_DataSource_Tabs"] = value;
        }
    }

    protected void GridView_RowCreated(Object sender, GridViewRowEventArgs e)
    {
    }

    protected void GridView_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnFolderId = e.Row.FindControl("hiddenField1") as HiddenField;
            //e.Row.CssClass = hdnFolderId.Value ;
            for (int i = 1; i < 7; i++)
            {
                HiddenField hdn = e.Row.FindControl("hiddenDID" + i) as HiddenField;
                CheckBox hdnChk = e.Row.FindControl("TabLocked" + i) as CheckBox;
                if (hdn.Value == "" || hdn.Value == null)
                    hdnChk.Visible = false;
            }
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();//ÖØÐÂÉèÖÃÊý¾ÝÔ´£¬°ó¶¨
        BindGrid();
    }

    protected void saveLockedTabData_Click(object sender, EventArgs e)
    {
        DataTable dtLocked = new DataTable();
        dtLocked.Columns.Add("DividerId", typeof(string));
        dtLocked.Columns.Add("Locked", typeof(int));
        dtLocked.Columns.Add("Index", typeof(int));
        dtLocked.Columns.Add("Action", typeof(int));
        LockedTabs[] lockedTab = JsonConvert.DeserializeObject<List<LockedTabs>>(checkBoxData.Value).ToArray();
        int index = 1;
        foreach (var item in lockedTab)
        {
            if (!string.IsNullOrEmpty(item.DividerId))
            {
                dtLocked.Rows.Add(new object[] { item.DividerId, item.Locked, index, item.Locked ? Enum_Tatva.DocumentsAndFoldersLogs.Lock.GetHashCode() : Enum_Tatva.DocumentsAndFoldersLogs.Unlock.GetHashCode() });
                index++;
            }
        }
        General_Class.UpdateLocksForSelectedTabs(dtLocked, Sessions.UserId);
        GetData();
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }
    protected void searchBox_TextChanged(object sender, EventArgs e)
    {

        DataRow[] dr = this.BackDataSource.Select("(FolderName Like '%" + searchBox.Text + "%') AND ( TabName1 Like '%" + searchBoxTab.Text + "%'OR TabName2 Like '%" + searchBoxTab.Text + "%'OR TabName3 Like '%" + searchBoxTab.Text + "%'OR TabName4 Like '%" + searchBoxTab.Text + "%'OR TabName5 Like '%" + searchBoxTab.Text + "%'OR TabName6 Like '%" + searchBoxTab.Text + "%')");
        if (dr.Length > 0)
            this.DataSource = dr.CopyToDataTable();
        else
            this.DataSource = new DataTable();
        BindGrid();
    }

    protected void locksearchtab_Click(object sender, EventArgs e)
    {
        string name = searchBoxTab.Text;
        General_Class.UpdateLockSearchTab(name, Sessions.UserId);
        GetData();
        BindGrid();
    }
}