﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ManageDocument.aspx.cs" Inherits="Office_ManageDocument" %>


<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register Src="~/Office/UserControls/ucAddToTab.ascx" TagPrefix="uc1" TagName="ucAddToTab" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register Src="../Controls/DocFind.ascx" TagName="DocFind" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css?v=1" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=2" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/edFiles/EdFilesLogs.js?v=4" type="text/javascript"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>extras/jquery-ui-1.8.22.custom.min.js"></script>

    <style>
        .txtIndex {
            width: 85%;
            height: 25px;
            padding-left: 20px;
        }

        .txtKey {
            margin-bottom: 5px;
        }

        #btnCheckedAll {
            float: right;
            margin-right: 3px;
            margin-top: 9px;
            height: 18px;
            width: 18px;
        }

        .btnIndex {
            height: 35px;
            width: 35px;
            margin-top: 28px;
        }

        .indexHeader {
            text-align: center;
            color: #83af33;
            font-size: 25px;
        }

        .docIndex {
            height: 93%;
            border: solid 1px lightgray;
            padding: 25px;
        }

        .btnAddtoTab {
            padding: 8px 10px 8px 35px;
            font-size: 15px;
            padding-right: 10px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 5px;
            background-image: url(../images/plus-icon.png);
            background-position: 7px center;
            background-repeat: no-repeat;
        }

        #btnSpltBtn {
            float: right;
            margin-bottom: 10px;
        }

        .lblRedact {
            font-size: 20px;
            right: 0px;
            margin-left: 15%;
            line-height: 35px;
            color: black;
        }

        .redact-close {
            background: url(../../images/icon-close.png) no-repeat right bottom;
            float: right;
            padding: 10px;
            cursor: pointer;
        }

        .selectalltab {
            font-size: 18px;
            float: right;
            padding: 5px;
            font-family: 'Open Sans', sans-serif !important;
            color: #fff;
            font-weight: 700;
            margin-right: 5px;
        }

        .leftPreview {
            height: 100%;
            float: left;
            width: 75%;
        }

        .rightPreview {
            height: 100%;
            float: right;
            width: 25%;
        }

        .preview-popup {
            width: 100%;
            max-width: 1400px;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 10%;
            right: 0;
            margin: auto;
            background: #fff;
            height: 85%;
        }

        .btnRedact {
            padding: 8px;
            font-size: 15px;
            padding-right: 10px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 10px;
        }

        .btnSplitShare {
            padding: 8px 10px 8px 42px;
            font-size: 15px;
            padding-right: 10px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 5px;
            background-image: url(../images/ic-mail.png);
            background-position: 7px center;
            background-repeat: no-repeat;
        }

        #tblIndexPreview {
            margin-top: 10px;
        }

        .tdIndex {
            padding-top: 10px;
        }

        .selecttabcheck {
            height: 37px;
            background-color: #83af33;
            float: left;
            display: inline-block;
            margin-right: 10px;
            max-width: 200px;
            width: auto;
        }

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }

        #popupclose {
            float: right;
            padding: 10px;
            cursor: pointer;
        }

        .popupcontent {
            padding: 10px;
        }

        .left-content {
            float: left;
            width: 920px;
        }

        .dropDownClass {
            font-size: 20px;
            margin-top: 15px;
            font-weight: bold;
        }

        .dropDownSelect {
            background: rgba(0, 0, 0, 0) url("../images/select-icon.png") no-repeat scroll 40px 10px;
            color: #414a54;
            font-family: open sans;
            font-size: 19px;
            font-weight: 600;
            height: 33px;
            padding: 0 6px;
            width: 65px;
            -webkit-appearance: none;
        }

        .btnPurge {
            /*margin-top: 10px;*/
            margin-right: 5px;
            float: right;
        }

        .btnPurgeLeft {
            margin-top: 10px;
            float: left;
        }

        .btnCopyFile {
            padding: 7px 10px 8px 35px;
            font-size: 15px;
            padding-right: 10px;
            width: auto;
            height: 30px;
            font-weight: normal;
            line-height: normal;
            margin-right: 5px;
            background-image: url(../images/plus-icon.png);
            background-position: 7px center;
            background-repeat: no-repeat;
        }
    </style>

    <script language="javascript" type="text/javascript">
        var deleteIndexData = "";
        var deleteIndexPreviewData = "";
        function OnShowShare(documentid) {
            $('#<%= DocumentIDs.ClientID %>').val(documentid);
            $('#dialog-share-book').dialog("open");
            $("#pdfLbl").hide();
            $('#<%= pdfName.ClientID %>').hide();
            $('#<%= email.ClientID %>').val('<%=Sessions.SwitchedEmailId%>');
            $('#<%= emailto.ClientID %>').val('');
            $('#<%= nameto.ClientID %>').val('');
            <%--$('#<%= title.ClientID %>').val('');--%>
            $('#<%= pdfName.ClientID %>').val('');
            $('#<%= content.ClientID %>').val('');
            $('#dialog-share-book input[name="linkexpiration"]').val('<%= Sessions.ExpirationTime %>');
            $('#<%= emailto.ClientID %>').css('border', '1px solid black');
            return false;
        }

        function SaveMail() {
            var email = $('#<%= email.ClientID %>').val();
            var emailsto = $('#<%= emailto.ClientID %>').val();
            var nameto = $('#<%= nameto.ClientID %>').val();
            var pdfName = $('#<%= pdfName.ClientID %>').val();
            var title = $('#<%= title.ClientID %>').val();
            var emailList = "";
            if (emailsto.length > 0) {
                emailList = emailsto.split(';');
                for (var i = 0; i < emailList.length; i++) {
                    var test = "";
                    if (!CheckEmailValidation(emailList[i])) {
                        alert("Please enter valid Email separated by ';' !")
                        $('#<%= emailto.ClientID %>').css('border', '1px solid red');
                        hideLoader();
                        return false;
                    }
                }
            }
            $('#<%= emailto.ClientID %>').css('border', '1px solid black');
            var content = $('#<%= content.ClientID %>').val();
            var Isvalid = false;

            if (emailsto == "") {
                CheckValidation($('#<%= emailto.ClientID %>'));
                CheckEmailValidation($('#<%= emailto.ClientID %>'))
                return false;
            }

            var nameToList = "";
            if (nameto.length > 0) {
                var isValid = /^[a-zA-Z\;\s]+$/.test(nameto)
                if (isValid)
                    nameToList = nameto.split(';');
                else {
                    alert("Please enter valid ToName separated by ';' !")
                    $('#<%= nameto.ClientID %>').css('border', '1px solid red');
                    hideLoader();
                    return false;
                }
            }
            if (emailList.length != nameToList.length) {
                alert("Number of Emails should match with number of ToNames!");
                $('#<%= emailto.ClientID %>').css('border', '1px solid red');
                $('#<%= nameto.ClientID %>').css('border', '1px solid red');
                hideLoader();
                return false;
            }

            var emailTo = [];

            for (var i = 0; i < emailList.length; i++) {
                emailTo.push({ "Email": emailList[i], "ToName": nameToList[i] });
            }
            var src = '';
            showLoader();
            $('#floatingCirclesG').show();
            if ($("#pdfLbl").is(":visible")) {
                if (pdfName == "" || pdfName == undefined) {
                    alert("Please enter new document name");
                    $("#pdfName").focus();
                    hideLoader();
                    return false;
                }
                else if (!/^[a-z\d][a-z\d\()_\-\s]+$/i.test(pdfName)) {
                    alert("Document name can not contain special characters");
                    $("#pdfName").focus();
                    hideLoader();
                    return false;
                }
                $('#dialog-share-book').dialog("close");
                $("#<%= hdnNewPdfName.ClientID %>").val(pdfName);
                $("#<%= hdnFromEmail.ClientID %>").val(email);
                $("#<%= hdnToEmail.ClientID %>").val(JSON.stringify(emailTo));
                $("#<%= hdnToName.ClientID %>").val(nameto);
                $("#<%= hdnMailContent.ClientID %>").val(content);
                $("#<%= hdnMailTitle.ClientID %>").val(title);
                $("#<%= hdnSplitBtn.ClientID %>").click();
            }
            else {
                $.ajax(
                    {
                        type: "POST",
                        url: '../EFileFolderJSONService.asmx/ShareMail',
                        data: "{ strSendMail:'" + email + "', strToMail:'" + JSON.stringify(emailTo) + "', documentIds:'" + $('#<%= DocumentIDs.ClientID %>').val() + "', strMailInfor:'" + content + "',SendName:'" + "" + "', ToName:'" + nameto + "',MailTitle:'" + title + "',Path:'" + src + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var id = data.d;
                            var title, message;
                            if (id === false) {
                                title = "Sharing EdFile document error";
                                message = "This document has not been shared successfully."
                            }
                            else {
                                title = "Sharing EdFile document success";
                                message = "This document has been shared successfully."
                            }

                            $('#dialog-message-all').dialog({
                                title: title,
                                open: function () {
                                    $("#spanText").text(message);
                                }
                            });

                            $('#dialog-message-all').dialog("open");

                            if (id > 0)
                                $('#dialog-share-book').dialog("close");
                            hideLoader();
                        },
                        fail: function (data) {

                            bookshelf.loaded.apply();

                            $('#dialog-message-all').dialog({
                                title: "Sharing EdFile document error",
                                open: function () {
                                    $("#spanText").text("This document has not been shared successfully.");
                                }
                            });
                            $('#dialog-message-all').dialog("open");

                            hideLoader();
                        }
                    });
            }
        }

        function CheckValidation(fieldObj) {
            if (fieldObj.val() == "") {
                fieldObj.css('border', '1px solid red');
            }
            else {
                fieldObj.css('border', '1px solid black');
            }
        }

        function CheckEmailValidation(value) {

            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)) {
                return true;
            }
            else {
                return false;
            }
        }


        function OpenModalPopUp() {
            if (GetSelectedFiles()) {
                $find('myMPE').show();
            }
            return false;
        }

        var checkArray = [];

        function openwindow(id) {
            var url = 'PDFDownloader.aspx?ID=' + id;
            var name = 'DownPDF';
            var iWidth = 250;
            var iHeight = 200;
            var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
            var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
            window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',toolbar=no,menubar=no,scrollbars=no,resizeable=no,location=no,status=no');
        }

        function disableScrollbar() {
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }

        function enableScrollbar() {
            $('html, body').css({
                'overflow': 'auto',
                'height': 'auto'
            });
        }

        function showLoaderOnPurging() {
            $find('myMPE').hide();
            showLoader();
        }

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');;
        }

        function showLoaderOnPurging() {
            $find('myMPE').hide();
            showLoader();
        }

        function GetSelectedFiles() {
            ids = "";
            if ($('.checkSingle:checked').length > 0) {
                $('.checkSingle:checked').each(function () {
                    if ($(this).parent().find("input[type='hidden'][id$=hdnDocId]") && $(this).closest('tr').parent().find("input[type='hidden'][id$=hdnDocId]").val())
                        ids += $(this).parent().find("input[type='hidden'][id$=hdnDocId]").val() + ",";
                });
                ids = ids.substring(0, ids.length - 1);
                $('#<%= DocumentIDs.ClientID %>').attr("value", ids); // set hidden field value
            }
            else {
                alert("Please select at least one document.");
                return false;
            }
            return true;
        }

        function pageLoad() {
            if (LoadFolders) {
                LoadFolders();
            }
        }

        $('#copyFileClose, #btnCancelAddToTabCopyFile').click(function (e) {
            e.preventDefault();
            $(this).closest('.popup-mainbox').hide();
            $('#overlay').hide();
            enableScrollbar();
        });

        $(document).on('click', '#<%= btnSelectAddToTab.ClientID%>', function (e) {
            var selectedFolder = $("#combobox").chosen().val();
            var selectedDivider = $("#comboboxDivider").chosen().val();
            if (selectedFolder == "0" && selectedDivider == "0") {
                alert("Please select at least one EdFile & one Tab");
                e.preventDefault();
                return false;
            }
            else if (selectedFolder == "0") {
                alert("Please select at least one EdFile");
                e.preventDefault();
                return false;
            }
            else if (selectedDivider == "0") {
                alert("Please select at least one Tab");
                e.preventDefault();
                return false;
            }
            var folderValue = $('.chosen-single').text();
            if (selectedFolder == undefined || selectedFolder == null || selectedFolder.length <= 0) {
                alert('Please select folder to move request files.');
                e.preventDefault();
                return false;
            }
            $('#overlay').css('z-index', '100010');
            $('#<%= hdnSelectedFolderID.ClientID%>').val(selectedFolder);
            $('#<%= hdnSelectedDividerID.ClientID%>').val(selectedDivider);
            $('#<%= hdnSelectedFolderText.ClientID%>').val(folderValue);
            $('#AddToTab_popup,#overlay').hide();
            showLoader();
            return true;
        });

        function hideLoader() {
            $('#overlay').hide();
            enableScrollbar();
        }

        function GetSelectedFiles() {
            ids = "";
            if ($('.checkSingle:checked').length > 0) {
                $('.checkSingle:checked').each(function () {
                    if ($(this).parent().find("input[type='hidden'][id$=hdnDocId]") && $(this).closest('tr').parent().find("input[type='hidden'][id$=hdnDocId]").val())
                        ids += $(this).parent().find("input[type='hidden'][id$=hdnDocId]").val() + ",";
                });
                ids = ids.substring(0, ids.length - 1);
                $('#<%= DocumentIDs.ClientID %>').attr("value", ids); // set hidden field value
            }
            else {
                alert("Please select at least one document.");
                return false;
            }
            return true;
        }

        function pageLoad() {
            if (LoadFolders) {
                LoadFolders();
            }
        }

        $(document).ready(function () {

            $(window).scroll(function () {
                $('#overlay:visible') && $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
            });

            $('#dialog-share-book').dialog({
                autoOpen: false,
                width: 450,
                //height: 540,
                resizable: false,
                modal: true,
                buttons: {
                    "Share Now": function () {
                        SaveMail();
                    },
                    "Close": function () {
                        $(this).dialog("close");
                    }
                },
                close: function () {

                }
            });

            $('.ui-dialog-buttonset').find('button').each(function () {
                $(this).find('span').text($(this).attr('text'));
            });

            var closePopup = document.getElementById("popupclose");
            var overlay = document.getElementById("overlay");
            var popup = document.getElementById("preview_popup");
            var button = document.getElementById("button");


            $('#copyFileClose, #btnCancelAddToTabCopyFile').click(function (e) {
                e.preventDefault();
                $(this).closest('.popup-mainbox').hide();
                $('#overlay').hide();
                enableScrollbar();
            });

            $('#<%= btnCopyFile.ClientID%>').click(function (e) {
                e.preventDefault();
                if (GetSelectedFiles()) {
                    $('#AddToTab_popup,#overlay').show().focus();
                    $("#floatingCirclesG").hide();
                    $('#combobox').trigger('chosen:open');
                    e.stopPropagation();
                    disableScrollbar();
                }
            });

            $("#btnIndexPreviewingSave").click(function (e) {
                var arrIndex = [];
                for (i = 0; i < $('table#tblIndexPreview tr').length; i++) {
                    var $item = $($('table#tblIndexPreview tr')[i]);
                    if (($item.find('#txtIndexKey').val() != null && $item.find('#txtIndexKey').val() != ""))
                        arrIndex.push({ IndexKey: $item.find('#txtIndexKey').val(), IndexValue: $item.find('#txtIndexValue').val(), IndexId: $($item).find('input.hdnIndexId').val() });
                    else
                        deleteIndexData = deleteIndexData + $($item).find('input.hdnIndexId').val() + ",";
                }
                var folderId = $('#' + '<%=hdnFolderId.ClientID%>').val();
                var dividerId = $('#' + '<%=hdnDividerId.ClientID%>').val();
                var documentId = $('#' + '<%=hdnDocId.ClientID%>').val();
                var test = JSON.stringify(arrIndex);
                deleteIndexData = deleteIndexData.substr(0, deleteIndexData.lastIndexOf(','));
                var data = { "folderId": folderId, "dividerId": dividerId, "documentId": documentId, "indexData": test, "deleteIndexData": deleteIndexData };
                $.ajax({
                    type: "POST",
                    url: "FileFolderBox.aspx/SaveIndexValues",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        deleteIndexData = "";
                        if (result.d == true) {
                            alert("Data has been saved successfully.");
                        }
                        else
                            alert("Data has not been saved. Please try again later.");

                    }
                });
            });

            $(document).on('click', '#<%= btnSelectAddToTab.ClientID%>', function (e) {
                var selectedFolder = $("#combobox").chosen().val();
                var selectedDivider = $("#comboboxDivider").chosen().val();
                if (selectedFolder == "0" && selectedDivider == "0") {
                    alert("Please select at least one EdFile & one Tab");
                    e.preventDefault();
                    return false;
                }
                else if (selectedFolder == "0") {
                    alert("Please select at least one EdFile");
                    e.preventDefault();
                    return false;
                }
                else if (selectedDivider == "0") {
                    alert("Please select at least one Tab");
                    e.preventDefault();
                    return false;
                }
                var folderValue = $('.chosen-single').text();
                if (selectedFolder == undefined || selectedFolder == null || selectedFolder.length <= 0) {
                    alert('Please select folder to move request files.');
                    e.preventDefault();
                    return false;
                }
                $('#overlay').css('z-index', '100010');
                $('#<%= hdnSelectedFolderID.ClientID%>').val(selectedFolder);
                $('#<%= hdnSelectedDividerID.ClientID%>').val(selectedDivider);
                $('#<%= hdnSelectedFolderText.ClientID%>').val(folderValue);
                $('#AddToTab_popup,#overlay').hide();
                showLoader();
                return true;
            });

            // Close Popup Event
            closePopup.onclick = function () {
                overlay.style.display = 'none';
                $("#preview_popup").css("display", "none");
                $("#floatingCirclesG").css('display', 'block');
                enableScrollbar();
                $('#reviewContent').removeAttr('src');
            };

            $('input:checkbox').not('.checkAll').click(function () {
                var isChecked = $(this).prop('checked');
                $(this).prop("checked", isChecked);
                docId = $(this).siblings().last().val();
                logId = $(this).siblings().first().val();
                if (!updateArray(docId, logId, isChecked))
                    pushArray(docId, logId, isChecked);
            })

            $('.checkAll').click(function () {
                $('.checkSingle').each(function () {
                    $(this).prop('checked', $('.checkAll').prop('checked'));
                    docId = $(this).siblings().last().val();
                    logId = $(this).siblings().first().val();
                    if (!updateArray(docId, logId, $('.checkAll').prop('checked')))
                        pushArray(docId, logId, $('.checkAll').prop('checked'));
                })
            });

            $("#btnDeleteIndexPreviewCancel").click(function () {
                disableScrollbar();
                $('#DivDeleteConfirmIndexPreview').hide();
                $('#preview_popup,#overlay').show().focus();
            });

            $('#<%= btnCopyFile.ClientID%>').click(function (e) {
                e.preventDefault();
                if (GetSelectedFiles()) {
                    $('#AddToTab_popup,#overlay').show().focus();
                    $("#floatingCirclesG").hide();
                    $('#combobox').trigger('chosen:open');
                    e.stopPropagation();
                    disableScrollbar();
                }
            });

            $("#btnIndexPreviewingCancel, #closeIndexPreview").click(function (e) {
                e.preventDefault();
                $('#overlay').hide();
                $('#preview_popup').hide().focus();
                $("#floatingCirclesG").css('display', 'none');
                $('#floatingCirclesG').addClass('hideControl');
                enableScrollbar();
                e.stopPropagation();
            });

            $("#<%= btnSaveSplitPdf.ClientID%>").click(function () {
                var fileName = $("#" + '<%= txtNewPdfName.ClientID%>').val();
                if (fileName == '' || fileName == undefined) {
                    alert("Please enter file name!");
                    return false;
                }
                var selectedFolder = $("#comboboxSplit").chosen().val();
                var selectedDivider = $("#comboboxDividerSplit").chosen().val();
                if (selectedFolder == "" || selectedFolder == undefined || selectedFolder == "0") {
                    alert("Please select folder");
                    return false;
                }
                else if (selectedDivider == "" || selectedDivider == undefined || selectedDivider == "0") {
                    alert("Please select divider");
                    return false;
                }
                var folderValue = $('.chosen-single').text();
                $('#<%= hdnSelectedFolderID.ClientID%>').val(selectedFolder);
                $('#<%= hdnSelectedDividerID.ClientID%>').val(selectedDivider);
                $('#<%= hdnSelectedFolderText.ClientID%>').val(folderValue);

                return true;
            });

            $('#popupclose_split, #btnCancelAddToTabSplit').click(function (e) {
                e.preventDefault();
                $(this).closest('.popup-mainbox').hide();
                $('#overlay').hide();
                enableScrollbar();
            });

            $("#comboboxSplit").change(function (event) {
                ChangeDivider();
            });
        });

        function PurgingNotAllowed() {
            alert('For security, logs cannot be purged. Please contact EdFiles if you would to purge logs !!');
            return false;
        }

        function CheckboxValidate() {
            if ($('input[type=checkbox]:checked').length > 0) {
                showLoader();
                $('#<%= checkBoxData.ClientID %>').val(JSON.stringify(checkArray))
                return true;
            }
            else {
                alert("Please select log to purge !!");
                return false;
            }
        }

        function updateArray(docId, logId, isChecked) {
            for (var i = 0; i < checkArray.length; i++) {
                if (checkArray[i].DocumentId == docId && checkArray[i].LogId == logId) {
                    checkArray[i].Checked = isChecked;
                    return true;
                }
            }
            return false;
        }

        function ShowConfirm(message) {
            if (confirm(message)) {
                window.location.href = window.location.href;
            }

            else {
                window.location.href = window.location.origin + '/Office/FileFolderBox.aspx?id=' + $('#<%= hdnSelectedFolderID.ClientID%>').val();
            }
        }

        function pushArray(docId, logId, isChecked) {
            checkArray.push({ 'DocumentId': docId, 'LogId': logId, 'Checked': isChecked });
        }

        function showPreview(ele) {
            showLoader();
            var id = JSON.parse($(ele).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId;
            var fileExtension = $(ele).closest('tr').find("input[type='hidden'][id$=hdnFileExtension]").val();
            if (fileExtension == 14) {
                window.open("PDFDownloader.aspx?ID=" + id, "_blank");
                hideLoader();
            }
            else {
                var data = { "documentId": JSON.parse($(ele).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId }
                $('#<%= hdnFolderId.ClientID%>').val($(ele).closest('tr').find("input[type='hidden'][id$=hdnFolderId]").val());
                $('#<%= hdnDividerId.ClientID%>').val($(ele).closest('tr').find("input[type='hidden'][id$=hdnDividerId]").val());
                $('#<%= hdnDocId.ClientID%>').val(JSON.parse($(ele).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId);
                $.ajax({
                    type: "POST",
                    url: "./FileFolderBox.aspx/GetIndexValues",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result.d.length > 0) {
                            var $this = $(ele);
                            var path = '/' + $($this).parent().find('[name*=hdnPath]').val();
                            path = path.replace(/\\/g, "/");
                            var newpath = $this.parent().find('[name*=hdnPath]').val();
                            $('#<%= hdnPath.ClientID %>').val(newpath);
                            var docId = JSON.parse($(ele).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId;
                            $("#<%= hdnFileId.ClientID %>").val(docId);
                            var fileName = $(ele)[0].innerHTML + ".pdf";
                            $('#<%= hdnFileName.ClientID %>').val(fileName);
                            if (!window.location.origin) {
                                window.location.origin = window.location.protocol + "//"
                                    + window.location.hostname
                                    + (window.location.port ? ':' + window.location.port : '');
                            }
                            var src = './Preview.aspx?data=' + window.location.origin + path + "?v=" + "<%= DateTime.Now.Ticks%>";
                            if (window.location.origin.indexOf('https://') > -1)
                                src = './Preview.aspx?data=' + window.location.origin + path + "?v=" + "<%= DateTime.Now.Ticks%>";
                            var popup = document.getElementById("preview_popup");
                            overlay.style.display = 'block';
                            popup.style.display = 'block';
                            $('#reviewContent').attr('src', src);
                            $('#preview_popup').focus();
                            disableScrollbar();
                            var index = 0;
                            var arrIndex = [];
                            $.each($.parseJSON(result.d), function (i, e) {
                                arrIndex.push(e);
                            });
                            $("#tblIndexPreview").find("tr:gt(0)").remove();
                            var row = $(".indexPreviewRow");
                            $(row).find("#txtIndexKey").val("");
                            $(row).find("#txtIndexValue").val("");
                            $(row).find("#btnAddIndex").show();
                            $(row).find("#btnRemoveIndex").hide();
                            if (arrIndex.length > 0)
                                $(row).find(".hdnIndexId").val(arrIndex[0].IndexId);
                            else
                                $(row).find(".hdnIndexId").val(0);
                            for (var i = 0; i < arrIndex.length; i++) {
                                if (i == 0) {
                                    SetIndexPreviewingValues(arrIndex[i], true);
                                }
                                else
                                    AppendIndexPreviewingDetails(arrIndex[i], i);
                            }
                            return false;
                        }
                    }
                });
            }
        };

        function RedirectToFolderPage(ele) {
            folderid = $(ele).parent().find('[name*=hdnFolderIDs]').val();
            CreateDocumentsAndFolderLogs(folderid, null, null, null, null, ActionEnum.View, null);
            if (folderid != undefined && folderid != 0) { ClearFolderIdSource(); window.location = window.location.origin + "/Office/FileFolderBox.aspx?id=" + folderid; }
        }

        function ClearFolderIdSource() {
            $.ajax(
                {
                    type: "POST",
                    url: '../EFileFolderJSONService.asmx/ClearFolderIdSource',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false
                });
        }

        function DisableWhileRedacting(elem) {
            $('.selecttabcheck').hide();
            $('.btnSplitShare').hide();
            $('.btnRedactShare').show();
            $('.btnStartRedact').hide();
            $('.btnStopRedact').show();
            $('.lblRedact').show();
            $("#btnSpltBtn").hide();
            $(elem.contentWindow.document).children().find('#pageRotateCw').css("cursor", "not-allowed").attr("disabled", "disabled");
            $(elem.contentWindow.document).children().find('#pageRotateCcw').css("cursor", "not-allowed").attr("disabled", "disabled");
        }

        function EnableAfterRedacting() {
            $('.selecttabcheck').show();
            $('.btnSplitShare').show();
            $('.btnStartRedact').show();
            $('.btnStopRedact').hide();
            $('.btnRedactShare').hide();
            $('.lblRedact').hide();
            $("#btnSpltBtn").show();
            var iframe = document.getElementById("reviewContent");
            $(iframe.contentWindow.document).children().find('.page').unbind("click");
            $(iframe.contentWindow.document).children().find('#pageRotateCw').css("cursor", "default").removeAttr("disabled");
            $(iframe.contentWindow.document).children().find('#pageRotateCcw').css("cursor", "default").removeAttr("disabled");
            $(iframe.contentWindow.document).children().find('.mydiv').remove();
        }

        function startRedacting() {
            var iframe = document.getElementById("reviewContent");
            DisableWhileRedacting(iframe);
            cords = [];
            $(iframe.contentWindow.document).find("#scaleSelect").change(function () {
                var scaleValue = $(this).val();
                if (scaleValue == 'auto' || scaleValue == 'page-fit' || scaleValue == 'page-width') {
                    scaleValue = 0.68;
                }
                else if (scaleValue == 'page-actual') {
                    scaleValue = 1;
                }

                var divs = $(this).parents().find("#mainContainer").find('.mydiv');
                var pageWidth = parseInt($(this).parents().find("#mainContainer").find('.page').first().css("width").split('px')[0]);
                var viewerWidth = parseInt($(this).parents().find("#mainContainer").find('#viewer').first().css("width").split('px')[0]);
                var diff = '';
                if (pageWidth < viewerWidth) {
                    diff = (viewerWidth - pageWidth) / 2;
                }
                for (var i = 0; i < divs.length; i++) {
                    var height = parseInt($(divs[i]).css("height").split('px')[0]);
                    var width = parseInt($(divs[i]).css("width").split('px')[0]);
                    var prevScale, prevDiff;
                    for (var j = 0; j < cords.length; j++) {
                        if (cords[j].id == $(divs[i]).attr("id")) {
                            prevScale = cords[j].prevScale;
                            prevDiff = cords[j].prevDiff;
                            cords[j].prevDiff = diff;
                            cords[j].prevScale = scaleValue;
                        }
                    }
                    height = (height / prevScale) * scaleValue;
                    width = (width / prevScale) * scaleValue;
                    $(divs[i]).css("height", height + "px");
                    $(divs[i]).css("width", width + "px");
                    var left = parseInt($(divs[i]).css("left").split('px')[0]);
                    var top = parseInt($(divs[i]).css("top").split('px')[0]);
                    //var prevDiff = cords.find(f => f.id == $(divs[i]).attr("id")).prevDiff;
                    if (prevDiff != '') {
                        if (prevDiff > left)
                            left = prevDiff - left;
                        else
                            left = left - prevDiff;
                    }
                    if (prevScale != '') {
                        left = left / prevScale;
                        top = top / prevScale;
                    }
                    left = scaleValue * left;
                    top = scaleValue * top;
                    left = left + diff;
                    //cords.find(f => f.id == $(divs[i]).attr("id")).prevDiff = diff;
                    //cords.find(f => f.id == $(divs[i]).attr("id")).prevScale = scaleValue;
                    $(divs[i]).css("left", left + "px");
                    $(divs[i]).css("top", top + "px");
                }
            });
            $(iframe.contentWindow.document).children().find('.page').click(function (e) {
                var pageIndex = $(this).attr("data-page-number");
                var scaleValue = $(this).parent().parent().parent().find("#scaleSelect").val();
                var divHeight = '25';
                var divWidth = '160';
                if (scaleValue == 'auto' || scaleValue == 'page-fit' || scaleValue == 'page-width') {
                    scaleValue = 0.68;
                }
                else if (scaleValue == 'page-actual') {
                    scaleValue = 1;
                }
                divHeight = scaleValue * divHeight;
                divWidth = scaleValue * divWidth;
                var top = e.offsetY;
                var left = e.offsetX;
                if (e.target.className != 'textLayer') {
                    top = parseInt($(e.target).css("top").split('px')[0]) + e.offsetY;
                    left = parseInt($(e.target).css("left").split('px')[0]) + e.offsetX;
                }
                if (pageIndex > 1) {
                    top = ((pageIndex - 1) * parseInt($(this).css("height").split('px')[0])) + top + ((pageIndex - 1) * 10);
                }
                var thisWidth = parseInt($(this).css("width").split('px')[0]);
                var parentWidth = parseInt($(this).parent().css("width").split('px')[0]);
                var diff = '';
                if (thisWidth < parentWidth) { diff = ((parentWidth - thisWidth) / 2); }
                left = left + diff;
                cords.push({ top: top, left: left, id: 'redactDiv' + redactIndex, height: divHeight, width: divWidth, prevDiff: diff, prevScale: scaleValue, pageNumber: pageIndex, pageWidth: 0, pageHeight: 0 });
                var redDiv = '<div class="mydiv" id="redactDiv' + redactIndex + '" style="background-color:black;position:absolute;resize:auto;overflow:auto;border:solid 1px black;z-index:1;height:' + divHeight + 'px;width:' + divWidth + 'px;top:' + top + 'px;left:' + left + 'px">' +
                    '<span onclick="removeRedactDiv(this);" id="redactClose' + redactIndex + '" class="ic-icon redact-close" style="background: url(../../images/icon-close.png) no-repeat right top;float: left;padding: 6px;cursor: pointer;background-size: 12px;"></span></div>';
                $(redDiv).insertAfter($(this));
                redactIndex++;
            });
        }

        function stopRedacting() {
            if (confirm("Are you sure want to discard the changes ?")) {
                EnableAfterRedacting();
            }
        }

        function OnSplitShare() {
            if (GetSplitPdfValue()) {
                enableScrollbar();
                $("#overlay").hide();
                var popup = document.getElementById("preview_popup");
                popup.style.display = 'none';
                $("#pdfLbl").show();
                $('#<%= pdfName.ClientID %>').show();
                $('#dialog-share-book').dialog("open");
                $('#<%= email.ClientID %>').val('<%=Sessions.SwitchedEmailId%>');
                $('#<%= emailto.ClientID %>').val('');
                $('#<%= nameto.ClientID %>').val('');
                $('#<%= title.ClientID %>').val('');
                $('#<%= pdfName.ClientID %>').val('');
                $('#<%= content.ClientID %>').val('');
                $('#dialog-share-book input[name="linkexpiration"]').val('<%= Sessions.ExpirationTime %>');
                $('#<%= emailto.ClientID %>').css('border', '1px solid black');
            }
        }

        function OnRedacting() {
            var iframe = document.getElementById("reviewContent");
            var divs = $(iframe.contentWindow.document).children().find('.mydiv');
            if (divs.length > 0) {
                EnableAfterRedacting();
                enableScrollbar();
                $("#overlay").hide();
                var popup = document.getElementById("preview_popup");
                popup.style.display = 'none';
                $("#pdfLbl").show();
                $('#<%= pdfName.ClientID %>').show();
                $('#dialog-share-book').dialog("open");
                $('#<%= email.ClientID %>').val('<%=Sessions.SwitchedEmailId%>');
                $('#<%= emailto.ClientID %>').val('');
                $('#<%= nameto.ClientID %>').val('');
                $('#<%= title.ClientID %>').val('');
                $('#<%= pdfName.ClientID %>').val('');
                $('#<%= content.ClientID %>').val('');
                $('#dialog-share-book input[name="linkexpiration"]').val('<%= Sessions.ExpirationTime %>');
                $('#<%= emailto.ClientID %>').css('border', '1px solid black');
                var pageHeight = $(iframe.contentWindow.document).children().find('.page').css("height").split('px')[0];
                var pageWidth = $(iframe.contentWindow.document).children().find('.page').css("width").split('px')[0];
                for (var i = 0; i < divs.length; i++) {
                    var height = $(divs[i]).css("height").split('px')[0];
                    var width = $(divs[i]).css("width").split('px')[0];
                    for (var j = 0; j < cords.length; j++) {
                        if (cords[j].id == $(divs[i]).attr("id")) {
                            cords[j].height = height;
                            cords[j].width = width;
                            cords[j].pageHeight = pageHeight;
                            cords[j].pageWidth = pageWidth;
                        }
                    }
                }
                $("#<%= hdnIsRedaction.ClientID %>").val(true);
                $('#<%= hdnCords.ClientID %>').val(JSON.stringify(cords));
            }
            else {
                alert("Please add redaction !!!");
            }
        }

        function AppendIndexPreviewingDetails(indexPair, index) {
            CreateDynamicIndexPreviewingTextBox(indexPair.IndexId, true);
            SetIndexPreviewingValues(indexPair, false);
        }

        function SetIndexPreviewingValues(indexPair, isRender) {
            var row = "";
            if (!isRender)
                row = document.getElementById("indexPreviewRow" + indexPair.IndexId);
            else
                row = $(".indexPreviewRow");
            $(row).find("#txtIndexKey").val(indexPair.IndexKey);
            $(row).find("#txtIndexValue").val(indexPair.IndexValue);
            $(row).find(".hdnIndexId").val(indexPair.IndexId);
        }

        function GetDynamicPreviewingTextBox(value) {
            var test = '<td><input type="hidden" value ="0" class="hdnIndexId"></td>' +
                '<td class="tdIndex"><input type="text" id="txtIndexKey" class="txtIndex txtKey" maxlength="20" placeholder="Key" /><input type="text" id="txtIndexValue" class="txtIndex" maxlength="20" placeholder="Value" /></td></td>' +
                        '<td><button id="btnAddIndex" class="btnIndex" type="button" onclick="return AddIndexPreviewingTextBoxes(this)"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/add-icon.png" title="Add"/> </button> </td>' +
                                    '<td><button id="btnRemoveIndex" class="btnIndex" type="button" onclick="return DisplayConfirmBox(this, true)"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" /> </button>'

            return test;
        }

        function AddIndexPreviewingTextBoxes(ele) {
            var index = parseInt($($(ele).parent().parent().find('[class*=hdnIndexId]')).val());
            if ($(ele).closest('tr').find('#txtIndexKey').val() == "") {
                alert("Please enter Key Value.");
                return false;
            }
            CreateDynamicIndexPreviewingTextBox(index, false);
            return false;
        }

        function CreateDynamicIndexPreviewingTextBox(index, isRender) {
            var tr = document.createElement('tr');
            tr.innerHTML = GetDynamicPreviewingTextBox(index);

            if (!isRender)
                tr.setAttribute("id", "indexPreviewRow" + (index = index + 1));
            else
                tr.setAttribute("id", "indexPreviewRow" + index);
            // var body = $(parentElement).parent();
            $('table#tblIndexPreview tr:last').after(tr);
            SetIndexPreviewingButtonVisibility();

            //Allow maximum of 5 key,value pairs
            if ($('table#tblIndexPreview tr').length == 5) {
                $(tr).find('#btnAddIndex').hide();
            }
        }

        function RemoveIndexPreviewTextBoxes() {
            $('#DivDeleteConfirmIndexPreview').hide();
            $('#preview_popup,#overlay').show().focus();
            if ($(indexRemoveEle).closest('tr').find('input.hdnIndexId').val() != 0) {
                var hdnId = $(indexRemoveEle).closest('tr').find('input.hdnIndexId').val();
                var test = deleteIndexData.indexOf(hdnId);
                if (test <= 0)
                    deleteIndexData = deleteIndexData + hdnId + ",";
            }
            $(indexRemoveEle).closest('tr').remove();
            SetIndexPreviewingButtonVisibility();
            indexRemoveEle = "";
            disableScrollbar();
            return false;
        }

        function SetIndexPreviewingButtonVisibility() {
            var rowCount = $('table#tblIndexPreview tr').length;
            for (i = 0; i < rowCount; i++) {
                var $item = $($('table#tblIndexPreview tr')[i]);

                if (i == (rowCount - 1)) {
                    $item.find('#btnRemoveIndex').show();
                    $item.find('#btnAddIndex').show();
                    if (i == 0) {
                        $item.find('#btnRemoveIndex').hide();
                    }
                }
                else if (i == 0) {
                    $item.find('#btnRemoveIndex').show();
                    $item.find('#btnAddIndex').hide();
                }
                else {
                    $item.find('#btnRemoveIndex').show();
                    $item.find('#btnAddIndex').hide();
                }
            }
        }

        function ShowAddToTabPopup() {
            if (GetSplitPdfValue()) {
                $.ajax(
                    {
                        type: "POST",
                        url: '../FlexWebService.asmx/GetFoldersData',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $('<option />', { value: 0, text: "Select a file" }).appendTo($("#comboboxSplit")); $.each(data.d, function (i, text) {
                                $('<option />', { value: i, text: text }).appendTo($("#comboboxSplit"));
                            });
                            $("#comboboxSplit").chosen({ width: "315px" });

                            ChangeDivider();
                        },
                        fail: function (data) {
                        }
                    });
                var popup = document.getElementById("preview_popup");
                popup.style.display = 'none';

                var popup = document.getElementById("preview_popup_split");
                popup.style.display = 'block';
            }
        }

        function GetSplitPdfValue() {
            var pageNumber = '';
            if ($('iFrame').contents().find('.check').length > 0) {
                $('iFrame').contents().find('.check').each(function (k, v) {
                    var val = $(v).is(":checked");
                    if (val) {
                        if (pageNumber == '')
                            pageNumber += $(v).attr('data-value') + ',';
                        else
                            pageNumber += $(v).attr('data-value') + ',';
                    }

                });
                pageNumber = pageNumber.substring(0, pageNumber.length - 1);

                if (pageNumber.length <= 0) {
                    alert("Please select at least one page.");
                    return false;
                }

                $("#" + '<%= hdnPageNumbers.ClientID %>').val(pageNumber);
            }
            else {
                alert('Please select thumbnail');
                return false;
            }
            return true;
        }

        function ChangeDivider() {
            $("#comboboxDividerSplit").html('');
            $("#comboboxDividerSplit").chosen("destroy");
            var selectedFolder = $("#comboboxSplit").chosen().val();
            $.ajax(
                {
                    type: "POST",
                    url: '../FlexWebService.asmx/GetDividersByEFF',
                    data: "{eff:" + parseInt(selectedFolder) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $('<option />', { value: 0, text: "Select a tab" }).appendTo($("#comboboxDividerSplit"));
                        $.each(data.d, function (i, text) {
                            $('<option />', { value: i, text: text }).appendTo($("#comboboxDividerSplit"));
                        });
                        $("#comboboxDividerSplit").chosen({ width: "315px" });
                    },
                    fail: function (data) {
                    }
                });
        }
        function checkAllThumbnail() {
            var selectAllValue = $("#btnCheckedAll").is(":checked");

            $('iFrame').contents().find('.check').each(function (k, v) {
                if (selectAllValue)
                    $(v).attr('checked', true);
                else
                    $(v).attr('checked', false);
            });
        }


        var indexRemoveEle = "";
        function DisplayConfirmBox(ele, isPreview) {
            if (!isPreview) {
                $("#PanelIndex").hide();
                $('#DivDeleteConfirmIndex,#overlay').show().focus();
                indexRemoveEle = ele;
            }
            else {
                $("#preview_popup").hide();
                $('#DivDeleteConfirmIndexPreview,#overlay').show().focus();
                indexRemoveEle = ele;
            }
            //  return false;
        }
    </script>
    <div class="popup-mainbox" id="DivDeleteConfirmIndexPreview" style="display: none; position: fixed; z-index: 999999; margin-left: 30%; margin-top: 5%;">
        <div>
            <div class="popup-head" id="Div7">
                Delete Index
            </div>
            <div class="popup-scroller">
                <p>
                    Caution! Are you sure you want to delete this index?
                </p>
            </div>
            <div class="popup-btn">
                <input id="btnDeleteIndexPreviewOkay" onclick="return RemoveIndexPreviewTextBoxes()" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />
                <input id="btnDeleteIndexPreviewCancel" type="button" value="No" class="btn green" />
            </div>
        </div>
    </div>
    <div id="dialog-share-book" title="Share this EdFile" style="display: none">
        <p class="validateTips">All form fields are required.</p>
        <br />
        <label for="email">From Email</label><br />
        <input type="email" name="email" id="email" runat="server" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <label for="emailto">To Email</label><br />
        <input type="email" name="emailto" id="emailto" runat="server" placeholder="Enter Email separated with';'" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <label for="nameto">To Name</label><br />
        <input type="text" name="nameto" id="nameto" runat="server" placeholder="Enter name separated with';'" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <label for="title">Subject</label><br />
        <input type="text" name="title" id="title" runat="server" value="EdFile Share" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" readonly="readonly" /><br />
        <label for="content">Message</label><br />
        <textarea name="content" id="content" rows="2" runat="server" cols="25" class="text ui-widget-content ui-corner-all" style="border: 1px solid black"></textarea><br />
        <label for="content">Link Expiration Time (In Hrs)</label><br />
        <input type="text" name="linkexpiration" id="expirationTime" class="text ui-widget-content ui-corner-all" style="border: 1px solid black; width: 80%;" />
        <input type="button" id="updateExpiration" onclick="UpdateExpirationTime()" class="btn-small green" style="float: right" value="UPDATE" /><br />
        <label for="pdfName" style="display: none" id="pdfLbl">New PDF Name</label><br />
        <asp:TextBox runat="server" ID="pdfName" Text="" placeholder="Enter new pdf name here" AutoPostBack="false" CssClass="text ui-widget-content ui-corner-all" Style="border: 1px solid black; display: none" />
        <span id="errorMsg"></span>
    </div>

    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <div class="popup-mainbox preview-popup" id="preview_popup_split" style="display: none; height: 460px; width: 395px; max-width: 395px; max-height: 460px; margin-top: 20px;">
        <div class="popupcontrols popup-head" style="width: 100%; height: 60px;">
            Split PDF Details
        <span id="popupclose_split" class="ic-icon ic-close popupclose"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%; background: #eaeaea; border-top: 2px solid darkgray;">
            <div class="popup-scroller" style="height: 75%; background: #eaeaea;">
                <p style="margin: 20px">
                    <label>New split Pdf name</label>
                    <asp:TextBox runat="server" ID="txtNewPdfName" Style="margin-bottom: 5px;"></asp:TextBox>
                    <br />
                    <label>1. Select an edFile from list</label>
                    <select id="comboboxSplit">
                    </select>

                    <label style="display: block; margin-top: 15px">2. Select a tab for the selected edFile</label>
                    <select id="comboboxDividerSplit" style="display: none;">
                    </select>
                </p>

            </div>
            <div class="popup-btn" style="background: #eaeaea;">

                <asp:Button ID="btnSaveSplitPdf" runat="server" Text="Save" CssClass="btn green" OnClick="btnSaveSplitPdf_Click" Style="margin-right: 5px;" />
                <input id="btnCancelAddToTabSplit" type="button" value="Cancel" class="btn green" />
            </div>
        </div>
    </div>
    <div class="popup-mainbox preview-popup" id="AddToTab_popup" style="display: none; height: 460px; width: 395px; max-width: 395px; max-height: 460px; margin-top: 20px;">
        <div class="popupcontrols popup-head" style="width: 100%; height: 60px;">
            Select Folder
        <span id="copyFileClose" class="ic-icon ic-close popupclose" style="float: right; cursor: pointer"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%; background: #eaeaea; border-top: 2px solid darkgray;">
            <div class="popup-scroller" style="height: 75%; background: #eaeaea;">
                <p style="margin: 20px">
                    <uc1:ucAddToTab ID="ucAddToTab" runat="server" />
                </p>

            </div>
            <div class="popup-btn" style="background: #eaeaea;">
                <%--<input  runat="server"  type="button" value="Save"  />--%>
                <asp:Button ID="btnSelectAddToTab" Text="Save" runat="server" CssClass="btn green" OnClick="btnSelectAddToTab_Click" Style="margin-right: 5px;" />
                <input id="btnCancelAddToTabCopyFile" type="button" value="Cancel" class="btn green" />
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="upanel1" runat="server">
        <ContentTemplate>
            <asp:Button ID="hdnSplitBtn" Style="display: none" runat="server" OnClick="hdnSplitBtn_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Document Retention Logs</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnFileName" />
    <asp:HiddenField runat="server" ID="hdnFileId" />
    <asp:HiddenField runat="server" ID="hdnPageNumbers" />
    <asp:HiddenField runat="server" ID="hdnPath" />
    <asp:HiddenField ID="hdnIsRedaction" runat="server" />
    <asp:HiddenField ID="hdnCords" runat="server" />
    <asp:HiddenField ID="hdnDividerId" runat="server" />
    <asp:HiddenField ID="hdnDocId" runat="server" />
    <asp:HiddenField ID="hdnFolderId" runat="server" />
    <asp:HiddenField ID="hdnToEmail" runat="server" />
    <asp:HiddenField ID="hdnMailTitle" runat="server" />
    <asp:HiddenField ID="hdnMailContent" runat="server" />
    <asp:HiddenField ID="hdnNewPdfName" runat="server" />
    <asp:HiddenField ID="hdnToName" runat="server" />
    <asp:HiddenField ID="hdnFromEmail" runat="server" />
    <asp:HiddenField ID="hdnSelectedFolderID" runat="server" />
    <asp:HiddenField ID="hdnSelectedDividerID" runat="server" />
    <asp:HiddenField ID="hdnSelectedFolderText" runat="server" />
    <asp:HiddenField ID="DocumentIDs" runat="server" />
    <asp:HiddenField ID="txtHasPrivilage" runat="server" />
    <asp:Panel runat="server" ID="panel1">
        <div class="page-container register-container">
            <div class="inner-wrapper">
                <div class="form-containt listing-contant">
                    <div class="left-content">
                        <div class="content-box listing-view">
                            <asp:Panel ID="panelSearch" runat="server">
                                <fieldset>
                                    <label>Folder Name</label>
                                    <asp:TextBox ID="textfieldFolder" runat="server" name="textfieldFolder"
                                        size="30" MaxLength="20" />
                                </fieldset>
                                <fieldset>
                                    <label>File Name</label>
                                    <asp:TextBox ID="textfieldFile" runat="server" name="textfieldFile"
                                        size="30" MaxLength="20" />
                                </fieldset>
                                <fieldset>
                                    <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                    <asp:Button ID="Submit2" name="Submit2" runat="server" Text="Search"
                                        align="absbottom" OnClick="Submit2_Click" Style="padding: 5px" CssClass="create-btn btn-small green" />
                                </fieldset>
                            </asp:Panel>

                            <div class="dropDownClass" style="float: left" bgcolor="#f8f8f5">
                                <label>Class :</label>
                                <asp:DropDownList CssClass="dropDownSelect" runat="server" AutoPostBack="true" OnSelectedIndexChanged="dropDownClass_SelectedIndexChanged" ID="dropDownClass">
                                    <asp:ListItem>1</asp:ListItem>
                                    <asp:ListItem>2</asp:ListItem>
                                    <asp:ListItem>3</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <asp:LinkButton runat="server" Style="display: none" ID="dummyBtn"></asp:LinkButton>
                            <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server" BehaviorID="myMPE"
                                CancelControlID="ButtonDeleteCancel" TargetControlID="dummyBtn"
                                PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                            </ajaxToolkit:ModalPopupExtender>
                            <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                ConfirmText="" TargetControlID="dummyBtn">
                            </ajaxToolkit:ConfirmButtonExtender>
                            <asp:Button runat="server" ID="purgeLog" ToolTip="Purge selected log(s) and file(s)" OnClick="purgeLog_Click" CssClass="create-btn btn-small green btnPurge" Text="Purge" />
                            <asp:Button ID="btnCopyFile" Text="Tab" CssClass="btn-small green btnCopyFile" runat="server" Style="float: right" ToolTip="Transfer document(s) to another folder"></asp:Button>
                            <asp:HiddenField ID="checkBoxData" runat="server" />
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <div class="work-area">
                                        <div class="overflow-auto">
                                            <asp:GridView ID="GridView1" runat="server" OnRowDeleting="GridView1_RowDeleting"
                                                OnRowDataBound="GridView1_RowDataBound" DataKeyNames="DocumentID " AllowSorting="True"
                                                AutoGenerateColumns="false" OnSorting="GridView1_Sorting" CssClass="datatable listing-datatable">
                                                <PagerSettings Visible="False" />
                                                <AlternatingRowStyle CssClass="odd" />
                                                <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <HeaderTemplate>
                                                            <%--<asp:CheckBox CssClass="checkAll" ID="checkAll" runat="server" />--%>
                                                            <input type="checkbox" id="checkAll" class="checkAll" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <%--<asp:CheckBox runat="server" ID="checkSingle" CssClass="checkSingle" />--%>
                                                            <asp:HiddenField runat="server" ID="hdnLogId" Value='<%#Eval("AuditLogId")%>' />
                                                            <input type="checkbox" id="checkSingle" class="checkSingle" />
                                                            <asp:HiddenField runat="server" ID="hdnDocId" Value='<%#Eval("DocumentID")%>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" SortExpression="DocumentName" HeaderText="Document Name" ShowHeader="true">
                                                        <%--   <HeaderTemplate>
                                                        <asp:Label ID="DocumentName" runat="server" Text="Document Name" />
                                                    </HeaderTemplate>--%>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("DocumentName")%>' name="DocumentName" Style="cursor: pointer; text-decoration: underline; color: blue; width: 50px;" OnClick="showPreview(this);" />
                                                            <asp:HiddenField ID="hdnFileExtension" runat="server" Value='<%# Eval("FileExtention") %>' />
                                                            <asp:HiddenField ID="hdnFullFileName" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("FullFileName") + "")%>' />
                                                            <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName") + "\\" + GetFileName(Eval("DocumentName") +".pdf") %>' />
                                                            <asp:HiddenField ID="hdnDividerId" runat="server" Value='<%#  Eval("DividerID")%>' />
                                                            <asp:HiddenField ID="hdnFolderId" runat="server" Value='<%#  Eval("FolderID")%>' />
                                                            <asp:HiddenField ID="hdnValues" runat="server" Value='<%#  PrepareDocs(Convert.ToInt32(Eval("DocumentId")), Convert.ToInt32(Eval("DocumentOrder")))%>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" ItemStyle-Wrap="false" SortExpression="FolderName" HeaderText="Folder Name" ShowHeader="true">
                                                        <%--<HeaderTemplate>
                                                        <asp:Label ID="DocumentName" runat="server" Text="Document Name" />
                                                    </HeaderTemplate>--%>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFolderName" runat="server" Text='<%#Eval("FolderName")%>' name="FolderName" Style="cursor: pointer; text-decoration: underline; color: blue" OnClick="RedirectToFolderPage(this);" />
                                                            <asp:HiddenField ID="hdnFolderIDs" runat="server" Value='<%# Eval("FolderID") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--<asp:BoundField DataField="FolderName" HeaderText="Folder Name" SortExpression="FolderName"></asp:BoundField>--%>
                                                    <asp:BoundField DataField="DividerName" HeaderText="Tab Name" SortExpression="DividerName"></asp:BoundField>
                                                    <asp:BoundField DataField="Class" HeaderText="Class" SortExpression="Class"></asp:BoundField>
                                                    <asp:BoundField DataField="ModifiedBy" HeaderText="User" SortExpression="ModifiedBy"></asp:BoundField>

                                                    <asp:BoundField DataField="Date" HeaderText="Create Date" DataFormatString="{0:MM.dd.yyyy}"
                                                        SortExpression="Date" />
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="100px"
                                                        ItemStyle-CssClass="table_tekst_edit">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="Label34" Text="File" runat="server" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton21" runat="server" Text="" CausesValidation="false" Visible='<%#Eval("Flag").ToString() == "0"? true: false %>' CssClass="ic-icon ic-download"
                                                                PostBackUrl='<%#string.Format("PDFDownloader.aspx?ID={0}",Eval("DocumentID"))%>' />
                                                            <asp:LinkButton ID="lnkShare" Style="margin-top: 2px;" runat="server" Visible='<%#Eval("Flag").ToString() == "0"? true: false %>' ToolTip="Share Document" CssClass="ic-icon ic-share" OnClientClick='<%# "return OnShowShare(" + Eval("DocumentID") + ",\"" + Eval("PathName") + "\")"%>' CausesValidation="False" Height="28px" Width="28px">
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Action" HeaderText="Action" SortExpression="Action"></asp:BoundField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <table width="100%" border="0" backcolor="#F8F8F5" cellpadding="2" cellspacing="0"
                                                        style="margin: 20px 0 10px 10px;">
                                                        <tr>
                                                            <td align="Center" class="podnaslov" backcolor="#F8F8F5">
                                                                <asp:Label ID="Label1" runat="server" />
                                                                There are no documents.
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:Button runat="server" ID="purgeLogBottom" ToolTip="Purge selected log(s) and file(s)" OnClick="purgeLog_Click" CssClass="create-btn btn-small green btnPurgeLeft" Text="Purge" />
                            <div class="prevnext" style="float: right; margin-top: 10px;" bgcolor="#f8f8f5">
                                <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                    OnPageSizeChanged="WebPager1_PageSizeChanged" PageSize="25" CssClass="prevnext custom_select"
                                    PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>
                            </div>
                        </div>
                    </div>
                    <div class="right-content">
                        <%--<div class="quick-find">
                    <uc1:QuickFind ID="QuickFind1" runat="server" />
                </div>--%>
                        <div class="quick-find" style="margin-top: 15px;">
                            <div class="find-inputbox">
                                <marquee scrollamount="1" id="marqueeside" runat="server"
                                    behavior="scroll" onmouseover="this.stop()" onmouseout="this.start()" scrolldelay="20"
                                    direction="up">
                               <%=SideMessage %></marquee>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="listing-table">
                    <asp:Panel class="popup-mainbox" ID="DivDeleteConfirmation" Style="display: none"
                        runat="server">
                        <div>
                            <div class="popup-head" id="PopupHeader">
                                Delete File
                            </div>
                            <%--  <div class="popup_Titlebar" id="PopupHeader">
                                    <div class="TitlebarRight" onclick="$get('ButtonDeleteCancel').click();">
                                    </div>
                                </div>--%>
                            <div class="popup-scroller">
                                <p>
                                    Caution! Are you sure you want to delete this file. Once you delete this file,
                                                it will be deleted for good
                                </p>
                                <p>
                                    Confirm Password:
                                                <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                                </p>
                            </div>
                            <div class="popup-btn">
                                <asp:Button runat="server" ID="ButtonDelete" Style="margin-right: 5px;" Text="Yes" CssClass="btn green" OnClientClick="showLoaderOnPurging();" OnClick="ButtonDelete_Click" />
                                <input id="ButtonDeleteCancel" type="button" value="No" class="btn green" />
                            </div>
                        </div>
                    </asp:Panel>


                    <div class="popup-mainbox preview-popup" id="preview_popup" style="display: none">
                        <div class="popupcontrols" style="width: 100%; height: 5%;">
                            <span id="popupclose" class="ic-icon ic-close"></span>
                        </div>
                        <div class="popupcontent" style="width: 100%; height: 95%;">
                            <div class="leftPreview">
                                <table style="width: 100%; height: 100%">
                                    <tr style="height: 5%">
                                        <td style="padding-bottom: 10px;">
                                            <%--<input type="button" value="Redacting" class="btn green btnStartRedact btnRedact" onclick="startRedacting()" id="startRedact" />--%>
                                            <input type="button" value="Discard Changes" class="btn green btnStopRedact btnRedact" onclick="stopRedacting()" style="display: none" id="stopRedact" />
                                            <div class="selecttabcheck"><span class="selectalltab">Select All</span><input type="checkbox" id="btnCheckedAll" value="Select All" onclick="checkAllThumbnail();" /></div>
                                            <input type="button" id="btnRedact" runat="server" style="display: none;" value="Redact" onclick="OnRedacting();" class="btn green btnRedactShare btnSplitShare" title="Share Redacted File" />
                                            <input type="button" id="btnSpltShare" runat="server" value="Share" onclick="OnSplitShare()" class="btn green btnSplitShare" title="Share Splitted File" />
                                            <asp:Label ID="lblRedact" Style="display: none" CssClass="lblRedact" runat="server">Please click on any part of the pdf to redact the text</asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <iframe id="reviewContent" style="width: 100%; height: 100%;"></iframe>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="rightPreview">
                                <div style="height: 7%">
                                    <input type="button" id="btnSpltBtn" value="Tab" onclick="ShowAddToTabPopup();" class="btn green btnAddtoTab" title="Add To Tab" />
                                </div>
                                <div class="docIndex">
                                    <h2 class="indexHeader">Indexing</h2>
                                    <table width="100%" id="tblIndexPreview">
                                        <tbody>
                                            <tr id="trIndex" class="indexPreviewRow">
                                                <td>
                                                    <input type="hidden" value="0" class="hdnIndexId" /></td>
                                                <td class="tdIndex">
                                                    <input type="text" id="txtIndexKey" class="txtIndex txtKey" maxlength="20" placeholder="Key" />
                                                    <input type="text" id="txtIndexValue" class="txtIndex" maxlength="20" placeholder="Value" /></td>
                                                <td>
                                                    <button id="btnAddIndex" class="btnIndex" type="button" onclick="return AddIndexPreviewingTextBoxes(this)">
                                                        <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/add-icon.png" alt="Add" title="Add" /></button></td>
                                                <td>
                                                    <button id="btnRemoveIndex" class="btnIndex" type="button" onclick="return DisplayConfirmBox(this, true)">
                                                        <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" />
                                                    </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div class="popup-btn" style="margin-top: 50px;">
                                        <input id="btnIndexPreviewingSave" type="button" value="Save" class="btn green" style="margin-right: 5px;" />
                                        <input id="btnIndexPreviewingCancel" type="button" value="Cancel" class="btn green" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>

</asp:Content>
