﻿using Shinetech.DAL;
using Shinetech.Engines;
using Shinetech.Framework.Controls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using System.Web.Services;
using Shinetech.Utility;
using iTextSharp.text.pdf;

public partial class Office_ManageDocument : System.Web.UI.Page
{
    public string virtualDir = ConfigurationManager.AppSettings["VirtualDir"];
    //Â¬Ô¶×Ú 2015-11-01
    public static string EdFileShare = System.Configuration.ConfigurationManager.AppSettings["EdFileShare"];
    public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    public static string splitPdfTepmPath = System.Configuration.ConfigurationManager.AppSettings["SplitPdf"];
    public string newFileName = string.Empty;
    public string fullFilePath = string.Empty;
    public string KEY = string.Empty;
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Shinetech.DAL.Account user = new Shinetech.DAL.Account(Membership.GetUser().ProviderUserKey.ToString());
            txtHasPrivilage.Value = user.HasPrivilegeDelete.Value.ToString().ToUpper();
            if (Request.QueryString.Keys.Count > 0)
            {
                panelSearch.Visible = false;
                btnCopyFile.Visible = true;
                KEY = Request.QueryString["dockey"];
                GetSearchedDocs(KEY);
            }
            else
            {
                panelSearch.Visible = true;
                btnCopyFile.Visible = false;
                GetFileData();
            }
            //GetFileData();
            BindGrid();
            purgeLog.ToolTip = "Purge selected log(s)";
            purgeLogBottom.ToolTip = "Purge selected log(s)";
        }
        if (this.Page.User.IsInRole("Users") && !Sessions.HasPrevilegeToDelete)
        {
            purgeLog.OnClientClick = "return PurgingNotAllowed()";
            purgeLogBottom.OnClientClick = "return PurgingNotAllowed()";
        }
        else
        {
            if (Request.QueryString.Keys.Count > 0)
            {
                purgeLog.ToolTip = "Purge selected file(s)";
                purgeLogBottom.ToolTip = "Purge selected file(s)";
                purgeLog.OnClientClick = "return OpenModalPopUp()";
                purgeLogBottom.OnClientClick = "return OpenModalPopUp()";
            }
            else
            {
                purgeLog.OnClientClick = "return CheckboxValidate()";
                purgeLogBottom.OnClientClick = "return CheckboxValidate()";
            }
        }
        if (Sessions.HasViewPrivilegeOnly)
        {
            purgeLog.Visible = false;
            purgeLogBottom.Visible = false;
            btnCopyFile.Visible = false;
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource"] as DataTable;
        }
        set
        {
            this.Session["DataSource"] = value;
        }
    }
    public DataTable DataSourceSearch
    {
        get
        {
            return this.Session["DataSourceSearch"] as DataTable;
        }
        set
        {
            this.Session["DataSourceSearch"] = value;
        }
    }
    public DataTable BackDataSource
    {
        get
        {
            return this.Session["Back_DataSource_Class"] as DataTable;
        }
        set
        {
            this.Session["Back_DataSource_Class"] = value;
        }
    }
    public DataTable BackDataSourceForSearch
    {
        get
        {
            return this.Session["Back_DataSourceSearch_Class"] as DataTable;
        }
        set
        {
            this.Session["Back_DataSourceSearch_Class"] = value;
        }
    }
    public string SideMessage
    {
        get
        {
            return this.Session["_Side_Message"] as string;
        }

        set
        {
            this.Session["_Side_Message"] = value;
        }
    }

    private void GetData(string folderId)
    {
        this.DataSource = FileFolderManagement.GetDocumentByUID(Sessions.SwitchedSessionId);
        //this.DataSource = FileFolderManagement.GetDocumentByUID(Membership.GetUser().ProviderUserKey.ToString(), folderId);
    }

    public string GetFileName(string fileName)
    {
        string encodeName = HttpUtility.UrlPathEncode(fileName);
        return encodeName.Replace("%", "$");
    }

    private void GetFileData()
    {
        string officeID1 = Sessions.SwitchedSessionId;
        //string officeID1 = Membership.GetUser().ProviderUserKey.ToString();
        if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users"))
        {
            this.DataSource = FileFolderManagement.GetSubDocumentByUID(officeID1, textfieldFile.Text.Trim(), textfieldFolder.Text.Trim());
        }

        else
        {
            this.DataSource = FileFolderManagement.GetDocumentByUID(officeID1, textfieldFile.Text.Trim(), textfieldFolder.Text.Trim());
        }
        this.BackDataSource = this.DataSource;
    }

    private void GetSearchedDocs(string key)
    {
        string uid = Sessions.SwitchedRackspaceId;
        key = string.Join(" ", key.Split(','));
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        key = string.Join(",", key.Split(new char[0], StringSplitOptions.RemoveEmptyEntries));
        this.DataSourceSearch = FileFolderManagement.GetSearchedDocsByUID(uid, textfieldFolder.Text.Trim(), key);
        this.BackDataSourceForSearch = this.DataSourceSearch;
    }

    private void BindGrid()
    {
        if (Request.QueryString.Keys.Count > 0)
            WebPager1.DataSource = this.DataSourceSearch;
        else
            WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        if (this.DataSource != null)
        {
            if (Request.QueryString.Keys.Count > 0)
                WebPager1.DataSource = this.DataSourceSearch;
            else
                WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        if (this.DataSource != null)
        {
            if (Request.QueryString.Keys.Count > 0)
                WebPager1.DataSource = this.DataSourceSearch;
            else
                WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
    }

    protected string GetFileExtention(StreamType ext)
    {
        string extname = ".pdf";
        switch (ext)
        {
            case StreamType.PDF:
                extname = ".pdf";
                break;
            case StreamType.Word:
                extname = ".doc";
                break;
            case StreamType.Excel:
                extname = ".xls";
                break;
            case StreamType.PPT:
                extname = ".ppt";
                break;
            case StreamType.RTF:
                extname = ".rtf";
                break;
            default:
                break;
        }

        return extname;
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //todo：删除物理文件，需要把文件移动另一个文件夹，以防止被重新上传的同名文件覆盖
        //
        string password = textPassword.Text.Trim();
        string strPassword = Shinetech.Utility.PasswordGenerator.GetMD5(password);

        if (UserManagement.GetPassword(User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel4, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);

        }
        else
        {
            string strDocumentID = GridView1.DataKeys[e.RowIndex].Value.ToString();

            Document doc = new Document(int.Parse(strDocumentID));

            if (doc.IsExist)
            {
                doc.DeleteDate.Value = DateTime.Now;
                doc.SavedID.Value = doc.DividerID.Value;
                doc.SavedID1.Value = doc.FileFolderID.Value;
                doc.UID1.Value = doc.UID.Value;

                doc.DividerID.Value = 0;
                doc.FileFolderID.Value = 0;
                doc.UID.Value = Guid.Empty.ToString();


                try
                {
                    //update filefolder size
                    DataTable dtFileFolder = FileFolderManagement.GetFileFolderSizeByDocumentID(strDocumentID);


                    string strFolderSize = dtFileFolder.Rows[0]["Otherinfo2"].ToString();
                    string strFolderID = dtFileFolder.Rows[0]["FolderID"].ToString();

                    Double newFolderSize = Convert.ToDouble(strFolderSize) - doc.Size.Value;
                    FileFolderManagement.UpdateFolderSize(strFolderID, newFolderSize);

                    //Common_Tatva.AuditLogByDocId(Convert.ToInt32(strDocumentID), Membership.GetUser().ProviderUserKey.ToString(), Enum_Tatva.Action.Delete.GetHashCode());
                    General_Class.AuditLogByDocId(Convert.ToInt32(strDocumentID), Membership.GetUser().ProviderUserKey.ToString(), Enum_Tatva.Action.Delete.GetHashCode());

                    //move document to recycle bin
                    doc.Update();

                }
                catch (Exception exp)
                {
                    Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, exp);
                }
            }

            GetFileData();
            BindGrid();
        }


        //this.UpdatePanel4.Update();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            if (Sessions.HasViewPrivilegeOnly)
                e.Row.Cells[0].Visible = false;
            if ((Page.User.IsInRole("Users")) && (!Sessions.HasPrevilegeToDownload || Sessions.HasViewPrivilegeOnly))
                e.Row.Cells[7].Visible = false;
        }

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if ((Page.User.IsInRole("Users")) && Sessions.HasViewPrivilegeOnly)
                e.Row.Cells[0].Visible = false;
            if ((Page.User.IsInRole("Users")) && (!Sessions.HasPrevilegeToDownload || Sessions.HasViewPrivilegeOnly))
                e.Row.Cells[7].Visible = false;
            System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
            string action = drv.Row["Action"].ToString();
            if ((action == "2" || action == "Delete"))
            {
                LinkButton libDw = e.Row.FindControl("LinkButton21") as LinkButton;
                libDw.Visible = false;
            }


            if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users"))
            {
                System.Data.DataRowView drv1 = e.Row.DataItem as DataRowView;
                string uid = drv1.Row[6].ToString();

                if (Sessions.SwitchedSessionId.ToLower() != uid.ToLower())
                {
                    //LinkButton libDel = e.Row.FindControl("libDelete") as LinkButton;
                    //libDel.Visible = false;
                }
            }

            if (this.Page.User.IsInRole("Users") && !Sessions.HasPrevilegeToDelete)
            {
                purgeLog.OnClientClick = "return PurgingNotAllowed()";
                purgeLogBottom.OnClientClick = "return PurgingNotAllowed()";
            }
            else
            {
                purgeLog.OnClientClick = "return CheckboxValidate()";
                purgeLogBottom.OnClientClick = "return CheckboxValidate()";
            }

        }
    }

    protected void GridView1_RowDeleting_backup(object sender, GridViewDeleteEventArgs e)
    {
        //todo：删除物理文件，需要把文件移动另一个文件夹，以防止被重新上传的同名文件覆盖
        //
        string password = textPassword.Text.Trim();
        string strPassword = Shinetech.Utility.PasswordGenerator.GetMD5(password);

        if (UserManagement.GetPassword(User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel4, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);

        }
        else
        {
            string strDocumentID = GridView1.DataKeys[e.RowIndex].Value.ToString();

            Document doc = new Document(int.Parse(strDocumentID));

            string ext = ".pdf";
            StreamType extValue = StreamType.PDF;
            try
            {
                extValue = (StreamType)doc.FileExtention.Value;
                ext = this.GetFileExtention(extValue);
            }
            catch
            {
            }

            //doc.Name
            //DataTable dtDocument = FileFolderManagement.GetDocInforByDocumentID(strDocumentID);
            string strPDFName = doc.Name.Value + ext;

            string strPDFPath = "~/" + doc.PathName.Value + "/" + strPDFName;
            if (File.Exists(Server.MapPath(strPDFPath)))
            {
                try
                {
                    File.Delete(Server.MapPath(strPDFPath));
                }
                catch
                {
                }

            }

            //删除文档页 page,并找到页路径进行删除
            DataTable dtPage = FileFolderManagement.GetDocumentByDocumentID(strDocumentID);
            for (int i = 0; i < dtPage.Rows.Count; i++)
            {
                string strPageID = dtPage.Rows[i]["PageID"].ToString();

                //delete quick note，删除每页对应的QuitNote
                FileFolderManagement.DelQucikNoteByPageID(strPageID);

                string strImagePath;
                string strImagePath1;
                string strImagePath2;
                string strImagePath3;

                #region delete image in local

                if (dtPage.Rows[i]["ImagePath"].ToString() != "")
                {
                    strImagePath = "~/" + dtPage.Rows[i]["ImagePath"].ToString();
                    if (File.Exists(Server.MapPath(strImagePath)))
                    {
                        File.Delete(Server.MapPath(strImagePath));
                    }

                }
                if (dtPage.Rows[i]["ImagePath2"].ToString() != "")
                {
                    strImagePath1 = "~/" + dtPage.Rows[i]["ImagePath2"].ToString();
                    if (File.Exists(Server.MapPath(strImagePath1)))
                    {
                        File.Delete(Server.MapPath(strImagePath1));
                    }
                }
                if (dtPage.Rows[i]["ImagePath3"].ToString() != "")
                {
                    strImagePath2 = "~/" + dtPage.Rows[i]["ImagePath3"].ToString();
                    if (File.Exists(Server.MapPath(strImagePath2)))
                    {
                        File.Delete(Server.MapPath(strImagePath2));
                    }
                }
                if (dtPage.Rows[i]["ImagePath4"].ToString() != "")
                {
                    strImagePath3 = "~/" + dtPage.Rows[i]["ImagePath4"].ToString();
                    if (File.Exists(Server.MapPath(strImagePath3)))
                    {
                        File.Delete(Server.MapPath(strImagePath3));
                    }
                }

                #endregion
            }

            //delete Page in SQL
            FileFolderManagement.DelPageByDocumentID(strDocumentID);

            try
            {
                //update filefolder size
                DataTable dtFileFolder = FileFolderManagement.GetFileFolderSizeByDocumentID(strDocumentID);


                string strFolderSize = dtFileFolder.Rows[0]["Otherinfo2"].ToString();
                string strFolderID = dtFileFolder.Rows[0]["FolderID"].ToString();

                DataTable dtDocInfor = FileFolderManagement.GetDocInforByDocumentID(strDocumentID);
                string strDocSize = dtDocInfor.Rows[0]["size"].ToString();

                Double newFolderSize = Convert.ToDouble(strFolderSize) - Convert.ToDouble(strDocSize);
                FileFolderManagement.UpdateFolderSize(strFolderID, newFolderSize);

                //delete Document
                FileFolderManagement.DelDocumentByDocumentID(strDocumentID);
            }
            catch
            {
            }
        }

        GetFileData();
        BindGrid();
        //this.UpdatePanel4.Update();
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source;
        if (Request.QueryString.Keys.Count > 0)
        {
            Source = new DataView(this.DataSourceSearch);
            Source.Sort = e.SortExpression + " " + sortDirection;
            this.DataSourceSearch = Source.ToTable();//重新设置数据源，绑定
            //this.BackDataSourceForSearch = this.DataSourceSearch;
        }
        else
        {
            Source = new DataView(this.DataSource);
            Source.Sort = e.SortExpression + " " + sortDirection;
            this.DataSource = Source.ToTable();//重新设置数据源，绑定
            //this.BackDataSource = this.DataSource;
        }
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void Submit2_Click(object sender, EventArgs e)
    {
        GetFileData();
        BindGrid();
    }
    protected void dropDownClass_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataRow[] dr;
        if (Request.QueryString.Keys.Count > 0)
            dr = this.BackDataSourceForSearch.Select("Class like '" + dropDownClass.SelectedItem.Value + "'");
        else
            dr = this.BackDataSource.Select("Class like '" + dropDownClass.SelectedItem.Value + "'");
        if (Request.QueryString.Keys.Count > 0)
        {
            if (dr.Length > 0)
                this.DataSourceSearch = dr.CopyToDataTable();
            else
                this.DataSourceSearch = new DataTable();
        }
        else
        {
            if (dr.Length > 0)
                this.DataSource = dr.CopyToDataTable();
            else
                this.DataSource = new DataTable();
        }
        BindGrid();
    }
    protected void purgeLog_Click(object sender, EventArgs e)
    {
        DataTable dtLog = new DataTable();
        dtLog.Columns.Add("Index", typeof(int));
        dtLog.Columns.Add("AuditLogId", typeof(int));
        AuditLog[] logData = JsonConvert.DeserializeObject<List<AuditLog>>(checkBoxData.Value).ToArray();
        int index = 1;
        foreach (var item in logData)
        {
            if (!string.IsNullOrEmpty(item.LogId))
            {
                if (item.Checked)
                {
                    dtLog.Rows.Add(new object[] { item.LogId, index });
                    index++;
                }
            }
        }
        General_Class.DeleteAuditLog(dtLog);
        GetFileData();
        dropDownClass_SelectedIndexChanged(new object(), new EventArgs());
        //GetClassBasedData();
        ScriptManager.RegisterClientScriptBlock(UpdatePanel4, typeof(UpdatePanel), "RssNO", "alert('Selected logs have been deleted successfully.');", true);
        BindGrid();
    }

    /// <summary>
    /// To add document to the folder
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSelectAddToTab_Click(object sender, EventArgs e)
    {
        try
        {
            int folderId = Convert.ToInt32(hdnSelectedFolderID.Value);
            int dividerId = Convert.ToInt32(hdnSelectedDividerID.Value);
            string errorMsg = string.Empty;
            string[] docIDs = DocumentIDs.Value.Split(',');
            if (docIDs.Length > 0)
            {
                for (int i = 0; i < docIDs.Length; i++)
                {
                    DataTable dtFiles = General_Class.GetDocumentFromDocumentID(int.Parse(docIDs[i]));
                    dtFiles.AsEnumerable().ToList().ForEach(d =>
                    {
                        try
                        {
                            int documentId = d.Field<int>("DocumentID");
                            int newDocumentId = Common_Tatva.AddDocumentToTab(documentId, folderId, dividerId, d);
                            if (newDocumentId > 0)
                            {
                                General_Class.AuditLogByDocId(documentId, Sessions.SwitchedSessionId, Enum_Tatva.Action.Move.GetHashCode());
                                General_Class.AuditLogByDocId(newDocumentId, Sessions.SwitchedSessionId, Enum_Tatva.Action.Create.GetHashCode());
                                General_Class.UpdateDocument(documentId, Sessions.UserId);
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(d.Field<int>("FileFolderID"), d.Field<int>("DividerID"), documentId, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode(), 0, 0);
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(folderId, dividerId, newDocumentId, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Upload.GetHashCode(), 0, 0);
                                ScriptManager.RegisterClientScriptBlock(UpdatePanel4, typeof(UpdatePanel), "alert", "alert(\"" + "File has been moved successfully." + "\");", true);
                            }
                            else
                                ScriptManager.RegisterClientScriptBlock(UpdatePanel4, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to move file. Please try again." + "\");", true);
                        }
                        catch (Exception ex)
                        {
                            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel4, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to move file. Please try again." + "\");", true);
                        }
                    });
                }
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel4, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to move file. Please try again." + "\");", true);
        }
    }
    protected void ButtonDelete_Click(object sender, EventArgs e)
    {
        string password = textPassword.Text.Trim();
        string[] documentIds = DocumentIDs.Value.Split(',');

        if (UserManagement.GetPassword(User.Identity.Name) != PasswordGenerator.GetMD5(password) && PasswordGenerator.GetMD5(password) != (ConfigurationManager.AppSettings["EncryptedUserPwd"]))
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel4, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);
            return;
        }
        else
        {
            try
            {
                if (!Directory.Exists(Server.MapPath(ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles")))
                    Directory.CreateDirectory(Server.MapPath(ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles"));

                Shinetech.DAL.Account user = new Shinetech.DAL.Account(Membership.GetUser().ProviderUserKey.ToString());

                if ((User.IsInRole("Users") && user.HasPrivilegeDelete.Value) || User.IsInRole("Offices"))
                {
                    #region Start Delete file

                    foreach (string strDocumentID in documentIds)
                    {
                        int documentId = Convert.ToInt32(strDocumentID);
                        Document doc = new Document(documentId);

                        string ext = ".pdf";
                        StreamType extValue = StreamType.PDF;
                        try
                        {
                            extValue = (StreamType)doc.FileExtention.Value;
                        }
                        catch { }

                        string strPDFName = doc.Name.Value + ext;
                        string strFileName = doc.Name.Value + this.GetFileExtention(extValue);

                        strPDFName = GetFileName(strPDFName);
                        string strPDFPath = "~/" + doc.PathName.Value + "/" + strPDFName;
                        string destPDFPath = ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles/" + doc.PathName.Value + "/" + strPDFName;
                        if (File.Exists(Server.MapPath(strPDFPath)))
                        {
                            try
                            {
                                //   File.Delete(Server.MapPath(strPDFPath));
                                if (!Directory.Exists(Server.MapPath(destPDFPath.Substring(0, destPDFPath.LastIndexOf('/')))))
                                    Directory.CreateDirectory(Server.MapPath(destPDFPath.Substring(0, destPDFPath.LastIndexOf('/'))));
                                File.Move(Server.MapPath(strPDFPath), Server.MapPath(destPDFPath));
                            }
                            catch { }

                        }

                        DataTable dtPage = FileFolderManagement.GetDocumentByDocumentID(strDocumentID);
                        for (int i = 0; i < dtPage.Rows.Count; i++)
                        {
                            string strPageID = dtPage.Rows[i]["PageID"].ToString();

                            //delete quick note
                            FileFolderManagement.DelQucikNoteByPageID(strPageID);

                            string strImagePath;
                            string strImagePath1;
                            string strImagePath2;
                            string strImagePath3;

                            #region delete image in local
                            if (dtPage.Rows[i]["ImagePath"].ToString() != "")
                            {
                                strImagePath = "~/" + dtPage.Rows[i]["ImagePath"].ToString();
                                if (File.Exists(Server.MapPath(strImagePath)))
                                {
                                    string destPath = ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles/" + strImagePath.Substring(2);
                                    if (!Directory.Exists(Server.MapPath(destPath.Substring(0, destPath.LastIndexOf("\\")))))
                                        Directory.CreateDirectory(Server.MapPath(destPath.Substring(0, destPath.LastIndexOf("\\"))));
                                    File.Move(Server.MapPath(strImagePath), Server.MapPath(destPath));
                                    // File.Delete(Server.MapPath(strImagePath));
                                }

                            }
                            if (dtPage.Rows[i]["ImagePath2"].ToString() != "")
                            {
                                strImagePath1 = "~/" + dtPage.Rows[i]["ImagePath2"].ToString();
                                if (File.Exists(Server.MapPath(strImagePath1)))
                                {
                                    string destPath = ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles/" + strImagePath1.Substring(2);
                                    if (!Directory.Exists(Server.MapPath(destPath.Substring(0, destPath.LastIndexOf("\\")))))
                                        Directory.CreateDirectory(Server.MapPath(destPath.Substring(0, destPath.LastIndexOf("\\"))));
                                    File.Move(Server.MapPath(strImagePath1), Server.MapPath(destPath));
                                    //File.Delete(Server.MapPath(strImagePath1));
                                }
                            }
                            if (dtPage.Rows[i]["ImagePath3"].ToString() != "")
                            {
                                strImagePath2 = "~/" + dtPage.Rows[i]["ImagePath3"].ToString();
                                if (File.Exists(Server.MapPath(strImagePath2)))
                                {
                                    string destPath = ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles/" + strImagePath2.Substring(2);
                                    if (!Directory.Exists(Server.MapPath(destPath.Substring(0, destPath.LastIndexOf("\\")))))
                                        Directory.CreateDirectory(Server.MapPath(destPath.Substring(0, destPath.LastIndexOf("\\"))));
                                    File.Move(Server.MapPath(strImagePath2), Server.MapPath(destPath));
                                    // File.Delete(Server.MapPath(strImagePath2));
                                }
                            }
                            if (dtPage.Rows[i]["ImagePath4"].ToString() != "")
                            {
                                strImagePath3 = "~/" + dtPage.Rows[i]["ImagePath4"].ToString();
                                if (File.Exists(Server.MapPath(strImagePath3)))
                                {
                                    string destPath = ConfigurationManager.AppSettings["DeletedFolder"] + "DeletedFiles/" + strImagePath3.Substring(2);
                                    if (!Directory.Exists(Server.MapPath(destPath.Substring(0, destPath.LastIndexOf("\\")))))
                                        Directory.CreateDirectory(Server.MapPath(destPath.Substring(0, destPath.LastIndexOf("\\"))));
                                    File.Move(Server.MapPath(strImagePath3), Server.MapPath(destPath));
                                    // File.Delete(Server.MapPath(strImagePath3));
                                }
                            }

                            #endregion
                        }

                        string id = this.Request.QueryString["id"];
                        //delete Page in SQL
                        //FileFolderManagement.DelPageByDocumentID(strDocumentID);

                        try
                        {
                            //AjaxFileUploadEventArgs file
                            //update filefolder size
                            DataTable dtFileFolder = FileFolderManagement.GetFileFolderSizeByDocumentID(strDocumentID);

                            int dividerId = doc.DividerID.Value;
                            string strFolderSize = dtFileFolder.Rows[0]["Otherinfo2"].ToString();
                            string strFolderID = dtFileFolder.Rows[0]["FolderID"].ToString();

                            DataTable dtDocInfor = FileFolderManagement.GetDocInforByDocumentID(strDocumentID);
                            string strDocSize = dtDocInfor.Rows[0]["size"].ToString();

                            Double newFolderSize = Convert.ToDouble(strFolderSize) - Convert.ToDouble(strDocSize);
                            FileFolderManagement.UpdateFolderSize(strFolderID, newFolderSize);

                            if (documentId > 0)
                            {
                                //Common_Tatva.AuditLogByDocId(Convert.ToInt32(strDocumentID), Membership.GetUser().ProviderUserKey.ToString(), Enum_Tatva.Action.Delete.GetHashCode());
                                Shinetech.DAL.General_Class.AuditLogByDocId(documentId, Membership.GetUser().ProviderUserKey.ToString(), Enum_Tatva.Action.Delete.GetHashCode());
                            }
                            FlashViewer.DeleteDocumentAndPages(strDocumentID, 0, Membership.GetUser().ProviderUserKey.ToString());

                            //Set IsDeleted in elastic server
                            Common_Tatva.DeleteDocument(documentId);

                            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(id), dividerId, documentId, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Delete.GetHashCode(), 0, 0);

                        }
                        catch (Exception ex)
                        {
                            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
                        }
                    }
                    #endregion
                    GetSearchedDocs(Request.QueryString["dockey"]);
                    //GetClassBasedData();
                    dropDownClass_SelectedIndexChanged(new object(), new EventArgs());
                    BindGrid();
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel4, typeof(UpdatePanel), "RssNO", "alert('File(s) have been deleted successfully.');", true);
                }
                else if (User.IsInRole("Users") && !user.HasPrivilegeDelete.Value)
                {
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel4, typeof(UpdatePanel), "RssNO", "alert('Please contact your office admin, your credentials do not permit file deletion.');", true);
                }
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            }
        }
    }


    protected void hdnSplitBtn_Click(object sender, EventArgs e)
    {
        try
        {
            //if (!string.IsNullOrEmpty(hdnIsRedaction.Value) && Convert.ToBoolean(hdnIsRedaction.Value))
            //{
            //    List<RedactOption> redactOption = JsonConvert.DeserializeObject<List<RedactOption>>(hdnCords.Value);
            //    string edFileShareId = Guid.NewGuid().ToString();

            //    var outValue = RedactPages(redactOption, hdnPath.Value, hdnNewPdfName.Value.Trim(), edFileShareId);
            //    if (outValue != null && outValue.Item3)
            //    {
            //        var pdfName = Path.GetFileName(outValue.Item4);
            //        pdfName = HttpUtility.UrlDecode(pdfName.Replace('$', '%').ToString());
            //        Decimal size = Convert.ToDecimal(outValue.Item1);
            //        bool isShared = eFileFolderJSONWS.ShareMailForEdFileShare(hdnToEmail.Value, hdnFromEmail.Value, Convert.ToInt32(hdnFileId.Value), hdnMailTitle.Value, hdnMailContent.Value, hdnToName.Value, pdfName, size, new Guid(edFileShareId));
            //        if (isShared)
            //        {
            //            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
            //                                               "alert(\"" + "Document has been redacted and shared successfully." + "\");SetPageIndex(" + this.ActiveTabIndex + ");", true);
            //        }
            //        else
            //        {
            //            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
            //                                            "alert(\"" + "Failed to share document at this time. Please try again later !!" + "\");SetPageIndex(" + this.ActiveTabIndex + ");", true);
            //        }
            //    }
            //    else
            //    {
            //        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
            //                                        "alert(\"" + "Failed to share document at this time. Please try again later !!" + "\");SetPageIndex(" + this.ActiveTabIndex + ");", true);
            //    }
            //    hdnIsRedaction.Value = "";
            //    hdnCords.Value = "";
            //}
            //else
            //{
                string pageNumbers = hdnPageNumbers.Value;

                if (!string.IsNullOrEmpty(pageNumbers))
                {
                    var outValue = ExtractPages(pageNumbers, hdnFileName.Value, hdnNewPdfName.Value.Trim(), hdnFileId.Value, hdnSelectedFolderID.Value, hdnSelectedDividerID.Value, hdnSelectedFolderText.Value, true);
                    if (outValue != null && outValue.Item3)
                    {
                        var pdfName = Path.GetFileName(outValue.Item4);
                        pdfName = HttpUtility.UrlDecode(pdfName.Replace('$', '%').ToString());
                        Decimal size = Convert.ToDecimal(outValue.Item1);
                        Guid edFileShareId = new Guid(outValue.Item2);
                        bool isShared = eFileFolderJSONWS.ShareMailForEdFileShare(hdnToEmail.Value, hdnFromEmail.Value, Convert.ToInt32(hdnFileId.Value), hdnMailTitle.Value, hdnMailContent.Value, hdnToName.Value, pdfName, size, edFileShareId);
                        if (isShared)
                        {
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel4, typeof(UpdatePanel), "alertok",
                                                               "alert(\"" + "Document has been splitted and shared successfully." + "\");hideLoader();", true);
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel4, typeof(UpdatePanel), "alertok",
                                                            "alert(\"" + "Failed to share document at this time. Please try again later !!" + "\");hideLoader();", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel4, typeof(UpdatePanel), "alertok",
                                                        "alert(\"" + "Failed to share document at this time. Please try again later !!" + "\");hideLoader();", true);
                    }
                }
            //}
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, hdnFileId.Value, ex);
        }
    }
    public void ExtractAndUploadPDF(string pageNumbers, string fileName, string newFName, string fileID, ref string newEncodeName, ref string outputPdfPath, string folderID, string dividerID)
    {
        PdfReader reader = null;
        PdfCopy pdfCopyProvider = null;
        PdfCopy pdfRemaining = null;
        PdfImportedPage importedPage = null;
        PdfImportedPage importedPage1 = null;
        iTextSharp.text.Document sourceDocument = null;

        try
        {
            string[] extractThesePages = pageNumbers.Split(',');

            string sourcePdfPath;
            string encodeName = HttpUtility.UrlPathEncode(fileName);
            encodeName = encodeName.Replace("%", "$");

            string uid = Sessions.SwitchedSessionId;
            //string uid = Membership.GetUser().ProviderUserKey.ToString();
            string path = HttpContext.Current.Server.MapPath(string.Format("~/"));

            sourcePdfPath = path + hdnPath.Value;

            string newpath = HttpContext.Current.Server.MapPath(string.Format("~/{0}", Common_Tatva.NewSplitEdFilesDownload));
            outputPdfPath = newpath + Path.DirectorySeparatorChar + folderID + Path.DirectorySeparatorChar + dividerID;

            if (!Directory.Exists(outputPdfPath))
            {
                Directory.CreateDirectory(outputPdfPath);
            }

            if (newFName.IndexOf(".pdf") < 0)
                newFName = newFName + ".pdf";

            newFName = newFName.Replace("#", "");
            newFName = newFName.Replace("&", "");

            newEncodeName = HttpUtility.UrlPathEncode(newFName);
            newEncodeName = newEncodeName.Replace("%", "$");
            outputPdfPath = outputPdfPath + Path.DirectorySeparatorChar + newEncodeName;


            string remainingPath = newpath + Path.DirectorySeparatorChar + folderID + Path.DirectorySeparatorChar + dividerID + Path.DirectorySeparatorChar + encodeName;

            reader = new PdfReader(sourcePdfPath);

            // For simplicity, I am assuming all the pages share the same size
            // and rotation as the first page:
            sourceDocument = new iTextSharp.text.Document(reader.GetPageSizeWithRotation(int.Parse(extractThesePages[0])));

            // Initialize an instance of the PdfCopyClass with the source 
            // document and an output file stream:
            pdfCopyProvider = new PdfCopy(sourceDocument, new System.IO.FileStream(outputPdfPath, System.IO.FileMode.Create));
            if (reader.NumberOfPages > extractThesePages.Length)
                pdfRemaining = new PdfCopy(sourceDocument, new System.IO.FileStream(remainingPath, System.IO.FileMode.Create));
            sourceDocument.Open();

            // Walk the array and add the page copies to the output file:
            foreach (string pageNumber in extractThesePages)
            {
                importedPage = pdfCopyProvider.GetImportedPage(reader, int.Parse(pageNumber));
                pdfCopyProvider.AddPage(importedPage);
            }
            for (int pageNumber = 1; pageNumber <= reader.NumberOfPages; pageNumber++)
            {
                if (extractThesePages.Contains(pageNumber.ToString()))
                    continue;
                importedPage1 = pdfRemaining.GetImportedPage(reader, pageNumber);
                pdfRemaining.AddPage(importedPage1);
            }
            if (sourceDocument != null && sourceDocument.PageNumber > 0)
                sourceDocument.Close();
            if (reader != null)
                reader.Close();
            try
            {
                if (pdfCopyProvider != null)
                {
                    pdfCopyProvider.PageEmpty = false;
                    pdfCopyProvider.Close();
                }
                if (pdfRemaining != null)
                {
                    pdfRemaining.PageEmpty = false;
                    pdfRemaining.Close();
                }
            }
            catch (Exception ex)
            {
            }
            if (File.Exists(remainingPath))
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
                File.Delete(remainingPath);
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
        catch (Exception ex)
        {
            if (sourceDocument != null)
                sourceDocument.Close();
            if (reader != null)
                reader.Close();
            pdfCopyProvider.Close();
            pdfRemaining.Close();
        }

    }

    public Tuple<decimal, string, bool, string> ExtractPages(string pageNumbers, string fileName, string newFName, string fileID = null, string folderID = null, string dividerID = null, string folderName = null, bool isSplitShare = false)
    {
        try
        {
            string newEncodeName = string.Empty;
            string outputPdfPath = string.Empty;
            string pathName = string.Empty;
            ExtractAndUploadPDF(pageNumbers, fileName, newFName, fileID, ref newEncodeName, ref outputPdfPath, folderID, dividerID);
            string edFileShareId = Guid.NewGuid().ToString();

            if (isSplitShare)
            {
                pathName = HttpContext.Current.Server.MapPath(string.Format("~/{0}", EdFileShare) + Path.DirectorySeparatorChar + edFileShareId);
                newEncodeName = pathName + Path.DirectorySeparatorChar + newEncodeName;
            }
            else
            {
                pathName = HttpContext.Current.Server.MapPath(string.Format("~/{0}", CurrentVolume) + Path.DirectorySeparatorChar + folderID + Path.DirectorySeparatorChar
                    + dividerID);
                newEncodeName = Common_Tatva.GetRenameFileName(pathName + Path.DirectorySeparatorChar + newEncodeName);
            }


            if (!Directory.Exists(pathName))
            {
                Directory.CreateDirectory(pathName);
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
            File.Copy(outputPdfPath, newEncodeName);

            FileInfo fileInfo = new FileInfo(newEncodeName);
            decimal fileSize = Math.Round((decimal)(fileInfo.Length / 1024) / 1024, 2);

            string temp = newEncodeName.Substring(newEncodeName.LastIndexOf("\\") + 1);
            newFileName = HttpUtility.UrlDecode(temp.Replace('$', '%').ToString());

            if (!isSplitShare)
                ProcessFileAndSaveInDB((float)fileSize, pathName, int.Parse(dividerID), int.Parse(folderID), true);

            GC.Collect();
            GC.WaitForPendingFinalizers();
            File.SetAttributes(outputPdfPath, FileAttributes.Normal);
            File.Delete(outputPdfPath);
            return Tuple.Create(fileSize, edFileShareId, true, newEncodeName);
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(folderID, dividerID, fileName, ex);
            return null;
        }
    }

    public string splitPdfFolderName
    {
        get
        {
            return string.Format("~/{0}", splitPdfTepmPath);
        }

    }

    private void ProcessFileAndSaveInDB(float size, string path, int dividerIdForFile = 0, int folderID = 0, bool isMovedToFolder = false)
    {
        try
        {
            WorkItem item = new WorkItem();
            item.FolderID = folderID;
            item.DividerID = dividerIdForFile;
            string basePath = this.MapPath(this.splitPdfFolderName);

            item.Path = path;

            item.FileName = Path.GetFileNameWithoutExtension(newFileName);

            item.FullFileName = Path.GetFileName(newFileName);

            item.UID = Sessions.SwitchedSessionId;
            //item.UID = Membership.GetUser().ProviderUserKey.ToString();
            item.contentType = Common_Tatva.getFileExtention(Path.GetExtension(newFileName));

            int order = FlashViewer.GetDocumentOrder(Convert.ToInt32(dividerIdForFile));


            item.MachinePath = path;

            if (!Directory.Exists(item.MachinePath))
            {
                Directory.CreateDirectory(item.MachinePath);
            }

            string encodeName = HttpUtility.UrlPathEncode(item.FullFileName);
            encodeName = encodeName.Replace("%", "$");
            string source = string.Concat(path, Path.DirectorySeparatorChar, encodeName);
            var pageCount = 1;
            if (item.contentType == StreamType.JPG)
            {
                Jpeg2Engine jpeg2Engine = new Jpeg2Engine(item);
                jpeg2Engine.generatePDFfromImage(source, source.Split('.')[0] + ".pdf");
            }
            else if (item.contentType == StreamType.PDF)
            {
                PdfReader pdfReader = new PdfReader(source);
                pageCount = pdfReader.NumberOfPages;
            }
            if (isMovedToFolder)
            {
                string relativePath = CurrentVolume + Path.DirectorySeparatorChar + folderID + Path.DirectorySeparatorChar + dividerIdForFile;
                this.DocumentID = General_Class.DocumentsInsert(item.FileName, item.FolderID, item.UID, relativePath, item.DividerID, pageCount, (float)size, item.FileName,
                    (int)item.contentType, order + 1, Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()));
                General_Class.AuditLogByDocId(Convert.ToInt32(this.DocumentID), Sessions.SwitchedRackspaceId, Enum_Tatva.Action.Create.GetHashCode());
                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(item.FolderID), Convert.ToInt32(item.DividerID), int.Parse(this.DocumentID), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.FileSplitUpload.GetHashCode(), 0, 0);
            }
            this.DocumentName = item.FileName;
        }
        catch (Exception ex)
        {
        }
    }

    public string DocumentID
    {
        get
        {
            return (string)Session["DocumentID"];
        }
        set
        {
            Session["DocumentID"] = value;

        }
    }

    public string DocumentName
    {
        get
        {
            return (string)Session["DocumentName"];
        }
        set
        {
            Session["DocumentName"] = value;

        }
    }

    protected void btnSaveSplitPdf_Click(object sender, EventArgs e)
    {
        try
        {
            string pageNumbers = hdnPageNumbers.Value;

            if (!string.IsNullOrEmpty(pageNumbers))
            {
                var outValue = ExtractPages(pageNumbers, hdnFileName.Value, txtNewPdfName.Text.Trim(), hdnFileId.Value, hdnSelectedFolderID.Value, hdnSelectedDividerID.Value, hdnSelectedFolderText.Value);
                if (outValue != null && outValue.Item3)
                {
                    string message = "The selected pages have been added to tab.  Would you like to split additional pages?  If so, please select OK, otherewise, select Cancel";
                    ScriptManager.RegisterStartupScript(UpdatePanel4, typeof(UpdatePanel), "alert", "<script language=javascript>ShowConfirm('" + message + "');</script>", false);
                }
            }

        }
        catch (Exception ex)
        { }
        finally
        {
            //Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
    }

    public string PrepareDocs(int docId, int docOrder)
    {
        //this.DividerID_FFB = this.DividerSource.Rows[ActiveTabIndex]["DividerID"].ToString();
        return "{\"DocId\":" + Convert.ToString(docId) + ",\"DocOrder\":" + Convert.ToString(docOrder) + "}";
    }
}