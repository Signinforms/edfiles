<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ModifyBox.aspx.cs" Inherits="ModifyOfficeBox" Title="" %>

<%--<%@ Register Src="../Controls/FileRequestControl.ascx" TagName="FileRequestControl"
    TagPrefix="uc2" %>--%>
<%@ Register Src="../Controls/BoxRequestControl.ascx" TagName="BoxRequestControl"
    TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .lblOfficeName
        {
            font-size: 15px;
            font-weight: bold;
            min-width: 200px;
        }

        .calenderTxtBox
        {
            background: url(../images/calendar-interface-symbol-tool.png) 100% 2px no-repeat;
        }

        .cancelBtn
        {
            margin-top: 5px;
            cursor: pointer;
            margin-left: 10px;
            border: none;
            width: 20px;
            background: url(../images/cancel-Btn.png) -3px no-repeat;
            height: 20px;
            position: absolute;
        }
    </style>
    <script language="javascript" type="text/javascript">
        $("div#btnprint").click(function () {
            $("div#divBoxContent").printArea();
        });

        function onCalendarShown(sender, args) {
            // Set new calendar position
            sender._popupDiv.parentElement.style.bottom = '0px';
            sender._popupDiv.parentElement.style.right = '40px';
            sender._popupDiv.parentElement.style.top = '';
            sender._popupDiv.parentElement.style.left = '';
        }

        function clearDate(a) {
            if ($(a).siblings("input:text").attr("id") == '<%=textScannedDate.ClientID %>') {
                $('#<%=textScannedDate.ClientID %>').val("Not Scanned");
            }
            else if ($(a).siblings("input:text").attr("id") == '<%=textPickupDate.ClientID %>') {
                $('#<%=textPickupDate.ClientID %>').val($('#<%=hdnPickupDate.ClientID %>').val());
            }
            else {
                $('#<%=textDestroyDate.ClientID %>').val("Never");
            }
    }
    function pageLoad() {
        if ($("#fieldSet1").children().find('.lblOfficeName').length < 1) {
            $("#fieldSet1").css("margin", "0");
        }
    }

    function printShow() {
    }
    </script>

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Modify Box</h1>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant modify-contant">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="left-content">
                            <div class="content-box listing-view">
                                <fieldset>
                                    <label>User Name</label>
                                    <asp:TextBox ID="textUserName" name="textUserName" ReadOnly="true" runat="server"
                                        size="50" MaxLength="23" />
                                </fieldset>

                                <fieldset>
                                    <label>Department Name</label>
                                    <asp:TextBox ID="textDepartmentName" name="textDepartmentName" runat="server"
                                        size="50" MaxLength="23" />
                                </fieldset>

                                <fieldset id="fieldSet1">
                                    <label>
                                        <asp:Label ID="lblOfficeName" CssClass="lblOfficeName" runat="server">Office Name</asp:Label></label>
                                    <asp:TextBox ID="textOfficeName" name="textOfficeName" ReadOnly="true" runat="server"
                                        size="50" MaxLength="23" />
                                </fieldset>

                                <fieldset>
                                    <label>Box Name</label>
                                    <asp:TextBox ID="textBoxName" name="textBoxName" runat="server"
                                        size="50" MaxLength="23" />
                                    <asp:RequiredFieldValidator runat="server" ID="telN" ControlToValidate="textBoxName"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The Box Name is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                        TargetControlID="telN" HighlightCssClass="validatorCalloutHighlight" />
                                    <span class="print-btn">
                                        <asp:Button ID="boxPrint" Text="Print" CssClass="create-btn btn-small green" runat="server" OnClick="boxPrint_OnClick" /></span>
                                </fieldset>

                                <fieldset>
                                    <label>Retention Class</label>
                                    <asp:DropDownList runat="server" ID="dropRetention">
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                    </asp:DropDownList>
                                </fieldset>

                                <fieldset>
                                    <label>Box Number</label>
                                    <asp:TextBox ID="textBoxNumber" name="textBoxNumber" runat="server"
                                        size="20" MaxLength="15" />
                                    <span class="barcode">
                                        <asp:Image ID="image1" runat="server" ImageAlign="AbsMiddle" ImageUrl="" />
                                    </span>
                                </fieldset>
                                <fieldset>
                                    <label>Warehouse</label>
                                    <asp:TextBox ID="textWarehouse" name="textWarehouse" ReadOnly="false" runat="server"
                                        size="50" MaxLength="23" />
                                </fieldset>
                                <fieldset>
                                    <label>Section</label>
                                    <asp:TextBox ID="textSection" name="textSection" ReadOnly="false" runat="server"
                                        size="50" MaxLength="23" />
                                </fieldset>
                                <fieldset>
                                    <label>Row</label>
                                    <asp:TextBox ID="textRow" name="textRow" ReadOnly="false" runat="server"
                                        size="50" MaxLength="23" />
                                </fieldset>
                                <fieldset>
                                    <label>Space</label>
                                    <asp:TextBox ID="textSpace" name="textSpace" ReadOnly="false" runat="server"
                                        size="50" MaxLength="50" />
                                </fieldset>
                                <fieldset>
                                    <label>Other</label>
                                    <asp:TextBox ID="textOther" name="textOther" ReadOnly="false" runat="server"
                                        size="50" MaxLength="50" />
                                </fieldset>
                                <fieldset>
                                    <label>Scanned Date</label>
                                    <asp:TextBox ID="textScannedDate" CssClass="calenderTxtBox" name="textScannedDate" runat="server"
                                        size="50" />
                                    <asp:Button ID="Button1" runat="server" CssClass="cancelBtn" OnClientClick="clearDate(this);return false;" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server"
                                        TargetControlID="textScannedDate" Format="MM-dd-yyyy" PopupButtonID="textScannedDate" OnClientShown="onCalendarShown">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RegularExpressionValidator ID="SodC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The scanned date is not a date"
                                        ControlToValidate="textScannedDate" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))|Not Scanned"
                                        Visible="true" Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
                                        TargetControlID="SodC" HighlightCssClass="validatorCalloutHighlight" />
                                </fieldset>

                                <fieldset>
                                    <label>Destroy Date</label>
                                    <asp:TextBox ID="textDestroyDate" CssClass="calenderTxtBox" name="textDestroyDate" runat="server"
                                        size="50" />
                                    <asp:Button ID="Button2" runat="server" CssClass="cancelBtn" OnClientClick="clearDate(this);return false;" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                        TargetControlID="textDestroyDate" Format="MM-dd-yyyy" OnClientShown="onCalendarShown">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RegularExpressionValidator ID="DobC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The destroy date is not a date"
                                        ControlToValidate="textDestroyDate" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))|Never"
                                        Visible="true" Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6"
                                        TargetControlID="DobC" HighlightCssClass="validatorCalloutHighlight" />
                                </fieldset>

                                <fieldset>
                                    <label>Box Pick-up Date</label>
                                    <asp:HiddenField runat="server" ID="hdnPickupDate" />
                                    <asp:TextBox ID="textPickupDate" CssClass="calenderTxtBox" name="textPickupDate" runat="server"
                                        size="50" />
                                    <asp:Button ID="Button3" runat="server" CssClass="cancelBtn" OnClientClick="clearDate(this);return false;" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" runat="server"
                                        TargetControlID="textPickupDate" Format="MM-dd-yyyy" OnClientShown="onCalendarShown">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:RegularExpressionValidator ID="BodC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The box pick-up date is not a date"
                                        ControlToValidate="textPickupDate" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                        Visible="true" Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
                                        TargetControlID="BodC" HighlightCssClass="validatorCalloutHighlight" />
                                </fieldset>
                            </div>


                            <div class="content-box listing-view upload-area">
                                <p>1. Please provide Excel Sheet File with Columns (First Name, Last Name, File#, File Class)</p>
                                <asp:FileUpload ID="fileExlt1" runat="server" CssClass="input-file"></asp:FileUpload>
                                <asp:Button runat="server" name="buttonUpload1" ID="buttonUpload1" OnClick="imageField_Click" CssClass="create-btn btn-small blue radius-none"
                                    Text="Save" />
                            </div>

                            <div class="content-box listing-view" style="margin-top: 15px; border: 0; padding: 0;">
                                <p>
                                    <strong>2. Please provide complete File information (Last Name, First Name, File#, File Class)</strong>
                                </p>
                                <div>&nbsp;</div>
                                <p>
                                    <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                                </p>
                                <ajaxToolkit:TabContainer runat="server" ID="fileTabs" ActiveTabIndex="0">

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel1" HeaderText="File01-10">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName1" size="30" MaxLength="30" />
                                                                    <asp:HiddenField ID="HiddenField1" Value="1" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName1" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile1" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass1" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName2" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField2" Value="2" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName2" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile2" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass2" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName3" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField3" Value="3" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName3" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile3" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass3" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName4" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField4" Value="4" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName4" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile4" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass4" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName5" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField5" Value="5" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName5" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile5" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass5" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName6" size="30" MaxLength="30" /><asp:HiddenField ID="HiddenField6" Value="6" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName6" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile6" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass6" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName7" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField7" Value="7" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName7" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile7" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass7" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName8" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField8" Value="8" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName8" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile8" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass8" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName9" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField9" Value="9" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName9" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile9" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass9" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName10" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField10" Value="10" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName10" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile10" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass10" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel2" HeaderText="File11-20">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName11" size="30" MaxLength="30" />
                                                                    <asp:HiddenField ID="HiddenField11" Value="11" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName11" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile11" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass11" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName12" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField12" Value="12" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName12" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile12" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass12" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName13" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField13" Value="13" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName13" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile13" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass13" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName14" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField14" Value="14" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName14" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile14" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass14" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName15" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField15" Value="15" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName15" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile15" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass15" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName16" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField16" Value="16" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName16" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile16" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass16" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName17" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField17" Value="17" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName17" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile17" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass17" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName18" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField18" Value="18" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName18" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile18" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass18" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName19" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField19" Value="19" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName19" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile19" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass19" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName20" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField20" Value="20" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName20" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile20" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass20" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel3" HeaderText="File21-30">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName21" size="30" MaxLength="30" />
                                                                    <asp:HiddenField ID="HiddenField21" Value="21" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName21" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile21" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass21" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName22" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField22" Value="22" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName22" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile22" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass22" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName23" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField23" Value="23" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName23" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile23" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass23" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName24" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField24" Value="24" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName24" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile24" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass24" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName25" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField25" Value="25" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName25" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile25" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass25" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName26" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField26" Value="26" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName26" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile26" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass26" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName27" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField27" Value="27" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName27" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile27" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass27" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName28" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField28" Value="28" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName28" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile28" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass28" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName29" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField29" Value="29" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName29" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile29" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass29" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName30" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField30" Value="30" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName30" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile30" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass30" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel4" HeaderText="File31-40">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName31" size="30" MaxLength="30" />
                                                                    <asp:HiddenField ID="HiddenField31" Value="31" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName31" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile31" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass31" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName32" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField32" Value="32" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName32" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile32" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass32" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName33" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField33" Value="33" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName33" size="30"
                                                                        MaxLength="30" />

                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile33" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass33" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName34" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField34" Value="34" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName34" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile34" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass34" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName35" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField35" Value="35" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName35" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile35" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass35" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName36" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField36" Value="36" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName36" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile36" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass36" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName37" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField37" Value="37" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName37" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile37" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass37" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName38" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField38" Value="38" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName38" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile38" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass38" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName39" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField39" Value="39" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName39" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile39" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass39" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName40" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField40" Value="40" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName40" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile40" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass40" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel5" HeaderText="File41-50">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName41" size="30" MaxLength="30" />
                                                                    <asp:HiddenField ID="HiddenField41" Value="41" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName41" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile41" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass41" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName42" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField42" Value="42" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName42" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile42" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass42" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName43" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField43" Value="43" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName43" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile43" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass43" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName44" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField44" Value="44" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName44" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile44" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass44" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName45" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField45" Value="45" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName45" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile45" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass45" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName46" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField46" Value="46" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName46" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile46" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass46" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName47" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField47" Value="47" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName47" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile47" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass47" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName48" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField48" Value="48" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName48" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile48" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass48" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName49" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField49" Value="49" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName49" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile49" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass49" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName50" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField50" Value="50" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName50" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile50" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass50" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel6" HeaderText="File51-60">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName51" size="30" MaxLength="30" />
                                                                    <asp:HiddenField ID="HiddenField51" Value="51" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName51" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile51" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass51" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName52" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField52" Value="52" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName52" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile52" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass52" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName53" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField53" Value="53" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName53" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile53" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass53" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName54" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField54" Value="54" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName54" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile54" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass54" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName55" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField55" Value="55" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName55" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile55" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass55" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName56" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField56" Value="56" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName56" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile56" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass56" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName57" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField57" Value="57" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName57" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile57" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass57" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName58" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField58" Value="58" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName58" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile58" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass58" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName59" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField59" Value="59" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName59" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile59" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass59" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName60" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField60" Value="60" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName60" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile60" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass60" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel7" HeaderText="File61-70">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName61" size="30" MaxLength="30" />
                                                                    <asp:HiddenField ID="HiddenField61" Value="61" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName61" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile61" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass61" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName62" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField62" Value="62" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName62" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile62" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass62" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName63" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField63" Value="63" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName63" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile63" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass63" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName64" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField64" Value="64" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName64" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile64" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass64" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName65" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField65" Value="65" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName65" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile65" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass65" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName66" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField66" Value="66" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName66" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile66" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass66" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName67" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField67" Value="67" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName67" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile67" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass67" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName68" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField68" Value="68" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName68" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile68" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass68" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName69" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField69" Value="69" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName69" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile69" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass69" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName70" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField70" Value="70" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName70" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile70" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass70" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel8" HeaderText="File71-80">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName71" size="30" MaxLength="30" />
                                                                    <asp:HiddenField ID="HiddenField71" Value="71" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName71" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile71" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass71" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName72" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField72" Value="72" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName72" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile72" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass72" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName73" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField73" Value="73" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName73" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile73" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass73" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName74" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField74" Value="74" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName74" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile74" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass74" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName75" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField75" Value="75" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName75" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile75" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass75" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName76" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField76" Value="76" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName76" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile76" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass76" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName77" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField77" Value="77" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName77" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile77" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass77" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName78" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField78" Value="78" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName78" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile78" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass78" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName79" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField79" Value="79" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName79" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile79" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass79" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName80" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField80" Value="80" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName80" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile80" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass80" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel9" HeaderText="File81-90">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName81" size="30" MaxLength="30" />
                                                                    <asp:HiddenField ID="HiddenField81" Value="81" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName81" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile81" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass81" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName82" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField82" Value="82" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName82" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile82" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass82" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName83" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField83" Value="83" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName83" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile83" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass83" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName84" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField84" Value="84" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName84" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile84" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass84" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName85" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField85" Value="85" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName85" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile85" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass85" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName86" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField86" Value="86" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName86" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile86" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass86" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName87" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField87" Value="87" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName87" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile87" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass87" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName88" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField88" Value="88" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName88" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile88" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass88" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName89" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField89" Value="89" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName89" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile89" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass89" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName90" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField90" Value="90" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName90" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile90" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass90" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                    <ajaxToolkit:TabPanel runat="server" ID="Panel10" HeaderText="File91-100">
                                        <ContentTemplate>
                                            <div class="multiple-fieldset">
                                                <table cellpadding="0" cellspacing="0" border="0" width="100%" class="fieldset-datatable">
                                                    <tbody>
                                                        <tr class="even">
                                                            <td width="6%" class="number">1</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName91" size="30" MaxLength="30" />
                                                                    <asp:HiddenField ID="HiddenField91" Value="91" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName91" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile91" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass91" size="30" MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">2</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName92" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField92" Value="92" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName92" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile92" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass92" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">3</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName93" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField93" Value="93" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName93" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile93" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass93" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">4</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName94" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField94" Value="94" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName94" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile94" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass94" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">5</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName95" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField95" Value="95" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName95" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile95" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass95" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">6</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName96" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField96" Value="96" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName96" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile96" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass96" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">7</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName97" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField97" Value="97" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName97" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox runat="server" ID="textFile97" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass97" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">8</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName98" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField98" Value="98" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName98" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile98" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass98" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="even">
                                                            <td width="6%" class="number">9</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName99" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField99" Value="99" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName99" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile99" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass99" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                        <tr class="odd">
                                                            <td width="6%" class="number">10</td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>First Name</label>
                                                                    <asp:TextBox runat="server" ID="textFirstName100" size="30"
                                                                        MaxLength="30" /><asp:HiddenField ID="HiddenField100" Value="100" runat="server" />
                                                                    <label>Last Name</label>
                                                                    <asp:TextBox runat="server" ID="textLastName100" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                            <td width="47%">
                                                                <fieldset>
                                                                    <label>File #</label>
                                                                    <asp:TextBox
                                                                        runat="server" ID="textFile100" size="30" MaxLength="30" />
                                                                    <label>File Class</label>
                                                                    <asp:TextBox runat="server" ID="textFileClass100" size="30"
                                                                        MaxLength="30" />
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </ContentTemplate>
                                    </ajaxToolkit:TabPanel>

                                </ajaxToolkit:TabContainer>
                                <fieldset>
                                    <label>&nbsp;</label>
                                    <div style="text-align: center">
                                        <asp:Button runat="server" name="imageField" ID="imageField" OnClick="imageField_Click" CssClass="create-btn btn green"
                                            Text="Save" />
                                    </div>
                                </fieldset>

                            </div>
                        </div>
                        <div class="right-content">
                            <%-- <div class="quick-find">
                            <uc1:QuickFind ID="QuickFind1" runat="server" />
                        </div>--%>
                            <div class="quick-find" style="margin-top: 15px;">
                                <div class="find-inputbox">
                                    <marquee scrollamount="1" id="marqueeside" runat="server"
                                        behavior="scroll" onmouseover="this.stop()" onmouseout="this.start()" scrolldelay="20"
                                        direction="up">
                                <%=MessageBody%></marquee>
                                </div>
                            </div>
                            <%--<div class="quick-find" style="margin-top: 15px;">
                            <uc2:FileRequestControl ID="fileRequest1" runat="server" />
                        </div>--%>
                            <div class="quick-find" style="margin-top: 15px;">
                                <uc2:BoxRequestControl ID="fileRequest1" runat="server" />
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="imageField" EventName="Click" />
                        <asp:PostBackTrigger ControlID="buttonUpload1" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <div>
        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="Button1ShowPopup" runat="server" Style="display: none" />
                <ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" PopupControlID="pnlPopup"
                    OnOkScript="anotherRequest()" CancelControlID="btnClose" OnCancelScript="closePage()"
                    OkControlID="btnRequset" DropShadow="true" BackgroundCssClass="modalBackground"
                    TargetControlID="Button1ShowPopup" />
                <!-- ModalPopup Panel-->
                <asp:Panel ID="pnlPopup" runat="server" Style="display: none;">
                    <div class="popup-mainbox" style="width: 325px">
                        <div class="popup-head">
                            File Request
                        </div>
                        <div class="popup-scroller">
                            <table width="100%" border="0" cellpadding="2" cellspacing="0" class="details-datatable">
                                <tbody>
                                    <tr>
                                        <td align="left" class="podnaslov">
                                            <strong>Hello&nbsp;<asp:Label ID="LabelUserName" Text="" runat="server" />,</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="100%" align="left" class="podnaslov">Your request: {<strong><asp:Label ID="LabelResult" Text="" runat="server" /></strong>}&nbsp;&nbsp;has been submitted.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="podnaslov"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div align="center" class="popup-btn">
                            <asp:Button ID="btnRequset" runat="server" Text="Request Another" CssClass="btn green" Style="width: auto;" />
                            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn green" />
                        </div>
                    </div>
                </asp:Panel>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="imageField" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
    </div>

    <script language="javascript" type="text/javascript">
        function anotherRequest() {
            window.location = window.location;

        }

        function closePage() {
            window.location = "http://www.edfiles.com/Office/Welcome.aspx";
        }
    </script>

    <!-- Preview Panel-->
    <asp:Button Style="display: none" ID="Button1Preview" runat="server"></asp:Button>
    <div>
        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalBackground"
            CancelControlID="btnClose2" PopupControlID="Panel1Preview" TargetControlID="Button1Preview">
        </ajaxToolkit:ModalPopupExtender>
        <!-- ModalPopup Panel-->
        <asp:Panel Style="display: none; z-index: 10001" ID="Panel1Preview" runat="server"
            CssClass="popup-mainbox" BackColor="White">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div id="divContainer" class="popup-head">Box Details</div>

                    <asp:HiddenField ID="HiddenField101" runat="server" />
                    <div id="divContentContainer" style="background-color: White">
                        <div id="divBoxContent" style="background-color: White;">
                            <table width="100%" class="details-datatable">
                                <tr style="border-width: 2px;">
                                    <td>Office Name
                                    </td>
                                    <td>
                                        <asp:TextBox ID="textOfficeName2" name="textOfficeName2" ReadOnly="true" runat="server" size="18" MaxLength="20" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Box Name
                                    </td>
                                    <td>
                                        <asp:TextBox ID="textBoxName2" name="textBoxName2" runat="server" size="18" MaxLength="20" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Box Number
                                    </td>
                                    <td>
                                        <asp:TextBox ID="textBoxNumber2" name="textBoxNumber2" runat="server" size="18" MaxLength="18" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Image ID="imageBarcode" runat="server" ImageAlign="AbsMiddle" ImageUrl="" Style="padding-bottom: 10px; width: 95%;" />
                                    </td>
                                </tr>
                            </table>
                        </div>


                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div align="center" style="width: 100%; background-color: white; padding: 20px 0;">
                <input id="btnprint" type="button" onclick='$("#divBoxContent").printArea();' value="Print" class="create-btn btn green" />
                <asp:Button ID="btnClose2" runat="server" Text="Close" CssClass="create-btn btn green" />
            </div>
        </asp:Panel>
    </div>
</asp:Content>
