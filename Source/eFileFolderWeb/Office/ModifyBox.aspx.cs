using System;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;

using DataQuicker2.Framework;
using ExcelDataReader;
using Shinetech.DAL;
using Shinetech.Utility;
using Account = Shinetech.DAL.Account;
using BarcodeLib;

public partial class ModifyOfficeBox : LicensePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //textOfficeName.Text = Membership.GetUser().UserName;

            string id = this.Request.QueryString["id"];
            int boxid;
            if (!string.IsNullOrEmpty(id) && int.TryParse(id, out boxid))
            {
                Box box = new Box(Convert.ToInt32(id));

                if (box.IsExist)
                {
                    if (box.UserId.ToString().ToLower() != Sessions.SwitchedSessionId.ToLower())
                    //if (box.UserId.ToString().ToLower() != Membership.GetUser().ProviderUserKey.ToString().ToLower())
                    {
                        return;
                    }

                    textBoxName.Text = box.BoxName.Value;
                    textBoxNumber.Text = box.BoxNumber.Value.ToUpper();

                    textWarehouse.Text = box.Warehouse.Value;
                    textSection.Text = box.Section.Value;
                    textRow.Text = box.Row.Value;
                    textSpace.Text = box.Space.Value;
                    textOther.Text = box.Other.Value;
                    textDepartmentName.Text = box.DepartmentName.Value;
                    dropRetention.SelectedIndex = (box.RetentionClass.Value) - 1;
                    hdnPickupDate.Value = DateTime.Now.ToString("MM-dd-yyyy");
                    if (box.ScannedDate != null && !box.ScannedDate.IsNull)
                    {
                        textScannedDate.Text = box.ScannedDate.ToString();
                        CalendarExtender1.SelectedDate = Convert.ToDateTime(box.ScannedDate.ToString());
                    }
                    else
                    {
                        textScannedDate.Text = "Not Scanned";
                    }
                    if (box.BoxPickupDate != null && !box.BoxPickupDate.IsNull)
                    {
                        textPickupDate.Text = box.BoxPickupDate.ToString();
                        CalendarExtender3.SelectedDate = Convert.ToDateTime(box.BoxPickupDate.ToString());
                    }
                    else
                    {
                        textPickupDate.Text = DateTime.Now.ToString("MM-dd-yyyy");
                    }
                    if (Convert.ToDateTime(box.DestroyDate.ToString()) != DateTime.MinValue && !box.DestroyDate.IsNull)
                    {
                        textDestroyDate.Text = box.DestroyDate.ToString();
                        CalendarExtender2.SelectedDate = Convert.ToDateTime(box.DestroyDate.ToString());
                    }
                    else
                    {
                        textDestroyDate.Text = "Never";
                    }
                    textUserName.Text = Sessions.SwitchedUserName;
                    //textUserName.Text = Membership.GetUser().UserName;
                    if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users"))
                    {
                        lblOfficeName.Visible = true;
                        DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(Sessions.SwitchedSessionId);
                        textOfficeName.Text = dtOfficeUser.Rows[1]["Name"].ToString();
                    }
                    else
                    {
                        textOfficeName.Visible = false;
                        lblOfficeName.Visible = false;
                    }
                    //textDestroyDate.Text = Convert.ToString(box.DestroyDate.Value);
                    //CalendarExtender2.SelectedDate = DateTime.Now.AddYears(1);
                    //if (box.DestroyDate.Value != null && box.DestroyDate.Value.Year != 1)
                    //{
                    //    CalendarExtender2.SelectedDate = box.DestroyDate.Value;
                    //}

                    BarcodeLib.Barcode b = new BarcodeLib.Barcode();
                    b.IncludeLabel = false;

                    b.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
                    b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;
                    BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39Extended;

                    b.Encode(type, textBoxNumber.Text,
                        Color.Black, Color.Bisque, 400, 25);

                    image1.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(b.GetImageData(SaveTypes.JPG));

                    DataTable table = FileFolderManagement.GetPaperFilesByBoxId(id);

                    int count = table.Rows.Count;

                    int tabIndex = 0;

                    for (int i = 1; i <= count; i++)
                    {
                        DataRow row = table.Rows[i - 1];
                        tabIndex = ((int)Math.Ceiling(i / 10f)) - 1;
                        //paper file
                        ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFirstName" + i)).Text =
                            row["FirstName"].ToString();
                        ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textLastName" + i)).Text =
                            row["LastName"].ToString();
                        ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFile" + i)).Text =
                            row["FileNumber"].ToString();
                        ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFileClass" + i)).Text =
                            row["FileName"].ToString();

                        ((HiddenField)this.fileTabs.Tabs[tabIndex].FindControl("HiddenField" + i)).Value =
                            row["FileID"].ToString();
                    }
                }
            }
        }
    }


    public string MessageBody
    {
        get { return this.Session["_Side_MessageBody"] as string; }

        set { this.Session["_Side_MessageBody"] = value; }
    }

    protected void imageField_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string id = this.Request.QueryString["id"];
            int boxid;
            if (!string.IsNullOrEmpty(id) && int.TryParse(id, out boxid))
            {
                Box box = new Box(boxid);
                if (box.IsExist)
                {
                    if (box.UserId.ToString().ToLower() != Sessions.SwitchedSessionId.ToLower())
                    {
                        return;
                    }

                    box.BoxName.Value = textBoxName.Text.Trim();
                    box.BoxNumber.Value = textBoxNumber.Text.Trim();
                    box.UserId.Value = Sessions.SwitchedSessionId;
                    //box.UserId.Value = Membership.GetUser().ProviderUserKey.ToString();

                    box.Warehouse.Value = textWarehouse.Text.Trim();
                    box.Section.Value = textSection.Text.Trim();
                    box.Row.Value = textRow.Text.Trim();
                    box.Space.Value = textSpace.Text.Trim();
                    box.Other.Value = textOther.Text.Trim();
                    if (textPickupDate.Text != null && textPickupDate.Text != "")
                        box.BoxPickupDate.Value = Convert.ToDateTime(textPickupDate.Text);
                    else
                        box.BoxPickupDate.Value = DateTime.Now;
                    box.DepartmentName.Value = textDepartmentName.Text.Trim();
                    box.RetentionClass.Value = Convert.ToInt16(dropRetention.SelectedItem.Value);
                    if (textScannedDate.Text != null && textScannedDate.Text != "Not Scanned" && textScannedDate.Text != "")
                        box.ScannedDate.Value = Convert.ToDateTime(textScannedDate.Text);
                    else
                    {
                        box.ScannedDate.Value = null;
                    }
                    //if (CalendarExtender2.SelectedDate != null)                       
                    //box.DestroyDate.Value = (DateTime)CalendarExtender2.SelectedDate;
                    //if (Convert.ToDateTime(textDestroyDate.Text) >= DateTime.Now)
                    if (textDestroyDate.Text != null && textDestroyDate.Text != "Never" && textDestroyDate.Text != "")
                        box.DestroyDate.Value = Convert.ToDateTime(textDestroyDate.Text);
                    else
                        box.DestroyDate.Value = null;


                    IDbConnection connection = DbFactory.CreateConnection();
                    connection.Open();
                    IDbTransaction transaction = connection.BeginTransaction();

                    try
                    {
                        if (fileExlt1.HasFile)
                        {
                            IExcelDataReader reader = null;
                            try
                            {
                                if (Path.GetExtension(fileExlt1.FileName).ToLower().Equals(".xlsx"))
                                {
                                    reader = ExcelReaderFactory.CreateOpenXmlReader(fileExlt1.PostedFile.InputStream);
                                }
                                else if (Path.GetExtension(fileExlt1.FileName).ToLower().Equals(".xls"))
                                {
                                    reader = ExcelReaderFactory.CreateBinaryReader(fileExlt1.PostedFile.InputStream);
                                }
                                else
                                {
                                    throw new ApplicationException("Please Provide Excel File with .xlsx or .xls extension");
                                }

                                DataSet result = reader.AsDataSet(new ExcelDataSetConfiguration()
                                {
                                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration
                                    {
                                        UseHeaderRow = true
                                    }
                                });

                                ArrayList items = new ArrayList();
                                int tabIndex = 0;

                                for (int i = 1; i <= 100; i++)
                                {
                                    tabIndex = ((int)Math.Ceiling(i / 10f)) - 1;

                                    int fileId =
                                        Convert.ToInt32(
                                            ((HiddenField)this.fileTabs.Tabs[tabIndex].FindControl("HiddenField" + i)).Value);

                                    if (result.Tables[0].Rows.Count >= i)
                                    {
                                        DataRow row = result.Tables[0].Rows[i - 1];
                                        //modify paper file
                                        PaperFile file1 = new PaperFile(fileId);
                                        file1.FirstName.Value = row[0].ToString().Trim();
                                        file1.LastName.Value = row[1].ToString().Trim();
                                        file1.FileNumber.Value = row[2].ToString().Trim();
                                        file1.FileName.Value = row[3].ToString().Trim();
                                        //file1.FileClass.Value = row[3].ToString().Trim();

                                        ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFirstName" + i)).Text = file1.FirstName.Value;
                                        ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textLastName" + i)).Text = file1.LastName.Value;
                                        ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFile" + i)).Text = file1.FileNumber.Value;
                                        ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFileClass" + i)).Text = file1.FileName.Value;

                                        items.Add(file1);
                                    }
                                }

                                foreach (PaperFile file in items)
                                {
                                    file.BoxId.Value = box.BoxId.Value;
                                    file.Update(transaction);
                                }
                            }
                            catch (Exception exp)
                            {
                                throw exp;
                            }
                            finally
                            {
                                if (reader != null)
                                {
                                    reader.Close();
                                    //connection.Close();
                                }
                            }
                        }
                        else
                        {
                            ArrayList items = new ArrayList();
                            int tabIndex = 0;
                            for (int i = 1; i <= 100; i++)
                            {
                                tabIndex = ((int)Math.Ceiling(i / 10f)) - 1;

                                int fileId =
                                    Convert.ToInt32(
                                        ((HiddenField)this.fileTabs.Tabs[tabIndex].FindControl("HiddenField" + i)).Value);

                                //create paper file
                                PaperFile file1 = new PaperFile(fileId);
                                file1.FirstName.Value =
                                    ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFirstName" + i)).Text.Trim();
                                file1.LastName.Value =
                                    ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textLastName" + i)).Text.Trim();
                                file1.FileNumber.Value =
                                    ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFile" + i)).Text.Trim();
                                file1.FileName.Value =
                                    ((TextBox)this.fileTabs.Tabs[tabIndex].FindControl("textFileClass" + i)).Text.Trim();

                                items.Add(file1);
                            }

                            foreach (PaperFile file in items)
                            {
                                file.BoxId.Value = box.BoxId.Value;
                                file.Update(transaction);
                            }
                        }

                        box.Update(transaction);

                        transaction.Commit();

                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                        "alert(\"" + "Update Box Successfully" + "\");", true);
                    }

                    catch (Exception ex)
                    {
                        transaction.Rollback();

                        string strWrongInfor = string.Format("Error : {0}", ex.Message);
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                                "alert(\"" + strWrongInfor + "\");", true);
                    }
                }


            }

        }
    }

    public bool ExistBox(string name, string uid)
    {
        return FileFolderManagement.ExistBox(name, uid);
    }


    public bool IsFileValid()
    {
        if (textFirstName1.Text.Trim() == "" && textFirstName2.Text.Trim() == "" && textFirstName3.Text.Trim() == ""
            && textFirstName4.Text.Trim() == "" && textFirstName5.Text.Trim() == "")
        {
            string strWrongInfor = string.Format("Error : You have to insert one file at least.");
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                    "alert(\"" + strWrongInfor + "\");", true);
            return false;
        }

        if (textFirstName1.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName1.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 1th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                        "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }

        if (textFirstName2.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName2.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 2th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                        "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }

        if (textFirstName3.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName3.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 3th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                        "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }

        if (textFirstName4.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName3.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 4th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                        "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }

        if (textFirstName5.Text.Trim() != "") // need to fill in other fields
        {
            if (textLastName5.Text.Trim() == "")
            {
                string strWrongInfor = "Error : You have to fill the last name field in the 4th file.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                        "alert(\"" + strWrongInfor + "\");", true);
                return false;
            }
        }


        return true;
    }

    protected void DropDownList1_OnSelectedIndexChanged(object sender, EventArgs e)
    {
    }

    private void sendeFileFormEmail(FileForm form, ArrayList items)
    {
        string strMailTemplet = getFileFormMailTemplate();

        strMailTemplet = strMailTemplet.Replace("[textOfficeName]", form.OfficeName.Value);
        strMailTemplet = strMailTemplet.Replace("[textUserName]", Sessions.SwitchedUserName);
        //strMailTemplet = strMailTemplet.Replace("[textUserName]", Membership.GetUser().UserName);
        strMailTemplet = strMailTemplet.Replace("[textPhone]", form.PhoneNumber.Value);
        strMailTemplet = strMailTemplet.Replace("[textRequestBy]", form.RequestedBy.Value);
        strMailTemplet = strMailTemplet.Replace("[textRequestDate]", form.RequestDate.Value.ToShortDateString());
        strMailTemplet = strMailTemplet.Replace("[textOfficeName]", form.OfficeName.Value);

        strMailTemplet = strMailTemplet.Replace("[textComment]", form.Comment.Value);

        int count = items.Count;
        int delta = 5 - count;

        FormFile file;

        for (int i = 0; i < count; i++)
        {
            file = items[i] as FormFile;
            strMailTemplet = strMailTemplet.Replace("[textFirstName" + (i + 1) + "]", file.FirstName.Value);
            strMailTemplet = strMailTemplet.Replace("[textLastName" + (i + 1) + "]", file.LastName.Value);
            strMailTemplet = strMailTemplet.Replace("[textFile" + (i + 1) + "]", file.FileNumber.Value);
            strMailTemplet = strMailTemplet.Replace("[textYear" + (i + 1) + "]", file.YearDate.Value);
        }

        for (int i = 5; i > count; i--)
        {
            strMailTemplet = strMailTemplet.Replace("[textFirstName" + i + "]", "");
            strMailTemplet = strMailTemplet.Replace("[textLastName" + i + "]", "");
            strMailTemplet = strMailTemplet.Replace("[textFile" + i + "]", "");
            strMailTemplet = strMailTemplet.Replace("[textYear" + i + "]", "");
        }

        string strEmailSubject = "File Form Request";

        Email.SendFileRequestService(Membership.GetUser().Email, strEmailSubject, strMailTemplet);
    }


    private string getFileFormMailTemplate()
    {
        string strMailTemplet = string.Empty;
        string physicalPath = Request.PhysicalPath;
        int index = physicalPath.LastIndexOf("\\");
        string mailTempletUrl = physicalPath.Substring(0, index + 1) + "FileForm.html";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    protected void btnClose_OnClick(object sender, EventArgs e)
    {
        Page.Response.Redirect("~/Office/Welcome.aspx");
    }

    protected void boxPrint_OnClick(object sender, EventArgs e)
    {
        BarcodeLib.Barcode b = new BarcodeLib.Barcode();
        b.IncludeLabel = false;

        b.RotateFlipType = RotateFlipType.RotateNoneFlipNone;
        b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;
        BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39Extended;

        textOfficeName2.Text = textOfficeName.Text;
        textBoxName2.Text = textBoxName.Text;
        textBoxNumber2.Text = textBoxNumber.Text;

        b.Encode(type, textBoxNumber.Text,
                Color.Black, Color.Bisque, 400, 40);
        imageBarcode.ImageUrl = "data:image/jpg;base64," + Convert.ToBase64String(b.GetImageData(SaveTypes.JPG));

        UpdatePanel2.Update();
        ModalPopupExtender2.Show();
    }



    protected void anotherRequest_Click(object sender, EventArgs e)
    {
        Page.Response.Redirect("~/Office/CreateFileRequest.aspx");
    }
}