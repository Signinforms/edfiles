﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="MultiUpload.aspx.cs" Inherits="Office_MultiUpload" %>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register Src="../Controls/UserFind.ascx" TagName="UserFind" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/style.css" rel="Stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/colorbox.css" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>css/main.css?v=2" rel="stylesheet" />
    <link rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>lib/TreeView/style.min.css" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/js/edFiles/EdFilesLogs.js?v=4" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/js/jquery.colorbox-min.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>extras/jquery-ui-1.8.22.custom.min.js"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style type="text/css">
        .white_content {
            display: none;
            position: fixed;
            top: 20%;
            left: 20%;
            width: 60%;
            height: 50%;
            padding: 16px;
            background-color: white;
            z-index: 999999999;
            overflow: auto;
        }

        .form-containt .content-box fieldset {
            font-size: initial;
        }

        .ajax__tab_body {
            height: auto !important;
        }

        .ajax__fileupload_dropzone {
            font-size: 15px;
            line-height: 120px !important;
            height: 120px !important;
        }

        input[type='checkbox'] {
            height: 18px;
            width: 18px;
            cursor: pointer;
        }

        .chckBox {
            float: right;
        }

        .find .tab {
            margin-left: 15px;
        }


        .find .find-input {
            height: 43px;
            padding: 5px;
            float: left;
            border: 2px solid #d4d4d4;
        }

        .find .quick-find-btn {
            width: 39px;
            height: 29px;
            font-size: 0;
            background-image: url(../images/quickfind-bg.png);
            background-repeat: no-repeat;
            background-position: center center;
            border-radius: 0;
            -webkit-border-radius: 0;
            position: absolute;
            border: none;
            margin-left: -45px;
            margin-top: 6px;
            background-color: transparent;
            cursor: pointer;
        }

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
            left: 0;
        }

        #floatingCirclesG {
            top: 50%;
            left: 50%;
            position: fixed;
        }

        .uploadManually {
            background-color: #000099;
            float: right;
            margin-left: 10px;
            visibility: visible;
            width: auto !important;
            padding-left: 10px;
            padding-right: 10px;
            font-weight: bold;
        }
    </style>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Multi Upload</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <asp:Panel ID="panelMain" runat="server" Visible="true">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="content-box listing-view">
                                <div id="uploadProgress" class="white_content" style="font-size: 14px;"></div>
                                <fieldset id="uploadFieldset" style="margin: 0px !important">
                                    <div id="divUpload">
                                        <div class='clearfix'></div>
                                        <asp:Image runat="server" ID="Imagelabel12" ImageUrl="../Images/loader_bg.gif" ImageAlign="Middle" />
                                        <asp:Label ID="label12" runat="server"></asp:Label>
                                        <asp:Label runat="server" ID="myThrobber" Style="display: none;"><img align="middle" alt="" src="uploading.gif"/></asp:Label>
                                        <label>Select or Drag & Drop the document(s) to upload in many tabs.</label>
                                        <ajaxToolkit:AjaxFileUpload ID="AjaxFileUpload1" runat="server" Padding-Bottom="4"
                                            Padding-Left="2" Padding-Right="1" Padding-Top="4"
                                            ThrobberID="myThrobber"
                                            ContextKeys="2"
                                            MaximumNumberOfFiles="10"
                                            AzureContainerName=""
                                            AllowedFileTypes="jpg,jpeg,pdf,doc,docx,xls,avi"
                                            OnUploadComplete="AjaxFileUpload1_UploadComplete"
                                            OnClientUploadComplete="onClientUploadComplete"
                                            OnUploadCompleteAll="AjaxFileUpload1_UploadCompleteAll"
                                            OnClientUploadCompleteAll="onClientUploadCompleteAll"
                                            OnUploadStart="AjaxFileUpload1_UploadStart"
                                            OnClientUploadStart="onClientUploadStart" />

                                        <%--<asp:Button ID="btnRefreshGrid" runat="server" Style="display: none" OnClick="btnRefreshGrid_Click" />--%>
                                        <div class='clearfix'></div>
                                        <div id="uploadCompleteInfo">
                                        </div>
                                </fieldset>
                                <div style="float: left; margin-top: 3%; margin-left: -5px;">
                                    <div class="prevnext custom_select">
                                        <cc1:WebPager ID="WebPager2" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" OnPageSizeChanged="WebPager1_PageSizeChanged"
                                            PageSize="50" CssClass="prevnext" PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>
                                    </div>
                                </div>
                                <div style="float: right; margin-top: 3%">
                                    <div class="find">
                                        <asp:TextBox ID="searchBox" runat="server" class="find-input" MaxLength="50" OnTextChanged="searchBox_TextChanged" autocomplete="off" placeholder="Enter folder name :" AutoPostBack="true" />
                                        <asp:Button ID="searchButton1" runat="server" ImageAlign="AbsMiddle" CausesValidation="false" OnClick="searchBox_TextChanged" Text="Search" class="quick-find-btn" />
                                        <asp:TextBox ID="searchBoxTab" runat="server" class="find-input tab" MaxLength="50" OnTextChanged="searchBox_TextChanged" autocomplete="off" placeholder="Enter Tab name :" AutoPostBack="true" />
                                        <asp:Button ID="searchButton2" runat="server" ImageAlign="AbsMiddle" CausesValidation="false" OnClick="searchBox_TextChanged" Text="Search" class="quick-find-btn searchtab" />

                                    </div>
                                </div>
                                <asp:HiddenField ID="checkBoxData" runat="server" />
                                <div class="form-containt listing-contant">
                                    <div class="overflow-auto">
                                        <asp:GridView ID="GridView1" runat="server" OnRowDataBound="GridView_RowDataBound"
                                            OnSorting="GridView1_Sorting" AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                                            <PagerSettings Visible="False" />
                                            <AlternatingRowStyle CssClass="odd" />
                                            <RowStyle CssClass="even" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:TemplateField ShowHeader="true">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label8" runat="server" Text="FolderName" />
                                                    </HeaderTemplate>
                                                    <HeaderStyle Width="150" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="folderName" runat="server" Text='<%#Eval("FolderName") %>'></asp:Label>
                                                        <asp:HiddenField ID="hdnFolderId" runat="server" Value='<%#Eval("FolderId") %>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="true">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label8" runat="server" Text="Tab" />
                                                    </HeaderTemplate>
                                                    <HeaderStyle Width="150" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="TabName1" runat="server" Text='<%#Eval("TabName1") %>'></asp:Label>
                                                        <asp:CheckBox ID="TabLocked1" CssClass="chckBox" runat="server" />
                                                        <asp:HiddenField ID="hiddenDID1" runat="server" Value='<%#Eval("TabId1") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="100" />
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="true">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label8" runat="server" Text="Tab" />
                                                    </HeaderTemplate>
                                                    <HeaderStyle Width="150" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="TabName2" runat="server" Text='<%#Eval("TabName2") %>'></asp:Label>
                                                        <asp:CheckBox ID="TabLocked2" CssClass="chckBox" runat="server" />
                                                        <asp:HiddenField ID="hiddenDID2" runat="server" Value='<%#Eval("TabId2") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="100" />
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="true">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label8" runat="server" Text="Tab" />
                                                    </HeaderTemplate>
                                                    <HeaderStyle Width="150" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="TabName3" runat="server" Text='<%#Eval("TabName3") %>'></asp:Label>
                                                        <asp:CheckBox ID="TabLocked3" CssClass="chckBox" runat="server" />
                                                        <asp:HiddenField ID="hiddenDID3" runat="server" Value='<%#Eval("TabId3") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="100" />
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="true">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label8" runat="server" Text="Tab" />
                                                    </HeaderTemplate>
                                                    <HeaderStyle Width="150" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="TabName4" runat="server" Text='<%#Eval("TabName4") %>'></asp:Label>
                                                        <asp:CheckBox ID="TabLocked4" CssClass="chckBox" runat="server" />
                                                        <asp:HiddenField ID="hiddenDID4" runat="server" Value='<%#Eval("TabId4") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="100" />
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="true">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label8" runat="server" Text="Tab" />
                                                    </HeaderTemplate>
                                                    <HeaderStyle Width="150" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="TabName5" runat="server" Text='<%#Eval("TabName5") %>'></asp:Label>
                                                        <asp:CheckBox ID="TabLocked5" CssClass="chckBox" runat="server" />
                                                        <asp:HiddenField ID="hiddenDID5" runat="server" Value='<%#Eval("TabId5") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="100" />
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="true">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label8" runat="server" Text="Tab" />
                                                    </HeaderTemplate>
                                                    <HeaderStyle Width="150" />
                                                    <ItemTemplate>
                                                        <asp:Label ID="TabName6" runat="server" Text='<%#Eval("TabName6") %>'></asp:Label>
                                                        <asp:CheckBox ID="TabLocked6" CssClass="chckBox" runat="server" />
                                                        <asp:HiddenField ID="hiddenDID6" runat="server" Value='<%#Eval("TabId6") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="100" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <asp:Label ID="Label2" runat="server" CssClass="Empty" />There is no record for searched folder name.
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                    <div class="prevnext custom_select" style="float: right; margin-top: 10px;">
                                        <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" OnPageSizeChanged="WebPager1_PageSizeChanged"
                                            PageSize="50" CssClass="prevnext" PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>
                                        <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
        </div>
    </div>

    <div id="overlay" style="display: none;">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>

    <script type="text/javascript">

        var checkArray = [];

        function pageLoad() {
            init();
            $(".ajax__fileupload_dropzone").text("Drag and Drop Pdf file(s) here");

            $('input:checkbox').not('.parentChk').not('.JchkAll').click(function () {
                var isChecked = $(this).prop('checked');
                $(this).prop("checked", isChecked);
                var tabId = $(this).parent().siblings().last().val();
                var folderId = $(this).parent().parent().parent().find('[id*=hdnFolderId]').val();
                if (isChecked)
                    pushArray(folderId, tabId);
                else
                    popArray(tabId);
            })
        }

        function disableScrollbar() {
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }

        function enableScrollbar() {
            $('html, body').css({
                'overflow': 'auto',
                'height': 'auto'
            });
        }

        function showLoader() {
            $('#overlay').show();
            disableScrollbar();
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
            enableScrollbar();
        }

        function popArray(tabId) {
            for (var i = 0; i < checkArray.length; i++) {
                if (checkArray[i].TabId == tabId) {
                    checkArray.splice(i, 1);
                    return;
                }
            }
            return false;
        }

        function pushArray(folderId, tabId) {
            checkArray.push({ 'FolderId': folderId, 'TabId': tabId });
        }

        function init() {
            $('input[type="file"]').change(function () {
                if ($("div.ajax__fileupload_fileItemInfo").length > 0) {
                    AddUploadButton();
                }
            });
        }

        function AddUploadButton() {
            $.each($("div.ajax__fileupload_fileItemInfo"), function (i, e) {
                if ($('.uploadManually').length == 0) {
                    var renameDiv = '<div id="uploadManually" class="ajax__fileupload_uploadbutton uploadManually">Upload</div>';
                    $(renameDiv).insertBefore('[id*=UploadOrCancelButton]');
                    $('[id*=UploadOrCancelButton]').addClass('uploadOnly');
                    $('#uploadManually').click(function () {
                        if (checkArray.length > 0) {
                            $('[id*=UploadOrCancelButton]').click();
                        }
                        else {
                            alert("Please select tab(s) to upload pdf(s)!!")
                        }
                    });
                }
                $('[id*=UploadOrCancelButton]').hide();
                $(e).find("div.removeButton").click(function () {
                    if ($("div.ajax__fileupload_fileItemInfo").length == 0) {
                        $('.uploadManually').remove();
                    }
                });
            });
        }

        function onClientUploadComplete(sender, e) {
            $('.uploadManually').remove();
            return false;
        }

        var myTimer;
        function onClientUploadStart(sender, e) {
            if (checkArray.length > 0) {
                $('.uploadManually').remove();
                $('#<%= checkBoxData.ClientID %>').val(JSON.stringify(checkArray));
                $.ajax(
                    {
                        type: "POST",
                        url: '../Office/MultiUpload.aspx/SaveDividerId',
                        data: "{ dividerIds:'" + $('#<%= checkBoxData.ClientID %>').val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: false,
                        success: function (result) {
                            document.getElementById('uploadCompleteInfo').innerHTML = "please wait while the " + e.get_filesInQueue() + " file is uploaded..";
                            showLoader();
                            $('[id*=ajaxfileupload1_Html5DropZone]').addClass('hideControl').css('display', 'none');
                            $('[id*=ajaxfileupload1_SelectFileContainer]').addClass('hideControl').css('display', 'none');
                            $('[id*=FileItemDeleteButton]').addClass('hideControl').css('display', 'none');
                            $('[id*=UploadOrCancelButton]').addClass('hideControl').css('display', 'none');
                            $('.TabDropDown').prop('disabled', 'disabled');
                            $('#uploadProgress').html('');
                            $('#uploadProgress').html('').html($('#divUpload').html());

                            document.getElementById('uploadProgress').style.display = 'block';
                            myTimer = setInterval(function () {
                                $('#uploadProgress').html('').html($('#divUpload').html());
                            }, 1000);
                        },
                        error: function (result) {
                            hideLoader();
                        }
                    });
            }
            return false;
        }

        function onClientUploadCompleteAll(sender, e) {
            $('.hideControl').removeClass('hideControl');
            $('.uploadProgress').find('select').removeAttr('disabled');

            $('#divUpload').clone().appendTo('#uploadFieldset');
            hideLoader();
            clearInterval(myTimer);
            document.getElementById('uploadProgress').style.display = 'none';
            checkArray = [];
            $('input:checkbox').prop("checked", false);
            alert("Your document(a) has been uploaded successfully.");
            setTimeout(function () {
                var vd = '<%= ConfigurationManager.AppSettings["VirtualDir"] %>';
                window.location.reload();
            }, 1000);
        }
    </script>
</asp:Content>

