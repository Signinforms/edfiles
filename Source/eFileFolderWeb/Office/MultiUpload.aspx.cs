﻿using AjaxControlToolkit;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using Shinetech.DAL;
using Shinetech.Engines;
using Shinetech.Framework.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Office_MultiUpload : System.Web.UI.Page
{
    public string newFileName = string.Empty;
    public string fullFilePath = string.Empty;
    public string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
    }

    public void GetData()
    {
        DataTable dtTabs = General_Class.GetTabsByOfficeId(Sessions.SwitchedRackspaceId);
        //DataTable dtTabs = General_Class.GetTabsByOfficeId(Membership.GetUser().ProviderUserKey.ToString());
        DataTable dtCloned = dtTabs.Clone();

        for (int i = 1; i < 7; i++)
        {
            dtCloned.Columns["TabLocked" + i].DataType = typeof(Boolean);
        }
        foreach (DataRow row in dtTabs.Rows)
        {
            dtCloned.ImportRow(row);
        }
        this.DataSource = dtCloned;
        this.BackDataSource = dtCloned;
        if (!string.IsNullOrEmpty(searchBox.Text) || !string.IsNullOrEmpty(searchBoxTab.Text))
        {
            DataRow[] dr = this.BackDataSource.Select("(FolderName Like '%" + searchBox.Text + "%') AND ( TabName1 Like '%" + searchBoxTab.Text + "%'OR TabName2 Like '%" + searchBoxTab.Text + "%'OR TabName3 Like '%" + searchBoxTab.Text + "%'OR TabName4 Like '%" + searchBoxTab.Text + "%'OR TabName5 Like '%" + searchBoxTab.Text + "%'OR TabName6 Like '%" + searchBoxTab.Text + "%')");
            if (dr.Length > 0)
                this.DataSource = dr.CopyToDataTable();
            else
                this.DataSource = new DataTable();
        }
    }
    public void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
        WebPager2.DataSource = this.DataSource;
        WebPager2.DataBind();
    }

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager2.CurrentPageIndex = e.NewPageIndex;
        if (this.DataSource != null)
        {
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
            WebPager2.DataSource = this.DataSource;
            WebPager2.DataBind();
        }
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager2.CurrentPageIndex = 0;
        if (this.DataSource != null)
        {
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
            WebPager2.DataSource = this.DataSource;
            WebPager2.DataBind();
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource_Tabs"] as DataTable;
        }
        set
        {
            this.Session["DataSource_Tabs"] = value;
        }
    }

    public DataTable BackDataSource
    {
        get
        {
            return this.Session["Back_DataSource_Tabs"] as DataTable;
        }
        set
        {
            this.Session["Back_DataSource_Tabs"] = value;
        }
    }

    protected void GridView_RowDataBound(Object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HiddenField hdnFolderId = e.Row.FindControl("hiddenField1") as HiddenField;
            for (int i = 1; i < 7; i++)
            {
                HiddenField hdn = e.Row.FindControl("hiddenDID" + i) as HiddenField;
                CheckBox hdnChk = e.Row.FindControl("TabLocked" + i) as CheckBox;
                if (hdn.Value == "" || hdn.Value == null)
                    hdnChk.Visible = false;
            }
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();//ÖØÐÂÉèÖÃÊý¾ÝÔ´£¬°ó¶¨
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }
    protected void searchBox_TextChanged(object sender, EventArgs e)
    {

        DataRow[] dr = this.BackDataSource.Select("(FolderName Like '%" + searchBox.Text + "%') AND ( TabName1 Like '%" + searchBoxTab.Text + "%'OR TabName2 Like '%" + searchBoxTab.Text + "%'OR TabName3 Like '%" + searchBoxTab.Text + "%'OR TabName4 Like '%" + searchBoxTab.Text + "%'OR TabName5 Like '%" + searchBoxTab.Text + "%'OR TabName6 Like '%" + searchBoxTab.Text + "%')");
        if (dr.Length > 0)
            this.DataSource = dr.CopyToDataTable();
        else
            this.DataSource = new DataTable();
        BindGrid();
    }

    protected void locksearchtab_Click(object sender, EventArgs e)
    {
        string name = searchBoxTab.Text;
        General_Class.UpdateLockSearchTab(name, Sessions.UserId);
        GetData();
        BindGrid();
    }

    protected void AjaxFileUpload1_UploadComplete(object sender, AjaxFileUploadEventArgs file)
    {
        FolderTabDetails[] folderTabDetails = JsonConvert.DeserializeObject<List<FolderTabDetails>>(Sessions.DividerIds).ToArray();
        try
        {
            bool isValid = true;
            foreach (var item in folderTabDetails)
            {
                byte[] fullfile = ProcessFile(AjaxFileUpload1, file, item.FolderId, item.TabId);
                ProcessFile(file, fullfile, item.FolderId, item.TabId, Enum_Tatva.Class.Permanent.GetHashCode());
                if (Convert.ToInt32(this.DocumentID) > 0)
                {
                    Shinetech.DAL.General_Class.AuditLogByDocId(Convert.ToInt32(this.DocumentID), Sessions.SwitchedSessionId, Enum_Tatva.Action.Create.GetHashCode());
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(item.FolderId, item.TabId, Convert.ToInt32(this.DocumentID), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Upload.GetHashCode(), 0, 0);
                }
                else isValid = false;
            }
            if (isValid)
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert('Your document(s) have been uploaded successfully.');", true);
            else
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert('Failed to upload some of the file(s). Please try again after sometime.');", true);
        }
        catch (Exception ex)
        {
        }
    }

    public byte[] ProcessFile(AjaxFileUpload AjaxFileUpload1, AjaxFileUploadEventArgs file, int folderId, int tabId)
    {
        try
        {
            string basePath = this.MapPath("~/" + this.CurrentVolume);

            string path = basePath + Path.DirectorySeparatorChar +
                        folderId + Path.DirectorySeparatorChar + tabId;
            var fileName = file.FileName;
            fileName = fileName.Replace("#", "");
            fileName = fileName.Replace("&", "");
            fileName = Common_Tatva.SubStringFilename(fileName);
            string encodeName = HttpUtility.UrlPathEncode(fileName);
            encodeName = encodeName.Replace("%", "$");
            string filePath = String.Concat(
                    path,
                    Path.DirectorySeparatorChar,
                    encodeName
                );
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            byte[] s = file.GetContents();

            filePath = String.Concat(path, Path.DirectorySeparatorChar, Common_Tatva.ChangeExtension(filePath));

            filePath = Common_Tatva.GetRenameFileName(filePath);
            newFileName = Path.GetFileName(filePath);
            newFileName = HttpUtility.UrlDecode(newFileName.Replace('$', '%').ToString());
            File.WriteAllBytes(filePath, s);
            fullFilePath = filePath;
            return s;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    public void ProcessFile(AjaxFileUploadEventArgs file, byte[] fileStream, int folderId, int tabId, int classIdForFile)
    {
        try
        {
            WorkItem item = new WorkItem();
            item.FolderID = folderId;
            item.DividerID = tabId;
            string basePath = this.MapPath("~/" + this.CurrentVolume);

            item.Path = this.CurrentVolume + Path.DirectorySeparatorChar +
                        item.FolderID + Path.DirectorySeparatorChar + item.DividerID;
            item.FileName = Path.GetFileNameWithoutExtension(newFileName);
            item.FullFileName = Path.GetFileName(newFileName);
            item.UID = Sessions.SwitchedSessionId;
            //item.UID = Membership.GetUser().ProviderUserKey.ToString();
            item.contentType = Common_Tatva.getFileExtention(Path.GetExtension(newFileName));
            item.Content = new MemoryStream(fileStream);
            int order = FlashViewer.GetDocumentOrder(tabId);


            item.MachinePath = string.Concat(basePath, Path.DirectorySeparatorChar, item.FolderID,
            Path.DirectorySeparatorChar, tabId);

            if (!Directory.Exists(item.MachinePath))
            {
                Directory.CreateDirectory(item.MachinePath);
            }

            float size = (float)Math.Round((decimal)(fileStream.Length / 1024) / 1024, 2); // IN MB.
            string encodeName = HttpUtility.UrlPathEncode(item.FullFileName);
            encodeName = encodeName.Replace("%", "$");
            string source = string.Concat(item.MachinePath, Path.DirectorySeparatorChar, encodeName);
            var pageCount = 1;
            if (item.contentType == StreamType.JPG)
            {
                Jpeg2Engine jpeg2Engine = new Jpeg2Engine(item);
                jpeg2Engine.generatePDFfromImage(source, source.Split('.')[0] + ".pdf");
            }
            else if (item.contentType == StreamType.PDF)
            {
                PdfReader pdfReader = new PdfReader(source);
                pageCount = pdfReader.NumberOfPages;
                pdfReader.Close();
            }
            this.DocumentID = General_Class.DocumentsInsert(item.FileName, item.FolderID, item.UID, item.Path, item.DividerID, pageCount, size, item.FileName, (int)item.contentType, order + 1, Convert.ToString(classIdForFile));
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(Convert.ToString(folderId), Convert.ToString(tabId), newFileName, ex);
            if ((File.Exists(fullFilePath)))
                File.Delete(fullFilePath);
        }
    }
    public string DocumentID
    {
        get
        {
            return (string)Session["Multi_DocumentID"];
        }
        set
        {
            Session["Multi_DocumentID"] = value;

        }
    }

    protected void AjaxFileUpload1_UploadCompleteAll(object sender, AjaxControlToolkit.AjaxFileUploadCompleteAllEventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert('Failed to upload some of the file(s). Please try again after sometime.');", true);
    }

    protected void AjaxFileUpload1_UploadStart(object sender, AjaxControlToolkit.AjaxFileUploadStartEventArgs e)
    {

    }

    [WebMethod]
    public static void SaveDividerId(string dividerIds)
    {
        Sessions.DividerIds = dividerIds;
    }
}