<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="NeweFileFolder.aspx.cs" Inherits="NeweFileFolder" Title="" %>

<%@ Register Src="../Controls/FileRequestControl.ascx" TagName="FileRequestControl"
    TagPrefix="uc2" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css?v=1" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=3" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/edFiles/EdFilesLogs.js?v=4" type="text/javascript"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>extras/jquery-ui-1.8.22.custom.min.js"></script>
    <style type="text/css">
        .cp_button {
            background-image: url(../../images/cp_button.png);
            width: 16px;
            height: 16px;
            border: none;
            background-repeat: no-repeat;
            /* margin-top: 14px; */
            margin: 10px;
        }

        .tempBox {
            margin-left: 10px;
            width: 200px !important;
        }

        table {
            white-space: normal;
            line-height: normal;
            font-weight: normal;
            font-size: medium;
            font-variant: normal;
            font-style: normal;
            color: -internal-quirk-inherit;
            text-align: start;
        }

        .color-label {
            float: left;
            height: 30px;
            padding: 8px 10px 0px 5px;
            vertical-align: middle;
            width: auto !important;
        }

        #overlay {
            display: none;
            position: fixed;
            top: 0;
            bottom: 0;
            background: rgba(0,0,0,.8);
            width: 100%;
            height: 100%;
            z-index: 99999;
        }
        /*.tabDropDown
        {
            max-width:150px !important;
        }*/
    </style>
    <script language="javascript" type="text/javascript">

        function openDetails() {
            var arrayObj = new Array();
            arrayObj = window.showModalDialog("Details.htm", window, "dialogWidth:460px;status:no;dialogHeight:110px;help:no");
        }

        $(document).ready(function () {

        });


    </script>
    <div class="title-container">
        <div class="inner-wrapper">
            <asp:HiddenField ID="hdnOfficeId" runat="server" />
            <asp:HiddenField ID="hdnfolderId" runat="server" />
            <h1>Create New EdFiles</h1>
            <p style="font-size: 20px;">
                Thank you for signing up to use EdFiles. Just as you would name a paper file
                                folder, please create and name your EdFiles by completing the info below. In
                                order to add documents to an edFile, you will need to create atleast one tab.Welcome.
           
            </p>
            <div class="clearfix"></div>
        </div>
    </div>
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div class="left-content">
                    <asp:Panel ID="panelMain" runat="server" Visible="true">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="content-title">Details</div>
                                <div class="content-box listing-view">
                                    <fieldset>
                                        <label>Templates</label>
                                        <asp:DropDownList ID="DropDownList3" runat="server" AutoPostBack="true" DataTextField="Name"
                                            DataValueField="TemplateID" CausesValidation="false" OnSelectedIndexChanged="OnSelectedIndexChaned_Templates" />
                                    </fieldset>
                                    <fieldset>
                                        <label>Folder Name</label>
                                        <asp:TextBox ID="txtFolderName" runat="server" size="30" CssClass="requiredGreen" OnTextChanged="txtFolderName_TextChanged"
                                            MaxLength="50" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator runat="server" ID="folderNameV" ControlToValidate="txtFolderName"
                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />Folder name is required." />
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                            TargetControlID="folderNameV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    </fieldset>

                                    <fieldset>
                                        <label>First Name</label>
                                        <asp:TextBox ID="textfield3" runat="server" size="30" MaxLength="50" CssClass="requiredGreen" />
                                        <asp:RequiredFieldValidator runat="server" ID="FNReq" ControlToValidate="textfield3"
                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />First name is required." />
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="FNReqE" TargetControlID="FNReq"
                                            HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    </fieldset>

                                    <fieldset>
                                        <label>Last Name</label>
                                        <asp:TextBox ID="TextBox1" runat="server" size="30" MaxLength="50" CssClass="requiredGreen" />
                                        <asp:RequiredFieldValidator runat="server" ID="LNReq" ControlToValidate="TextBox1"
                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />Last name is required." />
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="LNReqE" TargetControlID="LNReq"
                                            HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    </fieldset>

                                    <fieldset>
                                        <label>Address</label>
                                        <asp:TextBox name="textfield10" runat="server" ID="textfield10"
                                            size="40" MaxLength="200" />
                                    </fieldset>

                                    <fieldset>
                                        <label>City</label>
                                        <asp:TextBox runat="server" ID="txtCity" size="30" MaxLength="50" />
                                    </fieldset>

                                    <fieldset>
                                        <label>State/Province</label>
                                        <asp:DropDownList ID="drpState" runat="server" />
                                    </fieldset>

                                    <fieldset>
                                        <label>Other</label>
                                        <asp:TextBox runat="server" ID="txtOtherState" size="30"
                                            MaxLength="50" />
                                    </fieldset>

                                    <fieldset>
                                        <label>Zip/Postal</label>
                                        <asp:TextBox ID="txtPostal" runat="server" size="30" MaxLength="10" />
                                        <asp:RegularExpressionValidator ID="regZip" runat="server" ControlToValidate="txtPostal"
                                            Display="None" ErrorMessage="It is not a invalid Postal format." ValidationExpression="^\d{5}((-|\s)?\d{4})?$" />
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17"
                                            TargetControlID="regZip" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    </fieldset>

                                    <fieldset>
                                        <label>Tel.</label>
                                        <asp:TextBox name="textfieldTel" type="text" runat="server"
                                            ID="textfieldTel" size="30" MaxLength="50" />
                                        <asp:RegularExpressionValidator ID="TelV" ControlToValidate="textfieldTel" runat="server"
                                            ErrorMessage="<b>Required Field Missing</b><br />It is not a telphone format."
                                            ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                            TargetControlID="TelV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    </fieldset>

                                    <fieldset>
                                        <label>SSN</label>
                                        <asp:TextBox name="textfield9" type="text" runat="server"
                                            ID="textfield9" size="30" MaxLength="50" />
                                        <asp:RegularExpressionValidator ID="faxV" ControlToValidate="textfield9" runat="server"
                                            ErrorMessage="It is not a FAX format." ValidationExpression="^(\d{3}-?\d{2}-?\d{4}|XXX-XX-XXXX)$" Display="None"></asp:RegularExpressionValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8"
                                            TargetControlID="faxV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    </fieldset>


                                    <fieldset>
                                        <label>Email</label>
                                        <asp:TextBox ID="textfield13" name="textfield13" runat="server"
                                            size="30" MaxLength="50" />
                                        <asp:RegularExpressionValidator ID="rev_Email" runat="server" ErrorMessage="<b>Required Field Missing</b><br />It is not an email format."
                                            ControlToValidate="textfield13" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            Visible="true" Display="None"></asp:RegularExpressionValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
                                            TargetControlID="rev_Email" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    </fieldset>

                                    <fieldset>
                                        <label>File Number</label>
                                        <asp:TextBox ID="TextBoxFileNumber" runat="server" size="30" CssClass="requiredGreen"
                                            MaxLength="20" />
                                        <asp:RequiredFieldValidator runat="server" ID="FileNumberValidator" ControlToValidate="TextBoxFileNumber"
                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />You must fill in this field in order to create this new EdFile." />
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorExtenderFileNumber" TargetControlID="FileNumberValidator"
                                            HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    </fieldset>
                                    <fieldset>
                                        <label>DOB (If Applicable)</label>
                                        <asp:TextBox name="textfield5" runat="server" ID="textfield5" Placeholder="mm-dd-yyyy"
                                            size="15" MaxLength="50" />
                                        <%--                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" PopupPosition="BottomLeft" runat="server"
                                            TargetControlID="textfield5" Format="MM-dd-yyyy">
                                        </ajaxToolkit:CalendarExtender>--%>

                                        <%--<asp:RequiredFieldValidator runat="server" ID="DobV" ControlToValidate="textfield5"
                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The DOB is required!" />--%>
                                        <%--<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5"
                                            TargetControlID="DobV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />--%>
                                        <%--               <asp:RegularExpressionValidator ID="DobC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The DOB is not a date"
                                            ControlToValidate="textfield5" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                            Visible="true" Display="None"></asp:RegularExpressionValidator>
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6"
                                            TargetControlID="DobC" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />--%>
                                    </fieldset>
                                    <fieldset>
                                        <label>Alert 1(optional)</label>
                                        <asp:TextBox ID="txtBoxAlert1" runat="server" size="30"
                                            MaxLength="50" />
                                    </fieldset>

                                    <fieldset>
                                        <label>Status(optional)</label>
                                        <asp:TextBox ID="txtBoxAlert2" runat="server" size="30"
                                            MaxLength="50" />
                                    </fieldset>

                                    <fieldset>
                                        <label>Comments(optional)</label>
                                        <asp:TextBox ID="txtBoxComments" runat="server" size="30"
                                            MaxLength="50" />
                                    </fieldset>

                                    <fieldset>
                                        <label>Site 1(optional)</label>
                                        <asp:TextBox ID="txtBoxSite1" runat="server" size="30"
                                            MaxLength="50" />
                                    </fieldset>

                                    <fieldset>
                                        <label>Site 2(optional)</label>
                                        <asp:TextBox ID="txtBoxSite2" runat="server" size="30"
                                            MaxLength="50" />
                                    </fieldset>

                                    <fieldset>
                                        <label>Security</label>
                                        <asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Index_Changed">
                                            <asp:ListItem Text="Private" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Protected" Value="1" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Public" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox ID="folderCode" runat="server" MaxLength="20" TextMode="Password" Width="92" />
                                    </fieldset>


                                    <div runat="server" id="trgroups" style="padding: 5px 0">
                                        <div runat="server" id="divfield1" style="padding: 5px 0">
                                            <fieldset>
                                                <label style="width: auto !important; padding: 0 5px">Groups</label>
                                                <asp:CheckBox name="groupfield1" runat="server" Text=""
                                                    ID="groupfield1" size="30" />
                                                <asp:TextBox ID="grouplink1" Text="Group 1" runat="server" Height="30px" />
                                            </fieldset>

                                        </div>
                                        <div runat="server" id="divfield2" style="padding: 5px 0">
                                            <fieldset>
                                                <label>&nbsp;</label>
                                                <asp:CheckBox name="groupfield2" runat="server" Text=""
                                                    ID="groupfield2" size="30" />
                                                <asp:TextBox ID="grouplink2" Text="Group 2" runat="server" Height="30px" />
                                            </fieldset>
                                        </div>
                                        <div runat="server" id="divfield3" style="padding: 5px 0">
                                            <fieldset>
                                                <label>&nbsp;</label>
                                                <asp:CheckBox name="groupfield3" runat="server" Text=""
                                                    ID="groupfield3" size="30" />
                                                <asp:TextBox ID="grouplink3" Text="Group 3" runat="server" Height="30px" />
                                            </fieldset>
                                        </div>
                                        <div runat="server" id="divfield4" style="padding: 5px 0">
                                            <fieldset>
                                                <label>&nbsp;</label>
                                                <asp:CheckBox name="groupfield4" runat="server" Text=""
                                                    ID="groupfield4" size="30" />
                                                <asp:TextBox ID="grouplink4" Text="Group 4" runat="server" Height="30px" />
                                            </fieldset>
                                        </div>
                                        <div runat="server" id="divfield5" style="padding: 5px 0">
                                            <fieldset>
                                                <label>&nbsp;</label>
                                                <asp:CheckBox name="groupfield5" runat="server" Text=""
                                                    ID="groupfield5" size="30" />
                                                <asp:TextBox ID="grouplink5" Text="Group 5" runat="server" Height="30px" />
                                            </fieldset>
                                        </div>
                                    </div>

                                    <fieldset>
                                        <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="openDetails();return;"
                                            Text="Details" CssClass="Naslov_Y_Crven_2"></asp:LinkButton>
                                    </fieldset>

                                    <fieldset>
                                        <label>How many tabs do you want to create for the edFile?</label><br />
                                        <asp:DropDownList ID="DropDownList2" runat="server" CssClass="tabDropDown" AutoPostBack="true" OnSelectedIndexChanged="DropDownList2_OnSelectedChanged">
                                            <asp:ListItem Text="1" Value="1" Selected="true"></asp:ListItem>
                                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                            <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                            <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                            <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                            <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                            <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                            <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                            <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                        </asp:DropDownList>
                                        &nbsp;&nbsp;&nbsp;
                                       
                                        <asp:TextBox runat="server" CssClass="tempBox" ID="templateTextBox"></asp:TextBox>
                                        <asp:Button ID="btnSave" CssClass="create-btn btn-small green" runat="server" CausesValidation="false"
                                            Text="Insert" OnClick="OnClick_Save" />
                                    </fieldset>

                                    <div style="padding: 10px 0px">
                                        <asp:DataList ID="Repeater2" OnUpdateCommand="Repeater2_OnUpdateCommand" DataKeyField="Name"
                                            OnItemCreated="Repeater2_OnItemCreated" runat="server">
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <div>
                                                                <label style="padding: 0 5px">Name</label>
                                                                <asp:TextBox Style="padding: 0 5px; height: 30px" ID="textfield4" runat="server" Text='<%# Eval("Name")%>' />
                                                                <asp:RequiredFieldValidator Style="padding: 0 5px" runat="server" ID="textfield4Req" ControlToValidate="textfield4"
                                                                    Display="Dynamic" ErrorMessage="<b>Required Field Missing</b><br />Tab name is required." />
                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                                                    TargetControlID="textfield4Req" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div style="display: inline-flex">
                                                                <label class="color-label">Color</label>
                                                                <div style="border: solid 1px black; height: 32px; width: 70px">
                                                                    <asp:Label
                                                                        ID="lblSample"
                                                                        AutoCompleteType="None"
                                                                        runat="server"
                                                                        Style="display: block; height: 22px; width: 62px; margin: 3px;" />
                                                                </div>
                                                                <asp:TextBox
                                                                    ID="txtCardColor"
                                                                    AutoCompleteType="None"
                                                                    runat="server" Style="height: 0px; width: 0px; border: none" />
                                                                <asp:Button
                                                                    CssClass="cp_button"
                                                                    ID="btnPickColor"
                                                                    runat="server" />
                                                                <cc1:ColorPickerExtender
                                                                    ID="ColorPicker2"
                                                                    TargetControlID="txtCardColor"
                                                                    PopupButtonID="btnPickColor"
                                                                    PopupPosition="TopRight"
                                                                    SampleControlID="lblSample"
                                                                    SelectedColor='<%# Eval("Color")%>'
                                                                    OnClientColorSelectionChanged="Color_Changed"
                                                                    Enabled="True"
                                                                    runat="server"></cc1:ColorPickerExtender>
                                                            </div>
                                                        </td>
                                                    </tr>


                                                </table>
                                            </ItemTemplate>
                                            <AlternatingItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <div>
                                                                <label style="padding: 0 5px">Name</label>
                                                                <asp:TextBox ID="textfield8" Style="padding: 0 5px; height: 30px" runat="server" Text='<%# Eval("Name")%>' />
                                                                <asp:RequiredFieldValidator runat="server" ID="textfield8Req" ControlToValidate="textfield8"
                                                                    Display="Dynamic" ErrorMessage="<b>Required Field Missing</b><br />Tab name is required." />
                                                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
                                                                    TargetControlID="textfield8Req" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div style="display: inline-flex">
                                                                <label class="color-label">Color</label>
                                                                <div style="border: solid 1px black; height: 32px; width: 70px">
                                                                    <asp:Label
                                                                        ID="lblSample1"
                                                                        AutoCompleteType="None"
                                                                        runat="server" Style="display: block; height: 22px; width: 62px; margin: 3px;" />
                                                                </div>
                                                                <asp:TextBox
                                                                    ID="txtCardColor1"
                                                                    AutoCompleteType="None"
                                                                    runat="server" Style="height: 0px; width: 0px; border: none" />
                                                                <asp:Button
                                                                    CssClass="cp_button"
                                                                    ID="btnPickColor1"
                                                                    runat="server" />
                                                                <cc1:ColorPickerExtender
                                                                    ID="ColorPicker3"
                                                                    TargetControlID="txtCardColor1"
                                                                    PopupButtonID="btnPickColor1"
                                                                    PopupPosition="TopRight"
                                                                    SampleControlID="lblSample1"
                                                                    SelectedColor='<%# Eval("Color")%>'
                                                                    OnClientColorSelectionChanged="Color_Changed"
                                                                    Enabled="True"
                                                                    runat="server"></cc1:ColorPickerExtender>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                </div>
                                           
                                            </AlternatingItemTemplate>
                                        </asp:DataList>
                                    </div>

                                    <fieldset>
                                        <label>
                                            <%-- <asp:CheckBox ID="chkQA" Text="Do you want checklist form?" runat="server" onclick="javascript:$('#divQuestion').toggle();" oncheckedchanged="chkQA_CheckedChanged"/>--%>
                                            <%--<asp:CheckBox ID="chkQA" Text="Do you want checklist form?" runat="server" onclick="ShowchkQDiv(this)" />--%>
                                            <asp:CheckBox ID="chkQA" Text="Do you want checklist form?" AutoPostBack="True" runat="server" OnCheckedChanged="chkQA_CheckedChanged" />
                                        </label>
                                    </fieldset>
                                    <%-- <div style="padding: 10px 0px; display: none;" id="divQuestion" runat="server">--%>
                                    <div style="padding: 10px 0px" id="divQuestion" runat="server">
                                        <table style="margin-bottom: 10px; margin-left: -19px;">
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblFolderName1" runat="server" Style="font-size: 15px; font-weight: 500; color: black; margin-left: 30px;" CssClass="FolderName">Folder: </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlFolderName" runat="server" Style="overflow: scroll; width: 200px; height: 30px;" CssClass="FolderName" onchange="getFolderId(this.value)"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblFolderLogName" runat="server" Style="font-size: 15px; font-weight: 500; color: black; margin-left: 63px; display: none;" CssClass="FolderLogName">Folder Log: </asp:Label>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlFolderLog" runat="server" Style="overflow: scroll; width: 200px; height: 30px; display: none;" CssClass="FolderLogName" onchange="getFolderLogId(this.value)"></asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>

                                        <table border="0" width="100%" id="tblBRD" style="margin: 0">
                                            <tr>
                                                <td width="15%" style="padding-top: 5px">Question </td>
                                                <td>
                                                    <input type="text" id="tbxQuestion1" name="tbxQuestion1" class="question" style="padding: 0 5px; height: 30px; width: 100%" />
                                                </td>
                                                <td style="padding-top: 7px">
                                                    <a href="javascript:;" id="btnADDBRD">
                                                        <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/add-icon.png" class="btnAdd"
                                                            alt="Add" title="Add More" /></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <fieldset>
                                        <label>&nbsp;</label>
                                        <div style="text-align: center">
                                            <asp:Button runat="server" ID="ImageButton1" OnClick="ImageButton1_Click" CssClass="create-btn btn green checkQuestion" OnClientClick="btnCreateClick()" Text="Create" />
                                        </div>
                                    </fieldset>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="DropDownList2" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="DropDownList3" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                <asp:AsyncPostBackTrigger ControlID="ddlFolderName" EventName="SelectedIndexChanged" />
                                <asp:AsyncPostBackTrigger ControlID="ddlFolderLog" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </asp:Panel>



                    <asp:Panel ID="panelConfirm" runat="server" Visible="false">
                        <div class="content-box listing-view">
                            <asp:Label runat="server">Folder was created successfully. Next Step:</asp:Label>
                            <asp:Label ID="lbConfirm" runat="server" />
                            <p>
                                Do you want to
                                               
                                <asp:HyperLink ID="btnUpload" runat="server" CssClass="foot_url2 edFileLink" Text="Upload Documents"
                                    NavigateUrl="~/Office/FileFolderBox.aspx"></asp:HyperLink>
                                ,
                                               
                                <asp:HyperLink ID="btnNew" runat="server" CssClass="foot_url2" Text="Create New edFile"
                                    NavigateUrl="~/Office/NeweFileFolder.aspx"></asp:HyperLink>
                                or
                                               
                                <asp:HyperLink ID="btnsplit" runat="server" CssClass="foot_url2" Text="Split Document"
                                    NavigateUrl="~/Office/eFileSplit.aspx"></asp:HyperLink>
                                ?
                           
                            </p>
                            <input type="button" class="create-btn btn-small green" id="btnScandoc" name="btnScandoc" runat="server" causesvalidation="false" value="Scan Document" style="margin-top: 15px" onclick="ScanDocFile();" />
                        </div>
                    </asp:Panel>
                </div>
                <div class="right-content">
                    <%-- <div class="quick-find">
                        <uc1:QuickFind ID="QuickFind1" runat="server" />
                    </div>--%>
                    <div class="quick-find" style="margin-top: 15px;">
                        <div class="find-inputbox">
                            <marquee scrollamount="1" id="marqueeside" runat="server"
                                behavior="scroll" onmouseover="this.stop()" onmouseout="this.start()" scrolldelay="20"
                                direction="up">
                               <%=SideMessage %></marquee>
                        </div>
                    </div>
                </div>
            </div>

            <div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        //This code must be placed below the ScriptManager, otherwise the "Sys" cannot be used because it is undefined.
        Sys.Application.add_init(function () {
            // Store the color validation Regex in a "static" object off of
            // AjaxControlToolkit.ColorPickerBehavior.  If this _colorRegex object hasn't been
            // created yet, initialize it for the first time.
            if (!Sys.Extended.UI.ColorPickerBehavior._colorRegex) {
                Sys.Extended.UI.ColorPickerBehavior._colorRegex = new RegExp('^[A-Fa-f0-9]{6}$');
            }
        });
    </script>

    <script type="text/javascript">

        $(document).ready(function () {
            $('.iframe').colorbox({
                iframe: true, width: "80%", height: "95%", onComplete: function () {
                    //iframe: true, width: aWidth, height: aHeight, onComplete: function () {
                    $('iframe').live('load', function () {
                        $('iframe').contents().find("head")
                            .append($("<style type='text/css'>  .splash{overflow:visible;}  </style>"));
                    });
                }
            });
            //    alert(1);
            //    $('#ctl00_ContentPlaceHolder1_chkQA').prop("checked",false)
        });

        //function ShowchkQDiv(ele) {

        //    // if ($('#chkQA').prop("checked") == true)
        //    if ($(ele).prop("checked") == true) {
        //        $("#divQuestion").show();
        //    }
        //    else {
        //        $("#divQuestion").hide();
        //    }
        //}
        var fileid = '<%=Request.QueryString["FolderId"]%>';
        function ScanDocFile() {
            //var fileid = $('#' + '<%=hdnfolderId.ClientID%>').val();
            $('.iframe').attr('href', 'UploadScanDocNew.aspx?id=' + fileid + '&page=filefolderbox').trigger('click');
        }

        $('#btnADDBRD').die().live('click', function () {
            var $newRow = $('#tblBRD tbody tr:last').clone(true);

            var question = $(this).closest('tr').find('.question').val();
            if (question == "") {
                alert("Please enter valid question.");
                return false;
            }
            $('#tblBRD tbody tr:last').after($newRow);
            var addedRow = $('#tblBRD tbody tr:last').find('input');
            //$('#tblBRD tbody tr:last td:eq(0)').text('Question ' + ($('#tblBRD tbody tr').size()))
            $(addedRow).each(function (i) {
                var name = '';
                switch (i) {
                    case 0: name = 'tbxQuestion' + Math.random();
                }
                $(this).val('').prop('id', name).prop('name', name);
            });
            $(this).after('<a href="javascript:;" id="btnDeleteBRD"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" /></a>');
            $(this).remove();
        });

        $('#btnDeleteBRD').die().live('click', function () {
            var tblTRCount = $('#tblBRD tbody tr').size();
            if (tblTRCount != 1)
                $(this).parents().eq(1).remove();
        });

        function SetQuestions(items) {
            if (items) {
                var obj = JSON.parse(items);
                for (var i = 0; i < obj.length - 1; i++) {
                    $('#btnADDBRD').trigger('click');
                }

                $('#tblBRD tbody tr').each(function (i) {
                    $(this).find('input[type="text"]').val(obj[i]);
                });
            }
        }

        $(window).scroll(function () {
            $('#overlay:visible') && $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        });

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');;
        }
        function hideLoader() {
            $('#overlay').hide();
        }

        function btnCreateClick() {
            if (Page_ClientValidate()) {
                showLoader();
            }
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    GetFolderNameDetails();
                    $('.question').keypress(function (event) {
                        var keycode = (event.keyCode ? event.keyCode : event.which);
                        if (keycode == 46 || keycode == 190) {
                            event.preventDefault();
                        }
                    });

                }
            });
        };

        function GetFolderNameDetails() {
            var obj = {};
            var cntQue = 0;
            var officeId = $('#' + '<%=hdnOfficeId.ClientID%>').val();
            $.ajax({
                type: "POST",
                url: "FileFolderBox.aspx/GetFolderDetailsById",
                data: '{"officeId":"' + officeId + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var array = [];
                    if (result.d) {
                        var ddlFolder = $("[id*=ddlFolderName]");
                        ddlFolder.empty().append('<option selected="selected" value="0">Please select</option>');
                        $.each(result.d, function () {
                            ddlFolder.append($("<option></option>").val(this['folderId']).html(this['folderName']));
                        });
                    }
                }
            });
        }

        function pageLoad() {
            $('.edFileLink').click(function () {
                ClearFolderIdSource();
            });
        }

        function ClearFolderIdSource() {
            $.ajax(
                {
                    type: "POST",
                    url: '../EFileFolderJSONService.asmx/ClearFolderIdSource',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json"
                });
        }

        function getFolderId(value) {
            $('.FolderLogName').show();
            var folderId = value;
            $.ajax({
                type: "POST",
                url: "FileFolderBox.aspx/GetFolderLogDetailsById",
                data: '{"folderId":"' + folderId + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var array = [];
                    if (result.d) {
                        var ddlFolderLog = $("[id*=ddlFolderLog]");
                        ddlFolderLog.empty().append('<option selected="selected" value="0">Please select</option>');
                        $.each(result.d, function () {
                            ddlFolderLog.append($("<option></option>").val(this['folderLogId']).html(this['folderLogName']));
                        });
                    }
                }
            });
            return false;
        }

        function getFolderLogId(value) {
            var folderLogId = value;
            $.ajax({
                type: "POST",
                url: "FileFolderBox.aspx/GetQuestionByLogID",
                data: '{"folderLogId":"' + folderLogId + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var array = [];

                    if (result.d) {
                        $.each(result.d, function () {
                            var isInsert = false;
                            var queCnt = $('.question').length;
                            if (queCnt > 0) {
                                for (i = 0; i < queCnt; i++) {
                                    var que = $('.question').eq(i).val().trim();
                                    if (que.toLowerCase().trim() != this['que'].toLowerCase().trim()) {
                                        isInsert = true;
                                    }
                                    else {
                                        isInsert = false;
                                        break;
                                    }
                                }
                                if (isInsert) {
                                    if (que == "") {
                                        $('#tblBRD tbody tr:last').find('input').attr("rank", 0);
                                        $('#tblBRD tbody tr:last').find('input').removeAttr('rank');
                                    }
                                    AppendQuestionDetails(this['que'], 0);
                                }
                            }
                            else {
                                if ($('#tblBRD tbody tr:last').find('input').attr('rank') == 1) {
                                    $('#tblBRD tbody tr:last').find('input').attr("rank", 0);
                                    $('#tblBRD tbody tr:last').find('input').removeAttr('rank');
                                }
                                AppendQuestionDetails(this['que'], 0);
                            }
                        });

                    }
                }
            });

        }


        function AppendQuestionDetails(que, queId) {
            if ($('#tblBRD tbody tr:last').find('input').attr('rank') == 1) {
                $('#tblBRD tbody tr:last').find('input').removeAttr('rank');
                var $newRow = $('#tblBRD tbody tr:last').clone(true);
                $($newRow).find('input').val(que);
                $($newRow).find('input').attr("QuesId", queId);
                $($newRow).find('input').attr("rank", 1);
                $('#tblBRD tbody tr:last').find('input').attr("id", 'tbxQuestion' + Math.random());
                $('#tblBRD tbody tr:last').find('input').attr("name", 'tbxQuestion' + Math.random());
                $('#tblBRD tbody tr:last').find('td:last').append('<a id="btnDeleteBRD"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" /></a>');
                $('#tblBRD tbody tr:last').find('td:last').find('#btnADDBRD').remove();
                $('#tblBRD tbody tr:last').find('input').removeAttr('rank');
                $('#tblBRD tbody tr:last').after($newRow);
            }
            else {
                $('#tblBRD tbody tr:last').find('input').val(que);
                $('#tblBRD tbody tr:last').find('input').attr("QuesId", queId);
                $('#tblBRD tbody tr:last').find('input').attr("id", 'tbxQuestion' + Math.random());
                $('#tblBRD tbody tr:last').find('input').attr("name", 'tbxQuestion' + Math.random());
                $('#tblBRD tbody tr:last').find('input').attr("rank", 1);
            }
        }

        function checkQuestion(currentQuestion) {
            var queCnt = $('.question').length;
            if (queCnt == 0)
                return true;
            for (i = 0; i < queCnt; i++) {
                var Que = $('.question').eq(i).val();
                if (currentQuestion == Que) {
                    alert("Last question is exist.Please enter another question.");
                    return false;
                }
                else
                    return true;
            }
        }
        var check = false;

        function checkfolderlogs() {
            var cnt = 0;
            if ($('.Log').val() != "") {
                var queCnt = $('.question').length;
                for (i = 0; i < queCnt; i++) {
                    var Que = $('.question').eq(i).val();
                    if (Que == "") {
                        cnt += 1;
                    }
                }
                if (cnt == queCnt && cnt > 0) {
                    check = false;
                    alert("Please enter at least one question");
                }
            }
            else {
                cnt += 1;
                check = false;
                alert("Please Enter Valid Folder Log Name.");
            }
            if (cnt == 0) {
                check = true;
            }
        }

        $('.question').keypress(function (event) {
            alert("Hii");
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == 46 || keycode == 190) {
                event.preventDefault();
            }
        });

        function Color_Changed(sender) {
            sender.get_element().value = "#" + sender.get_selectedColor();
        }

    </script>
    <a class='iframe' href="javascript:;" style="display: none;">popup</a>
</asp:Content>
