using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Drawing;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;
using TextBox = System.Web.UI.WebControls.TextBox;
using Shinetech.DAL;
using AjaxControlToolkit;

public partial class NeweFileFolder : OverLicensePage
{
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!string.IsNullOrEmpty(Request.QueryString["FolderId"]))
        {
            panelMain.Visible = false;
            panelConfirm.Visible = true;
            string folderId = Request.QueryString["FolderId"].ToString();
            DataTable folderTable = FileFolderManagement.GetFolderInforByEFFID(folderId);
            DataTable dtDividers = FileFolderManagement.GetDividerByEffid(Convert.ToInt32(folderId));
            if (dtDividers != null && dtDividers.Rows.Count > 0)
            {
                this.DividerID_FFB = dtDividers.Rows[0]["DividerId"].ToString();
                this.EffID_FFB = folderId;
            }
            //display confirm page to load document
            lbConfirm.Text = string.Format("The edFile {0} has been created successfully.", folderTable.Rows[0]["FolderName"].ToString());
            btnUpload.NavigateUrl = btnUpload.NavigateUrl + "?id=" + folderId;
            hdnfolderId.Value = folderId;
            ImageButton1.Visible = false;
        }

        if (!Page.IsPostBack)
        {
            divQuestion.Visible = false;
            folderCode.Visible = true;
            trgroups.Visible = false;
            hdnOfficeId.Value = Convert.ToString(Sessions.SwitchedSessionId);

            //this.CalendarExtender3.SelectedDate = DateTime.Now;
            Dividers.Clear();

            string name = "Tab_Name1";
            DividerEntity entity = new DividerEntity(name);
            entity.Color = Colors[0];
            Dividers.Add(entity);

            this.Repeater2.DataSource = this.Dividers;
            this.Repeater2.DataBind();

            this.DropDownList3.DataSource = this.Templates;
            this.DropDownList3.DataBind();

            this.DropDownList2.Visible = true;
            this.btnSave.Text = "Save";

            InitTextBox();

            //GetFolderDetailsById();

            // 卢远宗 2014-10-7添加Groups
            string officeID1 = Sessions.SwitchedRackspaceId;
            //if (this.Page.User.IsInRole("Offices"))
            //{
            //    officeID1 = Membership.GetUser().ProviderUserKey.ToString();
            //}

            //else if (this.Page.User.IsInRole("Users"))
            //{
            //    DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(Membership.GetUser().ProviderUserKey.ToString());
            //    officeID1 = dtOfficeUser.Rows[0]["OfficeUID"].ToString();
            //    this.btnSave.Visible = false;
            //}

            Account officeUser = new Account(officeID1);
            string groupsName = officeUser.GroupsName.Value;

            //自定义Groups名称
            if (!Common_Tatva.IsUserSwitched() && groupsName != null && !string.IsNullOrEmpty(groupsName.Trim()))
            {
                string[] groupNames = groupsName.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 1; i <= groupNames.Length; i++)
                {
                    TextBox groupText = UpdatePanel2.FindControl("grouplink" + i) as TextBox;
                    groupText.Text = groupNames[i - 1];
                }
            }

            //仅显示该User的Groups 的CheckBox
            if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users"))
            {
                this.btnSave.Visible = false;
                var uid = Sessions.SwitchedSessionId;
                var act = new Shinetech.DAL.Account(uid);
                string groups = act.Groups.Value == null ? "" : act.Groups.Value;

                if (groupsName != null && !string.IsNullOrEmpty(groupsName.Trim()))
                {
                    string[] groupNames = groupsName.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 1; i <= groupNames.Length; i++)
                    {
                        TextBox groupText = UpdatePanel2.FindControl("grouplink" + i) as TextBox;

                        groupText.Text = groupNames[i - 1];
                    }
                }

                for (int i = 1; i <= 5; i++)
                {
                    TextBox groupText = UpdatePanel2.FindControl("grouplink" + i) as TextBox;
                    CheckBox groupfield = UpdatePanel2.FindControl("groupfield" + i) as CheckBox;

                    groupText.Visible = groups.Contains(i.ToString());
                    groupfield.Visible = groups.Contains(i.ToString());
                    UpdatePanel2.FindControl("divfield" + i).Visible = groups.Contains(i.ToString());

                }

            }

        }

        //  txtFolderName.Attributes.Add("onblur", "document.getElementById('" + textfield4. + "').innerHTML='Thank you';");

    }

    private bool BelongToGroup(string p, string[] groups)
    {
        foreach (string group in groups)
        {
            if (p.Contains(group))
            {
                return true;
            }
        }

        return false;
    }

    public string SideMessage
    {
        get
        {
            return this.Session["_Side_Message"] as string;
        }

        set
        {
            this.Session["_Side_Message"] = value;
        }
    }

    protected void InitTextBox()
    {
        this.drpState.DataSource = States;
        this.drpState.DataValueField = "StateID";
        this.drpState.DataTextField = "StateName";
        this.drpState.DataBind();
        this.drpState.SelectedValue = "CA";

    }

    public DataTable States
    {
        get
        {
            object obj = Application["States"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetStates();
                Application["States"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    public Dictionary<int, string> Colors
    {
        get
        {
            object obj = this.Application["Colors"];
            if (obj == null)
            {
                Dictionary<int, string> colors = new Dictionary<int, string>();
                colors.Add(0, "FF0000");
                colors.Add(1, "FF9933");
                colors.Add(2, "CC0066");
                colors.Add(3, "CC9999");
                colors.Add(4, "9900CC");
                colors.Add(5, "9999FF");
                colors.Add(6, "663300");
                colors.Add(7, "66CC33");
                colors.Add(8, "333366");
                colors.Add(9, "33CC99");
                colors.Add(10, "0033CC");
                colors.Add(11, "00CCFF");

                obj = colors;
                this.Application["Colors"] = obj;
            }

            return obj as Dictionary<int, string>;
        }
        set
        {
            this.Application["Colors"] = value;
        }
    }

    protected void ImageButton1_Click(object sender, EventArgs e)
    {
        string strFolderName = this.txtFolderName.Text.Trim();
        string firstname = this.textfield3.Text.Trim();
        string lastname = this.TextBox1.Text.Trim();
        string slevel = this.DropDownList1.SelectedValue;
        string date = this.textfield5.Text.Trim();
        string code = this.folderCode.Text.Trim();
        string groups = string.Empty;
        List<String> questions = new List<String>();

        CheckListQuestion objCheckListQuestion = new CheckListQuestion();
        objCheckListQuestion = new CheckListQuestion();
        if (chkQA.Checked)
        {
            for (int i = 34; i < Request.Form.AllKeys.Length; i++)
            {
                if (Request.Form.AllKeys[i].Contains("tbxQuestion") && !string.IsNullOrEmpty(Request.Form.AllKeys[i]) && Request.Form[i] != "")
                {
                    questions.Add(Convert.ToString(Request.Form[i]));
                }
            }
        }

        for (int i = 1; i <= 5; i++)
        {
            CheckBox groupBox = UpdatePanel2.FindControl("groupfield" + i) as CheckBox;
            if (groupBox != null && groupBox.Checked)
            {
                groups += i + "|";
            }
        }


        DataListItemCollection items = this.Repeater2.Items;
        Dictionary<string, Divider> list = new Dictionary<string, Divider>();
        if (items.Count == 0)
        {
            Divider newEntity = new Divider();
            newEntity.Name.Value = strFolderName;
            newEntity.Color.Value = "#e78302";
            list.Add(strFolderName, newEntity);
        }
        else
        {
            string color, tabname;
            TextBox box;
            TextBox boxColor;
            ColorPickerExtender colorpicker;
            int i = 0;
            foreach (DataListItem item in items)
            {
                box = item.Controls[1] as TextBox;
                colorpicker = item.Controls[13] as ColorPickerExtender;
                tabname = box.Text.Trim();
                boxColor = colorpicker.FindControl(colorpicker.TargetControlID) as TextBox;
                color = (string.IsNullOrEmpty(boxColor.Text) ? Colors[i] : boxColor.Text);

                if (list.ContainsKey(tabname))
                {
                    string strWrongInfor = string.Format("Error : Cannot create folder, each tab must have a unique name.", tabname);
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");SetQuestions('" + Newtonsoft.Json.JsonConvert.SerializeObject(questions) + "');hideLoader();", true);
                    divQuestion.Visible = true;
                    return;
                }
                else
                {
                    Divider newEntity = new Divider();
                    newEntity.Name.Value = tabname;
                    newEntity.Color.Value = (color.Length > 7) ? color.Substring(0, 7) : color;
                    list.Add(tabname, newEntity);
                }
                i++;
            }
        }


        try
        {
            FileFolder folder = new FileFolder();
            folder.FolderName.Value = strFolderName;
            folder.FirstName.Value = firstname;
            folder.Lastname.Value = lastname;

            //New Field for Folder in 2012-10-28

            folder.Email.Value = this.textfield13.Text.Trim();
            folder.Tel.Value = this.textfieldTel.Text.Trim();
            folder.FaxNumber.Value = this.textfield9.Text.Trim();

            folder.Address.Value = this.textfield10.Text.Trim();
            folder.StateID.Value = this.drpState.SelectedValue;
            folder.OtherState.Value = this.txtOtherState.Text.Trim();
            folder.City.Value = this.txtCity.Text.Trim();
            folder.Postal.Value = this.txtPostal.Text.Trim();


            folder.FileNumber.Value = this.TextBoxFileNumber.Text.Trim();
            folder.SecurityLevel.Value = Convert.ToInt32(slevel);

            folder.Alert1.Value = this.txtBoxAlert1.Text.Trim();
            folder.Alert2.Value = this.txtBoxAlert2.Text.Trim();
            folder.Alert3.Value = "";// this.txtBoxAlert3.Text.Trim();
            folder.Alert4.Value = "";//this.txtBoxAlert4.Text.Trim();
            folder.Comments.Value = this.txtBoxComments.Text.Trim();

            folder.TemplateId.Value = Convert.ToInt32(this.DropDownList3.SelectedItem.Value) == -1 ? 0 : Convert.ToInt32(this.DropDownList3.SelectedItem.Value);
            if (folder.SecurityLevel.Value == 2)
            {
                folder.FolderCode.Value = code;
            }

            //add by 卢远宗 2014-10-7
            folder.Groups.Value = groups;

            folder.DOB.Value = textfield5.Text;
            folder.CreatedDate.Value = DateTime.Now;
            folder.OfficeID.Value = Sessions.SwitchedSessionId;
            folder.OfficeID1.Value = Sessions.SwitchedRackspaceId;
            folder.Site1.Value = txtBoxSite1.Text;
            folder.Site2.Value = txtBoxSite2.Text;

            //if (this.Page.User.IsInRole("Offices"))
            //{
            //    folder.OfficeID1.Value = Membership.GetUser().ProviderUserKey.ToString();
            //}

            //else if (this.Page.User.IsInRole("Users"))
            //{
            //    DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(Membership.GetUser().ProviderUserKey.ToString());
            //    folder.OfficeID1.Value = dtOfficeUser.Rows[0]["OfficeUID"].ToString();
            //}

            string uid = folder.OfficeID1.Value;
            DataTable isExist = FileFolderManagement.IsFolderNameExist(uid, folder.FolderName.Value, folder.FileNumber.Value);
            if (isExist == null || isExist.Rows.Count == 0)
            {
                int newfolderId = FileFolderManagement.CreatNewFileFolder(folder, list);

                if (chkQA.Checked)
                {

                    string folderlogId = General_Class.InsertFolderlog(Convert.ToString(newfolderId), "Default");

                    DataTable dtChkQuestion = new DataTable();
                    dtChkQuestion.Columns.Add("FolderId", typeof(string));
                    dtChkQuestion.Columns.Add("FolderLogId", typeof(int));
                    dtChkQuestion.Columns.Add("Question", typeof(string));
                    dtChkQuestion.Columns.Add("createdon", typeof(DateTime));
                    var createdOn = DateTime.Now;

                    if (folderlogId != null)
                    {
                        //CheckListQuestion objCheckListQuestion = new CheckListQuestion();
                        for (int i = 34; i < Request.Form.AllKeys.Length; i++)
                        {
                            if (Request.Form.AllKeys[i].Contains("tbxQuestion") && !string.IsNullOrEmpty(Request.Form.AllKeys[i]) && Request.Form[i] != "")
                            {
                                if (!string.IsNullOrEmpty(Request.Form[i].ToString()))
                                {
                                    //DataRow[] drw = dtChkQuestion.Select("UPPER(LTRIM(RTRIM(Question))) Like '" + Request.Form[i].ToUpper().Trim() + "' + '%'");
                                    DataRow[] drw = dtChkQuestion.Select("Question Like '" + Request.Form[i] + "' + '%'");
                                    if (drw.Length == 0)
                                        dtChkQuestion.Rows.Add(new object[] { newfolderId, folderlogId, Request.Form[i], createdOn });
                                }
                            }
                        }
                        if (dtChkQuestion != null && dtChkQuestion.Rows.Count > 0)
                        {
                            //Insert Question
                            string cntFolderLog = General_Class.InsertQuestion(dtChkQuestion, Convert.ToString(newfolderId));
                        }
                    }
                }
                //成功创建folder后，设置storage charge 
                if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Offices"))
                {
                    Account account = new Account(Sessions.SwitchedRackspaceId);
                    if (account.IsExist)
                    {
                        if (account.LicenseID.Value != -1 && account.StorageChargeFlag.Value == "N")//表明客户已买license
                        {
                            try
                            {
                                account.StorageChargeFlag.Value = "Y";
                                account.StoragePaymentDate.Value = DateTime.Now; //以这天为准开始收storage fee
                                account.Update();
                            }
                            catch { }

                        }

                        if (account.LicenseID.Value == -1 && account.CreateDate.Value.AddDays(TrialDays) < DateTime.Now
                            && account.StorageChargeFlag.Value == "N") //如果是trial用户则
                        {
                            account.StorageChargeFlag.Value = "Y";
                            account.StoragePaymentDate.Value = DateTime.Now; //以这天为准开始收storage fee
                            account.Update();
                        }
                    }
                }
                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(newfolderId, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Create.GetHashCode(), 0, 0);
                Response.Redirect("NeweFileFolder.aspx?FolderId=" + newfolderId.ToString());
            }
            else
            {
                string strWrongInfor = string.Format("This EdFile cannot be created, the File Number you entered already exsists in your account.", strFolderName);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");hideLoader();", true);
            }
        }
        catch (Exception exp)
        {
            string strWrongInfor = string.Format("Error : the edFile name {0} failed to create, try again.", strFolderName);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");hideLoader();", true);
            return;
        }
    }

    protected void DropDownList2_OnSelectedChanged(object sender, EventArgs e)
    {
        int count = Convert.ToInt32(DropDownList2.SelectedValue.Trim());

        if (count > Dividers.Count)
        {
            int nCount = count - Dividers.Count;
            int number = Dividers.Count;
            for (int i = 0; i < nCount; i++)
            {
                string name = "Tab_Name" + ++number;
                DividerEntity entity = new DividerEntity(name);
                entity.Color = Colors[number - 1];
                Dividers.Add(entity);
            }

        }
        else if (count < Dividers.Count)
        {
            int nCount = Dividers.Count - count;
            Dividers.RemoveRange(count, nCount);
        }


        this.Repeater2.DataSource = this.Dividers;
        this.Repeater2.DataBind();
    }

    public List<DividerEntity> Dividers
    {
        get
        {
            List<DividerEntity> obj = this.Session["Dividers"] as List<DividerEntity>;

            if (obj == null)
            {
                obj = new List<DividerEntity>();
                this.Session["Dividers"] = obj;
            }
            return obj;
        }

        set
        {
            this.Session["Dividers"] = value;
        }
    }



    protected void Repeater2_OnItemCreated(object sender, DataListItemEventArgs e)
    {
        ColorPickerExtender control;
        DataListItem item;

        switch (e.Item.ItemType)
        {
            case ListItemType.Item:
                item = e.Item;
                control = item.FindControl("ColorPicker2") as ColorPickerExtender;
                control.ID = control.ID + item.ItemIndex;
                //control.SelectedColor = "FF0000";
                break;
            case ListItemType.AlternatingItem:
                item = e.Item;
                control = item.FindControl("ColorPicker3") as ColorPickerExtender;
                control.ID = control.ID + item.ItemIndex;
                //control.SelectedColor = "FF9933";
                break;
        }
    }

    protected void Repeater2_OnUpdateCommand(object source, DataListCommandEventArgs e)
    {

        DataListItem item = e.Item;
    }

    protected void OnClick_Save(object sender, EventArgs e)
    {
        try
        {
            Dictionary<string, DividerEntity> list = new Dictionary<string, DividerEntity>();
            DataListItemCollection items = this.Repeater2.Items;
            int tid = Convert.ToInt32(DropDownList3.SelectedValue);
            if (templateTextBox.Text == "")
            {
                string strWrong = "Error : the template name is empty, please input.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, typeof(UpdatePanel), "alertok", "alert(\"" + strWrong + "\");", true);
                return;
            }
            if (items.Count == 0)
            {
                string strWrong = "Error : the tab number is empty, please input.";
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrong + "\");", true);
                return;
            }
            else
            {

                string color, tabname;
                TextBox box;
                TextBox boxColor;
                ColorPickerExtender colorpicker;
                foreach (DataListItem item in items)
                {
                    box = item.Controls[1] as TextBox;
                    colorpicker = item.Controls[7] as ColorPickerExtender;
                    boxColor = colorpicker.FindControl(colorpicker.TargetControlID) as TextBox;
                    tabname = box.Text.Trim();
                    color = boxColor.Text;

                    if (list.ContainsKey(tabname))
                    {
                        //string strWrongInfor = string.Format("Error : the tab name {0} has existed, try another.", tabname);
                        string strWrongInfor = string.Empty;
                        if (tid > 0)
                            strWrongInfor = string.Format("Error : Cannot update template, each tab must have a unique name.");
                        else
                            strWrongInfor = string.Format("Error : Cannot create template, each tab must have a unique name.");
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                        return;
                    }
                    else
                    {
                        DividerEntity newEntity = new DividerEntity(tabname);
                        newEntity.Color = color;

                        list.Add(tabname, newEntity);
                    }
                }
            }

            string uid = Sessions.SwitchedRackspaceId;

            string content = "";

            foreach (DividerEntity item in list.Values)
            {
                content += item.Name + "|";
                content += item.Color + ";";
            }

            int tcount = 0;//DropDownList3.Items.Count;

            ArrayList listTemplates = this.Templates;

            foreach (TemplateEntity t in listTemplates)
            {
                if (t.OfficeID.ToLower() == uid.ToLower())
                {
                    tcount++;
                }
            }



            bool isExist = true;
            isExist = FileFolderManagement.IsTemplateNameExist(uid, templateTextBox.Text, tid);

            //if (this.Page.User.IsInRole("Offices"))
            //{
            //}
            //else if (this.Page.User.IsInRole("Users"))
            //{
            //    DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(uid);
            //    var officeID1 = dtOfficeUser.Rows[0]["OfficeUID"].ToString();
            //    isExist = FileFolderManagement.IsTemplateNameExist(officeID1, templateTextBox.Text, tid);
            //}

            if (!isExist)
            {
                if (tid > 0) // amd officeuserid = uid
                {
                    DataTable newList = new DataTable();
                    newList.Columns.AddRange(new DataColumn[2] {new DataColumn("Name", typeof(string)),
                                                                new DataColumn("Color",typeof(string))
                                                              });
                    if (list.Count > DividersCount)
                    {
                        int number = list.Count - DividersCount;
                        int i = list.Count;
                        foreach (DividerEntity item in list.Values)
                        {
                            if (i < number + 1)
                            {
                                newList.Rows.Add(item.Name, item.Color);
                            }
                            i--;
                        }
                    }
                    DataTable fileFolders = General_Class.GetFoldersByTemplateId(tid);
                    if (fileFolders != null && fileFolders.Rows.Count > 0)
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.AddRange(new DataColumn[4] { new DataColumn("EFFID", typeof(int)),
                                                                new DataColumn("Name", typeof(string)),
                                                                new DataColumn("Color",typeof(string)),
                                                                new DataColumn("OfficeId",typeof(string))
                                                              });
                        foreach (DataRow dr in fileFolders.Rows)
                        {
                            foreach (DataRow drNew in newList.Rows)
                            {
                                dt.Rows.Add(Convert.ToInt32(dr["FolderID"].ToString()), drNew["Name"].ToString(), drNew["Color"].ToString(), dr["OfficeId"].ToString());
                            }
                        }
                        General_Class.AddDividers(dt);
                    }
                    FileFolderManagement.UpdateFolderTemplate(tid, templateTextBox.Text, content);
                    DividersCount = list.Count;
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Edit.GetHashCode(), tid, 0);
                }
                else
                {
                    TemplateEntity template = new TemplateEntity(templateTextBox.Text);
                    template.OfficeID = uid;
                    template.Content = content;

                    tid = FileFolderManagement.SaveFolderTemplate(template);

                    if (tid == -1)
                    {
                        string strWrongInfor = "Error : the creation of template failed.";
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                        return;
                    }
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Create.GetHashCode(), tid, 0);
                    this.btnSave.Text = "Update";
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel2, typeof(UpdatePanel), "Redirect", "alert('Error : Can not create template, this name template already exists.');", true);
            }

            DropDownList3.DataSource = this.Templates;
            DropDownList3.DataBind();
            DropDownList3.SelectedValue = tid.ToString();

            UpdatePanel2.Update();
        }
        catch (Exception ex) { }
    }

    public int DividersCount
    {
        get
        {
            var obj = this.Session["DividersCount"];

            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(this.Session["DividersCount"]);
            }
        }

        set
        {
            this.Session["DividersCount"] = value;
        }
    }

    protected ArrayList Templates
    {
        get
        {
            string uid = Sessions.SwitchedRackspaceId;
            ArrayList temples = new ArrayList();
            TemplateEntity template;
            DataTable table = FileFolderManagement.GetTemplatesByUID(uid);
            foreach (DataRow item in table.Rows)
            {
                template = new TemplateEntity(item["Name"] as string);
                template.TemplateID = (int)item["TemplateID"];
                template.OfficeID = uid;
                template.Content = item["Content"] as string;
                template.IsLocked = string.IsNullOrEmpty(Convert.ToString(item["IsLocked"])) ? false : Convert.ToBoolean(item["IsLocked"]);

                temples.Add(template);
            }

            template = new TemplateEntity("-- Options --");
            template.TemplateID = -1;
            template.OfficeID = uid;
            template.Content = "";

            temples.Insert(0, template);

            return temples;
        }
    }

    protected void OnSelectedIndexChaned_Templates(object sender, EventArgs e)
    {

        if (DropDownList3.SelectedIndex > 0)
        {
            ArrayList list = this.Templates;

            this.btnSave.Visible = true;
            this.btnSave.Text = "Update";

            if (list[DropDownList3.SelectedIndex] != null)
            {
                TemplateEntity item = list[DropDownList3.SelectedIndex] as TemplateEntity;
                //Commented By Jenish 21 May
                //string uid = Membership.GetUser().ProviderUserKey.ToString();

                //if (item.OfficeID.ToLower() != uid.ToLower())
                //{
                //    this.btnSave.Visible = false;
                //    this.btnSave.Text = "Save";
                //}
            }
            foreach (TemplateEntity item in list)
            {
                if (item.TemplateID == Convert.ToUInt32(DropDownList3.SelectedValue))
                {
                    this.templateTextBox.Text = item.Name;
                    this.Repeater2.DataSource = item.Dividers;
                    this.Repeater2.DataBind();

                    this.Dividers = item.Dividers;
                    DropDownList2.SelectedValue = item.Dividers.Count.ToString();
                    UpdatePanel2.Update();
                    DividersCount = this.Dividers.Count;
                    if (this.Page.User.IsInRole("Users") && !Common_Tatva.IsUserSwitched() && item.IsLocked)
                    {
                        this.btnSave.Visible = false;
                    }
                    return;
                }
            }
        }
        else
        {
            if (this.Page.User.IsInRole("Users") && !Common_Tatva.IsUserSwitched())
            {
                this.btnSave.Visible = false;
            }
            this.templateTextBox.Text = "";
            this.btnSave.Text = "Save";
        }

        UpdatePanel2.Update();
    }

    protected void Index_Changed(Object sender, EventArgs e)
    {
        folderCode.Visible = (DropDownList1.SelectedValue == "2");

        var uid = Sessions.SwitchedSessionId;
        var act = new Shinetech.DAL.Account(uid);
        string groups = act.Groups.Value == null ? "" : act.Groups.Value;

        if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users"))
        {
            trgroups.Visible = (DropDownList1.SelectedValue == "0") && !string.IsNullOrEmpty(groups);
        }
        else if (this.Page.User.IsInRole("Offices"))
        {
            trgroups.Visible = (DropDownList1.SelectedValue == "0");
        }


    }
    protected void chkQA_CheckedChanged(object sender, EventArgs e)
    {
        divQuestion.Visible = chkQA.Checked ? true : false;
        //divQuestion.Visible = true;
    }

    public string DividerID_FFB
    {
        get
        {
            return (string)this.Session["DividerID_FFB"];
        }
        set
        {
            Session["DividerID_FFB"] = value;
        }
    }

    public string EffID_FFB
    {
        get
        {
            return (string)Session["EffID_FFB"];
        }
        set
        {
            Session["EffID_FFB"] = value;

        }
    }

    //public void GetFolderDetailsById()
    //{
    //    DataTable dtFolderDetails = new DataTable();
    //    dtFolderDetails = General_Class.GetFolderIdByOfficeID(Sessions.UserId);
    //    if (dtFolderDetails != null && dtFolderDetails.Rows.Count > 0)
    //    {
    //        ddlFolderName.DataSource = dtFolderDetails;
    //        ddlFolderName.DataTextField = "FolderName";
    //        ddlFolderName.DataValueField = "folderId";
    //        ddlFolderName.DataBind();
    //        ddlFolderName.Items.Insert(0, "Select");
    //    }
    //}

    //protected void ddlFolderName_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    string folderId = ddlFolderName.SelectedValue;
    //    lblFolderLogName.Attributes.Add("style", "font-size: 15px; font-weight: 500; color: black; margin-left: 30px;display:block;");
    //    ddlFolderLog.Attributes.Add("style", "overflow: scroll; width: 200px; height: 30px;display:block;");
    //    DataSet dsFolderLogDetails = new DataSet();
    //    DataTable dtFolderLogDetails = new DataTable();
    //    DataTable dtQuestionDetails = new DataTable();
    //    dsFolderLogDetails = General_Class.GetFolderLogDetailsByFolderId(folderId);
    //    if (dsFolderLogDetails != null && dsFolderLogDetails.Tables.Count > 0 && dsFolderLogDetails.Tables[0].Rows.Count > 0 && dsFolderLogDetails.Tables[0] != null)
    //    {
    //        dtFolderLogDetails = dsFolderLogDetails.Tables[0];
    //        if (dtFolderLogDetails != null && dtFolderLogDetails.Rows.Count > 0)
    //        {
    //            ddlFolderLog.DataSource = dtFolderLogDetails;
    //            ddlFolderLog.DataTextField = "LogName";
    //            ddlFolderLog.DataValueField = "FolderLogID";
    //            ddlFolderLog.DataBind();
    //            ddlFolderLog.Items.Insert(0, "Select");
    //        }
    //    }
    //}
    //protected void ddlFolderLog_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    string folderId = ddlFolderLog.SelectedValue;
    //    DataTable dtQuestionDetails = new DataTable();
    //    dtQuestionDetails = General_Class.GetQuestionByFolderLogID(Convert.ToInt32(folderId));
    //    if (dtQuestionDetails != null && dtQuestionDetails.Rows.Count > 0)
    //    {

    //    }
    //}
    protected void txtFolderName_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToInt32(DropDownList3.SelectedItem.Value) == -1 && DropDownList2.SelectedItem.Value == "1")
        {
            TextBox text1 = Repeater2.Items[0].FindControl("textfield4") as TextBox;
            if (text1 != null)
                text1.Text = txtFolderName.Text;
        }
        textfield3.Focus();
    }
}

