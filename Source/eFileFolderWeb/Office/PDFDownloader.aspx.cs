﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;


using Shinetech.DAL;
using Shinetech.Engines;
using Ionic.Zip; // dll for creating zip file for multiple download pdf

public partial class Office_PDFDownloader : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string[] strDocumentID = Request.QueryString["ID"].Split(',');
            ZipFile zip = new ZipFile();
            Common_Tatva.WriteDocumentsAndFoldersLog(0, 0, Convert.ToInt32(strDocumentID[0]), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), "Entered in function");

            foreach (var item in strDocumentID)
            {
                Common_Tatva.WriteDocumentsAndFoldersLog(0, 0, Convert.ToInt32(strDocumentID[0]), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), "strDocumentID: " + item);
                int documentID;
                if (strDocumentID == null || !int.TryParse(item, out documentID))
                {
                    return;
                }

                Document document = new Document(documentID);
                if (document.IsExist)
                {
                    Common_Tatva.WriteDocumentsAndFoldersLog(Convert.ToInt32(Convert.ToString(document.FileFolderID)), Convert.ToInt32(Convert.ToString(document.DividerID)), Convert.ToInt32(strDocumentID[0]), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), "document exists");
                    string ext = ".pdf";
                    StreamType extValue = StreamType.PDF;
                    try
                    {
                        extValue = (StreamType)document.FileExtention.Value;
                        if (extValue == StreamType.OTHERS)
                            ext = Path.GetExtension(document.FullFileName.Value);
                        else
                        {
                            ext = this.GetFileExtention(extValue);
                        }                       
                    }
                    catch
                    {
                    }

                    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();

                    bool isRenamed = !((string.IsNullOrEmpty(document.DisplayName.Value) ? document.Name.Value : document.DisplayName.Value) == document.Name.Value);
                    var strPDFDisplayName = string.IsNullOrEmpty(document.DisplayName.Value) ? (document.Name.Value + ext) : document.DisplayName.Value + ext;
                    string strPDFName = document.Name.Value + ext;
                    //string strJPEGDisplayName = string.IsNullOrEmpty(document.DisplayName.Value) ? (document.Name.Value + ".jpeg") : document.DisplayName.Value + ".jpeg";
                    string strJPEGDisplayName = string.IsNullOrEmpty(document.DisplayName.Value) ? (document.Name.Value + ext.ToLower().Replace(".jpg", ".jpeg")) : document.DisplayName.Value + ext.ToLower().Replace(".jpg", ".jpeg");
                    string pathStr = Server.MapPath("../");
                    pathStr += document.PathName.Value;

                    string encodeName = HttpUtility.UrlPathEncode(strPDFName);
                    encodeName = encodeName.Replace("%", "$").Replace("?", "$c3$b1");

                    string fullName = string.Concat(pathStr, Path.DirectorySeparatorChar, strPDFName);

                    string fullDisplayName = string.Concat(pathStr, Path.DirectorySeparatorChar, strPDFDisplayName);
                    string fullDisplayJPEGName = string.Concat(pathStr, Path.DirectorySeparatorChar, strJPEGDisplayName);
                    string fullName2 = string.Concat(pathStr, Path.DirectorySeparatorChar, encodeName);

                    string fullName3 = string.Empty;
                    string fullName4 = string.Empty;

                    if (pathStr.TrimStart().StartsWith("Volume\\"))
                    {
                        string pathStr1 = pathStr.Replace("Volume", "Volume2");

                        fullName3 = string.Concat(pathStr1, Path.DirectorySeparatorChar, strPDFName);
                        fullName4 = string.Concat(pathStr1, Path.DirectorySeparatorChar, strPDFName);
                    }

                    #region create zip file
                    if (strDocumentID.Length > 1)
                    {
                        if (File.Exists(fullName) || (fullName.ToLower().Contains(".jpg") && File.Exists(fullName.ToLower().Replace(".jpg", ".jpeg"))))
                        {
                            if (fullName.ToLower().Contains(".jpg") && File.Exists(fullName.ToLower().Replace(".jpg", ".jpeg")))
                            {
                                var result = zip.Any(entry => entry.FileName.ToLower().EndsWith(Path.GetFileName(fullName.ToLower().Replace(".jpg", ".jpeg"))));
                                if (result == false || isRenamed)
                                {
                                    if (!zip.Any(d => d.FileName == strJPEGDisplayName))
                                    {
                                        if (isRenamed)
                                        {
                                            if (!File.Exists(fullDisplayJPEGName))
                                            {
                                                File.Copy(fullName.ToLower().Replace(".jpg", ".jpeg"), fullDisplayJPEGName);
                                            }
                                            zip.AddFile(fullDisplayJPEGName.ToLower().Replace(".jpg", ".jpeg"), "/").FileName = strJPEGDisplayName;
                                            // File.Delete(fullDisplayName);
                                        }
                                        else
                                        {
                                            zip.AddFile(fullName.ToLower().Replace(".jpg", ".jpeg"), "/").FileName = strJPEGDisplayName;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                var result = zip.Any(entry => entry.FileName.EndsWith(Path.GetFileName(fullName)));
                                if (result == false || isRenamed)
                                {
                                    if (!zip.Any(d => d.FileName == strPDFDisplayName))
                                    {
                                        if (isRenamed)
                                        {
                                            if (!File.Exists(fullDisplayName))
                                            {
                                                File.Copy(fullName, fullDisplayName);
                                            }
                                            zip.AddFile(fullDisplayName, "/").FileName = strPDFDisplayName;
                                            // File.Delete(fullDisplayName);
                                        }
                                        else
                                        {
                                            zip.AddFile(fullName, "/").FileName = strPDFDisplayName;
                                        }
                                    }
                                }
                            }

                        }
                        else if (File.Exists(fullName2) || (fullName2.ToLower().Contains(".jpg") && File.Exists(fullName2.ToLower().Replace(".jpg", ".jpeg"))))
                        {
                            if (fullName2.ToLower().Contains(".jpg") && File.Exists(fullName2.ToLower().Replace(".jpg", ".jpeg")))
                            {

                                var result = zip.Any(entry => entry.FileName.ToLower().EndsWith(Path.GetFileName(fullName2.ToLower().Replace(".jpg", ".jpeg"))));
                                if (result == false || isRenamed)
                                {
                                    if (!zip.Any(d => d.FileName == strJPEGDisplayName))
                                    {
                                        if (isRenamed)
                                        {
                                            if (!File.Exists(fullDisplayJPEGName))
                                            {
                                                File.Copy(fullName2, fullDisplayJPEGName);
                                            }
                                            zip.AddFile(fullDisplayJPEGName.ToLower().Replace(".jpg", ".jpeg"), "/").FileName = strJPEGDisplayName;
                                            // File.Delete(fullDisplayName);
                                        }
                                        else
                                        {
                                            zip.AddFile(fullName2.ToLower().Replace(".jpg", ".jpeg"), "/").FileName = strJPEGDisplayName;
                                        }
                                    }
                                }

                            }
                            else
                            {
                                var result = zip.Any(entry => entry.FileName.EndsWith(Path.GetFileName(fullName2)));
                                if (result == false || isRenamed)
                                {
                                    if (!zip.Any(d => d.FileName == strPDFDisplayName))
                                    {
                                        if (isRenamed)
                                        {
                                            if (!File.Exists(fullDisplayName))
                                            {
                                                File.Copy(fullName2, fullDisplayName);
                                            }
                                            zip.AddFile(fullDisplayName, "/").FileName = strPDFDisplayName;
                                        }
                                        else
                                        {
                                            zip.AddFile(fullName2, "/").FileName = strPDFDisplayName;
                                        }
                                    }
                                }

                            }
                        }
                        else if (File.Exists(fullName3))
                        {
                            var result = zip.Any(entry => entry.FileName.EndsWith(Path.GetFileName(fullName3)));
                            if (result == false)
                                zip.AddFile(fullName3, "/");
                        }
                        else if (File.Exists(fullName4))
                        {
                            var result = zip.Any(entry => entry.FileName.EndsWith(Path.GetFileName(fullName4)));
                            if (result == false)
                                zip.AddFile(fullName4, "/");
                        }
                    }
                    #endregion

                    if (strDocumentID.Length == 1)
                    {

                        if (File.Exists(fullName) || (fullName.ToLower().Contains(".jpg") && File.Exists(fullName.ToLower().Replace(".jpg", ".jpeg"))))
                        {
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.Expires = 0;
                            Response.Buffer = true;
                            if (fullName.ToLower().Contains(".jpg") && File.Exists(fullName.ToLower().Replace(".jpg", ".jpeg")))
                            {
                                Response.AddHeader("Content-disposition", "attachment; filename=\"" + strJPEGDisplayName + "\"");
                            }
                            else
                            {
                                Response.AddHeader("Content-disposition", "attachment; filename=\"" + strPDFDisplayName + "\"");
                            }
                            Response.ContentType = "application/stream-" + ext.Substring(1);
                            if (fullName.ToLower().Contains(".jpg") && File.Exists(fullName.ToLower().Replace(".jpg", ".jpeg")))
                            {
                                byte[] buffer = File.ReadAllBytes(fullName.ToLower().Replace(".jpg", ".jpeg"));
                                Response.OutputStream.Write(buffer, 0, buffer.Length);
                            }
                            else
                            {
                                byte[] buffer = File.ReadAllBytes(fullName);
                                Response.OutputStream.Write(buffer, 0, buffer.Length);
                            }
                            try
                            {
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(Convert.ToString(document.FileFolderID)), Convert.ToInt32(Convert.ToString(document.DividerID)), Convert.ToInt32(Convert.ToString(strDocumentID[0])), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), 0, 0);
                                Common_Tatva.WriteDocumentsAndFoldersLog(Convert.ToInt32(Convert.ToString(document.FileFolderID)), Convert.ToInt32(Convert.ToString(document.DividerID)), Convert.ToInt32(Convert.ToString(strDocumentID[0])), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), "Downloaded");
                            }
                            catch (Exception ex)
                            {
                                Common_Tatva.WriteErrorLog(Convert.ToString(document.FileFolderID), Convert.ToString(document.DividerID), Convert.ToString(strDocumentID[0]), ex);
                            }
                            Response.End();
                        }

                        else if (File.Exists(fullName2) || (fullName2.ToLower().Contains(".jpg") && File.Exists(fullName2.ToLower().Replace(".jpg", ".jpeg"))))
                        {
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.Expires = 0;
                            Response.Buffer = true;
                            if (fullName2.ToLower().Contains(".jpg") && File.Exists(fullName2.ToLower().Replace(".jpg", ".jpeg")))
                            {
                                Response.AddHeader("Content-disposition", "attachment; filename=\"" + strJPEGDisplayName + "\"");
                            }
                            else
                            {
                                Response.AddHeader("Content-disposition", "attachment; filename=\"" + strPDFDisplayName + "\"");
                            }

                            Response.ContentType = "application/stream-" + ext.Substring(1);
                            if (fullName2.ToLower().Contains(".jpg") && File.Exists(fullName2.ToLower().Replace(".jpg", ".jpeg")))
                            {
                                byte[] buffer = File.ReadAllBytes(fullName2.ToLower().Replace(".jpg", ".jpeg"));
                                Response.OutputStream.Write(buffer, 0, buffer.Length);
                            }
                            else
                            {
                                byte[] buffer = File.ReadAllBytes(fullName2);
                                Response.OutputStream.Write(buffer, 0, buffer.Length);
                            }
                            try
                            {
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(Convert.ToString(document.FileFolderID)), Convert.ToInt32(Convert.ToString(document.DividerID)), Convert.ToInt32(Convert.ToString(strDocumentID[0])), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), 0, 0);
                                Common_Tatva.WriteDocumentsAndFoldersLog(Convert.ToInt32(Convert.ToString(document.FileFolderID)), Convert.ToInt32(Convert.ToString(document.DividerID)), Convert.ToInt32(Convert.ToString(strDocumentID[0])), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), "Downloaded");
                            }
                            catch (Exception ex)
                            {
                                Common_Tatva.WriteErrorLog(Convert.ToString(document.FileFolderID), Convert.ToString(document.DividerID), Convert.ToString(strDocumentID[0]), ex);
                            }
                            Response.End();
                        }

                        else if (fullName3 != string.Empty && File.Exists(fullName3))
                        {
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.Expires = 0;
                            Response.Buffer = true;

                            Response.AddHeader("Content-disposition", "attachment; filename=\"" + strPDFName + "\"");
                            Response.ContentType = "application/stream-" + ext.Substring(1);
                            byte[] buffer = File.ReadAllBytes(fullName3);
                            Response.OutputStream.Write(buffer, 0, buffer.Length);

                            try
                            {
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(Convert.ToString(document.FileFolderID)), Convert.ToInt32(Convert.ToString(document.DividerID)), Convert.ToInt32(Convert.ToString(strDocumentID[0])), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), 0, 0);
                                Common_Tatva.WriteDocumentsAndFoldersLog(Convert.ToInt32(Convert.ToString(document.FileFolderID)), Convert.ToInt32(Convert.ToString(document.DividerID)), Convert.ToInt32(Convert.ToString(strDocumentID[0])), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), "Downloaded");
                            }
                            catch (Exception ex)
                            {
                                Common_Tatva.WriteErrorLog(Convert.ToString(document.FileFolderID), Convert.ToString(document.DividerID), Convert.ToString(strDocumentID[0]), ex);
                            }
                            Response.End();
                        }
                        else if (fullName3 != string.Empty && File.Exists(fullName4))
                        {
                            Response.ClearHeaders();
                            Response.Clear();
                            Response.Expires = 0;
                            Response.Buffer = true;

                            Response.AddHeader("Content-disposition", "attachment; filename=\"" + strPDFName + "\"");
                            Response.ContentType = "application/stream-" + ext.Substring(1);
                            byte[] buffer = File.ReadAllBytes(fullName3);
                            Response.OutputStream.Write(buffer, 0, buffer.Length);

                            try
                            {
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(Convert.ToString(document.FileFolderID)), Convert.ToInt32(Convert.ToString(document.DividerID)), Convert.ToInt32(Convert.ToString(strDocumentID[0])), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), 0, 0);
                                Common_Tatva.WriteDocumentsAndFoldersLog(Convert.ToInt32(Convert.ToString(document.FileFolderID)), Convert.ToInt32(Convert.ToString(document.DividerID)), Convert.ToInt32(Convert.ToString(strDocumentID[0])), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), "Downloaded");
                            }
                            catch (Exception ex)
                            {
                                Common_Tatva.WriteErrorLog(Convert.ToString(document.FileFolderID), Convert.ToString(document.DividerID), Convert.ToString(strDocumentID[0]), ex);
                            }
                            Response.End();
                        }
                        else //return a there isn't this file messasge, and navigate a client to the original page
                        {
                            try
                            {
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(Convert.ToString(document.FileFolderID)), Convert.ToInt32(Convert.ToString(document.DividerID)), Convert.ToInt32(Convert.ToString(strDocumentID[0])), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0);
                                Common_Tatva.WriteDocumentsAndFoldersLog(Convert.ToInt32(Convert.ToString(document.FileFolderID)), Convert.ToInt32(Convert.ToString(document.DividerID)), Convert.ToInt32(Convert.ToString(strDocumentID[0])), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), "File Not Found");
                            }
                            catch (Exception ex)
                            {
                                Common_Tatva.WriteErrorLog(Convert.ToString(document.FileFolderID), Convert.ToString(document.DividerID), Convert.ToString(strDocumentID[0]), ex);
                            }
                            Response.Redirect("~/FileNotFound.aspx", true);
                        }
                    }
                }
            }
            #region for download zip file
            if (strDocumentID.Length > 1)
            {
                if (zip.Count == 0)
                    Response.Redirect("~/FileNotFound.aspx", true);
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=DownloadedFile.zip");
                Response.ContentType = "application/zip";
                zip.Save(Response.OutputStream);
                Response.End();
            }
            #endregion
        }
    }

    protected string GetFileExtention(StreamType ext)
    {
        string extname = ".pdf";
        switch (ext)
        {
            case StreamType.PDF:
                extname = ".pdf";
                break;
            case StreamType.Word:
                extname = ".doc";
                break;
            case StreamType.Excel:
                extname = ".xls";
                break;
            case StreamType.PPT:
                extname = ".ppt";
                break;
            case StreamType.RTF:
                extname = ".rtf";
                break;
            case StreamType.JPG:
                extname = ".jpg";
                break;
            case StreamType.PNG:
                extname = ".png";
                break;
            default:
                break;
        }

        return extname;
    }

}
