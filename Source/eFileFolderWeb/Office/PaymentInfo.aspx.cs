using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DataQuicker2.Framework;
using Shinetech.DAL;

public partial class PaymentInfo : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            if (!string.IsNullOrEmpty(Request["license"]) && Request["license"]=="-2")
            {
                Server.Transfer("~/WebUser/BoughtGiftCard.aspx");
            }

            this.DropDownList3.DataSource = States;
            this.DropDownList3.DataValueField = "StateID";
            this.DropDownList3.DataTextField = "StateName";
            this.DropDownList3.DataBind();

            this.DropDownListCountry.DataSource = this.Countries;
            this.DropDownListCountry.DataValueField = "CountryID";
            this.DropDownListCountry.DataTextField = "CountryName";
            this.DropDownListCountry.DataBind();

            this.DropDownListCountry.SelectedValue = "US";

            this.DropDownList1.DataSource = TypeofCard;
            this.DropDownList1.DataBind();

            string uid = Sessions.SwitchedSessionId;
            //string uid = Membership.GetUser().ProviderUserKey.ToString();

            Account item = new Account(uid);
            if (item.IsExist)
            {
                textfield6.Text = item.Firstname.Value;
                textfield7.Text = item.Lastname.Value;
                textfield14.Text = item.CompanyName.Value;
                textfield10.Text = item.BillingAddress.Value;
                textfield11.Text = item.BillingCity.Value;
                DropDownList3.SelectedValue = item.BillingStateId.Value;
                TextBoxOtherState.Text = item.BillingOtherState.Value;
                DropDownListCountry.SelectedValue = item.BillingCountryId.Value;
                TextBox2.Text = item.BillingPostal.Value;
                textfield18.Text = item.Email.Value;
                TextBox1.Text = item.Telephone.Value;

                DropDownList1.Text = item.CardType.Value;
                textfield3.Text = item.CreditCard.Value;
                if (item.ExpirationDate.Value.Year==1)
                {
                    textfiel13.Text = "";
                }
                else
                {
                    DateTimeFormatInfo myDTFI = new CultureInfo("en-US", false).DateTimeFormat;
                    textfiel13.Text = item.ExpirationDate.Value.ToString("MM/yyyy", myDTFI);
                }
                
            }
            
        }
    }

    public Account GetUpdatedAcccount(string uid)
    {
        Account item = new Account(uid);

        item.Firstname.Value = textfield6.Text.Trim();
        item.Lastname.Value = textfield7.Text.Trim();
        item.CompanyName.Value = textfield14.Text.Trim();
        item.BillingAddress.Value = textfield10.Text.Trim();
        item.BillingCity.Value = textfield11.Text.Trim();
        item.BillingStateId.Value = DropDownList3.SelectedValue;
        item.BillingOtherState.Value = TextBoxOtherState.Text;
        item.BillingCountryId.Value = DropDownListCountry.SelectedValue;
        item.BillingPostal.Value = TextBox2.Text.Trim();
        item.Email.Value = textfield18.Text.Trim();
        item.Telephone.Value = TextBox1.Text.Trim();

        item.CardType.Value = DropDownList1.Text.Trim();
        item.CreditCard.Value = textfield3.Text.Trim();

        //int licenceId;
        //if (int.TryParse(Request["license"], out licenceId))
        //{
        //    License license = new License(licenceId);
        //    if (license.IsExist)
        //    {
        //        item.LicenseID.Value = licenceId;
        //    }
        //    else
        //    {
        //        throw new ApplicationException("Invalid licence id");
        //    }
        //}
        //else
        //{
        //    throw new ApplicationException("Invalid licence id");
        //}

        try
        {
            item.ExpirationDate.Value = Convert.ToDateTime(textfiel13.Text);
        }
        catch (Exception exp)
        {
            throw new ApplicationException(exp.Message);
        }

        return item;
    }

    //Payment
    protected void ImageButton1_Click(object sender, EventArgs e)
    {
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        Account item = this.GetUpdatedAcccount(uid);
        double nAmount = 0;
        int nQuantity=0;

        int licenceId;
        if (Request["license"] == null || Request["license"].Trim() == "-1")
            return;

        if (int.TryParse(Request["license"], out licenceId))
        {
            LicenseKit license = new LicenseKit(licenceId);
            if (license.IsExist)
            {
                item.LicenseID.Value = licenceId;
                PayEFFNow(item, license);
            }
            else
            {
                throw new ApplicationException("Invalid licence id");
            }
        }
        else
        {
            throw new ApplicationException("Invalid licence id");
        }
    }


    public void PayEFFNow(Account item, LicenseKit license)
    {
        //try
        //{
        //    Hashtable vars = StorageTransManage.ReadHtmlPage(item, license.Price.Value);
        //    if ((string)vars[0] == "1")//成功购买license
        //    {
        //        StorageTransManage.InsertEFFTransaction(item, license.Quantity.Value, (string)vars[0], (string)vars[3],
        //            (string)vars[4], (string)vars[5], (string)vars[6], item.PartnerID.Value);
        //        UserManagement.UpdateUserLicense(item, license);

        //        //为该用户设置visitor的purchase字段

        //        Visitor visitor = new Visitor(this.Session.SessionID);
        //        if(visitor.IsExist)
        //        {
        //            visitor.PurchaseFlag.Value = "Y";
        //            visitor.PurchaseCharge.Value += license.Price.Value;
        //            visitor.Update();
        //        }
        //    }

        //    //give the user the transaction details
        //    LabelUserName.Text = Membership.GetUser().UserName;
        //    LabelResult.Text = (string)vars[0]=="1"?"successful":"failed";
        //    LabelCardType.Text = item.CardType.Value;
        //    LabelCardNo.Text = item.CreditCard.Value;
        //    DateTimeFormatInfo myDTFI = new CultureInfo("en-US", false).DateTimeFormat;
        //    LabelExpDate.Text = item.ExpirationDate.Value.ToString("MM/yyyy", myDTFI);
        //    LabelTransInfo.Text = (string) vars[3];
        //    LabelPrice.Text = string.Format("${0:F2}", license.Price.Value);
        //    LabelQuantity.Text = license.Quantity.Value.ToString();
        //    UpdatePanel3.Update();
        //    mdlPopup.Show();//show the transaction detials
        //}
        //catch (Exception ex) { throw new ApplicationException(ex.Message); }
    }

    public DataTable States
    {
        get
        {
            object obj = Application["States"];

            DataTable table = null;
            if(obj==null)
            {
                table = UserManagement.GetStates();
                Application["States"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    public DataTable Countries
    {
        get
        {
            object obj = Application["Countries"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetCountries();
                Application["Countries"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    public  ArrayList TypeofCard
    {
        get
        {
            object obj = Application["TypeofCard"];

            ArrayList list = new ArrayList();
            if (obj == null)
            {
                list.Add("Visa");
                list.Add("MasterCard");
                list.Add("Amex");
                list.Add("Discover");

                Application["TypeofCard"] = list;
            }
            else
            {
                list = obj as ArrayList;
            }

            return list;
        }
    }

    protected void btnClose_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("~/Office/Welcome.aspx");
    }
}
