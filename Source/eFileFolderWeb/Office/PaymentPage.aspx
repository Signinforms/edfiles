<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PaymentPage.aspx.cs" Inherits="PaymentPage" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Payment Information</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
                <ContentTemplate>
                    <div class="form-containt">
                        <div class="content-title">Credit Card Info</div>
                        <div class="content-box">
                            <fieldset>
                                <label>Type of Card</label>
                                <asp:DropDownList ID="DropDownList1" runat="server" />
                            </fieldset>
                            <fieldset>
                                <label>Card No.</label>
                                <asp:TextBox name="textfield3" runat="server" ID="textfield3"
                                    size="30" MaxLength="20" />
                                <asp:RequiredFieldValidator runat="server" ID="CardNOV" ControlToValidate="textfield3"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The Card No is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
                                    TargetControlID="CardNOV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                <asp:RegularExpressionValidator ID="CNV" ControlToValidate="textfield3" runat="server"
                                    ErrorMessage="<b>Required Field Format</b><br />It is not a Card No format."
                                    ValidationExpression="^\d{0,20}" Display="None"></asp:RegularExpressionValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
                                    TargetControlID="CNV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                            </fieldset>
                            <fieldset>
                                <label>Exp. Date </label>
                                <asp:TextBox ID="textfiel13" runat="server" size="15" MaxLength="20" />
                                <label align="right">
                                    (Format: MM/yyyy)</label><asp:RequiredFieldValidator runat="server" ID="CardExpDate"
                                        ControlToValidate="textfiel13" Display="None" ErrorMessage="<b>Required Field Missing</b><br />The Exp.Date is required!" /><ajaxToolkit:ValidatorCalloutExtender
                                            runat="Server" ID="CardExpDateAjax" TargetControlID="CardExpDate" HighlightCssClass="validatorCalloutHighlight" />
                                <asp:RegularExpressionValidator ID="CardExpDateFormat" ControlToValidate="textfiel13"
                                    runat="server" ErrorMessage="<b>Required Field Format</b><br />It is not a Exp.Date format."
                                    ValidationExpression="^\d{2}\/\d{4}" Display="None"></asp:RegularExpressionValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="CardExpDateFormatV" TargetControlID="CardExpDateFormat"
                                    HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                            </fieldset>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-containt">
                        <div class="content-title">Billing Contact Info</div>
                        <div class="content-box">
                            <fieldset>
                                <label>First Name</label>
                                <asp:TextBox name="textfield6" runat="server" ID="textfield6"
                                    size="30" MaxLength="20" />
                                <asp:RequiredFieldValidator runat="server" ID="firstNameV" ControlToValidate="textfield6"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The first name is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                    TargetControlID="firstNameV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                            </fieldset>
                            <fieldset>
                                <label>Last Name</label>
                                <asp:TextBox name="textfield7" runat="server" ID="textfield7"
                                    size="30" MaxLength="20" />
                                <asp:RequiredFieldValidator runat="server" ID="lastNameV" ControlToValidate="textfield7"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The last name is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10"
                                    TargetControlID="lastNameV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                            </fieldset>
                            <fieldset>
                                <label>Company Name </label>
                                <asp:TextBox name="textfield14" runat="server" ID="textfield14"
                                    size="40" MaxLength="50" />
                                <asp:RequiredFieldValidator runat="server" ID="companyNameN" ControlToValidate="textfield14"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The company name is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14"
                                    TargetControlID="companyNameN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                            </fieldset>
                            <fieldset>
                                <label>Address</label>
                                <asp:TextBox name="textfield10" runat="server" ID="textfield10"
                                    size="40" MaxLength="200" />
                                <asp:RequiredFieldValidator runat="server" ID="addressN" ControlToValidate="textfield10"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The address is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15"
                                    TargetControlID="addressN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                            </fieldset>
                            <fieldset>
                                <label>City</label>
                                <asp:TextBox name="textfield11" runat="server" ID="textfield11"
                                    size="30" MaxLength="30" />
                                <asp:RequiredFieldValidator runat="server" ID="CityV" ControlToValidate="textfield11"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The city is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8"
                                    TargetControlID="CityV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                            </fieldset>
                            <fieldset>
                                <label>State/Province</label>
                                <asp:DropDownList ID="DropDownList3" runat="server" />
                            </fieldset>
                            <fieldset>
                                <label>Other     </label>
                                <asp:TextBox ID="TextBoxOtherState" Text="" runat="server"
                                    size="30" MaxLength="30" />
                            </fieldset>
                            <fieldset>
                                <label>Country</label>
                                <asp:DropDownList ID="DropDownListCountry" runat="server" />
                            </fieldset>
                            <fieldset>
                                <label>Zip/Postal</label>
                                <asp:TextBox ID="TextBox2" name="TextBox2" runat="server"
                                    size="30" MaxLength="30" />
                                <asp:RequiredFieldValidator runat="server" ID="txtPostalN" ControlToValidate="TextBox2"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The postal is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17"
                                    TargetControlID="txtPostalN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                            </fieldset>
                            <fieldset>
                                <label>Email</label>
                                <asp:TextBox ID="textfield18" runat="server" size="30" MaxLength="50" />
                                <asp:RequiredFieldValidator runat="server" ID="EmailReq" ControlToValidate="textfield18"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />An email is required." />
                                <asp:RegularExpressionValidator ID="rev_Email" runat="server" ErrorMessage="<b>Required Field Missing</b><br />It is not an email format."
                                    ControlToValidate="textfield18" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    Visible="true" Display="None"></asp:RegularExpressionValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="EmailReqE" TargetControlID="EmailReq"
                                    HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                    TargetControlID="rev_Email" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                            </fieldset>
                            <fieldset>
                                <label>Telephone</label>
                                <asp:TextBox ID="TextBox1" name="TextBox1" runat="server"
                                    size="30" MaxLength="23" />
                                <asp:RequiredFieldValidator runat="server" ID="telN" ControlToValidate="TextBox1"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The telephone is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11"
                                    TargetControlID="telN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                <asp:RegularExpressionValidator ID="TelV" ControlToValidate="TextBox1" runat="server"
                                    ErrorMessage="<b>Required Field Missing</b><br />It is not a telphone format."
                                    ValidationExpression="^\d{7,}" Display="None"></asp:RegularExpressionValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                    TargetControlID="TelV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                            </fieldset>
                        </div>
                    </div>
                    <div class="form-submit-btn">
                        <asp:Button runat="server" name="ImageButton1" ID="Button1" Text="Submit" OnClick="ImageButton1_Click" CssClass="create-btn btn green" />
                    </div>
                </ContentTemplate>
                <Triggers>
                    <%--<asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />--%>
                </Triggers>
            </asp:UpdatePanel>

        </div>
    </div>

    <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Button1ShowPopup" runat="server" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" PopupControlID="pnlPopup"
                DropShadow="true" BackgroundCssClass="modalBackground" TargetControlID="Button1ShowPopup" />
            <!-- ModalPopup Panel-->
            <asp:Panel ID="pnlPopup" runat="server" CssClass="popup-mainbox">
                <div class="popup-head">
                    Transaction Details
                </div>
                <div class="popup-scroller">
                    <table width="100%" border="0" class="details-datatable">
                        <tr>
                            <td align="left" class="podnaslov">
                                <strong>Hello <asp:Label ID="LabelUserName" Text="" runat="server" />,</strong>
                            </td>
                        </tr>
                        <tr>
                            <td width="100%" align="left" class="podnaslov">This transaction is <strong>
                                <asp:Label ID="LabelResult" Text="" runat="server" /></strong> :&nbsp;&nbsp;<asp:Label
                                    ID="LabelTransInfo" Text="" runat="server" />
                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0" class="details-datatable">
                        <tr>
                            <td align="left" class="podnaslov" colspan="3">
                                <strong><span>Card details:</span></strong>
                            </td>
                        </tr>
                        <tr>
                            <td width="15%" align="left" class="podnaslov">Card Type
                            </td>
                            <td width="1%">&nbsp;
                            </td>
                            <td width="40%">
                                <span class="podnaslov">
                                    <asp:Label ID="LabelCardType" Text="" runat="server" /></span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="podnaslov">Card&nbsp;No.
                            </td>
                            <td>&nbsp;
                            </td>
                            <td>
                                <span class="podnaslov">
                                    <asp:Label ID="LabelCardNo" Text="" runat="server" /></span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="podnaslov">Exp.&nbsp;Date
                            </td>
                            <td>&nbsp;
                            </td>
                            <td>
                                <span class="podnaslov">
                                    <asp:Label ID="LabelExpDate" Text="" runat="server" /></span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="podnaslov">Price
                            </td>
                            <td>&nbsp;
                            </td>
                            <td>
                                <span class="podnaslov">
                                    <asp:Label ID="LabelPrice" Text="" runat="server" /></span>
                            </td>
                        </tr>
                        <%--<tr>
                                    <td align="left" class="podnaslov">Quantity</td>
                                    <td>&nbsp;</td>
                                    <td>
                                     <span class="podnaslov"><asp:Label ID="LabelQuantity" Text="" runat="server" /></span>                </td>
                                </tr>--%>
                    </table>
                    <hr width="95%" align="center" />
                </div>
                <div class="popup-btn">
                    <asp:Button ID="btnClose" runat="server" Text="Close" OnClick="btnClose_OnClick" CssClass="btn green" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
