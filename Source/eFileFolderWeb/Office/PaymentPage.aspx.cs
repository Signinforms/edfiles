using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DataQuicker2.Framework;
using Shinetech.DAL;
using Shinetech.Utility;
using Account=Shinetech.DAL.Account;

public partial class PaymentPage : BasePage
{

    private static readonly string eFFManagerLink = ConfigurationManager.AppSettings["WEBSITE"];
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            this.DropDownList3.DataSource = States;
            this.DropDownList3.DataValueField = "StateID";
            this.DropDownList3.DataTextField = "StateName";
            this.DropDownList3.DataBind();

            this.DropDownListCountry.DataSource = this.Countries;
            this.DropDownListCountry.DataValueField = "CountryID";
            this.DropDownListCountry.DataTextField = "CountryName";
            this.DropDownListCountry.DataBind();

            this.DropDownListCountry.SelectedValue = "US";

            this.DropDownList1.DataSource = TypeofCard;
            this.DropDownList1.DataBind();

            string uid = Sessions.SwitchedSessionId;
            //string uid = Membership.GetUser().ProviderUserKey.ToString();

            Account item = new Account(uid);
            if (item.IsExist)
            {
                textfield6.Text = item.Firstname.Value;
                textfield7.Text = item.Lastname.Value;
                textfield14.Text = item.CompanyName.Value;
                textfield10.Text = item.BillingAddress.Value;
                textfield11.Text = item.BillingCity.Value;
                DropDownList3.SelectedValue = item.BillingStateId.Value;
                TextBoxOtherState.Text = item.BillingOtherState.Value;
                DropDownListCountry.SelectedValue = item.BillingCountryId.Value;
                TextBox2.Text = item.BillingPostal.Value;
                textfield18.Text = item.Email.Value;
                TextBox1.Text = item.Telephone.Value;

                DropDownList1.Text = item.CardType.Value;
                textfield3.Text = item.CreditCard.Value;
                if (item.ExpirationDate.Value.Year==1)
                {
                    textfiel13.Text = "";
                }
                else
                {
                    DateTimeFormatInfo myDTFI = new CultureInfo("en-US", false).DateTimeFormat;
                    textfiel13.Text = item.ExpirationDate.Value.ToString("MM/yyyy", myDTFI);
                }
                
            }
            
        }
    }

    

    //保存帐号的基本付款信息
    public Account GetUpdatedAccount(string uid)
    {
        Account item = new Account(uid);

        item.Firstname.Value = textfield6.Text.Trim();
        item.Lastname.Value = textfield7.Text.Trim();
        item.CompanyName.Value = textfield14.Text.Trim();
        item.BillingAddress.Value = textfield10.Text.Trim();
        item.BillingCity.Value = textfield11.Text.Trim();
        item.BillingStateId.Value = DropDownList3.SelectedValue;
        item.BillingOtherState.Value = TextBoxOtherState.Text;
        item.BillingCountryId.Value = DropDownListCountry.SelectedValue;
        item.BillingPostal.Value = TextBox2.Text.Trim();
        item.Email.Value = textfield18.Text.Trim();
        item.Telephone.Value = TextBox1.Text.Trim();

        item.CardType.Value = DropDownList1.Text.Trim();
        item.CreditCard.Value = textfield3.Text.Trim();

        try
        {
            item.ExpirationDate.Value = Convert.ToDateTime(textfiel13.Text);
            item.Update();
        }
        catch (Exception exp)
        {
            throw new ApplicationException(exp.Message);
        }

        return item;
    }

    //Begin Payment
    protected void ImageButton1_Click(object sender, EventArgs e)
    {
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        Account item = this.GetUpdatedAccount(uid);

        
        if (Amount > 0)
        {
            PayEFFNow(item, Amount);
        }
        
    }

    public string ValidGiftNumber
    {
        get
        {
            if (this.Session["_ValidGiftNumber"] == null)
            {
                return "";
            }
            return this.Session["_ValidGiftNumber"] as string;
        }
        
    }


    public void PayEFFNow(Account item, double amount)
    {
        try
        {
            //进行付款
            Hashtable vars = StorageTransManage.ReadHtmlPage(item, amount);
            if ((string)vars[0] == "1")//成功购买license
            {
                if (AvailableEFFMgr)
                {
                    item.EFFManagerAvailable.Value =AvailableEFFMgr;
                    
                    //Send a confirmation email.
                    sendeFFManagerEmail(item);
                }

                string promotingID="0";
                int licensekitId = 0;
                

                if (StartKit != null && !StartKit.Equals(0)) //购买Kit
                {
                    if(StartKit is string) 
                    {
                        promotingID = StartKit as string;

                        PromoteKit kit = new PromoteKit(promotingID);
                        if(kit.IsExist)
                        {
                            licensekitId = kit.LicenceKitID.Value;
                            item.LicenseID.Value = licensekitId;
                            item.FileFolders.Value += kit.FolderCount.Value;
                            item.FreeMonths.Value = kit.FreeOnlineStorage.Value;
                        }
                    }
                    else if(StartKit is int)
                    {
                        licensekitId = Convert.ToInt32(StartKit);
                        item.LicenseID.Value = licensekitId;
                        if (item.LicenseID.Value==-1)
                        {
                            item.FreeMonths.Value = 0;
                        }
                        
                    }

                }

                if (!string.IsNullOrEmpty(CurrentFolderUID))
                {
                    FolderPrice fp = new FolderPrice(CurrentFolderUID);
                    if (fp.IsExist)
                    {
                        if (item.LicenseID.Value == -1)
                        {
                            item.FileFolders.Value = fp.Count.Value;
                        }
                        else
                        {
                            item.FileFolders.Value += fp.Count.Value; //累加
                        }

                    }
                }
               
                StorageTransManage.InsertEFFTransactionKit(item, amount,CurrentFolderUID,AvailableEFFMgr,
                    (string)vars[0], (string)vars[3],
                (string)vars[4], (string)vars[5], (string)vars[6], item.PartnerID.Value);
                item.Update(); //更新
                //不一定是购买licenseKit，而可能是购买FolderCount,或者EFFMgr,所以只能保持金额了
                //为该用户设置visitor的purchase字段

                Visitor visitor = new Visitor(this.Session.SessionID);
                if(visitor.IsExist)
                {
                    visitor.PurchaseFlag.Value = "Y";
                    visitor.PurchaseCharge.Value += amount;
                    visitor.Update();
                }
            }

            //give the user the transaction details
            LabelUserName.Text = Sessions.SwitchedUserName;
            //LabelUserName.Text = Membership.GetUser().UserName;
            LabelResult.Text = (string)vars[0]=="1"?"successful":"failed";
            LabelCardType.Text = item.CardType.Value;
            LabelCardNo.Text = item.CreditCard.Value;
            DateTimeFormatInfo myDTFI = new CultureInfo("en-US", false).DateTimeFormat;
            LabelExpDate.Text = item.ExpirationDate.Value.ToString("MM/yyyy", myDTFI);
            LabelTransInfo.Text = (string) vars[3];
            LabelPrice.Text = string.Format("${0:F2}", amount);
            //LabelQuantity.Text = license.Quantity.Value.ToString();
            UpdatePanel3.Update();
            mdlPopup.Show();//show the transaction detials
        }
        catch (Exception ex) { throw new ApplicationException(ex.Message); }
    }

    public DataTable States
    {
        get
        {
            object obj = Application["States"];

            DataTable table = null;
            if(obj==null)
            {
                table = UserManagement.GetStates();
                Application["States"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    public DataTable Countries
    {
        get
        {
            object obj = Application["Countries"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetCountries();
                Application["Countries"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    public  ArrayList TypeofCard
    {
        get
        {
            object obj = Application["TypeofCard"];

            ArrayList list = new ArrayList();
            if (obj == null)
            {
                list.Add("Visa");
                list.Add("MasterCard");
                //list.Add("Amex");
                list.Add("Discover");

                Application["TypeofCard"] = list;
            }
            else
            {
                list = obj as ArrayList;
            }

            return list;
        }
    }

    public object StartKit
    {
        get
        {
            if (this.Session["_StartKitID"] == null)
            {
                return 0;
            }
            return this.Session["_StartKitID"];
        }
    }

    protected double FolderAmount
    {
        get
        {

            if (this.Session["_FolderAmount"] == null)
                return 0;

            return Convert.ToDouble(this.Session["_FolderAmount"]);
        }

    }

    protected double eFFManageAmount
    {
        get
        {
            if (this.Session["_eFFManageAmount"] == null)
                return 0;

            return Convert.ToDouble(this.Session["_eFFManageAmount"]);
        }
    }


    public double Amount
    {
        get
        {
            return FolderAmount + eFFManageAmount + StartKitAmount;
        }
    }

    public double StartKitAmount
    {
        get
        {
            if (this.Session["_StartKitAmount"] == null)
                return 0;

            return Convert.ToDouble(this.Session["_StartKitAmount"]);
        }
    }


    public string CurrentFolderUID
    {
        get
        {
            if (this.Session["_CurrentFolderUID"] == null)
                return "0";

            return Convert.ToString(this.Session["_CurrentFolderUID"]);
        }
    }

    public bool AvailableEFFMgr
    {
        get
        {
            if (this.Session["_AvailableEFFMgr"] == null)
                return false;

            return Convert.ToBoolean(this.Session["_AvailableEFFMgr"]);
        }
    }


    protected void btnClose_OnClick(object sender, EventArgs e)
    {
        Response.Redirect("~/Office/Welcome.aspx");
    }

    private void sendeFFManagerEmail(Account item)
    {
        string strMailTemplet = getFFManagerMailTemplate();

        strMailTemplet = strMailTemplet.Replace("[UserName]", item.Name.Value);
        string linkStr = eFFManagerLink + "/eFileFolderManagerSetup.msi";
        strMailTemplet = strMailTemplet.Replace("[eFFManagerLink]", linkStr);

        string strEmailSubject = "eFF Manager Confirmation";

        Email.SendFromEFileFolder(item.Email.Value, strEmailSubject, strMailTemplet);

    }

    private string getFFManagerMailTemplate()
    {

        string strMailTemplet = string.Empty;
        string physicalPath = Request.PhysicalPath;
        int index = physicalPath.LastIndexOf("\\");
        string mailTempletUrl = physicalPath.Substring(0, index + 1) + "ConfirmeFFManager.htm";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

}
