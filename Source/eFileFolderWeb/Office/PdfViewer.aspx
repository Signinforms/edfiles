﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/BlankMasterPageNew.master" CodeFile="PdfViewer.aspx.cs" Inherits="Office_PdfViewer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css?v=1" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=3" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/edFiles/EdFilesLogs.js?v=4" type="text/javascript"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>extras/jquery-ui-1.8.22.custom.min.js"></script>
    <style>
        #overlay {
            display: none;
            position: fixed;
            top: 0;
            bottom: 0;
            background: rgba(0,0,0,.8);
            width: 100%;
            height: 100%;
            z-index: 99999;
        }
    </style>
    <script type="text/ecmascript">
    $(document).ready(function () {
        if ('<%= hdnShareID.Value %>' != "" && ('<%= hdnDocType.Value %>' == '4' || '<%= hdnDocType.Value %>' == '8')) {
            $('#dialog-share-book').dialog({
                autoOpen: false,
                width: 450,
                //height: 540,
                resizable: false,
                modal: true,
                buttons: {
                    "Submit": function () {
                        SaveMail();
                    },
                    "Close": function () {
                        $(this).dialog("close");
                        $('#dialog-message-all').dialog({
                            open: function () {
                                $("#spanText").text("Please submit details to view this link.");
                            }
                        });
                    }
                },
                close: function () {
                    $('#dialog-message-all').dialog({
                        open: function () {
                            $("#spanText").text("Please submit details to view this link.");
                        }
                    });
                }
            });
            showShare();
        }
        $('.ui-button-text').first().text("Submit");
        $('.ui-button-text').last().text("Close");
    });

    function showShare() {
        $('#dialog-share-book').dialog("open");
        $('#dialog-share-book input[name="email"]').val('');
        $('#dialog-share-book input[name="name"]').val('');
        $('#dialog-share-book textarea[name="content"]').val('');
        $('#dialog-share-book input[name="email"]').css('border', '1px solid black');
        $('#dialog-share-book input[name="emailto"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="email"]')) });
    }

    function GetParameterValues(param) {
        var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < url.length; i++) {
            var urlparam = url[i].split('=');
            if (urlparam[0] == param) {
                return urlparam[1];
            }
        }
    }

    function SaveMail() {
        var email = $('#dialog-share-book input[name="email"]').val();
        var name = $('#dialog-share-book input[name="name"]').val();
        var message = $('#dialog-share-book textarea[name="content"]').val();
        var mailId = GetParameterValues('MailId');
        var Isvalid = false;

        $('#dialog-share-book input[name="email"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="email"]')) });
        if (name == "") {
            $('#dialog-share-book input[name="name"]').css("border", "solid 1px red");
            return false;
        }
        else {
            $('#dialog-share-book input[name="name"]').css("border", "solid 1px black");
        }
        if (email == "") {
            CheckValidation($('#dialog-share-book input[name="email"]'));
            CheckEmailValidation($('#dialog-share-book input[name="email"]'))
            return false;
        }

        Isvalid = CheckEmailValidation($('#dialog-share-book input[name="email"]'));
        if (!Isvalid) { return Isvalid; }
        if ($("input[id='check1']:checked").length < 1) {
            alert("You must select the checkbox to access link.");
            return false;
        }

        var src = '';
        showLoader();
        if (mailId != undefined) {
            $.ajax(
                {
                    type: "POST",
                    url: '../EFileFolderJSONService.asmx/IsValidEmail',
                    data: "{ strEmail:'" + email + "',MailId:'" + mailId + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var id = data.d;
                        if (id == 0) {
                            alert('Document cannot be accessed by this EMail address!!');
                            window.location.reload();
                        }
                        else if (id == 1) {
                            $.ajax(
                                {
                                    type: "POST",
                                    url: '../EFileFolderJSONService.asmx/SaveShareLinkLog',
                                    data: "{ strEmail:'" + email + "', strMailInfor:'" + message + "', strName:'" + name + "',shareId:'" + <%= hdnShareID.Value %> + "'}",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: function (data) {
                                        var id = data.d;
                                        var title, message;
                                        if (id === 0) {
                                            title = "Error";
                                            message = "Something went wrong. Please try again later."
                                        }
                                        else {
                                            title = "Success";
                                            message = "Thankyou for your feedback."
                                        }

                                        $('#dialog-message-all').dialog({
                                            title: title,
                                            open: function () {
                                                $("#spanText").text(message);
                                            }
                                        });

                                        $('#dialog-message-all').dialog("open");


                                        if (id > 0) {
                                            $('#dialog-share-book').dialog("close");
                                            $('#<%= hdnBtn.ClientID %>').click();
                                        }
                                        hideLoader();
                                    },
                                    fail: function (data) {

                                        $('#dialog-message-all').dialog({
                                            title: "Error",
                                            open: function () {
                                                $("#spanText").text("Something went wrong. Please try again later.");
                                            }
                                        });
                                        $('#dialog-message-all').dialog("open");

                                        hideLoader();
                                    }
                                });

                        }
                    });
        }
        else {
            $.ajax(
                {
                    type: "POST",
                    url: '../EFileFolderJSONService.asmx/SaveShareLinkLog',
                    data: "{ strEmail:'" + email + "', strMailInfor:'" + message + "', strName:'" + name + "',shareId:'" + <%= hdnShareID.Value %> + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var id = data.d;
                        var title, message;
                        if (id === 0) {
                            title = "Error";
                            message = "Something went wrong. Please try again later."
                        }
                        else {
                            title = "Success";
                            message = "Thankyou for your feedback."
                        }

                        $('#dialog-message-all').dialog({
                            title: title,
                            open: function () {
                                $("#spanText").text(message);
                            }
                        });

                        $('#dialog-message-all').dialog("open");

                        if (id > 0) {
                            $('#dialog-share-book').dialog("close");
                            $('#<%= hdnBtn.ClientID %>').click();
                        }
                        hideLoader();
                    },
                    fail: function (data) {

                        $('#dialog-message-all').dialog({
                            title: "Error",
                            open: function () {
                                $("#spanText").text("Something went wrong. Please try again later.");
                            }
                        });
                        $('#dialog-message-all').dialog("open");

                        hideLoader();
                    }
                });
        }
    }

    function CheckValidation(fieldObj) {
        if (fieldObj.val() == "") {
            fieldObj.css('border', '1px solid red');
        }
        else {
            fieldObj.css('border', '1px solid black');
        }
    }

    function CheckEmailValidation(fieldObj) {

        var x = fieldObj.val();

        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(x)) {
            fieldObj.css('border', '1px solid black');
            return true;
        }
        else {
            fieldObj.css('border', '1px solid red');

            return false;
        }
    }

    function showLoader() {
        $('#overlay').show().height($(document).height());
        $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');;
    }
    function hideLoader() {
        $('#overlay').hide();
    }

    </script>
    <div>


        <div id="dialog-share-book" title="Receiver Confirmation" style="display: none">
            <%--<p class="validateTips">All form fields are required.</p>--%>
            <br />

            <label for="name">Name</label><span style="color: red;">*</span><br />
            <label for="name">Name</label><span style="color: red;">*</span><br />
            <input type="text" name="name" id="name" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
            <label for="email">Email</label><span style="color: red;">*</span><br />
            <input type="email" name="email" id="email" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
            <label for="content">Message</label><br />
            <textarea name="content" id="content" rows="2" cols="25" class="text ui-widget-content ui-corner-all" style="border: 1px solid black"></textarea><br />
            <input type="checkbox" id="check1" />
            <span style="font-size: 12px;">Please note by viewing the shared file, you are accepting to maintain confidentiality of the shared file.
                Any further dissemination of the shared file is strictly prohibited without written consent of the organization
               or the individual whom is sharing this file with you.  A log of this share and the time and date, along with your
               ip address will be kept for compliance.
              <br />
                <br />
                EdFiles.com</span>
        </div>
    </div>
    <div id="dialog-message-all" title="" style="display: none;">
        <p id="dialog-message-content">
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span>
            <span id="spanText"></span>
        </p>
    </div>
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <asp:HiddenField ID="hdnDocType" runat="server" />
    <asp:HiddenField ID="hdnShareID" runat="server" />
    <asp:Button OnClick="hdnBtn_Click" ID="hdnBtn" runat="server" />
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
