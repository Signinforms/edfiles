﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using Shinetech.DAL;
using Shinetech.Utility;
using Shinetech.Framework.Controls;
using AjaxControlToolkit;
using Shinetech.Engines;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Collections.Generic;
using JSIStudios.SimpleRESTServices.Client;
using net.openstack.Core.Domain;
using net.openstack.Providers.Rackspace;
using System.Net;

public partial class Office_PdfViewer : System.Web.UI.Page
{
    private string data = HttpContext.Current.Request.QueryString["data"];
    private string path = HttpContext.Current.Request.QueryString["path"];
    private string ID = HttpContext.Current.Request.QueryString["ID"];
    private int ExpirationTimeConfig = Convert.ToInt32(ConfigurationManager.AppSettings["ValidShareIdHours"]);
    private readonly string FileNotFound = "File not found";
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();

    public void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int folderID = 0;
            try
            {
                string errorMsg = string.Empty;

                string decode = QueryString.QueryStringDecode(data);
                string[] splitedData = decode.Split('&');
                string sessionID = string.Empty;
                DateTime shareDateTime = DateTime.Now;

                if (splitedData.Length > 3)
                {
                    string date = "yyyyMMddHHmmssfff";
                    shareDateTime = DateTime.ParseExact((splitedData[1].Split('=')[1]), date, CultureInfo.InvariantCulture);
                    folderID = int.Parse(splitedData[2].Split('=')[1]);
                    sessionID = splitedData[3].Split('=')[1];
                    hdnDocType.Value = folderID.ToString();
                }
                else
                {
                    folderID = int.Parse(splitedData[1].Split('=')[1]);
                    sessionID = splitedData[2].Split('=')[1];
                    hdnDocType.Value = folderID.ToString();
                }

                int documentID = 0;
                string shareID = string.Empty;
                if (folderID == (int)Enum_Tatva.Folders.EFileShare || folderID == (int)Enum_Tatva.Folders.WorkArea || folderID == (int)Enum_Tatva.Folders.inbox || folderID == (int)Enum_Tatva.Folders.EdForms
                    || folderID == (int)Enum_Tatva.Folders.EFileFlow || folderID == (int)Enum_Tatva.Folders.ViewerDownload || folderID == (int)Enum_Tatva.Folders.EFileSplit || folderID == (int)Enum_Tatva.Folders.Archive || folderID == (int)Enum_Tatva.Folders.EdFormsShare)
                    shareID = splitedData[0].Split('=')[1];
                else
                    documentID = int.Parse(splitedData[0].Split('=')[1]);


                string filePath = string.Empty, fileName = string.Empty, physicalPath = string.Empty;
                long fileSize = 0;


                switch (folderID)
                {
                    case ((int)Enum_Tatva.Folders.EFileFlow):
                        {
                            physicalPath = GetPath(shareID, Enum_Tatva.Folders.EFileFlow, sessionID, ref fileSize);
                            fileName = shareID;
                            break;
                        }
                    case ((int)Enum_Tatva.Folders.EFileShare):
                        {
                            physicalPath = GetPath(shareID, Enum_Tatva.Folders.EFileShare, sessionID, ref fileSize);
                            fileName = shareID.Substring(shareID.LastIndexOf('/') + 1);
                            break;
                        }
                    case ((int)Enum_Tatva.Folders.ViewerDownload):
                        {
                            physicalPath = path;
                            fileName = path.Substring(path.LastIndexOf('/') + 1);
                            break;
                        }

                    case ((int)Enum_Tatva.Folders.FileFolder):
                        {
                            DataTable dataTable = General_Class.GetDocumentFromDocumentID(documentID);
                            dataTable.AsEnumerable().ToList().ForEach(d =>
                            {
                                fileName = d.Field<string>("DisplayName");
                                filePath = d.Field<string>("PathName");
                            });
                            if (fileName.IndexOf('.') < 0)
                                fileName += ".pdf";
                            physicalPath = Server.MapPath("~/" + filePath + "/" + fileName);
                            System.IO.FileInfo file = new System.IO.FileInfo(physicalPath);

                            if (!file.Exists)
                            {
                                Response.Write("This file does not exist.");
                                return;
                            }
                            else
                            {
                                fileSize = file.Length;

                            }
                            break;
                        }

                    case ((int)Enum_Tatva.Folders.WorkArea):
                        {
                            if (splitedData.Length > 3)
                            {
                                int expirationTime = General_Class.GetExpirationTime(new Guid(sessionID));
                                if ((DateTime.Now - shareDateTime).TotalHours <= ((expirationTime > 0) ? expirationTime : ExpirationTimeConfig))
                                {
                                    //path = path.Replace("WorkArea", "workArea");
                                    //physicalPath = GetPath(path, Enum_Tatva.Folders.WorkArea, sessionID, ref fileSize);
                                    //fileName = path.Substring(path.LastIndexOf('/') + 1);
                                    hdnShareID.Value = splitedData[4].Split('=')[1];
                                    return;
                                }
                                else
                                {
                                    //errorMsg = "ShareID has been expired";
                                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"This ShareLink has been expired. Please contact your administrator for further help.\");", true);
                                    return;
                                }
                            }
                            else
                            {
                                path = path.Replace("WorkArea", "workArea");
                                physicalPath = GetPath(path, Enum_Tatva.Folders.WorkArea, sessionID, ref fileSize);
                                fileName = path.Substring(path.LastIndexOf('/') + 1);
                                break;
                            }
                        }

                    case ((int)Enum_Tatva.Folders.inbox):
                        {
                            physicalPath = GetPath(shareID, Enum_Tatva.Folders.inbox, sessionID, ref fileSize);
                            fileName = shareID.Substring(shareID.LastIndexOf('/') + 1);
                            break;
                        }

                    case ((int)Enum_Tatva.Folders.EFileSplit):
                        {
                            fileName = shareID;
                            string encodedName = HttpUtility.UrlPathEncode(shareID);
                            encodedName = encodedName.Replace("%", "$");
                            physicalPath = HttpContext.Current.Server.MapPath("~/" + System.Configuration.ConfigurationManager.AppSettings["SplitPdf"] + Path.DirectorySeparatorChar +
                                sessionID + Path.DirectorySeparatorChar + encodedName);
                            break;
                        }
                    case ((int)Enum_Tatva.Folders.EdForms):
                        {
                            physicalPath = GetPath(shareID, Enum_Tatva.Folders.EdForms, sessionID, ref fileSize);
                            fileName = shareID.Substring(shareID.LastIndexOf('/') + 1);
                            break;
                        }
                    case ((int)Enum_Tatva.Folders.EdFormsShare):
                        {
                            physicalPath = GetPath(shareID, Enum_Tatva.Folders.EdFormsShare, sessionID, ref fileSize, ID.ToLower());
                            fileName = shareID.Substring(shareID.LastIndexOf('/') + 1);
                            break;
                        }
                    case ((int)Enum_Tatva.Folders.Archive):
                        {
                            if (splitedData.Length > 3)
                            {
                                int expirationTime = General_Class.GetExpirationTime(new Guid(sessionID));
                                if ((DateTime.Now - shareDateTime).TotalHours <= ((expirationTime > 0) ? expirationTime : ExpirationTimeConfig))
                                {
                                    //path = path.Replace("WorkArea", "workArea");
                                    //physicalPath = GetPath(path, Enum_Tatva.Folders.WorkArea, sessionID, ref fileSize);
                                    //fileName = path.Substring(path.LastIndexOf('/') + 1);
                                    hdnShareID.Value = splitedData[4].Split('=')[1];
                                    return;
                                }
                                else
                                {
                                    //errorMsg = "ShareID has been expired";
                                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"This ShareLink has been expired. Please contact your administrator for further help.\");", true);
                                    return;
                                }
                            }
                            else
                            {
                                //path = path.Replace("WorkArea", "workArea");
                                physicalPath = GetPath(path, Enum_Tatva.Folders.Archive, sessionID, ref fileSize);
                                fileName = path.Substring(path.LastIndexOf('/') + 1);
                                break;
                            }
                        }
                }
                if (string.IsNullOrEmpty(physicalPath))
                {
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "window.open('', '_self');alert(\"" + "File Not Found." + "\");window.close();", true);
                    if (!string.IsNullOrEmpty(ID))
                    {
                        switch (folderID)
                        {
                            case ((int)Enum_Tatva.Folders.EFileFlow):
                                {
                                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, Convert.ToInt32(ID));
                                    break;
                                }
                            case ((int)Enum_Tatva.Folders.EFileShare):
                                {
                                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, 0, 0, ID);
                                    break;
                                }
                            //case ((int)Enum_Tatva.Folders.EFileSplit):
                            //    {
                            //        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), 0, 0, Convert.ToInt32(ID));
                            //        break;
                            //    }
                            case ((int)Enum_Tatva.Folders.inbox):
                                {
                                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, 0, 0, null, Convert.ToInt32(ID));
                                    break;
                                }
                            case ((int)Enum_Tatva.Folders.ViewerDownload):
                                {
                                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, Convert.ToInt32(ID));
                                    break;
                                }
                            case ((int)Enum_Tatva.Folders.WorkArea):
                                {
                                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, 0, Convert.ToInt32(ID));
                                    break;
                                }
                            case ((int)Enum_Tatva.Folders.Archive):
                                {
                                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, 0, 0, null, 0, Convert.ToInt32(ID));
                                    break;
                                }
                        }
                    }
                }
                else
                {
                    Response.Clear();
                    Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
                    Response.ContentType = "application/pdf";
                    Response.WriteFile(physicalPath.Trim());
                    Response.Flush();
                    Response.Close();
                    if (!string.IsNullOrEmpty(ID))
                    {
                        switch (folderID)
                        {
                            case ((int)Enum_Tatva.Folders.EFileFlow):
                                {
                                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), 0, 0, Convert.ToInt32(ID));
                                    break;
                                }
                            case ((int)Enum_Tatva.Folders.EFileShare):
                                {
                                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), 0, 0, 0, 0, ID);
                                    break;
                                }
                            //case ((int)Enum_Tatva.Folders.EFileSplit):
                            //    {
                            //        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), 0, 0, Convert.ToInt32(ID));
                            //        break;
                            //    }
                            case ((int)Enum_Tatva.Folders.inbox):
                                {
                                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), 0, 0, 0, 0, null, Convert.ToInt32(ID));
                                    break;
                                }
                            case ((int)Enum_Tatva.Folders.ViewerDownload):
                                {
                                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), 0, 0, Convert.ToInt32(ID));
                                    break;
                                }
                            case ((int)Enum_Tatva.Folders.WorkArea):
                                {
                                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), 0, 0, 0, Convert.ToInt32(ID));
                                    break;
                                }
                            case ((int)Enum_Tatva.Folders.Archive):
                                {
                                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), 0, 0, 0, 0, null, 0, Convert.ToInt32(ID));
                                    break;
                                }
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(ID))
                {
                    switch (folderID)
                    {
                        case ((int)Enum_Tatva.Folders.EFileFlow):
                            {
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, Convert.ToInt32(ID));
                                break;
                            }
                        case ((int)Enum_Tatva.Folders.EFileShare):
                            {
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, 0, 0, ID);
                                break;
                            }
                        //case ((int)Enum_Tatva.Folders.EFileSplit):
                        //    {
                        //        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), 0, 0, Convert.ToInt32(ID));
                        //        break;
                        //    }
                        case ((int)Enum_Tatva.Folders.inbox):
                            {
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, Convert.ToInt32(ID));
                                break;
                            }
                        case ((int)Enum_Tatva.Folders.ViewerDownload):
                            {
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, Convert.ToInt32(ID));
                                break;
                            }
                        case ((int)Enum_Tatva.Folders.WorkArea):
                            {
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, 0, Convert.ToInt32(ID));
                                break;
                            }
                        case ((int)Enum_Tatva.Folders.Archive):
                            {
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Download.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, 0, 0, null, 0, Convert.ToInt32(ID));
                                break;
                            }
                    }
                }
                Common_Tatva.WriteErrorLog(string.IsNullOrEmpty("pdfviewer.aspx") ? string.Empty : "pdfviewer.aspx", string.Empty, "pdfviewer.aspx", ex);
                Response.Redirect("~/FileNotFound.aspx", true);
            }
        }
    }

    private string GetPath(string fileName, Enum_Tatva.Folders folder, string sessionID, ref long fileSize, string folderID = null)
    {
        try
        {
            //var user = new CloudIdentity
            //{
            //    Username = "efilefolders",
            //    APIKey = "e1f471511ab344ecaf6ca7e6c8e4fc9e"
            //};

            //var cloudfiles = new CloudFilesProvider(user);

            //// Create or initialize your account's key used to generate temp urls
            //const string accountTempUrlHeader = "Temp-Url-Key";
            //var accountMetadata = cloudfiles.GetAccountMetaData();
            //string tempUrlKey;
            //if (!accountMetadata.ContainsKey(accountTempUrlHeader))
            //{
            //    tempUrlKey = Guid.NewGuid().ToString();
            //    var updateMetadataRequest = new Dictionary<string, string> { { accountTempUrlHeader, tempUrlKey } };
            //    cloudfiles.UpdateAccountMetadata(updateMetadataRequest);
            //}
            //else
            //{
            //    tempUrlKey = accountMetadata[accountTempUrlHeader];
            //}

            //// Generate a public URL for a cloud file which is good for 1 day
            //var containerName = sessionID;
            //var expiration = DateTimeOffset.UtcNow + TimeSpan.FromDays(1);
            //Uri tempUrl = cloudfiles.CreateTemporaryPublicUri(HttpMethod.GET, containerName, fileName, tempUrlKey, expiration);
            //return tempUrl.ToString();
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            string filePath = string.Empty;
            string errorMsg = string.Empty;
            if (folder == Enum_Tatva.Folders.WorkArea || folder == Enum_Tatva.Folders.EFileShare || folder == Enum_Tatva.Folders.inbox || folder == Enum_Tatva.Folders.Archive)
                filePath = fileName;
            else
                filePath = rackSpaceFileUpload.GeneratePath(fileName.ToLower(), folder, folderID);
            string newFileName = rackSpaceFileUpload.GetObject(ref errorMsg, filePath, sessionID, (int)folder);
            if (string.IsNullOrEmpty(errorMsg))
            {
                fileSize = rackSpaceFileUpload.GetSizeOfFile(ref errorMsg, filePath);
                if (folder == Enum_Tatva.Folders.Archive)
                    return HttpContext.Current.Server.MapPath("~/" + Common_Tatva.RackSpaceArchiveDownload) + "\\" + sessionID + "\\" + newFileName;
                else if (folder == Enum_Tatva.Folders.EdForms || folder == Enum_Tatva.Folders.EdFormsShare)
                    return HttpContext.Current.Server.MapPath("~/" + Common_Tatva.RackSpaceEdFormsDownload) + "\\" + sessionID + "\\" + newFileName;
                else return HttpContext.Current.Server.MapPath("~/" + Common_Tatva.RackSpaceWorkareaDownload) + "\\" + sessionID + "\\" + newFileName;
            }
            else
            {
                return string.Empty;
            }
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
    }
    protected void hdnBtn_Click(object sender, EventArgs e)
    {
        string decode = QueryString.QueryStringDecode(data);
        string[] splitedData = decode.Split('&');
        int folderID = 0;
        string sessionID = string.Empty;
        DateTime shareDateTime = DateTime.Now;
        string physicalPath = string.Empty, fileName = string.Empty;
        long fileSize = 0;
        if (splitedData.Length > 3)
        {
            string date = "yyyyMMddHHmmssfff";
            shareDateTime = DateTime.ParseExact((splitedData[1].Split('=')[1]), date, CultureInfo.InvariantCulture);
            folderID = int.Parse(splitedData[2].Split('=')[1]);
            sessionID = splitedData[3].Split('=')[1];
        }
        else
        {
            folderID = int.Parse(splitedData[1].Split('=')[1]);
            sessionID = splitedData[2].Split('=')[1];
        }
        int expirationTime = General_Class.GetExpirationTime(new Guid(sessionID));
        if ((DateTime.Now - shareDateTime).TotalHours <= ((expirationTime > 0) ? expirationTime : ExpirationTimeConfig))
        //if ((DateTime.Now - shareDateTime).TotalHours <= General_Class.GetExpirationTime(new Guid(sessionID)))
        {
            path = path.Replace("WorkArea", "workArea");
            physicalPath = GetPath(path, folderID == Enum_Tatva.Folders.WorkArea.GetHashCode() ? Enum_Tatva.Folders.WorkArea : Enum_Tatva.Folders.Archive, sessionID, ref fileSize);
            fileName = path.Substring(path.LastIndexOf('/') + 1);
        }
        else
        {
            //errorMsg = "ShareID has been expired";
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"This ShareLink has been expired. Please contact your administrator for further help.\");", true);
        }
        if (string.IsNullOrEmpty(physicalPath))
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "window.open('', '_self');alert(\"" + "File Not Found." + "\");window.close();", true);
            hdnShareID.Value = null;
        }
        else
        {
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
            Response.ContentType = "application/pdf";
            Response.WriteFile(physicalPath.Trim());
            Response.Flush();
            Response.Close();
            hdnShareID.Value = null;
        }
    }
}