﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Security;

public partial class Office_Preview : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Shinetech.DAL.Account account = new Shinetech.DAL.Account(Sessions.SwitchedSessionId);
        if (!string.IsNullOrEmpty(Sessions.SwitchedSessionId))
        {
            if (Sessions.HasViewPrivilegeOnly || (account.IsSubUser.Value.ToString() == "1" && !Sessions.HasPrevilegeToDownload))
            {
                secondaryDownload.Style.Add("display", "none");
                download.Style.Add("display", "none");
            }
        }
    }
}