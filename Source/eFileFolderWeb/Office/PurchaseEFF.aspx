<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PurchaseEFF.aspx.cs" Inherits="PurchaseEFF" Title="" %>

<%@ Register TagPrefix="fjx" Namespace="com.flajaxian" Assembly="com.flajaxian.FileUploader" %>
<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register TagPrefix="cust" Namespace="Shinetech.Engines.Adapters" Assembly="FormatEngine" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<asp:ScriptManager ID="scriptManager3" runat="server"/>--%>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Purchase EdFiles</h1>
            <p>
                Please select a qty from the choices below.
            </p>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div class="content-box listing-view">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <fieldset>
                                <label>The current license used now</label>
                            </fieldset>
                            <div class="work-area">
                                <div class="overflow-auto">
                                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="false"
                                        AllowSorting="false" OnRowDataBound="GridView2_RowDataBound" DataKeyNames="UID" CssClass="datatable listing-datatable">
                                        <PagerSettings Visible="False" />
                                        <AlternatingRowStyle CssClass="odd" />
                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                        <%-- <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                        <Columns>
                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                ItemStyle-CssClass="table_tekst_edit">
                                                <HeaderTemplate>
                                                    <asp:Label ID="Label81" runat="server" Text="Status" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="TextBox1" runat="server" Text="Used" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Name" HeaderText="Name">
                                                <%--  <HeaderStyle CssClass="form_title" Width="100px" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Quantity" HeaderText="Quantity">
                                                <%-- <HeaderStyle CssClass="form_title" Width="80px" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Price" HeaderText="Price" DataFormatString="${0:F2}">
                                                <%-- <HeaderStyle CssClass="form_title" Width="80px" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Period" HeaderText="Period" DataFormatString="Unlimited">
                                                <%-- <HeaderStyle CssClass="form_title" Width="80px" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Description" HeaderText="Description">
                                                <%-- <HeaderStyle CssClass="form_title" Width="180px" />--%>
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="clearfix"></div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <fieldset>
                                <label>Select one license to buy</label>
                            </fieldset>
                            <div class="work-area">
                                <div class="overflow-auto">
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false"
                                        AllowSorting="false" OnRowDataBound="GridView1_RowDataBound" DataKeyNames="UID" CssClass="datatable listing-datatable">
                                        <PagerSettings Visible="False" />
                                        <AlternatingRowStyle CssClass="odd" />
                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                        <%-- <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                        <Columns>
                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                ItemStyle-CssClass="table_tekst_edit">
                                                <HeaderTemplate>
                                                    <asp:Label ID="Label8" runat="server" Text="Selected" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <input name="RadioButton1" type="radio" value='<%# Eval("UID") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Name" HeaderText="Name">
                                                <%-- <HeaderStyle CssClass="form_title" Width="100px" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Quantity" HeaderText="Quantity">
                                                <%-- <HeaderStyle CssClass="form_title" Width="80px" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Price" HeaderText="Price" DataFormatString="${0:F2}">
                                                <%-- <HeaderStyle CssClass="form_title" Width="80px" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Period" HeaderText="Period" DataFormatString="Unlimited">
                                                <%--<HeaderStyle CssClass="form_title" Width="80px" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Description" HeaderText="Description">
                                                <%-- <HeaderStyle CssClass="form_title" Width="180px" />--%>
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <fieldset>
                                <label>&nbsp;</label>
                                <div style="text-align: center">
                                    <%--<asp:ImageButton ID="ImageButton1" OnClick="ImageButton1_OnClick" ImageUrl="~/images/purchase_btn.gif"
                                                            runat="server" />--%>
                                    <asp:Button ID="ImageButton1" OnClick="ImageButton1_OnClick" Text="Purchase"
                                        runat="server" CssClass="create-btn btn green" />
                                </div>
                            </fieldset>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <%--<div class="quick-find">
                    <uc1:QuickFind ID="QuickFind1" runat="server" />
                     <uc2:ReminderControl ID="ReminderControl1" runat="server" />
                </div>--%>
            </div>
        </div>
    </div>
</asp:Content>
