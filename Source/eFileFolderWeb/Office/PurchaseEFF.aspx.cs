using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class PurchaseEFF : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
    }

    #region 数据绑定
    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData()
    {
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        this.DataSource = LicenseManagement.GetLicenseWithoutTrial();
        this.LastDataSource = LicenseManagement.GetCurrentLicense(uid);
    }
    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        GridView1.DataSource = this.DataSource;
        GridView1.DataBind();

        GridView2.DataSource = this.LastDataSource;
        GridView2.DataBind();
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["License"] as DataTable;
        }
        set
        {
            this.Session["License"] = value;
        }
    }

    public DataTable LastDataSource
    {
        get
        {
            return this.Session["LastLicense"] as DataTable;
        }
        set
        {
            this.Session["LastLicense"] = value;
        }
    }
    #endregion

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
       
    }

    protected void ImageButton1_OnClick(object sender, EventArgs e)
    {
        string objItemValue = Request.Form["RadioButton1"];

        if(objItemValue!=null)
        {
            Response.Redirect("PaymentInfo.aspx?license=" + objItemValue, true);
        }

    }

    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
         if(e.Row.RowType==DataControlRowType.DataRow)
         {
            DataRowView Drv = e.Row.DataItem as DataRowView;
            if((int)Drv.Row["UID"]==-1)
            {
                e.Row.Cells[4].Text = "3 Days";
            }
            else
            {
                e.Row.Cells[3].Text = "-";
            }
           
         }
    }
}
