<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PurchasePage.aspx.cs" Inherits="PurchasePage" Title="" %>

<%@ Register TagPrefix="fjx" Namespace="com.flajaxian" Assembly="com.flajaxian.FileUploader" %>
<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register TagPrefix="cust" Namespace="Shinetech.Engines.Adapters" Assembly="FormatEngine" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Purchase EdFiles</h1>
            <p>
                The pricing for EdFiles is comprised of 2 components. Number of Folders you
                use and the amount of storage your folders take up, Please purchase folders and
                a starter kit in the following.
            </p>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
							<div class="left-content">
                <div class="content-box listing-view">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <fieldset style="display: none">
                                The current license used now
                                <div class='clearfix'></div>

                                <table style="border-bottom: 2px solid; border-left: 2px solid; width: 100%; border-collapse: collapse; border-top: 2px solid; border-right: 2px solid; display: none;"
                                    id="ctl00_ContentPlaceHolder1_GridView2"
                                    border="2 " rules="all" cellspacing="0" class="datatable listing-datatable">
                                    <tbody>
                                        <tr style="background-color: #dbd9cf" class="form_title">
                                            <th width="50" scope="col">
                                                <span id="ctl00_ContentPlaceHolder1_GridView2_ctl01_Label81">Status</span>
                                            </th>
                                            <th width="240" class="form_title" style="width: 240px" scope="col">Starter Kit
                                            </th>
                                            <th width="100" class="form_title" style="width: 100px" scope="col">Folders
                                            </th>
                                            <th width="180px" class="form_title" style="width: 180px" scope="col">eFF Manager
                                            </th>
                                        </tr>
                                        <tr style="background-color: #f8f8f5" class="table_tekst" align="center">
                                            <td style="width: 50px; white-space: nowrap" class="table_tekst_edit">
                                                <span id="ctl00_ContentPlaceHolder1_GridView2_ctl02_TextBox1">Used</span>
                                            </td>
                                            <td>
                                                <asp:Label ID="labelStartKit" Text="NA" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Label ID="labelForlderNumber" Text="NA" runat="server" />
                                            </td>
                                            <td>
                                                <asp:Label ID="labelEFFEnable" Text="NA" runat="server" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </fieldset>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <fieldset>
                                <p><strong>Folder Pricing</strong> </p>
                                <div class='clearfix'></div>
                            </fieldset>
                            <div class="work-area">
                                <div class="overflow-auto">
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false"
                                        AllowSorting="false" OnRowDataBound="GridView1_RowDataBound" DataKeyNames="UID" CssClass="datatable listing-datatable">
                                        <PagerSettings Visible="False" />
                                        <AlternatingRowStyle CssClass="odd" />
                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                        <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                        <Columns>
                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" HeaderStyle-Width="15%">
                                                <HeaderTemplate>
                                                    <asp:Label ID="Label8" runat="server" Text="Selected" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="RadioButton1" AutoPostBack="true" OnCheckedChanged="RadioCheckChanged"
                                                        runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Name" HeaderText="Name">
                                                <HeaderStyle CssClass="form_title" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Price" HeaderText="Price" DataFormatString="${0:F2}" HeaderStyle-Width="20%">
                                                <HeaderStyle CssClass="form_title" />
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>

                            <fieldset style="display: none">
                                <table width="100%" align="center" border="0" style="display: none">
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div align="center">
                                                <strong>Professional</strong>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div align="center">
                                                <p>
                                                (Up to
                                                                            <asp:Label ID="labelProKitUsers" Text="NA" runat="server" />
                                                Users)</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div align="center">
                                                <p>
                                                upto
                                                                            <asp:Label ID="labelProKitStorage" Text="NA" runat="server" />
                                                GB Storage</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div align="center">
                                                <p>
                                                add��t user
                                                                            <asp:Label ID="labelProKitPerUser" Text="NA" runat="server" /></p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div align="center">
                                                <p>
                                                add��t storage
                                                                            <asp:Label ID="labelProKit5GB" Text="NA" runat="server" />
                                                GB</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div align="center">
                                                <p>
                                                <asp:Label ID="labelProKitCost" Text="NA" runat="server" />
                                                a Month</p>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div align="center">
                                                <p>
                                                    <asp:RadioButton ID="radioProfKit" AutoPostBack="true" OnCheckedChanged="RadioProfKitCheckChanged"
                                                        runat="server" GroupName="Sl" />
                                                </p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <div class='clearfix'></div>
                            </fieldset>

                            <fieldset style="display: none">
                                <p>
                                <strong>Getting Started...Special Pricing.</strong>
                                </p>
                            </fieldset>

                            <fieldset>
                                <table width="100%" align="center" border="0" style="display: none">
                                    <tr>
                                        <td>
                                            <div align="center" class="STYLE1">
                                                <strong>The Pro. Plan Starter Kit</strong>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div align="center" class="STYLE2">
                                                <asp:Label ID="labelProPlanFolders" Text="NA" runat="server" />
                                                EdFiles,
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div align="center" class="STYLE2">
                                                <asp:Label ID="labelProPlanMonths" Text="NA" runat="server" />
                                                months online storage,
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div align="center" class="STYLE2">
                                                eFF File Manager Software
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div align="center" class="STYLE2">
                                                Unlimited Training &amp; Support
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div align="center" class="STYLE2">
                                                <asp:Label ID="labelProPlanCost" Text="NA" runat="server" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div align="center">
                                                <label>
                                                    <asp:RadioButton ID="radioPromotingProfKit" AutoPostBack="true" OnCheckedChanged="RadioPromotingProfKitCheckChanged"
                                                        runat="server" GroupName="Sl" />
                                                </label>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                <hr />
                                <div class='clearfix'></div>
                            </fieldset>

                            <fieldset class="no-margin">
                                <p>
                                    <strong>The Total :</strong>
                                    <span><strong style="float: right">
                                        <%=TotalAmount %></strong></span>
                                </p>
                            </fieldset>



                            <%--<asp:ImageButton ID="ImageButton1" OnClick="ImageButton1_OnClick" ImageUrl="~/images/purchase_btn.gif"
                                          
                                                      runat="server" />--%>
                                <div class="form-submit-btn" >
                                    <asp:Button ID="ImageButton1" OnClick="ImageButton1_OnClick" Text="Purchase"
                                        runat="server" class="creat-btn btn green" />
                                </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
								</div>
								<div class="right-content">
               <%-- <div class="quick-find">
                    <uc1:QuickFind ID="QuickFind1" runat="server" />
                </div>--%>
                <div class="quick-find" style="margin-top: 15px;">
                    <div class="find-inputbox">
                        <marquee scrollamount="1" id="marqueeside" runat="server"
                            behavior="scroll" onmouseover="this.stop()" onmouseout="this.start()" scrolldelay="20"
                            direction="up">
                                <%=SideMessage %>
                            </marquee>
                        <%--<uc2:ReminderControl ID="ReminderControl1" runat="server" />--%>
                    </div>
                </div>
								</div>
            </div>
        </div>
    </div>
</asp:Content>
