using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.DAL;

public partial class PurchasePage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            FolderAmount = 0;
            eFFManageAmount = 0;
            StartKitAmount = 0;

            StartKit = 0;

            CurrentFolderUID = "0";

            GetData();

            LicenseKit prokit = new LicenseKit(200);
            if (prokit.IsExist)
            {
                labelProKitUsers.Text = prokit.MaximalUserCount.Value.ToString();
                labelProKitStorage.Text = prokit.MaxStorageVolume.Value.ToString();
                labelProKitPerUser.Text = string.Format("${0:F2}", prokit.AdditionalUserPrice.Value);
                labelProKit5GB.Text = string.Format("${0:F2}", prokit.AdditionalStoragePrice.Value);
                labelProKitCost.Text = string.Format("${0:F2}", prokit.MonthPricePerUser.Value);
            }

            PromoteKit ppromotekit = new PromoteKit("200");
            if (ppromotekit.IsExist)
            {
                labelProPlanFolders.Text = ppromotekit.FolderCount.Value.ToString();
                labelProPlanMonths.Text = ppromotekit.FreeOnlineStorage.Value.ToString();
                labelProPlanCost.Text = string.Format("${0:F2}", ppromotekit.Price.Value);
            }

            BindGrid();
        }
    }

    public string SideMessage
    {
        get
        {
            return this.Session["_Side_Message"] as string;
        }

        set
        {
            this.Session["_Side_Message"] = value;
        }
    }

    #region 数据绑定
    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData()
    {
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        Account account = new Account(uid);
        if (account.IsExist)
        {
            if (account.LicenseID.Value == -1)
            {
                labelStartKit.Text = "Trial User";
                EnableRadioButton(true,false);
            }
            else if (account.LicenseID.Value == 100)
            {
                labelStartKit.Text = "Basic Plan";
                EnableRadioButton(false, true);
            }
            else if (account.LicenseID.Value == 200)
            {
                labelStartKit.Text = "Professional";
                EnableRadioButton(false, false);
            }

            labelForlderNumber.Text = account.FileFolders.Value.ToString();
            labelEFFEnable.Text = account.EFFManagerAvailable.Value ? "Enable" : "Disable";


        }
        this.DataSource = LicenseManagement.GetFolderPrices();
    }

    private void EnableRadioButton(bool enable,bool lowerkit)
    {
        radioProfKit.Enabled = enable;
        radioPromotingProfKit.Enabled = enable;

        if (lowerkit)
        {
            radioProfKit.Enabled = true;
        }
        

    }
    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        GridView1.DataSource = this.DataSource;
        GridView1.DataBind();
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["License"] as DataTable;
        }
        set
        {
            this.Session["License"] = value;
        }
    }

 
    #endregion

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
       
    }

    protected void ImageButton1_OnClick(object sender, EventArgs e)
    {
        if (Amount > 0)
        {
            Response.Redirect("PaymentPage.aspx", true);
        }

    }

    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
         if(e.Row.RowType==DataControlRowType.DataRow)
         {
            DataRowView Drv = e.Row.DataItem as DataRowView;
            if((int)Drv.Row["UID"]==-1)
            {
                e.Row.Cells[4].Text = "30 Days";
            }
            else
            {
                e.Row.Cells[3].Text = "-";
            }
           
         }
    }

    protected void CheckChangedHandler(Object sender, EventArgs e)
    {
        ProductPrice produce = new ProductPrice("001");
        if (produce.IsExist)
        {
            
            //if (effCheckbox.Checked)
            //{
            //    eFFManageAmount = produce.Price.Value;
            //    AvailableEFFMgr = true;
            //}
            //else
            //{
            //    eFFManageAmount = 0;
            //    AvailableEFFMgr = false;
            //}
        }
    }

    public bool AvailableEFFMgr
    {
        get
        {
            if (this.Session["_AvailableEFFMgr"] == null)
                return false;

            return Convert.ToBoolean(this.Session["_AvailableEFFMgr"]);
        }

        set
        {
            this.Session["_AvailableEFFMgr"] = value;
        }
    }

    protected double eFFManageAmount
    {
        get
        {
            if (this.Session["_eFFManageAmount"] == null)
                return 0;

            return Convert.ToDouble(this.Session["_eFFManageAmount"]);
        }
        set
        {
            this.Session["_eFFManageAmount"] = value;
        }
        
    }


    protected void RadioCheckChanged(Object sender, EventArgs e)
    {
        CheckBox rb = (CheckBox)sender;
        CheckBox rb1;
        foreach (GridViewRow oldrow in GridView1.Rows)
        {
            rb1 = (CheckBox)oldrow.FindControl("RadioButton1");
            if (rb != rb1)
            {
                rb1.Checked = false;
            }
        }

        GridViewRow row = (GridViewRow)rb.NamingContainer;

        DataRow dataItem = this.DataSource.Rows[row.DataItemIndex];
        double price = Convert.ToDouble(dataItem["Price"]);

        if (((CheckBox)row.FindControl("RadioButton1")).Checked)
        {
            FolderAmount = price;
            CurrentFolderUID = Convert.ToString(dataItem["UID"]);
        }
        else
        {
            FolderAmount = 0;
            CurrentFolderUID = "0";
        }
        
    }

    protected double FolderAmount
    {
        get{

            if (this.Session["_FolderAmount"] == null)
                return 0;

            return Convert.ToDouble(this.Session["_FolderAmount"]);
        }

        set{
            this.Session["_FolderAmount"] = value;
        }
        
    }


    public string CurrentFolderUID
    {
        get
        {
            if (this.Session["_CurrentFolderUID"] == null)
                return "0";

            return Convert.ToString(this.Session["_CurrentFolderUID"]);
        }

        set
        {
            this.Session["_CurrentFolderUID"] = value;
        }
    }

   
    public string TotalAmount
    {
        get
        {

            return string.Format("${0:F2}", Amount);
        }
    }

    public double Amount
    {
        get
        {
            return FolderAmount + eFFManageAmount + StartKitAmount;
        }
    }


    public double StartKitAmount
    {
        get
        {
            if (this.Session["_StartKitAmount"] == null)
                return 0;

            return Convert.ToDouble(this.Session["_StartKitAmount"]);
        }

        set
        {
            this.Session["_StartKitAmount"] = value;
        }
    }


    protected void RadioStartKitCheckChanged(Object sender, EventArgs e)
    {
        LicenseKit basickit = new LicenseKit(100);
        if (basickit.IsExist)
        {
            StartKitAmount = basickit.MonthPricePerUser.Value;
            StartKit = 100;
        }
        
    }

    protected void RadioProfKitCheckChanged(Object sender, EventArgs e)
    {
        LicenseKit prokit = new LicenseKit(200);
        if (prokit.IsExist)
        {
            StartKitAmount = prokit.MonthPricePerUser.Value;
            StartKit = 200;
        }
    }

    protected void RadioPromotingStartKitCheckChanged(Object sender, EventArgs e)
    {
        PromoteKit bpromotekit = new PromoteKit("100");
        if (bpromotekit.IsExist)
        {
            StartKitAmount = bpromotekit.Price.Value;
            StartKit = "100";
        }
    }

    protected void RadioPromotingProfKitCheckChanged(Object sender, EventArgs e)
    {
        PromoteKit ppromotekit = new PromoteKit("200");
        if (ppromotekit.IsExist)
        {
            StartKitAmount = ppromotekit.Price.Value;

            StartKit = "200";
        }
        
    }

    public object StartKit
    {
        get
        {
            if (this.Session["_StartKitID"] == null)
            {
                return 0;
            }
            return this.Session["_StartKitID"];
        }

        set
        {
            this.Session["_StartKitID"] = value;
        }
    }

}
