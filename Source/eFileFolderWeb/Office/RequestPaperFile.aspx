<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RequestPaperFile.aspx.cs" Inherits="RequestPaperFile" Title="" %>

<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
        function fillCell(row, cellNumber, text) {
            var cell = row.insertCell(cellNumber);
            cell.innerHTML = text;
            cell.style.borderBottom = cell.style.borderRight = "solid 1px #aaaaff";
        }
        function addToClientTable(name, text) {
            var table = document.getElementById("<%= clientSide.ClientID %>");
            var row = table.insertRow(0);
            fillCell(row, 0, name);
            fillCell(row, 1, text);
        }

        function uploadError(sender, args) {
            addToClientTable(args.get_fileName(), "<span style='color:red;'>" + args.get_errorMessage() + "</span>");
        }
        function uploadComplete(sender, args) {
            var contentType = args.get_contentType();
            var text = args.get_length() + " bytes";
            if (contentType.length > 0) {
                text += ", '" + contentType + "'";
            }
            addToClientTable(args.get_fileName(), text);
        }
    </script>

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Request Stored Paper Files</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div class="content-box">
                    <fieldset>
                        <p>Click '<i>Select File</i>' for storage file uploading.</p>
                    </fieldset>
                    <div style="margin-top: 20px">
                        <ajaxToolkit:AsyncFileUpload OnClientUploadError="uploadError" OnClientUploadComplete="uploadComplete"
                            runat="server" ID="AsyncFileUpload1" Width="100%" UploaderStyle="Traditional" UploadingBackColor="#CCFFFF"
                            ThrobberID="myThrobber" />
                        &nbsp;<asp:Label runat="server" ID="myThrobber" Style="display: none;">
                            <img align="absmiddle" alt="" src="uploading.gif" />
                        </asp:Label>
                        <div>
                            <asp:Label runat="server" Text="&nbsp;" ID="uploadResult" />
                            <br />
                            <table style="border-collapse: collapse; border-left: solid 1px #aaaaff; border-top: solid 1px #aaaaff;"
                                runat="server" cellpadding="3" id="clientSide" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
