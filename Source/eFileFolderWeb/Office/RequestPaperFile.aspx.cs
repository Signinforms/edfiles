using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using AjaxControlToolkit;
using DataQuicker2.Framework;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;
using Shinetech.Utility;
using Account = Shinetech.DAL.Account;
using Shinetech.Engines.Adapters;


public partial class RequestPaperFile : BasePage
{
    private static readonly string PDFPATH = ConfigurationManager.AppSettings["PdfPath"];

    protected void Page_Load(object sender, EventArgs e)
    {
        AsyncFileUpload1.UploadedComplete +=
            new EventHandler<AsyncFileUploadEventArgs>(AsyncFileUpload1_UploadedComplete);
        AsyncFileUpload1.UploadedFileError +=
            new EventHandler<AsyncFileUploadEventArgs>(AsyncFileUpload1_UploadedFileError);
    }

    private void AsyncFileUpload1_UploadedComplete(object sender, AsyncFileUploadEventArgs e)
    {
        string id = Request.QueryString["id"].ToString();
        string uid = Request.QueryString["uid"].ToString();

        PaperFile file = new PaperFile(Convert.ToInt32(id));

        if(!file.IsExist)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "size",
                                                "top.$get(\"" + uploadResult.ClientID +
                                                "\").innerHTML = 'File doesn't exist ';", true);

            return;
        }

        FileFolder folder;

        if (file.FolderId.Value != 0)
        {
            folder = new FileFolder(file.FolderId.Value);
        }
        else
        {
            folder = new FileFolder();

            folder.DOB.Value = null;
            folder.CreatedDate.Value = DateTime.Now;
            folder.OfficeID.Value = file.UserID.Value;
            folder.OfficeID1.Value = uid;
            folder.FolderName.Value = file.FileName.Value == ""
                                          ? file.LastName.Value + file.FirstName.Value
                                          : file.FileName.Value;
            folder.OtherInfo.Value = 1;
            folder.FirstName.Value = file.FirstName.Value;
            folder.Lastname.Value = file.LastName.Value;
            folder.SecurityLevel.Value = 0;

            folder.Create();

            file.FolderId.Value = folder.FolderID.Value;
            file.Update();
        }

        Divider divider;

        if (file.DividerId.Value != 0)
        {
            divider = new Divider(file.DividerId.Value);
        }
        else
        {
            divider = new Divider();
            divider.OfficeID.Value = file.UserID.Value;
            divider.EffID.Value = folder.FolderID.Value;
            divider.Name.Value = file.LastName.Value + file.FirstName.Value;
            divider.Color.Value = "#FF0000";
            divider.Create();

            file.DividerId.Value = divider.DividerID.Value;
            file.Update();
        }

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "size",
                                                "top.$get(\"" + uploadResult.ClientID +
                                                "\").innerHTML = 'Uploaded size: " +
                                                AsyncFileUpload1.FileBytes.Length.ToString() + ", Converting...';", true);

        string path = MapPath("~/StorageFiles/") + uid;
        //上传文件保存的目录
        string savePath = path + "\\" + Path.GetFileName(e.FileName); //"E:\\工程项目\\eFileFolders2010\\eFileFolderWeb\\StorageFiles\\/5669eac9-169b-40da-b361-f563952844d6/公示简本.pdf"

        if (!Directory.Exists(path))
        {
            Directory.CreateDirectory(path);
        }

        try
        {
            //保存临时文件
            AsyncFileUpload1.SaveAs(savePath);

            string filename = "";
            string filePath = "";

            string fid = folder.FolderID.Value.ToString();
            string tid = divider.DividerID.Value.ToString();

            string vpath = Server.MapPath("~/Volume");
            //用来转换的pdf保存路径
            string pathname = string.Format("{0}\\{1}\\{2}", vpath, fid, tid);

            string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, fid, Path.DirectorySeparatorChar, tid);
            if (!Directory.Exists(dstpath))
            {
                Directory.CreateDirectory(dstpath);
            }

            //文件名(无路径)
            filename = e.FileName;
            filePath = savePath;

            //对文件名进行字符变换
            string encodeName = HttpUtility.UrlPathEncode(filename);//E:\%e5%b7%a5%e7%a8%8b%e9%a1%b9%e7%9b%ae\eFileFolders2010\eFileFolderWeb\StorageFiles\5669eac9-169b-40da-b361-f563952844d6
            encodeName = encodeName.Replace("%", "$"); //E:\$e5$b7$a5$e7$a8$8b$e9$a1$b9$e7$9b$ae\eFileFolders2010\eFileFolderWeb\StorageFiles\5669eac9-169b-40da-b361-f563952844d6
            
            //复制的目标路径，包括文件名
            dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);

            //filePath问源文件的文件全名，包括文件路径
            if (File.Exists(filePath))
            {
                try
                {
                    File.Copy(filePath, dstpath, true);
                    ImageAdapter.ProcessPostFile(dstpath, fid, tid, uid, pathname, filePath);

                    try
                    {
                        File.Delete(filePath); //添加后删除原文
                    }
                    catch (Exception)
                    {
                    }
                    

                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "error",
                                                "top.$get(\"" + uploadResult.ClientID +
                                                "\").innerHTML = 'Converted Succesfully: " + "';", true);
                }
                catch(Exception exp)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "error",
                                                "top.$get(\"" + uploadResult.ClientID + "\").innerHTML = 'Error: " +
                                                exp.Message + "';", true);
                }
            }
        }
        catch (Exception exp)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "error",
                                        "top.$get(\"" + uploadResult.ClientID + "\").innerHTML = 'Error: " +
                                        exp.Message + "';", true);
        }
    }

    private void AsyncFileUpload1_UploadedFileError(object sender, AsyncFileUploadEventArgs e)
    {
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "error",
                                                "top.$get(\"" + uploadResult.ClientID + "\").innerHTML = 'Error: " +
                                                e.StatusMessage + "';", true);
    }
}