﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


using Shinetech.DAL;
using Shinetech.Engines;

public partial class Office_SharePDF : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            string strDocumentID = Request.QueryString["id"];
            string strUID = Request.QueryString["uid"];
            int documentID;
            if (strDocumentID == null || strUID == null)
            {
                return;
            }

            byte[] byteArray = System.Convert.FromBase64String(strDocumentID);
            strDocumentID = System.Text.Encoding.Default.GetString(byteArray);

            byteArray = System.Convert.FromBase64String(strUID);
            strUID = System.Text.Encoding.Default.GetString(byteArray);

            string ext = ".pdf";
            
            ext = Path.GetExtension(strDocumentID);

            string strPDFName = strDocumentID;
            string pathStr = Server.MapPath(string.Format("../ScanInBox/{0}", strUID));
            string fullName = Path.Combine(pathStr, strPDFName.TrimStart('\\'));

            if (File.Exists(fullName))
            {
                Response.ClearHeaders();
                Response.Clear();
                Response.Expires = 0;
                Response.Buffer = true;

                Response.AddHeader("Content-disposition", "attachment; filename=\"" + strPDFName + "\"");
                Response.ContentType = "application/stream-" + ext.Substring(1);
                byte[] buffer = File.ReadAllBytes(fullName);
                Response.OutputStream.Write(buffer, 0, buffer.Length);

                Response.End();

                return;
            }
            else //return a there isn't this file messasge, and navigate a client to the original page
            {
                Response.Redirect("~/FileNotFound.aspx", true);
            }
        }
    }

    protected string GetFileExtention(StreamType ext)
    {
        string extname = ".pdf";
        switch (ext)
        {
            case StreamType.PDF:
                extname = ".pdf";
                break;
            case StreamType.Word:
                extname = ".doc";
                break;
            case StreamType.Excel:
                extname = ".xls";
                break;
            case StreamType.PPT:
                extname = ".ppt";
                break;
            case StreamType.RTF:
                extname = ".rtf";
                break;
            case StreamType.JPG:
                extname = ".jpg";
                break;
            case StreamType.PNG:
                extname = ".png";
                break;
            default:
                break;
        }

        return extname;
    }

}
