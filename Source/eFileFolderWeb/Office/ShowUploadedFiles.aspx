﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ShowUploadedFiles.aspx.cs" Inherits="Office_ShowUploadedFiles" %>

<%--<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>--%>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%--<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>--%>
<asp:Content ID="Contentinbox" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/jquery-ui-1.8.22.custom.css" />
    <%--<link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />--%>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=2" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js"></script>

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Show Inbox Upload Files</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="inner-wrapper">
        <div class="page-container">
            <div class="dashboard-container">
                    <div style="overflow: hidden;">
                        <asp:Panel ID="panelMain" runat="server" Visible="true">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:Image runat="server" ImageUrl="../Images/loader_bg.gif" ID="lbsStatus" ImageAlign="Middle" />
                                    <div class='clearfix'></div>
                                    <div class="prevnext" style="float: right" bgcolor="#f8f8f5">
                                        <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" Style="text-align: right"
                                            PageSize="5" CssClass="prevnext" PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>
                                        <asp:HiddenField ID="FolderID" runat="server" />
                                    </div>
                                    <asp:GridView ID="GridView1" runat="server" AllowSorting="True"
                                        AutoGenerateColumns="false" BorderWidth="2" OnRowDataBound="GridView1_RowDataBound"
                                        CssClass="datatable listing-datatable">
                                        <PagerSettings Visible="False" />
                                        <AlternatingRowStyle CssClass="odd" />
                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                        <Columns>
                                      
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                <HeaderTemplate>
                                                    <asp:Label ID="FileUID" runat="server" Text="Document Name" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("FileUID")%>' name="DocumentName" CssClass="lblDocumentName" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="Size" HeaderText="Size(Mb)" DataFormatString="{0:F2}" ItemStyle-Width="5px"
                                                HtmlEncode="false" />
                                            <asp:BoundField DataField="DOB" HeaderText="Create Date" DataFormatString="{0:MM.dd.yyyy}" ItemStyle-Width="5px"
                                                SortExpression="DOB" />
                                            <asp:TemplateField ShowHeader="true" ItemStyle-Width="80px" ItemStyle-CssClass="table_tekst_edit">
                                                <HeaderTemplate>
                                                    <asp:Label ID="Label8" runat="server" Text="Add to Tab" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton3" runat="server" Text="Add" CommandArgument='<%# Eval("FilePath")%>'
                                                         OnClientClick="setConversionText();"></asp:LinkButton>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="ButtonAdd" runat="server" ConfirmText="Are you sure you want to add this document to the selected tab?"
                                                        TargetControlID="LinkButton3">
                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <table border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                <tr>
                                                    <td align="left" class="podnaslov">
                                                        <asp:Label ID="Label120" runat="server" />There are no files. right now.
                                                    </td>
                                                </tr>
                                            </table>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>

                    </div>
            </div>
        </div>
    </div>

    <script>
        function setConversionText() {
            var obj = document.getElementById("<%= lbsStatus.ClientID %>");
             obj.setAttribute("src", "../Images/wait24trans.gif");
             obj.setAttribute("Visible", "true");
        }

    </script>

</asp:Content>
