﻿//using Shinetech.Framework.Controls;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Configuration;
//using System.Data;
//using System.IO;
//using System.Linq;
//using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;

using System;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;

using System.Linq;
using DataQuicker2.Framework;
using Shinetech.DAL;
using Shinetech.Utility;
using BarcodeLib;
using Shinetech.Framework.Controls;
using AjaxControlToolkit;
using Shinetech.Engines;
using System.Web.Script.Serialization;
using System.Xml;
using System.Security.AccessControl;
using System.IO.Compression;
using System.Text;
using System.Collections.Generic;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Protocols;
using Account = Shinetech.DAL.Account;

using System.Xml;
using Shinetech.Utility;
using System.Web.Script.Services;


public partial class Office_ShowUploadedFiles : StoragePage
{
    public string FolderId;
    public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    public string virtualDir = ConfigurationManager.AppSettings["VirtualDir"];
    public string newFileName = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
    }

    public string FolderName
    {
        get
        {
            return string.Format("~/{0}", CurrentVolume);
        }

    }


    public DataTable DataSource
    {
        get
        {
            return this.Session["Files_Workarea"] as DataTable;
        }
        set
        {
            this.Session["Files_Workarea"] = value;
        }
    }

    private void GetData()
    {
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        DataTable table = GetWorkareaFiles(uid);
        if (table != null)
        {
            this.DataSource = table;
        }
    }

    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
        UpdatePanel1.Update();
    }

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    protected DataTable GetWorkareaFiles(string uid)
    {
        DataTable table = MakeDataTable();

        ArrayList uids = new ArrayList();
        uids.Add(uid);

        string path = Server.MapPath("~/" + Common_Tatva.Inbox);
        if (this.Page.User.IsInRole("Offices"))
        {
            uids.AddRange(UserManagement.GetSubUserList(uid));
        }
        else if (this.Page.User.IsInRole("Users"))
        {
            DataTable dtOfficeUser = UserManagement.GetOfficeUserIDBySubUserID(uid);
            var officeID1 = dtOfficeUser.Rows[0]["OfficeUID"].ToString();
            uids.Add(officeID1);

            IList subuses = UserManagement.GetSubUserList(officeID1);

            foreach (string sub in subuses)
            {
                if (sub.ToLower() != uid.ToLower())
                {
                    uids.Add(sub);
                }
            }

        }

        string tempPath = string.Empty;
        foreach (string id in uids)
        {
            tempPath = string.Concat(path, Path.DirectorySeparatorChar, id);

            try
            {
                if (Directory.Exists(tempPath))
                {
                    string[] files = Directory.GetFiles(tempPath);
                    DataRow row = null;
                    FileInfo fi1;

                    string ext = string.Empty;

                    foreach (string file in files)
                    {
                        row = table.NewRow();
                        row["FileUID"] = Path.GetFileNameWithoutExtension(file);

                        byte[] byteArray = System.Text.Encoding.Default.GetBytes(Path.GetFileName(file));
                        row["FileID"] = System.Convert.ToBase64String(byteArray);

                        byteArray = System.Text.Encoding.Default.GetBytes(id); //uid
                        row["UID"] = System.Convert.ToBase64String(byteArray); ;

                        row["FilePath"] = file;
                        fi1 = new FileInfo(file);
                        row["Size"] = ((double)fi1.Length) / (double)(1024 * 1024);//MB
                        row["DOB"] = fi1.CreationTime;

                        ext = Path.GetExtension(file);
                        table.Rows.Add(row);
                        fi1 = null;
                    }

                    string[] varpaths = Directory.GetDirectories(tempPath);
                    DirectoryInfo info;

                    foreach (string item in varpaths)
                    {
                        info = new DirectoryInfo(item);
                        files = Directory.GetFiles(item);
                        if (files.Length > 0)
                        {
                            foreach (string subfile in files)
                            {
                                row = table.NewRow();
                                row["FileUID"] = Path.GetFileName(subfile);

                                string subfileName = subfile.Substring(tempPath.Length);
                                byte[] byteArray = System.Text.Encoding.Default.GetBytes(subfileName);
                                row["FileID"] = System.Convert.ToBase64String(byteArray);

                                byteArray = System.Text.Encoding.Default.GetBytes(id); //uid
                                row["UID"] = System.Convert.ToBase64String(byteArray); ;

                                row["FilePath"] = subfile;
                                fi1 = new FileInfo(subfile);
                                row["Size"] = ((double)fi1.Length) / (double)(1024 * 1024);//MB
                                row["DOB"] = fi1.CreationTime;

                                ext = Path.GetExtension(subfile);
                                table.Rows.Add(row);
                                fi1 = null;
                            }
                        }
                    }
                }
                else
                {
                    Directory.CreateDirectory(tempPath);
                }
            }
            catch (Exception exp)
            {

            }
        }
        DataView dv = table.DefaultView;
        dv.Sort = "DOB desc";
        table = dv.ToTable();
        return table;
    }

    private DataTable MakeDataTable()
    {
        // Create new DataTable.
        DataTable table = new DataTable();

        // Declare DataColumn and DataRow variables.
        DataColumn column;

        // Create new DataColumn, set DataType, ColumnName
        // and add to DataTable.    
        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FileUID";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "FileID";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = System.Type.GetType("System.String");
        column.ColumnName = "UID";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "FilePath";
        table.Columns.Add(column);


        column = new DataColumn();
        column.DataType = Type.GetType("System.Double");
        column.ColumnName = "Size";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.DateTime");
        column.ColumnName = "DOB";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "FolderItems";
        column.DefaultValue = "";
        table.Columns.Add(column);

        return table;
    }


    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
            string path = drv.Row[3].ToString();
            path = path.Replace(@"\", "/");
        }
    }

    //protected void LinkButton3_Click(Object sender, CommandEventArgs e)
    //{
    //    string[] allowdExtensions = { ".pdf", ".jpeg", ".jpg" };
    //    string filePath = e.CommandArgument.ToString();
    //    string ext = Path.GetExtension(filePath).ToLower();

    //    if (!allowdExtensions.Contains(ext))
    //    {
    //        Page.ClientScript.RegisterStartupScript(this.GetType(), "CallMyFunction", "alert('You can't move this file.')", true);
    //        return;
    //    }

    //    if (File.Exists(filePath))
    //    {
    //        string uid = Membership.GetUser().ProviderUserKey.ToString();

    //        //if (EditableDropDownList1.Items.Count == 0) return;
    //        //string fid = EditableDropDownList1.SelectedValue;

    //        string fid = FolderID.Value;

    //        if (ListBox1.Items.Count == 0) return;
    //        string tid = ListBox1.SelectedValue;

    //        //卢远宗 2015-11-01
    //        string vpath = Server.MapPath(string.Format("~/{0}", CurrentVolume));
    //        string pathname = string.Format("{0}\\{1}\\{2}", vpath, fid, tid);

    //        string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, fid, Path.DirectorySeparatorChar, tid); //转换目标路径
    //        if (!Directory.Exists(dstpath))
    //        {
    //            Directory.CreateDirectory(dstpath);
    //        }

    //        string filename = Path.GetFileName(filePath);
    //        string encodeName = HttpUtility.UrlPathEncode(filename);
    //        encodeName = encodeName.Replace("%", "$");
    //        dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);
    //        bool existFile = false;


    //        if (File.Exists(filePath))
    //        {
    //            //拷贝文件并转换格式
    //            try
    //            {
    //                WorkItem item = new WorkItem();
    //                item.FolderID = Convert.ToInt32(fid);
    //                item.DividerID = Convert.ToInt32(tid);
    //                string basePath = this.MapPath(this.FolderName);

    //                item.Path = this.PathUrl + Path.DirectorySeparatorChar +
    //                            item.FolderID + Path.DirectorySeparatorChar + Convert.ToInt32(tid);
    //                item.FileName = Path.GetFileNameWithoutExtension(filePath);
    //                item.FullFileName = Path.GetFileName(filePath);
    //                item.UID = Membership.GetUser().ProviderUserKey.ToString();
    //                item.contentType = getFileExtention(Path.GetExtension(filename));
    //                int order = FlashViewer.GetDocumentOrder(Convert.ToInt32(Convert.ToInt32(tid)));


    //                item.MachinePath = string.Concat(basePath, Path.DirectorySeparatorChar, item.FolderID,
    //                Path.DirectorySeparatorChar, Convert.ToInt32(tid));

    //                if (!Directory.Exists(item.MachinePath))
    //                {
    //                    Directory.CreateDirectory(item.MachinePath);
    //                }


    //                existFile = true;

    //                File.Copy(filePath, dstpath, true);
    //                var pageCount = 1;
    //                float size;
    //                using (FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
    //                {
    //                    size = stream.Length / (1024 * 1024); // IN MB.
    //                    string encodeName1 = HttpUtility.UrlPathEncode(item.FullFileName);
    //                    encodeName = encodeName.Replace("%", "$");
    //                    string source = string.Concat(item.MachinePath, Path.DirectorySeparatorChar, encodeName);
    //                    //pageCount = 1;
    //                    if (item.contentType == StreamType.JPG)
    //                    {
    //                        Jpeg2Engine jpeg2Engine = new Jpeg2Engine(item);
    //                        jpeg2Engine.generatePDFfromImage(source, source.Split('.')[0] + ".pdf");
    //                    }
    //                    else if (item.contentType == StreamType.PDF)
    //                    {
    //                        iTextSharp.text.pdf.PdfReader pdfReader = new iTextSharp.text.pdf.PdfReader(source);
    //                        pageCount = pdfReader.NumberOfPages;
    //                    }
    //                }

    //                string documentID = General_Class.DocumentsInsert(item.FileName, item.FolderID, item.UID, item.Path, item.DividerID, pageCount, size, item.FileName, (int)item.contentType, order + 1, Convert.ToString(Enum_Tatva.Class.Class1.GetHashCode()));
    //                if (Convert.ToInt32(tid) > 0)
    //                {
    //                    General_Class.AuditLogByDocId(Convert.ToInt32(documentID), Membership.GetUser().ProviderUserKey.ToString(), Enum_Tatva.Action.Create.GetHashCode());
    //                }
    //                File.Delete(filePath); 

                   
    //            }
    //            catch
    //            {
                   
    //            }
    //        }

    //        if (existFile)
    //        {
    //            // Server.Transfer("~/Office/DocumentLoader.aspx?View=1");
    //            Page.Response.Redirect(Page.Request.Url.ToString(), true);
    //        }
    //        else
    //        {
    //            //lbsStatus.ImageUrl = "";
    //        }
    //    }
    //}

    protected static StreamType getFileExtention(string ext)
    {
        StreamType streamType = StreamType.PDF;

        switch (ext.ToLower())
        {
            case ".pdf":
                streamType = StreamType.PDF;
                break;
            case ".docx":
            case ".doc":
                streamType = StreamType.Word;
                break;
            case ".xls":
                streamType = StreamType.Excel;
                break;
            case ".ppt":
                streamType = StreamType.PPT;
                break;
            case ".rtf":
                streamType = StreamType.RTF;
                break;
            case ".odt":
                streamType = StreamType.ODT;
                break;
            case ".ods":
                streamType = StreamType.ODS;
                break;
            case ".odp":
                streamType = StreamType.ODP;
                break;
            case ".jpeg":
            case ".jpg":
                streamType = StreamType.JPG;
                break;
            case ".png":
                streamType = StreamType.PNG;
                break;
            default:
                break;
        }

        return streamType;
    }
}