<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="StoredPaperBox.aspx.cs" Inherits="StoredPaperBox" Title="" %>

<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">
        function openwindow(id) {
            var url = 'ModifyBox.aspx?id=' + id;
            var name = 'ModifyBox';
            window.open(url, name, '');
        }

    </script>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Stored Paper Files</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div>
                    <%--class="left-content"--%>
                    <div class="content-box listing-view">
                        <fieldset>
                            <label>First Name</label>
                            <input name="textfieldFirstName" type="text" runat="server" id="textfieldFirstName" maxlength="50" />
                        </fieldset>
                        <fieldset>
                            <label>Last Name</label>
                            <input name="textfieldLastName" type="text" runat="server" id="textfieldLastName" maxlength="50" />
                        </fieldset>
                        <fieldset>
                            <label>Box Name</label>
                            <input name="textfieldBoxName" type="text" runat="server" id="textfieldBoxName" maxlength="50" />
                        </fieldset>
                        <fieldset>
                            <label>File Name</label>
                            <input name="textfieldFileName" type="text" runat="server" id="textfieldFileName" maxlength="50" />
                        </fieldset>
                        <fieldset>
                            <label>Destroy Date</label>
                            <asp:TextBox ID="textDestroyDate" CssClass="calenderTxtBox" name="textDestroyDate" runat="server"
                                size="50" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server"
                                TargetControlID="textDestroyDate" Format="MM-dd-yyyy" PopupPosition="Right" >
                            </ajaxToolkit:CalendarExtender>
                            <asp:RegularExpressionValidator ID="DobC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The destroy date is not a date"
                                ControlToValidate="textDestroyDate" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))|Never"
                                Visible="true" Display="None"></asp:RegularExpressionValidator>
                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6"
                                TargetControlID="DobC" HighlightCssClass="validatorCalloutHighlight" />
                        </fieldset>
                        <fieldset>
                            <label>Retention Class</label>
                            <input name="textfieldRetention" type="text" runat="server" id="textRetentionClass" maxlength="5" />
                        </fieldset>
                        <fieldset>
                            <label>&nbsp;</label>
                            <asp:Button ID="searchButton1" OnClick="OnSearchButton_Click" runat="server" Text="Search" CssClass="create-btn btn-small green" />
                        </fieldset>
                    </div>

                    <div class="content-box listing-view" style="margin-top: 15px;">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <fieldset>
                                    <div class="prevnext" style="float: right" bgcolor="#f8f8f5">
                                        <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                            OnPageSizeChanged="WebPager1_PageSizeChanged" PageSize="15" CssClass="prevnext custom_select"
                                            PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>
                                        <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                    </div>
                                </fieldset>
                                <div class="work-area">
                                    <div class="overflow-auto">
                                        <asp:GridView ID="GridView1" runat="server" DataKeyNames="BoxId" OnSorting="GridView1_Sorting" CssClass="datatable listing-datatable"
                                            OnRowDeleting="GridView1_RowDeleting" AllowSorting="True" AutoGenerateColumns="false">
                                            <PagerSettings Visible="False" />
                                            <AlternatingRowStyle CssClass="odd" />
                                            <RowStyle CssClass="even" HorizontalAlign="Center" />
                                            <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                            <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                            <Columns>
                                                <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                    ItemStyle-CssClass="table_tekst_edit">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label8" runat="server" Text="Action" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="LinkButton1" runat="server" CssClass="ic-icon ic-view" NavigateUrl='<%#string.Format("~/Office/StoredPaperFiles.aspx?id={0}", Eval("BoxId"))%>'></asp:HyperLink>&nbsp;&nbsp;&nbsp;
                                                                    <asp:LinkButton ID="libDelete" runat="server" CssClass="ic-icon ic-delete" CausesValidation="false"
                                                                        CommandName="Delete"></asp:LinkButton>
                                                        <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                            CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                            PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                        </ajaxToolkit:ModalPopupExtender>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                            ConfirmText="" TargetControlID="libDelete">
                                                        </ajaxToolkit:ConfirmButtonExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="DepartmentName" HeaderText="Department Name" SortExpression="DepartmentName">
                                                    <%--<HeaderStyle CssClass="form_title" />--%>
                                                </asp:BoundField>
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="30px"
                                                    ItemStyle-CssClass="table_tekst_edit">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label34" Text="BoxName" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton21" runat="server" Text='<%#Bind("BoxName")%>' CausesValidation="false"
                                                            OnClientClick='<%#string.Format("javascript:openwindow({0})", Eval("BoxId"))%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="RetentionClass" HeaderText="Retention Class" SortExpression="RetentionClass">
                                                    <%--<HeaderStyle CssClass="form_title" />--%>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Warehouse" HeaderText="Warehouse" SortExpression="Warehouse">
                                                    <%--<HeaderStyle CssClass="form_title" />--%>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Section" HeaderText="Section" SortExpression="Section">
                                                    <%--<HeaderStyle CssClass="form_title" />--%>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Row" HeaderText="Row" SortExpression="Row">
                                                    <%--<HeaderStyle CssClass="form_title" />--%>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Space" HeaderText="Space" SortExpression="Space">
                                                    <%--<HeaderStyle CssClass="form_title" />--%>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Other" HeaderText="Other" SortExpression="Other">
                                                    <%--<HeaderStyle CssClass="form_title" />--%>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ScannedDate" HeaderText="ScannedDate" DataFormatString="{0:MM.dd.yyyy}" SortExpression="ScannedDate" />
                                                <asp:BoundField DataField="DestroyDate" HeaderText="DestroyDate" DataFormatString="{0:MM.dd.yyyy}" SortExpression="DestroyDate" />
                                                <asp:BoundField DataField="BoxPickupDate" HeaderText="PickupDate" DataFormatString="{0:MM.dd.yyyy}" SortExpression="BoxPickupDate" />
                                                <asp:BoundField DataField="BoxNumber" HeaderText="BoxNumber" SortExpression="BoxNumber">
                                                    <%--<HeaderStyle CssClass="form_title" />--%>
                                                </asp:BoundField>
                                                <%--   <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="FileName" HeaderText="FileName" SortExpression="FileName">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="FileNumber" HeaderText="FileNumber" SortExpression="FileNumber">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="RequestDate" HeaderText="RequestDate" SortExpression="RequestDate" >
                    <HeaderStyle CssClass="form_title" />
                  </asp:BoundField>
                                                                <asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="Label11" Text="Action" runat="server" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton5" runat="server" CausesValidation="false" Text="Request"
                                                                            OnCommand="LinkButton5_Click" CommandArgument='<%#Eval("FileID")%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <p>
                                                    <asp:Label ID="Label1" runat="server" />There is no Stored Paper File.
                                                </p>
                                            </EmptyDataTemplate>
                                        </asp:GridView>


                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <%--<div class="right-content">
                    <div class="quick-find">
                        <uc1:QuickFind ID="QuickFind1" runat="server" />
                    </div>
                </div>--%>
            </div>
        </div>
    </div>

    <asp:Panel class="popup-mainbox" ID="DivDeleteConfirmation" Style="display: none"
        runat="server">
        <div>
            <div class="popup-head" id="PopupHeader">
                Delete Folder
            </div>
            <div class="popup-scroller">
                <p>
                    Caution! Are you sure you want to delete this folder. Once you delete this file,
                                                                    it will be deleted for good
                </p>
                <p>
                    Confirm Password:
                                                                    <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                </p>
            </div>
            <div class="popup-btn">
                <input id="ButtonDeleleOkay" type="button" value="Yes" class="btn green" style="margin-right:5px;"/>
                <input id="ButtonDeleteCancel" type="button" value="No" class="btn green" />
            </div>
        </div>
    </asp:Panel>
    <!-- Tabs Editor Panel-->
    <asp:Button Style="display: block" ID="Button1ShowPopup" runat="server"></asp:Button>
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="btnClose1" PopupControlID="Panel1Tabs" TargetControlID="Button1ShowPopup">
    </ajaxToolkit:ModalPopupExtender>
    <!-- ModalPopup Panel-->
    <asp:Panel Style="display: none;" ID="Panel1Tabs" runat="server" CssClass="popup-mainbox">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divTabNum" class="popup-head">
                    &nbsp;File Details
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="popup-btn">
            <asp:Button ID="btnClose1" runat="server" Text="Close" CssClass="btn green" />
            <asp:HiddenField ID="HiddenField1" runat="server" />
        </div>
    </asp:Panel>
</asp:Content>
