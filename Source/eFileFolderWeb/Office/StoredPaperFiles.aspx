<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="StoredPaperFiles.aspx.cs" Inherits="StoredPaperFiles" Title="" %>

<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">
        function openwindow(id) {
            var url = 'ModifyBox.aspx?id=' + id;
            var name = 'ModifyBox';
            window.open(url, name, '');
        }

    </script>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Stored Paper Files</h1>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div class="content-box listing-view">
                    <fieldset>
                        <label>First Name</label>
                        <input name="textfieldFirstName" type="text" runat="server" id="textfieldFirstName" size="20" maxlength="50" />
                    </fieldset>

                    <fieldset>
                        <label>Last Name</label>
                        <input name="textfieldLastName" type="text" runat="server" id="textfieldLastName" size="20" maxlength="50" />
                    </fieldset>

                    <fieldset>
                        <label>Box Name</label>
                        <input name="textfieldBoxName" type="text" runat="server" id="textfieldBoxName" size="20" maxlength="50" />
                    </fieldset>

                    <fieldset>
                        <label>File Name</label>
                        <input name="textfieldFileName" type="text" runat="server" id="textfieldFileName" size="20" maxlength="50" />
                    </fieldset>

                    <fieldset>
                        <label>&nbsp;</label>
                        <asp:Button ID="searchButton1" ImageAlign="AbsMiddle" CssClass="create-btn btn-small green" Text="Search" OnClick="OnSearchButton_Click" runat="server" />
                    </fieldset>
                </div>
                <div class="content-box listing-view" style="margin-top: 15px;">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <fieldset>
                                <div class="prevnext" style="float: right" bgcolor="#f8f8f5">
                                    <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                        OnPageSizeChanged="WebPager1_PageSizeChanged" PageSize="15" CssClass="prevnext"
                                        PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>
                                    <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                </div>
                            </fieldset>
                            <div class="work-area">
                                <div class="overflow-auto">
                                    <asp:GridView ID="GridView1" runat="server" DataKeyNames="FileID" OnSorting="GridView1_Sorting"
                                        AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable datatable-small listing-datatable">
                                        <PagerSettings Visible="False" />
                                        <AlternatingRowStyle CssClass="odd" />
                                        <RowStyle CssClass="even" />
                                        <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                        <Columns>
                                            <asp:BoundField DataField="FileID" HeaderText="File ID" SortExpression="FileID" ItemStyle-Width="60px">
                                                <%--<HeaderStyle CssClass="form_title" />--%>
                                            </asp:BoundField>
                                            <%--<asp:BoundField DataField="DepartmentName" HeaderText="Department Name" SortExpression="DepartmentName">
                                                </asp:BoundField>--%>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="30px"
                                                ItemStyle-CssClass="table_tekst_edit">
                                                <HeaderTemplate>
                                                    <asp:Label ID="Label34" Text="BoxName" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton21" runat="server" Text='<%#Bind("BoxName")%>' CausesValidation="false"
                                                        OnClientClick='<%#string.Format("javascript:openwindow({0})", Eval("BoxId"))%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:BoundField DataField="RetentionClass" HeaderText="Retention Class" SortExpression="RetentionClass">
                                                </asp:BoundField>--%>
                                            <asp:BoundField DataField="Warehouse" HeaderText="Warehouse" SortExpression="Warehouse">
                                                <%--<HeaderStyle CssClass="form_title" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Section" HeaderText="Section" SortExpression="Section">
                                                <%--<HeaderStyle CssClass="form_title" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Row" HeaderText="Row" SortExpression="Row">
                                                <%--<HeaderStyle CssClass="form_title" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Space" HeaderText="Space" SortExpression="Space">
                                                <%--<HeaderStyle CssClass="form_title" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Other" HeaderText="Other" SortExpression="Other">
                                                <%--<HeaderStyle CssClass="form_title" />--%>
                                            </asp:BoundField>
                                            <%--<asp:BoundField DataField="ScannedDate" HeaderText="ScannedDate" DataFormatString="{0:MM.dd.yyyy}" SortExpression="ScannedDate" />--%>
                                                <asp:BoundField DataField="DestroyDate" HeaderText="DestroyDate" DataFormatString="{0:MM.dd.yyyy}" SortExpression="DestroyDate" />
                                                <%--<asp:BoundField DataField="BoxPickupDate" HeaderText="PickupDate" DataFormatString="{0:MM.dd.yyyy}" SortExpression="BoxPickupDate" />--%>
                                            <asp:BoundField DataField="BoxNumber" HeaderText="BoxNumber" SortExpression="BoxNumber">
                                                <%--<HeaderStyle CssClass="form_title" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName">
                                                <%--<HeaderStyle CssClass="form_title" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName">
                                                <%--<HeaderStyle CssClass="form_title" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FileName" HeaderText="FileName" SortExpression="FileName">
                                                <%--<HeaderStyle CssClass="form_title" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FileNumber" HeaderText="FileNumber" SortExpression="FileNumber">
                                                <%--<HeaderStyle CssClass="form_title" />--%>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status">
                                                <%--<HeaderStyle CssClass="form_title" />--%>
                                            </asp:BoundField>
                                            <asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit">
                                                <HeaderTemplate>
                                                    <asp:Label ID="Label11" Text="Action" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton5" runat="server" CausesValidation="false" ToolTip="Request" CssClass="ic-icon ic-request"
                                                        OnCommand="LinkButton5_Click" CommandArgument='<%#Eval("FileID")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <p>
                                                <asp:Label ID="Label1" runat="server" />There is no Stored Paper File.
                                            </p>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    <div>
                                        <!-- Tabs Editor Panel-->
                                        <asp:Button Style="display: none" ID="Button1ShowPopup" runat="server"></asp:Button>
                                        <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
                                            CancelControlID="btnClose1" PopupControlID="Panel1Tabs" TargetControlID="Button1ShowPopup">
                                        </ajaxToolkit:ModalPopupExtender>
                                        <!-- ModalPopup Panel-->
                                        <asp:Panel Style="display: none; " ID="Panel1Tabs" runat="server" CssClass="popup-mainbox">
                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div id="divTabNum" class="popup-head">
                                                        &nbsp;File Details
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <div class="popup-btn">
                                                <asp:Button ID="btnClose1" runat="server" Text="Close" CssClass="btn green" />
                                                <asp:HiddenField ID="HiddenField1" runat="server" />
                                            </div>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
               <%-- <div class="quick-find">
                    <uc1:QuickFind ID="QuickFind1" runat="server" />
                </div>--%>
            </div>
        </div>
    </div>
</asp:Content>
