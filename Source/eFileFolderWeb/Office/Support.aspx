﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Support.aspx.cs" Inherits="Office_Support" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Support</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div class="table-scroll">
                    <table>
                        <tr>
                            <td style="padding: 20px;" class="iframeTd">
                                <div style="padding-bottom: 15px; font-size: 18px; text-align: center">
                                    <label>Getting Started</label>
                                </div>
                                <iframe class="iframeSupport" width="560" height="315" src="https://www.youtube.com/embed/FYcc4T0Fy2I" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </td>
                            <td style="padding: 20px;" class="iframeTd">
                                <div style="padding-bottom: 15px; font-size: 18px; text-align: center">
                                    <label>Frequently Asked Questions 1</label>
                                </div>
                                <iframe class="iframeSupport" width="560" height="315" src="https://www.youtube.com/embed/ioFStRYRZ4U" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 20px;" class="iframeTd">
                                <div style="padding-bottom: 15px; font-size: 18px; text-align: center">
                                    <label>Frequently Asked Questions 2</label>
                                </div>
                                <iframe class="iframeSupport" width="560" height="315" src="https://www.youtube.com/embed/UQQNLozAKdc" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </td>
                            <td style="padding: 20px;" class="iframeTd">
                                <div style="padding-bottom: 15px; font-size: 18px; text-align: center">
                                    <label>Share Files & Lock Tabs</label>
                                </div>
                                <iframe class="iframeSupport" width="560" height="315" src="https://www.youtube.com/embed/sP1vXuhoq3g" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding: 20px;" class="iframeTd">
                                <div style="padding-bottom: 15px; font-size: 18px; text-align: center">
                                    <label>Split & Upload Scans</label>
                                </div>
                                <iframe class="iframeSupport" width="560" height="315" src="https://www.youtube.com/embed/lEoA7Wuq60Y" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
