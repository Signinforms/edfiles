﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TestThumbnails.aspx.cs" Inherits="Office_TestThumbnails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <!DOCTYPE html>

    <script src="http://cdnjs.cloudflare.com/ajax/libs/processing.js/1.4.1/processing-api.min.js"></script>
    <!--
  Created using jsbin.com
  Source can be edited via http://jsbin.com/pdfjs-helloworld-v2/8598/edit
-->
    <canvas id="the-canvas" style="border: 1px solid black"></canvas>
    <input id='pdf' type='file' />

    <!-- Use latest PDF.js build from Github -->
    <%--<script type="text/javascript" src="https://rawgithub.com/mozilla/pdf.js/gh-pages/build/pdf.js"></script>--%>
    <script src="../New/js/PdfJS/pdf.js" type="text/javascript"></script>


    <script type="text/javascript">
        //
        // Disable workers to avoid yet another cross-origin issue (workers need the URL of
        // the script to be loaded, and dynamically loading a cross-origin script does
        // not work)
        //
        PDFJS.disableWorker = true;
        //
        // Asynchronous download PDF as an ArrayBuffer
        //
        var pdf = document.getElementById('pdf');
        pdf.onchange = function (ev) {
            if (file = document.getElementById('pdf').files[0]) {
                fileReader = new FileReader();
                fileReader.onload = function (ev) {
                    console.log(ev);
                    PDFJS.getDocument("http://localhost:64812/SplitEdFiles/ed412b7f-5d2c-4859-8c45-293ce3aeb175/Correspondance_001-Ellis,Eloise.pdf").promise.then(function (pdf) {
                        //
                        // Fetch the first page
                        //
                        console.log(pdf)
                        pdf.getPage(1).then(function getPageHelloWorld(page) {
                            var scale = 1.5;
                            var viewport = page.getViewport(scale);
                            //
                            // Prepare canvas using PDF page dimensions
                            //
                            var canvas = document.getElementById('the-canvas');
                            var context = canvas.getContext('2d');
                            canvas.height = viewport.height;
                            canvas.width = viewport.width;
                            //
                            // Render PDF page into canvas context
                            //
                            var task = page.render({ canvasContext: context, viewport: viewport })
                            task.promise.then(function () {
                                console.log(canvas.toDataURL('image/jpeg'));
                            });
                        });
                    }).catch(console.error);
                };
                fileReader.readAsArrayBuffer(file);
            }
        }
    </script>


    <style id="jsbin-css">
</style>
    <script>
    </script>
</asp:Content>
