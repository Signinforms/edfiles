﻿using net.openstack.Core.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Office_Test_RackSpace : System.Web.UI.Page
{
    private string ConnectionString =
         System.Configuration.ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            GetAllLogForms();
        }
    }

    private void GetAllLogForms()
    {
        DataTable dtLogForms = new DataTable();
        dtLogForms.Columns.Add("FolderId", typeof(int));
        dtLogForms.Columns.Add("FileName", typeof(string));
        dtLogForms.Columns.Add("UID", typeof(string));
      


        DataTable dtFolders = GetAllFolders();
        foreach (DataRow item in dtFolders.Rows)
        {
            //item[""]
            //var path1 = HttpContext.Current.Server.MapPath("~/" + "Volume/" + Convert.ToString(item["FolderID"]) + "/LogForm/");
            var path2 = HttpContext.Current.Server.MapPath("~/" + "Volume2/" + Convert.ToString(item["FolderID"]) + "/LogForm/");

            if (Directory.Exists(path2))
            {
                var files = Directory.GetFiles(path2);
                if (files != null)
                {
                    foreach (var file in files)
                    {
                        if (file != null)
                        {
                            try
                            {
                                string fileName = string.Empty;
                                if (file.LastIndexOf('\\') > 0)
                                  fileName  = file.Substring(file.LastIndexOf('\\') + 1);
                                else if(file.LastIndexOf('/') > 0)
                                    fileName = file.Substring(file.LastIndexOf('/') + 1);
                                dtLogForms.Rows.Add(new object[] { Convert.ToString(item["FolderID"]), fileName, Convert.ToString(item["OfficeID"]) });
                            }
                            catch (Exception ex)
                            {
                            }
                        }
                    }
                }
            }
        }

        InsertLogFormRecords(dtLogForms);
 
    }


    private DataTable GetAllFolders()
    {
        using (SqlConnection connection = new SqlConnection(ConnectionString))
            try
            {
                // DateTime? someDate = null;
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "GetAllFolders";
                command.CommandType = CommandType.StoredProcedure;
                command.CommandTimeout = 1200;
                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = command;

                DataTable table = new DataTable();
                dataAdapter.Fill(table);
                return table;
            }
            catch (Exception ex)
            {
                return new DataTable();
            }
            finally
            {
                connection.Close();
            }
    }

    private string InsertLogFormRecords(DataTable dt)
    {
        using (SqlConnection connection = new SqlConnection(ConnectionString))
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = connection;
                command.CommandText = "Temp_InsertLogForm";
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter[] parameters = new SqlParameter[1];
                parameters[0] = new SqlParameter("@TempLogForm", SqlDbType.Structured);
                parameters[0].Value = dt;
                command.Parameters.AddRange(parameters);
                var value = command.ExecuteScalar();
                return Convert.ToString(value);
            }
            catch (Exception ex)
            {
                return "";
            }
            finally
            {
                connection.Close();
            }
    }

    //protected void btnCheckFiles_Click(object sender, EventArgs e)
    //{
    //    DataTable users = GetSubUsers();

    //    DataTable dtFiles = new DataTable();
    //    dtFiles.Columns.Add("UID", typeof(Guid));
    //    dtFiles.Columns.Add("PUID", typeof(Guid));
    //    dtFiles.Columns.Add("Name", typeof(string));
    //    dtFiles.Columns.Add("Size", typeof(string));
    //    dtFiles.Columns.Add("path", typeof(string));
    //    dtFiles.Columns.Add("error", typeof(string));
    //    dtFiles.Columns.Add("filename", typeof(string));
    //    dtFiles.Columns.Add("folderLog", typeof(string));

    //    if (users != null && users.Rows.Count > 0)
    //    {
    //        foreach (DataRow item in users.Rows)
    //        {
    //            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();

    //            try
    //            {
    //                IEnumerable<ContainerObject> containerObjectList = rackSpaceFileUpload.GetObjectList(Convert.ToString(item["UID"]));
    //                foreach (ContainerObject containerObj in containerObjectList)
    //                {
    //                    string name = string.Empty;
    //                    string size = string.Empty;
    //                    try
    //                    {
    //                        name = containerObj.Name;
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                    }
    //                    try
    //                    {
    //                        size = Math.Round((decimal)(containerObj.Bytes / 1024) / 1024, 2).ToString();
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                    }
    //                    string erroMsg = string.Empty;
    //                    try
    //                    {
    //                        //rackSpaceFileUpload.CreateContainer(Convert.ToString(item["OfficeUID"]), ref erroMsg);
    //                        MemoryStream memoryStream = rackSpaceFileUpload.GetObjectForTestRackspacePage(Convert.ToString(item["UID"]), name);
    //                        string fileName = name.Substring(name.LastIndexOf('/') + 1);
    //                        string folderName = name.Substring(0, name.IndexOf('/'));
    //                        if (folderName == Enum_Tatva.GetEnumDescription(Enum_Tatva.Folders.EFileFlow))
    //                        {
    //                            //rackSpaceFileUpload.UploadObjectForTestRackspacePage(ref erroMsg, fileName, Enum_Tatva.Folders.EFileFlow, memoryStream, null, Convert.ToString(item["OfficeUID"]), memoryStream.Length);
    //                            rackSpaceFileUpload.GetObjectForTestRackSpace(ref erroMsg, fileName, memoryStream, folderName, Convert.ToString(item["OfficeUID"]), Convert.ToString(item["UID"]));
    //                        }
    //                        else if (folderName == Enum_Tatva.GetEnumDescription(Enum_Tatva.Folders.inbox))
    //                        {
    //                            //rackSpaceFileUpload.UploadObjectForTestRackspacePage(ref erroMsg, fileName, Enum_Tatva.Folders.inbox, memoryStream, null, Convert.ToString(item["OfficeUID"]), memoryStream.Length);
    //                            rackSpaceFileUpload.GetObjectForTestRackSpace(ref erroMsg, fileName, memoryStream, folderName, Convert.ToString(item["OfficeUID"]), Convert.ToString(item["UID"]));
    //                        }
    //                        else if (folderName == Enum_Tatva.GetEnumDescription(Enum_Tatva.Folders.WorkArea))
    //                        {
    //                            //rackSpaceFileUpload.UploadObjectForTestRackspacePage(ref erroMsg, fileName, Enum_Tatva.Folders.WorkArea, memoryStream, null, Convert.ToString(item["OfficeUID"]), memoryStream.Length);
    //                            rackSpaceFileUpload.GetObjectForTestRackSpace(ref erroMsg, fileName, memoryStream, folderName, Convert.ToString(item["OfficeUID"]), Convert.ToString(item["UID"]));
    //                        }
    //                        //update database entries
    //                        dtFiles.Rows.Add(new object[] { item["UID"], item["OfficeUID"], name, size, name, erroMsg, fileName,folderName });
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                    }
    //                }
    //            }
    //            catch (Exception ex)
    //            {
    //            }   
    //        }
    //    }

    //    InsertRecords(dtFiles);
    //}

    //private DataTable GetSubUsers()
    //{
    //    using (SqlConnection connection = new SqlConnection(ConnectionString))
    //        try
    //        {
    //            // DateTime? someDate = null;
    //            connection.Open();
    //            SqlCommand command = new SqlCommand();
    //            command.Connection = connection;
    //            command.CommandText = "Temp_SubUsersFiles";
    //            command.CommandType = CommandType.StoredProcedure;

    //            SqlDataAdapter dataAdapter = new SqlDataAdapter();
    //            dataAdapter.SelectCommand = command;

    //            DataTable table = new DataTable();
    //            dataAdapter.Fill(table);
    //            return table;
    //        }
    //        catch (Exception ex)
    //        {
    //            return new DataTable();
    //        }
    //        finally
    //        {
    //            connection.Close();
    //        }
    //}

    //private string InsertRecords(DataTable dt)
    //{
    //    using (SqlConnection connection = new SqlConnection(ConnectionString))
    //        try
    //        {
    //            connection.Open();
    //            SqlCommand command = new SqlCommand();
    //            command.Connection = connection;
    //            command.CommandText = "Temp_Insert";
    //            command.CommandType = CommandType.StoredProcedure;
    //            SqlParameter[] parameters = new SqlParameter[1];
    //            parameters[0] = new SqlParameter("@TempRackSpaceFiles", SqlDbType.Structured);
    //            parameters[0].Value = dt;
    //            command.Parameters.AddRange(parameters);
    //            var value = command.ExecuteScalar();
    //            return Convert.ToString(value);
    //        }
    //        catch (Exception ex)
    //        {
    //            return "";
    //        }
    //        finally
    //        {
    //            connection.Close();
    //        }
    //}
}