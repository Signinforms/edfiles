﻿using Shinetech.Engines;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Shinetech.DAL;
using iTextSharp.text.pdf;

public partial class Office_UploadPDF : System.Web.UI.Page
{
    public string virtualDir = ConfigurationManager.AppSettings["VirtualDir"];
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            String strImageName;
            HttpFileCollection files = HttpContext.Current.Request.Files;
            HttpPostedFile uploadfile = files["RemoteFile"];
            //strImageName = DateTime.Now.Ticks.ToString() + ".pdf";


            string strpage = Page.Request.QueryString["page"];
            strImageName = Page.Request.QueryString["pdfname"] + ".pdf";
            string filesavepath = "";
            if (strpage == "documentloader" || strpage == "filefolderbox")
            {
                WorkItem item = new WorkItem();
                if (strpage == "documentloader")
                {
                    item.FolderID = Convert.ToInt32(this.EffID);
                    item.DividerID = Convert.ToInt32(this.DividerID);
                }
                else
                {
                    item.FolderID = Convert.ToInt32(this.EffID_FFB);
                    item.DividerID = strpage == "filefolderbox" ? Convert.ToInt32(Page.Request.QueryString["dividerId"]) : Convert.ToInt32(this.DividerID_FFB);
                }               
                string basePath = this.MapPath(this.FolderName);

                item.Path = this.PathUrl + Path.DirectorySeparatorChar +
                            item.FolderID + Path.DirectorySeparatorChar + item.DividerID;

                item.MachinePath = string.Concat(basePath, Path.DirectorySeparatorChar, item.FolderID,
                Path.DirectorySeparatorChar, item.DividerID);

                if (!Directory.Exists(item.MachinePath))
                {
                    Directory.CreateDirectory(item.MachinePath);
                }
                filesavepath = item.MachinePath;

                item.FileName = Path.GetFileNameWithoutExtension(strImageName);
                item.FullFileName = strImageName;
                item.UID = Sessions.SwitchedSessionId;
                item.contentType = getFileExtention(Path.GetExtension(strImageName));
                item.Content = uploadfile.InputStream;
                float size = (float)Math.Round((decimal)(uploadfile.InputStream.Length / 1024) / 1024, 2); // IN MB.
                string encodeName = HttpUtility.UrlPathEncode(item.FullFileName);
                encodeName = encodeName.Replace("%", "$");
                string source = string.Concat(item.MachinePath, Path.DirectorySeparatorChar, encodeName);
                if (File.Exists(source))
                {
                    source = Common_Tatva.GetRenameFileName(source);
                    item.FileName = HttpUtility.UrlDecode(Path.GetFileNameWithoutExtension(source).Replace("$", "%"));
                }
                uploadfile.SaveAs(source);
                PdfReader pdfReader = new PdfReader(source);
                var pageCount = pdfReader.NumberOfPages;
                try
                {
                    int order = FlashViewer.GetDocumentOrder(Convert.ToInt32(item.DividerID));
                    string newDocId = General_Class.DocumentsInsert(item.FileName, item.FolderID, item.UID, item.Path, item.DividerID, pageCount, size, item.FileName, (int)item.contentType, order + 1, Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()));
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(this.EffID_FFB), Convert.ToInt32(DividerID_FFB), Convert.ToInt32(newDocId), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.ScannedUpload.GetHashCode(), 0, 0);
                    IConvert task = EngineProvider.GetEngine(item);
                    PdfDesc result = task.Execute();
                    result.DocumentOrder = order + 1;

                    if (result != null)
                    {
                        task.callbackExecute(result);
                        //FilesuploadedRecently.Add(result.FileID); TODO
                    }
                }
                catch (Exception exp)
                {
                    Common_Tatva.WriteErrorLog(Convert.ToString(item.FolderID), Convert.ToString(item.DividerID), strImageName, exp);
                    //General_Class.WriteErrorLog(Convert.ToString(item.FolderID), Convert.ToString(item.DividerID), strImageName, exp);

                    FilesuploadedRecently.Add(75);
                    FilesuploadedRecently.Add(83);
                    FilesuploadedRecently.Add(84);
                    FilesuploadedRecently.Add(85);
                    FilesuploadedRecently.Add(86);
                }
            }
            else if (strpage == "eFileSplit")
            {
                string path = Server.MapPath("~/" + System.Configuration.ConfigurationManager.AppSettings["SplitPdf"] + "/" + Sessions.SwitchedSessionId);
                strImageName = strImageName.Replace("$", "");
                strImageName = strImageName.Replace("#", "");
                string encodeName = HttpUtility.UrlPathEncode(strImageName);
                encodeName = encodeName.Replace("%", "$");
                string fileName = String.Concat(
                    path,
                    Path.DirectorySeparatorChar,
                    encodeName
                );
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                if (File.Exists(fileName))
                {
                    fileName = Common_Tatva.GetRenameFileName(fileName);
                }
                uploadfile.SaveAs(fileName);
                float size = (float)Math.Round((decimal)(uploadfile.InputStream.Length / 1024) / 1024, 2); ; // IN MB.
                string newFileName = Path.GetFileName(fileName);
                newFileName = HttpUtility.UrlDecode(newFileName).Replace('$', '%').ToString();
                General_Class.DocumentsInsertForeFileSplit(newFileName, new Guid(Sessions.SwitchedSessionId), Convert.ToDecimal(size), Enum_Tatva.IsDelete.No.GetHashCode(), Sessions.UserId);
            }
            else if (strpage == "uploadworkfile")
            {
                string path = Server.MapPath("~/ScanInBox");
                string uid = Sessions.SwitchedSessionId;

                string fullPath = Path.Combine(path, uid);
                if (!Directory.Exists(fullPath))
                {
                    Directory.CreateDirectory(fullPath);
                }
                filesavepath = fullPath;
                uploadfile.SaveAs(filesavepath + "/" + strImageName);
            }

        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(Convert.ToString(this.EffID_FFB), Convert.ToString(this.DividerID_FFB), Page.Request.QueryString["pdfname"] + ".pdf", ex);
        }

    }

    protected static StreamType getFileExtention(string ext)
    {
        StreamType streamType = StreamType.PDF;

        switch (ext.ToLower())
        {
            case ".pdf":
                streamType = StreamType.PDF;
                break;
            case ".docx":
            case ".doc":
                streamType = StreamType.Word;
                break;
            case ".xls":
                streamType = StreamType.Excel;
                break;
            case ".ppt":
                streamType = StreamType.PPT;
                break;
            case ".rtf":
                streamType = StreamType.RTF;
                break;
            case ".odt":
                streamType = StreamType.ODT;
                break;
            case ".ods":
                streamType = StreamType.ODS;
                break;
            case ".odp":
                streamType = StreamType.ODP;
                break;
            case ".jpeg":
            case ".jpg":
                streamType = StreamType.JPG;
                break;
            case ".png":
                streamType = StreamType.PNG;
                break;
            default:
                break;
        }

        return streamType;
    }

    public ArrayList FilesuploadedRecently
    {
        get
        {
            if (this.Session["FilesuploadedRecently"] != null)
            {
                return this.Session["FilesuploadedRecently"] as ArrayList;
            }
            else
            {
                this.Session["FilesuploadedRecently"] = new ArrayList();
                return this.Session["FilesuploadedRecently"] as ArrayList;
            }

        }
        set
        {
            this.Session["FilesuploadedRecently"] = value;
        }
    }

    public string PathUrl
    {
        get { return "Volume2"; }
    }

    public string FolderName
    {
        get
        {
            return "~/Volume2";
        }

    }

    public string WorkAreaName
    {
        get
        {
            return "~/ScanInBox";
        }

    }

    public string DividerID
    {
        get
        {
            return (string)this.Session["DividerID"];
        }
        set
        {
            Session["DividerID"] = value;
        }
    }

    public string EffID
    {
        get
        {
            return (string)Session["EFFID"];
        }
        set
        {
            Session["EFFID"] = value;

        }
    }

    public string DividerID_FFB
    {
        get
        {
            return (string)this.Session["DividerID_FFB"];
        }
        set
        {
            Session["DividerID_FFB"] = value;
        }
    }

    public string EffID_FFB
    {
        get
        {
            return (string)Session["EffID_FFB"];
        }
        set
        {
            Session["EffID_FFB"] = value;

        }
    }
}