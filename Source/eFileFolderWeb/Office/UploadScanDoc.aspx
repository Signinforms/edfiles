﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UploadScanDoc.aspx.cs" Inherits="Office_UploadScanDoc" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Use Dynamic Web TWAIN to Upload</title>
    <script type="text/javascript" src="Resources/dynamsoft.webtwain.initiate.js"></script>
    <script type="text/javascript" src="Resources/dynamsoft.webtwain.config.js"></script>
    <link href="../New/css/style.css" rel="stylesheet" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
</head>
<body style="background-color: transparent">
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Document Scan</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div class="content-box listing-view">

                    <div class="iframe-checklist">
                        <fieldset>
                            <select size="1" id="source" style="position: relative; width: 200px; height: 42px; font-size: 15px; margin: 5px"></select>
                            <input type="text" style="position: relative; width: 200px; height: 42px; font-size: 15px; margin: 5px; padding: 5px;" placeholder="Enter PDF name to upload" id="btnfilename" />
                            <input type="button" value="Scan" onclick="AcquireImage();" class="btn green" style="margin: 5px" />
                            <input type="button" value="Load" onclick="LoadImage();" class="btn green" style="margin: 5px" />
                            <input type="button" value="Upload" onclick="UploadImage();" class="btn green" style="margin: 5px" />
                            <%-- <input type="text" id="StrName" name="StrName" />--%>
                        </fieldset>
                        <br />
                        <br />
                        <!-- dwtcontrolContainer is the default div id for Dynamic Web TWAIN control.If you need to rename the id, you should also change the id in the dynamsoft.webtwain.config.js accordingly. -->
                        <div id="dwtcontrolContainer"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        var qrStr = window.location.search;
        var spQrStr = qrStr.substring(1);
        var arrQrStr = new Array();
        var arr = spQrStr.split('&');
        //var queryvalue = arr[0].split('=')[1];
        var queryvalue = '<%=Request.QueryString["page"]%>';
        var fileid = '<%=Request.QueryString["id"]%>';

        var virtualDir = "<%= ConfigurationManager.AppSettings["VirtualDir"] %>";

        Dynamsoft.WebTwainEnv.RegisterEvent('OnWebTwainReady', Dynamsoft_OnReady); // Register OnWebTwainReady event. This event fires as soon as Dynamic Web TWAIN is initialized and ready to be used

        var DWObject;

        function Dynamsoft_OnReady() {
            DWObject = Dynamsoft.WebTwainEnv.GetWebTwain('dwtcontrolContainer'); // Get the Dynamic Web TWAIN object that is embeded in the div with id 'dwtcontrolContainer'
            if (DWObject) {
                var count = DWObject.SourceCount; // Populate how many sources are installed in the system
                for (var i = 0; i < count; i++)
                    document.getElementById("source").options.add(new Option(DWObject.GetSourceNameItems(i), i));  // Add the sources in a drop-down list

                $('.ds-dialog-wrap').remove();
                $('.ds-dwt-container').remove();
                //document.getElementById("imgTypejpeg").checked = true;
            }
        }

        function AcquireImage() {
            if (DWObject) {
                DWObject.SelectSourceByIndex(document.getElementById("source").selectedIndex);
                DWObject.OpenSource();
                DWObject.IfDisableSourceAfterAcquire = true;	// Scanner source will be disabled/closed automatically after the scan.
                DWObject.AcquireImage();
            }
        }

        //Callback functions for async APIs
        function OnSuccess() {
            console.log('successful');
        }

        function OnFailure(errorCode, errorString) {
            alert(errorString);
        }

        function LoadImage() {
            if (DWObject) {
                DWObject.IfShowFileDialog = true; // Open the system's file dialog to load image
                DWObject.LoadImageEx("", EnumDWT_ImageType.IT_ALL, OnSuccess, OnFailure); // Load images in all supported formats (.bmp, .jpg, .tif, .png, .pdf). sFun or fFun will be called after the operation
            }
        }

        // OnHttpUploadSuccess and OnHttpUploadFailure are callback functions.
        // OnHttpUploadSuccess is the callback function for successful uploads while OnHttpUploadFailure is for failed ones.
        function OnHttpUploadSuccess() {
            console.log('successful');
            if (queryvalue == "documentloader") {
                setTimeout(function () {
                    window.parent.location.href = virtualDir + "Office/DocumentLoading.aspx";
                }, 3000);
                //window.parent.location.href = virtualDir + "Office/DocumentLoader1.aspx";
            }
            else if (queryvalue == "filefolderbox")
                window.parent.location.href = virtualDir + "Office/FileFolderBox.aspx?id=" + fileid;
            else
                window.parent.location.href = virtualDir + "Office/UploadWorkFile.aspx";
            $('.ds-dialog-wrap').remove();
            $('.ds-dwt-container').remove();
        }

        function OnHttpUploadFailure(errorCode, errorString, sHttpResponse) {
            alert(errorString + sHttpResponse);
        }

        function IsNullOrWhiteSpace(value) {

            if (value == null) return true;

            return value.replace(/\s/g, '').length == 0;
        }

        function UploadImage() {
            var strpsdname = document.getElementById("btnfilename").value;
            if (IsNullOrWhiteSpace(strpsdname)) {
                alert("Please enter PDF name to upload");
            }
            else {
                if (DWObject) {
                    // If no image in buffer, return the function
                    if (DWObject.HowManyImagesInBuffer == 0)
                        return;

                    var strHTTPServer = location.hostname; //The name of the HTTP server. For example: "www.dynamsoft.com";
                    var CurrentPathName = unescape(location.pathname);
                    var CurrentPath = CurrentPathName.substring(0, CurrentPathName.lastIndexOf("/") + 1);
                    var strActionPage = CurrentPath + "UploadPDF.aspx?page=" + queryvalue + "&pdfname=" + strpsdname;
                    DWObject.IfSSL = false; // Set whether SSL is used
                    DWObject.HTTPPort = location.port == "" ? 80 : location.port;
                    var Digital = new Date();
                    var uploadfilename = Digital.getMilliseconds(); // Uses milliseconds according to local time as the file name

                    DWObject.HTTPUploadAllThroughPostAsPDF(strHTTPServer, strActionPage, uploadfilename + ".pdf", OnHttpUploadSuccess, OnHttpUploadFailure);
                }
            }
        }

    </script>
</body>
</html>

