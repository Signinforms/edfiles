﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UploadScanDocNew.aspx.cs" Inherits="Office_UploadScanDocNew" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../New/css/style.css" rel="stylesheet" />
    <title>Use Dynamic Web TWAIN to Upload</title>
    <script type="text/javascript" src="Resources/dynamsoft.webtwain.initiate.js"></script>
    <script type="text/javascript" src="Resources/dynamsoft.webtwain.config.js"></script>
    <script src="../New/js/menu/jquery-1.10.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../New/js/jquery.min.js"></script>
    <link href="../New/css/chosen.css" rel="stylesheet" />
    <script src="../New/js/chosen.jquery.js" type="text/javascript"></script>
</head>
<body style="background-color: transparent">
    <style>
        .btnAdd {
            padding: 8px 10px 8px 30px;
            font-size: 15px;
            padding-right: 10px;
            line-height: normal;
            float: right;
            width: auto !important;
            background-image: url(../images/plus-icon.png) !important;
            background-position: 10px center !important;
            background-repeat: no-repeat !important;
        }

        .btnRemove {
            padding: 8px 10px 8px 30px;
            font-size: 15px;
            padding-right: 10px;
            line-height: normal;
            float: right;
            width: auto !important;
            background-image: url(../images/remove-icon.png) !important;
            background-position: 8px center !important;
            background-repeat: no-repeat !important;
            margin-left: 10px;
        }

        .redAlert {
            border: solid red 1px;
        }

        #alertMax {
            color: red;
            line-height: 50px;
            margin-bottom: -50px;
        }

        #alertMaxTop {
            color: red;
            line-height: 50px;
            margin-bottom: -50px;
        }

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }

        #floatingCirclesG {
            top: 50%;
            left: 50%;
            position: fixed;
        }

        #floatingCirclesG {
            width: 128px;
            height: 128px;
            -webkit-transform: scale(0.6);
            -moz-transform: scale(0.6);
            -o-transform: scale(0.6);
            -ms-transform: scale(0.6);
        }

        .f_circleG {
            position: absolute;
            background-color: #f6f4f4;
            height: 23px;
            width: 23px;
            -moz-border-radius: 12px;
            -webkit-border-radius: 12px;
            border-radius: 12px;
            -webkit-animation-name: f_fadeG;
            -webkit-animation-duration: 1.04s;
            -webkit-animation-iteration-count: infinite;
            -webkit-animation-direction: linear;
            -moz-animation-name: f_fadeG;
            -moz-animation-duration: 1.04s;
            -moz-animation-iteration-count: infinite;
            -moz-animation-direction: linear;
            -o-animation-name: f_fadeG;
            -o-animation-duration: 1.04s;
            -o-animation-iteration-count: infinite;
            -o-animation-direction: linear;
            -ms-animation-name: f_fadeG;
            -ms-animation-duration: 1.04s;
            -ms-animation-iteration-count: infinite;
            -ms-animation-direction: linear;
        }

        #frotateG_01 {
            left: 0;
            top: 52px;
            -webkit-animation-delay: 0.39s;
            -moz-animation-delay: 0.39s;
            -o-animation-delay: 0.39s;
            -ms-animation-delay: 0.39s;
        }

        #frotateG_02 {
            left: 15px;
            top: 15px;
            -webkit-animation-delay: 0.52s;
            -moz-animation-delay: 0.52s;
            -o-animation-delay: 0.52s;
            -ms-animation-delay: 0.52s;
        }

        #frotateG_03 {
            left: 52px;
            top: 0;
            -webkit-animation-delay: 0.65s;
            -moz-animation-delay: 0.65s;
            -o-animation-delay: 0.65s;
            -ms-animation-delay: 0.65s;
        }

        #frotateG_04 {
            right: 15px;
            top: 15px;
            -webkit-animation-delay: 0.78s;
            -moz-animation-delay: 0.78s;
            -o-animation-delay: 0.78s;
            -ms-animation-delay: 0.78s;
        }

        #frotateG_05 {
            right: 0;
            top: 52px;
            -webkit-animation-delay: 0.91s;
            -moz-animation-delay: 0.91s;
            -o-animation-delay: 0.91s;
            -ms-animation-delay: 0.91s;
        }

        #frotateG_06 {
            right: 15px;
            bottom: 15px;
            -webkit-animation-delay: 1.04s;
            -moz-animation-delay: 1.04s;
            -o-animation-delay: 1.04s;
            -ms-animation-delay: 1.04s;
        }

        #frotateG_07 {
            left: 52px;
            bottom: 0;
            -webkit-animation-delay: 1.17s;
            -moz-animation-delay: 1.17s;
        }

        #frotateG_08 {
            left: 15px;
            bottom: 15px;
            -webkit-animation-delay: 1.3s;
            -moz-animation-delay: 1.3s;
            -o-animation-delay: 1.17s;
            -ms-animation-delay: 1.17s;
        }

        #frotateG_08 {
            left: 15px;
            bottom: 15px;
            -webkit-animation-delay: 1.3s;
            -moz-animation-delay: 1.3s;
            -o-animation-delay: 1.3s;
            -ms-animation-delay: 1.3s;
        }
    </style>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Document Scan</h1>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="page-container register-container">
        <div class="inner-wrapper">
            <h3 id="alertMaxTop" style="display: none;">Maximum number of files added !!</h3>
            <input type="button" value="Upload All" onclick="UploadImage();" class="btn green" style="margin-bottom: -10px; float: right" />
            <div class="form-containt listing-contant">

                <div class="content-box listing-view" data-id="0">
                    <div class="iframe-checklist">
                        <div style="float: left; width: 30%;">
                            <!-- dwtcontrolContainer is the default div id for Dynamic Web TWAIN control.If you need to rename the id, you should also change the id in the dynamsoft.webtwain.config.js accordingly. -->
                            <div id="dwtcontrolContainer0"></div>
                        </div>
                        <div style="float: left; width: 70%;">
                            <table style="width: 100%;">
                                <tr>
                                    <td>
                                        <select size="1" id="source0" class="drpSelect" style="position: relative; width: 300px; height: 42px; font-size: 15px"></select>
                                        <input type="button" value="Scan" onclick="AcquireImage(0);" class="btn green btnScan" style="margin-left: 2%; width: 10% !important" />
                                        <input type="button" value="Browse" title="Load File" onclick="LoadImage(0);" class="btn green btnLoad" style="margin-left: 2%; width: 10% !important" />
                                        <select id="comboBox" style="position: relative; width: 195px; height: 42px; font-size: 15px; margin-left: 15px"></select>
                                        <input type="button" title="Remove Document" onclick="RemoveDocument(0);" class="btn green btnRemove" style="display: none;" />
                                        <input type="button" title="Add New Document" onclick="AddDocument(this);" class="btn green btnAdd" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 3%">
                                        <input type="text" style="position: relative; width: 300px; height: 42px; font-size: 15px; padding: 5px;" placeholder="Enter PDF name to upload" class="btnFileName" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <h3 id="alertMax" style="display: none;">Maximum number of files added !!</h3>
                <input type="button" value="Upload All" onclick="UploadImage();" class="btn green" style="margin-top: 10px; float: right" />
            </div>
        </div>
    </div>

    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>

    <script type="text/javascript">
        var queryvalue = '<%=Request.QueryString["page"]%>';
        var virtualDir = "<%= ConfigurationManager.AppSettings["VirtualDir"] %>";
        var fileid = '<%=Request.QueryString["id"]%>';
        var tabIndex = '<%= Request.QueryString["TabIndex"] %>';
        var DWObjects = [];

        CreateDWTObjects(0);
        LoadTab()
        //Dynamsoft.WebTwainEnv.RegisterEvent('OnWebTwainReady'); // Register OnWebTwainReady event. This event fires as soon as Dynamic Web TWAIN is initialized and ready to be used

        function CreateDWTObjects(i) {
            Dynamsoft.WebTwainEnv.CreateDWTObject(
                "dwtcontrolContainer" + i,
                function (newDWObject) {
                    DWObjects.push(newDWObject);
                    var index = DWObjects.length - 1;
                    if (DWObjects[index]) {
                        var count = DWObjects[index].SourceCount; // Populate how many sources are installed in the system
                        for (var j = 0; j < count; j++)
                            document.getElementById("source" + index).options.add(new Option(DWObjects[index].GetSourceNameItems(j), j));  // Add the sources in a drop-down list
                    }
                },
                function (errorString) { alert(errorString); }
            );
        }

        function AddDocument(ele) {
            var index = $('.content-box').last().attr("data-id");
            var visibleIndex = $('.content-box:visible').last().attr("data-id");
            if (DWObjects[visibleIndex].HowManyImagesInBuffer > 0) {
                if ($('.content-box:visible').last().find('.btnFileName').val() == '' || $('.content-box:visible').last().find('.btnFileName').val() == undefined) {
                    alert("Please enter valid file name");
                    return;
                }
                else {
                    if ($('.content-box:visible').length < 10) {
                        var newContent = $('.content-box').last().clone();
                        var newIndex = parseInt(index) + 1;
                        $(newContent).css("margin-top", "2%");
                        $(newContent).attr("data-id", newIndex);
                        $(newContent).find('.btnScan').attr("onclick", "AcquireImage(" + newIndex + ");");
                        $(newContent).find('.btnLoad').attr("onclick", "LoadImage(" + newIndex + ");");
                        $(newContent).find('.btnRemove').attr("onclick", "RemoveDocument(" + newIndex + ");");
                        $(newContent).find('.btnRemove').removeAttr("style");
                        $(newContent).find('.btnFileName').val("");
                        $(newContent).find("#source" + index).find("option").remove();
                        $(newContent).find("#source" + index).attr("id", "source" + newIndex);
                        $(newContent).find("#dwtcontrolContainer" + index).attr("id", "dwtcontrolContainer" + newIndex);
                        $(ele).hide();
                        $(ele).parent().find('.btnScan').hide();
                        $(ele).parent().find('.btnLoad').hide();
                        $(ele).parent().find('.btnRemove').removeAttr("style");
                        $(newContent).insertAfter($('.content-box').last());
                        $(newContent).show();
                        DWObjects[visibleIndex].CloseSource();
                        CreateDWTObjects(newIndex);
                        if ($('.content-box:visible').length > 9) {
                            $(newContent).find('.btnAdd').hide();
                            //$(newContent).find('.btnRemove').removeAttr("style");
                            $("#alertMaxTop").show();

                            $("#alertMax").show();
                        }
                        else {
                            $(newContent).find('.btnAdd').show();
                            //$(newContent).find('.btnRemove').hide();
                        }
                    }
                    else {
                        alert("Already added max documents");
                        return;
                    }
                }
            }
            else {
                alert("Please load or scan a document");
                return;
            }
        }

        function RemoveDocument(i) {
            $('.content-box[data-id="' + i + '"]').hide();
            DWObjects[i] = null;
            $("#alertMax").hide();
            $("#alertMaxTop").hide();
            if ($('.content-box:visible').last().find('.btnRemove:visible').length > 0) {
                $('.content-box:visible').last().find('.btnRemove').hide();
                $('.content-box:visible').last().find('.btnAdd').show();
                $('.content-box:visible').last().find('.btnLoad').show();
                $('.content-box:visible').last().find('.btnScan').show();
            }
        }

        function IsNullOrWhiteSpace(value) {

            if (value == null) return true;

            return value.replace(/\s/g, '').length == 0;
        }

        function AcquireImage(i) {
            if (DWObjects[i]) {
                DWObjects[i].SelectSourceByIndex(document.getElementById("source" + i).selectedIndex);
                DWObjects[i].OpenSource();
                DWObjects[i].IfDisableSourceAfterAcquire = true;	// Scanner source will be disabled/closed automatically after the scan.
                DWObjects[i].AcquireImage();
            }
        }

        function OnFailure(errorCode, errorString) {
            alert(errorString);
        }

        function OnSuccess() { }

        function LoadImage(i) {
            if (DWObjects[i]) {
                DWObjects[i].IfShowFileDialog = true; // Open the system's file dialog to load image
                DWObjects[i].LoadImageEx("", EnumDWT_ImageType.IT_ALL, OnSuccess, OnFailure); // Load images in all supported formats (.bmp, .jpg, .tif, .png, .pdf). sFun or fFun will be called after the operation
            }
        }

        // OnHttpUploadSuccess and OnHttpUploadFailure are callback functions.
        // OnHttpUploadSuccess is the callback function for successful uploads while OnHttpUploadFailure is for failed ones.
        function OnHttpUploadSuccess(i) {
            DWObjects[i] = null;
            $('.content-box[data-id=' + i + ']').hide();
        }

        function OnHttpUploadFailure(errorCode, errorString, sHttpResponse) {
            isValid = false;
        }

        function asyncFailureFunc(errorCode, errorString) {
            alert("ErrorCode: " + errorCode + "\r" + "ErrorString:" + errorString);
        }

        function HideColorBox() {
            $('#colorbox', parent.document).hide();
        }

        var isValid = true;
        function UploadImage() {
            isValid = true;
            $('.content-box:visible').find('.btnFileName').each(function () {
                if ($(this).val() == "" || $(this).val() == undefined) {
                    isValid = false;
                    $(this).addClass('redAlert');
                }
                else
                    $(this).removeClass('redAlert');
            });
            if (isValid) {
                $('#colorbox', parent.document).hide();
                setTimeout(function () {
                    var strHTTPServer = location.hostname; //The name of the HTTP server. For example: "www.dynamsoft.com";
                    var CurrentPathName = unescape(location.pathname);
                    var CurrentPath = CurrentPathName.substring(0, CurrentPathName.lastIndexOf("/") + 1);
                    var strActionPage = CurrentPath + "UploadPDF.aspx?page=" + queryvalue + "&pdfname=";
                    for (var i = 0; i < DWObjects.length; i++) {
                        if (DWObjects[i]) {
                            var fileName = $('.content-box[data-id=' + i + ']').find('.btnFileName').val();
                            var dividerId = $('.content-box[data-id=' + i + ']').find('#comboBox').val();
                            if (DWObjects[i].HowManyImagesInBuffer == 0) {
                                isValid = false;
                                continue;
                            }
                            else {
                                var isLive = '<%= ConfigurationManager.AppSettings["IsLive"] %>';
                                if (isLive == 'true')
                                    DWObjects[i].IfSSL = true; // Set whether SSL is used
                                else
                                    DWObjects[i].IfSSL = false; // Set whether SSL is used
                                DWObjects[i].HTTPPort = location.port == "" ? 443 : location.port;
                                var newActionPage = strActionPage + fileName;
                                newActionPage = newActionPage + "&dividerId=" + dividerId;
                                var Digital = new Date();
                                var uploadfilename = Digital.getMilliseconds(); // Uses milliseconds according to local time as the file name
                                DWObjects[i].HTTPUploadAllThroughPostAsPDF(strHTTPServer, newActionPage, uploadfilename + ".pdf", OnHttpUploadSuccess(i), OnHttpUploadFailure);
                            }
                        }
                    }
                    if (isValid) {
                        alert("Document(s) have been uploaded successfully.")
                        if (queryvalue == "documentloader") {
                            setTimeout(function () {
                                window.parent.location.href = virtualDir + "Office/DocumentLoading.aspx";
                            }, 3000);
                        }
                        else if (queryvalue == "filefolderbox")
                            window.parent.location.href = virtualDir + "Office/FileFolderBox.aspx?id=" + fileid + "&tabIndex=" + tabIndex;
                        else if (queryvalue == "eFileSplit")
                            window.parent.location.href = virtualDir + "Office/eFileSplit.aspx";
                        else
                            window.parent.location.href = virtualDir + "Office/UploadWorkFile.aspx";
                    }
                    else {
                        $('#colorbox', parent.document).show();
                        if ($('.content-box:visible').length < 10) {
                            if ($('.content-box:visible').last().find('.btnRemove:visible').length > 0) {
                                $('.content-box:visible').last().find('.btnAdd').show();
                                $('.content-box:visible').last().find('.btnRemove').hide();
                            }
                            $("#alertMax").hide();
                            $("#alertMaxTop").hide();
                        }
                        alert("Having issue in uploading some of the file(s). Please try again !!");
                    }
                }, 100);
            }
            else alert("Please enter valid file name");
        }


        function LoadTab() {
            $("#comboBox").html('');
            $("#comboBox").chosen("destroy");
            $.ajax(
                {
                    type: "POST",
                    url: 'UploadScanDocNew.aspx/GetDividerByEffid',
                    contentType: "application/json; charset=utf-8",
                    data: '{"folderId":"' + fileid + '"}',
                    dataType: "json",
                    success: function (result) {
                        if (result.d) {
                            $.each(result.d, function (i, text) {
                                $('<option />', { value: i, text: text }).appendTo($("#comboBox"));
                            });
                        }
                    },
                    fail: function (data) {
                        alert("Failed to get Folders. Please try again!");
                    }
                });
        }
    </script>
</body>
</html>
