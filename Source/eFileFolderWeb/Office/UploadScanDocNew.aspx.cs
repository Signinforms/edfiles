﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Office_UploadScanDocNew : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    [WebMethod]
    public static Dictionary<string, string> GetDividerByEffid(int folderId)
    {
        Dictionary<string, string> dict = new Dictionary<string, string>();
        try
        {
            DataTable dtUsers = FileFolderManagement.GetDividerByEffid(folderId);
            if (dtUsers != null)
            {
                dtUsers.AsEnumerable().ToList().ForEach(d =>
                {
                    dict.Add(Convert.ToString(d.Field<int>("DividerID")), d.Field<string>("Name"));

                });
            }
        }
        catch (Exception ex)
        { }
        return dict;
    }

}