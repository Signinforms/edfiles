<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="UploadWorkFile.aspx.cs" Inherits="UploadWorkFile" Title="" %>

<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js"></script>
    <script language="javascript">

        $(function () {
            $('#<%= tagsXmlContent.ClientID %>').hide();
         });

        $(function () {
            var dWidth = window.screen.width;
            var dHeight = window.screen.height;
            var aWidth = Math.floor((80 * window.screen.width) / 100);
            var aHeight = Math.floor((57 * aWidth) / 100);

            $(".iframe").colorbox({
                iframe: true, width: "80%", height: "95%", onComplete: function () {
                    //iframe: true, width: aWidth, height: aHeight, onComplete: function () {
                    $('iframe').live('load', function () {
                        $('iframe').contents().find("head")
                          .append($("<style type='text/css'>  .splash{overflow:visible;}  </style>"));
                    });
                }
            });
        });

        function ScanDocFile() {
            $('.iframe').attr('href', 'UploadScanDoc.aspx?page=uploadworkfile').trigger('click');
        }

        function onClientUploadComplete(sender, e) {
            //onImageValidated("TRUE", e);
        }

        function onImageValidated(arg, context) {

            var test = document.getElementById("testuploaded");
            test.style.display = 'block';

            var fileList = document.getElementById("fileList");
            var item = document.createElement('div');
            item.style.padding = '4px';

            item.appendChild(createFileInfo(context));

            fileList.appendChild(item);
        }

        function createFileInfo(e) {
            var holder = document.createElement('div');
            holder.appendChild(document.createTextNode(e.get_fileName() + ' with size ' + e.get_fileSize() + ' bytes'));

            return holder;
        }



        function onClientUploadStart(sender, e) {
            document.getElementById('uploadCompleteInfo').innerHTML = 'Please wait while uploading ' + e.get_filesInQueue() + ' files...';
        }

        function onClientUploadCompleteAll(sender, e) {

            var args = JSON.parse(e.get_serverArguments()),
                unit = args.duration > 60 ? 'minutes' : 'seconds',
                duration = (args.duration / (args.duration > 60 ? 60 : 1)).toFixed(2);

            var info = 'At <b>' + args.time + '</b> server time <b>'
                + e.get_filesUploaded() + '</b> of <b>' + e.get_filesInQueue()
                + '</b> files were uploaded with status code <b>"' + e.get_reason()
                + '"</b> in <b>' + duration + ' ' + unit + '</b>';

            document.getElementById('uploadCompleteInfo').innerHTML = info;

            window.location = 'DocumentLoader1.aspx';
        }

    </script>

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Upload Work Area Files</h1>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div class="content-box">
                    <%--<fieldset>
                        <a href="javascript:;" style="font-family: Trebuchet MS;" onclick="AddNewTag()">Add New Custom field</a>
                        <div class='clearfix'></div>
                        <div id="taggrid"></div>
                    </fieldset>--%>

                    <fieldset>
                        <p style="margin:0px !important">Click '<i>Select File</i>' for scans uploading.</p>
                    </fieldset>
                    <div style="margin-top: 20px">
                         <asp:TextBox ID="tagsXmlContent" runat="server" Visible="true" Value="<Tags><document><Id>0</Id><FileName>Name of File</FileName><TagKey></TagKey><TagValue></TagValue></document></Tags>" />
                        <asp:Label runat="server" ID="myThrobber" Style="display: none;">
                                            <img align="absmiddle" alt="" src="uploading.gif"/>
                        </asp:Label>
                        <ajaxToolkit:AjaxFileUpload ID="AjaxFileUpload1" runat="server" Padding-Bottom="4"
                            Padding-Left="2" Padding-Right="1" Padding-Top="4" ThrobberID="myThrobber" OnClientUploadComplete="onClientUploadComplete"
                            OnUploadComplete="AjaxFileUpload1_OnUploadComplete" MaximumNumberOfFiles="10"
                            AllowedFileTypes="jpg,jpeg,pdf,doc,docx,xls" AzureContainerName="" OnClientUploadCompleteAll="onClientUploadCompleteAll"
                            OnUploadCompleteAll="AjaxFileUpload1_UploadCompleteAll" OnUploadStart="AjaxFileUpload1_UploadStart"
                            OnClientUploadStart="onClientUploadStart" />
                        <div id="uploadCompleteInfo">
                        </div>
                    </div>
                    <fieldset>
                        <input type="button" class="create-btn btn-small green" id="btnScandoc" name="btnScandoc" runat="server" causesvalidation="false" value="Scan Document" onclick="ScanDocFile();" />
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
    <a class='iframe' href="javascript:;" style="display: none;">popup</a>

    <script language="javascript">
        var hidTagsXml = document.getElementById("<%=tagsXmlContent.ClientID%>");

        var TemplateXml = hidTagsXml.value; //"<Tags><document><Id>0</Id><FileName>Name of File</FileName><TagKey>TagKey</TagKey><TagValue>SomeName</TagValue></document></Tags>";

        function getDocument() {
            var xmlDoc = null;
            // TemplateXml = hidTagsXml.value;

            if (window.DOMParser) {
                parser = new DOMParser();
                xmlDoc = parser.parseFromString(TemplateXml, "text/xml");
            }
            else {
                xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                xmlDoc.async = false;
                xmlDoc.loadXML(TemplateXml);
            }
            return xmlDoc;
        }


        //function GenerateGrid() {

        //    var xmlDoc = getDocument();
        //    //var strHTML = '<table id="tbltags" style="border-width:2px;border-style:solid;width:100%;border-collapse:collapse;"><tr class="form_title" style="background-color:#DBD9CF;text-align:left;" ><th style="display:none;">File Name</th><th>Custom Field Label</th><th>Custom Field Value</th><th>Action</th></tr>';
        //    var strHTML = '<table id="tbltags" class="datatable listing-datatable">' +
        //        '<tr>' +
        //            '<th style="display:none;">File Name</th>' +
        //            '<th>Custom Field Label</th>' +
        //            '<th>Custom Field Value</th>' +
        //            '<th style="width:90px;">Action</th>' +
        //        '</tr>';
        //    if (xmlDoc != null) {
        //        var ListOfElements = xmlDoc.getElementsByTagName('document');
        //        if (ListOfElements != null) {
        //            for (var i = 0; i < ListOfElements.length; i++) {
        //                strHTML += '<tr id="TRE-' + ListOfElements[i].childNodes[0].innerHTML + '"><td style="display:none;">' + ListOfElements[i].childNodes[1].innerHTML + '</td><td>' + ListOfElements[i].childNodes[2].innerHTML + '</td><td>' + ListOfElements[i].childNodes[3].innerHTML + '</td><td><a onclick="Edit(\'TRE-' + ListOfElements[i].childNodes[0].innerHTML + '\')" href="javascript:;" class="ic-icon ic-edit">Edit</a>&nbsp;<a href="javascript:;" onclick="RemoveTag(\'TRE-' + ListOfElements[i].childNodes[0].innerHTML + '\')" class="ic-icon ic-delete">Delete</a></td></tr>';
        //            }
        //        }
        //    }
        //    strHTML += '</table>';
        //    document.getElementById('taggrid').innerHTML = strHTML;

        //}

        function Edit(id) {
            try {
                id = id.split('-')[1];
                id = parseInt(id) + 1;

                if (id != null) {
                    var fileFileName = "";
                    var TagKey = "";
                    var TagValue = "";

                    var action = document.getElementById("tbltags").rows[id].cells[3].firstChild.innerHTML;
                    if (action == "Edit") {

                        fileFileName = document.getElementById("tbltags").rows[id].cells[0].innerHTML;
                        TagKey = document.getElementById("tbltags").rows[id].cells[1].innerHTML;
                        TagValue = document.getElementById("tbltags").rows[id].cells[2].innerHTML;

                        var tagKeyElement = '<input type="text" id="editTheTagKey" value="' + TagKey + '" >';
                        var tagValueElement = '<input type="text" id="editTheTagValue" value="' + TagValue + '" >';

                        document.getElementById("tbltags").rows[id].cells[1].innerHTML = tagKeyElement;
                        document.getElementById("tbltags").rows[id].cells[2].innerHTML = tagValueElement;
                        document.getElementById("tbltags").rows[id].cells[3].firstChild.innerHTML = "Update";

                    }
                    else {
                        fileFileName = document.getElementById("tbltags").rows[id].cells[0].innerHTML;
                        TagKey = document.getElementById("editTheTagKey").value;
                        TagValue = document.getElementById("editTheTagValue").value;

                        document.getElementById("tbltags").rows[id].cells[1].innerHTML = TagKey;
                        document.getElementById("tbltags").rows[id].cells[2].innerHTML = TagValue;
                        document.getElementById("tbltags").rows[id].cells[3].firstChild.innerHTML = "Edit";
                        UpdateXmlDocumentForTags(id, fileFileName, TagKey, TagValue);
                    }
                }
            }
            catch (ex) {
                //  alert(ex.message);
            }
        }

        function UpdateXmlDocumentForTags(id, fileName, tagKey, tagValue) {
            id = parseInt(id) - 1;
            var xmlDoc = getDocument();
            if (xmlDoc != null) {
                var ListOfElements = xmlDoc.getElementsByTagName('document');
                if (ListOfElements != null) {
                    for (var i = 0; i < ListOfElements.length; i++) {
                        if (i == id) {
                            ListOfElements[i].childNodes[1].innerHTML = fileName;
                            ListOfElements[i].childNodes[2].innerHTML = tagKey;
                            ListOfElements[i].childNodes[3].innerHTML = tagValue;
                        }
                    }
                }
                var xmlString = (new XMLSerializer()).serializeToString(xmlDoc);
                hidTagsXml.value = xmlString;
                TemplateXml = xmlString;
            }
        }

        function AddNewTag() {
            var rows = document.getElementById("tbltags").rows;
            var id = rows.length - 1;
            var xmlDoc = getDocument();
            if (xmlDoc != null) {
                var documentNode = xmlDoc.createElement('document');
                var InnerNode = '<Id>' + id.toString() + '</Id><FileName></FileName><TagKey></TagKey><TagValue></TagValue>';
                documentNode.innerHTML = InnerNode;
                var MasterTags = xmlDoc.getElementsByTagName('Tags');
                if (MasterTags != null) {
                    MasterTags[0].appendChild(documentNode);
                    var xmlString = (new XMLSerializer()).serializeToString(xmlDoc);
                    TemplateXml = xmlString;
                    GenerateGrid();
                    Edit('TRE-' + id.toString());

                }
            }
        }

        function RemoveTag(id) {

            id = id.split('-')[1];
            //id = parseInt(id) + 1;            

            var xmlDoc = getDocument();
            if (xmlDoc != null) {
                var ListOfElements = xmlDoc.getElementsByTagName('document');
                if (ListOfElements != null) {
                    for (var i = 0; i < ListOfElements.length; i++) {
                        if (i == parseInt(id)) {
                            xmlDoc.documentElement.removeChild(ListOfElements[i]);
                            break;
                        }
                    }
                }
                var xmlString = (new XMLSerializer()).serializeToString(xmlDoc);
                hidTagsXml.value = xmlString;
                TemplateXml = xmlString;
                GenerateGrid();
            }

        }

        GenerateGrid();
    </script>
</asp:Content>
