﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucAddToTab.ascx.cs" Inherits="Office_UserControls_ucAddToTab" %>

<link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>

<style>
    #comboboxDivider_chosen {
        margin-top:5px;
        display:block;
    }
    #combobox_chosen {
        display:block;
        margin-top:5px;
    }
    .chosen-results {
        max-height:100px !important;
    }
</style>

<label>1. Select an edFile from list</label>
<select id="combobox">
</select>

<label style="display:block; margin-top:15px">2. Select a tab for the selected edFile</label>
<select id="comboboxDivider" style="display: none;">
</select>

<script type="text/javascript">
    function LoadFolders() {
        $.ajax(
          {
              type: "POST",
              url: '../FlexWebService.asmx/GetFoldersData',
              contentType: "application/json; charset=utf-8",
              dataType: "json",
              success: function (data) {
                  $('<option />', { value: 0, text: "Select a file" }).appendTo($("#combobox"));
                  $.each(data.d, function (i, text) {
                      $('<option />', { value: i, text: text }).appendTo($("#combobox"));
                  });
                  $("#combobox").chosen({ width: "315px" });
                 
                  DisplayDivider();
              },
              fail: function (data) {
              }
          });
    }

    $("#combobox").change(function (event) {
        DisplayDivider();
    });

    function DisplayDivider() {
        $("#comboboxDivider").html('');
        $("#comboboxDivider").chosen("destroy");
        var selectedFolder = $("#combobox").chosen().val();
        $.ajax(
         {
             type: "POST",
             url: '../FlexWebService.asmx/GetDividersByEFF',
             data: "{eff:" + parseInt(selectedFolder) + "}",
             contentType: "application/json; charset=utf-8",
             dataType: "json",
             success: function (data) {
                 //$('<option />', { value: 0, text: "Select a tab" }).appendTo($("#comboboxDivider"));
                 $.each(data.d, function (i, text) {
                     $('<option />', { value: i, text: text }).appendTo($("#comboboxDivider"));
                 });
                 $("#comboboxDivider").chosen({ width: "315px" });
             },
             fail: function (data) {
             }
         });
    }
</script>