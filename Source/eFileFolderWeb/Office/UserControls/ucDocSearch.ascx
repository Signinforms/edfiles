﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucDocSearch.ascx.cs" Inherits="Office_UserControls_ucDocSearch" %>
<style type="text/css">
    .searchTab {
        width: 100%;
        height: 34px;
        padding-right: 5px;
        padding: 5px;
        border: 2px solid #d4d4d4;
        margin-top: 1%;
    }

    #searchGrid {
        padding: 1%;
        display: none;
    }

    .yellow {
        background-color: yellow;
    }

    label {
        font-size: 14px;
        display: inline-block;
    }

    .scrollable {
        max-height: 200px !important;
        overflow: hidden;
        display: inline-block;
        max-width: 100%;
        overflow: auto;
    }

    .btnNav {
        border-radius: 4px;
        height: 25px;
        width: 25px;
        cursor: pointer;
        float: right;
        margin-top: 10px;
    }

    .lnkExpand {
        text-decoration: underline;
        font-size: 17px;
        color: #83af33;
        margin-left: 10px;
        margin-top: 10px;
        float: right;
        cursor: pointer;
    }

    .lnkCollapse {
        text-decoration: underline;
        font-size: 17px;
        color: #01487f;
        margin-left: 10px;
        margin-top: 10px;
        float: right;
        cursor: pointer;
        display: none;
    }

    .btnPrevEnabled {
        background: url(../images/prev_green.png) 2px 3px no-repeat white;
        border: solid 1px #83af33;
    }

    .btnNextEnabled {
        margin-left: 10px;
        background: url(../images/next_green.png) 4px 3px no-repeat white;
        border: solid 1px #83af33;
    }

    .btnPrevDisabled {
        background: url(../images/prev_blue.png) 2px 3px no-repeat white;
        border: solid 1px #01487f;
        cursor: not-allowed;
        opacity: 0.2;
    }

    .btnNextDisabled {
        margin-left: 10px;
        background: url(../images/next_blue.png) 4px 3px no-repeat white;
        border: solid 1px #01487f;
        cursor: not-allowed;
        opacity: 0.2;
    }

    .pager_drop {
        float: right;
        margin-top: 10px;
        height: 30px;
        width: 50px;
        font-size: 20px;
    }

    .btnEdit {
        text-align: center !important;
        height: 30px;
        width: 94px;
        background-color: #94ba33;
        color: white;
        font-weight: bold;
        font-size: 21px;
        margin-top: 5px;
        border: none;
        cursor: pointer;
    }

        .btnEdit:hover {
            background-color: #01487f;
        }

    .btnNextPreview {
        padding: 8px 10px 8px 35px;
        font-size: 15px;
        padding-right: 10px;
        width: auto;
        height: auto;
        line-height: normal;
        margin-right: 5px;
        background-image: url(../images/left-arrow.png);
        background-position: 7px center;
        background-repeat: no-repeat;
        float: left;
    }

    .btnPrevPreview {
        padding: 8px 30px 8px 10px;
        font-size: 15px;
        width: auto;
        height: auto;
        line-height: normal;
        margin-right: 5px;
        background-image: url(../images/right-arrow.png);
        background-position: 50px center;
        background-repeat: no-repeat;
        float: right;
    }

    #popupdocclose {
        float: right;
        padding: 10px;
        cursor: pointer;
    }
</style>
<asp:HiddenField ID="hdnSelectedPath" runat="server" />
<asp:HiddenField ID="hdnObjId" runat="server" />
<asp:HiddenField ID="hdnSearchedId" runat="server" />
<asp:HiddenField ID="hdnNewFileNameOnly" runat="server" />
<asp:HiddenField ID="hdnFileNameOnly" runat="server" />
<asp:HiddenField ID="hdnFolderId" runat="server" />
<asp:HiddenField ID="hdnDividerId" runat="server" />
<asp:HiddenField ID="hdnFileId" runat="server" />

<div class="popup-mainbox preview-popup" id="preview_doc_popup" style="display: none">
    <div class="popupcontrols" style="width: 100%; height: 5%;">
        <span id="popupdocclose" class="ic-icon ic-close"></span>
    </div>
    <div class="popupcontent" style="width: 100%; height: 95%;">
        <table style="width: 100%; height: 100%">
            <tr style="height: 5%">
                <td style="text-align: center;">
                    <input type="button" id="btnPrevious" runat="server" value="PREVIOUS" onclick="ShowPreviousDoc();" style="margin-bottom: 10px;" class="btn green btnNextPreview" title="PREVIOUS" />
                    <div style="display: inline-flex; margin-bottom: 20px;">
                        <asp:Label ID="editableFile" runat="server" Style="font-size: 20px; padding-top: 10px;">Filename:</asp:Label>
                        <asp:Label ID="lblFileName" runat="server" Style="font-size: 15px; margin: 14px; margin-bottom: 0;"></asp:Label>
                        <asp:TextBox ID="fileNameFld" CssClass="fileNameField" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:Button ID="btnUpdate" CssClass="quick-find-btn btnEdit" OnClientClick="return makeEditable();" Style="display: inline-block" runat="server" Text="Rename" />
                        <asp:Button ID="btnSave" OnClientClick="return CheckFileValidation();" CssClass="quick-find-btn btnEdit" OnClick="btnSave_Click" Style="display: none; margin-left: 20px;" runat="server" Text="Save" />
                        <asp:Button ID="btnCancel" CssClass="quick-find-btn btnEdit" OnClientClick="return makeReadOnly();" Style="display: none; margin-left: 10px;" runat="server" Text="Cancel" />
                    </div>
                    <input type="button" id="btnNextClick" runat="server" value="NEXT" onclick="ShowNextDoc();" style="margin-bottom: 10px; float: right" class="btn green btnPrevPreview" title="NEXT" />
                </td>
            </tr>
            <tr>
                <td>
                    <iframe id="reviewDocContent" style="width: 100%; height: 100%;"></iframe>
                </td>
            </tr>
        </table>
    </div>
</div>

<div class="content-title" style="border: solid 1px #b8b8b8; padding-bottom: 10px;">
    <asp:HiddenField ID="SortExpression1" runat="server" Value=""></asp:HiddenField>
    <asp:HiddenField ID="SortDirectionSearch" runat="server" Value=""></asp:HiddenField>
    <table style="width: 100%">
        <tr>
            <td style="width: 80%; padding: 1%;">
                <asp:TextBox type="text" MaxLength="50" ID="searchKey" class="find-input searchTab" autocomplete="off" AutoCompleteType="Disabled" placeholder="Enter Keyword to search" runat="server" TabIndex="2" />
            </td>
            <td style="width: 11%; padding-top: 25px !important; padding: 1%;">
                <asp:Button ID="btnSearch" CssClass="quick-find-btn btn-small green" OnClientClick="return CheckSearchValidation();" OnClick="btnSearch_Click" Text="Search" runat="server" />
                <asp:Button ID="btnClear" CssClass="quick-find-btn btn-small green" OnClientClick="HideSearchGrid();" Text="Clear" runat="server" ToolTip="Clear" />
            </td>
        </tr>
    </table>
    <div id="searchGrid">
        <asp:UpdatePanel runat="server" ID="UpdatePanel3">
            <ContentTemplate>
                <asp:GridView ID="gridView" runat="server" OnRowDataBound="gridView_RowDataBound" OnSorting="gridView_Sorting"
                    AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable docTable" Style="margin-top: 20px;">
                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                    <AlternatingRowStyle CssClass="odd" />
                    <RowStyle CssClass="even" HorizontalAlign="Center" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemStyle CssClass="alignCenter" />
                            <HeaderTemplate>
                                <asp:Label ID="lblSearched" runat="server" Text="File Name" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label runat="server" ID="serchedName" Text='<%# Eval("Name")%>'></asp:Label>
                                <br />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:BoundField HeaderText="File Name" DataField="Name" SortExpression="Name" />--%>
                        <asp:BoundField HeaderText="Tab" DataField="DividerName" />
                        <%--<asp:BoundField HeaderText="Tab" DataField="TabName" SortExpression="FileName" />--%>
                        <asp:TemplateField>
                            <ItemStyle CssClass="alignCenter" />
                            <HeaderTemplate>
                                <asp:Label ID="lblSearched" runat="server" Text="Records" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label runat="server" ID="serchedData" CssClass="scrollable" Text='<%# Eval("JsonData")%>'></asp:Label>
                                <br />
                                <asp:Label ID="lblAll" CssClass="lnkExpand" runat="server">Show All</asp:Label>
                                <asp:Label ID="Label1" CssClass="lnkCollapse" runat="server">Show Less</asp:Label>
                                <asp:Button runat="server" ID="btnNext" OnClientClick="return navigateNext(this);" CssClass="btnNav btnNextEnabled" />
                                <asp:Button runat="server" ID="btnPrev" OnClientClick="return navigatePrev(this);" CssClass="btnNav btnPrevDisabled" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="Size" HeaderText="Size" ItemStyle-Width="10%" />
                        <asp:TemplateField ShowHeader="true">
                            <HeaderTemplate>
                                <asp:Label ID="lblMoveOrder" runat="server" Text="Preview|Download|Share" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-preview" CausesValidation="False" ToolTip="Preview"
                                    CommandName="" OnClientClick="ShowPreviewForDocument(this, false);return false;"></asp:LinkButton>
                                <asp:HyperLink ID="HyperLink1" runat="server" CausesValidation="false" CssClass="ic-icon ic-download" Target="_blank"
                                    NavigateUrl='<%#string.Format("../PDFDownloader.aspx?ID={0}", Eval("DocumentId"))%>' />
                                <asp:LinkButton ID="lnkShare" runat="server" CssClass="icon-btn ic-icon ic-share" Style="border: none; background-color: white; margin-left: 0px; float: none" ToolTip="Share File" />
                                <asp:HiddenField runat="server" ID="hdnFolderId" Value='<%# Eval("FolderId")%>' />
                                <asp:HiddenField runat="server" ID="hdnDividerId" Value='<%# Eval("DividerId")%>' />
                                <asp:HiddenField runat="server" ID="hdnDocId" Value='<%# Eval("DocumentId")%>' />
                                <asp:HiddenField runat="server" ID="hdnPathName" Value='<%# Eval("Path") + "/" + GetFileName(Eval("DisplayName") + ".pdf")%>' />
                                <asp:HiddenField runat="server" ID="hdnFileName" Value='<%#Eval("DisplayName")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <asp:Label ID="Label3" runat="server" CssClass="Empty" />There is no record for searched keyword.
                    </EmptyDataTemplate>
                </asp:GridView>
                <asp:DropDownList ID="pager" CssClass="pager_drop" onchange="showLoader()" runat="server" OnSelectedIndexChanged="pager_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
<script type="text/javascript">
    var min = 0;
    var max = 0; var i = 0;

    function UpdateDocumentSearch() {
        $('.docTable').find('tr').each(function () {
            $(this).attr('data-index', i);
            i++;
        });

        max = i;
        i = 0;

        var closePopup = document.getElementById("popupdocclose");
        closePopup.onclick = function () {
            var popup = document.getElementById("preview_doc_popup");
            var overlay = document.getElementById("overlay");
            overlay.style.display = 'none';
            popup.style.display = 'none';
            enableScrollbar();
            makeReadOnly();
            $('#reviewDocContent').removeAttr('src');
        };

        $("#" + '<%= searchKey.ClientID%>').keypress(function (event) {
            if (event.keyCode == 13) {
                $("#" + '<%=btnSearch.ClientID %>').click();
                return false;
            }
        });
    }

    function CheckFileValidation() {
        if ($("#<%= fileNameFld.ClientID %>").val() == "" || $("#<%= fileNameFld.ClientID %>").val() == undefined) {
            alert("Please enter valid file name");
            return false;
        }
        var popup = document.getElementById("preview_doc_popup");
        popup.style.display = 'none';
        return true;
    }

    function navigatePrev(event) {
        var id = parseInt($(event).siblings().first().find('label:visible').attr("id"), 10);
        if ($(event).siblings().find('#' + parseInt(id - 1)).length > 0) {
            $(event).siblings().first().find('label:visible').hide();
            $(event).siblings().find('#' + parseInt(id - 1)).delay(500).show();
            if ($(event).siblings().find('#' + parseInt(id - 2)).length == 0) {
                $(event).removeClass("btnPrevEnabled");
                $(event).addClass("btnPrevDisabled");
            }
            else {
                $(event).addClass("btnPrevEnabled");
                $(event).removeClass("btnPrevDisabled");
            }
            if ($(event).siblings().find('#' + parseInt(id)).length > 0) {
                $(event).siblings().parent().find('.btnNextDisabled').addClass("btnNextEnabled");
                $(event).siblings().parent().find('.btnNextEnabled').removeClass("btnNextDisabled");
            }
        }
        return false;
    }

    function navigateNext(event) {
        var id = parseInt($(event).siblings().first().find('label:visible').attr("id"), 10);
        if ($(event).siblings().find('#' + parseInt(id + 1)).length > 0) {
            $(event).siblings().first().find('label:visible').hide();
            $(event).siblings().find('#' + parseInt(id + 1)).delay(500).show();
            if ($(event).siblings().find('#' + parseInt(id + 2)).length == 0) {
                $(event).removeClass("btnNextEnabled");
                $(event).addClass("btnNextDisabled");
            }
            else {
                $(event).addClass("btnNextEnabled");
                $(event).removeClass("btnNextDisabled");
            }
            if ($(event).siblings().find('#' + parseInt(id)).length > 0) {
                $(event).siblings().parent().find('.btnPrevDisabled').addClass("btnPrevEnabled");
                $(event).siblings().parent().find('.btnPrevEnabled').removeClass("btnPrevDisabled");
            }
        }
        return false;
    }

    function CheckSearchValidation() {
        if ($('#<%= searchKey.ClientID %>').val() == "") {
            alert("please enter any keyword !!!");
            return false;
        }
        showLoader();
        return true;
    }

    function ShowSearchGrid() {
        if ($("#searchGrid").css("display") == "none")
            $("#searchGrid").show(500);
    }

    function HideSearchGrid() {
        $("#searchGrid").hide(500);
    }

    function ShowPreviewForDocument(obj, isContinue) {
        $('.docTable tr').removeClass('activetr');
        $(obj).closest('tr').addClass('activetr');
        if (!isContinue) {
            $('#<%= hdnFileNameOnly.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnFileName]").val());
            $('#<%= hdnSelectedPath.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnPathName]").val());
            $('#<%= hdnFileId.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnDocId]").val());
            $('#<%= hdnFolderId.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnFolderId]").val());
            $('#<%= hdnDividerId.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnDividerId]").val());
        }
        $('#<%= hdnObjId.ClientID%>').val(obj.id);
        $("#<%= lblFileName.ClientID %>").html($('#<%= hdnFileNameOnly.ClientID%>').val());

        $("#overlay").css("z-index", "9999");
        $("#" + '<%= hdnFileId.ClientID %>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnDocId]").val());
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//"
                + window.location.hostname
                + (window.location.port ? ':' + window.location.port : '');
        }
        src = './Preview.aspx?data=' + window.location.origin + "/" + $('#<%= hdnSelectedPath.ClientID%>').val() + "?v=" + "<%= DateTime.Now.Ticks%>";
        var popup = document.getElementById("preview_doc_popup");
        var overlay = document.getElementById("overlay");
        $('#reviewDocContent').attr('src', src);
        overlay.style.display = 'block';
        popup.style.display = 'block';
        $('#preview_doc_popup').focus();
        $('#preview_doc_popup').css("max-width", "1300px");
        var ci = $(obj).closest('tr').attr('data-index');
        if (ci == max - 1) {
            $(".btnPrevPreview").hide();
        }
        else {
            $(".btnPrevPreview").show();
        }
        if (ci == min + 1) {
            $(".btnNextPreview").hide();
        }
        else {
            $(".btnNextPreview").show();

        }
        return false;
    }

    function makeEditable() {
        $("#<%= lblFileName.ClientID %>").hide();
        $("#<%= btnUpdate.ClientID %>").hide();
        $("#<%= fileNameFld.ClientID %>").show();
        $("#<%= btnSave.ClientID %>").show();
        $("#<%= btnCancel.ClientID %>").show();
        $("#<%= fileNameFld.ClientID %>").val($("#<%= hdnFileNameOnly.ClientID %>").val());
        return false;
    }

    function makeReadOnly() {
        $("#<%= lblFileName.ClientID %>").show();
        $("#<%= btnUpdate.ClientID %>").show();
        $("#<%= fileNameFld.ClientID %>").hide();
        $("#<%= btnSave.ClientID %>").hide();
        $("#<%= btnCancel.ClientID %>").hide();
        return false;
    }

    function ShowPreviousDoc() {
        makeReadOnly();
        $("#overlay").css("z-index", "100002");
        var ci = $('.docTable').find('tr.activetr').attr('data-index');
        ci = parseInt(ci);
        var ni = 0;
        if (ci < max & ci > min + 1) {
            ni = ci - 1;
        }
        else {
            ni = max - 1;
        }

        $('.docTable').find('tr').each(function () {
            if ($(this).attr('data-index') == ni) {
                $(this).find('a.ic-preview').click();
            }
        })
    }

    function ShowNextDoc() {
        makeReadOnly();
        $("#overlay").css("z-index", "100002");
        var ci = $('.docTable').find('tr.activetr').attr('data-index');
        ci = parseInt(ci);
        var ni = 0;
        if (ci < max - 1 & ci > min) {
            ni = ci + 1;
        }
        else {
            ni = min + 1;
        }

        $('.docTable').find('tr').each(function () {
            if ($(this).attr('data-index') == ni) {
                $(this).find('a.ic-preview').click();
            }
        })
    }
</script>
