﻿using Elasticsearch.Net;
using Nest;
using Shinetech.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Office_UserControls_ucDocSearch : System.Web.UI.UserControl
{
    protected static string hostString = ConfigurationManager.AppSettings.Get("ElasticHostString");
    protected static string elasticIndexName = ConfigurationManager.AppSettings.Get("ElasticDocIndices");
    protected static Uri[] hosts = hostString.Split(',').Select(x => new Uri(x)).ToArray();
    protected static StaticConnectionPool connectionPool = new StaticConnectionPool(hosts);
    protected static ConnectionSettings settings = new ConnectionSettings(connectionPool)
                                                                         .DefaultMappingFor<DocumentIndex>(i => i
                                                                         .IndexName(elasticIndexName)
                                                                         .TypeName("pdf"))
                                                                         .DisableDirectStreaming();
    protected static ElasticClient highClient = new ElasticClient(settings);
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
    protected NLogLogger _logger = new NLogLogger();
    protected string FolderId = string.Empty;
    protected string DividerId = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        FolderId = Session["EffID_FFB"] as string;
        DividerId = Session["EffID_FFB"] as string;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
            int folderId = Convert.ToInt32(hdnFolderId.Value);
            int dividerId = Convert.ToInt32(hdnDividerId.Value);
            int documentId = Convert.ToInt32(hdnFileId.Value);
            int newDocumentId = 0;
            int documentOrderId = 0;
            DataSet page = new DataSet();

            string newFileName = fileNameFld.Text;
            string oldFilePath = hdnSelectedPath.Value;

            newFileName = Common_Tatva.SubStringFilename(newFileName);
            newFileName = HttpUtility.UrlPathEncode(newFileName).Replace("%", "$").Replace("&", "").Replace("#", "");

            string oldFolderPath = oldFilePath.Substring(0, oldFilePath.LastIndexOf('/') + 1);
            oldFilePath = oldFolderPath + HttpUtility.UrlPathEncode(hdnFileNameOnly.Value).Replace("%", "$").Replace("&", "").Replace("#", "");
            oldFolderPath = oldFolderPath.Substring(0, oldFolderPath.LastIndexOf('/'));

            var newFilePath = Common_Tatva.GetNewPathOnly(oldFolderPath, dividerId, newFileName);
            if (newFilePath == oldFilePath)
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel3, typeof(UpdatePanel), "alertok",
                                                                "alert(\"" + "Document has been edited successfully." + "\");ShowPreviewForDocument(" + hdnObjId.Value + ", true);ShowSearchGrid();", true);
                return;
            }

            if (newFilePath.IndexOf(".pdf") == -1)
                newFilePath = newFilePath + ".pdf";

            if (oldFilePath.IndexOf(".pdf") == -1)
                oldFilePath = oldFilePath + ".pdf";

            newFileName = newFileName.Replace("$20", " ");

            DataTable dtDoc = General_Class.GetDocumentFromDocumentID(documentId);
            if (dtDoc != null && dtDoc.Rows.Count > 0)
            {
                int documentOrder = Convert.ToInt32(dtDoc.Rows[0]["DocumentOrder"]);
                string className = Convert.ToString(dtDoc.Rows[0]["Class"]);
                // move document to new folder
                int moveRename = Common_Tatva.MoveFilesToAnotherFolder(newFilePath, oldFilePath);

                //insert file in database and folder
                if (moveRename == Convert.ToInt32(Enum_Tatva.FileMove.FileMoveSuccess))
                {
                    string documentIDOrder = Common_Tatva.InsertDocument(dividerId, newFilePath, documentOrder, newFileName, className, Membership.GetUser().ProviderUserKey.ToString());
                    if (documentIDOrder.Contains("#"))
                    {
                        newDocumentId = Convert.ToInt32(documentIDOrder.Split('#')[0]);
                        documentOrderId = Convert.ToInt32(documentIDOrder.Split('#')[1]);
                    }
                    else
                        newDocumentId = Convert.ToInt32(documentIDOrder);
                }

                #region Elastic Update

                //if (newDocumentId > 0)
                //    Common_Tatva.UpdateDocument(Convert.ToInt32(documentId), newDocumentId, dividerId, newFileName, newFilePath.Substring(0, newFilePath.LastIndexOf("/")));

                #endregion

                // delete document and pages from database // No need to pass switch user
                if (newDocumentId > 0)
                    page = FlashViewer.DeleteDocumentAndPages(documentId.ToString(), documentOrderId, Membership.GetUser().ProviderUserKey.ToString());

                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(folderId, Convert.ToInt32(dividerId), documentId, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Rename.GetHashCode(), 0, 0);
                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(folderId, Convert.ToInt32(dividerId), Convert.ToInt32(newDocumentId), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Create.GetHashCode(), 0, 0);
                string tempPath = newFilePath.Split(new[] { "/" + Path.GetFileName(newFilePath) }, StringSplitOptions.RemoveEmptyEntries)[0];
                tempPath = tempPath.Replace("/", @"\");


                if (newDocumentId > 0)
                {
                    Thread.Sleep(1000);
                    GetSearchedData(Convert.ToInt32(pager.SelectedItem.Value));
                    hdnFileNameOnly.Value = newFileName;
                    lblFileName.Text = newFileName;
                    hdnSelectedPath.Value = newFilePath;
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel3, typeof(UpdatePanel), "alertok",
                                                                "alert(\"" + "Document has been edited successfully." + "\");ShowPreviewForDocument(" + hdnObjId.Value + ", true);ShowSearchGrid();", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel3, typeof(UpdatePanel), "alertok",
                                                                   "alert(\"" + "Some error occurred. Please try again later !!" + "\");ShowPreviewForDocument(" + hdnObjId.Value + ", true);ShowSearchGrid();", true);
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel3, typeof(UpdatePanel), "alertok",
                                                                   "alert(\"" + "Some error occurred. Please try again later !!" + "\");ShowPreviewForDocument(" + hdnObjId.Value + ", true);ShowSearchGrid();", true);
            }
        }
        catch (Exception ex)
        {
            _logger.Info(ex);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel3, typeof(UpdatePanel), "alertok",
                                                                   "alert(\"" + "Some error occurred. Please try again later !!" + "\");ShowPreviewForDocument(" + hdnObjId.Value + ", true);ShowSearchGrid();", true);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            int totalHits = GetSearchedData();
            if (totalHits > 0)
            {
                pager.Items.Clear();
                int pageCount = ((totalHits % 10) == 0) ? ((totalHits / 10)) : ((totalHits / 10) + 1);
                for (int i = 0; i < pageCount; i++)
                {
                    pager.Items.Add(new ListItem((i + 1).ToString(), (i * 10).ToString()));
                }
                pager.Visible = true;
            }
            else
            {
                pager.Items.Clear();
                pager.Visible = false;
            }
            ScriptManager.RegisterClientScriptBlock(UpdatePanel3, typeof(UpdatePanel), "Alert", "ShowSearchGrid();", true);
        }
        catch (Exception ex)
        {
            _logger.Info(ex);
        }
    }

    /// <summary>
    /// Gets the searched data.
    /// </summary>
    /// <param name="page">The page.</param>
    /// <returns></returns>
    public int GetSearchedData(int page = 0)
    {
        var searchResponse = highClient
            .Search<DocumentIndex>(s => s
                .From(page)
                .Size(10)
                .Query(q => q
                    .Bool(b => b
                        .Must(mu => mu
                            .MultiMatch(mm => mm
                                .Fields(fs => fs
                                    .Field(f => f.FolderId))
                                .Query(FolderId)) &&
                            q.MultiMatch(mm => mm
                                .Fields(fs => fs
                                    .Field(f => f.DividerId))
                                .Query(FolderId)) &&
                            q.MultiMatch(mm => mm
                                .Fields(fs => fs
                                    .Field(f => f.IsDeleted))
                                .Query("false"))
                        )
                    )
                )
                .Highlight(h => h
                    .PreTags("<span class='yellow'>")
                    .PostTags("</span>")
                    .Encoder(HighlighterEncoder.Html)
                    .Fields(
                        fs => fs
                            .Field("jsonData.ngram")
                            .Type(HighlighterType.Plain)
                            .FragmentSize(50)
                            .ForceSource(true)
                            .Fragmenter(HighlighterFragmenter.Span)
                            .NumberOfFragments(150)
                            .NoMatchSize(50)
                            .PhraseLimit(100)
                            .BoundaryMaxScan(50)
                            .HighlightQuery(hl => hl
                                .Fuzzy(m => m
                                    .Name("ngram")
                                    .Boost(1.1)
                                    .Field("jsonData.ngram")
                                    .Value(searchKey.Text.ToLower())
                                    .Rewrite(MultiTermQueryRewrite.ConstantScore)
                                    .Transpositions(true)
                                 )
                            ),
                        fs => fs
                            .Field("name.ngram")
                            .Type(HighlighterType.Plain)
                            .FragmentSize(50)
                            .ForceSource(true)
                            .Fragmenter(HighlighterFragmenter.Span)
                            .NumberOfFragments(150)
                            .NoMatchSize(50)
                            .PhraseLimit(100)
                            .BoundaryMaxScan(50)
                            .HighlightQuery(hl => hl
                                .Fuzzy(m => m
                                    .Name("ngram.Name")
                                    .Boost(1.1)
                                    .Field("name.ngram")
                                    .Value(searchKey.Text.ToLower())
                                    .Rewrite(MultiTermQueryRewrite.ConstantScore)
                                    .Transpositions(true)
                                 )
                            )
                    )
                )
                .PostFilter(q => q
                    .MultiMatch(mm => mm
                        .Fields(fs => fs
                            .Field("jsonData.ngram")
                            .Field("name.ngram")
                        )
                        .Query(searchKey.Text.ToLower())
                        .Analyzer("standard")
                        .Boost(1.1)
                        .Slop(2)
                        .Fuzziness(Fuzziness.Auto)
                        .PrefixLength(2)
                        .MaxExpansions(2)
                        .Operator(Operator.Or)
                        .MinimumShouldMatch(1)
                        .FuzzyRewrite(MultiTermQueryRewrite.ConstantScore)
                        .TieBreaker(1.1)
                        .CutoffFrequency(0.001)
                        .Lenient()
                        .ZeroTermsQuery(ZeroTermsQuery.All)
                        .Name("ngram")
                        .AutoGenerateSynonymsPhraseQuery(false)
                    )
                )
            );

        DataTable dtNew = new DataTable();
        DataTable dtFinal = new DataTable();
        PropertyInfo[] props = typeof(DocumentIndex).GetProperties(BindingFlags.Public | BindingFlags.Instance);

        foreach (var prop in props)
        {
            dtNew.Columns.Add(prop.Name, prop.PropertyType);
            dtFinal.Columns.Add(prop.Name, prop.PropertyType);
        }

        if (searchResponse.Documents.Count > 0)
        {
            for (int i = 0; i < searchResponse.Documents.Count; i++)
            {
                var dr = dtNew.NewRow();
                dr["FolderId"] = searchResponse.Documents.ToList()[i].FolderId;
                dr["Name"] = searchResponse.Documents.ToList()[i].Name;
                dr["DocumentId"] = searchResponse.Documents.ToList()[i].DocumentId;
                dr["DividerId"] = searchResponse.Documents.ToList()[i].DividerId;
                dr["Path"] = searchResponse.Documents.ToList()[i].Path;
                dr["Size"] = searchResponse.Documents.ToList()[i].Size;
                dr["DisplayName"] = searchResponse.Documents.ToList()[i].Name;
                if (searchResponse.Hits.Count > 0)
                {
                    if (searchResponse.Hits.ToList()[i].MatchedQueries.Count <= 0)
                        continue;
                    foreach (var responses in searchResponse.Hits.ToList()[i].Highlights.ToList())
                    {
                        {
                            foreach (var highLights in responses.Value.Highlights)
                            {
                                if (responses.Key == "jsonData.ngram")
                                    dr["JsonData"] += (highLights + "<span>EdSplit</span>");
                                else dr["Name"] = highLights;
                            }
                        }
                    }
                }
                dtNew.Rows.Add(dr);
            }
            DataTable dtTabs = this.DividerSource;
            var results = from records in dtNew.AsEnumerable()
                          join tabs in dtTabs.AsEnumerable() on Convert.ToInt32(records["DividerId"]) equals Convert.ToInt32(tabs["DividerID"])
                          select new DocumentIndex
                          {
                              DocumentId = Convert.ToInt32(records["DocumentId"]),
                              FolderId = Convert.ToInt32(records["FolderId"]),
                              Name = Convert.ToString(records["Name"]),
                              DividerId = Convert.ToInt32(records["DividerId"]),
                              Path = Convert.ToString(records["Path"]).Replace("\\", "/"),
                              JsonData = Convert.ToString(records["JsonData"]),
                              Size = (float)records["Size"],
                              DisplayName = Convert.ToString(records["DisplayName"]),
                              DividerName = Convert.ToString(tabs["Name"]),
                          };
            //dtNew.Clear();
            foreach (var item in results.ToList())
            {
                var values = new object[props.Length];
                for (var i = 0; i < props.Length; i++)
                {
                    values[i] = props[i].GetValue(item, null);
                }
                dtFinal.Rows.Add(values);
            }
        }
        this.DataSource = dtFinal;
        gridView.DataSource = this.DataSource;
        gridView.DataBind();
        if (searchResponse.HitsMetadata != null)
            return Convert.ToInt32(searchResponse.HitsMetadata.Total);
        else return 0;
    }

    public DataTable DividerSource
    {
        get
        {
            return this.Session["DividerSource_FolderBox"] as DataTable;
        }
        set
        {
            this.Session["DividerSource_FolderBox"] = value;
        }
    }

    public string Sort_Expression
    {
        get
        {
            return SortExpression1.Value as string;
        }
        set
        {
            SortExpression1.Value = value;
        }
    }

    private SortOrder Sort_Direction
    {
        get
        {
            if (SortDirectionSearch == null)
            {
                return SortOrder.Descending;
            }
            if (SortDirectionSearch != null && string.IsNullOrEmpty(SortDirectionSearch.Value))
            {
                SortDirectionSearch.Value = Enum.GetName(typeof(SortOrder), SortOrder.Descending);
            }

            return (SortOrder)Enum.Parse(typeof(SortOrder), this.SortDirectionSearch.Value);

        }
        set
        {
            SortDirectionSearch.Value = Enum.GetName(typeof(SortOrder), value);
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource_Document"] as DataTable;
        }
        set
        {
            this.Session["DataSource_Document"] = value;
        }
    }

    protected void pager_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetSearchedData(Convert.ToInt32(pager.SelectedItem.Value));
    }

    protected void gridView_Sorting(object sender, GridViewSortEventArgs e)
    {
        Sort_Expression = e.SortExpression;
        if (Sort_Direction == SortOrder.Ascending)
            Sort_Direction = SortOrder.Descending;
        else
            Sort_Direction = SortOrder.Ascending;
        GetSearchedData(Convert.ToInt32(pager.SelectedItem.Value));
    }

    /// <summary>
    /// Gets the name of the file.
    /// </summary>
    /// <param name="fileName">Name of the file.</param>
    /// <returns></returns>
    public string GetFileName(string fileName)
    {
        string encodeName = HttpUtility.UrlPathEncode(fileName);
        return encodeName.Replace("%", "$").Replace("?", "$c3$b1");
    }

    protected void gridView_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                int DocumentID = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "DocumentID"));
                string Name = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "DisplayName"));
                string PathName = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "Path"));

                string oldPath = PathName + Path.DirectorySeparatorChar + GetFileName(Name) + ".pdf";

                oldPath = oldPath.Replace(@"\", "/");

                Label lbl = e.Row.FindControl("serchedData") as Label;
                Label btnAll = e.Row.FindControl("lblAll") as Label;
                List<string> results = new List<string>();
                if (lbl != null)
                {
                    results = lbl.Text.Split(new string[] { "<span>EdSplit</span>" }, StringSplitOptions.None).ToList();
                    lbl.Text = results[0];
                }
                var finalData = string.Empty;
                for (int i = 0; i < results.Count - 1; i++)
                {
                    if (i == 0)
                        finalData += "<label class='searchedLbl' id='" + i + "' style='display:inline-block'>" + results[i] + "<br /></label>";
                    else
                        finalData += "<label class='searchedLbl' id='" + i + "' style='display:none'>" + results[i] + "<br /></label>";
                }
                if (results.Count < 3)
                {
                    Button btnNext = e.Row.FindControl("btnNext") as Button;
                    if (btnNext != null)
                        btnNext.Visible = false;
                    Button btnPrev = e.Row.FindControl("btnPrev") as Button;
                    if (btnPrev != null)
                        btnPrev.Visible = false;
                    if (btnAll != null)
                        btnAll.Visible = false;
                }
                lbl.Text = finalData;
                oldPath = oldPath.Replace(@"\", "/");

                LinkButton lnkShare = (e.Row.FindControl("lnkShare") as LinkButton);
                if (lnkShare != null)
                {
                    lnkShare.Attributes.Add("onclick", "return OnShowShare(" + DocumentID + ", '" + oldPath + "')");

                }
            }
            catch (Exception ex)
            {
                //Common_Tatva.WriteElasticLog(string.Empty, hdnFolderId.Value, searchKey.Text, ex);
            }
        }
    }
}