﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucEdForms.ascx.cs" Inherits="Office_UserControls_ucEdForms" %>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%--<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>--%>
<%@ Register Src="~/Office/UserControls/ucWorkArea.ascx" TagPrefix="uc1" TagName="ucWorkArea" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Office/UserControls/ucAddToTab.ascx" TagPrefix="uc1" TagName="ucAddToTab" %>

<link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>css/jquery-ui-1.8.22.custom.css" />
<link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/colorbox.css" rel="stylesheet" />
<link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>css/main.css?v=1" rel="stylesheet" />
<link rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>lib/TreeView/style.min.css" />
<script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/js/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>extras/jquery-ui-1.8.22.custom.min.js"></script>

<style type="text/css">
    .singleAddToTab {
        cursor: pointer;
        background-image: url(../images/AddToTabWorkArea.png);
        margin-left: 0px;
        float: none;
        background-color: white;
        border: 0px;
        margin-top: -4px;
    }

    .ajax__tab_body {
        height: auto !important;
    }

    .white_content {
        display: none;
        position: fixed;
        top: 20%;
        left: 20%;
        width: 60%;
        height: 50%;
        padding: 16px;
        background-color: white;
        z-index: 999999999;
        overflow: auto;
    }

    #overlay {
        display: none;
        position: absolute;
        top: 0;
        bottom: 0;
        background: #999;
        width: 100%;
        height: 100%;
        opacity: 0.8;
        z-index: 99999;
    }

    .preview {
        position: relative;
        left: -10px;
    }

    td span {
        -ms-word-break: break-all;
        word-break: break-all;
        word-break: break-word;
        -webkit-hyphens: auto;
        -moz-hyphens: auto;
        hyphens: auto;
        max-width: 300px;
        white-space: normal !important;
    }

    td {
        max-width: 200px;
    }

    .actionMinWidth {
        min-width: 50px;
    }

    .chkpending, .chkinbox {
        margin-left: 10px;
    }

    .chkpendingAll, .chkinboxAll {
        margin-left: 10px;
    }

    .boxtitle {
        margin-top: 20px !important;
    }

    .file {
        background: url(../lib/TreeView/file_sprite.png) 0 0 no-repeat !important;
        width: 18px !important;
        height: 18px !important;
        margin-top: 4px !important;
        margin-left: 2px !important;
        margin-right: 5px !important;
    }


    /*16-09-2016*/
    .create-btn {
        background-image: url(../images/create-folder.png);
        cursor: pointer;
    }


    .btnAddtoTab, .btnAddtoWorkArea {
        padding: 8px 10px 8px 35px;
        font-size: 15px;
        padding-right: 10px;
        width: auto;
        height: auto;
        line-height: normal;
        margin-right: 5px;
        background-image: url(../images/plus-icon.png);
        background-position: 7px center;
        background-repeat: no-repeat;
    }

    .btnShare {
        padding: 6px 0px 6px 35px;
        width: auto;
        height: auto;
        line-height: normal;
        background-image: url(../images/share-icon.png);
        background-position: 7px center;
        background-repeat: no-repeat;
    }


    .addtotab-workarea {
        cursor: pointer;
        background-image: url(../images/AddToTabWorkArea.png);
    }

    .rename-btn {
        background-image: url(../images/rename.png);
        cursor: pointer;
    }

    .delete-btn {
        background-image: url(../images/delete.png);
        cursor: pointer;
    }

    .cancel-btn {
        background-image: url(../images/cancel-btn.png);
        cursor: pointer;
    }

    .send-btn {
        background-image: url(../images/send.png);
        cursor: pointer;
    }

    .preview-btn {
        cursor: pointer;
    }

    .register-container .form-containt .content-title {
        margin-bottom: 5px;
        border: none !important;
        padding: 0px !important;
    }

    .popupclose {
        float: right;
        padding: 10px;
        cursor: pointer;
    }

    .popupcontent {
        padding: 10px;
    }

    .form-containt .content-box fieldset {
        font-size: initial;
    }

    .ajax__fileupload_dropzone {
        font-size: 15px;
        line-height: 120px !important;
        height: 120px !important;
    }

    .lnkbtnPreview {
        margin-left: -10px;
    }

    .downloadFile {
        margin-left: -10px;
        cursor: pointer;
    }

    #floatingCirclesG {
        top: 50%;
        left: 50%;
        position: fixed;
    }

    .listing-contant .quick-find {
        background: #fff;
        padding: 0px 0px 0px 0px;
        width: 235px;
        margin-top: 0px;
    }

    .quick-find .find-inputbox .quick-find-btn {
        top: 3px;
        height: 30px;
        width: 30px;
    }

    .quick-find .find-inputbox .find-input {
        height: 35px;
        padding-right: 5px;
    }

    .shareDocument {
        cursor: pointer;
    }

    .ic-save, .ic-share {
        cursor: pointer;
    }

    .selectedTreeNodeLabel {
        display: inline-block;
        font-size: 14px;
        margin-top: 25px;
    }

    .listing-datatable {
        border-bottom: 2px !important;
        border-width: 1px !important;
    }

    .register-container .form-containt .content-box {
        border: none;
        padding: 0px 0px 0px 0px;
    }
</style>

<asp:HiddenField ID="hdnPendingIDs" runat="server" />
<asp:HiddenField ID="hdnSelectedFolderID" runat="server" />
<asp:HiddenField ID="hdnSelectedDividerID" runat="server" />
<asp:HiddenField ID="hdnSelectedFolderText" runat="server" />
<asp:HiddenField ID="hdnPreviewShareID" runat="server" />

<div id="overlay">
    <div id="floatingCirclesG">
        <div class="f_circleG" id="frotateG_01"></div>
        <div class="f_circleG" id="frotateG_02"></div>
        <div class="f_circleG" id="frotateG_03"></div>
        <div class="f_circleG" id="frotateG_04"></div>
        <div class="f_circleG" id="frotateG_05"></div>
        <div class="f_circleG" id="frotateG_06"></div>
        <div class="f_circleG" id="frotateG_07"></div>
        <div class="f_circleG" id="frotateG_08"></div>
    </div>
</div>

<div class="popup-mainbox preview-popup" id="preview_popup" style="display: none">
    <div class="popupcontrols" style="width: 100%; height: 6%;">
        <table style="width: 100%; height: 100%">
            <tr style="height: 6%">
                <td>
                    <input type="button" id="btnSpltBtn" runat="server" value="Tab" onclick="ShowAddToTabPopup();" style="margin: 10px;" class="btn green btnAddtoTab" title="Add To Tab" />
                    <span id="popupclose" class="ic-icon ic-close"></span>
                </td>
            </tr>
        </table>
    </div>
    <div class="popupcontent" style="width: 100%; height: 94%;">
        <iframe id="reviewContent" style="width: 100%; height: 100%;"></iframe>
    </div>
</div>

<div class="popup-mainbox preview-popup" id="AddToTab_popup" style="display: none; height: 460px; width: 395px; max-width: 395px; max-height: 460px; margin-top: 20px;">
    <div class="popupcontrols popup-head" style="width: 100%; height: 60px;">
        Select Folder
        <span id="Span2" class="ic-icon ic-close popupclose"></span>
    </div>
    <div class="popupcontent" style="width: 100%; height: 95%; background: #eaeaea; border-top: 2px solid darkgray;">
        <div class="popup-scroller" style="height: 75%; background: #eaeaea;">
            <p style="margin: 20px">
                <uc1:ucAddToTab ID="ucAddToTab" runat="server" />
            </p>

        </div>
        <div class="popup-btn" style="background: #eaeaea;">
            <%--<input  runat="server"  type="button" value="Save"  />--%>
            <asp:Button ID="btnSelectAddToTab" Text="Save" runat="server" CssClass="btn green" OnClick="btnSelectAddToTab_Click" Style="margin-right: 5px;" />
            <input id="btnCancelAddToTab" type="button" value="Cancel" class="btn green" />
        </div>
    </div>
</div>

<div class="page-container register-container" style="padding-top: 0; padding-bottom: 0px;">
    <div class="inner-wrapper">
        <div class="form-containt listing-contant">

            <div class="left-content" style="width: 100%">

                <asp:Panel ID="panelMain" runat="server" Visible="true">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="clear: both"></div>
                            <%--Inbox--%>
                            <div class="content-title boxtitle" style="display: inline; margin-bottom: 0px; font-size: 20px; vertical-align: middle; font-weight: normal;">Pending EdForms</div>
                            <div style="float: right; margin-bottom: 15px">
                                <asp:Button ID="btnAddtoTab" Text="Tab" CssClass="btn green btnAddtoTab" runat="server" ToolTip="Add To Tab"></asp:Button>
                                <cc1:WebPager ID="WebPager1" runat="server" Style="float: right" OnPageIndexChanged="WebPager1_PageIndexChanged" OnPageSizeChanged="WebPager1_PageSizeChanged"
                                    PageSize="5" CssClass="paging_position ucEdForms" PagerStyle="NumericPages" ControlToPaginate="EdFormsFolderGrid"></cc1:WebPager>
                                <asp:HiddenField ID="SortDirection1" runat="server" />
                            </div>
                            <div style="width: 100%; border-collapse: collapse; border-width: 0px 0px 1px; padding-top: 22px;"></div>
                            <div class="content-box listing-view">
                                <fieldset id="inboxFieldset">
                                    <div class='clearfix'></div>
                                    <asp:HiddenField ID="hdnEdFormsShareId" runat="server" />
                                    <asp:GridView ID="EdFormsFolderGrid" runat="server" AllowSorting="True"
                                        AutoGenerateColumns="false" BorderWidth="2" OnRowDataBound="EdFormsFolderGrid_RowDataBound"
                                        CssClass="datatable listing-datatable edFormsFolderGrid" Width="100%" OnSorting="EdFormsFolderGrid_Sorting">
                                        <PagerSettings Visible="False" />
                                        <AlternatingRowStyle CssClass="odd" />
                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="3%"
                                                ItemStyle-CssClass="table_tekst_edit">
                                                <HeaderTemplate>
                                                    <asp:CheckBox Text="" ID="chkSelectAll" runat="server" onclick="checkAll(this);" CssClass="chkpendingAll chkinboxAll" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList chkinbox" onclick="Check_Click(this);" />
                                                    <asp:HiddenField Value='<%#Eval("EdFormsUserShareId")%>' runat="server" ID="hdnEdFormsShareId" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="25%" SortExpression="FileName">
                                                <HeaderTemplate>
                                                    <asp:LinkButton ID="lblfileNameHdr" runat="server" Text="Name" CommandName="Sort" CommandArgument="FileName" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblfileName" runat="server" Text='<%#Eval("FileName")%>' name="lblfileName" CssClass="lblfileName" />
                                                    <asp:TextBox runat="server" MaxLength="50" Text='<%#Eval("FileName")%>'
                                                        ID="txtFileName" Style="display: none; height: 30px;" name="txtFileName" CssClass="txtFileName" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="SharedDate" HeaderText="Shared Date" DataFormatString="{0:M-dd-yyyy HH:mm:ss}" ItemStyle-Width="15%"
                                                SortExpression="SharedDate" />
                                            <asp:BoundField DataField="ReceivedDate" HeaderText="Received Date" DataFormatString="{0:M-dd-yyyy HH:mm:ss}" ItemStyle-Width="15%"
                                                SortExpression="ReceivedDate" />
                                            <asp:BoundField DataField="ToEmail" SortExpression="ToEmail" HeaderText="Submitted By" ItemStyle-Width="15%" />

                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="20%"
                                                ItemStyle-CssClass="table_tekst_edit actionMinWidth">
                                                <HeaderTemplate>
                                                    <asp:Label ID="LabelAction" runat="server" Text="Action" />
                                                </HeaderTemplate>
                                                <ItemTemplate>

                                                    <%--Edit--%>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text=""
                                                        CssClass="ic-icon ic-edit editInboxfile" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>

                                                    <asp:LinkButton ID="lbkUpdate" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                        ToolTip="Update" OnClientClick='<%# "return OnSaveRenameShare(this, \"" + Eval("EdFormsUserShareId") + "\",\"" + Eval("FileName") + "\")"%>' CssClass="ic-icon ic-save saveInboxfile" Style="display: none" name="lbkUpdate"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkCancel" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                        OnClientClick="cancleInboxFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancelInboxfile" Style="display: none" name="lnkCancel"></asp:LinkButton>
                                                    <%--Edit End--%>

                                                    <%--Delete--%>
                                                    <asp:LinkButton ID="lnkBtnDelete" runat="server" CssClass="ic-icon ic-delete deleteInboxfile" OnCommand="lnkBtnDelete_Command" CommandArgument='<%#string.Format("{0}", Eval("EdFormsUserShareId"))%>'></asp:LinkButton>


                                                    <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="lnkBtnDelete"
                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                    </ajaxToolkit:ModalPopupExtender>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="cbeDelFile" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                        ConfirmText="" TargetControlID="lnkBtnDelete">
                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                    <%--Delete End--%>

                                                    <asp:LinkButton ID="lnkbtnPreview" runat="server" CssClass="ic-icon ic-preview lnkbtnPreview" CausesValidation="False"
                                                        CommandName="" OnClientClick='<%# "return ShowPreviewForEdFormsShare(\"" + Eval("EdFormsUserShareId") + "\",\"" + Eval("FileName") + "\")"%>'></asp:LinkButton>
                                                    <asp:HyperLink ID="lnkBtnDownload" runat="server" CausesValidation="false" CssClass="ic-icon ic-download downloadFile lnkBtnDownload" Target="_blank" />
                                                    <asp:Button ID="btnAddToTab" runat="server" CssClass="icon-btn singleAddToTab" ToolTip="Add to tab" OnClientClick='<%# "return ShowPopForAddToTab(\"" + Eval("EdFormsUserShareId") + "\")"%>' />
                                                    <%--Notify--%>
                                                    <asp:LinkButton ID="lnkBtnNotify" ToolTip="Resend Form" runat="server" CssClass="ic-icon ic-notify" OnCommand="lnkBtnNotify_Command" CommandArgument='<%#string.Format("{0}{1}{2}", Eval("EdFormsUserShareId"), ",", Eval("ToEmail"))%>' CommandName='<%#string.Format("{0}", Eval("FileName"))%>'></asp:LinkButton>
                                                    <ajaxToolkit:ModalPopupExtender ID="lnkNotify_ModalPopupExtender" runat="server"
                                                        CancelControlID="ButtonNotifyCancel" OkControlID="ButtonNotifyOkay" TargetControlID="lnkBtnNotify"
                                                        PopupControlID="DivNotifyConfirmation" BackgroundCssClass="ModalPopupBG">
                                                    </ajaxToolkit:ModalPopupExtender>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="cbeNotify" runat="server" DisplayModalPopupID="lnkNotify_ModalPopupExtender"
                                                        ConfirmText="" TargetControlID="lnkBtnNotify">
                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                    <%--Notify--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            No Records found
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                    <br />

                                </fieldset>
                            </div>

                            <%--End Inbox--%>

                            <%--Delete File--%>
                            <asp:Panel class="popup-mainbox" ID="DivDeleteConfirmation" Style="display: none"
                                runat="server">
                                <div>
                                    <div class="popup-head" id="PopupHeader">
                                        Delete File
                                    </div>
                                    <div class="popup-scroller">
                                        <p style="margin: 20px">
                                            Caution! Are you sure you want to delete this file?
                                        </p>
                                        <p style="margin: 20px">
                                            Confirm Password:
                                        <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                                        </p>
                                    </div>
                                    <div class="popup-btn">
                                        <input id="ButtonDeleleOkay" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />
                                        <input id="ButtonDeleteCancel" type="button" value="No" class="btn green" />
                                    </div>
                                </div>
                            </asp:Panel>
                            <%--end Delete--%>

                            <%--Notify--%>
                            <asp:Panel class="popup-mainbox" ID="DivNotifyConfirmation" Style="display: none"
                                runat="server">
                                <div>
                                    <div class="popup-head" id="PopupHeaderNotify">
                                        Resend Form
                                    </div>
                                    <div class="popup-scroller">
                                        <label style="margin: 20px; font-size: 15px;">
                                            Name :
                                        </label>
                                        <p style="margin-top: 5px">
                                            <asp:TextBox ID="textName" TextMode="SingleLine" runat="server" Width="100%" />
                                        </p>
                                    </div>
                                    <div class="popup-scroller">
                                        <label style="margin: 20px; font-size: 15px;">
                                            Message :
                                        </label>
                                        <p style="margin-top: 5px">
                                            <asp:TextBox ID="textMessage" TextMode="MultiLine" runat="server" Width="100%" />
                                        </p>
                                    </div>
                                    <div class="popup-btn">
                                        <input id="ButtonNotifyOkay" type="button" value="Send" class="btn green" style="margin-right: 15px" />
                                        <input id="ButtonNotifyCancel" onclick="RemoveMessage();" type="button" value="Cancel" class="btn green" />
                                    </div>
                                </div>
                            </asp:Panel>
                            <%--Notify--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </div>
        </div>
    </div>
</div>

<script language="JavaScript" type="text/jscript">
    var ids = "", selectedButton = "", selectedPath = "", selectedChildNode = "", modalOpenedFrom = "";
    var userId = '<%= Sessions.SwitchedRackspaceId%>';

    $(document).ready(function () {
        pageUserLoad();
    });

    function pageUserLoad() {
        if ('<%=EdFormsFolderGrid.Rows.Count%>' == 0) {
            $('#<%=btnAddtoTab.ClientID%>').prop("disabled", true);
            $('#<%=btnAddtoTab.ClientID%>').css("opacity", "0.65");
        }
        else {
            $('#<%=btnAddtoTab.ClientID%>').prop("disabled", false);
            $('#<%=btnAddtoTab.ClientID%>').css("opacity", "1");
        }

        $(".ucEdForms select[name*='PageList']").css("height", "36px");
        $(".ucEdForms select[name*='PageList']").css("margin-top", "-3px");
        if (LoadFolders)
            LoadFolders();

        $('.popupclose').click(function () {
            $('.popup-mainbox').hide();
            $('#overlay').hide();
            enableScrollbar();
        });

        $("#btnCancelAddToTab").click(function () {
            $(this).closest('.popup-mainbox').hide();
            var popup = document.getElementById("preview_popup");
            if (popup.style.display == "none") {
                $('#overlay').hide();
                enableScrollbar();
            }
        });

        $('.editInboxfile').click(function (e) {
            e.preventDefault();
            removeAllInboxEditable();
            showInboxEdit($(this));
        });

        $('.cancelInboxfile').click(function (e) {
            e.preventDefault();
            removeAllInboxEditable();
        });

        $("#ButtonDeleleOkay").live({
            click: function () {
                showLoader();
            }
        });

        $('.shareDocument').click(function (e) {
            e.preventDefault();
        });

        var closePopup = document.getElementById("popupclose");
        closePopup.onclick = function () {
            var popup = document.getElementById("preview_popup");
            var overlay = document.getElementById("overlay");
            overlay.style.display = 'none';
            popup.style.display = 'none';
            //popup.style.maxWidth = '900px;'
            enableScrollbar();
            $('#reviewContent').removeAttr('src');
        };

        $(".lnkbtnPreview").load(function (e) {
            e.preventDefault();
            ShowInboxOpacity($(this));
        });

        $('#<%=btnAddtoTab.ClientID%>').click(function (e) {
            e.preventDefault();
            if (checkInbox()) {
                $('#AddToTab_popup,#overlay').show().focus();
                $('#combobox').trigger('chosen:open');
                e.stopPropagation();
                disableScrollbar();
            }
        });

        $('#<%= btnSelectAddToTab.ClientID%>').click(function (e) {
            var selectedFolder = $("#combobox").chosen().val();
            var selectedDivider = $("#comboboxDivider").chosen().val();
            if (selectedFolder == "0" && selectedDivider == "0") {
                alert("Please select at least one EdFile & one Tab");
                e.preventDefault();
                return false;
            }
            else if (selectedFolder == "0") {
                alert("Please select at least one EdFile");
                e.preventDefault();
                return false;
            }
            else if (selectedDivider == "0") {
                alert("Please select at least one Tab");
                e.preventDefault();
                return false;
            }
            var folderValue = $('.chosen-single').text();
            if (selectedFolder == undefined || selectedFolder == null || selectedFolder.length <= 0) {
                alert('Please select folder to move request files.');
                e.preventDefault();
                return false;
            }
            $('#overlay').css('z-index', '100010');
            $('#<%= hdnSelectedFolderID.ClientID%>').val(selectedFolder);
            $('#<%= hdnSelectedDividerID.ClientID%>').val(selectedDivider);
            $('#<%= hdnSelectedFolderText.ClientID%>').val(folderValue);
            return true;
        });
    }

    function ShowAddToTabPopup() {
        var popup = document.getElementById("preview_popup");
        popup.style.display = 'none';
        $('#AddToTab_popup,#overlay').show().focus();
        $('#combobox').trigger('chosen:open');
        disableScrollbar();
        return false;
    }

    function ShowPopForAddToTab(edFormsShareId) {
        $('#<%= hdnPendingIDs.ClientID%>').val(edFormsShareId); // set hidden field value
        $('#AddToTab_popup,#overlay').show().focus();
        $('#combobox').trigger('chosen:open');
        disableScrollbar();
        return false;
    }

    function ShowInboxOpacity(obj) {
        obj.style.getElementById("lnkbtnPreview")
        obj.style.opacity = 0.2;
    }

    function removeAllInboxEditable() {
        $('.txtFileName,.saveInboxfile,.cancelInboxfile').hide();
        $('.lblfileName,.editInboxfile,.singleAddToTab').show();
    }

    var myTimer;
    var fileIDDividerId = [];

    function showLoader() {
        $('#overlay').show().height($(document).height());
    }

    function hideLoader() {
        $('#overlay').hide();
    }

    function onClientButtonClick() {
        $find('pnlPopup2').hide();

        setTimeout(function () {
        }, 1000);
        return false;
    }

    function checkInbox() {
        showLoader();
        var pendingIDs = "";
        if ($('.edFormsFolderGrid input[type="checkbox"]:checked').length > 0) {
            $('.edFormsFolderGrid input[type="checkbox"]:checked').each(function () {
                if ($(this).closest('tr').find("input[type='hidden'][id$=hdnEdFormsShareId]") && $(this).closest('tr').find("input[type='hidden'][id$=hdnEdFormsShareId]").val())
                    pendingIDs += $(this).closest('tr').find("input[type='hidden'][id$=hdnEdFormsShareId]").val() + ",";
            });
            pendingIDs = pendingIDs.substring(0, pendingIDs.length - 1);
            $('#<%= hdnPendingIDs.ClientID%>').attr("value", pendingIDs); // set hidden field value
            return true;
        }
        else {
            hideLoader();
            alert("Please Select atleast one file.");
            return false;
        }
    }

    function showInboxEdit(ele) {
        $('.saveInboxfile,.cancelInboxfile').hide();
        $('.deleteInboxfile,.lnkbtnPreview,.editInboxfile,.lnkBtnDownload,.singleAddToTab').show();
        var $this = $(ele);
        $this.css('display', 'none');
        $this.parent().find('[name=lbkUpdate]').show();
        $this.parent().find('[name=lnkCancel]').show();
        var row = $(ele).closest("tr");
        $("td", row).each(function () {
            $(this).find('input[type=text]').css('border', '1px solid #c4c4c4');
            $(this).find('input[type = text]').show();
            if ($(this).find('input[type = text]').val() == undefined || $(this).find('input[type = text]').val() == "") { }
            else {
                oldname = $(this).find('input[type = text]').val();
            }
            $(this).find('[name=lblfileName]').hide();
            $(this).find('[name=lblClass]').hide();
            $(this).closest('td').find('.deleteInboxfile,.lnkbtnPreview,.lnkBtnDownload,.singleAddToTab').hide();
        });
        return false;
    }

    function cancleInboxFileName(ele) {
        var $this = $(ele);
        var InboxlabelName = '';
        var FileName = '';
        $this.css('display', 'none');
        $this.parent().find('[name=lbkUpdate]').hide();
        $this.parent().find('[name=lnkEdit]').show();
        $this.parent().find('.deleteInboxfile,.lnkbtnPreview,.lnkBtnDownload,.singleAddToTab').show();
        var row = $(ele).closest("tr");
        $("td", row).each(function () {
            var len = 0;
            len = $(this).find('input[type=text]').length;
            if (len > 0) {
                InboxlabelName = $(this).find('[name=lblfileName]').html();
                $(this).find('input[type = text]').val(InboxlabelName);
                $(this).find('input[type = text]').hide();
                $(this).find('[name=lblfileName]').show();
            }
            else {
            }
        });
    }

    function showPreview(URL) {
        hideLoader();
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//"
                + window.location.hostname
                + (window.location.port ? ':' + window.location.port : '');
        }

        var src = "../Office/Preview.aspx?data=" + window.location.origin + URL.trim();
        var popup = document.getElementById("preview_popup");
        var overlay = document.getElementById("overlay");
        $('#reviewContent').attr('src', src);
        overlay.style.display = 'block';
        popup.style.display = 'block';
        $('#preview_popup').focus();
        disableScrollbar();
        return false;
    }

    function ShowPreviewForEdFormsShare(edFormsUserShareId, fileName) {
        showLoader();
        $('#<%= hdnPendingIDs.ClientID %>').val(edFormsUserShareId);
        $('#<%= hdnPreviewShareID.ClientID%>').val(edFormsUserShareId);
        $.ajax({
            type: "POST",
            url: '../Office/EdForms.aspx/GetPreviewUrlForEdFormsShare',
            contentType: "application/json; charset=utf-8",
            data: "{ documentID:'" + edFormsUserShareId + "', edFormsShareId:'" + edFormsUserShareId + "', shareID:'" + fileName + "'}",
            dataType: "json",
            success: function (data) {
                if (data.d == "") {
                    hideLoader();
                    alert("Failed to get object from cloud");
                    return false;
                }
                else
                    return showPreview(data.d);
            },
            error: function (result) {
                console.log('Failed' + result.responseText);
                hideLoader();
            }
        });
        return false;
    }

    function disableScrollbar() {
        $('html, body').css({
            'overflow': 'hidden',
            'height': '100%'
        });
    }

    function enableScrollbar() {
        $('html, body').css({
            'overflow': 'auto',
            'height': 'auto'
        });
    }

    function OnSaveRenameShare(ele, edFormsUserShareId, oldName) {
        var newName = $(ele).parent().parent().find("input.txtFileName").val();
        if (newName == undefined || newName == "") {
            $('.txtFileName').css('border', '1px solid red');
            return false;
        }
        else if (newName.indexOf('.') > 0 && newName.substring(newName.indexOf('.') + 1).toLowerCase() != 'pdf') {
            alert("Only File with type 'pdf' is allowed.");
            $('.txtFileName').css('border', '1px solid red');
            return false;
        }
        else if (!/^[a-z\d][a-z\d\()_\-.\s]+$/i.test(newName)) {
            alert("File name can not contain special characters.");
            $('.txtFileName').css('border', '1px solid red');
            return false;
        }
        else {
            $('.txtFileName').css('border', '1px solid c4c4c4');
        }
        showLoader();
        $.ajax(
            {
                type: "POST",
                url: '../EFileFolderJSONService.asmx/RenameDocumentForEdFormsShare',
                data: "{ renameFile:'" + newName + "', edFormsShareId:'" + edFormsUserShareId + "',oldFileName:'" + oldName + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (result.d.length > 0)
                        alert("Failed to rename document. Please try again after sometime.");
                    hideLoader();
                    __doPostBack('', 'RefreshGrid@');
                },
                error: function (result) {
                    hideLoader();
                }
            });
    }

    function Check_Click(objRef) {
        var row = objRef.parentNode.parentNode.parentNode;
        var GridView = row.parentNode;
        var inputList = GridView.getElementsByTagName("input");

        for (var i = 0; i < inputList.length; i++) {
            var headerCheckBox = inputList[0];
            var checked = true;
            if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                if (!inputList[i].checked) {
                    checked = false;
                    break;
                }
            }
        }
        headerCheckBox.checked = checked;
    }

    function checkAll(objRef) {
        var inboxGridView = objRef.parentNode.parentNode.parentNode.parentNode;
        var inboxinputList = inboxGridView.getElementsByTagName("input");

        for (var i = 0; i < inboxinputList.length; i++) {
            var row = inboxinputList[i].parentNode.parentNode;
            if (inboxinputList[i].type == "checkbox" && objRef != inboxinputList[i]) {
                if (objRef.checked) {
                    inboxinputList[i].checked = true;
                }
                else {
                    inboxinputList[i].checked = false;
                }
            }
        }
    }

    function RemoveMessage() {
        $("#<%= textName.ClientID %>").val("");
        $("#<%= textMessage.ClientID %>").val("");
    }

</script>
