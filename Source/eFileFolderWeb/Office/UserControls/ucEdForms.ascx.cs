﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Shinetech.DAL;
using System.IO;
using System.Configuration;
using Shinetech.Utility;
using System.Web.Services;
using System.Web.Script.Services;
using Newtonsoft.Json;

public partial class Office_UserControls_ucEdForms : System.Web.UI.UserControl
{
    public string FolderId;
    public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    public string newFileName = string.Empty;
    public string SortType = "DESC";
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
    private NLogLogger _logger = new NLogLogger();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Sessions.HasViewPrivilegeOnly)
        {
            btnAddtoTab.Visible = false;
        }
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
        else
        {
            var argument = Request.Form["__EVENTARGUMENT"];
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower() == "refreshgrid@")
            {
                GetData();
                BindGrid();
            }
        }
    }

    private void GetData()
    {
        try
        {
            DataTable dtEdForms = new DataTable();
            dtEdForms = General_Class.GetEdFormsForUserFolder(Enum_Tatva.EdFormsStatus.Submit.GetHashCode(), (int)Enum_Tatva.EdFormsLog.Submit, Sessions.SwitchedRackspaceId);
            this.DataSource = dtEdForms;
        }
        catch (Exception ex)
        {
            _logger.Info(ex.Message, ex);
        }
    }

    private void BindGrid()
    {
        try
        {
            EdFormsFolderGrid.DataSource = this.DataSource;
            EdFormsFolderGrid.DataBind();
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
        catch (Exception ex)
        {
            _logger.Info(ex.Message, ex);
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["SubmittedEdForms"] as DataTable;
        }
        set
        {
            this.Session["SubmittedEdForms"] = value;
        }
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void WebPager1_PageIndexChanged(object sender, Shinetech.Framework.Controls.PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "pageUserLoad();", true);
    }

    protected void WebPager1_PageSizeChanged(object s, Shinetech.Framework.Controls.PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "pageUserLoad();", true);
    }

    protected void btnSelectAddToTab_Click(object sender, EventArgs e)
    {
        try
        {
            string pendingIDs = hdnPendingIDs.Value;
            int folderId = Convert.ToInt32(hdnSelectedFolderID.Value);
            int dividerId = Convert.ToInt32(hdnSelectedDividerID.Value);
            string errorMsg = string.Empty;
            if (!string.IsNullOrEmpty(pendingIDs))
            {
                DataTable dtFiles = General_Class.GetEdFormsUserShareById(pendingIDs);
                dtFiles.AsEnumerable().ToList().ForEach(d =>
                {
                    try
                    {
                        var file = d.Field<string>("FileName");
                        string edFormsUserShareId = d.Field<string>("EdFormsUserShareId").ToString();
                        RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                        string path = rackSpaceFileUpload.GeneratePath(file.ToLower(), Enum_Tatva.Folders.EdFormsShare, edFormsUserShareId.ToLower());
                        string dir = Server.MapPath("~/" + Common_Tatva.RackSpaceEdFormsDownload + Path.DirectorySeparatorChar + Sessions.SwitchedRackspaceId);
                        if (!Directory.Exists(dir))
                        {
                            Directory.CreateDirectory(dir);
                        }
                        string fileName = rackSpaceFileUpload.GetObject(ref errorMsg, path, Sessions.SwitchedRackspaceId, (int)Enum_Tatva.Folders.EdFormsShare);
                        if (string.IsNullOrEmpty(errorMsg))
                        {
                            var filePath = dir + "\\" + fileName;

                            string encodeName = HttpUtility.UrlPathEncode(file).Replace("%", "$");
                            encodeName = encodeName.Replace(encodeName, encodeName.Replace("$20", " "));
                            encodeName = Common_Tatva.SubStringFilename(encodeName);
                            encodeName = encodeName.Replace(encodeName, encodeName.Replace(" ", "$20"));
                            string vpath = Server.MapPath(string.Format("~/{0}", CurrentVolume));
                            string pathName = CurrentVolume + Path.DirectorySeparatorChar + folderId + Path.DirectorySeparatorChar + dividerId + Path.DirectorySeparatorChar + encodeName;

                            int documentOrder = FlashViewer.GetDocumentOrder(Convert.ToInt32(dividerId));
                            string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, folderId, Path.DirectorySeparatorChar, dividerId);
                            if (!Directory.Exists(dstpath))
                            {
                                Directory.CreateDirectory(dstpath);
                            }

                            dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);
                            if (File.Exists(dstpath))
                            {
                                dstpath = Common_Tatva.GetRenameFileName(dstpath);
                            }

                            File.Copy(filePath, dstpath, true);
                            string newFilename = Path.GetFileNameWithoutExtension(dstpath);
                            string displayName = HttpUtility.UrlDecode(newFilename.Replace('$', '%').ToString());
                            string documentIDOrder = Common_Tatva.InsertDocument(dividerId, pathName, documentOrder, displayName, Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()));
                            if (documentIDOrder == "0")
                            {
                                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to move file. There is a problem with the file format." + "\");pageUserLoad();hideLoader();", true);
                            }
                            else
                            {
                                General_Class.AuditLogByDocId(Convert.ToInt32(documentIDOrder.Split('#')[0]), Sessions.RackSpaceUserID, Enum_Tatva.Action.Create.GetHashCode());
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(folderId), Convert.ToInt32(dividerId), Convert.ToInt32(documentIDOrder.Split('#')[0]), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.InboxUpload.GetHashCode(), 0, 1);
                                General_Class.MoveEdFormsUserShare(edFormsUserShareId, Convert.ToInt32(folderId), Convert.ToInt32(dividerId), Sessions.UserId); ;
                                General_Class.ChangeEdFormsUserShareStatus(edFormsUserShareId, (int)Enum_Tatva.EdFormsStatus.Move);
                                string IP = string.Empty;
                                try
                                {
                                    IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                                }
                                catch (Exception ex)
                                {
                                    _logger.Info(ex);
                                }
                                General_Class.InsertIntoEdFormsUserLogs(edFormsUserShareId, (byte)Enum_Tatva.EdFormsStatus.Move, IP);
                                General_Class.UpdateEdFormsUserFolder(edFormsUserShareId, folderId);
                                File.Delete(filePath);
                                rackSpaceFileUpload.DeleteObject(ref errorMsg, path);
                                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "File has been moved." + "\");pageUserLoad();hideLoader();", true);
                            }
                        }
                        else
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to get object from cloud." + "\");pageUserLoad();hideLoader();", true);
                    }
                    catch (Exception ex)
                    {
                        Common_Tatva.WriteErrorLog("", "", "", ex);
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to move file." + "\");hideLoader();pageUserLoad();", true);
                    }

                });
            }

            string folderName = hdnSelectedFolderText.Value;
            folderName = folderName.Insert(folderName.IndexOf(')') + 1, "/");
            folderName = folderName.Remove(folderName.IndexOf('('), folderName.IndexOf(')') - folderName.IndexOf('(') + 1);
            string message = "File(s) have been moved to folder: " + folderName;
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + message + "\");pageUserLoad();hideLoader();", true);
            GetData();
            BindGrid();

        }
        catch (Exception ex)
        {
            _logger.Info(ex.Message, ex);
        }
        finally
        {
            ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "pageUserLoad();hideLoader();", true);
        }
    }

    protected void lnkBtnDelete_Command(object sender, CommandEventArgs e)
    {
        try
        {
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            string password = textPassword.Text.Trim();
            string strPassword = PasswordGenerator.GetMD5(password);
            string errorMsg = string.Empty;

            if (UserManagement.GetPassword(this.Page.User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
            {
                //show the password is wrong        
                ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssYes", "alert('The password you entered does not match our records, please try again or contact your administrator.');pageUserLoad();", true);
            }
            else
            {
                LinkButton lnkbtnDelete = sender as LinkButton;
                GridViewRow gvrow = lnkbtnDelete.NamingContainer as GridViewRow;
                string edFormsUserShareId = e.CommandArgument.ToString().Split(';')[0];
                DataTable dtFiles = General_Class.GetEdFormsUserShareById(edFormsUserShareId);
                dtFiles.AsEnumerable().ToList().ForEach(d =>
                {
                    var file = d.Field<string>("FileName");
                    string id = d.Field<string>("EdFormsUserShareId");
                    string pathName = rackSpaceFileUpload.GeneratePath(file, Enum_Tatva.Folders.EdFormsShare, id.ToLower());
                    rackSpaceFileUpload.DeleteObject(ref errorMsg, pathName);
                    General_Class.UpdateEdFormsForUserShare(file, id, false, true);
                    if (string.IsNullOrEmpty(errorMsg))
                    {
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "File " + file + " has been deleted successfully" + "\");pageUserLoad();", true);
                    }
                    else
                    { ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Failed to delete file: " + file + ". Please try again after sometime." + "\");pageUserLoad();", true); }
                });
                GetData();
                BindGrid();
            }
        }
        catch (Exception ex)
        {
            _logger.Info(ex.Message, ex);
        }
        finally
        {
            ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
        }
    }

    protected void EdFormsFolderGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortDirection = "";
            string sortExpression = e.SortExpression;
            if (this.Sort_Direction == SortDirection.Ascending)
            {
                this.Sort_Direction = SortDirection.Descending;
                sortDirection = "DESC";
            }
            else
            {
                this.Sort_Direction = SortDirection.Ascending;
                sortDirection = "ASC";
            }
            DataView Source = new DataView(this.DataSource);
            Source.Sort = e.SortExpression + " " + sortDirection;
            this.DataSource = Source.ToTable();
            EdFormsFolderGrid.DataSource = Source.ToTable();
            EdFormsFolderGrid.DataBind();
            WebPager1.CurrentPageIndex = 0;
            WebPager1.DataSource = Source.ToTable();
            WebPager1.DataBind();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "pageUserLoad();", true);
        }
        catch (Exception ex)
        {
            _logger.Info(ex.Message, ex);
        }
    }

    protected void EdFormsFolderGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
                string edFormsUserShareId = drv.Row["EdFormsUserShareId"].ToString();
                string fileName = drv.Row["FileName"].ToString();
                HyperLink lnkBtnDownload = (e.Row.FindControl("lnkBtnDownload") as HyperLink);
                if (lnkBtnDownload != null)
                {
                    lnkBtnDownload.Attributes.Add("href", "PdfViewer.aspx?ID=" + edFormsUserShareId + "&data=" + QueryString.QueryStringEncode("ID=" + fileName + "&folderID=" + (int)Enum_Tatva.Folders.EdFormsShare + "&sessionID=" + Sessions.SwitchedRackspaceId));
                }
            }
            catch (Exception ex)
            {
                _logger.Info(ex.Message, ex);
            }
        }
    }

    protected void lnkBtnNotify_Command(object sender, CommandEventArgs e)
    {
        EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
        string[] details = Convert.ToString(e.CommandArgument).Split(',');
        bool isSent = eFileFolderJSONWS.NotifyBackForEdForms(details[0], e.CommandName, details[1], textName.Text, textMessage.Text, Sessions.SwitchedEmailId);
        if (isSent)
        {
            GetData();
            BindGrid();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert('An EdForm has been resent successfully.');", true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert('An error occured while resending an EdForm. Please try again later!!');", true);
        }
    }
}