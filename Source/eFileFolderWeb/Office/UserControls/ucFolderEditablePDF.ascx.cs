﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Office_UserControls_ucFolderEditablePDF : System.Web.UI.UserControl
{

    public static string PDFUrl = string.Empty;
    public static string rackSpaceURL = string.Empty;
    public static string documentFullPath = string.Empty;
    public static string shareID = string.Empty;
    protected string PDFData = "";
    public string Signature = string.Empty;
    public string viewerUID = string.Empty;
    public static string UID = string.Empty;
    public static string SharedByID = string.Empty;
    public string logoURL
    {
        get
        {
            string filePath = Common_Tatva.UploadFolder.Replace("/", "") + Common_Tatva.UserLogoFolder + "/" + viewerUID + ".png";
            if (!File.Exists(Server.MapPath("~/" + filePath)))
                filePath = "";
            return filePath;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        ViewerEditablePdf editablePdf = new ViewerEditablePdf();
       HiddenField hf = (HiddenField)Parent.FindControl("hdnFolderId");
       PDFUrl = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.LastIndexOf('/') + 1) + "Volume2/" + hf.Value +"/LogForm/PRC_Certificated.pdf";
       string tempPath = "Volume2/" + hf.Value + "/LogForm/PRC_Certificated.pdf";
       documentFullPath = HttpContext.Current.Server.MapPath("~/" + tempPath);
      DocumentCollectionView docView = editablePdf.GetPDFData(documentFullPath);
      PDFData = docView.PDFData;
    }

    public void SetThis()
    {
 
    }
}