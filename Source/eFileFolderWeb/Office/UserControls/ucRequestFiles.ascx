﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucRequestFiles.ascx.cs" Inherits="Office_UserControls_ucRequestFiles" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%--<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>--%>
<%@ Register Src="~/Office/UserControls/ucWorkArea.ascx" TagPrefix="uc1" TagName="ucWorkArea" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/Office/UserControls/ucAddToTab.ascx" TagPrefix="uc1" TagName="ucAddToTab" %>


<link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/jquery-ui-1.8.22.custom.css" />
<link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css" rel="stylesheet" />
<link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=1" rel="stylesheet" />
<link rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/style.min.css" />
<script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js"></script>
<script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>extras/jquery-ui-1.8.22.custom.min.js"></script>

<style type="text/css">
    .ajax__tab_body
    {
        height: auto!important;
    }

    .white_content
    {
        display: none;
        position: fixed;
        top: 20%;
        left: 20%;
        width: 60%;
        height: 50%;
        padding: 16px;
        background-color: white;
        z-index: 999999999;
        overflow: auto;
    }

    #overlay
    {
        display: none;
        position: absolute;
        top: 0;
        bottom: 0;
        background: #999;
        width: 100%;
        height: 100%;
        opacity: 0.8;
        z-index: 99999;
    }

    .preview
    {
        position: relative;
        left: -10px;
    }

    td span
    {
        -ms-word-break: break-all;
        word-break: break-all;
        word-break: break-word;
        -webkit-hyphens: auto;
        -moz-hyphens: auto;
        hyphens: auto;
        max-width: 300px;
        white-space: normal!important;
    }

    td
    {
        max-width: 200px;
    }

    .actionMinWidth
    {
        min-width: 50px;
    }

    .chkpending, .chkinbox
    {
        margin-left: 10px;
    }

    .chkpendingAll, .chkinboxAll
    {
        margin-left: 10px;
    }

    .boxtitle
    {
        margin-top: 20px !important;
    }

    .file
    {
        background: url(../lib/TreeView/file_sprite.png) 0 0 no-repeat !important;
        width: 18px !important;
        height: 18px !important;
        margin-top: 4px!important;
        margin-left: 2px!important;
        margin-right: 5px!important;
    }


    /*16-09-2016*/
    .create-btn
    {
        background-image: url(../images/create-folder.png);
        cursor: pointer;
    }


    .btnAddtoTab, .btnAddtoWorkArea
    {
        padding: 8px 10px 8px 35px;
        font-size: 15px;
        padding-right: 10px;
        width: auto;
        height: auto;
        line-height: normal;
        margin-right: 5px;
        background-image: url(../images/plus-icon.png);
        background-position: 7px center;
        background-repeat: no-repeat;
    }

    .btnShare
    {
        padding: 6px 0px 6px 35px;
        width: auto;
        height: auto;
        line-height: normal;
        background-image: url(../images/share-icon.png);
        background-position: 7px center;
        background-repeat: no-repeat;
    }


    .addtotab-workarea
    {
        cursor: pointer;
        background-image: url(../images/AddToTabWorkArea.png);
    }

    .rename-btn
    {
        background-image: url(../images/rename.png);
        cursor: pointer;
    }

    .delete-btn
    {
        background-image: url(../images/delete.png);
        cursor: pointer;
    }

    .cancel-btn
    {
        background-image: url(../images/cancel-btn.png);
        cursor: pointer;
    }

    .send-btn
    {
        background-image: url(../images/send.png);
        cursor: pointer;
    }

    .preview-btn
    {
        cursor: pointer;
    }

    .register-container .form-containt .content-title
    {
        margin-bottom: 5px;
        border: none !important;
        padding: 0px!important;
    }

    .popupclose
    {
        float: right;
        padding: 10px;
        cursor: pointer;
    }

    .popupcontent
    {
        padding: 10px;
    }

    .form-containt .content-box fieldset
    {
        font-size: initial;
    }

    .ajax__fileupload_dropzone
    {
        font-size: 15px;
        line-height: 120px !important;
        height: 120px !important;
    }

    .lnkbtnPreview
    {
        margin-left: -10px;
    }

    .downloadFile
    {
        margin-left: -10px;
        cursor: pointer;
    }

    .preview-popup
    {
        width: 100%;
        max-width: 900px;
        position: fixed;
        z-index: 100001;
        left: 0;
        top: 10%;
        right: 0;
        margin: auto;
        background: #fff;
        height: 85%;
    }

    #floatingCirclesG
    {
        top: 50%;
        left: 50%;
        position: fixed;
    }

    .listing-contant .quick-find
    {
        background: #fff;
        padding: 0px 0px 0px 0px;
        width: 235px;
        margin-top: 0px;
    }

    .quick-find .find-inputbox .quick-find-btn
    {
        top: 3px;
        height: 30px;
        width: 30px;
    }

    .quick-find .find-inputbox .find-input
    {
        height: 35px;
        padding-right: 5px;
    }

    .shareDocument
    {
        cursor: pointer;
    }

    .ui-dialog
    {
        top: 15% !important;
        position: fixed;
    }

    .ic-save, .ic-share
    {
        cursor: pointer;
    }

    .selectedTreeNodeLabel
    {
        display: inline-block;
        font-size: 14px;
        margin-top: 25px;
    }

    .listing-datatable
    {
        border-bottom: 2px !important;
        border-width: 1px !important;
    }

    .register-container .form-containt .content-box
    {
        border: none;
        padding: 0px 0px 0px 0px;
    }

    .ui-dialog
    {
        width: 370px !important;
    }
</style>

<script language="JavaScript" type="text/jscript">
    var ids = "", selectedButton = "", selectedPath = "", selectedChildNode = "", modalOpenedFrom = "";
    var userId = '<%= Sessions.SwitchedRackspaceId%>';
    function pageLoad() {
        if(<%=grdRequestedFiles.Rows.Count %> == 0)
        {
            $('#<%=btnAddtoTab.ClientID%>').prop("disabled",true);
            $('#<%=btnAddtoTab.ClientID%>').css("opacity", "0.65");
            $('#<%=btnAddtoWorkArea.ClientID%>').prop("disabled",true);
            $('#<%=btnAddtoWorkArea.ClientID%>').css("opacity", "0.65");
        }
        else
        {
            $('#<%=btnAddtoTab.ClientID%>').prop("disabled",false);
            $('#<%=btnAddtoTab.ClientID%>').css("opacity", "1");
            $('#<%=btnAddtoWorkArea.ClientID%>').prop("disabled",false);
            $('#<%=btnAddtoWorkArea.ClientID%>').css("opacity", "1");
        }

        $("select[name*='PageList']").css("height", "36px");
        $("select[name*='PageList']").css("margin-top", "-3px");
        if (LoadFolders)
            LoadFolders();

        $('#<%=btnAddtoWorkArea.ClientID%>').click(function (e) {
            e.preventDefault();
            if (checkInbox()) {
                $('#WorkArea_popup,#overlay').show().focus();
                disableScrollbar();
                if (loadWorkTree)
                    loadWorkTree();
            }
        });

        $('#<%= btnPreviewWA.ClientID%>').click(function (e) {
            e.preventDefault();
            $('#WorkArea_popup,#overlay').show().focus();
            disableScrollbar();
            if (loadWorkTree)
                loadWorkTree();
        });
        $('.popupclose').click(function () {
            $('.popup-mainbox').hide();
            $('#overlay').hide();
            enableScrollbar();
        });

        $("#btnCancelWA,#btnCancelAddToTab").click(function () {
            $(this).closest('.popup-mainbox').hide();
            var popup = document.getElementById("preview_popup");
            if (popup.style.display == "none") {
                $('#overlay').hide();
                enableScrollbar();
            }
        });

        $('#<%= btnSelectWA.ClientID%>').click(function (e) {
            var isLeaf = false;
            var nodes = $("#jstree1").jstree(true).get_selected("full", true);
            if (nodes && nodes.length > 0) {
                var path = "", treeId = "";
                if (nodes[0].text.indexOf('.') > -1)
                    isLeaf = true;

                if (!isLeaf &&
                    nodes && nodes.length > 0 && nodes[0].li_attr && nodes[0].li_attr.path) {
                    $('#overlay').css('z-index', '100010');
                    path = nodes[0].li_attr.path;
                    treeId = nodes[0].li_attr.name;
                    $('#<%=hdnPath.ClientID%>').val(path);
                    $('#<%=hdnTreeId.ClientID%>').val(treeId);
                    return true;
                }
            }
            //alert('Please select folder to move request files.');
            path = "workArea";
            $('#<%=hdnPath.ClientID%>').val(path);
            $('#<%=hdnTreeId.ClientID%>').val(0);
            return true;
            //e.preventDefault();
        });

        $('#<%=btnAddtoTab.ClientID%>').click(function (e) {
            e.preventDefault();
            if (checkInbox()) {
                $('#AddToTab_popup,#overlay').show().focus();
                $('#combobox').trigger('chosen:open');
                e.stopPropagation();
                disableScrollbar();
        
            }
        });
        $('#<%= btnPreviewAddTotab.ClientID%>').click(function (e) {
            e.preventDefault();
            $('#AddToTab_popup,#overlay').show().focus();
            $('#combobox').trigger('chosen:open');
            e.stopPropagation();
            disableScrollbar();
        });


        $('#<%= btnSelectAddToTab.ClientID%>').click(function (e) {
            var selectedFolder = $("#combobox").chosen().val();
            var selectedDivider = $("#comboboxDivider").chosen().val();
            if (selectedFolder == "0" && selectedDivider == "0") {
                alert("Please select at least one EdFile & one Tab");
                e.preventDefault();
                return false;
            }
            else if (selectedFolder == "0") {
                alert("Please select at least one EdFile");
                e.preventDefault();
                return false;
            }
            else if (selectedDivider == "0") {
                alert("Please select at least one Tab");
                e.preventDefault();
                return false;
            }
            var folderValue = $('.chosen-single').text();
            if (selectedFolder == undefined || selectedFolder == null || selectedFolder.length <= 0) {
                alert('Please select folder to move request files.');
                e.preventDefault();
                return false;
            }
            $('#overlay').css('z-index', '100010');
            $('#<%= hdnSelectedFolderID.ClientID%>').val(selectedFolder);
            $('#<%= hdnSelectedDividerID.ClientID%>').val(selectedDivider);
            $('#<%= hdnSelectedFolderText.ClientID%>').val(folderValue);
            return true;
        });


        $('#dialog-share-book').dialog({
            autoOpen: false,
            width: 450,
            //height: 540,
            top: 300,
            resizable: false,
            modal: true,
            buttons: {
                "Close": function () {
                    $(this).dialog("close");
                },
                "Share Link": function () {
                    SaveMail();
                }
            },
            close: function () {
            }
        });

        $('.ui-dialog-buttonset').find('button').each(function () {
            $(this).find('span').text($(this).attr('text'));
            if($(this).attr('text') == "Share Link")
            {
                $(this).attr("title","Securely request documents from anyone in to your EdFiles' inbox");
            }
            
        })

        $("#dialog-message-all").dialog({
            autoOpen: false,
            resizable: false,
            modal: true,
            Ok: function () {
                $(this).dialog("close");
            },
            close: function (event, ui) {
                $(this).dialog("option", 'title', '');
                $('#dialog-confirm-content').empty();
            }

        });

        window.CheckValidation = function CheckValidation(fieldObj) {
            if (fieldObj.val() == "") {
                fieldObj.css('border', '1px solid red');
            }
            else {
                fieldObj.css('border', '1px solid black');
            }
        }

        window.CheckEmailValidation = function CheckEmailValidation(fieldObj) {
            var x = fieldObj.val();
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(x)) {
                fieldObj.css('border', '1px solid black');
                return true;
            }
            else {
                fieldObj.css('border', '1px solid red');

                return false;
            }
        }

        function SaveMail() {
            //var email = $('#dialog-share-book input[name="email"]').val();
            //var name = $('#dialog-share-book input[name="myname"]').val();
            var emailto = $('#dialog-share-book input[name="emailto"]').val();
            var nameto = $('#dialog-share-book input[name="nameto"]').val();

            //var title = $('#dialog-share-book input[name="title"]').val();
            var content = $('#dialog-share-book textarea[name="content"]').val();
            var Isvalid = false;

            //$('#dialog-share-book input[name="myname"]').keyup(function () { CheckValidation($('#dialog-share-book input[name="myname"]')) });
            //$('#dialog-share-book input[name="email"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="email"]')) });

            $('#dialog-share-book input[name="emailto"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="emailto"]')) });

            //if (email == "" || name == "" || emailto == "") {
            if (emailto == "") {
                //CheckValidation($('#dialog-share-book input[name="email"]'));
                //CheckValidation($('#dialog-share-book input[name="myname"]'));
                CheckValidation($('#dialog-share-book input[name="emailto"]'));
                //CheckEmailValidation($('#dialog-share-book input[name="email"]'));
                CheckEmailValidation($('#dialog-share-book input[name="emailto"]'))
                return false;
            }

            //if (!Isvalid) {
            //    Isvalid = CheckEmailValidation($('#dialog-share-book input[name="email"]'));
            //    if (!Isvalid)
            //        return Isvalid;
            //}

            Isvalid = CheckEmailValidation($('#dialog-share-book input[name="emailto"]'));
            if (!Isvalid)
            { return Isvalid; }

            var src = '';
            showLoader();
            if (modalOpenedFrom == "Inbox") {
                $.ajax(
                    {

                        type: "POST",
                        url: '../EFileFolderJSONService.asmx/ShareMailForInbox',
                        //data: "{ strSendMail:'" + email + "', strToMail:'" + emailto + "', documentId:'" + documentId + "', strMailInfor:'" + content + "',SendName:'" + name + "', ToName:'" + nameto + "',MailTitle:'" + title + "',Path:'" + src + "'}",
                        data: "{strToMail:'" + emailto + "',MailTitle:'" + "" + "',ToName:'" + nameto + "',SendName:'" + "" + "',strSendMail:'" + "" + "',strMailInfor:'" + content + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var id = data.d;

                            var title, message;
                            if (id === 0) {
                                title = "Sharing Inbox URL error";
                                message = "URL: " + data.d.toString().trim() + " has not been shared successfully."
                            }
                            else {
                                title = "Sharing Inbox URL success";
                                message = "URL: " + data.d.toString().trim() + " has been shared successfully."
                            }

                            $('#dialog-message-all').dialog({
                                title: title,
                                open: function () {
                                    $("#spanText").text(message);
                                }
                            });

                            $('#dialog-message-all').dialog("open");

                            if (id != null || id != "")
                                $('#dialog-share-book').dialog("close");
                            hideLoader();
                            modalOpenedFrom = "";
                        },
                        fail: function (data) {
                            bookshelf.loaded.apply();

                            $('#dialog-message-all').dialog({
                                title: "Sharing Inbox URL error",
                                open: function () {
                                    $("#spanText").text("This URL has not been shared successfully.");
                                }
                            });
                            $('#dialog-message-all').dialog("open");

                            hideLoader();
                            modalOpenedFrom = "";
                        }
                    });
            }
        }

        $('.editInboxfile').click(function (e) {
            e.preventDefault();
            removeAllInboxEditable();
            showInboxEdit($(this));
        });

        $('.editPendingfile').click(function (e) {
            e.preventDefault();
            removeAllPendingEditable();
            showPendingEdit($(this));
        });

        $('.cancelInboxfile').click(function (e) {
            e.preventDefault();
            removeAllInboxEditable();
        });

        $('.cancelPendingfile').click(function (e) {
            e.preventDefault();
            removeAllPendingEditable();
        });

        $("#ButtonDeleleOkay").live({
            click: function () {
                showLoader();
            }
        });

        $('.shareDocument').click(function (e) {
            e.preventDefault();
        });

        var closePopup = document.getElementById("popupclose");
        closePopup.onclick = function () {
            var popup = document.getElementById("preview_popup");
            var overlay = document.getElementById("overlay");
            overlay.style.display = 'none';
            popup.style.display = 'none';
            //popup.style.maxWidth = '900px;'
            enableScrollbar();
            $('#reviewContent').removeAttr('src');
        };

        $(".lnkbtnPreview").load(function (e) {
            e.preventDefault();
            ShowInboxOpacity($(this));
        });

        $(".lnkbtnPreviewPending").load(function (e) {
            e.preventDefault();
            ShowPendingOpacity($(this));
        });

    }

    function OnShare(dialogOpenedFrom) {
        modalOpenedFrom = dialogOpenedFrom;
        if (modalOpenedFrom == "Inbox") {
            $("#dialog-share-book").dialog({ title: "Records Request Link" });
        }
        else if (modalOpenedFrom == "WorkArea") {
            $("#dialog-share-book").dialog({ title: "Share this WorkArea Document" });
        }
        $('#dialog-share-book').dialog("open");
        $('#dialog-share-book input[name="email"]').val('');
        $('#dialog-share-book input[name="myname"]').val('');
        $('#dialog-share-book input[name="emailto"]').val('');
        $('#dialog-share-book input[name="nameto"]').val('');
        $('#dialog-share-book input[name="title"]').val('');
        $('#dialog-share-book textarea[name="content"]').val('');

        $('#dialog-share-book input[name="email"]').css('border', '1px solid black');
        $('#dialog-share-book input[name="myname"]').css('border', '1px solid black');
        $('#dialog-share-book input[name="emailto"]').css('border', '1px solid black');

        $('#dialog-share-book input[name="email"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="email"]')) });

        $('#dialog-share-book input[name="emailto"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="emailto"]')) });
    }

    function ShowInboxOpacity(obj) {
        obj.style.getElementById("lnkbtnPreview")
        obj.style.opacity = 0.2;
    }

    function ShowPendingOpacity(obj) {
        obj.style.getElementById("lnkbtnPreviewPending")
        obj.style.opacity = 0.2;
    }

    function removeAllInboxEditable() {
        $('.txtFileName,.saveInboxfile,.cancelInboxfile').hide();
        $('.lblfileName,.editInboxfile').show();
    }

    function removeAllPendingEditable() {
        $('.txtFileName,.savePendingfile,.cancelPendingfile').hide();
        $('.lblPendingfileName,.editPendingfile').show();
    }

    var myTimer;
    var fileIDDividerId = [];

    function showLoader() {
        $('#overlay').show().height($(document).height());
    }

    function hideLoader() {
        $('#overlay').hide();
    }

    function onClientButtonClick() {
        $find('pnlPopup2').hide();

        setTimeout(function () {
        }, 1000);
        return false;
    }

    function checkInbox() {
        showLoader();
        var inboxIDs = "", pendingIDs = "";
        if ($('table input[type="checkbox"]:checked').length > 0) {
            $('table input[type="checkbox"]:checked').each(function () {
                if ($(this).closest('tr').find("input[type='hidden'][id$=hdnTypeValues]").val() == "Inbox") {
                    if ($(this).closest('tr').find("input[type='hidden'][id$=hdnInboxValues]") && $(this).closest('tr').find("input[type='hidden'][id$=hdnInboxValues]").val())
                        inboxIDs += JSON.parse($(this).closest('tr').find("input[type='hidden'][id$=hdnInboxValues]").val()).InboxId + ",";
                }
                else if ($(this).closest('tr').find("input[type='hidden'][id$=hdnTypeValues]").val() == "Pending") {
                    if ($(this).closest('tr').find("input[type='hidden'][id$=hdnPendingValues]") && $(this).closest('tr').find("input[type='hidden'][id$=hdnPendingValues]").val())
                        pendingIDs += $(this).closest('tr').find("input[type='hidden'][id$=hdnPendingValues]").val() + ",";
                }
            });
            inboxIDs = inboxIDs.substring(0, inboxIDs.length - 1);
            pendingIDs = pendingIDs.substring(0, pendingIDs.length - 1);
            $('#<%= hdnInboxIDs.ClientID %>').attr("value", inboxIDs);
            $('#<%= hdnPendingIDs.ClientID %>').attr("value", pendingIDs); // set hidden field value
            return true;
        }
        else {
            hideLoader();
            alert("Please Select atleast one file.");
            return false;
        }
    }
    function checkInboxForWorkArea() {
        if (checkInbox()) {
            $find("mpe").show();
            return false;
        }
    }

    function showInboxEdit(ele) {
        $('.saveInboxfile,.cancelInboxfile').hide();
        $('.deleteInboxfile,.lnkbtnPreview,.editInboxfile,.lnkBtnDownload').show();
        var $this = $(ele);
        $this.css('display', 'none');
        $this.parent().find('[name=lbkUpdate]').show();
        $this.parent().find('[name=lnkCancel]').show();
        var row = $(ele).closest("tr");
        $("td", row).each(function () {
            $(this).find('input[type=text]').css('border', '1px solid #c4c4c4');
            $(this).find('input[type = text]').show();
            if ($(this).find('input[type = text]').val() == undefined || $(this).find('input[type = text]').val() == "")
            { }
            else
            {
                oldname = $(this).find('input[type = text]').val();
            }
            $(this).find('[name=lblfileName]').hide();
            $(this).find('[name=lblClass]').hide();
            $(this).closest('td').find('.deleteInboxfile,.lnkbtnPreview,.lnkBtnDownload').hide();
        });
        return false;
    }


    function cancleInboxFileName(ele) {
        var $this = $(ele);
        var InboxlabelName = '';
        var FileName = '';
        $this.css('display', 'none');
        $this.parent().find('[name=lbkUpdate]').hide();
        $this.parent().find('[name=lnkEdit]').show();
        $this.parent().find('.deleteInboxfile,.lnkbtnPreview,.lnkBtnDownload').show();
        var row = $(ele).closest("tr");
        $("td", row).each(function () {
            var len = 0;
            len = $(this).find('input[type=text]').length;
            if (len > 0) {
                InboxlabelName = $(this).find('[name=lblfileName]').html();
                $(this).find('input[type = text]').val(InboxlabelName);
                $(this).find('input[type = text]').hide();
                $(this).find('[name=lblfileName]').show();
            }
            else {
            }
        });
    }

    function showPreview(URL) {
        hideLoader();
        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//"
              + window.location.hostname
              + (window.location.port ? ':' + window.location.port : '');
        }
       
        var src =  "../Office/Preview.aspx?data=" + window.location.origin + URL.trim();
        var popup = document.getElementById("preview_popup");
        var overlay = document.getElementById("overlay");
        $('#reviewContent').attr('src', src);
        overlay.style.display = 'block';
        popup.style.display = 'block';
        $('#preview_popup').focus();
        disableScrollbar();
        return false;
    }

    function ShowPreviewForRequestedFiles(inboxId, eFileFlowShareId, folderID) {
        showLoader();
        if (folderID == 2)
            $("#<%= hdnPreviewShareID.ClientID%>").val(eFileFlowShareId);
            else if(folderID == 3)
                $("#<%= hdnPreviewInboxID.ClientID%>").val(inboxId);
        $.ajax({
            type: "POST",
            url: '../Office/Welcome.aspx/GetPreviewURL',
            contentType: "application/json; charset=utf-8",
            data: "{ documentID:'" + inboxId + "', folderID:'" + folderID + "', shareID:'" + eFileFlowShareId + "'}",
            dataType: "json",
            success: function (data) {
                if (data.d == "") {
                    hideLoader();
                    alert("Failed to get object from cloud");
                    return false;
                }
                else
                    return showPreview(data.d);
            },
            error: function (result) {
                console.log('Failed' + result.responseText);
                hideLoader();
            }
        });
        return false;
    }

    function disableScrollbar() {
        $('html, body').css({
            'overflow': 'hidden',
            'height': '100%'
        });
    }

    function enableScrollbar() {
        $('html, body').css({
            'overflow': 'auto',
            'height': 'auto'
        });
    }

    function OnSaveRenameForRequest(txtRenameObj, filepath, inboxID, eFileFlowShareID, ext, type) {
        var newName = $('#' + txtRenameObj)[0].value;
        if (newName == undefined || newName == "") {
            $('.txtFileName').css('border', '1px solid red');
            return false;
        }
        else if (newName.indexOf('.') > 0)
        {
            var extension = newName.substring(newName.indexOf('.') + 1).toLowerCase();
            if (inboxID && inboxID != "") {
                if ((extension != 'jpg') && (extension != 'jpeg') && (extension != 'pdf'))
                {
                    alert("Only File with type 'jpg,jpeg and pdf' is allowed.");
                    $('.txtDocumentName').css('border', '1px solid red');
                    return false;
                }
            }
            else if (eFileFlowShareID && eFileFlowShareID != "") {
                if (extension != 'pdf') {
                    alert("Only File with type 'pdf' is allowed.");
                    $('.txtDocumentName').css('border', '1px solid red');
                    return false;
                }
                newName = newName.substring(0, newName.indexOf('.'));
            }
        }
        if (!/^[a-z\d][a-z\d\()_\-.\s]+$/i.test(newName)) {
            alert("File name can not contain special characters.");
            $('.txtDocumentName').css('border', '1px solid red');
            return false;
        }
        else {
            $('.txtFileName').css('border', '1px solid c4c4c4');
        }
        showLoader();
        var userId = "<%= Sessions.SwitchedRackspaceId%>";
            $.ajax(
    {
        type: "POST",
        url: '../EFileFolderJSONService.asmx/RenameRequestDocument',
        data: "{ renameFile:'" + newName + "', path:'" + filepath + "',inboxId:'" + inboxID + "',eFileFlowShareId:'" + eFileFlowShareID + "',fileExtention:'" + ext + "',type:'" + type + "',userId:'" + userId + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            if (result.d.length > 0)
                alert("Failed to rename document. Please try again after sometime.");
            hideLoader();
            __doPostBack('', 'RefreshGrid@');
        },
        error: function (result) {
            hideLoader();
        }
    });
        }

        function autoCompleteEx_FolderItemSelected(sender, e) {
            var folderId = $get('<%= hndFolderId.ClientID %>');
            folderId.value = e.get_value();
            __doPostBack(sender.get_element().name, 'RefreshTabs');
        }

        function Check_Click(objRef) {
            var row = objRef.parentNode.parentNode.parentNode;
            var GridView = row.parentNode;
            var inputList = GridView.getElementsByTagName("input");

            for (var i = 0; i < inputList.length; i++) {
                var headerCheckBox = inputList[0];
                var checked = true;
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                        break;
                    }
                }
            }
            headerCheckBox.checked = checked;
        }

        function checkAll(objRef) {
            var inboxGridView = objRef.parentNode.parentNode.parentNode.parentNode;
            var inboxinputList = inboxGridView.getElementsByTagName("input");

            for (var i = 0; i < inboxinputList.length; i++) {
                var row = inboxinputList[i].parentNode.parentNode;
                if (inboxinputList[i].type == "checkbox" && objRef != inboxinputList[i]) {
                    if (objRef.checked) {
                        inboxinputList[i].checked = true;
                    }
                    else {
                        inboxinputList[i].checked = false;
                    }
                }
            }
        }

        function checkAllPending(objPendingRef) {
            var pendingGridView = objPendingRef.parentNode.parentNode.parentNode.parentNode;
            var pendinginputList = pendingGridView.getElementsByTagName("input");

            for (var i = 0; i < pendinginputList.length; i++) {
                var pendingrow = pendinginputList[i].parentNode.parentNode;
                if (pendinginputList[i].type == "checkbox" && objPendingRef != pendinginputList[i]) {
                    if (objPendingRef.checked) {
                        pendinginputList[i].checked = true;
                    }
                    else {
                        pendinginputList[i].checked = false;
                    }
                }
            }
        }

        function Check_ClickPending(objPendingRef) {
            var pendingrow = objPendingRef.parentNode.parentNode.parentNode;
            var pendingGridView = pendingrow.parentNode;
            var pendinginputList = pendingGridView.getElementsByTagName("input");

            for (var i = 0; i < pendinginputList.length; i++) {
                var pendingheaderCheckBox = pendinginputList[0];
                var checked = true;
                if (pendinginputList[i].type == "checkbox" && pendinginputList[i] != pendingheaderCheckBox) {
                    if (!pendinginputList[i].checked) {
                        checked = false;
                        break;
                    }
                }
            }
            pendingheaderCheckBox.checked = checked;
        }

        function autoCompleteEx_FolderItemSelected(sender, e) {
            var folderId = $get('<%= hndFolderId.ClientID %>');
            folderId.value = e.get_value();
            __doPostBack(sender.get_element().name, 'RefreshTabs');
        }

</script>

<asp:HiddenField ID="hdnInboxIDs" runat="server" />
<asp:HiddenField ID="hdnPendingIDs" runat="server" />
<asp:HiddenField ID="hdnTreeId" runat="server" />
<asp:HiddenField ID="hdnPath" runat="server" />
<asp:HiddenField ID="hdnSelectedFolderID" runat="server" />
<asp:HiddenField ID="hdnSelectedDividerID" runat="server" />
<asp:HiddenField ID="hdnSelectedFolderText" runat="server" />
<asp:HiddenField ID="hdnPreviewInboxID" runat="server" />
<asp:HiddenField ID="hdnPreviewShareID" runat="server" />

<asp:Button ID="TargetButton" runat="server" Style="display: none" />

<div id="overlay">
    <div id="floatingCirclesG">
        <div class="f_circleG" id="frotateG_01"></div>
        <div class="f_circleG" id="frotateG_02"></div>
        <div class="f_circleG" id="frotateG_03"></div>
        <div class="f_circleG" id="frotateG_04"></div>
        <div class="f_circleG" id="frotateG_05"></div>
        <div class="f_circleG" id="frotateG_06"></div>
        <div class="f_circleG" id="frotateG_07"></div>
        <div class="f_circleG" id="frotateG_08"></div>
    </div>
</div>
<div class="popup-mainbox preview-popup" id="preview_popup" style="display: none">
    <div class="popupcontrols" style="width: 100%; height: 5%;">
        <span id="popupclose" class="ic-icon ic-close popupclose"></span>
        <asp:Button ID="btnPreviewAddTotab" Text="Tab" CssClass="btn green btnAddtoTab" runat="server" Style="margin-left: 650px; margin-top: 8px;" ToolTip="Add To Tab"></asp:Button>
        <asp:Button ID="btnPreviewWA" runat="server" CssClass="btn green btnAddtoWorkArea" Text="WorkArea" Style="float: right; margin-top: 8px;" ToolTip="Add To WorkArea" />
    </div>
    <div class="popupcontent" style="width: 100%; height: 95%;">
        <iframe id="reviewContent" style="width: 100%; height: 100%;"></iframe>
    </div>
</div>

<div class="popup-mainbox preview-popup" id="WorkArea_popup" style="display: none">
    <div class="popupcontrols popup-head" style="width: 100%; height: 60px;">
        WorkArea
        <span id="Span1" class="ic-icon ic-close popupclose"></span>
    </div>
    <div class="popupcontent" style="width: 100%; height: 95%;">
        <div class="popup-scroller">
            <p style="margin: 20px">
                <uc1:ucWorkArea ID="ucWorkArea" runat="server" />
            </p>

        </div>
        <div class="popup-btn">
            <%--<input  runat="server"  type="button" value="Save"  />--%>
            <asp:Button ID="btnSelectWA" Text="Save" runat="server" CssClass="btn green" OnClick="btnSelectWA_Click" Style="margin-right: 5px;" />
            <input id="btnCancelWA" type="button" value="Cancel" class="btn green" />
        </div>
    </div>
</div>

<%--<div class="popup-mainbox preview-popup" id="AddToTab_popup" style="display: none; height:53%; width:23%; max-width:520px;">--%>
<div class="popup-mainbox preview-popup" id="AddToTab_popup" style="display: none; height: 460px; width: 395px; max-width: 395px; max-height: 460px; margin-top: 20px;">
    <div class="popupcontrols popup-head" style="width: 100%; height: 60px;">
        Select Folder
        <span id="Span2" class="ic-icon ic-close popupclose"></span>
    </div>
    <div class="popupcontent" style="width: 100%; height: 95%; background: #eaeaea; border-top: 2px solid darkgray;">
        <div class="popup-scroller" style="height: 75%; background: #eaeaea;">
            <p style="margin: 20px">
                <uc1:ucAddToTab ID="ucAddToTab" runat="server" />
            </p>

        </div>
        <div class="popup-btn" style="background: #eaeaea;">
            <%--<input  runat="server"  type="button" value="Save"  />--%>
            <asp:Button ID="btnSelectAddToTab" Text="Save" runat="server" CssClass="btn green" OnClick="btnSelectAddToTab_Click" Style="margin-right: 5px;" />
            <input id="btnCancelAddToTab" type="button" value="Cancel" class="btn green" />
        </div>
    </div>
</div>

<div class="page-container register-container" style="padding-top: 0; padding-bottom: 0px;">
    <div class="inner-wrapper">
        <div class="form-containt listing-contant">

            <div class="left-content" style="width: 100%">

                <asp:Panel ID="panelMain" runat="server" Visible="true">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div style="clear: both"></div>
                            <%--Inbox--%>
                            <div class="content-title boxtitle" style="display: inline; margin-bottom: 0px; font-size: 20px; vertical-align: middle; font-weight: normal;">Inbox & Pending Docs</div>
                            <div style="float: right; margin-bottom: 15px">
                                <asp:Button ID="btnAddtoTab" Text="Tab" CssClass="btn green btnAddtoTab" runat="server" ToolTip="Add To Tab"></asp:Button>
                                <asp:Button ID="btnAddtoWorkArea" runat="server" CssClass="btn green btnAddtoWorkArea" Text="WorkArea" ToolTip="Add To WorkArea" />
                                <asp:Button ID="lnkBtnShare" runat="server" CssClass="btn green shareDocument btnShare" CausesValidation="False" ToolTip="Share URL to unregistered user to upload file" OnClientClick="OnShare('Inbox')" Style="margin-right: 5px;"></asp:Button>
                                <div class="prevnext" style="float: right" bgcolor="#f8f8f5">
                                    <cc1:WebPager ID="WebPagerCombine" runat="server" OnPageIndexChanged="WebPagerCombine_PageIndexChanged"
                                        PageSize="5" CssClass="prevnext" PagerStyle="NumericPages" ControlToPaginate="grdRequestedFiles" Style="height: 30px;"></cc1:WebPager>
                                </div>
                            </div>
                            <div style="width: 100%; border-collapse: collapse; border-width: 0px 0px 1px; padding-top: 22px;"></div>
                            <div class="content-box listing-view">
                                <fieldset id="inboxFieldset">
                                    <%-- <div class="form_title_2">
                                        <asp:Image runat="server"
                                            ImageUrl="../Images/loader_bg.gif" ID="lbsStatus" ImageAlign="Middle" />
                                    </div>--%>
                                    <div class='clearfix'></div>


                                    <%--<asp:HiddenField ID="hndFolderIdforInbox" runat="server" />--%>
                                    <asp:HiddenField ID="hndFileNameforInbox" runat="server" />
                                    <asp:GridView ID="grdRequestedFiles" runat="server" AllowSorting="True"
                                        AutoGenerateColumns="false" BorderWidth="2" OnRowDataBound="grdRequestedFiles_RowDataBound"
                                        CssClass="datatable listing-datatable" Width="100%" OnSorting="grdRequestedFiles_Sorting">
                                        <PagerSettings Visible="False" />
                                        <AlternatingRowStyle CssClass="odd" />
                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="3%"
                                                ItemStyle-CssClass="table_tekst_edit">
                                                <HeaderTemplate>
                                                    <asp:CheckBox Text="" ID="chkSelectAll" runat="server" onclick="checkAll(this);" CssClass="chkpendingAll chkinboxAll" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList chkinbox" onclick="Check_Click(this);" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%--convert label into textbox--%>
                                            <%--<asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="50%" SortExpression="InboxId">--%>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="35%" SortExpression="FileName">
                                                <HeaderTemplate>
                                                    <%-- <asp:Label ID="lblInboxfileName" runat="server" Text="Name" />--%>
                                                    <asp:LinkButton ID="lblfileName" runat="server" Text="Name" CommandName="Sort" CommandArgument="FileName" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblfileName" runat="server" Text='<%#Eval("FileName")%>' name="lblfileName" CssClass="lblfileName" />
                                                    <asp:TextBox runat="server" MaxLength="50" Text='<%#Eval("FileName")%>'
                                                        ID="txtFileName" Style="display: none; height: 30px;" name="txtFileName" CssClass="txtFileName" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="CreatedDate" HeaderText="Received Date" DataFormatString="{0:M-dd-yyyy HH:mm:ss}" ItemStyle-Width="15%"
                                                SortExpression="CreatedDate" />
                                            <asp:BoundField DataField="Size" HeaderText="Size" DataFormatString="{0:F4}" ItemStyle-Width="9%"
                                                HtmlEncode="false" SortExpression="Size" />

                                            <asp:BoundField DataField="Type" HeaderText="Type" ItemStyle-Width="8%" SortExpression="Type" />

                                            <asp:TemplateField HeaderText="Submitted By" ItemStyle-Width="18%" SortExpression="ToEmail">
                                                <ItemTemplate>
                                                    <%--<asp:Label ID="lblSubmittedBy" runat="server" Text='<%# Eval("ToEmail") %>'></asp:Label>--%>
                                                    <asp:LinkButton ID="lblSubmittedBy" runat="server" Text='<%# Eval("ToEmail") %>' CommandName="Sort" CommandArgument="ToEmail" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="12%"
                                                ItemStyle-CssClass="table_tekst_edit actionMinWidth">
                                                <HeaderTemplate>
                                                    <asp:Label ID="LabelAction" runat="server" Text="Action" />
                                                </HeaderTemplate>
                                                <ItemTemplate>

                                                    <%--Edit--%>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text=""
                                                        CssClass="ic-icon ic-edit editInboxfile" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                    <%--<asp:HiddenField ID="hdnIDs" runat="server" Value='<%#  GetInboxId(Convert.ToInt32(Eval("InboxId"))) %>' />--%>
                                                    <asp:HiddenField ID="hdnInboxValues" runat="server" Value='<%#  GetInboxId(string.IsNullOrEmpty(Eval("InboxId").ToString()) ? 0 : Convert.ToInt32(Eval("InboxId"))) %>' />
                                                    <%--<asp:HiddenField ID="hdnPendingValues" runat="server" Value='<%#  GetPendingID(Eval("eFileFlowShareId").ToString()) %>' />--%>
                                                    <asp:HiddenField ID="hdnPendingValues" runat="server" Value='<%# Eval("eFileFlowShareId").ToString() %>' />
                                                    <asp:HiddenField ID="hdnTypeValues" runat="server" Value='<%#  Eval("Type") %>' />

                                                    <asp:LinkButton ID="lbkUpdate" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                        ToolTip="Update" CssClass="ic-icon ic-save saveInboxfile" Style="display: none" name="lbkUpdate"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkCancel" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                        OnClientClick="cancleInboxFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancelInboxfile" Style="display: none" name="lnkCancel"></asp:LinkButton>
                                                    <%--Edit End--%>

                                                    <%--Delete--%>
                                                    <%--<asp:LinkButton ID="lnkBtnInboxDelete" runat="server" CssClass="ic-icon ic-delete" OnCommand="lnkBtnInboxDelete_Click" CommandArgument='<%#string.Format("{0},{1}",Eval("FilePath"),Eval("InboxId")) %>' OnClientClick="GetInboxFileID()" OnClick="<%# Eval("inboxFileID") %>"></asp:LinkButton>--%>
                                                    <asp:LinkButton ID="lnkBtnDelete" runat="server" CssClass="ic-icon ic-delete deleteInboxfile" OnCommand="lnkBtnDelete_Command" CommandArgument='<%#string.Format("{0},{1},{2}",Eval("InboxId"),Eval("eFileFlowShareId"),Eval("Type")) %>'></asp:LinkButton>


                                                    <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="lnkBtnDelete"
                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                    </ajaxToolkit:ModalPopupExtender>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="cbeDelFile" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                        ConfirmText="" TargetControlID="lnkBtnDelete">
                                                    </ajaxToolkit:ConfirmButtonExtender>
                                                    <%--Delete End--%>

                                                    <asp:LinkButton ID="lnkbtnPreview" runat="server" CssClass="ic-icon ic-preview lnkbtnPreview" CausesValidation="False"
                                                        CommandName=""></asp:LinkButton>
                                                    <asp:HyperLink ID="lnkBtnDownload" runat="server" CausesValidation="false" CssClass="ic-icon ic-download downloadFile lnkBtnDownload" Target="_blank" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <%-- <asp:Panel ID="PanelInfor" runat="server" Visible="false">
                                                <div class="tekstDef" style="">
                                                    <table border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                        <tr>
                                                            <td align="left" class="podnaslov">
                                                                <asp:Label ID="Label120" runat="server" />There are no files. right now.
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </asp:Panel>--%> No Records found
                                        </EmptyDataTemplate>
                                    </asp:GridView>

                                    <br />

                                </fieldset>
                            </div>


                            <%--End Inbox--%>

                            <div style="clear: both"></div>
                            <div class="content-title boxtitle" style="display: none">FileFolders</div>
                            <div class="content-box listing-view" style="display: none">
                                <fieldset id="folderSelFieldset">
                                    1. Select an edFile
                                        <div class='clearfix'></div>
                                    <asp:HiddenField ID="hndFolderId" runat="server" />
                                    <asp:TextBox ID="FileFolderTextBoxId" runat="server" class="find-input" MaxLength="50" autocomplete="off" placeholder="Enter Folder Name" AutoPostBack="false" />
                                    <ajaxToolkit:AutoCompleteExtender
                                        runat="server" UseContextKey="true"
                                        BehaviorID="AutoCompleteEx2"
                                        ID="autoComplete1"
                                        TargetControlID="FileFolderTextBoxId" ServicePath="~/FlexWebService.asmx"
                                        ServiceMethod="GetCompletionFolderList"
                                        MinimumPrefixLength="1"
                                        CompletionInterval="1000"
                                        EnableCaching="true"
                                        CompletionSetCount="20"
                                        CompletionListCssClass="autocomplete_completionListElement"
                                        CompletionListItemCssClass="autocomplete_listItem"
                                        CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                        DelimiterCharacters="; :"
                                        ShowOnlyCurrentWordInCompletionListItem="true" OnClientItemSelected="autoCompleteEx_FolderItemSelected">
                                        <Animations>
                                                <OnShow>
                                                    <Sequence>
                                                        <OpacityAction Opacity="0" />
                                                        <HideAction Visible="true" />
                                                        <ScriptAction Script="
                                                            // Cache the size and setup the initial size
                                                            var behavior = $find('AutoCompleteEx2');
                                                            if (!behavior._height) {
                                                                var target = behavior.get_completionList();
                                                                behavior._height = target.offsetHeight - 2;
                                                                target.style.height = '0px';
                                                            }" />
                                                        <Parallel Duration=".4">
                                                            <FadeIn />
                                                            <Length PropertyKey="height" StartValue="0" EndValueScript="$find('AutoCompleteEx2')._height" />
                                                        </Parallel>
                                                    </Sequence>
                                                </OnShow>
                                                <OnHide>
                                                    <Parallel Duration=".4">
                                                        <FadeOut />
                                                        <Length PropertyKey="height" StartValueScript="$find('AutoCompleteEx2')._height" EndValue="0" />
                                                    </Parallel>
                                                </OnHide>
                                        </Animations>
                                    </ajaxToolkit:AutoCompleteExtender>
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                </fieldset>

                                <asp:HiddenField ID="HiddenField2" runat="server" />
                                <asp:HiddenField ID="HiddenField3" runat="server" />

                                <%--<div class="content-box listing-view" style="margin-top: 30px; padding-top: 20px">--%>
                                <fieldset>
                                    2. Select a tab for the selected edFile
                                        <div class='clearfix'></div>
                                    <asp:ListBox ID="lstBoxTab" Width="100%" Height="150" DataValueField="DividerID" DataTextField="Name"
                                        AutoPostBack="true" OnSelectedIndexChanged="lstBoxTab_OnSelectedIndexChanged"
                                        runat="server"></asp:ListBox>
                                </fieldset>
                            </div>

                            <%--Delete File--%>
                            <asp:Panel class="popup-mainbox" ID="DivDeleteConfirmation" Style="display: none"
                                runat="server">
                                <div>
                                    <div class="popup-head" id="PopupHeader">
                                        Delete File
                                    </div>
                                    <div class="popup-scroller">
                                        <p style="margin: 20px">
                                            Caution! Are you sure you want to delete this file?
                                        </p>
                                        <p style="margin: 20px">
                                            Confirm Password:
                                        <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                                        </p>
                                    </div>
                                    <div class="popup-btn">
                                        <input id="ButtonDeleleOkay" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />
                                        <input id="ButtonDeleteCancel" type="button" value="No" class="btn green" />
                                    </div>
                                </div>
                            </asp:Panel>

                            <asp:Panel class="popup-mainbox" ID="DivInboxDeleteConfirmation" Style="display: none"
                                runat="server">
                                <div>
                                    <div class="popup-head" id="Div1">
                                        Delete File
                                    </div>
                                    <div class="popup-scroller">
                                        <p style="margin: 20px">
                                            Caution! Are you sure you want to delete this file?
                                        </p>
                                        <p style="margin: 20px">
                                            Confirm Password:
                                        <asp:TextBox ID="TextBox1" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                                        </p>
                                    </div>
                                    <div class="popup-btn">
                                        <input id="ButtonInboxDeleleOkay" type="button" value="Yes" class="btn green" />
                                        <input id="ButtonInboxDeleteCancel" type="button" value="No" class="btn green" />

                                    </div>
                                </div>
                            </asp:Panel>
                            <%--end Delete--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
                <%--Share Document--%>
                <div id="dialog-share-book" title="Share this Inbox Document" style="display: none">
                    <p class="validateTips">All form fields are required.</p>
                    <br />
                    <%--  <br />--%>
                    <%--<label for="email" style="display:none;">My Email</label><br />
                    <input type="email" name="email" id="email" class="text ui-widget-content ui-corner-all" style="border: 1px solid black; display:none;" /><br />
                    <label for="myname" style="display:none;">My Name</label><br />
                    <input type="text" name="myname" id="myname" class="text ui-widget-content ui-corner-all" style="border: 1px solid black; display:none;" /><br />--%>
                    <label for="emailto">To Email</label><br />
                    <input type="email" name="emailto" id="emailto" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
                    <label for="nameto">To Name</label><br />
                    <input type="text" name="nameto" id="nameto" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
                    <%--<label for="title" style="display:none;">Title</label><br />
                    <input type="text" name="title" id="title" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black;display:none;" /><br />--%>
                    <label for="content">Message</label><br />
                    <textarea name="content" id="content" rows="2" cols="25" class="text ui-widget-content ui-corner-all" style="border: 1px solid black"></textarea><br />
                </div>
                <div id="dialog-message-all" title="" style="display: none;">
                    <p id="dialog-message-content">
                        <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span>
                        <span id="spanText"></span>
                    </p>
                </div>
                <%--End Share Document--%>
            </div>
        </div>
    </div>
</div>
<a class='iframe' href="javascript:;" style="display: none;">popup</a>