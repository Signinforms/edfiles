﻿using Shinetech.DAL;
using Shinetech.Framework.Controls;
using Shinetech.Utility;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Security;

public partial class Office_UserControls_ucRequestFiles : System.Web.UI.UserControl
{
    public string FolderId;
    public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    public string virtualDir = ConfigurationManager.AppSettings["VirtualDir"];
    public string newFileName = string.Empty;
    public string SortType = "DESC";
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Sessions.HasViewPrivilegeOnly)
        {
            btnAddtoTab.Visible = false;
            btnAddtoWorkArea.Visible = false;
            lnkBtnShare.Visible = false;
        }
        if (!Page.IsPostBack)
        {
            var argument = Request.Form["__EVENTARGUMENT"];
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
            {
                GetData();
                BindGrid();
            }
            argument = "";
            if (!Page.IsPostBack)
            {
                autoComplete1.ContextKey = Sessions.SwitchedSessionId;
                string uid = Sessions.SwitchedSessionId;
                GetData();
                BindGrid();
            }
        }
        else
        {

            var argument = Request.Form["__EVENTARGUMENT"];
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
            {
                GetData();
                BindGrid();
            }
            if (Request.Params.Get("__EVENTARGUMENT") != null && Convert.ToString(Request.Params.Get("__EVENTARGUMENT")) == "RefreshTabs")
            {
                DataBindDividers();
            }

        }
    }
    public string DividerID
    {
        get
        {
            return (string)this.Session["DividerID"];
        }
        set
        {
            Session["DividerID"] = value;
        }
    }
    public DataTable DataSource
    {
        get
        {
            return this.Session["Files_Workarea"] as DataTable;
        }
        set
        {
            this.Session["Files_Workarea"] = value;
        }
    }

    public DataTable DataSourcePending
    {
        get
        {
            return this.Session["Files_Workarea_pending"] as DataTable;
        }
        set
        {
            this.Session["Files_Workarea_pending"] = value;
        }
    }

    public DataTable DataSourceCombine
    {
        get
        {
            return this.Session["Files_Welcome"] as DataTable;
        }
        set
        {
            this.Session["Files_Welcome"] = value;
        }
    }

    public string DateTimeInfo
    {
        get
        {
            DateTimeFormatInfo myDTFI = new CultureInfo("en-US", false).DateTimeFormat;
            DateTime dt = DateTime.Now;
            return dt.ToString("MM/dd/yyy", myDTFI);

        }
    }
    public string EffID
    {
        get
        {
            return (string)Session["EFFID"];
        }
        set
        {
            Session["EFFID"] = value;

        }
    }

    public string FolderName
    {
        get
        {
            return string.Format("~/{0}", CurrentVolume);
        }

    }

    public string PathUrl
    {
        get { return CurrentVolume; }
    }

    #region FileFolder
    private void BindEFileFolders()
    {
        DataTable table = GetEFileFolders();

        if (!string.IsNullOrEmpty(EffID))
        {
            string folderId = EffID;
            hndFolderId.Value = folderId;
        }
        else
        {
            hndFolderId.Value = "0";

        }

        if (!string.IsNullOrEmpty(Page.Request.QueryString["FolderId"]))
        {
            string folderId = Page.Request.QueryString["FolderId"];
            hndFolderId.Value = folderId;
        }
    }

    public DataTable GetEFileFolders()
    {
        string uid = Sessions.SwitchedSessionId;
        //string uid = Sessions.UserId;
        Shinetech.DAL.Account account = new Shinetech.DAL.Account(uid);
        string officeId;
        DataTable effTable = null;
        if (account.IsExist)
        {
            if (account.IsSubUser.Value.Equals("1"))
            {
                officeId = account.OfficeUID.ToString();
            }
            else
            {
                officeId = uid;
            }

            effTable = FileFolderManagement.GetFileFoldersWithDisplayName(uid, officeId);
        }

        return effTable;
    }
    public DataTable GetDividersByEFF(int eff)
    {
        DataTable dividerTable = FileFolderManagement.GetDividers(eff);
        return dividerTable;
    }
    protected void ComboBoxFolder_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataBindDividers();
    }

    public void DataBindDividers()
    {
        if (FileFolderTextBoxId.Text.Trim() == "")
        {
            return;
        }
        int effid = Convert.ToInt32(hndFolderId.Value);
        this.EffID = hndFolderId.Value;

        lstBoxTab.DataSource = GetDividersByEFF(effid).DefaultView;
        lstBoxTab.DataBind();

        if (lstBoxTab.Items.Count > 0) lstBoxTab.SelectedIndex = 0;
        this.DividerID = lstBoxTab.SelectedValue;
    }
    protected void lstBoxTab_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        this.DividerID = lstBoxTab.SelectedValue;
    }

    public string GetFileName(string fileName)
    {
        string encodeName = HttpUtility.UrlPathEncode(fileName);
        return encodeName.Replace("%", "$");
    }
    #endregion

    #region PendingGrid
    protected void WebPagerPending_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        //WebPagerPending.CurrentPageIndex = e.NewPageIndex;
        //WebPagerPending.DataSource = this.DataSourcePending;
        //WebPagerPending.DataBind();
    }

    protected void GrdPending_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            if (Sessions.HasViewPrivilegeOnly)
            {
                e.Row.Cells[0].Visible = false;
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (Sessions.HasViewPrivilegeOnly)
            {
                e.Row.Cells[0].Visible = false;
            }
            System.Data.DataRowView drvPending = e.Row.DataItem as DataRowView;
            string eFileFlowShareId = drvPending.Row[0].ToString();
            string fileName = drvPending.Row[2].ToString() + drvPending.Row[11].ToString();
            string ext = Path.GetExtension(fileName);
            if (!fileName.ToLower().Contains(".pdf"))
            {
                LinkButton lnkbtnPreviewPending = e.Row.FindControl("lnkbtnPreviewPending") as LinkButton;
                lnkbtnPreviewPending.Enabled = false;
                lnkbtnPreviewPending.OnClientClick = null;
                lnkbtnPreviewPending.Attributes.Add("style", "opacity:0.5");
            }
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            string path = rackSpaceFileUpload.GeneratePath(fileName.ToLower(), Enum_Tatva.Folders.EFileShare, eFileFlowShareId);
            path = path.Replace(@"\", "/");
            TextBox txtPendingDocumentName = (e.Row.FindControl("txtPendingFileName") as TextBox);

            LinkButton lnkPendingUpdate = (e.Row.FindControl("lnkPendingUpdate") as LinkButton);
            if (lnkPendingUpdate != null)
            {
                lnkPendingUpdate.Attributes.Add("onclick", "OnSaveRenameForPending('" + txtPendingDocumentName.ClientID + "','" + path + "','" + eFileFlowShareId + "','" + ext + "');");
            }

            HyperLink lnkBtnPendingDownload = (e.Row.FindControl("lnkBtnPendingDownload") as HyperLink);
            if (lnkBtnPendingDownload != null)
            {
                path = rackSpaceFileUpload.GeneratePath(fileName.ToLower(), Enum_Tatva.Folders.EFileShare, eFileFlowShareId);
                lnkBtnPendingDownload.Attributes.Add("href", "PdfViewer.aspx?data=" + QueryString.QueryStringEncode("ID=" + path + "&folderID=" + (int)Enum_Tatva.Folders.EFileShare + "&sessionID=" + Sessions.SwitchedRackspaceId));
            }
        }
    }

    #endregion


    #region InboxGrid
    protected void grdInbox_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
            string inboxId = drv.Row[0].ToString();
            string fileName = drv.Row[1].ToString();
            string ext = Path.GetExtension(fileName);
            if (!fileName.ToLower().Contains(".pdf"))
            {
                LinkButton lnkbtnPreviewInbox = e.Row.FindControl("lnkbtnPreviewInbox") as LinkButton;
                lnkbtnPreviewInbox.Enabled = false;
                lnkbtnPreviewInbox.OnClientClick = null;
                lnkbtnPreviewInbox.Attributes.Add("style", "opacity:0.5");
            }
            else
            {
                LinkButton lnkbtnPreviewInbox = (e.Row.FindControl("lnkbtnPreviewInbox") as LinkButton);
                if (lnkbtnPreviewInbox != null)
                {
                    lnkbtnPreviewInbox.Attributes.Add("onclick", "ShowPreviewForInbox(" + int.Parse(inboxId) + ");");
                }
            }
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            //string path = rackSpaceFileUpload.GeneratePath(fileName + "." + "pdf", Enum_Tatva.Folders.Inbox, inboxId);
            string path = rackSpaceFileUpload.GeneratePath(fileName.ToLower(), Enum_Tatva.Folders.inbox, inboxId);
            path = path.Replace(@"\", "/");
            TextBox txtDocumentName = (e.Row.FindControl("txtInboxfileName") as TextBox);

            LinkButton lbkinboxUpdate = (e.Row.FindControl("lbkinboxUpdate") as LinkButton);
            if (lbkinboxUpdate != null)
            {
                lbkinboxUpdate.Attributes.Add("onclick", "OnSaveRenameForInbox('" + txtDocumentName.ClientID + "','" + path + "','" + inboxId + "','" + ext + "');");
            }

            HyperLink lnkBtnInboxDownload = (e.Row.FindControl("lnkBtnInboxDownload") as HyperLink);
            if (lnkBtnInboxDownload != null)
            {
                lnkBtnInboxDownload.Attributes.Add("href", "PdfViewer.aspx?data=" + QueryString.QueryStringEncode("ID=" + fileName + "&folderID=" + (int)Enum_Tatva.Folders.inbox + "&sessionID=" + Sessions.SwitchedRackspaceId));
            }

        }
    }

    private void BindGrid()
    {
        grdRequestedFiles.DataSource = this.DataSourceCombine.AsDataView();
        grdRequestedFiles.DataBind();
        WebPagerCombine.DataSource = this.DataSourceCombine;
        WebPagerCombine.DataBind();
        UpdatePanel1.Update();
    }

    private void GetData()
    {
        string uid = Sessions.SwitchedSessionId;
        string searchkey = string.Empty;
        if (string.IsNullOrEmpty(Sessions.SwitchedRackspaceId) && !string.IsNullOrEmpty(uid))
        {
            Shinetech.DAL.Account acc = new Shinetech.DAL.Account(uid);
            if (acc.IsSubUser.ToString() == "1")
                Sessions.SwitchedRackspaceId = acc.OfficeUID.ToString();
            else
                Sessions.SwitchedRackspaceId = uid;
        }
        this.DataSource = General_Class.GetInboxDocuments(Sessions.SwitchedRackspaceId);
        System.Data.DataColumn newColumn = new System.Data.DataColumn("Type", typeof(System.String));
        newColumn.DefaultValue = "Inbox";
        this.DataSource.Columns.Add(newColumn);
        DataView dvSource = this.DataSource.AsDataView();
        dvSource.Sort = "InboxId DESC";
        this.DataSource = dvSource.ToTable();

        DataTable dspendingdata = new DataTable();
        dspendingdata = General_Class.eFileFlowShareGetData(Sessions.SwitchedRackspaceId);
        dspendingdata.Columns.Add("FileExtention", typeof(string));
        if (dspendingdata != null && dspendingdata.Rows.Count > 0)
        {
            foreach (DataRow row in dspendingdata.Rows)
            {
                string fileName = row["FileName"].ToString();
                string ext = Path.GetExtension(fileName);
                fileName = Path.GetFileNameWithoutExtension(fileName);
                row["FileName"] = fileName + ext;
                row["FileExtention"] = ext;
            }
        }
        this.DataSourcePending = dspendingdata;
        DataView dvPendingSource = this.DataSourcePending.AsDataView();
        dvPendingSource.Sort = "eFileFlowShareId DESC";
        this.DataSourcePending = dvPendingSource.ToTable();
        System.Data.DataColumn newColumnPending = new System.Data.DataColumn("Type", typeof(System.String));
        newColumnPending.DefaultValue = "Pending";
        this.DataSourcePending.Columns.Add(newColumnPending);

        this.DataSourceCombine = new DataTable();
        this.DataSourceCombine = this.DataSource.Copy();
        this.DataSourceCombine.Merge(this.DataSourcePending);

        DataView dvSourceRequestedFiles = this.DataSourceCombine.AsDataView();
        dvSourceRequestedFiles.Sort = "CreatedDate DESC";

        this.DataSourceCombine = dvSourceRequestedFiles.ToTable();

        ViewState["SortExpression"] = "CreatedDate";
        ViewState["SortDirection"] = SortType;
    }

    protected void lbkinboxUpdate_Command(object sender, CommandEventArgs e)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "showLoader", "showLoader();", true);
        try
        {
            string password = textPassword.Text.Trim();
            string strPassword = PasswordGenerator.GetMD5(password);

            if (UserManagement.GetPassword(this.Page.User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
            {
                //show the password is wrong
                ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssYes", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);
            }
            else
            {
                string id = e.CommandArgument.ToString();
                DataTable dtFiles = General_Class.GetInboxFilesFrominboxIds(id);
                dtFiles.AsEnumerable().ToList().ForEach(d =>
                {
                    var file = d.Field<string>("FileName");
                    int inboxId = d.Field<int>("InboxId");

                    RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                    string objectPath = rackSpaceFileUpload.GeneratePath(file.ToLower(), Enum_Tatva.Folders.inbox);


                    General_Class.UpdateInbox(file, inboxId, 0, 0, Enum_Tatva.IsDelete.Yes.GetHashCode(), 0, 0);

                    GetData();
                    BindGrid();
                });
            }
        }
        catch (Exception ex)
        { }
        ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
    }
    #endregion

    public string GetInboxId(int InboxId = 0)
    {
        return "{\"InboxId\":" + Convert.ToString(InboxId) + "}";
    }

    protected void GrdPending_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (this.DataSourcePending != null)
        {
            //Sort the data.
            this.DataSourcePending.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression, "SortPendingExpression", "SortPendingDirection");
            BindGrid();
        }
    }
    protected void grdInbox_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (this.DataSource != null)
        {
            //Sort the data.
            this.DataSource.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression, "SortInboxExpression", "SortInboxDirection");
            BindGrid();
        }
    }

    private string GetSortDirection(string column, string sortExpressionID, string sortDirectionID)
    {

        // Retrieve the last column that was sorted.
        string sortExpression = ViewState[sortExpressionID] as string;

        if (sortExpression != null)
        {
            // Check if the same column is being sorted.
            // Otherwise, the default value can be returned.
            if (sortExpression == column)
            {
                string lastDirection = ViewState[sortDirectionID] as string;
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    SortType = "DESC";
                }
                else if ((lastDirection != null) && (lastDirection == "DESC"))
                {
                    SortType = "ASC";
                }
            }
        }

        // Save new values in ViewState.
        ViewState[sortDirectionID] = SortType;
        ViewState[sortExpressionID] = column;

        return SortType;
    }

    protected void lnkBtnPendingDownload_Click(object sender, EventArgs e)
    {
        try
        {
            string[] args = (sender as LinkButton).CommandArgument.Split(',');
            string fileName = args[1];
            string shareID = args[0];
            string extension = args[2];
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            string path = rackSpaceFileUpload.GeneratePath(fileName + extension.ToLower(), Enum_Tatva.Folders.EFileShare, shareID);
            HttpContext.Current.Response.Redirect("PdfViewer.aspx?data=" + QueryString.QueryStringEncode("ID=" + path + "&folderID=" + (int)Enum_Tatva.Folders.EFileShare + "&sessionID=" + Sessions.SwitchedRackspaceId));
        }
        catch (Exception ex)
        { }
    }
    protected void grdRequestedFiles_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            if (Sessions.HasViewPrivilegeOnly)
            {
                e.Row.Cells[0].Visible = false;
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (Sessions.HasViewPrivilegeOnly)
            {
                e.Row.Cells[0].Visible = false;
                HyperLink libDownload = e.Row.FindControl("lnkBtnDownload") as HyperLink;
                libDownload.Visible = false;
                LinkButton libDel = e.Row.FindControl("lnkBtnDelete") as LinkButton;
                libDel.Visible = false;
                LinkButton libEdit = e.Row.FindControl("lnkEdit") as LinkButton;
                libEdit.Visible = false;
            }
            System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
            string inboxId = drv.Row[0].ToString();
            string eFileFlowShareId = drv.Row[11].ToString();
            string fileName = string.Empty;
            string type = drv.Row[10].ToString();
            int folderID = 0;
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            string path = string.Empty;

            if (type == "Inbox")
            {
                folderID = (int)Enum_Tatva.Folders.inbox;
                fileName = drv.Row[1].ToString();
                path = rackSpaceFileUpload.GeneratePath(fileName.ToLower(), Enum_Tatva.Folders.inbox);
            }
            else
            {
                folderID = (int)Enum_Tatva.Folders.EFileShare;
                fileName = drv.Row[1].ToString();
                path = rackSpaceFileUpload.GeneratePath(fileName.ToLower(), Enum_Tatva.Folders.EFileShare, eFileFlowShareId);
            }
            string ext = Path.GetExtension(fileName);

            if (!fileName.ToLower().Contains(".pdf"))
            {
                LinkButton lnkbtnPreview = e.Row.FindControl("lnkbtnPreview") as LinkButton;
                if (lnkbtnPreview != null)
                {
                    lnkbtnPreview.Enabled = false;
                    lnkbtnPreview.OnClientClick = null;
                    lnkbtnPreview.Attributes.Add("style", "opacity:0.5");
                }
            }
            else
            {
                LinkButton lnkbtnPreview = (e.Row.FindControl("lnkbtnPreview") as LinkButton);
                if (lnkbtnPreview != null)
                {
                    lnkbtnPreview.Attributes.Add("onclick", "ShowPreviewForRequestedFiles('" + (string.IsNullOrEmpty(inboxId) ? "0" : inboxId) + "','" + eFileFlowShareId + "','" + folderID + "');");
                }
            }

            path = path.Replace(@"\", "/");
            TextBox txtFileName = (e.Row.FindControl("txtFileName") as TextBox);

            LinkButton lbkUpdate = (e.Row.FindControl("lbkUpdate") as LinkButton);
            if (lbkUpdate != null)
            {
                lbkUpdate.Attributes.Add("onclick", "OnSaveRenameForRequest('" + txtFileName.ClientID + "','" + path + "','" + inboxId + "','" + eFileFlowShareId + "','" + ext + "','" + type + "');");
            }

            HyperLink lnkBtnDownload = (e.Row.FindControl("lnkBtnDownload") as HyperLink);
            if (lnkBtnDownload != null)
            {
                if (folderID == 3)
                {
                    path = rackSpaceFileUpload.GeneratePath(fileName.ToLower(), Enum_Tatva.Folders.inbox);
                    lnkBtnDownload.Attributes.Add("href", "PdfViewer.aspx?ID=" + inboxId + "&data=" + QueryString.QueryStringEncode("ID=" + path + "&folderID=" + folderID + "&sessionID=" + Sessions.SwitchedRackspaceId));
                }
                else
                {
                    path = rackSpaceFileUpload.GeneratePath(fileName.ToLower(), Enum_Tatva.Folders.EFileShare, eFileFlowShareId);
                    lnkBtnDownload.Attributes.Add("href", "PdfViewer.aspx?ID=" + eFileFlowShareId + "&data=" + QueryString.QueryStringEncode("ID=" + path + "&folderID=" + folderID + "&sessionID=" + Sessions.SwitchedRackspaceId));
                }

            }
        }

    }
    protected void grdRequestedFiles_Sorting(object sender, GridViewSortEventArgs e)
    {
        if (this.DataSourceCombine != null)
        {
            //Sort the data.
            this.DataSourceCombine.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression, "SortExpression", "SortDirection");
            BindGrid();
        }
    }
    protected void WebPagerCombine_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPagerCombine.CurrentPageIndex = e.NewPageIndex;
        WebPagerCombine.DataSource = this.DataSourceCombine;
        WebPagerCombine.DataBind();
    }

    public string GetURL(int folderID, string shareID, string sessionID, int documentID)
    {
        string filePath = string.Empty, fileName = string.Empty, physicalPath = string.Empty;
        long fileSize = 0;
        RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
        switch (folderID)
        {
            case ((int)Enum_Tatva.Folders.EFileShare):
                {
                    DataTable dataTable = General_Class.GetPendingFilesFromeShareIds(shareID);
                    dataTable.AsEnumerable().ToList().ForEach(d =>
                    {
                        fileName = d.Field<string>("FileName");
                        physicalPath = rackSpaceFileUpload.GetPath(fileName, Enum_Tatva.Folders.EFileShare, sessionID, ref fileSize, shareID);
                    });
                    break;
                }
            case ((int)Enum_Tatva.Folders.inbox):
                {
                    DataTable dataTable = General_Class.GetInboxFilesFrominboxIds(documentID.ToString());
                    dataTable.AsEnumerable().ToList().ForEach(d =>
                    {
                        fileName = d.Field<string>("FileName");
                        physicalPath = rackSpaceFileUpload.GetPath(fileName, Enum_Tatva.Folders.inbox, sessionID, ref fileSize);
                    });
                    break;
                }
        }
        return physicalPath;
    }
    protected void lnkBtnDelete_Command(object sender, CommandEventArgs e)
    {
        try
        {
            string password = textPassword.Text.Trim();
            string strPassword = PasswordGenerator.GetMD5(password);
            string errorMsg = string.Empty;
            if (UserManagement.GetPassword(this.Page.User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
            {
                //show the password is wrong
                ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssYes", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);
            }
            else
            {
                string[] arguments = e.CommandArgument.ToString().Split(',');
                string inboxID = arguments[0];
                string shareID = arguments[1];
                if (arguments[2] == "Inbox")
                {
                    DataTable dtFiles = General_Class.GetInboxFilesFrominboxIds(inboxID);
                    dtFiles.AsEnumerable().ToList().ForEach(d =>
                    {
                        var file = d.Field<string>("FileName");
                        int inboxId = d.Field<int>("InboxId");

                        RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                        rackSpaceFileUpload.DeleteObject(ref errorMsg, rackSpaceFileUpload.GeneratePath(file.ToLower(), Enum_Tatva.Folders.inbox));

                        if (string.IsNullOrEmpty(errorMsg))
                            General_Class.UpdateInbox(file, inboxId, 0, 0, Enum_Tatva.IsDelete.Yes.GetHashCode(), 0, 0);
                        else
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Failed to delete file. Please try again after sometime." + "\");", true);
                        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Delete.GetHashCode(), 0, 0, 0, 0, null, inboxId);
                    });
                }
                else
                {
                    DataTable dtFiles = General_Class.GetPendingFilesFromeShareIds(shareID);
                    dtFiles.AsEnumerable().ToList().ForEach(d =>
                    {
                        var file = d.Field<string>("FileName");
                        string eFileFlowShareId = d.Field<Guid>("eFileFlowShareId").ToString();
                        RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();

                        string objectPath = rackSpaceFileUpload.GeneratePath(eFileFlowShareId + "/" + file.ToLower(), Enum_Tatva.Folders.EFileShare);

                        rackSpaceFileUpload.DeleteObject(ref errorMsg, objectPath);

                        if (string.IsNullOrEmpty(errorMsg))
                            General_Class.UpdateShare(file, d.Field<Guid>("eFileFlowShareId"), 0, 0, Enum_Tatva.IsDelete.Yes.GetHashCode(), 0, 0);
                        else
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Failed to delete file. Please try again after sometime." + "\");", true);
                        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Delete.GetHashCode(), 0, 0, 0, 0, shareID);
                    });
                }
                GetData();
                BindGrid();
            }
        }
        catch (Exception ex)
        { }
        ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
    }
    protected void btnSelectWA_Click(object sender, EventArgs e)
    {
        try
        {
            string inboxIDs = hdnInboxIDs.Value;
            string selectedPath = hdnPath.Value;
            string pendingIDs = hdnPendingIDs.Value;
            string errorMsg = string.Empty;
            if (!string.IsNullOrEmpty(hdnPreviewInboxID.Value))
            {
                inboxIDs = hdnPreviewInboxID.Value;
            }

            if (!string.IsNullOrEmpty(hdnPreviewShareID.Value))
            {
                pendingIDs = hdnPreviewShareID.Value;
            }

            if (!string.IsNullOrEmpty(inboxIDs))
            {
                DataTable dtFiles = General_Class.GetInboxFilesFrominboxIds(inboxIDs);
                dtFiles.AsEnumerable().ToList().ForEach(d =>
                {
                    try
                    {
                        var file = d.Field<string>("FileName");
                        file = file.Replace("$20", " ");
                        file = Common_Tatva.SubStringFilename(file);
                        int inboxId = d.Field<int>("InboxId");
                        decimal size = d.Field<decimal>("Size");
                        RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();

                        string oldPath = rackSpaceFileUpload.GeneratePath(file.ToLower(), Enum_Tatva.Folders.inbox);
                        string newPath = string.Empty;

                        string filePath = string.Empty;
                        if (string.IsNullOrEmpty(selectedPath) || selectedPath.IndexOf('.') > 0)
                        {
                            newPath = rackSpaceFileUpload.GeneratePath(file.ToLower(), Enum_Tatva.Folders.WorkArea);
                        }
                        else
                            newPath = selectedPath + "/" + file.ToLower();

                        string newName = rackSpaceFileUpload.MoveObject(ref errorMsg, oldPath, newPath, Enum_Tatva.Folders.WorkArea);
                        if (string.IsNullOrEmpty(errorMsg))
                        {
                            string workAreaID = General_Class.DocumentsInsertForWorkArea(newName.Substring(newName.LastIndexOf('/') + 1), Sessions.SwitchedRackspaceId, Membership.GetUser().ProviderUserKey.ToString(), size, Enum_Tatva.IsDelete.No.GetHashCode(), newName, Enum_Tatva.WorkAreaType.Upload.GetHashCode(), string.IsNullOrEmpty(hdnTreeId.Value) ? 0 : Convert.ToInt32(hdnTreeId.Value));
                            General_Class.UpdateInbox(file, inboxId, 0, string.IsNullOrEmpty(workAreaID) ? 0 : int.Parse(workAreaID), Enum_Tatva.IsDelete.No.GetHashCode(), 0, 1);

                            string message = "File(s) have been moved to workarea folder: " + newName.Substring(0, newName.LastIndexOf('/'));
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + message + "\");", true);
                            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode(), 0, 0, 0, 0, null, inboxId);
                            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Create.GetHashCode(), 0, 0, 0, int.Parse(workAreaID));
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to move file to workarea folder." + "\");", true);
                            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, 0, 0, null, inboxId);
                        }
                    }
                    catch (Exception ex)
                    { }

                });
            }
            if (!string.IsNullOrEmpty(pendingIDs))
            {

                DataTable dtFiles = General_Class.GetPendingFilesFromeShareIds(pendingIDs);
                dtFiles.AsEnumerable().ToList().ForEach(d =>
                {
                    try
                    {
                        var file = d.Field<string>("FileName");
                        file = file.Replace("$20", " ");
                        file = Common_Tatva.SubStringFilename(file);
                        string eFileFlowShareId = d.Field<Guid>("eFileFlowShareId").ToString();
                        RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();

                        string oldPath = rackSpaceFileUpload.GeneratePath(eFileFlowShareId + "/" + file.ToLower(), Enum_Tatva.Folders.EFileShare);
                        string newPath = string.Empty;

                        string filePath = string.Empty;
                        if (string.IsNullOrEmpty(selectedPath) || selectedPath.IndexOf('.') > 0)
                        {
                            newPath = rackSpaceFileUpload.GeneratePath(file.ToLower(), Enum_Tatva.Folders.WorkArea);
                        }
                        else
                            newPath = selectedPath + "/" + file.ToLower();


                        string newFileName = rackSpaceFileUpload.MoveObject(ref errorMsg, oldPath, newPath, Enum_Tatva.Folders.WorkArea);
                        if (string.IsNullOrEmpty(errorMsg))
                        {
                            long fileSize = rackSpaceFileUpload.GetSizeOfFile(ref errorMsg, newFileName);
                            decimal size = Convert.ToDecimal((fileSize) / 1024) / 1024;
                            string workAreaID = General_Class.DocumentsInsertForWorkArea(newFileName.Substring(newFileName.LastIndexOf('/') + 1), Sessions.SwitchedRackspaceId, Membership.GetUser().ProviderUserKey.ToString(), size, Enum_Tatva.IsDelete.No.GetHashCode(), newFileName, Enum_Tatva.WorkAreaType.Upload.GetHashCode(), string.IsNullOrEmpty(hdnTreeId.Value) ? 0 : Convert.ToInt32(hdnTreeId.Value));

                            General_Class.UpdateShare(file, d.Field<Guid>("eFileFlowShareId"), 0, string.IsNullOrEmpty(workAreaID) ? 0 : int.Parse(workAreaID), Enum_Tatva.IsDelete.No.GetHashCode(), 0, 1);

                            string message = "File(s) have been moved to workarea folder: " + newFileName.Substring(0, newFileName.LastIndexOf('/'));
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + message + "\");", true);
                            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode(), 0, 0, 0, 0, eFileFlowShareId);
                            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Create.GetHashCode(), 0, 0, 0, int.Parse(workAreaID));
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to move file to workarea folder." + "\");", true);
                            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, 0, 0, eFileFlowShareId);
                        }
                    }
                    catch (Exception ex)
                    {
                        Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
                    }
                });
            }
            GetData();
            BindGrid();
        }
        catch (Exception ex)
        {
        }
        finally
        {
            ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
            hdnPath.Value = "";
        }
    }
    protected void btnSelectAddToTab_Click(object sender, EventArgs e)
    {
        try
        {
            string inboxIDs = hdnInboxIDs.Value;
            string pendingIDs = hdnPendingIDs.Value;
            int folderId = Convert.ToInt32(hdnSelectedFolderID.Value);
            int dividerId = Convert.ToInt32(hdnSelectedDividerID.Value);
            string errorMsg = string.Empty;
            if (!string.IsNullOrEmpty(hdnPreviewInboxID.Value))
            {
                inboxIDs = hdnPreviewInboxID.Value;
            }

            if (!string.IsNullOrEmpty(hdnPreviewShareID.Value))
            {
                pendingIDs = hdnPreviewShareID.Value;
            }
            if (!string.IsNullOrEmpty(pendingIDs))
            {
                DataTable dtFiles = General_Class.GetPendingFilesFromeShareIds(pendingIDs);
                dtFiles.AsEnumerable().ToList().ForEach(d =>
                {
                    try
                    {
                        var file = d.Field<string>("FileName");
                        string eFileFlowShareId = d.Field<Guid>("eFileFlowShareId").ToString();
                        RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                        string path = rackSpaceFileUpload.GeneratePath(eFileFlowShareId + "/" + file.ToLower(), Enum_Tatva.Folders.EFileShare);
                        string dir = Server.MapPath("~/" + Common_Tatva.RackSpaceWorkareaDownload + Path.DirectorySeparatorChar + Sessions.SwitchedRackspaceId);
                        if (!Directory.Exists(dir))
                        {
                            Directory.CreateDirectory(dir);
                        }
                        string fileName = rackSpaceFileUpload.GetObject(ref errorMsg, path);
                        if (string.IsNullOrEmpty(errorMsg))
                        {
                            var filePath = dir + "\\" + fileName;

                            string encodeName = HttpUtility.UrlPathEncode(file).Replace("%", "$");
                            encodeName = encodeName.Replace(encodeName, encodeName.Replace("$20", " "));
                            encodeName = Common_Tatva.SubStringFilename(encodeName);
                            encodeName = encodeName.Replace(encodeName, encodeName.Replace(" ", "$20"));
                            string vpath = Server.MapPath(string.Format("~/{0}", CurrentVolume));
                            string pathName = string.Format("~/{0}", CurrentVolume) + Path.DirectorySeparatorChar + folderId + Path.DirectorySeparatorChar + dividerId + Path.DirectorySeparatorChar + encodeName;

                            int documentOrder = FlashViewer.GetDocumentOrder(Convert.ToInt32(dividerId));
                            string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, folderId, Path.DirectorySeparatorChar, dividerId);
                            if (!Directory.Exists(dstpath))
                            {
                                Directory.CreateDirectory(dstpath);
                            }

                            dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);
                            if (File.Exists(dstpath))
                            {
                                dstpath = Common_Tatva.GetRenameFileName(dstpath);
                            }

                            File.Copy(filePath, dstpath, true);
                            string newFilename = Path.GetFileNameWithoutExtension(dstpath);
                            string displayName = HttpUtility.UrlDecode(newFilename.Replace('$', '%').ToString());
                            string documentIDOrder = Common_Tatva.InsertDocument(dividerId, pathName, documentOrder, displayName, Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()));
                            if (documentIDOrder == "0")
                            {
                                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to move file. There is a problem with the file format." + "\");", true);
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, 0, 0, eFileFlowShareId);
                            }
                            else
                            {
                                General_Class.AuditLogByDocId(Convert.ToInt32(documentIDOrder.Split('#')[0]), Sessions.RackSpaceUserID, Enum_Tatva.Action.Create.GetHashCode());
                                General_Class.UpdateShare(file, d.Field<Guid>("eFileFlowShareId"), Convert.ToInt32(documentIDOrder.Split('#')[0]), 0, Enum_Tatva.IsDelete.No.GetHashCode(), 0, 1);
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode(), 0, 0, 0, 0, eFileFlowShareId);
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(folderId), Convert.ToInt32(dividerId), Convert.ToInt32(documentIDOrder.Split('#')[0]), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.PendingUpload.GetHashCode(), 0, 0);

                                File.Delete(filePath);
                                rackSpaceFileUpload.DeleteObject(ref errorMsg, path);
                                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "File has been moved." + "\");", true);
                            }
                        }
                        else
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to get object from cloud." + "\");", true);
                    }
                    catch (Exception ex)
                    {
                        Common_Tatva.WriteErrorLog("", "", "", ex);
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to move file." + "\");", true);
                    }

                });
            }

            if (!string.IsNullOrEmpty(inboxIDs))
            {
                DataTable dtFiles = General_Class.GetInboxFilesFrominboxIds(inboxIDs);

                dtFiles.AsEnumerable().ToList().ForEach(d =>
                {
                    try
                    {
                        var file = d.Field<string>("FileName");
                        int inboxId = d.Field<int>("InboxId");
                        RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                        string path = rackSpaceFileUpload.GeneratePath(file.ToLower(), Enum_Tatva.Folders.inbox);

                        //string dir = Server.MapPath("~/" + Common_Tatva.RackSpaceDownloadDirectory);
                        string dir = Server.MapPath("~/" + Common_Tatva.RackSpaceWorkareaDownload + Path.DirectorySeparatorChar + Sessions.SwitchedSessionId);
                        //string dir = Server.MapPath("~/" + Common_Tatva.RackSpaceWorkareaDownload + Path.DirectorySeparatorChar + Membership.GetUser().ProviderUserKey.ToString());
                        if (!Directory.Exists(dir))
                        {
                            Directory.CreateDirectory(dir);
                        }
                        string fileName = rackSpaceFileUpload.GetObject(ref errorMsg, path);
                        if (string.IsNullOrEmpty(errorMsg))
                        {
                            var filePath = dir + "\\" + fileName;
                            string encodeName = HttpUtility.UrlPathEncode(file).Replace("%", "$");
                            encodeName = encodeName.Replace(encodeName, encodeName.Replace("$20", " "));
                            encodeName = Common_Tatva.SubStringFilename(encodeName);
                            encodeName = encodeName.Replace(encodeName, encodeName.Replace(" ", "$20"));
                            string vpath = Server.MapPath(string.Format("~/{0}", CurrentVolume));
                            string pathName = string.Format("~/{0}", CurrentVolume) + Path.DirectorySeparatorChar + folderId + Path.DirectorySeparatorChar + dividerId + Path.DirectorySeparatorChar + encodeName;

                            int documentOrder = FlashViewer.GetDocumentOrder(Convert.ToInt32(dividerId));
                            string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, folderId, Path.DirectorySeparatorChar, dividerId);
                            if (!Directory.Exists(dstpath))
                            {
                                Directory.CreateDirectory(dstpath);
                            }

                            dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);
                            if (File.Exists(dstpath))
                            {
                                dstpath = Common_Tatva.GetRenameFileName(dstpath);
                            }

                            File.Copy(filePath, dstpath, true);
                            string newFilename = Path.GetFileNameWithoutExtension(dstpath);
                            string displayName = HttpUtility.UrlDecode(newFilename.Replace('$', '%').ToString());
                            string documentIDOrder = Common_Tatva.InsertDocument(dividerId, pathName, documentOrder, displayName, Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()));
                            if (documentIDOrder == "0")
                            {
                                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to move file. There is a problem with the file format." + "\");", true);
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, 0, 0, null, inboxId);
                            }
                            else
                            {
                                General_Class.AuditLogByDocId(Convert.ToInt32(documentIDOrder.Split('#')[0]), Sessions.RackSpaceUserID, Enum_Tatva.Action.Create.GetHashCode());
                                General_Class.UpdateInbox(file, inboxId, Convert.ToInt32(documentIDOrder.Split('#')[0]), 0, Enum_Tatva.IsDelete.No.GetHashCode(), 0, 1);
                                File.Delete(filePath);
                                rackSpaceFileUpload.DeleteObject(ref errorMsg, path);
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode(), 0, 0, 0, 0, null, inboxId);
                                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(folderId), Convert.ToInt32(dividerId), Convert.ToInt32(documentIDOrder.Split('#')[0]), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.InboxUpload.GetHashCode(), 0, 0);
                                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "File has been moved." + "\");", true);
                            }
                        }
                        else
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to get object from cloud." + "\");", true);
                    }
                    catch (Exception ex)
                    {
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + "Failed to move file." + "\");", true);
                    }
                });
            }
            string folderName = hdnSelectedFolderText.Value;
            folderName = folderName.Insert(folderName.IndexOf(')') + 1, "/");
            folderName = folderName.Remove(folderName.IndexOf('('), folderName.IndexOf(')') - folderName.IndexOf('(') + 1);
            string message = "File(s) have been moved to folder: " + folderName;
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert(\"" + message + "\");", true);
            GetData();
            BindGrid();

        }
        catch (Exception ex)
        { }
        finally
        {
            ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
        }
    }
}