﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ucWorkArea.ascx.cs" Inherits="Office_UserControls_ucWorkArea" %>

<script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jstree.min.js"></script>
<script type="text/javascript">
    function loadWorkTree() {
        $.ajax(
                        {
                            type: "POST",
                            url: '../Office/WorkArea.aspx/GetJsonDataForTree',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                var objList = JSON.parse(data.d);
                                if (objList !== undefined && objList.length > 0) {
                                    PrepareTree(objList);
                                }
                                else {
                                    var Tree = "<ul>";
                                    Tree = Tree + "<li id='Li1' path='workArea'>workArea</li></ul>";
                                    $("#jstree1").html(Tree);
                                    DrawTree();
                                }
                            },
                            error: function (result) {
                                console.log('Failed' + result.responseText);
                            }
                        });
    }

    window.PrepareTree = function PrepareTree(objList) {
        
        $('#jstree1').jstree('destroy');
        var Tree = "<ul>";
        Tree = Tree + "<li id='node" + 0 + "' ModifiedDate='" + '<%= DateTime.Now %>' + "' path='WorkArea'>WorkArea</li>";
        $("#jstree1").html(Tree);
        CreateTree("File", objList);
        //CreateTree("Folder", objList);
        DrawTree();
    }

    function CreateTree(prepareTreeFor, objList) {
        var remainingList = [];
        for (var i = 0; i < objList.length; i++) {
            var node = "";
            if (prepareTreeFor == "File") {
                if (objList[i].ObjectName.indexOf(userId) >= 0 || objList[i].ObjectName.indexOf('.') >= 0)
                    continue;
            }
            else if (prepareTreeFor == "Folder") {
                if (objList[i].ObjectName.indexOf(userId) >= 0 || objList[i].ObjectName.indexOf('.') < 0)
                    continue;
            }

            node = $("#jstree1").find("#node" + objList[i].ParentID);
            if (node.length == 0)
                remainingList.push(objList[i]);
            else
                node.append("<ul><li id='node" + objList[i].ObjectID + "' path='" + objList[i].Path + "' name='" + objList[i].ObjectID + "' ModifiedDate='" + objList[i].ModifiedDate + "' Size='" + objList[i].Size + "'>" + objList[i].ObjectName + "</li></ul>");

        }
        for (var i = remainingList.length - 1 ; i >= 0  ; i--) {
            var node = "";
            if (prepareTreeFor == "File") {
                if (remainingList[i].ObjectName.indexOf(userId) >= 0 || remainingList[i].ObjectName.indexOf('.') >= 0)
                    continue;
            }
            else if (prepareTreeFor == "Folder") {
                if (remainingList[i].ObjectName.indexOf(userId) >= 0 || remainingList[i].ObjectName.indexOf('.') < 0)
                    continue;
            }

            node = $("#jstree1").find("#node" + remainingList[i].ParentID);
            node.append("<ul><li id='node" + remainingList[i].ObjectID + "' path='" + remainingList[i].Path + "' name='" + remainingList[i].ObjectID + "' ModifiedDate='" + remainingList[i].ModifiedDate + "' Size='" + remainingList[i].Size + "'>" + remainingList[i].ObjectName + "</li></ul>");

        }
    }

    window.DrawTree = function DrawTree() {
        $('#jstree1').jstree();
        document.getElementById("circularG").style.display = "none";
        //document.getElementById('jsTreeParent').style.borderTop = "1px solid #E6E6E6";
        $('#WorkArea_popup').find('.popup-scroller').css('max-height', ($('#WorkArea_popup').height() - 140 ) + 'px');
        AssignOpenEventsJSTree();
        if (selectedButton == "Search") {
            hideLoader();
        }
        $('#jstree1').jstree('open_all'); //Expands all the nodes of tree
    }

    function AssignOpenEventsJSTree() {
        $('#jstree1').on('before_open.jstree', function (e, data) {
            var leafNode = $("#jstree1").find($(".jstree-leaf"));
            for (var i = 0; i < leafNode.length; i++) {
                var temp = leafNode[i];
                if (leafNode[i] != undefined && leafNode[i].textContent.indexOf('.') > 0) {
                    var childNode = $(leafNode[i]).find("a");
                    if (childNode != undefined) {
                        var node = childNode.find("i");
                        if (node != undefined) {
                            node.addClass("file file-ew node jstree-themeicon-custom");
                        }
                    }
                }
            }
        });
    }

</script>

<div style="position: relative; width: 100%; display: inline-block; padding-top: 10px;" id="jsTreeParent">
    <div id="jstree1">
        <div id="jsTreePreLoad" class="jstree jstree-1 jstree-default" role="tree" aria-multiselectable="true" tabindex="0" aria-activedescendant="node1" aria-busy="false">
            <ul class="jstree-container-ul jstree-children" role="group">
                <li role="treeitem" aria-selected="false" aria-level="1" aria-labelledby="node1_anchor" aria-expanded="false" id="node1" class="jstree-node  jstree-closed jstree-last">
                    <i class="jstree-icon jstree-ocl" role="presentation"></i>
                    <a class="jstree-anchor" href="#" tabindex="-1" id="node1_anchor">
                        <i class="jstree-icon jstree-themeicon" role="presentation"></i>workArea
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <label id="lblSelectedTreeNode" class="selectedTreeNodeLabel"></label>
    <div id="circularG">
        <div id="circularG_1" class="circularG"></div>
        <div id="circularG_2" class="circularG"></div>
        <div id="circularG_3" class="circularG"></div>
        <div id="circularG_4" class="circularG"></div>
        <div id="circularG_5" class="circularG"></div>
        <div id="circularG_6" class="circularG"></div>
        <div id="circularG_7" class="circularG"></div>
        <div id="circularG_8" class="circularG"></div>
    </div>
</div>