﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ViewDeletedItems.aspx.cs" Inherits="Office_ViewDeletedItems" %>

<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>View Deleted Items</h1>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div class="content-box listing-view">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                          
                                <div class="prevnext"  bgcolor="#f8f8f5">
                                    <label style="font-weight:bold; font-size:16px;color:black;">Deleted Folders</label>

                                    <cc1:WebPager style="float: right" ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" OnPageSizeChanged="WebPager1_PageSizeChanged" PageSize="10" CssClass="prevnext " PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>
                                    <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                </div>
                            <div class="work-area">
                                <div >
                                    <asp:GridView ID="GridView1" DataKeyNames="FolderID" runat="server"  OnSorting="GridView1_Sorting" CssClass="datatable listing-datatable boxWidth"
                                                                    AllowSorting="True" AutoGenerateColumns="false" Width="100%" BorderWidth="2" OnRowDeleting="GridView1_RowDeleting" style="border-bottom:2px solid #cccaca;">
                                                                    <PagerSettings Visible="False" />
                                                                    <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                                    <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                                    <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                                    <Columns>
                                                                        <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                            ItemStyle-CssClass="table_tekst_edit">
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="Label8" runat="server" Text="Action" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="libDelete" runat="server" Text="Restore" CausesValidation="false"
                                                                                    CommandName="delete"></asp:LinkButton>
                                                                               <%-- <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                                    CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                                    PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                                </ajaxToolkit:ModalPopupExtender>
                                                                                <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                                    ConfirmText="" TargetControlID="libDelete">
                                                                                </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="FolderName" HeaderText="FolderName" SortExpression="FolderName">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                       
                                                                        <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="DeleteDate" HeaderText="DeleteDate" DataFormatString="{0:MM.dd.yyyy}"
                                                                            SortExpression="DeleteDate" />
                                                                        <asp:BoundField DataField="DeletedName" HeaderText="DeletedBy" SortExpression="DeletedName" />
                                                                    </Columns>
                                                                    <EmptyDataTemplate>
                                                                        <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                                            <tr>
                                                                                <td align="left" class="podnaslov">
                                                                                    <asp:Label ID="Label1" runat="server" />There is no Stored File.
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="clearfix"></div>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" style="margin-top:40px;"> <%--style="margin-top: 15px;"--%>
                        <ContentTemplate>

                                <div class="prevnext" style="margin-top: 25px;" bgcolor="#f8f8f5">
                                    <label style="font-weight:bold; font-size:16px; color:black;">Deleted Documents</label>

                                    <cc1:WebPager style="float: right;" ID="WebPager2" runat="server" OnPageIndexChanged="WebPager2_PageIndexChanged"
                                                                    OnPageSizeChanged="WebPager2_PageSizeChanged" PageSize="10"  CssClass="prevnext" PagerStyle="NumericPages"
                                                                    ControlToPaginate="GridView2"></cc1:WebPager>
                                                                <asp:HiddenField ID="SortDirection2" runat="server" Value=""></asp:HiddenField>
                                </div>

                            <div class="work-area">
                                <div>
                                    <asp:GridView ID="GridView2" DataKeyNames="DocumentID" runat="server" OnSorting="GridView2_Sorting" CssClass="datatable listing-datatable boxWidth"
                                                                    AllowSorting="True" AutoGenerateColumns="false" Width="100%" BorderWidth="2" OnRowDeleting="GridView2_RowDeleting" style="border-bottom:2px solid #cccaca;">
                                                                    <PagerSettings Visible="False" />
                                                                    <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                                    <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                                    <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                                    <Columns>
                                                                        <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                            ItemStyle-CssClass="table_tekst_edit">
                                                                            <HeaderTemplate>
                                                                                <asp:Label ID="Label8" runat="server" Text="Action" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="libDelete" runat="server" Text="Restore" CausesValidation="false"
                                                                                    CommandName="delete"></asp:LinkButton>
                                                                               <%-- <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                                    CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                                    PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                                </ajaxToolkit:ModalPopupExtender>
                                                                                <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                                    ConfirmText="" TargetControlID="libDelete">
                                                                                </ajaxToolkit:ConfirmButtonExtender>--%>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="FolderName" HeaderText="FolderName" SortExpression="FolderName">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                       
                                                                        <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname">
                                                                            <HeaderStyle CssClass="form_title" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="DeleteDate" HeaderText="DeleteDate" DataFormatString="{0:MM.dd.yyyy}"
                                                                            SortExpression="DeleteDate" />
                                                                        <asp:BoundField DataField="DeletedName" HeaderText="DeletedBy"
                                                                            SortExpression="DeletedName" />
                                                                    </Columns>
                                                                    <EmptyDataTemplate>
                                                                        <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                                            <tr>
                                                                                <td align="left" class="podnaslov">
                                                                                    <asp:Label ID="Label1" runat="server" />There is no Stored File.
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </EmptyDataTemplate>
                                                                </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="WebPager2" EventName="PageIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
