﻿using Shinetech.DAL;
using Shinetech.Engines;
using Shinetech.Framework.Controls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Office_ViewDeletedItems : BasePage
{
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
    }

    public DataTable DataSourceFiles
    {
        get
        {
            return this.Session["DeletedFiles"] as DataTable;
        }
        set
        {
            this.Session["DeletedFiles"] = value;
        }
    }

    public DataTable DataSourceFolders
    {
        get
        {
            return this.Session["DeletedFolders"] as DataTable;
        }
        set
        {
            this.Session["DeletedFolders"] = value;
        }
    }

    private void GetData()
    {
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        string subUserId = string.Empty;
        Account acc = new Account(uid);
        if (acc.IsSubUser.ToString() == "1")
        {
            subUserId = acc.UID.ToString();
            uid = acc.OfficeUID.ToString();
        }
        this.DataSourceFiles = General_Class.GetDeletedDocuments(uid);
        this.DataSourceFolders = General_Class.GetDeletedFolders(uid, subUserId);
    }

    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSourceFolders;
        WebPager1.DataBind();

        WebPager2.DataSource = this.DataSourceFiles;
        WebPager2.DataBind();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    private SortDirection Sort_Direction1
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection2.Value))
            {
                SortDirection2.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection2.Value);

        }
        set
        {
            this.SortDirection2.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSourceFolders);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSourceFolders = Source.ToTable();
        BindGrid();
    }

    protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction1 == SortDirection.Ascending)
        {
            this.Sort_Direction1 = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction1 = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSourceFiles);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSourceFiles = Source.ToTable();
        BindGrid();
    }

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSourceFolders;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.DataSourceFolders;
        WebPager1.DataBind();
    }

    protected void WebPager2_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager2.CurrentPageIndex = e.NewPageIndex;
        WebPager2.DataSource = this.DataSourceFiles;
        WebPager2.DataBind();
    }
    protected void WebPager2_PageSizeChanged(object s, PageSizeChangeEventArgs e)
    {
        WebPager2.CurrentPageIndex = 0;
        WebPager2.DataSource = this.DataSourceFiles;
        WebPager2.DataBind();
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            string strFolderID = GridView1.DataKeys[e.RowIndex].Value.ToString();
            FileFolder folder = new FileFolder(int.Parse(strFolderID));
            if (folder.IsExist)
            {
                if (!FileFolderManagement.RestoreDeletedFolder(strFolderID, folder.Path.Value))
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert('Failed to restore Folder! Please contact Administrator.');", true);
                else
                {
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(int.Parse(strFolderID), 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Restore.GetHashCode(), 0, 0);
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert('Folder has been restored successfully.');", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert('Failed to restore Folder! Please contact Administrator.');", true);
        }
        GetData();
        BindGrid();

    }
    protected void LinkButton23_Command(object sender, CommandEventArgs e)
    {

    }


    protected void GridView2_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            string strDocumentID = GridView2.DataKeys[e.RowIndex].Value.ToString();
            Document document = new Document(int.Parse(strDocumentID));
            if (document.IsExist)
            {
                if (!FileFolderManagement.RestoreDeletedFile(strDocumentID))
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert('Failed to restore document! Please contact Administrator.');", true);
                else
                {
                    Document restoredDoc = new Document(int.Parse(strDocumentID));
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(restoredDoc.FileFolderID.Value, restoredDoc.DividerID.Value, int.Parse(strDocumentID), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Restore.GetHashCode(), 0, 0);
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert('Document has been restored successfully.');", true);
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert('Failed to restore document! Please contact Administrator.');", true);
        }
        GetData();
        BindGrid();
    }



}