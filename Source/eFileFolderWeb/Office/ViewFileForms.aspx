<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewFileForms.aspx.cs" Inherits="Office_FileForms" Title="" %>

<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.ComponentModel" %>
<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">
        function openwindow(id) {
            var url = 'ModifyBox.aspx?id=' + id;
            var name = 'ModifyBox';
            window.open(url, name, '');
        }

    </script>

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>View FileForm Requests</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
               <%-- <div class="quick-find" style="margin-bottom: 22px;">
                    <h3>Search here</h3>
                    <p>Enter search term : (RequestID, OfficeName, Lastname, Firstname..)</p>
                    <div class="find-inputbox">
                        <input name="textfield3" class="find-input" type="text" runat="server" id="textfield3" maxlength="50" />
                        <asp:ImageButton ID="searchButton1" CssClass="quick-find-btn btn green" OnClick="OnSearchButton_Click" runat="server" />
                    </div>
                </div>--%>
                <div class="content-box listing-view">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                          
                                <div class="prevnext" style="float: right" bgcolor="#f8f8f5">
                                    <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" OnPageSizeChanged="WebPager1_PageSizeChanged" PageSize="15" CssClass="prevnext custom_select" PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>
                                    <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                </div>
                            <div class="work-area">
                                <div class="overflow-auto">
                                    <asp:GridView ID="GridView1" runat="server" DataKeyNames="FileID,UID"
                                        OnSorting="GridView1_Sorting" AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable datatable listing-datatable">
                                        <PagerSettings Visible="False" />
                                        <AlternatingRowStyle CssClass="odd" />
                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                        <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                        <Columns>
                                            <asp:BoundField DataField="FileID" HeaderText="File ID" SortExpression="FileID"></asp:BoundField>

                                            <asp:BoundField DataField="UserName" HeaderText="UserName" SortExpression="UserName"></asp:BoundField>
                                            
                                            <asp:BoundField DataField="OfficeName" HeaderText="OfficeName" SortExpression="OfficeName"></asp:BoundField>

                                            <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname"></asp:BoundField>

                                            <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname"></asp:BoundField>

                                            <%-- <asp:BoundField DataField="FileName" HeaderText="FileName" SortExpression="FileName">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>--%>

                                            <asp:BoundField DataField="FileNumber" HeaderText="FileNumber" SortExpression="FileNumber"></asp:BoundField>

                                            <asp:BoundField DataField="YearDate" HeaderText="YearDate" SortExpression="YearDate"></asp:BoundField>

                                            <asp:BoundField DataField="RequestedBy" HeaderText="RequestedBy" SortExpression="RequestedBy"></asp:BoundField>

                                            <asp:BoundField DataField="PhoneNumber" HeaderText="Phone No." SortExpression="PhoneNumber"></asp:BoundField>

                                            <asp:BoundField DataField="RequestDate" HeaderText="RequestDate" DataFormatString="{0:MM.dd.yyyy}" SortExpression="RequestDate" />

                                            <%-- <asp:BoundField DataField="ConvertDate" HeaderText="ConvertedDate" SortExpression="ConvertDate" DataFormatString="{0:MM.dd.yyyy}" NullDisplayText="">
                                                                    <HeaderStyle CssClass="form_title" />
                                                                </asp:BoundField>--%>

                                            <asp:BoundField DataField="Name" HeaderText="Creator" SortExpression="Name"></asp:BoundField>

                                            <%--<asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="Label10" Text="File Qty" runat="server" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="false" Text='<%# Bind("FileQuantity") %>' OnClick="LinkButton4_Click" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status"></asp:BoundField>

                                            <%--<asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="Label11" Text="Action" runat="server" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton5" runat="server" CausesValidation="false" Text="Create" OnClick="LinkButton5_Click" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <p>
                                                <asp:Label ID="Label1" runat="server" />There is no File Form Request. 
                                            </p>
                                        </EmptyDataTemplate>

                                    </asp:GridView>
                                </div>
                            </div>
                            <div>
                                <!-- Tabs Editor Panel-->
                                <asp:Button Style="DISPLAY: none" ID="Button1ShowPopup" runat="server"></asp:Button>
                                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
                                    CancelControlID="btnClose1" PopupControlID="Panel1Tabs" TargetControlID="Button1ShowPopup">
                                </ajaxToolkit:ModalPopupExtender>



                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <div class="clearfix"></div>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" > <%--style="margin-top: 15px;"--%>
                        <ContentTemplate>
                                <div class="prevnext" style="float: right;margin-top: 25px;" bgcolor="#f8f8f5">
                                    <cc1:WebPager ID="WebPager3" runat="server" OnPageIndexChanged="WebPager3_PageIndexChanged"
                                        OnPageSizeChanged="WebPager3_PageSizeChanged" PageSize="15" CssClass="prevnext custom_select"
                                        PagerStyle="NumericPages" ControlToPaginate="GridView3"></cc1:WebPager>
                                    <asp:HiddenField ID="SortDirection3" runat="server" Value=""></asp:HiddenField>
                                </div>

                            <div class="work-area">
                                <div class="overflow-auto">
                                    <asp:GridView ID="GridView3" runat="server" DataKeyNames="FileID" OnSorting="GridView3_Sorting"
                                        AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable datatable listing-datatable">
                                        <PagerSettings Visible="False" />
                                        <AlternatingRowStyle CssClass="odd" />
                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                        <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                        <Columns>
                                            <asp:BoundField DataField="FileID" HeaderText="File ID" SortExpression="FileID"></asp:BoundField>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="30px"
                                                ItemStyle-CssClass="table_tekst_edit">
                                                <HeaderTemplate>
                                                    <asp:Label ID="Label34" Text="BoxName" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton21" runat="server" Text='<%#Bind("BoxName")%>' CausesValidation="false"
                                                        OnClientClick='<%#string.Format("javascript:openwindow({0})", Eval("BoxId"))%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="Warehouse" HeaderText="Warehouse" SortExpression="Warehouse"></asp:BoundField>
                                            <asp:BoundField DataField="Section" HeaderText="Section" SortExpression="Section"></asp:BoundField>
                                            <asp:BoundField DataField="Row" HeaderText="Row" SortExpression="Row"></asp:BoundField>
                                            <asp:BoundField DataField="Space" HeaderText="Space" SortExpression="Space"></asp:BoundField>
                                            <asp:BoundField DataField="Other" HeaderText="Other" SortExpression="Other"></asp:BoundField>
                                            <asp:BoundField DataField="DestroyDate" HeaderText="DestroyDate" DataFormatString="{0:MM.dd.yyyy}" SortExpression="DestroyDate" />
                                            <asp:BoundField DataField="BoxNumber" HeaderText="BoxNumber" SortExpression="BoxNumber"></asp:BoundField>
                                            <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName"></asp:BoundField>
                                            <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName"></asp:BoundField>
                                            <asp:BoundField DataField="FileName" HeaderText="FileName" SortExpression="FileName"></asp:BoundField>
                                            <asp:BoundField DataField="FileNumber" HeaderText="FileNumber" SortExpression="FileNumber"></asp:BoundField>
                                            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status"></asp:BoundField>
                                            <%--<asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit">
                                                                    <HeaderTemplate>
                                                                        <asp:Label ID="Label2" Text="Action" runat="server" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" Text="Request"
                                                                            OnCommand="LinkButton5_Click" CommandArgument='<%#Eval("FileID")%>' />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>--%>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <p>
                                                <asp:Label ID="Label3" runat="server" />There is no Stored Paper File.
                                            </p>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="GridView3" EventName="PageIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>

    <!-- ModalPopup Panel-->
    <asp:Panel Style="display: none;" ID="Panel1Tabs" runat="server" CssClass="popup-mainbox">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divTabNum" class="popup-head">&nbsp;File Details</div>
                <ajaxToolkit:ColorPickerExtender ID="ColorPicker1" runat="server" />
                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>
                        <div class="popup-scroller">
                            <asp:GridView ID="GridView2" ShowFooter="true"
                                runat="server" AutoGenerateColumns="false" DataKeyNames="FileID" CssClass="details-datatable">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="odd" />
                                <RowStyle CssClass="even" HorizontalAlign="Center" />
                                <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                <FooterStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />--%>
                                <Columns>
                                    <asp:BoundField DataField="FileID" HeaderText="File ID" SortExpression="FileID"></asp:BoundField>
                                    <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName"></asp:BoundField>
                                    <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName"></asp:BoundField>
                                    <asp:BoundField DataField="FileNumber" HeaderText="File#" SortExpression="FileNumber"></asp:BoundField>
                                    <asp:BoundField DataField="YearDate" HeaderText="Year" SortExpression="YearDate"></asp:BoundField>

                                    <asp:BoundField DataField="UploadDate" HeaderText="Sync Date" SortExpression="UploadDate" DataFormatString="{0:MM.dd.yyyy}" NullDisplayText=""></asp:BoundField>
                                    <asp:BoundField DataField="ConvertDate" HeaderText="ConvertedDate" SortExpression="ConvertDate" DataFormatString="{0:MM.dd.yyyy}" NullDisplayText=""></asp:BoundField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="popup-btn">
            <asp:Button ID="btnClose1" runat="server" Text="Close" CssClass="btn green" />
            <asp:HiddenField ID="HiddenField1" runat="server" />
        </div>
    </asp:Panel>
</asp:Content>
