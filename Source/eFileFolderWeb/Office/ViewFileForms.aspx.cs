using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using AjaxControlToolkit;
using DataQuicker2.Framework;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;

public partial class Office_FileForms : BasePage
{
    private static readonly string PDFPATH = ConfigurationManager.AppSettings["PdfPath"];
    //public string IsShowInsert;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
    }

    #region 数据绑定
    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData()
    {
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        this.DataSource = FileFolderManagement.GetFileFormsWithStatus(uid);
        this.DataSource3 = FileFolderManagement.GetPaperFiles(uid);
    }
    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();

        WebPager3.DataSource = this.DataSource3;
        WebPager3.DataBind();
    }
    #endregion

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    protected void WebPager3_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager3.CurrentPageIndex = e.NewPageIndex;
        WebPager3.DataSource = this.DataSource3;
        WebPager3.DataBind();
    }

    protected void WebPager3_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager3.CurrentPageIndex = 0;
        WebPager3.DataSource = this.DataSource3;
        WebPager3.DataBind();
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["FileFormSource"] as DataTable;
        }
        set
        {
            this.Session["FileFormSource"] = value;
        }
    }

    public DataTable DataSource3
    {
        get
        {
            return this.Session["StorePaper_Source"] as DataTable;
        }
        set
        {
            this.Session["StorePaper_Source"] = value;
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();
        BindGrid();
    }

    protected void GridView3_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction3 == SortDirection.Ascending)
        {
            this.Sort_Direction3 = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction3 = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource3);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource3 = Source.ToTable();
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    private SortDirection Sort_Direction3
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection3.Value))
            {
                SortDirection3.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection3.Value);

        }
        set
        {
            this.SortDirection3.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

 
    //File Quantity
    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;
        string formId = Convert.ToString(this.GridView1.DataKeys[row.RowIndex].Value);
        HiddenField1.Value = formId;

        DataView view = new DataView(GetFilesDataSource(formId));
        GridView2.DataSource = view;

        GridView2.DataBind();
        UpdatePanel3.Update();
        ModalPopupExtender1.Show();

    }

    //Resolver File Request for clients
    protected void LinkButton5_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;

        string formID = Convert.ToString(this.GridView1.DataKeys[row.RowIndex].Value);
        string uid = Convert.ToString(this.GridView1.DataKeys[row.RowIndex].Values[1]);

        this.Session["FileForm_UID"] = uid;
        this.Session["FileForm_FID"] = formID;

        this.Response.Redirect("~/Office/RequestDocument.aspx");
    }

    public DataTable GetFilesDataSource(string formId)
    {
        FormFile dv = new FormFile();
        ObjectQuery query = dv.CreateQuery();
        query.SetCriteria(dv.FormId, Convert.ToInt32(formId));

        DataTable table = new DataTable();

        try
        {
            query.Fill(table);

        }
        catch
        {
            table = null;
        }
        return table;
    }

    public void FillGridView()
    {
        string formId = HiddenField1.Value;

        DataTable table = GetFilesDataSource(formId);
        DataView view = new DataView(table);
        GridView2.DataSource = view;
        GridView2.DataBind();

    }



    protected void btnClose_Click(object sender, EventArgs e)
    {
        GetData();
        BindGrid();
        UpdatePanel1.Update();
    }


    //protected void OnSearchButton_Click(object sender, ImageClickEventArgs e)
    //{
    //    String keyword = this.textfield3.Value.Trim();
    //    string uid = Membership.GetUser().ProviderUserKey.ToString();
    //    this.DataSource = FileFolderManagement.GetFileFormsWithStatusByUID(uid, keyword);

    //    this.DataSource3 = FileFolderManagement.GetPaperFiles(uid, keyword);

    //    this.BindGrid();
    //}
}
