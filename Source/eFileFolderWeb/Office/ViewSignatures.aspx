<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewSignatures.aspx.cs" Inherits="ViewSignatures" Title="" %>

<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register Src="../Controls/TimePicker.ascx" TagName="TimePicker" TagPrefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript" charset="utf-8">
        $("div#btnprint").click(function () {
            $("div#divContent").printArea();
        });

        function openwindow() {
            var url = 'ExportSignatures.aspx';
            var name = 'ExportSignatures';
            var iWidth = 250;
            var iHeight = 200;
            var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
            var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
            window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',toolbar=no,menubar=no,scrollbars=no,resizeable=no,location=no,status=no');
        }

        function openwindowcsv() {
            var url = 'ExportSignatures.aspx?action=csv';
            var name = 'ExportSignatures';
            var iWidth = 250;
            var iHeight = 200;
            var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
            var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
            window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',toolbar=no,menubar=no,scrollbars=no,resizeable=no,location=no,status=no');
        }
    </script>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>View Signatures</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
							<div class="left-content">
                <div class="content-box listing-view">
                    <fieldset>
                        <label>Started Date</label>
                        <asp:TextBox ID="TextBoxStartDate1" runat="server" />
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" PopupPosition="BottomLeft" runat="server"
                            TargetControlID="TextBoxStartDate1" Format="MM-dd-yyyy" />
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender8" PopupPosition="BottomLeft" runat="server"
                            TargetControlID="TextBoxStartDate1" Format="MM-dd-yyyy" EnableViewState="False" />
                        <asp:RegularExpressionValidator ID="startDateC1" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The Started Date is not a date"
                            ControlToValidate="TextBoxStartDate1" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                            Visible="true" Display="None"></asp:RegularExpressionValidator>
                        <ajaxToolkit:ValidatorCalloutExtender
                            runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="startDateC1" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />

                    </fieldset>
                    <fieldset>
                        <label class="left">&nbsp;</label>
                        <uc3:TimePicker ID="TimePicker1" runat="server" />
                    </fieldset>

                    <fieldset>
                        <label>Ended Date</label>
                        <asp:TextBox ID="TextBoxEndDate1" runat="server" />
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender4" PopupPosition="BottomLeft" runat="server"
                            TargetControlID="TextBoxEndDate1" Format="MM-dd-yyyy" />
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender9" PopupPosition="BottomLeft" runat="server"
                            TargetControlID="TextBoxEndDate1" Format="MM-dd-yyyy" EnableViewState="False" />
                        <asp:RegularExpressionValidator ID="endDateC1" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The End Date is not a date"
                            ControlToValidate="TextBoxEndDate1" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                            Visible="true" Display="None"></asp:RegularExpressionValidator><ajaxToolkit:ValidatorCalloutExtender
                                runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="endDateC1" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                    </fieldset>
                    <fieldset>
                        <label class="left">&nbsp;</label>
                        <uc3:TimePicker ID="TimePicker2" runat="server" />
                    </fieldset>
                    <fieldset>
                        <label style="display: none">Type</label>
                        <asp:DropDownList ID="dropdQuestions" Width="150px" Visible="False" runat="server" />
                        <label>Print Name</label>
                        <asp:TextBox ID="TextBoxFirstName1" runat="server" MaxLength="20" />
                    </fieldset>
                    <fieldset>
                        <label>&nbsp;</label>
                        <asp:Button ID="ImageButton2" ImageAlign="AbsMiddle" CssClass="create-btn btn green" Text="Search" OnClick="ImageButton2_OnClick" runat="server" />
                    </fieldset>
                </div>

                <div class="content-box listing-view" style="margin-top: 10px;">
                    <asp:UpdatePanel ID="UpdatePanelResult" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <fieldset>
                                <div class="prevnext" style="float: right" bgcolor="#f8f8f5">
                                    <cc1:WebPager ID="WebPager1" ControlToPaginate="GridView1" PagerStyle="NumericPages"
                                        CssClass="prevnext" PageSize="25" OnPageSizeChanged="WebPager1_PageSizeChanged" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                        runat="server" />
                                    <asp:HiddenField ID="SortDirection1" runat="server" Value="" />
                                </div>
                            </fieldset>
                            <div class="work-area">
                                <div class="overflow-auto">
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false"
                                        AllowSorting="True" OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound"
                                        DataKeyNames="SignatureID" CssClass="datatable datatable-small listing-datatable">
                                        <PagerSettings Visible="False" />
                                        <AlternatingRowStyle CssClass="odd" />
                                        <RowStyle CssClass="even" />
                                        <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                        <Columns>
                                            <asp:BoundField DataField="SignatureID" HeaderText="ID" ItemStyle-Width="60px">
                                                <HeaderStyle CssClass="form_title" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PrintName" HeaderText="PrintName" SortExpression="PrintName">
                                                <HeaderStyle CssClass="form_title" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="SignInDate" HeaderText="SignIn Date" SortExpression="SignInDate" DataFormatString="{0:MM.dd.yyyy H:m tt}">
                                                <HeaderStyle CssClass="form_title" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CheckInDate" HeaderText="CheckIn Date" SortExpression="CheckInDate" NullDisplayText="" DataFormatString="{0:MM.dd.yyyy H:m tt}">
                                                <HeaderStyle CssClass="form_title" />
                                            </asp:BoundField>
                                            <asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit">
                                                <HeaderTemplate>
                                                    <asp:Label ID="Label10" Text="Questions" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="LinkButton5" runat="server" CausesValidation="false" Text='<%# Bind("QuestionQty") %>'
                                                        OnClick="LinkButton5_Click"></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit">
                                                <HeaderTemplate>
                                                    <asp:Label ID="Label1" Text="Wait Time" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="LinkButtonWait" runat="server" CausesValidation="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit">
                                                <HeaderTemplate>
                                                    <asp:Label ID="Label2" Text="Action" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:HiddenField ID="hidSignIn4" runat="server" Value="" />
                                                    <asp:HyperLink ID="LinkButton4" runat="server" CausesValidation="false" CssClass="ic-icon ic-view" />
                                                    <asp:LinkButton ID="LinkButton21" runat="server" ToolTip="CheckIn" CssClass="ic-icon ic-checkin" CausesValidation="false"
                                                        OnCommand="LinkButton21_Click" CommandArgument='<%# Eval("SignatureID")%>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <p>
                                                <asp:Label ID="Label3" runat="server" />There is no Signature be found.
                                            </p>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="form-submit-btn">
                                <asp:Button ID="Button1" Text="Update" CssClass="btn-small green" OnClick="ImageButton1_OnClick" runat="server" />
                                <input id="imgbuttn1" type="button" value="Export Excel" class="btn-small green" title="Export Excel" onclick="openwindow()" />
                                <input id="imgbuttn2" type="button" value="Export CSV" title="Export CSV" class="btn-small green" onclick="openwindowcsv()" />
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
								</div>
								<div class="right-content">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <%--<div class="quick-find">
                            <uc1:QuickFind ID="QuickFind1" runat="server" />
                        </div>--%>

                        <div class="quick-find" style="margin-top: 15px;">
                            <div style="text-align: center;">
                                <h3>Total SignIn</h3>
                            </div>
                            <div style="padding-top: 15px; text-align: center;" class="text-green">
                                <strong>
                                    <asp:Label ID="signNumber" runat="server" Text="NA"></asp:Label></strong>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
								</div>
            </div>

            <!-- Question Panel-->
            <asp:Panel Style="display: none; z-index: 10001" ID="Panel1Tabs" runat="server" CssClass="popup-mainbox" BackColor="White">
                <asp:Button Style="display: none" ID="Button1ShowPopup" CssClass="create-btn btn-small green" runat="server"></asp:Button>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
                    CancelControlID="btnClose1" PopupControlID="Panel1Tabs" TargetControlID="Button1ShowPopup">
                </ajaxToolkit:ModalPopupExtender>
                <!-- ModalPopup Panel-->
                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divTabNum" class="popup-head">
                            Answer Details
                        </div>
                        <asp:HiddenField ID="HiddenField1" runat="server" />
                        <div class="popup-scroller">
                            <asp:GridView ID="GridView2" ShowFooter="false" runat="server" CssClass="datatable listing-datatable"
                                AutoGenerateColumns="false" DataKeyNames="ID">
                                <PagerSettings Visible="False" />
                                <AlternatingRowStyle CssClass="odd" />
                                <RowStyle CssClass="even" HorizontalAlign="Center" />
                                <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                <%-- <FooterStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />--%>
                                <Columns>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                        <HeaderTemplate>
                                            <asp:Label ID="Label6" runat="server" Text="Question ID" />
                                        </HeaderTemplate>
                                        <%--<HeaderStyle CssClass="form_title" />--%>
                                        <ItemTemplate>
                                            <asp:Label ID="Label12" Width="120px" runat="server" Text='<%# Eval("QuestionID")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                        <HeaderTemplate>
                                            <asp:Label ID="Label4" runat="server" Text="Question Name" />
                                        </HeaderTemplate>
                                        <%--<HeaderStyle CssClass="form_title" />--%>
                                        <ItemTemplate>
                                            <asp:Label ID="Label5" Width="120px" runat="server" Text='<%# Eval("QuestionTitle")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                        <HeaderTemplate>
                                            <asp:Label ID="Label7" runat="server" Text="Answer" />
                                        </HeaderTemplate>
                                        <%--<HeaderStyle CssClass="form_title" />--%>
                                        <ItemTemplate>
                                            <asp:Label ID="Label8" Width="120px" runat="server" Text='<%# Eval("AnswerContent")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div align="center">
                    <asp:Button ID="btnClose1" runat="server" Text="Close" Style="margin: 20px" CssClass="create-btn btn green" />
                </div>
            </asp:Panel>

            <asp:Panel Style="display: none; z-index: 10001" ID="Panel1Preview" runat="server"
                Width="350px" CssClass="popup-mainbox" BackColor="White">
                <!-- Preview Panel-->
                <asp:Button Style="display: none" ID="Button1Preview" runat="server" CssClass="create-btn btn-small green"></asp:Button>
                <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalBackground"
                    CancelControlID="btnClose2" BehaviorID="MyMPE" PopupControlID="Panel1Preview" TargetControlID="Button1Preview">
                </ajaxToolkit:ModalPopupExtender>
                <!-- ModalPopup Panel-->
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="divContainer" class="popup-head">
                            &nbsp;Signature Details
                        </div>
                        <asp:HiddenField ID="HiddenField2" runat="server" />
                        <div id="divContentContainer" style="background-color: White">
                            <div id="divContent" style="background-color: White">
                                <img id="imgSignature" alt="Signature" width="150px" height="75px" />
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div align="center" style="padding: 20px 0;">
                    <input id="btnprint" type="button" onclick='$("#divContent").printArea();' value="Print" class="create-btn btn green" />
                    <asp:Button ID="btnClose2" runat="server" Text="Close" class="create-btn btn green"/>
                </div>
            </asp:Panel>
        </div>
    </div>
</asp:Content>
