using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DataQuicker2.Framework;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;
using Shinetech.Framework.Controls;

public partial class ViewSignatures : LicensePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            TimePicker1.Time = "1:59:00";
            TimePicker2.Time = "23:59:00";
            GetData();
            BindGrid();
        }
    }

    #region 数据绑定

    public void GetData()
    {
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();

        if (this.Page.User.IsInRole("Offices"))
        {
            this.DataSource = FileFolderManagement.Get50OfficeSignatures(uid);
            signNumber.Text = this.DataSource.Rows.Count.ToString();
        }
    }

    /// <summary>
    /// 获取数据源
    /// </summary>
    public void GetData(string name, string sdate,string edate)
    {
        string uid = Membership.GetUser().ProviderUserKey.ToString();

        if (this.Page.User.IsInRole("Offices"))
        {
            this.DataSource = FileFolderManagement.GetOfficeSignatures(name, sdate, edate, uid);

            signNumber.Text = this.DataSource.Rows.Count.ToString();
        }
        
    }

    /// <summary>
    /// 初始化绑定
    /// </summary>
    public void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
      
        UpdatePanelResult.Update();
    }
    #endregion

    public DataTable DataSource
    {
        get
        {
            return this.Session["SignIn_DataSource"] as DataTable;
        }
        set
        {
            this.Session["SignIn_DataSource"] = value;
        }
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();
        BindGrid();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if(e.Row.RowType==DataControlRowType.DataRow)
        {
            DataRowView item = e.Row.DataItem as DataRowView;
            if(item!=null)
            {
                string id = item["SignatureData"].ToString();
                HyperLink btnPrv = e.Row.FindControl("LinkButton4") as HyperLink;
                HiddenField hdSignIn = e.Row.FindControl("hidSignIn4") as HiddenField;
                if (btnPrv != null)
                {
                    hdSignIn.Value = id;
                    //btnPrv.OnClientClick = string.Format("alert('{0}')",id);
                    btnPrv.NavigateUrl = string.Format("javascript:showSignature('{0}');",e.Row.DataItemIndex); 
                }

                Label timeLabel = e.Row.FindControl("LinkButtonWait") as Label;

                if (!item["WaitTime"].Equals(DBNull.Value))
                {
                    TimeSpan waittime = TimeSpan.FromSeconds((int)(item["WaitTime"]));
                    timeLabel.Text = waittime.ToString();
                }
                
            }
            
        }
    }

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void LinkButton5_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;
        string sid = Convert.ToString(this.GridView1.DataKeys[row.RowIndex].Value);
        HiddenField1.Value = sid;

        string uid = Membership.GetUser().ProviderUserKey.ToString();

        DataView view = new DataView(GetQuestionDataSource(sid, uid));
        GridView2.DataSource = view;

        GridView2.DataBind();
        UpdatePanel3.Update();
        ModalPopupExtender1.Show();

    }

    public void FillGridView()
    {
        string sid = HiddenField1.Value;
        string uid = Membership.GetUser().ProviderUserKey.ToString();
        DataTable table = GetQuestionDataSource(sid, uid);
        DataView view = new DataView(table);
        GridView2.DataSource = view;
        GridView2.DataBind();

    }

    public DataTable GetQuestionDataSource(string sid,string did)
    {
        PatientQuestion dv = new PatientQuestion();
        ObjectQuery query = dv.CreateQuery();
        query.SetCriteria(dv.SignatureID, sid);
        query.SetCriteria(dv.DoctorID, did);
        query.SetOrderBy(dv.QuestionID,false);

        DataTable table = new DataTable();

        try
        {
            query.Fill(table);

        }
        catch
        {
            table = null;
        }
        return table;
    }

    public Signature GetSignature(string sid)
    {
        Signature dv = new Signature(Convert.ToInt32(sid));
        if (dv.IsExist)
        {
            return dv;
        }
        return null;
    }
  
    //search signatures
    protected void ImageButton2_OnClick(object sender, EventArgs e)
    {
        string uid = Membership.GetUser().ProviderUserKey.ToString();

        string sdate = this.TextBoxStartDate1.Text.Trim();
        string edate = this.TextBoxEndDate1.Text.Trim();
        string name = this.TextBoxFirstName1.Text.Trim();

        string time1 = TimePicker1.Time;
        string time2 = TimePicker2.Time;

        if (!string.IsNullOrEmpty(sdate))
        {
            sdate += " "+time1;
        }

        if (!string.IsNullOrEmpty(edate))
        {
            edate += " " + time2;
        }

        this.DataSource = this.DataSource = FileFolderManagement.GetOfficeSignatures(name, sdate, edate, uid);
        signNumber.Text = this.DataSource.Rows.Count.ToString();
        BindGrid();
    }

    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;
        string sid = Convert.ToString(this.GridView1.DataKeys[row.RowIndex].Value);
        HiddenField2.Value = sid;

        Signature sg = GetSignature(sid);
        HiddenField2.Value = sg.SignatureData.Value;
        string javascript = string.Format("<script>show_image('{0}');</script>", sg.SignatureData.Value);
      
        //imgSignature.Attributes["src"] = sg.SignatureData.Value;
        UpdatePanel1.Update();
        ModalPopupExtender2.Show();
    }

    //Update Signatures
    protected void ImageButton1_OnClick(object sender, EventArgs e)
    {
        GetData();
        BindGrid();
    }

    protected void ImageButton3_OnClick(object sender, ImageClickEventArgs e)
    {

    }

    protected void LinkButton21_Click(Object sender, CommandEventArgs e)
    {
        string sid = e.CommandArgument.ToString();
        Signature sig = new Signature(Convert.ToInt32(sid));
        if (sig.IsExist)
        {
            try
            {
                LinkButton btnLink = sender as LinkButton;
                if (btnLink == null) return;
                GridViewRow row = (GridViewRow)btnLink.NamingContainer;
                if (row == null) return;

                DataRow rowData = this.DataSource.Rows[row.RowIndex + WebPager1.CurrentPageIndex * WebPager1.PageSize];
                rowData["CheckInDate"] = DateTime.Now;
                rowData.AcceptChanges();

                sig.CheckInDate.Value = DateTime.Now;
                sig.Update();

                BindGrid();
            }
            catch { }

            
        }

    }
}
