<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewTransaction.aspx.cs" Inherits="Account_ViewTransaction" Title="" %>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Transactions</h1>
            <p>
                Below is EFF payment transaction list, by which you can view all EFF payment transactions
                detail happen. You can input started date and ended date to query the transactions
                within the date time.
            </p>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt">
                <div class="content-title">EFF Payment Transactions</div>

                <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <div class="content-box">
                            <fieldset>
                                <label>Started Date</label>
                                <asp:TextBox ID="TextBoxStartDate" runat="server" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender2" PopupPosition="BottomLeft" runat="server"
                                    TargetControlID="TextBoxStartDate" Format="MM-dd-yyyy" EnableViewState="False" />
                                <asp:RegularExpressionValidator ID="startDateC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The Started Date is not a date"
                                    ControlToValidate="TextBoxStartDate" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                    Visible="true" Display="None"></asp:RegularExpressionValidator><ajaxToolkit:ValidatorCalloutExtender
                                        runat="Server" ID="ValidatorCalloutExtender6" TargetControlID="startDateC" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                            </fieldset>
                            <fieldset>
                                <label>Ended Date</label>
                                <asp:TextBox
                                    ID="TextBoxEndDate" runat="server" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" PopupPosition="BottomLeft" runat="server"
                                    TargetControlID="TextBoxEndDate" Format="MM-dd-yyyy" EnableViewState="False" />
                                <asp:RegularExpressionValidator ID="endDateC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The End Date is not a date"
                                    ControlToValidate="TextBoxEndDate" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                    Visible="true" Display="None"></asp:RegularExpressionValidator><ajaxToolkit:ValidatorCalloutExtender
                                        runat="Server" ID="ValidatorCalloutExtender1" TargetControlID="endDateC" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                            </fieldset>
                            <fieldset>
                                <label>&nbsp;</label>
                                <asp:Button ID="ImageButton1" Text="Search" OnClick="ImageButton1_OnClick" runat="server" CssClass="create-btn btn green" />
                            </fieldset>
                        </div>
                        <div class="work-area">
                            <div class="overflow-auto">
                                <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false"
                                    AllowSorting="True" OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound"
                                    DataKeyNames="TransactionID" CssClass="datatable listing-datatable">
                                    <PagerSettings Visible="False" />
                                    <AlternatingRowStyle CssClass="odd" />
                                    <RowStyle CssClass="even" HorizontalAlign="Center" />
                                    <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                        <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                    --%><EmptyDataRowStyle HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:BoundField DataField="TransactionID" HeaderText="TransactionID" SortExpression="TransactionID"></asp:BoundField>
                                        <asp:BoundField DataField="FirstName" HeaderText="Firstname" SortExpression="Firstname"></asp:BoundField>
                                        <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname"></asp:BoundField>
                                        <asp:BoundField DataField="KitName" HeaderText="KitName" SortExpression="KitName"></asp:BoundField>
                                        <asp:BoundField DataField="MaximalUserCount" HeaderText="MaximalUser" SortExpression="MaximalUserCount" />
                                        <asp:BoundField DataField="MaxStorageVolume" HeaderText="MaxStorage" SortExpression="MaxStorageVolume"
                                            DataFormatString="{0}G" />
                                        <asp:BoundField DataField="MonthPricePerUser" HeaderText="MonthPrice" SortExpression="MonthPricePerUser"
                                            DataFormatString="${0:F2}" />
                                        <asp:BoundField DataField="CardType" HeaderText="CardType" SortExpression="CardType" />
                                        <asp:BoundField DataField="CardNumber" HeaderText="CardNumber" SortExpression="CardNumber" />
                                        <asp:BoundField DataField="CreatedDate" HeaderText="CreatedDate" SortExpression="CreatedDate"
                                            DataFormatString="{0:MM-dd-yyyy hh:mm}" />
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="70px"
                                            ItemStyle-CssClass="table_tekst_edit">
                                            <HeaderTemplate>
                                                <asp:Label ID="Label34" Text="Status" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="LinkButton21" runat="server" Text='<%# Eval("ResponseCode")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <div>
                                                    <asp:Label ID="Label1" runat="server" />There is no transaction.
                                           </div>
                                    </EmptyDataTemplate>
                                </asp:GridView>

                                <cc1:WebPager ID="WebPager1" ControlToPaginate="GridView1" PagerStyle="OnlyNextPrev"
                                    CssClass="prevnext" PageSize="15" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                    OnPageSizeChanged="WebPager1_PageSizeChanged"
                                    runat="server" />
                                <asp:HiddenField ID="SortDirection1" runat="server" Value="" />
                            </div>
                        </div>

                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>

            </div>
            <div class="clearfix"></div>
            <div class="form-containt">
                <div class="content-title">Storage Fee Transactions</div>

                <asp:UpdatePanel ID="UpdatePanel3" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <div class="content-box">
                            <fieldset>
                                <label>Started Date</label>
                                <asp:TextBox ID="TextBoxStartDate1" runat="server" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender3" PopupPosition="BottomLeft" runat="server"
                                    TargetControlID="TextBoxStartDate1" Format="MM-dd-yyyy" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender8" PopupPosition="BottomLeft" runat="server"
                                    TargetControlID="TextBoxStartDate1" Format="MM-dd-yyyy" EnableViewState="False" />
                                <asp:RegularExpressionValidator ID="startDateC1" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The Started Date is not a date"
                                    ControlToValidate="TextBoxStartDate1" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                    Visible="true" Display="None"></asp:RegularExpressionValidator><ajaxToolkit:ValidatorCalloutExtender
                                        runat="Server" ID="ValidatorCalloutExtender2" TargetControlID="startDateC1" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                            </fieldset>
                            <fieldset>
                                <label>Ended Date</label>
                                <asp:TextBox
                                    ID="TextBoxEndDate1" runat="server" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender4" PopupPosition="BottomLeft" runat="server"
                                    TargetControlID="TextBoxEndDate1" Format="MM-dd-yyyy" />
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender9" PopupPosition="BottomLeft" runat="server"
                                    TargetControlID="TextBoxEndDate1" Format="MM-dd-yyyy" EnableViewState="False" />
                                <asp:RegularExpressionValidator ID="endDateC1" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The End Date is not a date"
                                    ControlToValidate="TextBoxEndDate1" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                    Visible="true" Display="None"></asp:RegularExpressionValidator><ajaxToolkit:ValidatorCalloutExtender
                                        runat="Server" ID="ValidatorCalloutExtender3" TargetControlID="endDateC1" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                            </fieldset>
                            <fieldset>
                                <label>First Name</label>
                                <asp:TextBox ID="TextBoxFirstName1" runat="server" MaxLength="20" />
                            </fieldset>
                            <fieldset>
                                <label>Last Name</label>
                                <asp:TextBox ID="TextBoxLastName1" runat="server" MaxLength="20" />
                            </fieldset>
                            <fieldset>
                                <label>&nbsp;</label>
                                <asp:Button ID="ImageButton2" Text="Search" OnClick="ImageButton2_OnClick" runat="server" CssClass="create-btn btn green" />
                            </fieldset>
                        </div>
                        <div class="work-area">
                            <div class="overflow-auto">
                                <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="false"
                                    AllowSorting="True" OnSorting="GridView2_Sorting" ShowHeader="true" OnRowDataBound="GridView2_RowDataBound"
                                    DataKeyNames="StorageTransID" CssClass="datatable listing-datatable">
                                    <PagerSettings Visible="False" />
                                    <AlternatingRowStyle CssClass="odd" />
                                    <RowStyle CssClass="even" HorizontalAlign="Center" />
                                    <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                        <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                    --%><EmptyDataRowStyle HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:BoundField DataField="StorageTransID" HeaderText="ID" SortExpression="StorageTransID"></asp:BoundField>
                                        <asp:BoundField DataField="FirstName" HeaderText="Firstname" SortExpression="Firstname"></asp:BoundField>
                                        <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname"></asp:BoundField>
                                        <asp:BoundField DataField="Amount" HeaderText="Amount" SortExpression="Amount" DataFormatString="${0:F2}"></asp:BoundField>
                                        <asp:BoundField DataField="Price" HeaderText="Price" SortExpression="Price" DataFormatString="${0:F2}"></asp:BoundField>
                                        <asp:BoundField DataField="Size" HeaderText="Size(Mb)" SortExpression="Size" DataFormatString="{0:F2}" />
                                        <asp:BoundField DataField="CardType" HeaderText="CardType" SortExpression="CardType" />
                                        <asp:BoundField DataField="CardNumber" HeaderText="CardNumber" SortExpression="CardNumber" />
                                        <asp:BoundField DataField="ExpiredDate" HeaderText="Exp.Date" SortExpression="ExpiredDate"
                                            DataFormatString="{0:MM-yyyy}" />
                                        <asp:BoundField DataField="ExchangeDate" HeaderText="ChargedDate" SortExpression="ExchangeDate"
                                            DataFormatString="{0:MM-dd-yyyy}" />
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" SortExpression="ResponseCode"
                                            ItemStyle-Width="30px" ItemStyle-CssClass="table_tekst_edit">
                                            <HeaderTemplate>
                                                <asp:Label ID="Label34" Text="Status" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="LinkButton29" runat="server" Text='<%# Eval("ResponseCode")%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <div>
                                                    <asp:Label ID="Label1" runat="server" />There is no transaction.
                                             </div>
                                    </EmptyDataTemplate>
                                </asp:GridView>

                                <cc1:WebPager ID="WebPager2" ControlToPaginate="GridView2" PagerStyle="OnlyNextPrev"
                                    CssClass="prevnext" PageSize="15" OnPageIndexChanged="WebPager2_PageIndexChanged"
                                    OnPageSizeChanged="WebPager2_PageSizeChanged"
                                    runat="server" />
                                <asp:HiddenField ID="SortDirection2" runat="server" Value="" />
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ImageButton2" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>


</asp:Content>
