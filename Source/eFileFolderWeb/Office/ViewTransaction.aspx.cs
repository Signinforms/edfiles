using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Framework.Controls;

public partial class Account_ViewTransaction : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CalendarExtender2.SelectedDate = DateTime.Now.AddMonths(-1);
            TextBoxStartDate.Text = CalendarExtender2.SelectedDate.Value.ToString("MM-dd-yyy");
            CalendarExtender1.SelectedDate = DateTime.Now.AddDays(1);
            TextBoxEndDate.Text = CalendarExtender1.SelectedDate.Value.ToString("MM-dd-yyy");

            CalendarExtender3.SelectedDate = DateTime.Now.AddMonths(-1);
            TextBoxStartDate1.Text = CalendarExtender3.SelectedDate.Value.ToString("MM-dd-yyy");
            CalendarExtender4.SelectedDate = DateTime.Now.AddDays(1);
            TextBoxEndDate1.Text = CalendarExtender4.SelectedDate.Value.ToString("MM-dd-yyy");

            GetTransData1();
            GetTransData2();

            BindGridView1();
            BindGridView2();
        }
    }

    #region 数据绑定
    /// <summary>
    /// 获取数据源  
    /// </summary>
    private void GetTransData1()
    {
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        bool isUser = true;
        DateTime startDate = DateTime.ParseExact(this.TextBoxStartDate.Text, "mm-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
        DateTime endDate = DateTime.ParseExact(this.TextBoxEndDate.Text,"mm-dd-yyyy",System.Globalization.CultureInfo.InvariantCulture);
        EFFDataSource = LicenseManagement.GetEFFTransactions(isUser, uid, startDate, endDate);
    }

    private void GetTransData2()
    {
        string uid = Sessions.SwitchedSessionId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        DateTime startDate = DateTime.ParseExact(this.TextBoxStartDate1.Text, "mm-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture);
        DateTime endDate = DateTime.ParseExact(this.TextBoxEndDate1.Text, "mm-dd-yyyy", System.Globalization.CultureInfo.InvariantCulture); ;
        CalendarExtender3.SelectedDate = startDate;
        CalendarExtender4.SelectedDate = endDate;
        if (endDate.AddMonths(-12) > startDate)
        {
            startDate = endDate.AddMonths(-12);
            CalendarExtender3.SelectedDate = startDate;
        }
        this.StorageDataSource = LicenseManagement.GetStorageTransactions(true, uid, startDate, endDate,
            this.TextBoxFirstName1.Text.Trim(), this.TextBoxLastName1.Text.Trim(), 0);
    }


    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGridView1()
    {
        WebPager1.DataSource = this.EFFDataSource;
        if (this.EFFDataSource != null)
        {
            WebPager1.DataBind();
        }
    }

    private void BindGridView2()
    {
        WebPager2.DataSource = this.StorageDataSource;
        if (this.StorageDataSource != null)
        {
            WebPager2.DataBind();
        }
    }

    #endregion

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.EFFDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.EFFDataSource = Source.ToTable();
        BindGridView1();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            char[] xxCardNo = e.Row.Cells[7].Text.ToCharArray();
            int cnt = xxCardNo.Length;
            if (cnt > 4)
                for (int i = 0; i < cnt - 4; i++)
                {
                    xxCardNo[i] = 'x';
                }

            e.Row.Cells[7].Text = new string(xxCardNo);

            Label label = e.Row.FindControl("LinkButton21") as Label;
            if (label.Text == "1")
            {
                label.Text = "Success";
            }
            else
            {
                label.Text = "Failed";
            }
        }
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.EFFDataSource;
        WebPager1.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.EFFDataSource;
        WebPager1.DataBind();
    }

    public DataTable EFFDataSource
    {
        get
        {
            return this.Session["EFFTransactions"] as DataTable;
        }
        set
        {
            this.Session["EFFTransactions"] = value;
        }
    }

    public DataTable StorageDataSource
    {
        get
        {
            return this.Session["StorageDataSource"] as DataTable;
        }
        set
        {
            this.Session["StorageDataSource"] = value;
        }
    }

    protected void ImageButton1_OnClick(object sender, EventArgs e)
    {
        GetTransData1();
        BindGridView1();
    }

    protected void WebPager2_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager2.CurrentPageIndex = e.NewPageIndex;
        WebPager2.DataSource = this.StorageDataSource;
        WebPager2.DataBind();
    }

    protected void WebPager2_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager2.CurrentPageIndex = 0;
        WebPager2.DataSource = this.StorageDataSource;
        WebPager2.DataBind();
    }

    protected void ImageButton2_OnClick(object sender, EventArgs e)
    {
        GetTransData2();
        BindGridView2();
    }

    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView dataRowView = (DataRowView)e.Row.DataItem;
            if (dataRowView.Row["CardNumber"] != System.DBNull.Value)
            {
                string var = (string)dataRowView.Row["CardNumber"];
                char[] xxCardNo = var.ToCharArray();
                int cnt = xxCardNo.Length;
                if (cnt > 4)
                    for (int i = 0; i < cnt - 4; i++)
                    {
                        xxCardNo[i] = 'x';
                    }

                e.Row.Cells[7].Text = new string(xxCardNo);
            }
            else
            {
                e.Row.Cells[7].Text = "";
            }

            string code = (string)dataRowView.Row["ResponseCode"];
            Label label = e.Row.FindControl("LinkButton29") as Label;
            if (label.Text == "1")
            {
                label.Text = "Success";
            }
            else
            {
                label.Text = "Failed";
            }

        }
    }

    protected void GridView2_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction2 == SortDirection.Ascending)
        {
            this.Sort_Direction2 = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction2 = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.StorageDataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.StorageDataSource = Source.ToTable();
        BindGridView2();
    }

    private SortDirection Sort_Direction2
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection2.Value))
            {
                SortDirection2.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection2.Value);

        }
        set
        {
            this.SortDirection2.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }
}
