<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewUsers.aspx.cs" Inherits="ViewUsers" Title="" %>

<%@ Import Namespace="System.Data" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register Src="../Controls/UserFind.ascx" TagName="UserFind" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style>
        input[type='checkbox']
        {
            margin-top: 6px;
            height: 18px;
            width: 18px;
        }

        .divIPAdd, .divTimeRest
        {
            display: none;
            padding-top: 5px;
            padding-left: 5px;
        }

        .btn
        {
            font-size: 13px;
        }

        .custom_select
        {
            margin-top: 10px;
        }

        .divPubHoliday
        {
            display: none;
        }

        .btnIPAddr
        {
            padding-top: 6px;
            padding-bottom: 6px;
        }

        .tblLogin
        {
            display: none;
        }

        .txtTime, .optTime
        {
            width: 30% !important;
        }

        .overSelect
        {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }

        .tdTime
        {
            /*width:200px !important;*/
        }
        /*table.details-datatable tbody tr td + td {
            width:250px !important;
        }*/
        .selectBox-outer
        {
            position: relative;
            display: inline-block;
        }

        .selectBox select
        {
            width: 100%!important;
            font-weight: bold;
        }

        #publicHolidayList
        {
            width: 100%;
            display: none;
            border: 1px #dadada solid;
            position: absolute;
            left: 0;
            top: 100%;
            z-index: 9;
            background: #fff;
            max-height: 172px;
            overflow-y: auto;
        }

            #publicHolidayList label
            {
                display: block;
                padding: 0 5px 3px;
                width: auto;
                min-width: initial;
                cursor: pointer;
            }

                #publicHolidayList label:hover
                {
                    background-color: #e5e5e5;
                }

            #publicHolidayList input
            {
                display: inline-block;
            }

        table.details-datatable tbody tr td input[type="checkbox"], table.details-datatable tbody tr td input[type="button"]
        {
            width: auto!important;
            height: auto!important;
        }

        table.details-datatable tbody tr td input[type="checkbox"]
        {
            position: relative;
            top: 5px;
        }

        #tblIPAdd
        {
            width: 300px;
        }

            #tblIPAdd td
            {
                padding: 0;
            }

        .w0
        {
            width: 0!important;
        }

        .w36
        {
            width: 36px!important;
        }

        #tblLogin td
        {
            padding: 5px;
            width: auto!important;
        }

        .details-datatable > tbody > tr > td:first-child
        {
            width: 200px!important;
        }

        .details-datatable > tbody > tr > td:last-child
        {
            width: auto!important;
        }

        #tblLogin td.weekdays
        {
            width: 140px!important;
        }

        #overlay
        {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }

        .ic-edit-pwd
        {
            background: url(../images/icon-edit-psw.png) no-repeat right center;
        }

        .ic-icon
        {
            width: 35px;
        }

        #comboBoxUsers_chosen
        {
            display: block;
            margin-top: 5px;
        }

        .chosen-results
        {
            max-height: 200px !important;
        }

        .chosen-container
        {
            font-size: 15px;
        }

            .chosen-container .chosen-choices
            {
                max-height: 100px;
                overflow: auto;
            }
    </style>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>View Users</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <asp:Button runat="server" ID="hdnClose" Style="display: none;" OnClick="hdnClose_Click" />
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div>
                    <%--class="left-content"--%>
                    <div class="">
                        <div>
                            <p>Below is a list of all users associated with this account. Please verify and update appropriate user info.</p>
                        </div>
                        <div class="content-box listing-view">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:LoginView ID="LoginView2" runat="server">
                                        <RoleGroups>
                                            <asp:RoleGroup Roles="Administrators">
                                                <ContentTemplate>
                                                    <uc1:UserFind ID="UserFind1" runat="server" />
                                                </ContentTemplate>
                                            </asp:RoleGroup>
                                        </RoleGroups>
                                    </asp:LoginView>


                                    <%--<div style="float: right; margin-top: 15px;">
                                        <asp:HiddenField ID="checkBoxData" runat="server" />
                                        <input type="checkbox" onchange="selectAllCheckBoxes();" id="cbSelectAll"
                                            class="JchkAll" style="height: 20px; width: 20px;" /><span style="font-size: 15px; font-weight: bold; margin-left: 5px; margin-right: 10px;">Select/Deselect All</span>
                                        <asp:Button runat="server" ID="saveLockedTabData" ToolTip="Hide Locked Tabs for Selected User(s)" OnClientClick="CollectData()" OnClick="saveLockedTabData_Click" CssClass="create-btn btn-small green checkQuestion" Text="Submit" />
                                    </div>--%>
                                    <div class="prevnext custom_select" style="float: right">
                                        <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" OnPageSizeChanged="WebPager1_PageSizeChanged"
                                            PageSize="15" CssClass="prevnext" PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>
                                        <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>

                                    </div>

                                    <div class="work-area">
                                        <div class="overflow-auto">
                                            <asp:GridView ID="GridView1" runat="server" OnRowDataBound="GridView1_RowDataBound" DataKeyNames="UID" OnSorting="GridView1_Sorting"
                                                AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable">
                                                <PagerSettings Visible="False" />
                                                <AlternatingRowStyle CssClass="odd" />
                                                <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                <Columns>

                                                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"></asp:BoundField>
                                                    <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname"></asp:BoundField>
                                                    <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname"></asp:BoundField>
                                                    <asp:BoundField DataField="DOB" HeaderText="DOB" DataFormatString="{0:MM.dd.yyyy}"
                                                        SortExpression="DOB" Visible="false" />
                                                    <asp:BoundField DataField="IsApproved" HeaderText="IsActive" SortExpression="IsApproved" />
                                                    <asp:BoundField DataField="HasPrivilegeDelete" HeaderText="HasPrivilegeToDelete" />
                                                    <asp:BoundField DataField="HasPrivilegeDownload" HeaderText="HasPrivilegeToDownload" />
                                                    <asp:BoundField DataField="LastLoginDate" HeaderText="LastLoginDate" DataFormatString="{0:MM.dd.yyyy}"
                                                        SortExpression="LastLoginDate" />
                                                    <asp:TemplateField ShowHeader="true">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="Label8" runat="server" Text="Action" />
                                                        </HeaderTemplate>
                                                        <HeaderStyle Width="230" />
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="" OnClick="LinkButton1_Click" CssClass="ic-icon ic-edit"></asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton3" runat="server" Text="" CommandArgument='<%# Eval("UID")%>' OnCommand="LinkButton3_Click" CssClass="ic-icon ic-users"></asp:LinkButton>
                                                            <asp:LinkButton ID="LinkButton5" OnClientClick="return ChangeUserMode(this)" ToolTip="Disable User" runat="server" Data-IsDisable='<%# Eval("IsDisabled") %>' CommandName='<%# Eval("IsDisabled") %>' Text='<%# Eval("UID")%>' CssClass="ic-icon ic-disable"></asp:LinkButton>
                                                            <%--<ajaxToolkit:ModalPopupExtender ID="lnkDisable_ModalpopupExtender" runat="server"
                                                                CancelControlID="ButtonDisableCancel" OkControlID="ButtonDisableOkay" TargetControlID="LinkButton5"
                                                                PopupControlID="DivDisableConfirmation" BackgroundCssClass="ModalPopupBG">
                                                            </ajaxToolkit:ModalPopupExtender>
                                                            <ajaxToolkit:ConfirmButtonExtender ID="ButtonDisableUser" runat="server"
                                                                TargetControlID="LinkButton5" DisplayModalPopupID="lnkDisable_ModalpopupExtender">
                                                            </ajaxToolkit:ConfirmButtonExtender>--%>
                                                            <asp:LinkButton ID="LinkButton2" runat="server" Text="" CommandArgument='<%# Eval("UID")%>' OnCommand="LinkButton2_Click" CssClass="ic-icon ic-delete"></asp:LinkButton>
                                                            <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="LinkButton2"
                                                                PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                            </ajaxToolkit:ModalPopupExtender>
                                                            <ajaxToolkit:ConfirmButtonExtender ID="ButtonDeleteUser" runat="server"
                                                                TargetControlID="LinkButton2" DisplayModalPopupID="lnkDelete_ModalPopupExtender">
                                                            </ajaxToolkit:ConfirmButtonExtender>
                                                            <asp:LinkButton ID="LinkButton4" ToolTip="Update Password" runat="server" Text='<%# Eval("UID")%>' CommandArgument='<%# Eval("UID")%>' OnClientClick="ShowUpdatePwdPanel(this)" CssClass="ic-icon ic-edit-pwd"></asp:LinkButton>
                                                            <%--<asp:CheckBox ID="MyCheckBox" runat="server" Checked='<%#Eval("IsHideLockedTabs") %>' />--%>
                                                            <%--<input type="checkbox" style="margin-top: 6px; height: 18px; width: 18px;" class="chckAll" />--%>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="135" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>



                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged"></asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
                <%--<div class="right-content">

                    <div class="quick-find">
                        <asp:LoginView ID="LoginView3" runat="server">
                            <RoleGroups>
                                <asp:RoleGroup Roles="offices">
                                    <ContentTemplate>
                                        <uc1:UserFind ID="UserFind2" runat="server" />
                                    </ContentTemplate>
                                </asp:RoleGroup>
                            </RoleGroups>
                        </asp:LoginView>
                    </div>
                </div>--%>
            </div>
        </div>
    </div>

    <asp:Panel CssClass="popup-mainbox" ID="DivDeleteConfirmation" Style="display: none"
        runat="server">
        <div>
            <div class="popup-head" id="PopupHeader">
                Delete Subuser
                                        <%--<div class="TitlebarRight" onclick="$get('ButtonDeleteCancel').click();">
                                        </div>--%>
            </div>
            <div class="popup-scroller">
                <p>
                    Are you sure you want to delete this subuser?
                </p>
            </div>
            <div class="popup-btn">
                <input id="ButtonDeleleOkay" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />
                <input id="ButtonDeleteCancel" type="button" value="No" class="btn green" />
            </div>
        </div>
    </asp:Panel>

    <div class="popup-mainbox" id="DivDisableConfirmation" style="display: none; z-index: 100009; position: absolute; left: 37%; top: 45%;">
        <div>
            <div class="popup-head" id="PopupDisableHeader">
                Disable Subuser
            </div>
            <div class="popup-scroller">
                <p id="disableText">
                    Are you sure you want to disable this subuser?
                </p>
            </div>
            <div class="popup-btn">
                <asp:Button runat="server" ID="btnDisableOkay" OnClick="btnDisableOkay_Click" CssClass="btn green" CommandArgument="" Text="Yes" Style="margin-right: 5px;" />
                <%--<input id="ButtonDisableOkay" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />--%>
                <input id="ButtonDisableCancel" type="button" value="No" class="btn green" />
            </div>
        </div>
    </div>

    <div class="popup-mainbox" id="DivUpdatePwdConfirmation" style="display: none; z-index: 100009; position: absolute; left: 30%; top: 50%;">
        <div>
            <div class="popup-head" id="Div1">
                Update Password
            </div>
            <div class="popup-scroller">
                <p>
                    Are you sure you want to update password for this subuser?
                </p>
            </div>
            <div class="popup-btn">

                <input id="btnUpdatePwdOkay" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />
                <input id="btnUpdatePwdCancel" type="button" value="No" class="btn green" />
            </div>
        </div>
    </div>

    <asp:Button Style="display: none" ID="btnShowPopup" runat="server" CausesValidation="false"></asp:Button>
    <ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="btnClose" PopupControlID="pnlPopup" TargetControlID="btnShowPopup">
    </ajaxToolkit:ModalPopupExtender>

    <asp:Panel Style="display: none; width: 40%; max-height: calc(100vh - 200px); overflow-y: auto;"
        ID="pnlPopup" runat="server">
        <asp:UpdatePanel ID="updPnlCustomerDetail" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label ID="lblCustomerDetail" Width="100%" runat="server" CssClass="popup-head" Text="User Details"></asp:Label>
                <div class="popup-scroller" style="max-height: 600px;">
                    <asp:DetailsView ID="dvCustomerDetail" runat="server" DataKeyNames="UID" Width="100%"
                        BackColor="white" DefaultMode="Edit" AutoGenerateRows="false" AllowPaging="false"
                        OnDataBound="dvCustomerDetail_DataBound" CssClass="details-datatable" BorderStyle="None">
                        <Fields>
                            <asp:BoundField DataField="Name" ReadOnly="true" HeaderText="Name" />
                            <asp:TemplateField HeaderText="Firstname" ShowHeader="true">
                                <ItemTemplate>
                                    <asp:TextBox ID="textBoxFirstname" runat="server" MaxLength="20" Text='<%# DataBinder.Eval(Container.DataItem,"Firstname")%>' />
                                    <asp:RequiredFieldValidator runat="server" ID="firstNameV" ControlToValidate="textBoxFirstname"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The first name is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                        TargetControlID="firstNameV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Lastname" ShowHeader="true">
                                <ItemTemplate>
                                    <asp:TextBox ID="textBoxLastname" MaxLength="20" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Lastname")%>' />
                                    <asp:RequiredFieldValidator runat="server" ID="lastNameV" ControlToValidate="textBoxLastname"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The last name is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10"
                                        TargetControlID="lastNameV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%--<asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    <asp:Label ID="Label2" runat="server" Text="DOB" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox runat="server" MaxLength="50" Text='<%# DataBinder.Eval(Container.DataItem,"DOB")%>'
                                        ID="textfield5" />
                                    <asp:RequiredFieldValidator runat="server" ID="DobV" ControlToValidate="textfield5"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The DOB is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5"
                                        TargetControlID="DobV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    <asp:RegularExpressionValidator ID="DobC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The DOB is not a date"
                                        ControlToValidate="textfield5" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                        Visible="true" Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6"
                                        TargetControlID="DobC" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" PopupPosition="BottomLeft" SelectedDate='<%# DataBinder.Eval(Container.DataItem,"DOB")%>'
                                        runat="server" TargetControlID="textfield5" Format="MM-dd-yyyy">
                                    </ajaxToolkit:CalendarExtender>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                            <asp:CheckBoxField DataField="IsApproved" HeaderText="IsActive" />
                            <asp:CheckBoxField DataField="HasPrivilegeDelete" HeaderText="HasPrivilegeToDelete" />
                            <asp:CheckBoxField DataField="HasPrivilegeDownload" HeaderText="HasPrivilegeToDownload" />
                            <asp:CheckBoxField DataField="ViewPrivilege" HeaderText="ViewPrivilege" />

                            <asp:TemplateField HeaderText="Email" ShowHeader="true">
                                <ItemTemplate>
                                    <asp:TextBox ID="textBoxEmail" MaxLength="50" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Email")%>' />
                                    <asp:RequiredFieldValidator runat="server" ID="EmailReq" ControlToValidate="textBoxEmail"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />An email is required." />
                                    <asp:RegularExpressionValidator ID="rev_Email" runat="server" ErrorMessage="<b>Required Field Format</b><br />It is not an email format."
                                        ControlToValidate="textBoxEmail" ValidationExpression="^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        Visible="true" Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="EmailReqE" TargetControlID="EmailReq"
                                        HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                        TargetControlID="rev_Email" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Telephone" ShowHeader="true">
                                <ItemTemplate>
                                    <asp:TextBox ID="textBoxPhone" runat="server" MaxLength="23" Text='<%# DataBinder.Eval(Container.DataItem,"Telephone")%>' />
                                    <asp:RegularExpressionValidator ID="TelV" ControlToValidate="textBoxPhone" runat="server"
                                        ErrorMessage="<b>Required Field Format</b><br />It is not a telphone format."
                                        ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                        TargetControlID="TelV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="MobilePhone" ShowHeader="true">
                                <ItemTemplate>
                                    <asp:TextBox ID="textBoxMobile" MaxLength="20" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"MobilePhone")%>' />
                                    <asp:RegularExpressionValidator ID="mobileV" ControlToValidate="textBoxMobile" runat="server"
                                        ErrorMessage="<b>Required Field Missing</b><br />It is not a Mobile format."
                                        ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9"
                                        TargetControlID="mobileV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="FaxNumber" ShowHeader="true">
                                <ItemTemplate>
                                    <asp:TextBox ID="textBoxFax" MaxLength="23" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"FaxNumber")%>' />
                                    <asp:RegularExpressionValidator ID="faxV" ControlToValidate="textBoxFax" runat="server"
                                        ErrorMessage="<b>Required Field Missing</b><br />It is not a FAX format." ValidationExpression="^\d{7,}"
                                        Display="None"></asp:RegularExpressionValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8"
                                        TargetControlID="faxV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CompanyName" ShowHeader="true">
                                <ItemTemplate>
                                    <asp:TextBox ID="textBoxCompanyName" MaxLength="50" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"CompanyName")%>' />
                                    <asp:RequiredFieldValidator runat="server" ID="companyNameN" ControlToValidate="textBoxCompanyName"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The company name is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14"
                                        TargetControlID="companyNameN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Assign Offices">
                                <ItemTemplate>
                                    <select id="comboBoxUsers" multiple style="color: rgb(68,68,68); border-radius: 5px; background-color: #fff; border: 1px solid #aaa; box-shadow: 0 1px 0 #fff inset; background: linear-gradient(#fff 20%, #f6f6f6 50%, #eee 52%, #f4f4f4 100%); max-height: 100px;">
                                    </select>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Address" ShowHeader="true">
                                <ItemTemplate>
                                    <asp:TextBox ID="textBoxAddress" MaxLength="200" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Address")%>' />
                                    <asp:RequiredFieldValidator runat="server" ID="addressN" ControlToValidate="textBoxAddress"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The address is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15"
                                        TargetControlID="addressN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="City" ShowHeader="true">
                                <ItemTemplate>
                                    <asp:TextBox ID="textBoxCity" MaxLength="30" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"City")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="State/Province">
                                <ItemTemplate>
                                    <asp:DropDownList runat="server" ID="drpState">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="OtherState" ShowHeader="true">
                                <ItemTemplate>
                                    <asp:TextBox ID="textBoxOtherState" MaxLength="30" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"OtherState")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Country">
                                <ItemTemplate>
                                    <asp:DropDownList runat="server" ID="drpCountry" Width="51%">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Postal" ShowHeader="true">
                                <ItemTemplate>
                                    <asp:TextBox ID="textBoxPostal" MaxLength="30" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Postal")%>' />
                                    <asp:RequiredFieldValidator runat="server" ID="postalV" ControlToValidate="textBoxPostal"
                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The postal code is required!" />
                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17"
                                        TargetControlID="postalV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:CheckBoxField DataField="HasProtectedSecurity" HeaderText="HasProtectedSecurity" />
                            <asp:CheckBoxField DataField="HasPublicSecurity" HeaderText="HasPublicSecurity" />
                            <asp:BoundField DataField="RoleID" ReadOnly="true" HeaderText="RoleName" />
                            <asp:BoundField DataField="LastLoginDate" ReadOnly="true" HeaderText="LastLoginDate"
                                DataFormatString="{0:MM/dd/yyy}" />
                            <asp:TemplateField HeaderText="Hide Locked Tabs" ShowHeader="true">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkTabAccess" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Restrict IP Address" ShowHeader="true">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkIPAdd" runat="server" onclick="Javascript:OnChkIPAddClick();" />

                                    <table id="tblIPAdd" style="display: none;">
                                        <tbody>
                                            <tr id="indexRow0" class="indexRow">
                                                <td class="w0">
                                                    <input type="hidden" value="0" class="hdnIPId"></td>
                                                <td style="width: 100%;">
                                                    <input type="text" id="txtIPAdd" placeholder="IP Address" maxlength="100" style="width: 100%;" /></td>
                                                <td class="w36">
                                                    <button id="btnAddIPAddr" class="btnIPAddr " type="button" onclick="return AddIPAddTextBoxes(this)">
                                                        <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/add-icon.png" title="Add" /></button></td>
                                                <td class="w36">
                                                    <button id="btnRemoveIPAddr" class="btnIPAddr " type="button" onclick="return RemoveIPAddTextBoxes(this)">
                                                        <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" />
                                                    </button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div id="divIPAdd" class="divIPAdd">
                                        <input id="btnIPAddrSave" type="button" value="Save" class="blue btn-small save" onclick="return SaveIPAddr();" />
                                        <input id="btnIPAddrCancel" type="button" value="Cancel" class="blue btn-small" onclick="return CancelIPAddr();" />
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Restrict Login Times" ShowHeader="true">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkTimeSpan" runat="server" onclick="Javascript:OnChkTimeRestClick();" />
                                    <div class="clear divPubHoliday">
                                        <div class="selectBox-outer">
                                            <div class="selectBox" onclick="showCheckboxesPublicHolidays()">
                                                <select>
                                                    <option>Select Public Holiday(s)</option>
                                                </select>
                                                <div class="overSelect"></div>
                                            </div>
                                            <div id="publicHolidayList" style="display: none;"></div>
                                        </div>
                                    </div>
                                    <table id="tblLogin" class="tblLogin">
                                        <tr>
                                            <td class="weekdays">
                                                <label>WeekDays</label>
                                            </td>
                                            <td>
                                                <label>From Time: </label>
                                            </td>
                                            <td>
                                                <label>To Time:</label>
                                            </td>
                                        </tr>
                                        <%--All Days--%>
                                        <tr>
                                            <td class="tdTime">
                                                <input type="checkbox" class="chkWeekDays chkAllDays txtTime" onchange="return onChkAllDaysClick();" />
                                                <span>All Days</span>
                                            </td>
                                            <td class="tdTime">
                                                <input type="text" placeholder="Hour" class="txtTime txtFromHour" value="12" onchange=" return ValidateHourMin(this);" />
                                                <input type="text" placeholder="Minute" class="txtTime txtFromMin" value="00" onchange=" return ValidateHourMin(this);" />
                                                <select id="optAllFrom" class="optTime optFrom">
                                                    <option>AM</option>
                                                    <option>PM</option>
                                                </select>
                                            </td>
                                            <td class="tdTime">
                                                <input type="text" placeholder="Hour" class="txtTime txtToHour" value="11" onchange=" return ValidateHourMin(this);" />
                                                <input type="text" placeholder="Minute" class="txtTime txtToMin" value="59" onchange=" return ValidateHourMin(this);" />
                                                <select id="optAllTo" class="optTime optTo">
                                                    <option>AM</option>
                                                    <option selected="selected">PM</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <%--Sunday--%>
                                        <tr>
                                            <td class="tdTime">
                                                <input type="checkbox" class="chkWeekDays txtTime" />
                                                <span>Sunday</span>
                                            </td>
                                            <td class="tdTime">
                                                <input type="text" placeholder="Hour" class="txtTime txtFromHour" value="12" onchange=" return ValidateHourMin(this);" />
                                                <input type="text" placeholder="Minute" class="txtTime txtFromMin" value="00" onchange=" return ValidateHourMin(this);" />
                                                <select id="Select3" class="optTime optFrom">
                                                    <option>AM</option>
                                                    <option>PM</option>
                                                </select>
                                            </td>
                                            <td class="tdTime">
                                                <input type="text" placeholder="Hour" class="txtTime txtToHour" value="11" onchange=" return ValidateHourMin(this);" />
                                                <input type="text" placeholder="Minute" class="txtTime txtToMin" value="59" onchange=" return ValidateHourMin(this);" />
                                                <select id="Select4" class="optTime optTo">
                                                    <option>AM</option>
                                                    <option selected="selected">PM</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <%--Monday--%>
                                        <tr>
                                            <td class="tdTime">
                                                <input type="checkbox" class="chkWeekDays txtTime" />
                                                <span>Monday</span>
                                            </td>
                                            <td class="tdTime">
                                                <input type="text" placeholder="Hour" class="txtTime txtFromHour" value="12" onchange=" return ValidateHourMin(this);" />
                                                <input type="text" placeholder="Minute" class="txtTime txtFromMin" value="00" onchange=" return ValidateHourMin(this);" />
                                                <select id="Select5" class="optTime optFrom">
                                                    <option>AM</option>
                                                    <option>PM</option>
                                                </select>
                                            </td>
                                            <td class="tdTime">
                                                <input type="text" placeholder="Hour" class="txtTime txtToHour" value="11" onchange=" return ValidateHourMin(this);" />
                                                <input type="text" placeholder="Minute" class="txtTime txtToMin" value="59" onchange=" return ValidateHourMin(this);" />
                                                <select id="Select6" class="optTime optTo">
                                                    <option>AM</option>
                                                    <option selected="selected">PM</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <%--Tuesday--%>
                                        <tr>
                                            <td class="tdTime">
                                                <input type="checkbox" class="chkWeekDays txtTime" />
                                                <span>Tuesday</span>
                                            </td>
                                            <td class="tdTime">
                                                <input type="text" placeholder="Hour" class="txtTime txtFromHour" value="12" onchange=" return ValidateHourMin(this);" />
                                                <input type="text" placeholder="Minute" class="txtTime txtFromMin" value="00" onchange=" return ValidateHourMin(this);" />
                                                <select id="Select1" class="optTime optFrom">
                                                    <option>AM</option>
                                                    <option>PM</option>
                                                </select>
                                            </td>
                                            <td class="tdTime">
                                                <input type="text" placeholder="Hour" class="txtTime txtToHour" value="11" onchange=" return ValidateHourMin(this);" />
                                                <input type="text" placeholder="Minute" class="txtTime txtToMin" value="59" onchange=" return ValidateHourMin(this);" />
                                                <select id="Select2" class="optTime optTo">
                                                    <option>AM</option>
                                                    <option selected="selected">PM</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <%--WednesDay--%>
                                        <tr>
                                            <td class="tdTime">
                                                <input type="checkbox" class="chkWeekDays txtTime" />
                                                <span>Wednesday</span>
                                            </td>
                                            <td class="tdTime">
                                                <input type="text" placeholder="Hour" class="txtTime txtFromHour" value="12" onchange=" return ValidateHourMin(this);" />
                                                <input type="text" placeholder="Minute" class="txtTime txtFromMin" value="00" onchange=" return ValidateHourMin(this);" />
                                                <select id="Select7" class="optTime optFrom">
                                                    <option>AM</option>
                                                    <option>PM</option>
                                                </select>
                                            </td>
                                            <td class="tdTime">
                                                <input type="text" placeholder="Hour" class="txtTime txtToHour" value="11" onchange=" return ValidateHourMin(this);" />
                                                <input type="text" placeholder="Minute" class="txtTime txtToMin" value="59" onchange=" return ValidateHourMin(this);" />
                                                <select id="Select8" class="optTime optTo">
                                                    <option>AM</option>
                                                    <option selected="selected">PM</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <%-- Thursday--%>
                                        <tr>
                                            <td class="tdTime">
                                                <input type="checkbox" class="chkWeekDays txtTime" />
                                                <span>Thursday</span>
                                            </td>
                                            <td class="tdTime">
                                                <input type="text" placeholder="Hour" class="txtTime txtFromHour" value="12" onchange=" return ValidateHourMin(this);" />
                                                <input type="text" placeholder="Minute" class="txtTime txtFromMin" value="00" onchange=" return ValidateHourMin(this);" />
                                                <select id="Select9" class="optTime optFrom">
                                                    <option>AM</option>
                                                    <option>PM</option>
                                                </select>
                                            </td>
                                            <td class="tdTime">
                                                <input type="text" placeholder="Hour" class="txtTime txtToHour" value="11" onchange=" return ValidateHourMin(this);" />
                                                <input type="text" placeholder="Minute" class="txtTime txtToMin" value="59" onchange=" return ValidateHourMin(this);" />
                                                <select id="Select10" class="optTime optTo">
                                                    <option>AM</option>
                                                    <option selected="selected">PM</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <%--Friday--%>
                                        <tr>
                                            <td class="tdTime">
                                                <input type="checkbox" class="chkWeekDays txtTime" />
                                                <span>Friday</span>
                                            </td>
                                            <td class="tdTime">
                                                <input type="text" placeholder="Hour" class="txtTime txtFromHour" value="12" onchange=" return ValidateHourMin(this);" />
                                                <input type="text" placeholder="Minute" class="txtTime txtFromMin" value="00" onchange=" return ValidateHourMin(this);" />
                                                <select id="Select11" class="optTime optFrom">
                                                    <option>AM</option>
                                                    <option>PM</option>
                                                </select>
                                            </td>
                                            <td class="tdTime">
                                                <input type="text" placeholder="Hour" class="txtTime txtToHour" value="11" onchange=" return ValidateHourMin(this);" />
                                                <input type="text" placeholder="Minute" class="txtTime txtToMin" value="59" onchange=" return ValidateHourMin(this);" />
                                                <select id="Select12" class="optTime optTo">
                                                    <option>AM</option>
                                                    <option selected="selected">PM</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <%--Saturday--%>
                                        <tr>
                                            <td class="tdTime">
                                                <input type="checkbox" class="chkWeekDays txtTime" />
                                                <span>Saturday</span>
                                            </td>
                                            <td class="tdTime">
                                                <input type="text" placeholder="Hour" class="txtTime txtFromHour" value="12" onchange=" return ValidateHourMin(this);" />
                                                <input type="text" placeholder="Minute" class="txtTime txtFromMin" value="00" onchange=" return ValidateHourMin(this);" />
                                                <select id="Select13" class="optTime optFrom">
                                                    <option>AM</option>
                                                    <option>PM</option>
                                                </select>
                                            </td>
                                            <td class="tdTime">
                                                <input type="text" placeholder="Hour" class="txtTime txtToHour" value="11" onchange=" return ValidateHourMin(this);" />
                                                <input type="text" placeholder="Minute" class="txtTime txtToMin" value="59" onchange=" return ValidateHourMin(this);" />
                                                <select id="Select14" class="optTime optTo">
                                                    <option>AM</option>
                                                    <option selected="selected">PM</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                    <div id="divTimeRest" class="divTimeRest">
                                        <input id="btnSaveTimeRest" type="button" value="Save" class="blue btn-small" style="margin-right: 5px;" onclick="return SaveTimeRest();" />
                                        <input id="btnCancelTimeRest" type="button" value="Cancel" class="blue btn-small" />
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Fields>
                    </asp:DetailsView>
                </div>
                <asp:HiddenField ID="hdnIPAddresses" runat="server" />
                <asp:HiddenField ID="hdnTimeRest" runat="server" />
                <asp:HiddenField ID="hdnPublicHolidays" runat="server" />
                <asp:HiddenField ID="hdnUserMode" runat="server" />
                <asp:HiddenField ID="hdnUserID" runat="server" />
                <asp:HiddenField ID="hdnNewPwd" runat="server" />
                <asp:HiddenField ID="hdnConfirmNewPwd" runat="server" />
                <asp:HiddenField ID="hdnMultiUserIds" runat="server" />
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <div class="popup-btn">
            <asp:Button ID="btnSave" OnClick="btnSave_Click" OnClientClick="return OnBtnSaveClick()" runat="server" Text="Save" Style="margin-right: 5px;" CssClass="btn green"></asp:Button>
            <asp:Button ID="btnClose" OnClientClick="return CleanHdnValues();" runat="server" Text="Close" CssClass="btn green" CausesValidation="false"></asp:Button>
        </div>
    </asp:Panel>

    <asp:Button Style="display: none" ID="btnShowPopup2" runat="server"></asp:Button>
    <ajaxToolkit:ModalPopupExtender ID="mdlPopup2" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="btnClose2" PopupControlID="pnlPopup2" TargetControlID="btnShowPopup2">
    </ajaxToolkit:ModalPopupExtender>

    <asp:Panel Style="display: none;" ID="pnlPopup2" runat="server" CssClass="popup-mainbox">
        <div style="padding: 10px;">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:HiddenField ID="theUserText" runat="server" Value="" />
                    <asp:Label ID="Label1" runat="server" Width="100%" Text="Groups Details"
                        CssClass="popup-head"></asp:Label>
                    <div style="width: 100%; background-color: White; display: table" id="modelAjax" class="popup-scroller">
                        <div class="popup-group">
                            <asp:CheckBox name="groupfield1" Style="margin-right: 10px; width: 10%" runat="server" Text="" ID="groupfield1"
                                size="30" /><asp:TextBox ID="grouplink1" Text="Group 1" runat="server" Style="margin: 10px 0px 0px" /><br>
                            <asp:CheckBox name="groupfield2" Style="margin-right: 10px" runat="server" Text="" ID="groupfield2"
                                size="30" /><asp:TextBox ID="grouplink2" Text="Group 2" runat="server" Style="margin: 10px 0px 0px" /><br>
                            <asp:CheckBox name="groupfield3" Style="margin-right: 10px" runat="server" Text="" ID="groupfield3"
                                size="30" /><asp:TextBox ID="grouplink3" Text="Group 3" runat="server" Style="margin: 10px 0px 0px" /><br>
                            <asp:CheckBox name="groupfield4" Style="margin-right: 10px" runat="server" Text="" ID="groupfield4"
                                size="30" /><asp:TextBox ID="grouplink4" Text="Group 4" runat="server" Style="margin: 10px 0px 0px" /><br>
                            <asp:CheckBox name="groupfield5" Style="margin-right: 10px" runat="server" Text="" ID="groupfield5"
                                size="30" /><asp:TextBox ID="grouplink5" Text="Group 5" runat="server" Style="margin: 10px 0px 0px" />
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSave2" EventName="Click"></asp:AsyncPostBackTrigger>
                </Triggers>
            </asp:UpdatePanel>
            <div class="popup-btn">
                <asp:Button ID="btnSave2" OnClick="btnSave2_Click" runat="server" Text="Save" Style="margin-right: 5px;" CssClass="btn green"></asp:Button>
                <asp:Button ID="btnClose2" runat="server" Text="Close" CssClass="btn green"></asp:Button>
            </div>
        </div>
    </asp:Panel>



    <div style="display: none; width: 40%; z-index: 100009; position: absolute; left: 30%; top: 50%;" id="Panel1">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label ID="Label3" Width="100%" runat="server" CssClass="popup-head" Text="Update Password" Style="max-width: 500px;"></asp:Label>
                <div class="popup-scroller" style="max-height: 800px; max-width: 500px;">
                    <div style="margin-top: 5px;">
                        <asp:Label runat="server" Style="font-size: 16px; margin-left: 5px;">Enter New Password:</asp:Label>
                        <asp:TextBox ID="txtNewPassword" TextMode="Password" runat="server" MaxLength="20" Style="max-width: 250px; margin-left: 5px;" />
                    </div>
                    <div style="margin-top: 10px;">
                        <asp:Label ID="Label4" runat="server" Style="font-size: 16px; margin-left: 5px;">Confirm New Password:</asp:Label>
                        <asp:TextBox ID="txtNewConfirmPassword" TextMode="Password" runat="server" MaxLength="20" Style="max-width: 250px; margin-left: 5px;" />
                    </div>
                </div>

            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnUpdate" EventName="Click"></asp:AsyncPostBackTrigger>
            </Triggers>
        </asp:UpdatePanel>
        <div class="popup-btn" style="max-width: 500px;">
            <asp:Button ID="btnUpdate" OnClientClick="return ShowConfirmPwdDialog();" runat="server" Text="Save" CssClass="btn green" Style="margin-right: 5px;"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn green" CausesValidation="false"></asp:Button>
        </div>
    </div>

    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>

    <script type="text/javascript">
        var isShowHolidays = false;
        var WeekDays = {
            Sunday: 0,
            Monday: 1,
            Tuesday: 2,
            Wednesday: 3,
            Thursday: 4,
            Friday: 5,
            Saturday: 6,
            AllDays: 7
        }

        var ipAddRegEx6 = /^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/;
        var ipAddRegEx = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
        var numberRegExp = /^\d*(?:\.\d{1,2})?$/;

        $(document).ready(function () {

        });

        function disableScrollbar() {
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }

        function enableScrollbar() {
            $('html, body').css({
                'overflow': 'auto',
                'height': 'auto'
            });
        }

        //$(function () {
        //    $(window).resize(function () {
        //        // get the screen height and width  
        //        var maskHeight = $(window).height();
        //        var maskWidth = $(window).width();

        //        // calculate the values for center alignment
        //        var dialogTop = (maskHeight - $('#dialog-box').height()) / 2;
        //        var dialogLeft = (maskWidth - $('#dialog-box').width()) / 2;

        //        // assign values to the overlay and dialog box
        //        //$('#pnlPopup').css({ height: $(document).height(), width: $(document).width() }).show();
        //        $('#pnlPopup').css({ top: dialogTop, left: dialogLeft, position: "fixed" }).show();
        //    }).resize();
        //});



        function UnAssignOffice(ele, uid) {
            var assignedUID = $($('#comboBoxUsers').find('option')[ele.attr('data-option-array-index')]).val();
            var assignedName = $($('#comboBoxUsers').find('option')[ele.attr('data-option-array-index')]).html();
            $.ajax(
             {
                 type: "POST",
                 url: '../FlexWebService.asmx/UnAssignOffice',
                 contentType: "application/json; charset=utf-8",
                 data: '{"assignedUid":"' + assignedUID + '","assignedTo": "' + uid + '"}',
                 dataType: "json",
                 success: function (result) {
                     if (result.d.length > 0)
                         alert("Office " + assignedName + " has been unassigned successfully.");
                 },
                 fail: function (data) {
                 }
             });
        }

        function LoadUsers(uid) {
            $("#comboBoxUsers").html('');
            $("#comboBoxUsers").chosen("destroy");
            $.ajax(
             {
                 type: "POST",
                 url: '../FlexWebService.asmx/GetAssignedUsersForSubUsers',
                 contentType: "application/json; charset=utf-8",
                 data: '{"officeId":"' + '<%= Sessions.SwitchedSessionId %>' + '","subUserId": "' + uid + '"}',
                 dataType: "json",
                 success: function (result) {
                     var offices = [];
                     if (result.d) {
                         $.each(result.d.Item1, function (i, text) {
                             $('<option />', { value: i, text: text }).appendTo($("#comboBoxUsers"));
                         });

                         var officeUserCount = result.d.Item1.count;

                         $.each(result.d.Item2, function (i, text) {
                             if (!officeUserCount) {
                                 $('<option />', { value: i, text: text }).appendTo($("#comboBoxUsers"))
                             }
                             offices.push(i);
                         });
                     }
                     $("#   ").chosen({ width: "93%" });
                     $("#comboBoxUsers").val(offices).trigger("chosen:updated");
                     $chosen = $("#comboBoxUsers").chosen();
                     var chosen = $chosen.data("chosen");
                     var _fn = chosen.result_select;
                     chosen.result_select = function (evt) {
                         evt["metaKey"] = true;
                         evt["ctrlKey"] = true;
                         chosen.result_highlight.addClass("result-selected");
                         return _fn.call(chosen, evt);
                     };
                     $('.search-choice-close').click(function () {
                         UnAssignOffice($(this), uid);
                     });
                 },
                 fail: function (data) {
                     alert("Failed to get Offices. Please try again!");
                 }
             });
         }

         function OnBtnSaveClick() {
             showLoader();
             var children = $('.chosen-choices').children();
             var users = [];
             if (children.length > 1) {
                 for (var i = 0; i < children.length - 1; i++) {
                     if ($(children[i])) {
                         users.push({ 'UID': $($('#comboBoxUsers').find('option')[$(children[i]).find('a').attr('data-option-array-index')]).val() });
                     }
                 }
             }
             $("#" + '<%=hdnMultiUserIds.ClientID %>').val(JSON.stringify(users));
             return true;
         }


         function GetPublicHolidays() {
             $.ajax(
             {
                 type: "POST",
                 url: '../Office/ViewUsers.aspx/GetPublicHolidays',
                 contentType: "application/json; charset=utf-8",
                 dataType: "json",
                 success: function (result) {
                     if (result.d.length > 0) {
                         var holidays = JSON.parse(result.d);
                         for (var i = 0; i < holidays.length; i++) {
                             var container = $('#publicHolidayList');
                             $('<label for="cb' + holidays[i].HolidayID + '"><input type="checkbox" id="cb' + holidays[i].HolidayID + '" value=' + holidays[i].HolidayID + '> ' + holidays[i].Name.trim() + '</label>').appendTo(container);
                         }
                         if (isShowHolidays) {
                             var arrHolidays = $('#' + '<%=hdnPublicHolidays.ClientID%>').val().split(',');
                             var checkboxes = $("#publicHolidayList").find("input[type=checkbox]");
                             if (checkboxes.length > 0) {
                                 for (var i = 0; i < checkboxes.length; i++) {
                                     for (var j = 0; j < arrHolidays.length; j++) {
                                         if ($(checkboxes[i]).val() == arrHolidays[j].trim()) {
                                             $(checkboxes[i]).prop("checked", true);
                                             break;
                                         }
                                         else
                                             $(checkboxes[i]).prop("checked", false);
                                     }
                                 }
                             }
                         }
                     }
                 },
                 error: function (result) {
                 }
             });
         }

         $(document).on('click', '#<%= mdlPopup.ClientID%>', function (e) {
            e.preventDefault();
            disableScrollbar();
        });

        function pageLoad() {
            if ($('input[id$="chkIPAdd"]').prop("checked") && $('#' + '<%=hdnIPAddresses.ClientID%>').val() != undefined && $('#' + '<%=hdnIPAddresses.ClientID%>').val() != "" && $('#' + '<%=hdnIPAddresses.ClientID%>').val() != null) {
                 $("#tblIPAdd, #divIPAdd").show();
                 SetIPAddresses();
             }
             else if (!$('input[id$="chkIPAdd"]').prop("checked") && $('#' + '<%=hdnIPAddresses.ClientID%>').val() != undefined && $('#' + '<%=hdnIPAddresses.ClientID%>').val() != "" && $('#' + '<%=hdnIPAddresses.ClientID%>').val() != null) {
                 $("#tblIPAdd, #divIPAdd").hide();
                 SetIPAddresses();
             }
             else
                 $("#tblIPAdd, #divIPAdd").hide();

             if ($('input[id$="chkTimeSpan"]').prop("checked") && $('#' + '<%=hdnTimeRest.ClientID%>').val() != undefined && $('#' + '<%=hdnTimeRest.ClientID%>').val() != "" && $('#' + '<%=hdnTimeRest.ClientID%>').val() != null) {
                 $("#tblLogin, #divTimeRest, .divPubHoliday").show();
                 isShowHolidays = true;
                 GetPublicHolidays();
                 SetTimeRestrictions();

             }
             else if (!$('input[id$="chkTimeSpan"]').prop("checked") && $('#' + '<%=hdnTimeRest.ClientID%>').val() != undefined && $('#' + '<%=hdnTimeRest.ClientID%>').val() != "" && $('#' + '<%=hdnTimeRest.ClientID%>').val() != null) {
                 $("#tblLogin, #divTimeRest, .divPubHoliday").hide();
                 isShowHolidays = true;
                 GetPublicHolidays();
                 SetTimeRestrictions();
             }
             else
                 $("#tblLogin, #divTimeRest, .divPubHoliday").hide();

             //$("#comboBoxUsers").change(function () {
             //    $('.search-choice-close').click(function () {
             //        UnAssignUser($(this));
             //    });
             //});
         }

         function CleanHdnValues() {
             $('#' + '<%=hdnIPAddresses.ClientID%>').val("");
             $('#' + '<%=hdnClose.ClientID%>').click();
         }

         function OnChkIPAddClick() {
             if ($('input[id$="chkIPAdd"]').prop("checked")) {
                 $("#tblIPAdd, #divIPAdd").show();

             }
             else
                 $("#tblIPAdd, #divIPAdd").hide();
             return false;
         }

         function onChkAllDaysClick() {
             $("#tblLogin").find('tr').each(function (i, el) {
                 var chkWeek = $(el).children().find(".chkWeekDays");
                 var allDays = $($('table#tblLogin tr')).find(".chkAllDays").parent().parent();
                 if (!chkWeek.hasClass("chkAllDays") && chkWeek.length > 0) {
                     $(el).children().find(".chkWeekDays").prop("checked", $(".chkAllDays").prop("checked"));

                     if ($(".chkAllDays").prop("checked")) {
                         $(el).find("input, select, checkbox").prop("disabled", true);
                         $($('table#tblLogin tr')[i]).find(".txtFromHour").val($(allDays).find(".txtFromHour").val());
                         $($('table#tblLogin tr')[i]).find(".txtFromMin").val($(allDays).find(".txtFromMin").val());
                         $($('table#tblLogin tr')[i]).find(".txtToHour").val($(allDays).find(".txtToHour").val());
                         $($('table#tblLogin tr')[i]).find(".txtToMin").val($(allDays).find(".txtToMin").val());
                         $($('table#tblLogin tr')[i]).find(".optFrom").val($(allDays).find(".optFrom").val());
                         $($('table#tblLogin tr')[i]).find(".optTo").val($(allDays).find(".optTo").val());
                     }
                     else {
                         $(el).find("input, select, checkbox").prop("disabled", false);
                     }
                 }
             });
         }

         function ValidateHourMin(ele) {
             var val = $(ele).val();
             if (!val.match(numberRegExp)) {
                 alert("Only numbers are allowed!");
                 $(ele).val("").focus();
             }
             else if (($(ele).hasClass("txtFromHour") || $(ele).hasClass("txtToHour")) && (parseInt(val) > 12 || parseInt(val) <= 0)) {
                 alert("Please enter valid hours between 1 to 12.")
                 $(ele).val("").focus();
             }
             else if (($(ele).hasClass("txtFromMin") || $(ele).hasClass("txtToMin")) && (parseInt(val) > 59 || parseInt(val) < 0)) {
                 alert("Please enter valid minutes between 0 to 59.")
                 $(ele).val("").focus();
             }
             return false;
         }

         function selectAllCheckBoxes() {
             if ($('.JchkAll').is(':checked')) {
                 $('input:checkbox').not($('.JchkAll')).prop('checked', true);
             }
             else {
                 $('input:checkbox').not($('.JchkAll')).prop('checked', false);
             }
         }

         function CollectData() {
             var checkArray = [];
             $('input:checkbox').not($('.JchkAll')).each(function () {
                 var isCheckedOrNot = $(this).prop("checked");
                 var uid = $(this).siblings('a').last().text();
                 checkArray.push({ 'UID': uid, 'IsHideLockedTabs': isCheckedOrNot });
             })
         }

         function OnChkTimeRestClick() {
             if ($('input[id$="chkTimeSpan"]').prop("checked")) {
                 $("#tblLogin, #divTimeRest, .divPubHoliday").show();

             }
             else
                 $("#tblLogin, #divTimeRest, .divPubHoliday").hide();
         }

         function SaveIPAddr() {
             var csvIPAddr = "";
             for (i = 0; i < $('table#tblIPAdd tr').length; i++) {
                 var $item = $($('table#tblIPAdd tr')[i]);
                 if (($item.find('#txtIPAdd').val() != null && $item.find('#txtIPAdd').val() != ""))
                     csvIPAddr = csvIPAddr + $item.find('#txtIPAdd').val() + ",";
             }
             csvIPAddr = csvIPAddr.substr(0, csvIPAddr.lastIndexOf(','));
             if (csvIPAddr != "")
                 $('#' + '<%=hdnIPAddresses.ClientID%>').val("");
            else {
                alert("Please enter at least one IP Address to restrict user!");
                return;
            }
            $('#' + '<%=hdnIPAddresses.ClientID%>').val(csvIPAddr);
            return false;
        }


        function CancelIPAddr() {
            $("#tblIPAdd").find("tr:gt(0)").remove();
            var row = $(".indexRow");
            $(row).find("#txtIPAdd").val("");
            $(row).find("#btnAddIPAddr").parent().show();
            $(row).find("#btnRemoveIPAddr").parent().hide();
            $(row).find(".hdnIPId").val(0);
            $('#' + '<%=hdnIPAddresses.ClientID%>').val("");
        }

        function SetIPAddresses() {
            $("#tblIPAdd").find("tr:gt(0)").remove();
            var row = $(".indexRow");
            $(row).find("#txtIPAdd").val("");
            $(row).find("#btnAddIPAddr").parent().show();
            $(row).find("#btnRemoveIPAddr").parent().hide();
            var arrIP = $('#' + '<%=hdnIPAddresses.ClientID%>').val().split(',');
            $(row).find(".hdnIPId").val(0);
            for (var i = 0; i < arrIP.length; i++) {
                if (i == 0) {
                    SetIPValues(arrIP[i], true, i);
                }
                else
                    AppendIndexDetails(arrIP[i], i);
            }
        }

        function SetTimeRestrictions() {
            var arrTime = JSON.parse($('#' + '<%=hdnTimeRest.ClientID%>').val());
            $("#tblLogin").find('tr').each(function (i, el) {
                $(el).find(".txtFromHour").val(12);
                $(el).find(".txtToHour").val(11);
                $(el).find(".optFrom").val("AM");
                $(el).find(".optTo").val("PM");
                $(el).find("checkbox").prop("checked", false);
                if ($(el).find("span").text() != "" && $(el).find("span").text() != undefined) {
                    for (var i = 0; i < arrTime.length; i++) {
                        var day = $(el).find("span").text();
                        day = day.replace(' ', '');
                        if (WeekDays[day] == arrTime[i].Day) {
                            $(el).find(".chkWeekDays").prop("checked", true);
                            $(el).find(".txtFromHour").val(arrTime[i].From.split(':')[0]);
                            $(el).find(".txtFromMin").val(arrTime[i].From.split(':')[1].split(' ')[0]);
                            $(el).find(".txtToHour").val(arrTime[i].To.split(':')[0]);
                            $(el).find(".txtToMin").val(arrTime[i].To.split(':')[1].split(' ')[0]);
                            $(el).find(".optFrom").val(arrTime[i].From.split(':')[1].split(' ')[1]);
                            $(el).find(".optTo").val(arrTime[i].To.split(':')[1].split(' ')[1]);
                        }
                    }
                }
            });
        }

        function SaveTimeRest() {
            var arrTimeRest = [];

            var isCopy = false;
            for (i = 0; i < $('table#tblLogin tr').length; i++) {
                var day = $($('table#tblLogin tr')[i]).find("span").text();
                var fromHour = $($('table#tblLogin tr')[i]).find(".txtFromHour").val();
                var fromMin = $($('table#tblLogin tr')[i]).find(".txtFromMin").val();
                var toHour = $($('table#tblLogin tr')[i]).find(".txtToHour").val();
                var toMin = $($('table#tblLogin tr')[i]).find(".txtToMin").val();
                var fromTime = $($('table#tblLogin tr')[i]).find(".optFrom").val();
                var toTime = $($('table#tblLogin tr')[i]).find(".optTo").val();
                if ($($('table#tblLogin tr')[i]).find(".chkAllDays").length > 0 && $($('table#tblLogin tr')[i]).find(".chkWeekDays").prop("checked")) {
                    day = day.replace(' ', '');
                    arrTimeRest.push({ "Day": WeekDays[day], "From": fromHour + ":" + fromMin + " " + fromTime, "To": toHour + ":" + toMin + " " + toTime });
                    break;
                }
                else if ($($('table#tblLogin tr')[i]).find(".chkWeekDays").prop("checked")) {
                    arrTimeRest.push({ "Day": WeekDays[day], "From": fromHour + ":" + fromMin + " " + fromTime, "To": toHour + ":" + toMin + " " + toTime });
                }
            }

            var checkboxes = $("#publicHolidayList").find("input[type=checkbox]");
            var publicHolidays = "";
            if (checkboxes.length > 0) {
                for (var i = 0; i < checkboxes.length; i++) {
                    if ($(checkboxes[i]).prop("checked"))
                        publicHolidays += $(checkboxes[i]).val() + ", ";
                }
                publicHolidays = publicHolidays.substr(0, publicHolidays.lastIndexOf(','));
            }

            if (arrTimeRest.length <= 0 && publicHolidays == "") {
                alert("Please select either public holidays or weekdays to restrict user!");
                return;
            }
            if (arrTimeRest.length > 0)
                $("#" + '<%=hdnTimeRest.ClientID %>').val("");

            $("#" + '<%=hdnTimeRest.ClientID %>').val(JSON.stringify(arrTimeRest));


            if (publicHolidays != "")
                $("#" + '<%=hdnPublicHolidays.ClientID %>').val("");
            $("#" + '<%=hdnPublicHolidays.ClientID %>').val(publicHolidays);
        }

        function SetIPValues(ip, isRender, index) {
            var row = "";
            if (!isRender)
                row = document.getElementById("indexRow" + index);
            else
                row = $(".indexRow");
            $(row).find("#txtIPAdd").val(ip);
            $(row).find(".hdnIPId").val(index);
        }

        function AppendIndexDetails(ip, index) {
            CreateDynamicIPAddTextBox(index, true);
            SetIPValues(ip, false, index);
        }

        function GetDynamicTextBox(value) {
            var test = '<td class="w0"><input type="hidden" value="0" class="hdnIPId"></td>' +
                        '<td style="width:100%;"><input type="text" id="txtIPAdd"  style="width:100%;" maxlength="100" placeholder="IP Address" /></td>' +
                        '<td class="w36"><button id="btnAddIPAddr" class="btnIPAddr" type="button" width="90%" onclick="return AddIPAddTextBoxes(this)"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/add-icon.png" title="Add" /></button></td>' +
                        '<td class="w36"><button id="btnRemoveIPAddr" class="btnIPAddr" type="button" width="90%" onclick="return RemoveIPAddTextBoxes(this)"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" /></button></td>'
            return test;
        }

        function CreateDynamicIPAddTextBox(index, isRender) {
            var tr = document.createElement('tr');
            tr.innerHTML = GetDynamicTextBox(index);

            if (!isRender)
                tr.setAttribute("id", "indexRow" + (index = index + 1));
            else
                tr.setAttribute("id", "indexRow" + index);
            $('table#tblIPAdd tr:last').after(tr);
            SetIPAddButtonVisibility();

            //Allow maximum of 5 key,value pairs
            if ($('table#tblIPAdd tr').length == 5) {
                $(tr).find('#btnAddIPAddr').parent().hide();
            }
        }

        function RemoveIPAddTextBoxes(ele) {
            $(ele).closest('tr').remove();
            SetIPAddButtonVisibility();
            return false;
        }

        function SetIPAddButtonVisibility() {
            var rowCount = $('table#tblIPAdd tr').length;
            for (i = 0; i < rowCount; i++) {
                var $item = $($('table#tblIPAdd tr')[i]);

                if (i == (rowCount - 1)) {
                    $item.find('#btnRemoveIPAddr').parent().show();
                    $item.find('#btnAddIPAddr').parent().show();
                    if (i == 0) {
                        $item.find('#btnRemoveIPAddr').parent().hide();
                    }
                }
                else {
                    $item.find('#btnRemoveIPAddr').parent().show();
                    $item.find('#btnAddIPAddr').parent().hide()
                }
            }
        }

        var pubExpanded = false;
        function showCheckboxesPublicHolidays() {
            var checkboxes = document.getElementById("publicHolidayList");
            if (!pubExpanded) {
                checkboxes.style.display = "block";
                pubExpanded = true;
            } else {
                checkboxes.style.display = "none";
                pubExpanded = false;
            }
        }

        function AddIPAddTextBoxes(ele) {
            var index = parseInt($($(ele).parent().parent().find('[class*=hdnIPId]')).val());
            var ipAddr = $(ele).closest('tr').find('#txtIPAdd').val();
            if (ipAddr == "") {
                alert("Please enter IP Address.");
                return false;
            }
            else if (!ipAddr.match(ipAddRegEx) && (!ipAddr.match(ipAddRegEx6))) {
                alert("Please enter valid IP Address.");
                return false;
            }
            CreateDynamicIPAddTextBox(index, false);
            return false;
        }

        function ShowConfirmPwdDialog() {
            $("#" + '<%=hdnNewPwd.ClientID%>').val($("#" + '<%=txtNewPassword.ClientID%>').val());
            $("#" + '<%=hdnConfirmNewPwd.ClientID%>').val($("#" + '<%=txtNewConfirmPassword.ClientID%>').val());
            $("#Panel1").css("display", "none");
            $("#DivUpdatePwdConfirmation").css("display", "block");
        }

        function ShowUpdatePwdPanel(userid) {
            showLoader();
            $("#" + '<%=hdnUserID.ClientID%>').val($(userid).text());
            $("#Panel1 :input:password").val("");
            $("#Panel1").css("display", "block");
        }

        $("#btnUpdatePwdCancel").click(function () {
            $("#DivUpdatePwdConfirmation").css("display", "none");
            hideLoader();
        });

        $("#ButtonDisableCancel").click(function () {
            $("#DivDisableConfirmation").css("display", "none");
            hideLoader();
        });

        $("#" + '<%= btnCancel.ClientID%>').click(function (e) {
            e.preventDefault();
            $("#Panel1").css("display", "none");
            hideLoader();
            return false;
        });

        $("#btnUpdatePwdOkay").click(function () {
            showLoader();
            var confirmPassword = $("#" + '<%=hdnConfirmNewPwd.ClientID%>').val();
            var newPassword = $("#" + '<%=hdnNewPwd.ClientID%>').val();

            $("#" + '<%=hdnConfirmNewPwd.ClientID%>').val("");
            $("#" + '<%=hdnNewPwd.ClientID%>').val("");
            $("#" + '<%=txtNewPassword.ClientID%>').val("");
            $("#" + '<%=txtNewConfirmPassword.ClientID%>').val("");

            var pwdRegExp = /^\w{7,20}$/;
            var isSuccess = true;
            if (newPassword == "" || newPassword == undefined) {
                alert("Please enter New Password!");
                isSuccess = false;
            }
            else if (!pwdRegExp.test(newPassword)) {
                alert("Please enter valid New Password! (Password must be between 7 to 20 characters)");
                isSuccess = false;
            }
            else if (confirmPassword == "" || confirmPassword == undefined) {
                alert("Please enter Confirm Password!");
                isSuccess = false;
            }
            else if (!pwdRegExp.test(confirmPassword)) {
                alert("Please enter valid Confirm Password! (Password must be between 7 to 20 characters)");
                isSuccess = false;
            }
            else if (newPassword != confirmPassword) {
                alert("New Password and Confirm Password should be same!");
                isSuccess = false;
            }

            if (!isSuccess) {
                $("#DivUpdatePwdConfirmation").css("display", "none");
                $("#Panel1").css("display", "block");
                return false;
            }

            $.ajax(
            {
                type: "POST",
                url: '../Office/ViewUsers.aspx/UpdatePassword',
                contentType: "application/json; charset=utf-8",
                data: "{ confirmPassword:'" + confirmPassword + "', newPassword:'" + newPassword + "', UID:'" + $("#" + '<%=hdnUserID.ClientID%>').val() + "'}",
                dataType: "json",
                success: function (result) {
                    if (result.d == true) {
                        alert("Password updated successfully!");
                        hideLoader();
                        $("#DivUpdatePwdConfirmation").css("display", "none");
                    }
                    else {
                        alert("Failed to update password. Please try again!");
                        hideLoader();
                        $("#DivUpdatePwdConfirmation").css("display", "none");
                    }
                },
                error: function (result) {
                }
            });
        });

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
        }

        function ChangeUserMode(ele) {
            showLoader();
            var isDisable = $(ele).attr('Data-IsDisable');
            $('#<%= hdnUserID.ClientID %>').val($(ele).text());
            $('#<%= hdnUserMode.ClientID %>').val(isDisable);
            $()
            if (isDisable == "True") {
                $("#PopupDisableHeader").text("Enable Subuser");
                $("#disableText").text("Are you sure you want to enable this subuser?");
            }
            else {
                $("#PopupDisableHeader").text("Disable Subuser");
                $("#disableText").text("Are you sure you want to disable this subuser? Once you disable this user, he/she will not access his/her EdFiles account.");
            }
            $("#DivDisableConfirmation").css("display", "block");
            return false;
        }
    </script>
</asp:Content>
