using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Framework.Controls;
using Shinetech.Utility;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;
using System.Globalization;
using System.Linq;
using Account = Shinetech.DAL.Account;
using Newtonsoft.Json;
using System.Web.Services;
using System.Web.Script.Serialization;

public partial class ViewUsers : LicensePage
{
    private static readonly string PDFPATH = ConfigurationManager.AppSettings["PdfPath"];
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
    private static string UserId = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetData();//重新获取操作后的数据源
            BindGrid();//绑定GridView,为删除服务

        }
    }

    #region 分页
    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        if (this.DataSource != null)
        {
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        if (this.DataSource != null)
        {
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource_Users"] as DataTable;
        }
        set
        {
            this.Session["DataSource_Users"] = value;
        }
    }


    #endregion

    #region 数据绑定
    /// <summary>
    /// 获取数据源
    /// </summary>
    public void GetData()
    {
        if (User.IsInRole("Administrators"))
        {
            this.DataSource = UserManagement.GetOfficeUserList();
            //DataTable officeUsersList = UserManagement.GetWebUserList(Membership.GetUser().ProviderUserKey.ToString());
            //if (officeUsersList != null && officeUsersList.Columns.Contains("IsHideLockedTabs"))
            //{
            //    foreach (DataRow dr in officeUsersList.Rows)
            //    {
            //        if (dr["IsHideLockedTabs"].ToString() == "" || dr["IsHideLockedTabs"].ToString() == null)
            //            dr["IsHideLockedTabs"] = false;
            //    }
            //}
            //this.DataSource = officeUsersList;
        }
        else
        {
            this.DataSource = UserManagement.GetWebUserList(Sessions.SwitchedSessionId);
            foreach (DataRow drRow in this.DataSource.Rows)
            {
                if (string.IsNullOrEmpty(Convert.ToString(drRow["HasPrivilegeDownload"])))
                    drRow["HasPrivilegeDownload"] = true;
            }
            //this.DataSource = UserManagement.GetWebUserList(Membership.GetUser().ProviderUserKey.ToString());
            //DataTable usersList = UserManagement.GetWebUserList(Membership.GetUser().ProviderUserKey.ToString());
            //if (usersList != null && usersList.Columns.Contains("IsHideLockedTabs"))
            //{
            //    foreach (DataRow dr in usersList.Rows)
            //    {
            //        if (dr["IsHideLockedTabs"].ToString() == "" || dr["IsHideLockedTabs"].ToString() == null)
            //            dr["IsHideLockedTabs"] = false;
            //    }
            //}
            //this.DataSource = usersList;
        }
    }
    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }
    #endregion

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();//重新设置数据源，绑定
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    public void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;
        string uid = Convert.ToString(this.GridView1.DataKeys[row.RowIndex].Value);

        DataTable DataSourceIPAddr = General_Class.GetIPAddrForSubUser(uid);
        DataTable DataSourceTimeRest = General_Class.GetTimeRestForSubUser(uid);
        DataTable DataSourcePubHoliday = General_Class.GetPublicHolidayRestForSubUser(uid);
        DataView view = new DataView(this.DataSource as DataTable);
        view.RowFilter = "UID='" + uid + "'";
        dvCustomerDetail.DataSource = view;

        dvCustomerDetail.DataBind();
        DetailsView dsDetailsView = updPnlCustomerDetail.FindControl("dvCustomerDetail") as DetailsView;
        DropDownList drpStateIN = dsDetailsView.FindControl("drpState") as DropDownList;
        DropDownList drpCountryIN = dsDetailsView.FindControl("drpCountry") as DropDownList;
        drpStateIN.SelectedValue = (view[0]["StateID"].ToString() != "" && view[0]["StateID"].ToString() != "  " && view[0]["StateID"].ToString() != null) ? view[0]["StateID"].ToString() : "CA";
        drpCountryIN.SelectedValue = (view[0]["CountryID"].ToString() != "" && view[0]["CountryID"].ToString() != "  " && view[0]["CountryID"].ToString() != null) ? view[0]["CountryID"].ToString() : "US";
        view.RowStateFilter = DataViewRowState.ModifiedCurrent;
        CheckBox chkIP = dsDetailsView.FindControl("chkIPAdd") as CheckBox;
        if (chkIP != null)
        {
            DataSourceIPAddr.AsEnumerable().ToList().ForEach(d =>
            {
                chkIP.Checked = d.Field<bool>("IsIPRestricted");
                hdnIPAddresses.Value = d.Field<string>("IPAddresses").ToString();
            });
        }
        CheckBox chkTimeSpan = dsDetailsView.FindControl("chkTimeSpan") as CheckBox;
        if (chkTimeSpan != null)
        {
            List<LoginTimeRestriction> loginRestrictions = new List<LoginTimeRestriction>();
            DataSourceTimeRest.AsEnumerable().ToList().ForEach(d =>
            {
                chkTimeSpan.Checked = d.Field<bool>("IsTimeRestrictied");
                LoginTimeRestriction loginRest = new LoginTimeRestriction();

                loginRest.Day = d.Field<byte>("LoginDay"); //tinyint datattype in sql
                var from = DateTime.Today.Add(d.Field<TimeSpan>("FromTime"));
                loginRest.From = from.ToString("hh:mm tt", CultureInfo.InvariantCulture);
                var to = DateTime.Today.Add(d.Field<TimeSpan>("ToTime"));
                loginRest.To = to.ToString("hh:mm tt", CultureInfo.InvariantCulture);
                loginRest.IsTimeRestrictied = d.Field<bool>("IsTimeRestrictied");
                loginRest.UID = uid;
                loginRestrictions.Add(loginRest);
            });
            hdnTimeRest.Value = JsonConvert.SerializeObject(loginRestrictions, Formatting.Indented,
                 new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Include });

            DataSourcePubHoliday.AsEnumerable().ToList().ForEach(d =>
            {
                hdnPublicHolidays.Value = d.Field<string>("HolidayID").ToString();
            });
        }

        CheckBox chkTabAccess = dsDetailsView.FindControl("chkTabAccess") as CheckBox;
        if (chkTabAccess != null)
        {
            DataTable dtTabAccess = General_Class.GetTabRestForSubUser(uid);
            dtTabAccess.AsEnumerable().ToList().ForEach(d =>
            {
                chkTabAccess.Checked = d.Field<bool>("IsHideLockedTabs");
            });
        }

        updPnlCustomerDetail.Update();
        this.mdlPopup.Show();
        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNo", "LoadUsers('" + uid + "')", true);
    }

    protected void LinkButton3_Click(object sender, CommandEventArgs e)
    {
        string uid = e.CommandArgument.ToString();
        Account account = new Account(uid);

        theUserText.Value = uid;
        UserId = uid;

        if (this.Page.User.IsInRole("Offices"))
        {
            Account officeUser = new Account(Sessions.SwitchedSessionId);
            //Account officeUser = new Account(Membership.GetUser().ProviderUserKey.ToString());
            string groupsName = officeUser.GroupsName.Value;

            if (groupsName != null && !string.IsNullOrEmpty(groupsName.Trim()))
            {
                string[] groupNames = groupsName.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
                string p = account.Groups.Value == null ? "" : account.Groups.Value;
                for (int i = 1; i <= groupNames.Length; i++)
                {
                    TextBox groupText = UpdatePanel2.FindControl("grouplink" + i) as TextBox;
                    if (groupText != null)
                        groupText.Text = groupNames[i - 1];

                    CheckBox groupBox = UpdatePanel2.FindControl("groupfield" + i) as CheckBox;
                    if (groupBox != null)
                        groupBox.Checked = p.Contains(i.ToString());
                }
            }
        }
        eFileFolderJSONWS.InsertUserLogs(uid, Enum_Tatva.UserLogs.EditGroup.GetHashCode());
        UpdatePanel2.Update();
        this.mdlPopup2.Show();
    }

    private bool BelongToGroup(string p, string[] groups)
    {
        foreach (string group in groups)
        {
            if (p.Contains(group))
            {
                return true;
            }
        }

        return false;
    }

    protected void LinkButton2_Click(Object sender, CommandEventArgs e)
    {
        string uid = e.CommandArgument.ToString();
        Account account = new Account(uid);


        if (account.IsExist)
        {
            try
            {
                MembershipUser user = Membership.GetUser(account.Name.Value);
                Membership.DeleteUser(account.Name.Value);
                account.Delete();

                try
                {
                    General_Class.DeleteLoginRestriction(uid);
                }
                catch (Exception ex)
                {
                    Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
                }
                DataTable table = FileFolderManagement.GetFileFoldersByUserID(uid);

                foreach (DataRow item in table.Rows)
                {
                    try
                    {
                        string folderPath = "~/" + PDFPATH + "/" + item["FolderID"];
                        if (Directory.Exists(Server.MapPath(folderPath)))
                        {
                            Directory.Delete(Server.MapPath(folderPath), true);
                        }

                        FileFolderManagement.DelFileFolderByFolderID(item["FolderID"].ToString());
                    }
                    catch (Exception ex)
                    {
                        Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
                    }
                }
                eFileFolderJSONWS.InsertUserLogs(uid, Enum_Tatva.UserLogs.Delete.GetHashCode());
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            }
            GetData();
            BindGrid();
            UpdatePanel1.Update();
        }
    }

    protected void btnSave2_Click(object sender, EventArgs e)
    {

        string groups = string.Empty;
        string groupsName = string.Empty;

        for (int i = 1; i <= 5; i++)
        {
            CheckBox groupBox = UpdatePanel2.FindControl("groupfield" + i) as CheckBox;
            TextBox groupText = UpdatePanel2.FindControl("grouplink" + i) as TextBox;
            groupsName += string.Format("{0}|", string.IsNullOrEmpty(groupText.Text.Trim()) ? "Group " + i : groupText.Text.Trim());

            if (groupBox.Checked)
            {
                groups += i + "|";
            }
        }

        Account account = new Account(string.IsNullOrEmpty(theUserText.Value) ? UserId : theUserText.Value);

        //如果sub user创建的folder的private group被officeuser取消了，怎么办？ 卢远宗 2014-10-7

        account.Groups.Value = groups;

        account.Update();
        //eFileFolderJSONWS.InsertUserLogs(
        this.mdlPopup2.Hide();
        ScriptManager.RegisterClientScriptBlock(UpdatePanel2, typeof(UpdatePanel), "alertok", "alert(\"Groups have been saved successfully.\")", true);
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string uid = this.dvCustomerDetail.DataKey.Value.ToString();
        DataView dataTable = new DataView(this.DataSource);
        dataTable.RowFilter = "UID='" + uid + "'";
        dataTable.Sort = "UID";
        DataRowView drView = dataTable.FindRows(uid)[0];
        DetailsView dsDetailsView = updPnlCustomerDetail.FindControl("dvCustomerDetail") as DetailsView;

        try
        {
            drView.BeginEdit();

            Shinetech.DAL.Account account = new Shinetech.DAL.Account(uid);

            if (!account.IsExist) return;
            CheckBox chkIP = dsDetailsView.FindControl("chkIPAdd") as CheckBox;
            if (chkIP != null)
            {
                General_Class.InsertLoginRestriction(uid, chkIP.Checked, hdnIPAddresses.Value);
            }
            CheckBox chkTimeSpan = dsDetailsView.FindControl("chkTimeSpan") as CheckBox;
            if (chkTimeSpan != null)
            {
                DataTable dt = FileFolderManagement.PrepareLoginRestStruct(hdnTimeRest.Value, uid, chkTimeSpan.Checked);
                General_Class.UpdateTimeRestForSubUser(dt, uid);
                General_Class.InsertPublicHolidayRestriction(uid, chkTimeSpan.Checked, hdnPublicHolidays.Value);
            }
            CheckBox chkTabAccess = dsDetailsView.FindControl("chkTabAccess") as CheckBox;
            if (chkTabAccess != null)
                General_Class.UpdateTabAccessRestriction(uid, chkTabAccess.Checked);

            foreach (DetailsViewRow item in dvCustomerDetail.Rows)
            {
                #region Edit DataRow
                TableCell cell = item.Controls[1] as TableCell;
                if (cell != null && cell.HasControls())
                {
                    TextBox textBox;
                    DropDownList dropDownList;
                    switch (item.RowIndex)
                    {
                        case 1: //Firstname
                            textBox = cell.FindControl("textBoxFirstname") as TextBox;
                            drView["Firstname"] = textBox.Text.Trim();
                            account.Firstname.Value = textBox.Text.Trim();
                            break;
                        case 2: //Lastname
                            textBox = cell.FindControl("textBoxLastname") as TextBox;
                            drView["Lastname"] = textBox.Text.Trim();
                            account.Lastname.Value = textBox.Text.Trim();
                            break;
                        //case 3: //DOB 
                        //    textBox = cell.Controls[1] as TextBox;
                        //    textBox.Text = textBox.Text.Replace('-', '/');
                        //    drView["DOB"] = DateTime.ParseExact(textBox.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                        //    account.DOB.Value = DateTime.ParseExact(textBox.Text.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                        //    break;
                        case 3: //IsActive 
                            CheckBox checkBox = cell.Controls[0] as CheckBox;
                            bool isActive = (bool)drView["IsApproved"];
                            if (checkBox.Checked != isActive)
                            {
                                drView["IsApproved"] = checkBox.Checked;
                                MembershipUser user = Membership.GetUser(account.Name.Value);
                                if (user != null)
                                {
                                    user.IsApproved = checkBox.Checked;
                                    Membership.UpdateUser(user);
                                }
                            }
                            break;
                        case 4: //HasPrivilegeDelete 
                            CheckBox checkBox1 = cell.Controls[0] as CheckBox;
                            bool hasPrivilegeToDelete = (bool)drView["HasPrivilegeDelete"];
                            if (checkBox1.Checked != hasPrivilegeToDelete)
                            {
                                drView["HasPrivilegeDelete"] = checkBox1.Checked;
                                account.HasPrivilegeDelete.Value = checkBox1.Checked;
                            }
                            break;
                        case 5: //HasPrivilegeDownload
                            CheckBox checkBox3 = cell.Controls[0] as CheckBox;
                            bool hasPrivilegeToDownload = string.IsNullOrEmpty(drView["HasPrivilegeDownload"].ToString()) ? true : (bool)drView["HasPrivilegeDownload"];
                            //if (checkBox3.Checked != hasPrivilegeToDownload)
                            //{
                            drView["HasPrivilegeDownload"] = checkBox3.Checked;
                            account.HasPrivilegeDownload.Value = checkBox3.Checked;
                            //}
                            break;
                        case 6: //ViewPrivilege 
                            CheckBox checkBox2 = cell.Controls[0] as CheckBox;
                            bool viewPrivilege = string.IsNullOrEmpty(drView["ViewPrivilege"].ToString()) ? false : (bool)drView["ViewPrivilege"];
                            if (checkBox2.Checked != viewPrivilege)
                            {
                                drView["ViewPrivilege"] = checkBox2.Checked;
                                account.ViewPrivilege.Value = checkBox2.Checked;
                            }
                            break;
                        case 7: //Email
                            textBox = cell.FindControl("textBoxEmail") as TextBox;
                            drView["Email"] = textBox.Text.Trim();
                            account.Email.Value = textBox.Text.Trim();
                            break;
                        case 8: //Telephone
                            textBox = cell.FindControl("textBoxPhone") as TextBox;
                            drView["Telephone"] = textBox.Text.Trim();
                            account.Telephone.Value = textBox.Text.Trim();
                            break;
                        case 9: //MobilePhone
                            textBox = cell.FindControl("textBoxMobile") as TextBox;
                            drView["MobilePhone"] = textBox.Text.Trim();
                            account.MobilePhone.Value = textBox.Text.Trim();
                            break;
                        case 10: //FaxNumber 
                            textBox = cell.FindControl("textBoxFax") as TextBox;
                            drView["FaxNumber"] = textBox.Text.Trim();
                            account.FaxNumber.Value = textBox.Text.Trim();
                            break;
                        case 11: //CompanyName
                            textBox = cell.FindControl("textBoxCompanyName") as TextBox;
                            drView["CompanyName"] = textBox.Text.Trim();
                            account.CompanyName.Value = textBox.Text.Trim();
                            break;
                        case 13: //Address 
                            textBox = cell.FindControl("textBoxAddress") as TextBox;
                            drView["Address"] = textBox.Text.Trim();
                            account.Address.Value = textBox.Text.Trim();
                            break;
                        case 14://City
                            textBox = cell.FindControl("textBoxCity") as TextBox;
                            drView["City"] = textBox.Text.Trim();
                            account.City.Value = textBox.Text.Trim();
                            break;
                        case 16://OtherState
                            textBox = cell.FindControl("textBoxOtherState") as TextBox;
                            drView["OtherState"] = textBox.Text.Trim();
                            account.OtherState.Value = textBox.Text.Trim();
                            break;
                        case 18://Postal
                            textBox = cell.FindControl("textBoxPostal") as TextBox;
                            drView["Postal"] = textBox.Text.Trim();
                            account.Postal.Value = textBox.Text.Trim();
                            break;
                        case 15://State
                            dropDownList = cell.FindControl("drpState") as DropDownList;
                            drView["StateID"] = dropDownList.SelectedValue;
                            account.StateID.Value = dropDownList.SelectedValue;
                            break;
                        case 17://Country
                            dropDownList = cell.FindControl("drpCountry") as DropDownList;
                            drView["CountryID"] = dropDownList.SelectedValue;
                            account.CountryID.Value = dropDownList.SelectedValue;
                            break;
                        case 19: //ViewPrivilege 
                            CheckBox checkBox4 = cell.Controls[0] as CheckBox;
                            bool securityProtected = string.IsNullOrEmpty(drView["HasProtectedSecurity"].ToString()) ? false : (bool)drView["hasProtectedSecurity"];
                            if (checkBox4.Checked != securityProtected)
                            {
                                drView["ViewPrivilege"] = checkBox4.Checked;
                                account.HasProtectedSecurity.Value = checkBox4.Checked;
                            }
                            break;
                        case 20: //ViewPrivilege 
                            CheckBox checkBox5 = cell.Controls[0] as CheckBox;
                            bool securityPublic = string.IsNullOrEmpty(drView["HasPublicSecurity"].ToString()) ? false : (bool)drView["hasPublicSecurity"];
                            if (checkBox5.Checked != securityPublic)
                            {
                                drView["ViewPrivilege"] = checkBox5.Checked;
                                account.HasPublicSecurity.Value = checkBox5.Checked;
                            }
                            break;
                        default:
                            break;
                    }

                }
                #endregion
            }
            #region AssignUser

            if (!string.IsNullOrEmpty(this.hdnMultiUserIds.Value))
            {
                DataTable dtUsers = new DataTable();
                dtUsers.Columns.Add("Index", typeof(int));
                dtUsers.Columns.Add("UID", typeof(string));
                AssignedUser[] userData = JsonConvert.DeserializeObject<List<AssignedUser>>(hdnMultiUserIds.Value).ToArray();
                int index = 1;
                foreach (var i in userData)
                {
                    if (!string.IsNullOrEmpty(i.UID))
                    {
                        dtUsers.Rows.Add(new object[] { index, i.UID });
                        index++;
                    }
                }
                //No need to pass switched user
                General_Class.AssignUser(uid, Membership.GetUser().ProviderUserKey.ToString(), dtUsers);
            }

            #endregion
            //submit to database
            UserManagement.UpdateUser(account);
            drView.EndEdit();
            dataTable.Table.AcceptChanges();
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNo", "hideLoader();", true);
            eFileFolderJSONWS.InsertUserLogs(uid, Enum_Tatva.UserLogs.Edit.GetHashCode());
        }
        catch (Exception ex)
        {
            drView.EndEdit();
            dataTable.Table.RejectChanges();
        }
        dsDetailsView.DataBind();
        this.GridView1.DataSource = this.DataSource;
        this.GridView1.DataBind();
        UpdatePanel1.Update();
        this.mdlPopup.Hide();
        hdnIPAddresses.Value = "";
        hdnPublicHolidays.Value = "";
        hdnTimeRest.Value = "";
    }



    public DataTable States
    {
        get
        {
            object obj = Application["States"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetStates();
                Application["States"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    public DataTable Countries
    {
        get
        {
            object obj = Application["Countries"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetCountries();
                Application["Countries"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }


    protected void GridView1_DataBinding(object sender, EventArgs e)
    {


    }
    protected void dvCustomerDetail_DataBound(object sender, EventArgs e)
    {
        DetailsView dsDetailsView = updPnlCustomerDetail.FindControl("dvCustomerDetail") as DetailsView;
        DropDownList drpStateIN = dsDetailsView.FindControl("drpState") as DropDownList;
        if (drpStateIN != null)
        {
            drpStateIN.DataSource = States;
            drpStateIN.DataValueField = "StateID";
            drpStateIN.DataTextField = "StateName";
            drpStateIN.DataBind();
        }

        DropDownList drpCountryIn = dsDetailsView.FindControl("drpCountry") as DropDownList;
        if (drpCountryIn != null)
        {
            drpCountryIn.DataSource = Countries;
            drpCountryIn.DataValueField = "CountryID";
            drpCountryIn.DataTextField = "CountryName";
            drpCountryIn.DataBind();
        }
    }

    [WebMethod]
    public static string GetPublicHolidays()
    {
        try
        {
            DataTable dt = General_Class.GetPublicHolidays();
            return JsonConvert.SerializeObject(dt);
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    [WebMethod]
    public static bool UpdatePassword(string confirmPassword, string newPassword, string UID)
    {
        try
        {

            if (PasswordGenerator.GetMD5(newPassword) != PasswordGenerator.GetMD5(confirmPassword))
                return false;
            UserManagement.UpdatePassword(PasswordGenerator.GetMD5(newPassword), UID);
            EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
            eFileFolderJSONWS.InsertUserLogs(UID, Enum_Tatva.UserLogs.ChangePassword.GetHashCode());
            return true;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            return false;
        }
    }
    //protected void saveLockedTabData_Click(object sender, EventArgs e)
    //{
    //    DataTable dtLocked = new DataTable();
    //    dtLocked.Columns.Add("UID", typeof(string));
    //    dtLocked.Columns.Add("IsHideLockedTabs", typeof(int));
    //    dtLocked.Columns.Add("Index", typeof(int));
    //    LockedTabUsers[] hideLockedTab = JsonConvert.DeserializeObject<List<LockedTabUsers>>(checkBoxData.Value).ToArray();
    //    int index = 1;
    //    foreach (var item in hideLockedTab)
    //    {
    //        if (!string.IsNullOrEmpty(item.UID))
    //        {
    //            dtLocked.Rows.Add(new object[] { item.UID, item.IsHideLockedTabs, index });
    //            index++;
    //        }
    //    }
    //    General_Class.UpdateTabAccessRestrictionSelected(dtLocked);
    //    GetData();
    //    BindGrid();
    //}
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (String.IsNullOrEmpty(e.Row.Cells[4].Text.ToString()) || e.Row.Cells[4].Text == "&nbsp;")
                e.Row.Visible = false;
            LinkButton libDisable = e.Row.FindControl("LinkButton5") as LinkButton;
            if (libDisable.CommandName == "True")
            {
                e.Row.Style.Add("Opacity", "0.6");
                libDisable.ToolTip = "Enable Subuser";
                libDisable.CssClass = libDisable.CssClass.Replace("ic-icon ic-disable", "ic-icon ic-disable-back");
            }
        }
    }

    //protected void LinkButton5_Command(object sender, CommandEventArgs e)
    //{
    //}

    protected void btnDisableOkay_Click(object sender, EventArgs e)
    {
        string uid = hdnUserID.Value;
        bool isDisable = string.IsNullOrEmpty(hdnUserMode.Value) ? true : !Convert.ToBoolean(hdnUserMode.Value);
        bool isSuccess = General_Class.ChangeUserMode(uid, isDisable);
        if (isSuccess)
            ScriptManager.RegisterStartupScript(UpdatePanel2, typeof(UpdatePanel), "alertok", "alert(\"Subuser has been" + (isDisable ? " disabled " : " enabled ") + "successfully.\")", true);
        else
            ScriptManager.RegisterStartupScript(UpdatePanel2, typeof(UpdatePanel), "alertok", "alert(\"Failed to update the subuser. Please try again later.\")", true);
        GetData();
        BindGrid();
    }

    protected void hdnClose_Click(object sender, EventArgs e)
    {
        dvCustomerDetail.DataBind();
    }
}
