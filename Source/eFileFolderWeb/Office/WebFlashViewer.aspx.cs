using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.DAL;

public partial class WebFlashViewer : LicensePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            string strID = Request.QueryString["ID"];
            if(strID!=null && strID.Trim().Length!=0)
            { 
                #region strID
                int id;
                string vcode =  "AAAAAAAAAAAAAAA";
                if(int.TryParse(strID, out id))
                {
                    #region TryParse
                    try
                    {
                        FileFolder folder = new FileFolder(id);
                        
                        if (folder.IsExist) // 
                        {
                            #region folder.IsExist

                            try
                            {
                                folder.LastDate.Value = DateTime.Now;
                                folder.Update();
                            }catch{}
                           

                            if (!User.Identity.IsAuthenticated) // 该用户为非登录用户
                            {
                                if(folder.SecurityLevel.Value==2) // public权限
                                {
                                    vcode = FlashViewer.FlashViewer_GetValidateCode(id, 1);
                                }
                            }
                            else // 否则 登录权限
                            {
                                //当前用户本身是office，或者书籍创建者就是用户自己
                                if (User.IsInRole("Offices") || Sessions.SwitchedSessionId.Equals(folder.OfficeID.Value) || Common_Tatva.IsUserSwitched())
                                {
                                    vcode = FlashViewer.FlashViewer_GetValidateCode(id, 1);
                                }
                                else // 当前用户为subuser,或者不是书籍创建者
                                {
                                    if (folder.SecurityLevel.Value == 2)//public
                                    {
                                        vcode = FlashViewer.FlashViewer_GetValidateCode(id, 1);
                                    }
                                    else if (folder.SecurityLevel.Value == 1)//protected
                                    {
                                        Account account = new Account(Sessions.SwitchedSessionId);
                                        //Account account = new Account(Membership.GetUser().ProviderUserKey.ToString());

                                        if (folder.OfficeID1.Value == account.OfficeUID.ToString())
                                        {
                                            vcode = FlashViewer.FlashViewer_GetValidateCode(id, 1);
                                        }
                                        
                                    }
                                    else if (folder.SecurityLevel.Value == 0)
                                    {
                                        // 不是自己创建的，则必须同组
                                        var uid = Sessions.SwitchedSessionId;
                                        //var uid = Membership.GetUser().ProviderUserKey.ToString();
                                        var ac = new Shinetech.DAL.Account(uid);
                                        string[] groups = ac.Groups.Value != null ? ac.Groups.Value.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries) : new string[] { };

                                        if (BelongToGroup(folder.Groups.Value, groups))
                                        {
                                            vcode = FlashViewer.FlashViewer_GetValidateCode(id, 1);
                                        }
                                    }
                                }
                            }

                            #endregion
                        }
                        else
                        {
                            Profile.LastEFFolders.RemoveItem(id);
                        }
                       
                    }catch
                    {
                        Response.Clear();
                        Response.Write("<script>window.close()</script>");
                        //Response.Redirect("~/ErrorPage.aspx",true);
                        return;
                    }

                    #endregion 

                    //打开的viewer版本
                    if (Request.QueryString["V"] == "2")
                    {
                        if (User.IsInRole("Offices") || Common_Tatva.IsUserSwitched())
                            Response.Redirect(string.Format("../EFileFolderV2.html?ID={0}&DATA={1}&IsEdit=true&IsDenied=false&t={2}", strID, vcode, DateTime.UtcNow.ToString("yyyyMMddHHmmssfff")), true);
                        else
                        {
                            DataTable dtTabAccess = General_Class.GetTabRestForSubUser(Sessions.SwitchedSessionId);
                            //DataTable dtTabAccess = General_Class.GetTabRestForSubUser(Membership.GetUser().ProviderUserKey.ToString());
                            if (dtTabAccess.Rows.Count > 0)
                            {
                                //Check whether sub user allowed to see locked tabs or not
                                bool isAccess = dtTabAccess.Rows[0].Field<bool>("IsHideLockedTabs");
                                Response.Redirect(string.Format("../EFileFolderV2.html?ID={0}&DATA={1}&IsEdit=true&IsDenied={2}&t={3}", strID, vcode, isAccess, DateTime.UtcNow.ToString("yyyyMMddHHmmssfff")), true);
                            }
                            else
                                Response.Redirect(string.Format("../EFileFolderV2.html?ID={0}&DATA={1}&IsEdit=true&IsDenied=false&t={2}", strID, vcode, DateTime.UtcNow.ToString("yyyyMMddHHmmssfff")), true);
                        }
                        //Response.Redirect(string.Format("CheckListQuestion.aspx?ID={0}&DATA={1}&V={2}", strID, vcode, Request.QueryString["V"]), true);
                    }
                    else
                    {
                        if (User.IsInRole("Offices") || Common_Tatva.IsUserSwitched())
                            Response.Redirect(string.Format("../EFileFolderV1.html?ID={0}&DATA={1}&IsEdit=true&IsDenied=false&t={2}", strID, vcode, DateTime.UtcNow.ToString("yyyyMMddHHmmssfff")), true);
                        else
                        {
                            DataTable dtTabAccess = General_Class.GetTabRestForSubUser(Sessions.SwitchedSessionId);
                            //DataTable dtTabAccess = General_Class.GetTabRestForSubUser(Membership.GetUser().ProviderUserKey.ToString());
                            if (dtTabAccess.Rows.Count > 0)
                            {
                                //Check whether sub user allowed to see locked tabs or not
                                bool isAccess = dtTabAccess.Rows[0].Field<bool>("IsHideLockedTabs");
                                Response.Redirect(string.Format("../EFileFolderV1.html?ID={0}&DATA={1}&IsEdit=true&IsDenied={2}&t={3}", strID, vcode, isAccess, DateTime.UtcNow.ToString("yyyyMMddHHmmssfff")), true);
                            }
                            else
                                Response.Redirect(string.Format("../EFileFolderV1.html?ID={0}&DATA={1}&IsEdit=true&IsDenied=false&t={2}", strID, vcode, DateTime.UtcNow.ToString("yyyyMMddHHmmssfff")), true);
                    }
                        //Response.Redirect(string.Format("CheckListQuestion.aspx?ID={0}&DATA={1}&V={2}", strID, vcode, Request.QueryString["V"]), true);
                    }
                }

                #endregion
            }
        }
    }

    private bool BelongToGroup(string p, string[] groups)
    {
        foreach (string group in groups)
        {
            if (p.Contains(group))
            {
                return true;
            }
        }

        return false;
    }
}
