<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Welcome.aspx.cs" Inherits="Office_Welcome" Title="" %>

<%@ Register Src="../Controls/FolderReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%--<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<script type="text/javascript" src="../Scripts/jquery-1.4.4.min.js"></script>--%>
    <!--BEGINNING OF chatstat.com CODE-->
    <%--<script type='text/javascript'>
    var chat = 1;
    var chat_div_id = "chat_div";
    </script>
    <script type='text/javascript' src='https://adlink.chatstat.com/JSBuilder.aspx?csuid=55627' defer='defer'></script><div id="chat_div"></div>
    <noscript></noscript>
    <!--END OF chatstat.com CODE-->--%>
    <script language="JavaScript" type="text/jscript">

        function getOs() {
            var OsObject = "";
            if (navigator.userAgent.indexOf("MSIE") > 0) {
                return "MSIE";
            }
            if (isFirefox = navigator.userAgent.indexOf("Firefox") > 0) {
                return "Firefox";
            }
            if (isSafari = navigator.userAgent.indexOf("Safari") > 0) {
                return "Safari";
            }
            if (isCamino = navigator.userAgent.indexOf("Camino") > 0) {
                return "Camino";
            }
            if (isMozilla = navigator.userAgent.indexOf("Gecko/") > 0) {
                return "Gecko";
            }

        }
        var os = getOs();

        $(document).ready(function () {
            if ('<%= Sessions.HasViewPrivilegeOnly %>' == 'True') {
                $('#<%= HyperLink1.ClientID %>').parent().hide();
                $('#<%= HyperLink11.ClientID %>').parent().hide();
                $('#<%= HyperLink12.ClientID %>').parent().hide();
                $('#<%= HyperLink13.ClientID %>').parent().hide();
            }
        });

    </script>
    <style>
        .box
        {
            height: 150px !important;
            width: 150px !important;
        }

        .dashboard-table .box .dashboard-title
        {
            padding: 20px 0 0 !important;
        }

        .datatable td
        {
            font-family: "Open Sans", sans-serif;
        }
    </style>

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Dashboard</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <%--<uc3:ucRequestFiles ID="ucRequestFiles" runat="server" />--%>

    <div class="inner-wrapper">

        <div class="page-container" style="padding-top: 0px; padding-bottom: 0px;">
            <div class="dashboard-container">
                <div class="left-content">
                    <%--<asp:Button runat="server" ID="btnTemporary" OnClick="btnTemporary_Click" />--%>
                    <%-- <td height="186" valign="top" style="background-repeat: no-repeat;">--%>
                    <div style="overflow: hidden;">
                        <%-- <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="730" height="600" valign="top" background="../images/bg_main.gif" bgcolor="#f6f5f2"
                                        style="background-repeat: repeat-x;">
                                        <div style="background: url(../images/inner_b.gif); background-position: right top; background-repeat: no-repeat; margin-top: 10px;">
                                            <div style="width: 725px; height: 75px;">
                                                <div class="Naslov_Crven">
                                                    Welcome <span style="color: #333333;"><a target="_self" href="../Account/MyAccount.aspx"
                                                        class="user_url">
                                                        <asp:LoginName ID="LoginName2" runat="server" />
                                                    </a></span>
                                                    <br />
                                                    <span class="tekstDef">
                                                        <%=DateTimeInfo %>
                                                    </span>
                                                </div>
                                            </div>--%>

                        <%--<marquee scrollamount="1" id="marqueemain" runat="server" behavior="scroll"
                            onmouseover="this.stop()" onmouseout="this.start()" scrolldelay="20" direction="up"
                            height="80px" style="">
                                    <%=MessageContent %>
                                </marquee>--%>
                        <div class="dashboard-table" style="width: 737px">
                            <div class="box">
                                <asp:HyperLink ID="HyperLink10" NavigateUrl="~/Office/EdForms.aspx" runat="server">
                                    <asp:Image ID="image12" runat="server" ImageUrl="~/images/dashboard-icon9.png"
                                        border="0" />
                                    <div class="dashboard-title">EdForms</div>
                                </asp:HyperLink>
                            </div>
                            <div class="box">
                                <asp:HyperLink ID="HyperLink22" NavigateUrl="~/Office/eFileFolders.aspx" runat="server">
                                    <asp:Image ID="imagebutton4" runat="server" ImageUrl="~/images/dashboard-icon3.png" border="0" />
                                    <div class="dashboard-title">View EdFiles</div>
                                </asp:HyperLink>
                            </div>
                            <div class="box">
                                <asp:HyperLink ID="HyperLink23" NavigateUrl="~/Office/WorkArea.aspx" runat="server">
                                    <asp:Image ID="imagebutton1" runat="server" ImageUrl="~/images/dashboard-icon15.png" border="0" />
                                    <div class="dashboard-title">Work Area</div>
                                </asp:HyperLink>
                            </div>
                            <div class="box">
                                <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Office/NeweFileFolder.aspx" runat="server">
                                    <asp:Image ID="image3" runat="server" ImageUrl="~/images/dashboard-icon1.png"
                                        border="0" />
                                    <div class="dashboard-title">Create New EdFiles</div>
                                </asp:HyperLink>
                            </div>
                            <div class="box">
                                <asp:HyperLink ID="HyperLink3" NavigateUrl="~/Office/ManageDocument.aspx" runat="server">
                                    <asp:Image ID="image5" runat="server" ImageUrl="~/images/dashboard-icon10.png" border="0" />
                                    <div class="dashboard-title">Retention Logs</div>
                                </asp:HyperLink>
                            </div>
                            <asp:LoginView ID="LoginView1" runat="server">
                                <RoleGroups>
                                    <asp:RoleGroup Roles="Offices">
                                        <ContentTemplate>
                                            <!-- New Buttons-->

                                            <div class="box">
                                                <asp:HyperLink ID="HyperLink25" NavigateUrl="~/WebUser/WebNewUser.aspx" runat="server">
                                                    <asp:Image ID="image1" runat="server" ImageUrl="~/images/dashboard-icon5.png" border="0" />
                                                    <div class="dashboard-title">Create Users</div>
                                                </asp:HyperLink>
                                            </div>
                                            <div class="box">
                                                <asp:HyperLink ID="HyperLink26" NavigateUrl="~/Office/ViewUsers.aspx" runat="server">
                                                    <asp:Image ID="image2" runat="server" ImageUrl="~/images/dashboard-icon6.png" border="0" />
                                                    <div class="dashboard-title">View Users</div>
                                                </asp:HyperLink>
                                            </div>
                                            <div class="box">
                                                <asp:HyperLink ID="HyperLink4" NavigateUrl="~/Office/DocumentAndFolderLogs.aspx" runat="server">
                                                    <asp:Image ID="image6" runat="server" ImageUrl="~/images/dashboard-icon19.png" border="0" />
                                                    <div class="dashboard-title">Activity Logs</div>
                                                </asp:HyperLink>
                                            </div>
                                        </ContentTemplate>
                                    </asp:RoleGroup>
                                </RoleGroups>
                            </asp:LoginView>
                            <%--<div class="box">
                                <asp:HyperLink ID="HyperLink24" NavigateUrl="~/Search/SearchFileFolders.aspx" runat="server">
                                    <asp:Image ID="imagebutton2" runat="server" ImageUrl="~/images/dashboard-icon4.png" border="0" />
                                    <div class="dashboard-title">Search EdFiles</div>
                                </asp:HyperLink>
                            </div>--%>
                            <%-- <div class="box">
                                <asp:HyperLink ID="HyperLink2" NavigateUrl="~/Account/MyAccount.aspx" runat="server">
                                    <asp:Image ID="image4" runat="server" ImageUrl="~/images/dashboard-icon7.png" border="0" />
                                    <div class="dashboard-title">My Account</div>
                                </asp:HyperLink>
                            </div>--%>
                            <div class="box" style="display: none">
                                <asp:HyperLink ID="HyperLink21" NavigateUrl="~/Office/PurchasePage.aspx" runat="server">
                                    <asp:Image ID="imagebutton3" runat="server" ImageUrl="~/images/dashboard-icon8.png" border="0" />
                                    <div class="dashboard-title">Purchase EdFiles</div>
                                </asp:HyperLink>
                            </div>
                            <div class="box" style="display: none">
                                <asp:HyperLink ID="HyperLink34" NavigateUrl="~/WebUser/ViewEFF.aspx" runat="server">
                                    <asp:Image ID="image35" runat="server" ImageUrl="~/images/dashboard-icon9.png" border="0" />
                                    <div class="dashboard-title">View Shared EdFiles</div>
                                </asp:HyperLink>
                            </div>

                            <%-- <div class="box">
                                <asp:HyperLink ID="HyperLink4" NavigateUrl="~/Office/ViewTransaction.aspx" runat="server">
                                    <asp:Image ID="image6" runat="server" ImageUrl="~/images/dashboard-icon11.png" border="0" />
                                    <div class="dashboard-title">View Transactions</div>
                                </asp:HyperLink>
                            </div>--%>
                            <div class="box">
                                <asp:HyperLink ID="HyperLink5" NavigateUrl="~/Office/CreateFileRequest.aspx" runat="server">
                                    <asp:Image ID="image7" runat="server" ImageUrl="~/images/dashboard-icon12.png" border="0" />
                                    <div class="dashboard-title">Create File Request</div>
                                </asp:HyperLink>
                            </div>
                            <div class="box" style="display: none">
                                <asp:HyperLink ID="HyperLink7" NavigateUrl="~/Office/ViewSignatures.aspx" runat="server">
                                    <asp:Image ID="image9" runat="server" ImageUrl="~/images/dashboard-icon13.png" border="0" />
                                    <div class="dashboard-title">View Signins</div>
                                </asp:HyperLink>
                            </div>
                            <div class="box" style="display: none">
                                <asp:HyperLink ID="HyperLink8" NavigateUrl="~/Office/DefineQuestions.aspx" runat="server">
                                    <asp:Image ID="image10" runat="server" ImageUrl="~/images/dashboard-icon14.png" border="0" />
                                    <div class="dashboard-title">Define Questions</div>
                                </asp:HyperLink>
                            </div>
                            <div class="box">
                                <asp:HyperLink ID="HyperLink6" NavigateUrl="~/Office/StoredPaperBox.aspx" runat="server"
                                    Visible="true">
                                    <asp:Image ID="image8" runat="server" ImageUrl="~/images/dashboard-icon15.png" border="0" />
                                    <div class="dashboard-title">Stored Paper Files</div>
                                </asp:HyperLink>
                            </div>
                            <div class="box">
                                <asp:HyperLink ID="HyperLink16" NavigateUrl="~/Office/CreateNewBox.aspx" runat="server"
                                    Visible="true">
                                    <asp:Image ID="image14" runat="server" ImageUrl="~/images/dashboard-icon16.png" border="0" />
                                    <div class="dashboard-title">Create New Box</div>
                                </asp:HyperLink>
                            </div>
                            <div class="box">
                                <asp:HyperLink ID="HyperLink17" NavigateUrl="~/Office/ViewFileForms.aspx" runat="server"
                                    Visible="true">
                                    <asp:Image ID="image17" runat="server" ImageUrl="~/images/dashboard-icon17.png" border="0" />
                                    <div class="dashboard-title">File Requests</div>
                                </asp:HyperLink>
                            </div>

                            <%--<asp:LoginView ID="LoginView2" runat="server">
                                <RoleGroups>
                                    <asp:RoleGroup Roles="Offices">
                                        <ContentTemplate>
                                            <div class="box">
                                                <asp:HyperLink ID="HyperLink9" NavigateUrl="~/Office/AccessReport.aspx" runat="server"
                                                    Visible="true">
                                                    <asp:Image ID="image11" runat="server" ImageUrl="~/images/dashboard-icon18.png" border="0" />
                                                    <div class="dashboard-title">User Acess Report</div>
                                                </asp:HyperLink>
                                            </div>
                                        </ContentTemplate>
                                    </asp:RoleGroup>
                                </RoleGroups>
                            </asp:LoginView>--%>
                            <div class="box">
                                <asp:HyperLink ID="HyperLink11" NavigateUrl="~/Office/ViewDeletedItems.aspx" runat="server"
                                    Visible="true">
                                    <asp:Image ID="image13" runat="server" ImageUrl="~/images/dahsboard-icon20.png" border="0" />
                                    <div class="dashboard-title">View Deleted Items</div>
                                </asp:HyperLink>
                            </div>
                            <asp:LoginView ID="LoginView2" runat="server">
                                <RoleGroups>
                                    <asp:RoleGroup Roles="Offices">
                                        <ContentTemplate>
                                            <div class="box">
                                                <asp:HyperLink ID="HyperLink2" NavigateUrl="~/Office/EmailGroupGrid.aspx" runat="server"
                                                    Visible="true">
                                                    <asp:Image ID="image4" runat="server" ImageUrl="~/images/view-email-groups.png" border="0" />
                                                    <div class="dashboard-title">View Email Groups</div>
                                                </asp:HyperLink>
                                            </div>
                                        </ContentTemplate>
                                    </asp:RoleGroup>
                                </RoleGroups>
                            </asp:LoginView>
                            <div class="box">
                                <asp:HyperLink ID="HyperLink9" NavigateUrl="~/Office/eFileSplit.aspx" runat="server"
                                    Visible="true">
                                    <asp:Image ID="image11" runat="server" ImageUrl="~/images/split-upload.png" border="0" />
                                    <div class="dashboard-title">Split and Upload pdf</div>
                                </asp:HyperLink>
                            </div>
                            <div class="box">
                                <asp:HyperLink ID="HyperLink12" NavigateUrl="~/Office/CopyFolders.aspx" runat="server"
                                    Visible="true">
                                    <asp:Image ID="image15" runat="server" ImageUrl="~/images/copy-folder.png" border="0" />
                                    <div class="dashboard-title">Copy Folders</div>
                                </asp:HyperLink>
                            </div>
                            <div class="box">
                                <asp:HyperLink ID="HyperLink13" NavigateUrl="~/Office/CopyFolderLogs.aspx" runat="server"
                                    Visible="true">
                                    <asp:Image ID="image16" runat="server" ImageUrl="~/images/copy-folder-logs.png" border="0" />
                                    <div class="dashboard-title">Folder Transfer Logs</div>
                                </asp:HyperLink>
                            </div>
                            <asp:LoginView ID="LoginView3" runat="server">
                                <RoleGroups>
                                    <asp:RoleGroup Roles="Offices">
                                        <ContentTemplate>
                                            <div class="box">
                                                <asp:HyperLink ID="HyperLink14" NavigateUrl="~/Office/LockTabs.aspx" runat="server"
                                                    Visible="true">
                                                    <asp:Image ID="image18" runat="server" ImageUrl="~/images/lock-tabs.png" border="0" />
                                                    <div class="dashboard-title">Lock Tabs</div>
                                                </asp:HyperLink>"/>
                                            </div>
                                        </ContentTemplate>
                                    </asp:RoleGroup>
                                </RoleGroups>
                            </asp:LoginView>
                            <div class="box">
                                <asp:HyperLink ID="HyperLink15" NavigateUrl="~/Office/ArchiveSearch.aspx" runat="server"
                                    Visible="true">
                                    <asp:Image ID="image19" runat="server" ImageUrl="~/images/Archive-Search.png" border="0" />
                                    <div class="dashboard-title">Archives</div>
                                </asp:HyperLink>
                            </div>
                            <div class="box">
                                <asp:HyperLink ID="HyperLink18" NavigateUrl="~/Office/AutoUpload.aspx" runat="server"
                                    Visible="true">
                                    <asp:Image ID="image20" runat="server" ImageUrl="~/images/auto-upload.png" border="0" />
                                    <div class="dashboard-title">Auto Upload</div>
                                </asp:HyperLink>
                            </div>
                            <div class="box">
                                <asp:HyperLink ID="HyperLink19" NavigateUrl="~/Office/MultiUpload.aspx" runat="server"
                                    Visible="true">
                                    <asp:Image ID="image21" runat="server" ImageUrl="~/images/pdf-multi-upload.png" border="0" />
                                    <div class="dashboard-title">Multi Upload</div>
                                </asp:HyperLink>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right-content">
                    <td class="block" valign="top">
                        <%--<div class="quick-find quickfind-full">
                            <uc1:QuickFind ID="QuickFind1" runat="server" />
                            <div class="clearfix"></div>                            
                        </div>--%>
                        <%-- <div class="quick-find quickfind-full" style="background-color: #efefef;display:none">
                           <marquee scrollamount="1" id="marqueeside" runat="server"
                                behavior="scroll" onmouseover="this.stop()" onmouseout="this.start()" scrolldelay="20"
                                direction="up">
                                <%=SideMessage %>
                            </marquee>
                        </div>--%>
                        <uc2:ReminderControl ID="ReminderControl2" runat="server" />
                    </td>
                </div>
            </div>
        </div>
    </div>
    <%--</div>--%>
</asp:Content>
