using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Net;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.DAL;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Windows.Forms;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Collections.ObjectModel;
using System.Threading;
using System.Text;



public partial class Office_Welcome : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Sessions.HasViewPrivilegeOnly) 
        //{
        //    HyperLink1.Visible = false;
        //}
        if (!Page.IsPostBack)
        {
            string uid = Sessions.SwitchedSessionId;
            //string uid = Membership.GetUser().ProviderUserKey.ToString();
            string personalMessage = string.Empty;
            Shinetech.DAL.Account act = new Shinetech.DAL.Account(uid);
            if (act.IsExist)
            {
                personalMessage = act.MessageBody.Value ?? "";
            }

            Shinetech.DAL.Message msg = new Shinetech.DAL.Message(1);
            if (msg.IsExist)
            {
                MessageContent = msg.EnableFlag.Value ? msg.MainMessage.Value : "";
                SideMessage = msg.EnableFlag1.Value ? msg.SideMessage.Value : "";
                if (personalMessage.Length > 0) SideMessage += ("<br><font color='red'>" + personalMessage + "</font>");
            }
            if (Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users"))
            {
                String[] RoleList = { "Users" };
                LoginView1.RoleGroups[0].Roles = RoleList;
                LoginView2.RoleGroups[0].Roles = RoleList;
                LoginView3.RoleGroups[0].Roles = RoleList;
            }
        }
    }

    public string DateTimeInfo
    {
        get
        {
            DateTimeFormatInfo myDTFI = new CultureInfo("en-US", false).DateTimeFormat;
            DateTime dt = DateTime.Now;
            return dt.ToString("MM/dd/yyy", myDTFI);

        }
    }

    public string MessageContent
    {
        get
        {
            return this.Session["_Main_Message"] as string;
        }
        set
        {
            this.Session["_Main_Message"] = value;
        }
    }

    public string SideMessage
    {
        get
        {
            return this.Session["_Side_Message"] as string;
        }

        set
        {
            this.Session["_Side_Message"] = value;
        }
    }
    //protected void btnTemporary_Click(object sender, EventArgs e)
    //{
    //    var region = "{region}";
    //    // See https://github.com/openstacknetsdk/openstack.net/wiki/Connect-and-Authenticate#example for how to authenticate against OpenStack instead of Rackspace.
    //    var user = new CloudIdentity
    //    {
    //        Username = ConfigurationManager.AppSettings["RackSpaceUserName"],
    //        APIKey = ConfigurationManager.AppSettings["RackSpaceAPIKey"]
    //    };

    //    var cloudfiles = new CloudFilesProvider(user);

    //    // Create or initialize your account's key used to generate temp urls
    //    const string accountTempUrlHeader = "Temp-Url-Key";
    //    var accountMetadata = cloudfiles.GetAccountMetaData();
    //    string tempUrlKey;
    //    if (!accountMetadata.ContainsKey(accountTempUrlHeader))
    //    {
    //        tempUrlKey = Guid.NewGuid().ToString();
    //        var updateMetadataRequest = new Dictionary<string, string> { { accountTempUrlHeader, tempUrlKey } };
    //        cloudfiles.UpdateAccountMetadata(updateMetadataRequest, region);
    //    }
    //    else
    //    {
    //        tempUrlKey = accountMetadata[accountTempUrlHeader];
    //    }

    //    // Generate a public URL for a cloud file which is good for 1 day
    //    var containerName = "d4da1d9b-4766-4567-9e11-eaee6a794c66";
    //    var fileName = "workArea/askhita/akshita/encounter2507.pdf";
    //    var expiration = DateTimeOffset.UtcNow + TimeSpan.FromMinutes(20);
    //    Uri tempUrl = cloudfiles.CreateTemporaryPublicUri(JSIStudios.SimpleRESTServices.Client.HttpMethod.GET, containerName, fileName, tempUrlKey, expiration, null, false, user);
    //    string remoteUri = tempUrl.AbsoluteUri;
    //    string filename = "ms-banner.pdf", myStringWebResource = null;
    //    // Create a new WebClient instance.
    //    WebClient myWebClient = new WebClient();
    //    // Concatenate the domain with the Web resource filename.
    //    myStringWebResource = remoteUri;
    //    Console.WriteLine("Downloading File \"{0}\" from \"{1}\" .......\n\n", filename, myStringWebResource);
    //    // Download the Web resource and save it into the current filesystem folder.
    //    myWebClient.DownloadFile(myStringWebResource, filename);
    //}
}

