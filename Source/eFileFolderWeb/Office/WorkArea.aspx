﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="WorkArea.aspx.cs" Inherits="Office_WorkArea" %>

<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%--<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>--%>
<%@ Register Src="~/Office/UserControls/ucAddToTab.ascx" TagPrefix="uc1" TagName="ucAddToTab" %>
<%@ Register Src="~/Office/UserControls/ucWorkArea.ascx" TagPrefix="uc1" TagName="ucWorkArea" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/style.css" rel="Stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=2" rel="stylesheet" />
    <link rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/style.min.css" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>extras/jquery-ui-1.8.22.custom.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jstree.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/edFiles/EdFilesLogs.js?v=4" type="text/javascript"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style type="text/css">
        #btnCheckedAll {
            float: right;
            margin-right: 3px;
            margin-top: 9px;
            height: 18px;
            width: 18px;
        }

        .selectalltab {
            font-size: 18px;
            float: right;
            padding: 5px;
            font-family: 'Open Sans', sans-serif !important;
            color: #fff;
            font-weight: 700;
            margin-right: 5px;
        }

        .ajax__tab_body {
            height: auto !important;
        }

        .btnAddtoTab, .btnAddtoWorkArea {
            padding: 8px 10px 8px 35px;
            font-size: 15px;
            padding-right: 10px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 5px;
            background-image: url(../images/plus-icon.png);
            background-position: 7px center;
            background-repeat: no-repeat;
        }

        #btnCheckedAll {
            float: right;
            margin-right: 3px;
            margin-top: 9px;
            height: 18px;
            width: 18px;
        }

        .selectalltab {
            font-size: 18px;
            float: right;
            padding: 5px;
            font-family: 'Open Sans', sans-serif !important;
            color: #fff;
            font-weight: 700;
            margin-right: 5px;
        }



        .white_content {
            display: none;
            position: fixed;
            top: 20%;
            left: 20%;
            width: 60%;
            height: 50%;
            padding: 16px;
            background-color: white;
            z-index: 999999999;
            overflow: auto;
        }

        .datatable td {
            font-family: "Open Sans", sans-serif;
        }

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
            position: fixed;
        }

        .preview {
            position: relative;
            left: -10px;
        }

        td span {
            -ms-word-break: break-all;
            word-break: break-all;
            word-break: break-word;
            -webkit-hyphens: auto;
            -moz-hyphens: auto;
            hyphens: auto;
            max-width: 300px;
            white-space: normal !important;
        }

        /*td {
            max-width: 200px;
        }*/

        .actionMinWidth {
            min-width: 50px;
        }

        .chkpending, .chkinbox {
            margin-left: 10px;
        }

        .chkpendingAll, .chkinboxAll {
            margin-left: 10px;
        }

        .boxtitle {
            margin-top: 20px !important;
        }

        .file {
            background: url(../lib/TreeView/file_sprite.png) 0 0 no-repeat !important;
            width: 18px !important;
            height: 18px !important;
            margin-top: 4px !important;
            margin-left: 2px !important;
            margin-right: 5px !important;
        }


        /*16-09-2016*/
        .create-btn {
            background-image: url(../images/create-folder.png);
            cursor: pointer;
        }

        .addtotab-workarea {
            cursor: pointer;
            background-image: url(../images/AddToTabWorkArea.png);
            margin-left: 0px;
            float: none;
            background-color: white;
            border: 0px;
            margin-top: -4px;
        }

        .rename-btn {
            background-image: url(../images/rename.png);
            cursor: pointer;
        }

        .delete-btn {
            background-image: url(../images/delete.png);
            cursor: pointer;
            opacity: 0.5;
        }

        .cancel-btn {
            background-image: url(../images/cancel-btn.png);
            cursor: pointer;
        }

        .send-btn {
            background-image: url(../images/send.png);
            cursor: pointer;
        }

        .preview-btn {
            cursor: pointer;
        }

        .register-container .form-containt .content-title {
            margin-bottom: 5px;
        }

        #popupclose,
        .popupclose,
        #popupclose_thumbnail {
            float: right;
            padding: 10px;
            cursor: pointer;
        }

        .popupcontent {
            padding: 10px;
        }

        .form-containt .content-box fieldset {
            font-size: initial;
        }

        .ajax__fileupload_dropzone {
            font-size: 15px;
            line-height: 120px !important;
            height: 120px !important;
        }

        .lnkbtnPreviewInbox,
        .lnkbtnPreviewPending {
            margin-left: -10px;
        }

        .downloadFile {
            margin-left: -10px;
            cursor: pointer;
        }

        .preview-popup {
            width: 100%;
            max-width: 1100px;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 10%;
            right: 0;
            margin: auto;
            background: #fff;
            height: 85%;
        }

        .preview-popup-thumbnails {
            width: 100%;
            max-width: 1200px;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 10%;
            right: 0;
            margin: auto;
            background: #fff;
            height: 85%;
        }

        #floatingCirclesG {
            top: 50%;
            left: 50%;
            position: fixed;
        }

        .listing-contant .quick-find {
            background: #fff;
            padding: 0px 0px 0px 0px;
            width: 235px;
            margin-top: 0px;
        }

        .quick-find .find-inputbox .quick-find-btn {
            top: 3px;
            height: 30px;
            width: 30px;
        }

        .quick-find .find-inputbox .find-input {
            height: 35px;
            padding-right: 5px;
        }

        .shareDocument {
            cursor: pointer;
        }

        .ui-dialog {
            top: 15% !important;
            position: fixed;
        }

        .ic-save, .ic-share {
            cursor: pointer;
        }

        .selectedTreeNodeLabel {
            display: inline-block;
            font-size: 14px;
            margin-top: 25px;
        }

        #uploadProgress {
            font-size: 14px;
        }

        .workareatable {
            /*width: 80% !important;
            float: right;*/
            border-width: 1px !important;
        }

        .chosen-container-single .chosen-single {
            line-height: 33px !important;
        }

        .chosen-single {
            height: 35px !important;
        }

        #combobox2_chosen {
            margin-left: 5px;
            margin-bottom: 8px;
        }

        #combobox1_chosen {
            margin-bottom: 8px;
        }

        .chkSearch {
            font-size: 14px;
            vertical-align: super;
        }

        .rename-block {
            float: inherit !important;
        }

        input[type="radio"] {
            height: 18px;
            width: 18px;
            vertical-align: middle;
        }

        .chkSearch > label {
            vertical-align: sub;
        }

        .btnSearch {
            width: 39px !important;
            height: 29px !important;
            font-size: 0;
            background-image: url(../images/quickfind-bg.png) !important;
            background-repeat: no-repeat !important;
            background-position: center center !important;
            border-radius: 0 !important;
            -webkit-border-radius: 0 !important;
            top: 7px !important;
            position: relative !important;
            border: none !important;
            background-color: transparent !important;
            cursor: pointer !important;
        }

        .prevnext > table > tbody > tr > td > select {
            height: 25px;
            margin-top: 7px;
        }

        #circularG {
            position: absolute;
            top: 75px;
            left: 142px;
            width: 25px;
            height: 25px;
            margin: auto;
        }

        #thumnailImages .thumnail-outer {
            margin-bottom: 20px;
        }

            #thumnailImages .thumnail-outer span {
                float: left;
            }

            #thumnailImages .thumnail-outer .check {
                float: right;
            }

            #thumnailImages .thumnail-outer > img {
                display: block;
            }

        #comboboxDividerSplit_chosen {
            margin-top: 5px;
            display: block;
        }

        #comboboxSplit_chosen {
            display: block;
            margin-top: 5px;
        }

        .chosen-results {
            max-height: 200px !important;
        }
    </style>

    <script language="JavaScript" type="text/jscript">
        var ids = "", selectedButton = "", selectedPath = "", selectedChildNode = "", modalOpenedFrom = "", selectedTreeId = "";
        var userId = '<%= Sessions.SwitchedRackspaceId%>';

        function pageLoad() {
            if (LoadFolders)
                LoadFolders();

            //LoadIndexKeys();
            //$("#combobox2").chosen({ width: "135px" });

            $('.popupclose,#btnCancelAddToTab,#btnCancelAddToTabSplit,#popupclose_thumbnail').click(function (e) {
                e.preventDefault();
                $(this).closest('.popup-mainbox').hide();
                $('#overlay').hide();
                enableScrollbar();
            });



            $('#dialog-share-book').dialog({
                autoOpen: false,
                width: 450,
                //height: 540,
                top: 300,
                resizable: false,
                modal: true,
                buttons: {
                    "Share Now": function () {
                        SaveMail();
                    },
                    "Close": function () {
                        $(this).dialog("close");
                    }
                },
                close: function () {
                }
            });

            $('.ui-dialog-buttonset').find('button').each(function () {
                $(this).find('span').text($(this).attr('text'));
            })

            $("#dialog-message-all").dialog({
                autoOpen: false,
                resizable: false,
                modal: true,
                Ok: function () {
                    $(this).dialog("close");
                },
                close: function (event, ui) {
                    $(this).dialog("option", 'title', '');
                    $('#dialog-confirm-content').empty();
                }

            });

            window.CheckValidation = function CheckValidation(fieldObj) {
                if (fieldObj.val() == "") {
                    fieldObj.css('border', '1px solid red');
                }
                else {
                    fieldObj.css('border', '1px solid black');
                }
            }

            window.CheckEmailValidation = function CheckEmailValidation(fieldObj) {
                var x = fieldObj.val();
                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(x)) {
                    fieldObj.css('border', '1px solid black');
                    return true;
                }
                else {
                    fieldObj.css('border', '1px solid red');

                    return false;
                }
            }

            function SaveMail() {
                var email = $('#dialog-share-book input[name="email"]').val();
                //var name = $('#dialog-share-book input[name="myname"]').val();
                var emailto = $('#dialog-share-book input[name="emailto"]').val();
                var nameto = $('#dialog-share-book input[name="nameto"]').val();

                var title = $('#dialog-share-book input[name="title"]').val();
                var content = $('#dialog-share-book textarea[name="content"]').val();
                var Isvalid = false;

                //$('#dialog-share-book input[name="myname"]').keyup(function () { CheckValidation($('#dialog-share-book input[name="myname"]')) });
                //$('#dialog-share-book input[name="email"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="email"]')) });

                $('#dialog-share-book input[name="emailto"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="emailto"]')) });

                //if (email == "" || name == "" || emailto == "") {
                if (emailto == "") {
                    //CheckValidation($('#dialog-share-book input[name="email"]'));
                    //CheckValidation($('#dialog-share-book input[name="myname"]'));
                    CheckValidation($('#dialog-share-book input[name="emailto"]'));
                    //CheckEmailValidation($('#dialog-share-book input[name="email"]'));
                    CheckEmailValidation($('#dialog-share-book input[name="emailto"]'))
                    return false;
                }

                //if (!Isvalid) {
                //    Isvalid = CheckEmailValidation($('#dialog-share-book input[name="email"]'));
                //    if (!Isvalid)
                //        return Isvalid;
                //}

                Isvalid = CheckEmailValidation($('#dialog-share-book input[name="emailto"]'));
                if (!Isvalid) { return Isvalid; }

                var src = '';
                showLoader();

                var path1 = $("#" + '<%= hdnSharePath.ClientID%>').val();
                var filename = path1.replace("WorkArea", "workArea");
                var workAreaId;
                var URL = PrepareURLForWorkArea(null);
                $.ajax(
                    {

                        type: "POST",
                        url: '../EFileFolderJSONService.asmx/ShareMailForWorkArea',
                        data: "{strToMail:'" + emailto + "',MailTitle:'" + title + "',ToName:'" + nameto + "',SendName:'" + "" + "',strSendMail:'" + email + "',strMailInfor:'" + content + "',URL:'" + URL + "',fileName: '" + (filename == undefined || null ? 0 : filename) + "', workAreaId: '" + $('#<%= hdnShareWorkAreaId.ClientID %>').val() + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var id = data.d;
                            var title, message;
                            if (id === 0) {
                                title = "Sharing WorkArea URL error";
                                message = "This URL has not been shared successfully."
                            }
                            else {
                                title = "Sharing WorkArea URL success";
                                message = "This URL has been shared successfully."
                            }

                            $('#dialog-message-all').dialog({
                                title: title,
                                open: function () {
                                    $("#spanText").text(message);
                                }
                            });

                            $('#dialog-message-all').dialog("open");

                            if (id != null || id != "")
                                $('#dialog-share-book').dialog("close");
                            hideLoader();
                            modalOpenedFrom = "";
                        },
                        fail: function (data) {
                            bookshelf.loaded.apply();

                            $('#dialog-message-all').dialog({
                                title: "Sharing WorkArea URL error",
                                open: function () {
                                    $("#spanText").text("This URL has not been shared successfully.");
                                }
                            });
                            $('#dialog-message-all').dialog("open");

                            hideLoader();
                            modalOpenedFrom = "";
                        }
                    });

            }


            $(".ajax__fileupload_dropzone").text("Drag and Drop Pdf file(s) here");

            //document.getElementById('create-rename-block').style.display = "none";
            //document.getElementById('workAreaFolderBox').style.display = "none";

            //document.getElementById('jsTreeParent').style.borderTop = "0px";
            //document.getElementById("circularG").style.display = "block";

            $('.editInboxfile').click(function (e) {
                e.preventDefault();
                removeAllInboxEditable();
                showInboxEdit($(this));
            });

            $('.cancelInboxfile').click(function (e) {
                e.preventDefault();
                removeAllInboxEditable();
            });

            $("#ButtonDeleleOkay").live({
                click: function () {
                    showLoader();
                }
            });

            $('.shareDocument').click(function (e) {
                e.preventDefault();
            });

            var closePopup = document.getElementById("popupclose");
            closePopup.onclick = function () {
                var popup = document.getElementById("preview_popup");
                var overlay = document.getElementById("overlay");
                overlay.style.display = 'none';
                popup.style.display = 'none';
                enableScrollbar();
                $('#reviewContent').removeAttr('src');
            };

            //var closePopupThumb = document.getElementById("popupclose_thumbnail");
            //closePopupThumb.onclick = function () {
            //    var popup = document.getElementById("preview_popup_thumbnails");
            //    var overlay = document.getElementById("overlay");
            //    overlay.style.display = 'none';
            //    popup.style.display = 'none';
            //    enableScrollbar();
            //    $('#reviewContent_thumbnail').removeAttr('src');
            //};

            $(".lnkbtnPreviewInbox").load(function (e) {
                e.preventDefault();
                ShowInboxOpacity($(this));
            });

            try {

                function PrepareTree(objList) {
                    $('#jstree1').jstree('destroy');
                    if ($('#<%= hdnJsTreeData.ClientID %>'))//checking whether it is found on DOM, but not necessary
                    {
                        $('#<%= hdnJsTreeData.ClientID %>').val(JSON.stringify(objList));
                        funEncodeTags();
                    }

                    var Tree = "<ul>";
                    Tree = Tree + "<li id='node" + 0 + "' ModifiedDate='" + '<%= DateTime.Now %>' + "' path='WorkArea'>WorkArea</li>";
                    $("#jstree1").html(Tree);
                    CreateTree("File", objList);
                    //CreateTree("Folder", objList);
                    DrawTree();
                }

                function CreateTree(prepareTreeFor, objList) {
                    var remainingList = [];
                    for (var i = 0; i < objList.length; i++) {
                        var node = "";
                        if (prepareTreeFor == "File") {
                            if (objList[i].ObjectName.indexOf(userId) >= 0 || objList[i].ObjectName.indexOf('.') >= 0)
                                continue;
                        }
                        else if (prepareTreeFor == "Folder") {
                            if (objList[i].ObjectName.indexOf(userId) >= 0 || objList[i].ObjectName.indexOf('.') < 0)
                                continue;
                        }
                        if ($("#<%= txtSearch.ClientID %>").val().length > 0) {

                            if (objList[i].ObjectName.indexOf(userId) >= 0)
                                continue;
                            else if (objList[i].ObjectName.indexOf('.') > 0 && (objList[i].ObjectName.indexOf($("#<%= txtSearch.ClientID %>").val().trim()) < 0)) {
                                continue;
                            }
                            node = $("#jstree1").find("#node" + objList[i].ParentID);
                            if (node.length == 0)
                                remainingList.push(objList[i]);
                            else
                                node.append("<ul><li id='node" + objList[i].ObjectID + "' path='" + objList[i].Path + "' name='" + objList[i].ObjectID + "' ModifiedDate='" + objList[i].ModifiedDate + "' Size='" + objList[i].Size + "'>" + objList[i].ObjectName + "</li></ul>");
                            //node.append("<ul><li id='node" + objList[i].ObjectID + "' path='" + objList[i] + "' ModifiedDate='" + objList[i].ModifiedDate + "' Size='" + objList[i].Size + "'>" + objList[i].ObjectName + "</li></ul>");
                        }
                        else {
                            node = $("#jstree1").find("#node" + objList[i].ParentID);
                            if (node.length == 0)
                                remainingList.push(objList[i]);
                            else
                                node.append("<ul><li id='node" + objList[i].ObjectID + "' path='" + objList[i].Path + "' name='" + objList[i].ObjectID + "' ModifiedDate='" + objList[i].ModifiedDate + "' Size='" + objList[i].Size + "'>" + objList[i].ObjectName + "</li></ul>");
                            //node.append("<ul><li id='node" + objList[i].ObjectID + "' path='" + objList[i].Path + "' ModifiedDate='" + objList[i].ModifiedDate + "' Size='" + objList[i].Size + "'>" + objList[i].ObjectName + "</li></ul>");
                        }
                    }
                    for (var i = remainingList.length - 1; i >= 0; i--) {
                        var node = "";
                        if (prepareTreeFor == "File") {
                            if (remainingList[i].ObjectName.indexOf(userId) >= 0 || remainingList[i].ObjectName.indexOf('.') >= 0)
                                continue;
                        }
                        else if (prepareTreeFor == "Folder") {
                            if (remainingList[i].ObjectName.indexOf(userId) >= 0 || remainingList[i].ObjectName.indexOf('.') < 0)
                                continue;
                        }
                        if ($("#<%= txtSearch.ClientID %>").val().length > 0) {

                            if (remainingList[i].ObjectName.indexOf(userId) >= 0)
                                continue;
                            else if (remainingList[i].ObjectName.indexOf('.') > 0 && (remainingList[i].ObjectName.indexOf($("#<%= txtSearch.ClientID %>").val().trim()) < 0)) {
                                continue;
                            }
                            node = $("#jstree1").find("#node" + remainingList[i].ParentID);
                            node.append("<ul><li id='node" + remainingList[i].ObjectID + "' path='" + remainingList[i].Path + "' name='" + remainingList[i].ObjectID + "' ModifiedDate='" + remainingList[i].ModifiedDate + "' Size='" + remainingList[i].Size + "'>" + remainingList[i].ObjectName + "</li></ul>");
                        }
                        else {
                            node = $("#jstree1").find("#node" + remainingList[i].ParentID);
                            node.append("<ul><li id='node" + remainingList[i].ObjectID + "' path='" + remainingList[i].Path + "' name='" + remainingList[i].ObjectID + "' ModifiedDate='" + remainingList[i].ModifiedDate + "' Size='" + remainingList[i].Size + "'>" + remainingList[i].ObjectName + "</li></ul>");
                        }
                    }
                }
                function AssignOpenEventsJSTree() {
                    $('#jstree1').on('before_open.jstree', function (e, data) {
                        var leafNode = $("#jstree1").find($(".jstree-leaf"));
                        for (var i = 0; i < leafNode.length; i++) {
                            var temp = leafNode[i];
                            if (leafNode[i] != undefined && leafNode[i].textContent.indexOf('.') > 0) {
                                var childNode = $(leafNode[i]).find("a");
                                if (childNode != undefined) {
                                    var node = childNode.find("i");
                                    if (node != undefined) {
                                        node.addClass("file file-ew node jstree-themeicon-custom");
                                    }
                                }
                            }
                        }
                    });
                }

                function AssignClickEvent() {
                    $('#jstree1').on('changed.jstree', function (e, data) {
                        //if(process) {
                        if (data.selected.length > 1) {
                            for (var i = 1; i <= data.selected.length; i++) {
                                $('#jstree1').jstree('deselect_node', data.instance.get_node(data.selected[i]));
                            }
                        }
                        else {
                            if (data.node != undefined) {
                                var selectedNode = data.node.text;
                                selectedPath = data.node.li_attr.path;
                                selectedTreeId = data.node.li_attr.name;
                                selectedChildNode = data.node.id;
                                $("#<%= hdnSelectedNode.ClientID%>").val(selectedChildNode);

                                var hiddenSelectedPath = $('#<%= hdnSelectedPath.ClientID %>');
                                if (hiddenSelectedPath)//checking whether it is found on DOM, but not necessary
                                {
                                    $('#<%= hdnSelectedPath.ClientID %>').val("");
                                    $('#<%= hdnSelectedPath.ClientID %>').val(selectedPath);
                                }
                                $('#<%= hdnWorkAreaTreeId.ClientID %>').val("");
                                $('#<%= hdnWorkAreaTreeId.ClientID %>').val(selectedTreeId);
                                if (data.node.text.indexOf('.') > 0)
                                    $('#lblSelectedTreeNode').html("<b>File Name : </b>" + data.node.text + "<br /><b>Modified Date :</b> " + data.node.li_attr.modifieddate + "<br /><b>Size : </b>" + data.node.li_attr.size);
                                else {
                                    $('#lblSelectedTreeNode').html("");
                                    showLoader();
                                    $.ajax({
                                        type: "POST",
                                        url: '../Office/WorkArea.aspx/GetDocsForSelectedFolder',
                                        contentType: "application/json; charset=utf-8",
                                        data: "{ path:'" + $('#<%= hdnSelectedPath.ClientID %>').val() + "'}",
                                        dataType: "json",
                                        success: function (data) {
                                            if (data.d.length > 0)
                                                alert("Failed to search. Please try again after sometime.");
                                            hideLoader();
                                            $("#" + '<%= txtSearch.ClientID %>').val("");
                                            $("#" + '<%= txtIndexVal.ClientID%>').val("");
                                            __doPostBack('', 'RefreshGrid@');

                                        },
                                        error: function (result) {
                                            console.log('Failed' + result.responseText);
                                            hideLoader();
                                        }
                                    });
                                }
                            }
                        }
                        document.getElementById('workAreaFolderBox').style.display = "none";
                        //$("#create-rename-block").css("margin-right", "80px");
                    });
                }

                function DrawTree() {
                    $('#jstree1').jstree();
                    document.getElementById("circularG").style.display = "none";
                    document.getElementById('create-rename-block').style.display = "block";
                    document.getElementById('jsTreeParent').style.borderTop = "1px solid #E6E6E6";

                    AssignOpenEventsJSTree();
                    AssignClickEvent();
                    if (selectedButton == "Search") {
                        hideLoader();

                        selectedPath = "";
                        $('#<%= hdnSelectedPath.ClientID %>').val('');
                        $('#<%= hdnWorkAreaTreeId.ClientID %>').val('');
                        $('#lblSelectedTreeNode').html("");
                    }
                    $('#jstree1').jstree('open_all');//Expands all the nodes of tree
                    $('#jstree1').jstree('select_node', $("#<%= hdnSelectedNode.ClientID %>").val(), true);
                }

                function funEncodeTags() {
                    var mytext = $('#<%= hdnJsTreeData.ClientID %>').val();
                    var encode = $('<div />').text(mytext).html();
                    $('#<%= hdnJsTreeData.ClientID %>').val(encode);
                }

                function funDecodeTags() {
                    var mytext = $('#<%= hdnJsTreeData.ClientID %>').val();
                    var decode = $('<div/>').html(mytext).text();
                    $('#<%= hdnJsTreeData.ClientID %>').val(decode);
                }

                $('#ButtonDeleleOkayWorkArea').unbind().click(function (e) {
                    e.preventDefault();
                    if ($('#<%= hdnSharePath.ClientID %>').val() == "")
                        alert("Please select file to delete");
                    else {
                        showLoader();
                        selectedButton = "Delete";
                        $.ajax({
                            type: "POST",
                            url: '../Office/WorkArea.aspx/DeleteObject',
                            contentType: "application/json; charset=utf-8",
                            data: "{ objectPath:'" + $('#<%= hdnSharePath.ClientID %>').val() + "', password:'" + $('#<%= TextBox2.ClientID%>').val() + "', selectedPath:'" + $('#<%= hdnSelectedPath.ClientID %>').val() + "'}",
                            dataType: "json",
                            success: function (data) {
                                if (data.d.length > 0) {
                                    var objList = JSON.parse(data.d);
                                    if (objList.ErrorMessage && objList.ErrorMessage.length > 0 && objList.ErrorMessage.indexOf('password') < 0) {
                                        alert("Failed to delete file. Please try again after sometime.");
                                        hideLoader();
                                        return;
                                    }
                                    else if (objList.ErrorMessage && objList.ErrorMessage.length > 0 && objList.ErrorMessage.indexOf('password') > 0) {
                                        alert(objList.ErrorMessage);
                                        hideLoader();
                                        return;
                                    }
                                }
                                else {
                                    //window.location.reload();
                                    __doPostBack('', 'RefreshGrid@');
                                }
                            },
                            error: function (result) {
                                console.log('Failed' + result.responseText);
                                hideLoader();
                            }
                        });
                    }
                    return false;
                });
            }
            catch (e) { }
        }

        function LoadIndexKeys() {
            $("#combobox1").empty();
            $("#combobox1").chosen("destroy");
            $.ajax(
                {
                    type: "POST",
                    url: '../FlexWebService.asmx/GetWorkAreaIndexKeys',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $.each(data.d, function (i, text) {
                            $('<option />', { value: text, text: text }).appendTo($("#combobox1"));
                        });
                        $("#combobox1").chosen({ width: "150px" });
                        $("#combobox1").chosen().val($("#" + '<%= hdnIndexKey.ClientID%>').val());
                             $("#combobox1").trigger("chosen:updated");
                             $("#" + '<%= hdnIndexKey.ClientID%>').val("");
                             $("#combobox2").chosen().val($("#" + '<%= hdnIndexCrieteria.ClientID%>').val());
                             $("#combobox2").trigger("chosen:updated");
                             $("#" + '<%= hdnIndexCrieteria.ClientID%>').val("");
                    },
                    fail: function (data) {
                    }
                });
        }

        $(document).ready(function () {

            $("#btnDeleteIndexCancel").click(function () {
                $('#DivDeleteConfirmIndex').hide();
                $('#PanelIndex,#overlay').show().focus();
            });
            $("#<%= txtSearch.ClientID %>").keypress(function (e) {
                     //if (e.keyCode === 13) {
                     //    showLoader();
                     //    e.preventDefault();
                     //    selectedButton = "Search";
                     //    GetSearchDocs();
                     //}
                 });
                 $('#<%= btnSelectAddToTab.ClientID%>').click(function (e) {
                     var selectedFolder = $("#combobox").chosen().val();
                     var selectedDivider = $("#comboboxDivider").chosen().val();
                     if (selectedFolder == "0" && selectedDivider == "0") {
                         alert("Please select at least one EdFile & one Tab");
                         e.preventDefault();
                         return false;
                     }
                     else if (selectedFolder == "0") {
                         alert("Please select at least one EdFile");
                         e.preventDefault();
                         return false;
                     }
                     else if (selectedDivider == "0") {
                         alert("Please select at least one Tab");
                         e.preventDefault();
                         return false;
                     }
                     var folderValue = $('.chosen-single').first().text();
                     var tabValue = $('.chosen-single').last().text();
                     if (selectedFolder == undefined || selectedFolder == null || selectedFolder.length <= 0 || selectedFolder == "0") {
                         alert('Please select folder to move request files.');
                         e.preventDefault();
                         return false;
                     }
                     $('#overlay').css('z-index', '100010');
                     $('#<%= hdnSelectedFolderID.ClientID%>').val(selectedFolder);
                  $('#<%= hdnSelectedDividerID.ClientID%>').val(selectedDivider);
                  $('#<%= hdnSelectedFolderText.ClientID%>').val(folderValue);
                  $('#<%= hdnSelectedTabText.ClientID%>').val(tabValue);
                     return true;
                 });

                 $('#<%= btnSaveSplitPdf.ClientID%>').click(function (e) {
                     var selectedFolder = $("#comboboxSplit").chosen().val();
                     var selectedDivider = $("#comboboxDividerSplit").chosen().val();
                     if (selectedFolder == "0" && selectedDivider == "0") {
                         alert("Please select at least one EdFile & one Tab");
                         e.preventDefault();
                         return false;
                     }
                     else if (selectedFolder == "0") {
                         alert("Please select at least one EdFile");
                         e.preventDefault();
                         return false;
                     }
                     else if (selectedDivider == "0") {
                         alert("Please select at least one Tab");
                         e.preventDefault();
                         return false;
                     }
                     if (selectedFolder == undefined || selectedFolder == null || selectedFolder.length <= 0 || selectedFolder == "0") {
                         alert('Please select folder to move request files.');
                         e.preventDefault();
                         return false;
                     }
                     $('#overlay').css('z-index', '100010');
                     $('#<%= hdnSelectedFolderID.ClientID%>').val(selectedFolder);
                  $('#<%= hdnSelectedDividerID.ClientID%>').val(selectedDivider);
                     return true;
                 });

                 $("#txtValue, #txtKey").keypress(function (e) {
                     if (e.keyCode === 13) {
                         e.preventDefault();
                         $("#btnIndexSave").click();
                     }
                 });

                 $("#comboboxSplit").change(function (event) {
                     ChangeDivider();
                 });

                 $("#btnIndexSave").click(function (e) {
                     deleteIndexData = "";
                     var arrIndex = [];
                     for (i = 0; i < $('table#tblIndex tr').length; i++) {
                         var $item = $($('table#tblIndex tr')[i]);
                         if (($item.find('#txtKey').val() != null && $item.find('#txtKey').val() != "") &&
                             ($item.find('#txtValue').val() != null && $item.find('#txtValue').val() != "")) {
                             var result = arrIndex.filter(function (obj) {
                                 return obj.IndexKey.toLowerCase() == $item.find('#txtKey').val().toLowerCase();
                             });
                             if (result.length > 0) {
                                 alert("Can not enter duplicate keys.");
                                 return false;
                             }
                             arrIndex.push({ IndexKey: $item.find('#txtKey').val(), IndexValue: $item.find('#txtValue').val(), IndexId: $($item).find('input.hdnIndexId').val() });
                         }
                         else {
                             //alert("Please enter Key Value pair.");
                             //return false;
                             deleteIndexData = deleteIndexData + $($item).find('input.hdnIndexId').val() + ",";

                         }
                     }

                     var test = JSON.stringify(arrIndex);
                     deleteIndexData = deleteIndexData.substr(0, deleteIndexData.lastIndexOf(','));
                     var data = { "workAreaId": $("#" + '<%= hdnShareWorkAreaId.ClientID%>').val(), "indexData": test, "deleteIndexData": deleteIndexData };
                     $.ajax({
                         type: "POST",
                         url: "../Office/WorkArea.aspx/SaveWorkAreaIndexValues",
                         data: JSON.stringify(data),
                         contentType: "application/json; charset=utf-8",
                         dataType: "json",
                         success: function (result) {
                             deleteIndexData = "";
                             if (result.d == true) {
                                 alert("Data has been saved successfully.");
                                 // e.preventDefault();
                                 $('#overlay').hide();
                                 $('#PanelIndex').hide().focus();
                                 $("#floatingCirclesG").css('display', 'none');
                                 $('#floatingCirclesG').addClass('hideControl');
                                 //LoadIndexKeys();
                                 //e.stopPropagation();
                             }
                             else
                                 alert("Data has not been saved. Please try again later.");

                         }
                     });
                 });

                 $("#btnIndexCancel, #closeIndex").click(function (e) {
                     e.preventDefault();
                     $('#overlay').hide();
                     $('#PanelIndex').hide().focus();
                     $("#floatingCirclesG").css('display', 'none');
                     $('#floatingCirclesG').addClass('hideControl');
                     e.stopPropagation();
                 });

              //$('#dialog-share-book input[name="linkexpiration"]').val('<%= Sessions.ExpirationTime %>');
        });

        function GetSearchDocs() {
            showLoader();
            if ($("#" + '<%= txtIndexVal.ClientID %>').val().length == 0 && $("#" + '<%= txtSearch.ClientID %>').val().length == 0) {
                alert("Please enter document name or index value to search");
                hideLoader();
                return false;
            }
        }

        function OnShare(obj) {
            $("#dialog-share-book").dialog({ title: "Share this WorkArea Document" });
            $('#dialog-share-book').dialog("open");
            $('#dialog-share-book input[name="email"]').val('<%=Sessions.SwitchedEmailId%>');
                $('#dialog-share-book input[name="myname"]').val('');
                $('#dialog-share-book input[name="emailto"]').val('');
                $('#dialog-share-book input[name="nameto"]').val('');
                //$('#dialog-share-book input[name="title"]').val('');
                <%--if ('<%= User.IsInRole("Offices") %>' == 'True')
                    $("#expirationTime").attr("readonly", false);
                else {
                    $("#updateExpiration").hide();
                    $("#expirationTime").css("width", "95%");
                }--%>
                $('#dialog-share-book input[name="linkexpiration"]').val('<%= Sessions.ExpirationTime %>');
                //GetExpirationTime();
                $('#dialog-share-book textarea[name="content"]').val('');

                $('#dialog-share-book input[name="email"]').css('border', '1px solid black');
                $('#dialog-share-book input[name="myname"]').css('border', '1px solid black');
                $('#dialog-share-book input[name="emailto"]').css('border', '1px solid black');

                $('#dialog-share-book input[name="email"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="email"]')) });

                $('#dialog-share-book input[name="emailto"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="emailto"]')) });
                $(<%= hdnSharePath.ClientID%>).val($(obj).closest('tr').find("input[type='hidden'][id$=hdnPath]").val());
                $(<%= hdnShareWorkAreaId.ClientID%>).val($(obj).closest('tr').find("input[type='hidden'][id$=hdnWorkAreaId]").val());
        }

        function ShowInboxOpacity(obj) {
            obj.style.getElementById("lnkbtnPreviewInbox")
            obj.style.opacity = 0.2;
        }

        function removeAllInboxEditable() {
            $('.txtFileName,.saveInboxfile,.cancelInboxfile').hide();
            $('.lblfileName,.editInboxfile').show();
        }

        function onClientUploadComplete(sender, e) {
            return false;
        }

        function onClientUploadCompleteAll(sender, e) {
            $('.hideControl').removeClass('hideControl');
            $('.uploadProgress').find('select').removeAttr('disabled');

            $('#divUpload').clone().appendTo('#uploadFieldset');
            hideLoader();
            clearInterval(myTimer);
            document.getElementById('uploadProgress').style.display = 'none';
            __doPostBack('', 'RefreshGrid@');

            setTimeout(function () {
                var vd = '<%= ConfigurationManager.AppSettings["VirtualDir"] %>';
                window.location.reload();
            }, 2000);
        }

        var myTimer;
        var fileIDDividerId = [];
        function onClientUploadStart(sender, e) {
            $.ajax(
                {
                    type: "POST",
                    url: '../Office/WorkArea.aspx/SaveSelectedPath',
                    data: "{ selectedPath:'" + $('#<%= hdnSelectedPath.ClientID %>').val() + "', selectedTreeId:'" + $('#<%= hdnWorkAreaTreeId.ClientID %>').val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: false,
                    success: function (result) {
                        document.getElementById('uploadCompleteInfo').innerHTML = "please wait while the " + e.get_filesInQueue() + " file is uploaded..";
                        document.cookie = "Files=" + JSON.stringify(fileIDDividerId);
                        showLoader();
                        $('[id*=ajaxfileupload1_Html5DropZone]').addClass('hideControl').css('display', 'none');
                        $('[id*=ajaxfileupload1_SelectFileContainer]').addClass('hideControl').css('display', 'none');
                        $('[id*=FileItemDeleteButton]').addClass('hideControl').css('display', 'none');
                        $('[id*=UploadOrCancelButton]').addClass('hideControl').css('display', 'none');
                        $('.TabDropDown').prop('disabled', 'disabled');
                        $('#uploadProgress').html('');
                        $('#uploadProgress').html('').html($('#divUpload').html());

                        document.getElementById('uploadProgress').style.display = 'block';
                        myTimer = setInterval(function () {
                            $('#uploadProgress').html('').html($('#divUpload').html());
                        }, 1000);
                    },
                    error: function (result) {
                        hideLoader();
                    }
                });
        }

        function showLoader() {
            $('#overlay').show().height($(document).height());
        }

        function hideLoader() {
            $('#overlay').hide();
        }


        function onClientUploadCompleteAll(sender, e) {
            $('.hideControl').removeClass('hideControl');
            $('.uploadProgress').find('select').removeAttr('disabled');

            $('#divUpload').clone().appendTo('#uploadFieldset');
            hideLoader();
            clearInterval(myTimer);
            document.getElementById('uploadProgress').style.display = 'none';
            $('#<%= hdnJsTreeData.ClientID %>').val("");
            __doPostBack('', 'RefreshGrid@');

            return false;
        }

        function onClientButtonClick() {
            $find('pnlPopup2').hide();

            setTimeout(function () {
            }, 1000);
            return false;
        }

        function checkInbox() {
            showLoader();
            ids = "";
            if ($('table input[type="checkbox"]:checked').length > 0) {
                $('table input[type="checkbox"]:checked').each(function () {
                    if ($(this).closest('tr').find("input[type='hidden'][id$=hdnInboxValues]") && $(this).closest('tr').find("input[type='hidden'][id$=hdnInboxValues]").val())
                        ids += JSON.parse($(this).closest('tr').find("input[type='hidden'][id$=hdnInboxValues]").val()).InboxId + ",";
                });
                ids = ids.substring(0, ids.length - 1);
                $('#<%= hdnInboxIDs.ClientID %>').attr("value", ids); // set hidden field value
                return true;
            }
            else {
                hideLoader();
                alert("Please Select atleast one file.");
                return false;
            }
        }
        function checkInboxForWorkArea() {
            if ($('#<%= hdnSelectedPath.ClientID %>').val().length <= 0) {
                hideLoader();
                alert("Please select a folder in WorkArea.");
                return false;
            }
            return checkInbox();
        }

        function checkPending() {
            showLoader();
            ids = "";
            if ($('table input[type="checkbox"]:checked').length > 0) {
                $('table input[type="checkbox"]:checked').each(function () {
                    if ($(this).closest('tr').find("input[type='hidden'][id$=hdnPendingValues]") && $(this).closest('tr').find("input[type='hidden'][id$=hdnPendingValues]").val())
                        ids += $(this).closest('tr').find("input[type='hidden'][id$=hdnPendingValues]").val() + ",";
                });
                ids = ids.substring(0, ids.length - 1);
                $('#<%= hdnPendingIDs.ClientID %>').attr("value", ids); // set hidden field value
                return true;
            }
            else {
                hideLoader();
                alert("Please Select atleast one file.");
                return false;
            }
        }

        function checkPendingForWorkArea() {
            if ($('#<%= hdnSelectedPath.ClientID %>').val().length <= 0) {
                hideLoader();
                alert("Please select a folder in WorkArea.");
                return false;
            }
            return checkPending();
        }

        function showInboxEdit(ele) {
            $('.saveInboxfile,.cancelInboxfile').hide();
            $('.deleteInboxfile,.lnkbtnPreview,.editInboxfile,.lnkBtnDownload, .lnkBtnShare, .lnkBtnAddToTab').show();
            var $this = $(ele);
            $this.css('display', 'none');
            $this.parent().find('[name=lbkUpdate]').show();
            $this.parent().find('[name=lnkCancel]').show();
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                $(this).find('input[type=text]').css('border', '1px solid #c4c4c4');
                $(this).find('input[type = text]').show();
                if ($(this).find('input[type = text]').val() == undefined || $(this).find('input[type = text]').val() == "") { }
                else {
                    oldname = $(this).find('input[type = text]').val();
                }
                $(this).find('[name=lblfileName]').hide();
                $(this).find('[name=lblClass]').hide();
                $(this).parent().find('.deleteInboxfile,.lnkbtnPreview,.lnkBtnDownload, .lnkBtnShare, .lnkBtnAddToTab, .editdocs').hide();
            });
            return false;
        }

        function ShowDeleteError() {
            alert("For security, document cannot be deleted. Please contact EdFiles if you would to delete files from work area.");
            return false;
        }

        function SetPathVal(e) {
            $(<%= hdnSharePath.ClientID%>).val($(e).closest('tr').find("input[type='hidden'][id$=hdnPath]").val());
            return false;
        }

        function cancleInboxFileName(ele) {
            var $this = $(ele);
            var InboxlabelName = '';
            var FileName = '';
            $this.css('display', 'none');
            $this.parent().find('[name=lbkUpdate]').hide();
            $this.parent().find('[name=lnkEdit]').show();
            $this.parent().find('.deleteInboxfile,.lnkbtnPreview,.lnkBtnDownload, .lnkBtnShare, .lnkBtnAddToTab, .editdocs').show();
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                var len = 0;
                len = $(this).find('input[type=text]').length;
                if (len > 0) {
                    InboxlabelName = $(this).find('[name=InboxfileName]').html();
                    $(this).find('input[type = text]').val(InboxlabelName);
                    $(this).find('input[type = text]').hide();
                    $(this).find('[name=InboxfileName]').show();
                }
                else {
                }
            });
        }

        function checkAllThumbnail() {

            var selectAllValue = $("#btnCheckedAll").is(":checked");

            $('iFrame').contents().find('.check').each(function (k, v) {
                if (selectAllValue)
                    $(v).attr('checked', true);
                else
                    $(v).attr('checked', false);
            });
        }

        function ShowAddToTabPopup() {
            if (GetSplitPdfValue()) {
                $.ajax(
                    {
                        type: "POST",
                        url: '../FlexWebService.asmx/GetFoldersData',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $('<option />', { value: 0, text: "Select a file" }).appendTo($("#comboboxSplit")); $.each(data.d, function (i, text) {
                                $('<option />', { value: i, text: text }).appendTo($("#comboboxSplit"));
                            });
                            $("#comboboxSplit").chosen({ width: "315px" });

                            ChangeDivider();
                        },
                        fail: function (data) {
                        }
                    });
                var popup = document.getElementById("preview_popup");
                popup.style.display = 'none';

                var popup = document.getElementById("preview_popup_split");
                popup.style.display = 'block';
            }
        }

        function ChangeDivider() {
            $("#comboboxDividerSplit").html('');
            $("#comboboxDividerSplit").chosen("destroy");
            var selectedFolder = $("#comboboxSplit").chosen().val();
            $.ajax(
                {
                    type: "POST",
                    url: '../FlexWebService.asmx/GetDividersByEFF',
                    data: "{eff:" + parseInt(selectedFolder) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $('<option />', { value: 0, text: "Select a tab" }).appendTo($("#comboboxDividerSplit"));
                        $.each(data.d, function (i, text) {
                            $('<option />', { value: i, text: text }).appendTo($("#comboboxDividerSplit"));
                        });
                        $("#comboboxDividerSplit").chosen({ width: "315px" });
                    },
                    fail: function (data) {
                    }
                });
        }

        function ShowConfirm(message) {
            if (confirm(message)) {
                window.location.href = window.location.href;
            }
            else {
                ClearFolderIdSource();
                window.location.href = window.location.origin + '/Office/FileFolderBox.aspx?id=' + $('#<%= hdnSelectedFolderID.ClientID%>').val();
            }
        }

        function ClearFolderIdSource() {
            $.ajax(
                {
                    type: "POST",
                    url: '../EFileFolderJSONService.asmx/ClearFolderIdSource',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json"
                });
        }

        function GetSplitPdfValue() {
            var pageNumber = '';
            if ($('iFrame').contents().find('.check').length > 0) {
                $('iFrame').contents().find('.check').each(function (k, v) {
                    var val = $(v).is(":checked");
                    if (val) {
                        if (pageNumber == '')
                            pageNumber += $(v).attr('data-value') + ',';
                        else
                            pageNumber += $(v).attr('data-value') + ',';
                    }

                });
                pageNumber = pageNumber.substring(0, pageNumber.length - 1);

                if (pageNumber.length <= 0) {
                    alert("Please select at least one page.");
                    return false;
                }

                $("#" + '<%= hdnPageNumbers.ClientID %>').val(pageNumber);
            }
            else {
                alert('Please select thumbnail');
                return false;
            }
            return true;
        }

        var newname, Path, oldName, inboxId, inboxFileExtention;
        function OnSaveRename(txtRenameObj, path, ext, workAreaId) {
            var test = $('#<%= hdnSelectedPath.ClientID %>').val();
             var newFileName = $('#' + txtRenameObj)[0].value;
             var extension = "";
             if (newFileName && newFileName.indexOf('.') > 0)
                 extension = newFileName.substring(newFileName.indexOf('.') + 1).toLowerCase();

             //if (selectedPath == "") {
             //    alert("Please select file to rename");
             //}
             //else 
             if (newFileName == "") {
                 alert("Please enter new file name");
             }
             else if (!/^[a-z\d][a-z\d\()_\-.\s]+$/i.test(newFileName)) {
                 alert("File name can not contain special characters.");
             }
             else if (newFileName.indexOf('.') > 0 && (extension != 'jpg') && (extension != 'jpeg') && (extension != 'pdf') && (extension != 'doc') && (extension != 'docx') && (extension != 'xls')) {
                 alert("Only File with type 'jpg,jpeg,pdf,doc,docx and xls' is allowed.");
             }
             else {
                 showLoader();
                 $.ajax({
                     type: "POST",
                     url: '../Office/WorkArea.aspx/RenameObject',
                     contentType: "application/json; charset=utf-8",
                     data: "{ oldObjectPath:'" + path + "', newObjectPath:'" + newFileName + "', selectedPath:'" + $('#<%= hdnSelectedPath.ClientID %>').val() + "', workAreaId:'" + workAreaId + "', searchKey:'" + $('#<%= txtSearch.ClientID %>').val() + "'}",
                    dataType: "json",
                    success: function (data) {
                        if (data.d.length > 0) {
                            var objList = JSON.parse(data.d);
                            if (objList.ErrorMessage && objList.ErrorMessage.length > 0) {
                                alert("Failed to rename document. Please try again after sometime.");
                                hideLoader();
                            }
                        }
                        else {
                            __doPostBack('', 'RefreshGrid@');
                        }
                    },
                    error: function (result) {
                        console.log('Failed' + result.responseText);
                        hideLoader();
                        $('#lblSelectedTreeNode').html("");
                    }
                });
            }
        }

        function showPreview(URL) {
            // URL = URL.substr(1);
            hideLoader();
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//"
                    + window.location.hostname
                    + (window.location.port ? ':' + window.location.port : '');
            }


            //var src = 'http://docs.google.com/gview?url=' + window.location.origin + '/' + URL.trim() + '&embedded=true';
            //if (window.location.origin.indexOf('https://') > -1)
            //    src = 'https://docs.google.com/gview?url=' + window.location.origin + '/' + URL.trim() + '&embedded=true';

            src = './Preview.aspx?data=' + window.location.origin + URL.trim() + "?v=" + "<%= DateTime.Now.Ticks%>";
            var popup = document.getElementById("preview_popup");
            var overlay = document.getElementById("overlay");
            $('#reviewContent').attr('src', src);
            overlay.style.display = 'block';
            popup.style.display = 'block';
            $('#preview_popup').focus();
            disableScrollbar();
            return false;
        }

        function showPreviewForThumbnails(URL) {
            // URL = URL.substr(1);
            hideLoader();
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//"
                    + window.location.hostname
                    + (window.location.port ? ':' + window.location.port : '');
            }


            //var src = 'http://docs.google.com/gview?url=' + window.location.origin + '/' + URL.trim() + '&embedded=true';
            //if (window.location.origin.indexOf('https://') > -1)
            //    src = 'https://docs.google.com/gview?url=' + window.location.origin + '/' + URL.trim() + '&embedded=true';

            src = './Preview.aspx?data=' + window.location.origin + URL.trim() + "?v=" + "<%= DateTime.Now.Ticks%>";
            var popup = document.getElementById("preview_popup_thumbnails");
            var overlay = document.getElementById("overlay");
            $('#reviewContent_thumbnail').attr('src', src);
            overlay.style.display = 'block';
            popup.style.display = 'block';
            $('#preview_popup_thumbnails').focus();
            disableScrollbar();
            return false;
        }

        function showPreviewEFileSplit(URL) {
            // URL = URL.substr(1);
            hideLoader();
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//"
                    + window.location.hostname
                    + (window.location.port ? ':' + window.location.port : '');
            }


            //var src = 'http://docs.google.com/gview?url=' + window.location.origin + '/' + URL.trim() + '&embedded=true';
            //if (window.location.origin.indexOf('https://') > -1)
            //    src = 'https://docs.google.com/gview?url=' + window.location.origin + '/' + URL.trim() + '&embedded=true';

            src = './Preview.aspx?data=' + window.location.origin + URL.trim() + "?v=" + "<%= DateTime.Now.Ticks%>";
            var popup = document.getElementById("preview_popup");
            var overlay = document.getElementById("overlay");
            $('#reviewContent').attr('src', src);
            overlay.style.display = 'block';
            popup.style.display = 'block';
            $('#preview_popup').focus();
            disableScrollbar();
            return false;
        }


        function ShowPreviewForWorkArea(path, fileName, workAreaId) {
            showLoader();
            var folderID = <%= (int)Enum_Tatva.Folders.WorkArea %>
            $.ajax({
                type: "POST",
                url: '../Office/WorkArea.aspx/GetPreviewURL',
                contentType: "application/json; charset=utf-8",
                data: "{ documentID:'" + 0 + "', folderID:'" + folderID + "', shareID:'" + path + "', workAreaId:'" + workAreaId + "'}",
                dataType: "json",
                success: function (data) {
                    if (data.d == "") {
                        hideLoader();
                        alert("Failed to get thumbnails!");
                        return false;
                    }
                    else {
                        if (data.d.length > 1) {
                            $('.thumbnailsPart').show();
                            $("#thumnailImages").html("");
                            showPreview(data.d[0]);
                            $("#" + '<%= hdnFileName.ClientID %>').val(data.d[0]);
                            $("#" + '<%= hdnFileId.ClientID %>').val(data.d[2]);
                                $('.thumbnailsPart').hide();
                                $("#thumnailImages").html("");
                                $('#preview_popup').css("max-width", "1300px");
                            }
                        }
                    },
                    error: function (result) {
                        console.log('Failed' + result.responseText);
                        hideLoader();
                    }
                });
            return false;
        }

        function DownloadWADoc(path) {
            window.open(PrepareURLForWorkArea(path), '_blank');
        }

        function DisplayAddToTabModal(e) {
            e.preventDefault();
            $('#AddToTab_popup,#overlay').show().focus();
            $('#combobox').trigger('chosen:open');
            $(<%= hdnSharePath.ClientID%>).val($(e.currentTarget).closest('tr').find("input[type='hidden'][id$=hdnPath]").val());
            $(<%= hdnShareWorkAreaId.ClientID%>).val($(e.currentTarget).closest('tr').find("input[type='hidden'][id$=hdnWorkAreaId]").val());
            e.stopPropagation();
            disableScrollbar();
        }

        function PrepareURLForWorkArea(obj) {
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//"
                    + window.location.hostname
                    + (window.location.port ? ':' + window.location.port : '');
            }
            var hdnId;
            if (obj != null && obj != undefined) {
                var hdnObj = $(document.getElementById(obj)).closest('tr').find("input[type='hidden'][id$=hdnPath]");
                hdnId = $(document.getElementById(obj)).closest('tr').find("input[type='hidden'][id$=hdnWorkAreaId]").val();
                path = $(hdnObj).val();
                var tempURL = "path=" + path + "&ID=" + hdnId + "<%= "&Data="+ QueryString.QueryStringEncode("ID=&folderID=" +(int)Enum_Tatva.Folders.WorkArea + "&sessionID=" +Sessions.SwitchedRackspaceId )%>";
            }
            else {
                path = $("#" + '<%= hdnSharePath.ClientID%>').val();
                hdnId = $("#" + '<%= hdnShareWorkAreaId.ClientID%>').val();
                var tempURL = "path=" + path + "&ID=" + hdnId + "<%= "&Data="+ QueryString.QueryStringEncode("ID=&dateTime="+ DateTime.UtcNow.ToString("yyyyMMddHHmmssfff") +"&folderID=" +(int)Enum_Tatva.Folders.WorkArea + "&sessionID=" +Sessions.SwitchedRackspaceId )%>";
            }<%--var url = window.location.origin + "<%= ConfigurationManager.AppSettings["VirtualDir"]%>" + "Office/PdfViewer.aspx?" + tempURL;--%>
            var url = window.location.origin + "/Office/PdfViewer.aspx?" + tempURL;
            url = encodeURI(url);
            return url;
        }

        function disableScrollbar() {
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }

        function enableScrollbar() {
            $('html, body').css({
                'overflow': 'auto',
                'height': 'auto'
            });
        }

        var newPendingname, pendingFilePath, eFileFlowId, pendingFileExtention;
        function OnSaveRenameForPending(txtPendingRenameObj, pendingpath, eFileFlowShareID, ext) {
            pendingFilePath = pendingpath;
            eFileFlowShareId = eFileFlowShareID;
            pendingFileExtention = ext;
            newPendingname = $('#' + txtPendingRenameObj)[0].value;
            if (newPendingname == undefined || newPendingname == "") {
                $('.txtPendingfileName').css('border', '1px solid red');
                return false;
            }
            else {
                $('.txtPendingfileName').css('border', '1px solid c4c4c4');
            }
            var userId = "<%= Sessions.SwitchedRackspaceId%>";
            showLoader();
            $.ajax(
                {
                    type: "POST",
                    url: '../EFileFolderJSONService.asmx/RenamePendingDocument',
                    data: "{ renameFile:'" + newPendingname + "', path:'" + pendingFilePath + "',eFileFlowShareId:'" + eFileFlowShareId + "',pendingFileExtention:'" + pendingFileExtention + "',userId:'" + userId + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        hideLoader();
                        __doPostBack('', 'RefreshGrid@');
                    },
                    error: function (result) {
                        hideLoader();
                    }
                });
        }

        function Check_ClickInbox(objRef) {
            var row = objRef.parentNode.parentNode.parentNode;
            var GridView = row.parentNode;
            var inputList = GridView.getElementsByTagName("input");

            for (var i = 0; i < inputList.length; i++) {
                var headerCheckBox = inputList[0];
                var checked = true;
                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {
                    if (!inputList[i].checked) {
                        checked = false;
                        break;
                    }
                }
            }
            headerCheckBox.checked = checked;
        }

        function checkAllInbox(objRef) {
            var inboxGridView = objRef.parentNode.parentNode.parentNode.parentNode;
            var inboxinputList = inboxGridView.getElementsByTagName("input");

            for (var i = 0; i < inboxinputList.length; i++) {
                var row = inboxinputList[i].parentNode.parentNode;
                if (inboxinputList[i].type == "checkbox" && objRef != inboxinputList[i]) {
                    if (objRef.checked) {
                        inboxinputList[i].checked = true;
                    }
                    else {
                        inboxinputList[i].checked = false;
                    }
                }
            }
        }

        function checkAllPending(objPendingRef) {
            var pendingGridView = objPendingRef.parentNode.parentNode.parentNode.parentNode;
            var pendinginputList = pendingGridView.getElementsByTagName("input");

            for (var i = 0; i < pendinginputList.length; i++) {
                var pendingrow = pendinginputList[i].parentNode.parentNode;
                if (pendinginputList[i].type == "checkbox" && objPendingRef != pendinginputList[i]) {
                    if (objPendingRef.checked) {
                        pendinginputList[i].checked = true;
                    }
                    else {
                        pendinginputList[i].checked = false;
                    }
                }
            }
        }

        function Check_ClickPending(objPendingRef) {
            var pendingrow = objPendingRef.parentNode.parentNode.parentNode;
            var pendingGridView = pendingrow.parentNode;
            var pendinginputList = pendingGridView.getElementsByTagName("input");

            for (var i = 0; i < pendinginputList.length; i++) {
                var pendingheaderCheckBox = pendinginputList[0];
                var checked = true;
                if (pendinginputList[i].type == "checkbox" && pendinginputList[i] != pendingheaderCheckBox) {
                    if (!pendinginputList[i].checked) {
                        checked = false;
                        break;
                    }
                }
            }
            pendingheaderCheckBox.checked = checked;
        }

        function OpenIndexPopup(ele) {
            $("#" + '<%= hdnShareWorkAreaId.ClientID%>').val($(ele).closest('tr').find("input[type='hidden'][id$=hdnIndexWorkAreaId]").val());
            var data = { "workAreaId": $(ele).closest('tr').find("input[type='hidden'][id$=hdnIndexWorkAreaId]").val() }
            $.ajax({
                type: "POST",
                url: "../Office/WorkArea.aspx/GetWorkAreaIndexValues",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (result.d.length > 0) {
                        //$("#hndDividerIdIndex").val(JSON.parse($(ele).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()).DocId);
                        var $this = $(ele);
                        var path = '/' + $($this).parent().find('[name*=hdnPathIndex]').val();
                        path = path.replace(/\\/g, "/");

                        $('#PanelIndex,#overlay').show().focus();
                        $("#floatingCirclesG").css('display', 'none');
                        var index = 0;
                        var arrIndex = [];
                        $.each($.parseJSON(result.d), function (i, e) {
                            arrIndex.push(e);
                        });
                        $("#tblIndex").find("tr:gt(0)").remove();
                        var row = $("#tblIndex tr:first");
                        $(row).find("#txtKey").val("");
                        $(row).find("#txtValue").val("");
                        $(row).find("#btnAddIndexPair").show();
                        $(row).find("#btnRemoveIndexPair").hide();
                        if (arrIndex.length > 0) {
                            $(row).find(".hdnIndexId").val(arrIndex[0].IndexId);
                            $(row).attr("id", "indexRow" + arrIndex[0].IndexId);
                        }
                        else
                            $(row).find(".hdnIndexId").val(0);
                        for (var i = 0; i < arrIndex.length; i++) {
                            if (i == 0) {
                                SetIndexValues(arrIndex[i], true);
                            }
                            else
                                AppendIndexDetails(arrIndex[i], i);
                        }
                    }
                }
            });
        }

        function AppendIndexDetails(indexPair, index) {
            CreateDynamicIndexTextBox(indexPair.IndexId, true);
            SetIndexValues(indexPair, false);
        }

        function SetIndexValues(indexPair, isRender) {
            var row = "";
            if (!isRender)
                row = document.getElementById("indexRow" + indexPair.IndexId);
            else
                row = $("#tblIndex tr:first");
            $(row).find("#txtKey").val(indexPair.IndexKey);
            $(row).find("#txtValue").val(indexPair.IndexValue);
            $(row).find(".hdnIndexId").val(indexPair.IndexId);
        }

        function GetDynamicTextBox(value) {
            var test = '<td><input type="hidden" value ="0" class="hdnIndexId"></td>' +
                '<td><input type="text" ID = "txtKey" Width="90%" MaxLength="20" placeholder="Key" style="height:25px;margin-left:10px;"/></td>' +
                '<td><input type="text" ID= "txtValue" Width="90%" MaxLength="20" placeholder="Value" style="height:25px;"/></td>' +
                        '<td><button id="btnAddIndexPair" type="button" Width="90%"  OnClick="return AddIndexTextBoxes(this)"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/add-icon.png" title="Add" style="margin-bottom:0px;"/> </button> </td>' +
                        '<td><button id="btnRemoveIndexPair"  type="button" Width="90%"  OnClick="return DisplayConfirmBox(this)"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" style="margin-bottom:0px;"/> </button>'

            return test;
        }

        var indexRemoveEle = "";
        function DisplayConfirmBox(ele) {
            $("#PanelIndex").hide();
            $('#DivDeleteConfirmIndex,#overlay').show().focus();
            indexRemoveEle = ele;
        }

        function AddIndexTextBoxes(ele) {
            var index = parseInt($($(ele).parent().parent().find('[class*=hdnIndexId]')).val());
            if ($(ele).closest('tr').find('#txtKey').val() == "") {
                alert("Please enter Key Value.");
                return false;
            }
            CreateDynamicIndexTextBox(index, false);
            return false;
        }

        function CreateDynamicIndexTextBox(index, isRender) {
            var tr = document.createElement('tr');
            tr.innerHTML = GetDynamicTextBox(index);

            if (!isRender)
                tr.setAttribute("id", "indexRow" + (index = index + 1));
            else
                tr.setAttribute("id", "indexRow" + index);
            // var body = $(parentElement).parent();
            $('table#tblIndex tr:last').after(tr);
            $(tr).find("#txtKey").focus();
            SetIndexButtonVisibility();

            //Allow maximum of 5 key,value pairs
            if ($('table#tblIndex tr').length == 5) {
                $(tr).find('#btnAddIndexPair').hide();
            }
        }

        var deleteIndexData = "";
        function RemoveIndexTextBoxes() {
            $('#DivDeleteConfirmIndex').hide();
            $('#PanelIndex,#overlay').show().focus();
            //var index = parseInt($($(indexRemoveEle).parent().parent().find('[class*=hdnIndexId]')).val());
            //var parentElement = document.getElementById("indexRow" + index);
            //if (parentElement == null)
            //    parentElement = $('.indexRow');
            //$('table#tblIndex').find(parentElement).remove();
            if ($(indexRemoveEle).closest('tr').find('input.hdnIndexId').val() != 0) {
                var hdnId = $(indexRemoveEle).closest('tr').find('input.hdnIndexId').val();
                var test = deleteIndexData.indexOf(hdnId);
                if (test <= 0)
                    deleteIndexData = deleteIndexData + hdnId + ",";
            }
            $(indexRemoveEle).closest('tr').remove();
            SetIndexButtonVisibility();
            indexRemoveEle = "";
            return false;
        }


        function SetIndexButtonVisibility() {
            var rowCount = $('table#tblIndex tr').length;
            for (i = 0; i < rowCount; i++) {
                var $item = $($('table#tblIndex tr')[i]);

                if (i == (rowCount - 1)) {
                    $item.find('#btnRemoveIndexPair').show();
                    $item.find('#btnAddIndexPair').show();
                    if (i == 0) {
                        $item.find('#btnRemoveIndexPair').hide();
                    }
                }
                else if (i == 0) {
                    $item.find('#btnRemoveIndexPair').show();
                    $item.find('#btnAddIndexPair').hide();
                }
                else {
                    $item.find('#btnRemoveIndexPair').show();
                    $item.find('#btnAddIndexPair').hide();
                }
            }
        }

    </script>
    <asp:HiddenField ID="hdnInboxIDs" runat="server" />
    <asp:HiddenField ID="hdnPendingIDs" runat="server" />
    <asp:HiddenField runat="server" ID="hdnPageNumbers" />
    <asp:HiddenField ID="hdnSelectedFolderID" runat="server" />
    <asp:HiddenField ID="hdnSelectedDividerID" runat="server" />
    <asp:HiddenField ID="hdnSelectedTabText" runat="server" />
    <asp:HiddenField ID="hdnSelectedFolderText" runat="server" />
    <asp:HiddenField runat="server" ID="hdnFileName" />
    <asp:HiddenField runat="server" ID="hdnFileId" />
    <asp:HiddenField ID="hdnShareWorkAreaId" runat="server" />
    <asp:HiddenField ID="hdnSharePath" runat="server" />
    <asp:HiddenField ID="hdnWorkAreaTreeId" runat="server" />
    <asp:HiddenField ID="hdnSelectedPath" runat="server" />

    <div class="popup-mainbox preview-popup" id="AddToTab_popup" style="display: none; height: 460px; width: 395px; max-width: 395px; max-height: 460px; margin-top: 20px;">
        <div class="popupcontrols popup-head" style="width: 100%; height: 60px;">
            Select Folder
        <span id="Span2" class="ic-icon ic-close popupclose"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%;">
            <div class="popup-scroller" style="height: 75%">
                <p style="margin: 20px">
                    <uc1:ucAddToTab ID="ucAddToTab1" runat="server" />
                </p>

            </div>
            <div class="popup-btn">
                <%--<input  runat="server"  type="button" value="Save"  />--%>
                <asp:Button ID="btnSelectAddToTab" Text="Save" runat="server" CssClass="btn green" OnClick="btnSelectAddToTab_Click" Style="margin-right: 5px;" />
                <input id="btnCancelAddToTab" type="button" value="Cancel" class="btn green" />
            </div>
        </div>
    </div>


    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Work Area</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>

    <div class="popup-mainbox preview-popup" id="preview_popup" style="display: none">
        <div class="popupcontrols" style="width: 100%; height: 5%;">
            <span id="popupclose" class="ic-icon ic-close"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%;">
            <table style="width: 100%; height: 100%">
                <tr style="height: 5%">
                    <td>
                        <input type="button" id="btnSpltBtn" runat="server" value="Tab" onclick="ShowAddToTabPopup();" style="margin-bottom: 10px;" class="btn green btnAddtoTab" title="Add To Tab" />
                        <%--                        <input type="button" id="btnSplitAddToWA" runat="server" value="WorkArea" onclick="ShowAddtoWAPopup();" style="margin-bottom: 10px;" class="btn green btnAddtoWorkArea" title="Add To WorkArea" />--%>
                        <div class="selecttabcheck"><span class="selectalltab">Select All</span><input type="checkbox" id="btnCheckedAll" value="Select All" onclick="checkAllThumbnail();" /></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <iframe id="reviewContent" style="width: 100%; height: 100%;"></iframe>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <%--    <div class="popup-mainbox preview-popup-thumbnails" id="preview_popup_thumbnails" style="display: none">
        <div class="popupcontrols" style="width: 100%; height: 5%;">
            <span id="popupclose_thumbnail" class="ic-icon ic-close"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%;">
            <table style="width: 100%; height: 100%">
                <tr>
                    <td style="height: 100px; width: 250px !important;" class="thumbnailsPart">
                        <div style="margin-bottom: 5px;">
                            <input type="button" id="btnSpltBtn" value="Tab" runat="server" onclick="ShowAddToTabPopup();" style="margin-bottom: 10px;" class="btn green btnAddtoTab" title="Add To Tab" />
                            <input type="checkbox" id="btnCheckedAll" value="Select All" onclick="checkAllThumbnail();" style="float: right; margin-right: 20px; margin-top: 10px;" />
                        </div>
                        <div id="thumnailImages" style="height: 90%; overflow-y: scroll">
                        </div>
                    </td>
                    <td>
                        <iframe id="reviewContent_thumbnail" style="width: 100%; height: 100%;"></iframe>
                    </td>
                </tr>
            </table>
        </div>
    </div>--%>

    <div class="popup-mainbox preview-popup" id="preview_popup_split" style="display: none; height: 460px; width: 395px; max-width: 395px; max-height: 460px; margin-top: 20px;">
        <div class="popupcontrols popup-head" style="width: 100%; height: 60px;">
            Split PDF Details
        <span id="popupclose_split" class="ic-icon ic-close popupclose"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%; background: #eaeaea; border-top: 2px solid darkgray;">
            <div class="popup-scroller" style="height: 75%; background: #eaeaea;">
                <p style="margin: 20px">
                    <label>New split Pdf name</label>
                    <asp:TextBox runat="server" ID="txtNewPdfName" Style="margin-bottom: 5px;"></asp:TextBox>
                    <br />
                    <label>1. Select an edFile from list</label>
                    <select id="comboboxSplit">
                    </select>

                    <label style="display: block; margin-top: 15px">2. Select a tab for the selected edFile</label>
                    <select id="comboboxDividerSplit" style="display: none;">
                    </select>
                </p>

            </div>
            <div class="popup-btn" style="background: #eaeaea;">

                <asp:Button ID="btnSaveSplitPdf" runat="server" Text="Save" CssClass="btn green" OnClick="btnSaveSplitPdf_Click" Style="margin-right: 5px;" />
                <input id="btnCancelAddToTabSplit" type="button" value="Cancel" class="btn green" />
            </div>
        </div>
    </div>


    <div class="page-container register-container" id="workarea" style="padding-top: 0">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">

                <div class="left-content">

                    <asp:Panel ID="panelMain" runat="server" Visible="true">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div id="workareatreeview" class="content-box listing-view" style="padding-top: 0px;">
                                    <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>


                                    <div id="uploadProgress" class="white_content" style="font-size: 14px;"></div>

                                    <fieldset id="uploadFieldset">
                                        <div id="divUpload">
                                            <div class='clearfix'></div>
                                            <asp:Image runat="server" ID="Imagelabel12" ImageUrl="../Images/loader_bg.gif" ImageAlign="Middle" />
                                            <asp:Label ID="label12" runat="server"></asp:Label>
                                            <asp:Label runat="server" ID="myThrobber" Style="display: none;"><img align="absmiddle" alt="" src="uploading.gif"/></asp:Label>
                                            <label>Work Area is your document inbox, designed to hold documents temporarily that are not ready to be filed in a tab or in an active folder.</label>
                                            <ajaxToolkit:AjaxFileUpload ID="AjaxFileUpload1" runat="server" Padding-Bottom="4"
                                                Padding-Left="2" Padding-Right="1" Padding-Top="4"
                                                ThrobberID="myThrobber"
                                                ContextKeys="2"
                                                MaximumNumberOfFiles="10"
                                                AzureContainerName=""
                                                
                                                OnUploadComplete="AjaxFileUpload1_OnUploadComplete"
                                                OnClientUploadComplete="onClientUploadComplete"
                                                OnUploadCompleteAll="AjaxFileUpload1_UploadCompleteAll"
                                                OnClientUploadCompleteAll="onClientUploadCompleteAll"
                                                OnUploadStart="AjaxFileUpload1_UploadStart"
                                                OnClientUploadStart="onClientUploadStart" />

                                            <%--<asp:Button ID="btnRefreshGrid" runat="server" Style="display: none" OnClick="btnRefreshGrid_Click" />--%>
                                            <div class='clearfix'></div>
                                            <div id="uploadCompleteInfo">
                                            </div>
                                    </fieldset>


                                    <div style="position: relative; width: 100%; display: inline-block; border-top: 1px solid #E6E6E6; padding: 10px;" id="jsTreeParent">
                                        <span>
                                            <span>
                                                <%--<asp:button id="btnworkareacreate" runat="server" cssclass="icon-btn create-btn" onclientclick="return false" tooltip="create folder" style="float: inherit; display: inline-block; margin-top: 7px;" />
                                                <span class="create-folderbox" id="workareafolderbox" style="float: inherit; min-width: 260px; max-width: 260px; position: absolute; margin-left: 5px; margin-top: 7px; margin-right: 80px;">
                                                    <asp:textbox id="txtworkarea" runat="server" class="find-input folderbox" maxlength="150" autocomplete="off" autopostback="false" tabindex="101" style="width: 165px;" />
                                                    <asp:button id="btncancel" runat="server" cssclass="icon-btn cancel-btn" onclientclick="return false" tooltip="cancel" tabindex="103" style="float: right; vertical-align: top" />
                                                    <asp:button id="btnperformaction" runat="server" cssclass="icon-btn ic-save ic-icon" onclientclick="return false" tooltip="save" tabindex="102" style="float: right; vertical-align: top" />
                                                </span>--%>
                                                <div style="display: inline-block; float: right; padding: 10px;" id="create-rename-block">
                                                    <div class="rename-block">
                                                        <asp:TextBox runat="server" ID="txtIndexVal" autocomplete="off" AutoCompleteType="Disabled" MaxLength="50" placeholder="Enter Index Value to search" class="find-input" Style="margin-left: 5px; height: 34px; padding-right: 5px; padding: 5px; border: 2px solid #d4d4d4" TabIndex="1"></asp:TextBox>
                                                        <asp:TextBox type="text" MaxLength="50" ID="txtSearch" class="find-input" autocomplete="off" AutoCompleteType="Disabled" placeholder="Enter Document Name to search" runat="server" TabIndex="2" Style="width: 210px; margin-left: 5px; height: 34px; padding-right: 5px; padding: 5px; border: 2px solid #d4d4d4" />
                                                        <asp:Button ID="btnWorkAreaSearch" OnClientClick="return GetSearchDocs();" OnClick="btnWorkAreaSearch_Click" imagealign="AbsMiddle" CssClass="quick-find-btn btnSearch" runat="server" TabIndex="3" Style="top: 3px; height: 30px; width: 30px;" />
                                                    </div>
                                                </div>
                                            </span>

                                            <%--                                                <div id="jstree1" style="width: 20%; float: left; margin-top: 20px; max-height: 530px; overflow: auto;">
                                                    <div id="jstreepreload" class="jstree jstree-1 jstree-default" role="tree" aria-multiselectable="true" tabindex="0" aria-activedescendant="node1" aria-busy="false">
                                                        <ul class="jstree-container-ul jstree-children" role="group">
                                                            <li role="treeitem" aria-selected="false" aria-level="1" aria-labelledby="node1_anchor" aria-expanded="false" id="node1" class="jstree-node  jstree-closed jstree-last">
                                                                <i class="jstree-icon jstree-ocl" role="presentation"></i>
                                                                <a class="jstree-anchor" href="#" tabindex="-1" id="node1_anchor">
                                                                    <i class="jstree-icon jstree-themeicon" role="presentation"></i>workarea
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>--%>
                                        </span>
                                        <%--<label id="lblSelectedTreeNode" class="selectedTreeNodeLabel"></label>--%>
                                        <%--<div id="circularG">
                                            <div id="circularG_1" class="circularG"></div>
                                            <div id="circularG_2" class="circularG"></div>
                                            <div id="circularG_3" class="circularG"></div>
                                            <div id="circularG_4" class="circularG"></div>
                                            <div id="circularG_5" class="circularG"></div>
                                            <div id="circularG_6" class="circularG"></div>
                                            <div id="circularG_7" class="circularG"></div>
                                            <div id="circularG_8" class="circularG"></div>
                                        </div>--%>
                                        <div>
                                            <div class="table-toolbar">
                                                <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" PageSize="10" CssClass="opt-select" PagerStyle="NumericPages" ControlToPaginate="grdWorkAreaFiles"></cc1:WebPager>
                                            </div>
                                            <asp:GridView ID="grdWorkAreaFiles" runat="server" AllowSorting="True" AllowPaging="true"
                                                AutoGenerateColumns="false" BorderWidth="2" OnRowDataBound="grdWorkAreaFiles_RowDataBound"
                                                CssClass="datatable listing-datatable workareatable" Width="100%" OnSorting="grdWorkAreaFiles_Sorting">
                                                <PagerSettings Visible="False" />
                                                <AlternatingRowStyle CssClass="odd" />
                                                <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                <Columns>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="15%" SortExpression="FileName">
                                                        <HeaderTemplate>
                                                            <asp:LinkButton ID="lblfileName" runat="server" Text="Document Name" CommandName="Sort" CommandArgument="FileName" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblfileName" runat="server" Text='<%#Eval("FileName")%>' name="lblfileName" CssClass="lblfileName" Style="cursor: pointer; text-decoration: underline; color: blue" />
                                                            <asp:TextBox runat="server" MaxLength="50" Text='<%#Eval("FileName")%>'
                                                                ID="txtFileName" Style="display: none; height: 30px;" name="txtFileName" CssClass="txtFileName" />
                                                            <asp:HiddenField ID="hdnIndexWorkAreaId" runat="server" Value='<%# Eval("WorkAreaId") %>' />
                                                            <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="CreatedDate" HeaderText="Modified Date" ItemStyle-Width="20%"
                                                        HtmlEncode="false" SortExpression="CreatedDate" />

                                                    <asp:BoundField DataField="Size" HeaderText="Size" DataFormatString="{0:F4}" ItemStyle-Width="6%"
                                                        HtmlEncode="false" SortExpression="Size" />

                                                    <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="17%"
                                                        ItemStyle-CssClass="table_tekst_edit actionMinWidth">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="LabelAction" runat="server" Text="Action" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <%--Edit--%>
                                                            <asp:LinkButton ID="lnkEdit" runat="server" Text=""
                                                                CssClass="ic-icon ic-edit editInboxfile" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                            <asp:HiddenField ID="hdnWorkAreaId" Value='<%# Eval("WorkAreaId").ToString() %>' runat="server" />
                                                            <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName").ToString() %>' />

                                                            <asp:LinkButton ID="lbkUpdate" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                                ToolTip="Update" CssClass="ic-icon ic-save saveInboxfile" Style="display: none" name="lbkUpdate"></asp:LinkButton>
                                                            <asp:LinkButton ID="lnkCancel" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                OnClientClick="cancleInboxFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancelInboxfile" Style="display: none" name="lnkCancel"></asp:LinkButton>
                                                            <%--Edit End--%>

                                                            <%--Delete--%>
                                                            <asp:Button ID="btnWorkAreaDelete" runat="server" CssClass="ic-icon ic-delete deleteInboxfile" ToolTip="Delete File/Folder" OnClientClick="return ShowDeleteError();" Style="border: none; cursor: pointer" />
                                                            <%--Delete End--%>
                                                            <asp:LinkButton ID="btnIndexing" runat="server" CssClass="ic-icon ic-index editdocs" CausesValidation="False" ToolTip="File Indexing"
                                                                CommandName=""></asp:LinkButton>
                                                            <asp:LinkButton ID="btnWorkAreaPreview" runat="server" CssClass="ic-icon ic-preview lnkbtnPreview" CausesValidation="False" ToolTip="Preview File"
                                                                CommandName=""></asp:LinkButton>
                                                            <asp:HyperLink ID="btnWorkAreaDownload" runat="server" CausesValidation="false" CssClass="ic-icon ic-download downloadFile lnkBtnDownload" Target="_blank" ToolTip="Download File" />
                                                            <asp:Button ID="btnWorkAreaShare" runat="server" CssClass="icon-btn ic-icon ic-share lnkBtnShare" Style="border: none; background-color: white; margin-left: 0px; float: none" ToolTip="Share File" OnClientClick="OnShare(this)" />
                                                            <asp:Button ID="btnWorkAreaAddToTab" runat="server" CssClass="icon-btn addtotab-workarea lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event)" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    No Records found
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                            <div class="table-toolbar">
                                                <cc1:WebPager ID="WebPager2" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" PageSize="10" CssClass="opt-select" PagerStyle="NumericPages" ControlToPaginate="grdWorkAreaFiles"></cc1:WebPager>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <asp:HiddenField ID="hdnJsTreeData" runat="server" EnableViewState="false" />
                                <asp:HiddenField ID="hdnSelectedNode" runat="server" EnableViewState="false" />
                                <asp:HiddenField ID="hdnIndexKey" runat="server" EnableViewState="false" />
                                <asp:HiddenField ID="hdnIndexCrieteria" runat="server" EnableViewState="false" />
                                </div>
                                <%--End WorkArea--%>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                    <%--Share Document--%>
                    <div id="dialog-share-book" title="Share this Inbox Document" style="display: none">
                        <p class="validateTips">All form fields are required.</p>
                        <br />
                        <%-- <br />--%>
                        <label for="email">From Email</label><br />
                        <input type="email" name="email" id="email" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
                        <%--<label for="myname" style="display:none;">My Name</label><br />
                    <input type="text" name="myname" id="myname" class="text ui-widget-content ui-corner-all" style="border: 1px solid black;display:none" /><br />--%>
                        <label for="emailto">To Email</label><br />
                        <input type="email" name="emailto" id="emailto" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
                        <label for="nameto">To Name</label><br />
                        <input type="text" name="nameto" id="nameto" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
                        <label for="title">Subject</label><br />
                        <input type="text" name="title" id="title" value="WorkArea Share" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" readonly="readonly" /><br />
                        <label for="content">Message</label><br />
                        <textarea name="content" id="content" rows="2" cols="25" class="text ui-widget-content ui-corner-all" style="border: 1px solid black"></textarea><br />
                        <label for="content">Link Expiration Time (In Hrs)</label><br />
                        <input type="text" name="linkexpiration" id="expirationTime" class="text ui-widget-content ui-corner-all" style="border: 1px solid black; width: 80%;" />
                        <input type="button" id="updateExpiration" onclick="UpdateExpirationTime()" class="btn-small green" style="float: right" value="UPDATE" /><br />
                        <span id="errorMsg"></span>
                    </div>
                    <div id="dialog-message-all" title="" style="display: none;">
                        <p id="dialog-message-content">
                            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span>
                            <span id="spanText"></span>
                        </p>
                    </div>
                    <%--End Share Document--%>

                    <asp:Panel class="popup-mainbox" ID="panelWorkAreaDelete" Style="display: none"
                        runat="server">
                        <div>
                            <div class="popup-head" id="Div2">
                                Delete File
                            </div>
                            <div class="popup-scroller">
                                <p style="margin: 20px">
                                    Caution! Are you sure you want to delete this file?
                                </p>
                                <p style="margin: 20px">
                                    Confirm Password:
                                                    <asp:TextBox ID="TextBox2" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                                </p>
                            </div>
                            <div class="popup-btn">
                                <input id="ButtonDeleleOkayWorkArea" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />
                                <input id="ButtonDeleteCancelWorkArea" type="button" value="No" class="btn green" />
                            </div>
                        </div>
                    </asp:Panel>

                    <div class="popup-mainbox preview-popup" id="PanelIndex" style="display: none; max-width: 500px; height: 30%; top: auto">
                        <div>
                            <div class="popup-head" id="Div4">
                                WorkArea Indexing
                <span id="closeIndex" class="ic-icon ic-close" style="float: right; cursor: pointer"></span>
                            </div>
                            <div class="popup-scroller-index" style="margin-top: 10px;">
                                <div class="popup-row">
                                    <input type="hidden" id="hndDividerIdIndex" />
                                    <div class="popup-left">

                                        <table width="100%" id="tblIndex">
                                            <tbody>
                                                <tr id="indexRow0" class="indexRow">
                                                    <td>
                                                        <input type="hidden" value="0" class="hdnIndexId"></td>
                                                    <td>
                                                        <input type="text" id="txtKey" width="90%" maxlength="20" placeholder="Key" style="height: 25px; margin-left: 10px;" /></td>
                                                    <td>
                                                        <input type="text" id="txtValue" width="90%" maxlength="20" placeholder="Value" style="height: 25px;" /></td>
                                                    <td>
                                                        <button id="btnAddIndexPair" type="button" width="90%" onclick="return AddIndexTextBoxes(this)">
                                                            <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/add-icon.png" title="Add" style="margin-bottom: 0px;" /></button></td>
                                                    <td>
                                                        <button id="btnRemoveIndexPair" type="button" width="90%" onclick="return DisplayConfirmBox(this)">
                                                            <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" style="margin-bottom: 0px;" />
                                                        </button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>


                                    </div>
                                </div>
                            </div>
                            <div class="popup-btn">
                                <input id="btnIndexSave" type="button" value="Save" class="btn green" style="margin-right: 5px;" />
                                <input id="btnIndexCancel" type="button" value="Cancel" class="btn green" />
                            </div>

                        </div>
                    </div>

                    <div class="popup-mainbox" id="DivDeleteConfirmIndex" style="display: none; position: fixed; z-index: 999999; margin-left: 20%; margin-top: 5%;">
                        <div>
                            <div class="popup-head" id="Div5">
                                Delete Index
                            </div>
                            <div class="popup-scroller">
                                <p>
                                    Caution! Are you sure you want to delete this index?
                                </p>
                            </div>
                            <div class="popup-btn">
                                <input id="btnDeleteIndexOkay" onclick="return RemoveIndexTextBoxes()" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />
                                <input id="btnDeleteIndexCancel" type="button" value="No" class="btn green" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a class='iframe' href="javascript:;" style="display: none;">popup</a>
</asp:Content>


