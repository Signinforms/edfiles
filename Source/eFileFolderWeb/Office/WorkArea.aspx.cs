﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using Shinetech.DAL;
using Shinetech.Utility;
using Shinetech.Framework.Controls;
using AjaxControlToolkit;
using Shinetech.Engines;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web.Script.Services;
using System.Drawing;
using net.openstack.Core.Domain;
using net.openstack.Providers.Rackspace;
//using DnnSun.WebControls.Toolkit;

using Shinetech.Engines.Adapters;
using iTextSharp.text.pdf;
using System.Web.Hosting;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using System.Text.RegularExpressions;

public partial class Office_WorkArea : System.Web.UI.Page
{
    public string FolderId;
    public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    public string virtualDir = ConfigurationManager.AppSettings["VirtualDir"];
    public string newFileName = string.Empty;
    public string SortType = "DESC";
    public static string splitWorkAreaPath = System.Configuration.ConfigurationManager.AppSettings["WorkAreaThumbNailPath"];
    public string fullFilePath = string.Empty;
    public static string ThumbnailPath = System.Configuration.ConfigurationManager.AppSettings["ThumbNailPath"];
    public static CloudIdentity user = new CloudIdentity
    {
        Username = ConfigurationManager.AppSettings["RackSpaceUserName"],
        APIKey = ConfigurationManager.AppSettings["RackSpaceAPIKey"]
    };
    public static CloudFilesProvider cloudfilesProvider = new CloudFilesProvider(user);
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
    private NLogLogger _logger = new NLogLogger();
    //public DataTable dspendingdata;

    protected void Page_Load(object sender, EventArgs e)
    {
        var argument = Request.Form["__EVENTARGUMENT"];
        if (!Page.IsPostBack)
        {
            GetData();
            BindGrid();
        }
        else
        {
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
            {
                BindGrid();
            }
        }
        argument = "";
        if (Sessions.HasViewPrivilegeOnly)
        {
            //btnWorkAreaCreate.Visible = false;
            AjaxFileUpload1.Visible = false;
            btnSpltBtn.Visible = false;
        }
    }

    public string DocumentID
    {
        get
        {
            return (string)Session["DocumentID"];
        }
        set
        {
            Session["DocumentID"] = value;

        }
    }

    public string DocumentName
    {
        get
        {
            return (string)Session["DocumentName"];
        }
        set
        {
            Session["DocumentName"] = value;

        }
    }

    public string EffID_FFB
    {
        get
        {
            return (string)this.Session["EffID_FFB"];
        }
        set
        {
            this.Session["EffID_FFB"] = value;
        }
    }

    public string PathUrl
    {
        get { return splitWorkAreaPath; }
    }

    public string splitPdfFolderName
    {
        get
        {
            return string.Format("~/{0}", splitWorkAreaPath);
        }

    }
    public Dictionary<string, string> UploadErrorFileName
    {
        get
        {
            return (Session["UploadErrorFileName"] != null) ? (Dictionary<string, string>)Session["UploadErrorFileName"] : new Dictionary<string, string>();
        }
        set
        {
            Session["UploadErrorFileName"] = value;
        }
    }

    #region WorkArea
    [WebMethod]
    public static string GetJsonDataForTree()
    {
        TreeView treeView = new TreeView();
        Office_WorkArea WA = new Office_WorkArea();
        return treeView.GetJsonDataForTree(Sessions.SwitchedRackspaceId);
    }

    [WebMethod]
    public static Array GetPreviewURL(int documentID, int folderID, string shareID, int workAreaId)
    {
        Office_WorkArea workarea = new Office_WorkArea();
        shareID = shareID.Replace("%22", "\"").Replace("%27", "'");
        shareID = shareID.Replace("WorkArea", "workArea");
        shareID = shareID.Substring(0, shareID.LastIndexOf('/')) + shareID.Substring(shareID.LastIndexOf('/')).ToLower();
        return workarea.GetURL(folderID, shareID, Sessions.SwitchedRackspaceId, documentID, workAreaId);
    }

    public Array GetURL(int folderID, string shareID, string sessionID, int documentID, int workAreaId)
    {
        string type = Path.GetExtension(shareID).Split('.')[1];
        string filePath = string.Empty, fileName = string.Empty, physicalPath = string.Empty;
        long fileSize = 0;
        //int workAreaId = 0;
        RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
        physicalPath = rackSpaceFileUpload.GetPath(shareID, Enum_Tatva.Folders.WorkArea, sessionID, ref fileSize);
        if (type == StreamType.JPG.ToString().ToLower())
            type = "1";
        else if (type == StreamType.PDF.ToString().ToLower())
            type = "2";

        string[] outPut = new string[3];
        outPut[0] = physicalPath;
        outPut[1] = type;
        outPut[2] = workAreaId.ToString();
        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Preview.GetHashCode(), 0, 0, 0, workAreaId);
        return outPut;
    }

    [WebMethod]
    public static string DeleteObject(string objectPath, string password, string selectedPath)
    {
        try
        {
            Office_WorkArea workArea = new Office_WorkArea();
            string errorMsg = string.Empty;
            objectPath = objectPath.Replace("%22", "\"").Replace("%27", "'");
            objectPath = objectPath.Replace("WorkArea", "workArea");
            if (workArea.CheckPassword(password))
            {
                RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                List<string> objectToDelete = rackSpaceFileUpload.DeleteObject(ref errorMsg, objectPath);
                if (objectToDelete.Count > 0)
                {
                    foreach (string path in objectToDelete)
                    {
                        string oldFileName = string.Empty;
                        oldFileName = path.Substring(path.LastIndexOf('/') + 1);
                        DataTable dtFiles = General_Class.GetWorkAreaDocumentForCRUD(Sessions.SwitchedRackspaceId, oldFileName, path);
                        dtFiles.AsEnumerable().ToList().ForEach(d =>
                        {
                            var WorkareaId = d.Field<int>("WorkAreaId");
                            General_Class.UpdateWorkArea(oldFileName, WorkareaId, 0, Enum_Tatva.IsDelete.Yes.GetHashCode(), 0, 0, path);
                            General_Class.DeleteWorkAreaIndexValues(WorkareaId, Sessions.SwitchedRackspaceId);
                        });
                    }
                }
                else
                {
                    return "{\"ErrorMessage\":" + "\"" + errorMsg + "\"}";
                }
            }
            else
            {
                errorMsg = "The password you entered does not match our records, please try again or contact your administrator.";
                return "{\"ErrorMessage\":" + "\"" + errorMsg + "\"}";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
        Office_WorkArea WA = new Office_WorkArea();
        WA.DataSource = General_Class.GetWorkAreaDocuments(Sessions.SwitchedRackspaceId);
        return string.Empty;
    }

    public bool CheckPassword(string password)
    {
        string strPassword = PasswordGenerator.GetMD5(password);
        if (UserManagement.GetPassword(User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
        {
            return false;
        }
        return true;
    }

    [WebMethod]
    public static string RenameObject(string oldObjectPath, string newObjectPath, string selectedPath, string workAreaId, string searchKey)
    {
        try
        {
            EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            string errorMsg = string.Empty;
            newObjectPath = Common_Tatva.SubStringFilename(newObjectPath);
            oldObjectPath = oldObjectPath.Replace("%22", "\"").Replace("%27", "'");
            oldObjectPath = oldObjectPath.Replace("WorkArea", "workArea");
            string oldFileName = string.Empty;
            oldFileName = oldObjectPath.Substring(oldObjectPath.LastIndexOf('/') + 1);
            if (oldFileName == newObjectPath)
                return string.Empty;
            string newPath = rackSpaceFileUpload.RenameObject(ref errorMsg, oldObjectPath, newObjectPath, Enum_Tatva.Folders.WorkArea);
            if (string.IsNullOrEmpty(errorMsg))
            {
                string newFileName = newPath.Substring(newPath.LastIndexOf('/') + 1);
                General_Class.UpdateWorkArea(newFileName, Convert.ToInt32(workAreaId), 0, Enum_Tatva.IsDelete.No.GetHashCode(), 1, 0, newPath);
                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Rename.GetHashCode(), 0, 0, 0, Convert.ToInt32(workAreaId));
            }
            else
            {
                return "{\"ErrorMessage\":" + "\"" + errorMsg + "\"}";
            }
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
        Office_WorkArea WA = new Office_WorkArea();
        WA.DataSource = General_Class.GetWorkAreaDocuments(Sessions.SwitchedRackspaceId, searchKey);
        return string.Empty;
    }

    [WebMethod]
    public static string CreateFolder(string path, string folderName, string treeId)
    {
        try
        {
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            string errorMsg = string.Empty;
            path = path.Replace("%22", "\"").Replace("%27", "'");
            path = path.Replace("WorkArea", "workArea");
            if (folderName.ToLower().Equals("workarea"))
                return "{\"ErrorMessage\":" + "\"" + "This folder name is not allowed!" + "\"}";
            rackSpaceFileUpload.CreateFolder(ref errorMsg, path, folderName);
            if (!string.IsNullOrEmpty(errorMsg))
                return "{\"ErrorMessage\":" + "\"" + errorMsg + "\"}";
            int created = General_Class.CreateFolderForWorkArea(folderName, path, Sessions.SwitchedRackspaceId, 1, 0, !string.IsNullOrEmpty(treeId) ? Convert.ToInt32(treeId) : 0);
            if (created == 0)
                return "{\"ErrorMessage\":" + "\"Failed to create folder at this time. Please try again later !!!\"}";
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(folderName, path, string.Empty, ex);
        }
        TreeView treeView = new TreeView();
        return treeView.GetJsonDataForTree(Sessions.SwitchedRackspaceId);
    }

    [WebMethod]
    public static void SaveSelectedPath(string selectedPath, string selectedTreeId)
    {
        selectedPath = selectedPath.Replace("%22", "\"").Replace("%27", "'");
        Sessions.SelectedPath = selectedPath;
        Sessions.SelectedTreeId = Convert.ToInt32(string.IsNullOrEmpty(selectedTreeId) ? "0" : selectedTreeId);
    }

    #endregion

    #region FileUpload
    protected void AjaxFileUpload1_OnUploadComplete(object sender, AjaxFileUploadEventArgs file)
    {
        string guid = Guid.NewGuid().ToString();
        string uid = Sessions.SwitchedRackspaceId;
        string workAreaID = string.Empty;
        string fileName = string.Empty;
        try
        {
            string path = Server.MapPath("~/" + Common_Tatva.eFileFlow);

            Common_Tatva.WriteLogs(guid + " Step 1 path:" + path);
            string fullPath = Path.Combine(path, uid);
            string selectedPath = Sessions.SelectedPath;
            Common_Tatva.WriteLogs(guid + " Step 2 path: " + path + " Selected Path: " + selectedPath);
            if (AjaxFileUpload1.IsInFileUploadPostBack)
            {
                RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                if (!string.IsNullOrEmpty(selectedPath) && selectedPath.IndexOf('.') < 0)
                {
                    string newPath = string.Empty;
                    if (selectedPath.Length > Enum_Tatva.GetEnumDescription(Enum_Tatva.Folders.WorkArea).Length + 1)
                    {
                        newPath = selectedPath.Substring(Enum_Tatva.GetEnumDescription(Enum_Tatva.Folders.WorkArea).Length + 1);
                        fileName = newPath + "/" + file.FileName;
                    }
                    else
                        fileName = file.FileName;
                }
                else
                {
                    fileName = file.FileName;
                }
                Common_Tatva.WriteLogs(guid + " Step 3 path: " + path + " Selected Path: " + selectedPath + " New Filename: " + fileName);
                string errorMsg = string.Empty;
                string newFileNamePath = rackSpaceFileUpload.UploadObject(ref errorMsg, fileName, Enum_Tatva.Folders.WorkArea, file.GetStreamContents(), selectedPath.Substring(selectedPath.IndexOf('/') + 1));
                Common_Tatva.WriteLogs(guid + " Step 4 path: " + path + " Selected Path: " + selectedPath + " New Filename: " + fileName + " New Path: " + newFileNamePath);
                if (string.IsNullOrEmpty(errorMsg))
                {
                    decimal size = Math.Round((decimal)(file.FileSize / 1024) / 1024, 2);
                    workAreaID = General_Class.DocumentsInsertForWorkArea(newFileNamePath.Substring(newFileNamePath.LastIndexOf('/') + 1), Sessions.SwitchedRackspaceId, Membership.GetUser().ProviderUserKey.ToString(), size, Enum_Tatva.IsDelete.No.GetHashCode(), newFileNamePath, Enum_Tatva.WorkAreaType.Upload.GetHashCode(), Sessions.SelectedTreeId);
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Upload.GetHashCode(), 0, 0, 0, Convert.ToInt32(workAreaID));
                    Common_Tatva.WriteLogs(guid + " Step 5 path: " + path + " Selected Path: " + selectedPath + " New Filename: " + fileName + "New Path: " + newFileNamePath + " WorkareaId : " + workAreaID);
                    Common_Tatva.WriteErrorLog("Updating database for WorkArea by Email for OfficeId == " + uid, workAreaID, fileName, new Exception());
                }
                else
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Failed to upload file. Please try again after sometime." + "\");", true);
            }
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog("Exception: Updating database for WorkArea by Email for OfficeId == " + uid, workAreaID, fileName, ex);
        }
        file.DeleteTemporaryData();
    }

    protected void AjaxFileUpload1_UploadCompleteAll(object sender, AjaxFileUploadCompleteAllEventArgs e)
    {
        var startedAt = (DateTime)Session["uploadTime"];
        var now = DateTime.Now;
        e.ServerArguments = new JavaScriptSerializer()
            .Serialize(new
            {
                duration = (now - startedAt).Seconds,
                time = DateTime.Now.ToShortTimeString()
            });
        txtSearch.Text = "";
        this.DataSource = General_Class.GetWorkAreaDocuments(Sessions.SwitchedRackspaceId, string.Empty);
    }

    protected void AjaxFileUpload1_UploadStart(object sender, AjaxFileUploadStartEventArgs e)
    {
        var now = DateTime.Now;
        e.ServerArguments = now.ToShortTimeString();
        Session["uploadTime"] = now;
    }

    #endregion

    protected void btnWorkAreaAddToTab_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
        hdnJsTreeData.Value = "";
    }

    protected void btnSelectAddToTab_Click(object sender, EventArgs e)
    {
        EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
        string selectedFilePath = hdnSharePath.Value;
        selectedFilePath = selectedFilePath.Replace("%22", "\"").Replace("%27", "'");
        selectedFilePath = selectedFilePath.Replace("WorkArea", "workArea");
        try
        {
            int folderId = Convert.ToInt32(hdnSelectedFolderID.Value);
            int dividerId = Convert.ToInt32(hdnSelectedDividerID.Value);

            var file = selectedFilePath.Substring(selectedFilePath.LastIndexOf('/') + 1);
            selectedFilePath = selectedFilePath.Replace(file, file.ToLower());
            string errorMsg = string.Empty;
            int WorkareaId = Convert.ToInt32(hdnShareWorkAreaId.Value);

            string dir = Server.MapPath("~/" + Common_Tatva.RackSpaceWorkareaDownload + Path.DirectorySeparatorChar + Sessions.SwitchedRackspaceId);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            var filePath = dir + "\\" + rackSpaceFileUpload.GetObject(ref errorMsg, selectedFilePath);
            if (string.IsNullOrEmpty(errorMsg))
            {
                string encodeName = HttpUtility.UrlPathEncode(file).Replace("%", "$");
                encodeName = encodeName.Replace(encodeName, encodeName.Replace("$20", " "));
                encodeName = Common_Tatva.SubStringFilename(encodeName);
                encodeName = encodeName.Replace(encodeName, encodeName.Replace(" ", "$20"));
                string vpath = Server.MapPath(string.Format("~/{0}", CurrentVolume));
                string pathName = CurrentVolume + Path.DirectorySeparatorChar + folderId + Path.DirectorySeparatorChar + dividerId + Path.DirectorySeparatorChar + encodeName;

                int documentOrder = FlashViewer.GetDocumentOrder(Convert.ToInt32(dividerId));
                string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, folderId, Path.DirectorySeparatorChar, dividerId);
                if (!Directory.Exists(dstpath))
                {
                    Directory.CreateDirectory(dstpath);
                }

                dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);
                if (File.Exists(dstpath))
                {
                    dstpath = Common_Tatva.GetRenameFileName(dstpath);
                }
                File.Copy(filePath, dstpath, true);

                string newFilename = Path.GetFileNameWithoutExtension(dstpath);
                string displayName = HttpUtility.UrlDecode(newFilename.Replace('$', '%').ToString());

                string documentIDOrder = Common_Tatva.InsertDocument(dividerId, pathName, documentOrder, displayName, Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()));
                if (documentIDOrder == "0")
                {
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Failed to move file. There is a problem with the file format." + "\");", true);
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, 0, WorkareaId);
                }
                else
                {
                    General_Class.AuditLogByDocId(Convert.ToInt32(documentIDOrder.Split('#')[0]), Sessions.SwitchedRackspaceId, Enum_Tatva.Action.Create.GetHashCode());
                    General_Class.UpdateWorkArea(file, WorkareaId, Convert.ToInt32(documentIDOrder.Split('#')[0]), Enum_Tatva.IsDelete.No.GetHashCode(), 0, 1, selectedFilePath);
                    File.Delete(filePath);
                    rackSpaceFileUpload.DeleteObject(ref errorMsg, selectedFilePath);
                    string folderName = hdnSelectedFolderText.Value;
                    string tabName = hdnSelectedTabText.Value;
                    folderName = folderName.Insert(folderName.IndexOf(')') + 1, "/");
                    folderName = folderName.Remove(folderName.IndexOf('('), folderName.IndexOf(')') - folderName.IndexOf('(') + 1);
                    folderName = folderName.Replace("\n", string.Empty);
                    folderName = folderName.Replace("\r", string.Empty);
                    folderName = folderName.Replace("/", string.Empty);
                    folderName = folderName.Replace(" ", string.Empty);
                    tabName = tabName.Replace("\n", string.Empty);
                    tabName = tabName.Replace("\r", string.Empty);
                    tabName = tabName.Replace("/", string.Empty);
                    tabName = tabName.Replace(" ", string.Empty);

                    string message = "File(s) have been moved to folder: " + folderName + "& tab: " + tabName;
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + message + "\");", true);
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode(), 0, 0, 0, WorkareaId);
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(folderId), Convert.ToInt32(dividerId), Convert.ToInt32(documentIDOrder.Split('#')[0]), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.WorkAreaMoveUpload.GetHashCode(), 0, 0);
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Failed to get object from cloud." + "\");", true);
                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, 0, WorkareaId);
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
            hdnJsTreeData.Value = "";
            this.DataSource = General_Class.GetWorkAreaDocuments(Sessions.SwitchedRackspaceId, string.Empty);
            BindGrid();
        }
    }

    public string DividerID
    {
        get
        {
            return (string)this.Session["DividerID"];
        }
        set
        {
            Session["DividerID"] = value;
        }
    }
    public DataTable DataSource
    {
        get
        {
            return this.Session["Files_Workarea"] as DataTable;
        }
        set
        {
            this.Session["Files_Workarea"] = value;
        }
    }

    private string WA_Sort_Direction
    {
        get
        {
            return (string)this.Session["WA_Sort_Direction"];
        }
        set
        {
            this.Session["WA_Sort_Direction"] = value;
        }
    }

    private void BindGrid()
    {
        grdWorkAreaFiles.DataSource = this.DataSource.AsDataView();
        grdWorkAreaFiles.DataBind();
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
        WebPager2.DataSource = this.DataSource;
        WebPager2.DataBind();
        UpdatePanel1.Update();
    }

    private void GetData()
    {
        try
        {
            if (!string.IsNullOrEmpty(Sessions.SwitchedRackspaceId))
            {
                this.DataSource = General_Class.GetWorkAreaDocuments(Sessions.SwitchedRackspaceId);
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    public DataTable WorkArea_DataSource
    {
        get
        {
            return this.Session["WorkArea_DataSource"] as DataTable;
        }
        set
        {
            this.Session["WorkArea_DataSource"] = value;
        }
    }

    protected void grdRequestedFiles_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
            string inboxId = drv.Row[0].ToString();
            string eFileFlowShareId = drv.Row[11].ToString();
            string fileName = string.Empty;
            string ext = Path.GetExtension(fileName);
            string type = drv.Row[10].ToString();
            int folderID = 0;
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            string path = string.Empty;

            if (type == "Inbox")
            {
                folderID = (int)Enum_Tatva.Folders.inbox;
                fileName = drv.Row[1].ToString();
                path = rackSpaceFileUpload.GeneratePath(fileName.ToLower(), Enum_Tatva.Folders.inbox);
            }
            else
            {
                folderID = (int)Enum_Tatva.Folders.EFileShare;
                fileName = drv.Row[1].ToString();
                path = rackSpaceFileUpload.GeneratePath(fileName.ToLower(), Enum_Tatva.Folders.EFileShare, eFileFlowShareId);
            }

            if (!fileName.ToLower().Contains(".pdf"))
            {
                LinkButton lnkbtnPreview = e.Row.FindControl("lnkbtnPreview") as LinkButton;
                if (lnkbtnPreview != null)
                {
                    lnkbtnPreview.Enabled = false;
                    lnkbtnPreview.OnClientClick = null;
                    lnkbtnPreview.Attributes.Add("style", "opacity:0.5");
                }
            }
            else
            {
                LinkButton lnkbtnPreview = (e.Row.FindControl("lnkbtnPreview") as LinkButton);
                if (lnkbtnPreview != null)
                {
                    //lnkbtnPreview.Attributes.Add("onclick", "ShowPreviewForRequestedFiles('" + (string.IsNullOrEmpty(inboxId) ? "0" : inboxId) + "','" + eFileFlowShareId + "','" + folderID + "');");
                }
            }

            path = path.Replace(@"\", "/");
            TextBox txtFileName = (e.Row.FindControl("txtFileName") as TextBox);

            LinkButton lbkUpdate = (e.Row.FindControl("lbkUpdate") as LinkButton);
            if (lbkUpdate != null)
            {
                lbkUpdate.Attributes.Add("onclick", "OnSaveRenameForRequest('" + txtFileName.ClientID + "','" + path + "','" + inboxId + "','" + eFileFlowShareId + "','" + ext + "','" + type + "');");
            }

            HyperLink lnkBtnDownload = (e.Row.FindControl("lnkBtnDownload") as HyperLink);
            if (lnkBtnDownload != null)
            {
                if (folderID == 3)
                {
                    path = rackSpaceFileUpload.GeneratePath(fileName.ToLower(), Enum_Tatva.Folders.inbox);
                    lnkBtnDownload.Attributes.Add("href", "PdfViewer.aspx?data=" + QueryString.QueryStringEncode("ID=" + path + "&folderID=" + folderID + "&sessionID=" + Sessions.SwitchedRackspaceId));
                }
                else
                {
                    path = rackSpaceFileUpload.GeneratePath(fileName.ToLower(), Enum_Tatva.Folders.EFileShare, eFileFlowShareId);
                    lnkBtnDownload.Attributes.Add("href", "PdfViewer.aspx?data=" + QueryString.QueryStringEncode("ID=" + path + "&folderID=" + folderID + "&sessionID=" + Sessions.SwitchedRackspaceId));
                }

            }
        }

    }
    protected void grdWorkAreaFiles_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (Sessions.HasViewPrivilegeOnly)
            {
                HyperLink libDownload = e.Row.FindControl("btnWorkAreaDownload") as HyperLink;
                libDownload.Visible = false;
                Button libDel = e.Row.FindControl("btnWorkAreaDelete") as Button;
                libDel.Visible = false;
                LinkButton libEdit = e.Row.FindControl("lnkEdit") as LinkButton;
                libEdit.Visible = false;
                LinkButton libIndex = e.Row.FindControl("btnIndexing") as LinkButton;
                libIndex.Visible = false;
                Button libShare = e.Row.FindControl("btnWorkAreaShare") as Button;
                libShare.Visible = false;
                Button libAddToTab = e.Row.FindControl("btnWorkAreaAddToTab") as Button;
                libAddToTab.Visible = false;
            }
            System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
            string path = drv["PathName"].ToString();
            string fileName = drv["FileName"].ToString();
            path = path.Replace(@"\", "/");

            int workAreaId = 0;

            HiddenField hdnWorkAreaId = e.Row.FindControl("hdnWorkAreaId") as HiddenField;
            if (hdnWorkAreaId != null)
                workAreaId = Convert.ToInt32(hdnWorkAreaId.Value);

            TextBox txtDocumentName = (e.Row.FindControl("txtFileName") as TextBox);
            Label lblFileName = (e.Row.FindControl("lblfileName") as Label);
            if (lblFileName != null)
            {
                lblFileName.Attributes.Add("onclick", "ShowPreviewForWorkArea('" + path + "','" + fileName + "', '" + workAreaId + "');");
            }
            LinkButton lnkBtnUpdate = (e.Row.FindControl("lbkUpdate") as LinkButton);
            if (lnkBtnUpdate != null)
            {
                lnkBtnUpdate.Attributes.Add("onclick", "OnSaveRename('" + txtDocumentName.ClientID + "','" + path + "','" + fileName + "','" + workAreaId + "');");
            }
            LinkButton btnIndexing = (e.Row.FindControl("btnIndexing") as LinkButton);
            if (btnIndexing != null)
            {
                btnIndexing.Attributes.Add("onclick", "OpenIndexPopup(this);");
            }

            LinkButton lnkBtnPreview = (e.Row.FindControl("btnWorkAreaPreview") as LinkButton);
            if (lnkBtnPreview != null)
            {
                lnkBtnPreview.Attributes.Add("onclick", "ShowPreviewForWorkArea('" + path + "','" + fileName + "', '" + workAreaId + "');");
            }
            HyperLink lnkBtnDownload = (e.Row.FindControl("btnWorkAreaDownload") as HyperLink);
            if (lnkBtnDownload != null)
            {
                lnkBtnDownload.Attributes.Add("onclick", "DownloadWADoc('" + lnkBtnDownload.ClientID + "');");
            }

        }
    }

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager2.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
        WebPager2.DataSource = this.DataSource;
        WebPager2.DataBind();
    }
    protected void grdWorkAreaFiles_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    private string GetSortDirection(string column)
    {

        // Retrieve the last column that was sorted.
        string sortExpression = ViewState["SortExpression"] as string;

        if (sortExpression != null)
        {
            // Check if the same column is being sorted.
            // Otherwise, the default value can be returned.
            if (sortExpression == column)
            {
                string lastDirection = ViewState["SortDirection"] as string;
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    SortType = "DESC";
                }
                else if ((lastDirection != null) && (lastDirection == "DESC"))
                {
                    SortType = "ASC";
                }
            }
        }

        // Save new values in ViewState.
        ViewState["SortDirection"] = SortType;
        ViewState["SortExpression"] = column;

        return SortType;
    }

    #region Index

    public string GetFileName(string fileName)
    {
        string encodeName = HttpUtility.UrlPathEncode(fileName);
        return encodeName.Replace("%", "$");
    }

    [WebMethod]
    public static bool SaveWorkAreaIndexValues(int workAreaId, string indexData, string deleteIndexData)
    {
        try
        {
            EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
            IndexKeyValue[] indexKeyValue = JsonConvert.DeserializeObject<List<IndexKeyValue>>(indexData).ToArray();
            //workareaPath = workareaPath.Replace("WorkArea", "workArea");
            DataTable dtIndex = new DataTable();
            dtIndex.Columns.Add("IndexId", typeof(int));
            dtIndex.Columns.Add("WorkAreaId", typeof(int));
            dtIndex.Columns.Add("IndexKey", typeof(string));
            dtIndex.Columns.Add("IndexValue", typeof(string));
            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.DocIndex.GetHashCode(), 0, 0, 0, workAreaId);
            foreach (IndexKeyValue item in indexKeyValue)
            {
                dtIndex.Rows.Add(new object[] { item.IndexId, workAreaId, item.IndexKey, item.IndexValue });
            }
            General_Class.InsertWorkAreaIndexValues(dtIndex, deleteIndexData, workAreaId, Sessions.SwitchedRackspaceId);
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    [WebMethod]
    public static string GetWorkAreaIndexValues(string workAreaId)
    {
        try
        {
            Dictionary<string, IndexKeyValue> dicIndexValues = new Dictionary<string, IndexKeyValue>();
            DataTable dtIndexValues = General_Class.GetWorkAreaIndexValues(Convert.ToInt32(workAreaId), Sessions.SwitchedRackspaceId);
            if (dtIndexValues != null && dtIndexValues.Rows.Count > 0)
            {
                for (int j = 0; j < dtIndexValues.Rows.Count; j++)
                {
                    IndexKeyValue indexKeyValue = new IndexKeyValue();
                    indexKeyValue.IndexId = Int32.Parse(dtIndexValues.Rows[j]["IndexId"].ToString());
                    indexKeyValue.IndexValue = Convert.ToString(dtIndexValues.Rows[j]["IndexValue"]);
                    indexKeyValue.IndexKey = Convert.ToString(dtIndexValues.Rows[j]["IndexKey"]);
                    dicIndexValues.Add(Convert.ToString(dtIndexValues.Rows[j]["IndexId"]), indexKeyValue);
                }
            }
            return JsonConvert.SerializeObject(dicIndexValues, Newtonsoft.Json.Formatting.Indented);
        }
        catch (Exception ex)
        {
            return "";
        }
    }
    #endregion

    [WebMethod]
    public static string GetDocsForSelectedFolder(string path)
    {
        Office_WorkArea WA = new Office_WorkArea();
        try
        {
            //string uid = Sessions.SwitchedRackspaceId;
            //TreeView treeView = new TreeView();
            if (!string.IsNullOrEmpty(Sessions.SwitchedRackspaceId))
            {
                //List<RackSpaceObject> objList = treeView.GetWorkareList(Sessions.SwitchedRackspaceId, path);
                WA.DataSource = General_Class.GetWorkAreaDocuments(Sessions.SwitchedRackspaceId);
                DataView dvSource = WA.DataSource.AsDataView();
                dvSource.Sort = WA.WA_Sort_Direction;
                WA.DataSource = dvSource.ToTable();
            }
            return string.Empty;
        }
        catch (Exception ex)
        {
            return ex.Message;
        }
    }

    #region SplitWorkArea

    protected void btnSaveSplitPdf_Click(object sender, EventArgs e)
    {
        try
        {
            string pageNumbers = hdnPageNumbers.Value;
            if (!string.IsNullOrEmpty(pageNumbers))
            {
                bool isSuccess = ExtractPages(pageNumbers, hdnFileName.Value, txtNewPdfName.Text.Trim(), hdnFileId.Value, hdnSelectedFolderText.Value);
                if (isSuccess)
                {
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Split.GetHashCode(), 0, 0, 0, Convert.ToInt32(hdnFileId.Value));
                    string message = "The selected pages have been added to tab.  Would you like to split additional pages?  If so, please select OK, otherewise, select Cancel";
                    ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "alert", "<script language=javascript>ShowConfirm('" + message + "');</script>", false);
                }
                else
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Split.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, 0, Convert.ToInt32(hdnFileId.Value));
            }

        }
        catch (Exception ex)
        { }
        finally
        {
            //Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
    }

    [WebMethod]
    public static string SplitPdf(string FileName, Int32 Id, string PageNumber, string NewFileName)
    {
        return PageNumber;
    }

    public byte[] ProcessFile(AjaxFileUpload AjaxFileUpload1, AjaxFileUploadEventArgs file, int dividerIdForFile, string path)
    {

        try
        {
            string fileName = ProcessAndSaveFile(file.FileName, path);
            if (!string.IsNullOrEmpty(fileName))
            {
                byte[] s = file.GetContents();
                AjaxFileUpload1.SaveAs(fileName);
                string fullFilePath = fileName;
                return s;
            }
            else
                return null;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(Convert.ToString(this.EffID_FFB), dividerIdForFile.ToString(), newFileName, ex);
            return null;
        }
    }

    private string ProcessAndSaveFile(string fName, string path)
    {
        try
        {
            //fName = fName.Replace("#", "");
            //fName = fName.Replace("&", "");
            //string encodeName = HttpUtility.UrlPathEncode(fName);
            //encodeName = encodeName.Replace("%", "$");
            fName = fName.Replace("#", "");
            fName = fName.Replace("&", "");
            fName = Common_Tatva.SubStringFilename(fName);
            string encodeName = HttpUtility.UrlPathEncode(fName);
            encodeName = encodeName.Replace("%", "$");

            string fileName = String.Concat(
                    path,
                    Path.DirectorySeparatorChar,
                    encodeName
                );
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }


            fileName = String.Concat(path, Path.DirectorySeparatorChar, Common_Tatva.ChangeExtension(fileName));

            fileName = Common_Tatva.GetRenameFileName(fileName);

            newFileName = Path.GetFileName(fileName);
            newFileName = HttpUtility.UrlDecode(newFileName.Replace('$', '%').ToString());
            return fileName;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
    }

    private void ProcessFileAndSaveInDB(float size, string path, int dividerIdForFile = 0, int folderID = 0, bool isMovedToFolder = false)
    {
        try
        {
            WorkItem item = new WorkItem();
            item.FolderID = folderID;
            item.DividerID = dividerIdForFile;
            string basePath = this.MapPath(this.splitPdfFolderName);

            item.Path = path;

            item.FileName = Path.GetFileNameWithoutExtension(newFileName);

            item.FullFileName = Path.GetFileName(newFileName);

            item.UID = Sessions.SwitchedSessionId;
            item.contentType = Common_Tatva.getFileExtention(Path.GetExtension(newFileName));

            int order = FlashViewer.GetDocumentOrder(Convert.ToInt32(dividerIdForFile));


            item.MachinePath = path;

            if (!Directory.Exists(item.MachinePath))
            {
                Directory.CreateDirectory(item.MachinePath);
            }

            string encodeName = HttpUtility.UrlPathEncode(item.FullFileName);
            encodeName = encodeName.Replace("%", "$");
            string source = string.Concat(path, Path.DirectorySeparatorChar, encodeName);
            var pageCount = 1;
            if (item.contentType == StreamType.JPG)
            {
                Jpeg2Engine jpeg2Engine = new Jpeg2Engine(item);
                jpeg2Engine.generatePDFfromImage(source, source.Split('.')[0] + ".pdf");
            }
            else if (item.contentType == StreamType.PDF)
            {
                PdfReader pdfReader = new PdfReader(source);
                pageCount = pdfReader.NumberOfPages;
            }
            if (isMovedToFolder)
            {
                string relativePath = CurrentVolume + Path.DirectorySeparatorChar + folderID + Path.DirectorySeparatorChar + dividerIdForFile;
                this.DocumentID = General_Class.DocumentsInsert(item.FileName, item.FolderID, item.UID, relativePath, item.DividerID, pageCount, (float)size, item.FileName,
                    (int)item.contentType, order + 1, Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()));
                //Need to ask
                General_Class.AuditLogByDocId(Convert.ToInt32(this.DocumentID), Sessions.SwitchedSessionId, Enum_Tatva.Action.Move.GetHashCode());
                General_Class.CreateDocumentsAndFolderLogs(new Guid(Sessions.SwitchedSessionId), item.FolderID, 0, Convert.ToInt32(this.DocumentID), item.DividerID, 0, 0, 0, null, 0, Enum_Tatva.DocumentsAndFoldersLogs.WorkAreaSplitUpload.GetHashCode(), 0, 0, 0, 0, 0);
            }
            this.DocumentName = item.FileName;
        }
        catch (Exception ex)
        {
        }
    }


    public void ProcessFile(AjaxFileUploadEventArgs file, byte[] fileStream, int dividerIdForFile, string path)
    {
        try
        {

            float size = (float)Math.Round((decimal)(fileStream.Length / 1024) / 1024, 2); ; // IN MB.

            ProcessFileAndSaveInDB(size, path);
            Guid guid = new Guid(Sessions.SwitchedSessionId);
            General_Class.DocumentsInsertForeFileSplit(newFileName, guid, Convert.ToDecimal(size), Enum_Tatva.IsDelete.No.GetHashCode(), Sessions.UserId);

            fullFilePath = path;

        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(Convert.ToString(this.EffID_FFB), Convert.ToString(dividerIdForFile), newFileName, ex);

            Dictionary<string, string> UploadErrorFileName1 = new Dictionary<string, string>();
            UploadErrorFileName1.Add(file.FileId, file.FileName);

            UploadErrorFileName = UploadErrorFileName1;

            if ((File.Exists(fullFilePath)))
                File.Delete(fullFilePath);
        }
    }

    public void ExtractAndUploadPDF(string pageNumbers, string fileName, string newFName, string fileID, ref string newEncodeName, ref string outputPdfPath)
    {
        PdfReader reader = null;
        PdfCopy pdfCopyProvider = null;
        PdfCopy pdfRemaining = null;
        PdfImportedPage importedPage = null;
        PdfImportedPage importedPage1 = null;
        iTextSharp.text.Document sourceDocument = null;
        RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
        string errorMsg = string.Empty;
        try
        {
            string[] extractThesePages = pageNumbers.Split(',');

            string sourcePdfPath;
            string encodeName = HttpUtility.UrlPathEncode(fileName);
            encodeName = encodeName.Replace("%20", " ");

            string uid = Sessions.SwitchedSessionId;
            string path = HttpContext.Current.Server.MapPath("~/");

            sourcePdfPath = path + encodeName;


            outputPdfPath = path + Path.DirectorySeparatorChar + Common_Tatva.RackSpaceNewSplitWorkareaDownload + Path.DirectorySeparatorChar + uid;

            if (!Directory.Exists(outputPdfPath))
            {
                Directory.CreateDirectory(outputPdfPath);
            }

            if (newFName.IndexOf(".pdf") < 0)
                newFName = newFName + ".pdf";

            newFName = newFName.Replace("#", "");
            newFName = newFName.Replace("&", "");

            newEncodeName = HttpUtility.UrlPathEncode(newFName);
            newEncodeName = newEncodeName.Replace("%", "$");
            outputPdfPath = outputPdfPath + Path.DirectorySeparatorChar + newEncodeName;


            string remainingPath = path + Path.DirectorySeparatorChar + Common_Tatva.RackSpaceNewSplitWorkareaDownload + Path.DirectorySeparatorChar + uid + Path.DirectorySeparatorChar + encodeName.Substring(encodeName.LastIndexOf('/') + 1);

            reader = new PdfReader(sourcePdfPath);

            // For simplicity, I am assuming all the pages share the same size
            // and rotation as the first page:
            sourceDocument = new iTextSharp.text.Document(reader.GetPageSizeWithRotation(int.Parse(extractThesePages[0])));

            // Initialize an instance of the PdfCopyClass with the source 
            // document and an output file stream:
            pdfCopyProvider = new PdfCopy(sourceDocument, new System.IO.FileStream(outputPdfPath, System.IO.FileMode.Create));
            if (reader.NumberOfPages > extractThesePages.Length)
                pdfRemaining = new PdfCopy(sourceDocument, new System.IO.FileStream(remainingPath, System.IO.FileMode.Create));
            sourceDocument.Open();

            // Walk the array and add the page copies to the output file:
            foreach (string pageNumber in extractThesePages)
            {
                importedPage = pdfCopyProvider.GetImportedPage(reader, int.Parse(pageNumber));
                pdfCopyProvider.AddPage(importedPage);
            }
            for (int pageNumber = 1; pageNumber <= reader.NumberOfPages; pageNumber++)
            {
                if (extractThesePages.Contains(pageNumber.ToString()))
                    continue;
                importedPage1 = pdfRemaining.GetImportedPage(reader, pageNumber);
                pdfRemaining.AddPage(importedPage1);
            }
            if (sourceDocument != null && sourceDocument.PageNumber > 0)
                sourceDocument.Close();
            if (reader != null)
                reader.Close();
            try
            {
                if (pdfCopyProvider != null)
                {
                    pdfCopyProvider.PageEmpty = false;
                    pdfCopyProvider.Close();
                }
                if (pdfRemaining != null)
                {
                    pdfRemaining.PageEmpty = false;
                    pdfRemaining.Close();
                }
            }
            catch (Exception ex)
            {
            }
            if (File.Exists(remainingPath))
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                File.Copy(remainingPath, sourcePdfPath, true);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                File.Delete(remainingPath);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                //FileInfo fileInfo = new FileInfo(sourcePdfPath);
                //decimal fileSize = Math.Round((decimal)(fileInfo.Length / 1024) / 1024, 2);
                //General_Class.UpdateFileSplitSize(int.Parse(fileID), (long)fileSize);
            }
            //else
            //{
            //    General_Class.UpdateFileSplit(fileName, int.Parse(fileID), Enum_Tatva.IsDelete.No.GetHashCode(), 0, 1);
            //}
        }
        catch (Exception ex)
        {
            if (sourceDocument != null)
                sourceDocument.Close();
            if (reader != null)
                reader.Close();
            pdfCopyProvider.Close();
            pdfRemaining.Close();
        }

    }

    public bool ExtractPages(string pageNumbers, string fileName, string newFName, string fileID, string folderName)
    {
        string folderID = hdnSelectedFolderID.Value;
        string dividerID = hdnSelectedDividerID.Value;
        try
        {

            string newEncodeName = string.Empty;
            string outputPdfPath = string.Empty;

            ExtractAndUploadPDF(pageNumbers, fileName, newFName, fileID, ref newEncodeName, ref outputPdfPath);


            string pathName = HttpContext.Current.Server.MapPath(string.Format("~/{0}", CurrentVolume) + Path.DirectorySeparatorChar + folderID + Path.DirectorySeparatorChar
                + dividerID);
            newEncodeName = Common_Tatva.GetRenameFileName(pathName + Path.DirectorySeparatorChar + newEncodeName);

            if (!Directory.Exists(pathName))
            {
                Directory.CreateDirectory(pathName);
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
            File.Copy(outputPdfPath, newEncodeName);

            FileInfo fileInfo = new FileInfo(newEncodeName);
            decimal fileSize = Math.Round((decimal)(fileInfo.Length / 1024) / 1024, 2);

            string temp = newEncodeName.Substring(newEncodeName.LastIndexOf("\\") + 1);
            newFileName = HttpUtility.UrlDecode(temp.Replace('$', '%').ToString());

            ProcessFileAndSaveInDB((float)fileSize, pathName, int.Parse(dividerID), int.Parse(folderID), true);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            File.SetAttributes(outputPdfPath, FileAttributes.Normal);
            File.Delete(outputPdfPath);

            return true;

        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(folderID, dividerID, fileName, ex);
            return false;
        }
    }

    #endregion

    protected void btnWorkAreaSearch_Click(object sender, EventArgs e)
    {
        this.DataSource = General_Class.SearchWorkAreaDocs(txtSearch.Text, txtIndexVal.Text, hdnSelectedPath.Value, Sessions.SwitchedRackspaceId);
        this.WorkArea_DataSource = this.DataSource;
        BindGrid();
        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "hideLoader();", true);
    }
}
