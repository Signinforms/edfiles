using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Security.AccessControl;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Engines.Adapters;

public partial class WorkAreaUploader : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!Page.IsPostBack)
        {
            string path = Server.MapPath("~/ScanInBox");

            string userName = Request.Form["uid"];
            string uid = UserManagement.ExistUserName(userName);
            if (uid == null)
            {
                Response.Write("The user doesn't exist");
                Response.End();
            }

            HttpFileCollection files = Request.Files;
            if (files.Count == 0)
            {   
                Response.End();
            }

            HttpPostedFile file = files[0];

            if (file != null && file.ContentLength > 0)
            {
                // flash 会自动发送文件名到 Request.Form["fileName"]
               
                string fileName = Request.Form["fileName"];
                string pathname = string.Format("{0}\\{1}", path, uid);
                string savePath = string.Format("{0}\\{1}\\{2}", path, uid, fileName);
                try
                {
                    if (!Directory.Exists(pathname))
                    {
                        try
                        {
                            Directory.CreateDirectory(pathname);
                        }
                        catch(Exception exp)
                        {
                            Response.Write(exp.Message);
                            Response.End();
                        }
                        
                    }
                    file.SaveAs(savePath);
                   
                }
                catch (Exception exp)
                {
                    Response.Write(exp.Message);
                    Response.End();
                }
               
            }

        }
    }
}
