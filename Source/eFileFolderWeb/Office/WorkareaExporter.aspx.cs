﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;

using Shinetech.DAL;
using Shinetech.Engines;
using Aspose.Cells;
using Style = Aspose.Cells.Style;

public partial class Office_WorkareaExporter : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            DataTable dtWorkarea = General_Class.GetWorkAreaDocuments(Sessions.RackSpaceUserID);
            dtWorkarea.Columns.Add("No of documents");
            foreach (DataRow dr in dtWorkarea.Rows)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(dr["PathName"])))
                    dr["PathName"] = dr["PathName"].ToString().Substring(0, dr["PathName"].ToString().LastIndexOf('/'));
            }
            dtWorkarea.DefaultView.Sort = "PathName ASC";
            dtWorkarea = dtWorkarea.DefaultView.ToTable();
            var dtGroups = dtWorkarea.AsEnumerable().GroupBy(r => r.Field<string>("PathName")).Select(r => new { Str = r.Key, Count = r.Count() });
            foreach (DataRow dr in dtWorkarea.Rows)
            {
                foreach (var i in dtGroups)
                {
                    if (dr["PathName"].ToString() != "workArea" && dr["PathName"].ToString() == i.Str)
                        dr["No Of Documents"] = i.Count;
                    else if (dr["PathName"].ToString() == "workArea")
                        dr["No Of Documents"] = 1;
                }
            }
            OutFileToStream(dtWorkarea, "WorkArea Report");
        }
    }

    public void OutFileToStream(DataTable dt, string tableName)
    {
        dt.Columns.Remove("WorkAreaId");
        dt.Columns.Remove("IsDeleted");
        dt.Columns.Remove("DocumentId");
        dt.Columns.Remove("ModifyDate");
        dt.Columns.Remove("Size");
        dt.Columns.Remove("UID");
        Workbook workbook = new Workbook(); //工作簿
        Worksheet sheet = workbook.Worksheets[0]; //工作表
        Cells cells = sheet.Cells;//单元格


        //为标题设置样式    
        Aspose.Cells.Style styleTitle = workbook.Styles[workbook.Styles.Add()];//新增样式
        styleTitle.HorizontalAlignment = TextAlignmentType.Center;//文字居中
        styleTitle.Font.Name = "Arial";//文字字体
        styleTitle.Font.Size = 16;//文字大小
        styleTitle.Font.IsBold = true;//粗体

        //样式2
        Style style2 = workbook.Styles[workbook.Styles.Add()];//新增样式
        style2.HorizontalAlignment = TextAlignmentType.Center;//文字居中
        style2.Font.Name = "Arial";//文字字体
        style2.Font.Size = 12;//文字大小
        style2.Font.IsBold = true;//粗体
        style2.IsTextWrapped = true;//单元格内容自动换行
        style2.Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thin;
        style2.Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thin;
        style2.Borders[BorderType.TopBorder].LineStyle = CellBorderType.Thin;
        style2.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thin;

        //样式3
        Style style3 = workbook.Styles[workbook.Styles.Add()];//新增样式
        style3.HorizontalAlignment = TextAlignmentType.Center;//文字居中
        style3.Font.Name = "Arial";//文字字体
        style3.Font.Size = 12;//文字大小
        style3.Borders[BorderType.LeftBorder].LineStyle = CellBorderType.Thin;
        style3.Borders[BorderType.RightBorder].LineStyle = CellBorderType.Thin;
        style3.Borders[BorderType.TopBorder].LineStyle = CellBorderType.Thin;
        style3.Borders[BorderType.BottomBorder].LineStyle = CellBorderType.Thin;

        int Colnum = dt.Columns.Count;//表格列数
        int Rownum = dt.Rows.Count;//表格行数

        //生成行1 标题行   
        cells.Merge(0, 0, 1, Colnum);//合并单元格
        cells[0, 0].PutValue(tableName);//填写内容
        cells[0, 0].SetStyle(styleTitle);
        cells.SetRowHeight(0, 58);

        //生成行2 列名行
        for (int i = 0; i < Colnum; i++)
        {
            cells[1, i].PutValue(dt.Columns[i].ColumnName);
            cells[1, i].SetStyle(style2);
            cells.SetRowHeight(1, 25);
        }

        //生成数据行
        for (int i = 0; i < Rownum; i++)
        {
            for (int k = 0; k < Colnum; k++)
            {
                cells[2 + i, k].PutValue(dt.Rows[i][k].ToString());
                cells[2 + i, k].SetStyle(style3);
            }
            cells.SetRowHeight(2 + i, 24);
        }

        // workbook.SaveOptions.SaveFormat = SaveFormat.Xlsx;
        sheet.AutoFitColumns();
        workbook.Save(Response, tableName + ".xls", ContentDisposition.Attachment, workbook.SaveOptions);
    }
}