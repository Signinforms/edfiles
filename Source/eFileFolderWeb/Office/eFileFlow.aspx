﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="eFileFlow.aspx.cs" Inherits="Office_eFileFlow" %>

<%--<%@ Register Src="../Controls/FolderReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>--%>
<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register Src="~/Office/UserControls/ucRequestFiles.ascx" TagPrefix="uc3" TagName="ucRequestFiles" %>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%--<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />--%>

    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>js/bootstrap-multiselect.js"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/bootstrap-multiselect.css" rel="stylesheet" type="text/css">

    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=2" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>extras/jquery-ui-1.8.22.custom.min.js"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/edFiles/EdFilesLogs.js?v=4" type="text/javascript"></script>

    <style type="text/css">
        .white_content
        {
            display: none;
            position: fixed;
            top: 20%;
            left: 20%;
            width: 60%;
            height: 50%;
            padding: 16px;
            background-color: white;
            z-index: 999999999;
            overflow: auto;
        }

        body
        {
            font-family: "Open Sans", sans-serif;
        }

        #overlay
        {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }

        .actionMinWidth
        {
            min-width: 50px;
        }

        td span
        {
            -ms-word-break: break-all;
            word-break: break-all;
            word-break: break-word;
            -webkit-hyphens: auto;
            -moz-hyphens: auto;
            hyphens: auto;
            max-width: 300px;
            white-space: normal!important;
        }

        .ajax__fileupload_dropzone
        {
            font-size: 15px;
            line-height: 120px !important;
            height: 120px !important;
            font-family: "Open Sans", sans-serif;
        }

        td
        {
            max-width: 200px;
        }

        .boxWidth
        {
            width: 1100px;
            margin: auto;
        }

        .paging_position
        {
            position: relative;
            left: 365px;
        }

        select
        {
            /*border: solid 1px #c4c4c4;*/
            height: 30px;
            line-height: 30px;
            padding: 5px;
            max-width: 230px;
            /*width: 100%;*/
        }

        #popupclose
        {
            float: right;
            padding: 10px;
            cursor: pointer;
        }

        .popupcontent
        {
            padding: 10px;
        }

        .downloadFile
        {
            margin-left: -10px;
            margin-top: -2px;
            cursor: pointer;
        }

        .preview-popup
        {
            width: 100%;
            max-width: 1100px;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 10%;
            right: 0;
            margin: auto;
            background: #fff;
            height: 85%;
        }

        #drpGroups
        {
            float: right;
            margin-right: 5px;
        }

        .txtEmails
        {
            float: right;
            height: 30px;
            margin-right: 5px;
            padding-left: 5px;
        }

        #groupList
        {
            width: 100%;
            display: none;
            border: 1px #dadada solid;
            position: absolute;
            left: 0;
            top: 100%;
            z-index: 9;
            background: #fff;
            max-height: 235px;
            overflow-y: auto;
            font-size: 12px;
        }

            #groupList label
            {
                display: block;
                padding: 0 5px;
                width: auto;
                min-width: initial;
                cursor: pointer;
                margin-top: 3px;
            }

                #groupList label:hover
                {
                    background-color: #e5e5e5;
                }

            #groupList input
            {
                display: inline-block;
                vertical-align: middle;
            }

        .selectBox-outer
        {
            position: relative;
            /*display: inline-block;*/
            margin-right: 5px;
            font-family: 'open sans', sans-serif;
        }

        .selectBox-outermail
        {
            position: relative;
            font-family: 'open sans', sans-serif;
        }

        .selectBox select
        {
            width: 100%;
        }

        .overSelect
        {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            font-size: 15px;
        }

        #drpGroups_chosen
        {
            display: block;
            margin-top: 5px;
        }

        #drpGroups_chosen
        {
            display: block;
            margin-top: 5px;
        }

        #mailGroups_chosen
        {
            display: block;
        }

        #mailGroups_chosen
        {
            display: block;
        }

        #folderList
        {
            width: 100%;
            display: none;
            border: 1px #dadada solid;
            position: absolute;
            left: 0;
            top: 100%;
            z-index: 9;
            background: #fff;
            max-height: 235px;
            overflow-y: auto;
        }

            #folderList label
            {
                display: block;
                padding: 0 5px;
                width: auto;
                min-width: initial;
                cursor: pointer;
                margin-top: 3px;
            }

                #folderList label:hover
                {
                    background-color: #e5e5e5;
                }

            #folderList input
            {
                display: inline-block;
            }

        .collapseFolder
        {
            display: none;
            background-color: white;
            margin-top: -5px;
            position: absolute;
            z-index: 10000;
            left: 44%;
            border: none;
            border-bottom-right-radius: 100%;
            border-bottom-left-radius: 100%;
            background: url(../images/up-arrow-key.png) 2px -3px no-repeat white;
            height: 24px;
            width: 30px;
            border: solid 1px #94ba33;
            cursor: pointer;
            top: 100%;
        }

        .chosen-results
        {
            max-height: 200px !important;
        }

        .chosen-container
        {
            font-size: 14px;
        }

        .ui-dialog
        {
            top: 300px !important;
        }

        .chosen-container .chosen-choices
        {
            max-height: 70px;
            overflow: auto;
        }

        .chosen-container-single .chosen-single
        {
            color: rgb(0,0,0);
        }
    </style>
    <script language="JavaScript" type="text/jscript">
        var docId, fileName, pathForSharemail;
        var pubExpanded = false;
        function pageLoad() {
            //$(function () {
            //    $('[id*=drpEmail]').multiselect({
            //        includeSelectAllOption: true
            //    });
            //});
            //$('.chosen-select').chosen();

            GetEmailToSend();
            $('body').click(function (evt) {
                if (evt.target.className == "chosen-search-input" || evt.target.className == "chosen-search-input default" || evt.target.className == "chosen-results" || evt.target.className == "search-choice" || evt.target.className == "chosen-choices" || $(evt.target).parent().attr("class") == "search-choice" || evt.target.className == "result-selected") {
                    $('.chosen-drop').css("display", "block");
                    $('.collapseFolder').css("display", "block");
                    return;
                }
                else {
                    $('.chosen-drop').css("display", "none");
                    $('.collapseFolder').css("display", "none");
                }

            });
            $("#comboBoxFolder").change(function () {
                $('.search-choice span').click(function () {
                    $('.collapseFolder').css("display", "block");
                })
                $('.search-choice-close').click(function () {
                    if ($('.chosen-choices').children().length > 2)
                        $('.collapseFolder').css("display", "none");
                })
            })

            GetEmailGroups();
            $('.editDocument').click(function (e) {
                e.preventDefault();
                removeAllEditable();
                showEdit($(this));
            });

            $('.cancelDocument').click(function (e) {
                e.preventDefault();
                removeAllEditable();
            });

            $('.shareDocument').click(function (e) {
                e.preventDefault();
                //OnShowShare($(this));
            });

            var closePopup = document.getElementById("popupclose");
            closePopup.onclick = function () {
                var popup = document.getElementById("preview_popup");
                var overlay = document.getElementById("overlay");
                overlay.style.display = 'none';
                popup.style.display = 'none';
                $('#floatingCirclesG').css('display', 'block');
                enableScrollbar();
                $('#reviewContent').removeAttr('src');
            };
        }
        $(document).ready(function () {
            $(".ajax__fileupload_dropzone").text("Drag and Drop Pdf file(s) here");
            $('#dialog-share-book').dialog({
                autoOpen: false,
                width: 450,
                //height: 540,
                resizable: false,
                modal: true,
                buttons: {
                    "Share Now": function () {
                        SaveMail();
                    },
                    "Close": function () {
                        $(this).dialog("close");
                    }
                },
                close: function () {
                }
            });

            $("#dialog-message-all").dialog({
                autoOpen: false,
                resizable: false,
                modal: true,
                Ok: function () {
                    $(this).dialog("close");
                },
                close: function (event, ui) {
                    $(this).dialog("option", 'title', '');
                    $('#dialog-confirm-content').empty();
                }

            });

            $("#ButtonDeleleOkay").live({
                click: function () {
                    showLoader();
                }
            });

            $('.ui-button-text').each(function (i) {
                $(this).html($(this).parent().attr('text'))
            })

            <%--$('#dialog-share-book input[name="linkexpiration"]').val('<%= Sessions.ExpirationTime %>');--%>
        });

        function showPreview(URL) {
            hideLoader();
            disableScrollbar();
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//"
                  + window.location.hostname
                  + (window.location.port ? ':' + window.location.port : '');
            }
            var popup = document.getElementById("preview_popup");
            var overlay = document.getElementById("overlay");
            $('#reviewContent').attr('src', './Preview.aspx?data=' + window.location.origin + URL.trim() + "?v=" + "<%= DateTime.Now.Ticks%>");
            //$('#reviewContent').attr('src', src.trim());
            overlay.style.display = 'block';
            popup.style.display = 'block';
            $('#floatingCirclesG').css('display', 'none');
            //$('#preview_popup').css('top', ($(window).scrollTop() + 50) + 'px');
            $('#preview_popup').focus();
            return false;
        }

        function disableScrollbar() {
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }

        function enableScrollbar() {
            $('html, body').css({
                'overflow': 'auto',
                'height': 'auto'
            });
        }


        function ShowPreviewForEFileFlow(eFileFlowId, fileName) {
            showLoader();
            var folderID = <%= (int)Enum_Tatva.Folders.EFileFlow %>
                           $.ajax({
                               type: "POST",
                               url: '../Office/eFileFlow.aspx/GetPreviewURL',
                               contentType: "application/json; charset=utf-8",
                               data: "{ documentID:'" + eFileFlowId + "', folderID:'" + folderID + "', shareID:'" + fileName + "'}",
                               dataType: "json",
                               success: function (data) {
                                   if (data.d == "") {
                                       hideLoader();
                                       disableScrollbar();
                                       alert("Failed to get object from cloud");
                                       return false;
                                   }
                                   else
                                       showPreview(data.d);
                               },
                               error: function (result) {
                                   console.log('Failed' + result.responseText);
                                   hideLoader();
                               }
                           });
            return false;
        }

        function removeAllEditable() {
            $('.txtDocumentName,.saveDocument,.cancelDocument').hide();
            $('.lblDocumentName,.editDocument').show();
        }

        function GetEmailGroups() {
            $("#groupList").html('');
            $("#drpGroups").html('');
            $("#drpGroups").chosen("destroy");
            $.ajax(
             {
                 type: "POST",
                 url: '../FlexWebService.asmx/GetEmailGroups',
                 contentType: "application/json; charset=utf-8",
                 data: '{"officeId":"' + '<%= Sessions.SwitchedSessionId %>' + '"}',
                 dataType: "json",
                 success: function (result) {
                     if (result.d.length > 0) {
                         var emailGroups = JSON.parse(result.d);
                         if (emailGroups.length > 0) {
                             $('<label><input type="checkbox" name="select-all" id="select-all" onclick="toggle(this);"/>Select All</label>').appendTo($('#groupList'));
                             for (var i = 0; i < emailGroups.length; i++) {
                                 var container = $('#groupList');
                                 $('<label for="cb' + emailGroups[i].GroupId + '"><input type="checkbox" id="cb' + emailGroups[i].GroupId + '" value=' + emailGroups[i].GroupId + '> ' + emailGroups[i].GroupName.trim() + '</label>').appendTo(container);
                             }
                         }
                     }
                     $("#drpGroups").chosen({ width: "315px" });
                     // hideLoader();

                 },
                 fail: function (data) {
                     alert("Failed to get Folders. Please try again!");
                     hideLoader();
                 }
             });
         }

         function GetEmailToSend() {
             $("#folderList").html('');
             $("#comboBoxFolder").html('');
             $("#comboBoxFolder").chosen("destroy");
             $.ajax(
              {
                  type: "POST",
                  url: '../Office/eFileFlow.aspx/GetMailToSend',
                  contentType: "application/json; charset=utf-8",
                  data: '{"officeId":"' + '<%= Sessions.SwitchedRackspaceId %>' + '"}',
                  dataType: "json",
                  success: function (result) {
                      if (result.d) {
                          var emailGroups = JSON.parse(result.d);
                          if (emailGroups.length > 0) {
                              $('<option value="All Folders">All Folders</option>').appendTo($('#comboBoxFolder'));
                              //$('<option value="Email Groups">Email Groups</option>').appendTo($('#comboBoxFolder'));
                              for (var i = 0; i < emailGroups.length; i++) {
                                  if (emailGroups[i].Type == 1)
                                      $('<option />', { value: emailGroups[i].Alert + "(Alert)", text: emailGroups[i].Alert.trim() + "(Alert)" }).appendTo($("#comboBoxFolder"));
                                  else if (emailGroups[i].Type == 2)
                                      $('<option />', { value: emailGroups[i].Alert + "(Status)", text: emailGroups[i].Alert.trim() + "(Status)" }).appendTo($("#comboBoxFolder"));
                              };
                              $('.chosen-toggle').each(function (index) {
                                  $(this).on('click', function () {
                                      $(this).parent().parent().find('option').prop('selected', $(this).hasClass('select')).parent().trigger('chosen:updated');
                                  });
                              });
                          }
                          $("#comboBoxFolder").chosen({ width: "100%" });
                          $chosen = $("#comboBoxFolder").chosen();

                          var chosen = $chosen.data("chosen");
                          var _fn = chosen.result_select;
                          chosen.result_select = function (evt) {
                              evt["metaKey"] = true;
                              evt["ctrlKey"] = true;
                              chosen.result_highlight.addClass("result-selected");
                              return _fn.call(chosen, evt);
                          };
                          //$('.chosen-results').after('<asp:Button runat="server" ID="collapse" CssClass="collapseFolder" OnClientClick="collapseFolder();return false;"></asp:Button>');

                          // hideLoader();
                      }
                  },
                  fail: function (data) {
                      alert("Failed to get Folders. Please try again!");
                      hideLoader();
                  }
              });
          }

          function toggle(source) {
              var checkboxes = $("#groupList").find("input[type=checkbox]");
              for (var i = 0; i < checkboxes.length; i++) {
                  if (checkboxes[i] != source)
                      checkboxes[i].checked = source.checked;
              }
          }

          function toggleMailGroup(source) {
              var checkboxes = $("#folderList").find("input[type=checkbox]");
              for (var i = 0; i < checkboxes.length; i++) {
                  if (checkboxes[i] != source)
                      checkboxes[i].checked = source.checked;
              }
          }

          function collapseFolder() {
              pubExpanded = false;
              $('.chosen-drop').css("display", "none");
              $('.collapseFolder').css("display", "none");
          }

          function getOs() {
              var OsObject = "";
              if (navigator.userAgent.indexOf("MSIE") > 0) {
                  return "MSIE";
              }
              if (isFirefox = navigator.userAgent.indexOf("Firefox") > 0) {
                  return "Firefox";
              }
              if (isSafari = navigator.userAgent.indexOf("Safari") > 0) {
                  return "Safari";
              }
              if (isCamino = navigator.userAgent.indexOf("Camino") > 0) {
                  return "Camino";
              }
              if (isMozilla = navigator.userAgent.indexOf("Gecko/") > 0) {
                  return "Gecko";
              }

          }
          var os = getOs();

          function onClientUploadComplete(sender, e) {
              return false;
          }

          var myTimer;
          var fileIDDividerId = [];
          function onClientUploadStart(sender, e) {
              document.getElementById('uploadCompleteInfo').innerHTML = "please wait while the " + e.get_filesInQueue() + " file is uploaded..";
              document.cookie = "Files=" + JSON.stringify(fileIDDividerId);
              showLoader();

              $('[id*=ajaxfileuploadeFileFlow_Html5DropZone]').addClass('hideControl').css('display', 'none');
              $('[id*=ajaxfileuploadeFileFlow_SelectFileContainer]').addClass('hideControl').css('display', 'none');
              $('[id*=FileItemDeleteButton]').addClass('hideControl').css('display', 'none');
              $('[id*=UploadOrCancelButton]').addClass('hideControl').css('display', 'none');
              $('.TabDropDown').prop('disabled', 'disabled');
              $('#uploadProgress').html('');
              $('#uploadProgress').html('').html($('#divUpload').html());

              document.getElementById('uploadProgress').style.display = 'block';
              myTimer = setInterval(function () {
                  $('#uploadProgress').html('').html($('#divUpload').html());
              }, 1000);

          }

          function showLoader() {
              $('#overlay').show().height($(document).height());
              $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
          }

          function hideLoader() {
              $('#overlay').hide();
          }

          function onClientUploadCompleteAll(sender, e) {
              $('.hideControl').removeClass('hideControl');
              $('.uploadProgress').find('select').removeAttr('disabled');

              //$('#divUpload').clone().appendTo('#uploadFieldset');
              clearInterval(myTimer);
              document.getElementById('uploadProgress').style.display = 'none';
              hideLoader();
              //__doPostBack('<%= UpdatePanel1.ClientID %>', '');

             setTimeout(function () {
                 var vd = '<%= ConfigurationManager.AppSettings["VirtualDir"] %>';
                window.location.reload();
            }, 2000);
        }

        function onClientButtonClick() {
            $find('pnlPopup2').hide();

            setTimeout(function () {
            }, 1000);
            return false;
        }

        function showEdit(ele) {
            $('.saveDocument,.cancelDocument').hide();
            $('.delete,.shareDocument,.editDocument, .lnkbtnPreview, .downloadFile').show();
            var $this = $(ele);
            $this.css('display', 'none');
            $this.parent().find('[name=lnkUpdate]').show();
            $this.parent().find('[name=lnkcancel]').show();
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                $(this).find('input[type=text]').css('border', '1px solid #c4c4c4');//1px solid #c4c4c4
                $(this).find('input[type = text]').show();
                if ($(this).find('input[type = text]').val() == undefined || $(this).find('input[type = text]').val() == "")
                { }
                else
                {
                    oldName = $(this).find('input[type = text]').val();
                }
                $(this).find('[name=DocumentName]').hide();
                $(this).find('[name=lblClass]').hide();
                $(this).closest('td').find('.delete,.shareDocument, .lnkbtnPreview, .downloadFile').hide();
            });
            return false;
        }
        function cancleFileName(ele) {
            var $this = $(ele);
            var LabelName = '';
            var FileName = '';
            $this.css('display', 'none');
            $this.parent().find('[name=lnkUpdate]').hide();
            $this.parent().find('[name=lnkBtnEdit]').show();
            $this.parent().find('.delete,.shareDocument, .lnkbtnPreview, .downloadFile').show();
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                var len = 0;
                len = $(this).find('input[type=text]').length;
                if (len > 0) {
                    LabelName = $(this).find('[name=DocumentName]').html();
                    $(this).find('input[type = text]').val(LabelName);
                    $(this).find('input[type = text]').hide();
                    $(this).find('[name=DocumentName]').show();
                }
                else {
                }
            });
        }

        var newname, Path, oldName, eFileFlowId, oldFileName;
        function OnSaveRename(txtRenameObj, path, eFileFlowID, fileName) {
            Path = path;
            eFileFlowId = eFileFlowID;
            newname = $('#' + txtRenameObj)[0].value;
            oldFileName = fileName;
            if (newname == undefined || newname == "") {
                $('.txtDocumentName').css('border', '1px solid red');
                return false;
            }
            else if (!/^[a-z\d][a-z\d\()_\-.\s]+$/i.test(newname)) {
                alert("File name can not contain special characters.");
                $('.txtDocumentName').css('border', '1px solid red');
                return false;
            }
            else if (newname.indexOf('.') > 0 && newname.substring(newname.indexOf('.') + 1).toLowerCase() != 'pdf') {
                alert("Only File with type 'pdf' is allowed.");
                $('.txtDocumentName').css('border', '1px solid red');
                return false;
            }
            else {
                $('.txtDocumentName').css('border', '1px solid c4c4c4');
            }
            showLoader();
            $.ajax(
    {
        type: "POST",
        url: '../EFileFolderJSONService.asmx/RenameDocument',
        data: "{ renameFile:'" + newname + "', path:'" + Path + "',eFileFlowId:'" + eFileFlowId + "',oldFileName:'" + oldFileName + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            if (result.d.length > 0)
                alert("Failed to rename document. Please try again after sometime.");
            hideLoader();
            __doPostBack('', 'RefreshGrid@');
        },
        error: function (result) {
            hideLoader();
        }
    });
        }

        function OnShowShare() {
            if ($('table input[type="checkbox"]:checked').length <= 0) {
                alert("Please Select Atleast one document.");
                hideLoader();
                return false;
            }

            $('#dialog-share-book').dialog("open");
            $('#dialog-share-book input[name="emailto"]').val('');
            $("#comboBoxFolder").val('').trigger('chosen:updated');
            $('#dialog-share-book input[name="nameto"]').val('');
            $('#dialog-share-book input[name="email"]').val('<%=Sessions.SwitchedEmailId%>');
            $('#dialog-share-book input[name="linkexpiration"]').val('<%= Sessions.ExpirationTime %>');
            //$('#dialog-share-book input[name="title"]').val('')
            $('#dialog-share-book textarea[name="content"]').val('');
            //GetExpirationTime();
            $('#dialog-share-book input[name="emailto"]').css('border', '1px solid black');

        }

        function CheckValidation(fieldObj) {
            if (fieldObj.val() == "") {
                fieldObj.css('border', '1px solid red');
            }
            else {
                fieldObj.css('border', '1px solid black');
            }
        }

        function CheckEmailValidation(fieldObj) {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(fieldObj)) {
                //fieldObj.css('border', '1px solid black');
                return true;
            }
            else {
                //fieldObj.css('border', '1px solid red');

                return false;
            }
        }

        function CloseModal() {
            $('#dialog-share-book').dialog("close");
        }

        function SendMail(emailList, docId, fileName, emailto, nameto, content, pathForSharemail, nameToList, email, title) {
            for (var i = 0; i < emailList.length; i++) {
                emailto.push({ "Email": emailList[i], "ToName": nameToList[i] });
            }
            var src = '';
            var userId = "<%= Sessions.SwitchedRackspaceId%>"
            $.ajax(
                {
                    type: "POST",
                    url: '../EFileFolderJSONService.asmx/ShareMailForeFileFlow',
                    //data: "{ strSendMail:'" + email + "', strToMail:'" + emailto + "', documentId:'" + documentId + "', strMailInfor:'" + content + "',SendName:'" + name + "', ToName:'" + nameto + "',MailTitle:'" + title + "',Path:'" + src + "'}",
                    data: "{eFileFlowId:'" + docId + "',FileName:'" + fileName + "',strToMail:'" + JSON.stringify(emailto) + "',MailTitle:'" + title + "',ToName:'" + nameto + "',SendName:'" + "" + "',strSendMail:'" + email + "',strMailInfor:'" + content + "',pathName:'" + pathForSharemail + "',userId:'" + userId + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var errorMsg = data.d;
                        var title, message;
                        if (errorMsg != "") {
                            title = "Sharing EdFileFlow document error";
                            message = errorMsg;
                        }
                        else {
                            title = "Sharing EdFileFlow document success";
                            message = "This document has been shared successfully."
                        }

                        $('#dialog-message-all').dialog({
                            title: title,
                            open: function () {
                                $("#spanText").text(message);
                            }
                        });

                        $('#dialog-message-all').dialog("open");

                        if (errorMsg == null || errorMsg == "")
                            $('#dialog-share-book').dialog("close");
                        hideLoader();
                    },
                    fail: function (data) {
                        bookshelf.loaded.apply();

                        $('#dialog-message-all').dialog({
                            title: "Sharing EdFileFlow document error",
                            open: function () {
                                $("#spanText").text("This document has not been shared successfully.");
                            }
                        });
                        $('#dialog-message-all').dialog("open");

                        hideLoader();
                    }
                });
        }
        function SaveMail() {

            showLoader();
            ids = "";
            var children = $('.chosen-choices').children();
            var folders = "";
            if ($('table input[type="checkbox"]:checked').length > 0) {
                $('table input[type="checkbox"]:checked').each(function () {
                    if ($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]") && $(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val() && JSON.parse($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()))
                        ids += JSON.parse($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()) + ",";
                });
                ids = ids.substring(0, ids.length - 1);
                docId = ids;
            }
            else {
                alert("Please Select Atleast one document.");
                hideLoader();
                return false;
            }

            //var checked_checkboxes = $("[id*=chkFruits] input:checked");
            //var message = "";
            //checked_checkboxes.each(function () {
            //    var value = $(this).val();
            //    var text = $(this).closest("td").find("label").html();
            //    message += text + ",";
            //});
            if (children.length > 1) {
                for (var i = 0; i < children.length - 1; i++) {
                    if ($(children[i])) {
                        folders += $($('#comboBoxFolder').find('option')[$(children[i]).find('a').attr('data-option-array-index')]).val() + ", ";
                    }
                }
                folders = folders.substr(0, folders.lastIndexOf(','));
            }
            //if (folders.length <= 0 || children.length < 2) {
            //    hideLoader();
            //    alert("Please select at least one folder to copy.");
            //    return false;
            //}
            $("#" + '<%=hdnFolderID.ClientID %>').val(folders);


             var nameto = $('#dialog-share-book input[name="nameto"]').val();
             var emails = $('#dialog-share-book input[name="emailto"]').val();
            //var emails = document.getElementById("multiselect").title;
             var emailgroups = folders;
             var email = $('#dialog-share-book input[name="email"]').val();
             var title = $('#dialog-share-book input[name="title"]').val();
             var emailList = "";
             if (emails.length > 0) {
                 var emailsList = emails.split(';');
                 for (var i = 0; i < emailsList.length; i++) {
                     var test = "";
                     if (!CheckEmailValidation(emailsList[i])) {
                         alert("Please enter valid Email separated by ';' !")
                         $('#dialog-share-book input[name="emailto"]').css('border', '1px solid red');
                         hideLoader();
                         return false;
                     }
                 }
             }

             if (emailgroups.length == 0 && emails.length == 0) {
                 alert("Please select any type of Email");
                 hideLoader();
                 return false;
             }

             if (emails.length > 0 && emailgroups.length > 0) {
                 emailgroups = emailgroups + "," + emails.split(';');
                 emailList = emailgroups.split(',');
             }
             else if (emailgroups.length == 0 && emails.length > 0) {
                 emailgroups = emails.split(';');
                 emailList = emailgroups;
             }
             else
                 emailList = emailgroups.split(',');

             $('#dialog-share-book input[name="emailto"]').css('border', '1px solid black');

             var nameToList = "";
             if (nameto.length > 0) {
                 var isValid = /^[a-zA-Z\;\s]+$/.test(nameto);
                 if (isValid)
                     nameToList = nameto.split(';');
                 else {
                     alert("Please enter valid ToName separated by ';' !")
                     $('#dialog-share-book input[name="nameto"]').css('border', '1px solid red');
                     hideLoader();
                     return false;
                 }
             }
             if (emailList.length != nameToList.length) {
                 alert("Number of Emails should match with number of ToNames!");
                 $('#dialog-share-book input[name="nameto"]').css('border', '1px solid red');
                 hideLoader();
                 return false;
             }

             $('#dialog-share-book input[name="emailto"]').css('border', '1px solid black');
             $('#dialog-share-book input[name="nameto"]').css('border', '1px solid black');

             var content = $('#dialog-share-book textarea[name="content"]').val();
             var Isvalid = false;
             var checkboxes = $("#groupList").find("input[type=checkbox]");
             var groups = "";
             if (checkboxes.length > 0) {
                 for (var i = 1; i < checkboxes.length; i++) {
                     if ($(checkboxes[i]).prop("checked"))
                         groups += $(checkboxes[i]).val().trim() + ", ";
                 }
                 groups = groups.substr(0, groups.lastIndexOf(','));
             }

             var emailto = [];
             if (groups.length > 0) {

                 $.ajax(
                  {
                      type: "POST",
                      url: '../FlexWebService.asmx/GetEmails',
                      contentType: "application/json; charset=utf-8",
                      data: '{"GroupID":"' + groups + '"}',
                      dataType: "json",
                      success: function (result) {

                          if (result.d.length > 0) {
                              var emails = JSON.parse(result.d);
                              if (emails.length > 0) {
                                  var toEmail = JSON.parse(emails);
                                  for (var i = 0; i < toEmail.length; i++) {
                                      emailto.push({ "Email": toEmail[i].Email, "ToName": toEmail[i].ToName });
                                  }


                                  SendMail(emailList, docId, fileName, emailto, nameto, content, pathForSharemail, nameToList, email, title);
                              }
                          }

                      },
                      fail: function (data) {
                          alert("Failed to share document. Please try again!");
                          hideLoader();
                      }
                  });
             }
             else {
                 if (emailList.length <= 0) {
                     alert("Please select Email Group or enter Email to share this document!");
                     hideLoader();
                     return false;

                 }
                 SendMail(emailList, docId, fileName, emailto, nameto, content, pathForSharemail, nameToList, email, title);
             }


         }

         function checkAll(objRef) {
             var GridView = objRef.parentNode.parentNode.parentNode;

             var inputList = GridView.getElementsByTagName("input");

             for (var i = 0; i < inputList.length; i++) {

                 //Get the Cell To find out ColumnIndex

                 var row = inputList[i].parentNode.parentNode;

                 if (inputList[i].type == "checkbox" && objRef != inputList[i]) {

                     if (objRef.checked) {
                         inputList[i].checked = true;
                     }

                     else {
                         inputList[i].checked = false;
                     }

                 }

             }

         }

         function Check_Click(objRef) {
             //Get the Row based on checkbox            
             var row = objRef.parentNode.parentNode.parentNode;
             //Get the reference of GridView

             var GridView = row.parentNode;

             //Get all input elements in Gridview

             var inputList = GridView.getElementsByTagName("input");

             for (var i = 0; i < inputList.length; i++) {

                 //The First element is the Header Checkbox

                 var headerCheckBox = inputList[0];

                 //Based on all or none checkboxes

                 //are checked check/uncheck Header Checkbox

                 var checked = true;

                 if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {

                     if (!inputList[i].checked) {

                         checked = false;
                         break;
                     }
                 }
             }
             headerCheckBox.checked = checked;
         }

         function CkeckCheckbox(e) {
             //var e = $(this);
             if ($(this).prop("checked") == true) {
                 $(this).prop("checked", false)
             }
             else
                 $(this).prop("checked", true);

         }

         function showCheckboxesPublicHolidays() {
             var checkboxes = document.getElementById("groupList");
             if (!pubExpanded) {
                 checkboxes.style.display = "block";
                 pubExpanded = true;
             } else {
                 checkboxes.style.display = "none";
                 pubExpanded = false;
             }
             return false;
         }

         function showCheckboxes() {
             var checkboxes = document.getElementById("mailList");
             if (!pubExpanded) {
                 checkboxes.style.display = "block";
                 pubExpanded = true;
             } else {
                 checkboxes.style.display = "none";
                 pubExpanded = false;
             }
             return false;
         }


    </script>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>EdForms</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <uc3:ucRequestFiles ID="ucRequestFiles" runat="server" />
    <asp:Button ID="Button1ShowPopup2" runat="server" Style="display: none" />
    <%-- <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BehaviorID="pnlPopup2"
        PopupControlID="pnlPopup2" DropShadow="true" BackgroundCssClass="modalBackground"
        Enabled="True" TargetControlID="Button1ShowPopup2" />--%>
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server"
        PopupControlID="pnlPopup2" DropShadow="true" BackgroundCssClass="modalBackground"
        Enabled="True" TargetControlID="Button1ShowPopup2" />
    <asp:Panel ID="pnlPopup2" Style="display: none"
        runat="server" class="popup-mainbox">
        <div>
            <div class="popup-head" id="Div1">
                Message
            </div>
            <div class="popup-scroller">
                <p>
                    Your document has been uploaded successfully...It will be converting to the edFile format in our server over the next few minutes, depending on the size of the uploaded document. You can now select additional files to upload or navigate away from this web page. 
                </p>
            </div>
            <div class="popup-btn">
                <asp:Button ID="ShowPopup2Button2" runat="server" OnClientClick="return onClientButtonClick()"
                    Text="Close" class="btn green" />
                <%--<input id="btnuploadclose" type="button" value="Close"  />--%>
            </div>
        </div>
    </asp:Panel>
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>
    <div class="popup-mainbox preview-popup" id="preview_popup" style="display: none">
        <div class="popupcontrols" style="width: 100%; height: 5%;">
            <span id="popupclose" class="ic-icon ic-close"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%;">
            <iframe id="reviewContent" style="width: 100%; height: 100%;"></iframe>
        </div>
    </div>

    <div class="inner-wrapper">
        <div class="page-container">
            <div class="dashboard-container">
                <div class="left-content">
                    <div>
                        <asp:Panel ID="panelMain" runat="server" Visible="true">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>

                                    <%--<fieldset id="uploadFieldset" style="width: 650px; font: 20px bold; font-weight:bold">--%>
                                    <div id="uploadProgress" class="white_content" style="font-size: 14px;"></div>

                                    <fieldset id="uploadFieldset" style="width: 650px; font-size: 20px; font-family: 'Segoe UI'; font-weight: normal">
                                        <%--Your Fillable Forms--%>
                                        <asp:LinkButton ID="lnkBtnShare" OnClientClick="OnShowShare();" runat="server" CssClass="ic-icon ic-share shareDocument" CausesValidation="False" Height="28px" Width="28px" ToolTip="Share File" Style="float: right; margin-top: 2px;">
                                        </asp:LinkButton>

                                        <div id="boxToMail" runat="server" class="selectBox-outer" style="width: 315px; display: inline-block; float: right">
                                            <div class="selectBox " onclick="showCheckboxesPublicHolidays()">
                                                <select id="drpGroups" data-placeholder="Select Group to send Mail">
                                                    <option>Select Group to send Mail</option>
                                                </select>
                                                <div class="overSelect"></div>
                                            </div>
                                            <div id="groupList" style="display: none;"></div>
                                        </div>

                                        <div id="divUpload">
                                            <div class='clearfix'></div>
                                            <asp:Image runat="server" ID="Image12" ImageUrl="../Images/loader_bg.gif" ImageAlign="Middle" />
                                            <asp:Label ID="label1" runat="server"></asp:Label>
                                            <asp:Label runat="server" ID="Label2" Style="display: none;">
                                <img align="absmiddle" alt="" src="uploading.gif"/></asp:Label>


                                            <ajaxtoolkit:ajaxfileupload id="ajaxfileuploadeFileFlow" runat="server" padding-bottom="4"
                                                padding-left="2" padding-right="1" padding-top="4" throbberid="mythrobber"
                                                maximumnumberoffiles="10"
                                                allowedfiletypes="pdf"
                                                azurecontainername=""
                                                onuploadcomplete="AjaxfileuploadeFileFlow_onuploadcomplete1"
                                                onclientuploadcomplete="onClientUploadComplete"
                                                onuploadcompleteall="AjaxfileuploadeFileFlow_uploadcompleteall1"
                                                onuploadstart="AjaxfileuploadeFileFlow_uploadstart1"
                                                onclientuploadstart="onClientUploadStart"
                                                onclientuploadcompleteall="onClientUploadCompleteAll"
                                                contextkeys="2" xmlns:ajaxtoolkit="ajaxcontroltoolkit"
                                                cssclass="boxWidth" />
                                            <div class='clearfix'></div>
                                            <div id="uploadCompleteInfo">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <br />
                                    <br />

                                    <%--GridView--%>
                                    <div class="form_title_2">
                                        <asp:Image runat="server"
                                            ImageUrl="../Images/loader_bg.gif" ID="lbsStatus" ImageAlign="Middle" />
                                    </div>
                                    <div class='clearfix'></div>
                                    <asp:GridView ID="grdeFileFlow" runat="server" AllowSorting="True" DataKeyNames="eFileFlowId"
                                        AutoGenerateColumns="false" BorderWidth="2" OnRowDataBound="GrideFileFlow_RowDataBound"
                                        CssClass="datatable listing-datatable boxWidth" OnSorting="grdeFileFlow_Sorting">
                                        <PagerSettings Visible="False" />
                                        <AlternatingRowStyle CssClass="odd" />
                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                        <Columns>

                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                                ItemStyle-CssClass="table_tekst_edit">
                                                <HeaderTemplate>
                                                    <asp:CheckBox Text="" ID="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                    <asp:HiddenField runat="server" ID="hdnValues" Value='<%#  Convert.ToInt32(Eval("eFileFlowId")) %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <%--convert label into textbox--%>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" ItemStyle-Wrap="false" SortExpression="eFileFlowId">
                                                <HeaderTemplate>
                                                    <%--<asp:Label ID="lblDocumentName" runat="server" Text="Document Name" />--%>
                                                    <asp:LinkButton ID="lblDocumentName1" runat="server" Text="Form Name" CommandName="Sort" CommandArgument="eFileFlowId" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("Name")%>' name="DocumentName" CssClass="lblDocumentName" />
                                                    <asp:TextBox runat="server" MaxLength="50" Text='<%#Eval("Name")%>'
                                                        ID="TextDocumentName" Style="display: none; height: 30px;" name="txtDocumentName" CssClass="txtDocumentName" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--end--%>

                                            <%--<asp:BoundField DataField="Document" HtmlEncode="false" SortExpression="Document"/>--%>
                                            <asp:BoundField DataField="Size" HeaderText="Size(Mb)" DataFormatString="{0:F2}" ItemStyle-Width="15%"
                                                HtmlEncode="false" SortExpression="Size" />
                                            <asp:BoundField DataField="Date" HeaderText="Created Date" DataFormatString="{0:MM.dd.yyyy}" ItemStyle-Width="15%"
                                                SortExpression="Date" />

                                            <%--rename start--%>
                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="20%"
                                                ItemStyle-CssClass="table_tekst_edit actionMinWidth">
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblAction" runat="server" Text="Action" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkBtnEdit" runat="server" Text="" CommandArgument='<%# Eval("FilePath")%>'
                                                        CssClass="ic-icon ic-edit editDocument" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkBtnUpdate" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                        ToolTip="Update" CssClass="ic-icon ic-save saveDocument" Style="display: none" CommandArgument='<%# Eval("FilePath")%>' name="lnkUpdate"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkBtnCancel" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancelDocument" Style="display: none" name="lnkcancel"></asp:LinkButton>

                                                    <asp:LinkButton ID="lnkBtnDelete" runat="server" CssClass="ic-icon ic-delete delete" OnCommand="LinkButtonDelete_Click" ToolTip="Delete File"
                                                        CommandArgument='<%# Eval("FilePath")%>'></asp:LinkButton>
                                                    <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="lnkBtnDelete"
                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                    </ajaxToolkit:ModalPopupExtender>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                        ConfirmText="" TargetControlID="lnkBtnDelete">
                                                    </ajaxToolkit:ConfirmButtonExtender>

                                                    <%--     <asp:LinkButton ID="lnkBtnShare" runat="server" CssClass="ic-icon ic-share shareDocument" CausesValidation="False" Height="28px" Width="28px" ToolTip="Share File">
                                                    </asp:LinkButton>--%>

                                                    <asp:LinkButton ID="lnkbtnPreview" runat="server" CssClass="ic-icon ic-preview lnkbtnPreview" CausesValidation="False"
                                                        CommandName=""></asp:LinkButton>
                                                    <asp:HyperLink ID="lnkBtnDownload" runat="server" CausesValidation="false" CssClass="ic-icon ic-download downloadFile" Target="_blank" />

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--rename end--%>
                                        </Columns>
                                        <%--<EmptyDataTemplate>
                                            <table border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                <tr>
                                                    <td align="left" class="podnaslov">
                                                        <asp:Label ID="Label120" runat="server" />There are no files. right now.
                                                    </td>
                                                </tr>
                                            </table>
                                        </EmptyDataTemplate>--%>
                                    </asp:GridView>

                                    <div class="prevnext" bgcolor="#f8f8f5" style="float: right; padding: 5px;">
                                        <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                            PageSize="5" CssClass="paging_position " PagerStyle="NumericPages" ControlToPaginate="grdeFileFlow"></cc1:WebPager>
                                        <asp:HiddenField ID="FolderID" runat="server" />
                                    </div>
                                    <br />
                                    <br />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>

                        <%--Delete File--%>
                        <asp:Panel class="popup-mainbox" ID="DivDeleteConfirmation" Style="display: none"
                            runat="server">
                            <div>
                                <div class="popup-head" id="PopupHeader">
                                    Delete File
                                </div>
                                <div class="popup-scroller">
                                    <p>
                                        Caution! Are you sure you want to delete this file. Once you delete this file, it
                                                        will be deleted for good
                                    </p>
                                    <p>
                                        Confirm Password:
                                       <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                                    </p>
                                </div>
                                <div class="popup-btn">
                                    <input id="ButtonDeleleOkay" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />
                                    <input id="ButtonDeleteCancel" type="button" value="No" class="btn green" />
                                </div>
                            </div>
                        </asp:Panel>
                        <%--end Delete--%>

                        <%--Share Document--%>
                        <div id="dialog-share-book" title="Share this eFile" style="display: none">
                            <p class="validateTips">All form fields are required.</p>
                            <br />
                            <label for="email">From Email</label><br />
                            <input type="email" name="email" id="email" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
                            <label for="emailto">To Email</label><br />
                            <input type="email" name="emailto" id="emailto" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" placeholder="Enter Email separated with';'" /><br />
                            <label for="emailto">To Email Group</label><br />
                            <div class="selectBox-outer ui-widget-content ui-corner-all" style="width: 55%; margin-bottom: 12px">
                                <div class="selectBox">
                                    <select id="comboBoxFolder" multiple class="text ui-widget-content ui-corner-all" style="border: 1px solid black">
                                    </select>
                                    <button type="button" class="chosen-toggle select create-btn btn-small green" style="margin-left: 105%; position: absolute; bottom: 0; height: 30px; width: 65px;">
                                        Select all
                                    </button>
                                    <button type="button" class="chosen-toggle deselect create-btn btn-small green" style="margin-left: 137%; position: absolute; bottom: 0; height: 30px; width: 81px;">Deselect all</button>
                                    <%--<asp:Button runat="server" ID="collapse" CssClass="collapseFolder" OnClientClick="collapseFolder();return false;"></asp:Button>--%>
                                    <%--<div class="overSelect"></div>--%>
                                </div>
                                <div id="folderList" style="display: none; margin-top: -1px; border-radius: 0 0 4px 4px; background-clip: padding-box"></div>
                                <asp:HiddenField runat="server" ID="hdnFolderID" />
                            </div>
                            <label for="nameto">To Name</label><br />
                            <input type="text" name="nameto" id="nameto" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" placeholder="Enter Name separated with';'" /><br />
                            <label for="title">Subject</label><br />
                            <input type="text" name="title" id="title" value="EdFile Share" readonly="readonly" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
                            <label for="content">Message</label><br />
                            <textarea name="content" id="content" rows="2" cols="25" class="text ui-widget-content ui-corner-all" style="border: 1px solid black"></textarea><br />
                            <label for="content">Link Expiration Time (In Hrs)</label><br />
                            <input type="text" name="linkexpiration" id="expirationTime" class="text ui-widget-content ui-corner-all" style="border: 1px solid black; width: 80%;" />
                            <input type="button" id="updateExpiration" onclick="UpdateExpirationTime()" class="btn-small green" style="float: right" value="UPDATE" /><br />
                            <span id="errorMsg"></span>
                        </div>
                        <div id="dialog-message-all" title="" style="display: none;">
                            <p id="dialog-message-content">
                                <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span>
                                <span id="spanText"></span>
                            </p>
                        </div>
                        <%--End Share Document--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

