﻿using System;
using System.ComponentModel;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;

using System.Linq;
using DataQuicker2.Framework;
using Shinetech.DAL;
using Shinetech.Utility;
using BarcodeLib;
using Shinetech.Framework.Controls;
using AjaxControlToolkit;
using Shinetech.Engines;
using System.Web.Script.Serialization;
using System.Xml;
using System.Security.AccessControl;
using System.IO.Compression;
using System.Text;
using System.Collections.Generic;
using System.Threading;
using System.Web.Services;
using System.Web.Services.Protocols;

using System.Web.Script.Services;

//using DnnSun.WebControls.Toolkit;

using Shinetech.Engines.Adapters;
using Newtonsoft.Json;

public partial class Office_eFileFlow : System.Web.UI.Page
{
    public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    public string virtualDir = ConfigurationManager.AppSettings["VirtualDir"];
    public string SortType = "DESC";
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (Sessions.HasViewPrivilegeOnly)
            {
                //ajaxfileuploadeFileFlow.Visible = false;
                lnkBtnShare.Visible = false;
                boxToMail.Style.Add("display", "none");
            }
            var argument = Request.Form["__EVENTARGUMENT"];
            if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
            {
                GetData();
                BindGrid();
            }
            argument = "";
            //ajaxfileuploadeFileFlow.Enabled = true;
            if (!Page.IsPostBack)
            {
                string uid = Sessions.SwitchedSessionId;
                GetData();
                BindGrid();
            }
            //if (!ajaxfileuploadeFileFlow.IsInFileUploadPostBack)
            {
                GetData();
                BindGrid();
            }
        }
        else
        {
            //if (ajaxfileuploadeFileFlow.IsInFileUploadPostBack)
            //{
            //    // do for ajax file upload partial postback request
            //}
            //else
            //{
                var argument = Request.Form["__EVENTARGUMENT"];
                if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
                {
                    GetData();
                    BindGrid();
                }
            //}
        }
    }
    public DataTable DataSource
    {
        get
        {
            return this.Session["Files_Workarea"] as DataTable;
        }
        set
        {
            this.Session["Files_Workarea"] = value;
        }
    }


    public string DocumentID
    {
        get
        {
            return (string)Session["DocumentID"];
        }
        set
        {
            Session["DocumentID"] = value;

        }
    }

    public string DocumentName
    {
        get
        {
            return (string)Session["DocumentName"];
        }
        set
        {
            Session["DocumentName"] = value;

        }
    }

    public DataTable DividerSource
    {
        get
        {
            return this.Session["DividerSource_FolderBox"] as DataTable;
        }
        set
        {
            this.Session["DividerSource_FolderBox"] = value;
        }
    }

    public string DateTimeInfo
    {
        get
        {
            DateTimeFormatInfo myDTFI = new CultureInfo("en-US", false).DateTimeFormat;
            DateTime dt = DateTime.Now;
            return dt.ToString("MM/dd/yyy", myDTFI);

        }
    }

    public string MessageContent
    {
        get
        {
            return this.Session["_Main_Message"] as string;
        }
        set
        {
            this.Session["_Main_Message"] = value;
        }
    }

    public string SideMessage
    {
        get
        {
            return this.Session["_Side_Message"] as string;
        }

        set
        {
            this.Session["_Side_Message"] = value;
        }
    }

    public string FolderName
    {
        get
        {
            return string.Format("~/{0}", CurrentVolume);
        }

    }

    public string WorkAreaName
    {
        get
        {
            return "~/WorkArea";
        }

    }


    #region FileUpload
    protected void AjaxfileuploadeFileFlow_onuploadcomplete1(object sender, AjaxFileUploadEventArgs file)
    {
        try
        {
            string path = Server.MapPath("~/" + Common_Tatva.eFileFlow);
            string uid = Sessions.SwitchedRackspaceId;

            string fullPath = Path.Combine(path, Sessions.SwitchedRackspaceId);

            if (ajaxfileuploadeFileFlow.IsInFileUploadPostBack)
            {
                var fullname = Path.Combine(fullPath, file.FileName);
                string fileName = Path.GetFileName(fullname);
                decimal size;

                size = Math.Round((decimal)(file.FileSize / 1024) / 1024, 2);
                Guid guid = new Guid(Sessions.SwitchedRackspaceId);


                RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                string errorMsg = string.Empty;
                string newFileName1 = rackSpaceFileUpload.UploadObject(ref errorMsg, file.FileName, Enum_Tatva.Folders.EFileFlow, file.GetStreamContents());
                if (string.IsNullOrEmpty(errorMsg))
                {
                    string eFileFlowId = General_Class.DocumentsInsertForEdForms(newFileName1.Substring(newFileName1.LastIndexOf('/') + 1), guid, size, Enum_Tatva.IsDelete.No.GetHashCode());
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Upload.GetHashCode(), 0, 0, Convert.ToInt32(eFileFlowId));
                }
                else
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Failed to upload file. Please try again after sometime." + "\");", true);

                GetData();
                BindGrid();
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void AjaxfileuploadeFileFlow_uploadcompleteall1(object sender, AjaxFileUploadCompleteAllEventArgs e)
    {
        var startedAt = (DateTime)Session["uploadTime"];
        var now = DateTime.Now;
        e.ServerArguments = new JavaScriptSerializer()
            .Serialize(new
            {
                duration = (now - startedAt).Seconds,
                time = DateTime.Now.ToShortTimeString()
            });

    }

    protected void AjaxfileuploadeFileFlow_uploadstart1(object sender, AjaxFileUploadStartEventArgs e)
    {
        var now = DateTime.Now;
        e.ServerArguments = now.ToShortTimeString();
        Session["uploadTime"] = now;
    }

    #endregion

    #region Grid
    protected void GrideFileFlow_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            if (Sessions.HasViewPrivilegeOnly)
            {
                e.Row.Cells[0].Visible = false;
            }
        }
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (Sessions.HasViewPrivilegeOnly) 
            {
                e.Row.Cells[0].Visible = false;
                HyperLink libDownload = e.Row.FindControl("lnkBtnDownload") as HyperLink;
                libDownload.Visible = false;
                LinkButton libDel = e.Row.FindControl("lnkBtnDelete") as LinkButton;
                libDel.Visible = false;
                LinkButton libEdit = e.Row.FindControl("lnkBtnEdit") as LinkButton;
                libEdit.Visible = false;
            }
            System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
            string path = drv.Row["FilePath"].ToString();
            string eFileFlowId = drv.Row["eFileFlowId"].ToString();
            string fileName = drv.Row["Name"].ToString();

            path = path.Replace(@"\", "/");

            TextBox txtDocumentName = (e.Row.FindControl("TextDocumentName") as TextBox);

            LinkButton lnkBtnUpdate = (e.Row.FindControl("lnkBtnUpdate") as LinkButton);
            if (lnkBtnUpdate != null)
            {
                lnkBtnUpdate.Attributes.Add("onclick", "OnSaveRename('" + txtDocumentName.ClientID + "','" + path + "','" + eFileFlowId + "','" + fileName + "');");
            }


            LinkButton lnkbtnPreview = (e.Row.FindControl("lnkbtnPreview") as LinkButton);
            if (lnkbtnPreview != null)
            {
                lnkbtnPreview.Attributes.Add("onclick", "ShowPreviewForEFileFlow('" + int.Parse(eFileFlowId) + "','" + fileName + "');");
            }

            HyperLink lnkBtnDownload = (e.Row.FindControl("lnkBtnDownload") as HyperLink);
            if (lnkBtnDownload != null)
            {
                lnkBtnDownload.Attributes.Add("href", "PdfViewer.aspx?ID=" + eFileFlowId + "&data=" + QueryString.QueryStringEncode("ID=" + fileName + "&folderID=" + (int)Enum_Tatva.Folders.EFileFlow + "&sessionID=" + Sessions.SwitchedRackspaceId));
            }
        }
    }


    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
        UpdatePanel1.Update();
    }

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }
    private void GetData()
    {
        string uid = Sessions.SwitchedRackspaceId;

        Guid guid = new Guid(uid);

        this.DataSource = General_Class.GetEdForms(guid);
        this.DataSource.Columns.Add("FilePath", typeof(string));
        if (this.DataSource != null && this.DataSource.Rows.Count > 0)
        {
            foreach (DataRow row in this.DataSource.Rows)
            {
                string path = Server.MapPath("~/" + Common_Tatva.eFileFlow);
                string tempPath = string.Concat(path, Path.DirectorySeparatorChar, uid);
                if (Directory.Exists(tempPath))
                {
                    string[] files = Directory.GetFiles(tempPath);
                    foreach (string file in files)
                    {
                        if (file.Contains(row["Name"].ToString()))
                            row["FilePath"] = file;
                    }
                }
            }
        }
        DataView dvSource = this.DataSource.AsDataView();
        dvSource.Sort = "Date" + " " + SortType;
        this.DataSource = dvSource.ToTable();
        ViewState["SortWorkAreaDirection"] = SortType;
        ViewState["SortWorkAreaExpression"] = "Date";
    }

    #endregion

    #region DeleteFile
    protected void LinkButtonDelete_Click(Object sender, CommandEventArgs e)
    {
        try
        {
            string password = textPassword.Text.Trim();
            string strPassword = PasswordGenerator.GetMD5(password);
            string errorMsg = string.Empty;

            if (UserManagement.GetPassword(User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
            {
                //show the password is wrong        
                ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssYes", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);
            }
            else
            {
                try
                {
                    LinkButton lnkbtnDelete = sender as LinkButton;
                    GridViewRow gvrow = lnkbtnDelete.NamingContainer as GridViewRow;
                    int eFileFlowid = Convert.ToInt32(grdeFileFlow.DataKeys[gvrow.RowIndex].Value.ToString());

                    DataTable dtFiles = General_Class.GetEdFormsById(eFileFlowid);
                    dtFiles.AsEnumerable().ToList().ForEach(d =>
                    {
                        var file = d.Field<string>("Name");
                        int id = d.Field<int>("eFileFlowid");

                        RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                        rackSpaceFileUpload.DeleteObject(ref errorMsg, rackSpaceFileUpload.GeneratePath(file.ToLower(), Enum_Tatva.Folders.EFileFlow));
                        if (string.IsNullOrEmpty(errorMsg))
                        {
                            General_Class.UpdateEdForms(file, eFileFlowid, Enum_Tatva.IsDelete.Yes.GetHashCode(), 0);
                            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Delete.GetHashCode(), 0, 0, id);
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "File " + file + " has been deleted successfully" + "\");", true);
                        }
                        else
                            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Failed to delete file: " + file + ". Please try again after sometime." + "\");", true);
                        GetData();
                        BindGrid();
                    });

                }
                catch (Exception exp) { Console.WriteLine(exp.Message); }

            }
        }
        catch (Exception ex)
        { }
        finally
        {
            ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
        }
        
    }
    #endregion

    protected void grdeFileFlow_Sorting(object sender, GridViewSortEventArgs e)
    {
        //SetSortDirection(e.SortDirection.ToString());
        if (this.DataSource != null)
        {
            //Sort the data.
            this.DataSource.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            BindGrid();
        }
    }

    private string GetSortDirection(string column)
    {

        // Retrieve the last column that was sorted.
        string sortExpression = ViewState["SortWorkAreaExpression"] as string;

        if (sortExpression != null)
        {
            // Check if the same column is being sorted.
            // Otherwise, the default value can be returned.
            if (sortExpression == column)
            {
                string lastDirection = ViewState["SortWorkAreaDirection"] as string;
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    SortType = "DESC";
                }
                else if ((lastDirection != null) && (lastDirection == "DESC"))
                {
                    SortType = "ASC";
                }
            }
        }

        // Save new values in ViewState.
        ViewState["SortWorkAreaDirection"] = SortType;
        ViewState["SortWorkAreaExpression"] = column;

        return SortType;
    }

    [WebMethod]
    public static string GetPreviewURL(int documentID, int folderID, string shareID)
    {
        RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
        string errorMsg = string.Empty;
        string path = rackSpaceFileUpload.GeneratePath(shareID.ToLower(), Enum_Tatva.Folders.EFileFlow);
        string newFileName1 = rackSpaceFileUpload.GetObject(ref errorMsg, path);
        EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Preview.GetHashCode(), 0, 0, documentID);
        if (string.IsNullOrEmpty(errorMsg))
            return Common_Tatva.RackSpaceWorkareaDownload + "/" + Sessions.SwitchedRackspaceId + "/" + newFileName1;
        else
            return string.Empty;
    }

    /// <summary>
    /// Get all mail Groups
    /// </summary>
    /// <param name="officeId"></param>
    /// <returns></returns>
    [WebMethod]
    public static string GetMailToSend(string officeId)
    {
        try
        {
            return JsonConvert.SerializeObject(General_Class.GetEmailGroupEfileflow(officeId));
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(string.Empty, string.Empty, string.Empty, ex);
            return string.Empty;
        }
    }

    [WebMethod]
    public static string GetPreviewURLForInbox(int documentID, int folderID, string shareID)
    {
        Office_UserControls_ucRequestFiles ucRequestFiles = new Office_UserControls_ucRequestFiles();
        EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
        if (documentID != 0)
            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Preview.GetHashCode(), 0, 0, 0, 0, null, documentID);
        else
            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Preview.GetHashCode(), 0, 0, 0, 0, shareID, 0);
        return ucRequestFiles.GetURL(folderID, shareID, Sessions.SwitchedRackspaceId, documentID);
    }
}
