<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="eFileFolders.aspx.cs" Inherits="Office_eFileFolders" Title="" %>

<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.ComponentModel" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css?v=1" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=3" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/edFiles/EdFilesLogs.js?v=4" type="text/javascript"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>extras/jquery-ui-1.8.22.custom.min.js"></script>

    <style>
        .cp_button {
            background-image: url(../../images/cp_button.png);
            width: 16px !important;
            height: 16px;
            border: none !important;
            background-repeat: no-repeat;
            /* margin-top: 14px; */
            margin: 10px;
        }

        td span {
            -ms-word-break: break-all;
            word-break: break-all;
            word-break: break-word;
            -webkit-hyphens: auto;
            -moz-hyphens: auto;
            hyphens: auto;
            max-width: 300px;
            white-space: normal !important;
        }

        body {
            font-family: "Open Sans", sans-serif;
        }

        .datatable td {
            font-family: "Open Sans", sans-serif;
        }

        .backBtn {
            width: 32px;
            height: 32px;
            border: none;
            cursor: pointer;
            background: url('../images/left-arrow (2).png') no-repeat center center;
        }

            .backBtn:hover {
                width: 32px;
                height: 32px;
                border: none;
                background: url('../images/left-arrow (4).png') no-repeat center center;
            }

        td {
            max-width: 300px;
        }

        .actionMinWidth {
            min-width: 220px;
        }

        .logFormPreveiw {
            background: url(../images/file-view.png) no-repeat center center;
            cursor: pointer;
        }

        .checkForGroup {
            float: left;
            height: 15px;
            width: 15px;
        }
        /*.textForGroup
        {
            width:80%;
        }*/
        .ui-dialog {
            top: 300px !important;
        }

        #overlay {
            display: none;
            position: fixed;
            top: 0;
            bottom: 0;
            background: rgba(0,0,0,.8);
            width: 100%;
            height: 100%;
            z-index: 99999;
        }
    </style>
    <script language="javascript" type="text/javascript">

        //var resizeTimer;
        //function resizeColorBox() {
        //    if (resizeTimer) clearTimeout(resizeTimer);
        //    resizeTimer = setTimeout(function () {
        //        if ($('#cboxOverlay').is(':visible')) {
        //            $.colorbox.load(true);
        //        }
        //    }, 300)
        //}

        //// Resize Colorbox when resizing window or changing mobile device orientation
        //jQuery(window).resize(resizeColorBox);
        //window.addEventListener("orientationchange", resizeColorBox, false);
        var fileName, pathForSharemail;
        $(document).ready(function () {

            var array = [];
            $('.grid-outer').find('a').not('.developer-data').each(function () {
                setTimeout(function () {
                    window.open($(this).attr("href"), '_blank');
                }, 300);
                //array.push($(this).attr("href"));
            })

            array.forEach(function (e) {
                window.open(e, '_blank');
            });

            $('#dialog-share-book').dialog({
                autoOpen: false,
                width: 450,
                //height: 540,
                resizable: false,
                modal: true,
                buttons: {
                    "Share Now": function () {
                        SaveMail();
                    },
                    "Close": function () {
                        $(this).dialog("close");
                    }
                },
                close: function () {
                }
            });
            $('.ui-button-text').each(function (i) {
                $(this).html($(this).parent().attr('text'))
            })

            $("#dialog-message-all").dialog({
                autoOpen: false,
                resizable: false,
                modal: true,
                Ok: function () {
                    $(this).dialog("close");
                },
                close: function (event, ui) {
                    $(this).dialog("option", 'title', '');
                    $('#dialog-confirm-content').empty();
                }

            });
            $(window).scroll(function () {
                $('#overlay:visible') && $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
            });
        });

        function pageLoad() {
            $('.parentCheckbox').click(function () {
                $('input:checkbox').not('.parentCheckbox').prop("checked", $('.parentCheckbox').prop("checked"));
            });
            $(".edFileLink").click(function () {
                var folderId = $(this).attr("href").split("?id=")[1];
                CreateDocumentsAndFolderLogs(folderId, null, null, null, null, ActionEnum.View, null);
            });
        }



        $(function () {
            var dWidth = screen.width;
            var dHeight = screen.height;
            //var aWidth = Math.floor((80 * screen.width) / 100);
            var aWidth = Math.floor((95 * screen.width) / 100);
            var aHeight = Math.floor((57 * aWidth) / 100);

            var fWidth, fHeight;
            if (dWidth > dHeight) {
                //alert(1);
                fWidth = "80%";
                fHeight = "95%";
            } else {
                //alert(0);
                fWidth = aWidth;
                fHeight = aWidth;
            }


            $(".iframe").colorbox({
                //iframe: true, width: "80%", height: "95%", onComplete: function () {
                iframe: true, width: fWidth, height: fHeight, onComplete: function () {
                    $('iframe').live('load', function () {
                        $('iframe').contents().find("head")
                            .append($("<style type='text/css'>  .splash{overflow:visible;}  </style>"));
                    });
                }
            });
        });

        function DeleteTabNotAllowed() {
            alert('For security, tabs cannot be deleted. Please contact EdFiles if you would to delete tabs !!');
            return false;
        }

        function callPopUp(id) {
            $('.iframe').attr('href', 'WebFlashViewer.aspx?V=2&ID=' + id).trigger('click');
            //$('.iframe').attr('href', 'http://wiki.com').trigger('click');
        }
        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');;
        }
        function hideLoader() {
            $('#overlay').hide();
        }

        //function InsertEdFileLogsAndRedirct(folderId) {

        //    window.location.href = 'FileFolderBox.aspx?id=' + folderId + '';
        //    CreateDocumentsAndFolderLogs(folderId, null, null, null, null, ActionEnum.View, null);
        //}

        function InsertViewEdFilesAsBookLog(folderId) {
            window.open("WebFlashViewer.aspx?V=2&ID=" + folderId + "", '_blank', "toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no");
            CreateDocumentsAndFolderLogs(folderId, null, null, null, null, ActionEnum.BookView, null);
        }

        var folderId = 0;
        function SaveEdFilesLog() {

            CreateDocumentsAndFolderLogs(folderId, null, null, null, null, ActionEnum.Edit, null);
        }

        function SetFolderID(a) {
            folderId = $(a).siblings().first().text();
        }

        function openwindow(id) {
            var url = 'EFFDownloader.aspx?ID=' + id;
            var name = 'DownEFF';
            var iWidth = 250;
            var iHeight = 200;
            var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
            var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
            window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',toolbar=no,menubar=no,scrollbars=no,resizeable=no,location=no,status=no');
        }

        function ShowPreviewForRequestedFiles(LogFormId, FolderId) {

            window.open(window.location.origin + "/ViewerTemp.aspx?id=" + LogFormId, '_blank');
            CreateDocumentsAndFolderLogs(FolderId, null, null, LogFormId, null, ActionEnum.Preview, null);
            return false;
        }


        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;

            var inputList = GridView.getElementsByTagName("input");

            for (var i = 0; i < inputList.length; i++) {

                //Get the Cell To find out ColumnIndex

                var row = inputList[i].parentNode.parentNode;

                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {

                    if (objRef.checked) {
                        inputList[i].checked = true;
                    }

                    else {
                        inputList[i].checked = false;
                    }

                }

            }

        }

        function Check_Click(objRef) {
            //Get the Row based on checkbox            
            var row = objRef.parentNode.parentNode.parentNode;
            //Get the reference of GridView

            var GridView = row.parentNode;

            //Get all input elements in Gridview

            var inputList = GridView.getElementsByTagName("input");

            for (var i = 0; i < inputList.length; i++) {

                //The First element is the Header Checkbox

                var headerCheckBox = inputList[0];

                //Based on all or none checkboxes

                //are checked check/uncheck Header Checkbox

                var checked = true;

                if (inputList[i].type == "checkbox" && inputList[i] != headerCheckBox) {

                    if (!inputList[i].checked) {

                        checked = false;
                        break;
                    }
                }
            }
            headerCheckBox.checked = checked;
        }

        function CloseModal() {
            $('#dialog-share-book').dialog("close");
        }

        function SaveMail() {
            showLoader();
            ids = "";
            if ($('table input[type="checkbox"]:checked').length > 0) {
                $('table input[type="checkbox"]:checked').each(function () {
                    if ($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]") && $(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val() && JSON.parse($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()))
                        ids += JSON.parse($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val()) + ",";
                });
                ids = ids.substring(0, ids.length - 1);
                docId = ids;
            }
            else if ($('#<%= hdnFolderId.ClientID %>').val() != "" && $('#<%= hdnFolderId.ClientID %>').val() != undefined) {
                docId = $('#<%= hdnFolderId.ClientID %>').val();
            }
            else {
                alert("Please Select Atleast one document.");
                hideLoader();
                return false;
            }

            var nameto = $('#dialog-share-book input[name="nameto"]').val();
            var emails = $('#dialog-share-book input[name="emailto"]').val();
            var email = $('#dialog-share-book input[name="email"]').val();
            var title = $('#dialog-share-book input[name="title"]').val();
            var emailList = "";
            if (emails.length > 0) {
                emailList = emails.split(';');
                for (var i = 0; i < emailList.length; i++) {
                    var test = "";
                    if (!CheckEmailValidation(emailList[i])) {
                        alert("Please enter valid Email separated by ';' !")
                        $('#dialog-share-book input[name="emailto"]').css('border', '1px solid red');
                        hideLoader();
                        return false;
                    }
                }
            }
            $('#dialog-share-book input[name="emailto"]').css('border', '1px solid black');

            var nameToList = "";
            if (nameto.length > 0) {
                //var isValid = /^[a-zA-Z0-9!@#\$%\^\&*\,\)\(;+=._-]+$/.test(nameto);
                var isValid = /^[a-zA-Z\;\s]+$/.test(nameto)
                if (isValid)
                    nameToList = nameto.split(';');
                else {
                    alert("Please enter valid ToName separated by ';' !")
                    $('#dialog-share-book input[name="nameto"]').css('border', '1px solid red');
                    hideLoader();
                    return false;
                }
            }
            if (emailList.length != nameToList.length) {
                alert("Number of Emails should match with number of ToNames!");
                $('#dialog-share-book input[name="emailto"]').css('border', '1px solid red');
                $('#dialog-share-book input[name="nameto"]').css('border', '1px solid red');
                hideLoader();
                return false;
            }

            $('#dialog-share-book input[name="emailto"]').css('border', '1px solid black');
            $('#dialog-share-book input[name="nameto"]').css('border', '1px solid black');

            var content = $('#dialog-share-book textarea[name="content"]').val();
            var Isvalid = false;
            var checkboxes = $("#groupList").find("input[type=checkbox]");
            var groups = "";
            if (checkboxes.length > 0) {
                for (var i = 1; i < checkboxes.length; i++) {
                    if ($(checkboxes[i]).prop("checked"))
                        groups += $(checkboxes[i]).val().trim() + ", ";
                }
                groups = groups.substr(0, groups.lastIndexOf(','));
            }
            var emailto = [];
            if (emailList.length > 0) {
                for (var i = 0; i < emailList.length; i++) {
                    emailto.push({ "Email": emailList[i], "ToName": nameToList[i] });
                }
                var userId = "<%= Sessions.SwitchedRackspaceId%>";
                var validateCode;
                $.ajax(
                    {
                        type: "POST",
                        url: '../EFileFolderJSONService.asmx/MailFirend',
                        data: "{ strSendMail:'" + email + "', strToMail:'" + JSON.stringify(emailto) + "', folderID:'" + docId + "', strMailInfor:'" + content + "',validateCode :'" + validateCode + "',SendName:'" + name + "', ToName:'" + nameto + "',MailTitle:'" + title + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if ($('#<%= hdnFolderId.ClientID %>').val() == "" || $('#<%= hdnFolderId.ClientID %>').val() == undefined) {
                                var splitid = data.d;
                                splitid = splitid.substring(0, splitid.length - 1);
                                $('table input[type="checkbox"]:checked').each(function () {
                                    if ($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]") && $(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val() && JSON.parse($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val())) {
                                        var idcheck = JSON.parse($(this).closest('tr').find("input[type='hidden'][id$=hdnValues]").val());
                                    }
                                    if (splitid.includes(idcheck)) {
                                        $(this).removeAttr('checked');
                                    }
                                    else {
                                        $(this).attr('checked');
                                    }
                                });
                                $('table input[type="checkbox"][id$=chkSelectAll]').removeAttr('checked');
                                var isChecked = $("table input[type=checkbox]").is(":checked");
                                data.d = "";
                                var errorMsg = data.d;

                                var title, message;

                                if (isChecked) {
                                    title = "Sharing EdFiles document error";
                                    message = "Failed to check security for some folders, please try again later !!!"
                                }
                                else {
                                    title = "Sharing EdFiles document success";
                                    message = "All checked folders have been shared successfully."
                                }
                            }
                            else {
                                if (data.d == "") {
                                    title = "Sharing EdFiles document error";
                                    message = "Failed to check security for this folder, please try again later !!!"
                                }
                                else {
                                    title = "Sharing EdFiles document success";
                                    message = "An EdFile has been shared successfully";
                                }
                            }
                            $('#<%= hdnFolderId.ClientID %>').val("");

                            $('#dialog-message-all').dialog({
                                title: title,
                                open: function () {
                                    $("#spanText").text(message);
                                }
                            });

                            $('#dialog-message-all').dialog("open");


                            $('#dialog-share-book').dialog("close");
                            //$('table input[type="checkbox"]').removeAttr('checked');

                            hideLoader();
                        },
                        fail: function (data) {
                            bookshelf.loaded.apply();

                            $('#dialog-message-all').dialog({
                                title: "Sharing EdFiles document error",
                                open: function () {
                                    $("#spanText").text("We were not able to share EdFiles at this time. Please try again later.");
                                }
                            });
                            $('#dialog-message-all').dialog("open");

                            hideLoader();
                        }
                    });
            }

            else {
                if (emailList.length <= 0) {
                    alert("Please Enter Email to share this document!");
                    hideLoader();
                    return false;

                }
            }

        }

        function OnShowShare(singleShare, ele) {
            if (!singleShare && $('table input[type="checkbox"]:checked').length <= 0) {
                alert("Please Select Atleast one document.");
                hideLoader();
                return false;
            }
            if (singleShare) {
                $('#<%= hdnFolderId.ClientID %>').val($(ele).attr("name"));
            }

            $('#dialog-share-book').dialog("open");
            $('#dialog-share-book input[name="emailto"]').val('');
            $('#dialog-share-book input[name="nameto"]').val('');
            $('#dialog-share-book input[name="email"]').val('<%=Sessions.SwitchedEmailId%>');
            $('#dialog-share-book input[name="title"]').css('background-color', 'black');
            $('#dialog-share-book textarea[name="content"]').val('');
            <%--if ('<%= User.IsInRole("Offices") %>' == 'True')
                $("#expirationTime").attr("readonly", false);
            else {
                $("#updateExpiration").hide();
                $("#expirationTime").css("width", "95%");
            }--%>
            GetExpirationTime();
            $('#dialog-share-book input[name="emailto"]').css('border', '1px solid black');

        }

        function CheckEmailValidation(fieldObj) {
            if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(fieldObj)) {
                //fieldObj.css('border', '1px solid black');
                return true;
            }
            else {
                //fieldObj.css('border', '1px solid red');

                return false;
            }
        }

    </script>
    <%--<asp:ScriptManager ID="scriptManager" runat="server"/>--%>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>View EdFiles</h1>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="page-container" style="padding: 0px;">
        <div class="inner-wrapper">
            <div class="listing-contant efile-page">
                <%--<div class="listing-view">
                    <h1>View eFileFolders</h1>
                    <p>
                        Below are all the eFileFolders that you currently have in your account. Click on
                                    the view link to see any of your eFileFolder. You can save the eFileFolder (eff
                                    file) on your computer by clicking on the down link. You will need to launch our
                                    website to view any locally installed .eff files.
                    </p>
                </div>--%>

                <%--<div class="quick-find">
                    <uc1:QuickFind ID="QuickFind2" runat="server" />--%>
                <%--  <uc2:ReminderControl ID="ReminderControl1" runat="server" />--%>
                <%--</div>--%>

                <%--<div class="clearfix"></div>--%>

                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:HiddenField ID="txtForBackBtn" runat="server" />
                        <asp:HiddenField ID="txtHasViewPrivilege" runat="server" />
                        <asp:HiddenField ID="txtHasPrivilage" runat="server" />
                        <asp:HiddenField ID="hdnFolderId" runat="server" />
                        <asp:HiddenField ID="hdnLetter" runat="server" />
                        <div class="listing-table view-folder-table">
                            <div class="table-toolbar">

                                <div class="sorter">
                                    <ul>
                                        <li>
                                            <asp:LinkButton ID="LinkButton6" runat="server" Text="A" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton7" runat="server" Text="B" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton8" runat="server" Text="C" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton9" runat="server" Text="D" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton10" runat="server" Text="E" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton11" runat="server" Text="F" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton12" runat="server" Text="G" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton13" runat="server" Text="H" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton14" runat="server" Text="I" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton15" runat="server" Text="J" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton16" runat="server" Text="K" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton17" runat="server" Text="L" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton18" runat="server" Text="M" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton19" runat="server" Text="N" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton20" runat="server" Text="O" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton23" runat="server" Text="P" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton24" runat="server" Text="Q" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton25" runat="server" Text="R" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton26" runat="server" Text="S" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton27" runat="server" Text="T" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton28" runat="server" Text="U" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton29" runat="server" Text="V" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton30" runat="server" Text="W" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton31" runat="server" Text="X" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton32" runat="server" Text="Y" OnClick="LinkButon_Letter_Click" /></li>
                                        <li>
                                            <asp:LinkButton ID="LinkButton33" runat="server" Text="Z" OnClick="LinkButon_Letter_Click" /></li>

                                    </ul>

                                </div>
                                <%--<cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" OnPageSizeChanged="WebPager1_PageSizeChanged"
                                    PageSize="50" CssClass="opt-select" Style="" PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>--%>
                                <asp:HiddenField ID="SortExpression1" runat="server" Value=""></asp:HiddenField>
                                <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>

                            </div>

                            <div class="table-toolbar">
                                <asp:Button OnClick="backFromLetters_Click" CommandName="Back" CssClass="backBtn" ToolTip="Back to previous page" runat="server" ID="backFromLetters2"></asp:Button>
                                <cc1:WebPager ID="WebPager2" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" OnPageSizeChanged="WebPager1_PageSizeChanged"
                                    PageSize="50" CssClass="opt-select" Style="" PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>
                                <asp:Label runat="server" ID="multiShare" Style="font-size: 18px; float: right; padding: 5px;">Multiple EdFiles Share</asp:Label>
                                <asp:LinkButton ID="lnkBtnShare" OnClientClick="OnShowShare(false);" runat="server" CssClass="ic-icon ic-share shareDocument" CausesValidation="False" Height="28px" Width="28px" ToolTip="Select Checkboxes to Share File" Style="float: right; margin: 2px 0px 5px;">
                                </asp:LinkButton>
                            </div>
                            <div class="table-scroll">
                                <asp:GridView ID="GridView1" runat="server" OnRowDeleting="GridView1_RowDeleting"
                                    DataKeyNames="FolderID" OnRowDataBound="GridView1_RowDataBound" OnSorting="GridView1_Sorting"
                                    AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable listing-datatable" OnRowCreated="GridView1_RowCreated">
                                    <%--Width="100%" BorderWidth="0"--%>
                                    <PagerSettings Visible="False" />
                                    <AlternatingRowStyle CssClass="odd" />
                                    <RowStyle CssClass="even" HorizontalAlign="Center" />
                                    <%--  <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />--%>
                                    <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                    <Columns>
                                        <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="30px"
                                            ItemStyle-CssClass="table_tekst_edit">
                                            <HeaderTemplate>
                                                <asp:CheckBox Text="" ID="chkSelectAll" runat="server" onclick="checkAll(this);" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" ID="chkStatus" CssClass="chkList" onclick="Check_Click(this);" />
                                                <asp:HiddenField runat="server" ID="hdnValues" Value='<%#  Convert.ToInt32(Eval("FolderID")) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ShowHeader="true" ItemStyle-Width="50%" ItemStyle-CssClass="actionMinWidth">
                                            <HeaderTemplate>
                                                <asp:Label ID="Label8" runat="server" Text="Action" />
                                            </HeaderTemplate>
                                            <ItemTemplate>

                                                <asp:LinkButton ID="LinkButton21" runat="server" CausesValidation="false" ToolTip="Download" CssClass="ic-icon ic-download"
                                                    OnClick="LinkButton21_Click" Text='<%# Eval("FolderID")%>' />
                                                <ajaxToolkit:ModalPopupExtender ID="lnkArchive_ModalPopupExtender" runat="server"
                                                    CancelControlID="Button2ArchiveClose" OkControlID="Button1ArchiveOK" TargetControlID="LinkButton21"
                                                    PopupControlID="DivArchiveConfirmation" BackgroundCssClass="ModalPopupBG">
                                                </ajaxToolkit:ModalPopupExtender>
                                                <ajaxToolkit:ConfirmButtonExtender ID="cbeArchiveEFileFolder" runat="server" DisplayModalPopupID="lnkArchive_ModalPopupExtender"
                                                    ConfirmText="" TargetControlID="LinkButton21"></ajaxToolkit:ConfirmButtonExtender>

                                                <asp:LinkButton ID="LinkButton1" runat="server" Text="" ToolTip="Edit" CausesValidation="false"
                                                    OnClick="LinkButton1_Click" OnClientClick="SetFolderID(this)" CssClass="ic-icon ic-edit">
                                                </asp:LinkButton>&nbsp;&nbsp;&nbsp;
                                               
                                                <asp:LinkButton ID="shareFile" ToolTip="Share" OnClientClick="OnShowShare(true, this);" Name='<%# Eval("FolderID")%>' runat="server" Text="" CausesValidation="false" CssClass="ic-icon ic-share"></asp:LinkButton>
                                                <asp:LinkButton ID="libDelete" runat="server" ToolTip="Delete" Text="" CausesValidation="false"
                                                    CommandName="Delete" CssClass="ic-icon ic-delete"></asp:LinkButton>
                                                <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                    CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                    PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                </ajaxToolkit:ModalPopupExtender>
                                                <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                    ConfirmText="" TargetControlID="libDelete"></ajaxToolkit:ConfirmButtonExtender>

                                                <%--<asp:LinkButton ID="LinkButton22" runat="server" CausesValidation="false" CssClass="ic-icon ic-view"
                                                    CommandArgument='<%#string.Format("{0},{1},{2}",Eval("FolderID"),Eval("Firstname"),Eval("Lastname")) %>'
                                                    OnCommand="LinkButton2_Click" OnClientClick='<%#string.Format("window.open(\"WebFlashViewer.aspx?ID={0}\",\"\",\"toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no\")",Eval("FolderID"))%>' />--%>

                                                <%--<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="false" CssClass="ic-icon ic-document"
                                                    CommandArgument='<%#string.Format("{0},{1},{2}",Eval("FolderID"),Eval("Firstname"),Eval("Lastname")) %>'
                                                    OnCommand="LinkButton2_Click" OnClientClick='<%# string.Format("callPopUp({0})", Eval("FolderID")) %>' />--%>

                                                <!-- changed to open html viewer in new page - 20Jan2015 -->
                                                <asp:LinkButton ID="LinkButton34" runat="server" CausesValidation="false" CssClass="ic-icon ic-document" Style="margin-right: 5px; margin-top: 2px;"
                                                    CommandArgument='<%#string.Format("{0},{1},{2}",Eval("FolderID"),Eval("Firstname"),Eval("Lastname")) %>'
                                                    OnCommand="LinkButton2_Click" OnClientClick='<%#string.Format("javascript:InsertViewEdFilesAsBookLog({0})", Eval("FolderID"))%>' />
                                                <%-- <asp:LinkButton ID="lnkFolderLogs" runat="server" Text="" CssClass="ic-icon ic-logfile" CausesValidation="false" ToolTip="Folder log "
                                                  CommandArgument='<%#string.Format("{0}",Eval("FolderID"))%>' OnClientClick='<%#string.Format("window.open(\"CheckListQuestionsAndAnswers.aspx?ID={0}\",\"_blank\")",Eval("FolderID"))%>' 
                                                   OnCommand="lnkFolderLogs_Command" Style="margin-top: 5px"  />--%>
                                                <asp:LinkButton ID="btnPreview1" runat="server" CssClass="ic-icon logFormPreveiw"></asp:LinkButton>
                                                <asp:LinkButton ID="btnPreview2" runat="server" CssClass="ic-icon logFormPreveiw"></asp:LinkButton>
                                                <asp:LinkButton ID="btnPreview3" runat="server" CssClass="ic-icon logFormPreveiw"></asp:LinkButton>
                                                <asp:LinkButton ID="btnPreview4" runat="server" CssClass="ic-icon logFormPreveiw"></asp:LinkButton>
                                                <asp:LinkButton ID="btnPreview5" runat="server" CssClass="ic-icon logFormPreveiw"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:HyperLinkField DataTextField="ComboFileNumber" HeaderText="FolderName" DataNavigateUrlFields="FolderID" ItemStyle-Width="21%"
                                            DataNavigateUrlFormatString="~/Office/FileFolderBox.aspx?id={0}" SortExpression="FolderName" ControlStyle-CssClass="edFileLink">
                                            <HeaderStyle Width="190px" />
                                        </asp:HyperLinkField>
                                        <%--<asp:TemplateField ShowHeader="true" ItemStyle-Width="21%" HeaderText="FolderName">
                                            <ItemTemplate>
                                                <asp:LinkButton Text='<%# Eval("FolderName") %>' runat="server" ID="lnkFolderName"
                                                    OnClientClick='<%#string.Format("javascript:InsertEdFileLogsAndRedirct({0})", Eval("FolderID"))%>'>
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname" ItemStyle-Width="8%">
                                            <%--<HeaderStyle CssClass="form_title" />--%>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname" ItemStyle-Width="8%">
                                            <%--<HeaderStyle CssClass="form_title" />--%>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FileNumber" HeaderText="FileNumber" SortExpression="FileNumber" ItemStyle-Width="8%">
                                            <%--<HeaderStyle CssClass="form_title" />--%>
                                        </asp:BoundField>
                                        <%--<asp:BoundField DataField="DOB" HeaderText="DOB" SortExpression="DOB"/>--%>
                                        <%-- <asp:BoundField DataField="Name" HeaderText="Creator" SortExpression="Name">
                                                                <HeaderStyle CssClass="form_title" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label3" runat="server" Text="Security" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TextBoxLevel" Text='<%# Bind("SecurityLevel") %>' runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                        <asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit" ItemStyle-Width="3%" HeaderText="Tabs">
                                            <HeaderTemplate>
                                                <asp:Label ID="Label10" Text="Tabs" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="false" Text='<%# Bind("OtherInfo") %>'
                                                    OnClick="LinkButton4_Click"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="OtherInfo2" HeaderText="Size(Mb)" DataFormatString="{0:F2}" SortExpression="OtherInfo2" ItemStyle-Width="6%"
                                            HtmlEncode="false"></asp:BoundField>
                                        <asp:BoundField DataField="Alert2" HeaderText="Alert" SortExpression="Alert2" ItemStyle-Wrap="false" ItemStyle-Width="5%" Visible="false" />
                                        <asp:BoundField DataField="Alert1" HeaderText="Alert" SortExpression="Alert1" ItemStyle-Wrap="false" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="Alert2" HeaderText="Status" SortExpression="Alert2" ItemStyle-Wrap="false" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="Site1" HeaderText="Site" SortExpression="Site1" ItemStyle-Wrap="false" ItemStyle-Width="5%" />
                                        <asp:BoundField DataField="CreatedDate" HeaderText="CreateDate" DataFormatString="{0:MM.dd.yyyy}"
                                            SortExpression="CreatedDate" ItemStyle-Width="3%" />
                                        <%--<asp:TemplateField ShowHeader="true" ItemStyle-Width="3%">
                                            <HeaderTemplate>
                                                <asp:Label ID="Label34" Text="EFF" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton22" runat="server" Text="View" CausesValidation="false"
                                                    CommandArgument='<%#string.Format("{0},{1},{2}",Eval("FolderID"),Eval("Firstname"),Eval("Lastname")) %>'
                                                    OnCommand="LinkButton2_Click" OnClientClick='<%#string.Format("window.open(\"WebFlashViewer.aspx?V=2&ID={0}\",\"\",\"toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no\")",Eval("FolderID"))%>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>

                                        <%--     <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="false" ItemStyle-Width="30px"
                                            ItemStyle-CssClass="table_tekst_edit">
                                            <HeaderTemplate>
                                                <asp:Label ID="Label4" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                              
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="false" ItemStyle-Width="30px"
                                            ItemStyle-CssClass="table_tekst_edit">
                                            <HeaderTemplate>
                                                <asp:Label ID="Label41" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>

                                        <%--  <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                            ItemStyle-CssClass="table_tekst_edit" >--%>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <asp:Label ID="Label1" runat="server" />There is no folder. please check the link
                                                                        to
                                                                       
                                        <asp:HyperLink ID="HyperLink1" runat="server" Text="Create New EdFile" NavigateUrl="~/Office/NeweFileFolder.aspx"></asp:HyperLink>
                                        right now.
                                   
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>

                            <div class="table-toolbar">
                                <asp:Button OnClick="backFromLetters_Click" CommandName="Back" CssClass="backBtn" ToolTip="Back to previous page" runat="server" ID="backFromLetters1"></asp:Button>
                                <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged" OnPageSizeChanged="WebPager1_PageSizeChanged"
                                    PageSize="50" CssClass="opt-select" Style="" PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>
                            </div>
                            <asp:Panel class="popup-mainbox" ID="DivDeleteConfirmation" Style="display: none" runat="server">
                                <div>
                                    <div class="popup-head" id="PopupHeader">
                                        Delete Folder
                                   
                                    </div>
                                    <div class="popup-scroller">
                                        <p>
                                            Caution! Are you sure you want to delete this folder. Once you delete this file,
                                                                    it will be deleted for good
                                       
                                        </p>
                                        <p>
                                            Confirm Password:
                                                                   
                                            <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                                        </p>
                                    </div>
                                    <div class="popup-btn">
                                        <input id="ButtonDeleleOkay" type="button" value="Yes" class="btn green" style="margin-right: 20px;" />
                                        <input id="ButtonDeleteCancel" type="button" value="No" class="btn green" />
                                    </div>
                                </div>
                            </asp:Panel>


                            <asp:Button Style="display: none" ID="btnShowMessagePopup" runat="server"></asp:Button>
                            <ajaxToolkit:ModalPopupExtender ID="mdlPopupMessage" runat="server" BackgroundCssClass="modalBackground"
                                CancelControlID="btnMessageClose" PopupControlID="pnlMessagePopup" TargetControlID="btnShowMessagePopup">
                            </ajaxToolkit:ModalPopupExtender>


                            <asp:Panel Style="display: none" ID="pnlMessagePopup" runat="server" BackColor="White" Width="450px">
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div>
                                            <asp:Label ID="Label2" runat="server" Font-Bold="true" Text="Warning Message" BackColor="white" />
                                        </div>
                                        <div style="background-color: White; color: #000; padding-left: 5px; padding-right: 5px;">
                                            <p>
                                                Your login doesn't have permission to edit or delete EdFiles. Please contact
                                                                        administrator to make this change.
                                           
                                            </p>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div align="center" style="background-color: white; padding: 20px 0;">
                                    <asp:Button ID="btnMessageClose" runat="server" Text="Close" />
                                </div>
                            </asp:Panel>


                            <asp:Button Style="display: none" ID="btnShowPopup" runat="server"></asp:Button>
                            <ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" BackgroundCssClass="modalBackground"
                                CancelControlID="btnClose" PopupControlID="pnlPopup" TargetControlID="btnShowPopup">
                            </ajaxToolkit:ModalPopupExtender>


                            <asp:Panel Style="display: none" ID="pnlPopup" runat="server" CssClass="popup-mainbox">
                                <asp:UpdatePanel ID="updPnlFolderDetail" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:Label ID="lblFileFolderDetail" runat="server" Font-Bold="true" Text="edFile Details"
                                            BackColor="#eaeaea" Width="100%" CssClass="popup-head" />
                                        <div class="popup-scroller">
                                            <asp:DetailsView ID="dvFolderDetail" DataKeyNames="FolderID,OfficeID" OnDataBound="FolderDetail_OnDataBound"
                                                AllowPaging="false" AutoGenerateRows="false" runat="server" DefaultMode="Edit"
                                                Width="100%" BackColor="white" CssClass="details-datatable" Border="0">
                                                <Fields>
                                                    <%--  <asp:BoundField DataField="FolderID" HeaderText="FolderID"/>--%>
                                                    <asp:BoundField DataField="FolderName" HeaderText="FolderName" />
                                                    <asp:BoundField DataField="Firstname" HeaderText="Firstname" />
                                                    <asp:BoundField DataField="Lastname" HeaderText="Lastname" />

                                                    <asp:BoundField DataField="Email" HeaderText="Email" />
                                                    <asp:BoundField DataField="Address" HeaderText="Address" />
                                                    <asp:BoundField DataField="City" HeaderText="City" />
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="Label35" runat="server" Text="State" />
                                                        </HeaderTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlStates" runat="server">
                                                            </asp:DropDownList>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="OtherState" HeaderText="OtherState" />
                                                    <asp:BoundField DataField="Postal" HeaderText="Postal" />
                                                    <asp:BoundField DataField="Tel" HeaderText="Tel." />
                                                    <asp:BoundField DataField="FaxNumber" HeaderText="FaxNumber" />
                                                    <asp:BoundField DataField="FileNumber" HeaderText="FileNumber" />
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblDOB" runat="server" Text="DOB" />
                                                        </HeaderTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox name="textfield5" Placeholder="mm-dd-yyyy" runat="server" ID="textfield5"
                                                                size="15" MaxLength="50" Text='<% #Eval("DOB")%>' />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Alert1" HeaderText="Alert1" />
                                                    <asp:BoundField DataField="Alert2" HeaderText="Status" />
                                                    <asp:BoundField DataField="Site1" HeaderText="Site1" />
                                                    <asp:BoundField DataField="Comments" HeaderText="Comments" />
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="Label5" runat="server" Text="Security" />
                                                        </HeaderTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="DropDownList1" runat="server" OnDataBinding="DropDownList1_DataBinding" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"
                                                                DataValueField="SecurityLevel" AutoPostBack="true">
                                                                <asp:ListItem Text="Private" Value="0" />
                                                                <asp:ListItem Text="Protected" Value="1" />
                                                                <asp:ListItem Text="Public" Value="2" />
                                                            </asp:DropDownList>
                                                            <asp:TextBox ID="folderCode" runat="server" MaxLength="20" TextMode="Password" Width="92" />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="label6" runat="server" Text="Groups" />
                                                        </HeaderTemplate>
                                                        <EditItemTemplate>
                                                            <div runat="server" id="trgroups" style="padding: 5px 0">
                                                                <div runat="server" id="divfield1" style="padding: 5px 0">
                                                                    <fieldset style="border: none">
                                                                        <label>&nbsp;</label>
                                                                        <asp:CheckBox name="groupfield1" runat="server" Text=""
                                                                            ID="groupfield1" CssClass="checkForGroup" size="20" />
                                                                        <asp:TextBox ID="grouplink1" Text="Group 1" runat="server" Width="80%" Height="30px" />
                                                                    </fieldset>

                                                                </div>
                                                                <div runat="server" id="divfield2" style="padding: 5px 0">
                                                                    <fieldset style="border: none">
                                                                        <label>&nbsp;</label>
                                                                        <asp:CheckBox name="groupfield2" runat="server" Text=""
                                                                            ID="groupfield2" CssClass="checkForGroup" size="20" />
                                                                        <asp:TextBox ID="grouplink2" Text="Group 2" runat="server" Width="80%" Height="30px" />
                                                                    </fieldset>
                                                                </div>
                                                                <div runat="server" id="divfield3" style="padding: 5px 0">
                                                                    <fieldset style="border: none">
                                                                        <label>&nbsp;</label>
                                                                        <asp:CheckBox name="groupfield3" runat="server" Text=""
                                                                            ID="groupfield3" size="20" CssClass="checkForGroup" />
                                                                        <asp:TextBox ID="grouplink3" Text="Group 3" runat="server" Width="80%" Height="30px" />
                                                                    </fieldset>
                                                                </div>
                                                                <div runat="server" id="divfield4" style="padding: 5px 0">
                                                                    <fieldset style="border: none">
                                                                        <label>&nbsp;</label>
                                                                        <asp:CheckBox name="groupfield4" runat="server" Text=""
                                                                            ID="groupfield4" CssClass="checkForGroup" size="20" />
                                                                        <asp:TextBox ID="grouplink4" Text="Group 4" runat="server" Width="80%" Height="30px" />
                                                                    </fieldset>
                                                                </div>
                                                                <div runat="server" id="divfield5" style="padding: 5px 0">
                                                                    <fieldset style="border: none">
                                                                        <label>&nbsp;</label>
                                                                        <asp:CheckBox name="groupfield5" runat="server" Text=""
                                                                            ID="groupfield5" CssClass="checkForGroup" size="20" />
                                                                        <asp:TextBox ID="grouplink5" Text="Group 5" runat="server" Width="80%" Height="30px" />
                                                                    </fieldset>
                                                                </div>
                                                            </div>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="Label17" runat="server" Text="Security Code" />
                                                        </HeaderTemplate>
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="textCodeBox" MaxLength="20" Text='<%# DataBinder.Eval(Container.DataItem,"FolderCode")%>'
                                                                runat="server" />
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="TemplateName" ReadOnly="true" HeaderText="Template" />
                                                    <asp:BoundField DataField="OtherInfo" ReadOnly="true" HeaderText="Tabs Number" />
                                                    <asp:BoundField DataField="OtherInfo2" ReadOnly="true" HeaderText="Total Size(Mb)"
                                                        DataFormatString="{0:F2}" HtmlEncode="false" />
                                                </Fields>
                                            </asp:DetailsView>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <div class="popup-btn">
                                    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" CssClass="btn green" Style="margin-right: 20px;" />
                                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn green" />
                                </div>
                            </asp:Panel>


                            <asp:Button Style="display: none" ID="Button1ShowPopup" runat="server"></asp:Button>
                            <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
                                CancelControlID="btnClose1" PopupControlID="Panel1Tabs" TargetControlID="Button1ShowPopup">
                            </ajaxToolkit:ModalPopupExtender>

                            <asp:Panel Style="display: none; z-index: 10001" ID="Panel1Tabs" runat="server" BackColor="white" CssClass="popup-mainbox">
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <div id="divTabNum" class="popup-head">Tabs Details</div>
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <div class="popup-scroller">
                                                    <asp:GridView ID="GridView2" ShowFooter="true" OnRowEditing="GridView2_OnRowEditing"
                                                        OnRowCancelingEdit="GridView2_RowCancelingEdit" OnRowUpdating="GridView2_RowUpdating"
                                                        OnRowDataBound="GridView2_OnRowDataBound" OnRowCommand="GridView2_OnRowCommand"
                                                        OnRowDeleting="GridView2_OnRowDeleting" runat="server" AutoGenerateColumns="false" CssClass="datatable listing-datatable"
                                                        DataKeyNames="DividerID">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                        <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                        <FooterStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="100px" ItemStyle-Wrap="false"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label18" runat="server" Text="Action" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton5" CommandName="Edit" runat="server" Text="" CssClass="ic-icon ic-edit"></asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton3" CommandName="Delete" CausesValidation="false" runat="server" CssClass="ic-icon ic-delete"
                                                                        Text=""></asp:LinkButton>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure to delete the tab ?(then delete everything, tab and the docs inside that tab)"
                                                                        TargetControlID="LinkButton3"></ajaxToolkit:ConfirmButtonExtender>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:LinkButton ID="lnkInsert" runat="server" CausesValidation="False" CommandName="Insert" CssClass="ic-icon ic-add"></asp:LinkButton>
                                                                </FooterTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update" ToolTip="Save" CssClass="ic-icon ic-save"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel" ToolTip="Cancel" CssClass="ic-icon ic-cancel"></asp:LinkButton>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label6" runat="server" Text="Name" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label12" Width="80px" runat="server" Text='<%# Eval("Name")%>' />
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:TextBox ID="txtNewName" Text="" runat="server"></asp:TextBox>
                                                                </FooterTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"Name")%>'
                                                                        ID="textfield10" />
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label7" runat="server" Text="Color" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <cc1:LabelExender ID="LabelExender1" Color='<%# Eval("Color")%>' BorderWidth="1px"
                                                                        Width="100%" runat="server">&nbsp;</cc1:LabelExender>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <div style="display: inline-flex">
                                                                        <div style="border: solid 1px black; height: 32px; width: 70px">
                                                                            <asp:Label
                                                                                ID="lblSample1"
                                                                                AutoCompleteType="None"
                                                                                runat="server" Style="display: block; height: 22px; width: 62px; margin: 3px;" />
                                                                        </div>
                                                                        <asp:TextBox
                                                                            ID="txtCardColor1"
                                                                            AutoCompleteType="None"
                                                                            runat="server" Style="height: 0px; width: 0px; border: none" />
                                                                        <asp:Button
                                                                            CssClass="cp_button"
                                                                            ID="btnPickColor1"
                                                                            runat="server" />
                                                                        <cc1:ColorPickerExtender
                                                                            ID="ColorPicker3"
                                                                            TargetControlID="txtCardColor1"
                                                                            PopupButtonID="btnPickColor1"
                                                                            PopupPosition="TopRight"
                                                                            SampleControlID="lblSample1"
                                                                            SelectedColor='<%# Eval("ColorWithoutHash")%>'
                                                                            OnClientColorSelectionChanged="Color_Changed"
                                                                            Enabled="True"
                                                                            runat="server"></cc1:ColorPickerExtender>
                                                                    </div>
                                                                </EditItemTemplate>
                                                                <FooterTemplate>
                                                                    <div style="display: inline-flex; margin-top: 4px;">
                                                                        <div style="border: solid 1px black; height: 32px; width: 70px">
                                                                            <asp:Label
                                                                                ID="lblSample2"
                                                                                AutoCompleteType="None"
                                                                                runat="server" Style="display: block; height: 22px; width: 62px; margin: 3px;" />
                                                                        </div>
                                                                        <asp:TextBox
                                                                            ID="txtCardColor2"
                                                                            AutoCompleteType="None"
                                                                            runat="server" Style="height: 0px; width: 0px; border: none" />
                                                                        <asp:Button
                                                                            CssClass="cp_button"
                                                                            ID="btnPickColor2"
                                                                            runat="server" />
                                                                        <cc1:ColorPickerExtender
                                                                            ID="ColorPicker5"
                                                                            TargetControlID="txtCardColor2"
                                                                            PopupButtonID="btnPickColor2"
                                                                            PopupPosition="TopRight"
                                                                            SampleControlID="lblSample2"
                                                                            OnClientColorSelectionChanged="Color_Changed"
                                                                            SelectedColor="FF0000"
                                                                            Enabled="True"
                                                                            runat="server"></cc1:ColorPickerExtender>
                                                                    </div>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="60px" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label62" runat="server" Text="Locked" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Label122" Width="60px" runat="server" Text='<%# Eval("Locked")%>' />
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    <asp:CheckBox ID="txtLocked" Text="" Checked="False" runat="server"></asp:CheckBox>
                                                                </FooterTemplate>
                                                                <EditItemTemplate>
                                                                    <asp:CheckBox runat="server" ID="checkLocked" Checked='<%# DataBinder.Eval(Container.DataItem,"Locked")%>' />
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <div class="popup-btn">
                                    <asp:Button ID="btnSave1" runat="server" OnClick="btnClose_Click" Text="Close" CssClass="btn green" />
                                    <asp:Button ID="btnClose1" runat="server" Text="Close" CssClass="btn green" Style="display: none" />
                                    <asp:HiddenField ID="HiddenField1" runat="server" />
                                </div>
                            </asp:Panel>
                        </div>
                        </div>
                   
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged" />
                        <%--<asp:AsyncPostBackTrigger ControlID="LinkButton6" EventName="PageIndexChanged" />--%>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Panel class="popup-mainbox" ID="DivArchiveConfirmation" Style="display: none" runat="server">
                    <div>
                        <div class="popup-head" id="Div3">
                            Download Folder
                       
                        </div>
                        <div class="popup-scroller">
                            <p>
                                Please enter your password to download a zipped folder of all the documents from within this EdFile.
                           
                            </p>
                            <p>
                                Confirm Password:
                   
                                <asp:TextBox ID="txtBoxArchivePwd" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                            </p>
                        </div>
                        <div class="popup-btn">
                            <input id="Button1ArchiveOK" type="button" value="Yes" class="btn green" style="margin-right: 20px;" />
                            <input id="Button2ArchiveClose" type="button" value="No" class="btn green" />
                        </div>
                    </div>
                </asp:Panel>

                <%--Share Document--%>
                <div id="dialog-share-book" title="Share this eFile" style="display: none">
                    <p class="validateTips">All form fields are required.</p>
                    <br />
                    <label for="email">From Email</label><br />
                    <input type="email" name="email" id="email" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
                    <label for="emailto">To Email</label><br />
                    <input type="email" name="emailto" id="emailto" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" placeholder="Enter Email separated with';'" /><br />
                    <label for="nameto">To Name</label><br />
                    <input type="text" name="nameto" id="nameto" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" placeholder="Enter Name separated with';'" /><br />
                    <label for="title">Subject</label><br />
                    <input type="text" name="title" id="title" value="EdFile Share" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" readonly="readonly" /><br />
                    <label for="content">Message</label><br />
                    <textarea name="content" id="content" rows="2" cols="25" class="text ui-widget-content ui-corner-all" style="border: 1px solid black"></textarea><br />
                    <label for="content">Link Expiration Time (In Hrs)</label><br />
                    <input type="text" name="linkexpiration" id="expirationTime" class="text ui-widget-content ui-corner-all" style="border: 1px solid black; width: 80%;" />
                    <input type="button" id="updateExpiration" onclick="UpdateExpirationTime()" class="btn-small green" style="float: right" value="UPDATE" /><br />
                    <span id="errorMsg"></span>
                </div>
                <div id="dialog-message-all" title="" style="display: none;">
                    <p id="dialog-message-content">
                        <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span>
                        <span id="spanText"></span>
                    </p>
                </div>
                <%--End Share Document--%>
            </div>
        </div>
        <div id="overlay">
            <div id="floatingCirclesG">
                <div class="f_circleG" id="frotateG_01"></div>
                <div class="f_circleG" id="frotateG_02"></div>
                <div class="f_circleG" id="frotateG_03"></div>
                <div class="f_circleG" id="frotateG_04"></div>
                <div class="f_circleG" id="frotateG_05"></div>
                <div class="f_circleG" id="frotateG_06"></div>
                <div class="f_circleG" id="frotateG_07"></div>
                <div class="f_circleG" id="frotateG_08"></div>
            </div>
        </div>
    </div>
    <a class='iframe' href="javascript:;" style="display: none;">popup</a>
    <script type="text/javascript">
        function LoadColorPicker() {
            //This code must be placed below the ScriptManager, otherwise the "Sys" cannot be used because it is undefined.
            Sys.Application.add_init(function () {
                // Store the color validation Regex in a "static" object off of
                // AjaxControlToolkit.ColorPickerBehavior.  If this _colorRegex object hasn't been
                // created yet, initialize it for the first time.
                if (!Sys.Extended.UI.ColorPickerBehavior._colorRegex) {
                    Sys.Extended.UI.ColorPickerBehavior._colorRegex = new RegExp('^[A-Fa-f0-9]{6}$');
                }
            });
        }

        function Color_Changed(sender) {
            sender.get_element().value = "#" + sender.get_selectedColor();
        }
    </script>
</asp:Content>

