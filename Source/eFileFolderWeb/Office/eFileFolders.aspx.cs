using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using AjaxControlToolkit;
using DataQuicker2.Framework;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;
using Shinetech.Utility;

public partial class Office_eFileFolders : LicensePage//System.Web.UI.Page
{
    private static readonly string PDFPATH = ConfigurationManager.AppSettings["PdfPath"];
    private static readonly string BackupPath = ConfigurationManager.AppSettings["BackupPath"];
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
    private NLogLogger _logger = new NLogLogger();

    //public string IsShowInsert;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            //Shinetech.DAL.Account user = new Shinetech.DAL.Account(Sessions.SwitchedSessionId);
            //txtHasPrivilage.Value = user.HasPrivilegeDelete.Value.ToString().ToUpper();
            //txtHasViewPrivilege.Value = Common_Tatva.HasViewPrivilegeOnly().ToString().ToUpper();
            backFromLetters1.Visible = false;
            backFromLetters2.Visible = false;
            GetData();
            BindGrid();
        }
    }

    #region 数据绑定

    private System.Collections.Generic.IDictionary<string, string> UserGroups
    {
        get
        {
            object keys = Session["UserGroups"];

            if (keys == null)
            {
                DataTable table = UserManagement.GetUsersGroups();
                IDictionary<string, string> ugs = new Dictionary<string, string>();
                foreach (DataRow row in table.Rows)
                {
                    ugs.Add(row[0].ToString(), row[1].ToString());
                }

                Session["UserGroups"] = ugs;
                keys = ugs;
            }

            return keys as IDictionary<string, string>;

        }
    }


    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData()
    {

        string officeID1 = Sessions.SwitchedRackspaceId;
        if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users")) // 
        {
            var folders = FileFolderManagement.GetFileFoldersInfor(officeID1);

            //如果是Sub user 则进行筛选groups
            var uid = Membership.GetUser().ProviderUserKey.ToString();
            var act = new Shinetech.DAL.Account(uid);
            string[] groups = act.Groups.Value != null ? act.Groups.Value.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries) : new string[] { };
            bool? hasProtectedSecurity = (act.HasProtectedSecurity.IsNull || Convert.ToBoolean(act.HasProtectedSecurity.ToString())) ? true : false;
            bool? hasPublicSecurity = (act.HasPublicSecurity.IsNull || Convert.ToBoolean(act.HasPublicSecurity.ToString())) ? true : false;
            int count = folders.Rows.Count;

            for (int i = 0; i < count; i++)
            {
                DataRow row = folders.Rows[i];
                //当该Folder不是这个用户创建的folder，且当该folder是私有的，并且该Folder并不在该用户所属的Groups中
                if (!uid.Equals(row["OfficeID"].ToString()))
                {
                    string security = row["SecurityLevel"].ToString();
                    if ((security.Equals("0") && !BelongToGroup(row["Groups"].ToString(), groups)) || (security.Equals("1") && !hasProtectedSecurity.Value) || (security.Equals("2") && !hasPublicSecurity.Value))
                    {
                        row.Delete();
                    }
                }
            }

            folders.AcceptChanges();

            this.DataSource = folders;
        }
        else
        {
            this.DataSource = FileFolderManagement.GetFileFoldersInfor(officeID1);
        }
        try
        {
            string sortDirection = "";
            string sortExpression = this.Sort_Expression;
            if (!string.IsNullOrEmpty(sortExpression))
            {
                if (this.Sort_Direction == SortDirection.Ascending)
                {
                    sortDirection = "ASC";
                }
                else
                {
                    sortDirection = "DESC";
                }
                DataView Source = new DataView(this.DataSource);
                Source.Sort = sortExpression + " " + sortDirection;
                this.DataSource = Source.ToTable();
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
        foreach (DataRow drRow in this.DataSource.Rows)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(drRow["FileNumber"])))
                drRow["ComboFileNumber"] = drRow["FolderName"] + " (" + drRow["FileNumber"] + ")";
            else
                drRow["ComboFileNumber"] = drRow["FolderName"];
            //drRow["CreatedDate"] = string.IsNullOrEmpty(Convert.ToString(drRow["CreatedDate"])) ? "" : Convert.ToDateTime(drRow["CreatedDate"]).ToString("MM-dd-yyyy");
        }
        SetFolderIdSource();
    }

    private bool BelongToGroup(string p, string[] groups)
    {
        foreach (string group in groups)
        {
            if (p.Contains(group))
            {
                return true;
            }
        }

        return false;
    }

    private void GetData(string letter)
    {
        string officeID1 = Sessions.SwitchedRackspaceId;
        if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users")) // 
        {
            //如果是Sub user 则进行筛选groups

            var folders = FileFolderManagement.GetFileFoldersInfor(officeID1, letter);

            //如果是Sub user 则进行筛选groups
            var uid = Membership.GetUser().ProviderUserKey.ToString();
            var act = new Shinetech.DAL.Account(uid);
            string[] groups = act.Groups.Value != null ? act.Groups.Value.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries) : new string[] { };
            bool? hasProtectedSecurity = (act.HasProtectedSecurity.IsNull || Convert.ToBoolean(act.HasProtectedSecurity.ToString())) ? true : false;
            bool? hasPublicSecurity = (act.HasPublicSecurity.IsNull || Convert.ToBoolean(act.HasPublicSecurity.ToString())) ? true : false;
            int count = folders.Rows.Count;

            for (int i = 0; i < count; i++)
            {
                DataRow row = folders.Rows[i];
                if (!uid.Equals(row["OfficeID"].ToString()))
                {
                    string security = row["SecurityLevel"].ToString();
                    if ((security.Equals("0") && !BelongToGroup(row["Groups"].ToString(), groups)) || (security.Equals("1") && !hasProtectedSecurity.Value) || (security.Equals("2") && !hasPublicSecurity.Value))
                    {
                        row.Delete();
                    }
                }
            }

            folders.AcceptChanges();

            this.DataSource = folders;
        }
        else
        {
            this.DataSource = FileFolderManagement.GetFileFoldersInfor(officeID1, letter);
        }
        try
        {
            string sortDirection = "";
            string sortExpression = this.Sort_Expression;
            if (!string.IsNullOrEmpty(sortExpression))
            {
                if (this.Sort_Direction == SortDirection.Ascending)
                {
                    sortDirection = "ASC";
                }
                else
                {
                    sortDirection = "DESC";
                }
                DataView Source = new DataView(this.DataSource);
                Source.Sort = sortExpression + " " + sortDirection;
                this.DataSource = Source.ToTable();
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
        foreach (DataRow drRow in this.DataSource.Rows)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(drRow["FileNumber"])))
                drRow["ComboFileNumber"] = drRow["FolderName"] + " (" + drRow["FileNumber"] + ")";
            else
                drRow["ComboFileNumber"] = drRow["FolderName"];
            drRow["CreatedDate"] = string.IsNullOrEmpty(Convert.ToString(drRow["CreatedDate"])) ? "" : Convert.ToDateTime(drRow["CreatedDate"]).ToString("MM-dd-yyyy");
        }
        SetFolderIdSource();
    }

    public DataTable States
    {
        get
        {
            object obj = Application["States"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetStates();
                Application.Lock();
                Application["States"] = table;
                Application.UnLock();
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        //GridView1.DataSource = this.DataSource;
        //GridView1.DataBind();
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
        WebPager2.DataSource = this.DataSource;
        WebPager2.DataBind();
    }
    #endregion

    /// <summary>
    /// Handles the PageIndexChanged event of the WebPager1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="PageChangedEventArgs"/> instance containing the event data.</param>
    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager2.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager2.DataSource = this.DataSource;
        if (WebPager1.CurrentPageIndex == 0)
        {
            backFromLetters1.Visible = false;
            backFromLetters2.Visible = false;
        }
        else
        {
            backFromLetters1.Visible = true;
            backFromLetters2.Visible = true;
        }
        WebPager1.DataBind();
        WebPager2.DataBind();
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
        WebPager2.CurrentPageIndex = 0;
        WebPager2.DataSource = this.DataSource;
        WebPager2.DataBind();
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource"] as DataTable;
        }
        set
        {
            this.Session["DataSource"] = value;
        }
    }

    /// <summary>
    /// Handles the Sorting event of the GridView1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        this.Sort_Expression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;
        this.DataSource = Source.ToTable();
        SetFolderIdSource();
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    /// <summary>
    /// Gets or sets the folder identifier source.
    /// </summary>
    /// <value>
    /// The folder identifier source.
    /// </value>
    private string FolderIdSource
    {
        get
        {
            return this.Session["FolderIdSource"] as string;
        }
        set
        {
            this.Session["FolderIdSource"] = value;
        }
    }

    private string Sort_Expression
    {
        get
        {
            return this.SortExpression1.Value;
        }
        set
        {
            this.SortExpression1.Value = value;
        }
    }

    /// <summary>
    /// Handles the Click event of the btnSave control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string folderId = this.dvFolderDetail.DataKey.Value.ToString();
        DataView dataTable = new DataView(this.DataSource);
        dataTable.RowFilter = "FolderID=" + folderId;
        dataTable.Sort = "FolderID";
        DataRowView drView = dataTable.FindRows(folderId)[0];

        try
        {
            drView.BeginEdit();

            Shinetech.DAL.FileFolder folder = new Shinetech.DAL.FileFolder(Convert.ToInt32(folderId));
            if (!folder.IsExist) return;

            foreach (DetailsViewRow item in dvFolderDetail.Rows)
            {
                #region Edit DataRow
                TableCell cell = item.Controls[1] as TableCell;
                if (cell != null && cell.HasControls())
                {
                    int i = 0;

                    if (item.RowIndex == 18 || item.RowIndex == 17 || item.RowIndex == 6)
                    {
                        i = 1;
                    }

                    Control control = cell.Controls[i];

                    if (control is TextBox)
                    {
                        TextBox textBox = control as TextBox;
                        switch (item.RowIndex)
                        {
                            case 0://FolderName
                                drView["FolderName"] = textBox.Text.Trim();

                                folder.FolderName.Value = textBox.Text.Trim();
                                break;
                            case 1: //Firstname
                                drView["Firstname"] = textBox.Text.Trim();
                                folder.FirstName.Value = textBox.Text.Trim();
                                break;
                            case 2: //Lastname
                                drView["Lastname"] = textBox.Text.Trim();
                                folder.Lastname.Value = textBox.Text.Trim();
                                break;

                            case 3: //Email
                                drView["Email"] = textBox.Text.Trim();
                                folder.Email.Value = textBox.Text.Trim();
                                break;
                            case 4: //Address
                                drView["Address"] = textBox.Text.Trim();
                                folder.Address.Value = textBox.Text.Trim();
                                break;
                            case 5: //City
                                drView["City"] = textBox.Text.Trim();
                                folder.City.Value = textBox.Text.Trim();
                                break;
                            case 7: // OtherState
                                drView["OtherState"] = textBox.Text.Trim();
                                folder.OtherState.Value = textBox.Text.Trim();
                                break;
                            case 8: // Postal
                                drView["Postal"] = textBox.Text.Trim();
                                folder.Postal.Value = textBox.Text.Trim();
                                break;
                            case 9: // Tel.
                                drView["Tel"] = textBox.Text.Trim();
                                folder.Tel.Value = textBox.Text.Trim();
                                break;
                            case 10: // FaxNumber
                                drView["FaxNumber"] = textBox.Text.Trim();
                                folder.FaxNumber.Value = textBox.Text.Trim();
                                break;
                            case 11: //FileNumber
                                drView["FileNumber"] = textBox.Text.Trim();
                                folder.FileNumber.Value = textBox.Text.Trim();
                                break;
                            case 12: //DOB
                                //drView["DOB"] = textBox.Text.Trim();
                                TextBox dobText = this.dvFolderDetail.FindControl("textfield5") as TextBox;
                                if (dobText != null)
                                    folder.DOB.Value = dobText.Text;
                                break;
                            case 13: //Alert1
                                drView["Alert1"] = textBox.Text.Trim();
                                folder.Alert1.Value = textBox.Text.Trim();
                                break;
                            case 14: //Alert2
                                drView["Alert2"] = textBox.Text.Trim();
                                folder.Alert2.Value = textBox.Text.Trim();
                                break;
                            case 15: //Site1
                                drView["Site1"] = textBox.Text.Trim();
                                folder.Site1.Value = textBox.Text.Trim();
                                break;
                            case 16: //Comments
                                drView["Comments"] = textBox.Text.Trim();
                                folder.Comments.Value = textBox.Text.Trim();
                                break;
                            case 18: //FolderCode
                                drView["FolderCode"] = textBox.Text.Trim();
                                folder.FolderCode.Value = textBox.Text.Trim();
                                break;

                            default:
                                break;
                        }
                    }
                    else if (control is LiteralControl)
                    {
                        if (item.RowIndex == 12)
                        {
                            TextBox dobText = this.dvFolderDetail.FindControl("textfield5") as TextBox;
                            if (dobText != null)
                                folder.DOB.Value = dobText.Text;
                        }
                    }
                    else if (control is DropDownList)
                    {

                        DropDownList droplist = control as DropDownList;
                        if (item.RowIndex == 6)
                        {
                            drView["StateID"] = droplist.SelectedValue;
                            folder.StateID.Value = droplist.SelectedValue;
                        }
                        else
                        {
                            TextBox folderCode = this.dvFolderDetail.FindControl("folderCode") as TextBox;
                            string groups = string.Empty;
                            string folderCodeText = string.Empty;

                            drView["SecurityLevel"] = droplist.SelectedIndex;

                            if (folder.SecurityLevel.Value == 2 && (droplist.SelectedIndex == 0 || droplist.SelectedIndex == 1))
                            {
                                // delete current all share code
                                FileFolderManagement.ClearFolderValidCode(folder.FolderID.Value);
                            }
                            else if (droplist.SelectedIndex == 0)
                            {
                                for (int j = 1; j <= 5; j++)
                                {
                                    CheckBox groupBox = this.dvFolderDetail.FindControl("groupfield" + j) as CheckBox;
                                    if (groupBox != null && groupBox.Checked)
                                    {
                                        groups += j + "|";
                                    }
                                }
                            }

                            if (droplist.SelectedIndex == 2)
                                folderCodeText = folderCode.Text;

                            folder.FolderCode.Value = folderCodeText;
                            folder.Groups.Value = groups;
                            folder.SecurityLevel.Value = droplist.SelectedIndex;

                        }

                    }
                }
                #endregion
            }

            //submit to database
            FileFolderManagement.UpdateFileFolder(folder);
            drView.EndEdit();
            dataTable.Table.AcceptChanges();
            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(folderId), 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Edit.GetHashCode(), 0, 0);
        }
        catch (Exception ex)
        {
            drView.EndEdit();
            dataTable.Table.RejectChanges();
        }
        finally
        {
            dataTable.Dispose();
        }

        if (string.IsNullOrEmpty(hdnLetter.Value))
            GetData();
        else
            GetData(hdnLetter.Value);
        BindGrid();
        this.GridView1.DataSource = this.DataSource;
        this.GridView1.DataBind();
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
        WebPager2.DataSource = this.DataSource;
        WebPager2.DataBind();
        UpdatePanel1.Update();
        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"An EdFile has been saved successfully.\")", true);
    }

    /// <summary>
    /// Handles the Click event of the LinkButton1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;
        string uid = Convert.ToString(this.GridView1.DataKeys[row.RowIndex].Value);

        DataView view = new DataView(this.DataSource as DataTable);
        view.RowFilter = "FolderID=" + uid;
        dvFolderDetail.DataSource = view;
        dvFolderDetail.DataBind();
        view.RowStateFilter = DataViewRowState.ModifiedCurrent;
        updPnlFolderDetail.Update();
        this.mdlPopup.Show();
    }

    protected void LinkButton2_Click(Object sender, CommandEventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;

        ((MasterPage)this.Master).AddFolderItem(e.CommandArgument.ToString());
    }

    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;
        string effid = Convert.ToString(this.GridView1.DataKeys[row.RowIndex].Value);
        HiddenField1.Value = effid;

        DataTable view = FileFolderManagement.GetDividerByEffid(Convert.ToInt32(effid));
        if (view != null && view.Rows.Count > 0)
        {
            view.Columns.Add("ColorWithoutHash");
            foreach (DataRow dr in view.Rows)
            {
                dr["ColorWithoutHash"] = Convert.ToString(dr["Color"]).Replace("#", "");
            }
        }
        GridView2.DataSource = view;

        GridView2.DataBind();
        UpdatePanel3.Update();
        ModalPopupExtender1.Show();
        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "LoadColorPicker()", true);
    }

    public DataTable GetTabsDataSource(string effid)
    {
        Divider dv = new Divider();
        ObjectQuery query = dv.CreateQuery();
        query.SetCriteria(dv.EffID, Convert.ToInt32(effid));

        DataTable table = new DataTable();

        try
        {
            query.Fill(table);

        }
        catch
        {
            table = null;
        }
        return table;
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (Sessions.HasViewPrivilegeOnly)
            {
                LinkButton libDownload = e.Row.FindControl("LinkButton21") as LinkButton;
                libDownload.Visible = false;
                LinkButton libDel = e.Row.FindControl("libDelete") as LinkButton;
                libDel.Visible = false;
                LinkButton libEdit = e.Row.FindControl("LinkButton1") as LinkButton;
                libEdit.Visible = false;
            }
            System.Data.DataRowView drvnew = e.Row.DataItem as DataRowView;
            string FileFolderID = drvnew.Row["FolderID"].ToString();
            if (!Common_Tatva.IsUserSwitched() && (this.Page.User.IsInRole("Users") && !Sessions.HasPrevilegeToDelete))
            {
                LinkButton libDel = e.Row.FindControl("libDelete") as LinkButton;
                libDel.Visible = false;
            }
            if (!Common_Tatva.IsUserSwitched() && (this.Page.User.IsInRole("Users") && !Sessions.HasPrevilegeToDownload))
            {
                LinkButton libDownload = e.Row.FindControl("LinkButton21") as LinkButton;
                libDownload.Visible = false;
            }

            var textBox = e.Row.Cells[7] as TableCell;
            if (textBox != null)
            {
                var rv = e.Row.DataItem as DataRowView;
                textBox.Text += Convert.ToString(rv.Row.ItemArray[9]);
            }

            DataTable dataLogForm = General_Class.GetLogFormDocs(Sessions.SwitchedRackspaceId, int.Parse(FileFolderID));
            LinkButton lnkBtnPreveiw1 = e.Row.FindControl("btnPreview1") as LinkButton;
            LinkButton lnkBtnPreveiw2 = e.Row.FindControl("btnPreview2") as LinkButton;
            LinkButton lnkBtnPreveiw3 = e.Row.FindControl("btnPreview3") as LinkButton;
            LinkButton lnkBtnPreveiw4 = e.Row.FindControl("btnPreview4") as LinkButton;
            LinkButton lnkBtnPreveiw5 = e.Row.FindControl("btnPreview5") as LinkButton;

            if (dataLogForm != null && dataLogForm.Rows.Count > 0)
            {
                if (lnkBtnPreveiw1 != null)
                {
                    lnkBtnPreveiw1.Attributes.Add("onclick", "ShowPreviewForRequestedFiles('" + dataLogForm.Rows[0]["ID"] + "', '" + dataLogForm.Rows[0]["FolderID"] + "');");
                    lnkBtnPreveiw1.Attributes.Add("style", "display:inline-block;");
                    lnkBtnPreveiw1.ToolTip = dataLogForm.Rows[0]["FileName"].ToString();
                }

                if (dataLogForm.Rows.Count > 1 && lnkBtnPreveiw2 != null)
                {
                    lnkBtnPreveiw2.Attributes.Add("onclick", "ShowPreviewForRequestedFiles('" + dataLogForm.Rows[1]["ID"] + "', '" + dataLogForm.Rows[1]["FolderID"] + "');");
                    lnkBtnPreveiw2.Attributes.Add("style", "display:inline-block;");
                    lnkBtnPreveiw2.ToolTip = dataLogForm.Rows[1]["FileName"].ToString();
                }
                else
                {
                    lnkBtnPreveiw2.Attributes.Add("style", "display:none");
                }

                if (dataLogForm.Rows.Count > 2 && lnkBtnPreveiw3 != null)
                {
                    lnkBtnPreveiw3.Attributes.Add("onclick", "ShowPreviewForRequestedFiles('" + dataLogForm.Rows[2]["ID"] + "', '" + dataLogForm.Rows[2]["FolderID"] + "');");
                    lnkBtnPreveiw3.Attributes.Add("style", "display:inline-block;");
                    lnkBtnPreveiw3.ToolTip = dataLogForm.Rows[2]["FileName"].ToString();
                }
                else
                {
                    lnkBtnPreveiw3.Attributes.Add("style", "display:none");
                }

                if (dataLogForm.Rows.Count > 3 && lnkBtnPreveiw4 != null)
                {
                    lnkBtnPreveiw4.Attributes.Add("onclick", "ShowPreviewForRequestedFiles('" + dataLogForm.Rows[3]["ID"] + "', '" + dataLogForm.Rows[3]["FolderID"] + "');");
                    lnkBtnPreveiw4.Attributes.Add("style", "display:inline-block;");
                    lnkBtnPreveiw4.ToolTip = dataLogForm.Rows[3]["FileName"].ToString();
                }
                else
                {
                    lnkBtnPreveiw4.Attributes.Add("style", "display:none");
                }

                if (dataLogForm.Rows.Count > 4 && lnkBtnPreveiw5 != null)
                {
                    lnkBtnPreveiw5.Attributes.Add("onclick", "ShowPreviewForRequestedFiles('" + dataLogForm.Rows[4]["ID"] + "', '" + dataLogForm.Rows[4]["FolderID"] + "');");
                    lnkBtnPreveiw5.Attributes.Add("style", "display:inline-block;");
                    lnkBtnPreveiw5.ToolTip = dataLogForm.Rows[4]["FileName"].ToString();
                }
                else
                {
                    lnkBtnPreveiw5.Attributes.Add("style", "display:none");
                }
            }
            else
            {
                lnkBtnPreveiw1.Attributes.Add("style", "display:none;");
                lnkBtnPreveiw2.Attributes.Add("style", "display:none;");
                lnkBtnPreveiw3.Attributes.Add("style", "display:none;");
                lnkBtnPreveiw4.Attributes.Add("style", "display:none;");
                lnkBtnPreveiw5.Attributes.Add("style", "display:none;");
            }
        }

    }

    protected void DropDownList1_DataBinding(object sender, EventArgs e)
    {
        string officeID1 = Sessions.SwitchedRackspaceId;
        Shinetech.DAL.Account officeUser = new Shinetech.DAL.Account(officeID1);
        string groupsName = officeUser.GroupsName.Value;

        //自定义Groups名称
        if (groupsName != null && !string.IsNullOrEmpty(groupsName.Trim()))
        {
            string[] groupNames = groupsName.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 1; i <= groupNames.Length; i++)
            {
                TextBox groupText = this.dvFolderDetail.FindControl("grouplink" + i) as TextBox;
                groupText.Text = groupNames[i - 1];
            }
        }

        //仅显示该User的Groups 的CheckBox
        if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users"))
        {
            //this.btnSave.Visible = false;
            var uid = Membership.GetUser().ProviderUserKey.ToString();
            var act = new Shinetech.DAL.Account(uid);
            string groups = act.Groups.Value == null ? "" : act.Groups.Value;

            if (groupsName != null && !string.IsNullOrEmpty(groupsName.Trim()))
            {
                string[] groupNames = groupsName.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 1; i <= groupNames.Length; i++)
                {
                    TextBox groupText = this.dvFolderDetail.FindControl("grouplink" + i) as TextBox;

                    groupText.Text = groupNames[i - 1];
                }
            }

            for (int i = 1; i <= 5; i++)
            {
                TextBox groupText = this.dvFolderDetail.FindControl("grouplink" + i) as TextBox;
                CheckBox groupfield = this.dvFolderDetail.FindControl("groupfield" + i) as CheckBox;

                groupText.Visible = groups.Contains(i.ToString());
                groupfield.Visible = groups.Contains(i.ToString());
                this.dvFolderDetail.FindControl("divfield" + i).Visible = groups.Contains(i.ToString());

            }

        }
        TextBox folderCode = this.dvFolderDetail.FindControl("folderCode") as TextBox;
        DropDownList DropDownList1 = this.dvFolderDetail.FindControl("DropDownList1") as DropDownList;
        Control trgroups = this.dvFolderDetail.FindControl("trgroups") as Control;
        Label label6 = this.dvFolderDetail.FindControl("label6") as Label;
        folderCode.Visible = true;
        trgroups.Visible = false;
        label6.Visible = false;
        DropDownList list = (DropDownList)sender;
        DetailsView dv = (DetailsView)list.NamingContainer;
        DataRowView drv = (DataRowView)dv.DataItem;
        list.SelectedIndex = (int)drv.Row["SecurityLevel"];
        folderCode.Visible = (list.SelectedIndex == 2);
        if (list.SelectedIndex == 0)
        {
            var uid = Sessions.SwitchedSessionId;
            var act = new Shinetech.DAL.Account(uid);
            string groups = act.Groups.Value == null ? "" : act.Groups.Value;
            label6.Visible = true;
            if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users"))
            {
                trgroups.Visible = (DropDownList1.SelectedValue == "0") && !string.IsNullOrEmpty(groups);
            }
            else
            {
                trgroups.Visible = (DropDownList1.SelectedValue == "0");
            }
            for (int i = 1; i <= 5; i++)
            {
                CheckBox groupBox = this.dvFolderDetail.FindControl("groupfield" + i) as CheckBox;
                if (drv.Row["Groups"].ToString().IndexOf(i.ToString()) != -1)
                {
                    groupBox.Checked = true;
                }
            }
        }
        else if (list.SelectedIndex == 2)
            folderCode.Text = drv.Row["FolderCode"].ToString();
    }

    protected void ddlStates_DataBinding(object sender, EventArgs e)
    {
        DropDownList list = (DropDownList)sender;

        DetailsView dv = (DetailsView)list.NamingContainer;
        DataRowView drv = (DataRowView)dv.DataItem;
        list.SelectedValue = drv.Row["StateID"] == DBNull.Value ? "AA" : (string)drv.Row["StateID"];
    }

    protected void Repeater2_OnItemCreated(object sender, DataListItemEventArgs e)
    {

    }

    protected void GridView2_OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView2.EditIndex = e.NewEditIndex;
        FillGridView();
    }

    public void FillGridView()
    {
        string effid = HiddenField1.Value;

        DataTable view = FileFolderManagement.GetDividerByEffid(Convert.ToInt32(effid));
        if (view != null && view.Rows.Count > 0)
        {
            view.Columns.Add("ColorWithoutHash");
            foreach (DataRow dr in view.Rows)
            {
                dr["ColorWithoutHash"] = Convert.ToString(dr["Color"]).Replace("#", "");
            }
        }
        GridView2.DataSource = view;
        GridView2.DataBind();

    }

    protected void GridView2_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        GridViewRow item = GridView2.Rows[e.RowIndex];
        TextBox txtBoxDOB = (TextBox)item.FindControl("textfield8");
        //string dtDOB = txtBoxDOB.Text;
        TextBox txtBoxName = (TextBox)item.FindControl("textfield10");
        string strName = txtBoxName.Text;

        ColorPickerExtender txtBoxColor = (ColorPickerExtender)item.FindControl("ColorPicker3");
        CheckBox checkBoxLocked = (CheckBox)item.FindControl("checkLocked");
        TextBox boxColor = (TextBox)txtBoxColor.FindControl(txtBoxColor.TargetControlID);

        int id = Convert.ToInt32(GridView2.DataKeys[e.RowIndex].Values[0].ToString());

        Divider objDivider = new Divider(id);
        if (objDivider.IsExist)
        {
            objDivider.Name.Value = strName.Trim();
            //objDivider.DOB.Value = DateTime.Parse(dtDOB);
            objDivider.Color.Value = boxColor.Text;
            objDivider.Locked.Value = checkBoxLocked.Checked;
            objDivider.Update();
        }

        GridView2.EditIndex = -1;
        FillGridView();
        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(HiddenField1.Value), id, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Edit.GetHashCode(), 0, 0);
    }

    protected void GridView2_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView2.EditIndex = -1;
        FillGridView();
    }

    protected void GridView2_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = Convert.ToInt32(GridView2.DataKeys[e.RowIndex].Values[0].ToString());

        string effid = HiddenField1.Value;
        DataTable dtFolder = new DataTable();
        dtFolder = FileFolderManagement.GetFolderInforByEFFID(effid);
        int tabNum = Convert.ToInt16(dtFolder.Rows[0]["OtherInfo"].ToString());
        if (tabNum <= 1)
        {
            return;
        }
        else
        {
            FileFolderManagement.DeleteDivider(id);
            Divider objDivider = new Divider(id);
            if (objDivider.IsExist)
            {
                //Here we do not need to pass switch users, because it is being deleted by LoggedIn User
                General_Class.DeleteDividerAndDocuments(Sessions.UserId, id, Enum_Tatva.Action.Delete.GetHashCode());
            }
            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(effid), id, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Delete.GetHashCode(), 0, 1);

            FillGridView();
            if (string.IsNullOrEmpty(hdnLetter.Value))
                GetData();
            else
                GetData(hdnLetter.Value);
            GridView1.DataSource = DataSource;
            GridView1.DataBind();
            BindGrid();
            IsShowInsert();
        }
    }

    protected void GridView2_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        switch (e.CommandName)
        {
            case "Insert":
                GridViewRow item = GridView2.FooterRow;
                TextBox txtBoxName = (TextBox)item.FindControl("txtNewName");
                string strName = txtBoxName.Text;

                if (strName.Trim().Equals(""))
                {
                    return;
                }
                ColorPickerExtender txtBoxColor = (ColorPickerExtender)item.FindControl("ColorPicker5");
                TextBox boxColor = (TextBox)txtBoxColor.FindControl(txtBoxColor.TargetControlID);

                CheckBox checkBoxLocked = (CheckBox)item.FindControl("txtLocked");

                string effid = HiddenField1.Value;


                Divider objDivider = new Divider();
                objDivider.Name.Value = strName;
                objDivider.Color.Value = boxColor.Text;
                objDivider.Locked.Value = checkBoxLocked.Checked;
                objDivider.EffID.Value = Convert.ToInt32(effid);
                objDivider.OfficeID.Value = Sessions.SwitchedSessionId;
                bool isExist = General_Class.IsExistDividerForFolder(0, Convert.ToInt32(HiddenField1.Value), strName.Trim());
                if (!isExist)
                {
                    int dividerId = FileFolderManagement.InsertDivider(objDivider);
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(effid), dividerId, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Create.GetHashCode(), 0, 0);
                }
                FillGridView();

                break;
            case "Edit":
                break;
            case "Delete":
                break;
        }
    }

    protected void Move(string sourcePath, string destinationPath)
    {
        try
        {
            System.IO.Directory.Move(sourcePath, destinationPath);
            System.Console.WriteLine("The directory move is complete.");
        }
        catch (ArgumentNullException)
        {
            System.Console.WriteLine("Path is a null reference.");
        }
        catch (System.Security.SecurityException)
        {
            System.Console.WriteLine("The caller does not have the " +
                "required permission.");
        }
        catch (ArgumentException)
        {
            System.Console.WriteLine("Path is an empty string, " +
                "contains only white spaces, " +
                "or contains invalid characters.");
        }
        catch (System.IO.IOException)
        {
            System.Console.WriteLine("An attempt was made to move a " +
                "directory to a different " +
                "volume, or destDirName " +
                "already exists.");
        }
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        string password = textPassword.Text.Trim();
        string strPassword = PasswordGenerator.GetMD5(password);

        if (UserManagement.GetPassword(User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);

        }
        else
        {
            //不用移动或者备份到其它目录
            string folderID = string.Empty;
            try
            {
                Shinetech.DAL.Account user = new Shinetech.DAL.Account(Sessions.SwitchedSessionId);

                if ((user.IsSubUser.ToString() == "1" && user.HasPrivilegeDelete.Value) || user.IsSubUser.ToString() == "0")
                {
                    string strFolderID = GridView1.DataKeys[e.RowIndex].Value.ToString();
                    folderID = strFolderID;

                    Dictionary<bool, string> result = FileFolderManagement.MoveDeletedFolder(strFolderID);
                    if (result.ContainsKey(true))
                    {
                        FileFolder folder = new FileFolder(Convert.ToInt32(strFolderID));
                        General_Class.AuditLogByFolderId(Convert.ToInt32(strFolderID), Sessions.SwitchedSessionId, Enum_Tatva.Action.Delete.GetHashCode());
                        string path = string.Empty;
                        result.TryGetValue(true, out path);
                        FileFolderManagement.RecycleFolderByID(strFolderID, Membership.GetUser().ProviderUserKey.ToString(), path.Substring(path.IndexOf('/') + 1));
                        if (user.IsSubUser.ToString() == "1")
                        {
                            Shinetech.DAL.Account officeUser = new Shinetech.DAL.Account(user.OfficeUID.ToString());
                            FlashViewer.HtmlViewer_SendMailForDeleteFolder(officeUser.Email.ToString(), folder.FolderName.Value, user.Name.Value, officeUser.Name.Value);
                        }
                        else
                            FlashViewer.HtmlViewer_SendMailForDeleteFolder(ConfigurationManager.AppSettings["AJIWANIEMAIL"], folder.FolderName.Value, user.Name.Value, "Admin");
                        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(strFolderID), 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Delete.GetHashCode(), 0, 0);
                        if (string.IsNullOrEmpty(hdnLetter.Value))
                            GetData();
                        else
                            GetData(hdnLetter.Value);
                        BindGrid();
                    }
                    else
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alert", "alert('Failed to delete EdFile! Please contact your Administratior.');", true);
                }
            }
            catch (Exception ex)
            {
                Common_Tatva.WriteErrorLog(folderID, string.Empty, string.Empty, ex);
            }

        }
    }

    protected void GridView2_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users") && !Sessions.HasPrevilegeToDelete)
        {
            LinkButton linkBtn = e.Row.FindControl("LinkButton3") as LinkButton;
            if (linkBtn != null)
            {
                linkBtn.Visible = false;
            }
        }
        IsShowInsert();
    }

    protected void btnClose_Click(object sender, EventArgs e)
    {
        //LinkButton6.CssClass = LinkButton6.CssClass.Replace("active", "");
        //LinkButton7.CssClass = LinkButton7.CssClass.Replace("active", "");
        //LinkButton8.CssClass = LinkButton8.CssClass.Replace("active", "");
        //LinkButton9.CssClass = LinkButton9.CssClass.Replace("active", "");
        //LinkButton10.CssClass = LinkButton10.CssClass.Replace("active", "");
        //LinkButton11.CssClass = LinkButton11.CssClass.Replace("active", "");
        //LinkButton12.CssClass = LinkButton12.CssClass.Replace("active", "");
        //LinkButton13.CssClass = LinkButton13.CssClass.Replace("active", "");
        //LinkButton14.CssClass = LinkButton14.CssClass.Replace("active", "");
        //LinkButton15.CssClass = LinkButton15.CssClass.Replace("active", "");
        //LinkButton16.CssClass = LinkButton16.CssClass.Replace("active", "");
        //LinkButton17.CssClass = LinkButton17.CssClass.Replace("active", "");
        //LinkButton18.CssClass = LinkButton18.CssClass.Replace("active", "");
        //LinkButton19.CssClass = LinkButton19.CssClass.Replace("active", "");
        //LinkButton20.CssClass = LinkButton20.CssClass.Replace("active", "");
        //LinkButton23.CssClass = LinkButton23.CssClass.Replace("active", "");
        //LinkButton24.CssClass = LinkButton24.CssClass.Replace("active", "");
        //LinkButton25.CssClass = LinkButton25.CssClass.Replace("active", "");
        //LinkButton26.CssClass = LinkButton26.CssClass.Replace("active", "");
        //LinkButton27.CssClass = LinkButton27.CssClass.Replace("active", "");
        //LinkButton28.CssClass = LinkButton28.CssClass.Replace("active", "");
        //LinkButton29.CssClass = LinkButton29.CssClass.Replace("active", "");
        //LinkButton30.CssClass = LinkButton30.CssClass.Replace("active", "");
        //LinkButton31.CssClass = LinkButton31.CssClass.Replace("active", "");
        //LinkButton32.CssClass = LinkButton32.CssClass.Replace("active", "");
        //LinkButton33.CssClass = LinkButton33.CssClass.Replace("active", "");
        //WebPager1.CurrentPageIndex = 0;
        //backFromLetters.Visible = false;
        //hdnLetter.Value = "";
        if (string.IsNullOrEmpty(hdnLetter.Value))
            GetData();
        else
            GetData(hdnLetter.Value);
        BindGrid();
        UpdatePanel1.Update();
    }

    public void IsShowInsert()
    {
        string effid = HiddenField1.Value;
        DataTable dtFolder = new DataTable();
        dtFolder = FileFolderManagement.GetFolderInforByEFFID(effid);
        int tabNum = Convert.ToInt16(dtFolder.Rows[0]["OtherInfo"].ToString());
        if (tabNum >= 12)
        {
            GridView2.ShowFooter = false;
        }
        else
        {
            GridView2.ShowFooter = true;
        }
    }

    protected void FolderDetail_OnDataBound(object sender, EventArgs e)
    {
        if (dvFolderDetail.Rows.Count > 0)
        {
            string txtFolderName = "ctl01";
            ((TextBox)dvFolderDetail.Rows[0].Cells[1].Controls[0]).ID = txtFolderName;
            ((TextBox)dvFolderDetail.Rows[0].Cells[1].Controls[0]).MaxLength = 50;
            RequiredFieldValidator v_efn = new RequiredFieldValidator();
            v_efn.ControlToValidate = txtFolderName;
            v_efn.Display = ValidatorDisplay.None;
            v_efn.ErrorMessage = "<b>Required Field Missing</b><br />Folder name is required.";
            v_efn.ID = Guid.NewGuid().ToString();
            ValidatorCalloutExtender vce = new ValidatorCalloutExtender();
            vce.ID = Guid.NewGuid().ToString();
            vce.TargetControlID = v_efn.ID;
            dvFolderDetail.Rows[0].Cells[1].Controls.Add(v_efn);
            dvFolderDetail.Rows[0].Cells[1].Controls.Add(vce);

            string txtFirstName = "ctl02";
            ((TextBox)dvFolderDetail.Rows[1].Cells[1].Controls[0]).ID = txtFirstName;
            ((TextBox)dvFolderDetail.Rows[1].Cells[1].Controls[0]).MaxLength = 50;
            RequiredFieldValidator v_efirstName = new RequiredFieldValidator();
            v_efirstName.ControlToValidate = txtFirstName;
            v_efirstName.Display = ValidatorDisplay.None;
            v_efirstName.ErrorMessage = "<b>Required Field Missing</b><br />First name is required.";
            v_efirstName.ID = Guid.NewGuid().ToString();
            ValidatorCalloutExtender vfirstNameE = new ValidatorCalloutExtender();
            vfirstNameE.ID = Guid.NewGuid().ToString();
            vfirstNameE.TargetControlID = v_efirstName.ID;
            dvFolderDetail.Rows[1].Cells[1].Controls.Add(v_efirstName);
            dvFolderDetail.Rows[1].Cells[1].Controls.Add(vfirstNameE);

            string txtLastName = "ctl03";
            ((TextBox)dvFolderDetail.Rows[2].Cells[1].Controls[0]).ID = txtLastName;
            ((TextBox)dvFolderDetail.Rows[2].Cells[1].Controls[0]).MaxLength = 50;
            RequiredFieldValidator v_eLastName = new RequiredFieldValidator();
            v_eLastName.ControlToValidate = txtLastName;
            v_eLastName.Display = ValidatorDisplay.None;
            v_eLastName.ErrorMessage = "<b>Required Field Missing</b><br />Last name is required.";
            v_eLastName.ID = Guid.NewGuid().ToString();
            ValidatorCalloutExtender vLastNameE = new ValidatorCalloutExtender();
            vLastNameE.ID = Guid.NewGuid().ToString();
            vLastNameE.TargetControlID = v_eLastName.ID;
            dvFolderDetail.Rows[2].Cells[1].Controls.Add(v_eLastName);
            dvFolderDetail.Rows[2].Cells[1].Controls.Add(vLastNameE);

            DetailsView dv = (DetailsView)dvFolderDetail;
            DataRowView drv = (DataRowView)dv.DataItem;

            DropDownList list = (DropDownList)dvFolderDetail.Rows[6].Cells[1].Controls[1];
            list.DataSource = this.States;
            list.DataTextField = "StateName";
            list.DataValueField = "StateID";
            list.DataBind();
            list.SelectedValue = drv.Row["StateID"] == DBNull.Value ? "AK" : (string)drv.Row["StateID"];
        }

        DetailsView view = sender as DetailsView;

        int keyvar = (int)view.DataKey.Value;
        string keyvar1 = (string)view.DataKey[1].ToString();

        DropDownList control = this.dvFolderDetail.FindControl("DropDownList1") as DropDownList;

        //Client said to remove restriction
        //if (keyvar1.Equals(Sessions.SwitchedSessionId) || User.IsInRole("Offices") || txtHasPrivilage.Value.Equals("TRUE"))
        ////if (keyvar1.Equals(Membership.GetUser().ProviderUserKey.ToString()) || User.IsInRole("Offices") || txtHasPrivilage.Value.Equals("TRUE"))
        //{
        //    control.Enabled = true;
        //}
        //else
        //{
        //    control.Enabled = false;
        //}
    }

    protected void LinkButon_Letter_Click(object sender, EventArgs e)
    {
        LinkButton button = sender as LinkButton;

        LinkButton6.CssClass = LinkButton6.CssClass.Replace("active", "");
        LinkButton7.CssClass = LinkButton7.CssClass.Replace("active", "");
        LinkButton8.CssClass = LinkButton8.CssClass.Replace("active", "");
        LinkButton9.CssClass = LinkButton9.CssClass.Replace("active", "");
        LinkButton10.CssClass = LinkButton10.CssClass.Replace("active", "");
        LinkButton11.CssClass = LinkButton11.CssClass.Replace("active", "");
        LinkButton12.CssClass = LinkButton12.CssClass.Replace("active", "");
        LinkButton13.CssClass = LinkButton13.CssClass.Replace("active", "");
        LinkButton14.CssClass = LinkButton14.CssClass.Replace("active", "");
        LinkButton15.CssClass = LinkButton15.CssClass.Replace("active", "");
        LinkButton16.CssClass = LinkButton16.CssClass.Replace("active", "");
        LinkButton17.CssClass = LinkButton17.CssClass.Replace("active", "");
        LinkButton18.CssClass = LinkButton18.CssClass.Replace("active", "");
        LinkButton19.CssClass = LinkButton19.CssClass.Replace("active", "");
        LinkButton20.CssClass = LinkButton20.CssClass.Replace("active", "");
        LinkButton23.CssClass = LinkButton23.CssClass.Replace("active", "");
        LinkButton24.CssClass = LinkButton24.CssClass.Replace("active", "");
        LinkButton25.CssClass = LinkButton25.CssClass.Replace("active", "");
        LinkButton26.CssClass = LinkButton26.CssClass.Replace("active", "");
        LinkButton27.CssClass = LinkButton27.CssClass.Replace("active", "");
        LinkButton28.CssClass = LinkButton28.CssClass.Replace("active", "");
        LinkButton29.CssClass = LinkButton29.CssClass.Replace("active", "");
        LinkButton30.CssClass = LinkButton30.CssClass.Replace("active", "");
        LinkButton31.CssClass = LinkButton31.CssClass.Replace("active", "");
        LinkButton32.CssClass = LinkButton32.CssClass.Replace("active", "");
        LinkButton33.CssClass = LinkButton33.CssClass.Replace("active", "");

        if (button != null)
        {
            hdnLetter.Value = button.Text.Trim();
            this.GetData(button.Text.Trim());
            this.BindGrid();
            if (WebPager1.CurrentPageIndex == 0)
            {
                backFromLetters1.Visible = false;
                backFromLetters2.Visible = false;
            }
            else
            {
                backFromLetters1.Visible = true;
                backFromLetters2.Visible = true;
            }
            button.CssClass = "active";
        }
    }
    protected void lnkFolderLogs_Command(object sender, CommandEventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
    }

    protected void LinkButton21_Click(object sender, EventArgs e)
    {
        string password = txtBoxArchivePwd.Text.Trim();
        string strPassword = PasswordGenerator.GetMD5(password);

        if (UserManagement.GetPassword(User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);
        }
        else
        {
            LinkButton btnLink = sender as LinkButton;
            if (btnLink == null) return;
            GridViewRow row = (GridViewRow)btnLink.NamingContainer;
            if (row == null) return;
            string effid = Convert.ToString(this.GridView1.DataKeys[row.RowIndex].Value);
            Response.Redirect(string.Format("~/Office/ArchiveDownloader.aspx?ID={0}", effid), true);
            return;
        }


    }

    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {

        Shinetech.DAL.Account user = new Shinetech.DAL.Account(Sessions.SwitchedSessionId);
        //Shinetech.DAL.Account user = new Shinetech.DAL.Account(Membership.GetUser().ProviderUserKey.ToString());

        if (user.IsSubUser.Value.ToString() == "1")
        {
            //for (int i = 0; i < GridView1.Columns.Count; i++)
            //{
            //    if (GridView1.Columns[i].HeaderText == "Tabs")
            //    {
            //        GridView1.Columns[i].Visible = false;
            //        break;
            //    }
            //}

        }

    }
    protected void backFromLetters_Click(object sender, EventArgs e)
    {
        WebPager1.CurrentPageIndex = WebPager1.CurrentPageIndex - 1;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
        WebPager2.CurrentPageIndex = WebPager2.CurrentPageIndex - 1;
        WebPager2.DataSource = this.DataSource;
        WebPager2.DataBind();
        if (WebPager1.CurrentPageIndex == 0)
        {
            backFromLetters1.Visible = false;
            backFromLetters2.Visible = false;
        }
    }
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        TextBox folderCode = this.dvFolderDetail.FindControl("folderCode") as TextBox;
        DropDownList DropDownList1 = this.dvFolderDetail.FindControl("DropDownList1") as DropDownList;
        Control trgroups = this.dvFolderDetail.FindControl("trgroups") as Control;
        TextBox groupText = dvFolderDetail.FindControl("grouplink1") as TextBox;
        Label label6 = this.dvFolderDetail.FindControl("label6") as Label;
        if (folderCode != null && DropDownList1 != null && trgroups != null && label6 != null)
        {
            folderCode.Visible = (DropDownList1.SelectedValue == "2");

            var uid = Sessions.SwitchedSessionId;
            //var uid = Membership.GetUser().ProviderUserKey.ToString();
            var act = new Shinetech.DAL.Account(uid);
            string groups = act.Groups.Value == null ? "" : act.Groups.Value;

            if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users"))
            {
                trgroups.Visible = (DropDownList1.SelectedValue == "0") && !string.IsNullOrEmpty(groups);
                label6.Visible = (DropDownList1.SelectedValue == "0") && !string.IsNullOrEmpty(groups);
            }
            else
            {
                trgroups.Visible = (DropDownList1.SelectedValue == "0");
                label6.Visible = (DropDownList1.SelectedValue == "0");
            }
        }
    }
    protected void test1_Click(object sender, EventArgs e)
    {

    }

    /// <summary>
    /// Sets the folder identifier source.
    /// </summary>
    private void SetFolderIdSource()
    {
        FolderIdSource = string.Empty;
        foreach (DataRow drRow in this.DataSource.Rows)
        {
            FolderIdSource += (drRow["FolderID"]) + ", ";
        }
    }
}
