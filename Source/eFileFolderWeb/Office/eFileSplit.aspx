﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="eFileSplit.aspx.cs" Inherits="Office_eFileSplit" %>

<%--<%@ Register Src="../Controls/FolderReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>--%>
<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>

<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%--<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>--%>
<%@ Register Src="~/Office/UserControls/ucAddToTab.ascx" TagPrefix="uc1" TagName="ucAddToTab" %>
<%@ Register Src="~/Office/UserControls/ucWorkArea.ascx" TagPrefix="uc1" TagName="ucWorkArea" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <%--<link rel="stylesheet" href="../New/css/DocumentPreview/viewer.css" />--%>
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/colorbox.css" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>css/main.css?v=2" rel="stylesheet" />
    <link rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/style.min.css" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/jquery.colorbox-min.js" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/edFiles/EdFilesLogs.js?v=4" type="text/javascript"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>extras/jquery-ui-1.8.22.custom.min.js"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"] %>New/js/chosen.jquery.js" type="text/javascript"></script>
    <%--<script src="../New/js/PdfJS/thumbnail.js"></script>--%>

    <style type="text/css">
        .selecttabcheck {
            height: 37px;
            background-color: #83af33;
            float: right;
            max-width: 200px;
            width: auto;
        }

        #btnCheckedAll {
            float: right;
            margin-right: 3px;
            margin-top: 9px;
            height: 18px;
            width: 18px;
        }

        .selectalltab {
            font-size: 18px;
            float: right;
            padding: 5px;
            font-family: 'Open Sans', sans-serif !important;
            color: #fff;
            font-weight: 700;
            margin-right: 5px;
        }

        .btnAddtoTab, .btnAddtoWorkArea {
            padding: 8px 10px 8px 35px;
            font-size: 15px;
            padding-right: 10px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 5px;
            background-image: url(../images/plus-icon.png);
            background-position: 7px center;
            background-repeat: no-repeat;
        }

        #btnCheckedAll {
            float: right;
            margin-right: 3px;
            margin-top: 9px;
            height: 18px;
            width: 18px;
        }

        .selectalltab {
            font-size: 18px;
            float: right;
            padding: 5px;
            font-family: 'Open Sans', sans-serif !important;
            color: #fff;
            font-weight: 700;
            margin-right: 5px;
        }

        .white_content {
            display: none;
            position: fixed;
            top: 20%;
            left: 20%;
            width: 60%;
            height: 50%;
            padding: 16px;
            background-color: white;
            z-index: 999999999;
            overflow: auto;
        }

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }

        .actionMinWidth {
            min-width: 50px;
        }

        td span {
            -ms-word-break: break-all;
            word-break: break-all;
            word-break: break-word;
            -webkit-hyphens: auto;
            -moz-hyphens: auto;
            hyphens: auto;
            max-width: 300px;
            white-space: normal !important;
        }

        td {
            max-width: 250px;
        }

        .boxWidth {
            width: 1100px;
            margin: auto;
        }

        .paging_position {
            position: relative;
            left: 365px;
        }

        select {
            /*border: solid 1px #c4c4c4;*/
            height: 30px;
            line-height: 30px;
            padding: 5px;
            max-width: 230px;
            /*width: 100%;*/
        }

        #popupclose {
            float: right;
            padding: 10px;
            cursor: pointer;
        }

        #popupclose_split {
            float: right;
            padding: 10px;
            cursor: pointer;
        }

        .popupcontent {
            padding: 10px;
        }

        .downloadFile {
            margin-left: -10px;
            margin-top: -2px;
            cursor: pointer;
        }

        .preview-popup {
            width: 100%;
            max-width: 1250px;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 10%;
            right: 0;
            margin: auto;
            background: #fff;
            height: 85%;
        }

        .preview-popup-split {
            width: 100%;
            max-width: 600px;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 10%;
            right: 0;
            margin: auto;
            background: #fff;
            height: 50%;
        }

        .selectBox-outer {
            position: relative;
            display: inline-block;
            margin-right: 5px;
            font-family: 'open sans', sans-serif;
        }

        .selectBox select {
            width: 100%;
        }

        .overSelect {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
            font-size: 15px;
        }

        #drpGroups_chosen {
            display: block;
            margin-top: 5px;
        }

        #drpGroups_chosen {
            display: block;
            margin-top: 5px;
        }

        .chosen-results {
            max-height: 200px !important;
        }

        .chosen-container {
            font-size: 14px;
        }

        .ui-dialog {
            top: 300px !important;
        }

        .chosen-container-single .chosen-single {
            color: rgb(0,0,0);
        }

        #toolbarViewerLeft {
            width: inherit !important;
        }

        #thumnailImages .thumnail-outer {
            margin-bottom: 20px;
        }

            #thumnailImages .thumnail-outer span {
                float: left;
            }

            #thumnailImages .thumnail-outer .check {
                float: right;
            }

            #thumnailImages .thumnail-outer > img {
                display: block;
            }

        .ajax__fileupload_dropzone {
            font-size: 15px;
            line-height: 120px !important;
            height: 120px !important;
            font-family: "Open Sans", sans-serif;
        }

        .selected .thumbnailSelectionRing .thumbnailImage {
            border: 5px solid darkgray !important;
        }

        canvas {
            height: 190px;
            width: 200px;
        }
    </style>
    <script type="text/jscript">
        var docId, fileName, pathForSharemail, selectedButton = "";
        var pubExpanded = false;
        var SplitValue = new Array();
        var userId = "<%= Sessions.SwitchedRackspaceId%>";

        function pageLoad() {
            $(".ajax__fileupload_dropzone").text("Drag and Drop Pdf file(s) here.");
            $("#btnCheckedAll").attr("checked", false);

            if (LoadFolders)
                LoadFolders();

            $('.editDocument').click(function (e) {
                e.preventDefault();
                removeAllEditable();
                showEdit($(this));
            });

            $('.cancelDocument').click(function (e) {
                e.preventDefault();
                removeAllEditable();
            });

            $('.shareDocument').click(function (e) {
                e.preventDefault();
            });

            var closePopup = document.getElementById("popupclose");
            closePopup.onclick = function () {
                var popup = document.getElementById("preview_popup");
                var overlay = document.getElementById("overlay");
                overlay.style.display = 'none';
                popup.style.display = 'none';

                $('#floatingCirclesG').css('display', 'block');
                enableScrollbar();
                $('#reviewContent').removeAttr('src');
            };

            //var closePopup_split = document.getElementById("popupclose_split");
            //closePopup_split.onclick = function () {
            //    var popupSplit = document.getElementById("preview_popup_split");
            //    var overlay = document.getElementById("overlay");
            //    overlay.style.display = 'none';
            //    popupSplit.style.display = 'none';
            //};

            $('#popupclose_split, #btnCancelAddToTab, #popupclose_splitWA, #btnCancelAddToWA').click(function (e) {
                e.preventDefault();
                $(this).closest('.popup-mainbox').hide();
                $('#overlay').hide();
                enableScrollbar();
            });

        }

        function ShowConfirm(message, isFileExist) {
            if (confirm(message)) {
                if (isFileExist)
                    ShowPreviewForEFileSplit($("#<%= hdnFileId.ClientID%>").val(), $("#<%= hdnFileName.ClientID%>").val());
                else
                    window.location.href = window.location.href;
            }
            else {
                if ($('#<%= hdnSelectedFolderID.ClientID%>').val() == "" || $('#<%= hdnSelectedFolderID.ClientID%>').val() == undefined) {
                    window.location.href = window.location.origin + '/Office/WorkArea.aspx';
                }
                else {
                    ClearFolderIdSource();
                    window.location.href = window.location.origin + '/Office/FileFolderBox.aspx?id=' + $('#<%= hdnSelectedFolderID.ClientID%>').val();
                }
            }
        }

        function ClearFolderIdSource() {
            $.ajax(
                    {
                        type: "POST",
                        url: '../EFileFolderJSONService.asmx/ClearFolderIdSource',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json"
                    });
        } 

        function ValidateFilename() {
            if ($('#<%= txtNewPdfNameWA.ClientID %>').val() == "") {
                alert("Please enter new filename !!!");
                return false;
            }
            return true;
        }

        function ShowConfirmWA(message, isFileExist) {
            if (confirm(message)) {
                if (isFileExist)
                    ShowPreviewForEFileSplit($("#<%= hdnFileId.ClientID%>").val(), $("#<%= hdnFileName.ClientID%>").val());
                else
                    window.location.href = window.location.href;
            }
            else {
                window.location.href = window.location.origin + '/Office/WorkArea.aspx?';
            }
        }

        $(document).ready(function () {
            $("#<%= btnSaveSplitPdf.ClientID%>").click(function () {
            var fileName = $("#" + '<%= txtNewPdfName.ClientID%>').val();
            if (fileName == '' || fileName == undefined) {
                alert("Please enter file name!");
                return false;
            }
            var selectedFolder = $("#combobox").chosen().val();
            var selectedDivider = $("#comboboxDivider").chosen().val();
            if (selectedFolder == "" || selectedFolder == undefined || selectedFolder == "0") {
                alert("Please select folder");
                return false;
            }
            else if (selectedDivider == "" || selectedDivider == undefined || selectedDivider == "0") {
                alert("Please select divider");
                return false;
            }
            var folderValue = $('.chosen-single').text();
            $('#<%= hdnSelectedFolderID.ClientID%>').val(selectedFolder);
            $('#<%= hdnSelectedDividerID.ClientID%>').val(selectedDivider);
            $('#<%= hdnSelectedFolderText.ClientID%>').val(folderValue);
            return true;
        });

        $('#<%= btnAddToWA.ClientID%>').click(function (e) {
            if ($('#<%= txtNewPdfNameWA.ClientID %>').val() == "") {
                    alert("Please enter new filename !!!");
                    return false;
                }
                var isLeaf = false;
                var nodes = $("#jstree1").jstree(true).get_selected("full", true);
                if (nodes && nodes.length > 0) {
                    var path = "", treeId = "";
                    if (nodes[0].text.indexOf('.') > -1)
                        isLeaf = true;

                    if (!isLeaf &&
                        nodes && nodes.length > 0 && nodes[0].li_attr && nodes[0].li_attr.path) {
                        $('#overlay').css('z-index', '100010');
                        path = nodes[0].li_attr.path;
                        treeId = nodes[0].li_attr.name;
                        $('#<%=hdnPath.ClientID%>').val(path);
                        $('#<%=hdnTreeId.ClientID%>').val(treeId);
                        return true;
                    }
                }
                path = "workArea";
                $('#<%=hdnPath.ClientID%>').val(path);
                $('#<%=hdnTreeId.ClientID%>').val(0);
                return true;
            });

            $("#ButtonDeleleOkay").live({
                click: function () {
                    showLoader();
                }
            });

            $('.ui-button-text').each(function (i) {
                $(this).html($(this).parent().attr('text'))
            })

            $('.iframe').colorbox({
                iframe: true, width: "80%", height: "95%", onComplete: function () {
                    //iframe: true, width: aWidth, height: aHeight, onComplete: function () {
                    $('iframe').live('load', function () {
                        $('iframe').contents().find("head")
                            .append($("<style type='text/css'>  .splash{overflow:visible;}  </style>"));
                    });
                }
            });

        });

        $(document).on('click', '.check', function () {
            var isChecked = true;

            $('.check').each(function (k, v) {
                isChecked = $(v).is(":checked");
                if (!isChecked) {
                    return false;
                }
            });
            if (isChecked)
                $("#btnCheckedAll").attr('checked', true);
            else
                $("#btnCheckedAll").attr('checked', false);
        });

        function showPreview(URL) {
            hideLoader();
            disableScrollbar();
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//"
                    + window.location.hostname
                    + (window.location.port ? ':' + window.location.port : '');
            }

            URL = URL.replace('\\', '/');
            src = './Preview.aspx?data=' + window.location.origin + "/" + URL.trim() + "?v=" + "<%= DateTime.Now.Ticks%>";
            var popup = document.getElementById("preview_popup");
            var overlay = document.getElementById("overlay");
            $('#reviewContent').attr('src', src.trim());
            overlay.style.display = 'block';
            popup.style.display = 'block';
            $('#floatingCirclesG').css('display', 'none');
            $('#preview_popup').focus();
            return false;
        }

        function disableScrollbar() {
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }

        function enableScrollbar() {
            $('html, body').css({
                'overflow': 'auto',
                'height': 'auto'
            });
        }

        function ShowPreviewForEFileSplit(Id, fileName) {
            showLoader();
            $.ajax({
                type: "POST",
                url: '../Office/eFileSplit.aspx/GetPreviewURL',
                contentType: "application/json; charset=utf-8",
                data: "{ documentID:'" + Id + "', fileName:'" + fileName + "'}",
                dataType: "json",
                success: function (data) {
                    if (data.d == "") {
                        hideLoader();
                        disableScrollbar();
                        alert("Failed to get document!");
                        return false;
                    }
                    else {
                        showPreview("" + data.d);
                        $("#" + '<%= hdnFileId.ClientID %>').val(Id);
                        $("#" + '<%= hdnFileName.ClientID %>').val(fileName);
                        $('#preview_popup').css("max-width", "1200px");
                    }
                },
                error: function (result) {
                    console.log('Failed' + result.responseText);
                    hideLoader();
                }
            });
            return false;
        }


        function GetThumbnail(fileName, ID) {

            showLoader();
            $("#" + '<%= hdnFileId.ClientID %>').val(ID);
            $("#" + '<%= hdnFileName.ClientID %>').val(fileName);
            $("#thumnailImages").html("");
            $.ajax({
                type: "POST",
                url: '../Office/eFileSplit.aspx/GetThumbnail',
                contentType: "application/json; charset=utf-8",
                data: "{ docId:'" + ID + "',fileName:'" + fileName + "'}",
                dataType: "json",
                success: function (data) {
                    if (data.d == "") {
                        hideLoader();
                        disableScrollbar();
                        alert("Failed to get Thumbnails!");
                        return false;
                    }
                    else {
                        //showPreview(data.d);
                        if (data.d.length > 0) {
                            var html = "";
                            data.d.forEach(function (v, k) {
                                html += '<div class="thumnail-outer"><input type="checkbox" class="check" data-value="' + (k + 1) + '"/> <span>' + (k + 1) + '</span><div class="clearfix"></div> <img src=' + v + '></div>';
                            });
                            $("#thumnailImages").html(html);
                        }
                    }

                },
                error: function (result) {
                    console.log('Failed' + result.responseText);
                    hideLoader();
                }
            });
            return false;
        }

        function checkAllThumbnail() {

            var selectAllValue = $("#btnCheckedAll").is(":checked");

            $('iFrame').contents().find('.check').each(function (k, v) {
                if (selectAllValue)
                    $(v).attr('checked', true);
                else
                    $(v).attr('checked', false);
            });
        }

        function ShowAddToTabPopup() {
            if (GetSplitPdfValue()) {
                var popup = document.getElementById("preview_popup");
                popup.style.display = 'none';

                var popup = document.getElementById("preview_popup_split");
                popup.style.display = 'block';
            }
        }

        function ShowAddtoWAPopup() {
            if (GetSplitPdfValue()) {
                var popup = document.getElementById("preview_popup");
                popup.style.display = 'none';

                var popup = document.getElementById("preview_popup_split_WA");
                popup.style.display = 'block';
                //if (loadWorkTree())
                //    loadWorkTree();
            }
        }

        function GetSplitPdfValue() {
            var pageNumber = '';
            if ($('iFrame').contents().find('.check').length > 0) {
                $('iFrame').contents().find('.check').each(function (k, v) {
                    var val = $(v).is(":checked");
                    if (val) {
                        if (pageNumber == '')
                            pageNumber += $(v).attr('data-value') + ',';
                        else
                            pageNumber += $(v).attr('data-value') + ',';
                    }

                });
                pageNumber = pageNumber.substring(0, pageNumber.length - 1);

                if (pageNumber.length <= 0) {
                    alert("Please select at least one page.");
                    return false;
                }

                $("#" + '<%= hdnPageNumbers.ClientID %>').val(pageNumber);
            }
            else {
                alert('Please select thumbnail');
                return false;
            }
            return true;
        }

        function removeAllEditable() {
            $('.txtDocumentName,.saveDocument,.cancelDocument').hide();
            $('.lblDocumentName,.editDocument').show();
        }

        function toggle(source) {
            var checkboxes = $("#groupList").find("input[type=checkbox]");
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i] != source)
                    checkboxes[i].checked = source.checked;
            }
        }

        function onClientUploadComplete(sender, e) {
            return false;
        }

        var myTimer;
        var fileIDDividerId = [];
        function onClientUploadStart(sender, e) {
            document.getElementById('uploadCompleteInfo').innerHTML = "please wait while the " + e.get_filesInQueue() + " file is uploaded..";
            document.cookie = "Files=" + JSON.stringify(fileIDDividerId);
            showLoader();

            $('[id*=ajaxfileuploadeFileFlow_Html5DropZone]').addClass('hideControl').css('display', 'none');
            $('[id*=ajaxfileuploadeFileFlow_SelectFileContainer]').addClass('hideControl').css('display', 'none');
            $('[id*=FileItemDeleteButton]').addClass('hideControl').css('display', 'none');
            $('[id*=UploadOrCancelButton]').addClass('hideControl').css('display', 'none');
            $('.TabDropDown').prop('disabled', 'disabled');
            $('#uploadProgress').html('');
            $('#uploadProgress').html('').html($('#divUpload').html());

            document.getElementById('uploadProgress').style.display = 'block';
            myTimer = setInterval(function () {
                $('#uploadProgress').html('').html($('#divUpload').html());
            }, 1000);

        }

        function showLoader() {
            $('#overlay').show().height($(document).height());
            $('#floatingCirclesG').css('left', '50%').css('top', ($(window).scrollTop() + ($(window).height() / 2) - 10) + 'px');
        }

        function hideLoader() {
            $('#overlay').hide();
        }

        function onClientUploadCompleteAll(sender, e) {
            $('.hideControl').removeClass('hideControl');
            $('.uploadProgress').find('select').removeAttr('disabled');

            //$('#divUpload').clone().appendTo('#uploadFieldset');
            clearInterval(myTimer);
            document.getElementById('uploadProgress').style.display = 'none';
            hideLoader();
            //__doPostBack('<%= UpdatePanel1.ClientID %>', '');

            setTimeout(function () {
                var vd = '<%= ConfigurationManager.AppSettings["VirtualDir"] %>';
                window.location.reload();
            }, 2000);
        }

        function onClientButtonClick() {
            $find('pnlPopup2').hide();

            setTimeout(function () {
            }, 1000);
            return false;
        }

        function ScanDocFile() {
            $('.iframe').attr('href', 'UploadScanDocNew.aspx?page=eFileSplit').trigger('click');
        }

        function showEdit(ele) {
            $('.saveDocument,.cancelDocument').hide();
            $('.delete,.shareDocument,.editDocument, .lnkbtnPreview, .downloadFile').show();
            var $this = $(ele);
            $this.css('display', 'none');
            $this.parent().find('[name=lnkUpdate]').show();
            $this.parent().find('[name=lnkcancel]').show();
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                $(this).find('input[type=text]').css('border', '1px solid #c4c4c4');//1px solid #c4c4c4
                $(this).find('input[type = text]').show();
                if ($(this).find('input[type = text]').val() == undefined || $(this).find('input[type = text]').val() == "") { }
                else {
                    oldName = $(this).find('input[type = text]').val();
                }
                $(this).find('[name=DocumentName]').hide();
                $(this).find('[name=lblClass]').hide();
                $(this).closest('td').find('.delete,.shareDocument, .lnkbtnPreview, .downloadFile').hide();
            });
            return false;
        }
        function cancleFileName(ele) {
            var $this = $(ele);
            var LabelName = '';
            var FileName = '';
            $this.css('display', 'none');
            $this.parent().find('[name=lnkUpdate]').hide();
            $this.parent().find('[name=lnkBtnEdit]').show();
            $this.parent().find('.delete,.shareDocument, .lnkbtnPreview, .downloadFile').show();
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                var len = 0;
                len = $(this).find('input[type=text]').length;
                if (len > 0) {
                    LabelName = $(this).find('[name=DocumentName]').html();
                    $(this).find('input[type = text]').val(LabelName);
                    $(this).find('input[type = text]').hide();
                    $(this).find('[name=DocumentName]').show();
                }
                else {
                }
            });
        }

        var newname, Path, oldName, eFileSplitId, oldFileName;
        function OnSaveRename(txtRenameObj, path, eFileSplitID, fileName) {
            Path = path;
            eFileSplitId = eFileSplitID;
            newname = $('#' + txtRenameObj)[0].value;
            oldFileName = fileName;
            if (newname == undefined || newname == "") {
                $('.txtDocumentName').css('border', '1px solid red');
                return false;
            }
            else if (!/^[a-z\d][a-z\d\()_\-.\s]+$/i.test(newname)) {
                alert("File name can not contain special characters.");
                $('.txtDocumentName').css('border', '1px solid red');
                return false;
            }
            else if (newname.indexOf('.') > 0 && newname.substring(newname.indexOf('.') + 1).toLowerCase() != 'pdf') {
                alert("Only File with type 'pdf' is allowed.");
                $('.txtDocumentName').css('border', '1px solid red');
                return false;
            }
            else {
                $('.txtDocumentName').css('border', '1px solid c4c4c4');
            }
            showLoader();
            $.ajax(
                {
                    type: "POST",
                    url: '../EFileFolderJSONService.asmx/RenameDocumentForSplitPdf',
                    data: "{ renameFile:'" + newname + "', path:'" + Path + "',eFileSplitId:'" + eFileSplitId + "',oldFileName:'" + oldFileName + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result.d.length > 0)
                            alert("Failed to rename document. Please try again after sometime.");
                        hideLoader();
                        __doPostBack('', 'RefreshGrid@');
                    },
                    error: function (result) {
                        hideLoader();
                    }
                });
        }


    </script>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>EdFile Split</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnFileName" />
    <asp:HiddenField runat="server" ID="hdnFileId" />
    <asp:HiddenField runat="server" ID="hdnPageNumbers" />
    <asp:HiddenField runat="server" ID="hdnSelectedFolderID" />
    <asp:HiddenField runat="server" ID="hdnSelectedDividerID" />
    <asp:HiddenField runat="server" ID="hdnSelectedFolderText" />
    <asp:HiddenField runat="server" ID="hdnTreeId" />
    <asp:HiddenField runat="server" ID="hdnPath" />
    <asp:Button ID="Button1ShowPopup2" runat="server" Style="display: none" />

    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server"
        PopupControlID="pnlPopup2" DropShadow="true" BackgroundCssClass="modalBackground"
        Enabled="True" TargetControlID="Button1ShowPopup2" />
    <asp:Panel ID="pnlPopup2" Style="display: none"
        runat="server" class="popup-mainbox">
        <div>
            <div class="popup-head" id="Div1">
                Message
            </div>
            <div class="popup-scroller">
                <p>
                    Your document has been uploaded successfully...It will be converting to the edFile format in our server over the next few minutes, depending on the size of the uploaded document. You can now select additional files to upload or navigate away from this web page. 
                </p>
            </div>
            <div class="popup-btn">
                <asp:Button ID="ShowPopup2Button2" runat="server" OnClientClick="return onClientButtonClick()"
                    Text="Close" class="btn green" />
                <%--<input id="btnuploadclose" type="button" value="Close"  />--%>
            </div>
        </div>
    </asp:Panel>
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>

    <div class="popup-mainbox preview-popup" id="preview_popup" style="display: none">
        <div class="popupcontrols" style="width: 100%; height: 5%;">
            <span id="popupclose" class="ic-icon ic-close"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%;">
            <table style="width: 100%; height: 100%">
                <tr style="height: 5%">
                    <td>
                        <input type="button" id="btnSpltBtn" runat="server" value="Tab" onclick="ShowAddToTabPopup();" style="margin-bottom: 10px;" class="btn green btnAddtoTab" title="Add To Tab" />
                        <input type="button" id="btnSplitAddToWA" runat="server" value="WorkArea" onclick="ShowAddtoWAPopup();" style="margin-bottom: 10px;" class="btn green btnAddtoWorkArea" title="Add To WorkArea" />
                        <div class="selecttabcheck"><span class="selectalltab">Select All</span><input type="checkbox" id="btnCheckedAll" value="Select All" onclick="checkAllThumbnail();" /></div>
                    </td>
                </tr>
                <tr>
                    <%--<td style="height: 100px; width: 250px;" class="thumbnailsPart">
                        <div style="margin-bottom: 5px;">
                            
                         </div>
                        <div id="thumnailImages" style="height: 90%; overflow-y: scroll">
                        </div>
                    </td>--%>


                    <td>
                        <iframe id="reviewContent" style="width: 100%; height: 100%;"></iframe>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <div class="popup-mainbox preview-popup" id="preview_popup_split" style="display: none; height: 460px; width: 395px; max-width: 395px; max-height: 460px; margin-top: 20px;">
        <div class="popupcontrols popup-head" style="width: 100%; height: 60px;">
            Split PDF Details
        <span id="popupclose_split" class="ic-icon ic-close popupclose"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%; background: #eaeaea; border-top: 2px solid darkgray;">
            <div class="popup-scroller" style="height: 75%; background: #eaeaea;">
                <p style="margin: 20px">
                    <label>New split Pdf name</label>
                    <asp:TextBox runat="server" ID="txtNewPdfName" Style="margin-bottom: 5px;"></asp:TextBox>
                    <br />
                    <uc1:ucAddToTab ID="ucAddToTab" runat="server" />
                </p>

            </div>
            <div class="popup-btn" style="background: #eaeaea;">

                <asp:Button ID="btnSaveSplitPdf" runat="server" Text="Save" CssClass="btn green" OnClick="btnSaveSplitPdf_Click" Style="margin-right: 5px;" />
                <input id="btnCancelAddToTab" type="button" value="Cancel" class="btn green" />
            </div>
        </div>
    </div>

    <div class="popup-mainbox preview-popup" id="preview_popup_split_WA" style="display: none; height: auto; width: 800px; max-width: 800px; max-height: 600px; margin-top: 20px;">
        <div class="popupcontrols popup-head" style="width: 100%; height: 60px;">
            Split PDF Details
        <span id="popupclose_splitWA" class="ic-icon ic-close popupclose" style="float: right; cursor: pointer;"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%; background: #eaeaea; border-top: 2px solid darkgray;">
            <div class="popup-scroller" style="height: 75%; background: #eaeaea;">
                <p style="margin: 20px">
                    <label>New split Pdf name</label>
                    <asp:TextBox runat="server" ID="txtNewPdfNameWA" Style="margin-bottom: 5px; width: 225px;"></asp:TextBox>
                    <br />
                    <label style="font-weight: bold">The file will be moved to main workarea folder by default.</label>
                    <%--<uc1:ucWorkArea ID="ucWorkArea1" runat="server" />--%>
                </p>

            </div>
            <div class="popup-btn" style="background: #eaeaea;">

                <asp:Button ID="btnAddToWA" runat="server" Text="Save" CssClass="btn green" OnClick="btnAddToWA_Click" Style="margin-right: 5px;" />
                <input id="btnCancelAddToWA" type="button" value="Cancel" class="btn green" />
            </div>
        </div>
    </div>

    <div class="inner-wrapper">
        <div class="page-container">
            <div class="dashboard-container">
                <div class="left-content">
                    <div>
                        <asp:Panel ID="panelMain" runat="server" Visible="true">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>

                                    <div id="uploadProgress" class="white_content" style="font-size: 14px;"></div>
                                    <fieldset id="uploadFieldset" style="width: 650px; font-size: 20px; font-family: 'Segoe UI'; font-weight: normal">
                                        <%--Your Fillable Forms--%>

                                        <div id="divUpload">
                                            <div class='clearfix'></div>
                                            <asp:Image runat="server" ID="Image12" ImageUrl="../Images/loader_bg.gif" ImageAlign="Middle" />
                                            <asp:Label ID="label1" runat="server"></asp:Label>
                                            <asp:Label runat="server" ID="Label2" Style="display: none;">
                                <img align="absmiddle" alt="" src="uploading.gif"/></asp:Label>


                                            <ajaxtoolkit:ajaxfileupload id="ajaxfileuploadeFileFlow" runat="server" padding-bottom="4"
                                                padding-left="2" padding-right="1" padding-top="4" throbberid="mythrobber"
                                                maximumnumberoffiles="10"
                                                allowedfiletypes="pdf"
                                                azurecontainername=""
                                                onuploadcomplete="AjaxfileuploadeFileFlow_onuploadcomplete1"
                                                onclientuploadcomplete="onClientUploadComplete"
                                                onuploadcompleteall="AjaxfileuploadeFileFlow_uploadcompleteall1"
                                                onuploadstart="AjaxfileuploadeFileFlow_uploadstart1"
                                                onclientuploadstart="onClientUploadStart"
                                                onclientuploadcompleteall="onClientUploadCompleteAll"
                                                contextkeys="2" xmlns:ajaxtoolkit="ajaxcontroltoolkit"
                                                cssclass="boxWidth" />
                                            <div class='clearfix'></div>
                                            <div id="uploadCompleteInfo">
                                            </div>
                                        </div>
                                    </fieldset>
                                    <fieldset>
                                        <input type="button" class="create-btn btn-small green" id="btnScandoc" name="btnScandoc" runat="server" causesvalidation="false" value="Scan Document" onclick="ScanDocFile();" />
                                    </fieldset>
                                    <br />
                                    <br />

                                    <%--GridView--%>
                                    <div class="form_title_2">
                                        <asp:Image runat="server"
                                            ImageUrl="../Images/loader_bg.gif" ID="lbsStatus" ImageAlign="Middle" />
                                    </div>
                                    <div class='clearfix'></div>
                                    <asp:GridView ID="grdeFileSplit" runat="server" AllowSorting="True" DataKeyNames="ID"
                                        AutoGenerateColumns="false" BorderWidth="2" OnRowDataBound="GrideFileSplit_RowDataBound"
                                        CssClass="datatable listing-datatable boxWidth" OnSorting="grdeFileSplit_Sorting">
                                        <PagerSettings Visible="False" />
                                        <AlternatingRowStyle CssClass="odd" />
                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                        <Columns>

                                            <%--convert label into textbox--%>
                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="50px" ItemStyle-Wrap="false" SortExpression="ID">
                                                <HeaderTemplate>
                                                    <%--<asp:Label ID="lblDocumentName" runat="server" Text="Document Name" />--%>
                                                    <asp:LinkButton ID="lblDocumentName1" runat="server" Text="Form Name" CommandName="Sort" CommandArgument="ID" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocumentName" runat="server" Text='<%#Eval("Name")%>' name="DocumentName" CssClass="lblDocumentName" Style="cursor: pointer; text-decoration: underline; color: blue"
                                                        OnClick='<%# string.Format("ShowPreviewForEFileSplit({0}, \"{1}\");", Eval("ID"), Eval("Name")) %>' />
                                                    <asp:TextBox runat="server" MaxLength="50" Text='<%#Eval("Name")%>'
                                                        ID="TextDocumentName" Style="display: none; height: 30px;" name="txtDocumentName" CssClass="txtDocumentName" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--end--%>

                                            <asp:BoundField DataField="Size" HeaderText="Size(Mb)" DataFormatString="{0:F2}" ItemStyle-Width="15%"
                                                HtmlEncode="false" SortExpression="Size" />
                                            <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" DataFormatString="{0:MM.dd.yyyy}" ItemStyle-Width="15%"
                                                SortExpression="CreatedDate" />

                                            <%--rename start--%>
                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="20%"
                                                ItemStyle-CssClass="table_tekst_edit actionMinWidth">
                                                <HeaderTemplate>
                                                    <asp:Label ID="lblAction" runat="server" Text="Action" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkBtnEdit" runat="server" Text="" CommandArgument='<%# Eval("FilePath")%>'
                                                        CssClass="ic-icon ic-edit editDocument" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkBtnUpdate" runat="server" CausesValidation="True" CommandName="Update" AutoPostBack="False"
                                                        ToolTip="Update" CssClass="ic-icon ic-save saveDocument" Style="display: none" CommandArgument='<%# Eval("FilePath")%>' name="lnkUpdate"></asp:LinkButton>
                                                    <asp:LinkButton ID="lnkBtnCancel" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                        OnClientClick="cancleFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancelDocument" Style="display: none" name="lnkcancel"></asp:LinkButton>

                                                    <asp:LinkButton ID="lnkBtnDelete" runat="server" CssClass="ic-icon ic-delete delete" OnCommand="LinkButtonDelete_Click" ToolTip="Delete File"
                                                        CommandArgument='<%# Eval("FilePath")%>'></asp:LinkButton>
                                                    <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="lnkBtnDelete"
                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                    </ajaxToolkit:ModalPopupExtender>
                                                    <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                        ConfirmText="" TargetControlID="lnkBtnDelete">
                                                    </ajaxToolkit:ConfirmButtonExtender>

                                                    <asp:LinkButton ID="lnkbtnPreview" runat="server" CssClass="ic-icon ic-preview lnkbtnPreview" CausesValidation="False"
                                                        CommandName=""></asp:LinkButton>
                                                    <asp:HyperLink ID="lnkBtnDownload" runat="server" CausesValidation="false" CssClass="ic-icon ic-download downloadFile" Target="_blank" />

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--rename end--%>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <table border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 10px 10px;">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label120" runat="server" />There are no files. right now.
                                                    </td>
                                                </tr>
                                            </table>
                                        </EmptyDataTemplate>
                                    </asp:GridView>

                                    <div class="prevnext" bgcolor="#f8f8f5" style="float: right; padding: 5px;">
                                        <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                            PageSize="5" CssClass="paging_position " PagerStyle="NumericPages" ControlToPaginate="grdeFileSplit"></cc1:WebPager>
                                        <asp:HiddenField ID="FolderID" runat="server" />
                                    </div>
                                    <br />
                                    <br />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>

                        <%--Delete File--%>
                        <asp:Panel class="popup-mainbox" ID="DivDeleteConfirmation" Style="display: none"
                            runat="server">
                            <div>
                                <div class="popup-head" id="PopupHeader">
                                    Delete File
                                </div>
                                <div class="popup-scroller">
                                    <p>
                                        Caution! Are you sure you want to delete this file. Once you delete this file, it
                                                        will be deleted for good
                                    </p>
                                    <p>
                                        Confirm Password:
                                       <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                                    </p>
                                </div>
                                <div class="popup-btn">
                                    <input id="ButtonDeleleOkay" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />
                                    <input id="ButtonDeleteCancel" type="button" value="No" class="btn green" />
                                </div>
                            </div>
                        </asp:Panel>
                        <%--end Delete--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="viewerContainer" tabindex="0">
        <div id="viewer" class="pdfViewer"></div>
    </div>
    <a class='iframe' href="javascript:;" style="display: none;">popup</a>
</asp:Content>

