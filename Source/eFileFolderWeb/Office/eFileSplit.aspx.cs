﻿using System;
using System.Data;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using Shinetech.DAL;
using Shinetech.Utility;
using Shinetech.Framework.Controls;
using AjaxControlToolkit;
using Shinetech.Engines;
using System.Web.Script.Serialization;
using System.Collections.Generic;
using System.Web.Services;
using iTextSharp.text.pdf;


public partial class Office_eFileSplit : System.Web.UI.Page
{
    public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    public static string splitPdfTepmPath = System.Configuration.ConfigurationManager.AppSettings["SplitPdf"];
    public string virtualDir = ConfigurationManager.AppSettings["VirtualDir"];
    public string SortType = "DESC";
    string newFileName = string.Empty;
    public string fullFilePath = string.Empty;
    public static string ThumbnailPath = System.Configuration.ConfigurationManager.AppSettings["ThumbNailPath"];
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (!Page.IsPostBack)
        {
            if (Sessions.HasViewPrivilegeOnly)
            {
                btnSpltBtn.Style.Add("display", "none");
                btnSplitAddToWA.Style.Add("display", "none");
            }
            GetData();
            BindGrid();
        }
        var argument = Request.Form["__EVENTARGUMENT"];
        if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
        {
            GetData();
            BindGrid();
        }
        argument = "";
    }
    public DataTable DataSource
    {
        get
        {
            return this.Session["Files_EdFileSplit"] as DataTable;
        }
        set
        {
            this.Session["Files_EdFileSplit"] = value;
        }
    }

    public string DocumentID
    {
        get
        {
            return (string)Session["DocumentID"];
        }
        set
        {
            Session["DocumentID"] = value;

        }
    }

    public string DocumentName
    {
        get
        {
            return (string)Session["DocumentName"];
        }
        set
        {
            Session["DocumentName"] = value;

        }
    }

    public string EffID_FFB
    {
        get
        {
            return (string)this.Session["EffID_FFB"];
        }
        set
        {
            this.Session["EffID_FFB"] = value;
        }
    }

    public string PathUrl
    {
        get { return splitPdfTepmPath; }
    }

    public string splitPdfFolderName
    {
        get
        {
            return string.Format("~/{0}", splitPdfTepmPath);
        }

    }
    public Dictionary<string, string> UploadErrorFileName
    {
        get
        {
            return (Session["UploadErrorFileName"] != null) ? (Dictionary<string, string>)Session["UploadErrorFileName"] : new Dictionary<string, string>();
        }
        set
        {
            Session["UploadErrorFileName"] = value;
        }
    }
    #region FileUpload
    protected void AjaxfileuploadeFileFlow_onuploadcomplete1(object sender, AjaxFileUploadEventArgs file)
    {
        try
        {
            string path = Server.MapPath("~/" + this.PathUrl);
            string uid = Sessions.SwitchedRackspaceId;
            //string uid = Membership.GetUser().ProviderUserKey.ToString();

            string fullPath = Path.Combine(path, uid);
            Int32 dividerIdForFile = 0;
            if (ajaxfileuploadeFileFlow.IsInFileUploadPostBack)
            {
                var fullname = Path.Combine(fullPath, file.FileName);
                string fileName = Path.GetFileName(fullname);
                decimal size;

                size = Math.Round((decimal)(file.FileSize / 1024) / 1024, 2);
                Guid guid = new Guid(uid);

                byte[] fullfile = this.ProcessFile(ajaxfileuploadeFileFlow, file, 0, fullPath);
                this.ProcessFile(file, fullfile, dividerIdForFile, fullPath);

                string errorMsg = string.Empty;

                GetData();
                BindGrid();
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void AjaxfileuploadeFileFlow_uploadcompleteall1(object sender, AjaxFileUploadCompleteAllEventArgs e)
    {
        var startedAt = (DateTime)Session["uploadTime"];
        var now = DateTime.Now;
        e.ServerArguments = new JavaScriptSerializer()
            .Serialize(new
            {
                duration = (now - startedAt).Seconds,
                time = DateTime.Now.ToShortTimeString()
            });

    }

    protected void AjaxfileuploadeFileFlow_uploadstart1(object sender, AjaxFileUploadStartEventArgs e)
    {
        var now = DateTime.Now;
        e.ServerArguments = now.ToShortTimeString();
        Session["uploadTime"] = now;
    }

    #endregion

    #region Grid
    protected void GrideFileSplit_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
            string path = drv.Row["FilePath"].ToString();
            string ID = drv.Row["ID"].ToString();
            string fileName = drv.Row["Name"].ToString();

            path = path.Replace(@"\", "/");

            TextBox txtDocumentName = (e.Row.FindControl("TextDocumentName") as TextBox);

            LinkButton lnkBtnUpdate = (e.Row.FindControl("lnkBtnUpdate") as LinkButton);
            if (lnkBtnUpdate != null)
            {
                lnkBtnUpdate.Attributes.Add("onclick", "OnSaveRename('" + txtDocumentName.ClientID + "','" + path + "','" + ID + "','" + fileName + "');");
            }


            LinkButton lnkbtnPreview = (e.Row.FindControl("lnkbtnPreview") as LinkButton);
            if (lnkbtnPreview != null)
            {
                lnkbtnPreview.Attributes.Add("onclick", "ShowPreviewForEFileSplit('" + int.Parse(ID) + "','" + fileName + "');");
            }

            HyperLink lnkBtnDownload = (e.Row.FindControl("lnkBtnDownload") as HyperLink);
            if (lnkBtnDownload != null)
            {
                lnkBtnDownload.Attributes.Add("href", "PdfViewer.aspx?data=" + QueryString.QueryStringEncode("ID=" + fileName + "&folderID=" + (int)Enum_Tatva.Folders.EFileSplit + "&sessionID=" + Sessions.SwitchedRackspaceId));
            }
        }
    }


    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
        UpdatePanel1.Update();
    }

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    private void GetData()
    {
        string uid = Sessions.SwitchedRackspaceId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        Guid guid = new Guid(uid);

        this.DataSource = General_Class.GeteFileSplitDocuments(guid);
        this.DataSource.Columns.Add("FilePath", typeof(string));
        if (this.DataSource != null && this.DataSource.Rows.Count > 0)
        {
            string path = Server.MapPath("~/" + this.PathUrl);
            string tempPath = string.Concat(path, Path.DirectorySeparatorChar, uid);
            if (Directory.Exists(tempPath))
            {
                string[] files = Directory.GetFiles(tempPath);
                foreach (string file in files)
                {
                    foreach (DataRow row in this.DataSource.Rows)
                    {
                        if (file.Contains(row["Name"].ToString()))
                            row["FilePath"] = file;
                    }
                }
            }
        }
        DataView dvSource = this.DataSource.AsDataView();
        dvSource.Sort = "CreatedDate" + " " + SortType;
        this.DataSource = dvSource.ToTable();
        ViewState["SortEdFileSplitDirection"] = SortType;
        ViewState["SortEdFileSplitExpression"] = "CreatedDate";
    }

    #endregion

    #region DeleteFile
    protected void LinkButtonDelete_Click(Object sender, CommandEventArgs e)
    {
        try
        {
            string password = textPassword.Text.Trim();
            string strPassword = PasswordGenerator.GetMD5(password);
            string errorMsg = string.Empty;

            if (UserManagement.GetPassword(User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
            {
                //show the password is wrong        
                ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "RssYes", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);
            }
            else
            {
                try
                {
                    LinkButton lnkbtnDelete = sender as LinkButton;
                    GridViewRow gvrow = lnkbtnDelete.NamingContainer as GridViewRow;
                    int ID = Convert.ToInt32(grdeFileSplit.DataKeys[gvrow.RowIndex].Value.ToString());

                    DataTable dtFiles = General_Class.GetFileSplitById(ID);
                    dtFiles.AsEnumerable().ToList().ForEach(d =>
                    {
                        var file = d.Field<string>("Name");
                        General_Class.UpdateFileSplit(file, ID, Enum_Tatva.IsDelete.Yes.GetHashCode(), 0, 0, Sessions.UserId);
                        GetData();
                        BindGrid();
                    });

                }
                catch (Exception exp) { Console.WriteLine(exp.Message); }

            }
        }
        catch (Exception ex)
        { }
        finally
        {
            ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
        }

    }
    #endregion

    protected void grdeFileSplit_Sorting(object sender, GridViewSortEventArgs e)
    {
        //SetSortDirection(e.SortDirection.ToString());
        if (this.DataSource != null)
        {
            //Sort the data.
            this.DataSource.DefaultView.Sort = e.SortExpression + " " + GetSortDirection(e.SortExpression);
            BindGrid();
        }
    }

    private string GetSortDirection(string column)
    {

        // Retrieve the last column that was sorted.
        string sortExpression = ViewState["SortEdFileSplitExpression"] as string;

        if (sortExpression != null)
        {
            // Check if the same column is being sorted.
            // Otherwise, the default value can be returned.
            if (sortExpression == column)
            {
                string lastDirection = ViewState["SortEdFileSplitDirection"] as string;
                if ((lastDirection != null) && (lastDirection == "ASC"))
                {
                    SortType = "DESC";
                }
                else if ((lastDirection != null) && (lastDirection == "DESC"))
                {
                    SortType = "ASC";
                }
            }
        }

        // Save new values in ViewState.
        ViewState["SortEdFileSplitDirection"] = SortType;
        ViewState["SortEdFileSplitExpression"] = column;

        return SortType;
    }

    [WebMethod]
    public static string GetPreviewURL(int documentID, string fileName)
    {
        // RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
        string errorMsg = string.Empty;
        string uid = Sessions.SwitchedRackspaceId;
        //string uid = Membership.GetUser().ProviderUserKey.ToString();
        string path = splitPdfTepmPath + Path.DirectorySeparatorChar + uid;

        string encodeName = HttpUtility.UrlPathEncode(fileName);
        encodeName = encodeName.Replace("%", "$");

        string fullPath = Path.Combine(path, encodeName);

        string faildFileName = string.Empty;
        //CreateThumbnailImage(encodeName, ref faildFileName, documentID);

        return fullPath;
    }

    protected void btnSaveSplitPdf_Click(object sender, EventArgs e)
    {
        try
        {
            string pageNumbers = hdnPageNumbers.Value;
            if (!string.IsNullOrEmpty(pageNumbers))
            {
                Dictionary<bool, bool> dict = ExtractPages(pageNumbers, hdnFileName.Value, txtNewPdfName.Text.Trim(), hdnFileId.Value, hdnSelectedFolderID.Value, hdnSelectedDividerID.Value, hdnSelectedFolderText.Value);
                if (dict.Keys.Contains(true))
                {
                    string message = "The selected pages have been added to tab.  Would you like to split additional pages?  If so, please select OK, otherewise, select Cancel";
                    ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "alert", "<script language=javascript>ShowConfirm('" + message + "'," + dict.Values.ToList()[0].ToString().ToLower() + ");</script>", false);
                }
            }

        }
        catch (Exception ex)
        { }
        finally
        {
            //Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
    }

    [WebMethod]
    public static string SplitPdf(string FileName, Int32 Id, string PageNumber, string NewFileName)
    {
        return PageNumber;
    }

    public byte[] ProcessFile(AjaxFileUpload AjaxFileUpload1, AjaxFileUploadEventArgs file, int dividerIdForFile, string path)
    {

        try
        {
            string fileName = ProcessAndSaveFile(file.FileName, path);
            if (!string.IsNullOrEmpty(fileName))
            {
                byte[] s = file.GetContents();
                AjaxFileUpload1.SaveAs(fileName);
                string fullFilePath = fileName;
                return s;
            }
            else
                return null;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(Convert.ToString(this.EffID_FFB), dividerIdForFile.ToString(), newFileName, ex);
            return null;
        }
    }

    private string ProcessAndSaveFile(string fName, string path)
    {
        try
        {
            //fName = fName.Replace("#", "");
            //fName = fName.Replace("&", "");
            //string encodeName = HttpUtility.UrlPathEncode(fName);
            //encodeName = encodeName.Replace("%", "$");
            fName = fName.Replace("#", "");
            fName = fName.Replace("&", "");
            fName = Common_Tatva.SubStringFilename(fName);
            string encodeName = HttpUtility.UrlPathEncode(fName);
            encodeName = encodeName.Replace("%", "$");

            string fileName = String.Concat(
                    path,
                    Path.DirectorySeparatorChar,
                    encodeName
                );
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }


            fileName = String.Concat(path, Path.DirectorySeparatorChar, Common_Tatva.ChangeExtension(fileName));

            fileName = Common_Tatva.GetRenameFileName(fileName);

            newFileName = Path.GetFileName(fileName);
            newFileName = HttpUtility.UrlDecode(newFileName.Replace('$', '%').ToString());
            return fileName;
        }
        catch (Exception ex)
        {
            return string.Empty;
        }
    }

    private void ProcessFileAndSaveInDB(float size, string path, int dividerIdForFile = 0, int folderID = 0, bool isMovedToFolder = false)
    {
        try
        {
            WorkItem item = new WorkItem();
            item.FolderID = folderID;
            item.DividerID = dividerIdForFile;
            string basePath = this.MapPath(this.splitPdfFolderName);

            item.Path = path;

            item.FileName = Path.GetFileNameWithoutExtension(newFileName);

            item.FullFileName = Path.GetFileName(newFileName);

            item.UID = Sessions.SwitchedRackspaceId;
            //item.UID = Membership.GetUser().ProviderUserKey.ToString();
            item.contentType = Common_Tatva.getFileExtention(Path.GetExtension(newFileName));

            int order = FlashViewer.GetDocumentOrder(Convert.ToInt32(dividerIdForFile));


            item.MachinePath = path;

            if (!Directory.Exists(item.MachinePath))
            {
                Directory.CreateDirectory(item.MachinePath);
            }

            string encodeName = HttpUtility.UrlPathEncode(item.FullFileName);
            encodeName = encodeName.Replace("%", "$");
            string source = string.Concat(path, Path.DirectorySeparatorChar, encodeName);
            var pageCount = 1;
            if (item.contentType == StreamType.JPG)
            {
                Jpeg2Engine jpeg2Engine = new Jpeg2Engine(item);
                jpeg2Engine.generatePDFfromImage(source, source.Split('.')[0] + ".pdf");
            }
            else if (item.contentType == StreamType.PDF)
            {
                PdfReader pdfReader = new PdfReader(source);
                pageCount = pdfReader.NumberOfPages;
            }
            if (isMovedToFolder)
            {
                string relativePath = CurrentVolume + Path.DirectorySeparatorChar + folderID + Path.DirectorySeparatorChar + dividerIdForFile;
                this.DocumentID = General_Class.DocumentsInsert(item.FileName, item.FolderID, item.UID, relativePath, item.DividerID, pageCount, (float)size, item.FileName,
                    (int)item.contentType, order + 1, Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()));
                General_Class.AuditLogByDocId(Convert.ToInt32(this.DocumentID), Sessions.SwitchedRackspaceId, Enum_Tatva.Action.Create.GetHashCode());
                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(item.FolderID), Convert.ToInt32(item.DividerID), int.Parse(this.DocumentID), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.EFileSplitUpload.GetHashCode(), 0, 0);
            }
            this.DocumentName = item.FileName;
        }
        catch (Exception ex)
        {
        }
    }

    public void ProcessFile(AjaxFileUploadEventArgs file, byte[] fileStream, int dividerIdForFile, string path)
    {
        try
        {

            float size = (float)Math.Round((decimal)(fileStream.Length / 1024) / 1024, 2); ; // IN MB.

            ProcessFileAndSaveInDB(size, path);
            Guid guid = new Guid(Sessions.SwitchedRackspaceId);
            //Guid guid = new Guid(Membership.GetUser().ProviderUserKey.ToString());
            General_Class.DocumentsInsertForeFileSplit(newFileName, guid, Convert.ToDecimal(size), Enum_Tatva.IsDelete.No.GetHashCode(), Sessions.UserId);

            fullFilePath = path;

        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(Convert.ToString(this.EffID_FFB), Convert.ToString(dividerIdForFile), newFileName, ex);

            Dictionary<string, string> UploadErrorFileName1 = new Dictionary<string, string>();
            UploadErrorFileName1.Add(file.FileId, file.FileName);

            UploadErrorFileName = UploadErrorFileName1;

            if ((File.Exists(fullFilePath)))
                File.Delete(fullFilePath);
        }
    }

    public bool ExtractAndUploadPDF(string pageNumbers, string fileName, string newFName, string fileID, ref string newEncodeName, ref string outputPdfPath)
    {
        PdfReader.unethicalreading = true;
        PdfReader reader = null;
        PdfCopy pdfCopyProvider = null;
        PdfCopy pdfRemaining = null;
        PdfImportedPage importedPage = null;
        PdfImportedPage importedPage1 = null;
        iTextSharp.text.Document sourceDocument = null;

        try
        {
            string[] extractThesePages = pageNumbers.Split(',');

            string sourcePdfPath;
            string encodeName = HttpUtility.UrlPathEncode(fileName);
            encodeName = encodeName.Replace("%", "$");

            string uid = Sessions.SwitchedRackspaceId;
            //string uid = Membership.GetUser().ProviderUserKey.ToString();
            string path = HttpContext.Current.Server.MapPath("~/" + splitPdfTepmPath + Path.DirectorySeparatorChar + uid);

            sourcePdfPath = path + Path.DirectorySeparatorChar + encodeName;


            outputPdfPath = path + Path.DirectorySeparatorChar + System.Configuration.ConfigurationManager.AppSettings["NewSplitPdf"]
                + Path.DirectorySeparatorChar + fileID;

            if (!Directory.Exists(outputPdfPath))
            {
                Directory.CreateDirectory(outputPdfPath);
            }

            if (newFName.IndexOf(".pdf") < 0)
                newFName = newFName + ".pdf";

            newFName = newFName.Replace("#", "");
            newFName = newFName.Replace("&", "");

            newEncodeName = HttpUtility.UrlPathEncode(newFName);
            newEncodeName = newEncodeName.Replace("%", "$");
            outputPdfPath = outputPdfPath + Path.DirectorySeparatorChar + newEncodeName;


            string remainingPath = path + Path.DirectorySeparatorChar + System.Configuration.ConfigurationManager.AppSettings["NewSplitPdf"]
               + Path.DirectorySeparatorChar + fileID + Path.DirectorySeparatorChar + encodeName;

            reader = new PdfReader(sourcePdfPath);
            // For simplicity, I am assuming all the pages share the same size
            // and rotation as the first page:
            sourceDocument = new iTextSharp.text.Document(reader.GetPageSizeWithRotation(int.Parse(extractThesePages[0])));

            // Initialize an instance of the PdfCopyClass with the source 
            // document and an output file stream:
            pdfCopyProvider = new PdfCopy(sourceDocument, new System.IO.FileStream(outputPdfPath, System.IO.FileMode.Create));
            if (reader.NumberOfPages > extractThesePages.Length)
                pdfRemaining = new PdfCopy(sourceDocument, new System.IO.FileStream(remainingPath, System.IO.FileMode.Create));
            sourceDocument.Open();

            // Walk the array and add the page copies to the output file:
            foreach (string pageNumber in extractThesePages)
            {
                importedPage = pdfCopyProvider.GetImportedPage(reader, int.Parse(pageNumber));
                pdfCopyProvider.AddPage(importedPage);
            }
            for (int pageNumber = 1; pageNumber <= reader.NumberOfPages; pageNumber++)
            {
                if (extractThesePages.Contains(pageNumber.ToString()))
                    continue;
                importedPage1 = pdfRemaining.GetImportedPage(reader, pageNumber);
                pdfRemaining.AddPage(importedPage1);
            }
            if (sourceDocument != null && sourceDocument.PageNumber > 0)
                sourceDocument.Close();
            if (reader != null)
                reader.Close();
            try
            {
                if (pdfCopyProvider != null)
                {
                    pdfCopyProvider.PageEmpty = false;
                    pdfCopyProvider.Close();
                }
                if (pdfRemaining != null)
                {
                    pdfRemaining.PageEmpty = false;
                    pdfRemaining.Close();
                }
            }
            catch (Exception ex)
            {
            }
            if (File.Exists(remainingPath))
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                File.Copy(remainingPath, sourcePdfPath, true);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                File.Delete(remainingPath);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                FileInfo fileInfo = new FileInfo(sourcePdfPath);
                decimal fileSize = Math.Round((decimal)(fileInfo.Length / 1024) / 1024, 2);
                General_Class.UpdateFileSplitSize(int.Parse(fileID), (long)fileSize, Sessions.UserId);
                return true;
            }
            else
            {
                General_Class.UpdateFileSplit(fileName, int.Parse(fileID), Enum_Tatva.IsDelete.No.GetHashCode(), 0, 1, Sessions.UserId);
                return false;
            }
        }
        catch (Exception ex)
        {
            if (sourceDocument != null)
                sourceDocument.Close();
            if (reader != null)
                reader.Close();
            pdfCopyProvider.Close();
            pdfRemaining.Close();
            return false;
        }

    }

    public Dictionary<bool, bool> ExtractPages(string pageNumbers, string fileName, string newFName, string fileID, string folderID, string dividerID, string folderName)
    {
        try
        {
            Dictionary<bool, bool> dict = new Dictionary<bool, bool>();
            string newEncodeName = string.Empty;
            string outputPdfPath = string.Empty;

            bool isFileExist = ExtractAndUploadPDF(pageNumbers, fileName, newFName, fileID, ref newEncodeName, ref outputPdfPath);


            string pathName = HttpContext.Current.Server.MapPath(string.Format("~/{0}", CurrentVolume) + Path.DirectorySeparatorChar + folderID + Path.DirectorySeparatorChar
                + dividerID);
            newEncodeName = Common_Tatva.GetRenameFileName(pathName + Path.DirectorySeparatorChar + newEncodeName);

            if (!Directory.Exists(pathName))
            {
                Directory.CreateDirectory(pathName);
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
            File.Copy(outputPdfPath, newEncodeName);

            FileInfo fileInfo = new FileInfo(newEncodeName);
            decimal fileSize = Math.Round((decimal)(fileInfo.Length / 1024) / 1024, 2);

            string temp = newEncodeName.Substring(newEncodeName.LastIndexOf("\\") + 1);
            newFileName = HttpUtility.UrlDecode(temp.Replace('$', '%').ToString());

            ProcessFileAndSaveInDB((float)fileSize, pathName, int.Parse(dividerID), int.Parse(folderID), true);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            File.SetAttributes(outputPdfPath, FileAttributes.Normal);
            File.Delete(outputPdfPath);

            dict.Add(true, isFileExist);
            return dict;

        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(folderID, dividerID, fileName, ex);
            return null;
        }
    }

    protected void btnAddToWA_Click(object sender, EventArgs e)
    {
        try
        {
            string pageNumbers = hdnPageNumbers.Value;
            string selectedPath = hdnPath.Value;
            if (!string.IsNullOrEmpty(pageNumbers))
            {
                string newPath = string.Empty;
                string fileName = string.Empty;
                string filePath = string.Empty;
                RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();

                string newEncodeName = string.Empty;
                string outputPdfPath = string.Empty;

                bool isFileExist = ExtractAndUploadPDF(pageNumbers, hdnFileName.Value, txtNewPdfNameWA.Text.Trim(), hdnFileId.Value, ref newEncodeName, ref outputPdfPath);

                if (!string.IsNullOrEmpty(selectedPath) && selectedPath.IndexOf('.') < 0)
                {
                    if (selectedPath.Length > Enum_Tatva.GetEnumDescription(Enum_Tatva.Folders.WorkArea).Length + 1)
                    {
                        newPath = selectedPath.Substring(Enum_Tatva.GetEnumDescription(Enum_Tatva.Folders.WorkArea).Length + 1);
                        fileName = newPath + "/" + txtNewPdfNameWA.Text.Trim();
                    }
                    else
                        fileName = txtNewPdfNameWA.Text.Trim();
                }
                else
                {
                    fileName = txtNewPdfNameWA.Text.Trim();
                }
                if (fileName.IndexOf('.') < 0)
                    fileName += ".pdf";

                string errorMsg = string.Empty;
                FileStream stream = null;
                try
                {
                    stream = File.Open(outputPdfPath, FileMode.Open);
                    string newFileNamePath = rackSpaceFileUpload.UploadObject(ref errorMsg, fileName.ToLower(), Enum_Tatva.Folders.WorkArea, stream, newPath);
                    if (string.IsNullOrEmpty(errorMsg))
                    {
                        FileInfo fileInfo = new FileInfo(outputPdfPath);
                        decimal fileSize = Math.Round((decimal)(fileInfo.Length / 1024) / 1024, 2);

                        string workAreaID = General_Class.DocumentsInsertForWorkArea(newFileNamePath.Substring(newFileNamePath.LastIndexOf('/') + 1), Sessions.SwitchedRackspaceId, Sessions.UserId, fileSize, Enum_Tatva.IsDelete.No.GetHashCode(), newFileNamePath, Enum_Tatva.WorkAreaType.SplitUpload.GetHashCode(), string.IsNullOrEmpty(hdnTreeId.Value) ? 0 : Convert.ToInt32(hdnTreeId.Value));
                        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Create.GetHashCode(), 0, 0, 0, int.Parse(workAreaID));
                        string message = "The selected pages have been uploaded to WorkArea.  Would you like to split additional pages?  If so, please select OK, otherewise, select Cancel";
                        ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "alert", "<script language=javascript>ShowConfirm('" + message + "'," + isFileExist.ToString().ToLower() + ");</script>", false);
                    }
                    else
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Failed to upload file. Please try again after sometime." + "\");", true);
                }
                catch (Exception ex)
                { }
                finally
                {
                    stream.Close();
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
}