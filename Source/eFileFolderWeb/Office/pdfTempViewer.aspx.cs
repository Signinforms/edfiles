﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Office_pdfTempViewer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string data = HttpContext.Current.Request.QueryString["data"];
        string embed = "<embed src=\"{0}\" #toolbar=0&navpanes=0&statusbar=0\" width=\"850\" height=\"100%\"></embed>";
        ltEmbed.Text = string.Format(embed, ResolveUrl("~" + data));
    }
}