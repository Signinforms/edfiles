﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="test.aspx.cs" Inherits="Office_test" %>

<%--<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>--%>

<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register Src="../Controls/DocFind.ascx" TagName="DocFind" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script language="javascript" type="text/javascript">
        function openwindow(id) {
            var url = 'PDFDownloader.aspx?ID=' + id;
            var name = 'DownPDF';
            var iWidth = 250;
            var iHeight = 200;
            var iTop = (window.screen.availHeight - 30 - iHeight) / 2;
            var iLeft = (window.screen.availWidth - 10 - iWidth) / 2;
            window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',toolbar=no,menubar=no,scrollbars=no,resizeable=no,location=no,status=no');
        }

    </script>


    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Manage Document</h1>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div class="left-content">
                    <div class="content-box listing-view">
                        <fieldset>
                            <label>Folder Name</label>
                            <asp:TextBox ID="textfieldFolder" runat="server" name="textfieldFolder"
                                size="30" MaxLength="20" />
                        </fieldset>
                        <fieldset>
                            <label>File Name</label>
                            <asp:TextBox ID="textfieldFile" runat="server" name="textfieldFile"
                                size="30" MaxLength="20" />
                        </fieldset>
                        <fieldset>
                            <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                            <asp:Button ID="Submit2" name="Submit2" runat="server" Text="Search"
                                align="absbottom" OnClick="Submit2_Click" Style="padding: 5px" CssClass="create-btn btn-small green" />
                        </fieldset>
                        <div class="prevnext" style="float: right" bgcolor="#f8f8f5">
                            <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                OnPageSizeChanged="WebPager1_PageSizeChanged" PageSize="25" CssClass="prevnext custom_select"
                                PagerStyle="NumericPages" ControlToPaginate="GridView1"></cc1:WebPager>
                        </div>
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <div class="work-area">
                                    <div class="overflow-auto">
                                        <asp:GridView ID="GridView1" runat="server" OnRowDeleting="GridView1_RowDeleting"
                                            OnRowDataBound="GridView1_RowDataBound" DataKeyNames="DocumentID " AllowSorting="True"
                                            AutoGenerateColumns="false" OnSorting="GridView1_Sorting" CssClass="datatable listing-datatable">
                                            <PagerSettings Visible="False" />
                                            <AlternatingRowStyle CssClass="odd" />
                                            <RowStyle CssClass="even" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:BoundField DataField="DocumentName" HeaderText="Document Name" SortExpression="DocumentName"></asp:BoundField>
                                                <asp:BoundField DataField="FolderName" HeaderText="Folder Name" SortExpression="FolderName"></asp:BoundField>
                                                <asp:BoundField DataField="DividerName" HeaderText="Tab Name" SortExpression="TabName"></asp:BoundField>
                                                <asp:BoundField DataField="ModifiedBy" HeaderText="Modifier" SortExpression="ModifiedBy"></asp:BoundField>

                                                <asp:BoundField DataField="Date" HeaderText="Create Date" DataFormatString="{0:MM.dd.yyyy}"
                                                    SortExpression="Date" />
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="30px"
                                                    ItemStyle-CssClass="table_tekst_edit">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label34" Text="File" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton21" runat="server" Text="" CausesValidation="false" Visible='<%#Eval("Flag").ToString() == "0"? true: false %>' CssClass="ic-icon ic-download"
                                                            PostBackUrl='<%#string.Format("PDFDownloader.aspx?ID={0}",Eval("DocumentID"))%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                    ItemStyle-CssClass="table_tekst_edit">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label8" runat="server" Text="Delete" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="libDelete" runat="server" Text="" CausesValidation="False"
                                                            CommandName="Delete" CssClass="ic-icon ic-delete" Visible='<%#Eval("Flag").ToString() == "0"? true: false %>'></asp:LinkButton>
                                                        <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                            CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                            PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                        </ajaxToolkit:ModalPopupExtender>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                            ConfirmText="" TargetControlID="libDelete">
                                                        </ajaxToolkit:ConfirmButtonExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Action" HeaderText="Action" SortExpression="Action"></asp:BoundField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <table width="100%" border="0" backcolor="#F8F8F5" cellpadding="2" cellspacing="0"
                                                    style="margin: 20px 0 10px 10px;">
                                                    <tr>
                                                        <td align="Center" class="podnaslov" backcolor="#F8F8F5">
                                                            <asp:Label ID="Label1" runat="server" />
                                                            There are no documents.
                                                        </td>
                                                    </tr>
                                                </table>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="right-content">
                    <%--<div class="quick-find">
                    <uc1:QuickFind ID="QuickFind1" runat="server" />
                </div>--%>
                    <div class="quick-find" style="margin-top: 15px;">
                        <div class="find-inputbox">
                            <marquee scrollamount="1" id="marqueeside" runat="server"
                                behavior="scroll" onmouseover="this.stop()" onmouseout="this.start()" scrolldelay="20"
                                direction="up">
                               <%=SideMessage %></marquee>
                        </div>
                    </div>
                </div>

            </div>
            <div class="listing-table">
                <asp:Panel class="popup-mainbox" ID="DivDeleteConfirmation" Style="display: none"
                    runat="server">
                    <div>
                        <div class="popup-head" id="PopupHeader">
                            Delete File
                        </div>
                        <%--  <div class="popup_Titlebar" id="PopupHeader">
                                    <div class="TitlebarRight" onclick="$get('ButtonDeleteCancel').click();">
                                    </div>
                                </div>--%>
                        <div class="popup-scroller">
                            <p>
                                Caution! Are you sure you want to delete this file. Once you delete this file,
                                                it will be deleted for good
                            </p>
                            <p>
                                Confirm Password:
                                                <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                            </p>
                        </div>
                        <div class="popup-btn">
                            <input id="ButtonDeleleOkay" type="button" value="Yes" class="btn green" />
                            <input id="ButtonDeleteCancel" type="button" value="No" class="btn green" />
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>


</asp:Content>


