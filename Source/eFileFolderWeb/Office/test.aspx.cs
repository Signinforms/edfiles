﻿using Shinetech.DAL;
using Shinetech.Engines;
using Shinetech.Framework.Controls;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Office_test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetFileData();
            BindGrid();
        }
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource"] as DataTable;
        }
        set
        {
            this.Session["DataSource"] = value;
        }
    }

    public string SideMessage
    {
        get
        {
            return this.Session["_Side_Message"] as string;
        }

        set
        {
            this.Session["_Side_Message"] = value;
        }
    }

    private void GetData(string folderId)
    {
        this.DataSource = FileFolderManagement.GetDocumentByUID(Membership.GetUser().ProviderUserKey.ToString(), folderId);
    }

    private void GetFileData()
    {
        string officeID1 = Membership.GetUser().ProviderUserKey.ToString();
        if (this.Page.User.IsInRole("Offices"))
        {
            this.DataSource = FileFolderManagement.GetDocumentByUID(officeID1, textfieldFile.Text.Trim(), textfieldFolder.Text.Trim());
        }

        else if (this.Page.User.IsInRole("Users"))
        {
            this.DataSource = FileFolderManagement.GetSubDocumentByUID(officeID1, textfieldFile.Text.Trim(), textfieldFolder.Text.Trim());
        }
    }

    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }
    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        if (this.DataSource != null)
        {
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        if (this.DataSource != null)
        {
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
    }

    protected string GetFileExtention(StreamType ext)
    {
        string extname = ".pdf";
        switch (ext)
        {
            case StreamType.PDF:
                extname = ".pdf";
                break;
            case StreamType.Word:
                extname = ".doc";
                break;
            case StreamType.Excel:
                extname = ".xls";
                break;
            case StreamType.PPT:
                extname = ".ppt";
                break;
            case StreamType.RTF:
                extname = ".rtf";
                break;
            default:
                break;
        }

        return extname;
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //todo：删除物理文件，需要把文件移动另一个文件夹，以防止被重新上传的同名文件覆盖
        //
        string password = textPassword.Text.Trim();
        string strPassword = Shinetech.Utility.PasswordGenerator.GetMD5(password);

        if (UserManagement.GetPassword(User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel4, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);

        }
        else
        {
            string strDocumentID = GridView1.DataKeys[e.RowIndex].Value.ToString();

            Document doc = new Document(int.Parse(strDocumentID));

            if (doc.IsExist)
            {
                doc.DeleteDate.Value = DateTime.Now;
                doc.SavedID.Value = doc.DividerID.Value;
                doc.SavedID1.Value = doc.FileFolderID.Value;
                doc.UID1.Value = doc.UID.Value;

                doc.DividerID.Value = 0;
                doc.FileFolderID.Value = 0;
                doc.UID.Value = Guid.Empty.ToString();


                try
                {
                    //update filefolder size
                    DataTable dtFileFolder = FileFolderManagement.GetFileFolderSizeByDocumentID(strDocumentID);


                    string strFolderSize = dtFileFolder.Rows[0]["Otherinfo2"].ToString();
                    string strFolderID = dtFileFolder.Rows[0]["FolderID"].ToString();

                    Double newFolderSize = Convert.ToDouble(strFolderSize) - doc.Size.Value;
                    FileFolderManagement.UpdateFolderSize(strFolderID, newFolderSize);

                    //Common_Tatva.AuditLogByDocId(Convert.ToInt32(strDocumentID), Membership.GetUser().ProviderUserKey.ToString(), Enum_Tatva.Action.Delete.GetHashCode());
                    General_Class.AuditLogByDocId(Convert.ToInt32(strDocumentID), Membership.GetUser().ProviderUserKey.ToString(), Enum_Tatva.Action.Delete.GetHashCode());

                    //move document to recycle bin
                    doc.Update();

                }
                catch (Exception exp)
                {
                }
            }

            GetFileData();
            BindGrid();
        }


        //this.UpdatePanel4.Update();
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
            string action = drv.Row[6].ToString();
            if ((action == "2" || action == "Delete"))
            {
                LinkButton libDel = e.Row.FindControl("libDelete") as LinkButton;
                libDel.Visible = false;
                LinkButton libDw = e.Row.FindControl("LinkButton21") as LinkButton;
                libDw.Visible = false;
                //BoundField bfaction = e.Row.FindControl("action") as BoundField;
            }


            //if (this.Page.User.IsInRole("Users"))
            //{
            //    System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
            //    string uid = drv.Row[6].ToString();

            //    if (Membership.GetUser().ProviderUserKey.ToString().ToLower() != uid.ToLower())
            //    {
            //        LinkButton libDel = e.Row.FindControl("libDelete") as LinkButton;
            //        libDel.Visible = false;
            //    }
            //}

        }
    }

    protected void GridView1_RowDeleting_backup(object sender, GridViewDeleteEventArgs e)
    {
        //todo：删除物理文件，需要把文件移动另一个文件夹，以防止被重新上传的同名文件覆盖
        //
        string password = textPassword.Text.Trim();
        string strPassword = Shinetech.Utility.PasswordGenerator.GetMD5(password);

        if (UserManagement.GetPassword(User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
        {
            ScriptManager.RegisterClientScriptBlock(UpdatePanel4, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);

        }
        else
        {
            string strDocumentID = GridView1.DataKeys[e.RowIndex].Value.ToString();

            Document doc = new Document(int.Parse(strDocumentID));

            string ext = ".pdf";
            StreamType extValue = StreamType.PDF;
            try
            {
                extValue = (StreamType)doc.FileExtention.Value;
                ext = this.GetFileExtention(extValue);
            }
            catch
            {
            }

            //doc.Name
            //DataTable dtDocument = FileFolderManagement.GetDocInforByDocumentID(strDocumentID);
            string strPDFName = doc.Name.Value + ext;

            string strPDFPath = "~/" + doc.PathName.Value + "/" + strPDFName;
            if (File.Exists(Server.MapPath(strPDFPath)))
            {
                try
                {
                    File.Delete(Server.MapPath(strPDFPath));
                }
                catch
                {
                }

            }

            //删除文档页 page,并找到页路径进行删除
            DataTable dtPage = FileFolderManagement.GetDocumentByDocumentID(strDocumentID);
            for (int i = 0; i < dtPage.Rows.Count; i++)
            {
                string strPageID = dtPage.Rows[i]["PageID"].ToString();

                //delete quick note，删除每页对应的QuitNote
                FileFolderManagement.DelQucikNoteByPageID(strPageID);

                string strImagePath;
                string strImagePath1;
                string strImagePath2;
                string strImagePath3;

                #region delete image in local

                if (dtPage.Rows[i]["ImagePath"].ToString() != "")
                {
                    strImagePath = "~/" + dtPage.Rows[i]["ImagePath"].ToString();
                    if (File.Exists(Server.MapPath(strImagePath)))
                    {
                        File.Delete(Server.MapPath(strImagePath));
                    }

                }
                if (dtPage.Rows[i]["ImagePath2"].ToString() != "")
                {
                    strImagePath1 = "~/" + dtPage.Rows[i]["ImagePath2"].ToString();
                    if (File.Exists(Server.MapPath(strImagePath1)))
                    {
                        File.Delete(Server.MapPath(strImagePath1));
                    }
                }
                if (dtPage.Rows[i]["ImagePath3"].ToString() != "")
                {
                    strImagePath2 = "~/" + dtPage.Rows[i]["ImagePath3"].ToString();
                    if (File.Exists(Server.MapPath(strImagePath2)))
                    {
                        File.Delete(Server.MapPath(strImagePath2));
                    }
                }
                if (dtPage.Rows[i]["ImagePath4"].ToString() != "")
                {
                    strImagePath3 = "~/" + dtPage.Rows[i]["ImagePath4"].ToString();
                    if (File.Exists(Server.MapPath(strImagePath3)))
                    {
                        File.Delete(Server.MapPath(strImagePath3));
                    }
                }

                #endregion
            }

            //delete Page in SQL
            FileFolderManagement.DelPageByDocumentID(strDocumentID);

            try
            {
                //update filefolder size
                DataTable dtFileFolder = FileFolderManagement.GetFileFolderSizeByDocumentID(strDocumentID);


                string strFolderSize = dtFileFolder.Rows[0]["Otherinfo2"].ToString();
                string strFolderID = dtFileFolder.Rows[0]["FolderID"].ToString();

                DataTable dtDocInfor = FileFolderManagement.GetDocInforByDocumentID(strDocumentID);
                string strDocSize = dtDocInfor.Rows[0]["size"].ToString();

                Double newFolderSize = Convert.ToDouble(strFolderSize) - Convert.ToDouble(strDocSize);
                FileFolderManagement.UpdateFolderSize(strFolderID, newFolderSize);

                //delete Document
                FileFolderManagement.DelDocumentByDocumentID(strDocumentID);
            }
            catch
            {
            }
        }

        GetFileData();
        BindGrid();
        //this.UpdatePanel4.Update();
    }

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();//重新设置数据源，绑定
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }
    protected void Submit2_Click(object sender, EventArgs e)
    {
        GetFileData();
        BindGrid();
    }
}