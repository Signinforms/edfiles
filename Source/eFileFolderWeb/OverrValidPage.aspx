﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="OverrValidPage.aspx.cs" Inherits="OverrValidPage" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt">
                <div class="content-box">
                    <div>
                        You have no more unused EdFiles, please click
                        <asp:HyperLink ID="HyperLink1" NavigateUrl="~/Office/PurchasePage.aspx" Text="here"
                            runat="server" CssClass="foot_url2" />
                        to purchase new EdFiles.
                    </div>
                    <div style="height: 13px"></div>
                    <div>
                        Do you want to                        
                        <asp:HyperLink ID="btnUpload" CssClass="foot_url2" runat="server" Text="Upload Documents" NavigateUrl="~/Office/DocumentLoader.aspx"></asp:HyperLink>
                        or
                        <asp:HyperLink ID="btnNew" CssClass="foot_url2" runat="server" Text="View your EdFiles" NavigateUrl="~/Office/eFileFolders.aspx"></asp:HyperLink>
                        ?
                    </div>

                </div>
            </div>
        </div>
    </div>    
</asp:Content>
