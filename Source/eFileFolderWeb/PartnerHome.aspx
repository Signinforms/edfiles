<%@ Page Language="C#" MasterPageFile="~/PartnerSite.master" AutoEventWireup="true"
	CodeFile="PartnerHome.aspx.cs" Inherits="_PartnerHome" Title="" %>

<%@ Register TagPrefix="fjx" Namespace="com.flajaxian" Assembly="com.flajaxian.FileUploader" %>
<%@ Register TagPrefix="cust" Namespace="Shinetech.Engines.Adapters" Assembly="FormatEngine" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
	<script type="text/javascript">
		var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
		document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
	</script>
	<script type="text/javascript">
		try {
			var pageTracker = _gat._getTracker("UA-672921-3");
			pageTracker._trackPageview();
		} catch(err) {}
	</script>
	
	<!--BEGINNING OF chatstat.com CODE-->
	<script type='text/javascript'>
	var chat = 1;
	var chat_div_id = "chat_div";
	</script>
	<script type='text/javascript' src='https://adlink.chatstat.com/JSBuilder.aspx?csuid=55627' defer='defer'></script><div id="chat_div"></div>
	<noscript></noscript>
	<!--END OF chatstat.com CODE-->


	<script language="JavaScript" type="text/javascript">
	function FileStateChanged(uploader, file, httpStatus, isLast){
		Flajaxian.fileStateChanged(uploader, file, httpStatus, isLast);
		if(file.state > Flajaxian.File_Uploading && isLast){
			Flajaxian_onclick();
		}
	}
 
	function Flajaxian_onclick() {
		var prm = Sys.WebForms.PageRequestManager.getInstance();
		prm._doPostBack('UpdatePanelEFF', '');
	}
	
	</script>
	<table>
		<tr>
			<td valign="top" style="background-repeat: no-repeat;">
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td width="730" height="453" valign="top" background="images/bg_main.gif" bgcolor="#f6f5f2"
							style="background-repeat: repeat-x;">
							<table style="background-position: right top; background-repeat: no-repeat; margin-top: 10px;">
								<tr>
									<td>
										<div style="width: 679px;">
											<%--<div class="Naslov_B_Crven"><b>CONVERT YOUR PAPERS INTO ELECTRONIC, ON SCREEN CHARTS.</b><br />No more handling paper.</div><div class="demo_click"><a href="#"><img src="images/demo_b.gif" border="0" /></a></div>--%>
											<div class="Naslov_BZ_Crven">

												<script language="JavaScript" type="text/javascript">
								  function FileStateChanged(uploader, file, httpStatus, isLast){
		Flajaxian.fileStateChanged(uploader, file, httpStatus, isLast);
		if(file.state > Flajaxian.File_Uploading && isLast){
			Flajaxian_onclick();
		}
	}

	function Flajaxian_onclick() {
		var prm = Sys.WebForms.PageRequestManager.getInstance();
		prm._doPostBack('UpdatePanelEFF', '');


	}

	// Version check for the Flash Player that has the ability to start Player Product Install (6.0r65)
	var hasProductInstall = DetectFlashVer(6, 0, 65);

	// Version check based upon the values defined in globals
	var hasRequestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);

	if ( hasProductInstall && !hasRequestedVersion ) {
		// DO NOT MODIFY THE FOLLOWING FOUR LINES
		// Location visited after installation is complete if installation is required
		var MMPlayerType = (isIE == true) ? "ActiveX" : "PlugIn";
		var MMredirectURL = window.location;
		document.title = document.title.slice(0, 47) + " - Flash Player Installation";
		var MMdoctitle = document.title;

		AC_FL_RunContent(
			"src", "playerProductInstall",
			"FlashVars", "MMredirectURL="+MMredirectURL+'&MMplayerType='+MMPlayerType+'&MMdoctitle='+MMdoctitle+"",
			"width", "100%",
			"height", "290",
			"wmode","opaque",
			"align", "middle",
			"id", "MiniFolderDemo",
			"quality", "high",
			"bgcolor", "#e7e7db",
			"name", "MiniFolderDemo",
			"allowScriptAccess","sameDomain",
			"type", "application/x-shockwave-flash",
			"pluginspage", "http://www.adobe.com/go/getflashplayer"
		);
	} else if (hasRequestedVersion) {
		// if we've detected an acceptable version
		// embed the Flash Content SWF when all tests are passed
		AC_FL_RunContent(
				"src", "MiniFolderDemo",
				"width", "100%",
				"wmode","opaque",
				"height", "290",
				"align", "middle",
				"id", "MiniFolderDemo",
				"quality", "high",
				"bgcolor", "#e7e7db",
				"name", "MiniFolderDemo",
				"allowScriptAccess","sameDomain",
				"type", "application/x-shockwave-flash",
				"pluginspage", "http://www.adobe.com/go/getflashplayer"
		);
	  } else {  // flash is too old or we can't detect the plugin
		var alternateContent = 'Alternate HTML content should be placed here. '
		+ 'This content requires the Adobe Flash Player. '
		+ '<a href=http://www.adobe.com/go/getflash/>Get Flash</a>';
		document.write(alternateContent);  // insert non-flash content
	  }

												</script>

											</div>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="Naslov_B_Crven">
											<b>Introducing EdFiles...They Look, Feel and Work just like Paper File Folders,
												but without the Physical Paper File Folders.</b></div>
										<%--<div class="tekstTitleDef">
											<br />
										   <font color="#333333">Easy to Use. Easy to Try. (</font>
											<asp:HyperLink ID="HyperLink4" ForeColor="#9a9897" runat="server" NavigateUrl="~/WebUser/PartnerUser.aspx"
												Text="FREE Trial"></asp:HyperLink>
											<font color="#333333">)</font>
										</div>--%>
										<div class="podnaslov">
											EdFiles will Securely & Conveniently allow you to Manage, Store and Retrieve
											Documents and Files with our Patent Pending File Folder Viewer that Looks, Feels
											and Works just like Paper File Folders, but without the actual File Folders.                                            
										</div>
										<div class="podnaslov">Worried about the tremendous cost of scanning paper��Let us scan your current paper files and transform them into EdFiles for FREE. 
										 <strong>Call 1-657-217-3260 </strong>to see if you qualify 
										 for our <a href="STORAGECONV.pdf" target="_blank">Storage Conversion Program</a>.</div> 
										<div class="AuthorizeNetSeal"> <script type="text/javascript" language="javascript">
										   var ANS_customer_id = "82f45bba-e260-46db-8842-62e483a8dd92";</script> 
										<script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js" ></script> 
										<a href="http://www.authorize.net/" id="AuthorizeNetText" target="_blank">Online Payments</a> </div>
									</td>
								</tr>
							</table>
							<embed src="efilefolders_audio2.wav"  volume="100" autostart="true" hidden="true" loop="false"></embed>
						</td>
						<td width="10" align="center" valign="top" style="background: url(images/lin.gif) 5px 0 repeat-y #fff;">
							<img src="images/bai.gif" height="10px" width="100%" /></td>
						<td valign="top" background="images/bg_main.gif" bgcolor="#f6f5f2" style="background-repeat: repeat-x;">
							<table>
								<tr valign="top">
									<td style="margin-left: 0px; color: #f6f5f2;">
										<table>
											<tr height="4px">
											<tr>
												<td>
													<asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="">
														<asp:Image ID="image5" runat="server" ImageUrl="images/step_7.png" />
													</asp:HyperLink>
												</td>
											</tr>
											<tr height="4px">
											</tr>
											<tr>
												<td>
													<%--<asp:HyperLink ID="hypImage1" runat="server" NavigateUrl="~/WebUser/PartnerUser.aspx">
														<asp:Image ID="image1" runat="server" ImageUrl="images/step_1.png" />
													</asp:HyperLink>--%>
												</td>
											</tr>
											<tr height="4px">
											</tr>
											<tr>
												<td>
													<asp:HyperLink ID="hypImage2" runat="server" NavigateUrl="~/WebUser/WebLogin.aspx">
														<asp:Image ID="image2" runat="server" ImageUrl="images/step_2.png" />
													</asp:HyperLink>
												</td>
											</tr>
											<tr height="4px">
											</tr>
											<tr>
												<td>
													<asp:HyperLink ID="hypImage3" runat="server" NavigateUrl="~/WebUser/WebLogin.aspx">
														<asp:Image ID="image3" runat="server" ImageUrl="images/step_3.png" />
													</asp:HyperLink>
												</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td>
										<div class="div_background" style="width: 100%; height: 3px">
										</div>
										<div class="Naslov_Z_Crven">
											<a href="InstallAIR.aspx">Download EdFiles Viewer<br />
											</a>
										</div>
									</td>
								</tr>
								<tr>
									<td>
										<div class="Naslov_Plav_1" style="width: 200px;">
											<br />
											View EFF Files</div>
										<div class="Naslov_Plav_2" style="width: 200px;">
											<fjx:FileUploader ID="FileUploader1" TransparentBackground="true" MaxNumberFiles="5" MaxFileSize="10KB" MaxFileSizeReachedMessage="No files bigger than {0} are allowed"
												AllowedFileTypes="EFF Files(*eff):*.eff;" OnFileReceived="FileUploader1_OnFileReceived"
												UseInsideUpdatePanel="true" JsFunc_FileStateChanged="FileStateChanged" runat="server">
												<Adapters>
												</Adapters>
											</fjx:FileUploader>
										</div>
										<div class="Naslov_Plav_2" style="width: 100%;">
											<asp:Repeater ID="Repeater1" runat="server">
												<HeaderTemplate>
													<table width="100%" border="0" cellpadding="2" cellspacing="0">
												</HeaderTemplate>
												<ItemTemplate>
													<tr>
														<td align="left" class="tekst_2">
															<asp:Label ID="Label1" CssClass="reminder_tekst" Font-Bold="true" Text='<%# string.Format("{0}.",Eval("Number"))%>'
																runat="server" />
															<asp:LinkButton ID="LinkButton1" CausesValidation="false" CssClass="reminder_tekst"
																ForeColor="Blue" Font-Bold="true" runat="server" Text='<%# string.Format("{0}",Eval("FolderName"))%>'
																OnClientClick='<%# string.Format("window.open(\"Office/WebFlashViewer.aspx?ID={0}\",\"eBook\",\"toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no\")",Eval("FolderID"))%>' />
															<asp:Label ID="Label2" CssClass="reminder_tekst" Font-Bold="true" Text='<%# string.Format("{0:dd.MM.yyyy}",Eval("DOB"))%>'
																runat="server" /></td>
													</tr>
												</ItemTemplate>
												<AlternatingItemTemplate>
													<tr>
														<td align="left" bgcolor="#e8e7e1" class="tekst_2">
															<asp:Label ID="Label3" CssClass="reminder_tekst" Font-Bold="true" Text='<%# string.Format("{0}.",Eval("Number"))%>'
																runat="server" />
															<asp:LinkButton ID="LinkButton2" CausesValidation="false" CssClass="reminder_tekst"
																ForeColor="Blue" Font-Bold="true" runat="server" Text='<%# string.Format("{0} ",Eval("FolderName"))%>'
																OnClientClick='<%# string.Format("window.open(\"Office/WebFlashViewer.aspx?ID={0}\",\"eBook\",\"toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no\")",Eval("FolderID"))%>' />
															<asp:Label ID="Label4" CssClass="reminder_tekst" Font-Bold="true" Text='<%# string.Format("{0:dd.MM.yyyy}",Eval("DOB"))%>'
																runat="server" /></td>
													</tr>
												</AlternatingItemTemplate>
												<FooterTemplate>
													</table>
												</FooterTemplate>
											</asp:Repeater>
										</div>
										
									</td>
								</tr>
								<tr>
									<td>
										<div class="Naslov_Plav_4" style="width: 200px;">
											EdFiles News</div>
										<table>
										<tr ><td class="Naslov_Z_Crven_1">
											<a href="http://investor.news.com/cnet?GUID=8204395&Page=MediaViewer&ChannelID=3197" target="_blank">CNET News
											</a></td>
										</tr>
										<tr><td class="Naslov_Z_Crven_1">
											<a href="http://www.coolest-gadgets.com/20090220/efilefolders-looks-and-works-just-like-paper-folders/" target="_blank">Coolest-Gadgets
											</a></td>
										</tr>
										<tr ><td class="Naslov_Z_Crven_1">
											<a href="http://www.youtube.com/watch?v=uXD2YWNlpd4&layer_token=76c6b6dacf12dab2" target="_blank">YouTube Video
											</a></td>
										</tr>
										<tr ><td><a href="Security.aspx"><img src="images/SecurityApproach.png"  width="121"/></a></td></tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</asp:Content>
