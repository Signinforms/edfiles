using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using Shinetech.DAL;
using Shinetech.Utility;
using System.Runtime.Remoting.Messaging;
using Account = Shinetech.DAL.Account;


public partial class PartnerSite : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {


    }

    public string DateTimeInfo
    {
        get
        {
            DateTimeFormatInfo myDTFI = new CultureInfo("en-US", false).DateTimeFormat;

            DateTime dt = DateTime.Now;
            return dt.ToString("dddd dd", myDTFI) + "th " + dt.ToString("MMMM yyyy", myDTFI);
        }
    }

    public RecentFolder RecentFolder
    {
        get
        {
            RecentFolder recent = null;

            if (this.Session["RecentFolder"] == null)
            {
                string uid = Membership.GetUser().ProviderUserKey.ToString();
                recent = new RecentFolder(uid);
            }
            else
            {
                recent = this.Session["RecentFolder"] as RecentFolder;
            }

            return recent;
        }
    }

    public void AddFolderItem(string CommandArgument)
    {
        string uid = Membership.GetUser().ProviderUserKey.ToString();
        string temp = CommandArgument.ToString();
        string[] items = temp.Split(',');
        string key = items[0];
        string firstname = items[1];
        string lastname = items[2];

        FolderItem item = new FolderItem(key);
        item.FirstName = firstname;
        item.LastName = lastname;
        item.DOB = DateTime.Now;
        item.UID = uid;
        item.Number = 1;

        RecentFolder.Add(item);


    }
    protected void LoginButton_Click(object sender, ImageClickEventArgs e)
    {
        string strURL = Request.Url.LocalPath.ToString();
        strURL = Path.GetFileName(strURL);


        Login login1 = this.loginView1.FindControl("login1") as Login;
        string strUserName = login1.UserName;
        string strPassword = PasswordGenerator.GetMD5(login1.Password);

        if (strPassword.Trim() == "" || strUserName.Trim() == "")
        {
            if (strURL == "Default.aspx")
            {
                Response.Redirect("WebUser/WebLogin.aspx");
            }
            else
            {
                Response.Redirect("WebLogin.aspx");
            }

        }
        else
        {
            if (strPassword == UserManagement.GetPassword(strUserName))
            {
                if (UserManagement.IsApproved(strUserName))
                {
                    FormsAuthentication.RedirectFromLoginPage(login1.UserName, login1.RememberMeSet);

                    string uid = Membership.GetUser(strUserName).ProviderUserKey.ToString();
                    Account account = new Account(uid);
                    string ip = Request.ServerVariables["REMOTE_ADDR"];

                    UserManagement.InsertVisitor(account, ip,this.Session.SessionID,true);

                    if (strURL == "Default.aspx")
                    {
                        Response.Redirect("default.aspx");
                    }
                    else
                    {
                        Response.Redirect("~/default.aspx");
                    }

                }
                else
                {
                    DataTable dtTemp = new DataTable();
                    dtTemp = UserManagement.IsApprovedInTemp(strUserName);
                    if (dtTemp.Rows.Count == 0)
                    {
                        string strWrongInfor = String.Format("The user {0} is locked by administrator.", strUserName);
                        Page.RegisterStartupScript("RssOk", "<script type='text/javascript'>alert('" + strWrongInfor + "')</script>");
                    }
                    else
                    {
                        if (strURL == "Default.aspx")
                        {
                            Response.Redirect("WebUser/UserConfirmation.aspx?UserName=" + strUserName);
                        }
                        else
                        {
                            Response.Redirect("WebUser/UserConfirmation.aspx?UserName=" + strUserName);
                        }
                    }

                }


            }
        }

    }

    protected void libForgotP_Click(object sender, EventArgs e)
    {
        string strUserName;
        Login login1 = this.loginView1.FindControl("login1") as Login;
        strUserName = login1.UserName;
        ;
        if (strUserName != "")
        {
            if (Membership.GetUser(strUserName) == null)
            {
                // ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert('this user name is not exist.');", true);
                Page.RegisterStartupScript("RssOk", "<script type='text/javascript'>alert('this user name is not exist.')</script>");
                return;
            }
            else
            {
                Response.Redirect("WebUser/ForgetPassword.aspx?UserName=" + strUserName);
            }
        }
        else
        {
            Response.Redirect("WebUser/ForgetPassword.aspx");
        }




    }
}
