<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="Pricing.aspx.cs" Inherits="Pricing" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="970" height="453" valign="top" background="images/bg_main.gif" bgcolor="#f6f5f2"
                                        style="background-repeat: repeat-x;">
                                        <div style="background: url(../images/inner_b.gif); background-position: right top;
                                            background-repeat: no-repeat; margin-top: 10px;">
                                            <div style="width: 725px; height: 60px;">
                                                <div class="Naslov_Crven">
                                                    Pricing</div>
                                                <%--<div class="demo_click">
                                                </div>--%>
                                            </div>
                                            <div style="width: 679px;">
                                            <%--<div class="Naslov_B_Crven"><b>CONVERT YOUR PAPERS INTO ELECTRONIC, ON SCREEN CHARTS.</b><br />No more handling paper.</div><div class="demo_click"><a href="#"><img src="images/demo_b.gif" border="0" /></a></div>--%>
                                            <div class="Naslov_BZ_Crven">

                                                <script language="JavaScript" type="text/javascript">
                                  

    // Version check for the Flash Player that has the ability to start Player Product Install (6.0r65)
    var hasProductInstall = DetectFlashVer(6, 0, 65);

    // Version check based upon the values defined in globals
    var hasRequestedVersion = DetectFlashVer(requiredMajorVersion, requiredMinorVersion, requiredRevision);

    if ( hasProductInstall && !hasRequestedVersion ) {
	    // DO NOT MODIFY THE FOLLOWING FOUR LINES
	    // Location visited after installation is complete if installation is required
	    var MMPlayerType = (isIE == true) ? "ActiveX" : "PlugIn";
	    var MMredirectURL = window.location;
        document.title = document.title.slice(0, 47) + " - Flash Player Installation";
        var MMdoctitle = document.title;

	    AC_FL_RunContent(
		    "src", "playerProductInstall",
		    "FlashVars", "MMredirectURL="+MMredirectURL+'&MMplayerType='+MMPlayerType+'&MMdoctitle='+MMdoctitle+"",
		    "width", "100%",
		    "height", "290",
		    "wmode","opaque",
		    "align", "middle",
		    "id", "MiniFolderDemo",
		    "quality", "high",
		    "bgcolor", "#e7e7db",
		    "name", "MiniFolderDemo",
		    "allowScriptAccess","sameDomain",
		    "type", "application/x-shockwave-flash",
		    "pluginspage", "http://www.adobe.com/go/getflashplayer"
	    );
    } else if (hasRequestedVersion) {
	    // if we've detected an acceptable version
	    // embed the Flash Content SWF when all tests are passed
	    AC_FL_RunContent(
			    "src", "MiniFolderDemo",
			    "width", "100%",
			    "wmode","opaque",
			    "height", "290",
			    "align", "middle",
			    "id", "MiniFolderDemo",
			    "quality", "high",
			    "bgcolor", "#e7e7db",
			    "name", "MiniFolderDemo",
			    "allowScriptAccess","sameDomain",
			    "type", "application/x-shockwave-flash",
			    "pluginspage", "http://www.adobe.com/go/getflashplayer"
	    );
      } else {  // flash is too old or we can't detect the plugin
        var alternateContent = 'Alternate HTML content should be placed here. '
  	    + 'This content requires the Adobe Flash Player. '
   	    + '<a href=http://www.adobe.com/go/getflash/>Get Flash</a>';
        document.write(alternateContent);  // insert non-flash content
      }

                                                </script>

                                            </div>
                                            <div class="podnaslovPricing" style="padding: 5px; background-color: #f6f5f2;">
                                                By now you have seen and experienced what EdFiles are and how they work. If
                                                you have not seen the demo or still do not completely understand how EdFiles
                                                work, please call 800-579-9190 to speak to one of our customer service representatives
                                                for additional information on EdFiles.</div>
                                            <div class="podnaslovPricing" style="padding: 5px; background-color: #f6f5f2;">
                                                If you are ready to use and purchase EdFiles, below is the pricing breakdown
                                                on EdFiles and Storage Fees.</div>
                                            <div class="podnaslovPricing" style="padding: 5px; background-color: #f6f5f2;">
                                                <table width="85%">
                                                    <tr>
                                                        <td colspan="2">
                                                            EdFiles will be sold in the following breakdowns.
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView runat="server" ID="gvBilling" BorderWidth="0" AllowPaging="True" AutoGenerateColumns="False"
                                                                GridLines="None" DataKeyNames="UID" Width="100%" BorderStyle="None" PageSize="8">
                                                                <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                      <asp:LinkButton ID="LinkButton1" runat="server"  Text='<%# Bind("Name") %>' OnClick="LinkButton1_Click"></asp:LinkButton>
                                                                     </ItemTemplate></asp:TemplateField>
                                                                    
                                                                    <asp:BoundField DataField="Price" DataFormatString="${0:F2}">
                                                                        <HeaderStyle Width="75%" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    <%--<tr>
                                                        <td>
                                                            25 Pack
                                                        </td>
                                                        <td align="left">
                                                            $9.95 (pricing discount 10% on larger quantities...Make this links to purchase)
                                                        </td>
                                                    </tr>--%>
                                                </table>
                                            </div>
                                            <%--<div class="podnaslovPricing" style="padding: 5px; background-color: #f6f5f2;">
                                                In addition to the purchasing eFileFolders, you will be required to pay a small
                                                monthly storage fee. This fee is to cover the actual storage cost of all the information
                                                that you will store in each of your eFileFolders. A $19.95 fee per gigabyte will be assessed on a monthly basis.
                                            </div>--%>
                                            <div class="podnaslovPricing" style="padding: 5px; background-color: #f6f5f2;">
                                                Welcome to the "Evolution of the File Folder", we strongly believe that you will
                                                find EdFiles as a cost saving, easy to use technology solution to improve your
                                                file storage.</div>
                                            <div class="podnaslovPricing" style="padding: 5px; background-color: #f6f5f2;">
                                                For additional assistance, please call 800-579-9190</div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="10" align="center">
                            <img src="images/lin.gif" width="1" height="453" vspace="10" /></td>
                        <td valign="top" background="images/bg_main.gif" bgcolor="#f6f5f2" style="background-repeat: repeat-x;">
                            <div class="Naslov_Plav" style="width: 200px;">
                                New User?</div>
                            <table width="200" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 20px 10px;">
                                <tr>
                                    <td align="center">
                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/WebUser/WebOfficeUser.aspx">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/register.gif" />
                                            <!--<input name="Submit2" type="image" value="GO" src="images/register.gif" align="absbottom"/> -->
                                        </asp:HyperLink></td>
                                </tr>
                            </table>
                            <div class="call" align="center">
                                Register directly by calling
                            </div>
                            <div align="center" class="phone">
                                800-579-9190</div>
                                <div class="div_background" style="width: 100%; height: 3px">
                                        </div>
                                <div  class="Naslov_Z_Crven">
                                            <a href="InstallAIR.aspx" class="Naslov_Y_Crven_1">Download EdFiles Viewer<br />
                                            </a>
                                        </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
