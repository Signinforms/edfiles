using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Pricing : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            CurrentFolderUID = "0";
            GetData();
            BindGrid();
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!this.Page.User.Identity.IsAuthenticated)
        {
            this.MasterPageFile = "~/LoginMasterPage.master";
        }
    }

    #region 数据绑定
    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData()
    {
        this.DataSource = LicenseManagement.GetLicenseWithoutTrial();
    }
    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        gvBilling.DataSource = this.DataSource;
        gvBilling.DataKeyNames = new string[] { "UID" };//主键
        gvBilling.DataBind();
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["License"] as DataTable;
        }
        set
        {
            this.Session["License"] = value;
        }
    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;
        string uid = Convert.ToString(this.gvBilling.DataKeys[row.RowIndex].Value);
        CurrentFolderUID = uid;

        Response.Redirect("~/Office/PaymentPage.aspx",true);
    }

    public string CurrentFolderUID
    {
        get
        {
            if (this.Session["_CurrentFolderUID"] == null)
                return "0";

            return Convert.ToString(this.Session["_CurrentFolderUID"]);
        }

        set
        {
            this.Session["_CurrentFolderUID"] = value;
        }
    }


    #endregion
}
