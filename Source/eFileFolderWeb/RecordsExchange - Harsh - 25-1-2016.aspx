<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RecordsExchange.aspx.cs" Inherits="RecordsExchange" Title="Online file folders, EdFiles, Electronic Document Storage Scanning -Records Exchange" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="970" height="453" valign="top" background="images/bg_main.gif" bgcolor="#f6f5f2"
                                                style="background-repeat: repeat-x;">
                                                <div style="background: url(../images/inner_b.gif); background-position: right top; background-repeat: no-repeat; margin-top: 10px;">
                                                    <div style="width: 725px; height: 60px;">
                                                        <div class="Naslov_Crven">
                                                            Health Records Exchange Details & Enrollment Form
                                                        </div>
                                                    </div>
                                                    <div style="width: 679px;">

                                                        <div class="Naslov_BZ_Crven">
                                                        </div>
                                                        <div class="podnaslov" style="padding: 5px; margin-bottom: 0px; background-color: #f6f5f2;">
                                                            As a healthcare provider, you already know, maintaining access to IN-ACTIVE Patient��s records, years after their last visit to your office is a requirement that must be met.  
                                                    Depending on your specialty, the length of time varies from 7 years to 28 years to forever!
                                                <br />
                                                            Below are 3 questions��If you answer yes to any one questions��you are eligible to enroll in the Healthcare Provider��s Records Exchange Program at NO Direct Cost to you.
                                                    <div class="podnaslov1" style="padding: 0px; margin-bottom: 0px; background-color: #f6f5f2;">
                                                        <div>
                                                            1.	Do you have IN-ACTIVE Patient��s Records taking up space in your office?
                                                        </div>
                                                        <div>
                                                            2.	Are you $ paying fees to store IN-ACTIVE Patient��s Records?
                                                        </div>
                                                        <div>
                                                            3.	Is the Cost of Storing or Scanning/Archiving these IN-ACTIVE Patient��s Records Too Much for You or not worth the expense?
                                                        </div>
                                                    </div>
                                                        </div>
                                                        <div class="podnaslov" style="padding: 0px; margin-bottom: 0px; background-color: #f6f5f2;">
                                                            If you answered yes to any of these��There is a better way!  To get started, please fill in the form below and we can get your enrollment 
                                                    started.  Call <strong>1-855-334-5336 x1</strong>, if you need immediate answers to your questions.
                                                        </div>
                                                        <div class="podnaslov" style="padding: 5px; margin-bottom: 0px; background-color: #f6f5f2;">

                                                            <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 0;">
                                                                <tr>
                                                                    <td align="left" class="podnaslov">Office&nbsp;Name:</td>
                                                                    <td width="30%">
                                                                        <asp:TextBox ID="textfield6" name="textfield6" runat="server" class="form_tekst_field"
                                                                            size="30" MaxLength="20" />
                                                                        <asp:RequiredFieldValidator runat="server" ID="firstNameV" ControlToValidate="textfield6"
                                                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The office name is required!" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                                                            TargetControlID="firstNameV" HighlightCssClass="validatorCalloutHighlight" />
                                                                    </td>
                                                                    <td width="17%" rowspan="4" valign="middle">
                                                                        <div class="tekstDef">
                                                                        </div>
                                                                    </td>
                                                                    <td align="left" class="podnaslov">Contact&nbsp;Name:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="textfield7" runat="server" class="form_tekst_field" size="30" MaxLength="20" />
                                                                        <asp:RequiredFieldValidator runat="server" ID="lastNameV" ControlToValidate="textfield7"
                                                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The contact name is required!" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10"
                                                                            TargetControlID="lastNameV" HighlightCssClass="validatorCalloutHighlight" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="podnaslov">City:</td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" class="form_tekst_field" ID="txtCity" size="30" MaxLength="30" />
                                                                    </td>
                                                                    <td align="left" class="podnaslov">State:</td>
                                                                    <td>
                                                                        <asp:DropDownList ID="drpState" runat="server" class="form_tekst_field" Width="185px" /></td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="podnaslov">Tel.:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="textfield8" name="textfield8" runat="server" class="form_tekst_field"
                                                                            size="30" MaxLength="23" />
                                                                        <asp:RequiredFieldValidator runat="server" ID="telN" ControlToValidate="textfield8"
                                                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The telephone is required!" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11"
                                                                            TargetControlID="telN" HighlightCssClass="validatorCalloutHighlight" />
                                                                        <asp:RegularExpressionValidator ID="TelV" ControlToValidate="textfield8" runat="server"
                                                                            ErrorMessage="<b>Required Field Missing</b><br />It is not a telphone format."
                                                                            ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                                                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                                                            TargetControlID="TelV" HighlightCssClass="validatorCalloutHighlight" />
                                                                    </td>
                                                                    <td align="left" class="podnaslov">Fax.:</td>
                                                                    <td>
                                                                        <asp:TextBox name="textfield9" type="text" runat="server" class="form_tekst_field"
                                                                            ID="textfield9" size="30" MaxLength="23" />
                                                                        <asp:RegularExpressionValidator ID="faxV" ControlToValidate="textfield8" runat="server"
                                                                            ErrorMessage="<b>Required Field Missing</b><br />It is not a FAX format." ValidationExpression="[^A-Za-z]{7,}"
                                                                            Display="None"></asp:RegularExpressionValidator>
                                                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8"
                                                                            TargetControlID="faxV" HighlightCssClass="validatorCalloutHighlight" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" class="podnaslov">Email:</td>
                                                                    <td>
                                                                        <asp:TextBox ID="textfield13" runat="server" class="form_tekst_field" size="30" MaxLength="50" />
                                                                        <asp:RequiredFieldValidator runat="server" ID="EmailReq" ControlToValidate="textfield13"
                                                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />An email is required." />
                                                                        <asp:RegularExpressionValidator ID="rev_Email" runat="server" ErrorMessage="<b>Required Field Missing</b><br />It is not an email format."
                                                                            ControlToValidate="textfield13" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                            Visible="true" Display="None"></asp:RegularExpressionValidator>
                                                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="EmailReqE" TargetControlID="EmailReq"
                                                                            HighlightCssClass="validatorCalloutHighlight" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                                                            TargetControlID="rev_Email" HighlightCssClass="validatorCalloutHighlight" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <p>1. Where are your In-Active Charts currently stored?<asp:DropDownList ID="drpBoxPlaces" runat="server" class="form_tekst_field" Width="125px" /></p>
                                                            <p>
                                                                2. How many 3rd party records requests and patient transfer requests do you get in your office on a monthly basis? (Approximately)
                                                        <asp:TextBox name="textfieldRecord" type="text" runat="server" class="form_tekst_field"
                                                            ID="textfieldRecord" size="20" MaxLength="10" />
                                                            </p>
                                                            <p>
                                                                3. How many years have you been in private practice?
                                                        <asp:TextBox name="textfieldPracticeYear" type="text" runat="server" class="form_tekst_field"
                                                            ID="textfieldPracticeYear" size="20" MaxLength="10" />
                                                            </p>
                                                            <p>
                                                                4. For how many years do you need to store your charts?
                                                        <asp:TextBox name="textfieldChartYear" type="text" runat="server" class="form_tekst_field"
                                                            ID="textfieldChartYear" size="20" MaxLength="10" />

                                                            </p>
                                                            <p>
                                                                5. Are you using still using paper charts?
                                                       <asp:DropDownList ID="drpBoxUsePaper" runat="server" class="form_tekst_field" Width="105px" />
                                                            </p>
                                                            <p>
                                                                6. Will you convert to electronic health records?
                                                        <asp:DropDownList ID="drpBoxUseElectronic" runat="server" class="form_tekst_field" Width="105px" />
                                                            </p>
                                                            <p>
                                                                7. Considering retirement and or selling your practice?
                                                        <asp:DropDownList ID="drpBoxsellPractice" runat="server" class="form_tekst_field" Width="105px" />
                                                            </p>
                                                            <p><strong>Absolutely NO Direct Cost to you!  Let us show you how.</strong></p>
                                                        </div>

                                                        <div align="center" style="height: 30px;">
                                                            <asp:Button ID="ImageButton1" runat="server" Text="Submit" Font-Bold="True" OnClick="ImageButton1_Click" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="10" align="center">
                                    <img src="images/lin.gif" width="1" height="453" vspace="10" /></td>
                                <td valign="top" background="images/bg_main.gif" bgcolor="#f6f5f2" style="background-repeat: repeat-x;">
                                    <div class="Naslov_Plav" style="width: 200px;">
                                        New User?
                                    </div>
                                    <table width="200" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 20px 10px;">
                                        <tr>
                                            <td align="center">
                                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/WebUser/WebOfficeUser.aspx">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/register.gif" />

                                                </asp:HyperLink></td>
                                        </tr>
                                    </table>
                                    <div class="call" align="center">
                                        Register directly by calling
                                    </div>
                                    <div align="center" class="phone">
                                        800-579-9190
                                    </div>
                                    <div class="div_background" style="width: 100%; height: 3px">
                                    </div>
                                    <div class="Naslov_Z_Crven">
                                        <a href="InstallAIR.aspx" class="Naslov_Y_Crven_1">Download edFile Viewer<br />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
