<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RecordsExchange.aspx.cs" Inherits="RecordsExchange" Title="" %>
<%@ Register Src="~/Controls/RegisterDirectly.ascx" TagName="ucRegisterDirectly" TagPrefix="ucRegisterDirectly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        select {
            font-size:13px;
            font-family:Arial;
        }

        .inputBox {
            font-size: initial;
            color: initial;
            font-size: 14px;
        }
        .ajax__validatorcallout_error_message_cell {
            line-height:normal;
            vertical-align:top
        }

        .link-head {
            height:47px !important;
        }
    </style>
 <div class="title-container aboutus-title">
  <div class="inner-wrapper">
   <h1 style="padding-top:15px">Health Records Exchange Details & Enrollment Form</h1>
  </div>
 </div>
 <div class="page-container register-container">
  <div class="inner-wrapper">
   <div class="aboutus-contant">
    <div class="left-content">
     <div class="work_content">
      <p>As a healthcare provider, you already know, maintaining access to IN-ACTIVE Patient��s records, years after their last visit to your office is a requirement that must be met. Depending on your specialty, the length of time varies from 7 years to 28 years to forever!</p>
      <p>Below are 3 questions��If you answer yes to any one questions��you are eligible to enroll in the Healthcare Provider��s Records Exchange Program at NO Direct Cost to you.</p>
     </div>
     <ul class="mrg-bt-20">
      <li>Do you have IN-ACTIVE Patient��s Records taking up space in your office?</li>
      <li>Are you $ paying fees to store IN-ACTIVE Patient��s Records?</li>
      <li>Is the Cost of Storing or Scanning/Archiving these IN-ACTIVE Patient��s Records Too Much for You or not worth the expense?</li>
     </ul>
     <div class="work_content">
      <p>If you answered yes to any of these��There is a better way!  To get started, please fill in the form below and we can get your enrollment started.  Call <strong>1-657-217-3260</strong>, if you need immediate answers to your questions.</p>
     </div>
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
      <ContentTemplate>
       <div class="form-containt">
        <div class="content-box">
         <!--<fieldset>-->
         <fieldset>
         <label>Office Name</label>
         <asp:TextBox ID="textfield6" name="textfield6" runat="server"
                                                                            size="30" MaxLength="20" CssClass="inputBox"/>
         <asp:RequiredFieldValidator runat="server" ID="firstNameV" ControlToValidate="textfield6"
                                                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The office name is required!" />
         <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                                                            TargetControlID="firstNameV" HighlightCssClass="validatorCalloutHighlight" />
         </fieldset>
         <!--</fieldset>-->
         <fieldset>
         <label>Contact&nbsp;Name:</label>
         <asp:TextBox ID="textfield7" runat="server" size="30" MaxLength="20" CssClass="inputBox"/>
         <asp:RequiredFieldValidator runat="server" ID="lastNameV" ControlToValidate="textfield7"
                                                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The contact name is required!" />
         <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10"
                                                                            TargetControlID="lastNameV" HighlightCssClass="validatorCalloutHighlight" />
         </fieldset>
         <fieldset>
         <label>City</label>
         <asp:TextBox runat="server" ID="txtCity" size="30" MaxLength="30" CssClass="inputBox"/>
         </fieldset>
         <fieldset>
         <label>State:</label>
         <asp:DropDownList ID="drpState" runat="server" />
         </fieldset>
         <fieldset>
         <label>Tel.:</label>
         <asp:TextBox ID="textfield8" name="textfield8" runat="server"
                                                                            size="30" MaxLength="23" CssClass="inputBox"/>
         <asp:RequiredFieldValidator runat="server" ID="telN" ControlToValidate="textfield8"
                                                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The telephone is required!" />
         <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11"
                                                                            TargetControlID="telN" HighlightCssClass="validatorCalloutHighlight" />
         <asp:RegularExpressionValidator ID="TelV" ControlToValidate="textfield8" runat="server"
                                                                            ErrorMessage="<b>Required Field Missing</b><br />It is not a telphone format."
                                                                            ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
         <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                                                            TargetControlID="TelV" HighlightCssClass="validatorCalloutHighlight" />
         </fieldset>
         <fieldset>
         <label>Fax.:</label>
         <asp:TextBox name="textfield9" type="text" runat="server"
                                                                            ID="textfield9" size="30" MaxLength="23" CssClass="inputBox"/>
         <asp:RegularExpressionValidator ID="faxV" ControlToValidate="textfield8" runat="server"
                                                                            ErrorMessage="<b>Required Field Missing</b><br />It is not a FAX format." ValidationExpression="[^A-Za-z]{7,}"
                                                                            Display="None"></asp:RegularExpressionValidator>
         <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8"
                                                                            TargetControlID="faxV" HighlightCssClass="validatorCalloutHighlight" />
         </fieldset>
         <fieldset>
         <label>Email:</label>
         <asp:TextBox ID="textfield13" runat="server" size="30" MaxLength="50" CssClass="inputBox"/>
         <asp:RequiredFieldValidator runat="server" ID="EmailReq" ControlToValidate="textfield13"
                                                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />An email is required." />
         <asp:RegularExpressionValidator ID="rev_Email" runat="server" ErrorMessage="<b>Required Field Missing</b><br />It is not an email format."
                                                                            ControlToValidate="textfield13" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                                            Visible="true" Display="None"></asp:RegularExpressionValidator>
         <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="EmailReqE" TargetControlID="EmailReq"
                                                                            HighlightCssClass="validatorCalloutHighlight" />
         <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                                                            TargetControlID="rev_Email" HighlightCssClass="validatorCalloutHighlight" />
         </fieldset>
         <fieldset>
         <div class="work_content">
          <p>1. Where are your In-Active Charts currently stored?
           <asp:DropDownList ID="drpBoxPlaces" runat="server" Width="105px" />
          </p>
          <p>2. How many 3rd party records requests and patient transfer requests do you get in your office on a monthly basis? (Approximately)
           <asp:TextBox name="textfieldRecord" type="text" runat="server" ID="textfieldRecord" size="20" MaxLength="10" Width="105px" />
          </p>
          <p>3. How many years have you been in private practice?
           <asp:TextBox name="textfieldPracticeYear" type="text" runat="server"
                                                            ID="textfieldPracticeYear" size="20" MaxLength="10" Width="105px" />
          </p>
          <p> 4. For how many years do you need to store your charts?
           <asp:TextBox name="textfieldChartYear" type="text" runat="server"
                                                            ID="textfieldChartYear" size="20" MaxLength="10" Width="105px" />
          </p>
          <p> 5. Are you using still using paper charts?
           <asp:DropDownList ID="drpBoxUsePaper" runat="server" Width="105px" />
          </p>
          <p> 6. Will you convert to electronic health records?
           <asp:DropDownList ID="drpBoxUseElectronic" runat="server" Width="105px" />
          </p>
          <p> 7. Considering retirement and or selling your practice?
           <asp:DropDownList ID="drpBoxsellPractice" runat="server" Width="105px" />
          </p>
          <p><strong>Absolutely NO Direct Cost to you!  Let us show you how.</strong></p>
         </div>				 
         </fieldset>
        </div>
				<div class="form-submit-btn">									
						<asp:Button ID="ImageButton1" class="create-btn  btn green" runat="server" Text="Submit" Font-Bold="True" OnClick="ImageButton1_Click" />
				 </div>
       </div>
      </ContentTemplate>
      <Triggers>
       <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
      </Triggers>
     </asp:UpdatePanel>     
    </div>
		<div class="right-content">
      <ucRegisterDirectly:ucRegisterDirectly ID="ucRegister" runat="server" />
     </div>
   </div>
  </div>
 </div>
</asp:Content>
