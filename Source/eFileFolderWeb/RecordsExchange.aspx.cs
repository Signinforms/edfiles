using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Utility;

public partial class RecordsExchange : System.Web.UI.Page
{
    private static readonly string AJIWANIEMAIL = ConfigurationManager.AppSettings["AJIWANIEMAIL"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ArrayList YesNo = new ArrayList();
            YesNo.Add("-Option-");
            YesNo.Add("Yes"); YesNo.Add("No");

            ArrayList places = new ArrayList();
            places.Add("Office");
            places.Add("Storage Unit");
            places.Add("Storage Company");
            places.Add("Home");
            places.Add("Other");

            drpBoxPlaces.DataSource = places;
            drpBoxUsePaper.DataSource = YesNo;
            drpBoxUseElectronic.DataSource = YesNo;
            drpBoxsellPractice.DataSource = YesNo;


            drpBoxPlaces.DataBind();
            drpBoxUsePaper.DataBind();
            drpBoxUseElectronic.DataBind();
            drpBoxsellPractice.DataBind();

            this.drpState.DataSource = States;
            this.drpState.DataValueField = "StateID";
            this.drpState.DataTextField = "StateName";
            this.drpState.DataBind();
        }


    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!this.Page.User.Identity.IsAuthenticated)
        {
            this.MasterPageFile = "~/LoginMasterPage.master";
        }
    }

    protected void ImageButton1_Click(object sender, EventArgs e)
    {
        try
        {
            sendRecordsExchangeEmail(textfield7.Text.Trim(), AJIWANIEMAIL);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Thank you for interest in the Records Exchange...we will get in contact with you very shortly.  If you have a couple of minutes and would like to speak to someone right away...call 1-657-217-3260.  This way we can get the process of enrollment started right away." + "\");", true);
        }
        catch (Exception ex)
        {
            string strWrongInfor = string.Format("Error : {0}", ex.Message);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
        }

    }

    public DataTable States
    {
        get
        {
            object obj = Application["States"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetStates();
                Application["States"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    private void sendRecordsExchangeEmail(string contactName, string emailaddr)
    {
        string strMailTemplet = getStorageMailTemplate();

        strMailTemplet = strMailTemplet.Replace("[Office]", textfield6.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[Contact]", contactName);
        strMailTemplet = strMailTemplet.Replace("[City]", txtCity.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[State]", drpState.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[Tel]", textfield8.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[Fax]", textfield9.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[Email]", textfield13.Text.Trim());

        strMailTemplet = strMailTemplet.Replace("[Place]", drpBoxPlaces.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[Records]", textfieldRecord.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[PracticeYears]", textfieldPracticeYear.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[StoreCharts]", textfieldChartYear.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[Charts]", drpBoxUsePaper.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[Electronic]", drpBoxUseElectronic.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[Practice]", drpBoxsellPractice.Text.Trim());



        string strEmailSubject = string.Format("Records Exchange Form From {0} on EdFiles", contactName);

        Email.SendFromEFileFolder(emailaddr, strEmailSubject, strMailTemplet);

        clearForm();
    }


    protected void clearForm()
    {
        textfield6.Text = "";
        textfield7.Text = "";
        textfield8.Text = "";
        textfield9.Text = "";
        textfield13.Text = "";

        txtCity.Text = "";
        drpState.SelectedIndex = 0;
        drpBoxPlaces.SelectedIndex = 0;

        textfieldRecord.Text = "";
        textfieldPracticeYear.Text = "";
        textfieldChartYear.Text = "";

        drpBoxUsePaper.SelectedIndex = 0;
        drpBoxUseElectronic.SelectedIndex = 0;
        drpBoxsellPractice.SelectedIndex = 0;


    }

    private string getStorageMailTemplate()
    {
        string strMailTemplet = string.Empty;
        string physicalPath = Request.PhysicalPath;
        int index = physicalPath.LastIndexOf("\\");
        string mailTempletUrl = physicalPath.Substring(0, index + 1) + "RecordsExchange.html";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }
}
