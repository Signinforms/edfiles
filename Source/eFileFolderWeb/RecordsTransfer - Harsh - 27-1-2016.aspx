<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RecordsTransfer.aspx.cs" Inherits="RecordsTransfer" Title="Online file folders, EdFiles, Electronic Document Storage Scanning -Records Transfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table>
        <tr>
            <td height="186" valign="top" style="background-repeat: no-repeat;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="970" height="453" valign="top" background="images/bg_main.gif" bgcolor="#f6f5f2"
                                                style="background-repeat: repeat-x;">
                                                <div style="background: url(../images/inner_b.gif); background-position: right top; background-repeat: no-repeat; margin-top: 10px;">
                                                    <div style="width: 725px; height: 120px;">
                                                        <div class="Naslov_Z_Crven_Central">
                                                            <strong>Records Transfer/Request Form<br>
                                                            </strong>
                                                            Fax Request to 866-390-5850<br>
                                                            Customer Service Tel. (855) 334-5336<br>
                                                            Email Requests to:<a style="text-decoration: none" href="mailto:info@edfiles.com">info@edfiles.com</a>
                                                        </div>
                                                    </div>
                                                    <div class="Naslov_Z_Crven_Central">
                                                        Office Name:
                                                        <asp:TextBox runat="server" ID="txtOfficeName" Width="250px"></asp:TextBox>
                                                        <div class="podnaslov">(From where you want records transferred from)</div>
                                                        <asp:RequiredFieldValidator runat="server" ID="officeNameV" ControlToValidate="txtOfficeName"
                                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The office name is required!" />
                                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                                            TargetControlID="officeNameV" HighlightCssClass="validatorCalloutHighlight" />
                                                    </div>

                                                    <div class="Naslov_Z_Crven_Central">
                                                        <strong>REQUEST FOR TRANSFER OF HEALTH INFORMATION</strong>
                                                    </div>

                                                    <div style="width: 725px;">
                                                        <div class="Naslov_BZ_Crven">
                                                        </div>
                                                        <div class="podnaslov" style="padding: 5px; margin-bottom: 0px; background-color: #f6f5f2;">
                                                            <font style="font-style: italic;">As required by the Health Information Portability and Accountability Act of 1996 (HIPAA) and California law, EdFiles may not 
use or disclose your individually identifiable health information except as provided in our Notice of Privacy Practices witho ut your authorization. Your completion of this form means that you are giving permission for the transfer of health information described 
below. Please review and complete this form carefully. It may be invalid if not fully completed.</font>
                                                            <br />
                                                            <p>I hereby request the transfer of health information for: </p>
                                                            <div class="podnaslov1" style="padding: 0px; margin-bottom: 0px; background-color: #f6f5f2;">
                                                                <div>
                                                                    <asp:TextBox runat="server" ID="txtPatientName" Width="90%"></asp:TextBox>
                                                                    <asp:RequiredFieldValidator runat="server" ID="patientNameV" ControlToValidate="txtPatientName"
                                                                        Display="None" ErrorMessage="<b>Required Field Missing</b><br />The patient��s name and address is required!" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                                                        TargetControlID="patientNameV" HighlightCssClass="validatorCalloutHighlight" />
                                                                    <br>
                                                                </div>
                                                                <div>
                                                                    (Print patient��s name and address)
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="podnaslov" style="padding: 0px; margin-bottom: 0px; background-color: #f6f5f2;">
                                                            <strong>RECORDS TO BE TRANSFERRED:</strong>
                                                        </div>
                                                        <div class="podnaslov" style="padding: 5px; margin-bottom: 0px; background-color: #f6f5f2;">
                                                            I would like the following transferred:
                                                             <div class="podnaslov" style="padding: 0px; margin-bottom: 0px; background-color: #f6f5f2;">
                                                                 <asp:RadioButton runat="server" ID="radioAllRecords" Text="All the records" GroupName="RecordTransfer" />
                                                                 <asp:RadioButton runat="server" ID="radioPartialRecords" Text="The portion of the records concerning:" GroupName="RecordTransfer" />
                                                                 <asp:TextBox runat="server" ID="txtPartialRecords" Width="90%"></asp:TextBox><br>
                                                                 (Specify type of disease, accident, dates of treatment, or other portion of records.)
                                                             </div>

                                                        </div>
                                                        <div class="podnaslov" style="padding: 0px; margin-bottom: 0px; background-color: #f6f5f2;">
                                                            <strong>PLEASE TRANSFER THESE RECORDS TO:</strong><asp:TextBox runat="server" ID="txtTheseRecords" Width="90%"></asp:TextBox><br>
                                                            (Name and address of health care provider to whom the records are to be delivered.)
                                                        </div>
                                                        <div class="podnaslov" style="padding: 5px; margin-bottom: 0px; background-color: #f6f5f2;">
                                                            <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 0;">
                                                                <tr>
                                                                    <td>Signed</td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtSigned" Width="80%"></asp:TextBox></td>
                                                                    <td>Date</td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtDate" Width="60%"></asp:TextBox></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>Print Name</td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtPrintName" Width="80%"></asp:TextBox></td>
                                                                    <td>Telephone</td>
                                                                    <td>
                                                                        <asp:TextBox runat="server" ID="txtTel" Width="60%"></asp:TextBox></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="podnaslov" style="padding: 5px; margin-bottom: 0px; background-color: #f6f5f2;">
                                                            If not signed by the patient, please indicate relationship:
                                                            <div class="podnaslov" style="padding: 0px; margin-bottom: 0px; background-color: #f6f5f2;">
                                                                <asp:RadioButton runat="server" ID="rdParent" Text="parent or guardian of minor patient" GroupName="relationship" /><br>
                                                                <asp:RadioButton runat="server" ID="rdConservator" Text="guardian or conservator of an incompetent patient" GroupName="relationship" /><br>
                                                                <asp:RadioButton runat="server" ID="rdBeneficiary" Text="beneficiary or personal representative of deceased patient" GroupName="relationship" />
                                                            </div>
                                                        </div>
                                                        <div class="Naslov_Z_Crven_Central">
                                                            <p>Administrative & Retrieval charges may apply.</p>
                                                        </div>
                                                        <div align="center" style="height: 30px;">
                                                            <asp:Button ID="ImageButton1" runat="server" Text="Submit" Font-Bold="True" OnClick="ImageButton1_Click" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="10" align="center">
                                    <img src="images/lin.gif" width="1" height="453" vspace="10" /></td>
                                <td valign="top" background="images/bg_main.gif" bgcolor="#f6f5f2" style="background-repeat: repeat-x;">
                                    <div class="Naslov_Plav" style="width: 200px;">
                                        New User?
                                    </div>
                                    <table width="200" border="0" cellpadding="2" cellspacing="0" style="margin: 20px 0 20px 10px;">
                                        <tr>
                                            <td align="center">
                                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/WebUser/WebOfficeUser.aspx">
                                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/register.gif" />

                                                </asp:HyperLink></td>
                                        </tr>
                                    </table>
                                    <div class="call" align="center">
                                        Register directly by calling
                                    </div>
                                    <div align="center" class="phone">
                                        800-579-9190
                                    </div>
                                    <div class="div_background" style="width: 100%; height: 3px">
                                    </div>
                                    <div class="Naslov_Z_Crven_Central">
                                        <a href="InstallAIR.aspx" class="Naslov_Y_Crven_1">Download edFile Viewer<br />
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        </table>

                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
