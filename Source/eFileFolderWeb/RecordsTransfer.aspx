<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="RecordsTransfer.aspx.cs" Inherits="RecordsTransfer" Title="" %>
<%@ Register Src="~/Controls/RegisterDirectly.ascx" TagName="ucRegisterDirectly" TagPrefix="ucRegisterDirectly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        label {
            font-family: Open Sans, sans-serif;
            font-size:16px;
            font-weight:normal;
        }

        h1, h2 {
            font-family: Open Sans, sans-serif;
            font-weight:bold;
        }

         .inputBox {
            font-size:initial;color:initial;font-size:14px;
        }

        td {
            font-size:15px;
            font-weight:normal;
            font-family:'Open Sans', sans-serif;
            color:#000;
        }

        .ajax__validatorcallout_error_message_cell {
            line-height:normal;
            vertical-align:top
        }

    </style>
 <div class="title-container">
  <div class="inner-wrapper">
   <h1 style="padding-top:15px">Record Transfer</h1>
   <div class="clearfix"></div>
  </div>
 </div>
 <div class="page-container register-container">
  <div class="inner-wrapper">
   <div class="aboutus-contant record-transfer-page">
    <div class="left-content">
     <h1>Records Transfer/Request Form</h1>
     <p>Fax Request to 866-390-5850</p>
     <p>Customer Service Tel. (657)-217-3260 </p>
     <p>Email Requests to:<a style="text-decoration: none; color:#444" href="mailto:info@edfiles.com">info@edfiles.com</a></p>
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
      <ContentTemplate>
       <div class="form-containt">
        <div class="content-box">
         <fieldset>
         <label>Office Name:</label>
         <asp:TextBox runat="server" ID="txtOfficeName" CssClass="inputBox"></asp:TextBox>
				 <label class="text-caption">(From where you want records transferred from)</label>
         </fieldset>
         <asp:RequiredFieldValidator runat="server" ID="officeNameV" ControlToValidate="txtOfficeName"
                                                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The office name is required!" />
         <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                                            TargetControlID="officeNameV" HighlightCssClass="validatorCalloutHighlight" />
         <h1 style="padding-bottom:0px; line-height:normal">REQUEST FOR TRANSFER OF HEALTH INFORMATION</h1>
         <font style="font-style: italic;font-size:16px; line-height:normal; color:#000;font-family:'Open Sans', sans-serif">As required by the Health Information Portability and Accountability Act of 1996 (HIPAA) and California law, EdFiles may not 
         use or disclose your individually identifiable health information except as provided in our Notice of Privacy Practices witho ut your authorization. Your completion of this form means that you are giving permission for the transfer of health information described 
         below. Please review and complete this form carefully. It may be invalid if not fully completed.</font>
         <p style="line-height:normal;padding-bottom:0px;">I hereby request the transfer of health information for: </p>
         <fieldset>
         <asp:TextBox runat="server" ID="txtPatientName" Width="90%" CssClass="inputBox" Style="margin-right:5px;"></asp:TextBox>
         <label class="text-caption" > (Print patient��s name and address) </label>
         </fieldset>
         <asp:RequiredFieldValidator runat="server" ID="patientNameV" ControlToValidate="txtPatientName" Display="None" ErrorMessage="<b>Required Field Missing</b><br />The patient��s name and address is required!" />
         <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                                                        TargetControlID="patientNameV" HighlightCssClass="validatorCalloutHighlight" />
         <h2 style="color:#000; padding-bottom:0px">RECORDS TO BE TRANSFERRED:</h2>
         <p style="margin-top:20px">I would like the following transferred:</p>
         <fieldset>
         <asp:RadioButton runat="server" ID="radioAllRecords" Text="All the records" GroupName="RecordTransfer" />
         <asp:RadioButton runat="server" ID="radioPartialRecords" Text="The portion of the records concerning:" GroupName="RecordTransfer" />
         <asp:TextBox runat="server" ID="txtPartialRecords" Width="90%" CssClass="inputBox"></asp:TextBox>
         <label class="text-caption">(Specify type of disease, accident, dates of treatment, or other portion of records.)</label>
         </fieldset>
         <h2>PLEASE TRANSFER THESE RECORDS TO:</h2>
         <fieldset>
         <asp:TextBox runat="server" ID="txtTheseRecords" CssClass="inputBox"></asp:TextBox>
         <label class="text-caption">(Name and address of health care provider to whom the records are to be delivered.)</label>
         </fieldset>
         <div>
          <table width="100%" border="0" cellpadding="2" cellspacing="0" style="margin: 0;height:52px">
           <tr>
            <td style="width:115px;">Signed</td>
            <td><asp:TextBox runat="server" ID="txtSigned" Width="80%" CssClass="inputBox"></asp:TextBox></td>
            <td>Date</td>
            <td><asp:TextBox runat="server" ID="txtDate" Width="60%" CssClass="inputBox"></asp:TextBox></td>
           </tr>
           <tr>
            <td>Print Name</td>
            <td><asp:TextBox runat="server" ID="txtPrintName" Width="80%" CssClass="inputBox"></asp:TextBox></td>
            <td>Telephone</td>
            <td><asp:TextBox runat="server" ID="txtTel" Width="60%" CssClass="inputBox"></asp:TextBox></td>
           </tr>
          </table>
          <p style="margin-top:20px;"> If not signed by the patient, please indicate relationship:</p>
          <div style="height:66px;">
           <asp:RadioButton runat="server" ID="rdParent" Text="parent or guardian of minor patient" GroupName="relationship" style="margin-top: 5px;margin-right: 3px;margin-left: 5px;"/>
           <br>
           <asp:RadioButton runat="server" ID="rdConservator" Text="guardian or conservator of an incompetent patient" GroupName="relationship" style="margin-top: 5px;margin-right: 3px;margin-left: 5px;"/>
           <br>
           <asp:RadioButton runat="server" ID="rdBeneficiary" Text="beneficiary or personal representative of deceased patient" GroupName="relationship" style="margin-top: 5px;margin-right: 3px;margin-left: 5px;"/>
          </div>
         </div>
         <p>Administrative & Retrieval charges may apply.</p>
         <div align="center">
          <asp:Button ID="ImageButton1" runat="server" Text="Submit" Font-Bold="True" OnClick="ImageButton1_Click" class="create-btn btn green" />
         </div>
        </div>
       </div>
      </ContentTemplate>
      <Triggers>
       <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
      </Triggers>
     </asp:UpdatePanel>
    </div>
    <div class="right-content">
     <ucRegisterDirectly:ucRegisterDirectly ID="ucRegister" runat="server" />
    </div>
   </div>
  </div>
 </div>
</asp:Content>
