using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Utility;

public partial class RecordsTransfer : System.Web.UI.Page
{
    private static readonly string INFOEMAIL = ConfigurationManager.AppSettings["INFOEMAIL"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            clearForm();
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!this.Page.User.Identity.IsAuthenticated)
        {
            this.MasterPageFile = "~/LoginMasterPage.master";
        }
    }

    protected void ImageButton1_Click(object sender, EventArgs e)
    {
        try
        {
            sendRecordsTransferEmail(txtOfficeName.Text.Trim(), INFOEMAIL);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + "Thank you for interest in the Records Transfer...we will get in contact with you very shortly.  If you have a couple of minutes and would like to speak to someone right away...call 1-657-217-3260.  This way we can get the process of enrollment started right away." + "\");", true);
        }
        catch (Exception ex)
        {
            string strWrongInfor = string.Format("Error : {0}", ex.Message);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
        }

    }


    private void sendRecordsTransferEmail(string contactName, string emailaddr)
    {
        string strMailTemplet = getRecordsTransferMailTemplate();

        strMailTemplet = strMailTemplet.Replace("[OfficeName]", txtOfficeName.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[PatientName]", txtPatientName.Text.Trim());

        if (radioAllRecords.Checked)
        {
            strMailTemplet = strMailTemplet.Replace("[radioAllRecords]", "checked");
            strMailTemplet = strMailTemplet.Replace("[radioPartialRecords]", "");
        }
        else
        {
            strMailTemplet = strMailTemplet.Replace("[radioAllRecords]", "");
            strMailTemplet = strMailTemplet.Replace("[radioPartialRecords]", "checked");
        }

        strMailTemplet = strMailTemplet.Replace("[PartialRecords]", txtPartialRecords.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[TheseRecords]", txtTheseRecords.Text.Trim());

        strMailTemplet = strMailTemplet.Replace("[Signed]", txtSigned.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[Date]", txtDate.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[PrintName]", txtPrintName.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[Tel]", txtTel.Text.Trim());

        if (rdParent.Checked)
        {
            strMailTemplet = strMailTemplet.Replace("[rdParent]", "checked");
            strMailTemplet = strMailTemplet.Replace("[rdConservator]", "");
            strMailTemplet = strMailTemplet.Replace("[rdBeneficiary]", "");
        }
        else if (rdConservator.Checked)
        {
            strMailTemplet = strMailTemplet.Replace("[rdParent]", "");
            strMailTemplet = strMailTemplet.Replace("[rdConservator]", "checked");
            strMailTemplet = strMailTemplet.Replace("[rdBeneficiary]", "");
        }
        else
        {
            strMailTemplet = strMailTemplet.Replace("[rdParent]", "");
            strMailTemplet = strMailTemplet.Replace("[rdConservator]", "");
            strMailTemplet = strMailTemplet.Replace("[rdBeneficiary]", "checked");
        }


        string strEmailSubject = string.Format("Records Transfer Form From {0} on EdFiles", contactName);

        Email.SendFromEFileFolder(emailaddr, strEmailSubject, strMailTemplet);

        clearForm();
    }


    protected void clearForm()
    {
        txtOfficeName.Text = "";
        txtPatientName.Text = "";
        radioAllRecords.Checked = true;
        txtPartialRecords.Text = "";
        txtTheseRecords.Text = "";
        txtSigned.Text = "";

        txtSigned.Text = "";
        txtDate.Text = "";
        txtPrintName.Text = "";
        txtTel.Text = "";

        rdParent.Checked = true;
    }

    private string getRecordsTransferMailTemplate()
    {
        string strMailTemplet = string.Empty;
        string physicalPath = Request.PhysicalPath;
        int index = physicalPath.LastIndexOf("\\");
        string mailTempletUrl = physicalPath.Substring(0, index + 1) + "RecordsTransfer.html";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }
}
