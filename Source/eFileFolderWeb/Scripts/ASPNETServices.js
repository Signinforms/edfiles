﻿/*Authentication Functions*/
// alert("ASPNETService.js - has been loaded.");

//Authenticate the user by passing the credentials to the 
//AJAX AuthenticationService.

function doLogin(UserName, Password)
{
// alert("TRACE - logging in");
	Sys.Services.AuthenticationService.login(
	    UserName,
	    Password,
	    false,
	    null,
	    null,
	    OnLoginComplete,
	    OnAuthenticationFailed,
	    "UserContext.");
}

//Actions after the user is authenticated.
function OnLoginComplete(validCredentials, userContext, methodName)
{
	if(validCredentials == true)
	{
        // alert("TRACE - Logoin Success");
	}
	else
	{
		// alert("TRACE - Could not login");
	}
}

//Actions if authentication fails.
function OnAuthenticationFailed(error_object, userContext, methodName)
{	
    alert("Authentication failed with this error: " +
	    error_object.get_message());
}

//Actions when the user logs out.
function OnClickLogout()
{
  // alert("TRACE - logging out");
  
	Sys.Services.AuthenticationService.logout(
	 null, 
	 OnLogoutComplete, 
	 OnAuthenticationFailed,
	 null
	 );
}

//Actions after the logout is complete.
function OnLogoutComplete(result, userContext, methodName)
{
    // alert("TRACE - logged out");
}		




//REQUIRED: Notifies the ScriptManager that the script has been loaded.    
if (typeof(Sys) !== "undefined") Sys.Application.notifyScriptLoaded();