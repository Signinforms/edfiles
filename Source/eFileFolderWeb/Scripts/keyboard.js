
var VKI_attach, VKI_close;
(function () {
    var self = this;

    this.VKI_version = "1.45";
    this.VKI_showVersion = false;
    this.VKI_target = false;
    this.VKI_shift = this.VKI_shiftlock = false;
    this.VKI_altgr = this.VKI_altgrlock = false;
    this.VKI_dead = false;
    this.VKI_deadBox = false; // Show the dead keys checkbox
    this.VKI_deadkeysOn = false;  // Turn dead keys on by default
    this.VKI_numberPad = false;  // Allow user to open and close the number pad
    this.VKI_numberPadOn = false;  // Show number pad by default
    this.VKI_kts = this.VKI_kt = "US Standard"; //"US International";  // Default keyboard layout
    this.VKI_langAdapt = false;  // Use lang attribute of input to select keyboard
    this.VKI_size = 4;  // Default keyboard size (1-5)
    this.VKI_sizeAdj = true;  // Allow user to adjust keyboard size
    this.VKI_clearPasswords = false;  // Clear password fields on focus
    this.VKI_imageURI = "images/keyboard.png";  // If empty string, use imageless mode
    this.VKI_clickless = 0;  // 0 = disabled, > 0 = delay in ms
    this.VKI_activeTab = 0;  // Tab moves to next: 1 = element, 2 = keyboard enabled element
    this.VKI_keyCenter = 3;

    this.VKI_isIE = /*@cc_on!@*/false;
    this.VKI_isIE6 = /*@if(@_jscript_version == 5.6)!@end@*/false;
    this.VKI_isIElt8 = /*@if(@_jscript_version < 5.8)!@end@*/false;
    this.VKI_isWebKit = RegExp("KHTML").test(navigator.userAgent);
    this.VKI_isOpera = RegExp("Opera").test(navigator.userAgent);
    this.VKI_isMoz = (!this.VKI_isWebKit && navigator.product == "Gecko");

    /* ***** i18n text strings ************************************* */
    this.VKI_i18n = {
        '00': "Display Number Pad",
        '01': "Display virtual keyboard",
        '02': "Select keyboard layout",
        '03': "Dead keys",
        '04': "On",
        '05': "Off",
        '06': "Close the keyboard",
        '07': "Clear",
        '08': "Clear this input",
        '09': "Version",
        '10': "Decrease keyboard size",
        '11': "Increase keyboard size"
    };
    /* ****************************************************************
    * Attach the keyboard to an element
    *
    */
    VKI_attach = function (elem) {
        if (elem.getAttribute("VKI_attached")) return false;
        if (self.VKI_imageURI) {
           
            var keybut = document.createElement('img');
            keybut.src = self.VKI_imageURI;
            keybut.alt = self.VKI_i18n['01'];
            keybut.className = "keyboardInputInitiator";
            keybut.title = self.VKI_i18n['01'];
            keybut.elem = elem;
            keybut.onclick = function (e) {
                e = e || event;
                if (e.stopPropagation) { e.stopPropagation(); } else e.cancelBubble = true;
                self.VKI_show(this.elem);
            };
            elem.parentNode.insertBefore(keybut, (elem.dir == "rtl") ? elem : elem.nextSibling);
        } else elem.onfocus = function () { if (self.VKI_target != this) self.VKI_show(this); };
        elem.setAttribute("VKI_attached", 'true');
        if (self.VKI_isIE) {
            elem.onclick = elem.onselect = elem.onkeyup = function (e) {
                if ((e || event).type != "keyup" || !this.readOnly)
                    this.range = document.selection.createRange();
            };
        }
        VKI_addListener(elem, 'click', function (e) {
            if (self.VKI_target == this) {
                e = e || event;
                if (e.stopPropagation) { e.stopPropagation(); } else e.cancelBubble = true;
            } return false;
        }, false);
        //        if (self.VKI_isMoz)
        //            elem.addEventListener('blur', function () { this.setAttribute('_scrollTop', this.scrollTop); }, false);
    };

    /* ****************************************************************
    * Close the keyboard interface
    *
    */
    this.VKI_close = VKI_close = function () {
        if (this.VKI_target) {
            try {
                this.VKI_keyboard.parentNode.removeChild(this.VKI_keyboard);
                if (this.VKI_isIE6) this.VKI_iframe.parentNode.removeChild(this.VKI_iframe);
            } catch (e) { }
            if (this.VKI_kt != this.VKI_kts) {
                kbSelect.firstChild.nodeValue = this.VKI_kt = this.VKI_kts;
                this.VKI_buildKeys();
            } kbSelect.getElementsByTagName('ol')[0].style.display = ""; ;
            this.VKI_target.focus();
            if (this.VKI_isIE) {
                setTimeout(function () { self.VKI_target = false; }, 0);
            } else this.VKI_target = false;
        }
    };


    /* ****************************************************************
    * Insert text at the cursor
    *
    */
    this.VKI_insert = function (text) {
        this.VKI_target.focus();
        if (this.VKI_target.maxLength) this.VKI_target.maxlength = this.VKI_target.maxLength;
        if (typeof this.VKI_target.maxlength == "undefined" ||
        this.VKI_target.maxlength < 0 ||
        this.VKI_target.value.length < this.VKI_target.maxlength) {
            if (this.VKI_target.setSelectionRange && !this.VKI_target.readOnly && !this.VKI_isIE) {
                var rng = [this.VKI_target.selectionStart, this.VKI_target.selectionEnd];
                this.VKI_target.value = this.VKI_target.value.substr(0, rng[0]) + text + this.VKI_target.value.substr(rng[1]);
                if (text == "\n" && this.VKI_isOpera) rng[0]++;
                this.VKI_target.setSelectionRange(rng[0] + text.length, rng[0] + text.length);
            } else if (this.VKI_target.createTextRange && !this.VKI_target.readOnly) {
                try {
                    this.VKI_target.range.select();
                } catch (e) { this.VKI_target.range = document.selection.createRange(); }
                this.VKI_target.range.text = text;
                this.VKI_target.range.collapse(true);
                this.VKI_target.range.select();
            } else this.VKI_target.value += text;

            this.VKI_target.focus();
        } else if (this.VKI_target.createTextRange && this.VKI_target.range)
            this.VKI_target.range.select();
    };


    /* ****************************************************************
    * Show the keyboard interface
    *
    */
    this.VKI_show = function (elem) {
        if (this.VKI_target) {
            this.VKI_target.blur();
        }

        this.VKI_target = elem;
        if (this.VKI_isIE) {
            if (!this.VKI_target.range) {
                this.VKI_target.range = this.VKI_target.createTextRange();
                this.VKI_target.range.moveStart('character', this.VKI_target.value.length);
            } this.VKI_target.range.select();
        }

        if (this.VKI_clearPasswords && this.VKI_target.type == "password") this.VKI_target.value = "";

        this.VKI_target.keyboardPosition = "absolute";
        this.VKI_target.focus();

    };


    function VKI_addListener(elem, type, func, cap) {
        if (elem.addEventListener) {
            elem.addEventListener(type, function (e) { func.call(elem, e); }, cap);
        } else if (elem.attachEvent)
            elem.attachEvent('on' + type, function () { func.call(elem); });
    }


    function VKI_findPos(obj) {
        var curleft = curtop = 0, scr = obj;
        while ((scr = scr.parentNode) && scr != document.body) {
            curleft -= scr.scrollLeft || 0;
            curtop -= scr.scrollTop || 0;
        }
        do {
            curleft += obj.offsetLeft;
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
        return [curleft, curtop];
    }

    /* ***** Find tagged input & textarea elements ***************** */
    function VKI_buildKeyboardInputs() {
        var inputElems = [
      document.getElementsByTagName('input'),
      document.getElementsByTagName('textarea')
    ];
        for (var x = 0, elem; elem = inputElems[x++]; )
            for (var y = 0, ex; ex = elem[y++]; )
                if (ex.nodeName == "TEXTAREA" || ex.type == "text" || ex.type == "password")
                    if (ex.className.indexOf("keyboardInput") > -1) VKI_attach(ex);

        VKI_addListener(document.documentElement, 'click', function (e) {
            self.VKI_close();
        }, false);
    }

    VKI_addListener(window, 'load', VKI_buildKeyboardInputs, false);

})();