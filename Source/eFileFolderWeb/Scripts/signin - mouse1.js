﻿
var isStarted = true;
var totals = [];
var gt;
var path;
//window.addEventListener('load', function () {
$(document).ready(function () {
    // lift js into a functional language...
    Array.map = function (ar, f) {
        var results = [];
        for (var i = 0; i < ar.length; i++)
            results.push(f(ar[i], i));
        return results;
    };

    Array.each = Array.map;

    Array.partition = function (ar, n) {
        var partitions = [];
        var partition;
        Array.map(ar, function (item, i) {
            if (i % n == 0) partitions.push(partition = []);
            partition.push(item);
        });
        return partitions;
    };

    var firstClick = true;
    // get the canvas element and its context
    var canvas = document.getElementById('sketchpad');

    if (!canvas.getContext) {
        return;
    }

    canvas.setAttribute('width', '400px'); // clears the canvas
    canvas.setAttribute('height', '100px'); // clears the canvas

    var context = canvas.getContext("2d");
    context.lineCap = 'round';
    context.lineJoin = 'round';
    context.lineWidth = 5;
    context.fillStyle = "rgb(255,255,255)";

    context.clearRect(0, 0, canvas.width, canvas.height);

    drawWaterPrint();
    init(canvas);

    // prevent elastic scrolling
    document.documentElement.addEventListener('scroll', function (event) {
        event.preventDefault();

    }, false); // end body.onscroll

});            // end window.load

function drawWaterPrint() {
    var canvas = document.getElementById('sketchpad');
    var context = canvas.getContext("2d");
    context.font = "italic 18pt Calibri";
    context.lineWidth = 1;
    var text = "please sign here";
    context.strokeStyle = "blue"; // stroke color
    var metrix = context.measureText(text);
    context.strokeText(text, (canvas.width - metrix.width) / 2, (canvas.height) / 2);
    isStarted = true;
}

function drawPaths(ctx, data) {
    //    var gPoints = Array.map(
    //				data,
    //				function (pts, i) {
    //				    return Array.map(Array.partition(pts, 2),
    //							 function (item) {
    //							     return { x: item[0], y: item[1] }
    //							 })
    //				});

    ; var gPoints = data;
    // and make some Beziers:
    var gBeziers = Array.map(gPoints, function (pts) { return new Bezier(pts) });
    ctx.beginPath();
    Array.each(gBeziers, function (bez) { bez.draw(ctx) });
    ctx.stroke();
    //    var px1 = totals[0][totals[0].length - 2];
    //    var py1 = totals[0][totals[0].length - 1]
    //    totals[0] = [];
    //    totals[0].push(px1);
    //    totals[0].push(py1);
}

function init(canvas) {
    if (!canvas) {
        alert("null canvas");
    }
    var context = canvas.getContext('2d');
    //paper.setup(canvas);
    
    context.lineCap = 'round';
    context.lineJoin = 'round';
    context.lineWidth = 5;

    var startx = getLeft(canvas);
    var starty = getTop(canvas);

    // create a drawer which tracks touch movements
    var drawer = {
        isDrawing: false,
        mousedown: function (coors) {
            if (isStarted) {
                isStarted = false;
                emptyCanvas();
            }

            totals[0] = [];
            startx = getLeft(canvas);
            starty = getTop(canvas);
            context.beginPath();
            context.moveTo(coors.x - startx, coors.y - starty);

            totals[0].push(coors.x - startx);
            totals[0].push(coors.y - starty);

//            path = new Path();

//            path.strokeColor = 'black';
//            path.add(new Point(coors.x - startx, coors.y - starty));

            this.isDrawing = true;

            this.gt = setTimeout(function () {
                this.isDrawing = false;
            }, 350);

        },
        mousemove: function (coors) {
            if (this.isDrawing) {
                clearTimeout(this.gt);
                context.lineTo(coors.x - startx, coors.y - starty);
                context.stroke();
                totals[0].push(coors.x - startx);
                totals[0].push(coors.y - starty);
               
//                path.add(new Point(coors.x - startx, coors.y - starty));
                //                this.gt = setTimeout(function () {
                //                    this.isDrawing = false;

                ////                    drawPaths(context, totals[0]);
                //                }, 350);

                //                if (totals[0].length >= 8)
                //                    drawPaths(context, totals);
            }
        },
        mouseup: function (coors) {
            if (this.isDrawing) {
                this.mousemove(coors);
                this.isDrawing = false;

//                path.simplify(10);

//                paper.view.draw();
            }

        }
    };
    // create a function to pass touch events and coordinates to drawer
    function draw(event) {
        event.preventDefault();
        // get the touch coordinates
        //eventLog(event);
        var coors = {
            //                        x: event.targetTouches[0].pageX,
            //                        y: event.targetTouches[0].pageY
            x: event.pageX,
            y: event.pageY
        };
        // pass the coordinates to the appropriate handler
        drawer[event.type](coors);

    }

    //attach the touchstart, touchmove, touchend event listeners.
    //        canvas.addEventListener('touchstart', draw, false);
    //        canvas.addEventListener('touchmove', draw, false);
    //        canvas.addEventListener('touchend', draw, false);

    canvas.addEventListener('mousedown', draw, false);
    canvas.addEventListener('mousemove', draw, false);
    canvas.addEventListener('mouseup', draw, false);
}

function eventLog(e) {
    var x = event.targetTouches[0].pageX;
    var y = event.targetTouches[0].pageY;
}

function getTop(e) {
    var offset = e.offsetTop;
    if (e.offsetParent != null)
        offset += getTop(e.offsetParent);
    return offset;
}

function getLeft(e) {
    var offset = e.offsetLeft;
    if (e.offsetParent != null)
        offset += getLeft(e.offsetParent);
    return offset;
}

function doSave() {
    if (!validateFields()) {

        return;
    }
    var label = $("#label_datetime")[0];
    var date = new Date();
    var testStr = date.format("MM/dd/yyyy - hh:mm a");
    label.innerHTML = "Mon. " + testStr;

    submitJson();

}

Date.prototype.format = function (format) {
    /*
    * eg:format="yyyy-MM-dd hh:mm:ss";
            
    */
    var o = {
        "M+": this.getMonth() + 1,  //month
        "d+": this.getDate(),     //day
        "h+": getShortHours(this.getHours()),    //hour
        "m+": this.getMinutes(),  //minute
        "s+": this.getSeconds(), //second
        "q+": Math.floor((this.getMonth() + 3) / 3),  //quarter
        "S": this.getMilliseconds(), //millisecond
        "a": getAPM(this.getHours()) //AM/PM
    }

    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    }

    for (var k in o) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
        }
    }
    return format;
}

function getShortHours(hour) {
    var ap = "am";
    if (hour > 11) { ap = "pm"; }
    if (hour > 12) { hour = hour - 12; }
    if (hour == 0) { hour = 12; }
    if (hour < 10) { hour = "0" + hour; }
    return hour;
} // function getShortHours()

function getAPM(hour) {
    var ap = "am";
    if (hour > 11) { ap = "pm"; }
    return ap;
} // function getShortHours()

function submitJson() {
    var imgdata = getBase64Image();

    var content = "";
    $(".td_right input").each(function () {
        content = content + this.value + "|";
    });

    content = content.substring(0, content.length - 1);
    content = content.replace("|", ", ");

    var idcontent = "";
    $(".td_left input").each(function () {
        idcontent = idcontent + this.value + "|";
    });

    var doctorid = "";
    $(".title_sheet input").each(function () {
        doctorid = this.value;
    });

    $.showprogress('Saving', 'Please Wait...', '<img src="images/loadingimage.gif"/>');

    $.ajax({
        type: "POST",
        url: '../FlexWebService.asmx/TestMultiJson',
        data: "{fields:'" + content + "',ids:'" + idcontent + "',doctorid:'" + doctorid + "',data:'" + imgdata + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            emptyFields();
            $.hideprogress();
            alert(
                "Sign In Successfull, Welcome." + '\n' +
                "Please be seated...someone will be with you shortly." + '\n'
              );
        },
        error: function (msg) {
            $.hideprogress();
        }
    });


};

function validateFields() {
    var pname = $(".td_right input")[0].value;
    var fname = $(".td_right input")[1].value;

    if (!pname || pname.length == 0) {
        alert("Last Name: this field are required");
        return false;
    }

    if (!fname || fname.length == 0) {
        alert("First Name: this field are required");
        return false;
    }

    //    if (!(/^\w+, \w+$/.test(pname))) {
    //        alert("Print Name:{" + pname + "} the format is't valid");
    //        return false;
    //    }


    return true;
}

function emptyFields() {
    $(".td_right input").each(function () {
        this.value = "";
    });

    $("#label_datetime").each(function () {
        this.innerHTML = "";
    });

    totals = [];
    emptyCanvas();
    drawWaterPrint();
}

function emptyCanvas() {
    var canvas = document.getElementById('sketchpad');
    var context = canvas.getContext('2d');
    context.lineCap = 'round';
    context.lineJoin = 'round';
    context.lineWidth = 5;

    context.fillStyle = "rgb(255,255,255)";
    context.clearRect(0, 0, canvas.width, canvas.height);
}

function getBase64Image() {
    var canvas = document.getElementById('sketchpad');
    var imageData = Canvas2Image.saveAsBMP(canvas, true, canvas.width * 0.5, canvas.height * 0.5); //canvas.toDataURL("image/png");
    //alert(imageData.length);
    var enstr = lzw_encode(imageData);
    // alert(enstr.length);
    var result = base64Encode(enstr);
    //alert(result.length);
    return result;
}


