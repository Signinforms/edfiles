<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SearchFileFolders.aspx.cs" Inherits="Office_SearchFileFolders" Title="" %>

<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Search EdFiles</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="left-content">
                <div><a href="SearchScannedDocuments.aspx">Search Scanned Documents</a></div>
                <div>&nbsp;</div>
                <div>
                    <p class="no-margin">Please use the box below to quickly look up EdFiles in your account.</p>
                </div>
                <div class="form-containt listing-contant">
                    <div class="content-box listing-view">
                        <fieldset>
                            <label>Folder&nbsp;Name</label>
                            <asp:TextBox ID="txtFolderName" runat="server" size="30" MaxLength="20" />
                        </fieldset>
                        <fieldset>
                            <label>First&nbsp;Name</label>
                            <asp:TextBox ID="textfield2" runat="server" name="textfield2"
                                size="30" MaxLength="20" />
                        </fieldset>
                        <fieldset>
                            <label>Last&nbsp;Name</label>
                            <asp:TextBox name="textfield4" runat="server" ID="textfield5"
                                size="30" MaxLength="20" />
                        </fieldset>
                        <fieldset>
                            <label>File&nbsp;Number</label>
                            <asp:TextBox name="textfieldFN" runat="server" ID="textfieldFN"
                                size="30" MaxLength="20" />
                        </fieldset>
                        <%--<fieldset>
                            <label>Check/Wire No.</label>
                            <asp:TextBox name="textfieldCheckNo" runat="server" ID="textfieldCheckNo"
                                size="30" MaxLength="20" />
                        </fieldset>--%>
                        <%--<fieldset>
                            <label>Amount</label>
                            <asp:TextBox name="textfieldAmount" runat="server" ID="textfieldAmount"
                                size="30" MaxLength="20" />
                        </fieldset>--%>
                        <fieldset style="display: none">
                            <label>DOB</label>
                            <asp:TextBox name="textfield7" runat="server" ID="textfield7"
                                size="15" MaxLength="10" />
                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" PopupPosition="Right" runat="server"
                                TargetControlID="textfield7" Format="MM-dd-yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <asp:RegularExpressionValidator ID="DobC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The DOB is not a date"
                                ControlToValidate="textfield7" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                                Visible="true" Display="None"></asp:RegularExpressionValidator>
                            <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6"
                                TargetControlID="DobC" HighlightCssClass="validatorCalloutHighlight" />
                        </fieldset>

                        <div class="form-submit-btn">
                            <asp:Button ID="Submit2" name="Submit2" Text="Search" runat="server" align="absbottom" OnClick="Submit2_Click" CssClass="create-btn btn green" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="right-content">
                <div class="quick-find" style="margin-top: 15px; background-color: #efefef;">
                    <div class="find-inputbox">
                        <marquee scrollamount="1" id="marqueeside" runat="server" behavior="scroll" onmouseover="this.stop()" onmouseout="this.start()" scrolldelay="20"
                            direction="up">
                               <%=SideMessage %>
                            </marquee>
                    </div>
                </div>
            </div>
        </div>


        <div class="form-submit-btn">
        </div>
    </div>
</asp:Content>
