using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Office_SearchFileFolders : LicensePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public string SideMessage
    {
        get
        {
            return this.Session["_Side_Message"] as string;
        }

        set
        {
            this.Session["_Side_Message"] = value;
        }
    }


    public void Submit2_Click(object sender,EventArgs e)
    {
        string strFolderName = this.txtFolderName.Text.Trim();
        string firstname = this.textfield2.Text.Trim();
        string lastname = this.textfield5.Text.Trim();
        string date = this.textfieldFN.Text.Trim();

        //string amount = this.textfieldAmount.Text.Trim();
        //string wireNo = this.textfieldCheckNo.Text.Trim();

        try
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("?fon=" + strFolderName);
            builder.Append("&fn=" + firstname);
            builder.Append("&ln=" + lastname);
            builder.Append("&dt=" + date);
            //builder.Append("&am=" + amount);
            //builder.Append("&no=" + wireNo);

            Page.Response.Redirect("~/Search/SearchFolderResult.aspx" + builder.ToString());
        }
        catch
        {   
        }
       
    }

    
}
