<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SearchFolderResult.aspx.cs" Inherits="SearchFolderResult" Title="" %>

<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%--<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>--%>
<%@ Register Src="~/Office/UserControls/ucAddToTab.ascx" TagPrefix="uc1" TagName="ucAddToTab" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="../css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/style.css" rel="Stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/colorbox.css" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>css/main.css?v=2" rel="stylesheet" />
    <link rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>lib/TreeView/style.min.css" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>lib/TreeView/jquery-1.8.3.min.js" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/js/jquery.colorbox-min.js"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>extras/jquery-ui-1.8.22.custom.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>lib/TreeView/jstree.min.js"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/js/edFiles/EdFilesLogs.js?v=4" type="text/javascript"></script>
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/chosen.css" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/js/chosen.jquery.js" type="text/javascript"></script>
    <style type="text/css">
        .cp_button {
            background-image: url(../../images/cp_button.png);
            width: 16px !important;
            height: 16px;
            border: none !important;
            background-repeat: no-repeat;
            /* margin-top: 14px; */
            margin: 10px;
        }

        #comboboxDividerSplit_chosen {
            margin-top: 5px;
            display: block;
        }

        .fileNameField {
            font-size: 15px;
            height: 30px;
            width: 250px;
            line-height: 5px;
            margin-left: 15px;
            margin-top: 5px;
            padding-left: 5px;
        }

        .btnEdit {
            text-align: center !important;
            height: 30px;
            width: 94px;
            background-color: #94ba33;
            color: white;
            font-weight: bold;
            font-size: 21px;
            margin-top: 5px;
            border: none;
            cursor: pointer;
        }

            .btnEdit:hover {
                background-color: #01487f;
            }

        .btnNextPreview {
            padding: 8px 10px 8px 35px;
            font-size: 15px;
            padding-right: 10px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 5px;
            background-image: url(../images/left-arrow.png);
            background-position: 7px center;
            background-repeat: no-repeat;
            float: left;
        }

        .btnPrevPreview {
            padding: 8px 30px 8px 10px;
            font-size: 15px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 5px;
            background-image: url(../images/right-arrow.png);
            background-position: 50px center;
            background-repeat: no-repeat;
            float: right;
        }

        .ui-dialog {
            margin-top: 5% !important;
            position: fixed;
        }

        #comboboxSplit_chosen {
            display: block;
            margin-top: 5px;
        }

        .downloadFile {
            margin-right: 10px;
            cursor: pointer;
        }

        .ic-save, .ic-share {
            cursor: pointer;
            border: none;
            background-color: transparent;
            margin-left: 0px;
            float: none;
            margin-right: 10px;
        }

        #btnCheckedAll {
            float: right;
            margin-right: 3px;
            margin-top: 9px;
            height: 18px;
            width: 18px;
        }

        .selectalltab {
            font-size: 18px;
            float: right;
            padding: 5px;
            font-family: 'Open Sans', sans-serif !important;
            color: #fff;
            font-weight: 700;
            margin-right: 5px;
        }

        .btnAddtoTab, .btnAddtoWorkArea {
            padding: 8px 10px 8px 35px;
            font-size: 15px;
            padding-right: 10px;
            width: auto;
            height: auto;
            line-height: normal;
            margin-right: 5px;
            background-image: url(../images/plus-icon.png);
            background-position: 7px center;
            background-repeat: no-repeat;
        }

        #btnCheckedAll {
            float: right;
            margin-right: 3px;
            margin-top: 9px;
            height: 18px;
            width: 18px;
        }

        .selectalltab {
            font-size: 18px;
            float: right;
            padding: 5px;
            font-family: 'Open Sans', sans-serif !important;
            color: #fff;
            font-weight: 700;
            margin-right: 5px;
        }



        .white_content {
            display: none;
            position: fixed;
            top: 20%;
            left: 20%;
            width: 60%;
            height: 50%;
            padding: 16px;
            background-color: white;
            z-index: 999999999;
            overflow: auto;
        }

        .datatable td {
            font-family: "Open Sans", sans-serif;
        }

        .ajax__tab_body {
            height: auto !important;
        }

        .logFormPreveiw {
            background: url(../images/file-view.png) no-repeat center center;
            cursor: pointer;
        }

        .checkForGroup {
            float: left;
            height: 15px;
            width: 15px;
        }

        .addtotab-workarea {
            cursor: pointer;
            background-image: url(../images/AddToTabWorkArea.png);
            margin-left: 0px;
            float: none;
            background-color: transparent;
            border: 0px;
            margin-top: -4px;
        }

        .preview {
            position: relative;
            left: -10px;
        }

        #popupclose,
        .popupclose,
        #popupclose_thumbnail {
            float: right;
            padding: 10px;
            cursor: pointer;
        }

        .popupcontent {
            padding: 10px;
        }

        .preview-popup {
            width: 100%;
            max-width: 1100px;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 10%;
            right: 0;
            margin: auto;
            background: #fff;
            height: 85%;
        }

        .preview-popup-thumbnails {
            width: 100%;
            max-width: 1200px;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 10%;
            right: 0;
            margin: auto;
            background: #fff;
            height: 85%;
        }

        #overlay {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
            position: fixed;
        }

        #floatingCirclesG {
            top: 50%;
            left: 50%;
            position: fixed;
        }
    </style>

    <script type="text/javascript">
        var min = 0;
        var max = 0; var i = 0;

        function makeEditable() {
            $("#<%= lblFileName.ClientID%>").hide();
            $("#<%= btnArchiveUpdate.ClientID%>").hide();
            $("#<%= fileNameFld.ClientID%>").show();
            $("#<%= btnArchiveSave.ClientID%>").show();
            $("#<%= btnArchiveCancel.ClientID%>").show();
            $("#<%= fileNameFld.ClientID%>").val($("#<%= hdnFileNameOnly.ClientID%>").val());
            return false;
        }

        function makeReadOnly() {
            $("#<%= lblFileName.ClientID%>").show();
            $("#<%= btnArchiveUpdate.ClientID%>").show();
            $("#<%= fileNameFld.ClientID%>").hide();
            $("#<%= btnArchiveSave.ClientID%>").hide();
            $("#<%= btnArchiveCancel.ClientID%>").hide();
            return false;
        }

        function ShowPrevious() {
            makeReadOnly();
            $("#overlay").css("z-index", "100002");
            var ci = $('.listing-datatable-archive').find('tr.activetr').attr('data-index');
            ci = parseInt(ci);
            var ni = 0;
            if (ci < max & ci > min + 1) {
                ni = ci - 1;
            }
            else {
                ni = max - 1;
            }

            $('.listing-datatable-archive').find('tr').each(function () {
                if ($(this).attr('data-index') == ni) {
                    $(this).find('a.ic-preview').click();
                }
            })
        }

        function ShowNext() {
            makeReadOnly();
            $("#overlay").css("z-index", "100002");
            var ci = $('.listing-datatable-archive').find('tr.activetr').attr('data-index');
            ci = parseInt(ci);
            var ni = 0;
            if (ci < max - 1 & ci > min) {
                ni = ci + 1;
            }
            else {
                ni = min + 1;
            }

            $('.listing-datatable-archive').find('tr').each(function () {
                if ($(this).attr('data-index') == ni) {
                    $(this).find('a.ic-preview').click();
                }
            })
        }

        function CheckFileValidation() {
            if ($("#<%= fileNameFld.ClientID%>").val() == "" || $("#<%= fileNameFld.ClientID%>").val() == undefined) {
                alert("Please enter valid file name");
                return false;
            }
            showLoader();
            return true;
        }

        $(document).ready(function () {
            $("#btnIndexSave").click(function (e) {
                deleteIndexData = "";
                var arrIndex = [];
                for (i = 0; i < $('table#tblIndex tr').length; i++) {
                    var $item = $($('table#tblIndex tr')[i]);
                    if (($item.find('#txtKey').val() != null && $item.find('#txtKey').val() != "")) {
                        var result = arrIndex.filter(function (obj) {
                            return obj.IndexKey.toLowerCase() == $item.find('#txtKey').val().toLowerCase();
                        });
                        if (result.length > 0) {
                            alert("Can not enter duplicate keys.");
                            return false;
                        }
                        arrIndex.push({ IndexKey: $item.find('#txtKey').val(), IndexValue: $item.find('#txtValue').val(), IndexId: $($item).find('input.hdnIndexId').val() });
                    }
                    else {
                        deleteIndexData = deleteIndexData + $($item).find('input.hdnIndexId').val() + ",";
                    }
                }

                var test = JSON.stringify(arrIndex);
                deleteIndexData = deleteIndexData.substr(0, deleteIndexData.lastIndexOf(','));
                var data = { "workAreaId": $("#" + '<%= hdnFileId.ClientID%>').val(), "indexData": test, "deleteIndexData": deleteIndexData };
                $.ajax({
                    type: "POST",
                    url: "../Office/WorkArea.aspx/SaveWorkAreaIndexValues",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        deleteIndexData = "";
                        if (result.d == true) {
                            alert("Data has been saved successfully.");
                            $('#overlay').hide();
                            $('#PanelIndex').hide().focus();
                            $("#floatingCirclesG").css('display', 'none');
                            $('#floatingCirclesG').addClass('hideControl');
                        }
                        else
                            alert("Data has not been saved. Please try again later.");
                    }
                });
            });
        });

        function showLoader() {
            $('#overlay').show().height($(document).height());
        }

        function hideLoader() {
            $('#overlay').hide();
        }

        function openwindow(id) {
            var url = '../Office/ModifyBox.aspx?id=' + id;
            var name = 'ModifyBox';
            window.open(url, name, '');
        }

        function InsertViewEdFilesAsBookLog(folderId) {
            window.open("../Office/WebFlashViewer.aspx?V=2&ID=" + folderId + "", '_blank', "toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no");
            CreateDocumentsAndFolderLogs(folderId, null, null, null, null, ActionEnum.BookView, null);
        }


        function setConversionText() {
            var obj = document.getElementById("<%= lbsStatus.ClientID%>");
            obj.setAttribute("src", "../Images/wait24trans.gif");
            obj.setAttribute("Visible", "true");
            obj.innerHTML = "(Converting, Please Wait.)";
        }

        function EditTabNotAllowed() {
            alert('For security, tabs cannot be edited. Please contact EdFiles if you would to edit tabs !!');
            return false;
        }

        function GetArgsFromHref(sHref, sArgName) {
            var args = sHref.split("?");
            var retval = "";

            if (args[0] == sHref) /*参数为空*/ {
                return retval; /*无需做任何处理*/
            }
            var str = args[1];
            args = str.split("&");
            for (var i = 0; i < args.length; i++) {
                str = args[i];
                var arg = str.split("=");
                if (arg.length <= 1) continue;
                if (arg[0] == sArgName) retval = arg[1];
            }
            return retval;
        }

        function pageLoad() {
            if (LoadFolders)
                LoadFolders();

            $('.listing-datatable-archive').find('tr').each(function () {

                $(this).attr('data-index', i);
                i++;
            })
            max = i;
            i = 0;

            $('#dialog-share-book').dialog({
                autoOpen: false,
                width: 450,
                //height: 540,
                top: 300,
                resizable: false,
                modal: true,
                buttons: {
                    "Share Now": function () {
                        SaveMail();
                    },
                    "Close": function () {
                        $(this).dialog("close");
                    }
                },
                close: function () {
                }
            });

            $('.ui-dialog-buttonset').find('button').each(function () {
                $(this).find('span').text($(this).attr('text'));
            })

            $("#dialog-message-all").dialog({
                autoOpen: false,
                resizable: false,
                modal: true,
                Ok: function () {
                    $(this).dialog("close");
                },
                close: function (event, ui) {
                    $(this).dialog("option", 'title', '');
                    $('#dialog-confirm-content').empty();
                }

            });

            window.CheckValidation = function CheckValidation(fieldObj) {
                if (fieldObj.val() == "") {
                    fieldObj.css('border', '1px solid red');
                }
                else {
                    fieldObj.css('border', '1px solid black');
                }
            }

            window.CheckEmailValidation = function CheckEmailValidation(fieldObj) {
                var x = fieldObj.val();
                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(x)) {
                    fieldObj.css('border', '1px solid black');
                    return true;
                }
                else {
                    fieldObj.css('border', '1px solid red');

                    return false;
                }
            }

            function SaveMail() {
                var email = $('#dialog-share-book input[name="email"]').val();
                var emailto = $('#dialog-share-book input[name="emailto"]').val();
                var nameto = $('#dialog-share-book input[name="nameto"]').val();

                var title = $('#dialog-share-book input[name="title"]').val();
                var content = $('#dialog-share-book textarea[name="content"]').val();
                var Isvalid = false;

                $('#dialog-share-book input[name="emailto"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="emailto"]')) });

                if (emailto == "") {
                    CheckValidation($('#dialog-share-book input[name="emailto"]'));
                    CheckEmailValidation($('#dialog-share-book input[name="emailto"]'))
                    return false;
                }

                Isvalid = CheckEmailValidation($('#dialog-share-book input[name="emailto"]'));
                if (!Isvalid) { return Isvalid; }

                var src = '';
                showLoader();

                var path1 = $("#" + '<%= hdnFilePath.ClientID%>').val();
                var filename = path1.replace("WorkArea", "workArea");
                var URL = PrepareURLForFile(null, $('#<%= hdnShareType.ClientID%>').val());
                if ($('#<%= hdnShareType.ClientID%>').val() == '<%= (int)Enum_Tatva.Folders.WorkArea%>') {
                    $.ajax(
                        {

                            type: "POST",
                            url: '../EFileFolderJSONService.asmx/ShareMailForWorkArea',
                            data: "{strToMail:'" + emailto + "',MailTitle:'" + title + "',ToName:'" + nameto + "',SendName:'" + "" + "',strSendMail:'" + email + "',strMailInfor:'" + content + "',URL:'" + URL + "',fileName: '" + (filename == undefined || null ? 0 : filename) + "', workAreaId: '" + $('#<%= hdnFileId.ClientID%>').val() + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                var id = data.d;
                                var title, message;
                                if (id === 0) {
                                    title = "Sharing WorkArea URL error";
                                    message = "This URL has not been shared successfully."
                                }
                                else {
                                    title = "Sharing WorkArea URL success";
                                    message = "This URL has been shared successfully."
                                }

                                $('#dialog-message-all').dialog({
                                    title: title,
                                    open: function () {
                                        $("#spanText").text(message);
                                    }
                                });

                                $('#dialog-message-all').dialog("open");

                                if (id != null || id != "")
                                    $('#dialog-share-book').dialog("close");
                                hideLoader();
                                modalOpenedFrom = "";
                            },
                            fail: function (data) {
                                bookshelf.loaded.apply();

                                $('#dialog-message-all').dialog({
                                    title: "Sharing WorkArea URL error",
                                    open: function () {
                                        $("#spanText").text("This URL has not been shared successfully.");
                                    }
                                });
                                $('#dialog-message-all').dialog("open");

                                hideLoader();
                                modalOpenedFrom = "";
                            }
                        });
                }
                else {
                    var archiveId = $("#" + '<%= hdnFileId.ClientID%>').val();
                    $.ajax(
                        {

                            type: "POST",
                            url: '../EFileFolderJSONService.asmx/ShareMailForArchive',
                            //data: "{ strSendMail:'" + email + "', strToMail:'" + emailto + "', documentId:'" + documentId + "', strMailInfor:'" + content + "',SendName:'" + name + "', ToName:'" + nameto + "',MailTitle:'" + title + "',Path:'" + src + "'}",
                            data: "{strToMail:'" + emailto + "',MailTitle:'" + title + "',ToName:'" + nameto + "',SendName:'" + "" + "',strSendMail:'" + email + "',strMailInfor:'" + content + "',URL:'" + URL + "',archiveId: '" + (archiveId == undefined || 0 ? 0 : archiveId) + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                var id = data.d;
                                var title, message;
                                if (id === 0) {
                                    title = "Sharing Archive URL error";
                                    message = "This URL has not been shared successfully."
                                }
                                else {
                                    title = "Sharing Archive URL success";
                                    message = "This URL has been shared successfully."
                                }

                                $('#dialog-message-all').dialog({
                                    title: title,
                                    open: function () {
                                        $("#spanText").text(message);
                                    }
                                });

                                $('#dialog-message-all').dialog("open");

                                if (id != null || id != "")
                                    $('#dialog-share-book').dialog("close");
                                hideLoader();
                                modalOpenedFrom = "";
                            },
                            fail: function (data) {
                                bookshelf.loaded.apply();

                                $('#dialog-message-all').dialog({
                                    title: "Sharing WorkArea URL error",
                                    open: function () {
                                        $("#spanText").text("This URL has not been shared successfully.");
                                    }
                                });
                                $('#dialog-message-all').dialog("open");

                                hideLoader();
                                modalOpenedFrom = "";
                            }
                        });
                }

            }

            $(".edFileLink").click(function () {
                var folderId = $(this).attr("href").split("?id=")[1];
                CreateDocumentsAndFolderLogs(folderId, null, null, null, null, ActionEnum.View, null);
            });

            $('.editInboxfile').click(function (e) {
                e.preventDefault();
                removeAllInboxEditable();
                showInboxEdit($(this));
            });

            $("#btnDeleteIndexCancel").click(function () {
                $('#DivDeleteConfirmIndex').hide();
                $('#PanelIndex,#overlay').show().focus();
            });

            $('.cancelInboxfile').click(function (e) {
                e.preventDefault();
                removeAllInboxEditable();
            });

            $("#txtValue, #txtKey").keypress(function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    $("#btnIndexSave").click();
                }
            });

            $("#btnIndexCancel, #closeIndex").click(function (e) {
                e.preventDefault();
                $('#overlay').hide();
                $('#PanelIndex').hide().focus();
                $("#floatingCirclesG").css('display', 'none');
                $('#floatingCirclesG').addClass('hideControl');
                e.stopPropagation();
            });

            $("#comboboxSplit").change(function (event) {
                ChangeDivider();
            });

            $('.popupclose,#btnCancelAddToTab,#btnCancelAddToTabSplit,#popupclose_thumbnail').click(function (e) {
                e.preventDefault();
                $(this).closest('.popup-mainbox').hide();
                $('#overlay').hide();
                enableScrollbar();
                makeReadOnly();
            });

            $('#<%= btnSelectAddToTab.ClientID%>').click(function (e) {
                var selectedFolder = $("#combobox").chosen().val();
                var selectedDivider = $("#comboboxDivider").chosen().val();
                if (selectedFolder == "0" && selectedDivider == "0") {
                    alert("Please select at least one EdFile & one Tab");
                    e.preventDefault();
                    return false;
                }
                else if (selectedFolder == "0") {
                    alert("Please select at least one EdFile");
                    e.preventDefault();
                    return false;
                }
                else if (selectedDivider == "0") {
                    alert("Please select at least one Tab");
                    e.preventDefault();
                    return false;
                }
                var folderValue = $('.chosen-single').first().text();
                var tabValue = $('.chosen-single').last().text();
                if (selectedFolder == undefined || selectedFolder == null || selectedFolder.length <= 0 || selectedFolder == "0") {
                    alert('Please select folder to move request files.');
                    e.preventDefault();
                    return false;
                }
                $('#overlay').css('z-index', '100010');
                $('#<%= hdnSelectedFolderID.ClientID%>').val(selectedFolder);
                $('#<%= hdnSelectedDividerID.ClientID%>').val(selectedDivider);
                $('#<%= hdnSelectedFolderText.ClientID%>').val(folderValue);
                $('#<%= hdnSelectedTabText.ClientID%>').val(tabValue);
                return true;
            });

            $('#<%= btnSaveSplitPdf.ClientID%>').click(function (e) {
                var selectedFolder = $("#comboboxSplit").chosen().val();
                var selectedDivider = $("#comboboxDividerSplit").chosen().val();
                if (selectedFolder == "0" && selectedDivider == "0") {
                    alert("Please select at least one EdFile & one Tab");
                    e.preventDefault();
                    return false;
                }
                else if (selectedFolder == "0") {
                    alert("Please select at least one EdFile");
                    e.preventDefault();
                    return false;
                }
                else if (selectedDivider == "0") {
                    alert("Please select at least one Tab");
                    e.preventDefault();
                    return false;
                }
                if (selectedFolder == undefined || selectedFolder == null || selectedFolder.length <= 0 || selectedFolder == "0") {
                    alert('Please select folder to move request files.');
                    e.preventDefault();
                    return false;
                }
                $('#overlay').css('z-index', '100010');
                $('#<%= hdnSelectedFolderID.ClientID%>').val(selectedFolder);
                $('#<%= hdnSelectedDividerID.ClientID%>').val(selectedDivider);
                return true;
            });
        }

        function ShowPreviewForRequestedFiles(id, folderId) {
            window.open(window.location.origin + "/ViewerTemp.aspx?id=" + id, '_blank');
            CreateDocumentsAndFolderLogs(folderId, null, null, id, null, ActionEnum.Preview, null);
            return false;
        }

        function OnShare(obj, folder) {
            if (folder == 'workarea') {
                $('#<%= hdnShareType.ClientID%>').val('<%= (int)Enum_Tatva.Folders.WorkArea%>');
            }
            else {
                $('#<%= hdnShareType.ClientID%>').val('<%= (int)Enum_Tatva.Folders.Archive%>');
            }
            $("#dialog-share-book").dialog({ title: "Share this " + folder + " Document" });
            $('#dialog-share-book').dialog("open");
            $('#dialog-share-book input[name="email"]').val('<%=Sessions.SwitchedEmailId%>');
            $('#dialog-share-book input[name="myname"]').val('');
            $('#dialog-share-book input[name="emailto"]').val('');
            $('#dialog-share-book input[name="nameto"]').val('');
            //$('#dialog-share-book input[name="title"]').val('');
               <%--if ('<%= User.IsInRole("Offices")%>' == 'True')
                   $("#expirationTime").attr("readonly", false);
               else {
                   $("#updateExpiration").hide();
                   $("#expirationTime").css("width", "95%");
               }--%>
            $('#dialog-share-book input[name="linkexpiration"]').val('<%= Sessions.ExpirationTime%>');
            //GetExpirationTime();
            $('#dialog-share-book textarea[name="content"]').val('');

            $('#dialog-share-book input[name="email"]').css('border', '1px solid black');
            $('#dialog-share-book input[name="myname"]').css('border', '1px solid black');
            $('#dialog-share-book input[name="emailto"]').css('border', '1px solid black');

            $('#dialog-share-book input[name="email"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="email"]')) });

            $('#dialog-share-book input[name="emailto"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="emailto"]')) });
            $(<%= hdnFilePath.ClientID%>).val($(obj).closest('tr').find("input[type='hidden'][id$=hdnPath]").val());
            $(<%= hdnFileId.ClientID%>).val($(obj).closest('tr').find("input[type='hidden'][id$=hdnFileId]").val());
        }

        function ShowInboxOpacity(obj) {
            obj.style.getElementById("lnkbtnPreviewInbox")
            obj.style.opacity = 0.2;
        }

        function removeAllInboxEditable() {
            $('.txtFileName,.saveInboxfile,.cancelInboxfile').hide();
            $('.lblfileName,.editInboxfile').show();
        }

        function showInboxEdit(ele) {
            $('.saveInboxfile,.cancelInboxfile').hide();
            $('.deleteInboxfile,.lnkbtnPreview,.editInboxfile,.lnkBtnDownload, .lnkBtnShare, .lnkBtnAddToTab').show();
            var $this = $(ele);
            $this.css('display', 'none');
            $this.parent().find('[name=lbkUpdate]').show();
            $this.parent().find('[name=lnkCancel]').show();
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                $(this).find('input[type=text]').css('border', '1px solid #c4c4c4');
                $(this).find('input[type = text]').show();
                if ($(this).find('input[type = text]').val() == undefined || $(this).find('input[type = text]').val() == "") { }
                else {
                    oldname = $(this).find('input[type = text]').val();
                }
                $(this).find('[name=lblfileName]').hide();
                $(this).find('[name=lblClass]').hide();
                $(this).parent().find('.deleteInboxfile,.lnkbtnPreview,.lnkBtnDownload, .lnkBtnShare, .lnkBtnAddToTab, .editdocs').hide();
            });
            return false;
        }

        function ShowPreviewForEdForms(edFormsUserShareId, fileName) {
            showLoader();
            $('.forWorkArea').hide();
            $('.forArchive').hide();
            $('#<%= hdnFileId.ClientID%>').val(edFormsUserShareId);
            $.ajax({
                type: "POST",
                url: '../Office/EdFormsFolder.aspx/GetPreviewURL',
                contentType: "application/json; charset=utf-8",
                data: "{ edFormsUserShareId:'" + edFormsUserShareId + "', fileName:'" + fileName + "'}",
                dataType: "json",
                success: function (data) {
                    if (data.d == "") {
                        hideLoader();
                        alert("Failed to get object from cloud");
                        return false;
                    }
                    else
                        showPreview(data.d);
                },
                error: function (result) {
                    console.log('Failed' + result.responseText);
                    hideLoader();
                }
            });
            return false;
        }

        function ShowPreviewForWorkArea(path, fileName, workAreaId) {
            showLoader();
            $('.forWorkArea').show();
            $('.forArchive').hide();
            var folderID = <%= (int)Enum_Tatva.Folders.WorkArea%>
                $.ajax({
                    type: "POST",
                    url: '../Office/WorkArea.aspx/GetPreviewURL',
                    contentType: "application/json; charset=utf-8",
                    data: "{ documentID:'" + 0 + "', folderID:'" + folderID + "', shareID:'" + path + "', workAreaId:'" + workAreaId + "'}",
                    dataType: "json",
                    success: function (data) {
                        if (data.d == "") {
                            hideLoader();
                            alert("Failed to get thumbnails!");
                            return false;
                        }
                        else {
                            if (data.d.length > 1) {
                                showPreview(data.d[0]);
                                $("#" + '<%= hdnFileName.ClientID%>').val(data.d[0]);
                                   $("#" + '<%= hdnFileId.ClientID%>').val(data.d[2]);
                                   $('#preview_popup').css("max-width", "1300px");
                               }
                           }
                       },
                       error: function (result) {
                           console.log('Failed' + result.responseText);
                           hideLoader();
                       }
                   });
            return false;
        }

        function ShowPreviewForArchive(obj, isContinue) {
            $('.listing-datatable-archive tr').removeClass('activetr');
            $('.forWorkArea').hide();
            $('.forArchive').show();
            $(obj).closest('tr').addClass('activetr');
            if (!isContinue) {
                $('#<%= hdnFileNameOnly.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnFileName]").val());
                $('#<%= hdnFilePath.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnPath]").val());
                $('#<%= hdnFileId.ClientID%>').val($(obj).closest('tr').find("input[type='hidden'][id$=hdnFileId]").val());
            }
            $('#<%= hdnObjId.ClientID%>').val(obj.id);
            //}
            showLoader();
            $("#<%= lblFileName.ClientID%>").html($('#<%= hdnFileNameOnly.ClientID%>').val());

            var folderID = '<%= (int)Enum_Tatva.Folders.Archive%>';

            $.ajax({
                type: "POST",
                url: '../Office/ArchiveSearch.aspx/GetPreviewURL',
                contentType: "application/json; charset=utf-8",
                data: "{ archiveID:'" + $('#<%= hdnFileId.ClientID%>').val() + "', folderID:'" + folderID + "', shareID:'" + $('#<%= hdnFilePath.ClientID%>').val() + "', isSplit:'" + false + "'}",
                dataType: "json",
                success: function (data) {
                    $("#overlay").css("z-index", "9999");

                    if (data.d == "") {
                        hideLoader();
                        alert("Failed to get thumbnails!");
                        return false;
                    }
                    else {
                        if (data.d.length > 1) {
                            showPreview(data.d[0]);
                            $("#" + '<%= hdnFileName.ClientID%>').val(data.d[0]);
                            $("#" + '<%= hdnFileId.ClientID%>').val(data.d[2]);
                            $('#preview_popup').css("max-width", "1300px");
                        }
                    }
                    var ci = $(obj).closest('tr').attr('data-index');
                    if (ci == max - 1) {
                        $(".btnPrevPreview").hide();
                    }
                    else {
                        $(".btnPrevPreview").show();
                    }
                    if (ci == min + 1) {
                        $(".btnNextPreview").hide();
                    }
                    else {
                        $(".btnNextPreview").show();

                    }
                },
                error: function (result) {
                    $("#overlay").css("z-index", "9999");

                    console.log('Failed' + result.responseText);
                    hideLoader();
                }
            });
            return false;
        }

        function DisplayAddToTabModal(e, folder) {
            if (folder == 'workarea') {
                $('#<%= hdnShareType.ClientID%>').val('<%= (int)Enum_Tatva.Folders.WorkArea%>');
            }
            else {
                $('#<%= hdnShareType.ClientID%>').val('<%= (int)Enum_Tatva.Folders.EdForms%>');
            }
            e.preventDefault();
            $('#AddToTab_popup,#overlay').show().focus();
            $('#combobox').trigger('chosen:open');
            $(<%= hdnFilePath.ClientID%>).val($(e.currentTarget).closest('tr').find("input[type='hidden'][id$=hdnPath]").val());
            $(<%= hdnFileName.ClientID%>).val($(e.currentTarget).closest('tr').find("input[type='hidden'][id$=hdnFileName]").val());
            $(<%= hdnFileId.ClientID%>).val($(e.currentTarget).closest('tr').find("input[type='hidden'][id$=hdnFileId]").val());
            e.stopPropagation();
            disableScrollbar();
        }

        function DownloadDoc(obj, folderId) {
            window.open(PrepareURLForFile(obj, folderId), '_blank');
        }

        function PrepareURLForFile(obj, folderId) {
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//"
                    + window.location.hostname
                    + (window.location.port ? ':' + window.location.port : '');
            }
            var hdnId;
            var tempURL = '';
            if (obj != null && obj != undefined) {
                var hdnObj = $(document.getElementById(obj)).closest('tr').find("input[type='hidden'][id$=hdnPath]");
                hdnId = $(document.getElementById(obj)).closest('tr').find("input[type='hidden'][id$=hdnFileId]").val();
                path = $(hdnObj).val();
                if (folderId == '<%=(int)Enum_Tatva.Folders.WorkArea%>') {
                    tempURL = "path=" + path + "&ID=" + hdnId + "<%= "&Data=" + QueryString.QueryStringEncode("ID=&folderID=" + (int)Enum_Tatva.Folders.WorkArea + "&sessionID=" + Sessions.SwitchedRackspaceId)%>";
                }
                else {
                    tempURL = "path=" + path + "&ID=" + hdnId + "<%= "&Data=" + QueryString.QueryStringEncode("ID=&folderID=" + (int)Enum_Tatva.Folders.Archive + "&sessionID=" + Sessions.SwitchedRackspaceId)%>";
                }
            }
            else {
                path = $("#" + '<%= hdnFilePath.ClientID%>').val();
                hdnId = $("#" + '<%= hdnFileId.ClientID%>').val();
                if (folderId == '<%=(int)Enum_Tatva.Folders.WorkArea%>') {
                    tempURL = "path=" + path + "&ID=" + hdnId + "<%= "&Data=" + QueryString.QueryStringEncode("ID=&dateTime=" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff") + "&folderID=" + (int)Enum_Tatva.Folders.WorkArea + "&sessionID=" + Sessions.SwitchedRackspaceId)%>";
                }
                else {
                    tempURL = "path=" + path + "&ID=" + hdnId + "<%= "&Data=" + QueryString.QueryStringEncode("ID=&dateTime=" + DateTime.UtcNow.ToString("yyyyMMddHHmmssfff") + "&folderID=" + (int)Enum_Tatva.Folders.Archive + "&sessionID=" + Sessions.SwitchedRackspaceId)%>";
                }
            }
            var url = window.location.origin + "/Office/PdfViewer.aspx?" + tempURL;
            url = encodeURI(url);
            return url;
        }

        function disableScrollbar() {
            $('html, body').css({
                'overflow': 'hidden',
                'height': '100%'
            });
        }

        function enableScrollbar() {
            $('html, body').css({
                'overflow': 'auto',
                'height': 'auto'
            });
        }

        function cancleInboxFileName(ele) {
            var $this = $(ele);
            var InboxlabelName = '';
            var FileName = '';
            $this.css('display', 'none');
            $this.parent().find('[name=lbkUpdate]').hide();
            $this.parent().find('[name=lnkEdit]').show();
            $this.parent().find('.deleteInboxfile,.lnkbtnPreview,.lnkBtnDownload, .lnkBtnShare, .lnkBtnAddToTab, .editdocs').show();
            var row = $(ele).closest("tr");
            $("td", row).each(function () {
                var len = 0;
                len = $(this).find('input[type=text]').length;
                if (len > 0) {
                    InboxlabelName = $(this).find('[name=InboxfileName]').html();
                    $(this).find('input[type = text]').val(InboxlabelName);
                    $(this).find('input[type = text]').hide();
                    $(this).find('[name=InboxfileName]').show();
                }
                else {
                }
            });
        }

        var newname, Path, oldName, inboxId, inboxFileExtention;
        function OnSaveRename(txtRenameObj, path, ext, workAreaId) {
            var test = $('#<%= hdnFilePath.ClientID%>').val();
            var newFileName = $('#' + txtRenameObj)[0].value;
            var extension = "";
            if (newFileName && newFileName.indexOf('.') > 0)
                extension = newFileName.substring(newFileName.indexOf('.') + 1).toLowerCase();

            if (newFileName == "") {
                alert("Please enter new file name");
            }
            else if (!/^[a-z\d][a-z\d\()_\-.\s]+$/i.test(newFileName)) {
                alert("File name can not contain special characters.");
            }
            else if (newFileName.indexOf('.') > 0 && (extension != 'jpg') && (extension != 'jpeg') && (extension != 'pdf') && (extension != 'doc') && (extension != 'docx') && (extension != 'xls')) {
                alert("Only File with type 'jpg,jpeg,pdf,doc,docx and xls' is allowed.");
            }
            else {
                showLoader();
                $.ajax({
                    type: "POST",
                    url: '../Office/WorkArea.aspx/RenameObject',
                    contentType: "application/json; charset=utf-8",
                    data: "{ oldObjectPath:'" + path + "', newObjectPath:'" + newFileName + "', selectedPath:'" + $('#<%= hdnFilePath.ClientID%>').val() + "', workAreaId:'" + workAreaId + "', searchKey:''}",
                    dataType: "json",
                    success: function (data) {
                        if (data.d.length > 0) {
                            var objList = JSON.parse(data.d);
                            if (objList.ErrorMessage && objList.ErrorMessage.length > 0) {
                                alert("Failed to rename document. Please try again after sometime.");
                                hideLoader();
                            }
                        }
                        else {
                            alert("File has been renamed successfully.");
                            __doPostBack('', 'RefreshGrid@');
                        }
                    },
                    error: function (result) {
                        console.log('Failed' + result.responseText);
                        hideLoader();
                        $('#lblSelectedTreeNode').html("");
                    }
                });
            }
        }

        function OpenIndexPopup(ele) {
            $("#" + '<%= hdnFileId.ClientID%>').val($(ele).closest('tr').find("input[type='hidden'][id$=hdnIndexWorkAreaId]").val());
            var data = { "workAreaId": $(ele).closest('tr').find("input[type='hidden'][id$=hdnIndexWorkAreaId]").val() }
            $.ajax({
                type: "POST",
                url: "../Office/WorkArea.aspx/GetWorkAreaIndexValues",
                data: JSON.stringify(data),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (result.d.length > 0) {
                        var $this = $(ele);
                        var path = '/' + $($this).parent().find('[name*=hdnPathIndex]').val();
                        path = path.replace(/\\/g, "/");

                        $('#PanelIndex,#overlay').show().focus();
                        $("#floatingCirclesG").css('display', 'none');
                        var index = 0;
                        var arrIndex = [];
                        $.each($.parseJSON(result.d), function (i, e) {
                            arrIndex.push(e);
                        });
                        $("#tblIndex").find("tr:gt(0)").remove();
                        var row = $("#tblIndex tr:first");
                        $(row).find("#txtKey").val("");
                        $(row).find("#txtValue").val("");
                        $(row).find("#btnAddIndexPair").show();
                        $(row).find("#btnRemoveIndexPair").hide();
                        if (arrIndex.length > 0) {
                            $(row).find(".hdnIndexId").val(arrIndex[0].IndexId);
                            $(row).attr("id", "indexRow" + arrIndex[0].IndexId);
                        }
                        else
                            $(row).find(".hdnIndexId").val(0);
                        for (var i = 0; i < arrIndex.length; i++) {
                            if (i == 0) {
                                SetIndexValues(arrIndex[i], true);
                            }
                            else
                                AppendIndexDetails(arrIndex[i], i);
                        }
                    }
                }
            });
        }

        function AppendIndexDetails(indexPair, index) {
            CreateDynamicIndexTextBox(indexPair.IndexId, true);
            SetIndexValues(indexPair, false);
        }

        function SetIndexValues(indexPair, isRender) {
            var row = "";
            if (!isRender)
                row = document.getElementById("indexRow" + indexPair.IndexId);
            else
                row = $("#tblIndex tr:first");
            $(row).find("#txtKey").val(indexPair.IndexKey);
            $(row).find("#txtValue").val(indexPair.IndexValue);
            $(row).find(".hdnIndexId").val(indexPair.IndexId);
        }

        function GetDynamicTextBox(value) {
            var test = '<td><input type="hidden" value ="0" class="hdnIndexId"></td>' +
                '<td><input type="text" ID = "txtKey" Width="90%" MaxLength="20" placeholder="Key" style="height:25px;margin-left:10px;"/></td>' +
                '<td><input type="text" ID= "txtValue" Width="90%" MaxLength="20" placeholder="Value" style="height:25px;"/></td>' +
                '<td><button id="btnAddIndexPair" type="button" Width="90%"  OnClick="return AddIndexTextBoxes(this)"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/images/add-icon.png" title="Add" style="margin-bottom:0px;"/> </button> </td>' +
                        '<td><button id="btnRemoveIndexPair"  type="button" Width="90%"  OnClick="return DisplayConfirmBox(this)"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/images/delete-icon.png" alt="Remove" title="Remove" style="margin-bottom:0px;"/> </button>'

            return test;
        }

        var indexRemoveEle = "";
        function DisplayConfirmBox(ele) {
            $("#PanelIndex").hide();
            $('#DivDeleteConfirmIndex,#overlay').show().focus();
            indexRemoveEle = ele;
        }

        function AddIndexTextBoxes(ele) {
            var index = parseInt($($(ele).parent().parent().find('[class*=hdnIndexId]')).val());
            if ($(ele).closest('tr').find('#txtKey').val() == "") {
                alert("Please enter the Key Value.");
                return false;
            }
            CreateDynamicIndexTextBox(index, false);
            return false;
        }

        function CreateDynamicIndexTextBox(index, isRender) {
            var tr = document.createElement('tr');
            tr.innerHTML = GetDynamicTextBox(index);

            if (!isRender)
                tr.setAttribute("id", "indexRow" + (index = index + 1));
            else
                tr.setAttribute("id", "indexRow" + index);
            // var body = $(parentElement).parent();
            $('table#tblIndex tr:last').after(tr);
            $(tr).find("#txtKey").focus();
            SetIndexButtonVisibility();

            //Allow maximum of 5 key,value pairs
            if ($('table#tblIndex tr').length == 5) {
                $(tr).find('#btnAddIndexPair').hide();
            }
        }

        var deleteIndexData = "";
        function RemoveIndexTextBoxes() {
            $('#DivDeleteConfirmIndex').hide();
            $('#PanelIndex,#overlay').show().focus();
            if ($(indexRemoveEle).closest('tr').find('input.hdnIndexId').val() != 0) {
                var hdnId = $(indexRemoveEle).closest('tr').find('input.hdnIndexId').val();
                var test = deleteIndexData.indexOf(hdnId);
                if (test <= 0)
                    deleteIndexData = deleteIndexData + hdnId + ",";
            }
            $(indexRemoveEle).closest('tr').remove();
            SetIndexButtonVisibility();
            indexRemoveEle = "";
            return false;
        }


        function SetIndexButtonVisibility() {
            var rowCount = $('table#tblIndex tr').length;
            for (i = 0; i < rowCount; i++) {
                var $item = $($('table#tblIndex tr')[i]);

                if (i == (rowCount - 1)) {
                    $item.find('#btnRemoveIndexPair').show();
                    $item.find('#btnAddIndexPair').show();
                    if (i == 0) {
                        $item.find('#btnRemoveIndexPair').hide();
                    }
                }
                else if (i == 0) {
                    $item.find('#btnRemoveIndexPair').show();
                    $item.find('#btnAddIndexPair').hide();
                }
                else {
                    $item.find('#btnRemoveIndexPair').show();
                    $item.find('#btnAddIndexPair').hide();
                }
            }
        }

        function showPreview(URL) {
            // URL = URL.substr(1);
            hideLoader();
            if (!window.location.origin) {
                window.location.origin = window.location.protocol + "//"
                    + window.location.hostname
                    + (window.location.port ? ':' + window.location.port : '');
            }


            //var src = 'http://docs.google.com/gview?url=' + window.location.origin + '/' + URL.trim() + '&embedded=true';
            //if (window.location.origin.indexOf('https://') > -1)
            //    src = 'https://docs.google.com/gview?url=' + window.location.origin + '/' + URL.trim() + '&embedded=true';

            src = '../Office/Preview.aspx?data=' + window.location.origin + URL.trim() + "?v=" + "<%= DateTime.Now.Ticks%>";
            var popup = document.getElementById("preview_popup");
            var overlay = document.getElementById("overlay");
            $('#reviewContent').attr('src', src);
            overlay.style.display = 'block';
            popup.style.display = 'block';
            $('#preview_popup').focus();
            disableScrollbar();
            return false;
        }

        function checkAllThumbnail() {

            var selectAllValue = $("#btnCheckedAll").is(":checked");

            $('iFrame').contents().find('.check').each(function (k, v) {
                if (selectAllValue)
                    $(v).attr('checked', true);
                else
                    $(v).attr('checked', false);
            });
        }

        function GetSplitPdfValue() {
            var pageNumber = '';
            if ($('iFrame').contents().find('.check').length > 0) {
                $('iFrame').contents().find('.check').each(function (k, v) {
                    var val = $(v).is(":checked");
                    if (val) {
                        if (pageNumber == '')
                            pageNumber += $(v).attr('data-value') + ',';
                        else
                            pageNumber += $(v).attr('data-value') + ',';
                    }

                });
                pageNumber = pageNumber.substring(0, pageNumber.length - 1);

                if (pageNumber.length <= 0) {
                    alert("Please select at least one page.");
                    return false;
                }

                $("#" + '<%= hdnPageNumbers.ClientID%>').val(pageNumber);
            }
            else {
                alert('Please select thumbnail');
                return false;
            }
            return true;
        }

        function ShowAddToTabPopup() {
            if (GetSplitPdfValue()) {
                $.ajax(
                    {
                        type: "POST",
                        url: '../FlexWebService.asmx/GetFoldersData',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $('<option />', { value: 0, text: "Select a file" }).appendTo($("#comboboxSplit")); $.each(data.d, function (i, text) {
                                $('<option />', { value: i, text: text }).appendTo($("#comboboxSplit"));
                            });
                            $("#comboboxSplit").chosen({ width: "315px" });

                            ChangeDivider();
                        },
                        fail: function (data) {
                        }
                    });
                var popup = document.getElementById("preview_popup");
                popup.style.display = 'none';

                var popup = document.getElementById("preview_popup_split");
                popup.style.display = 'block';
            }
        }

        function ChangeDivider() {
            $("#comboboxDividerSplit").html('');
            $("#comboboxDividerSplit").chosen("destroy");
            var selectedFolder = $("#comboboxSplit").chosen().val();
            $.ajax(
                {
                    type: "POST",
                    url: '../FlexWebService.asmx/GetDividersByEFF',
                    data: "{eff:" + parseInt(selectedFolder) + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $('<option />', { value: 0, text: "Select a tab" }).appendTo($("#comboboxDividerSplit"));
                        $.each(data.d, function (i, text) {
                            $('<option />', { value: i, text: text }).appendTo($("#comboboxDividerSplit"));
                        });
                        $("#comboboxDividerSplit").chosen({ width: "315px" });
                    },
                    fail: function (data) {
                    }
                });
        }

        function ShowConfirm(message) {
            if (confirm(message)) {
                window.location.href = window.location.href;
            }
            else {
                window.location.href = window.location.origin + '/Office/FileFolderBox.aspx?id=' + $('#<%= hdnSelectedFolderID.ClientID%>').val();
            }
        }

    </script>
    <%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
        TagPrefix="cc1" %>
    <asp:HiddenField ID="hdnPageNumbers" runat="server" />
    <asp:HiddenField ID="hdnSelectedFolderID" runat="server" />
    <asp:HiddenField ID="hdnSelectedDividerID" runat="server" />
    <asp:HiddenField ID="hdnSelectedTabText" runat="server" />
    <asp:HiddenField ID="hdnSelectedFolderText" runat="server" />
    <asp:HiddenField ID="hdnFileName" runat="server" />
    <asp:HiddenField ID="hdnFileId" runat="server" />
    <asp:HiddenField ID="hdnFilePath" runat="server" />
    <asp:HiddenField ID="hdnObjId" runat="server" />
    <asp:HiddenField ID="hdnFileNameOnly" runat="server" />
    <asp:HiddenField ID="hdnShareType" runat="server" />
    <div id="overlay">
        <div id="floatingCirclesG">
            <div class="f_circleG" id="frotateG_01"></div>
            <div class="f_circleG" id="frotateG_02"></div>
            <div class="f_circleG" id="frotateG_03"></div>
            <div class="f_circleG" id="frotateG_04"></div>
            <div class="f_circleG" id="frotateG_05"></div>
            <div class="f_circleG" id="frotateG_06"></div>
            <div class="f_circleG" id="frotateG_07"></div>
            <div class="f_circleG" id="frotateG_08"></div>
        </div>
    </div>

    <%--Preview Document--%>
    <div class="popup-mainbox preview-popup" id="preview_popup" style="display: none">
        <div class="popupcontrols" style="width: 100%; height: 5%;">
            <span id="popupclose" class="ic-icon ic-close popupclose"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%;">
            <table style="width: 100%; height: 100%">
                <tr style="height: 5%">
                    <td>
                        <div style="text-align: center" class="forArchive">
                            <input type="button" id="btnPrevious" runat="server" value="PREVIOUS" onclick="ShowPrevious();" class="btn green btnNextPreview" title="PREVIOUS" />
                            <div style="display: inline-flex; margin-bottom: 20px;">
                                <asp:Label ID="editableFile" runat="server" Style="font-size: 20px; padding-top: 10px;">Filename:</asp:Label>
                                <asp:Label ID="lblFileName" runat="server" Style="font-size: 15px; margin: 14px; margin-bottom: 0;"></asp:Label>
                                <asp:TextBox ID="fileNameFld" CssClass="fileNameField" runat="server" Style="display: none;"></asp:TextBox>
                                <asp:Button ID="btnArchiveUpdate" CssClass="quick-find-btn btnEdit" OnClientClick="return makeEditable();" Style="display: inline-block" runat="server" Text="Rename" />
                                <asp:Button ID="btnArchiveSave" OnClientClick="return CheckFileValidation();" CssClass="quick-find-btn btnEdit" OnClick="btnArchiveSave_Click" Style="display: none; margin-left: 20px;" runat="server" Text="Save" />
                                <asp:Button ID="btnArchiveCancel" CssClass="quick-find-btn btnEdit" OnClientClick="return makeReadOnly();" Style="display: none; margin-left: 10px;" runat="server" Text="Cancel" />
                            </div>
                            <input type="button" id="btnNext" runat="server" value="NEXT" onclick="ShowNext();" class="btn green btnPrevPreview" title="NEXT" />
                        </div>
                        <div class="forWorkArea">
                            <input type="button" id="btnSpltBtn" runat="server" value="Tab" onclick="ShowAddToTabPopup();" style="margin-bottom: 10px;" class="btn green btnAddtoTab" title="Add To Tab" />
                            <div class="selecttabcheck"><span class="selectalltab">Select All</span><input type="checkbox" id="btnCheckedAll" value="Select All" onclick="checkAllThumbnail();" /></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <iframe id="reviewContent" style="width: 100%; height: 100%;"></iframe>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <%--End Preview Document--%>

    <%--Share Document--%>
    <div id="dialog-share-book" title="Share this Inbox Document" style="display: none">
        <p class="validateTips">All form fields are required.</p>
        <br />
        <%-- <br />--%>
        <label for="email">From Email</label><br />
        <input type="email" name="email" id="email" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <%--<label for="myname" style="display:none;">My Name</label><br />
                    <input type="text" name="myname" id="myname" class="text ui-widget-content ui-corner-all" style="border: 1px solid black;display:none" /><br />--%>
        <label for="emailto">To Email</label><br />
        <input type="email" name="emailto" id="emailto" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <label for="nameto">To Name</label><br />
        <input type="text" name="nameto" id="nameto" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <label for="title">Subject</label><br />
        <input type="text" name="title" id="title" value="EdFile share" class="text ui-widget-content ui-corner-all" style="border: 1px solid black"  readonly="readonly"/><br />
        <label for="content">Message</label><br />
        <textarea name="content" id="content" rows="2" cols="25" class="text ui-widget-content ui-corner-all" style="border: 1px solid black"></textarea><br />
        <label for="content">Link Expiration Time (In Hrs)</label><br />
        <input type="text" name="linkexpiration" id="expirationTime" class="text ui-widget-content ui-corner-all" style="border: 1px solid black; width: 80%;" />
        <input type="button" id="updateExpiration" onclick="UpdateExpirationTime()" class="btn-small green" style="float: right" value="UPDATE" /><br />
        <span id="errorMsg"></span>
    </div>
    <div id="dialog-message-all" title="" style="display: none;">
        <p id="dialog-message-content">
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span>
            <span id="spanText"></span>
        </p>
    </div>
    <%--End Share Document--%>
    <div class="popup-mainbox preview-popup" id="PanelIndex" style="display: none; max-width: 500px; height: 30%; top: auto">
        <div>
            <div class="popup-head" id="Div4">
                WorkArea Indexing
               
                <span id="closeIndex" class="ic-icon ic-close" style="float: right; cursor: pointer"></span>
            </div>
            <div class="popup-scroller-index" style="margin-top: 10px;">
                <div class="popup-row">
                    <input type="hidden" id="hndDividerIdIndex" />
                    <div class="popup-left">

                        <table width="100%" id="tblIndex">
                            <tbody>
                                <tr id="indexRow0" class="indexRow">
                                    <td>
                                        <input type="hidden" value="0" class="hdnIndexId"></td>
                                    <td>
                                        <input type="text" id="txtKey" width="90%" maxlength="20" placeholder="Key" style="height: 25px; margin-left: 10px;" /></td>
                                    <td>
                                        <input type="text" id="txtValue" width="90%" maxlength="20" placeholder="Value" style="height: 25px;" /></td>
                                    <td>
                                        <button id="btnAddIndexPair" type="button" width="90%" onclick="return AddIndexTextBoxes(this)">
                                            <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/images/add-icon.png" title="Add" style="margin-bottom: 0px;" /></button></td>
                                    <td>
                                        <button id="btnRemoveIndexPair" type="button" width="90%" onclick="return DisplayConfirmBox(this)">
                                            <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/images/delete-icon.png" alt="Remove" title="Remove" style="margin-bottom: 0px;" />
                                        </button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
            <div class="popup-btn">
                <input id="btnIndexSave" type="button" value="Save" class="btn green" style="margin-right: 5px;" />
                <input id="btnIndexCancel" type="button" value="Cancel" class="btn green" />
            </div>

        </div>
    </div>

    <div class="popup-mainbox" id="DivDeleteConfirmIndex" style="display: none; position: fixed; z-index: 999999; margin-left: 37%; top: auto;">
        <div>
            <div class="popup-head" id="Div5">
                Delete Index
           
            </div>
            <div class="popup-scroller">
                <p>
                    Caution! Are you sure you want to delete this index?
               
                </p>
            </div>
            <div class="popup-btn">
                <input id="btnDeleteIndexOkay" onclick="return RemoveIndexTextBoxes()" type="button" value="Yes" class="btn green" style="margin-right: 5px;" />
                <input id="btnDeleteIndexCancel" type="button" value="No" class="btn green" />
            </div>
        </div>
    </div>

    <div class="popup-mainbox preview-popup" id="AddToTab_popup" style="display: none; height: 460px; width: 395px; max-width: 395px; max-height: 460px; margin-top: 20px;">
        <div class="popupcontrols popup-head" style="width: 100%; height: 60px;">
            Select Folder
       
            <span id="Span2" class="ic-icon ic-close popupclose"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%;">
            <div class="popup-scroller" style="height: 75%">
                <p style="margin: 20px">
                    <uc1:ucAddToTab ID="ucAddToTab1" runat="server" />
                </p>

            </div>
            <div class="popup-btn">
                <asp:Button ID="btnSelectAddToTab" Text="Save" runat="server" CssClass="btn green" OnClick="btnSelectAddToTab_Click" Style="margin-right: 5px;" />
                <input id="btnCancelAddToTab" type="button" value="Cancel" class="btn green" />
            </div>
        </div>
    </div>

    <div class="popup-mainbox preview-popup" id="preview_popup_split" style="display: none; height: 460px; width: 395px; max-width: 395px; max-height: 460px; margin-top: 20px;">
        <div class="popupcontrols popup-head" style="width: 100%; height: 60px;">
            Split PDF Details
       
            <span id="popupclose_split" class="ic-icon ic-close popupclose"></span>
        </div>
        <div class="popupcontent" style="width: 100%; height: 95%; background: #eaeaea; border-top: 2px solid darkgray;">
            <div class="popup-scroller" style="height: 75%; background: #eaeaea;">
                <p style="margin: 20px">
                    <label>New split Pdf name</label>
                    <asp:TextBox runat="server" ID="txtNewPdfName" Style="margin-bottom: 5px;"></asp:TextBox>
                    <br />
                    <label>1. Select an edFile from list</label>
                    <select id="comboboxSplit">
                    </select>

                    <label style="display: block; margin-top: 15px">2. Select a tab for the selected edFile</label>
                    <select id="comboboxDividerSplit" style="display: none;">
                    </select>
                </p>

            </div>
            <div class="popup-btn" style="background: #eaeaea;">

                <asp:Button ID="btnSaveSplitPdf" runat="server" Text="Save" CssClass="btn green" OnClick="btnSaveSplitPdf_Click" Style="margin-right: 5px;" />
                <input id="btnCancelAddToTabSplit" type="button" value="Cancel" class="btn green" />
            </div>
        </div>
    </div>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Search EdFiles</h1>
            <div class="clearfix">
            </div>
        </div>
    </div>
    <div class="page-container register-container" style="padding: 0px;">
        <div class="inner-wrapper">
            <%-- <div class="quick-find">
                <uc1:QuickFind ID="QuickFind1" runat="server" />
            </div>--%>
            <div class="clearfix">
            </div>
            <div class="form-containt listing-contant">
                <div class="content-box listing-view">

                    <div style="margin-top: 10px">
                        <ajaxToolkit:TabContainer runat="server" ID="fileTabs" ActiveTabIndex="0" Width="100%">
                            <ajaxToolkit:TabPanel runat="server" ID="Panel1" HeaderText="EdFiles">
                                <ContentTemplate>
                                    <div class="prevnext" style="float: right" bgcolor="#f8f8f5">
                                        <cc1:WebPager ID="WebPager1" ControlToPaginate="EdFilesGrid" PagerStyle="NumericPages"
                                            CssClass="prevnext custom_select" PageSize="15" OnPageIndexChanged="WebPager1_PageIndexChanged" runat="server" />
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanelResult" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:HiddenField ID="txtHasViewPrivilege" runat="server" />
                                            <asp:HiddenField ID="txtHasPrivilage" runat="server" />
                                            <asp:HiddenField ID="EdFilesSortDirection" runat="server" Value="" />
                                            <div class="work-area">
                                                <div class="overflow-auto">
                                                    <asp:GridView ID="EdFilesGrid" runat="server" AutoGenerateColumns="false" AllowSorting="True"
                                                        OnSorting="EdFilesGrid_Sorting" OnRowDataBound="EdFilesGrid_RowDataBound" DataKeyNames="FolderID"
                                                        OnRowDeleting="EdFilesGrid_RowDeleting" CssClass="datatable listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false" ItemStyle-Width="50px"
                                                                ItemStyle-CssClass="actionMinWidth">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Action" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton6" runat="server" CausesValidation="false" CssClass="ic-icon ic-download"
                                                                        OnClick="LinkButton6_Click" Text='<%# Eval("FolderID")%>' />
                                                                    <ajaxToolkit:ModalPopupExtender ID="lnkArchive_ModalPopupExtender" runat="server"
                                                                        CancelControlID="Button2ArchiveClose" OkControlID="Button1ArchiveOK" TargetControlID="LinkButton6"
                                                                        PopupControlID="DivArchiveConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="cbeArchiveEFileFolder" runat="server" DisplayModalPopupID="lnkArchive_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="LinkButton6"></ajaxToolkit:ConfirmButtonExtender>
                                                                    <asp:LinkButton ID="LinkButton1" runat="server" Text="" CausesValidation="false"
                                                                        OnClick="LinkButton1_Click" CssClass="ic-icon ic-edit"></asp:LinkButton>
                                                                    <asp:LinkButton ID="libDelete" runat="server" Text="" CausesValidation="false" CommandName="Delete"
                                                                        CssClass="ic-icon ic-delete"></asp:LinkButton>
                                                                    <asp:LinkButton ID="LinkButton2" runat="server" Text="" CssClass="ic-icon ic-view" Style="display: none;"
                                                                        CommandArgument='<%#string.Format("{0},{1},{2}", Eval("FolderID"), Eval("Firstname"), Eval("Lastname"))%>'
                                                                        OnCommand="LinkButton2_Click" OnClientClick='<%#string.Format("window.open(\"../Office/WebFlashViewer.aspx?V=2&ID={0}\",\"eBook\",\"toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no\")", Eval("FolderID"))%>' />
                                                                    <asp:LinkButton ID="LinkButton21" runat="server" Text="" CausesValidation="false"
                                                                        CssClass="ic-icon ic-document" CommandArgument='<%#string.Format("{0},{1},{2}", Eval("FolderID"), Eval("Firstname"), Eval("Lastname"))%>'
                                                                        OnCommand="LinkButton2_Click" OnClientClick='<%#string.Format("javascript:InsertViewEdFilesAsBookLog({0})", Eval("FolderID"))%>' />
                                                                    <asp:LinkButton ID="btnPreview1" runat="server" CssClass="ic-icon logFormPreveiw"></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnPreview2" runat="server" CssClass="ic-icon logFormPreveiw"></asp:LinkButton>

                                                                    <asp:LinkButton ID="btnPreview3" runat="server" CssClass="ic-icon logFormPreveiw"></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnPreview4" runat="server" CssClass="ic-icon logFormPreveiw"></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnPreview5" runat="server" CssClass="ic-icon logFormPreveiw"></asp:LinkButton>

                                                                    <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                                                                        CancelControlID="ButtonDeleteCancel" OkControlID="ButtonDeleleOkay" TargetControlID="libDelete"
                                                                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                                                                    </ajaxToolkit:ModalPopupExtender>
                                                                    <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                                                                        ConfirmText="" TargetControlID="libDelete"></ajaxToolkit:ConfirmButtonExtender>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:HyperLinkField DataTextField="ComboFileNumber" HeaderText="FolderName" DataNavigateUrlFields="FolderID"
                                                                DataNavigateUrlFormatString="~/Office/FileFolderBox.aspx?id={0}" SortExpression="FolderName" ControlStyle-CssClass="edFileLink">
                                                                <%--  <HeaderStyle CssClass="form_title" />--%>
                                                            </asp:HyperLinkField>
                                                            <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname">
                                                                <%-- <HeaderStyle CssClass="form_title" />--%>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname">
                                                                <%--  <HeaderStyle CssClass="form_title" />--%>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FileNumber" HeaderText="FileNumber" SortExpression="FileNumber">
                                                                <%-- <HeaderStyle CssClass="form_title" />--%>
                                                            </asp:BoundField>

                                                            <%--<asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label3" runat="server" Text="Security" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="TextBoxLevel" Text='<%#Bind("SecurityLevel")%>' runat="server" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit" HeaderText="Tabs">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label10" Text="Tabs" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton4" runat="server" CausesValidation="false" Text='<%#Bind("OtherInfo")%>'
                                                                        OnClick="LinkButton4_Click"></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="OtherInfo2" SortExpression="OtherInfo2" HeaderText="Size(Mb)" DataFormatString="{0:F2}"
                                                                HtmlEncode="false" />
                                                            <asp:BoundField DataField="Alert1" HeaderText="Alert" SortExpression="Alert1" />
                                                            <asp:BoundField DataField="Alert2" HeaderText="Status" SortExpression="Alert2" />
                                                            <asp:BoundField DataField="Site1" HeaderText="Site" SortExpression="Site1" />
                                                            <asp:BoundField DataField="CreatedDate" HeaderText="CreateDate" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="CreatedDate" ItemStyle-Width="3%" />
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />There is no EdFile for searched keyword.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                        </ajaxToolkit:TabContainer>
                    </div>

                    <div style="margin-top: 10px">
                        <ajaxToolkit:TabContainer runat="server" ID="TabContainer1" ActiveTabIndex="0" Width="100%">
                            <ajaxToolkit:TabPanel runat="server" ID="Panel2" HeaderText="Box Files">
                                <ContentTemplate>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="prevnext" style="float: right" bgcolor="#f8f8f5">
                                                <cc1:WebPager ID="WebPager2" runat="server" OnPageIndexChanged="WebPager2_PageIndexChanged"
                                                    PageSize="15" CssClass="prevnext custom_select"
                                                    PagerStyle="NumericPages" ControlToPaginate="BoxGrid"></cc1:WebPager>
                                                <asp:HiddenField ID="BoxSortDirection" runat="server" Value=""></asp:HiddenField>
                                            </div>
                                            <div class="work-area">
                                                <div class="overflow-auto">
                                                    <asp:GridView ID="BoxGrid" runat="server" DataKeyNames="FileID" OnSorting="BoxGrid_Sorting"
                                                        AllowSorting="True" AutoGenerateColumns="false" CssClass="datatable datatable-small listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" />
                                                        <Columns>
                                                            <asp:BoundField DataField="FileID" HeaderText="File ID" SortExpression="FileID" ItemStyle-Width="60px"></asp:BoundField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true" ItemStyle-Width="30px"
                                                                ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label34" Text="BoxName" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton21" runat="server" Text='<%#Bind("BoxName")%>' CausesValidation="false"
                                                                        OnClientClick='<%#string.Format("javascript:openwindow({0})", Eval("BoxId"))%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="Warehouse" HeaderText="Warehouse" SortExpression="Warehouse" />
                                                            <asp:BoundField DataField="Section" HeaderText="Section" SortExpression="Section" />
                                                            <asp:BoundField DataField="Row" HeaderText="Row" SortExpression="Row" />
                                                            <asp:BoundField DataField="Space" HeaderText="Space" SortExpression="Space" />
                                                            <asp:BoundField DataField="Other" HeaderText="Other" SortExpression="Other" />
                                                            <asp:BoundField DataField="DestroyDate" HeaderText="DestroyDate" DataFormatString="{0:MM.dd.yyyy}" SortExpression="DestroyDate" />
                                                            <asp:BoundField DataField="ScannedDate" HeaderText="ScannedDate" DataFormatString="{0:MM.dd.yyyy}" SortExpression="ScannedDate" />
                                                            <asp:BoundField DataField="BoxNumber" HeaderText="BoxNumber" SortExpression="BoxNumber" />
                                                            <asp:BoundField DataField="FirstName" HeaderText="FirstName" SortExpression="FirstName" />
                                                            <asp:BoundField DataField="LastName" HeaderText="LastName" SortExpression="LastName" />
                                                            <asp:BoundField DataField="FileName" HeaderText="FileName" SortExpression="FileName" />
                                                            <asp:BoundField DataField="FileNumber" HeaderText="FileNumber" SortExpression="FileNumber" />
                                                            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-CssClass="table_tekst_edit">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label11" Text="Action" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton5" runat="server" CausesValidation="false" ToolTip="Request" CssClass="ic-icon ic-request"
                                                                        OnCommand="LinkButton5_Click" OnClientClick="showLoader();" CommandArgument='<%#Eval("FileID")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />There is no Stored Paper File for searched keyword.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="WebPager2" EventName="PageIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                        </ajaxToolkit:TabContainer>
                    </div>

                    <div style="margin-top: 10px;">
                        <ajaxToolkit:TabContainer runat="server" ID="TabContainer2" ActiveTabIndex="0" Width="100%">
                            <ajaxToolkit:TabPanel runat="server" ID="Panel3" HeaderText="Workarea">
                                <ContentTemplate>
                                    <asp:UpdatePanel ID="UpdatePanel13" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Image runat="server" ImageUrl="../Images/loader_bg.gif" ID="lbsStatus" ImageAlign="Middle" />
                                            <div class="prevnext" style="float: right" bgcolor="#f8f8f5">
                                                <cc1:WebPager ID="WebPager3" runat="server" OnPageIndexChanged="WebPager3_PageIndexChanged"
                                                    PageSize="15" CssClass="prevnext custom_select" PagerStyle="NumericPages" ControlToPaginate="WorkAreaGrid"></cc1:WebPager>
                                                <asp:HiddenField ID="WorkAreaSortDirection" runat="server" Value=""></asp:HiddenField>
                                            </div>
                                            <div class="work-area">
                                                <div class="overflow-auto">
                                                    <asp:GridView ID="WorkAreaGrid" runat="server" AllowSorting="True"
                                                        AutoGenerateColumns="false" OnRowDataBound="WorkAreaGrid_RowDataBound"
                                                        CssClass="datatable datatable-small listing-datatable" OnSorting="WorkAreaGrid_Sorting">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <Columns>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="15%" SortExpression="FileName">
                                                                <HeaderTemplate>
                                                                    <asp:LinkButton ID="lblfileName" runat="server" Text="Document Name" CommandName="Sort" CommandArgument="FileName" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblfileName" runat="server" Text='<%#Eval("FileName")%>' name="lblfileName" CssClass="lblfileName" Style="cursor: pointer; text-decoration: underline; color: blue" />
                                                                    <asp:TextBox runat="server" MaxLength="50" Text='<%#Eval("FileName")%>'
                                                                        ID="txtFileName" Style="display: none; height: 30px;" name="txtFileName" CssClass="txtFileName" />
                                                                    <asp:HiddenField ID="hdnIndexWorkAreaId" runat="server" Value='<%# Eval("WorkAreaId")%>' />
                                                                    <asp:HiddenField ID="hdnPathIndex" runat="server" Value='<%# Eval("PathName")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:BoundField DataField="CreatedDate" HeaderText="Modified Date" ItemStyle-Width="20%"
                                                                HtmlEncode="false" SortExpression="CreatedDate" />

                                                            <asp:BoundField DataField="Size" HeaderText="Size" DataFormatString="{0:F4}" ItemStyle-Width="6%"
                                                                HtmlEncode="false" SortExpression="Size" />

                                                            <asp:TemplateField ShowHeader="false" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false" ItemStyle-Width="17%"
                                                                ItemStyle-CssClass="table_tekst_edit actionMinWidth">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="LabelAction" runat="server" Text="Action" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <%--Edit--%>
                                                                    <asp:LinkButton ID="lnkEdit" runat="server" Text=""
                                                                        CssClass="ic-icon ic-edit editInboxfile" name="lnkEdit" ToolTip="Rename File" Style="margin-left: -12px"></asp:LinkButton>
                                                                    <asp:HiddenField ID="hdnFileId" Value='<%# Eval("WorkAreaId").ToString()%>' runat="server" />
                                                                    <asp:HiddenField ID="hdnPath" runat="server" Value='<%# Eval("PathName").ToString()%>' />

                                                                    <asp:LinkButton ID="lbkUpdate" runat="server" CausesValidation="True" AutoPostBack="False"
                                                                        ToolTip="Update" CssClass="ic-icon ic-save saveInboxfile" Style="display: none" name="lbkUpdate"></asp:LinkButton>
                                                                    <asp:LinkButton ID="lnkCancel" runat="server" CausesValidation="False" CommandName="Cancel" AutoPostBack="False"
                                                                        OnClientClick="cancleInboxFileName(this)" ToolTip="Cancel" CssClass="ic-icon ic-cancel cancelInboxfile" Style="display: none" name="lnkCancel"></asp:LinkButton>
                                                                    <%--Edit End--%>
                                                                    <asp:LinkButton ID="btnIndexing" runat="server" CssClass="ic-icon ic-index editdocs" CausesValidation="False" ToolTip="File Indexing"
                                                                        CommandName=""></asp:LinkButton>
                                                                    <asp:LinkButton ID="btnWorkAreaPreview" runat="server" CssClass="ic-icon ic-preview lnkbtnPreview" CausesValidation="False" ToolTip="Preview File"
                                                                        CommandName=""></asp:LinkButton>
                                                                    <asp:HyperLink ID="btnWorkAreaDownload" runat="server" CausesValidation="false" CssClass="ic-icon ic-download downloadFile lnkBtnDownload" Target="_blank" ToolTip="Download File" />
                                                                    <asp:Button ID="btnWorkAreaShare" runat="server" CssClass="icon-btn ic-icon ic-share lnkBtnShare" ToolTip="Share File" OnClientClick="OnShare(this, 'workarea')" />
                                                                    <asp:Button ID="btnWorkAreaAddToTab" runat="server" CssClass="icon-btn addtotab-workarea lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event, 'workarea')" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />There is no WorkArea files for searched keyword.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="WebPager3" EventName="PageIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                        </ajaxToolkit:TabContainer>
                    </div>

                    <div style="margin-top: 10px;">
                        <ajaxToolkit:TabContainer runat="server" ID="TabContainer3" ActiveTabIndex="0" Width="100%">
                            <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="Archive">
                                <ContentTemplate>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Image runat="server" ImageUrl="../Images/loader_bg.gif" ID="Image1" ImageAlign="Middle" />
                                            <div class="prevnext" style="float: right" bgcolor="#f8f8f5">
                                                <cc1:WebPager ID="WebPager4" runat="server" OnPageIndexChanged="WebPager4_PageIndexChanged"
                                                    PageSize="10" CssClass="prevnext custom_select" PagerStyle="NumericPages" ControlToPaginate="ArchiveGrid"></cc1:WebPager>
                                                <asp:HiddenField ID="ArchiveSortDirection" runat="server" Value=""></asp:HiddenField>
                                            </div>
                                            <div class="work-area">
                                                <div class="overflow-auto">
                                                    <asp:GridView ID="ArchiveGrid" runat="server" AllowSorting="True"
                                                        AutoGenerateColumns="false" OnRowDataBound="ArchiveGrid_RowDataBound"
                                                        CssClass="datatable datatable-small listing-datatable-archive" OnSorting="ArchiveGrid_Sorting">
                                                        <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <Columns>
                                                            <asp:BoundField HeaderText="File Name" DataField="FileName" SortExpression="FileName" />
                                                            <asp:BoundField DataField="CreatedDate" HeaderText="Created Date" SortExpression="CreatedDate" ItemStyle-Width="25%" />
                                                            <asp:BoundField DataField="Size" HeaderText="Size" SortExpression="Size" ItemStyle-Width="10%" />
                                                            <asp:TemplateField ShowHeader="true">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Action" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-preview" CausesValidation="False" ToolTip="Preview"
                                                                        OnClientClick="ShowPreviewForArchive(this, false);return false;"></asp:LinkButton>
                                                                    <asp:HyperLink ID="btnArchiveDownload" runat="server" CausesValidation="false" CssClass="ic-icon ic-download downloadFile lnkBtnDownload" Target="_blank" ToolTip="Download File" />
                                                                    <asp:Button ID="btnArchiveShare" runat="server" CssClass="icon-btn ic-icon ic-share lnkBtnShare" ToolTip="Share File" OnClientClick="OnShare(this, 'archive')" />
                                                                    <asp:HiddenField runat="server" ID="hdnFileId" Value='<%# Eval("ArchiveId")%>' />
                                                                    <asp:HiddenField runat="server" ID="hdnPath" Value='<%# Eval("PathName")%>' />
                                                                    <asp:HiddenField runat="server" ID="hdnFileName" Value='<%#Eval("FileName")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />There is no Archived Records for searched keyword.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="WebPager4" EventName="PageIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                        </ajaxToolkit:TabContainer>
                    </div>

                    <div style="margin-top: 10px;">
                        <ajaxToolkit:TabContainer runat="server" ID="TabContainer4" ActiveTabIndex="0" Width="100%">
                            <ajaxToolkit:TabPanel runat="server" ID="TabPanel2" HeaderText="EdForms">
                                <ContentTemplate>
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Image runat="server" ImageUrl="../Images/loader_bg.gif" ID="Image2" ImageAlign="Middle" />
                                            <div class="prevnext" style="float: right" bgcolor="#f8f8f5">
                                                <cc1:WebPager ID="WebPager5" runat="server" OnPageIndexChanged="WebPager5_PageIndexChanged"
                                                    PageSize="10" CssClass="prevnext custom_select" PagerStyle="NumericPages" ControlToPaginate="EdFormsGrid"></cc1:WebPager>
                                                <asp:HiddenField ID="EdFormsSortDirection" runat="server" Value=""></asp:HiddenField>
                                            </div>
                                            <div class="work-area">
                                                <div class="overflow-auto">
                                                    <asp:GridView ID="EdFormsGrid" runat="server" OnSorting="EdFormsGrid_Sorting" OnRowDataBound="EdFormsGrid_RowDataBound"
                                                        AllowSorting="True" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover datatable listing-datatable" Width="100%" Style="margin-top: 20px;">
                                                        <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <Columns>
                                                            <asp:BoundField DataField="FileName" HeaderText="File Name" SortExpression="FileName" ItemStyle-Width="25%" />
                                                            <asp:BoundField DataField="ReceivedDate" HeaderText="Received Date" DataFormatString="{0:MM-dd-yyyy HH:mm:ss}" SortExpression="ReceivedDate" ItemStyle-Width="15%" />
                                                            <asp:BoundField DataField="StatusName" HeaderText="Status" SortExpression="StatusName" ItemStyle-Width="10%" />
                                                            <asp:BoundField DataField="ToEmail" HeaderText="Submitted By" SortExpression="ToEmail" ItemStyle-Width="15%" />
                                                            <asp:BoundField DataField="SharedDate" HeaderText="Shared Date" DataFormatString="{0:MM-dd-yyyy HH:mm:ss}" SortExpression="SharedDate" ItemStyle-Width="15%" />
                                                            <asp:TemplateField ShowHeader="true" ItemStyle-Width="20%">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="lblMoveOrder" runat="server" Text="Action" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="libPreview" runat="server" CssClass="ic-icon ic-preview" CausesValidation="False" ToolTip="Preview"
                                                                        CommandName="" OnClientClick='<%# "return ShowPreviewForEdForms(\"" + Eval("EdFormsUserShareId") + "\",\"" + Eval("FileName") + "\")"%>'></asp:LinkButton>
                                                                    <asp:HyperLink ID="lnkBtnDownload" runat="server" CausesValidation="false" CssClass="ic-icon ic-download downloadFile lnkBtnDownload" Target="_blank" />
                                                                    <asp:Button ID="btnAddToTab" runat="server" CssClass="icon-btn addtotab-workarea lnkBtnAddToTab" ToolTip="Add to tab" OnClientClick="DisplayAddToTabModal(event, 'edForms')" />
                                                                    <asp:HiddenField runat="server" ID="hdnFileId" Value='<%# Eval("EdFormsUserShareId")%>' />
                                                                    <asp:HiddenField runat="server" ID="hdnFileName" Value='<%#Eval("FileName")%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                <asp:Label ID="Label1" runat="server" />There is no submitted EdForms for searched keyword.
                                                           
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="WebPager5" EventName="PageIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                        </ajaxToolkit:TabContainer>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- Tabs Editor Panel-->
    <asp:Button Style="display: none" ID="Button1ShowPopup" runat="server"></asp:Button>
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="btnClose1" PopupControlID="Panel1Tabs" TargetControlID="Button1ShowPopup">
    </ajaxToolkit:ModalPopupExtender>
    <!-- ModalPopup Panel-->
    <asp:Panel Style="display: none; z-index: 10001" ID="Panel1Tabs" runat="server" CssClass="popup-mainbox">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="divTabNum" class="popup-head">
                    &nbsp;Tabs Details
               
                </div>
                <div class="popup-scroller">
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                    <asp:GridView ID="GridView12" ShowFooter="true" OnRowEditing="GridView12_OnRowEditing"
                        OnRowCancelingEdit="GridView12_RowCancelingEdit" OnRowUpdating="GridView12_RowUpdating"
                        OnRowDataBound="GridView12_OnRowDataBound" OnRowCommand="GridView12_OnRowCommand"
                        OnRowDeleting="GridView12_OnRowDeleting" runat="server" Width="100%" AutoGenerateColumns="false"
                        DataKeyNames="DividerID" CssClass="datatable listing-datatable">
                        <PagerSettings Visible="False" />
                        <AlternatingRowStyle CssClass="odd" />
                        <RowStyle CssClass="even" />
                        <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                        <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                        <FooterStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />--%>
                        <Columns>
                            <asp:TemplateField ShowHeader="false" ItemStyle-Width="100px" ItemStyle-Wrap="false"
                                ItemStyle-CssClass="table_tekst_edit">
                                <HeaderTemplate>
                                    <asp:Label ID="Label18" runat="server" Text="Action" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:LinkButton ID="LinkButton5" CommandName="Edit" runat="server" Text="" CssClass="ic-icon ic-edit"></asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton3" CommandName="Delete" CausesValidation="false" runat="server"
                                        Text="" CssClass="ic-icon ic-delete"></asp:LinkButton>
                                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" ConfirmText="Are you sure to delete the tab ?(then delete everything, tab and the docs inside that tab)"
                                        TargetControlID="LinkButton3"></ajaxToolkit:ConfirmButtonExtender>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:LinkButton ID="lnkInsert" runat="server" CausesValidation="False" CommandName="Insert"
                                        ToolTip="Insert" CssClass="ic-icon ic-add"></asp:LinkButton>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    <asp:LinkButton ID="lbkUpdate1" runat="server" CausesValidation="True" CommandName="Update"
                                        ToolTip="Update" CssClass="ic-icon ic-save"></asp:LinkButton>
                                    <asp:LinkButton ID="lnkCancel1" runat="server" CausesValidation="False" CommandName="Cancel"
                                        ToolTip="Cancel" CssClass="ic-icon ic-cancel"></asp:LinkButton>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="80px" ItemStyle-Wrap="false">
                                <HeaderTemplate>
                                    <asp:Label ID="Label6" runat="server" Text="Name" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="Label12" Width="120px" runat="server" Text='<%#Eval("Name")%>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:TextBox ID="txtNewName" Text="" runat="server"></asp:TextBox>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "Name")%>'
                                        ID="textfield10" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false">
                                <HeaderTemplate>
                                    <asp:Label ID="Label7" runat="server" Text="Color" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <cc1:LabelExender ID="LabelExender1" Color='<%#Eval("Color")%>' BorderWidth="1px"
                                        Width="80px" runat="server">&nbsp;</cc1:LabelExender>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <div style="display: inline-flex">
                                        <div style="border: solid 1px black; height: 32px; width: 70px">
                                            <asp:Label
                                                ID="lblSample1"
                                                AutoCompleteType="None"
                                                runat="server" Style="display: block; height: 22px; width: 62px; margin: 3px;" />
                                        </div>
                                        <asp:TextBox
                                            ID="txtCardColor1"
                                            AutoCompleteType="None"
                                            runat="server" Style="height: 0px; width: 0px; border: none" />
                                        <asp:Button
                                            CssClass="cp_button"
                                            ID="btnPickColor1"
                                            runat="server" />
                                        <cc1:ColorPickerExtender
                                            ID="ColorPicker3"
                                            TargetControlID="txtCardColor1"
                                            PopupButtonID="btnPickColor1"
                                            PopupPosition="TopRight"
                                            SampleControlID="lblSample1"
                                            SelectedColor='<%# Eval("ColorWithoutHash")%>'
                                            OnClientColorSelectionChanged="Color_Changed"
                                            Enabled="True"
                                            runat="server"></cc1:ColorPickerExtender>
                                    </div>
                                </EditItemTemplate>
                                <FooterTemplate>
                                    <div style="display: inline-flex; margin-top: 4px;">
                                        <div style="border: solid 1px black; height: 32px; width: 70px">
                                            <asp:Label
                                                ID="lblSample2"
                                                AutoCompleteType="None"
                                                runat="server" Style="display: block; height: 22px; width: 62px; margin: 3px;" />
                                        </div>
                                        <asp:TextBox
                                            ID="txtCardColor2"
                                            AutoCompleteType="None"
                                            runat="server" Style="height: 0px; width: 0px; border: none" />
                                        <asp:Button
                                            CssClass="cp_button"
                                            ID="btnPickColor2"
                                            runat="server" />
                                        <cc1:ColorPickerExtender
                                            ID="ColorPicker5"
                                            TargetControlID="txtCardColor2"
                                            PopupButtonID="btnPickColor2"
                                            PopupPosition="TopRight"
                                            SampleControlID="lblSample2"
                                            OnClientColorSelectionChanged="Color_Changed"
                                            SelectedColor="FF0000"
                                            Enabled="True"
                                            runat="server"></cc1:ColorPickerExtender>
                                    </div>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="60px" ItemStyle-Wrap="false">
                                <HeaderTemplate>
                                    <asp:Label ID="Label62" runat="server" Text="Locked" />
                                </HeaderTemplate>
                                <%--  <HeaderStyle CssClass="form_title" />--%>
                                <ItemTemplate>
                                    <asp:Label ID="Label122" Width="60px" runat="server" Text='<%# Eval("Locked")%>' />
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:CheckBox ID="txtLocked" Text="" Checked="False" runat="server"></asp:CheckBox>
                                </FooterTemplate>
                                <EditItemTemplate>
                                    <asp:CheckBox runat="server" ID="checkLocked" Checked='<%# DataBinder.Eval(Container.DataItem, "Locked")%>' />
                                </EditItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
                <triggers></triggers>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="popup-btn">
            <asp:Button ID="btnSave1" runat="server" OnClick="btnClose_Click" Text="Close" CssClass="btn green" />
            <asp:Button ID="btnClose1" runat="server" Text="Close" Style="display: none" CssClass="btn green" />
        </div>
    </asp:Panel>
    <!---- -->
    <asp:Panel class="popup-mainbox" ID="DivDeleteConfirmation" Style="display: none"
        runat="server">
        <div>
            <div class="popup-head" id="PopupHeader">
                Delete Folder
           
            </div>
            <div class="popup-scroller">
                <p>
                    Caution! Are you sure you want to delete this folder. Once you delete this file,
                    it will be deleted for good
               
                </p>
                <p>
                    Confirm Password:
                   
                    <asp:TextBox ID="textPassword" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                </p>
            </div>
            <div class="popup-btn">
                <input id="ButtonDeleleOkay" type="button" value="Yes" class="btn green" style="margin-right: 20px;" />
                <input id="ButtonDeleteCancel" type="button" value="No" class="btn green" />
            </div>
        </div>
    </asp:Panel>
    <!---- -->
    <asp:Panel class="popup-mainbox" ID="DivArchiveConfirmation" Style="display: none" runat="server">
        <div>
            <div class="popup-head" id="Div3">
                Download Folder
           
            </div>
            <div class="popup-scroller">
                <p>
                    Please enter your password to download a zipped folder of all the documents from within this EdFile.
               
                </p>
                <p>
                    Confirm Password:
                   
                    <asp:TextBox ID="txtBoxArchivePwd" runat="server" Width="230px" MaxLength="20" TextMode="Password" />
                </p>
            </div>
            <div class="popup-btn">
                <input id="Button1ArchiveOK" type="button" value="Yes" class="btn green" style="margin-right: 20px;" />
                <input id="Button2ArchiveClose" type="button" value="No" class="btn green" />
            </div>
        </div>
    </asp:Panel>
    <!-- EFileFolder Edit Message Panel-->
    <asp:Button Style="display: none" ID="btnShowMessagePopup" runat="server"></asp:Button>
    <ajaxToolkit:ModalPopupExtender ID="mdlPopupMessage" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="btnMessageClose" PopupControlID="pnlMessagePopup" TargetControlID="btnShowMessagePopup">
    </ajaxToolkit:ModalPopupExtender>
    <asp:Panel Style="display: none" ID="pnlMessagePopup" runat="server" BackColor="White"
        Width="450px">
        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div>
                    <asp:Label ID="Label2" runat="server" Font-Bold="true" Text="Warning Message" BackColor="lightblue"
                        Width="450px" />
                </div>
                <div style="background-color: White; color: #000; padding-left: 5px; padding-right: 5px;">
                    <p>
                        Your login doesn't have permission to edit or delete EdFiles. Please contact
                        administrator to make this change.
                   
                    </p>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div align="right" style="width: 450px; background-color: lightblue; margin: 0px;">
            <asp:Button ID="btnMessageClose" runat="server" Text="Close" Width="50px" />
        </div>
    </asp:Panel>
    <!-- EFileFolder Editor Panel-->
    <asp:Button Style="display: none" ID="btnShowPopup" runat="server"></asp:Button>
    <ajaxToolkit:ModalPopupExtender ID="mdlPopup" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="btnClose" PopupControlID="pnlPopup" TargetControlID="btnShowPopup">
    </ajaxToolkit:ModalPopupExtender>
    <!-- ModalPopup Panel-->
    <asp:Panel Style="display: none" ID="pnlPopup" runat="server" CssClass="popup-mainbox">
        <asp:UpdatePanel ID="updPnlFolderDetail" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label ID="lblFileFolderDetail" runat="server" Font-Bold="true" Text="EdFiles Details"
                    BackColor="#eaeaea" Width="100%" CssClass="popup-head" />
                <div class="popup-scroller">
                    <asp:DetailsView ID="dvFolderDetail" DataKeyNames="FolderID,OfficeID" OnDataBound="FolderDetail_OnDataBound"
                        AllowPaging="false" AutoGenerateRows="false" runat="server" DefaultMode="Edit"
                        Width="100%" BackColor="white" CssClass="details-datatable" Border="0">
                        <Fields>
                            <asp:BoundField DataField="FolderName" HeaderText="FolderName" />
                            <asp:BoundField DataField="Firstname" HeaderText="Firstname" />
                            <asp:BoundField DataField="Lastname" HeaderText="Lastname" />
                            <asp:BoundField DataField="Email" HeaderText="Email" />
                            <asp:BoundField DataField="Address" HeaderText="Address" />
                            <asp:BoundField DataField="City" HeaderText="City" />
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    <asp:Label ID="Label35" runat="server" Text="State" />
                                </HeaderTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlStates" runat="server">
                                    </asp:DropDownList>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="OtherState" HeaderText="OtherState" />
                            <asp:BoundField DataField="Postal" HeaderText="Postal" />
                            <asp:BoundField DataField="Tel" HeaderText="Tel." />
                            <asp:BoundField DataField="FaxNumber" HeaderText="FaxNumber" />
                            <asp:BoundField DataField="FileNumber" HeaderText="FileNumber" />
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    <asp:Label ID="lblDOB" runat="server" Text="DOB" />
                                </HeaderTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox name="textfield5" Placeholder="mm-dd-yyyy" runat="server" ID="textfield5"
                                        size="15" MaxLength="50" Text='<% #Eval("DOB")%>' />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Alert1" HeaderText="Alert1" />
                            <asp:BoundField DataField="Alert2" HeaderText="Status" />
                            <asp:BoundField DataField="Site1" HeaderText="Site1" />
                            <asp:BoundField DataField="Comments" HeaderText="Comments" />
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    <asp:Label ID="Label5" runat="server" Text="Security" />
                                </HeaderTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="DropDownList1" runat="server" OnDataBinding="DropDownList1_DataBinding" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"
                                        DataValueField="SecurityLevel" AutoPostBack="true">
                                        <asp:ListItem Text="Private" Value="0" />
                                        <asp:ListItem Text="Protected" Value="1" />
                                        <asp:ListItem Text="Public" Value="2" />
                                    </asp:DropDownList>
                                    <asp:TextBox ID="folderCode" runat="server" MaxLength="20" TextMode="Password" Width="92" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    <asp:Label ID="label6" runat="server" Text="Groups" />
                                </HeaderTemplate>
                                <EditItemTemplate>
                                    <div runat="server" id="trgroups" style="padding: 5px 0">
                                        <div runat="server" id="divfield1" style="padding: 5px 0">
                                            <fieldset style="border: none">
                                                <label>&nbsp;</label>
                                                <asp:CheckBox name="groupfield1" runat="server" Text=""
                                                    ID="groupfield1" CssClass="checkForGroup" size="20" />
                                                <asp:TextBox ID="grouplink1" Text="Group 1" runat="server" Width="80%" Height="30px" />
                                            </fieldset>

                                        </div>
                                        <div runat="server" id="divfield2" style="padding: 5px 0">
                                            <fieldset style="border: none">
                                                <label>&nbsp;</label>
                                                <asp:CheckBox name="groupfield2" runat="server" Text=""
                                                    ID="groupfield2" CssClass="checkForGroup" size="20" />
                                                <asp:TextBox ID="grouplink2" Text="Group 2" runat="server" Width="80%" Height="30px" />
                                            </fieldset>
                                        </div>
                                        <div runat="server" id="divfield3" style="padding: 5px 0">
                                            <fieldset style="border: none">
                                                <label>&nbsp;</label>
                                                <asp:CheckBox name="groupfield3" runat="server" Text=""
                                                    ID="groupfield3" size="20" CssClass="checkForGroup" />
                                                <asp:TextBox ID="grouplink3" Text="Group 3" runat="server" Width="80%" Height="30px" />
                                            </fieldset>
                                        </div>
                                        <div runat="server" id="divfield4" style="padding: 5px 0">
                                            <fieldset style="border: none">
                                                <label>&nbsp;</label>
                                                <asp:CheckBox name="groupfield4" runat="server" Text=""
                                                    ID="groupfield4" CssClass="checkForGroup" size="20" />
                                                <asp:TextBox ID="grouplink4" Text="Group 4" runat="server" Width="80%" Height="30px" />
                                            </fieldset>
                                        </div>
                                        <div runat="server" id="divfield5" style="padding: 5px 0">
                                            <fieldset style="border: none">
                                                <label>&nbsp;</label>
                                                <asp:CheckBox name="groupfield5" runat="server" Text=""
                                                    ID="groupfield5" CssClass="checkForGroup" size="20" />
                                                <asp:TextBox ID="grouplink5" Text="Group 5" runat="server" Width="80%" Height="30px" />
                                            </fieldset>
                                        </div>
                                    </div>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                                <HeaderTemplate>
                                    <asp:Label ID="Label17" runat="server" Text="Security Code" />
                                </HeaderTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="textCodeBox" MaxLength="20" Text='<%#DataBinder.Eval(Container.DataItem, "FolderCode")%>'
                                        runat="server" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="TemplateName" ReadOnly="true" HeaderText="Template" />
                            <asp:BoundField DataField="OtherInfo" ReadOnly="true" HeaderText="Tabs Number" />
                            <asp:BoundField DataField="OtherInfo2" ReadOnly="true" HeaderText="Total Size(Mb)"
                                DataFormatString="{0:F2}" HtmlEncode="false" />
                        </Fields>
                    </asp:DetailsView>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnSave" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <div class="popup-btn">
            <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" Text="Save" CssClass="btn green" Style="margin-right: 20px;" />
            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn green" />
        </div>
    </asp:Panel>
    <!-- Tabs Editor Panel-->
    <asp:Button Style="display: none" ID="Button1" runat="server"></asp:Button>
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender2" runat="server" BackgroundCssClass="modalBackground"
        CancelControlID="btnClose1" PopupControlID="Panel1Tabs" TargetControlID="Button1ShowPopup">
    </ajaxToolkit:ModalPopupExtender>
    <!-- ModalPopup Panel-->
    <asp:Panel Style="display: none; z-index: 10001" ID="Panel4" runat="server" Width="600px"
        BackColor="lightblue">
        <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div id="div1">
                    &nbsp;File Details
               
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div align="right" style="width: 95%; background-color: lightblue">
            <asp:Button ID="Button2" runat="server" Text="Close" Width="50px" />
            <asp:HiddenField ID="HiddenField2" runat="server" />
        </div>
    </asp:Panel>
    <script type="text/javascript">
        function LoadColorPicker() {
            debugger
            //This code must be placed below the ScriptManager, otherwise the "Sys" cannot be used because it is undefined.
            Sys.Application.add_init(function () {
                // Store the color validation Regex in a "static" object off of
                // AjaxControlToolkit.ColorPickerBehavior.  If this _colorRegex object hasn't been
                // created yet, initialize it for the first time.
                if (!Sys.Extended.UI.ColorPickerBehavior._colorRegex) {
                    Sys.Extended.UI.ColorPickerBehavior._colorRegex = new RegExp('^[A-Fa-f0-9]{6}$');
                }
            });
        }

        function Color_Changed(sender) {
            sender.get_element().value = "#" + sender.get_selectedColor();
        }
    </script>
</asp:Content>
