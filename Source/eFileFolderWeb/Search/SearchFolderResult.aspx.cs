using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DataQuicker2.Framework;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;
using AjaxControlToolkit;
using Shinetech.Framework.Controls;
using Shinetech.Utility;
using System.IO;
using Shinetech.Engines.Adapters;
using System.Collections.Generic;
using iTextSharp.text.pdf;
using Shinetech.Engines;
using System.Linq;

/// <summary>
/// SearchFolderResult
/// </summary>
/// <seealso cref="LicensePage"/>
public partial class SearchFolderResult : LicensePage
{
    private static readonly string PDFPATH = ConfigurationManager.AppSettings["PdfPath"];
    private static readonly string INFOEMAIL = ConfigurationManager.AppSettings["INFOEMAIL"];
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
    public static string CurrentVolume = System.Configuration.ConfigurationManager.AppSettings["CurrentVolume"];
    public string virtualDir = ConfigurationManager.AppSettings["VirtualDir"];
    public string newFileName = string.Empty;
    public static string splitWorkAreaPath = System.Configuration.ConfigurationManager.AppSettings["WorkAreaThumbNailPath"];
    public NLogLogger _logger = new NLogLogger();
    public RackSpaceFileUpload rackspacefileupload = new RackSpaceFileUpload();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            var argument = Request.Form["__EVENTARGUMENT"];
            if (!Page.IsPostBack)
            {
                GetData();
                BindGrid();
            }
            else
            {
                if (argument != null && !string.IsNullOrEmpty(argument) && Convert.ToString(argument).ToLower().Contains("refreshgrid"))
                {
                    GetData();
                    BindGrid();
                }
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    #region DataBind

    /// <summary>
    /// Gets the data.
    /// </summary>
    public void GetData()
    {
        string key = Request.QueryString["key"];
        GetData(key);
    }

    /// <summary>
    /// Gets the data.
    /// </summary>
    /// <param name="key">The key.</param>
    public void GetData(string key)
    {
        try
        {
            DataSet dsSearchedData = new DataSet();
            string officeId = Sessions.SwitchedRackspaceId;
            var uid = Sessions.SwitchedSessionId;
            if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users"))
            {
                if (!key.Contains(","))
                    key = string.Join(":", key.Split(new char[0], StringSplitOptions.RemoveEmptyEntries));
                dsSearchedData = General_Class.GetCoprehensiveSearchForFolder(key, officeId, uid);
                if (dsSearchedData != null && dsSearchedData.Tables.Count > 4)
                {
                    DataTable folders = dsSearchedData.Tables[0];
                    var act = new Shinetech.DAL.Account(uid);
                    string[] groups = act.Groups.Value != null ? act.Groups.Value.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries) : new string[] { };
                    bool? hasProtectedSecurity = (act.HasProtectedSecurity.IsNull || Convert.ToBoolean(act.HasProtectedSecurity.ToString())) ? true : false;
                    bool? hasPublicSecurity = (act.HasPublicSecurity.IsNull || Convert.ToBoolean(act.HasPublicSecurity.ToString())) ? true : false;
                    int count = folders.Rows.Count;

                    for (int i = 0; i < count; i++)
                    {
                        DataRow row = folders.Rows[i];
                        if (!uid.Equals(row["OfficeID"].ToString()))
                        {
                            string security = row["SecurityLevel"].ToString();
                            if ((security.Equals("0") && !BelongToGroup(row["Groups"].ToString(), groups)) || (security.Equals("1") && !hasProtectedSecurity.Value) || (security.Equals("2") && !hasPublicSecurity.Value))
                            {
                                row.Delete();
                            }
                        }
                    }
                    folders.AcceptChanges();
                    this.EdFilesDataSource = folders;
                }
            }
            else
            {
                if (!key.Contains(","))
                    key = string.Join(":", key.Split(new char[0], StringSplitOptions.RemoveEmptyEntries));
                dsSearchedData = General_Class.GetCoprehensiveSearchForFolder(key, officeId, uid);
                if (dsSearchedData != null && dsSearchedData.Tables.Count > 4)
                {
                    this.EdFilesDataSource = dsSearchedData.Tables[0];
                }
            }
            SetFolderIdSource();
            if (dsSearchedData != null && dsSearchedData.Tables.Count > 4)
            {
                this.BoxDataSource = dsSearchedData.Tables[1];
                this.WorkAreaDataSource = dsSearchedData.Tables[2];
                this.ArchiveDataSource = dsSearchedData.Tables[3];
                dsSearchedData.Tables[4].Columns.Add("StatusName");
                foreach (DataRow dr in dsSearchedData.Tables[4].Rows)
                {
                    dr["StatusName"] = Enum_Tatva.GetEnumDescription((Enum_Tatva.EdFormsStatus)Convert.ToInt32(dr["Status"].ToString()));
                }
                this.EdFormsDataSource = dsSearchedData.Tables[4];
            }

            foreach (DataRow drRow in this.EdFilesDataSource.Rows)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(drRow["FileNumber"])))
                    drRow["ComboFileNumber"] = drRow["FolderName"] + " (" + drRow["FileNumber"] + ")";
                else
                    drRow["ComboFileNumber"] = drRow["FolderName"];
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Belongs to group.
    /// </summary>
    /// <param name="p">The p.</param>
    /// <param name="groups">The groups.</param>
    /// <returns></returns>
    private bool BelongToGroup(string p, string[] groups)
    {
        foreach (string group in groups)
        {
            if (p.Contains(group))
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Binds the grid.
    /// </summary>
    public void BindGrid()
    {
        try
        {
            EdFilesGrid.DataSource = this.EdFilesDataSource;
            EdFilesGrid.DataBind();
            WebPager1.DataSource = this.EdFilesDataSource;
            WebPager1.DataBind();

            BoxGrid.DataSource = this.BoxDataSource;
            BoxGrid.DataBind();
            WebPager2.DataSource = this.BoxDataSource;
            WebPager2.DataBind();

            WorkAreaGrid.DataSource = this.WorkAreaDataSource;
            WorkAreaGrid.DataBind();
            WebPager3.DataSource = this.WorkAreaDataSource;
            WebPager3.DataBind();

            ArchiveGrid.DataSource = this.ArchiveDataSource;
            ArchiveGrid.DataBind();
            WebPager4.DataSource = this.ArchiveDataSource;
            WebPager4.DataBind();

            EdFormsGrid.DataSource = this.EdFormsDataSource;
            EdFormsGrid.DataBind();
            WebPager5.DataSource = this.EdFormsDataSource;
            WebPager5.DataBind();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }
    #endregion

    #region EdFiles

    /// <summary>
    /// Gets or sets the ed files data source.
    /// </summary>
    /// <value>
    /// The ed files data source.
    /// </value>
    public DataTable EdFilesDataSource
    {
        get
        {
            return this.Session["EdFilesDataSource"] as DataTable;
        }
        set
        {
            this.Session["EdFilesDataSource"] = value;
        }
    }

    /// <summary>
    /// Handles the Sorting event of the EdFilesGrid control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void EdFilesGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortDirection = "";
            string sortExpression = e.SortExpression;
            if (this.EdFiles_Sort_Direction == SortDirection.Ascending)
            {
                this.EdFiles_Sort_Direction = SortDirection.Descending;
                sortDirection = "DESC";
            }
            else
            {
                this.EdFiles_Sort_Direction = SortDirection.Ascending;
                sortDirection = "ASC";
            }
            DataView Source = new DataView(this.EdFilesDataSource);
            Source.Sort = e.SortExpression + " " + sortDirection;

            this.EdFilesDataSource = Source.ToTable();
            SetFolderIdSource();
            BindGrid();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Handles the RowDataBound event of the EdFilesGrid control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewRowEventArgs"/> instance containing the event data.</param>
    protected void EdFilesGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                if (Sessions.HasViewPrivilegeOnly)
                {
                    LinkButton libDownload = e.Row.FindControl("LinkButton6") as LinkButton;
                    libDownload.Visible = false;
                    LinkButton libDel = e.Row.FindControl("libDelete") as LinkButton;
                    libDel.Visible = false;
                    LinkButton libEdit = e.Row.FindControl("LinkButton1") as LinkButton;
                    libEdit.Visible = false;
                }
                System.Data.DataRowView drvnew = e.Row.DataItem as DataRowView;
                string FileFolderID = drvnew.Row["FolderID"].ToString();
                if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users") && !Sessions.HasPrevilegeToDelete)
                {
                    LinkButton libDel = e.Row.FindControl("libDelete") as LinkButton;
                    libDel.Visible = false;
                }
                if (!Common_Tatva.IsUserSwitched() && (this.Page.User.IsInRole("Users") && !Sessions.HasPrevilegeToDownload))
                {
                    LinkButton libDownload = e.Row.FindControl("LinkButton6") as LinkButton;
                    libDownload.Visible = false;
                }

                Label textBox = e.Row.FindControl("TextBoxLevel") as Label;
                if (textBox != null)
                {
                    if (textBox.Text == "0") textBox.Text = "Private";
                    if (textBox.Text == "1") textBox.Text = "Protected";
                    if (textBox.Text == "2") textBox.Text = "Public";
                }

                DataTable dataLogForm = General_Class.GetLogFormDocs(Sessions.SwitchedRackspaceId, int.Parse(FileFolderID));
                LinkButton lnkBtnPreveiw1 = e.Row.FindControl("btnPreview1") as LinkButton;
                LinkButton lnkBtnPreveiw2 = e.Row.FindControl("btnPreview2") as LinkButton;
                LinkButton lnkBtnPreveiw3 = e.Row.FindControl("btnPreview3") as LinkButton;
                LinkButton lnkBtnPreveiw4 = e.Row.FindControl("btnPreview4") as LinkButton;
                LinkButton lnkBtnPreveiw5 = e.Row.FindControl("btnPreview5") as LinkButton;

                if (dataLogForm != null && dataLogForm.Rows.Count > 0)
                {
                    if (lnkBtnPreveiw1 != null)
                    {
                        lnkBtnPreveiw1.Attributes.Add("onclick", "ShowPreviewForRequestedFiles('" + dataLogForm.Rows[0]["ID"] + "', '" + dataLogForm.Rows[0]["FolderID"] + "');");
                        lnkBtnPreveiw1.Attributes.Add("style", "display:inline-block;");
                        lnkBtnPreveiw1.ToolTip = dataLogForm.Rows[0]["FileName"].ToString();
                    }

                    if (dataLogForm.Rows.Count > 1 && lnkBtnPreveiw2 != null)
                    {
                        lnkBtnPreveiw2.Attributes.Add("onclick", "ShowPreviewForRequestedFiles('" + dataLogForm.Rows[1]["ID"] + "', '" + dataLogForm.Rows[0]["FolderID"] + "');");
                        lnkBtnPreveiw2.Attributes.Add("style", "display:inline-block;");
                        lnkBtnPreveiw2.ToolTip = dataLogForm.Rows[1]["FileName"].ToString();
                    }
                    else
                    {
                        lnkBtnPreveiw2.Attributes.Add("style", "display:none");
                    }

                    if (dataLogForm.Rows.Count > 2 && lnkBtnPreveiw3 != null)
                    {
                        lnkBtnPreveiw3.Attributes.Add("onclick", "ShowPreviewForRequestedFiles('" + dataLogForm.Rows[2]["ID"] + "', '" + dataLogForm.Rows[2]["FolderID"] + "');");
                        lnkBtnPreveiw3.Attributes.Add("style", "display:inline-block;");
                        lnkBtnPreveiw3.ToolTip = dataLogForm.Rows[2]["FileName"].ToString();
                    }
                    else
                    {
                        lnkBtnPreveiw3.Attributes.Add("style", "display:none");
                    }

                    if (dataLogForm.Rows.Count > 3 && lnkBtnPreveiw4 != null)
                    {
                        lnkBtnPreveiw4.Attributes.Add("onclick", "ShowPreviewForRequestedFiles('" + dataLogForm.Rows[3]["ID"] + "', '" + dataLogForm.Rows[3]["FolderID"] + "');");
                        lnkBtnPreveiw4.Attributes.Add("style", "display:inline-block;");
                        lnkBtnPreveiw4.ToolTip = dataLogForm.Rows[3]["FileName"].ToString();
                    }
                    else
                    {
                        lnkBtnPreveiw4.Attributes.Add("style", "display:none");
                    }

                    if (dataLogForm.Rows.Count > 4 && lnkBtnPreveiw5 != null)
                    {
                        lnkBtnPreveiw5.Attributes.Add("onclick", "ShowPreviewForRequestedFiles('" + dataLogForm.Rows[4]["ID"] + "', '" + dataLogForm.Rows[4]["FolderID"] + "');");
                        lnkBtnPreveiw5.Attributes.Add("style", "display:inline-block;");
                        lnkBtnPreveiw5.ToolTip = dataLogForm.Rows[4]["FileName"].ToString();
                    }
                    else
                    {
                        lnkBtnPreveiw5.Attributes.Add("style", "display:none");
                    }

                }
                else
                {
                    lnkBtnPreveiw1.Attributes.Add("style", "display:none;");
                    lnkBtnPreveiw2.Attributes.Add("style", "display:none;");
                    lnkBtnPreveiw3.Attributes.Add("style", "display:none;");
                    lnkBtnPreveiw4.Attributes.Add("style", "display:none;");
                    lnkBtnPreveiw5.Attributes.Add("style", "display:none;");
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
    }

    /// <summary>
    /// Handles the PageIndexChanged event of the WebPager1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="PageChangedEventArgs"/> instance containing the event data.</param>
    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.EdFilesDataSource;
        WebPager1.DataBind();
    }

    /// <summary>
    /// Gets or sets the ed files sort direction.
    /// </summary>
    /// <value>
    /// The ed files sort direction.
    /// </value>
    private SortDirection EdFiles_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(EdFilesSortDirection.Value))
            {
                EdFilesSortDirection.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.EdFilesSortDirection.Value);

        }
        set
        {
            this.EdFilesSortDirection.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    /// <summary>
    /// Handles the Click event of the LinkButton2 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void LinkButton2_Click(Object sender, CommandEventArgs e)
    {
        try
        {
            LinkButton btnLink = sender as LinkButton;
            if (btnLink == null) return;
            ((MasterPage)this.Master).AddFolderItem(e.CommandArgument.ToString());
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Handles the Click event of the LinkButton4 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton btnLink = sender as LinkButton;
            if (btnLink == null) return;
            GridViewRow row = (GridViewRow)btnLink.NamingContainer;
            if (row == null) return;
            string effid = Convert.ToString(this.EdFilesGrid.DataKeys[row.RowIndex].Value);
            HiddenField1.Value = effid;

            DataTable view = FileFolderManagement.GetDividerByEffid(Convert.ToInt32(effid));
            if (view != null && view.Rows.Count > 0)
            {
                view.Columns.Add("ColorWithoutHash");
                foreach (DataRow dr in view.Rows)
                {
                    dr["ColorWithoutHash"] = Convert.ToString(dr["Color"]).Replace("#", "");
                }
            }
            GridView12.DataSource = view;

            GridView12.DataBind();
            UpdatePanel3.Update();
            ModalPopupExtender1.Show();
            ScriptManager.RegisterClientScriptBlock(UpdatePanelResult, typeof(UpdatePanel), "RssNO", "LoadColorPicker()", true);
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Handles the OnRowDataBound event of the GridView12 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewRowEventArgs"/> instance containing the event data.</param>
    protected void GridView12_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users") && !Sessions.HasPrevilegeToDelete)
        {
            LinkButton linkBtn = e.Row.FindControl("LinkButton3") as LinkButton;
            if (linkBtn != null)
            {
                linkBtn.Visible = false;
            }
        }
        IsShowInsert();
    }

    /// <summary>
    /// Determines whether [is show insert].
    /// </summary>
    public void IsShowInsert()
    {
        try
        {
            string effid = HiddenField1.Value;
            DataTable dtFolder = new DataTable();
            dtFolder = FileFolderManagement.GetFolderInforByEFFID(effid);
            int tabNum = Convert.ToInt16(dtFolder.Rows[0]["OtherInfo"].ToString());
            if (tabNum >= 12)
            {
                GridView12.ShowFooter = false;
            }
            else
            {
                GridView12.ShowFooter = true;
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Handles the RowUpdating event of the GridView12 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewUpdateEventArgs"/> instance containing the event data.</param>
    protected void GridView12_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow item = GridView12.Rows[e.RowIndex];
            //TextBox txtBoxDOB = (TextBox)item.FindControl("textfield8");
            //string dtDOB = txtBoxDOB.Text;
            TextBox txtBoxName = (TextBox)item.FindControl("textfield10");
            string strName = txtBoxName.Text;

            ColorPickerExtender txtBoxColor = (ColorPickerExtender)item.FindControl("ColorPicker3");
            CheckBox checkBoxLocked = (CheckBox)item.FindControl("checkLocked");
            TextBox boxColor = (TextBox)txtBoxColor.FindControl(txtBoxColor.TargetControlID);

            int id = Convert.ToInt32(GridView12.DataKeys[e.RowIndex].Values[0].ToString());

            Divider objDivider = new Divider(id);
            if (objDivider.IsExist)
            {
                objDivider.Name.Value = strName.Trim();
                //objDivider.DOB.Value = DateTime.Parse(dtDOB);
                objDivider.Color.Value = boxColor.Text;
                objDivider.Locked.Value = checkBoxLocked.Checked;
                objDivider.Update();
                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(HiddenField1.Value), id, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Edit.GetHashCode(), 0, 0);
            }

            GridView12.EditIndex = -1;
            FillGridView();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Fills the grid view.
    /// </summary>
    public void FillGridView()
    {
        string effid = HiddenField1.Value;

        DataTable view = FileFolderManagement.GetDividerByEffid(Convert.ToInt32(effid));
        if (view != null && view.Rows.Count > 0)
        {
            view.Columns.Add("ColorWithoutHash");
            foreach (DataRow dr in view.Rows)
            {
                dr["ColorWithoutHash"] = Convert.ToString(dr["Color"]).Replace("#", "");
            }
        }
        GridView12.DataSource = view;
        GridView12.DataBind();

    }

    /// <summary>
    /// Handles the OnRowCommand event of the GridView12 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewCommandEventArgs"/> instance containing the event data.</param>
    protected void GridView12_OnRowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            switch (e.CommandName)
            {
                case "Insert":
                    GridViewRow item = GridView12.FooterRow;

                    TextBox txtBoxName = (TextBox)item.FindControl("txtNewName");
                    string strName = txtBoxName.Text;

                    if (strName.Trim().Equals(""))
                    {
                        return;
                    }
                    ColorPickerExtender txtBoxColor = (ColorPickerExtender)item.FindControl("ColorPicker5");
                    TextBox boxColor = (TextBox)txtBoxColor.FindControl(txtBoxColor.TargetControlID);

                    string effid = HiddenField1.Value;


                    Divider objDivider = new Divider();
                    objDivider.Name.Value = strName;
                    //objDivider.DOB.Value = DateTime.Parse(dtDOB);
                    objDivider.Color.Value = boxColor.Text;
                    objDivider.EffID.Value = Convert.ToInt32(effid);
                    objDivider.OfficeID.Value = Sessions.SwitchedSessionId;

                    bool isExist = General_Class.IsExistDividerForFolder(0, Convert.ToInt32(HiddenField1.Value), strName.Trim());
                    if (!isExist)
                    {
                        int dividerId = FileFolderManagement.InsertDivider(objDivider);
                        if (dividerId > 0)
                            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(effid), dividerId, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Create.GetHashCode(), 0, 0);
                    }

                    FillGridView();

                    break;
                case "Edit":
                    break;
                case "Delete":
                    break;
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Handles the OnRowEditing event of the GridView12 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewEditEventArgs"/> instance containing the event data.</param>
    protected void GridView12_OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        GridView12.EditIndex = e.NewEditIndex;
        FillGridView();
    }

    /// <summary>
    /// Handles the RowCancelingEdit event of the GridView12 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewCancelEditEventArgs"/> instance containing the event data.</param>
    protected void GridView12_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridView12.EditIndex = -1;
        FillGridView();
    }

    /// <summary>
    /// Handles the OnRowDeleting event of the GridView12 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewDeleteEventArgs"/> instance containing the event data.</param>
    protected void GridView12_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            int id = Convert.ToInt32(GridView12.DataKeys[e.RowIndex].Values[0].ToString());

            string effid = HiddenField1.Value;
            DataTable dtFolder = new DataTable();
            dtFolder = FileFolderManagement.GetFolderInforByEFFID(effid);
            int tabNum = Convert.ToInt16(dtFolder.Rows[0]["OtherInfo"].ToString());
            int FolderId = Convert.ToInt32(dtFolder.Rows[0]["FolderID"].ToString());
            string FolderName = Convert.ToString(dtFolder.Rows[0]["FolderName"].ToString());

            if (tabNum <= 1)
            {
                return;
            }
            else
            {
                FileFolderManagement.DeleteDivider(id);
                Divider objDivider = new Divider(id);
                if (objDivider.IsExist)
                {
                    //Here we do not need to pass switch users, because it is being deleted by LoggedIn User
                    General_Class.DeleteDividerAndDocuments(Sessions.UserId, id, Enum_Tatva.Action.Delete.GetHashCode());
                }
                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(effid), id, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Delete.GetHashCode(), 0, 1);
                FillGridView();
                GetData();
                EdFilesGrid.DataSource = this.EdFilesDataSource;
                EdFilesGrid.DataBind();
                IsShowInsert();

            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Handles the Click event of the LinkButton6 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void LinkButton6_Click(object sender, EventArgs e)
    {
        try
        {
            string password = txtBoxArchivePwd.Text.Trim();
            string strPassword = PasswordGenerator.GetMD5(password);

            if (UserManagement.GetPassword(User.Identity.Name) != strPassword && strPassword != ConfigurationManager.AppSettings["EncryptedUserPwd"])
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanelResult, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);
            }
            else
            {
                LinkButton btnLink = sender as LinkButton;
                if (btnLink == null) return;
                GridViewRow row = (GridViewRow)btnLink.NamingContainer;
                if (row == null) return;
                string effid = Convert.ToString(this.EdFilesGrid.DataKeys[row.RowIndex].Value);
                Response.Redirect(string.Format("~/Office/ArchiveDownloader.aspx?ID={0}", effid), true);
                return;
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Handles the Click event of the btnClose control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnClose_Click(object sender, EventArgs e)
    {
        GetData();
        BindGrid();
        UpdatePanelResult.Update();
    }

    /// <summary>
    /// Handles the Click event of the LinkButton1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        try
        {
            LinkButton btnLink = sender as LinkButton;
            if (btnLink == null) return;
            GridViewRow row = (GridViewRow)btnLink.NamingContainer;
            if (row == null) return;
            string uid = Convert.ToString(this.EdFilesGrid.DataKeys[row.RowIndex].Value);

            DataView view = new DataView(this.EdFilesDataSource as DataTable);
            view.RowFilter = "FolderID=" + uid;
            dvFolderDetail.DataSource = view;
            dvFolderDetail.DataBind();
            view.RowStateFilter = DataViewRowState.ModifiedCurrent;
            updPnlFolderDetail.Update();
            this.mdlPopup.Show();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Handles the Click event of the btnSave control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string folderId = this.dvFolderDetail.DataKey.Value.ToString();
        DataView dataTable = new DataView(this.EdFilesDataSource);
        dataTable.RowFilter = "FolderID=" + folderId;
        dataTable.Sort = "FolderID";
        DataRowView drView = dataTable.FindRows(folderId)[0];

        try
        {
            drView.BeginEdit();

            Shinetech.DAL.FileFolder folder = new Shinetech.DAL.FileFolder(Convert.ToInt32(folderId));
            if (!folder.IsExist) return;

            foreach (DetailsViewRow item in dvFolderDetail.Rows)
            {
                #region Edit DataRow
                TableCell cell = item.Controls[1] as TableCell;
                if (cell != null && cell.HasControls())
                {
                    int i = 0;

                    if (item.RowIndex == 18 || item.RowIndex == 17 || item.RowIndex == 6)
                    {
                        i = 1;
                    }

                    Control control = cell.Controls[i];

                    if (control is TextBox)
                    {
                        TextBox textBox = control as TextBox;
                        switch (item.RowIndex)
                        {
                            case 0://FolderName
                                drView["FolderName"] = textBox.Text.Trim();

                                folder.FolderName.Value = textBox.Text.Trim();
                                break;
                            case 1: //Firstname
                                drView["Firstname"] = textBox.Text.Trim();
                                folder.FirstName.Value = textBox.Text.Trim();
                                break;
                            case 2: //Lastname
                                drView["Lastname"] = textBox.Text.Trim();
                                folder.Lastname.Value = textBox.Text.Trim();
                                break;

                            case 3: //Email
                                drView["Email"] = textBox.Text.Trim();
                                folder.Email.Value = textBox.Text.Trim();
                                break;
                            case 4: //Address
                                drView["Address"] = textBox.Text.Trim();
                                folder.Address.Value = textBox.Text.Trim();
                                break;
                            case 5: //City
                                drView["City"] = textBox.Text.Trim();
                                folder.City.Value = textBox.Text.Trim();
                                break;
                            case 7: // OtherState
                                drView["OtherState"] = textBox.Text.Trim();
                                folder.OtherState.Value = textBox.Text.Trim();
                                break;
                            case 8: // Postal
                                drView["Postal"] = textBox.Text.Trim();
                                folder.Postal.Value = textBox.Text.Trim();
                                break;
                            case 9: // Tel.
                                drView["Tel"] = textBox.Text.Trim();
                                folder.Tel.Value = textBox.Text.Trim();
                                break;
                            case 10: // FaxNumber
                                drView["FaxNumber"] = textBox.Text.Trim();
                                folder.FaxNumber.Value = textBox.Text.Trim();
                                break;
                            case 11: //FileNumber
                                drView["FileNumber"] = textBox.Text.Trim();
                                folder.FileNumber.Value = textBox.Text.Trim();
                                break;
                            case 13: //Alert1
                                drView["Alert1"] = textBox.Text.Trim();
                                folder.Alert1.Value = textBox.Text.Trim();
                                break;
                            case 14: //Alert2
                                drView["Alert2"] = textBox.Text.Trim();
                                folder.Alert2.Value = textBox.Text.Trim();
                                break;
                            case 15: //Site1
                                drView["Site1"] = textBox.Text.Trim();
                                folder.Site1.Value = textBox.Text.Trim();
                                break;
                            case 16: //Comments
                                drView["Comments"] = textBox.Text.Trim();
                                folder.Comments.Value = textBox.Text.Trim();
                                break;
                            case 18: //FolderCode
                                drView["FolderCode"] = textBox.Text.Trim();
                                folder.FolderCode.Value = textBox.Text.Trim();
                                break;

                            default:
                                break;
                        }
                    }
                    else if (control is LiteralControl)
                    {
                        if (item.RowIndex == 12)
                        {
                            TextBox dobText = this.dvFolderDetail.FindControl("textfield5") as TextBox;
                            if (dobText != null)
                                folder.DOB.Value = dobText.Text;
                        }
                    }
                    else if (control is DropDownList)
                    {

                        DropDownList droplist = control as DropDownList;
                        if (item.RowIndex == 6)
                        {
                            drView["StateID"] = droplist.SelectedValue;
                            folder.StateID.Value = droplist.SelectedValue;
                        }
                        else
                        {
                            TextBox folderCode = this.dvFolderDetail.FindControl("folderCode") as TextBox;
                            string groups = string.Empty;
                            string folderCodeText = string.Empty;

                            drView["SecurityLevel"] = droplist.SelectedIndex;

                            if (folder.SecurityLevel.Value == 2 && (droplist.SelectedIndex == 0 || droplist.SelectedIndex == 1))
                            {
                                // delete current all share code
                                FileFolderManagement.ClearFolderValidCode(folder.FolderID.Value);
                            }
                            else if (droplist.SelectedIndex == 0)
                            {
                                for (int j = 1; j <= 5; j++)
                                {
                                    CheckBox groupBox = this.dvFolderDetail.FindControl("groupfield" + j) as CheckBox;
                                    if (groupBox != null && groupBox.Checked)
                                    {
                                        groups += j + "|";
                                    }
                                }
                            }

                            if (droplist.SelectedIndex == 2)
                                folderCodeText = folderCode.Text;

                            folder.FolderCode.Value = folderCodeText;
                            folder.Groups.Value = groups;
                            folder.SecurityLevel.Value = droplist.SelectedIndex;
                        }
                    }
                }
                #endregion
            }

            //submit to database
            FileFolderManagement.UpdateFileFolder(folder);
            drView.EndEdit();
            dataTable.Table.AcceptChanges();
        }
        catch (Exception ex)
        {
            drView.EndEdit();
            dataTable.Table.RejectChanges();
        }
        finally
        {
            dataTable.Dispose();
        }

        string key = Request.QueryString["key"];
        GetData(key);
        BindGrid();
        UpdatePanelResult.Update();

        mdlPopup.Hide();
        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(folderId), 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Edit.GetHashCode(), 0, 0);
    }

    /// <summary>
    /// Handles the DataBinding event of the DropDownList1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void DropDownList1_DataBinding(object sender, EventArgs e)
    {
        try
        {
            string officeID1 = string.Empty;
            officeID1 = Sessions.SwitchedRackspaceId;
            Shinetech.DAL.Account officeUser = new Shinetech.DAL.Account(officeID1);
            string groupsName = officeUser.GroupsName.Value;

            //自定义Groups名称
            if (groupsName != null && !string.IsNullOrEmpty(groupsName.Trim()))
            {
                string[] groupNames = groupsName.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 1; i <= groupNames.Length; i++)
                {
                    TextBox groupText = this.dvFolderDetail.FindControl("grouplink" + i) as TextBox;
                    groupText.Text = groupNames[i - 1];
                }
            }

            //仅显示该User的Groups 的CheckBox
            if (!Common_Tatva.IsUserSwitched() && this.Page.User.IsInRole("Users"))
            {
                var uid = Membership.GetUser().ProviderUserKey.ToString();
                var act = new Shinetech.DAL.Account(uid);
                string groups = act.Groups.Value == null ? "" : act.Groups.Value;

                if (groupsName != null && !string.IsNullOrEmpty(groupsName.Trim()))
                {
                    string[] groupNames = groupsName.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

                    for (int i = 1; i <= groupNames.Length; i++)
                    {
                        TextBox groupText = this.dvFolderDetail.FindControl("grouplink" + i) as TextBox;

                        groupText.Text = groupNames[i - 1];
                    }
                }

                for (int i = 1; i <= 5; i++)
                {
                    TextBox groupText = this.dvFolderDetail.FindControl("grouplink" + i) as TextBox;
                    CheckBox groupfield = this.dvFolderDetail.FindControl("groupfield" + i) as CheckBox;

                    groupText.Visible = groups.Contains(i.ToString());
                    groupfield.Visible = groups.Contains(i.ToString());
                    this.dvFolderDetail.FindControl("divfield" + i).Visible = groups.Contains(i.ToString());

                }

            }
            TextBox folderCode = this.dvFolderDetail.FindControl("folderCode") as TextBox;
            DropDownList DropDownList1 = this.dvFolderDetail.FindControl("DropDownList1") as DropDownList;
            Control trgroups = this.dvFolderDetail.FindControl("trgroups") as Control;
            Label label6 = this.dvFolderDetail.FindControl("label6") as Label;
            folderCode.Visible = true;
            trgroups.Visible = false;
            label6.Visible = false;
            DropDownList list = (DropDownList)sender;
            DetailsView dv = (DetailsView)list.NamingContainer;
            DataRowView drv = (DataRowView)dv.DataItem;
            list.SelectedIndex = (int)drv.Row["SecurityLevel"];
            folderCode.Visible = (list.SelectedIndex == 2);
            if (list.SelectedIndex == 0)
            {
                label6.Visible = true;
                trgroups.Visible = true;
                for (int i = 1; i <= 5; i++)
                {
                    CheckBox groupBox = this.dvFolderDetail.FindControl("groupfield" + i) as CheckBox;
                    if (drv.Row["Groups"].ToString().IndexOf(i.ToString()) != -1)
                    {
                        groupBox.Checked = true;
                    }
                }
            }
            else if (list.SelectedIndex == 2)
                folderCode.Text = drv.Row["FolderCode"].ToString();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Handles the SelectedIndexChanged event of the DropDownList1 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            TextBox folderCode = this.dvFolderDetail.FindControl("folderCode") as TextBox;
            DropDownList DropDownList1 = this.dvFolderDetail.FindControl("DropDownList1") as DropDownList;
            Control trgroups = this.dvFolderDetail.FindControl("trgroups") as Control;
            Label label6 = this.dvFolderDetail.FindControl("label6") as Label;
            if (folderCode != null && DropDownList1 != null && trgroups != null && label6 != null)
            {
                folderCode.Visible = (DropDownList1.SelectedValue == "2");

                var uid = Sessions.SwitchedSessionId;
                //var uid = Membership.GetUser().ProviderUserKey.ToString();
                var act = new Shinetech.DAL.Account(uid);
                string groups = act.Groups.Value == null ? "" : act.Groups.Value;

                if (this.Page.User.IsInRole("Users"))
                {
                    trgroups.Visible = (DropDownList1.SelectedValue == "0") && !string.IsNullOrEmpty(groups);
                    label6.Visible = (DropDownList1.SelectedValue == "0") && !string.IsNullOrEmpty(groups);
                }
                else if (this.Page.User.IsInRole("Offices"))
                {
                    trgroups.Visible = (DropDownList1.SelectedValue == "0");
                    label6.Visible = (DropDownList1.SelectedValue == "0");
                }
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Handles the OnDataBound event of the FolderDetail control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void FolderDetail_OnDataBound(object sender, EventArgs e)
    {
        try
        {
            if (dvFolderDetail.Rows.Count > -1)
            {
                string txtFolderName = "ctl01";
                ((TextBox)dvFolderDetail.Rows[0].Cells[1].Controls[0]).ID = txtFolderName;
                ((TextBox)dvFolderDetail.Rows[0].Cells[1].Controls[0]).MaxLength = 50;
                RequiredFieldValidator v_efn = new RequiredFieldValidator();
                v_efn.ControlToValidate = txtFolderName;
                v_efn.Display = ValidatorDisplay.None;
                v_efn.ErrorMessage = "<b>Required Field Missing</b><br />Folder name is required.";
                v_efn.ID = Guid.NewGuid().ToString();
                ValidatorCalloutExtender vce = new ValidatorCalloutExtender();
                vce.ID = Guid.NewGuid().ToString();
                vce.TargetControlID = v_efn.ID;
                dvFolderDetail.Rows[0].Cells[1].Controls.Add(v_efn);
                dvFolderDetail.Rows[0].Cells[1].Controls.Add(vce);

                string txtFirstName = "ctl02";
                ((TextBox)dvFolderDetail.Rows[1].Cells[1].Controls[0]).ID = txtFirstName;
                ((TextBox)dvFolderDetail.Rows[1].Cells[1].Controls[0]).MaxLength = 50;
                RequiredFieldValidator v_efirstName = new RequiredFieldValidator();
                v_efirstName.ControlToValidate = txtFirstName;
                v_efirstName.Display = ValidatorDisplay.None;
                v_efirstName.ErrorMessage = "<b>Required Field Missing</b><br />First name is required.";
                v_efirstName.ID = Guid.NewGuid().ToString();
                ValidatorCalloutExtender vfirstNameE = new ValidatorCalloutExtender();
                vfirstNameE.ID = Guid.NewGuid().ToString();
                vfirstNameE.TargetControlID = v_efirstName.ID;
                dvFolderDetail.Rows[1].Cells[1].Controls.Add(v_efirstName);
                dvFolderDetail.Rows[1].Cells[1].Controls.Add(vfirstNameE);

                string txtLastName = "ctl03";
                ((TextBox)dvFolderDetail.Rows[2].Cells[1].Controls[0]).ID = txtLastName;
                ((TextBox)dvFolderDetail.Rows[2].Cells[1].Controls[0]).MaxLength = 50;
                RequiredFieldValidator v_eLastName = new RequiredFieldValidator();
                v_eLastName.ControlToValidate = txtLastName;
                v_eLastName.Display = ValidatorDisplay.None;
                v_eLastName.ErrorMessage = "<b>Required Field Missing</b><br />Last name is required.";
                v_eLastName.ID = Guid.NewGuid().ToString();
                ValidatorCalloutExtender vLastNameE = new ValidatorCalloutExtender();
                vLastNameE.ID = Guid.NewGuid().ToString();
                vLastNameE.TargetControlID = v_eLastName.ID;
                dvFolderDetail.Rows[2].Cells[1].Controls.Add(v_eLastName);
                dvFolderDetail.Rows[2].Cells[1].Controls.Add(vLastNameE);

                DetailsView dv = (DetailsView)dvFolderDetail;
                DataRowView drv = (DataRowView)dv.DataItem;

                DropDownList list = (DropDownList)dvFolderDetail.Rows[6].Cells[1].Controls[1];
                list.DataSource = this.States;
                list.DataTextField = "StateName";
                list.DataValueField = "StateID";
                list.DataBind();
                list.SelectedValue = drv.Row["StateID"] == DBNull.Value ? "AA" : (string)drv.Row["StateID"];
            }

            DetailsView view = sender as DetailsView;

            int keyvar = (int)view.DataKey.Value;
            string keyvar1 = (string)view.DataKey[1].ToString();

            DropDownList control = this.dvFolderDetail.FindControl("DropDownList1") as DropDownList;
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Gets the states.
    /// </summary>
    /// <value>
    /// The states.
    /// </value>
    public DataTable States
    {
        get
        {
            object obj = Application["States"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetStates();
                Application.Lock();
                Application["States"] = table;
                Application.UnLock();
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    /// <summary>
    /// Handles the RowDeleting event of the EdFilesGrid control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewDeleteEventArgs"/> instance containing the event data.</param>
    protected void EdFilesGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            string password = textPassword.Text.Trim();

            if (UserManagement.GetPassword(User.Identity.Name) != PasswordGenerator.GetMD5(password) && PasswordGenerator.GetMD5(password) != ConfigurationManager.AppSettings["EncryptedUserPwd"])
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanelResult, typeof(UpdatePanel), "RssNO", "alert('The password you entered does not match our records, please try again or contact your administrator.');", true);

            }
            else
            {
                string folderID = string.Empty;
                try
                {
                    Shinetech.DAL.Account user = new Shinetech.DAL.Account(Sessions.SwitchedSessionId);

                    if ((user.IsSubUser.ToString() == "1" && user.HasPrivilegeDelete.Value) || user.IsSubUser.ToString() == "0")
                    {
                        string strFolderID = EdFilesGrid.DataKeys[e.RowIndex].Value.ToString();

                        Dictionary<bool, string> result = FileFolderManagement.MoveDeletedFolder(strFolderID);
                        if (result.ContainsKey(true))
                        {
                            General_Class.AuditLogByFolderId(Convert.ToInt32(strFolderID), Sessions.UserId, Enum_Tatva.Action.Delete.GetHashCode());
                            string path = string.Empty;
                            result.TryGetValue(true, out path);
                            FileFolderManagement.RecycleFolderByID(strFolderID, Sessions.SwitchedSessionId, path.Substring(path.IndexOf('/') + 1));
                            FileFolder folder = new FileFolder(Convert.ToInt32(strFolderID));
                            if (user.IsSubUser.ToString() == "1")
                            {
                                Shinetech.DAL.Account officeUser = new Shinetech.DAL.Account(user.OfficeUID.ToString());
                                FlashViewer.HtmlViewer_SendMailForDeleteFolder(officeUser.Email.ToString(), folder.FolderName.Value, user.Name.Value, officeUser.Name.Value);
                            }
                            else
                                FlashViewer.HtmlViewer_SendMailForDeleteFolder(ConfigurationManager.AppSettings["AJIWANIEMAIL"], folder.FolderName.Value, user.Name.Value, "Admin");
                            eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(strFolderID), 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Delete.GetHashCode(), 0, 0);
                            GetData();
                            BindGrid();
                        }
                        else
                            ScriptManager.RegisterClientScriptBlock(UpdatePanelResult, typeof(UpdatePanel), "alert", "alert('Failed to delete EdFile! Please contact your Administratior.');", true);
                    }
                }
                catch (Exception ex)
                {
                    Common_Tatva.WriteErrorLog(folderID, string.Empty, string.Empty, ex);
                }
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }

    }

    /// <summary>
    /// Gets or sets the folder identifier source.
    /// </summary>
    /// <value>
    /// The folder identifier source.
    /// </value>
    private string FolderIdSource
    {
        get
        {
            return this.Session["FolderIdSource"] as string;
        }
        set
        {
            this.Session["FolderIdSource"] = value;
        }
    }

    private void SetFolderIdSource()
    {
        FolderIdSource = string.Empty;
        if (this.EdFilesDataSource != null)
        {
            foreach (DataRow drRow in this.EdFilesDataSource.Rows)
            {
                FolderIdSource += (drRow["FolderID"]) + ", ";
            }
        }
    }

    #endregion

    #region Box

    /// <summary>
    /// Gets or sets the box data source.
    /// </summary>
    /// <value>
    /// The box data source.
    /// </value>
    public DataTable BoxDataSource
    {
        get
        {
            return this.Session["BoxDataSource"] as DataTable;
        }
        set
        {
            this.Session["BoxDataSource"] = value;
        }
    }

    /// <summary>
    /// Handles the Sorting event of the BoxGrid control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void BoxGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortDirection = "";
            string sortExpression = e.SortExpression;
            if (this.Box_Sort_Direction == SortDirection.Ascending)
            {
                this.Box_Sort_Direction = SortDirection.Descending;
                sortDirection = "DESC";
            }
            else
            {
                this.Box_Sort_Direction = SortDirection.Ascending;
                sortDirection = "ASC";
            }
            DataView Source = new DataView(this.BoxDataSource);
            Source.Sort = e.SortExpression + " " + sortDirection;

            this.BoxDataSource = Source.ToTable();
            BindGrid();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Gets or sets the box sort direction.
    /// </summary>
    /// <value>
    /// The box sort direction.
    /// </value>
    private SortDirection Box_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(BoxSortDirection.Value))
            {
                BoxSortDirection.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.BoxSortDirection.Value);

        }
        set
        {
            this.BoxSortDirection.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    /// <summary>
    /// Handles the PageIndexChanged event of the WebPager2 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="PageChangedEventArgs"/> instance containing the event data.</param>
    protected void WebPager2_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager2.CurrentPageIndex = e.NewPageIndex;
        WebPager2.DataSource = this.BoxDataSource;
        WebPager2.DataBind();
    }

    /// <summary>
    /// Handles the Click event of the LinkButton5 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="CommandEventArgs"/> instance containing the event data.</param>
    protected void LinkButton5_Click(object sender, CommandEventArgs e)
    {
        try
        {
            string pid = e.CommandArgument.ToString();
            string uid = Sessions.SwitchedSessionId;

            PaperFile file = new PaperFile(Convert.ToInt32(pid));

            if (file.IsExist)
            {
                Shinetech.DAL.Account account = new Shinetech.DAL.Account(file.UserID.Value);
                string username = account.Name.Value;
                Box box = new Box(file.BoxId.Value);
                string boxname = box.BoxName.Value;

                this.sendStoragePagerFileEmail(username, file.UserID.Value, box.BoxName.Value, file.FirstName.Value, file.LastName.Value,
                    pid, INFOEMAIL);

                try
                {
                    file.RequestDate.Value = DateTime.Now;
                    file.Flag.Value = true;
                    file.Update();

                    GetData();
                    BindGrid();

                    UpdatePanel2.Update();

                    string strWrongInfor = "Request is submitted, please allow 24 - 48 hours for the file to be retrieved and scanned";
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel2, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");hideLoader();", true);
                    return;
                }
                catch (Exception ex)
                {
                    _logger.Error(ex);
                }
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Sends the storage pager file email.
    /// </summary>
    /// <param name="username">The username.</param>
    /// <param name="userid">The userid.</param>
    /// <param name="boxname">The boxname.</param>
    /// <param name="fistname">The fistname.</param>
    /// <param name="lastname">The lastname.</param>
    /// <param name="fileId">The file identifier.</param>
    /// <param name="emailaddr">The emailaddr.</param>
    private void sendStoragePagerFileEmail(string username, string userid, string boxname, string fistname, string lastname, string fileId, string emailaddr)
    {
        string strMailTemplet = getStorageMailTemplate();

        strMailTemplet = strMailTemplet.Replace("[username]", username);
        strMailTemplet = strMailTemplet.Replace("[userid]", userid);
        strMailTemplet = strMailTemplet.Replace("[boxname]", boxname);
        strMailTemplet = strMailTemplet.Replace("[firstname]", fistname);
        strMailTemplet = strMailTemplet.Replace("[lastname]", lastname);
        strMailTemplet = strMailTemplet.Replace("[fileid]", fileId);

        string strEmailSubject = "Storage Paper File Request";

        Email.SendFromEFileFolder(emailaddr, strEmailSubject, strMailTemplet);

    }

    /// <summary>
    /// Gets the storage mail template.
    /// </summary>
    /// <returns></returns>
    private string getStorageMailTemplate()
    {
        try
        {
            string strMailTemplet = string.Empty;
            string physicalPath = Request.PhysicalPath;
            int index = physicalPath.LastIndexOf("\\");
            physicalPath = physicalPath.Substring(0, index - 1);
            index = physicalPath.LastIndexOf("\\");
            string mailTempletUrl = Path.Combine(physicalPath.Substring(0, index + 1), "Office") + "\\StoragePaperFile.htm";
            StreamReader sr = new StreamReader(mailTempletUrl);
            string sLine = "";
            while (sLine != null)
            {
                sLine = sr.ReadLine();
                if (sLine != null)
                    strMailTemplet += sLine;
            }
            sr.Close();
            return strMailTemplet;
        }
        catch (Exception ex)
        {
            return string.Empty;
            _logger.Error(ex);
        }
    }

    #endregion

    #region WorkArea

    /// <summary>
    /// Handles the RowDataBound event of the WorkAreaGrid control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewRowEventArgs"/> instance containing the event data.</param>
    protected void WorkAreaGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (Sessions.HasViewPrivilegeOnly)
                {
                    HyperLink libDownload = e.Row.FindControl("btnWorkAreaDownload") as HyperLink;
                    libDownload.Visible = false;
                    Button libDel = e.Row.FindControl("btnWorkAreaDelete") as Button;
                    libDel.Visible = false;
                    LinkButton libEdit = e.Row.FindControl("lnkEdit") as LinkButton;
                    libEdit.Visible = false;
                    LinkButton libIndex = e.Row.FindControl("btnIndexing") as LinkButton;
                    libIndex.Visible = false;
                    Button libShare = e.Row.FindControl("btnWorkAreaShare") as Button;
                    libShare.Visible = false;
                    Button libAddToTab = e.Row.FindControl("btnWorkAreaAddToTab") as Button;
                    libAddToTab.Visible = false;
                }
                System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
                string path = drv["PathName"].ToString();
                string fileName = drv["FileName"].ToString();
                path = path.Replace(@"\", "/");

                int workAreaId = 0;

                HiddenField hdnWorkAreaId = e.Row.FindControl("hdnFileId") as HiddenField;
                if (hdnWorkAreaId != null)
                    workAreaId = Convert.ToInt32(hdnWorkAreaId.Value);

                TextBox txtDocumentName = (e.Row.FindControl("txtFileName") as TextBox);
                Label lblFileName = (e.Row.FindControl("lblfileName") as Label);
                if (lblFileName != null)
                {
                    lblFileName.Attributes.Add("onclick", "ShowPreviewForWorkArea('" + path + "','" + fileName + "', '" + workAreaId + "');");
                }
                LinkButton lnkBtnUpdate = (e.Row.FindControl("lbkUpdate") as LinkButton);
                if (lnkBtnUpdate != null)
                {
                    lnkBtnUpdate.Attributes.Add("onclick", "OnSaveRename('" + txtDocumentName.ClientID + "','" + path + "','" + fileName + "','" + workAreaId + "');");
                }
                LinkButton btnIndexing = (e.Row.FindControl("btnIndexing") as LinkButton);
                if (btnIndexing != null)
                {
                    btnIndexing.Attributes.Add("onclick", "OpenIndexPopup(this);");
                }

                LinkButton lnkBtnPreview = (e.Row.FindControl("btnWorkAreaPreview") as LinkButton);
                if (lnkBtnPreview != null)
                {
                    lnkBtnPreview.Attributes.Add("onclick", "ShowPreviewForWorkArea('" + path + "','" + fileName + "', '" + workAreaId + "');");
                }
                HyperLink lnkBtnDownload = (e.Row.FindControl("btnWorkAreaDownload") as HyperLink);
                if (lnkBtnDownload != null)
                {
                    lnkBtnDownload.Attributes.Add("onclick", "DownloadDoc('" + lnkBtnDownload.ClientID + "','" + (int)Enum_Tatva.Folders.WorkArea + "');");
                }
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Handles the Sorting event of the WorkAreaGrid control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void WorkAreaGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortDirection = "";
            string sortExpression = e.SortExpression;
            if (this.WorkArea_Sort_Direction == SortDirection.Ascending)
            {
                this.WorkArea_Sort_Direction = SortDirection.Descending;
                sortDirection = "DESC";
            }
            else
            {
                this.WorkArea_Sort_Direction = SortDirection.Ascending;
                sortDirection = "ASC";
            }
            DataView Source = new DataView(this.WorkAreaDataSource);
            Source.Sort = e.SortExpression + " " + sortDirection;

            this.WorkAreaDataSource = Source.ToTable();
            BindGrid();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Gets or sets the work area sort direction.
    /// </summary>
    /// <value>
    /// The work area sort direction.
    /// </value>
    private SortDirection WorkArea_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(WorkAreaSortDirection.Value))
            {
                WorkAreaSortDirection.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.WorkAreaSortDirection.Value);

        }
        set
        {
            this.WorkAreaSortDirection.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    /// <summary>
    /// Gets or sets the work area data source.
    /// </summary>
    /// <value>
    /// The work area data source.
    /// </value>
    public DataTable WorkAreaDataSource
    {
        get
        {
            return this.Session["WorkAreaDataSource"] as DataTable;
        }
        set
        {
            this.Session["WorkAreaDataSource"] = value;
        }
    }

    /// <summary>
    /// Handles the PageIndexChanged event of the WebPager3 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="PageChangedEventArgs"/> instance containing the event data.</param>
    protected void WebPager3_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager3.CurrentPageIndex = e.NewPageIndex;
        WebPager3.DataSource = this.WorkAreaDataSource;
        WebPager3.DataBind();
    }

    /// <summary>
    /// Handles the Click event of the btnSelectAddToTab control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnSelectAddToTab_Click(object sender, EventArgs e)
    {
        EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
        string selectedFilePath = string.Empty;
        int workareaId = 0;
        string edFormsUserShareId = string.Empty;
        if (hdnShareType.Value == Enum_Tatva.Folders.WorkArea.GetHashCode().ToString())
        {
            selectedFilePath = hdnFilePath.Value;
            selectedFilePath = selectedFilePath.Replace("%22", "\"").Replace("%27", "'");
            selectedFilePath = selectedFilePath.Replace("WorkArea", "workArea");
            workareaId = Convert.ToInt32(hdnFileId.Value);
        }
        else
        {
            edFormsUserShareId = hdnFileId.Value.ToLower();
            selectedFilePath = rackspacefileupload.GeneratePath(hdnFileName.Value.ToLower(), Enum_Tatva.Folders.EdFormsShare, edFormsUserShareId);
        }

        try
        {
            int folderId = Convert.ToInt32(hdnSelectedFolderID.Value);
            int dividerId = Convert.ToInt32(hdnSelectedDividerID.Value);

            var file = selectedFilePath.Substring(selectedFilePath.LastIndexOf('/') + 1);
            selectedFilePath = selectedFilePath.Replace(file, file.ToLower());
            string errorMsg = string.Empty;

            string dir = Server.MapPath("~/" + Common_Tatva.RackSpaceWorkareaDownload + Path.DirectorySeparatorChar + Sessions.SwitchedRackspaceId);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }

            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            var filePath = dir + "\\" + rackSpaceFileUpload.GetObject(ref errorMsg, selectedFilePath);
            if (string.IsNullOrEmpty(errorMsg))
            {
                string encodeName = HttpUtility.UrlPathEncode(file).Replace("%", "$");
                encodeName = encodeName.Replace(encodeName, encodeName.Replace("$20", " "));
                encodeName = Common_Tatva.SubStringFilename(encodeName);
                encodeName = encodeName.Replace(encodeName, encodeName.Replace(" ", "$20"));
                string vpath = Server.MapPath(string.Format("~/{0}", CurrentVolume));
                string pathName = string.Format("~/{0}", CurrentVolume) + Path.DirectorySeparatorChar + folderId + Path.DirectorySeparatorChar + dividerId + Path.DirectorySeparatorChar + encodeName;

                int documentOrder = FlashViewer.GetDocumentOrder(Convert.ToInt32(dividerId));
                string dstpath = string.Concat(vpath, Path.DirectorySeparatorChar, folderId, Path.DirectorySeparatorChar, dividerId);
                if (!Directory.Exists(dstpath))
                {
                    Directory.CreateDirectory(dstpath);
                }

                dstpath = string.Concat(dstpath, Path.DirectorySeparatorChar, encodeName);
                if (File.Exists(dstpath))
                {
                    dstpath = Common_Tatva.GetRenameFileName(dstpath);
                }
                File.Copy(filePath, dstpath, true);

                string newFilename = Path.GetFileNameWithoutExtension(dstpath);
                string displayName = HttpUtility.UrlDecode(newFilename.Replace('$', '%').ToString());

                string documentIDOrder = Common_Tatva.InsertDocument(dividerId, pathName, documentOrder, displayName, Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()));
                if (documentIDOrder == "0")
                {
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel13, typeof(UpdatePanel), "alertok", "alert(\"" + "Failed to move file. There is a problem with the file format." + "\");", true);
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, 0, workareaId);
                }
                else
                {
                    General_Class.AuditLogByDocId(Convert.ToInt32(documentIDOrder.Split('#')[0]), Sessions.SwitchedRackspaceId, Enum_Tatva.Action.Create.GetHashCode());
                    File.Delete(filePath);
                    rackSpaceFileUpload.DeleteObject(ref errorMsg, selectedFilePath);
                    string folderName = hdnSelectedFolderText.Value;
                    string tabName = hdnSelectedTabText.Value;
                    folderName = folderName.Insert(folderName.IndexOf(')') + 1, "/");
                    folderName = folderName.Remove(folderName.IndexOf('('), folderName.IndexOf(')') - folderName.IndexOf('(') + 1);
                    folderName = folderName.Replace("\n", string.Empty);
                    folderName = folderName.Replace("\r", string.Empty);
                    folderName = folderName.Replace("/", string.Empty);
                    folderName = folderName.Replace(" ", string.Empty);
                    tabName = tabName.Replace("\n", string.Empty);
                    tabName = tabName.Replace("\r", string.Empty);
                    tabName = tabName.Replace("/", string.Empty);
                    tabName = tabName.Replace(" ", string.Empty);

                    string message = "File(s) have been moved to folder: " + folderName + "& tab: " + tabName;
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel13, typeof(UpdatePanel), "alertok", "alert(\"" + message + "\");", true);
                    if (workareaId > 0)
                    {
                        eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode(), 0, 0, 0, workareaId);
                        General_Class.UpdateWorkArea(file, workareaId, Convert.ToInt32(documentIDOrder.Split('#')[0]), Enum_Tatva.IsDelete.No.GetHashCode(), 0, 1, selectedFilePath);
                    }
                    else
                    {
                        General_Class.MoveEdFormsUserShare(edFormsUserShareId, Convert.ToInt32(folderId), Convert.ToInt32(dividerId), Sessions.UserId);
                        General_Class.ChangeEdFormsUserShareStatus(edFormsUserShareId, (int)Enum_Tatva.EdFormsStatus.Move);
                        string IP = string.Empty;
                        try
                        {
                            IP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                        }
                        catch (Exception ex)
                        {
                            _logger.Info(ex);
                        }
                        General_Class.InsertIntoEdFormsUserLogs(edFormsUserShareId, (byte)Enum_Tatva.EdFormsStatus.Move, IP);
                    }
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(Convert.ToInt32(folderId), Convert.ToInt32(dividerId), Convert.ToInt32(documentIDOrder.Split('#')[0]), 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.WorkAreaMoveUpload.GetHashCode(), 0, 0);
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel13, typeof(UpdatePanel), "alertok", "alert(\"" + "Failed to get object from cloud." + "\");", true);
                eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Move.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, 0, workareaId);
            }
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
        finally
        {
            ScriptManager.RegisterStartupScript(UpdatePanel13, typeof(UpdatePanel), "hideLoader", "hideLoader();", true);
            GetData();
            BindGrid();
        }
    }

    /// <summary>
    /// Handles the Click event of the btnSaveSplitPdf control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnSaveSplitPdf_Click(object sender, EventArgs e)
    {
        try
        {
            string pageNumbers = hdnPageNumbers.Value;
            if (!string.IsNullOrEmpty(pageNumbers))
            {
                bool isSuccess = ExtractPages(pageNumbers, hdnFileName.Value, txtNewPdfName.Text.Trim(), hdnFileId.Value, hdnSelectedFolderText.Value);
                if (isSuccess)
                {
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Split.GetHashCode(), 0, 0, 0, Convert.ToInt32(hdnFileId.Value));
                    string message = "The selected pages have been added to tab.  Would you like to split additional pages?  If so, please select OK, otherewise, select Cancel";
                    ScriptManager.RegisterStartupScript(UpdatePanel2, typeof(UpdatePanel), "alert", "<script language=javascript>ShowConfirm('" + message + "');</script>", false);
                }
                else
                    eFileFolderJSONWS.InsertDocumentsAndFolderLogs(0, 0, 0, 0, 0, Enum_Tatva.DocumentsAndFoldersLogs.Split.GetHashCode(), Enum_Tatva.DocumentsAndFoldersLogsSubAction.FileNotFound.GetHashCode(), 0, 0, Convert.ToInt32(hdnFileId.Value));
            }

        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
        finally
        {
        }
    }

    /// <summary>
    /// Extracts the pages.
    /// </summary>
    /// <param name="pageNumbers">The page numbers.</param>
    /// <param name="fileName">Name of the file.</param>
    /// <param name="newFName">New name of the f.</param>
    /// <param name="fileID">The file identifier.</param>
    /// <param name="folderName">Name of the folder.</param>
    /// <returns></returns>
    public bool ExtractPages(string pageNumbers, string fileName, string newFName, string fileID, string folderName)
    {
        string folderID = hdnSelectedFolderID.Value;
        string dividerID = hdnSelectedDividerID.Value;
        try
        {

            string newEncodeName = string.Empty;
            string outputPdfPath = string.Empty;

            ExtractAndUploadPDF(pageNumbers, fileName, newFName, fileID, ref newEncodeName, ref outputPdfPath);


            string pathName = HttpContext.Current.Server.MapPath(string.Format("~/{0}", CurrentVolume) + Path.DirectorySeparatorChar + folderID + Path.DirectorySeparatorChar
                + dividerID);
            newEncodeName = Common_Tatva.GetRenameFileName(pathName + Path.DirectorySeparatorChar + newEncodeName);

            if (!Directory.Exists(pathName))
            {
                Directory.CreateDirectory(pathName);
            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
            File.Copy(outputPdfPath, newEncodeName);

            FileInfo fileInfo = new FileInfo(newEncodeName);
            decimal fileSize = Math.Round((decimal)(fileInfo.Length / 1024) / 1024, 2);

            string temp = newEncodeName.Substring(newEncodeName.LastIndexOf("\\") + 1);
            newFileName = HttpUtility.UrlDecode(temp.Replace('$', '%').ToString());

            ProcessFileAndSaveInDB((float)fileSize, pathName, int.Parse(dividerID), int.Parse(folderID), true);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            File.SetAttributes(outputPdfPath, FileAttributes.Normal);
            File.Delete(outputPdfPath);

            return true;
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteErrorLog(folderID, dividerID, fileName, ex);
            return false;
        }
    }

    /// <summary>
    /// Extracts the and upload PDF.
    /// </summary>
    /// <param name="pageNumbers">The page numbers.</param>
    /// <param name="fileName">Name of the file.</param>
    /// <param name="newFName">New name of the f.</param>
    /// <param name="fileID">The file identifier.</param>
    /// <param name="newEncodeName">New name of the encode.</param>
    /// <param name="outputPdfPath">The output PDF path.</param>
    public void ExtractAndUploadPDF(string pageNumbers, string fileName, string newFName, string fileID, ref string newEncodeName, ref string outputPdfPath)
    {
        PdfReader reader = null;
        PdfCopy pdfCopyProvider = null;
        PdfCopy pdfRemaining = null;
        PdfImportedPage importedPage = null;
        PdfImportedPage importedPage1 = null;
        iTextSharp.text.Document sourceDocument = null;
        RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
        string errorMsg = string.Empty;
        try
        {
            string[] extractThesePages = pageNumbers.Split(',');

            string sourcePdfPath;
            string encodeName = HttpUtility.UrlPathEncode(fileName);
            encodeName = encodeName.Replace("%20", " ");

            string uid = Sessions.SwitchedSessionId;
            string path = HttpContext.Current.Server.MapPath("~/");

            sourcePdfPath = path + encodeName;

            outputPdfPath = path + Path.DirectorySeparatorChar + Common_Tatva.RackSpaceNewSplitWorkareaDownload + Path.DirectorySeparatorChar + uid;

            if (!Directory.Exists(outputPdfPath))
            {
                Directory.CreateDirectory(outputPdfPath);
            }

            if (newFName.IndexOf(".pdf") < 0)
                newFName = newFName + ".pdf";

            newFName = newFName.Replace("#", "");
            newFName = newFName.Replace("&", "");

            newEncodeName = HttpUtility.UrlPathEncode(newFName);
            newEncodeName = newEncodeName.Replace("%", "$");
            outputPdfPath = outputPdfPath + Path.DirectorySeparatorChar + newEncodeName;


            string remainingPath = path + Path.DirectorySeparatorChar + Common_Tatva.RackSpaceNewSplitWorkareaDownload + Path.DirectorySeparatorChar + uid + Path.DirectorySeparatorChar + encodeName.Substring(encodeName.LastIndexOf('/') + 1);

            reader = new PdfReader(sourcePdfPath);

            // For simplicity, I am assuming all the pages share the same size
            // and rotation as the first page:
            sourceDocument = new iTextSharp.text.Document(reader.GetPageSizeWithRotation(int.Parse(extractThesePages[0])));

            // Initialize an instance of the PdfCopyClass with the source 
            // document and an output file stream:
            pdfCopyProvider = new PdfCopy(sourceDocument, new System.IO.FileStream(outputPdfPath, System.IO.FileMode.Create));
            if (reader.NumberOfPages > extractThesePages.Length)
                pdfRemaining = new PdfCopy(sourceDocument, new System.IO.FileStream(remainingPath, System.IO.FileMode.Create));
            sourceDocument.Open();

            // Walk the array and add the page copies to the output file:
            foreach (string pageNumber in extractThesePages)
            {
                importedPage = pdfCopyProvider.GetImportedPage(reader, int.Parse(pageNumber));
                pdfCopyProvider.AddPage(importedPage);
            }
            for (int pageNumber = 1; pageNumber <= reader.NumberOfPages; pageNumber++)
            {
                if (extractThesePages.Contains(pageNumber.ToString()))
                    continue;
                importedPage1 = pdfRemaining.GetImportedPage(reader, pageNumber);
                pdfRemaining.AddPage(importedPage1);
            }
            if (sourceDocument != null && sourceDocument.PageNumber > 0)
                sourceDocument.Close();
            if (reader != null)
                reader.Close();
            try
            {
                if (pdfCopyProvider != null)
                {
                    pdfCopyProvider.PageEmpty = false;
                    pdfCopyProvider.Close();
                }
                if (pdfRemaining != null)
                {
                    pdfRemaining.PageEmpty = false;
                    pdfRemaining.Close();
                }
            }
            catch (Exception ex)
            {
            }
            if (File.Exists(remainingPath))
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                File.Copy(remainingPath, sourcePdfPath, true);
                GC.Collect();
                GC.WaitForPendingFinalizers();
                File.Delete(remainingPath);
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
        catch (Exception ex)
        {
            if (sourceDocument != null)
                sourceDocument.Close();
            if (reader != null)
                reader.Close();
            pdfCopyProvider.Close();
            pdfRemaining.Close();
        }
    }

    /// <summary>
    /// Processes the file and save in database.
    /// </summary>
    /// <param name="size">The size.</param>
    /// <param name="path">The path.</param>
    /// <param name="dividerIdForFile">The divider identifier for file.</param>
    /// <param name="folderID">The folder identifier.</param>
    /// <param name="isMovedToFolder">if set to <c>true</c> [is moved to folder].</param>
    private void ProcessFileAndSaveInDB(float size, string path, int dividerIdForFile = 0, int folderID = 0, bool isMovedToFolder = false)
    {
        try
        {
            WorkItem item = new WorkItem();
            item.FolderID = folderID;
            item.DividerID = dividerIdForFile;
            string basePath = this.MapPath(this.splitPdfFolderName);

            item.Path = path;

            item.FileName = Path.GetFileNameWithoutExtension(newFileName);

            item.FullFileName = Path.GetFileName(newFileName);

            item.UID = Sessions.SwitchedSessionId;
            item.contentType = Common_Tatva.getFileExtention(Path.GetExtension(newFileName));

            int order = FlashViewer.GetDocumentOrder(Convert.ToInt32(dividerIdForFile));


            item.MachinePath = path;

            if (!Directory.Exists(item.MachinePath))
            {
                Directory.CreateDirectory(item.MachinePath);
            }

            string encodeName = HttpUtility.UrlPathEncode(item.FullFileName);
            encodeName = encodeName.Replace("%", "$");
            string source = string.Concat(path, Path.DirectorySeparatorChar, encodeName);
            var pageCount = 1;
            if (item.contentType == StreamType.JPG)
            {
                Jpeg2Engine jpeg2Engine = new Jpeg2Engine(item);
                jpeg2Engine.generatePDFfromImage(source, source.Split('.')[0] + ".pdf");
            }
            else if (item.contentType == StreamType.PDF)
            {
                PdfReader pdfReader = new PdfReader(source);
                pageCount = pdfReader.NumberOfPages;
            }
            if (isMovedToFolder)
            {
                string relativePath = CurrentVolume + Path.DirectorySeparatorChar + folderID + Path.DirectorySeparatorChar + dividerIdForFile;
                this.DocumentID = General_Class.DocumentsInsert(item.FileName, item.FolderID, item.UID, relativePath, item.DividerID, pageCount, (float)size, item.FileName,
                    (int)item.contentType, order + 1, Convert.ToString(Enum_Tatva.Class.Permanent.GetHashCode()));
                //Need to ask
                General_Class.AuditLogByDocId(Convert.ToInt32(this.DocumentID), Sessions.SwitchedSessionId, Enum_Tatva.Action.Move.GetHashCode());
                General_Class.CreateDocumentsAndFolderLogs(new Guid(Sessions.SwitchedSessionId), item.FolderID, 0, Convert.ToInt32(this.DocumentID), item.DividerID, 0, 0, 0, null, 0, Enum_Tatva.DocumentsAndFoldersLogs.WorkAreaSplitUpload.GetHashCode(), 0, 0, 0, 0, 0);
            }
            this.DocumentName = item.FileName;
        }
        catch (Exception ex)
        {
        }
    }

    /// <summary>
    /// Gets or sets the document identifier.
    /// </summary>
    /// <value>
    /// The document identifier.
    /// </value>
    public string DocumentID
    {
        get
        {
            return (string)Session["DocumentID"];
        }
        set
        {
            Session["DocumentID"] = value;

        }
    }

    /// <summary>
    /// Gets or sets the name of the document.
    /// </summary>
    /// <value>
    /// The name of the document.
    /// </value>
    public string DocumentName
    {
        get
        {
            return (string)Session["DocumentName"];
        }
        set
        {
            Session["DocumentName"] = value;

        }
    }

    /// <summary>
    /// Gets the name of the split PDF folder.
    /// </summary>
    /// <value>
    /// The name of the split PDF folder.
    /// </value>
    public string splitPdfFolderName
    {
        get
        {
            return string.Format("~/{0}", splitWorkAreaPath);
        }

    }

    /// <summary>
    /// Gets or sets the side message.
    /// </summary>
    /// <value>
    /// The side message.
    /// </value>
    public string SideMessage
    {
        get
        {
            return this.Session["_Side_Message"] as string;
        }

        set
        {
            this.Session["_Side_Message"] = value;
        }
    }

    #endregion

    #region Archive

    /// <summary>
    /// Gets or sets the archive data source.
    /// </summary>
    /// <value>
    /// The archive data source.
    /// </value>
    public DataTable ArchiveDataSource
    {
        get
        {
            return this.Session["ArchiveDataSource"] as DataTable;
        }
        set
        {
            this.Session["ArchiveDataSource"] = value;
        }
    }

    /// <summary>
    /// Handles the PageIndexChanged event of the WebPager4 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="PageChangedEventArgs"/> instance containing the event data.</param>
    protected void WebPager4_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager4.CurrentPageIndex = e.NewPageIndex;
        WebPager4.DataSource = this.ArchiveDataSource;
        WebPager4.DataBind();
    }

    /// <summary>
    /// Handles the RowDataBound event of the ArchiveGrid control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewRowEventArgs"/> instance containing the event data.</param>
    protected void ArchiveGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink lnkBtnDownload = (e.Row.FindControl("btnArchiveDownload") as HyperLink);
            if (lnkBtnDownload != null)
            {
                lnkBtnDownload.Attributes.Add("onclick", "DownloadDoc('" + lnkBtnDownload.ClientID + "','" + (int)Enum_Tatva.Folders.Archive + "');");
            }
        }
    }

    /// <summary>
    /// Handles the Sorting event of the ArchiveGrid control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void ArchiveGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        try
        {
            string sortDirection = "";
            string sortExpression = e.SortExpression;
            if (this.Archive_Sort_Direction == SortDirection.Ascending)
            {
                this.Archive_Sort_Direction = SortDirection.Descending;
                sortDirection = "DESC";
            }
            else
            {
                this.Archive_Sort_Direction = SortDirection.Ascending;
                sortDirection = "ASC";
            }
            DataView Source = new DataView(this.ArchiveDataSource);
            Source.Sort = e.SortExpression + " " + sortDirection;

            this.ArchiveDataSource = Source.ToTable();
            BindGrid();
        }
        catch (Exception ex)
        {
            _logger.Error(ex);
        }
    }

    /// <summary>
    /// Gets or sets the archive sort direction.
    /// </summary>
    /// <value>
    /// The archive sort direction.
    /// </value>
    private SortDirection Archive_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(ArchiveSortDirection.Value))
            {
                ArchiveSortDirection.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.ArchiveSortDirection.Value);

        }
        set
        {
            this.ArchiveSortDirection.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    /// <summary>
    /// Handles the Click event of the btnArchiveSave control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
    protected void btnArchiveSave_Click(object sender, EventArgs e)
    {
        try
        {
            string errorMsg = string.Empty;
            string newFileName = fileNameFld.Text.Trim();
            newFileName = newFileName.Replace("&", "").Replace("#", "");
            newFileName = newFileName.IndexOf(".pdf") > 0 ? newFileName : newFileName + ".pdf";
            string oldFileName = hdnFilePath.Value;
            string oldPath = oldFileName.Substring(0, oldFileName.LastIndexOf('/') + 1);
            oldFileName = oldPath + hdnFileNameOnly.Value;
            newFileName = Common_Tatva.SubStringFilename(newFileName);
            oldPath = oldPath.Substring(0, oldPath.LastIndexOf('/'));
            string newObjectName = rackspacefileupload.RenameObject(ref errorMsg, oldFileName, newFileName, Enum_Tatva.Folders.Archive, Sessions.SwitchedRackspaceId);
            if (string.IsNullOrEmpty(errorMsg) && !string.IsNullOrEmpty(newObjectName))
            {
                newFileName = newObjectName.Substring(newObjectName.LastIndexOf('/') + 1);
                int count = General_Class.UpdateArchive(Convert.ToInt32(hdnFileId.Value), newFileName, newObjectName);
                EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();
                bool isValid = eFileFolderJSONWS.UpdateArchiveInElastic(hdnFileId.Value, newFileName, newObjectName, oldPath.Substring(oldPath.LastIndexOf('/') + 1));
                GetData();
                BindGrid();
                hdnFileNameOnly.Value = newFileName;
                lblFileName.Text = newFileName;
                hdnFilePath.Value = newObjectName;
                ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                               "<script language=javascript>alert(\"" + "Document has been edited successfully." + "\");ShowPreviewForArchive(" + hdnObjId.Value + ", true);</script>", false);
            }
            else
            {
                ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                               "<script language=javascript>alert(\"" + "Some error occurred. Please try again later !!" + "\");ShowPreviewForArchive(" + hdnObjId.Value + ", true);</script>", false);
            }
        }
        catch (Exception ex)
        {
            _logger.Info(ex);
            ScriptManager.RegisterStartupScript(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                               "<script language=javascript>alert(\"" + "Some error occurred. Please try again later !!" + "\");ShowPreviewForArchive(" + hdnObjId.Value + ", true);</script>", false);
        }
    }

    #endregion

    #region EdForms

    /// <summary>
    /// Gets or sets the ed forms data source.
    /// </summary>
    /// <value>
    /// The ed forms data source.
    /// </value>
    public DataTable EdFormsDataSource
    {
        get
        {
            return this.Session["EdFormsDataSource"] as DataTable;
        }
        set
        {
            this.Session["EdFormsDataSource"] = value;
        }
    }

    /// <summary>
    /// Gets or sets the ed forms sort direction.
    /// </summary>
    /// <value>
    /// The ed forms sort direction.
    /// </value>
    private SortDirection EdForms_Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(EdFormsSortDirection.Value))
            {
                EdFormsSortDirection.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.EdFormsSortDirection.Value);

        }
        set
        {
            this.EdFormsSortDirection.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    /// <summary>
    /// Handles the Sorting event of the EdFormsGrid control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewSortEventArgs"/> instance containing the event data.</param>
    protected void EdFormsGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.EdForms_Sort_Direction == SortDirection.Ascending)
        {
            this.EdForms_Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.EdForms_Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.EdFormsDataSource);

        Source.Sort = e.SortExpression + " " + sortDirection;
        this.EdFormsDataSource = Source.ToTable();
        BindGrid();
    }

    /// <summary>
    /// Handles the RowDataBound event of the EdFormsGrid control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="GridViewRowEventArgs"/> instance containing the event data.</param>
    protected void EdFormsGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                System.Data.DataRowView drv = e.Row.DataItem as DataRowView;
                string edFormsUserShareId = drv.Row["EdFormsUserShareId"].ToString();
                string fileName = drv.Row["FileName"].ToString();
                HyperLink lnkBtnDownload = (e.Row.FindControl("lnkBtnDownload") as HyperLink);
                if (lnkBtnDownload != null)
                {
                    lnkBtnDownload.Attributes.Add("href", "../Office/PdfViewer.aspx?ID=" + edFormsUserShareId + "&data=" + QueryString.QueryStringEncode("ID=" + fileName + "&folderID=" + (int)Enum_Tatva.Folders.EdFormsShare + "&sessionID=" + Sessions.SwitchedRackspaceId));
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }
        }
    }

    /// <summary>
    /// Handles the PageIndexChanged event of the WebPager5 control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="PageChangedEventArgs"/> instance containing the event data.</param>
    protected void WebPager5_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager5.CurrentPageIndex = e.NewPageIndex;
        WebPager5.DataSource = this.EdFormsDataSource;
        WebPager5.DataBind();
    }

    #endregion
}
