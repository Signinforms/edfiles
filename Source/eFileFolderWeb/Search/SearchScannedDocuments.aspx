﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="SearchScannedDocuments.aspx.cs" Inherits="Search_SearchScannedDocuments" %>

<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
        TagPrefix="cc1" %>



    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Search Scanned Documents </h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div class="content-box listing-view">
                    <asp:Panel ID="pnlDefineSearchCriteria" runat="server">
                        <ajaxToolkit:TabContainer runat="server" ID="fileTabs" ActiveTabIndex="0">
                            <ajaxToolkit:TabPanel runat="server" ID="Panel1" HeaderText="Define Search Criteria">
                                <ContentTemplate>
                                    <asp:UpdatePanel ID="UpdatePanelResult" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <fieldset>
                                                <label>Search Criteria</label>
                                                <asp:DropDownList ID="drpAvailableTags" runat="server" DataTextField="Tag" DataValueField="Tag" style="max-width:210px"></asp:DropDownList>
                                                <asp:ImageButton ID="btnAddKey" runat="server" CssClass="icon icon-plus" ImageUrl="~/images/plus.png" OnClick="btnAddKey_Click" Style="width: 16px" />
                                            </fieldset>
                                            <fieldset>
                                                <label>Document Name</label>
                                                <asp:TextBox ID="txtDocumentName" runat="server" style="max-width:210px"></asp:TextBox>
                                            </fieldset>
                                            <fieldset>
                                            <asp:GridView ID="grdUserSearch" runat="server" AutoGenerateColumns="false"  Width="68%"
                                                 ShowHeader="false" OnRowCommand="grdUserSearch_RowCommand">
                                                <PagerSettings Visible="False" />
                                                <RowStyle CssClass="border-none" />
                                                <Columns>
                                                    <asp:BoundField DataField="TagKey" ItemStyle-CssClass="form-label" ItemStyle-BorderStyle="None"/>
                                                    <asp:TemplateField>
                                                        <ItemStyle BorderStyle="None" />
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtsearchTageValue" runat="server" CssClass="form-textbox" style="max-width:210px;width:210px;margin-left:0;" ></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-CssClass="align-middle" >
                                                        <ItemStyle BorderStyle="None" />
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="btnremovekey" runat="server" CommandName="Deletekey" CommandArgument='<%# Eval("TagKey") %>' ImageUrl="~/images/cancel.png" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            </fieldset>

                                            <fieldset>
                                                <label>&nbsp;</label>
                                                <asp:Button CssClass="create-btn btn green" ID="btnSearch" Text="Search" runat="server" OnClick="btnSearch_Click" />
                                            </fieldset>


                                            <div class="work-area">
                                                <div class="overflow-auto">
                                                    <asp:GridView ID="SearchGridView" runat="server"
                                                        DataKeyNames="DocumentID " AllowSorting="True" AutoGenerateColumns="false" AllowPaging="true" PageSize="15"
                                                        CssClass="datatable listing-datatable">
                                                        <PagerSettings Visible="False" />
                                                        <AlternatingRowStyle CssClass="odd" />
                                                        <RowStyle CssClass="even" HorizontalAlign="Center" />
                                                        <%--<SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                        <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                                        <Columns>
                                                            <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label8" runat="server" Text="Action" />
                                                                </HeaderTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="true">
                                                                <HeaderTemplate>
                                                                    <asp:Label ID="Label34" Text="File" runat="server" />
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="LinkButton21" runat="server" Text="Down" CssClass="ic-icon ic-document" CausesValidation="false"
                                                                        PostBackUrl='<%#string.Format("../office/PDFDownloader.aspx?ID={0}",Eval("DocumentID"))%>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="Name" HeaderText="Document Name" SortExpression="Name">
                                                                <%--<HeaderStyle CssClass="form_title" />--%>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DOB" HeaderText="Create Date" DataFormatString="{0:MM.dd.yyyy}"
                                                                SortExpression="DOB" />
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <p>
                                                                There are no documents.
                                                            </p>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </ajaxToolkit:TabPanel>
                        </ajaxToolkit:TabContainer>
                    </asp:Panel>
                </div>
                <div class="tekstDef" style="">
                </div>
            </div>
        </div>
    </div>
</asp:Content>

