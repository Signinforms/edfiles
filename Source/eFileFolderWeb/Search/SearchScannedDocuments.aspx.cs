﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DataQuicker2.Framework;
using Shinetech.DAL;
using AjaxControlToolkit;
using Shinetech.Framework.Controls;
using Shinetech.Utility;
using System.IO;
using Shinetech.Engines.Adapters;
using System.Collections.Specialized;

public partial class Search_SearchScannedDocuments   : LicensePage
{
    private static readonly string PDFPATH = ConfigurationManager.AppSettings["PdfPath"];
    private static readonly string INFOEMAIL = ConfigurationManager.AppSettings["INFOEMAIL"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {            
            BindGrid();
        }
    }

    public  string UID
    {
        get
        {
            return Sessions.SwitchedSessionId;
            //return Membership.GetUser().ProviderUserKey.ToString();
        }
    }

    
    

    public void BindGrid()
    {
        DataTable GridSource = FileFolderManagement.GetUserDocumentSearchCriteria(UID);
        this.grdUserSearch.DataSource = GridSource;
        this.grdUserSearch.DataBind();

        DataTable DataSource = FileFolderManagement.GetUserDocumentAvailableSearchKeys(UID);        
        this.drpAvailableTags.DataSource = DataSource;
        this.drpAvailableTags.DataBind();

    }
    protected void btnAddKey_Click(object sender, ImageClickEventArgs e)
    {

        DataTable ExistingItems = FileFolderManagement.GetUserDocumentSearchCriteria(UID);
        if (ExistingItems != null && ExistingItems.Rows.Count < 5)
        {
            FileFolderManagement.AddUserSearchCriteria(this.drpAvailableTags.SelectedValue, UID);
        }    
        this.drpAvailableTags.DataSource = null;
        BindGrid();
    }
    protected void grdUserSearch_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Deletekey" && e.CommandArgument != null)
        {
            FileFolderManagement.DeleteUserSearchCriteria(e.CommandArgument.ToString(), UID);
            this.drpAvailableTags.DataSource = null;
            BindGrid();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        NameValueCollection SearParams = new NameValueCollection();
        SearParams.Add("@UID", UID);
        foreach (GridViewRow row in grdUserSearch.Rows)
        {
            if (row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtTagValue = (row.FindControl("txtsearchTageValue") as TextBox);
                string TagKey = (row.Cells[0]).Text;
                SearParams.Add(TagKey ?? string.Empty, txtTagValue.Text ?? string.Empty);
                
            }
        }
        if ( SearParams != null && SearParams.Count > 0)
        {
            DataTable dt = FileFolderManagement.SearchDocumentByTags(SearParams, UID,txtDocumentName.Text ?? string.Empty);
            SearchGridView.DataSource= dt;
            SearchGridView.DataBind();
        }
    }
}