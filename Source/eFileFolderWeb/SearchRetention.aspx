<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="SearchRetention.aspx.cs" Inherits="SearchRetention" Title="" %>

<%@ Import Namespace="System.Data" %>
<%@ Register Src="~/Controls/RegisterDirectly.ascx" TagName="ucRegisterDirectly" TagPrefix="ucRegisterDirectly" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register Src="~/Controls/UserFind.ascx" TagName="UserFind" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        p, input, table {
            line-height: normal;
        }

        tr {
            font-family: 'Open Sans', sans-serif;
        }

        th,td {
            vertical-align: top;
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-right-style:solid;
            border-right-width:1px;
            border-left-style:solid;
            border-left-width:1px;
        }

        span > table > tbody > tr > td {
            border-bottom-style: none;
            border-bottom-width: 0px;
            border-left-style: none;
            border-left-width: 0px;
            border-right-style: none;
            border-right-width: 0px;
        }

        .work_content td {
            text-align:center!important;
        }
        .link-head {
            height:47px !important;
        }
    </style>
    <div class="title-container aboutus-title">
        <div class="inner-wrapper">
            <h1 style="padding-top:15px;">Records Retention Manual</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    
    <div class="inner-wrapper">
        <div class="page-container">
            <div class="inner-wrapper">
                <div class="aboutus-contant">
					<div class="left-content">
                        <div class="work_content">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    
                                        <p><b>Our Mission:</b>                                               
                                            To assist school districts and other educational institutions Store, Organize &
                                            Manage Information, Not Paper!
                                        </p>
                                        <p>
                                            As a courtesy to our school district customers, and to assist all school districts,
                                            EdFiles has developed an online reference tool of CASBO's (California Association
                                            of School Business Officials) records retention manual.
                                        </p>
                                        <p>
                                            The Records Retention Manual is designed as a quick reference to the retention period
                                            of documents. While the lists of documents may not be totally inclusive, most documents
                                            used in school districts are represented. Various documents may appear in more than
                                            one area of responsibility. Old documents, no longer required, remain listed because
                                            they must still be maintained. The retention periods indicated are to assist district
                                            personnel in both the retention of permanent records and the timely destruction
                                            of documents.
                                        </p>
                                        <p>
                                            If your district requires any assistance in records retention, records conversion
                                            to electronic format and document scanning and archiving, please contact us at <strong>1-657-217-3260</strong>.
                                            <%--or <strong>1-855-334-5336</strong> or <strong>714-522-7700</strong>.--%>
                                        </p>
                                        <div>
                                            <table style="margin: 0px 20px 0px 35px" cellspacing="1" cellpadding="0" width="920"
                                                bgcolor="#ffffff" border="0">
                                                <tbody>
                                                    <div background="../images/bg_main.gif" bgcolor="#f6f5f2">
                                                        <table border="0" cellpadding="0" cellspacing="0" style="margin: 10px 0 20px 10px;">
                                                            <tr>
                                                                <td height="20" colspan="3" align="left" valign="top" class="tekstDef" style="padding-top: 0px;
                                                                    padding-bottom: 0px; border-bottom-style:none;border-bottom-width:0px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;">
                                                                    Enter search term:
                                                                    <asp:DropDownList ID="dropsdownlist1" runat="server" class="form_tekst_field" />
                                                                    <asp:TextBox ID="txtKey" runat="server" class="form_tekst_field" MaxLength="50" />
                                                                    <asp:Button runat="server" ID="btnSearch" OnClick="btnSearch_Click" Text="Search" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="GridView1" runat="server" DataKeyNames="Id" OnSorting="GridView1_Sorting"
                                                                AllowSorting="True" AutoGenerateColumns="false" Width="100%" BorderWidth="2" Style="height:40%; width:920px">
                                                                <PagerSettings Visible="False" />
                                                                <AlternatingRowStyle CssClass="table_tekst" BackColor="#DFDFDF" />
                                                                <RowStyle CssClass="table_tekst" HorizontalAlign="Center" BackColor="#F8F8F5" />
                                                                <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                                                <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />
                                                                <Columns>
                                                                    <asp:BoundField DataField="Department" HeaderText="Department" SortExpression="Department">
                                                                        <HeaderStyle CssClass="form_title" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="RequiredForm" HeaderText="RequiredForm" SortExpression="RequiredForm">
                                                                        <HeaderStyle CssClass="form_title" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="ClassRecord" HeaderText="ClassRecord" SortExpression="ClassRecord">
                                                                        <HeaderStyle CssClass="form_title" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Title" HeaderText="Title" SortExpression="Title" />
                                                                    <asp:BoundField DataField="RequiredHardCopyRetention" HeaderText="RequiredHardCopy" />
                                                                    <asp:BoundField DataField="RecommendedPeriod" HeaderText="RecommendedPeriod" SortExpression="RecommendedPeriod" />
                                                                    <asp:BoundField DataField="HardCopyRetention" HeaderText="HardCopyRetention" SortExpression="HardCopyRetention" />
                                                                    <asp:BoundField DataField="MediaElectronicRetention" HeaderText="MediaElectronic"
                                                                        SortExpression="MediaElectronicRetention" />
                                                                </Columns>
                                                            </asp:GridView>
                                                            <cc1:WebPager ID="WebPager1" runat="server" OnPageIndexChanged="WebPager1_PageIndexChanged"
                                                                OnPageSizeChanged="WebPager1_PageSizeChanged" PageSize="15" CssClass="prevnext"
                                                                PagerStyle="OnlyNextPrev" ControlToPaginate="GridView1" Style="border-bottom-style:none;border-bottom-width:0px;border-left-style:none;border-left-width:0px;border-right-style:none;border-right-width:0px;" ></cc1:WebPager>
                                                            <asp:HiddenField ID="SortDirection1" runat="server" Value=""></asp:HiddenField>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="prevnext" bgcolor="#f8f8f5">
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click"></asp:AsyncPostBackTrigger>
                                    <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged"></asp:AsyncPostBackTrigger>
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <div class="right-content">
						<ucRegisterDirectly:ucRegisterDirectly ID="ucRegister" runat="server" />
					</div>
				</div>
            </div>
        </div>
    </div>    
</asp:Content>
