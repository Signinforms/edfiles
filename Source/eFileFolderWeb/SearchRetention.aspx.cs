using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Framework.Controls;
using Shinetech.Utility;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Framework.Controls;
using System.IO;
using Account = Shinetech.DAL.Account;

public partial class SearchRetention :BasePage
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            List<string> sorts = new List<string>();
            sorts.Add("Department");
            sorts.Add("ClassRecord");
            sorts.Add("Title");
            sorts.Add("RequiredForm");
            sorts.Add("RequiredHardCopyRetention");
            sorts.Add("RecommendedPeriod");
            sorts.Add("HardCopyRetention");
            sorts.Add("MediaElectronicRetention");
            dropsdownlist1.DataSource = sorts;
            dropsdownlist1.DataBind();

            GetData();//重新获取操作后的数据源
            BindGrid();//绑定GridView,为删除服务

        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!this.Page.User.Identity.IsAuthenticated)
        {
            this.MasterPageFile = "~/LoginMasterPage.master";
        }
    }

    #region 分页
    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        if (this.DataSource != null)
        {
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
    }

    protected void WebPager1_PageSizeChanged(object sender, PageSizeChangeEventArgs e)
    {
        WebPager1.CurrentPageIndex = 0;
        if (this.DataSource != null)
        {
            WebPager1.DataSource = this.DataSource;
            WebPager1.DataBind();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string strKey = this.txtKey.Text;

        GetRetention(strKey);
        BindGrid();

        UpdatePanel1.Update();
    }

    public void GetRetention(string key)
    {
        this.DataSource = UserManagement.GetRetentionList(this.dropsdownlist1.Text,key);
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource_Retention"] as DataTable;
        }
        set
        {
            this.Session["DataSource_Retention"] = value;
        }
    }


    #endregion

    #region 数据绑定
    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData()
    {
        this.DataSource = UserManagement.GetRetentionList();
    }
    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }
    #endregion

    protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();//重新设置数据源，绑定
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

   
}
