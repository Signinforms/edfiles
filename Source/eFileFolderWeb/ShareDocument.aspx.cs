﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Shinetech.DAL;

public partial class ShareDocument : System.Web.UI.Page
{
    private int ExpirationTimeConfig = Convert.ToInt32(ConfigurationManager.AppSettings["ValidShareIdHours"]);
    private string data = HttpContext.Current.Request.QueryString["data"];
    private string edFileSharePath = ConfigurationManager.AppSettings["EdFileShare"];

    public string MailID
    {
        get
        {
            return Convert.ToString(Request.QueryString["MailID"]);
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string decode = QueryString.QueryStringDecode(data);
            string[] splitedData = decode.Split('&');
            hdnShareID.Value = (splitedData[2].Split('=')[1]);
            string sessionId = string.Empty;
            if (splitedData.Length > 3)
                sessionId = splitedData[3].Split('=')[1];
            string date = "yyyyMMddHHmmssfff";
            DateTime shareDateTime = DateTime.ParseExact((splitedData[1].Split('=')[1]), date, CultureInfo.InvariantCulture);
            int expirationTime = General_Class.GetExpirationTime(new Guid(sessionId));
            if ((DateTime.Now - shareDateTime).TotalHours > ((expirationTime > 0) ? expirationTime : ExpirationTimeConfig))
            {
                ScriptManager.RegisterClientScriptBlock(TemplateControl, typeof(UpdatePanel), "alertok", "alert(\"This ShareLink has been expired. Please contact your administrator for further help.\");", true);
                hdnShareID.Value = null;
            }
        }
        //else {
        //    GetDocument();
        //}
    }

    public void GetDocument()
    {

    }

    protected void hdnBtn_Click(object sender, EventArgs e)
    {
        string decode = QueryString.QueryStringDecode(data);
        string[] splitedData = decode.Split('&');
        string sessionId = string.Empty;
        string date = "yyyyMMddHHmmssfff";
        if (splitedData.Length > 3)
            sessionId = splitedData[3].Split('=')[1];
        DateTime shareDateTime = DateTime.ParseExact((splitedData[1].Split('=')[1]), date, CultureInfo.InvariantCulture);
        int expirationTime = General_Class.GetExpirationTime(new Guid(sessionId));
        if ((DateTime.Now - shareDateTime).TotalHours <= ((expirationTime > 0) ? expirationTime : ExpirationTimeConfig))
        {
            if (Convert.ToInt32(MailID) > 0)
            {
                DataSet dsDocument = FlashViewer.GetDocumentFromMailID(Convert.ToInt32(MailID));
                if (dsDocument != null && dsDocument.Tables[0] != null && dsDocument.Tables[0].Rows.Count > 0)
                {
                    string folderPath = Convert.ToString(dsDocument.Tables[0].Rows[0]["PathName"]);
                    if (!string.IsNullOrEmpty(folderPath))
                        folderPath = folderPath.Replace(@"\", "/");
                    string fileName = Convert.ToString(dsDocument.Tables[0].Rows[0]["Name"]);

                    string encodeName = HttpUtility.UrlPathEncode(fileName);
                    encodeName = encodeName.Replace("%", "$");
                    if (Convert.ToInt32(dsDocument.Tables[0].Rows[0]["FileExtention"]) == 14)
                    {
                        string pathStr = Server.MapPath(folderPath);
                        //pathStr += folderPath;
                        string fullName2 = string.Concat(pathStr, System.IO.Path.DirectorySeparatorChar, (dsDocument.Tables[0].Rows[0]["FullFileName"]).ToString().Replace(" ", "$20"));
                        if (!System.IO.File.Exists(fullName2))
                        {
                            fullName2 = string.Concat(pathStr, System.IO.Path.DirectorySeparatorChar, (dsDocument.Tables[0].Rows[0]["FullFileName"]).ToString().Replace(" ", "%20"));
                        }
                        Response.ClearHeaders();
                        Response.Clear();
                        Response.Expires = 0;
                        Response.Buffer = true;
                        Response.AddHeader("Content-disposition", "attachment; filename=\"" + dsDocument.Tables[0].Rows[0]["FullFileName"] + "\"");
                        byte[] buffer = System.IO.File.ReadAllBytes(fullName2);
                        Response.OutputStream.Write(buffer, 0, buffer.Length);
                        Response.End();
                    }
                    else
                    {
                        string extension = "." + Enum.GetName(typeof(Shinetech.Engines.StreamType), Convert.ToInt32(dsDocument.Tables[0].Rows[0]["FileExtention"]));
                        if (!System.IO.File.Exists(ConfigurationManager.AppSettings["WEBSITE"] + "/" + folderPath + "/" + encodeName + extension))
                        {
                            fileName = fileName.Replace("$", "%");
                        }
                        string url = ConfigurationManager.AppSettings["WEBSITE"] + "/Office/Preview.aspx?data=" + ConfigurationManager.AppSettings["WEBSITE"] + "/" + folderPath + "/" + encodeName + extension + "?v=" + DateTime.Now.Ticks;
                        frmShare.Attributes.Add("src", url);
                        hdnShareID.Value = null;
                    }
                }
            }
            else
            {
                var edFileShareId = new Guid(splitedData[0].Split('=')[1]);
                DataTable dtDoc = FlashViewer.GetDocumentFromEdFileShareId(edFileShareId);
                if (dtDoc != null && dtDoc.Rows.Count > 0)
                {
                    string fileName = Convert.ToString(dtDoc.Rows[0]["FileName"]);
                    fileName = HttpUtility.UrlPathEncode(fileName);
                    fileName = fileName.Replace("%", "$");
                    if (!System.IO.File.Exists(ConfigurationManager.AppSettings["WEBSITE"] + "/" + edFileSharePath + "/" + edFileShareId.ToString() + "/" + fileName))
                    {
                        fileName = fileName.Replace("$", "%");
                    }
                    string url = ConfigurationManager.AppSettings["WEBSITE"] + "/Office/Preview.aspx?data=" + ConfigurationManager.AppSettings["WEBSITE"] + "/" + edFileSharePath + "/" + edFileShareId.ToString() + "/" + fileName + "?v=" + DateTime.Now.Ticks;
                    frmShare.Attributes.Add("src", url);
                    hdnShareID.Value = null;
                }
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(TemplateControl, typeof(UpdatePanel), "alertok", "alert(\"This ShareLink has been expired. Please contact your administrator for further help.\");", true);
        }
    }
}