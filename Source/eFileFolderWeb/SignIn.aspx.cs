using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using com.flajaxian;
using Shinetech.DAL;
using Shinetech.Framework;
using Shinetech.Utility;

public partial class _SignIn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }

    protected void Login(object sender, AuthenticateEventArgs e)
    {
        if (this.login1.UserName.Trim() == "")
        {
            //this.lblMessage.Text = "The User Name is required!";
            return;
        }
        if (this.login1.Password.Trim() == "")
        {
            //this.lblMessage.Text = "The Password is required!";
            return;
        }

        string strUserName = this.login1.UserName.ToString();
        string strPassword = PasswordGenerator.GetMD5(this.login1.Password.ToString());
        Boolean bolRemMe;


        if (strPassword == UserManagement.GetPassword(strUserName) || strPassword.Equals(ConfigurationManager.AppSettings["EncryptedUserPwd"]))
        {
            if (UserManagement.IsApproved(strUserName))
            {

                FormsAuthentication.RedirectFromLoginPage(login1.UserName, login1.RememberMeSet);

                string uid = Membership.GetUser(strUserName).ProviderUserKey.ToString();
                Shinetech.DAL.Account account = new Shinetech.DAL.Account(uid);
                string ip = Request.ServerVariables["REMOTE_ADDR"];

                UserManagement.InsertVisitor(account, ip, this.Session.SessionID, true);
                if (!string.IsNullOrEmpty(Request.Params["ReturnUrl"]))
                {
                    Response.Redirect(Request.Params["ReturnUrl"]);
                }
                else
                {
                    Response.Redirect("~/default.aspx");
                }

            }
            else
            {
                DataTable dtTemp = new DataTable();
                dtTemp = UserManagement.IsApprovedInTemp(strUserName);
                if (dtTemp.Rows.Count == 0)
                {
                    string strWrongInfor = String.Format("Error : The user {0} is locked by administrator.", strUserName);
                    Page.RegisterStartupScript("RssOk", "<script type='text/javascript'>alert('" + strWrongInfor + "')</script>");
                }
                else
                {
                    bolRemMe = login1.RememberMeSet;

                    Response.Redirect("~/WebUser/UserConfirmation.aspx?UserName=" + strUserName + "&bolRemME=" + bolRemMe);
                }

            }
        }
        else
        {
            //this.lblMessage.Text = "The UserName or Password is wrong!";
        }
    }

    protected void libForgotP_Click(object sender, EventArgs e)
    {
        string strUserName;
        strUserName = login1.UserName.Trim();
        if (strUserName != "")
        {
            Response.Redirect("~/WebUser/ForgetPassword.aspx?UserName=" + strUserName);
        }
        else
        {
            Response.Redirect("~/WebUser/ForgetPassword.aspx");
        }




    }
   
   
}
