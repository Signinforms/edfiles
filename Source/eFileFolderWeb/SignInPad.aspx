﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignInPad.aspx.cs" EnableViewState="false"
    Inherits="SignInPad" Title="Sign In Pad" %>

<!DOCTYPE HTML>
<html lang="en" >
<head id="Head1" runat="server" class="ui-mobile">
    <title>sketchpad</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/jquery.mobile-1.0rc2.css" />
    
    <link rel="stylesheet" type="text/css" href="css/signin.css" />
    <%--<link rel="stylesheet" type="text/css" href="css/keyboard.css" />--%>
    <link rel="stylesheet" href="css/loadingbox.css" />
    <script type="text/javascript" src="Scripts/jquery-1.6.4.js"></script>
    <script type="text/javascript" src="Scripts/bezier.js"></script>
    <script type="text/javascript" src="Scripts/path.js"></script>
    
    <%--<script type="text/javascript" src="Scripts/keyboard.js" charset="UTF-8"></script>--%>
    <script type="text/javascript" src="Scripts/base64.js" charset="UTF-8"></script>
    <script type="text/javascript" src="Scripts/canvas2image.js" charset="UTF-8"></script>
    <script type="text/javascript" src="Scripts/compress.js" charset="UTF-8"></script>
    <script type="text/javascript" src="Scripts/msgwindow.js" charset="UTF-8"></script>
    <script type="text/javascript" src="Scripts/jquery.mobile-1.0rc2.js"></script>
    <script type="text/javascript" src="Scripts/signin.js" charset="UTF-8"></script>
   <%-- <script type="text/javascript" src="Scripts/paper.js"></script>--%>
   
</head>
<body>
    <div >
        <form id="form1" runat="server" style="overflow: visible;">
        <!-- /first page -->
        <div style="min-height: 600px;" class="ui-page ui-body-c ui-page-active" tabindex="0"
            data-url="foo" data-role="page" id="foo">
            <div id="canvas_container_title" data-role="content" data-theme="c" id="one">
                <table id="title_sheet_sign" class="title_sheet">
                    <tr>
                        <td align="center" style="margin-bottom: 10px;">
                            <strong class="title_signin">Sign In Pad&trade;</strong><asp:HiddenField ID="hidFieldId"
                                runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="font-size: 13pt;">
                            Office Name:<strong>
                                <%=DoctorName%></strong>
                        </td>
                    </tr>
                    <tr>
                        <td class="title_sign_td" colspan="2">
                            1- Sign Name In Box Below
                        </td>
                    </tr>
                    <tr>
                        <td class="title_sign_td" colspan="2">
                            2- Print First, Last - Click Box For Keyboard
                        </td>
                    </tr>
                    <tr>
                        <td class="title_sign_td" colspan="2">
                            3- Answer Questions
                        </td>
                    </tr>
                    <tr>
                        <td class="title_sign_td" colspan="2">
                            4- Click save.
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td class="td_right_left" colspan="2">
                            <canvas id="sketchpad" style="background-color: White; border-color: Red; border-width: 2px; width:400px; height:100px;">Sorry, your browser is not supported.</canvas>
                        </td>
                    </tr>
                    <tr>
                        <td class="td_left">
                            <strong style="font-size: 20px">TYPE NAME</strong>
                        </td>
                        <td class="td_right">
                            <table id="printNameTable">
                                <tr id="tr_printName">
                                    <td>
                                        First
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100%; text-align: center;">
                                        <asp:TextBox ID="txBoxLastName" Width="90%" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Last
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 100%; text-align: center">
                                        <asp:TextBox ID="txBoxFirstName" Width="90%" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="height: 15px;">
                        <td colspan="2" class="td_Separator">
                        </td>
                    </tr>
                </table>
            </div>
            <div style="margin: 0 auto; width: 600px; text-align: center;" data-theme="b" data-role="footer">
                <table style="width: 80%; margin: 0 auto;">
                    <tr>
                        <td>
                            <a data-role="button" data-theme="b" href="javascript:emptyFields();">Clear</a>
                        </td>
                        <td style="width: 50%">
                            &nbsp;&nbsp;
                        </td>
                        <td>
                            <a data-theme="b" data-role="button" onclick="javascript:doSwitchPage();" data-transition="slide">
                                Continue</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <!-- /second page -->
        <div data-url="two" data-role="page" id="two" data-theme="c">
            <div role="banner" class="ui-header ui-bar-b" data-role="header">
                <h1 aria-level="1" role="heading" tabindex="0" class="ui-title">
                    Questions</h1>
            </div>
            <div id="canvas_container" data-role="content">
                <asp:Repeater ID="Repeater3" runat="server">
                    <HeaderTemplate>
                        <table border="0" style="border-top-width: 1px;" align="center" cellpadding="0" cellspacing="0">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="left" class="td_left">
                                <asp:HiddenField ID="hidField2" runat="server" Value='<%# Eval("QuestionID")%>' />
                                <strong style="font-size: 20px">
                                    <asp:Label Text='<%# Eval("QuestionTitle")%>' runat="server"></asp:Label></strong>
                            </td>
                            <td class="td_center_textbox" align="center">
                                <asp:TextBox runat="server" ID="txtBoxContent1" Text="" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <SeparatorTemplate>
                        <%--<tr style="height: 15px;">
                            <td colspan="2" class="td_Separator">
                            </td>
                        </tr>--%>
                    </SeparatorTemplate>
                    <FooterTemplate>
                        <tr style="height: 15px;">
                            <td colspan="2" class="td_Separator">
                            </td>
                        </tr>
                        </table></FooterTemplate>
                </asp:Repeater>
                <asp:Repeater ID="Repeater2" runat="server">
                    <HeaderTemplate>
                        <table border="0" align="center" cellpadding="0" cellspacing="0">
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="left" class="td_left">
                                <asp:HiddenField ID="hidField1" runat="server" Value='<%# Eval("QuestionID")%>' />
                                <strong style="font-size: 20px">
                                    <asp:Label Text='<%# Eval("QuestionTitle")%>' runat="server"></asp:Label></strong>
                            </td>
                            <td class="td_center" align="center">
                                <input class="hide" type="hidden" id="inputvalue" value="N" />
                                <asp:RadioButtonList ID="CheckList1Answer" CssClass="rd_list" runat="server" AutoPostBack="False"
                                    TextAlign="Right" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="2">
                                    <asp:ListItem Enabled="True" Text="Yes" Value="Y" Selected="False" />
                                    <asp:ListItem Enabled="True" Text="No" Value="N" Selected="True" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <SeparatorTemplate>
                        <%--<tr style="height: 15px;">
                            <td colspan="2" class="td_Separator">
                            </td>
                        </tr>--%>
                    </SeparatorTemplate>
                    <FooterTemplate>
                        <tr style="height: 15px;">
                            <td colspan="2" class="td_Separator">
                            </td>
                        </tr>
                        </table></FooterTemplate>
                </asp:Repeater>
            </div>
            <div style="margin: 0 auto; width: 600px; text-align: center;" data-theme="b" data-role="footer">
                <table style="width: 85%; margin: 0 auto;">
                    <tr>
                        <td>
                            <a href="#one" data-direction="reverse" data-role="button" data-theme="b">Back</a>
                        </td>
                        <td>
                        </td>
                        <td>
                            <a data-theme="b" data-role="button" href="javascript:emptyFields();">Clear</a>
                        </td>
                        <td>
                        </td>
                        <td>
                            <a data-theme="b" data-role="button" onclick="javascript:doSave();">Save</a>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div data-url="progress" data-role="page" id="progress" data-theme="c">
            <div role="banner" class="ui-header ui-bar-b" data-role="header">
                <h1 aria-level="1" role="heading" tabindex="0" class="ui-title">
                    Saving</h1>
            </div>
            <div data-role="content" data-theme="c" id="one" align="center">
                <p>
                    Please wait...</p>
            </div>
        </div>
        <div data-url="status" data-role="page" id="status" data-theme="c">
            <div role="banner" class="ui-header ui-bar-b" data-role="header">
                <h1 aria-level="1" role="heading" tabindex="0" class="ui-title">
                    Sign In Successfull, Welcome.</h1>
            </div>
            <div data-role="content" data-theme="c" id="one" align="center">
                <p>
                    Please be seated...someone will be with you shortly.</p>
                <p>
                    <a data-role="button" style="width: :80px;" data-theme="b" onclick="javascript:reloadSignIn();">
                        Done</a></p>
            </div>
        </div>
        <div data-url="error" data-role="page" id="error" data-theme="c">
            <div role="banner" class="ui-header ui-bar-b" data-role="header">
                <h1 aria-level="1" role="heading" tabindex="0" class="ui-title">
                    Failed to Sign In.</h1>
            </div>
            <div data-role="content" data-theme="c" id="one" align="center">
                <p>
                    Please try input again...</p>
                <p>
                    <a data-role="button" data-theme="c" onclick="javascript:reloadSignIn();">Exit</a></p>
            </div>
        </div>
        </form>
    </div>
</body>
</html>
