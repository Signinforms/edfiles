using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using Shinetech.DAL;

public partial class SignInPad2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Request.IsAuthenticated)
        { 
            Response.Redirect("~/LoginSignIn.aspx",true);
            return;
        }

        if (!Page.IsPostBack)
        {
            List<QuestionEntity> list = new List<QuestionEntity>();

            //search the customized questions
            string doctorId = Sessions.SwitchedSessionId;
            //string doctorId = Membership.GetUser().ProviderUserKey.ToString();

            hidFieldId.Value = doctorId;
            DataTable doctorTable = UserManagement.GetDoctorQuestions(doctorId);

            foreach (DataRow dataRow in doctorTable.Rows)
            {
                QuestionEntity entity = new QuestionEntity();
                entity.QuestionID = Convert.ToInt32(dataRow["QuestionID"]);
                entity.QuestionTitle = Convert.ToString(dataRow["QuestionTitle"]);
                entity.OfficeID = doctorId;

                list.Add(entity);
            }

            if(list.Count==0)//use the default ones
            {
                DataTable table = UserManagement.GetQuestionDefines();

                foreach (DataRow dataRow in table.Rows)
                {
                    QuestionEntity entity = new QuestionEntity();
                    entity.QuestionID = Convert.ToInt32(dataRow["QuestionID"]);
                    entity.QuestionTitle = Convert.ToString(dataRow["QuestionTitle"]);
                    if (!string.IsNullOrEmpty(entity.QuestionTitle))
                    {
                        list.Add(entity);
                    }
                }
            }

            this.Repeater2.DataSource = list;
            this.Repeater2.DataBind();
        }
    }

    public QuestionEntity FindQuestion(int qid, List<QuestionEntity> questions)
    {
        foreach (QuestionEntity item in questions)
        {
            if (item.QuestionID == qid)
            {
                return item;
            }
        }

        return null;
    }

    public  string DoctorName
    {
        get
        {
            string doctorId = Sessions.SwitchedSessionId;
            //string doctorId = Membership.GetUser().ProviderUserKey.ToString();
            Account account = new Account(doctorId);
            return account.Lastname.Value + ", " + account.Firstname.Value;
        }
    }

}
