﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignInPad3.aspx.cs" EnableViewState="false" Inherits="SignInPad3"
    Title="Sign In Pad" %>

<!DOCTYPE HTML>
<html lang="en">
<head id="Head1" runat="server">
     <title>sketchpad</title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=768px, maximum-scale=1.0">
    <link rel="stylesheet" href="css/jquery.mobile-1.0a3.min.css" />
    <script type="text/javascript" src="Scripts/jquery.mobile-1.0a3.js"></script>
    <link rel="stylesheet" type="text/css" href="css/signin.css"/>
    <link rel="stylesheet" type="text/css" href="css/keyboard.css"/>
    <link rel="stylesheet" href="css/loadingbox.css" />
    <script type="text/javascript" src="Scripts/bezier.js"></script>
    <script type="text/javascript" src="Scripts/path.js"></script> 
    <script type="text/javascript" src="Scripts/jquery-1.4.4.min.js"></script>
    <script type="text/javascript" src="Scripts/keyboard.js" charset="UTF-8"></script>
    <script type="text/javascript" src="Scripts/base64.js" charset="UTF-8"></script>
    <script type="text/javascript" src="Scripts/canvas2image.js" charset="UTF-8"></script>
    <script type="text/javascript" src="Scripts/compress.js" charset="UTF-8"></script>
    <script type="text/javascript" src="Scripts/msgwindow.js" charset="UTF-8"></script>
    <script type="text/javascript" src="Scripts/signin.js" charset="UTF-8"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="container">
       <div id="canvas_container_title">
        <table id="title_sheet_sign" class="title_sheet"><tr >
            <td align="center" style="margin-bottom:30px;"><strong class="title_signin"> Sign In Pad&trade;</strong><asp:HiddenField ID="hidFieldId" runat="server" /></td></tr>
            <tr><td align="center" style="font-size:13pt;">Office Name:<strong> <%=DoctorName%></strong></td></tr>
            <tr><td class="title_sign_td" colspan="2" >1-  Sign Name In Box Below</td></tr>
            <tr><td class="title_sign_td" colspan="2" >2-  Print First, Last - Click Box For Keyboard</td></tr>
            <tr><td class="title_sign_td" colspan="2">3-  Answer Questions</td></tr>
            <tr><td class="title_sign_td" colspan="2">4-  Click save.</td></tr>
        </table> 
       </div>
        <div id="canvas_container">
            <asp:Repeater ID="Repeater2" runat="server">
                <HeaderTemplate>
                    <table border="0" align="center" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="td_right_left" colspan="2">
                                <canvas id="sketchpad" style="background-color:White; border-color:Red; border-width:2px;">Sorry, your browser is not supported.</canvas>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_left">
                                <strong style="font-size: 20px">TYPE NAME</strong>
                            </td>
                            <td class="td_right">
                            <table id="printNameTable"><tr id="tr_printName"><td>First</td><td>Last</td></tr>
                                <tr><td style="width: 145px"><asp:TextBox ID="txBoxLastName" Width="140px" runat="server"  class="keyboardInput"/>
                                </td><td style="width: 145px"><asp:TextBox ID="txBoxFirstName" Width="140px" runat="server" class="keyboardInput" /></td>
                                </tr>
                            </table>
                                
                            </td>
                        </tr>
                        <tr style="height: 15px;">
                            <td colspan="2" class="td_Separator">
                            </td>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="left" class="td_left">
                            <asp:HiddenField ID="hidField1" runat="server" Value='<%# Eval("QuestionID")%>' />
                            <strong style="font-size: 20px">
                                <asp:Label Text='<%# Eval("QuestionTitle")%>' runat="server"></asp:Label></strong>
                        </td>
                        <td class="td_center" align="center">
                            <input class="hide" type="hidden" id="inputvalue" value="N" />

                           <asp:RadioButtonList ID="CheckList1Answer" CssClass="rd_list" runat="server"  AutoPostBack="False" TextAlign="Right" RepeatDirection="Horizontal" RepeatLayout="Table"
                               RepeatColumns="2">
                                <asp:ListItem Enabled="True" Text="Yes" Value="Y" Selected="False" />
                                <asp:ListItem Enabled="True" Text="No" Value="N" Selected="True" />
                           </asp:RadioButtonList>
                           
                          
                        </td>
                    </tr>
                </ItemTemplate>
                <SeparatorTemplate>
                    <tr style="height: 15px;">
                        <td colspan="2" class="td_Separator">
                        </td>
                    </tr>
                </SeparatorTemplate>
                <FooterTemplate>
                    <tr style="height: 15px;">
                        <td colspan="2" class="td_Separator">
                        </td>
                    </tr>
                    </table></FooterTemplate>
            </asp:Repeater>
        </div>
        <div style="margin:0 auto; width:600px; text-align:center;">
            <table style="width:100px;margin:0 auto;">
                <tr>
                    <td class="td_save">
                        <a id="linkButtonClear" class="save_url" href="javascript:emptyFields();">Clear</a>
                    </td>
                    <td>&nbsp;&nbsp;</td>
                    <td class="td_save"><a id="linkButtonSave" class="save_url" href="javascript:doSave();">Save</a></td>
                </tr>              
            </table>
        </div>
    </div>
    </form>
</body>
</html>
