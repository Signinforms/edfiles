<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="StorageConversion.aspx.cs" Inherits="StorageConversion" Title="" %>
<%@ Register Src="~/Controls/RegisterDirectly.ascx" TagName="ucRegisterDirectly" TagPrefix="ucRegisterDirectly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        h3 {
            padding-bottom:0px;
        }
        p span {
            display:inline-block;
        }
    </style>
    <div class="title-container aboutus-title" style="padding-bottom:0px;">
        <div class="inner-wrapper">
            <h1 style="margin-top:15px;">Storage Conversion</h1>
            <p>�Store, Organize and Retrieve Information, NOT Paper.</p>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="inner-wrapper">
        <div class="page-container">
            <div class="inner-wrapper">
                <div class="aboutus-contant">
										<div class="left-content">
											<div class="work_content">
												<p>
														We have made converting paper file folders into edFiles very simple. Depending on how long you need to store your files and how much you are  currently spending on storing & accessing files, we have 3 simple options:
												</p>
												<h3 class="sub-title"><a href="Estimator.aspx">Option 1</a>:</h3>
												<p>														
														File Imaging/Scanning Service
														<br />
														SCAN EVERYTHING NOW.<br />
														Eliminate the paper storage all together and put everything online or CDs/External Drives. Retrieve files at your fingertips and save $1000s on storage and time.
												</p>
												<h3 class="sub-title"><a href="SmartStorage.aspx">Option 2</a>:</h3>
												<p>		
														SmartStorage Service
														<br />
														SCAN ON DEMAND<br />
														No Retrieval Costs. Eliminate the hassle/burden of searching for files  in storage units or paying file retrieval & delivery fees for traditional Record Storage Services.
												</p>
												<h3 class="sub-title"><a href="ConversionProgram.aspx">Option 3</a>: </h3>
												<p>			
														Storage Conversion
														<br />
														Program SCAN OVER TIME<br />
														Instead of letting your boxes sit in storage collecting dust, they are gradually scanned, backed up and then destroyed.
												</p>
												<p>
														To get started, Call <span class="blue">1-657-217-3260</span> or click on the link for each program for further details.
												</p>
											</div>
										</div>
										<div class="right-content">
											<ucRegisterDirectly:ucRegisterDirectly ID="ucRegister" runat="server" />
										</div>                    
                </div>
            </div>
        </div>
    </div>
</asp:Content>
