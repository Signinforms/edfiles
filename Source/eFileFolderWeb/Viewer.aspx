﻿<%@ Page Title="" Language="C#" MasterPageFile="~/BlankMasterPage.master" AutoEventWireup="true" CodeFile="Viewer.aspx.cs" Inherits="Viewer" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="css/Signature.css" rel="stylesheet" />
    <link href="css/signature-pad.css" rel="stylesheet" />
    <link type="text/css" rel="stylesheet" href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>css/jquery-ui-1.8.22.custom.css" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/css/colorbox.css?v=1" rel="stylesheet" />
    <link href="<%= ConfigurationManager.AppSettings["VirtualDir"]%>css/main.css?v=3" rel="stylesheet" />
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/js/jquery.colorbox-min.js" type="text/javascript"></script>
    <script src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>New/js/edFiles/EdFilesLogs.js?v=4" type="text/javascript"></script>
    <script type="text/javascript" src="<%= ConfigurationManager.AppSettings["VirtualDir"]%>extras/jquery-ui-1.8.22.custom.min.js"></script>
    <style type="text/css">
        .pdfpage
        {
            position: relative;
            top: 0;
            left: 0;
            /*border: solid 1px black;*/
            /*margin: 10px;*/
        }

            .pdfpage > canvas
            {
                position: absolute;
                top: 0;
                left: 0;
            }

            .pdfpage > div
            {
                position: absolute;
                top: 0;
                left: 0;
            }

        .inputControl
        {
            background: transparent;
            border: 0px none;
            position: absolute;
            margin: auto;
            font-size: 13px;
            color: black !important;
        }

            .inputControl[type='checkbox']
            {
                margin: 0px;
                width: 18px;
                height: 18px;
            }

        .inputHint
        {
            border: 1px solid #d8dadd;
            /*opacity: 1;*/
            background: #fff;
            /*opacity: 1;
        background: #ccc;*/
            position: absolute;
        }

        .modal_dialog
        {
            width: 1300px !important;
        }

        .ui-dialog
        {
            width: 450px !important;
            left: 750px;
            font-size: 13px;
        }

            .ui-dialog label
            {
                font-weight: normal;
                margin-bottom: 0px;
            }

        #divContent
        {
            width: auto !important;
        }

        .box
        {
            width: 200px;
        }


        .signature-btn
        {
            background-image: url(images/signature.png) !important;
            background-repeat: no-repeat !important;
            background-position: center !important;
        }

        .ui-widget-header
        {
            background-image: none !important;
            background-color: #83af33;
            border: 0px !important;
            border-radius: 0px;
        }

        .modal_dialog .modal-header
        {
            padding: 0px;
        }

        .float-l
        {
            float: left;
        }

        .float-r
        {
            float: right;
        }

        .signature-popup
        {
            width: 100%;
            max-width: 750px;
            position: fixed;
            z-index: 100001;
            left: 0;
            top: 195px;
            right: 0;
            margin: auto;
        }

            .signature-popup .popup-btn
            {
                display: inline-block;
            }

                .signature-popup .popup-btn .btn
                {
                    margin-right: 10px;
                }

            .signature-popup .ajax__tab_header
            {
                display: table;
                width: 100%;
            }

                .signature-popup .ajax__tab_header > span
                {
                    display: inline-block;
                }

                .signature-popup .ajax__tab_header .ajax__tab_tab > span
                {
                    padding: 5px 20px;
                    min-width: 100px;
                }

            .signature-popup .popup-scroller input
            {
                width: 100%;
            }

        .input-control
        {
            width: 100%;
            display: inline-block;
        }

        .cedarville-row
        {
            width: 100%;
            display: tabel;
        }

            .cedarville-row .input-control
            {
                width: 30%;
            }

        .modalParentDiv
        {
            border: 1px solid #d8dadd;
            padding: 3px 2px 2px 2px;
            height: 50px;
            width: 200px;
            position: absolute;
        }

        .imagePosition
        {
            position: absolute;
        }

        .btn
        {
            padding: 0px 0px 0px 0px !important;
        }

        .ajax__tab_xp .ajax__tab_body
        {
            border: 0px;
            border-bottom: 1px solid #999999;
        }

        .cancel-btn
        {
            background-color: transparent;
            background-image: url(images/cancel-signature.png);
            background-repeat: no-repeat;
            cursor: pointer;
        }

            .cancel-btn:hover, .cancel-btn:focus
            {
                opacity: 0.7;
            }

        .popup-scroller > div
        {
            padding: 15px;
        }

        .popup-scroller .ajax__tab_body
        {
            padding: 15px 0px !important;
        }

        .popup-scroller .ajax__tab_header span, .signature-popup .ajax__tab_header .ajax__tab_tab > span
        {
            padding: 3px 10px;
        }

            .popup-scroller .ajax__tab_header span.ajax__tab_active
            {
                padding: 6px 10px;
            }

        .cedarville-row .input-control
        {
            background-color: #EEEEEE;
            border-color: #DDDDDD;
            font-size: 14px;
            height: auto;
            padding: 8px 5px;
        }

            .cedarville-row .input-control.SelectedFont, .cedarville-row .input-control:hover, .cedarville-row .input-control:focus
            {
                background-color: #C2DC91;
            }


        .ajax__tab_xp .ajax__tab_body
        {
            border: 0px;
        }

        .ajax__tab_xp .ajax__tab_header
        {
            background-image: none;
        }

        .popup-btn
        {
            background-color: #eaeaea;
        }

        .btn.green.signature-btn, .btn-small.green.signature-btn
        {
            background-color: transparent;
            background-repeat: no-repeat;
        }

            .btn-small.green.signature-btn:hover, .btn-small.green.signature-btn:focus
            {
                opacity: 0.7;
            }

        .modalBody
        {
            overflow: hidden;
        }

        #overlay
        {
            display: none;
            position: absolute;
            top: 0;
            bottom: 0;
            background: #999;
            width: 100%;
            height: 100%;
            opacity: 0.8;
            z-index: 99999;
        }

        .header-container .logo a img
        {
            max-height: 100px;
        }

        #floatingCirclesG
        {
            top: 50%;
            left: 50%;
            position: fixed;
        }

        .ui-dialog .ui-dialog-buttonpane button
        {
            float:left;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            if ('<%= hdnShareID.Value%>' != "" && ($('#' + '<%= errorMessageText.ClientID%>').val() != 'ShareID has been expired' && (($('#' + '<%= errorMessageText.ClientID%>').val() != 'Not a valid ShareID') || ('<%= PDFUrl%>' != '')))) {
                $('#dialog-share-book').dialog({
                    autoOpen: false,
                    width: 450,
                    //height: 540,
                    resizable: false,
                    modal: true,
                    buttons: {
                        "Submit": function () {
                            SaveMail();
                        },
                        "Close": function () {
                            $(this).dialog("close");
                            $('#dialog-message-all').dialog({
                                open: function () {
                                    $("#spanText").text("Please submit details to view this link.");
                                }
                            });
                        }
                    },
                    close: function () {
                        $('#dialog-message-all').dialog({
                            open: function () {
                                $("#spanText").text("Please submit details to view this link.");
                            }
                        });
                    }
                });
                showShare();
            }
        });

        function showShare() {
            $('#dialog-share-book').dialog("open");
            $('#dialog-share-book input[name="email"]').val('');
            $('#dialog-share-book input[name="name"]').val('');
            $('#dialog-share-book textarea[name="content"]').val('');
            $('#dialog-share-book input[name="email"]').css('border', '1px solid black');
            $('#dialog-share-book input[name="emailto"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="email"]')) });
        }

        function SaveMail() {
            var email = $('#dialog-share-book input[name="email"]').val();
            var name = $('#dialog-share-book input[name="name"]').val();
            var message = $('#dialog-share-book textarea[name="content"]').val();
            var Isvalid = false;

            //$('#dialog-share-book input[name="myname"]').keyup(function () { CheckValidation($('#dialog-share-book input[name="myname"]')) });
            //$('#dialog-share-book input[name="email"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="email"]')) });

            $('#dialog-share-book input[name="email"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="email"]')) });
            if (name == "") {
                $('#dialog-share-book input[name="name"]').css("border", "solid 1px red");
                return false;
            }
            else {
                $('#dialog-share-book input[name="name"]').css("border", "solid 1px black");
            }
            //if (email == "" || name == "" || emailto == "") {
            if (email == "") {
                //CheckValidation($('#dialog-share-book input[name="email"]'));
                //CheckValidation($('#dialog-share-book input[name="myname"]'));
                CheckValidation($('#dialog-share-book input[name="email"]'));
                //CheckEmailValidation($('#dialog-share-book input[name="email"]'));
                CheckEmailValidation($('#dialog-share-book input[name="email"]'))
                return false;
            }

            //if (!Isvalid) {

            //    Isvalid = CheckEmailValidation($('#dialog-share-book input[name="email"]'));
            //    if (!Isvalid)
            //        return Isvalid;
            //}

            Isvalid = CheckEmailValidation($('#dialog-share-book input[name="email"]'));
            if (!Isvalid)
            { return Isvalid; }
            if ($("input[id='check1']:checked").length < 1) {
                alert("You must select the checkbox to access link.");
                return false;
            }


            //var url = "<%= ConfigurationManager.AppSettings["VirtualDir"]%>" + oldPath;
            //url = url.replace(/\\/g, "/");

            //var src = 'http://docs.google.com/gview?url=' + window.location.origin + url + '&embedded=true';

            var src = '';
            showLoader();
            $.ajax(
                {
                    type: "POST",
                    url: 'EFileFolderJSONService.asmx/SaveShareLinkLog',
                    data: "{ strEmail:'" + email + "', strMailInfor:'" + message + "', strName:'" + name + "',shareId:'" + <%= hdnShareID.Value%> + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var id = data.d;
                        var title, message;
                        if (id === 0) {
                            title = "Error";
                            message = "Something went wrong. Please try again later."
                        }
                        else {
                            title = "Success";
                            message = "Thankyou for your feedback."
                        }

                        $('#dialog-message-all').dialog({
                            title: title,
                            open: function () {
                                $("#spanText").text(message);
                            }
                        });

                        $('#dialog-message-all').dialog("open");

                        if (id > 0) {
                            $('#dialog-share-book').dialog("close");
                            $('#mainContent').css("display", "block");
                        }
                        hideLoader();
                    },
                    fail: function (data) {

                        $('#dialog-message-all').dialog({
                            title: "Error",
                            open: function () {
                                $("#spanText").text("Something went wrong. Please try again later.");
                            }
                        });
                        $('#dialog-message-all').dialog("open");

                        hideLoader();
                    }
                });
            }

            function CheckValidation(fieldObj) {
                if (fieldObj.val() == "") {
                    fieldObj.css('border', '1px solid red');
                }
                else {
                    fieldObj.css('border', '1px solid black');
                }
            }

            function CheckEmailValidation(fieldObj) {

                var x = fieldObj.val();

                if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(x)) {
                    fieldObj.css('border', '1px solid black');
                    return true;
                }
                else {
                    fieldObj.css('border', '1px solid red');

                    return false;
                }
            }
    </script>

    <div class="title-container">
        <div class="inner-wrapper">
            <h1 style="margin-bottom:5px;margin-top:5px;">Ed File Share</h1>
            <div class="clearfix"></div>
        </div>
    </div>
     <div id="overlay">
            <div id="floatingCirclesG">
                <div class="f_circleG" id="frotateG_01"></div>
                <div class="f_circleG" id="frotateG_02"></div>
                <div class="f_circleG" id="frotateG_03"></div>
                <div class="f_circleG" id="frotateG_04"></div>
                <div class="f_circleG" id="frotateG_05"></div>
                <div class="f_circleG" id="frotateG_06"></div>
                <div class="f_circleG" id="frotateG_07"></div>
                <div class="f_circleG" id="frotateG_08"></div>
            </div>
      </div>

    <div id="dialog-share-book" title="Receiver Confirmation" style="display: none">
        <%--<p class="validateTips">All form fields are required.</p>--%>
        <br />

        <label for="name">Name</label><span style="color:red;">*</span><br />
        <input type="text" name="name" id="name" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <label for="email">Email</label><span style="color:red;">*</span><br />
        <input type="email" name="email" id="email" value="" class="text ui-widget-content ui-corner-all" style="border: 1px solid black" /><br />
        <label for="content">Message</label><br />
        <textarea name="content" id="content" rows="2" cols="25" class="text ui-widget-content ui-corner-all" style="border: 1px solid black"></textarea><br />
        <input type="checkbox" id="check1" />
            <span style="font-size:12px;">Please note by viewing the shared file, you are accepting to maintain confidentiality of the shared file.
                  Any further dissemination of the shared file is strictly prohibited without written consent of the organization
                 or the individual whom is sharing this file with you.  A log of this share and the time and date, along with your
                 ip address will be kept for compliance.
                <br /><br />
                EdFiles.com</span>
    </div>
    <div id="dialog-message-all" title="" style="display: none;">
        <p id="dialog-message-content">
            <span class="ui-icon ui-icon-circle-check" style="float: left; margin: 0 7px 50px 0;"></span>
            <span id="spanText"></span>
        </p>
    </div>
    <div class="page-container register-container " id="mainContent" style="display:none">
        <div class="inner-wrapper">
            <div style="padding-top: 10px;">
                <div class="alert alert-danger alert-dismissible" role="alert" id="errorMsgRegion1" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                </div>
                <div class="alert alert-success alert-dismissible" role="alert" id="successMsgRegion1" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                </div>

                <input type="hidden" name="Data" id="pdfData" value="Data" />
                <input type="hidden" name="PDFURL" id="PDFURL" value="PDFURL" />
                <input type="hidden" name="PDFSource" id="PDFSource" value="PDFSource" />
                <input type="hidden" name="JobId" id="JobId" value="JobId" />
                <input type="hidden" name="DocId" id="DocId" value="DocId" />


            </div>
            <div class="form-containt listing-contant ">
                <div class="content-box listing-view">

                    <div id="viewer"></div>

                    <asp:Panel class="popup-mainbox" ID="DivDeleteConfirmation" Style="display: none"
                        runat="server">
                       <%-- <asp:UpdatePanel ID="updateParent" runat="Server">
                            <ContentTemplate>--%>
                                <div>
                                    <div class="popup-head" id="PopupHeader">
                                        Submit File
                                    </div>
                                    <div class="popup-scroller">
                                        <p style="margin: 20px">
                                            Once you submit this document, you will not be able to make any additional changes.  Are you ready to submit?
                                        </p>
                                    </div>
                                    <div class="popup-btn">
                                        <input id="ButtonDeleleOkay" type="button" value="Yes" class="btn green" style="margin-right:5px;"/>

                                        <input id="ButtonDeleteCancel" type="button" value="No" class="btn green" />

                                        <input id="ButtonOkay" type="button" value="Yes" class="btn green" style="display:none;"/>

                                <input id="ButtonCancel" type="button" value="No" class="btn green" style="display:none"/>
                                    </div>
                                </div>
                    </asp:Panel>
                        <ajaxToolkit:ModalPopupExtender ID="lnkDelete_ModalPopupExtender" runat="server"
                        CancelControlID="ButtonDeleteCancel" TargetControlID="btnSubmit"
                        PopupControlID="DivDeleteConfirmation" BackgroundCssClass="ModalPopupBG">
                    </ajaxToolkit:ModalPopupExtender>
                    <ajaxToolkit:ConfirmButtonExtender ID="ConfirmButtonExtender1" runat="server" DisplayModalPopupID="lnkDelete_ModalPopupExtender"
                        ConfirmText="" TargetControlID="btnSubmit">
                    </ajaxToolkit:ConfirmButtonExtender>
                    <asp:HiddenField ID="errorMessageText" runat="server"/>
                    <div style="float: right">
                        <asp:Button class=" btn green " Style="width: 150px; margin-right: 5px;" runat="server" name="Save" ID="btnSave" Text="Save" ToolTip="Save and complete this form later." />
                        <asp:Button class=" btn green " Style="width: 150px; margin-right: 5px;" runat="server" name="Submit" ID="btnSubmit" Text="Submit" />
                       
                        <asp:Button class=" btn green " Style="width: 150px; margin-right: 5px;" runat="server" name="Download" ID="btnDownload" Text="download" OnClick="btnDownload_Click" />
                        <button class="cancel-btn  btn cancelSign" style="display: none;" id="btnCancelSign" title="Cancel"></button>
                        <div id="modalDiv" style="display: none">
                            <button class="signature-btn btn green modalTest" id="btnModalPopup" style="top: 0px;background-color:white;" title="Sign"></button>
                        </div>
                        <canvas id="canvasSaveValue" style="display: none; background-color: white"></canvas>
                    </div>

                    

                </div>
            </div>
        </div>

    </div>
    <asp:HiddenField ID="hdnShareID" runat="server"/>
    <asp:Button OnClick="hdnBtn_Click" ID="hdnBtn" runat="server" />

    <div class="popup-mainbox signature-popup"  Style="display: none;" id="DivSignature">
                       <div >
                            <div class="popup-head"">
                                <div class="signDialogHeaderText ">Type your signature</div>
                           </div>
                           <div class="popup-scroller">
                               <ajaxToolkit:TabContainer runat="server" ID="fileTabs" ActiveTabIndex="0" AutoPostBack="false">
                                   <ajaxToolkit:TabPanel runat="server" ID="panelText" HeaderText="Text" OnClientClick="Text()">
                                       <ContentTemplate>
                                           <div id="divContent">
                                               <div>
                                                   <div class="col-md-12 textinput" style="margin-top: 5px;">
                                                       <input class="input-control" id="txtSignatureInput" onkeyup="DisplayText()" placeholder="Signature"/>
                                                   </div>
                                               </div>
                                               <div class="cedarville-row">

                                                   <div class="box cedarville_cursive signbox input-control" id="divCedarville">Signature
                                                   </div>
                                                   <div class="box kristi signbox input-control" id="divKristi">Signature
                                                   </div>
                                                   <div class="box mr_dafoe signbox input-control" id="divDafoe">Signature
                                                   </div>
                                                   <div class="box sacramento box signbox input-control" id="divsacramento">Signature
                                                   </div>
                                                   <div class="box montez signbox input-control" id="divMontez">Signature
                                                   </div>
                                                   <div class="box reenie_beanie signbox input-control" id="divReenie_beanie">Signature
                                                   </div>
                                               </div>


                                           </div>
                                       </ContentTemplate>
                                   </ajaxToolkit:TabPanel>
                                   <ajaxToolkit:TabPanel runat="server" ID="panelDraw" HeaderText="Draw" OnClientClick="Draw()">
                                       <ContentTemplate>
                                           <div id="divContentSign" style="display: none">
                                               <div id="signature-pad">
                                                   <div align="center">
                                                       <canvas id="canvasDraw" style="cursor: default; width: 600px; height: 200px; background-color: white"></canvas>
                                                       <asp:HiddenField ID="ImgInformation" runat="server" />
                                                   </div>
                                               </div>
                                           </div>
                                       </ContentTemplate>
                                   </ajaxToolkit:TabPanel>
                               </ajaxToolkit:TabContainer>
                           </div>
                       </div>
                        <div class="popup-btn">
                            <div style="margin-left:5px">
                                   <div id="divGreen" class="penColour greenColour" val="green"></div>
                                   <div id="divBlue" class="penColour blueColour" val="blue"></div>
                                   <div id="divBlack" class="penColour blackColour blackColourActive" val="black"></div>
                               </div>
                            <div class="float-r">
                                <button class="btn green" id="modalCancel">Cancel</button>
                                <button class="btn green" id="btnSign">Sign</button>
                            </div>
                        </div>
                            </div>
    
    <div class="ModalPopupBG" style="position: fixed; left: 0px; top: 0px; z-index: 10000; width: 100%; height: 100%;display:none;"></div>
    
    
    <script src="lib/pdf/pdf.js"></script>
    <script src="js/signature_pad.js"></script>
    <script src="js/app.js"></script>
    <script src="js/jquery-ui.js" type="text/javascript"></script>

    <script type="text/javascript">
        var signType = "text";
        var signaturePad;
        var $modalOpenedFrom = "";

        //'use strict';
        $(function () {
            //$("#loading-image").hide();
            if ('<%= PDFUrl%>' != '') {
                init('<%= PDFUrl + "?h=" + DateTime.Now.Ticks%>');
            }
            else {
                if ($('#' + '<%= errorMessageText.ClientID%>').val() == 'ShareID has been expired') {
                    alert('This ShareLink has been expired. Please contact your administrator for further help.');
                }
                else {
                    alert('File is not available.');
                }
            }
        });

        var pdfItemsStr = '<%= PDFData %>';
        pdfItemsStr = pdfItemsStr.replace(/\\/g, '/');
        var pdfItems = [];
        if (pdfItemsStr)
            pdfItems = JSON.parse(pdfItemsStr);
        var pdfDoc = null,
            pageNum = 1,
            pageRendering = false,
            pageNumPending = null,
            pageLeftPending = 0,
            pageTopPending = 0,
            pdfPageWidth = 0,
            pdfPageHeight = 0,
            pdfRotate = 0;
        var formFields = {};
        var div = document.getElementById('viewer');

        /**
       * Asynchronously downloads PDF.
       */
        function init(fileName) {
            $("#loading-image").show();
            var path = fileName;
            $.ajaxSetup({ cache: false });
            PDFJS.workerSrc = "lib/pdf/pdf.worker.js"; //'../Scripts/pdf/pdf.worker.js';
            PDFJS.getDocument(path).then(function (pdfDoc_) {
                pdfDoc = pdfDoc_;
                // document.getElementById('page_count').textContent = pdfDoc.numPages;
                if (pdfDoc.numPages == 0)
                    $("#loading-image").hide();
                for (var i = 1; (pdfDoc.numPages == 1 ? i <= pdfDoc.numPages : i < pdfDoc.numPages) ; i++) {
                    // Initial/first page rendering
                    renderPage(i, 0, 0, true);
                }
            });
        }

        function removeOptions(selectbox) {
            var i;
            for (i = selectbox.options.length - 1; i >= 0; i--) {
                selectbox.remove(i);
            }
        }

        function addSelectBoxForPageAcroFieldsToForm(page) {
            return;
            page.getAnnotations().then(
                function (items) {
                    var selectField = document.getElementById("selectField");
                    removeOptions(selectField);
                    var optionsHTML = [];
                    if (items.length == 0) {
                        optionsHTML.push("<option value=\"console.log('no fields clicked - doing nothing');\">No fields</option>");
                    }
                    for (var i = 0; i < items.length; i++) {
                        var item = items[i];
                        switch (item.subtype) {
                            case 'Widget':

                                if (item.fieldType != 'Tx' && item.fieldType != 'Btn' && item.fieldType != 'Ch') {
                                    break;
                                }
                                var fieldName;
                                if (item.fieldType == 'Tx') {
                                    fieldName = 'Inputfield with key ' + item.fullName + ' (position: ' + item.rect + ')';
                                }
                                if (item.fieldType == 'Btn') {

                                    if (item.fieldFlags & 32768) {
                                        fieldName = 'Radiobutton with key ' + item.fullName + ' (position: ' + item.rect + ')';
                                    } else if (item.fieldFlags & 65536) {
                                        fieldName = 'Pushbutton with key ' + item.fullName + ' (position: ' + item.rect + ')';
                                    } else {
                                        fieldName = 'Checkbox with key ' + item.fullName + ' (position: ' + item.rect + ')';
                                    }
                                }
                                if (item.fieldType == 'Ch') {
                                    fieldName = 'Selectbox with key ' + item.fullName + ' (position: ' + item.rect + ')';
                                }

                                x = item.rect[0];
                                if (pdfRotate == 90) {
                                    y = pdfPageWidth - item.rect[1];
                                }
                                else if (pdfRotate == 0) {
                                    y = pdfPageHeight - item.rect[1];
                                }
                                else {
                                    //TODO: other rotates
                                }
                                optionsHTML.push("<option value=\"queueRenderPage(" + pageNum + "," + x + "," + y + ");\">" + fieldName + "</option>");
                        }
                    }
                    selectField.innerHTML = optionsHTML.join('\n');
                });
        }

        function renderPage(pageNum, xOffset, yOffset, renderAcroFieldSelect) {
            pageRendering = true;
            // Using promise to fetch the page
            pdfDoc.getPage(pageNum).then(function (page) {
                var scale = 1.5;
                var viewport = page.getViewport(scale);
                var pageDisplayWidth = viewport.width;
                var pageDisplayHeight = viewport.height;
                var pageDivHolder = document.createElement('div');
                pageDivHolder.className = 'pdfpage';
                pageDivHolder.style.width = (pageDisplayWidth + 1) + 'px';
                pageDivHolder.style.height = (pageDisplayHeight + 5) + 'px';
                div.appendChild(pageDivHolder);

                // Prepare canvas using PDF page dimensions
                var canvas = document.createElement('canvas');
                var context = canvas.getContext('2d');
                canvas.id = "CanvasPdf";
                canvas.width = pageDisplayWidth;
                canvas.height = pageDisplayHeight;
                pageDivHolder.appendChild(canvas);

                $('#popupViewer').find('.vertical-align-center').css('width', (pageDisplayWidth + 30) + 'px');

                // Render PDF page into canvas context
                var renderContext = {
                    canvasContext: context,
                    viewport: viewport
                };
                var renderTask = page.render(renderContext);

                // Wait for rendering to finish
                renderTask.promise.then(function () {
                    if (renderAcroFieldSelect) {
                        addSelectBoxForPageAcroFieldsToForm(page);
                    }
                    pageRendering = false;
                    if (pageNumPending !== null) {
                        // New page rendering is pending
                        renderPage(pageNumPending, pageLeftPending, pageTopPending);
                        pageNumPending = null;
                        pageLeftPending = 0;
                        pageTopPending = 0;
                    }
                });
                // Prepare and populate form elements layer
                var formDiv = document.createElement('div');
                pageDivHolder.appendChild(formDiv);
                setupForm(formDiv, page, viewport);
                // Update page counters



                if (pageNum >= pdfDoc.numPages) {
                    //document.getElementById('page_num').textContent = 1;
                    $("#loading-image").hide();
                    return;
                }
                pageNum++;
                queueRenderPage(pageNum, 0, 0, true);

            });
        }

        function setupForm(div, content, viewport) {
            function bindInputItem(input, item) {
                if (input.name in formFields) {
                    var value = formFields[input.name];
                    if (input.type == 'checkbox') {
                        input.checked = value;
                    }
                    else if (this.type == 'radio') { input.checked = value; }
                    else if (!input.type || input.type == 'text') {
                        input.value = value;
                    }
                }
                input.onchange = function pageViewSetupInputOnBlur() {
                    if (input.type == 'checkbox') {
                        formFields[input.name] = input.checked;
                    }
                    else if (this.type == 'radio') { formFields[input.name] = input.checked; }
                    else if (!input.type || input.type == 'text') {
                        formFields[input.name] = input.value;
                    }
                };
            }
            function createElementWithStyle(tagName, item) {
                var element = document.createElement(tagName);
                var rect = PDFJS.Util.normalizeRect(
                  viewport.convertToViewportRectangle(item.rect));
                element.style.left = Math.floor(rect[0]) + 'px';
                element.style.top = Math.floor(rect[1]) + 'px';
                element.style.width = Math.ceil(rect[2] - rect[0]) + 'px';
                element.style.height = Math.ceil(rect[3] - rect[1]) + 'px';
                return element;
            }
            function assignFontStyle(element, item) {
                var fontStyles = '';
                if ('fontSize' in item) {
                    fontStyles += 'font-size: ' + Math.round(item.fontSize *
                                                             viewport.fontScale) + 'px;';
                }
                switch (item.textAlignment) {
                    case 0:
                        fontStyles += 'text-align: left;';
                        break;
                    case 1:
                        fontStyles += 'text-align: center;';
                        break;
                    case 2:
                        fontStyles += 'text-align: right;';
                        break;
                }
                element.setAttribute('style', element.getAttribute('style') + fontStyles);
            }

            content.getAnnotations().then(function (items) {
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    switch (item.subtype) {
                        case 'Widget':
                            if (item.fieldType != 'Tx' && item.fieldType != 'Btn' &&
                                item.fieldType != 'Ch') {
                                break;
                            }
                            var inputDiv = createElementWithStyle('div', item);
                            inputDiv.className = 'inputHint';
                            div.appendChild(inputDiv);
                            var input;
                            //item.flags = getInheritableProperty(annotation, 'Ff') || 0;
                            if (item.fieldType == 'Tx') {
                                input = createElementWithStyle('input', item);
                            }
                            if (item.fieldType == 'Btn') {
                                input = createElementWithStyle('input', item);
                                if (item.fieldFlags & 32768) {
                                    input.type = 'radio';
                                    // radio button is not supported
                                } else if (item.fieldFlags & 65536) {
                                    input.type = 'button';
                                    // pushbutton is not supported
                                } else {
                                    input.type = 'checkbox';
                                }
                            }
                            if (item.fieldType == 'Ch') {
                                input = createElementWithStyle('select', item);
                                // select box is not supported
                            }
                            input.className = 'inputControl';
                            if ((input.type == 'radio' || input.type == 'checkbox') && item.fullName) {
                                $.each(item.fullName, function (i) {
                                    if (i == (item.fullName.length - 1) && validation.isGlyphsNumber(this)) {
                                    }
                                    else
                                        input.name += ((i > 0) ? "." : "") + this;
                                });

                            }
                            else
                                input.name = item.fullName.join(".");
                            input.title = item.alternativeText;

                            assignFontStyle(input, item);
                            bindInputItem(input, item);
                            div.appendChild(input);
                            if (input.name.toLowerCase().indexOf('signature') > -1) {
                                //var img = '<img src="" style="position:absolute;' + $(input).attr('style') + '" name="' + $(input).attr('name') + '" id="' + $(input).attr('name') + '" />';
                                $(input).hide();
                                $(input).prev('div').hide();
                                //$(div).append(img);
                            }

                            break;
                    }
                }
                setInputValuesAtInit();
            });
        }
        /**
         * If another page rendering in progress, waits until the rendering is
         * finised. Otherwise, executes rendering immediately.
         */
        function queueRenderPage(num, xOffset, yOffset) {
            if (pageRendering) {
                pageNumPending = num;
                pageLeftPending = xOffset;
                pageTopPending = yOffset;
            } else {
                renderPage(num, xOffset, yOffset, false);
            }
        }

        function getInheritableProperty(annotation, name) {
            var item = annotation;
            while (item && !item.has(name)) {
                item = item.get('Parent');
            }
            if (!item)
                return null;
            return item.get(name);
        }

        $(document).ready(function myfunction() {
            <%-- $(document).find(".logo").children().find("img").attr("src", ".." + "<%= ConfigurationManager.AppSettings["VirtualDir"]%>" + "<%= Common_Tatva.UploadFolder.Replace("/","") + Common_Tatva.UserLogoFolder %>"
                                                                       + "/" + "<%= Sessions.UserId%>" + ".png"); --%>
            if ("<%= logoURL%>" != '') {
                $(document).find(".logo").children().find("img").attr("src", "<%= logoURL%>");
            }

            $('#modalCancel').click(function (e) {
                e.preventDefault();
                OnCancel();
            });

            $('#btnSign').click(function (e) {
                e.preventDefault();
                OnSignClick();
            });

            $('.signbox').click(function () {
                setBox(this);
            });

            $('.penColour').click(function () {
                ChangeColor(this);
            });

            $('#ButtonDeleleOkay').click(function (e) {
                e.preventDefault();
                $("#<%= DivDeleteConfirmation.ClientID%>").show();

                $("#<%= DivDeleteConfirmation.ClientID%>").find("#PopupHeader").text("Download File");
                $("#<%= DivDeleteConfirmation.ClientID%>").find(".popup-scroller").children().text("Would you like to dowload a copy for your records?");
                $("#<%= DivDeleteConfirmation.ClientID%>").find(".popup-btn").children()[0].style.display = "none";
                $("#<%= DivDeleteConfirmation.ClientID%>").find(".popup-btn").children()[1].style.display = "none";
                $("#<%= DivDeleteConfirmation.ClientID%>").find(".popup-btn").children()[2].style.display = "inline-block";
                $("#<%= DivDeleteConfirmation.ClientID%>").find(".popup-btn").children()[3].style.display = "inline-block";
                e.stopPropagation();
                return false;
            });

            function PrepareURLForDownload(documentPath) {
                if (!window.location.origin) {
                    window.location.origin = window.location.protocol + "//"
                      + window.location.hostname
                      + (window.location.port ? ':' + window.location.port : '');
                }

                var tempURL = "path=" + documentPath + "<%= "&Data="+ QueryString.QueryStringEncode("ID=0&folderID=" +(int)Enum_Tatva.Folders.ViewerDownload + "&sessionID=" +Sessions.SwitchedSessionId )%>";
                //var tempURL = "path=" + documentPath + "<%= "&Data=" + QueryString.QueryStringEncode("ID=0&folderID=" + (int)Enum_Tatva.Folders.ViewerDownload + "&sessionID=" + Sessions.UserId)%>";
                var url = window.location.origin + "/Office/PdfViewer.aspx?" + tempURL;
                window.open(url, '_blank');
            }

            $('#ButtonOkay').click(function (e) {
                e.preventDefault();
                $("#<%= DivDeleteConfirmation.ClientID%>").hide();
                SubmitData(true);
            });

            $('#ButtonCancel').click(function (e) {
                e.preventDefault();
                $("#<%= DivDeleteConfirmation.ClientID%>").hide();
                SubmitData(false);
            });

            $('#<%= btnSave.ClientID%>').click(function (e) {
                showLoader();
                e.preventDefault();
                $.each(pdfItems, function (k, data) {
                    if ($('input[name="' + data.FieldName + '"]').length > 0) {
                        if (data.Type.toString() == '<%= InputTypes.TEXT_FIELD.GetHashCode()%>') {
                            data.Value = $('input[name="' + data.FieldName + '"]').val();
                        }
                        else if (data.Type.toString() == '<%= InputTypes.RADIO_BUTTON.GetHashCode()%>') {
                            data.Value = $('input[name="' + data.FieldName + '"]:checked').val() ? $('input[name="' + data.FieldName + '"]:checked').val() : '';
                        }
                        else if (data.Type.toString() == '<%= InputTypes.CHECK_BOX.GetHashCode()%>') {
                            //if ($('input[name="' + data.FieldName + '"]').prop('checked'))
                            //    data.Value = $('input[name="' + data.FieldName + '"]').val();
                            //else
                            //    data.Value = "";
                            data.Value = $('input[name="' + data.FieldName + '"]:checked').val() ? $('input[name="' + data.FieldName + '"]:checked').val() : '';
                        }
                }
                });
                var data1 = JSON.stringify(pdfItems);
                var obj = {};
                obj.Data = JSON.stringify(pdfItems);
                obj.PDFURL = $('#PDFURL').val();
                obj.PDFSource = $('#PDFSource').val();
                obj.JobId = $('#JobId').val();
                obj.DocId = $('#DocId').val();
                var data = JSON.stringify(obj);
                var bas64Data = { 'strDocumentCollectionView': data1, 'base64String': GetSignatureData(), 'data': window.location.search.split("?")[1].split('&')[0].split("=")[1], 'created': window.location.search.split("?")[1].split("=")[2].split("&")[0] };
                $.ajax({
                    url: 'Viewer.aspx/SavePdfData',
                    type: "POST",
                    dataType: 'json',
                    contentType: 'application/json; charset=utf-8', // Not to set any content header
                    processData: false, // Not to process data
                    data: JSON.stringify(bas64Data),
                    success: function (result) {
                        hideLoader();
                        showMessage("Document has been saved successfully.Please copy this url for completing this form later.", false);
                    },
                    error: function (result) {
                        hideLoader();
                        showMessage("Document has not been saved.", true);
                    }

                });
                return false;
                // $('#pdfData').val(JSON.stringify(pdfItems));
            });

            function SubmitData(isDownload) {
                showLoader();
                $.each(pdfItems, function (k, data) {
                    if ($('input[name="' + data.FieldName + '"]').length > 0) {
                        if (data.Type.toString() == '<%= InputTypes.TEXT_FIELD.GetHashCode()%>') {
                            data.Value = $('input[name="' + data.FieldName + '"]').val();
                        }
                        else if (data.Type.toString() == '<%= InputTypes.RADIO_BUTTON.GetHashCode()%>') {
                            data.Value = $('input[name="' + data.FieldName + '"]:checked').val() ? $('input[name="' + data.FieldName + '"]:checked').val() : '';
                        }
                        else if (data.Type.toString() == '<%= InputTypes.CHECK_BOX.GetHashCode()%>') {
                            data.Value = $('input[name="' + data.FieldName + '"]:checked').val() ? $('input[name="' + data.FieldName + '"]:checked').val() : '';
                        }
                }
                });

            var data1 = JSON.stringify(pdfItems);
            var bas64Data = { 'strDocumentCollectionView': data1, 'base64String': GetSignatureData(), 'isDownload': isDownload, 'data': window.location.search.split("?")[1].split('&')[0].split("=")[1], 'created': window.location.search.split("?")[1].split("=")[2].split("&")[0] };
            var temp = JSON.stringify(bas64Data);
            $.ajax({
                url: 'Viewer.aspx/SubmitPdfData',
                type: "POST",
                dataType: 'json',
                contentType: 'application/json; charset=utf-8', // Not to set any content header
                processData: false, // Not to process data
                data: JSON.stringify(bas64Data),
                success: function (result) {
                    if (result.d.length > 0) {
                        PrepareURLForDownload(result.d);
                    }
                    hideLoader();
                    showMessage("Document has been saved successfully.", false);
                    window.location = window.location.origin + "/default.aspx";
                },
                error: function (result) {
                    hideLoader();
                    showMessage("Document has not been saved.", true);
                }

            });
            return false;
        }
        });

    function GetSignatureData() {
        var base64String = [];
        var canvas1 = document.getElementById('canvasSaveValue');
        var ctx1 = canvas1.getContext('2d');
        var imgNode = $("img[class*='Signature']");
        if (imgNode.length <= 0) {
            imgNode = $("img[class*='signature']");
        }

        for (var i = 0; i < imgNode.length; i++) {
            if (imgNode[i]) {
                canvas1.width = imgNode.width();
                canvas1.height = imgNode.height();
            }
            var src = $(imgNode[i]).attr("src");
            var temp = src.split(';');
            var newSrc = temp[1].replace("base64,", "");
            var className = "";
            var classArr = $(imgNode[i]).attr("class").split(" ");
            for (var index = 0; index < classArr.length; index++) {
                if (classArr[index].indexOf("signature") !== -1 || classArr[index].indexOf("Signature") !== -1) {
                    className = classArr[index];
                    break;
                }
            }
            base64String.push({ baseString: newSrc, name: className });
            ctx1.clearRect(0, 0, canvas1.width, canvas1.height);
        }
        return base64String;
    }

    var validation = {
        isGlyphsNumber: function (str) {
            var pattern = /^`+\d+$/;
            return pattern.test(str);  // returns a boolean
        }
    };
    //Own Function to set input values at Initialization time.
    var setInputValuesAtInit = function () {
        signControls();

        $.each(pdfItems, function (k, data) {
            if (data.Type.toString() == '<%= InputTypes.TEXT_FIELD.GetHashCode()%>') {
                $('input[name="' + data.FieldName + '"]').val(data.Value);
                data.PdfItemProperties && data.PdfItemProperties.MaxLength && $('input[name="' + data.FieldName + '"]').attr('maxLength', data.PdfItemProperties.MaxLength);
            }
            else if (data.Type.toString() == '<%= InputTypes.RADIO_BUTTON.GetHashCode()%>') {
                var values = data.AvailableValues;
                values = $.grep(values, function (e, index) {
                    return e.toLowerCase() != 'off';
                });
                $.each($('input[name="' + data.FieldName + '"]'), function (i) {
                    this.value = ((values.length - (i + 1)) > -1 && values.length - (i + 1) < values.length) ? values[values.length - (i + 1)] : '';
                });
                $('input[name="' + data.FieldName + '"]').filter('[value=' + data.Value + ']').prop('checked', true);
            }
            else if (data.Type.toString() == '<%= InputTypes.CHECK_BOX.GetHashCode()%>') {
                var values = data.AvailableValues;
                if ($('input[name="' + data.FieldName + '"]').length == 1) {
                    $.each($('input[name="' + data.FieldName + '"]'), function (i) {
                        this.value = values[0];
                        if (data.Value == this.value)
                            $(this).prop('checked', true);
                    });
                }
                else {
                    $.each($('input[name="' + data.FieldName + '"]'), function (i) {
                        this.value = values[values.length - (i + 1)];
                        if (data.Value == this.value)
                            $(this).prop('checked', true);
                    });
                }
            }
        });

        $('input[type="checkbox"]').click(function () {
            if ($(this).prop('checked'))
                $('input[name="' + $(this).attr('name') + '"]').not($(this)).prop('checked', false);
        });

        $('#btnSubmit').click(function (e) {
            e.preventDefault();
        });
    };
function signControls() {
    var parnetNode = [];
    parnetNode = $("input[name*='Signature']");
    if (parnetNode.length <= 0) {
        parnetNode = $("input[name*='signature']");
    }
    if (!$(parnetNode).parent('div').hasClass('modalParentDiv')) {
        for (var i = 0; i < parnetNode.length; i++) {
            var $childNode = $("#modalDiv").children().clone();
            $childNode.attr("id", $childNode.attr("id") + $(parnetNode[i]).attr("name"));
            $childNode.attr("name", $(parnetNode[i]).attr("name"));
            var div = document.createElement("div");
            var parentElement = document.getElementsByName($(parnetNode[i]).attr("name"));
            var parentDiv = "";
            if (i == 0)
                parentDiv = $(parnetNode).parent();
            else
                parentDiv = $(parnetNode).parents("div").first().parent();
            $(parentDiv).append($(div));
            $(div).addClass("modalParentDiv");

            $(div).css("left", $(parnetNode[i]).css("left"));
            $(div).css("top", $(parnetNode[i]).css("top"));
            $(div).css("width", $(parnetNode[i]).css("width"));
            $(div).css("height", $(parnetNode[i]).height() + 2 + "px");
            $(div).append($(parentElement));

            $($childNode).insertAfter($(parnetNode[i]));
            $($childNode).css("height", $(parnetNode[i]).height() + 1 + "px");
            $($childNode).css("width", $(parnetNode[i]).height() + "px");
            $($childNode).css("background-size", $(parnetNode[i]).height() - 10 + "px");
            $($childNode).css("position", "absolute");
            $($childNode).css("border", "0px");
            $($childNode).css("left", "-" + $(parnetNode[i]).height() + "px");

            var $buttonNode = $("button[id='btnCancelSign']").clone();
            $buttonNode.attr("id", $buttonNode.attr("id") + $(parnetNode[i]).attr("name"));
            $($buttonNode).css("height", $(parnetNode[i]).height() + 1 + "px");
            $($buttonNode).css("width", $(parnetNode[i]).height() + "px");
            $($buttonNode).css("left", $(parnetNode[i]).css("width"));
            $($buttonNode).css("display", "block");
            $($buttonNode).css("background-size", $($buttonNode).width() - 10 + "px");
            $($buttonNode).css("position", "absolute");
            $($buttonNode).insertAfter($($childNode));

        }
        openSign();
        OnCancelSign();
    }
}

function openSign() {

    $('.modalTest').unbind('click').bind('click', function (e) {
        e.preventDefault();
        $("body").addClass("modalBody");
        $("#DivSignature,.ModalPopupBG").show();
        $modalOpenedFrom = $(this);
        resizeCanvas();
    });
}

function OnCancelSign() {
    $('.cancelSign').unbind('click').bind('click', function (e) {
        e.preventDefault();
        var temp = $(this);
        $(temp).parent().find("img").remove();
        $(temp).parent().css("background", "transparent");
    });
}

function showMessage(msg, isError) {
    alert(msg);
    //$(".alert").hide();
    //var visible = isError ? "errorMsgRegion1" : "successMsgRegion1";
    //var inVisible = isError ? "successMsgRegion1" : "errorMsgRegion1";
    //$("#" + inVisible).hide();
    //$("#" + visible).html(closeButton + msg);
    //$("#" + visible).show();
    //$("#" + visible).fadeOut(5000);
}

function setBox(obj) {
    $('.signbox').removeClass('SelectedFont');
    $(obj).addClass('SelectedFont');
    var text = $(obj).text();
    var fontfamily = $(obj).css("font-family");
    var fontcolor = $(obj).css("color");
}

function Text() {
    signType = "text";
    $("#divContentSign").hide();
    $("#divContent").show();
    document.getElementById("txtSignatureInput").value = "";
    DisplayText();
}
function Draw() {
    signaturePad.maxWidth = 2.5;
    signaturePad.minWidth = 2.5;
    signType = "draw";
    $("#divContent").hide();
    $("#divContentSign").show();

    resizeCanvas();
}

function DisplayText() {
    var x = document.getElementById("txtSignatureInput").value;
    if (x == "") {
        x = "Signature";
    }
    document.getElementById("divCedarville").innerHTML = x;
    document.getElementById("divKristi").innerHTML = x;
    document.getElementById("divDafoe").innerHTML = x;
    document.getElementById("divsacramento").innerHTML = x;
    document.getElementById("divMontez").innerHTML = x;
    document.getElementById("divReenie_beanie").innerHTML = x;
}

var wrapper = document.getElementById("signature-pad"),
canvas = wrapper.querySelector("canvas"),
signaturePad;

signaturePad = new SignaturePad(canvas);
signaturePad.maxWidth = 2.5;
signaturePad.minWidth = 2.5;

function resizeCanvas(width1, height1) {

    var ratio = Math.max(window.devicePixelRatio || 1, 1);

    canvas.width = canvas.offsetWidth * ratio;

    canvas.height = canvas.offsetHeight * ratio;

    canvas.getContext("2d").scale(ratio, ratio);
}

function ChangeColor(obj) {
    signaturePad && signaturePad.clear();
    var fontcolor = $(obj).attr('val');
    if (fontcolor == "blue") {
        $("#divBlack").removeClass('blackColourActive');
        $("#divGreen").removeClass('greenColourActive');
        $(obj).addClass('blueColourActive');

        signaturePad.penColor = "blue";
        $('.signbox').css('color', 'blue');
    }
    else if (fontcolor == "green") {
        $("#divBlack").removeClass('blackColourActive');
        $("#divBlue").removeClass('blueColourActive');
        $(obj).addClass('greenColourActive');

        signaturePad.penColor = "Green";
        $('.signbox').css('color', 'green');
    }
    else if (fontcolor == "black") {
        $("#divBlue").removeClass('blueColourActive');
        $("#divGreen").removeClass('greenColourActive');
        $(obj).addClass('blackColourActive');

        signaturePad.penColor = "black";
        $('.signbox').css('color', 'black');
    }
}

function OnSignClick() {
    var img = "", selectedText = "", selectedFont = "", selectedColor = "", selectedFontFamily = "";
    if (signType == "text") {
        //At least one signature should be selected
        if (document.getElementById("txtSignatureInput").value.trim().length <= 0) {
            alert("Please enter Signature text.");
            return;
        }
        else if ($('.SelectedFont').text().trim().length > 25) {
            alert("Signature can not contain more than 25 characters.");
            return;
        }
        else if ($('.SelectedFont').text() == "") {
            alert("Please select signature.");
            return;
        }

        selectedText = $('.SelectedFont').text(); //Saves selected text
        selectedColor = $('.SelectedFont').css('color'); //Saves selected color
        selectedFontFamily = $('.SelectedFont').css('font-family');

    }

    //Appends div tag and set signature
    var parentStyle = $("input[name='" + $modalOpenedFrom.attr("name") + "'][class='inputControl']").attr('style');
    var parnetNode = $("input[name='" + $modalOpenedFrom.attr("name") + "'][class='inputControl']");
    var divOfParentNode = $(parnetNode).parent();
    $(("." + parnetNode.attr('name'))).remove();
    if (signType == "text") {

        var canvas2 = document.getElementById('canvasSaveValue');
        var ctx2 = canvas2.getContext('2d');

        ctx2.fillStyle = selectedColor;
        ctx2.font = selectedFontFamily;
        ctx2.font = parnetNode.height() + parnetNode.height() + "px" + " " + selectedFontFamily;
        ctx2.fillText(selectedText, 10, 70, $(parnetNode).width());
        var src = ctx2.canvas.toDataURL();
        img = document.createElement('img'); // create a Image Element
        img.src = src;

        ctx2.clearRect(0, 0, canvas2.width, canvas2.height);

    }
    else {
        var canvas1 = document.getElementById('canvasDraw');

        var src = canvas1.toDataURL("image/png"); // cache the image data source
        img = document.createElement('img'); // create a Image Element
        img.src = src;   //image source
        signaturePad.clear();
        signaturePad.penColor = "black";
    }
    var buttonNode = $("button[name='" + $modalOpenedFrom.attr("name") + "']");
    $(img).css({
        width: ($(parnetNode).width()),
        height: ($(parnetNode).height()),
        left: "0px",
        top: "0px",
        position: 'absolute'
    })

    $(divOfParentNode).css("background", "white");
    $(img).addClass("imagePosition");
    $(img).addClass($modalOpenedFrom.attr("name"));
    $(divOfParentNode).append(img);
    //Closes modal and clear the content of modal
    OnCancel();
    document.getElementById("txtSignatureInput").value = "";
    DisplayText();
}

function OnCancel() {
    document.getElementById("txtSignatureInput").value = "";
    DisplayText();
    signaturePad.clear();
    $('.signbox').removeClass('SelectedFont');
    $("#divBlue").removeClass('blueColourActive');
    $("#divGreen").removeClass('greenColourActive');
    $("#divBlack").addClass('blackColourActive');

    $("#DivSignature,.ModalPopupBG").hide();
    $('.signbox').css('color', 'black');
    signaturePad.penColor = "black";
    $("body").removeClass("modalBody");
}

function showLoader() {
    $('#overlay').show().height($(document).height());
}

function hideLoader() {
    $('#overlay').hide();
}

    </script>
</asp:Content>


