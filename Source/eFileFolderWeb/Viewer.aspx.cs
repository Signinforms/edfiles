﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp.text.pdf;
using Shinetech.DAL;
using System.Data;
using System.Configuration;
using net.openstack.Core.Domain;
using Shinetech.Utility;
using System.Web.Script.Serialization;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

public partial class Viewer : System.Web.UI.Page
{

    public static string PDFUrl = string.Empty;
    public static string rackSpaceURL = string.Empty;
    public static string documentFullPath = string.Empty;
    public static string shareID = string.Empty;
    protected string PDFData = "";
    public static string viewerUID = string.Empty;
    public static string UID = string.Empty;
    public static string SharedByID = string.Empty;
    private static int ExpirationTimeConfig = Convert.ToInt32(ConfigurationManager.AppSettings["ValidShareIdHours"]);
    public string logoURL
    {
        get
        {
            string filePath = Common_Tatva.UploadFolder.Replace("/", "") + Common_Tatva.UserLogoFolder + "/" + viewerUID + ".png";
            if (!File.Exists(Server.MapPath("~/" + filePath)))
                filePath = "";
            return filePath;
        }
    }
    public ViewerEditablePdf viewerEditablePdf = new ViewerEditablePdf();

    public string eFileFlowShareId
    {
        get
        {
            return Convert.ToString(Request.QueryString["Data"]);
        }
    }

    public static string uid
    {
        get
        {
            return Convert.ToString(HttpContext.Current.Request.QueryString["Data"]);
        }
    }

    public static string sharedID
    {
        get
        {
            return Convert.ToString(HttpContext.Current.Request.QueryString["created"]);
        }
    }

    //public class Base64String
    //{
    //    public string baseString { get; set; }
    //    public string name { get; set; }
    //}


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                //  UID = (string)Session["viewerID"];
                string errorMsg = string.Empty;
                shareID = Request.QueryString["data"];
                hdnShareID.Value = QueryString.QueryStringDecode(Request.QueryString["id"]);
                errorMsg = GetFileInfo(shareID);
                errorMessageText.Value = errorMsg;
                if (string.IsNullOrEmpty(errorMsg))
                {
                    DocumentCollectionView docView = viewerEditablePdf.GetPDFData(documentFullPath);
                    docView.DocId = eFileFlowShareId;
                    UID = uid;
                    SharedByID = sharedID;
                    PDFData = docView.PDFData;
                }
            }
        }
        catch (Exception ex)
        {

        }
    }


    private static string GetFileInfo(string shareID)
    {
        string errorMsg = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(shareID))
            {
                string tempPath = "WorkArea/eFileFlowShareTemp/" + shareID;
                string dirPath = HttpContext.Current.Server.MapPath("~/" + tempPath);
                if (!Directory.Exists(dirPath))
                {
                    Directory.CreateDirectory(dirPath);
                }
                foreach (string file in Directory.GetFiles(dirPath))
                {
                    File.Delete(file);
                }

                DataTable dt = General_Class.CheckSharedIdExists(new Guid(shareID));
                if (dt.Rows.Count <= 0)
                {
                    errorMsg = "Not a valid ShareID";
                }
                else
                {
                    dt.AsEnumerable().ToList().ForEach(d =>
                    {
                        viewerUID = d.Field<Guid>("UID").ToString();
                        DateTime shareDateTime = d.Field<DateTime>("Date");
                        int expirationTime = General_Class.GetExpirationTime(new Guid(viewerUID));
                        if ((DateTime.Now - shareDateTime).TotalHours <= ((expirationTime > 0) ? expirationTime : ExpirationTimeConfig))
                        {

                            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
                            IEnumerable<ContainerObject> objectList = rackSpaceFileUpload.GetObjectList(viewerUID, Enum_Tatva.GetEnumDescription(Enum_Tatva.Folders.EFileShare) + "/" + shareID);
                            foreach (ContainerObject obj in objectList)
                            {
                                if (obj.Name.Contains(shareID))
                                {
                                    //string dir = HttpContext.Current.Server.MapPath("~/" + Common_Tatva.RackSpaceDownloadDirectory);
                                    string dir = HttpContext.Current.Server.MapPath("~/" + Common_Tatva.RackSpaceWorkareaDownload);
                                    if (!Directory.Exists(dir))
                                    {
                                        Directory.CreateDirectory(dir);
                                    }
                                    string newFileName = rackSpaceFileUpload.GetObject(ref errorMsg, obj.Name, viewerUID);
                                    File.Move(dir + "\\" + viewerUID + "\\" + newFileName, dirPath + "\\" + newFileName);
                                    documentFullPath = dirPath + "\\" + newFileName;
                                    documentFullPath = documentFullPath.Replace("\\", "//");
                                    rackSpaceURL = Enum_Tatva.GetEnumDescription(Enum_Tatva.Folders.EFileShare) + "/" + shareID + "/" + obj.Name.Substring(obj.Name.LastIndexOf('/') + 1);
                                    PDFUrl = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.LastIndexOf('/') + 1) + tempPath + "/" + newFileName;

                                    break;
                                }
                            }
                        }
                        else
                        {
                            errorMsg = "ShareID has been expired";
                        }
                    });

                }
            }
        }
        catch (Exception ex) 
        {
            errorMsg = ex.Message;
        }
        return errorMsg;
    }

    //private void GetPDFData()
    //{
    //    try
    //    {
    //        DocumentCollectionView documentCollectionView = new DocumentCollectionView();
    //        if (System.IO.File.Exists(documentFullPath))
    //        {
    //            documentCollectionView.PdfItems = GetPDFItems(documentFullPath);
    //            documentCollectionView.PDFSource = documentFullPath;
    //            documentCollectionView.Data = documentCollectionView.ParsedData;
    //            PDFData = documentCollectionView.Data = documentCollectionView.Data.Replace(@"\\", @"\").Replace(@"\", @"\\").Replace(System.Environment.NewLine, @"<br />");
    //        }
    //        documentCollectionView.DocId = eFileFlowShareId;
    //        UID = uid;
    //        SharedByID = sharedID;
    //    }
    //    catch (Exception ex)
    //    { }
    //}

    /// <summary>
    /// Get all pdf inputs with it's value in list of PdfItems result. It will use iTextSharp to read pdf.
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    private List<PdfItems> GetPDFItems(string fileName)
    {
        string pdfTemplate = fileName;//Server.MapPath("~/pdfs/f1040ezt.pdf");

        MemoryStream memStream = new MemoryStream();
        using (FileStream fileStream = System.IO.File.OpenRead(pdfTemplate))
        {
            memStream.SetLength(fileStream.Length);
            fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
        }

        string newFile = fileName;
        PdfReader pdfReader = null;
        List<PdfItems> lstPdfItems = new List<PdfItems>();
        try
        {
            pdfReader = new PdfReader(memStream);
            AcroFields af = pdfReader.AcroFields;
            StringBuilder sb = new StringBuilder();

            foreach (var field in af.Fields)
            {
                if (Convert.ToString(field.Key) == "untitled46")
                {

                }
                PdfItems k = new PdfItems(lstPdfItems.Count, field.Key, af.GetField(Convert.ToString(field.Key)), af.GetFieldType(Convert.ToString(field.Key)));
                if (k.Type == (int)InputTypes.RADIO_BUTTON || k.Type == (int)InputTypes.CHECK_BOX || k.Type == (int)InputTypes.LIST_BOX || k.Type == (int)InputTypes.COMBO_BOX)
                {
                    try { k.AvailableValues.AddRange(GetCheckBoxExportValue(af, Convert.ToString(field.Key))); }
                    catch { }
                }
                else if (k.Type == (int)InputTypes.TEXT_FIELD)
                {
                    try
                    {
                        AcroFields.Item fieldItem = af.GetFieldItem(Convert.ToString(field.Key));
                        PdfDictionary pdfDictionary = (PdfDictionary)fieldItem.GetWidget(0);
                        int maxFieldLength = Int32.Parse(pdfDictionary.GetAsNumber(PdfName.MAXLEN).ToString());
                        k.PdfItemProperties.MaxLength = maxFieldLength;
                    }
                    catch (Exception) { }
                }
                if (k.Type == (int)InputTypes.CHECK_BOX)
                {
                    string a = af.GetField(Convert.ToString(field.Key));
                }
                lstPdfItems.Add(k);

                if (Convert.ToString(field.Key) == "topmostSubform[0].Page1[0].Entity[0].p1-t4[0]" || Convert.ToString(field.Key) == "topmostSubform[0].Page1[0].f1_040_0_[0]")
                {

                }
            }
        }
        catch (Exception ex)
        {
        }
        finally
        {
            if (pdfReader != null)
                pdfReader.Close();
            memStream.Close();
            memStream.Dispose();
        }
        return lstPdfItems;
    }

    public string[] GetCheckBoxExportValue(AcroFields fields, string cbFieldName)
    {
        AcroFields.Item fd = ((AcroFields.Item)fields.GetFieldItem(cbFieldName));
        Hashtable names = new Hashtable();
        string[] outs = new string[fd.GetValue(0).Length];
        PdfDictionary pd = ((PdfDictionary)fd.GetWidget(0)).GetAsDict(PdfName.AP);
        for (int k1 = 0; k1 < fd.GetValue(0).Length; ++k1)
        {
            PdfDictionary dic = (PdfDictionary)fd.GetWidget(k1);
            dic = dic.GetAsDict(PdfName.AP);
            if (dic == null)
                continue;
            dic = dic.GetAsDict(PdfName.N);
            if (dic == null)
                continue;
            foreach (PdfName pname in dic.Keys)
            {
                String name = PdfName.DecodeName(pname.ToString());
                if (name.ToLower() != "off")
                {
                    //k.AvailableValues.Add(name);
                    names[name] = null;
                    outs[(outs.Length - k1) - 1] = name;
                }
            }
        }
        return outs;
    }

    [WebMethod]
    public static bool SavePdfData(string strDocumentCollectionView, ViewerEditablePdf.Base64String[] base64String, string data, string created)
    {
        ViewerEditablePdf editablePdf = new ViewerEditablePdf();
        UID = data.Trim();
        shareID = data.Trim();
        SharedByID = created.Trim();
        GetFileInfo(data.Trim());
        editablePdf.SavePdfData(strDocumentCollectionView, base64String, documentFullPath);
        //  var uid = Convert.ToString(HttpContext.Current.Request.QueryString["Data"]);
        //try
        //{
        //    List<PdfItems> documentCollectionView = new List<PdfItems>();
        //    documentCollectionView = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PdfItems>>(strDocumentCollectionView);
        //    JavaScriptSerializer serializer = new JavaScriptSerializer();
        //    List<Base64String> baseData = base64String.ToList<Base64String>();
        //    FillPDFAndSave(documentCollectionView, documentFullPath, Enum_Tatva.Status.Saved, baseData, UID);
        //}
        //catch (Exception ex)
        //{
        //}
        UploadAndDeleteObject(Enum_Tatva.Status.Saved, UID);

        return true;
    }

    /// <summary>
    /// It has file name which we are going to fill with default vlaues also it has default values then it will just fill pdf contents with default values.
    /// </summary>
    /// <param name="lstPdfItems"></param>
    /// <param name="fileName"></param>
    //private static void FillPDFAndSave(List<PdfItems> lstPdfItems, string fileName, Enum_Tatva.Status status, List<Base64String> base64String = null, string uid = null)
    //{
    //    MemoryStream memStream = new MemoryStream();
    //    using (FileStream fileStream = System.IO.File.OpenRead(fileName))
    //    {
    //        memStream.SetLength(fileStream.Length);
    //        fileStream.Read(memStream.GetBuffer(), 0, (int)fileStream.Length);
    //        fileStream.Close();
    //    }

    //    PdfReader pdfReader = null;
    //    PdfStamper pdfStamper = null;
    //    try
    //    {
    //        pdfReader = new PdfReader(memStream);
    //        pdfStamper = new PdfStamper(pdfReader, new FileStream(fileName, FileMode.Create));

    //        AcroFields pdfFormFields = pdfStamper.AcroFields;


    //        foreach (PdfItems item in lstPdfItems)
    //        {
    //            if (Convert.ToString(item.FieldName).ToLower().Contains("signature"))
    //            {
    //                try
    //                {
    //                    float[] fieldPosition = null;
    //                    fieldPosition = pdfFormFields.GetFieldPositions(Convert.ToString(item.FieldName));

    //                    string data = string.Empty;
    //                    foreach (Base64String str in base64String)
    //                    {
    //                        if (str.name.Equals(item.FieldName))
    //                            data = str.baseString;

    //                    }

    //                    byte[] byteConvertedImage = Convert.FromBase64String(data);
    //                    MemoryStream ms = new MemoryStream(byteConvertedImage, 0, byteConvertedImage.Length);
    //                    // Convert byte[] to Image
    //                    ms.Write(byteConvertedImage, 0, byteConvertedImage.Length);
    //                    System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);

    //                    Bitmap newImage = ScaleImage(new Bitmap(image), Convert.ToInt32(Math.Abs(fieldPosition[3] - fieldPosition[1] - 3)), Convert.ToInt32(Math.Abs(fieldPosition[4] - fieldPosition[2] - 3)));

    //                    using (var b = new Bitmap(image.Width, image.Height))
    //                    {
    //                        b.SetResolution(image.HorizontalResolution, image.VerticalResolution);

    //                        using (var g = Graphics.FromImage(b))
    //                        {
    //                            g.Clear(Color.White);
    //                            g.DrawImageUnscaled(image, 0, 0);
    //                        }

    //                        iTextSharp.text.Image image1 = iTextSharp.text.Image.GetInstance(b, System.Drawing.Imaging.ImageFormat.Jpeg);
    //                        image1.SetAbsolutePosition(fieldPosition[1], fieldPosition[2]);
    //                        image1.ScaleAbsolute(Math.Abs(fieldPosition[3] - fieldPosition[1] - 3), Math.Abs(fieldPosition[4] - fieldPosition[2] - 3));
    //                        // change the content on top of page 1
    //                        var pageNum = (fieldPosition.Length > 0 && !string.IsNullOrEmpty(Convert.ToString(fieldPosition[0]))) ? Convert.ToInt32(fieldPosition[0]) : 1;
    //                        PdfContentByte overContent = pdfStamper.GetOverContent(pageNum);
    //                        overContent.AddImage(image1);

    //                        pdfFormFields.SetFieldProperty(Convert.ToString(item.FieldName),
    //                                   "setfflags",
    //                                    PdfFormField.FF_READ_ONLY,
    //                                    null);
    //                    }
    //                }

    //                catch (Exception ex)
    //                {
    //                }
    //            }
    //            else
    //            {
    //                pdfFormFields.SetField(Convert.ToString(item.FieldName), item.Value);
    //            }
    //        }

    //        pdfStamper.FormFlattening = false;
    //        pdfStamper.Close();
    //        UploadAndDeleteObject(status, uid);
    //    }
    //    catch (Exception ex)
    //    {

    //    }
    //    finally
    //    {
    //        // close the pdf
    //        if (pdfStamper != null)
    //            pdfStamper.Close();
    //        if (pdfReader != null)
    //            pdfReader.Close();
    //        memStream.Close();
    //        memStream.Dispose();
    //    }
    //}

    /// <summary>
    /// This method will resize image as per aspect ratio. Here we are passing maxHeight and maxWidth. Based on these params, it will find lowest value and resize it accordingly.
    /// </summary>
    /// <param name="original"></param>
    /// <param name="maxWidth"></param>
    /// <param name="maxHeight"></param>
    /// <returns></returns>
    public static Bitmap ScaleImage(Bitmap original, int maxWidth, int maxHeight)
    {
        int originalWidth = original.Width;
        int originalHeight = original.Height;

        // To preserve the aspect ratio
        float ratioX = (float)maxWidth / (float)originalWidth;
        float ratioY = (float)maxHeight / (float)originalHeight;
        float ratio = Math.Min(ratioX, ratioY);

        // New width and height based on aspect ratio
        int newWidth = (int)(originalWidth * ratio);
        int newHeight = (int)(originalHeight * ratio);

        // Convert other formats (including CMYK) to RGB.
        Bitmap newImage = new Bitmap(newWidth, newHeight, PixelFormat.Format64bppArgb);

        //ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
        EncoderParameters myEncoderParameters = new EncoderParameters(1);
        myEncoderParameters.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.ColorDepth, 32L);

        // Draws the image in the specified size with quality mode set to HighQuality
        using (Graphics graphics = Graphics.FromImage(newImage))
        {
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.CompositingQuality = CompositingQuality.HighQuality;
            graphics.PixelOffsetMode = PixelOffsetMode.HighSpeed;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            graphics.DrawImage(original, 0, 0, newWidth, newHeight);
        }
        return newImage;
    }

    /// <summary>
    /// Uploads saved data to rackspace
    /// </summary>
    /// <param name="status">File to be submitted or saved</param>
    private static void UploadAndDeleteObject(Enum_Tatva.Status status, string uid)
    {
        try
        {
            RackSpaceFileUpload rackSpaceFileUpload = new RackSpaceFileUpload();
            using (FileStream fileStream = new FileStream(documentFullPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None))
            {
                MemoryStream memoryStream = new MemoryStream();
                memoryStream.SetLength(fileStream.Length);
                fileStream.Read(memoryStream.GetBuffer(), 0, (int)fileStream.Length);
                string errorMsg = string.Empty;
                rackSpaceFileUpload.UploadObjectForViewer(ref errorMsg, rackSpaceURL, memoryStream, SharedByID);
                fileStream.Close();
                //if ((int)status == 3)
                //    File.Delete(documentFullPath);
                General_Class.UpdateShare(rackSpaceURL.Substring(rackSpaceURL.LastIndexOf('/') + 1), new Guid(shareID), 0, 0, Enum_Tatva.IsDelete.No.GetHashCode(), 0, 1, (int)status);
            }
        }
        catch (Exception ex)
        {

        }
    }


    [WebMethod]
    public static string SubmitPdfData(string strDocumentCollectionView, ViewerEditablePdf.Base64String[] base64String, bool isDownload, string data, string created)
    {
        string destinationPath = string.Empty;
        //  var uid = Convert.ToString(HttpContext.Current.Request.QueryString["Data"]);
        try
        {
            UID = data.Trim();
            SharedByID = created.Trim();
            shareID = data.Trim();
            GetFileInfo(data.Trim());
            
            List<PdfItems> documentCollectionView = new List<PdfItems>();

            documentCollectionView = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PdfItems>>(strDocumentCollectionView);

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<ViewerEditablePdf.Base64String> baseData = base64String.ToList<ViewerEditablePdf.Base64String>();
            ViewerEditablePdf editablePdf = new ViewerEditablePdf();
            editablePdf.FillPDFAndSave(documentCollectionView, documentFullPath, Enum_Tatva.Status.Submitted, baseData, UID);
            UploadAndDeleteObject(Enum_Tatva.Status.Submitted, UID);

            if (isDownload)
            {
                //destinationPath = HttpContext.Current.Server.MapPath("~/" + Common_Tatva.RackSpaceDownloadDirectory) + "\\" + documentFullPath.Substring(documentFullPath.LastIndexOf("//") + 2);
                destinationPath = HttpContext.Current.Server.MapPath("~/" + Common_Tatva.RackSpaceWorkareaDownload) + "\\" + (string.IsNullOrEmpty(SharedByID) ? Sessions.SwitchedRackspaceId : SharedByID) + "\\" + documentFullPath.Substring(documentFullPath.LastIndexOf("//") + 2);
                File.Copy(documentFullPath, destinationPath);
                destinationPath = destinationPath.Replace("\\", "//");
            }

        }
        catch (Exception ex)
        { }
        return destinationPath;
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        DownloadFile();
    }

    private void DownloadFile()
    {
        try
        {
            GetFileInfo(Convert.ToString(Request.QueryString["Data"]).Trim());
            System.IO.FileInfo file = new System.IO.FileInfo(documentFullPath);
            if (file.Exists)
            {
                string fileName = file.Name.Replace(",", "");
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.WriteFile(file.FullName);

            }
            else
            {
                HttpContext.Current.Response.Write("This file does not exist.");
            }
        }
        catch (Exception ex)
        { }
    }
    protected void hdnBtn_Click(object sender, EventArgs e)
    {

    }
}

//public enum InputTypes
//{
//    BUTTON = 1,
//    CHECK_BOX = 2,
//    RADIO_BUTTON = 3,
//    TEXT_FIELD = 4,
//    LIST_BOX = 5,
//    COMBO_BOX = 6
//}

//public class DocumentCollectionView
//{
//    public string Data { get; set; }
//    public List<PdfItems> PdfItems { get; set; }
//    public string PDFURL { get; set; }
//    public string DocId { get; set; }
//    public string PDFSource { get; set; }
//    public string ParsedData
//    {
//        get
//        {
//            try
//            {
//                return Newtonsoft.Json.JsonConvert.SerializeObject(PdfItems);
//            }
//            catch (Exception)
//            {
//                return "";
//            }
//        }
//    }

//    public List<PdfItems> ParsedPdfItems
//    {
//        get
//        {
//            List<PdfItems> items = new List<PdfItems>();
//            try
//            {
//                items = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PdfItems>>(Data);
//            }
//            catch (Exception)
//            {
//            }
//            return items;
//        }

//    }
//}

//public class PdfItems
//{
//    public PdfItems()
//    { }

//    public PdfItems(int SrNo, object fieldName, string value, int type)
//    {
//        this.SrNo = SrNo;
//        this.Type = type;
//        FieldName = fieldName;
//        Value = value;
//        AvailableValues = new List<string>();
//        PdfItemProperties = new PdfItemProperties();
//    }
//    private int? _SrNo;
//    public int? SrNo { get { return _SrNo; } set { _SrNo = value; } }
//    private object _FieldName;
//    public object FieldName { get { return _FieldName; } set { _FieldName = value; } }
//    private string _Value;
//    public string Value { get { return _Value; } set { _Value = value; } }
//    private int _Type;
//    public int Type { get { return _Type; } set { _Type = value; } }
//    private List<string> _AvailableValues;
//    public List<string> AvailableValues { get { return _AvailableValues; } set { _AvailableValues = value; } }
//    private PdfItemProperties _PdfItemProperties;
//    public PdfItemProperties PdfItemProperties { get { return _PdfItemProperties; } set { _PdfItemProperties = value; } }
//}

//public class PdfItemProperties
//{
//    private int _MaxLength;
//    public int MaxLength { get { return _MaxLength; } set { _MaxLength = value; } }
//}