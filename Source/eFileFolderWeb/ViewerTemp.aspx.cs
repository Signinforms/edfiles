﻿using iTextSharp.text.pdf;
using net.openstack.Core.Domain;
using Newtonsoft.Json;
using Shinetech.DAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ViewerTemp : System.Web.UI.Page
{
    public static string PDFUrl = string.Empty;
    protected string PDFData = "";
    //public string viewerUID = string.Empty;
    public string logoURL = string.Empty;


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Sessions.HasViewPrivilegeOnly)
            {
                btnDownload.Visible = false;
                Button2.Visible = false;
            }
            PDFUrl = PDFData = string.Empty;
            string logFormID = Request.QueryString["id"];
            Common_Tatva.WriteLogFomLog(logFormID, "Page_Load : ViewerTemp.aspx", "Function Entered");

            DataTable logFormDetails = General_Class.GetLogFormDetails(string.IsNullOrEmpty(logFormID) ? 0 : int.Parse(logFormID));
            if (logFormDetails != null & logFormDetails.Rows.Count > 0)
            {
                string tempPath = "Volume/" + logFormDetails.Rows[0]["FolderID"] + "/LogForm/" + logFormDetails.Rows[0]["FileName"];
                string documentFullPath = HttpContext.Current.Server.MapPath("~/" + tempPath);
                if (!Directory.Exists(documentFullPath.Substring(0, documentFullPath.LastIndexOf("\\"))))
                {
                    tempPath = "Volume2/" + logFormDetails.Rows[0]["FolderID"] + "/LogForm/" + logFormDetails.Rows[0]["FileName"];
                    documentFullPath = HttpContext.Current.Server.MapPath("~/" + tempPath);
                    PDFUrl = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.LastIndexOf('/') + 1) + "Volume2/" + logFormDetails.Rows[0]["FolderID"] + "/LogForm/" + logFormDetails.Rows[0]["FileName"] + "?v=" + DateTime.Now.Ticks;
                }
                else if (Directory.Exists(documentFullPath.Substring(0, documentFullPath.LastIndexOf("\\"))))
                {
                    PDFUrl = HttpContext.Current.Request.Url.AbsoluteUri.Substring(0, HttpContext.Current.Request.Url.AbsoluteUri.LastIndexOf('/') + 1) + "Volume/" + logFormDetails.Rows[0]["FolderID"] + "/LogForm/" + logFormDetails.Rows[0]["FileName"] + "?v=" + DateTime.Now.Ticks;
                }
                Common_Tatva.WriteLogFomLog(logFormID, "Page_Load : ViewerTemp.aspx : documentFullPath : ", documentFullPath);
                ViewerEditablePdf editablePdf = new ViewerEditablePdf();
                DocumentCollectionView docView = editablePdf.GetPDFData(documentFullPath);
                Common_Tatva.WriteLogFomLog(logFormID, "Page_Load : ViewerTemp.aspx ", "After GetPDFData");
                PDFData = docView.PDFData;
            }

            //GetPDFData();
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteLogFomLog("0", "Page_Load : ViewerTemp.aspx", "In catch : ErrorMessage: " + ex.Message);
        }
    }

    [WebMethod]
    public static string[] SavePdfData(string strDocumentCollectionView, ViewerEditablePdf.Base64String[] base64String, string logFormId)
    {

        bool isSuccess = false;
        string UID = string.Empty;
        DataTable logFormDetails = null;
        try
        {
            Common_Tatva.WriteLogFomLog(logFormId, "SavePdfData : ViewerTemp.aspx", "Function Entered");
            ViewerEditablePdf editablePdf = new ViewerEditablePdf();
            logFormDetails = General_Class.GetLogFormDetails(string.IsNullOrEmpty(logFormId) ? 0 : int.Parse(logFormId));
            Common_Tatva.WriteLogFomLog(logFormId, "SavePdfData : ViewerTemp.aspx", "After GetLogFormDetails");
            string tempPath = "Volume/" + logFormDetails.Rows[0]["FolderID"] + "/LogForm/" + logFormDetails.Rows[0]["FileName"];
            string documentFullPath = HttpContext.Current.Server.MapPath("~/" + tempPath);
            if (!Directory.Exists(documentFullPath.Substring(0, documentFullPath.LastIndexOf("\\"))))
            {
                tempPath = "Volume2/" + logFormDetails.Rows[0]["FolderID"] + "/LogForm/" + logFormDetails.Rows[0]["FileName"];
                documentFullPath = HttpContext.Current.Server.MapPath("~/" + tempPath);
            }

            Common_Tatva.WriteLogFomLog(logFormId, "SavePdfData : ViewerTemp.aspx : documentFullPath : ", documentFullPath);
            Dictionary<bool, string> retValue = editablePdf.SavePdfData(strDocumentCollectionView, base64String, documentFullPath);

            UID = Convert.ToString(logFormDetails.Rows[0]["UID"]);
            if (retValue != null && retValue.FirstOrDefault().Key)
                isSuccess = UploadAndDeleteObject(Enum_Tatva.Status.Saved, UID, strDocumentCollectionView, logFormId, documentFullPath);
            else
                General_Class.InsertLogForLogForm(DateTime.Now, string.IsNullOrEmpty(logFormId) ? 0 : int.Parse(logFormId), GetSavedValues(strDocumentCollectionView), documentFullPath, UID, !retValue.FirstOrDefault().Key, retValue.FirstOrDefault().Value);
            Common_Tatva.WriteLogFomLog(logFormId, "SavePdfData : ViewerTemp.aspx", "Function Existed");
        }
        catch (Exception ex)
        {
            General_Class.InsertLogForLogForm(DateTime.Now, string.IsNullOrEmpty(logFormId) ? 0 : int.Parse(logFormId), GetSavedValues(strDocumentCollectionView), string.Empty, UID, true, ex.Message);
            Common_Tatva.WriteLogFomLog(logFormId, "SavePdfData : ViewerTemp.aspx", "In catch : ErrorMessage: " + ex.Message);
            return new string[] { "false", ex.Message };
        }
        if (logFormDetails != null)
            return new string[] { isSuccess.ToString(), logFormDetails.Rows[0]["FolderID"].ToString() };
        else
            return new string[] { isSuccess.ToString(), "0" };
    }

    private static string GetSavedValues(string strDocumentCollectionView)
    {
        try
        {
            Common_Tatva.WriteLogFomLog("0", "GetSavedValues", "Function entered");
            List<PdfItems> documentCollectionView = new List<PdfItems>();
            documentCollectionView = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PdfItems>>(strDocumentCollectionView);
            List<LogKeyValuePair> logs = new List<LogKeyValuePair>();
            foreach (PdfItems item in documentCollectionView)
            {
                if (!string.IsNullOrEmpty(item.Value))
                {
                    LogKeyValuePair logPair = new LogKeyValuePair();
                    if (item.FieldName.ToString().IndexOf('_') >= 0)
                        logPair.Key = item.FieldName.ToString().Substring(0, item.FieldName.ToString().IndexOf('_'));
                    else
                        logPair.Key = item.FieldName.ToString();
                    logPair.Value = item.Value;
                    logs.Add(logPair);
                }
            }
            Common_Tatva.WriteLogFomLog("0", "GetSavedValues", "Function exited");
            return JsonConvert.SerializeObject(logs);
        }
        catch (Exception ex)
        {
            Common_Tatva.WriteLogFomLog("0", "GetSavedValues", "Catch : ErrorMessage : " + ex.Message);
            return string.Empty;
        }
    }

    /// <summary>
    /// Uploads saved data to rackspace
    /// </summary>
    /// <param name="status">File to be submitted or saved</param>
    private static bool UploadAndDeleteObject(Enum_Tatva.Status status, string uid, string strDocumentCollectionView, string logFormId, string documentFullPath)
    {
        string savedValues = GetSavedValues(strDocumentCollectionView);
        Common_Tatva.WriteLogFomLog(logFormId, "UploadAndDeleteObject", "Function entered");
        FileStream fileStream = null;
        MemoryStream memoryStream = new MemoryStream();
        try
        {
            fileStream = new FileStream(documentFullPath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
            memoryStream.SetLength(fileStream.Length);
            fileStream.Read(memoryStream.GetBuffer(), 0, (int)fileStream.Length);
            string errorMsg = string.Empty;
            fileStream.Write(memoryStream.GetBuffer(), 0, (int)fileStream.Length);
            fileStream.Close();
            fileStream.Dispose();
            memoryStream.Close();
            memoryStream.Dispose();
            Common_Tatva.WriteLogFomLog(logFormId, "UploadAndDeleteObject", "After memoryStream and fileStream closed");
            General_Class.UpdateLogForm(DateTime.Now, int.Parse(logFormId));
            Common_Tatva.WriteLogFomLog(logFormId, "UploadAndDeleteObject", "After UpdateLogForm");
            General_Class.InsertLogForLogForm(DateTime.Now, string.IsNullOrEmpty(logFormId) ? 0 : int.Parse(logFormId), savedValues, documentFullPath, uid, false);
            Common_Tatva.WriteLogFomLog(logFormId, "UploadAndDeleteObject", "After InsertLogForLogForm");
            Common_Tatva.WriteLogFomLog(logFormId, "UploadAndDeleteObject", "Function Exited");
            return true;

        }
        catch (Exception ex)
        {
            General_Class.InsertLogForLogForm(DateTime.Now, string.IsNullOrEmpty(logFormId) ? 0 : int.Parse(logFormId), savedValues, documentFullPath, uid, true, ex.Message);
            Common_Tatva.WriteLogFomLog(logFormId, "UploadAndDeleteObject", "In catch : ErrorMessage: " + ex.Message);
            return false;
        }
        finally
        {
            if (fileStream != null)
            {
                fileStream.Close();
                fileStream.Dispose();
            }
            memoryStream.Close();
            memoryStream.Dispose();
        }
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        DownloadFile();
    }

    private void DownloadFile()
    {
        try
        {
            string logFormID = Request.QueryString["id"];
            DataTable logFormDetails = General_Class.GetLogFormDetails(string.IsNullOrEmpty(logFormID) ? 0 : int.Parse(logFormID));

            string tempPath = "Volume/" + logFormDetails.Rows[0]["FolderID"] + "/LogForm/" + logFormDetails.Rows[0]["FileName"];
            string documentFullPath = HttpContext.Current.Server.MapPath("~/" + tempPath);
            if (!Directory.Exists(documentFullPath.Substring(0, documentFullPath.LastIndexOf("\\"))))
            {
                tempPath = "Volume2/" + logFormDetails.Rows[0]["FolderID"] + "/LogForm/" + logFormDetails.Rows[0]["FileName"];
                documentFullPath = HttpContext.Current.Server.MapPath("~/" + tempPath);
            }

            System.IO.FileInfo file = new System.IO.FileInfo(documentFullPath);
            if (file.Exists)
            {
                HttpContext.Current.Response.Clear();
                //var fileName = file.DirectoryName + "\\" + MakeValidFileName(file.Name);
                //HttpContext.Current.Response.WriteFile(fileName);
                var fileName = MakeValidFileName(file.Name);
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + fileName);
                HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.WriteFile(file.FullName);

            }
            else
            {
                HttpContext.Current.Response.Write("This file does not exist.");
            }
        }
        catch (Exception ex)
        { }
    }

    public static string MakeValidFileName(string name)
    {
        string invalidChars = Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
        string invalidReStr = string.Format(@"[{0}]+", invalidChars);
        string replace = Regex.Replace(name, invalidReStr, "_").Replace(";", "").Replace(",", "");
        return replace;
    }
}


public class LogKeyValuePair
{
    public string Key { get; set; }
    public string Value { get; set; }
}


