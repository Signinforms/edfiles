<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="BoughtGiftCard.aspx.cs" Inherits="BoughtGiftCard" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Bought Gift Card</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt form-half-containt">
                <div class="content-title">Please Enter Gift Card No.</div>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <div class="content-box">

                            <fieldset>
                                <label>
                                    <asp:TextBox ID="txtBoxCarNo" runat="server" MaxLength="10" /></label>
                                <asp:ImageButton ID="LoginButton" runat="server" OnClick="OnClick_ImageButton" ImageUrl="~/images/continue_btn.gif" />
                            </fieldset>
                            <fieldset>
                                <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                            </fieldset>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>


</asp:Content>
