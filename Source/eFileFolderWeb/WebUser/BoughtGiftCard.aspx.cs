using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.DAL;
using Shinetech.Utility;
using Account = Shinetech.DAL.Account;

public partial class BoughtGiftCard : BasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
      
           
        }
    }

    protected void OnClick_ImageButton(object sender, ImageClickEventArgs e)
    {
        if (this.txtBoxCarNo.Text.Trim() == "")
        {
            this.lblMessage.Text = "The Gif Card No. is required.";
            return;
        }

        Gift gift = new Gift(this.txtBoxCarNo.Text.Trim());
        if(gift.IsExist)
        {
            if(gift.UseFlag.Value)
            {
                this.lblMessage.Text = "This Card No. you entered has already been used.  To purchase additional EdFiles visit\r\n" +
                                        "http://www.edfiles.com/Office/PurchasePage.aspx or Call 800-579-9190 or email partner@efilefolders.com";
                return;
            }

            string uid = Sessions.SwitchedSessionId;
            //string uid = Membership.GetUser().ProviderUserKey.ToString();
            Account item = new  Account(uid);
            if (item.IsExist)
            {
                // give this user basic start kit licese

                PromoteKit license = new PromoteKit("100");
                if (license.IsExist)
                {

                    try
                    {
                        item.LicenseID.Value = license.LicenceKitID.Value;
                        item.FileFolders.Value = license.FolderCount.Value;
                        item.FreeMonths.Value = license.FreeOnlineStorage.Value;
                        item.EFFManagerAvailable.Value = true;
                        try
                        {
                            gift.UseFlag.Value = true;
                            gift.UserID.Value = item.UID.Value;
                            gift.UserName.Value = item.Name.Value;
                            gift.InputDate.Value = DateTime.Now;
                            gift.Update();
                        }
                        catch { }

                        item.Update();
                        this.lblMessage.Text = "";
                        string strWrongInfor = String.Format("Welcome.  You have successfully created 50 EdFiles with 2 months of storage.");
                        ScriptManager.RegisterClientScriptBlock(UpdatePanel3, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                      
                        //send email to info@edfiles.com

                        Response.Redirect("~/Office/Welcome.aspx", true);

                    }
                    catch (Exception)
                    {
                        this.lblMessage.Text = "There are some system error,Please try again.";
                    }

                }
            }
        }
        else
        {
            this.lblMessage.Text = "Invalid Card No., please try again.  If the message presists, call 800-579-9190.";
        }
    }
}
