﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ForgetPassword.aspx.cs" Inherits="WebUser_ForgetYourPassword" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">
        body
        {
            font-family : 'Open Sans', sans-serif !important;
        }
    </style>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1 style="padding-top:15px;">Forgot Password</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="register-ads">
                <%--<div class="trial-block" style="line-height:normal">
                    <div class="title">FREE TRIAL</div>
                    <p>The best way to experience EdFiles is by trying them. Register and receive 5 Free EdFiles of FREE Access. Once you have tried EdFiles and would like to convert your paper file folders in EdFiles, just purchase a pack. See the Pricing Tab for details.</p>
                </div>--%>
                <div class="calling-block">
                    <span>Register, Create and View EdFiles.... Call</span>
                    <span class="call-number">657-217-3260</span>
                    <%--<span>FREE to further assistance.</span>--%>
                </div>
            </div>
            <div class="form-containt form-half-containt">
                <div class="content-title">Forgot Password</div>
                <div class="content-box">
                    <fieldset>
                        <label>Username</label>
                        <asp:TextBox ID="txtUserName" runat="server" MaxLength="50" Style="font-size:initial;color:initial;font-size:14px;"/>
                    </fieldset>
                    <fieldset>
                        <label>&nbsp;</label>
                        <asp:LinkButton CssClass="create-btn btn green" ID="LinkButton1" runat="server" Text="Submit" OnClick="libGetNewP_Click"></asp:LinkButton>
                        <%--<asp:LinkButton CssClass="create-btn btn green" ID="libGetNewP" runat="server" Text="Get your new password" OnClick="libGetNewP_Click"></asp:LinkButton>--%>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
