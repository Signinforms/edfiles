﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Utility;
using System.IO;

public partial class WebUser_ForgetYourPassword : System.Web.UI.Page
{
    public string strNewPassword = string.Empty;
    public string strUserEMail = string.Empty;
    public string strUserName = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.Request.QueryString["UserName"] == null || Page.Request.QueryString["UserName"] == ""))
        {
            strUserName = Page.Request.QueryString["UserName"];
            txtUserName.Text = strUserName;
        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!this.Page.User.Identity.IsAuthenticated)
        {
            this.MasterPageFile = "~/LoginMasterPage.master";
        }
    }

    protected void libGetNewP_Click(object sender, EventArgs e)
    {
        makeNewPassword();
        strUserName = this.txtUserName.Text.ToString();
        if (Membership.GetUser(strUserName) == null)
        {
            Page.RegisterStartupScript("RssNO", "<script>alert('That username does not exist in our system!');</script>");
            txtUserName.Text = "";
            this.txtUserName.Focus();
            return;
        }
        else
        {
            DataTable dtUserInfor = UserManagement.getUserInfor(strUserName);            
            txtUserName.Text  = "";
            if (dtUserInfor.Rows[0]["IsSubUser"].ToString() =="1"){
                Page.RegisterStartupScript("RssNO", "<script>alert('Please contact your account administrator for password assistance.');</script>");
            }
            else{
                UserManagement.ChangePassword(PasswordGenerator.GetMD5(strNewPassword), strUserName);
                strUserEMail = dtUserInfor.Rows[0]["Email"].ToString();
                sendEmail();
                Page.RegisterStartupScript("RssNO", "<script>alert('Please retrieve a temporary password from your registered email address.If you do not receive an email..check your spam folders. For assistance call 1-657-217-3260');</script>");

            }
            
            Response.Write(" <meta   http-equiv=refresh   content=1;URL=WebLogin.aspx");
        }

    }

    private string makeNewPassword()
    {
        strNewPassword = System.Guid.NewGuid().ToString().Substring(0, 8);
        return strNewPassword;
    }

    private void sendEmail()
    {
        string strMailTemplet = getMailTempletStr();
        strMailTemplet = strMailTemplet.Replace("[[UserName]]", strUserName);
        strMailTemplet = strMailTemplet.Replace("[[Password]]", strNewPassword);

        string strEmailSubject = "set a new password for edFile user";

        Email.SendFromEFileFolder(strUserEMail, strEmailSubject, strMailTemplet);
    }

    private string getMailTempletStr()
    {
        string strMailTemplet = string.Empty;
        string physicalPath = Request.PhysicalPath;
        int index = physicalPath.LastIndexOf("\\");
        string mailTempletUrl = physicalPath.Substring(0, index + 1) + "SendPassword.htm";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

}
