<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="PartnerUser.aspx.cs" Inherits="PartnerUser" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        label {
            font-family:'Open Sans', sans-serif
        }
        select {
            font-size:14px;
            font-family:Arial;
            color:black;
        }
        .inputBox {
            font-size:initial;color:initial;font-size:14px;
        }
        .ajax__validatorcallout_error_message_cell {
            line-height:normal;
            vertical-align:top
        }
    </style>
    <div class="title-container">
        <div class="inner-wrapper">
							<h1 style="padding-top:15px;">Register your Office...FREE Trial!</h1>
							<p>
									In order to start using or trying EdFiles, please complete form below. Select
													a username, password and the DOB for safeguarding your EdFiles from unauthorized
													access. All of our new user receive 5 FREE EdFiles to Try and
													Experience EdFiles. If you require additional assitance, please call 800-579-9190
													24 hours a day, 7 Days a Week.
							</p>
							<div class="clearfix"></div>
					</div>
			</div>
	
			<div class="page-container register-container">
					<div class="inner-wrapper">
							<div class="left-content">
								<div class="form-containt">
									<div class="content-title" style="height:20px;">Login Details</div>
									<div class="content-box">
											<fieldset>
													<label>Username</label>
													<asp:TextBox name="textfield3" runat="server"  ID="textfield3"
															size="30" MaxLength="50" ControlToValidate="textfield3" CssClass="inputBox"/>
													<asp:RequiredFieldValidator runat="server" ID="userNameV" ControlToValidate="textfield3"
															Display="None" ErrorMessage="<b>Required Field Missing</b><br />The UserName is required!" />
													<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="uesrNameValidate" TargetControlID="userNameV"
															HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
	
											</fieldset>
											<fieldset>
													<label>Password</label>
													<asp:TextBox TextMode="password" name="textfield4" runat="server" 
															ID="textfield4" size="30" MaxLength="20" CssClass="inputBox"/>
													<asp:RequiredFieldValidator runat="server" ID="passwordV" ControlToValidate="textfield4"
															Display="None" ErrorMessage="<b>Required Field Missing</b><br />The password is required!" />
													<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
															TargetControlID="passwordV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
													<asp:RegularExpressionValidator ID="passwordN" runat="server" ControlToValidate="textfield4"
															ErrorMessage="<b>Required Field Missing</b><br />The password must be more than 6 numbers!"
															ValidationExpression="^\w{7,20}$" Display="None"></asp:RegularExpressionValidator>
													<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
															TargetControlID="passwordN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
											</fieldset>
									</div>
							</div>
							<div class="form-containt">
									<div class="content-title">Personal Details</div>
									<div class="content-box">
											<fieldset>
													<label>First Name</label>
													<asp:TextBox ID="textfield6" name="textfield6" runat="server" 
															size="30" MaxLength="20" CssClass="inputBox"/>
													<asp:RequiredFieldValidator runat="server" ID="firstNameV" ControlToValidate="textfield6"
															Display="None" ErrorMessage="<b>Required Field Missing</b><br />The first name is required!" />
													<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
															TargetControlID="firstNameV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
											</fieldset>
											<fieldset>
													<label>Last Name</label>
													<asp:TextBox ID="textfield7" runat="server"  size="30" MaxLength="20" CssClass="inputBox"/>
													<asp:RequiredFieldValidator runat="server" ID="lastNameV" ControlToValidate="textfield7"
															Display="None" ErrorMessage="<b>Required Field Missing</b><br />The last name is required!" />
													<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10"
															TargetControlID="lastNameV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
											</fieldset>
	
											<fieldset>
													<label>Telephone</label>
													<asp:TextBox ID="textfield8" name="textfield8" runat="server" 
															size="30" MaxLength="23" CssClass="inputBox"/>
													<asp:RequiredFieldValidator runat="server" ID="telN" ControlToValidate="textfield8"
															Display="None" ErrorMessage="<b>Required Field Missing</b><br />The telephone is required!" />
													<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11"
															TargetControlID="telN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
													<asp:RegularExpressionValidator ID="TelV" ControlToValidate="textfield8" runat="server"
															ErrorMessage="<b>Required Field Missing</b><br />It is not a telphone format."
															ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
													<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
															TargetControlID="TelV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
											</fieldset>
											<fieldset>
													<label>Email</label>
													<asp:TextBox ID="textfield13" runat="server"  size="30" MaxLength="50" CssClass="inputBox"/>
													<asp:RequiredFieldValidator runat="server" ID="EmailReq" ControlToValidate="textfield13"
															Display="None" ErrorMessage="<b>Required Field Missing</b><br />An email is required." />
													<asp:RegularExpressionValidator ID="rev_Email" runat="server" ErrorMessage="<b>Required Field Missing</b><br />It is not an email format."
															ControlToValidate="textfield13" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
															Visible="true" Display="None" Style="line-height:normal"></asp:RegularExpressionValidator>
													<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="EmailReqE" TargetControlID="EmailReq"
															HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
													<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
															TargetControlID="rev_Email" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
											</fieldset>
	
											<fieldset>
													<label>Country</label>
													<asp:DropDownList ID="drpCountry" runat="server" />
											</fieldset>
	
											<fieldset>
													<label>Bought it from</label>
													<asp:TextBox ID="TextBoxBoughtFrom" name="TextBoxBoughtFrom" Text="EdFiles" runat="server" 
															size="30" MaxLength="23" CssClass="inputBox"/>
													<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="TextBoxBoughtFrom"
															Display="None" ErrorMessage="<b>Required Field Missing</b><br />Please tell us the name of the store or the website you purchased the Eff Gift Card From" />
													<ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5"
															TargetControlID="RequiredFieldValidator1" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
											</fieldset>
	
											<fieldset>
													<a href="EdFiles Terms of Service.doc" style="    font-family: 'Open Sans', sans-serif;font-size: 16px;">By registering you agree to edFiles.com's Terms of Service</a></td>
											</fieldset>
									</div>
							</div>
							<div class="podnaslov">
									 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
															<ContentTemplate>
																	<asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
															</ContentTemplate>
															<Triggers>
																	<asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
															</Triggers>
													</asp:UpdatePanel>
							</div>
							<div class="form-submit-btn">
									<asp:Button runat="server" ID="ImageButton1" OnClick="ImageButton1_Click" Text="Create" class="create-btn  btn green" />
							</div>
						</div>
						<div class="right-content">
							<div class="register-ads">
									<div class="trial-block" style="line-height:normal">
											<div class="title">FREE TRIAL</div>
											<p>The best way to experience EdFiles is by trying them. Register and receive 5 Free EdFiles of FREE Access. Once you have tried EdFiles and would like to convert your paper file folders in EdFiles, just purchase a pack. See the Pricing Tab for details.</p>
									</div>
									<div class="calling-block">
											<span>Register directly by calling</span>
											<p class="call-number"><span>800</span><span>-579-9190</span></p>
									</div>
							</div>
						</div>
            
        </div>
    </div>

</asp:Content>
