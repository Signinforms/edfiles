using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Utility;
using Shinetech.DAL;
using Account=Shinetech.DAL.Account;
using System.IO;

public partial class PartnerUser : BasePage
{
    public string strRandom = string.Empty;
    public string strMailLink = string.Empty;
   
    private static readonly string MAILLINK = ConfigurationManager.AppSettings["MAILLINK"];

    private static readonly string PARTNEREMAIL = ConfigurationManager.AppSettings["PARTNEREMAIL"];

    protected void Page_Load(object sender, EventArgs e)
    {
        
        getRandomStr();
        if (!Page.IsPostBack)
        {
            pageLoad();
        }
    }

    protected void pageLoad()
    {
        drpCountry.DataSource = Countries;
        drpCountry.DataValueField = "CountryID";
        drpCountry.DataTextField = "CountryName";
        drpCountry.DataBind();
        drpCountry.SelectedValue = "US";
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!this.Page.User.Identity.IsAuthenticated)
        {
            this.MasterPageFile = "~/LoginMasterPage.master";
        }
    }


    public DataTable States
    {
        get
        {
            object obj = Application["States"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetStates();
                Application["States"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    public DataTable Countries
    {
        get
        {
            object obj = Application["Countries"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetCountries();
                Application["Countries"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    protected void ImageButton1_Click(object sender, EventArgs e)
    {
        string userName = this.textfield3.Text.Trim(); 
        string password = PasswordGenerator.GetMD5(this.textfield4.Text.Trim());
        string createDate = DateTime.Now.ToString();

        string telno = this.textfield8.Text.Trim();
        string email = this.textfield13.Text.Trim();
        string firstname = this.textfield6.Text.Trim();
        string lastname = this.textfield7.Text.Trim();

        string mobile = "";
        string fax = "";
        string companyName = "";
        string address = "";

        string strCountryID = this.drpCountry.SelectedValue;
        string strState = "AA";
        string strOtherState = "";
        string strCity = "";
        string strPostal = "";
        string strBoughtFrom = TextBoxBoughtFrom.Text.Trim();

        getRandomStr();

        if (Membership.GetUser(userName) == null)
        {
            try
            {
                MembershipUser user = Membership.CreateUser(userName, password);
                user.Email = email;
                user.IsApproved = false;
                Roles.CreateRole("Administrator");
                Roles.AddUserToRole(userName, "Administrator");
                Membership.UpdateUser(user);
                

                try
                {
                    #region Create user
                    Account webUser = new Account(user.ProviderUserKey.ToString());
                    webUser.Name.Value = user.UserName;
                    webUser.ApplicationName.Value = Membership.ApplicationName;
                    webUser.IsSubUser.Value = "0";
                    webUser.DOB.Value = DateTime.Parse(createDate);
                    webUser.Firstname.Value = firstname;
                    webUser.Lastname.Value = lastname;
                    webUser.Email.Value = email;
                    webUser.Telephone.Value = telno;

                    webUser.MobilePhone.Value = mobile;
                    webUser.FaxNumber.Value = fax;
                    webUser.CompanyName.Value = companyName;
                    webUser.Address.Value = address;

                    webUser.CountryID.Value = strCountryID;
                    webUser.StateID.Value = strState;
                    webUser.OtherState.Value = strOtherState;
                    webUser.City.Value = strCity;
                    webUser.Postal.Value = strPostal;
                    webUser.BoughtFrom.Value = strBoughtFrom;

                    webUser.RoleID.Value = "Offices";

                    //记录PartnerUser信息
                    if (PartnerID!=null)
                    {
                        webUser.PartnerID.Value = Convert.ToInt32(PartnerID);
                    }


                    UserManagement.CreatNewAccount(webUser);

                    if(PartnerID!=null)
                    {
                        string ip = Request.ServerVariables["REMOTE_ADDR"];
                        UserManagement.InsertVisitor(webUser, ip, this.Session.SessionID);
                    }

                    TempAccount tempAccount = new TempAccount();
                    UserManagement.UpdateTempAccount(user.ProviderUserKey.ToString(), this.textfield3.Text.Trim(), this.textfield4.Text.Trim());

                    this.UserID = webUser.UID.ToString();
                    #endregion
                }
                catch
                {
                    Membership.DeleteUser(user.UserName);
                    string strWrongInfor = String.Format("Error : failed to create the {0} office, please try again.", this.textfield3.Text);
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                    //lblMessage.Text = String.Format("Error : failed to create the {0} office, please try again.", this.textfield3.Text);
                    return;
                }

                try
                {
                    VerificationNumber vn = new VerificationNumber(this.UserID);
                    vn.RandomNumber.Value = strRandom;
                    vn.UserName.Value = userName;
                    vn.InDate.Value = DateTime.Now;
                    UserManagement.CreatVerificationNumber(vn);

                    sendEmail();
                    sendEmailToPartner();
                }
                catch
                { }

                string bgt = "";
                if(!string.IsNullOrEmpty(Request["bought"]))
                {
                    bgt = Request["bought"] as string;
                }
                Response.Redirect("UserConfirmation.aspx?UserName=" + this.textfield3.Text.Trim() + "&bought=" + bgt);

            }
            catch(Exception ex)
            {
                string strWrongInfor = String.Format("Error : failed to create the {0} office, please try again.", this.textfield3.Text);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                //lblMessage.Text = String.Format("Error : failed to create the {0} office, please try again.", this.textfield3.Text);
                return;
            }
        }
        else
        {
            string strWrongInfor = String.Format("Error : {0} office alreadt exits, please input again.", this.textfield3.Text);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
            //lblMessage.Text = String.Format("Error : {0} office alreadt exits, please input again.", this.textfield3.Text);
            return;
        }

        
    }

    public string PartnerID
    {
        get
        {
            object obj = this.Session["Session_Visitor"];
            string pid = null;
            if(obj!=null)
            {
                pid = Convert.ToString(obj);
            }

            return pid;

        }
    }

    public string UserID
    {
        get
        {
            return this.Session["UserID"] as string;
        }
        set
        {
            this.Session["UserID"] = value;
        }
    }

    private string getRandomStr()
    {
        strRandom =  System.Guid.NewGuid().ToString().Substring(0, 8);//随机生成8位即包含字符又包含数字的字符串
        return strRandom;
    }

    private void sendEmail()
    {
        strMailLink = "click this to finish the registration <a href = ' " + MAILLINK + "'";
        strMailLink += " target='_blank'> ";
        strMailLink += " " + MAILLINK + "";
        strMailLink += "</a>";




        string strMailTemplet = getMailTempletStr();
        strMailTemplet = strMailTemplet.Replace("[[UserName]]", this.textfield3.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[[Password]]", this.textfield4.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[[DOB]]", DateTime.Now.ToShortDateString());
        strMailTemplet = strMailTemplet.Replace("[[Email]]", this.textfield13.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[[Code]]", strRandom);
        strMailTemplet = strMailTemplet.Replace("[[MailLink]]", strMailLink);

        string strEmailSubject = "Activate your EdFiles";

        Email.SendFromEFileFolder(this.textfield13.Text.Trim(), strEmailSubject, strMailTemplet);
    }

    private string getMailTempletStr()
    {
        string strMailTemplet = string.Empty;
        string physicalPath = Request.PhysicalPath;
        int index = physicalPath.LastIndexOf("\\");
        string mailTempletUrl = physicalPath.Substring(0, index + 1) + "Mail.htm";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }


    private void sendEmailToPartner()
    {
        string strMailTemplet = getMailTempletStrToPartner();
        strMailTemplet = strMailTemplet.Replace("[[UserName]]", this.textfield3.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[[FirstName]]", this.textfield6.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[[LastName]]", this.textfield7.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[[Email]]", this.textfield13.Text.Trim());
        strMailTemplet = strMailTemplet.Replace("[[Code]]", strRandom);
      

        string strEmailSubject = "New Registration Notification";

        Email.SendFromEFileFolder(PARTNEREMAIL, strEmailSubject, strMailTemplet);
    }

    private string getMailTempletStrToPartner()
    {
        string strMailTemplet = string.Empty;
        string physicalPath = Request.PhysicalPath;
        int index = physicalPath.LastIndexOf("\\");
        string mailTempletUrl = physicalPath.Substring(0, index + 1) + "MailToPartner.htm";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }
}
