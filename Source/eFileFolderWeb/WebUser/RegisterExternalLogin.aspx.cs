﻿using System;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.DAL;
using System.Net;
using Shinetech.Utility;
using Account = Shinetech.DAL.Account;
using System.Globalization;

public partial class WebRegisterExternalLogin : BasePage
{
    protected string ProviderName
    {
        get { return (string)ViewState["ProviderName"] ?? String.Empty; }
        private set { ViewState["ProviderName"] = value; }
    }

    protected string ProviderAccountKey
    {
        get { return (string)ViewState["ProviderAccountKey"] ?? String.Empty; }
        private set { ViewState["ProviderAccountKey"] = value; }
    }

    private void RedirectOnFail()
    {
        Response.Redirect("~/WebUser/WebLogin.aspx");
    }

    protected void Page_Load()
    {
        if (!IsPostBack)
        {
            //var manager = new UserManager();
            //var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            //var signInManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();
            var loginInfo = Context.GetOwinContext().Authentication.GetExternalLoginInfo();
            if (loginInfo == null)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertok",
                                                          "alert(\"" + "Sorry, we are not able to proceed your request for logging in with " + ProviderName + " at this time. Please contact EdFiles administrator." + "\");window.location = 'WebLogin.aspx';", true);
                return;
            }
            DataSet dsNew = new DataSet();
            if (!string.IsNullOrEmpty(loginInfo.Email))
                dsNew = General_Class.IsEmailExists(loginInfo.Email);
            else
            {
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                         "alert(\"" + "For logging in with " + ProviderName + ", we need your Email address. Please give permission to EdFiles for accessing your Email information." + "\");window.location = 'WebLogin.aspx';", true);
                return;
            }
            if (dsNew != null && Convert.ToBoolean(dsNew.Tables[0].Rows[0]["IsExist"]) && dsNew.Tables.Count > 1)
            {
                string ip = Request.ServerVariables["REMOTE_ADDR"];
                string strUserName = Convert.ToString(dsNew.Tables[1].Rows[0]["Name"]);
                string uid = Membership.GetUser(strUserName).ProviderUserKey.ToString();
                Account account = new Account(uid);

                bool isValidUser = true;
                if (account.IsSubUser.ToString() == "1")
                {
                    if (ip.LastIndexOf('%') > 0)
                        ip = ip.Substring(0, ip.LastIndexOf('%'));

                    isValidUser = isValidUser = General_Class.ValidatePublicHolidayRestriction(uid, DateTime.Now.Date.ToString("dd/MM/yyyy"));
                    if (isValidUser)
                        isValidUser = General_Class.ValidateIPLoginRestriction(uid, ip);
                    if (isValidUser)
                        isValidUser = General_Class.ValidateTimeLoginRestriction(uid, System.DateTime.Now.TimeOfDay, (int)System.DateTime.Now.DayOfWeek);
                }
                if (isValidUser)
                {
                    FormsAuthentication.RedirectFromLoginPage(strUserName, true);

                    Sessions.UserId = uid;
                    Sessions.UserName = strUserName;

                    if (account.IsSubUser.ToString() == "1")
                        Sessions.RackSpaceUserID = account.OfficeUID.ToString();
                    else
                        Sessions.RackSpaceUserID = uid;

                    Sessions.SwitchedRackspaceId = Sessions.RackSpaceUserID;
                    Sessions.SwitchedSessionId = Sessions.UserId;
                    Sessions.SwitchedUserName = Sessions.UserName;

                    UserManagement.InsertVisitor(account, ip, this.Session.SessionID, true);
                    UserManagement.InsertUserAccess(account, this.Session.SessionID, ip);

                    if (!string.IsNullOrEmpty(Request.Params["ReturnUrl"]))
                    {
                        Response.Redirect(Request.Params["ReturnUrl"]);
                    }
                    else
                    {
                        Response.Redirect("~/default.aspx");
                    }
                }
                else
                {
                    string strWrongInfor = String.Format("Error : Your login is not authorized to access EdFiles at this time. Please contact your administrator for login schedule.", strUserName);
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok",
                                                         "alert(\"" + strWrongInfor + "window.location = 'WebLogin.aspx';", true);
                    return;
                }
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertok",
                                                          "alert(\"" + "Sorry, we are not able to proceed your request for logging in with " + ProviderName + " at this time. Please contact EdFiles administrator." + "\");window.location = 'WebLogin.aspx';", true);
                return;
            }
        }
    }
}