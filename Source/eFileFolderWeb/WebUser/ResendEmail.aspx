<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ResendEmail.aspx.cs" Inherits="WebUser_ResendEmail" Title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <div class="title-container">
        <div class="inner-wrapper">
            <h1>An verification e-mail</h1>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="inner-wrapper">
        <div class="page-container">
            <div class="inner-wrapper">
                <div class="aboutus-contant">
                   <p>An verification e-mail has been resend to you successfully.</p>
                    <p>
                       Please click <strong><a href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>WebUser/UserConfirmation.aspx">here</a> </strong> to enter the verification number provided
                    </p>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

