<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="UserConfirmation.aspx.cs" Inherits="UserConfirmation" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>User Confirmation</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt form-half-containt">
                <p>
                    An e-mail has been send to verify your registration. Please click on the link provided
                                    in the e-mail message or enter the verification number provided
                </p>
                <div class="podnaslov">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="content-box">
                    <fieldset>
                        <label>
                            <asp:TextBox name="textfield3" runat="server" class="form_tekst_field" ID="textfield3"
                                size="20" /></label>
                        <asp:ImageButton runat="server" name="ImageButton1" ID="ImageButton1" ImageUrl="~/images/submit_btn.gif"
                            ImageAlign="Middle" OnClick="imageField_Click" />
                    </fieldset>
                </div>
                <p></p>
                <p>
                    Please click
                    <asp:LinkButton ID="LinkButton1" OnClick="LinkButton1_Click" Style="color: #333333" runat="server"><strong>here</strong></asp:LinkButton>
                    to resend the verification e-mail
                </p>
            </div>
        </div>
    </div>
</asp:Content>
