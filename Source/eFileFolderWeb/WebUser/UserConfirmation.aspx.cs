using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Configuration;
using System.Web.Profile;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.Utility;
using Shinetech.DAL;
using System.IO;
using Account = Shinetech.DAL.Account;

public partial class UserConfirmation : System.Web.UI.Page
{
    string strRandom;
    string strUserName;
    string strPassword;
    string strDOB;
    string strEmail;
    string UserID;
    Boolean bolRemME = false;
    public string strMailLink = string.Empty;
  

    private static readonly string MAILLINK = ConfigurationManager.AppSettings["MAILLINK"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!(Page.Request.QueryString["UserName"] == null || Page.Request.QueryString["UserName"] == ""))
        {
            strUserName = Convert.ToString(Page.Request.QueryString["UserName"]);
        }
        if (!(Page.Request.QueryString["bolRemME"] == null || Page.Request.QueryString["bolRemME"] == ""))
        {
            bolRemME = Convert.ToBoolean(bolRemME);
        }
        UserID = UserManagement.getUserIDByUserName(strUserName).Rows[0][0].ToString();

    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!this.Page.User.Identity.IsAuthenticated)
        {
            this.MasterPageFile = "~/LoginMasterPage.master";
        }
    }


    protected void imageField_Click(object sender, ImageClickEventArgs e)
    {
        string code = this.textfield3.Text.Trim();
        VerificationNumber vn = new VerificationNumber(this.UserID);
        string test = vn.RandomNumber.Value;
        if (vn.IsExist)
        {
            if (vn.RandomNumber.Value == code)
            {
                string strUserName = vn.UserName.ToString();
                MembershipUser user = Membership.GetUser(strUserName);
                user.IsApproved = true;
                
                Membership.UpdateUser(user);
                Account account = new Account(this.UserID);

               

                if (account.IsExist)
                {
                    account.CreateDate.Value = DateTime.Now;
                    account.StoragePaymentDate.Value = DateTime.Now;
                    account.Update();
                }

                //发送一封邮件给顾客
                sendWelcomeEmail();

                FormsAuthentication.RedirectFromLoginPage(strUserName, false);
                lblMessage.Text = string.Format("Congratulation, the office: {0} is registered successfully. You can sign in now.", vn.UserName);
                UserManagement.DeleteTempAccount(strUserName);
               
                if (!string.IsNullOrEmpty(Request["bought"]))
                {
                    Response.Redirect("~/WebUser/BoughtGiftCard.aspx",true);
                    return;
                }

                Response.Redirect("~/Default.aspx");
            }
            else
            {
                string strWrongInfor = String.Format("The verification number is not matched.");
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                //lblMessage.Text = string.Format("The verification number is'nt matched.", vn.UserName);
            }
        }

    }

    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(this.UserID))
        {
            lblMessage.Text = string.Format("Please register first.");
            return;
        }

        VerificationNumber vn = new VerificationNumber(this.UserID);
        if (vn.IsExist)
        {
            strRandom = getRandomStr();
            vn.RandomNumber.Value = strRandom;
            vn.InDate.Value = DateTime.Now;
            UserManagement.UpdateVerificationNumber(vn);

            sendEmail();
        }
        else
        {
            lblMessage.Text = string.Format("Please register first.");
            return;
        }

    }

    private void sendEmail()
    {
        getUserInfor(strUserName);


        strMailLink = "click this to finish the registration <a href = ' " + MAILLINK + "'";
        strMailLink += " target='_blank'> ";

     
        strMailLink += " " + MAILLINK + "";
        strMailLink += "</a>";

        string strMailTemplet = getMailTempletStr();
        strMailTemplet = strMailTemplet.Replace("[[UserName]]", strUserName);
        strMailTemplet = strMailTemplet.Replace("[[Password]]", strPassword);
        //strMailTemplet = strMailTemplet.Replace("[[DOB]]", strDOB);
        strMailTemplet = strMailTemplet.Replace("[[Email]]", strEmail);

        strMailTemplet = strMailTemplet.Replace("[[Code]]", strRandom);
        strMailTemplet = strMailTemplet.Replace("[[MailLink]]", strMailLink);


        string strEmailSubject = "Activate your EdFiles";

        Email.SendFromEFileFolder(strEmail, strEmailSubject, strMailTemplet);

    }

    private void sendWelcomeEmail()
    {
        getUserInfor(strUserName);
        string strMailTemplet = getWelcomeMailTemplate();

        strMailTemplet = strMailTemplet.Replace("[UserName]", strUserName);

        string strEmailSubject = "Welcome to EdFiles";

        Email.SendFromEFileFolder(strEmail, strEmailSubject, strMailTemplet);

    }

    private string getMailTempletStr()
    {
        string strMailTemplet = string.Empty;
        string physicalPath = Request.PhysicalPath;
        int index = physicalPath.LastIndexOf("\\");
        string mailTempletUrl = physicalPath.Substring(0, index + 1) + "Mail.htm";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    private string getWelcomeMailTemplate()
    {

        string strMailTemplet = string.Empty;
        string physicalPath = Request.PhysicalPath;
        int index = physicalPath.LastIndexOf("\\");
        string mailTempletUrl = physicalPath.Substring(0, index + 1) + "DearUser.htm";
        StreamReader sr = new StreamReader(mailTempletUrl);
        string sLine = "";
        while (sLine != null)
        {
            sLine = sr.ReadLine();
            if (sLine != null)
                strMailTemplet += sLine;
        }
        sr.Close();
        return strMailTemplet;
    }

    private string getRandomStr()
    {
        strRandom = System.Guid.NewGuid().ToString().Substring(0, 8);//随机生成8位即包含字符又包含数字的字符串
        return strRandom;
    }


    private void getUserInfor(string strUserName)
    {
        DataTable userInfor = new DataTable();
        userInfor = UserManagement.getUserInfor(strUserName);

        strUserName = userInfor.Rows[0]["name"].ToString();
        strPassword = UserManagement.GetPasswordFromTempAccount(strUserName);
        strDOB = userInfor.Rows[0]["DOB"].ToString();
        strEmail = userInfor.Rows[0]["Email"].ToString();


    }

}
