<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="ViewEFF.aspx.cs" Inherits="ViewEFF" Title="" %>

<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.ComponentModel" %>
<%--<%@ Register Assembly="Toolkit.ColorPicker" Namespace="DnnSun.WebControls.Toolkit"
    TagPrefix="cc1" %>--%>
<%@ Register TagPrefix="fjx" Namespace="com.flajaxian" Assembly="com.flajaxian.FileUploader" %>
<%--<%@ Register Src="../Controls/ReminderControl.ascx" TagName="ReminderControl" TagPrefix="uc2" %>--%>
<%@ Register Src="../Controls/ReminderControl1.ascx" TagName="ReminderControl" TagPrefix="uc2" %>
<%@ Register Assembly="Shinetech.Framework" Namespace="Shinetech.Framework.Controls"
    TagPrefix="cc1" %>
<%@ Register TagPrefix="cust" Namespace="Shinetech.Engines.Adapters" Assembly="FormatEngine" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<asp:ScriptManager ID="scriptManager1" runat="server"/>--%>

    <script type="text/javascript">
        function FileStateChanged(uploader, file, httpStatus, isLast) {
            Flajaxian.fileStateChanged(uploader, file, httpStatus, isLast);
            //    if(file.state > Flajaxian.File_Uploading && isLast){
            //        Flajaxian_onclick();
            //    }

            if (file.state == 3 && isLast) {
                alert("Successfully uploaded EFF.");
                Flajaxian_onclick();
            }
            else if (file.state == 5 && isLast) {
                alert("Failed uploaded EFF.");
            }
        }

        function Flajaxian_onclick() {

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            //prm._doPostBack('UpdatePanel3', '');
            prm._doPostBack('WebPager1', '');

        }
    </script>

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>View Shared EdFiles </h1>
            <p>
                Use the loader below to view EdFiles saved on your hard drive.  Click on the browse button and select the .eff file from your computer to upload and view the EdFiles.  This way you can share your EdFiles.  Please feel free to chat, email or call customer service with any questions.
            </p>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="form-containt listing-contant">
                <div class="left-content">
                    <div class="content-box listing-view">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <fieldset>
                                    <div class="prevnext" style="float: right">
                                        <cc1:WebPager ID="WebPager1" ControlToPaginate="GridView1" PagerStyle="NumericPages" CssClass="prevnext"
                                            PageSize="10" OnPageIndexChanged="WebPager1_PageIndexChanged" runat="server" />
                                        <asp:HiddenField ID="SortDirection1" runat="server" Value="" />
                                    </div>
                                </fieldset>
                                <div class="work-area">
                                    <div class="overflow-auto">
                                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="false"
                                            AllowSorting="True" OnSorting="GridView1_Sorting" OnRowDataBound="GridView1_RowDataBound" DataKeyNames="EFFID"
                                            EmptyDataText="You have no EFF folders now." CssClass="datatable listing-datatable">
                                            <PagerSettings Visible="False" />
                                            <AlternatingRowStyle CssClass="odd" />
                                            <RowStyle CssClass="even" HorizontalAlign="Center" />
                                            <%-- <SelectedRowStyle CssClass="table_tekst" BackColor="#F8F8F5" />
                                    <HeaderStyle CssClass="form_title" BackColor="#DBD9CF" />--%>
                                            <Columns>
                                                <asp:TemplateField ShowHeader="false" ItemStyle-Wrap="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label8" runat="server" Text="Action" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="ic-icon ic-delete" Text="Delete" OnClick="LinkButton1_Click"></asp:LinkButton>
                                                        <ajaxToolkit:ConfirmButtonExtender ID="cbeDelEFileFolder" runat="server" ConfirmText="Are you sure to delete this EFF ?"
                                                            TargetControlID="LinkButton1">
                                                        </ajaxToolkit:ConfirmButtonExtender>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="FolderName" HeaderText="FolderName" SortExpression="FolderName"></asp:BoundField>
                                                <asp:BoundField DataField="Firstname" HeaderText="Firstname" SortExpression="Firstname"></asp:BoundField>
                                                <asp:BoundField DataField="Lastname" HeaderText="Lastname" SortExpression="Lastname"></asp:BoundField>
                                                <asp:BoundField DataField="DOB" HeaderText="DOB" DataFormatString="{0:MM.dd.yyyy}" SortExpression="DOB" />
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label3" runat="server" Text="Security" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="TextBoxLevel" Text='<%# Bind("SecurityLevel") %>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="OtherInfo" HeaderText="Tabs Number" />
                                                <asp:BoundField DataField="OtherInfo2" HeaderText="Total Size(Mb)" DataFormatString="{0:F2}" HtmlEncode="false" />
                                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ShowHeader="false">
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Label4" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <%--<asp:LinkButton ID="LinkButton2" runat="server"  Text="View" CommandArgument='<%#string.Format("{0},{1},{2}",Eval("FolderID"),Eval("Firstname"),Eval("Lastname")) %>' OnCommand="LinkButton2_Click" OnClientClick='<%#string.Format("window.open(\"../EFileFolderV1.html?ID={0}\",\"eBook\",\"toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no\")",Eval("FolderID"))%>'/>--%>
                                                        <asp:LinkButton ID="LinkButton2" runat="server" CssClass="ic-icon ic-view" CommandArgument='<%#string.Format("{0},{1},{2}",Eval("FolderID"),Eval("Firstname"),Eval("Lastname")) %>' OnCommand="LinkButton2_Click" OnClientClick='<%#string.Format("window.open(\"../Office/WebFlashViewer.aspx?ID={0}\",\"eBook\",\"toolbar=no,menubar=no,location=no,Resizable=yes,fullscreen=yes,status=no\")",Eval("FolderID"))%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="WebPager1" EventName="PageIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <fieldset>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <p>
                                        <ajaxToolkit:CollapsiblePanelExtender ID="PanelExtender1" runat="Server" TargetControlID="ContentPanel" ExpandControlID="TitlePanel"
                                            CollapseControlID="TitlePanel" Collapsed="false" TextLabelID="Label1" ExpandedText="Hide EFF loader..."
                                            CollapsedText="Show EFF loader..." ImageControlID="Image1" ExpandedImage="~/images/collapse_blue.jpg"
                                            CollapsedImage="~/images/expand_blue.jpg" SuppressPostBack="true">
                                        </ajaxToolkit:CollapsiblePanelExtender>
                                        <asp:Panel ID="TitlePanel" runat="server" CssClass="collapsePanelHeader">
                                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/expand_blue.jpg" />
                                            <asp:Label ID="Label1" Font-Bold="true" runat="server">Show EFF loader...</asp:Label>
                                        </asp:Panel>
                                        <%--EFF Loader--%>
                                        <asp:Panel ID="ContentPanel" runat="server" CssClass="collapsePanel">
                                            <p>
                                                <br>
                                                Click the Browse button to upload EFF
                                            </p>
                                            <fjx:FileUploader ID="FileUploader1" MaxNumberFiles="10" MaxFileSize="1MB" MaxFileSizeReachedMessage="No files bigger than {0} are allowed"
                                                AllowedFileTypes="EFF Files(*eff):*.eff;" UseInsideUpdatePanel="true" JsFunc_FileStateChanged="FileStateChanged" runat="server">
                                                <Adapters>
                                                    <cust:EFFAdapter Runat="server" />
                                                </Adapters>
                                            </fjx:FileUploader>
                                        </asp:Panel>
                                    </p>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </fieldset>
                    </div>
                </div>
                <div class="right-content">
                    <%--<div class="quick-find">
                        <uc1:QuickFind ID="QuickFind1" runat="server" />
                    </div>--%>
                    <div class="quick-find" style="margin-top: 15px;">
                        <div class="find-inputbox">
                            <uc2:ReminderControl ID="ReminderControl1" runat="server" />
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
