using System;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using AjaxControlToolkit;
using DataQuicker2.Framework;
//using DnnSun.WebControls.Toolkit;
using Shinetech.DAL;
using Shinetech.Engines.Adapters;
using Shinetech.Framework.Controls;

using com.flajaxian;


public partial class ViewEFF : LicensePage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            if (FileUploader1.FileIsPosted)
            {
                FileUploader1.State["UID"] = Sessions.SwitchedSessionId;
                //FileUploader1.State["UID"] = Membership.GetUser().ProviderUserKey.ToString();
            }
            else
            {
                GetData();
                BindGrid();
            }
        }
        else
        {
            GetData();
            BindGrid();
        }
    }

    #region 数据绑定
    /// <summary>
    /// 获取数据源
    /// </summary>
    private void GetData()
    {
        try
        {
            string uid = Sessions.SwitchedSessionId;
            //string uid = Membership.GetUser().ProviderUserKey.ToString();
            this.DataSource = FileFolderManagement.GetSharedEFFs(uid);
        }
        catch
        {
            this.DataSource = new DataTable();
        }



    }
    /// <summary>
    /// 初始化绑定
    /// </summary>
    private void BindGrid()
    {
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }
    #endregion

    protected void WebPager1_PageIndexChanged(object sender, PageChangedEventArgs e)
    {
        WebPager1.CurrentPageIndex = e.NewPageIndex;
        WebPager1.DataSource = this.DataSource;
        WebPager1.DataBind();
    }

    public DataTable DataSource
    {
        get
        {
            return this.Session["DataSource"] as DataTable;
        }
        set
        {
            this.Session["DataSource"] = value;
        }
    }

   protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortDirection = "";
        string sortExpression = e.SortExpression;
        if (this.Sort_Direction == SortDirection.Ascending)
        {
            this.Sort_Direction = SortDirection.Descending;
            sortDirection = "DESC";
        }
        else
        {
            this.Sort_Direction = SortDirection.Ascending;
            sortDirection = "ASC";
        }
        DataView Source = new DataView(this.DataSource);
        Source.Sort = e.SortExpression + " " + sortDirection;

        this.DataSource = Source.ToTable();
        BindGrid();
    }

    private SortDirection Sort_Direction
    {
        get
        {
            if (string.IsNullOrEmpty(SortDirection1.Value))
            {
                SortDirection1.Value = Enum.GetName(typeof(SortDirection), SortDirection.Ascending);
            }

            return (SortDirection)Enum.Parse(typeof(SortDirection), this.SortDirection1.Value);

        }
        set
        {
            this.SortDirection1.Value = Enum.GetName(typeof(SortDirection), value);
        }
    }

    protected void LinkButton2_Click(Object sender, CommandEventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;

        ((MasterPage)this.Master).AddFolderItem(e.CommandArgument.ToString());
    }


    public DataTable GetTabsDataSource(string effid)
    {
        Divider dv = new Divider();
        ObjectQuery query = dv.CreateQuery();
        query.SetCriteria(dv.EffID, Convert.ToInt32(effid));

        DataTable table = new DataTable();

        try
        {
            query.Fill(table);
            
        }
        catch
        {
            table = null;
        }
        return table; 
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label textBox = e.Row.FindControl("TextBoxLevel") as Label;
            if(textBox!=null)
            {
                if (textBox.Text == "0") textBox.Text = "Private";
                if (textBox.Text == "1") textBox.Text = "Protected";
                if (textBox.Text == "2") textBox.Text = "Public";
            }
        }
    }

    /// <summary>
    /// Remove an EFF from list
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        LinkButton btnLink = sender as LinkButton;
        if (btnLink == null) return;
        GridViewRow row = (GridViewRow)btnLink.NamingContainer;
        if (row == null) return;
        int effId = Convert.ToInt32(this.GridView1.DataKeys[row.RowIndex].Value);
        try
        {
            FileFolderManagement.RemoveEFFolder(effId);
        }
        catch {}
        finally
        {
            GetData();
            BindGrid();
        }

    }
}


