<%@ Page Language="C#" MasterPageFile="~/LoginMasterPage.master" AutoEventWireup="true"
    CodeFile="WebLogin.aspx.cs" Inherits="WebLogin" Title="" %>

<%@ Register Src="~/Controls/OpenAuth.ascx" TagName="OpenAuth" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style type="text/css">

        #socialLoginList
        {
            margin:auto;
        }
        body
        {
            font-family: 'Open Sans', sans-serif !important;
        }

        p
        {
            padding: 0 !important;
        }

        label
        {
            min-width: 120px !important;
        }

        .loginOption
        {
            padding-left: 55px;
            font-family: ' Open Sans', sans-serif;
            border: solid 1px #b8b8b8;
            color: black;
            background-position: 15px !important;
            width:100% !important;
        }

        .Google
        {
            background: url("../images/google2.png") no-repeat 0 8px transparent;
        }

        .Google:active
        {
            background: url("../images/google2.png") no-repeat 0 8px transparent;
        }

        .Microsoft
        {
            background: url("../images/office2.png") no-repeat 0 8px transparent;
        }

        .Microsoft:active
        {
            background: url("../images/office2.png") no-repeat 0 8px transparent;
        }

        .nav-outer ul li a
        {
            font-weight: 600;
            font-size: 15px;
            color: #c3c3c3;
            padding: 10px;
            display: block;
            text-transform: none;
            color: #343434;
            font-family: 'Segoe UI' !important;
            line-height: 19px;
        }

        /*.innerSpan
        {
            position: absolute;
            z-index: 999;
            background-color: #ffffff;
            font-size: 14px;
            display: inline-block;
            height: 22px;
            width: 30px;
            line-height: 1.6;
            top: -11px;
            border-radius: 50%;
        }

        .outerSpan
        {
            text-align: center;
            position: relative;
            border-right: 1px solid #eee;
            font-size: 10px;
            height: 100%;
            display: inline-block;
            z-index: 1;
        }*/
    </style>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1 style="padding-top: 20px">Account Login</h1>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="left-content">
                <div class="form-containt loginBox" style="display: inline-block;width:60%">
                    <div class="content-title" style="margin-top: 22px;">Login Details</div>
                    <div class="content-box" style="margin-top: 25px;">
                        <asp:Login ID="login1" RememberMeSet="false" runat="server" OnAuthenticate="Login" Width="100%">
                            <LayoutTemplate>
                                <div>
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <fieldset>
                                    <label>Username</label>
                                    <asp:TextBox ID="UserName" runat="server" MaxLength="50" autofocus="true" Style="font-size: initial; color: initial; font-size: 14px;" />
                                </fieldset>
                                <fieldset>
                                    <label>Password</label>
                                    <asp:TextBox ID="Password" TextMode="Password" runat="server" MaxLength="28" Style="font-size: initial; color: initial; font-size: 14px;" />
                                </fieldset>
                                <fieldset>
                                    <label>&nbsp;</label>
                                    <asp:CheckBox ID="RememberMe" runat="server" Text="Remember me next time." />
                                </fieldset>
                                <fieldset>
                                    <label>&nbsp;</label>
                                    <asp:Button ID="LoginButton" CssClass="create-btn btn green" Text="Log In" CommandName="Login" runat="server" TabIndex="0" />

                                    <%--<asp:Button ID="BtnSignUp" CssClass="create-btn btn green" Text="Sign Up" runat="server" TabIndex="0" OnClick="BtnSignUp_Click" Style="margin-left:10px;"/>--%>
                                    <%-- <asp:HyperLink ID="HyperLink2" runat="server" CssClass="create-btn btn green" NavigateUrl="~/WebUser/WebOfficeUser.aspx">
																							Sign Up
																					</asp:HyperLink>--%>
                                </fieldset>
                            </LayoutTemplate>
                        </asp:Login>
                        <fieldset>
                            <label>&nbsp;</label>
                            <a href="<%= ConfigurationManager.AppSettings["VirtualDir"] %>WebUser/ForgetPassword.aspx" style="font-family: 'Open Sans', sans-serif; font-size: 16px; line-height: normal; display: inline">Forgot Your Password?</a>
                        </fieldset>

                    </div>
                </div>
                <div class="mainSpanDiv">
                    <span class="outerSpan">
                        <span class="innerSpan">OR</span>
                    </span>
                </div>
                <div class="loginOptionBox">
                    <uc1:OpenAuth ID="OpenAuth1" runat="server" />
                </div>
            </div>
            <div class="right-content" style="line-height: normal">
                <div class="register-ads">
                    <%-- <div class="trial-block">
                        <div class="title">FREE TRIAL</div>
                        <p>The best way to experience EdFiles is by trying them. Register and receive 5 Free EdFiles of FREE Access. Once you have tried EdFiles and would like to convert your paper file folders in EdFiles, just purchase a pack. See the Pricing Tab for details.</p>
                    </div>--%>
                    <div class="calling-block">
                        <span>Register directly by calling</span>
                        <%--<p class="call-number"><span>1-855</span><span>-334-5336</span></p> --%>
                        <p class="call-number"><span>1-657</span><span>-217-3260</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            $(document).keydown(function (e) {
                if (e.keyCode == 13) {
                    var input = $('input[id$="LoginButton"][src$="submit_btn.gif"]');
                    if (input) {
                        input.click();
                    }
                    return true;
                } else if (e.keyCode == 8 || e.keyCode == 46) {
                    return true;
                }

            });
        });
    </script>
</asp:Content>
