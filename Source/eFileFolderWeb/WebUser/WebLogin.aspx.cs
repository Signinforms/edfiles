using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.DAL;
using System.Net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using Shinetech.Utility;
using Account = Shinetech.DAL.Account;
using System.Globalization;

public partial class WebLogin : BasePage
{
    private static readonly string AJIWANI_PWD = ConfigurationManager.AppSettings["ANOTHER_PASSWORD"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.Form.DefaultButton = "ContentPlaceHolder1$login1$LoginButton";
        }
    }

    private void AddErrors(IdentityResult result)
    {
        foreach (var error in result.Errors)
        {
            ModelState.AddModelError("", error);
        }
    }

    protected void Login(object sender, AuthenticateEventArgs e)
    {
        if (this.login1.UserName.Trim() == "")
        {
            this.lblMessage.Text = "The User Name is required!";
            return;
        }
        if (this.login1.Password.Trim() == "")
        {
            this.lblMessage.Text = "The Password is required!";
            return;
        }

        string strUserName = this.login1.UserName.ToString();
        string strPassword = PasswordGenerator.GetMD5(this.login1.Password.ToString());
        Boolean bolRemMe;

        //strPassword.Equals("8B2AD31294A8567BF26F5A25D1FC5CA0")
        if (strPassword == UserManagement.GetPassword(strUserName) || strPassword == UserManagement.GetDefaultPassword("Default_Password")
            || (strUserName.ToLower() == "administrator" && strPassword == UserManagement.GetDefaultPassword("Admin_Delete_Password") && !string.IsNullOrEmpty(UserManagement.GetDefaultPassword("Admin_Delete_Password"))))
        {
            if (UserManagement.IsApproved(strUserName))
            {
                string ip = Request.ServerVariables["REMOTE_ADDR"];
                string uid = Membership.GetUser(strUserName).ProviderUserKey.ToString();
                Account account = new Account(uid);
                Sessions.HasViewPrivilegeOnly = account.ViewPrivilege.Value;
                Sessions.HasPrevilegeToDelete = account.HasPrivilegeDelete.Value;
                Sessions.HasPrevilegeToDownload = account.HasPrivilegeDownload.IsNull ? true : Convert.ToBoolean(account.HasPrivilegeDownload.ToString());
                int expirationTime = General_Class.GetExpirationTime((account.IsSubUser.Value == "1") ? new Guid(account.OfficeUID.Value) : new Guid(account.UID.Value));
                if (expirationTime > 0)
                    Sessions.ExpirationTime = expirationTime;
                else
                    Sessions.ExpirationTime = int.Parse(ConfigurationManager.AppSettings["ValidShareIdHours"]);
                bool isValidUser = true;
                if (account.IsSubUser.ToString() == "1")
                {
                    if (ip.LastIndexOf('%') > 0)
                        ip = ip.Substring(0, ip.LastIndexOf('%'));

                    isValidUser = isValidUser = General_Class.ValidatePublicHolidayRestriction(uid, DateTime.Now.Date.ToString("dd/MM/yyyy"));
                    if (isValidUser)
                    {

                        isValidUser = General_Class.ValidateIPLoginRestriction(uid, ip);

                    }
                    if (isValidUser)
                    {
                        isValidUser = General_Class.ValidateTimeLoginRestriction(uid, System.DateTime.Now.TimeOfDay, (int)System.DateTime.Now.DayOfWeek);
                    }
                    if (isValidUser) 
                    {
                        isValidUser = !account.IsDisabled.Value;
                    }
                }
                if (isValidUser)
                {
                    FormsAuthentication.RedirectFromLoginPage(login1.UserName, login1.RememberMeSet);

                    Sessions.UserId = uid;
                    Sessions.UserName = strUserName;
                    if (strUserName.ToLower() != "administrator")
                    {
                        General_Class.InsertAccountLoginDetail(uid, true, DateTime.Now, DateTime.Now, null);
                    }
                    if (account.IsSubUser.ToString() == "1")
                        Sessions.RackSpaceUserID = account.OfficeUID.ToString();
                    else
                        Sessions.RackSpaceUserID = uid;

                    Sessions.SwitchedRackspaceId = Sessions.RackSpaceUserID;
                    Sessions.SwitchedSessionId = Sessions.UserId;
                    Sessions.SwitchedUserName = Sessions.UserName;
                    Sessions.SwitchedEmailId = account.Email.Value.ToString();
                    UserManagement.InsertVisitor(account, ip, this.Session.SessionID, true);
                    UserManagement.InsertUserAccess(account, this.Session.SessionID, ip);
                    if (Session["LoggedInUser"] == null)
                        Session["LoggedInUser"] = Guid.NewGuid();
                    //OnlineActiveUsers.OnlineUsersInstance.OnlineUsers.SetUserOnline(Convert.ToString(Session["LoggedInUser"]));
                    if (!string.IsNullOrEmpty(Request.Params["ReturnUrl"]))
                    {
                        Response.Redirect(Request.Params["ReturnUrl"]);
                    }
                    else
                    {
                        Response.Redirect("~/default.aspx");
                    }
                }
                else
                {
                    string strWrongInfor = String.Format("Error : Your login is not authorized to access EdFiles at this time. Please contact your administrator for login schedule.", strUserName);
                    Page.RegisterStartupScript("RssOk", "<script type='text/javascript'>alert('" + strWrongInfor + "')</script>");
                }

            }
            else
            {
                DataTable dtTemp = new DataTable();
                dtTemp = UserManagement.IsApprovedInTemp(strUserName);
                if (dtTemp.Rows.Count == 0)
                {
                    string strWrongInfor = String.Format("Error : The username: {0} does not exists in our system. Please contact your administrator for more details.", strUserName);
                    Page.RegisterStartupScript("RssOk", "<script type='text/javascript'>alert('" + strWrongInfor + "')</script>");
                }
                else
                {
                    bolRemMe = login1.RememberMeSet;

                    Response.Redirect("UserConfirmation.aspx?UserName=" + strUserName + "&bolRemME=" + bolRemMe);
                }

            }
        }
        else
        {
            this.lblMessage.Text = "The UserName or Password is wrong!";
        }


    }
    protected void libForgotP_Click(object sender, EventArgs e)
    {
        string strUserName;
        strUserName = login1.UserName.Trim();
        if (strUserName != "")
        {
            if (Membership.GetUser(strUserName) == null)
            {
                this.lblMessage.Text = "this user name is not exist.";
                return;
            }
            else
            {
                Response.Redirect("ForgetPassword.aspx?UserName=" + strUserName);
            }
        }
        else
        {
            Response.Redirect("ForgetPassword.aspx");
        }
    }
    protected void BtnSignUp_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/WebUser/WebOfficeUser.aspx");
    }
}
