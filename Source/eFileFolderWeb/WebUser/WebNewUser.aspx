<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="WebNewUser.aspx.cs" Inherits="WebUser_WebNewUser" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%-- <link href="../New/css/bootstrap.min.css" rel="stylesheet" />
    <script src="../New/js/edFiles/bootstrap.min.js"></script>--%>
    <%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<link href="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css"
    rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js"></script>
<link href="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/css/bootstrap-multiselect.css"
    rel="stylesheet" type="text/css" />
<script src="http://cdn.rawgit.com/davidstutz/bootstrap-multiselect/master/dist/js/bootstrap-multiselect.js"
    type="text/javascript"></script>    --%>
    <style>
        .txtTime, .optTime
        {
            width: 30% !important;
        }

        .tdTime
        {
            width: 170px;
            font-size:15px;
        }

        .btnIPAddr
        {
            padding-top: 5px;
            padding-bottom: 5px;
        }

        .divIPAdd, .divTimeRest
        {
            display: none;
            padding-top: 5px;
            margin-left: 10px;
        }

        .divPubHoliday
        {
            display: none;
        }

        .tblLogin
        {
            display: none;
        }

        .multiselect
        {
            width: 200px;
        }


        .selectBox-outer
        {
            position: relative;
            display: inline-block;
        }

        .selectBox select
        {
            width: 100%;
            font-weight: bold;
        }

        .overSelect
        {
            position: absolute;
            left: 0;
            right: 0;
            top: 0;
            bottom: 0;
        }

        #publicHolidayList
        {
            width: 100%;
            display: none;
            border: 1px #dadada solid;
            position: absolute;
            left: 0;
            top: 100%;
            z-index: 9;
            background: #fff;
            max-height: 172px;
            overflow-y: auto;
        }

            #publicHolidayList label
            {
                display: block;
                padding: 0 5px;
                width: auto;
                min-width: initial;
                cursor: pointer;
            }

                #publicHolidayList label:hover
                {
                    background-color: #e5e5e5;
                }

            #publicHolidayList input
            {
                display: inline-block;
            }
    </style>

    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Create Users</h1>
            <p>Please input the fields below to create a new user.</p>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <asp:UpdatePanel ID="UpdatePanelUser" runat="server">
                <ContentTemplate>
                    <div class="form-containt">
                        <div class="content-title">Login Details</div>
                        <div class="content-box">
                            <fieldset>
                                <label>Username</label>
                                <asp:TextBox name="textfield3" runat="server" ID="textfield3" CssClass="requiredGreen"
                                    size="30" MaxLength="120" ControlToValidate="textfield3" />
                                <asp:RequiredFieldValidator runat="server" ID="userNameV" ControlToValidate="textfield3"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The UserName is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="uesrNameValidate" TargetControlID="userNameV"
                                    HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                            </fieldset>
                            <fieldset>
                                <label>Password</label>
                                <asp:TextBox TextMode="password" name="textfield4" runat="server" CssClass="requiredGreen"
                                    ID="textfield4" size="30" MaxLength="20" />
                                <asp:RequiredFieldValidator runat="server" ID="passwordV" ControlToValidate="textfield4"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The password is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
                                    TargetControlID="passwordV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                <asp:RegularExpressionValidator ID="passwordN" runat="server" ControlToValidate="textfield4"
                                    ErrorMessage="<b>Required Field Missing</b><br />The password must be more than 6 numbers!"
                                    ValidationExpression="^\w{7,20}$" Display="None">
                                </asp:RegularExpressionValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
                                    TargetControlID="passwordN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                            </fieldset>
                            <fieldset>
                                <label>Privilege 1</label>
                                <asp:CheckBox name="boxfield5" runat="server" Text="Privileges to delete folders" ID="boxfield5"
                                    size="30" />
                            </fieldset>
                            <fieldset>
                                <label>Privilege 2</label>
                                <asp:CheckBox name="prvToDwnload" runat="server" Text="Privileges to download folders" ID="prvToDwnload"
                                    size="30" />
                            </fieldset>
                            <fieldset>
                                <label>Privilege 3</label>
                                <asp:CheckBox name="boxfield6" runat="server" Text="Privileges to view only" ID="boxfield6"
                                    size="30" />
                            </fieldset>
                            <fieldset>
                                <label>Restrict</label>
                                <asp:CheckBox name="boxfield5" runat="server" Text="Restrict IP Address" ID="chkIpAdd" size="30" Checked="false" />
                                <%--   <div>--%>

                                <table id="tblIPAdd" style="display: none;">
                                    <tbody>
                                        <tr id="indexRow0" class="indexRow">
                                            <td>
                                                <label>&nbsp;</label>
                                            </td>
                                            <td>
                                                <input type="hidden" value="0" class="hdnIPId"></td>
                                            <td>
                                                <input type="text" id="txtIPAdd" width="100%" placeholder="IP Address" /></td>
                                            <td>
                                                <button id="btnAddIPAddr" class="btnIPAddr" type="button" width="90%" onclick="return AddIPAddTextBoxes(this)">
                                                    <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/add-icon.png" title="Add" /></button></td>
                                            <td>
                                                <button id="btnRemoveIPAddr" class="btnIPAddr" type="button" width="90%" onclick="return RemoveIPAddTextBoxes(this)">
                                                    <img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" />
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div id="divIPAdd" class="divIPAdd">
                                    <label>&nbsp;</label>
                                    <input id="btnIPAddrSave" type="button" value="Save" class="blue btn-small" style="margin-right: 5px;" />
                                    <input id="btnIPAddrCancel" type="button" value="Cancel" class="blue btn-small" />
                                </div>
                                <%-- </div>--%>
                            </fieldset>
                            <fieldset>
                                <label>Restrict</label>
                                <asp:CheckBox name="boxfield5" runat="server" Text="Restrict Login Times" ID="chkTimeSpan" size="30" Checked="false" />
                                <div class="clear divPubHoliday">
                                    <label>&nbsp;</label>
                                    <div class="selectBox-outer">
                                        <div class="selectBox" onclick="showCheckboxesPublicHolidays()">
                                            <select>
                                                <option>Select Public Holiday(s)</option>
                                            </select>
                                            <div class="overSelect"></div>
                                        </div>
                                        <div id="publicHolidayList" style="display: none;"></div>
                                    </div>
                                </div>

                                <table id="tblLogin" class="tblLogin">
                                    <tr>
                                        <td>
                                            <label>&nbsp;</label>
                                        </td>
                                        <td>
                                            <label>WeekDays</label>
                                        </td>
                                        <td>
                                            <label>From Time: </label>
                                        </td>
                                        <td>
                                            <label>To Time:</label>
                                        </td>
                                    </tr>
                                    <%--All Days--%>
                                    <tr>
                                        <td>
                                            <label>&nbsp;</label></td>
                                        <td class="tdTime">
                                            <input type="checkbox" class="chkWeekDays chkAllDays" />
                                            <span>All Days</span>
                                        </td>
                                        <td class="tdTime">
                                            <input type="text" placeholder="Hour" class="txtTime txtFromHour" value="12" />
                                            <input type="text" placeholder="Minute" class="txtTime txtFromMin" value="00" />
                                            <select id="optAllFrom" class="optTime optFrom">
                                                <option>AM</option>
                                                <option>PM</option>
                                            </select>
                                        </td>
                                        <td class="tdTime">
                                            <input type="text" placeholder="Hour" class="txtTime txtToHour" value="11" />
                                            <input type="text" placeholder="Minute" class="txtTime txtToMin" value="59" />
                                            <select id="optAllTo" class="optTime optTo">
                                                <option>AM</option>
                                                <option selected="selected">PM</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <%--Sunday--%>
                                    <tr>
                                        <td>
                                            <label>&nbsp;</label></td>
                                        <td class="tdTime">
                                            <input type="checkbox" class="chkWeekDays" />
                                            <span>Sunday</span>
                                        </td>
                                        <td class="tdTime">
                                            <input type="text" placeholder="Hour" class="txtTime txtFromHour" value="12" />
                                            <input type="text" placeholder="Minute" class="txtTime txtFromMin" value="00" />
                                            <select id="Select3" class="optTime optFrom">
                                                <option>AM</option>
                                                <option>PM</option>
                                            </select>
                                        </td>
                                        <td class="tdTime">
                                            <input type="text" placeholder="Hour" class="txtTime txtToHour" value="11" />
                                            <input type="text" placeholder="Minute" class="txtTime txtToMin" value="59" />
                                            <select id="Select4" class="optTime optTo">
                                                <option>AM</option>
                                                <option selected="selected">PM</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <%--Monday--%>
                                    <tr>
                                        <td>
                                            <label>&nbsp;</label></td>
                                        <td class="tdTime">
                                            <input type="checkbox" class="chkWeekDays" />
                                            <span>Monday</span>
                                        </td>
                                        <td class="tdTime">
                                            <input type="text" placeholder="Hour" class="txtTime txtFromHour" value="12" />
                                            <input type="text" placeholder="Minute" class="txtTime txtFromMin" value="00" />
                                            <select id="Select5" class="optTime optFrom">
                                                <option>AM</option>
                                                <option>PM</option>
                                            </select>
                                        </td>
                                        <td class="tdTime">
                                            <input type="text" placeholder="Hour" class="txtTime txtToHour" value="11" />
                                            <input type="text" placeholder="Minute" class="txtTime txtToMin" value="59" />
                                            <select id="Select6" class="optTime optTo">
                                                <option>AM</option>
                                                <option selected="selected">PM</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <%--Tuesday--%>
                                    <tr>
                                        <td>
                                            <label>&nbsp;</label></td>
                                        <td class="tdTime">
                                            <input type="checkbox" class="chkWeekDays" />
                                            <span>Tuesday</span>
                                        </td>
                                        <td class="tdTime">
                                            <input type="text" placeholder="Hour" class="txtTime txtFromHour" value="12" />
                                            <input type="text" placeholder="Minute" class="txtTime txtFromMin" value="00" />
                                            <select id="Select1" class="optTime optFrom">
                                                <option>AM</option>
                                                <option>PM</option>
                                            </select>
                                        </td>
                                        <td class="tdTime">
                                            <input type="text" placeholder="Hour" class="txtTime txtToHour" value="11" />
                                            <input type="text" placeholder="Minute" class="txtTime txtToMin" value="59" />
                                            <select id="Select2" class="optTime optTo">
                                                <option>AM</option>
                                                <option selected="selected">PM</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <%--WednesDay--%>
                                    <tr>
                                        <td>
                                            <label>&nbsp;</label></td>
                                        <td class="tdTime">
                                            <input type="checkbox" class="chkWeekDays" />
                                            <span>Wednesday</span>
                                        </td>
                                        <td class="tdTime">
                                            <input type="text" placeholder="Hour" class="txtTime txtFromHour" value="12" />
                                            <input type="text" placeholder="Minute" class="txtTime txtFromMin" value="00" />
                                            <select id="Select7" class="optTime optFrom">
                                                <option>AM</option>
                                                <option>PM</option>
                                            </select>
                                        </td>
                                        <td class="tdTime">
                                            <input type="text" placeholder="Hour" class="txtTime txtToHour" value="11" />
                                            <input type="text" placeholder="Minute" class="txtTime txtToMin" value="59" />
                                            <select id="Select8" class="optTime optTo">
                                                <option>AM</option>
                                                <option selected="selected">PM</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <%-- Thursday--%>
                                    <tr>
                                        <td>
                                            <label>&nbsp;</label></td>
                                        <td class="tdTime">
                                            <input type="checkbox" class="chkWeekDays" />
                                            <span>Thursday</span>
                                        </td>
                                        <td class="tdTime">
                                            <input type="text" placeholder="Hour" class="txtTime txtFromHour" value="12" />
                                            <input type="text" placeholder="Minute" class="txtTime txtFromMin" value="00" />
                                            <select id="Select9" class="optTime optFrom">
                                                <option>AM</option>
                                                <option>PM</option>
                                            </select>
                                        </td>
                                        <td class="tdTime">
                                            <input type="text" placeholder="Hour" class="txtTime txtToHour" value="11" />
                                            <input type="text" placeholder="Minute" class="txtTime txtToMin" value="59" />
                                            <select id="Select10" class="optTime optTo">
                                                <option>AM</option>
                                                <option selected="selected">PM</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <%--Friday--%>
                                    <tr>
                                        <td>
                                            <label>&nbsp;</label></td>
                                        <td class="tdTime">
                                            <input type="checkbox" class="chkWeekDays" />
                                            <span>Friday</span>
                                        </td>
                                        <td class="tdTime">
                                            <input type="text" placeholder="Hour" class="txtTime txtFromHour" value="12" />
                                            <input type="text" placeholder="Minute" class="txtTime txtFromMin" value="00" />
                                            <select id="Select11" class="optTime optFrom">
                                                <option>AM</option>
                                                <option>PM</option>
                                            </select>
                                        </td>
                                        <td class="tdTime">
                                            <input type="text" placeholder="Hour" class="txtTime txtToHour" value="11" />
                                            <input type="text" placeholder="Minute" class="txtTime txtToMin" value="59" />
                                            <select id="Select12" class="optTime optTo">
                                                <option>AM</option>
                                                <option selected="selected">PM</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <%--Saturday--%>
                                    <tr>
                                        <td>
                                            <label>&nbsp;</label></td>
                                        <td class="tdTime">
                                            <input type="checkbox" class="chkWeekDays" />
                                            <span>Saturday</span>
                                        </td>
                                        <td class="tdTime">
                                            <input type="text" placeholder="Hour" class="txtTime txtFromHour" value="12" />
                                            <input type="text" placeholder="Minute" class="txtTime txtFromMin" value="00" />
                                            <select id="Select13" class="optTime optFrom">
                                                <option>AM</option>
                                                <option>PM</option>
                                            </select>
                                        </td>
                                        <td class="tdTime">
                                            <input type="text" placeholder="Hour" class="txtTime txtToHour" value="11" />
                                            <input type="text" placeholder="Minute" class="txtTime txtToMin" value="59" />
                                            <select id="Select14" class="optTime optTo">
                                                <option>AM</option>
                                                <option selected="selected">PM</option>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                                <div id="divTimeRest" class="divTimeRest">
                                    <label>&nbsp;</label>
                                    <input id="btnSaveTimeRest" type="button" value="Save" class="blue btn-small" style="margin-right: 5px;" />
                                    <input id="btnCancelTimeRest" type="button" value="Cancel" class="blue btn-small" />

                                </div>
                            </fieldset>
                            <fieldset>
                                <label>Hide Locked Tabs</label>
                                <asp:CheckBox name="chkRestrictTab" runat="server" ID="chkRestrictTab" size="30" Checked="false" />
                            </fieldset>
                            <fieldset>
                                <label>First Name</label>
                                <asp:TextBox ID="textfield6" name="textfield6" runat="server" CssClass="requiredGreen"
                                    size="30" MaxLength="20" />
                                <asp:RequiredFieldValidator runat="server" ID="firstNameV" ControlToValidate="textfield6"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The first name is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                                    TargetControlID="firstNameV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                            </fieldset>
                            <fieldset>
                                <label>Last Name</label>
                                <asp:TextBox ID="textfield7" runat="server" size="30" MaxLength="20" CssClass="requiredGreen" />
                                <asp:RequiredFieldValidator runat="server" ID="lastNameV" ControlToValidate="textfield7"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The last name is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10"
                                    TargetControlID="lastNameV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                            </fieldset>
                            <fieldset>
                                <label>Email</label>
                                <asp:TextBox ID="textfield13" runat="server" size="30" MaxLength="50" CssClass="requiredGreen" />
                                <asp:RequiredFieldValidator runat="server" ID="EmailReq" ControlToValidate="textfield13"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />An email is required." />
                                <asp:RegularExpressionValidator ID="rev_Email" runat="server" ErrorMessage="<b>Required Field Missing</b><br />It is not an email format."
                                    ControlToValidate="textfield13" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                    Visible="true" Display="None"></asp:RegularExpressionValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="EmailReqE" TargetControlID="EmailReq"
                                    HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                                    TargetControlID="rev_Email" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                            </fieldset>
                            <fieldset>
                                <label>Telephone</label>
                                <asp:TextBox ID="textfield8" name="textfield8" runat="server" CssClass="requiredGreen"
                                    size="30" MaxLength="23" />
                                <asp:RequiredFieldValidator runat="server" ID="telN" ControlToValidate="textfield8"
                                    Display="None" ErrorMessage="<b>Required Field Missing</b><br />The telephone is required!" />
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11"
                                    TargetControlID="telN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                                <asp:RegularExpressionValidator ID="TelV" ControlToValidate="textfield8" runat="server"
                                    ErrorMessage="<b>Required Field Missing</b><br />It is not a telphone format."
                                    ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                                    TargetControlID="TelV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                            </fieldset>
                            <fieldset>
                                <label>Mobile</label>
                                <asp:TextBox name="textfield11" runat="server" ID="textfield11"
                                    size="30" MaxLength="20" />
                                <asp:RegularExpressionValidator ID="mobileV" ControlToValidate="textfield11" runat="server"
                                    ErrorMessage="<b>Required Field Missing</b><br />It is not a Mobile format."
                                    ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                                <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9"
                                    TargetControlID="mobileV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft" />
                            </fieldset>



                        </div>
                    </div>
                    <div class="form-containt">
                        <%--  <div class="content-title">Personal Details</div>--%>
                        <div class="content-box">
                            <fieldset>
                                <label>Protected Groups</label>
                                <asp:CheckBox name="groupfield0" runat="server" Text="" ID="protectedSec"
                                    size="30" /><label>Privileges to view Protected EdFiles</label>
                            </fieldset>
                            <fieldset>
                                <label>Private Groups</label>
                                <asp:CheckBox name="groupfield1" runat="server" Text="" ID="groupfield1"
                                    size="30" /><asp:TextBox ID="grouplink1" Text="Group 1" runat="server" />
                            </fieldset>
                            <fieldset>
                                <label>&nbsp;</label>
                                <asp:CheckBox name="groupfield2" runat="server" Text="" ID="groupfield2"
                                    size="30" /><asp:TextBox ID="grouplink2" Text="Group 2" runat="server" />
                            </fieldset>
                            <fieldset>
                                <label>&nbsp;</label>
                                <asp:CheckBox name="groupfield3" runat="server" Text="" ID="groupfield3"
                                    size="30" /><asp:TextBox ID="grouplink3" Text="Group 3" runat="server" />
                            </fieldset>
                            <fieldset>
                                <label>&nbsp;</label>
                                <asp:CheckBox name="groupfield4" runat="server" Text="" ID="groupfield4"
                                    size="30" /><asp:TextBox ID="grouplink4" Text="Group 4" runat="server" />
                            </fieldset>
                            <fieldset>
                                <label>&nbsp;</label>
                                <asp:CheckBox name="groupfield5" runat="server" Text="" ID="groupfield5"
                                    size="30" /><asp:TextBox ID="grouplink5" Text="Group 5" runat="server" />
                            </fieldset>
                            <fieldset>
                                <label>Public Groups</label>
                                <asp:CheckBox name="groupfield6" runat="server" Text="" ID="publicSec"
                                    size="30" /><label>Privileges to view Public EdFiles</label>
                            </fieldset>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
            <div class="podnaslov">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div class="form-submit-btn">
                <asp:Button runat="server" ID="ImageButton1" OnClick="ImageButton1_Click" Text="Create" CssClass="create-btn btn green" />
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hdnIPAddresses" runat="server" />
    <asp:HiddenField ID="hdnTimeRest" runat="server" />
    <asp:HiddenField ID="hdnPublicHolidays" runat="server" />
    <script type="text/javascript">
        var pubExpanded = false;
        var WeekDays = {
            Sunday: 0,
            Monday: 1,
            Tuesday: 2,
            Wednesday: 3,
            Thursday: 4,
            Friday: 5,
            Saturday: 6,
            AllDays: 7
        }
        var ipAddRegEx6 = /^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$/;
        var ipAddRegEx = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
        var numberRegExp = /^\d*(?:\.\d{1,2})?$/;

        function showCheckboxesPublicHolidays() {
            var checkboxes = document.getElementById("publicHolidayList");
            if (!pubExpanded) {
                checkboxes.style.display = "block";
                pubExpanded = true;
            } else {
                checkboxes.style.display = "none";
                pubExpanded = false;
            }
            return false;
        }

        $(document).ready(function () {

            $("#<%= chkIpAdd.ClientID %>").prop("checked", false);
            $("#<%= chkTimeSpan.ClientID %>").prop("checked", false);

            GetPublicHolidays();

            $("#<%= chkIpAdd.ClientID %>").change(function (e) {
                if ($("#<%= chkIpAdd.ClientID %>").prop("checked")) {
                        $("#tblIPAdd, #divIPAdd").show();

                    }
                    else
                        $("#tblIPAdd, #divIPAdd").hide();

                });

            $("#<%= chkTimeSpan.ClientID %>").change(function (e) {
                if ($("#<%= chkTimeSpan.ClientID %>").prop("checked")) {
                    $("#tblLogin, #divTimeRest, .divPubHoliday").show();

                }
                else
                    $("#tblLogin, #divTimeRest, .divPubHoliday").hide();

            });

            function GetPublicHolidays() {
                $.ajax(
                {
                    type: "POST",
                    url: '../WebUser/WebNewUser.aspx/GetPublicHolidays',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        if (result.d.length > 0) {
                            var holidays = JSON.parse(result.d);
                            for (var i = 0; i < holidays.length; i++) {
                                var container = $('#publicHolidayList');

                                $('<label for="cb' + holidays[i].HolidayID + '"><input type="checkbox" id="cb' + holidays[i].HolidayID + '" value=' + holidays[i].HolidayID + '> ' + holidays[i].Name.trim() + '</label>').appendTo(container);
                            }
                        }
                    },
                    error: function (result) {
                    }
                });
            }

            $(".txtTime").change(function () {
                var val = $(this).val();
                if (!val.match(numberRegExp)) {
                    alert("Only numbers are allowed!");
                    $(this).val("").focus();
                }
                else if (($(this).hasClass("txtFromHour") || $(this).hasClass("txtToHour")) && (parseInt(val) > 12 || parseInt(val) <= 0)) {
                    alert("Please enter valid hours between 1 to 12.")
                    $(this).val("").focus();
                }
                else if (($(this).hasClass("txtFromMin") || $(this).hasClass("txtToMin")) && (parseInt(val) > 59 || parseInt(val) < 0)) {
                    alert("Please enter valid minutes between 0 to 59.")
                    $(this).val("").focus();
                }
            });

            $(".chkAllDays").change(function () {

                $("#tblLogin").find('tr').each(function (i, el) {
                    var chkWeek = $(el).children().find(".chkWeekDays");
                    var allDays = $($('table#tblLogin tr')).find(".chkAllDays").parent().parent();
                    if (!chkWeek.hasClass("chkAllDays") && chkWeek.length > 0) {
                        $(el).children().find(".chkWeekDays").prop("checked", $(".chkAllDays").prop("checked"));

                        if ($(".chkAllDays").prop("checked")) {
                            $(el).find("input, select, checkbox").prop("disabled", true);
                            $($('table#tblLogin tr')[i]).find(".txtFromHour").val($(allDays).find(".txtFromHour").val());
                            $($('table#tblLogin tr')[i]).find(".txtFromMin").val($(allDays).find(".txtFromMin").val());
                            $($('table#tblLogin tr')[i]).find(".txtToHour").val($(allDays).find(".txtToHour").val());
                            $($('table#tblLogin tr')[i]).find(".txtToMin").val($(allDays).find(".txtToMin").val());
                            $($('table#tblLogin tr')[i]).find(".optFrom").val($(allDays).find(".optFrom").val());
                            $($('table#tblLogin tr')[i]).find(".optTo").val($(allDays).find(".optTo").val());
                        }
                        else {
                            $(el).find("input, select, checkbox").prop("disabled", false);
                        }
                    }
                });

            });
        });

        function AddIPAddTextBoxes(ele) {
            var index = parseInt($($(ele).parent().parent().find('[class*=hdnIPId]')).val());
            var ipAddr = $(ele).closest('tr').find('#txtIPAdd').val();
            if (ipAddr == "") {
                alert("Please enter IP Address.");
                return false;
            }
            else if (!ipAddr.match(ipAddRegEx) && (!ipAddr.match(ipAddRegEx6))) {
                alert("Please enter valid IP Address.");
                return false;
            }
            CreateDynamicIPAddTextBox(index, false);
            return false;
        }

        function GetDynamicTextBox(value) {
            var test = '<td><label>&nbsp;</label></td>' +
                        '<td><input type="hidden" value="0" class="hdnIPId"></td>' +
                        '<td><input type="text" id="txtIPAdd" width="100%" maxlength="100" placeholder="IP Address" /></td>' +
                        '<td><button id="btnAddIPAddr" class="btnIPAddr" type="button" width="90%" onclick="return AddIPAddTextBoxes(this)"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/add-icon.png" title="Add" /></button></td>' +
                        '<td><button id="btnRemoveIPAddr" class="btnIPAddr" type="button" width="90%" onclick="return RemoveIPAddTextBoxes(this)"><img src="<%= ConfigurationManager.AppSettings["VirtualDir"].ToString() %>New/images/delete-icon.png" alt="Remove" title="Remove" /></button></td>'
            return test;
        }

        function CreateDynamicIPAddTextBox(index, isRender) {
            var tr = document.createElement('tr');
            tr.innerHTML = GetDynamicTextBox(index);

            if (!isRender)
                tr.setAttribute("id", "indexRow" + (index = index + 1));
            else
                tr.setAttribute("id", "indexRow" + index);
            $('table#tblIPAdd tr:last').after(tr);
            SetIPAddButtonVisibility();

            //Allow maximum of 5 key,value pairs
            if ($('table#tblIPAdd tr').length == 5) {
                $(tr).find('#btnAddIPAddr').parent().hide();
            }
        }

        var deleteIndexData = "";
        function RemoveIPAddTextBoxes(ele) {
            if ($(ele).closest('tr').find('input.hdnIPId').val() != 0) {
                var hdnId = $(ele).closest('tr').find('input.hdnIPId').val();
                var test = deleteIndexData.indexOf(hdnId);
                if (test <= 0)
                    deleteIndexData = deleteIndexData + hdnId + ",";
            }
            $(ele).closest('tr').remove();
            SetIPAddButtonVisibility();
            return false;
        }

        function SetIPAddButtonVisibility() {
            var rowCount = $('table#tblIPAdd tr').length;
            for (i = 0; i < rowCount; i++) {
                var $item = $($('table#tblIPAdd tr')[i]);

                if (i == (rowCount - 1)) {
                    $item.find('#btnRemoveIPAddr').parent().show();
                    $item.find('#btnAddIPAddr').parent().show();
                    if (i == 0) {
                        $item.find('#btnRemoveIPAddr').parent().hide();
                    }
                }
                else {
                    $item.find('#btnRemoveIPAddr').parent().show();
                    $item.find('#btnAddIPAddr').parent().hide()
                }
            }
        }

        $("#btnIPAddrSave").click(function (e) {
            var csvIPAddr = "";
            $('#' + '<%=hdnIPAddresses.ClientID%>').val("");
            for (i = 0; i < $('table#tblIPAdd tr').length; i++) {
                var $item = $($('table#tblIPAdd tr')[i]);
                if (($item.find('#txtIPAdd').val() != null && $item.find('#txtIPAdd').val() != ""))
                    csvIPAddr = csvIPAddr + $item.find('#txtIPAdd').val() + ",";
            }
            csvIPAddr = csvIPAddr.substr(0, csvIPAddr.lastIndexOf(','));
            $('#' + '<%=hdnIPAddresses.ClientID%>').val(csvIPAddr);
        });

        $("#btnSaveTimeRest").click(function () {
            var arrTimeRest = [];
            $("#" + '<%=hdnTimeRest.ClientID %>').val("");
            $("#" + '<%=hdnPublicHolidays.ClientID %>').val("");
            var isCopy = false;
            for (i = 0; i < $('table#tblLogin tr').length; i++) {

                var day = $($('table#tblLogin tr')[i]).find("span").text();
                var fromHour = $($('table#tblLogin tr')[i]).find(".txtFromHour").val();
                var fromMin = $($('table#tblLogin tr')[i]).find(".txtFromMin").val();
                var toHour = $($('table#tblLogin tr')[i]).find(".txtToHour").val();
                var toMin = $($('table#tblLogin tr')[i]).find(".txtToMin").val();
                var fromTime = $($('table#tblLogin tr')[i]).find(".optFrom").val();
                var toTime = $($('table#tblLogin tr')[i]).find(".optTo").val();

                if ($($('table#tblLogin tr')[i]).find(".chkAllDays").length > 0 && $($('table#tblLogin tr')[i]).find(".chkWeekDays").prop("checked")) {
                    day = day.replace(' ', '');
                    if (fromHour == "" || fromHour == undefined || toHour == "" || toHour == undefined) {
                        alert("Restriction hours can not be blank!")
                        return;
                    }
                    arrTimeRest.push({ "Day": WeekDays[day], "From": fromHour + ":" + fromMin + " " + fromTime, "To": toHour + ":" + toMin + " " + toTime });
                    break;
                }
                else if ($($('table#tblLogin tr')[i]).find(".chkWeekDays").prop("checked")) {
                    if (fromHour == "" || fromHour == undefined || toHour == "" || toHour == undefined) {
                        alert("Restriction hours can not be blank!")
                        return;
                    }
                    arrTimeRest.push({ "Day": WeekDays[day], "From": fromHour + ":" + fromMin + " " + fromTime, "To": toHour + ":" + toMin + " " + toTime });
                }
            }
            $("#" + '<%=hdnTimeRest.ClientID %>').val(JSON.stringify(arrTimeRest));
            var checkboxes = $("#publicHolidayList").find("input[type=checkbox]");
            var publicHolidays = "";
            if (checkboxes.length > 0) {
                for (var i = 0; i < checkboxes.length; i++) {
                    if ($(checkboxes[i]).prop("checked"))
                        publicHolidays += $(checkboxes[i]).val() + ", ";
                }
                publicHolidays = publicHolidays.substr(0, publicHolidays.lastIndexOf(','));
            }
            $("#" + '<%=hdnPublicHolidays.ClientID %>').val(publicHolidays);
        });

        $("#btnIPAddrCancel").click(function (e) {
            $("#tblIPAdd").find("tr:gt(0)").remove();
            var row = $(".indexRow");
            $(row).find("#txtIPAdd").val("");
            $(row).find("#btnAddIPAddr").parent().show();
            $(row).find("#btnRemoveIPAddr").parent().hide();
            $(row).find(".hdnIPId").val(0);
            $('#' + '<%=hdnIPAddresses.ClientID%>').val("");
        });



    </script>
</asp:Content>
