using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Shinetech.DAL;
using Shinetech.Utility;
using Account = Shinetech.DAL.Account;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Services;

public partial class WebUser_WebNewUser : LicensePage
{

    public string strRandom = string.Empty;
    EFileFolderJSONWS eFileFolderJSONWS = new EFileFolderJSONWS();

    protected void Page_Load(object sender, EventArgs e)
    {
        getRandomStr();

        if (!Page.IsPostBack)
        {
            //string mode = WebConfigurationManager.AppSettings["https_http"].ToLower();
            //if (mode.Equals("https") && !Page.Request.IsSecureConnection)
            //{
            //    string lsURL = "https://";
            //    string port = WebConfigurationManager.AppSettings["https_port"];
            //    lsURL = string.Concat(lsURL, Request.Url.Host, ":", port, Request.Url.PathAndQuery);
            //    lsURL = Server.UrlDecode(lsURL);
            //    Response.Redirect(lsURL, true);
            //    return;
            //}

            pageLoad();
        }
    }

    protected void pageLoad()
    {
        //drpState.DataSource = States;
        //drpState.DataValueField = "StateID";
        //drpState.DataTextField = "StateName";
        //drpState.DataBind();

        //drpCountry.DataSource = Countries;
        //drpCountry.DataValueField = "CountryID";
        //drpCountry.DataTextField = "CountryName";
        //drpCountry.DataBind();
        //drpCountry.SelectedValue = "US";
        //if(this.Page.User.IsInRole("Offices"))
        if (Common_Tatva.IsUserSwitched() || this.Page.User.IsInRole("Offices"))
        {
            Account officeUser = new Account(Sessions.SwitchedSessionId);
            //Account officeUser = new Account(Membership.GetUser().ProviderUserKey.ToString());
            string groupsName = officeUser.GroupsName.Value;

            if (groupsName != null && !string.IsNullOrEmpty(groupsName.Trim()))
            {
                string[] groupNames = groupsName.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 1; i <= groupNames.Length; i++)
                {
                    TextBox groupText = UpdatePanelUser.FindControl("grouplink" + i) as TextBox;
                    groupText.Text = groupNames[i - 1];
                }
            }

            //

        }


    }

    public DataTable States
    {
        get
        {
            object obj = Application["States"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetStates();
                Application["States"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    public DataTable Countries
    {
        get
        {
            object obj = Application["Countries"];

            DataTable table = null;
            if (obj == null)
            {
                table = UserManagement.GetCountries();
                Application["Countries"] = table;
            }
            else
            {
                table = obj as DataTable;
            }

            return table;
        }
    }

    private string getRandomStr()
    {
        strRandom = System.Guid.NewGuid().ToString().Substring(0, 8);//随机生成8位即包含字符又包含数字的字符串
        return strRandom;
    }

    protected void ImageButton1_Click(object sender, EventArgs e)
    {
        string newUserId = string.Empty;
        string userName = this.textfield3.Text.Trim();
        string password = PasswordGenerator.GetMD5(this.textfield4.Text.Trim());
        string createDate = DateTime.Now.ToShortDateString();// this.textfield5.Text.Trim();

        string email = this.textfield13.Text.Trim();
        string firstname = this.textfield6.Text.Trim();
        string lastname = this.textfield7.Text.Trim();

        string telephone = this.textfield8.Text.Trim();
        string mobile = this.textfield11.Text.Trim();
        string fax = ""; //this.textfield9.Text.Trim();
        string companyName = "";//this.textfield14.Text.Trim();
        string address = "";//this.textfield10.Text.Trim();


        string strCountryID = ""; //this.drpCountry.SelectedValue;
        string strState = ""; //this.drpState.SelectedValue;
        string strOtherState = "";// this.txtOtherState.Text.Trim();
        string strCity = ""; //this.txtCity.Text.Trim();
        string strPostal = ""; //this.txtPostal.Text.Trim();

        bool hasPrivilegeToDelete = boxfield5.Checked;
        bool hasPrivilegeForProtected = protectedSec.Checked;
        bool hasPrivilegeForPublic = publicSec.Checked;
        bool hasPrivilegeToDownload = prvToDwnload.Checked;
        bool viewPrivilege = boxfield6.Checked;

        string groups = string.Empty;
        string groupsName = string.Empty;

        for (int i = 1; i <= 5; i++)
        {
            CheckBox groupBox = UpdatePanelUser.FindControl("groupfield" + i) as CheckBox;
            TextBox groupText = UpdatePanelUser.FindControl("grouplink" + i) as TextBox;
            groupsName += string.Format("{0}|", string.IsNullOrEmpty(groupText.Text.Trim()) ? "Group " + i : groupText.Text.Trim());

            if (groupBox.Checked)
            {
                groups += i + "|";
            }
        }

        //卢远宗 2014-10-23，自定义分组名称,保存在该officeuser的Groups下面


        if (Membership.GetUser(userName) == null)
        {
            try
            {

                MembershipUser user = Membership.CreateUser(userName, password);
                user.Email = email;
                user.IsApproved = true;
                //Roles.CreateRole("Users");
                Roles.AddUserToRole(userName, "Users");
                Membership.UpdateUser(user);


                try
                {
                    Account officeUser = new Account(Sessions.SwitchedSessionId);
                    //Account officeUser = new Account(Membership.GetUser().ProviderUserKey.ToString());
                    officeUser.GroupsName.Value = groupsName;
                    officeUser.Update();

                    #region Create user
                    Account webUser = new Account(user.ProviderUserKey.ToString());
                    webUser.Name.Value = user.UserName;
                    webUser.ApplicationName.Value = Membership.ApplicationName;
                    webUser.IsSubUser.Value = "1";
                    webUser.DOB.Value = DateTime.Parse(createDate);
                    webUser.Firstname.Value = firstname;
                    webUser.Lastname.Value = lastname;
                    webUser.Email.Value = email;
                    webUser.OfficeUID.Value = Sessions.SwitchedSessionId;
                    //webUser.OfficeUID.Value = Membership.GetUser().ProviderUserKey.ToString();
                    webUser.PartnerID.Value = officeUser.PartnerID.Value;
                    webUser.Telephone.Value = telephone;
                    webUser.MobilePhone.Value = mobile;
                    webUser.FaxNumber.Value = fax;
                    webUser.CompanyName.Value = companyName;
                    webUser.Address.Value = address;

                    webUser.CountryID.Value = strCountryID;
                    webUser.StateID.Value = strState;
                    webUser.OtherState.Value = strOtherState;
                    webUser.City.Value = strCity;
                    webUser.Postal.Value = strPostal;

                    webUser.RoleID.Value = "Users";

                    webUser.HasPrivilegeDelete.Value = hasPrivilegeToDelete;
                    webUser.HasPrivilegeDownload.Value = hasPrivilegeToDownload;
                    webUser.ViewPrivilege.Value = viewPrivilege;
                    webUser.HasProtectedSecurity.Value = hasPrivilegeForProtected;
                    webUser.HasPublicSecurity.Value = hasPrivilegeForPublic;
                    webUser.Groups.Value = groups;

                    UserManagement.CreatNewAccount(webUser);

                    newUserId = webUser.UID.ToString();
                    //this.UserID = webUser.UID.ToString();

                    if (chkIpAdd.Checked && !string.IsNullOrEmpty(hdnIPAddresses.Value))
                        General_Class.InsertLoginRestriction(newUserId, chkIpAdd.Checked, hdnIPAddresses.Value);
                    //General_Class.InsertLoginRestriction(this.UserID, chkIpAdd.Checked, hdnIPAddresses.Value);
                    if (chkTimeSpan.Checked)
                    {
                        if (!string.IsNullOrEmpty(hdnTimeRest.Value))
                        {
                            DataTable dt = FileFolderManagement.PrepareLoginRestStruct(hdnTimeRest.Value, newUserId, chkTimeSpan.Checked);
                            General_Class.InsertTimeLoginRest(dt, newUserId);
                            //DataTable dt = FileFolderManagement.PrepareLoginRestStruct(hdnTimeRest.Value, this.UserID, chkTimeSpan.Checked);
                            //General_Class.InsertTimeLoginRest(dt, this.UserID);
                        }
                        if (!string.IsNullOrEmpty(hdnPublicHolidays.Value))
                            General_Class.InsertPublicHolidayRestriction(newUserId, chkTimeSpan.Checked, hdnPublicHolidays.Value);
                        //General_Class.InsertPublicHolidayRestriction(this.UserID, chkTimeSpan.Checked, hdnPublicHolidays.Value);
                    }

                    General_Class.InsertTabAccessRestriction(newUserId, chkRestrictTab.Checked);
                    //General_Class.InsertTabAccessRestriction(this.UserID, chkRestrictTab.Checked);
                    eFileFolderJSONWS.InsertUserLogs(newUserId, Enum_Tatva.UserLogs.Create.GetHashCode());
                    string strWrongInfor = String.Format("Subuser {0} has been created successfully.", this.textfield3.Text);
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert('" + strWrongInfor + "');", true);
                    // lblMessage.Text = String.Format("create the {0} office successfully.", this.textfield3.Text);
                    #endregion
                }
                catch
                {
                    Membership.DeleteUser(user.UserName);
                    string strWrongInfor = String.Format("Error : failed to create the {0} office, please try again.", this.textfield3.Text);
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                    //lblMessage.Text = String.Format("Error : failed to create the {0} office, please try again.", this.textfield3.Text);
                    return;
                }

                try
                {
                    VerificationNumber vn = new VerificationNumber(newUserId);
                    //VerificationNumber vn = new VerificationNumber(this.UserID);
                    vn.RandomNumber.Value = strRandom;
                    vn.UserName.Value = userName;
                    vn.InDate.Value = DateTime.Now;
                    UserManagement.CreatVerificationNumber(vn);

                    string strWrongInfor = String.Format("Message: A sub user name \"{0}\" has successfully been created.", this.textfield3.Text);
                    ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                    SetTextBoxNull();
                    //lblMessage.Text = String.Format("Message: create {0} user successfully.", this.textfield3.Text);
                }
                catch
                { }


            }
            catch
            {
                string strWrongInfor = String.Format("Error : failed to create the {0} office, please try again.", this.textfield3.Text);
                ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
                //lblMessage.Text = String.Format("Error : failed to create the {0} office, please try again.", this.textfield3.Text);
                return;
            }
        }
        else
        {
            string strWrongInfor = String.Format("Error : {0} office alreadt exits, please input again.", this.textfield3.Text);
            ScriptManager.RegisterClientScriptBlock(UpdatePanel1, typeof(UpdatePanel), "alertok", "alert(\"" + strWrongInfor + "\");", true);
            //lblMessage.Text = String.Format("Error : {0} office alreadt exits, please input again.", this.textfield3.Text);
            return;
        }
    }

    public string UserID
    {
        get
        {
            return this.Session["UserID"] as string;
        }
        set
        {
            this.Session["UserID"] = value;
        }
    }

    private void SetTextBoxNull()
    {

        this.textfield3.Text = "";
        this.textfield4.Text = "";
        // this.textfield5.Text = "";

        this.textfield13.Text = "";
        this.textfield6.Text = "";
        this.textfield7.Text = "";

        this.textfield8.Text = "";
        this.textfield11.Text = "";
        //this.textfield9.Text = "";
        //this.textfield14.Text = "";
        // this.textfield10.Text = "";

        // this.drpCountry.SelectedIndex = 1;
        // this.drpState.SelectedIndex = 1;
        // this.txtOtherState.Text = "";
        // this.txtCity.Text = "";
        // this.txtPostal.Text = "";
    }

    [WebMethod]
    public static string GetPublicHolidays()
    {
        try
        {
            DataTable dt = General_Class.GetPublicHolidays();
            return JsonConvert.SerializeObject(dt);
        }
        catch (Exception ex)
        {
            return "";
        }
    }

}
