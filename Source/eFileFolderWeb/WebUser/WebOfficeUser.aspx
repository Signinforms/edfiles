<%@ Page Language="C#" MasterPageFile="~/LoginMasterPage.master" AutoEventWireup="true"
    CodeFile="WebOfficeUser.aspx.cs" Inherits="WebOfficeUser" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .inputBox {
            font-size:initial;color:initial;font-size:14px;
        }
        .ajax__validatorcallout_error_message_cell {
            line-height:normal;
            vertical-align:top
        }
        select {
            font-size:14px;
            color:black;
        }
        .calendar1 {
            line-height:normal;
            border-bottom-style:solid;
            border-bottom-width:1px;
            border-bottom-color:rgb(0,0,0);
            border-top-style:solid;
            border-top-width:1px;
            border-top-color:rgb(0,0,0);
            border-left-style:solid;
            border-left-width:1px;
            border-left-color:rgb(0,0,0);
            border-right-style:solid;
            border-right-width:1px;
            border-right-color:rgb(0,0,0);
            background-color:#ffffff;

        }

        .ajax__calendar_today .ajax__calendar_day {
            border: 1px solid #ffffff;
            border-color: #0066cc;
        }
    </style>
    <div class="title-container">
        <div class="inner-wrapper">
            <h1>Register your Office</h1>
            <p>In order to start using or trying EdFiles, please complete form below. Select a username, password and the DOB for safeguarding your EdFiles from unauthorized access. All of our new user receive 5 FREE EdFiles to Try and Experience EdFiles. If you require additional assitance, please call 800-579-9190 FREE 24 hours a day, 7 Days a Week.</p>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="page-container register-container">
        <div class="inner-wrapper">
            <div class="register-ads">
                <div class="trial-block" style="line-height:normal">
                    <div class="title">FREE TRIAL</div>
                    <p>The best way to experience EdFiles is by trying them. Register and receive 5 Free EdFiles of FREE Access. Once you have tried EdFiles and would like to convert your paper file folders in EdFiles, just purchase a pack. See the Pricing Tab for details.</p>
                </div>
                <div class="calling-block">
                    <span>Register, Create and View EdFiles...FREE. Call</span>
                    <span class="call-number">1-657-217-3260</span>
                    <span>FREE to further assistance.</span>
                </div>
            </div>
            <div class="form-containt form-half-containt">
                <div class="content-title">Login Details</div>
                <div class="content-box">
                    <fieldset>
                        <label>User Name</label>
                        <asp:TextBox name="textfield3" runat="server" ID="textfield3" size="30" MaxLength="20" ControlToValidate="textfield3" CssClass="inputBox"/>
                        <asp:RequiredFieldValidator runat="server" ID="userNameV" ControlToValidate="textfield3"
                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The UserName is required!" />
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="uesrNameValidate" TargetControlID="userNameV"
                            HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                    </fieldset>
                    <fieldset>
                        <label>Password</label>
                        <asp:TextBox TextMode="password" name="textfield4" runat="server" ID="textfield4" size="30" MaxLength="20" CssClass="inputBox"/>
                        <asp:RequiredFieldValidator runat="server" ID="passwordV" ControlToValidate="textfield4"
                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The password is required!" />
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender3"
                            TargetControlID="passwordV" HighlightCssClass="validatorCalloutHighlight"  PopupPosition="TopLeft"/>
                        <asp:RegularExpressionValidator ID="passwordN" runat="server" ControlToValidate="textfield4"
                            ErrorMessage="<b>Required Field Missing</b><br />The password must be more than 6 numbers!"
                            ValidationExpression="^\w{7,20}$" Display="None"></asp:RegularExpressionValidator>
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender4"
                            TargetControlID="passwordN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                    </fieldset>
                    <fieldset>
                        <label>DOB</label>
                        <asp:TextBox name="textfield5" runat="server" ID="textfield5" size="30" CssClass="inputBox"/>
                        <ajaxToolkit:CalendarExtender ID="CalendarExtender2" PopupPosition="BottomLeft" runat="server" 
                            TargetControlID="textfield5" Format="MM-dd-yyyy" CssClass="calendar1">
                        </ajaxToolkit:CalendarExtender>
                        <asp:RequiredFieldValidator runat="server" ID="DobV" ControlToValidate="textfield5"
                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The DOB is required!" />
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender5"
                            TargetControlID="DobV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                        <asp:RegularExpressionValidator ID="DobC" runat="server" ErrorMessage="<b>Required Field Missing</b><br />The DOB is not a date"
                            ControlToValidate="textfield5" ValidationExpression="(((((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))-([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3}))|(02-29-(([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00)))"
                            Visible="true" Display="None"></asp:RegularExpressionValidator>
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender6"
                            TargetControlID="DobC" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                    </fieldset>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="form-containt">
                <div class="content-title">Personal Details</div>
                <div class="content-box">
                    <fieldset>
                        <label>First Name</label>
                        <asp:TextBox ID="textfield6" name="textfield6" runat="server" size="30" MaxLength="20" CssClass="inputBox"/>
                        <asp:RequiredFieldValidator runat="server" ID="firstNameV" ControlToValidate="textfield6"
                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The first name is required!" />
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender2"
                            TargetControlID="firstNameV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                    </fieldset>
                    <fieldset>
                        <label>Last Name</label>
                        <asp:TextBox ID="textfield7" runat="server" size="30" MaxLength="20" CssClass="inputBox"/>
                        <asp:RequiredFieldValidator runat="server" ID="lastNameV" ControlToValidate="textfield7"
                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The last name is required!" />
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender10"
                            TargetControlID="lastNameV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                    </fieldset>
                    <fieldset>
                        <label>Company Name</label>
                        <asp:TextBox name="textfield14" runat="server" ID="textfield14" size="40" MaxLength="50" CssClass="inputBox"/>
                        <asp:RequiredFieldValidator runat="server" ID="companyNameN" ControlToValidate="textfield14"
                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The company name is required!" />
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender14"
                            TargetControlID="companyNameN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                    </fieldset>

                </div>
            </div>
            <div class="form-containt">
                <div class="content-title">Contact Details</div>
                <div class="content-box half-fieldset">
                    <fieldset>
                        <label>Email</label>
                        <asp:TextBox ID="textfield13" runat="server" size="30" MaxLength="50" CssClass="inputBox"/>
                        <asp:RequiredFieldValidator runat="server" ID="EmailReq" ControlToValidate="textfield13"
                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />An email is required." />
                        <asp:RegularExpressionValidator ID="rev_Email" runat="server" ErrorMessage="<b>Required Field Missing</b><br />It is not an email format."
                            ControlToValidate="textfield13" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                            Visible="true" Display="None"></asp:RegularExpressionValidator>
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="EmailReqE" TargetControlID="EmailReq"
                            HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender1"
                            TargetControlID="rev_Email" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                    </fieldset>
                    <fieldset>
                        <label>Telephone</label>
                        <asp:TextBox ID="textfield8" name="textfield8" runat="server"
                            size="30" MaxLength="23" CssClass="inputBox"/>
                        <asp:RequiredFieldValidator runat="server" ID="telN" ControlToValidate="textfield8"
                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The telephone is required!" />
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender11"
                            TargetControlID="telN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                        <asp:RegularExpressionValidator ID="TelV" ControlToValidate="textfield8" runat="server"
                            ErrorMessage="<b>Required Field Missing</b><br />It is not a telphone format."
                            ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender7"
                            TargetControlID="TelV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                    </fieldset>
                    <fieldset>
                        <label>Mobile</label>
                        <asp:TextBox name="textfield11" runat="server" ID="textfield11"
                            size="30" MaxLength="20" CssClass="inputBox"/>
                        <asp:RegularExpressionValidator ID="mobileV" ControlToValidate="textfield11" runat="server"
                            ErrorMessage="<b>Required Field Missing</b><br />It is not a Mobile format."
                            ValidationExpression="[^A-Za-z]{7,}" Display="None"></asp:RegularExpressionValidator>
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender9"
                            TargetControlID="mobileV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                    </fieldset>

                    <fieldset>
                        <label>Fax</label>
                        <asp:TextBox name="textfield9" type="text" runat="server"
                            ID="textfield9" size="30" MaxLength="23" CssClass="inputBox"/>
                        <asp:RegularExpressionValidator ID="faxV" ControlToValidate="textfield8" runat="server"
                            ErrorMessage="<b>Required Field Missing</b><br />It is not a FAX format." ValidationExpression="[^A-Za-z]{7,}"
                            Display="None"></asp:RegularExpressionValidator>
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender8"
                            TargetControlID="faxV" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                    </fieldset>
                    <fieldset class="full-fieldset">
                        <label>Address</label>
                        <asp:TextBox name="textfield10" runat="server" ID="textfield10"
                            size="40" MaxLength="200" CssClass="inputBox"/>
                        <asp:RequiredFieldValidator runat="server" ID="addressN" ControlToValidate="textfield10"
                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The address is required!" />
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender15"
                            TargetControlID="addressN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                    </fieldset>
                    <fieldset>
                        <label>City</label>
                        <asp:TextBox runat="server" ID="txtCity" size="30" MaxLength="30" CssClass="inputBox"/>
                    </fieldset>
                    <fieldset>
                        <label>State</label>
                        <asp:DropDownList ID="drpState" runat="server" />
                    </fieldset>
                    <fieldset class="last">
                        <label>Other</label>
                        <asp:TextBox runat="server" ID="txtOtherState" size="30" MaxLength="30" CssClass="inputBox"/>
                    </fieldset>
                    <fieldset>
                        <label>Country</label>
                        <asp:DropDownList ID="drpCountry" runat="server" />
                    </fieldset>

                    <fieldset>
                        <label>Zip/Postal</label>
                        <asp:TextBox ID="txtPostal" runat="server" size="30" MaxLength="30" CssClass="inputBox"/>
                        <asp:RequiredFieldValidator runat="server" ID="txtPostalN" ControlToValidate="txtPostal"
                            Display="None" ErrorMessage="<b>Required Field Missing</b><br />The postal is required!" />
                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="ValidatorCalloutExtender17"
                            TargetControlID="txtPostalN" HighlightCssClass="validatorCalloutHighlight" PopupPosition="TopLeft"/>
                    </fieldset>

                </div>
            </div>
            <div class="podnaslov">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblMessage" CssClass="label_message" runat="server" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ImageButton1" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div class="form-submit-btn">
                <asp:Button  runat="server" ID="ImageButton1" OnClick="ImageButton1_Click" Text="Create"  CssClass="create-btn btn green" />
            </div>
        </div>
    </div>
</asp:Content>
