using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Serialization;
using Shinetech.Framework;

public partial class XmlWebFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string effid = Request.QueryString["id"];

        XmlEFFolder objEFF = new XmlEFFolder();
        objEFF.head = new Head();
        objEFF.folders[0] = new Folder(Convert.ToInt32(effid));

        try
        {
            XmlSerializer xmls = new XmlSerializer(typeof(XmlEFFolder));
            MemoryStream ms = new MemoryStream();
            xmls.Serialize(ms, objEFF);

            Response.Clear();
            //Response.Charset = "utf-8";
            //Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.ContentType = "application/octet-stream ";
            //Response.Buffer = false;
            Response.AppendHeader("Content-disposition ", "attachment;filename= " + effid + ".eff");
            Response.Write("fsdfsdfsdfsdf");

            Response.End();
            ms.Dispose();
        }
        catch (Exception exp)
        {
            Console.Write(exp.Message);
        }
    }
}
