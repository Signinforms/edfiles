﻿<%@ Page Language="C#" MasterPageFile="~/LoginMasterPage.master" AutoEventWireup="true" CodeFile="AboutUs.aspx.cs" Inherits="edFilesDocs_AboutUs" %>

<%@ Register Src="~/Controls/RegisterDirectly.ascx" TagName="ucRegisterDirectly" TagPrefix="ucRegisterDirectly" %>
<asp:content id="Content1" contentplaceholderid="ContentPlaceHolder1" runat="Server">
	<div class="wrapper" style="padding-top:0px;">
		<!-- Start Banner -->
		<section class="inner-banner text-center about-banner" style="line-height:18px;height:205px;">
			<div class="container" style="height:125px;">
				<h2 class="h2_new">About Us</h2>
				<h4 class="h4_new">Our belief...The reason for our existence is:<span>To Store, Organize and Manage Information...Not Paper!</span></h4>				
			</div>
		</section>
		<!-- End Banner -->

		<!-- Start Main Content -->
		<main class="content-block cms-content" style="font-size:14px;line-height:18px;">
			<div class="container">
				<h4>We firmly believe that, all the valuable and necessary information, which currently resides in paper form, i.e., file storage boxes, file cabinets and records storage facilities, needs to be:</h4>
				<ul class="default-listing">
					<li>Conveniently and securely accessible electronically from anywhere.</li>
					<li>This Information needs to be easily searchable.</li>
					<li>It must be Preserved and Backed up (If it is worth storing, it needs to be preserved. Otherwise, why store it?)</li>
					<li>Needs to be formatted for Analysis, Reporting and Integration.</li>
					<li>Freely retrievable without ANY retrieval and delivery costs &amp; hassles.</li>
					<li>And Most of all...It must be available to all type and size budgets.</li>
				</ul>

				<h4>How do we help organizations?</h4>
				<ul class="default-listing">
					<li>By simplifying the process of converting large amounts of paper based files to useful electronic data.</li>
					<li>By eliminating the fear of critical business disruptions and loss of valuable information, utilizing 100s of contingencies and case study findings.</li>
					<li>With our exclusive, 3D file folder interface for scanned paper file folders and by adhering to a 100% commitment to quality scanning service!</li>
				</ul>

				<h4>What is it that we do?</h4>
				<ul class="default-listing">
					<li>Assist in converting, current &amp; historical paper files to digital/electronic format.</li>
					<li>Provide insight in the gradual implementation of efficient and compliant, reduced paper dependent, filing systems.</li>
					<li>Most of all, we help you store, organize and manage information...not paper.</li>
				</ul>

				<div class="bottom-banner">
					<span>Why are you still storing paper?</span>
					<img src="../images/bottom-banner-icon.png" alt="" class="icon">
					<ul class="contact-info">
						<li><a href="#" title="1-657-217-3260"><img src="../images/call-icon.png" alt=""> 1-657-217-3260</a></li>
						<li><a href="#" title="www.edfiles.com"><img src="../images/web-icon.png" alt=""> www.edfiles.com</a></li>
					</ul>
				</div>
			</div>
		</main>
		<!-- End Main Content -->
	</div>

    	<%--<a href="tel:16572173260" class="call-link" style="font-size:18px;"><span class="icon"><img src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/images/edFiles/call-icon-white.png" alt=""></span> For further assistance, Call <span class="phone-no" style="font-size:24px;">1-657-217-3260</span></a>--%>

	</asp:content>
