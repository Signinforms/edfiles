﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="edFilesDocs_ContactUs" %>

<%@ Register TagPrefix="fjx" Namespace="com.flajaxian" Assembly="com.flajaxian.FileUploader" %>
<%@ Register TagPrefix="cust" Namespace="Shinetech.Engines.Adapters" Assembly="FormatEngine" %>
<%@ Import Namespace="System.Web.Configuration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
   <%-- <link type="text/css" rel="stylesheet" href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/css/jquery-turnjs.ui.css" />
    <link type="text/css" rel="stylesheet" href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString() %>Flip/css/jquery-ui-1.8.22.custom.css" />
    <link rel="stylesheet" type="text/css" href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/edFiles/style_edFiles.css" />
    <link href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/bootstrap.min.css" />--%>
    
    <% if (this.Page.User.Identity.IsAuthenticated)
      {%>
           <link rel="stylesheet" type="text/css" href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/edFiles/style_edFiles.css" />
       <%} %>
    
    <style>
        .links-content {
            display:block !important;
        }

        .navbar-fixed-top {
            top:auto;
            border-width:0px;
        }

        .navbar-fixed-bottom, .navbar-fixed-top {
            position:relative;
            right:auto;
            left:auto;
            z-index:99999;
            margin-bottom:0px;
        }
        .link-head {
            height:47px !important;
        }

        .contact_security {
            padding-top:0px;
            min-height:0;
        }

    </style>
    
	<div class="wrapper" style="padding-top:0px;min-height:0">
		<!-- Start Banner -->
		<section class="inner-banner text-center about-banner">
			<div class="container">
				<h2 style="height:62px; font-size:35px;">Contact Us</h2>
				<h4 style="font-size:28px;line-height:34px;"><span>We are always ready to help you!</span></h4>				
			</div>
		</section>
		<!-- End Banner -->

		<!-- Start Main Content -->
		<main class="content-block cms-content text-center" style="font-size:14px;line-height:18px;">
			<div class="container">
				<p class="call-detail">please Call <%--<a href="tel:16572173260" title="1-657-217-3260" class="call-link-mobile">1-657-217-3260</a> or--%> <a href="tel:16572173260" title="1-657-217-3260" class="call-link-mobile">1-657-217-3260</a>.</p>	
				<address>
					<span>EdFiles, Inc.</span>
					<span>261 E. Imperial Hwy.  #550</span>
					<span>Fullerton,  CA. 92835</span>
					<span><a href="mailto:info@edfiles.com" title="info@edfiles.com">info@edfiles.com</a></span>
				</address>				
			</div>
		</main>
		<!-- End Main Content -->
	</div>

    <a href="tel:16572173260" class="call-link" style="font-size:18px;"><span class="icon"><img src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/images/edFiles/call-icon-white.png" alt=""></span> For further assistance, Call <span class="phone-no" style="font-size:24px;">1-657-217-3260</span></a>

    </asp:Content>
