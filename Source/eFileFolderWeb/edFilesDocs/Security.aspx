﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Security.aspx.cs" Inherits="edFilesDocs_Security" %>

<%@ Register Src="~/Controls/RegisterDirectly.ascx" TagName="ucRegisterDirectly" TagPrefix="ucRegisterDirectly" %>
<%@ Import Namespace="System.Web.Configuration" %>

<asp:content id="Content1" contentplaceholderid="ContentPlaceHolder1" runat="Server">
    <%--<link rel="stylesheet" type="text/css" href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/edFiles/edFiles_reset.css" />--%>
    <%--<link rel="stylesheet" type="text/css" href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/edFiles/edFiles_font-awesome.css" />--%>
    <%--<link rel="stylesheet" type="text/css" href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/bootstrap.min.css" />--%>

    <%--<link rel="stylesheet" type="text/css" href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/media.css" />--%>
    <%--<link rel="stylesheet" type="text/css" href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/edFiles/style_edFiles.css" />--%>

    <% if (this.Page.User.Identity.IsAuthenticated)
      {%>
           <link rel="stylesheet" type="text/css" href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/css/edFiles/style_edFiles.css" />
       <%} %>

    <style>
        .links-content {
            display:block !important;
        }

        .navbar-fixed-top {
            top:auto;
            border-width:0px;
        }

        .navbar-fixed-bottom, .navbar-fixed-top {
            position:relative;
            right:auto;
            left:auto;
            z-index:99999;
            margin-bottom:0px;
        }
        .link-head {
            height:47px !important;
        }

        .contact_security {
            padding-top:0px;
        }


    </style>

	<div class="wrapper" style="padding-top:0px;">
		
		<!-- Start Banner -->
		<section class="inner-banner text-center about-banner" >
			<div class="container">
				<h2 style="height:62px; font-size:35px;">Security</h2>
				<h4 style="font-size:28px;line-height:34px;"><span>At EdFiles, Security is Priority One. On all Levels!</span></h4>				
			</div>
		</section>
		<!-- End Banner -->

		<!-- Start Main Content -->
		<main class="content-block cms-content" style="font-size:14px;line-height:18px;">
			<div class="container">
				<p>Our Data Center covers all three critical security areas: <b>physical security, operational security,</b> and <b>system security</b>. Physical security includes locking down and logging all physical access to servers at our data center. Operational security involves creating business processes that follow security best practices to limit access to confidential information and maintain tight security over time. System security involves locking down customer systems from the inside, starting with hardened operating systems and up-to-date patching.</p>
				<h4>Operational Security</h4>
				<ul class="default-listing">
					<li>ISO17799-based policies and procedures, regularly reviewed as part of our SAS70 Type II audit process</li>
					<li>All employees trained on documented information security and privacy procedures</li>
					<li>Access to confidential information restricted to authorized personnel only, according to documented processes</li>
					<li>Systems access logged and tracked for auditing purposes</li>
					<li>Secure document-destruction policies for all sensitive information</li>
					<li>Fully documented change-management procedures</li>
					<li>Independently audited disaster recovery and business continuity plans in place and support services</li>
				</ul>

				<h4>Physical Security</h4>
				<div class="security-icon-box">
					<img src="../images/security-icon.png" alt="" class="security-icon">
					<ul class="default-listing">
						<li>Data center access limited to Authorized data center technicians</li>
						<li>Biometricscanning for controlled data center access</li>
						<li>Security camera monitoring at alldata center locations</li>
						<li>24x7 onsite staff provides additional protection against unauthorized entry</li>
						<li>Unmarked facilities to help maintain low profile</li>
						<li>Physical security audited by an independent firm</li>
					</ul>					
				</div>

				<h4>System Security</h4>
				<ul class="default-listing">
					<li>System installation using hardened, patched OS</li>
					<li>System patching configured by Rackspace to provide ongoing protection from exploits</li>
					<li>Dedicated firewall and VPN services to help block unauthorized system access</li>
					<li>Data protection with managed backup solutions</li>
					<li>Dedicated intrusion detection devices to provide an additional layer of protection against unauthorized system access</li>
					<li>Distributed Denial of Service (DDoS) mitigation services based on proprietary Rackspace PrevenTier™ system</li>
				</ul>

				<h5>User Security</h5>
				<p><span>You control your files.</span> Documents stored in EdFiles are private by default. They are only accessible to others if you choose to share them or make them public.</p>

				<h5>99.9% uptime guarantee</h5>
				<p>We give a 99.9% uptime guarantee to make sure that you always have access to your files. </p>

				<h5>Detailed Security Compliance For:</h5>
				<ul class="inline-listing clearfix" style="line-height:18px; font-size:14px;">
					<li>HIPAA</li>
					<li>SOX</li>
					<li>GLBA</li>
					<li>FISMA</li>
					<li>SAS-70</li>
				</ul>

			</div>
		</main>
		<!-- End Main Content -->
	</div>
    <a href="tel:16572173260" class="call-link" style="font-size:18px;"><span class="icon"><img src="<%= WebConfigurationManager.AppSettings["VirtualDir"].ToString()%>New/images/edFiles/call-icon-white.png" alt=""></span> For further assistance, Call <span class="phone-no" style="font-size:24px;">1-657-217-3260</span></a>
    </asp:content>
