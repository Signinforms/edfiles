/* Documentation sample */

(function($) {

var sampleName = 'magazine3',
	samplePath = 'samples/magazine3/';

// Why this?  Chrome has the fault:
// http://code.google.com/p/chromium/issues/detail?id=128488
function isChrome() {

	return navigator.userAgent.indexOf('Chrome')!=-1;

}

function loadPage(page) {

	var img = $('<img />');
	img.load(function() {
		var container = $('.sample-mag3 .p'+page);
		img.css({width: container.width(), height: container.height()});
		img.appendTo($('.sample-mag3 .p'+page));
		container.find('.loader').remove();
	});

	img.attr('src', samplePath + 'pages/' +  page + '.jpg');

}

function addPage(page, book) {

	var id, pages = book.turn('pages');

	var element = $('<div />', {});

	if (book.turn('addPage', element, page)) {
		element.html('<div class="gradient"></div><div class="loader"></div>');
		loadPage(page);
	}
}

bookshelf.loadSample(sampleName, function(action) {

	var sample = this;

	bookshelf.preloadImgs(['1.jpg'], samplePath + 'pages/',
	function() {

	bookshelf.loaded(sampleName);

	if (action=='preload') {
		return;
	}

	sample.previewWidth = 115;
	sample.previewHeight = 73;
	sample.previewSrc = samplePath + 'pics/preview.jpg';
	sample.tableContents = 5;
	sample.shareLink = 'http://' + location.host + '/#'+samplePath;
	sample.shareText = 'Turn.js: Make a flipbook with HTML5 via @turnjs';


	// Report that the flipbook is loaded

	if (!sample.flipbook) {

		var bookClass = (Modernizr.csstransforms) ?
			'mag1-transform sample-mag3' :
			'sample-mag3';

		sample.flipbook = $('<div />', {'class': bookClass}).
			html('<div ignore="1" class="next-button"></div> <div ignore="1" class="previous-button"></div>').
			appendTo($('#book-zoom'));
		
	
		sample.flipbook.find('.next-button').mouseover(function() {
			$(this).addClass('next-button-hover');
		}).mouseout(function() {
			$(this).removeClass('next-button-hover');
		}).mousedown(function() {
			$(this).addClass('next-button-down');
			return false;
		}).mouseup(function() {
			$(this).removeClass('next-button-down');
		}).click(function() {
			sample.flipbook.turn('next');
		});
	
		sample.flipbook.find('.previous-button').mouseover(function() {
			$(this).addClass('previous-button-hover');
		}).mouseout(function() {
			$(this).removeClass('previous-button-hover');
		}).mousedown(function() {
			$(this).addClass('previous-button-down');
			return false;
		}).mouseup(function() {
			$(this).removeClass('previous-button-down');
		}).click(function() {
			sample.flipbook.turn('previous');
		});
	
		setTimeout(function() {
			sample.flipbook.turn({
				elevation: 50,
				acceleration: !isChrome(),
				gradients: true,
				autoCenter: true,
				duration: 1000,
				pages: 12,
				when: {

				turning: function(e, page, view) {
					
					var book = $(this),
					currentPage = book.turn('page'),
					pages = book.turn('pages');
					
					if (!$('.splash .bookshelf').is(':visible'))
						Hash.go('samples/' + sampleName+'/'+page).update();

					
					if (page==1)
						$('.previous-button').hide();
					else
						$('.previous-button').show();
						
					
					if (page==pages)
						$('.next-button').hide();
					else
						$('.next-button').show();

				},

				turned: function(e, page, view) {

					var book = $(this);

					$('#slider').slider('value', getViewNumber(book, page));

					if (page!=1 && page!=book.turn('pages'))
						$('.sample-mag3 .tabs').fadeIn(500);

					book.turn('center');

				},

				start: function(e, pageObj) {
			
					bookshelf.moveBar(true);

				},

				end: function(e, pageObj) {
				
					var book = $(this);

					setTimeout(function() {
						$('#slider').slider('value', getViewNumber(book));
					}, 1);

					bookshelf.moveBar(false);

				},

				missing: function (e, pages) {

					for (var i = 0; i < pages.length; i++)
						addPage(pages[i], $(this));

				}

			}
		});

		$('#slider').slider('option', 'max', numberOfViews(sample.flipbook));

		sample.flipbook.addClass('animated');
		bookshelf.showSample();

	}, 0);

	} else {
			
		bookshelf.showSample();

	}

	});

});

})(jQuery);