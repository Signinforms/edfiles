/**
* Copyright (c) 2012 Emmanuel Garcia
* All rights reserved
*
* turnjs.com
* turnjs.com/license.txt
**/
var $dragPage = null;
var bookshelfEdit = {
    allowEdit: true
};
(function ($) {
    var $jsonmotheds = new JSonService();
    $jsonmotheds.initialize();
    var currentFolderName,
        previewNumPages,
        zoomOutButton,
        scrollTimer,
        scrollPage,
        scrollX = null,
        demoHash = '',
        scrolling = false,
        slide_int,
        params = {
            id: null,
            name: null,
            start: 1,
            uid: null,
            pwd: null,
            sid: null,
            isEdit: false,
            isAccessDenied: false,
            status: $status.loaded
        },
        loadingcircle = null,
        loadingcircleMoveFile = null,
        book_content;

    bookshelf = {
        login: function (id, sid, isEdit, isAccessDenied) {
            $jsonmotheds.getFolderXML(id, sid, isEdit, isAccessDenied, this.loginSuccess, this.loginFailed);
            return true;
        },

        loginSuccess: function (data) {
            bookshelf.loaded.apply();
            if (data.d !== null && data.d.ErrorCode === 0) {
                var args = new Array();
                args.push(data.d);
                bookshelf.init.apply(this, args);
            } else if (data.d.ErrorCode === -9) {
                $('#dialog-enter-pin').dialog('open');
            }
            else if (data.d.ErrorCode === -3) {
                $('#dialog-message-content').text("This is a private folder, it cannot be shared.  If you want to share this folder, change the folder to public.");
                $('#dialog-message-all').dialog('option', 'title', " Error");

                $('#dialog-message-all').dialog("open");
            }
            else if (data.d.ErrorCode === -2) {
                $('#dialog-message-content').text("The EdFiles you are trying to view is no longer accessible.");
                $('#dialog-message-all').dialog('option', 'title', " Error");

                $('#dialog-message-all').dialog("open");
            }
            else if (data.d.ErrorCode === -1) {
                $('#dialog-message-content').text("The folder doesn��t exist.");
                $('#dialog-message-all').dialog('option', 'title', " Error");

                $('#dialog-message-all').dialog("open");
            }
            else {
                $('#dialog-message-content').text("The EdFiles you are trying to view is no longer accessible.");
                $('#dialog-message-all').dialog('option', 'title', " Error");

                $('#dialog-message-all').dialog("open");
            }
            $("#userLogo").css("background", "rgba(0, 0, 0, 0) url('../Upload/UserLogo/" + data.d.UID + ".png') no-repeat scroll center center");
            bookshelfEdit.allowEdit = data.d.AllowEdit;
            bookshelf.getSesionPrivilege();
            return true;
        },

        verifySecurity: function (sender, pwd) {
            //console.log(data);
            bookshelf.loading.apply();
            var success = function (data) {
                bookshelf.loaded.apply();

                var result = $.parseJSON(data.d);
                if (result.ErrorCode.value !== pwd) {
                    $('#shelf-pin').val('');
                    $('#dialog-enter-pin').dialog('open');
                } else {
                    bookshelf.loading.apply();
                    $jsonmotheds.getFolderXMLSecurity(params.id, pwd, bookshelf.loginSuccess, bookshelf.loginFailed);
                }
            };

            var failed = function (data) {
                console.log(data);
                bookshelf.loaded.apply();
                $('#dialog-message-content').text(data.responseText);
                $('#dialog-message-all').dialog('option', 'title', " Error");

                $('#dialog-message-all').dialog("open");

            };

            $jsonmotheds.verifySecurity(params.id, pwd, success, failed);
        },

        loginFailed: function (data) {
            console.log(data);
            bookshelf.unloaded.apply();
            $('#dialog-message-content').text("Failed to get folder details:" + data.responseText);
            $('#dialog-message-all').dialog('option', 'title', "Warning");

            $('#dialog-message-all').dialog("open");
        },

        //��ʼ��folderĿ¼��dividers
        init: function (d) {
            book_content = d;
            bookshelf.loading.apply();
            //loading book
            bookshelf.setTitle(book_content.Name);
            bookshelf.initshelf.apply();
            initdividers(book_content);

            bookshelf.loaded.apply();
        },

        setTitle: function viewSetTitle(title) {
            //document.title = title;           
        },

        //load divider content by id and sid
        initdivider: function (id) {
            //alert(99);
            $jsonmotheds.getTabXML(id, params.sid, this.loaddividerSuccess, this.loaddivierFailed);
            return true;
        },

        loaddividerSuccess: function (data) {
            if (data.d !== null && data.d.ErrorCode === 0) {
                if (!bookshelfEdit.allowEdit) {
                    $('.hideFooter').hide();
                    $("#toc").sortable('disable');
                    $("#dividers-front li,#dividers-back li").droppable('disable');
                }
                var items = $dividers.Items;
                items[$current_divider_nth]["cached"] = true;

                //save the divider's data in this items[current_divider_nth]
                items[$current_divider_nth]['fid'] = data.d.FolderId;
                items[$current_divider_nth]['files'] = data.d.FileItemList;
                items[$current_divider_nth]['notes'] = data.d.NewNoteList;
                items[$current_divider_nth]['qotes'] = data.d.QuiteNoteList;
                items[$current_divider_nth]['meta'] = data.d.MetaXML;
                items[$current_divider_nth]['total'] = data.d.TotalPages;
                trigger("initedtoc", $current_divider, items[$current_divider_nth]);
                bookshelf.loaded.apply();
            }
        },

        loaddivierFailed: function (data) {
            bookshelf.unloaded.apply();
            //todo
            setTimeout(function () {
                $('#dialog-message-all').dialog('option', 'title', "Loading Tab Error");
                $('#dialog-message-content').text("Failed to get tab detials:" + data.statusText);


                $('#dialog-message-all').dialog("open");
            }, 500);

            var items = $dividers.Items;
            items[$current_divider_nth]["cached"] = false;

            //save the divider's data in this items[current_divider_nth]
            items[$current_divider_nth]['fid'] = null;
            items[$current_divider_nth]['files'] = [];
            items[$current_divider_nth]['notes'] = [];
            items[$current_divider_nth]['qotes'] = [];
            items[$current_divider_nth]['meta'] = '';
            items[$current_divider_nth]['total'] = 1;
            trigger("initedtoc", $current_divider, items[$current_divider_nth]);

        },

        loaded: function () {
            $current_status = $status.loaded;
            setTimeout(function () { $('.flash').hide(); }, 500);
        },

        unloaded: function () {
            $current_status = $status.unloaded;
            setTimeout(function () { $('.flash').hide(); }, 500);
        },

        loading: function () {
            $current_status = $status.loading;
            if (loadingcircle === null) {
                loadingcircle = $('<div id="floatingCirclesG">\
						<div class="f_circleG" id="frotateG_01"> </div>\
						<div class="f_circleG" id="frotateG_02"> </div>\
						<div class="f_circleG" id="frotateG_03"> </div>\
						<div class="f_circleG" id="frotateG_04"> </div>\
						<div class="f_circleG" id="frotateG_05"> </div>\
						<div class="f_circleG" id="frotateG_06"> </div>\
						<div class="f_circleG" id="frotateG_07"> </div>\
						<div class="f_circleG" id="frotateG_08"> </div>\
					  </div>');
            }

            if ($('.flash').has('#floatingCirclesG').length === 0) {
                $('.flash').append(loadingcircle);
            }

            $('.flash').show();

        },

        loadedMoveFile: function () {
            $current_status_moveFile = $statusMoveFile.loadedMoveFile;
            setTimeout(function () { $('.LoadingMoveFile').hide(); $('.flashMoveFile').hide(); }, 500);
        },

        unloadedMoveFile: function () {
            $current_status_moveFile = $statusMoveFile.unloadedMoveFile;
            setTimeout(function () { $('.LoadingMoveFile').hide(); $('.flashMoveFile').hide(); }, 500);
        },

        loadingMoveFile: function () {
            $current_status_moveFile = $statusMoveFile.loadingMoveFile;
            if (loadingcircleMoveFile === null) {
                loadingcircleMoveFile = $('<div id="floatingCirclesGMoveFile">\
      <div class="f_circleG" id="frotateG_01"> </div>\
      <div class="f_circleG" id="frotateG_02"> </div>\
      <div class="f_circleG" id="frotateG_03"> </div>\
      <div class="f_circleG" id="frotateG_04"> </div>\
      <div class="f_circleG" id="frotateG_05"> </div>\
      <div class="f_circleG" id="frotateG_06"> </div>\
      <div class="f_circleG" id="frotateG_07"> </div>\
      <div class="f_circleG" id="frotateG_08"> </div>\
       </div>');
            }

            if ($('.LoadingMoveFile').has('#floatingCirclesGMoveFile').length === 0) {
                $('.LoadingMoveFile').append(loadingcircleMoveFile);
            }

            $('.LoadingMoveFile').show();
            $('.flashMoveFile').show();
        },

        // show the shelf div after folder ready
        initshelf: function () {
            jQXB.setDataSource('booktitle', book_content).doBind('booktitle');
            jQXB.setDataSource('bookalerts', book_content.alertsList).doBind('bookalerts');
        },

        regExp: function (names, callback) {
            var parts = [];
            for (var i = 0; i < names.length; i++) {
                var reg = new RegExp("(^|\\?|&)" + names[i] + "=([^&]*)(\\s|&|$)", "i");
                if (reg.test(location.href)) {
                    parts.push(unescape(RegExp.$2.replace(/\+/g, " ")));
                } else {
                    callback.nop(names);
                    return;
                }

            }

            callback.yep(names, parts);

        },

        // close the folder and back the bookshelf 
        close: function () {


        },

        // after login valid, we open the folder by folder id
        open: function (folderName) {

        },

        preloadImgs: function (pics, path, callback) {

            var loaded = 0,
                load = function (src) {
                    var tmp = $('<img />').
                        bind('load', function () {
                            loaded++;
                            if (loaded == pics.length && typeof (callback) == 'function')
                                callback();
                            tmp = null;
                        }).attr('src', path + src);
                };

            if (pics.length === 0) {
                if (typeof (callback) == 'function')
                    callback();
            } else
                for (var i = 0; i < pics.length; i++)
                    load(pics[i]);

        },

        zoomOutButton: function (show) {

            if (!zoomOutButton) {
                zoomOutButton = $('<i />', { 'class': 'icon zoom-out' }).
                    appendTo($('.splash')).
                    mouseover(function () {
                        zoomOutButton.addClass('zoom-out-hover');
                    }).
                    mouseout(function () {
                        zoomOutButton.removeClass('zoom-out-hover');
                    }).
                    click(function () {
                        $('.splash').zoom('zoomOut');
                        $(this).hide();
                    });
            }

            zoomOutButton.css({ display: (show) ? '' : 'none' });

        },

        deleteNote: function (sender, id) {

            //bookshelf.loading.apply();
            $('.flashMoveFile').css("height", $(window).height());
            bookshelf.loadingMoveFile.apply();

            var success = function (data) {

                var result = data.d;
                if (result === 0) {
                    $('#dialog-message-content').text("Failed to delete this note.");
                    $('#dialog-message-all').dialog('option', 'title', "Delete Note Error");

                    $('#dialog-message-all').dialog("open");
                    return;
                }
                else if (result === -2) {
                    $('#dialog-message-content').text("Failed to check the security code.");
                    $('#dialog-message-all').dialog('option', 'title', "Delete Note Error");

                    $('#dialog-message-all').dialog("open");
                } else {
                    if ($book_contents[id]) {
                        try {

                            var displayPageNum;
                            $dividers.Items[$current_divider_nth].notes = $.grep($dividers.Items[$current_divider_nth].notes, function (e) {
                                if (e.Id && id && parseInt(e.Id) == parseInt(id)) {
                                    displayPageNum = e.start;
                                    return false;
                                }
                                else
                                    return true;
                            });

                            if (displayPageNum > 1)
                                displayPageNum = displayPageNum - 1;
                            else
                                displayPageNum = undefined;

                            BindDividerOnIndexSearch(false, true, 0, false, displayPageNum);

                            bookshelf.loadedMoveFile.apply();
                        }
                        catch (err) {
                            //console.log(err);
                            bookshelf.loadedMoveFile.apply();
                        }
                    }
                }

                bookshelf.loadedMoveFile.apply();
            };

            var failed = function (data) {
                bookshelf.loadedMoveFile.apply();
                sender.dialog("close");

                $('#dialog-message-content').text(data.responseText);
                $('#dialog-message-all').dialog('option', 'title', "Delete Note Error");

                $('#dialog-message-all').dialog("open");
            };

            $jsonmotheds.deleteNote(id, params.sid, success, failed);
        },

        updateNoteth: function (page, pages) {

            $('div[noteref][pageth]').each(function (index, elment) {
                var pageth = $(elment).attr('pageth');
                if (pageth && parseInt(pageth) > page) {
                    var noteref = $(elment).attr('noteref');
                    var note = $book_contents[noteref];
                    if (note) {
                        note.updatePage(parseInt(pageth) - 1);
                    }
                }
            });
        },

        clearNotes: function (id) {

            var items = $dividers.Items;
            var notes = items[$current_divider_nth]['notes'];
            var index = bookshelf.getNote(parseInt(id), notes);
            if (id !== null) {
                notes.remove(notes[index]);
                var item = $items_auto_source[$items_auto_source.length - 1];
                item.count--;
                //$items_auto_source = $.grep($items_auto_source, function (e) {
                //    if (e.value && id && parseInt(e.value) == parseInt(id)) {
                //               return false;
                //           }
                //           else
                //               return true;
                //       });
                if (notes.length > 0) {
                    if (index == 0) {
                        var noteStart = notes[0].start - 2;
                    }
                    else {
                        var noteStart = notes[0].start - 1;
                    }

                    $.each(notes, function (i, item) {
                        item.start = noteStart + 1;
                        noteStart += 1;
                    });


                    $("#toc li").each(function (j, item) {
                        $.each(notes, function (i, e) {
                            if (item.firstChild.innerHTML == e.Title) {
                                item.lastChild.innerHTML = e.start;
                                //item.lastChild.innerHtml = e.start;
                            }
                            //alert(item.innerText + " " + item.label);
                        });

                    });
                }
            }
            if (notes.length === 0) {
                bookshelf.cleartocWithNote();
                var pages = $(".sj-book").turn('pages');
                items[$current_divider_nth]['total'] = pages;
                $('#slider-book').slider('option', 'max', pages);
                //$('.sj-book').turn('previous');

                $total_page_numb = pages;
                delete $book_contents[id];
            }
            else {
                bookshelf.cleartocWithNote();
                var pages = $(".sj-book").turn('pages');
                items[$current_divider_nth]['total'] = pages;
                $('#slider-book').slider('option', 'max', pages);
                $('.sj-book').turn('previous');

                $total_page_numb = pages;
                delete $book_contents[id];
            }
        },

        getNote: function (id, notes) {
            for (var i = 0; i < notes.length; i++) {
                if (notes[i].Id === id) return i;
            }

            return null;
        },

        getContent: function (id, contents) {
            for (var i = 0; i < contents.length; i++) {
                if (contents[i].id === id) return i;
            }

            return null;
        },

        cleartocWithNote: function () {
            var note = null;
            for (var i = 0; i < $items_auto_source.length; i++) {
                if ($items_auto_source[i].isNote) {
                    note = $items_auto_source[i];
                    break;
                }
            }

            if (note) {
                $items_auto_source.remove(note);
            }
        },
        //todo
        updateNote: function (sender, id) {



            var title, author, content;

            if (id === null) {

                title = $('#dialog-add-note input[name="title"]').val();
                author = $('#dialog-add-note input[name="author"]').val();
                content = $('#dialog-add-note textarea[name="content"]').val();

                $('#dialog-add-note input[name="title"]').keyup(function () { CheckValidation($('#dialog-add-note input[name="title"]')) });
                $('#dialog-add-note input[name="author"]').keyup(function () { CheckValidation($('#dialog-add-note input[name="author"]')) });
                $('#dialog-add-note textarea[name="content"]').keyup(function () { CheckValidation($('#dialog-add-note textarea[name="content"]')) });

                if (title == "" || author == "" || content == "") {
                    CheckValidation($('#dialog-add-note input[name="title"]'));
                    CheckValidation($('#dialog-add-note input[name="author"]'));
                    CheckValidation($('#dialog-add-note textarea[name="content"]'));
                    return false;
                }



            }
            else {
                title = $('#dialog-edit-note input[name="title"]').val();
                author = $('#dialog-edit-note input[name="author"]').val();
                content = $('#dialog-edit-note textarea[name="content"]').val();

                $('#dialog-edit-note input[name="title"]').keyup(function () { CheckValidation($('#dialog-edit-note input[name="title"]')) });
                $('#dialog-edit-note input[name="author"]').keyup(function () { CheckValidation($('#dialog-edit-note input[name="author"]')) });
                $('#dialog-edit-note textarea[name="content"]').keyup(function () { CheckValidation($('#dialog-edit-note textarea[name="content"]')) });


                if (title == "" || author == "" || content == "") {
                    CheckValidation($('#dialog-edit-note input[name="title"]'));
                    CheckValidation($('#dialog-edit-note input[name="author"]'));
                    CheckValidation($('#dialog-edit-note textarea[name="content"]'));
                    return false;
                }


            }

            var detail = { Id: id, Title: title, Content: content, Author: author };
            $('.flashMoveFile').css("height", $(window).height());
            bookshelf.loadingMoveFile.apply();
            //bookshelf.loading.apply();

            var success = function (data) {
                detail.Id = data.d;
                if (detail.Id === 0) {
                    $('#dialog-message-content').text("Failed to insert new note.");
                    $('#dialog-message-all').dialog('option', 'title', "Update Note Error");

                    $('#dialog-message-all').dialog("open");
                    return;
                }
                else if (detail.Id === -2) {
                    $('#dialog-message-content').text("Failed to check the security code.");
                    $('#dialog-message-all').dialog('option', 'title', "Update Note Error");

                    $('#dialog-message-all').dialog("open");
                } else {

                    if (id > 0) {
                        var displayPageNum;
                        $.each($dividers.Items[$current_divider_nth].notes, function (i, e) {
                            if (e.Id && id && parseInt(e.Id) == parseInt(id)) {
                                e.Title = title;
                                e.Author = author;
                                e.Content = content;
                                displayPageNum = e.start;
                            }
                        });
                        BindDividerOnIndexSearch(false, true, 0, false, displayPageNum);
                    }
                    else {
                        bookshelf.insertNote(detail);
                    }
                }

                //bookshelf.loaded.apply();
                bookshelf.loadedMoveFile.apply();
                sender.dialog("close");
            };

            var failed = function (data) {
                //bookshelf.loaded.apply();
                bookshelf.loadedMoveFile.apply();
                sender.dialog("close");

                $('#dialog-message-content').text(data.responseText);
                $('#dialog-message-all').dialog('option', 'title', "Update Note Error");

                $('#dialog-message-all').dialog("open");
            };

            var dividerid = $dividers.GetValue($current_divider_nth).id;
            $jsonmotheds.updateNote(params.uid, id, dividerid, content, params.sid, title, author, success, failed);
        },

        getPageNote: function () {
            var item = null;
            for (var i = 0; i < $items_auto_source.length; i++) {
                if ($items_auto_source[i].isNote) {
                    item = $items_auto_source[i];
                    break;
                }
            }

            return item;
        },

        getNoteById: function (id, notes) {
            var item = null;
            for (var i = 0; i < notes.length; i++) {
                if (notes[i].Id === id) {
                    item = notes[i];
                    break;
                }
            }

            return item;
        },

        //insertNote: function (detail) {

        //    var book = $(".sj-book");
        //    var pages = $(".sj-book").turn('pages');

        //    var toc = $("#toc");
        //    var items = $dividers.Items;
        //    var target = items[$current_divider_nth];
        //    var notes = target['notes'];
        //    var note = { Id: detail.Id, Author: detail.Author, Title: detail.Title, Content: detail.Content, DividerId: target["id"] };
        //    target.total = target.total + 1;

        //    if (!$book_contents[detail.Id]) {
        //        try {
        //            if ($('.empty[pageth="' + pages + '"]').length > 0) {
        //                var pageNode = new NotePage(detail.Id, pages, $current_divider, {}, detail);
        //                $book_contents[detail.Id] = pageNode;
        //                $(".sj-book").turn('addPage', pageNode.element, pages);

        //            } else {
        //                pageNode = new NotePage(detail.Id, ++pages, $current_divider, {}, detail);

        //                $book_contents[detail.Id] = pageNode;
        //                $(".sj-book").turn('addPage', pageNode.element);
        //                var element = $('<div class="empty" pageth="' + (pages + 1) + '"/>');
        //                $(".sj-book").turn('addPage', element);

        //                $('#slider-book').slider('option', 'max', pages);
        //            }

        //            pageNode.initialize();

        //            var lastObj = $items_auto_source[$items_auto_source.length - 1];


        //            if ($items_auto_source.length == 0)
        //            {
        //                $total_page_numb = 1;
        //            }
        //            else
        //            {
        //                if (lastObj.isNote) {
        //                    var notes = items[$current_divider_nth]['notes'];
        //                    var lastNote = notes[notes.length - 1];
        //                    $total_page_numb = lastNote.start + 1;
        //                }
        //                else {

        //                    $total_page_numb = lastObj.start + lastObj.count;
        //                }

        //            }
        //            notes.push(note);
        //            $items_auto_source.push({
        //                value: target["id"],
        //                isNote: true,
        //                label: 'Page Notes',
        //                start: $total_page_numb,
        //                count: 1,
        //                pages: notes
        //            });
        //            addNote(toc, target, note);



        //            $(".sj-book").turn('page', pages);
        //        } catch (err) {
        //            // console.log(err);
        //        }
        //    } else {
        //        pageNode = $book_contents[detail.Id];
        //        var item = bookshelf.getNoteById(detail.Id, notes);
        //        if (item !== null) {
        //            item.Title = note.Title;
        //            item.Author = note.Author;
        //            item.Content = note.Content;
        //        }

        //        pageNode.updateUI(note);
        //    }
        //},

        insertNote: function (detail) {
            var nextStartNote;
            var dividerData = $dividers.Items[$current_divider_nth];
            var notes = dividerData.notes;
            var files = dividerData.files;
            if (notes.length > 0) {
                nextStartNote = notes[notes.length - 1].start + 1;
            }
            else if (files.length > 0) {
                nextStartNote = files[files.length - 1].start + files[files.length - 1].Count;
            }
            else
                nextStartNote = 1;

            var note = { Id: detail.Id, Author: detail.Author, Title: detail.Title, Content: detail.Content, DividerId: $dividers.Items[$current_divider_nth].id, start: nextStartNote };
            notes.push(note);

            BindDividerOnIndexSearch(false, true, 0, false, nextStartNote);
        },

        updateQote: function (sender, details) {
            var pagekey = details.pagekey;

            bookshelf.loading.apply();
            var success = function (data) {
                var id = data.d.QuiteNoteList[0].Id;
                if (id === 0) {
                    $('#dialog-message-content').text("Failed to insert new quick note.");
                    $('#dialog-message-all').dialog('option', 'title', "Update Qote Error");

                    $('#dialog-message-all').dialog("open");
                    return;
                }
                else if (id === -2) {
                    $('#dialog-message-content').text("Failed to check the security code.");
                    $('#dialog-message-all').dialog('option', 'title', "Update Qote Error");

                    $('#dialog-message-all').dialog("open");
                } else {
                    var items = $dividers.Items;
                    var target = items[$current_divider_nth];
                    var qotes = target['qotes'];

                    var pageview = $book_contents[pagekey];
                    if (!pageview.qotes.ContainsKey(id)) {
                        try {
                            //var qbox = new QoteBox(id, pageview.element, details, true);
                            var qbox = new QoteBox(id, pageview.el, details, true);
                            pageview.qotes.Add(id, qbox);

                            var qote = data.d.QuiteNoteList[0];
                            qotes.push(qote);

                        } catch (e) { }
                    }
                    else {

                        $.each(qotes, function (i, e) {
                            if (e.Id && parseInt(e.Id) == parseInt(id)) {
                                e.Content = data.d.QuiteNoteList[0].Content;
                                e.Fixed = data.d.QuiteNoteList[0].Fixed;
                                e.LastModDate = data.d.QuiteNoteList[0].LastModDate;
                                e.Opened = data.d.QuiteNoteList[0].Opened;
                                e.PageId = data.d.QuiteNoteList[0].PageId;
                                e.PageKey = data.d.QuiteNoteList[0].PageKey;
                                e.X = data.d.QuiteNoteList[0].X;
                                e.Y = data.d.QuiteNoteList[0].Y;
                                e.Id = data.d.QuiteNoteList[0].Id;
                            }
                        });
                    }
                }

                bookshelf.loaded.apply();

            };

            var failed = function (data) {
                $('#dialog-message-content').text(data.responseText);
                $('#dialog-message-all').dialog('option', 'title', "Update Qote Error");

                $('#dialog-message-all').dialog("open");

                bookshelf.loaded.apply();
            };
            var dividerid = $dividers.GetValue($current_divider_nth).id;
            $jsonmotheds.updateQote(params.uid, details.id, pagekey, details.content, dividerid, details.x,
                details.y, params.sid, success, failed);
        },

        deleteQote: function (sender, id) {
            bookshelf.loading.apply();

            var success = function (data) {

                sender.destroy();
                var items = $dividers.Items;
                var target = items[$current_divider_nth];
                var qotes = target['qotes'];
                target['qotes'] = $.grep(target['qotes'], function (e) {
                    if (id && e.Id && parseInt(e.Id) == parseInt(id)) {
                        return false;
                    }
                    else {
                        return true;
                    }
                });

                bookshelf.loaded.apply();
            };

            var failed = function (data) {
                $('#dialog-message-content').text(data.toString());
                $('#dialog-message-all').diloag('option', 'title', "Delete Qote Error");

                $('#dialog-message-all').dialog("open");

                bookshelf.loaded.apply();
            };

            if (id) {
                $jsonmotheds.deleteQote(id, params.sid, success, failed);
            }
        },
        // : 
        mailTo: function (sender) {
            var email = $('#dialog-share-book input[name="email"]').val();
            //var name = $('#dialog-share-book input[name="myname"]').val();
            var emailto = $('#dialog-share-book input[name="emailto"]').val();
            var nameto = $('#dialog-share-book input[name="nameto"]').val();

            var title = $('#dialog-share-book input[name="title"]').val();
            var content = $('#dialog-share-book textarea[name="content"]').val();
            var Isvalid = false;

            //$('#dialog-share-book input[name="myname"]').keyup(function () {  CheckValidation($('#dialog-share-book input[name="myname"]')) });
            //$('#dialog-share-book input[name="email"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="email"]')) });

            //$('#dialog-share-book input[name="emailto"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="emailto"]')) });

            //if (email == "" || name == "" || emailto == "") {

            var emailList = "";
            if (emailto.length > 0) {
                emailList = emailto.split(';');
                for (var i = 0; i < emailList.length; i++) {
                    var test = "";
                    if (!CheckEmailValidation(emailList[i])) {
                        alert("Please enter valid Email separated by ';' !")
                        $('#dialog-share-book input[name="emailto"]').css('border', '1px solid red');
                        hideLoader();
                        return false;
                    }
                }
            }
            if (emailto == "") {
                //CheckValidation($('#dialog-share-book input[name="email"]'));
                //CheckValidation($('#dialog-share-book input[name="myname"]'));
                CheckValidation($('#dialog-share-book input[name="emailto"]'));
                //CheckEmailValidation($('#dialog-share-book input[name="email"]'));
                CheckEmailValidation($('#dialog-share-book input[name="emailto"]'))
                return false;
            }

            var nameToList = "";
            if (nameto.length > 0) {
                //var isValid = /^[a-zA-Z0-9!@#\$%\^\&*\,\)\(;+=._-]+$/.test(nameto);
                var isValid = /^[a-zA-Z\;\s]+$/.test(nameto)
                if (isValid)
                    nameToList = nameto.split(';');
                else {
                    alert("Please enter valid ToName separated by ';' !")
                    $('#dialog-share-book input[name="nameto"]').css('border', '1px solid red');
                    hideLoader();
                    return false;
                }
            }
            if (emailList.length != nameToList.length) {
                alert("Number of Emails should match with number of ToNames!");
                $('#dialog-share-book input[name="emailto"]').css('border', '1px solid red');
                $('#dialog-share-book input[name="nameto"]').css('border', '1px solid red');
                hideLoader();
                return false;
            }

            var emailsTo = [];

            for (var i = 0; i < emailList.length; i++) {
                emailsTo.push({ "Email": emailList[i], "ToName": nameToList[i] });
            }

            //if (!Isvalid) {

            //    Isvalid = CheckEmailValidation($('#dialog-share-book input[name="email"]'));
            //    if (!Isvalid)
            //        return Isvalid;
            //}

            //Isvalid =  CheckEmailValidation($('#dialog-share-book input[name="emailto"]'));
            //if (!Isvalid)
            //{ return Isvalid; }


            var details = {
                title: title, content: content, email: email,
                name: "", emailto: JSON.stringify(emailsTo), nameto: nameto
            };
            bookshelf.loading.apply();
            //bookshelf.loadingMoveFile.apply();
            var success = function (data) {
                //bookshelf.loaded.apply();

                var id = data.d;
                if (id === 0) {
                    $('#dialog-message-content').text("Failed to share EdFiles.");
                    $('#dialog-message-all').dialog('option', 'title', "Share EdFiles Error");

                    $('#dialog-message-all').dialog("open");
                    return;
                }
                else if (id === -2) {
                    $('#dialog-message-content').text("Failed to check the security code.");
                    $('#dialog-message-all').dialog('option', 'title', "Sharing EdFiles Error");

                    $('#dialog-message-all').dialog("open");
                } else {
                    $('#dialog-message-content').text(data.responseText);
                    $('#dialog-message-all').dialog('option', 'title', "Sharing EdFiles Success");
                    $("#spanText").text("This folder has been shared  Successfully.");
                    $('#dialog-message-all').dialog("open");
                }
                sender.dialog("close");
                bookshelf.loaded.apply();
                // bookshelf.loadedMoveFile.apply();

                $("#spanText").tex('');


            };

            var failed = function (data) {
                bookshelf.loaded.apply();

                $('#dialog-message-content').text("Failed to share EdFiles:" + data.responseText);
                $('#dialog-message-all').dialog('option', 'title', "Sharing EdFiles Error");

                $('#dialog-message-all').dialog("open");

                sender.dialog("close");

            };
            $jsonmotheds.mailTo(params.id, params.sid, details.email, details.emailto, details.name,
                details.nameto, details.title, details.content, success, failed);
        },

        slider: function (page) {
            var $slider = $('#slider-book');
            var sliderpage = $slider.slider('option', 'value');

            if (typeof (sliderpage) !== 'object' && sliderpage !== page) {
                $slider.slider('option', 'value', page);
                var max = $slider.slider('option', 'max');
                $('#slider-bar-number').val(page + '/' + max);
            }
        },

        render: function (view) {
            var pages = view, k = 0;
            var selectors = "div[pageref][pageth='" + pages[0] + "']";
            var noteselectors = "div[noteref][pageth='" + pages[0] + "']";
            var book = $('.sj-book');
            var next = [];
            if (book.turn('hasPage', pages[0] + 2)) {
                next = $('.sj-book').turn('view', pages[0] + 2);
            }

            while (++k < pages.length) {
                selectors += ",div[pageref][pageth='" + pages[k] + "']";
                noteselectors += ",div[noteref][pageth='" + pages[k] + "']";
            }

            PDFView.highPriorityPages = next;
            PDFView.highestPriorityPages = view;

            $('.sj-book').find(selectors).each(function (index, elment) {
                var key = $(this).attr('pageref');
                $page_promises.GetValue(key).then(function (data) { //onData to then
                    $book_contents[key].setPage(data);
                    $book_contents[key].initialize();
                    $book_contents[key].renderView('page');
                });
            });

            $('.sj-book').find(noteselectors).each(function (index, elment) {
                var key = $(this).attr('noteref');
                var note = $book_contents[key];
                if (note) {
                    note.initialize();
                }
            });

        },

        searchBook: function () {
            var size = $('.sj-book').turn('size');
            var height = size.height + 8;
            $('#book-search').css({ height: height });
            $('#book-search-result').css({ height: height - 100 });

            if ($('#book-search').is(':visible')) {
                $('#book-search').hide();
                $('#book-search-items').children().remove();
                $('#search-item-text').val("");
                $('#results-count').text("");
                $('.textLayer').find('.selected').remove();
                PDFFindController.active = false;
            } else {
                $('#book-search').show();
                PDFFindController.active = true;
            }

            //$('.sj-book').turn('center');

            $('#book-search').position({
                of: $("#book-zoom .sj-book"),
                my: "left top",
                at: "right top",
                offset: "45 -8",
                collision: "fit none"
            });
        },

        getExpiration: function () {
            $jsonmotheds.getExpirationTime(this.expirationSucess, this.expirationFailure);
            return true;
        },

        expirationSucess: function (data) {
            $("#email").val(data.d.email);
            $("#expirationTime").val(data.d.expirationTime);
            //if (data.d.isOffice)
            //    $("#expirationTime").attr("readonly", false);
            //else {
            //    $("#updateExpiration").hide();
            //    $("#expirationTime").css("width", "95%");
            //}
        },

        expirationFailure: function (data, error) {
            $("#email").val(data.d.email);
            $("#expirationTime").val(data.d.expirationTime);
            //if (data.d.isOffice)
            //    $("#expirationTime").attr("readonly", false);
            //else {
            //    $("#updateExpiration").hide();
            //    $("#expirationTime").css("width", "95%");
            //}
        },

        getSesionPrivilege: function () {
            $jsonmotheds.getSesionPrivilege(1, this.getSesionPrivilegeSucess, this.getSesionPrivilegeFailure);
            return true;
        },

        getSesionPrivilegeSucess: function (data) {
            if (data.d.hasViewPrivilegeOnly == true) {
                $('.share-email, .share-book, .print-page, .download-file').removeClass();
                $('.ic-edit-search').removeAttr("src");
            }
            if (data.d.hasPrivilegeToDownload != true)
            {
                $('.download-file').removeClass();
            }
        },

        getSesionPrivilegeFailure: function (data, error) {
            if (data.d == true) {
                $('.share-email, .share-book, .print-page, .download-file').removeClass();
                $('.ic-edit-search').removeAttr("src");
            }
        }
    };


    function splashHeight() {
        return ($('.splash').hasClass('preview')) ? 800 : 700;
    }

    function scrollTop(top, speed) {
        scrolling = true;

        $('html,body').animate({ scrollTop: top }, speed, function () {
            scrolling = false;
            setCurrentHash();
        });
    }



    function navigation(where) {
        var tokens = where.split(' ');
        switch (tokens[0]) {
            case 'page-note':
                $('#dialog-add-note').dialog('open');
                $('#dialog-add-note input[name="title"]').val('');
                $('#dialog-add-note input[name="author"]').val('');
                $('#dialog-add-note textarea[name="content"]').val('');

                $('#dialog-add-note input[name="title"]').css('border', '1px solid black');
                $('#dialog-add-note input[name="author"]').css('border', '1px solid black');
                $('#dialog-add-note textarea[name="content"]').css('border', '1px solid black');
                break;
            case 'quick-note':
                var book = $('.sj-book');
                var view = book.turn('view');
                var viewLeft = $('.sj-book .p' + view[0]), viewRight = $('.sj-book .p' + view[1]);

                if (!viewLeft.hasClass('hard') && typeof (viewLeft.attr('pageref')) !== 'undefined') {

                    $('<div >', { 'class': 'drop-note' }).
                        html('<div>Press drop here</div>').
                        appendTo(viewLeft).
                        delay(1000).
                        animate({ opacity: 0 }, 500, function () {
                            $(this).remove();
                        });
                }

                if (!viewRight.hasClass('hard') && typeof (viewRight.attr('pageref')) !== 'undefined') {

                    $('<div >', { 'class': 'drop-note' }).
                        html('<div>Press drop here</div>').
                        appendTo(viewRight).
                        delay(1000).
                        animate({ opacity: 0 }, 500, function () {
                            $(this).remove();
                        });
                }

                break;
            case 'print-page':
                $('#dialog-print-page').dialog('open');

                break;
            case 'prevs-page':
                var sliderbar = $('#slider-book');
                if (sliderbar.slider('option', 'value') > 1) {
                    $('.sj-book').turn('previous');
                }
                break;
            case 'next-page':
                sliderbar = $('#slider-book');
                if (sliderbar.slider('option', 'value') < sliderbar.slider('option', 'max')) {
                    $('.sj-book').turn('next');
                }

                break;
            case 'table-contents':
                $('.sj-book').turn('page', 1);
                break;
            case 'share-email':
                $('#dialog-share-book').dialog('open');

                $('#dialog-share-book input[name="email"]').val('');
                $('#dialog-share-book input[name="myname"]').val('');
                $('#dialog-share-book input[name="emailto"]').val('');
                $('#dialog-share-book input[name="nameto"]').val('');

                $('#dialog-share-book input[name="title"]').val('');
                $('#dialog-share-book textarea[name="content"]').val('');

                $('#dialog-share-book input[name="email"]').css('border', '1px solid black');
                $('#dialog-share-book input[name="myname"]').css('border', '1px solid black');
                $('#dialog-share-book input[name="emailto"]').css('border', '1px solid black');

                $('#dialog-share-book input[name="email"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="email"]')) });

                $('#dialog-share-book input[name="emailto"]').blur(function () { CheckEmailValidation($('#dialog-share-book input[name="emailto"]')) });
                bookshelf.getExpiration();
                break;
            case 'share-book':
                $('#dialog-print-page').dialog('open'); // same as print
                break;
            case 'share-pinterest':
                break;
            case 'search-book':
                bookshelf.searchBook();
                break;
                //case 'update-expiration':
                //    var expirationTime = $("#expirationTime").val();
                //    bookshelf.updateExpiration(expirationTime);
                //    break;
            case 'search-qotes':
                var size = $('.sj-book').turn('size');
                var height = size.height + 8;
                $('#book-qotes').css({ height: height });
                $('#qote-search-result').css({ height: height - 70 })

                if ($('#book-qotes').is(':visible')) {
                    $('#book-qotes').hide();
                } else {
                    $('#book-qotes').show();
                }

                $('.sj-book').turn('center');


                break;
        }

    }

    function update_page_slider() {
        var offset = $('#slider-book .ui-slider-handle').offset();
        var value = $('#slider-book').slider('option', 'value');
        $('#current_page_value').text(value).css({ left: offset.left });
        $('#current_page_value').fadeIn();
    }

    function update_zoom_slider() {
        var offset = $('#slider-zoom .ui-slider-handle').offset();
        var value = $('#slider-zoom').slider('option', 'value');
        $('#current_zoom_value').text(value * 100 + '%').css({ left: offset.left });
        $('#current_zoom_value').fadeIn();
    }

    // DOMReady

    $(document).ready(function () {

        if (!$.isTouch) {
            $('.go-up').click(function (e) {
                scrollTop(0, 'fast');
                e.preventDefault();
            });
        }

        jQXB.initialize();
        jQXB.compatibilitymode = true;
        // Share icons
        //$('.share .icon').bind($.mouseEvents.over, function(e) {

        //    var className = $.trim($(this).
        //        attr('class').
        //        replace(/\b([a-z-]*hover|icon)\b/g, ''));
        //    $(this).addClass(className + '-hover');

        //}).bind($.mouseEvents.out, function(e) {

        //    var className = $.trim($(this).
        //        attr('class').
        //        replace(/\b([a-z-]*hover|icon)\b/g, ''));
        //    $(this).removeClass(className + '-hover');

        //});

        clickElement($('.share .icon'), function (e) {
            navigation($.trim($(this).
                attr('class').
                replace(/\b([a-z-]*hover|icon)\b/g, '')));

        });

        //clickElement($('.update-expiration'), function (e) {
        //    navigation($.trim($(this).
        //        attr('class').
        //        replace(/\b([a-z-]*hover|icon)\b/g, '')));

        //});

        clickElement($('.mailOnly .icon'), function (e) {
            navigation($.trim($(this).
                attr('class').
                replace(/\b([a-z-]*hover|icon)\b/g, '')));

        });

        // Close button
        clickElement($('.quit'), function () {
            window.open('', '_self');
            window.close();

        });

        // Back to the shelf
        clickElement($('.back'), function () {
            bookshelf.loading.apply();
            switchView(viewStatus.self);
            bookshelf.loaded.apply();
        });

        clickElement($('#dialog-print-page input'), function (event) {
            var nth = 0;
            switch (event.currentTarget.id) {
                case 'left-page':
                    nth = 0;
                    break;
                case 'right-page':
                    nth = 1;
                    break;
            }

            var view = $('.sj-book').turn('view');
            var page = $('.sj-book .p' + view[nth]);
            var pageid = null;

            if (page && !page.isNote) {
                pageid = page.attr('pageref');
                if (pageid) {
                    var pageView = $book_contents[pageid];
                    PDFView.beforePrint(pageView);
                }
            }


            $('#printPreviewContainer').dialog('open');
            $('#dialog-print-page').dialog('close');
        });


        clickElement($('#book-search-title .book-search-exit a'), function () {
            $('#book-search').hide();
            $('.sj-book').turn('center');
        });

        clickElement($('#qote-title .book-search-exit a'), function () {
            $('#book-qotes').hide();
            $('.sj-book').turn('center');
        });

        clickElement($('.share .download-file'), function () {

            var view = $('.sj-book').turn('view');
            var pageLeft = $('.sj-book .p' + view[0]);
            var pageRight = $('.sj-book .p' + view[1]);
            var pageidR, pageid, idR, id, urlR, url = null;

            if (pageRight && !pageRight.isNote) {
                pageidR = pageRight.attr('pageref');
                if (pageidR != undefined) {
                    idR = pageidR.split('-')[0];
                    if (pageidR) {
                        idR = pageidR.split('-')[0];
                        urlR = getItemSource(idR);
                        if (urlR)
                            var path = urlR;
                        if (urlR.indexOf("%") >= 0) {
                            path = urlR.replace(/\%/g, '$');
                        }
                        else if (urlR.indexOf('$') >= 0) {
                            path = urlR.replace(/\\\u0024/g, '%');
                        }
                        //PDFView.download(path);
                        PDFView.download(idR);
                        //return;
                    }
                }
                else {
                    if (pageRight.attr('noteref')) {
                        alert("Right Page Note is not Download.");
                    }

                }
            }

            if (pageLeft && !pageLeft.isNote) {
                pageid = pageLeft.attr('pageref');
                if (pageid != undefined) {
                    if (pageid) {
                        id = pageid.split('-')[0];
                        url = getItemSource(id);
                        if (urlR != url) {
                            // PDFView.download(url);
                            PDFView.download(id);
                            return;
                        }
                    }
                }
                else {
                    if (pageLeft.attr('noteref')) {
                        alert("Left Page Note is not Download.");
                    }

                }
            }

        });

        PDFFindBar.initialize();
        PDFFindController.initialize();

        // Initialize buttons

        $("'[placeholder]'").focus(function () {
            var input = $(this);
            if (input.val() == input.attr("'placeholder'")) {
                input.val("''");
                input.removeClass("'placeholder'");
            }
        })
            .blur(function () {
                var input = $(this);
                if (input.val() == "''" || input.val() == input.attr("'placeholder'")) {
                    input.addClass("'placeholder'");
                    input.val(input.attr("'placeholder'"));
                }
            }).blur();


        $(".tool .quick-note").draggable({
            appendTo: $('.sj-book'), //"body"
            helper: "clone",
            start: function (event, ui) {
                var book = $('.sj-book');
                var view = book.turn('view');
                var viewLeft = $('.sj-book .p' + view[0]), viewRight = $('.sj-book .p' + view[1]);

                if (!viewLeft.hasClass('hard') && typeof (viewLeft.attr('pageref')) !== 'undefined') {

                    $('<div >', { 'class': 'drop-note' }).
                        html('<div>Press drop here</div>').
                        appendTo(viewLeft).
                        delay(1000).
                        animate({ opacity: 0 }, 500, function () {
                            $(this).remove();
                        });

                    viewLeft.bind('mouseenter', function (eventObject) {
                        $dragPage = viewLeft;
                    });

                    viewLeft.bind('mouseleave', function (eventObject) {
                        $dragPage = null;
                    });
                }

                if (!viewRight.hasClass('hard') && typeof (viewRight.attr('pageref')) !== 'undefined') {

                    $('<div >', { 'class': 'drop-note' }).
                        html('<div>Press drop here</div>').
                        appendTo(viewRight).
                        delay(1000).
                        animate({ opacity: 0 }, 500, function () {
                            $(this).remove();
                        });

                    viewRight.bind('mouseenter', function (eventObject) {
                        $dragPage = viewRight;
                    });

                    viewRight.bind('mouseleave', function (eventObject) {
                        $dragPage = null;
                    });
                }

            },
            zIndex: 9999,
            drag: function (event, ui) {
            },
            stop: function (event, ui) {
                var view = $('.sj-book').turn('view');
                var pageLeft = $('.sj-book .p' + view[0]);
                var pageRight = $('.sj-book .p' + view[1]);

                pageLeft.unbind('mouseenter');
                pageLeft.unbind('mouseleave');

                pageRight.unbind('mouseenter');
                pageRight.unbind('mouseleave');

                if ($dragPage) {
                    var pos = $dragPage.offset();
                    var x = event.pageX - pos.left,
                      y = event.pageY - pos.top;

                    var pagekey = $dragPage.attr('pageref');

                    bookshelf.updateQote($dragPage, { id: null, pagekey: pagekey, x: x, y: y, content: '', visible: true }, true);

                }
            }
        });


        $("#slider-book").slider({
            value: 1,
            min: 1,
            max: 50,
            step: 1,
            start: function (event, ui) {
                //var $current_value = $('<div id="current_page_value"></div>');
                var $current_value = $('<div id="current_page_value" style="padding-top: -5px; padding-left: -5px; margin-left: 0px; margin-top: -2px;z-index:1000;"></div>');
                $(this).append($current_value).append($current_value);
                $current_value.empty();
                slide_int = setInterval(update_page_slider, 10);
            },
            slide: function (event, ui) {
                setTimeout(update_page_slider, 10);
            },
            stop: function (event, ui) {
                clearInterval(slide_int);
                slide_int = null;

                var $current_value = $('#current_page_value');
                $current_value.fadeOut(500);
                setTimeout(function () { $('#current_page_value').remove(); }, 500);

                var current_page = $('#slider-book').slider('option', 'value');
                var max = $('#slider-book').slider('option', 'max');
                $('#slider-bar-number').val(current_page + '/' + max);
                $('.sj-book').turn('page', current_page);
            }
        });


        $('#slider-bar-number').keypress(function (e) {
            if (e.keyCode === 13) {
                var v = $('#slider-bar-number').val();

                var max = $('#slider-book').slider('option', 'max');
                var min = $('#slider-book').slider('option', 'min');
                var current = $('#slider-book').slider('option', 'value');
                if (parseInt(v) > max) {
                    $('#slider-bar-number').val(current);
                    v = current;
                } else if (parseInt(v) < min) {
                    $('#slider-bar-number').val(current);
                    v = current;
                }

                $('#slider-book').slider('option', 'value', v);
                $('.sj-book').turn('page', v);
                return true;
            } else if (e.keyCode === 8 || e.keyCode === 46) { //|| e.keyCode === 0 || e.keyCode === 47
                return true;
            } else if (!isNumberKey(e)) {
                return false;
            }

        });

        $('#slider-bar-number').focus(function (e) {
            var v = $('#slider-bar-number').val();
            var strs = v.split('/', 2);
            if (strs.length > 0) {
                $('#slider-bar-number').val(strs[0]);
            }

        });

        $('#slider-bar-number').blur(function (e) {
            var max = $('#slider-book').slider('option', 'max');
            var v = $('#slider-bar-number').val();
            var strs = v.split('/', 2);
            if (strs.length > 0) {
                $('#slider-bar-number').val(strs[0] + '/' + max);
            }

        });

        $('#slider-bar-number').change(function (e) {
            var v = $('#slider-bar-number').val();
            var strs = v.split('/', 2);
            if (strs.length > 0) {
                v = strs[0];
            }

            var max = $('#slider-book').slider('option', 'max');
            var min = $('#slider-book').slider('option', 'min');
            var current = $('#slider-book').slider('option', 'value');
            if (parseInt(v) > max) {
                $('#slider-bar-number').val(current);
            }

            if (parseInt(v) < min) {
                $('#slider-bar-number').val(current);
            }

        });


    });

    // Window events
    $(window).load(function () {
        bookshelf.regExp(["ID", "DATA", "ISEDIT", "ISDENIED"], {

            yep: function (path, parts) {
                var id = parts[0],
                    sid = parts[1],
                    isEdit = parts[2];
                isAccessDenied = parts[3];
                if (sid.length > 15) {
                    sid = sid.substring(0, 15);
                }
                if (id === '' || sid === '') {
                    return;
                }

                params.id = id;
                params.sid = id + "_" + sid;
                params.isEdit = isEdit;
                params.isAccessDenied = isAccessDenied;

                //begin login
                try {
                    bookshelf.loading.apply();
                    bookshelf.login(params.id, params.sid, params.isEdit, params.isAccessDenied);
                } catch (exp) {
                }

            },

            nop: function (path) {
            }
        });

    });

    function CheckValidation(fieldObj) {
        if (fieldObj.val() == "") {
            fieldObj.css('border', '1px solid red');
        }
        else {
            fieldObj.css('border', '1px solid black');
        }
    }

    function CheckEmailValidation(fieldObj) {

        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(fieldObj)) {
            return true;
        }
        else {
            return false;
        }
    }

})(jQuery);