$(window).load(function() {
	//Prevent Page Reload on all # links
	$("a[href='#']").click(function(e) {
		e.preventDefault();
	});

	// On scroll header small
	$(window).scroll(function(e) {
		if($(window).scrollTop() > 300)
			$(".wrapper").addClass('small-header');
		else
			$(".wrapper").removeClass('small-header');
	});


	// FooterAdj
	function footerAdj(){
		var mt = $("#footer").innerHeight();
		$("#footer").css({"margin-top": -mt});
		$(".wrapper").css({"padding-bottom": mt});
	};
	footerAdj();
	$(window).resize(function() {
		setTimeout(function(){
			footerAdj();
		}, 300);
	});


	// MainNavigation
	$(".nav-icon").click(function(e){
		$("#mainNavigation").stop(true, true).slideToggle(300);
	});


	
	//fast click for touch devices  
	FastClick.attach(document.body);

});