Hashtable = function(){
    /// <summary>
    ///
    /// </summary>
    //---------------------------------------------------------
    // PROPERTY: Items
    //---------------------------------------------------------
    this.Items;
    this.Count;
    //---------------------------------------------------------
    // METHOD: GetKeys
    //---------------------------------------------------------
    // Get array of keys from the Hashtable.
    this.GetKeys = function(){
        var keys = new Array();
        for (var property in this.Items){
            keys.push(property);
        }
        return keys;
    }
    //---------------------------------------------------------
    // METHOD: GetValues
    //---------------------------------------------------------
    // Get array of values from the Hashtable.
    this.GetValues = function(){
        var values = new Array();
        for (var property in this.Items){
            values.push(this.Items[property]);
        }
        return values;
    }
    //---------------------------------------------------------
    // METHOD: GetValue
    //---------------------------------------------------------
    // Get value by key.
    this.GetValue = function(key){
        return this.Items[key];
    }
    //---------------------------------------------------------
    // METHOD: GetKey
    //---------------------------------------------------------
    // Get first key by value;
    this.GetKey = function(value){
        var key;
        for (var property in this.Items){
            if (this.Items[property] == value){
                key = property;
                break;
            }
        }
        return key;
    }
    //---------------------------------------------------------
    // METHOD: Add
    //---------------------------------------------------------
    // Adds an element with the specified key and value into the Hashtable.
    this.Add = function(key,value){
        var containsKey = this.ContainsKey(key);
        if (!containsKey) this.Count++;
        this.Items[key] = value;
        return !containsKey;
    }
    //---------------------------------------------------------
    // METHOD: Remove
    //---------------------------------------------------------
    // Removes the element with the specified key from the Hashtable.
    this.Remove = function(key){
        var containsKey = this.ContainsKey(key);
        if (containsKey){
            this.Count--;
            delete this.Items[key];
        }
        return !containsKey;
    }
    //---------------------------------------------------------
    // METHOD: Clear
    //---------------------------------------------------------
    // Removes all items.
    this.Clear = function(){
        this.Items = {};
    }
    //---------------------------------------------------------
    // METHOD: ContainsKey
    //---------------------------------------------------------
    // Determines whether the Hashtable contains a specific key.
    this.ContainsKey = function(key){
        return (typeof(this.Items[key]) != "undefined");
    }
    //---------------------------------------------------------
    // METHOD: ContainsValue
    //---------------------------------------------------------
    // Determines whether the Hashtable contains a specific value.
    this.ContainsValue = function(value){
        return (typeof(this.GetKey(value)) != "undefined");
    }
    //---------------------------------------------------------
    // METHOD: ToString
    //---------------------------------------------------------
    // Returns a String that represents the current Object.
    this.ToString = function(){
        return (this.GetType().Name+"[Count="+Count+"]");
    }
    //---------------------------------------------------------
    // INIT: Class
    //---------------------------------------------------------
    this.InitializeClass = function(){
        // Use array to store items (slower).
        this.Items = {};
        this.Count = new Number();
    }
    this.InitializeClass();
}