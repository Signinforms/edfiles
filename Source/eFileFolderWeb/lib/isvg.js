/*!
 * isvg
 *
 * Released under the MIT license.
 *

Copyright (c) 2011 Dustin Oprea

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT  OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 */
 var $divider_caches = new Hashtable;
 
(function($) {
	
    $.fn.isvg = function(options, options2) {
            // Options:
            //
            // Data:            (req) readonly string
            // DataIsUrl:       (opt) readonly bool
            // ObserverTree:    (opt) readonly object
            // Observers:       (opt) readonly array
            // Callback :       (opt) readonly object
			// Size:			(opt) readonly page width and height	

            var SvgLoader = {
                                ConfNs: "http://www.w3.org/2000/svg"
                            }

            var $this = $(this);
            var ns = { }
            var nsRoot = null;

            SvgLoader.LoadDataIntoContainer = function(arguments)
            {
                // Argument array:
                //
                // $Parent:         (req) readonly $
                // $SvgData:        (req) readonly $
                // ObserverTree:    (opt) readonly object
                // Observers:       (opt) readonly array
                // CurrentPath:     (opt) readonly string
                //
                // The observer-tree functionality is much more efficient. Use "Observers" 
                // only if you must.
                //
                // Examples:
                //
                //  var observerTree = { }
                //
                //  observerTree.svg = { }
                //  observerTree.svg.clipPath = { }
                //  observerTree.svg.clipPath.g = { }
                //  observerTree.svg.clipPath.g["_"] = function() { alert("here 1!"); }
                //
                //  observers = [ ]
                //
                //  observers[observers.length] = {
                //                                  On: "svg clipPath g",
                //                                  Handler: function() { alert("here 2!"); }
                //                                }
                //

                var $parent = arguments.$Parent;
                var children = arguments.$SvgData.children();
                var observerTree = (typeof(arguments.ObserverTree) != "undefined" ? arguments.ObserverTree : [ ]);
                var flatObservers = (typeof(arguments.Observers) != "undefined" ? arguments.Observers : [ ]);
                var currentPath = (typeof(arguments.CurrentPath) != "undefined" ? arguments.CurrentPath : null);
  
                var i = 0;
                var $lastAddedNode = null;

                while(i < children.length)
                {
                    var $thisChild = $(children[i]);

                    // Ignore the helper functions added by jQuery.
                    if(typeof(thisChild) == "function")
                    {
                        i++;
                        continue;
                    }
                    
                    var tagName = $thisChild.get(0).tagName;

                    // If there's no tag, than we don't care about it. Comments will end up 
                    // here.
                    if(typeof(tagName) == "undefined")
                    {
                        i++;
                        continue;
                    }
                    
                    var newChildNode = document.createElementNS(SvgLoader.ConfNs, tagName);
                    var $newChildNode = $(newChildNode);

                    var attributes = $thisChild[0].attributes;

                    var j = 0;
                    while(j < attributes.length)
                    {
                        var newAttribute = attributes[j];

                        if(newAttribute.name.substr(0, 5) == "xmlns")
                        {
                            if(newAttribute.name == "xmlns" && arguments.CurrentPath == undefined)
                                nsRoot = newAttribute.nodeValue;
                        
                            else if(newAttribute.name.length > 6)
                                ns[newAttribute.name.substr(6)] = newAttribute.nodeValue;
                        }

                        newChildNode.setAttribute(newAttribute.name, newAttribute.nodeValue);

                        j++;
                    }
					
					if($parent[0].tagName == 'svg')
					{
						$parent.attr({version:'1.1',viewBox:'0 0 '+options.Size.width*3+' '+options.Size.height*3,
										width:'100%',height:'100%',preserveAspectRatio:'xMidYMid meet'});						
					}

                    if($($thisChild[0]).children().length == 0)
                        $newChildNode.text($($thisChild[0]).text());
                    
                    var childPath = (currentPath != null ? (currentPath + " " + tagName) : tagName);
                    
                    SvgLoader.LoadDataIntoContainer({ 
                                                        $Parent: $newChildNode, 
                                                        $SvgData: $thisChild, 
                                                        ObserverTree: $.extend({ }, observerTree[tagName]), 
                                                        Observers: flatObservers,
                                                        CurrentPath: childPath
                                                    });
                    
                    $parent.append($newChildNode);
                    
                    // The observer contains an object who properties and properties of 
                    // properties describe the various levels of observers that are to be 
                    // triggered at those specific points in the SVG tree. An element named 
                    // "_" contains the actual description.
                    if(typeof(observerTree[tagName]) != "undefined" && typeof(observerTree[tagName]["_"]) != "undefined")
                        observerTree[tagName]["_"](newChildNode, observerTree);

                    // There is an observer for this particular ID.
                    var childId = $newChildNode.attr("id");
                    if(childId != null && typeof(observerTree["#" + childId]) != "undefined")
                        observerTree["#" + childId](newChildNode, observerTree);
                    
                    for(var j in flatObservers)
                    {
                        // Each is expected to be an object with at least keys "On" 
                        // and "Handler".
                    
                        var thisObserver = flatObservers[j];

                        if(childPath == thisObserver.On || childId != null && ("#" + childId) == thisObserver.On)
                            thisObserver.Handler(newChildNode, thisObserver);
                    }

                    $lastAddedNode = $newChildNode;
                    
                    i++;
                }

                // The very top-most call will return the $ object for the DOM tree of the 
                // loaded SVG data.    
                return $lastAddedNode;
            }

            var SvgLabeler = function(arguments)
            {

                // Arguments array:
                //
                // Selector:            (req) readonly string
                // TypeIdent:           (req) readonly string
                // LabelAttribute:      (req) readonly string
                // LabelClass:          (req) readonly string
                //
                // This call adds popup labels to the SVG.

                var labelAttribute = arguments.LabelAttribute;
                var typeIdent = arguments.TypeIdent;

                if(typeof(SvgLabeler.labels) == "undefined")
                    SvgLabeler.labels = { }

                if(typeof(SvgLabeler.labels[typeIdent]) == "undefined")
                {
                    var $newLabel = $('<div />').addClass(arguments.LabelClass).appendTo($('body'));
                
                    SvgLabeler.labels[typeIdent] = {
                                                    Instance:   $newLabel,
                                                    Width:      null,
                                                    Height:     null
                                                }
                }
                
                var $activeObjects = $this.find(arguments.Selector);

                $activeObjects.live("mouseover", function(e) {
                        var hoverText = e.target.getAttribute(labelAttribute);

                        var labelInfo = SvgLabeler.labels[typeIdent];

                        labelInfo.Instance.text(hoverText);

                        labelInfo.Instance.show();

                        labelInfo.Width = labelInfo.Instance.width();
                        labelInfo.Height = labelInfo.Instance.height();
                    });

                $activeObjects.live("mouseout", function(e) {
                        var labelInfo = SvgLabeler.labels[typeIdent];

                        labelInfo.Instance.hide();
                    });

                $activeObjects.live("mousemove", function(e) {
                        var labelInfo = SvgLabeler.labels[typeIdent];

			            if (labelInfo.Instance.is(':visible')) {
				            labelInfo.Instance.css({
		                			left: e.pageX - 15 - labelInfo.Width,
		                			top: e.pageY - 15 - labelInfo.Height
		                		});
			            }
		            });

            }

            var SvgHighlighting = function(arguments)
            {

                // Arguments array:
                //
                // Selector:            (req) readonly string
                // FillColor:           (opt) readonly string
                // ParentRefAttribute:  (opt) readonly string
                // ParentFillColor:     (opt) readonly string
                //

                if(typeof(arguments.FillColor) == "undefined")
                    arguments.FillColor = "black";

                if(typeof(arguments.ParentFillColor) == "undefined")
                    arguments.ParentFillColor = "black";

                var fillColor = arguments.FillColor;
                var parentRefAttribute = arguments.ParentRefAttribute;
                var parentFillColor = arguments.ParentFillColor;

                //>>>> Highlight counties.

                var $activeObjects = $this.find(arguments.Selector);

                // These are "live" calls because the SVG data may not be 
                // apprehended quickly enough by the browser for us to be able 
                // to hook into it.

                $activeObjects.live("mouseover", function(e) {
                        SvgHighlighting.originalStoreColor = e.target.style.fill;
                        e.target.style.fill = fillColor;

                        if(typeof(parentFillColor) != "undefined" && typeof(parentRefAttribute) != "undefined")
                        {
                            var parentId = e.target.getAttribute(parentRefAttribute);
                            var parent;

                            if(typeof(parentId) != "undefined" && (parent = document.getElementById(parentId)) != null)
                            {
                                SvgHighlighting.originalParentColor = parent.style.fill;
                                parent.style.fill = parentFillColor;
                            }
                        }
                    });

                $activeObjects.live("mouseout", function(e) {
                        e.target.style.fill = SvgHighlighting.originalStoreColor;
                        SvgHighlighting.originalStoreColor = null;

                        if(typeof(parentFillColor) != "undefined" 
                                && SvgHighlighting.originalParentColor != null
                                && typeof(parentRefAttribute) != "undefined"
                            )
                        {
                            var parentId = e.target.getAttribute(parentRefAttribute);
                            var parent;

                            if(typeof(parentId) != "undefined" && (parent = document.getElementById(parentId)) != null)
                            {
                                parent.style.fill = SvgHighlighting.originalParentColor;
                                SvgHighlighting.originalParentColor = null;
                            }
                        }
                    });

                //<<<<
            }

            SvgHighlighting.originalStoreColor = null;
            SvgHighlighting.originalParentColor = null;

            function ProcessOptions(arguments)
            {
                // Arguments array:
                //
                // DefaultParams:   (req) readwrite object
                // RequiredOptions: (req) readonly array
                // GivenOptions:    (req) readonly object
                //

                for(var i in arguments.RequiredOptions)
                    if(typeof(arguments.RequiredOptions[i]) != "function" && typeof(arguments.GivenOptions[arguments.RequiredOptions[i]]) == "undefined")
                        throw ("Please provide option \"" + arguments.RequiredOptions[i] + "\".");

                $.extend(arguments.DefaultParams, arguments.GivenOptions);
            }

            if(options == "AddHighlighting")
                SvgHighlighting(options2);

            else if(options == "AddLabels")
                SvgLabeler(options2);
            
            else
            {
                // Initialization.
            
                var defaultParams = { 
                                        DataIsUrl: false
                                    }

                ProcessOptions({
                                DefaultParams: defaultParams,

                                RequiredOptions: [
                                                    "Data"
                                                ],
                                                
                                GivenOptions: options
                            });

                var observerTree = defaultParams.ObserverTree;
                var observers = defaultParams.Observers;

                // If we have to first load the data through AJAX, "this" will no 
                // longer refer to the right object.
                var $this = this;

                function LoadSvgFileInternal(xmlData)
                {
					var $data = $(xmlData);
					
					if(!$divider_caches.ContainsKey(options.Data))
					{
						$divider_caches.Add(options.Data,$data);
					}                	
					
                    SvgLoader.LoadDataIntoContainer({ 
                                                        $Parent:        $this, 
                                                        $SvgData:       $data, 
                                                        ObserverTree:   observerTree,
                                                        Observers:      observers
                                                    });
													
				    defaultParams.Callback.apply(this,options.Target);
                
                }

                if(options.DataIsUrl){
					if(!$divider_caches.ContainsKey(options.Data))
						$.get(defaultParams.Data, LoadSvgFileInternal, "xml");
					else{
						SvgLoader.
								LoadDataIntoContainer({ 
                                                        $Parent:        $this, 
                                                        $SvgData:       $divider_caches.GetValue(defaultParams.Data), 
                                                        ObserverTree:   observerTree,
                                                        Observers:      observers
                                                    });
													
								defaultParams.Callback.apply(this,options.Target);
					}
				}
                else
					SvgLoader.
								LoadDataIntoContainer({ 
                                                        $Parent:        $this, 
                                                        $SvgData:       $(defaultParams.Data), 
                                                        ObserverTree:   observerTree,
                                                        Observers:      observers
                                                    });
								defaultParams.Callback.apply(this,options.Target);
                    
            }
        }
})(jQuery);

