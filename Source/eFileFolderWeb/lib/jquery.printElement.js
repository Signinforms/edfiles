(function ($) {
    var printAreaCount = 0;
    $.fn.printArea = function () {
        var ele = $(this);
        //var idPrefix = "printArea_";
        //removePrintArea(idPrefix + printAreaCount);
        //printAreaCount++;
        //var iframeId = idPrefix + printAreaCount;
        //var iframeStyle = 'position:absolute;width:0px;height:0px;left:-500px;top:-500px;';
        //iframe = document.createElement('IFRAME');
        //$(iframe).attr({
        //    style: iframeStyle,
        //    id: iframeId
        //});
        //document.body.appendChild(iframe);
        //var doc = iframe.contentWindow.document;
        //$(document).find("link")
        //    .filter(function() {
        //        return $(this).attr("rel").toLowerCase() == "stylesheet";
        //    })
        //    .each(function() {
        //        doc.write('<link type="text/css" rel="stylesheet" href="' +
        //            $(this).attr("href") + '" >');
        //    });
        //doc.write('<div id="printDivContainer" class="' + $(ele).attr("class") + '">' + '</div>');
        var canvas = $(ele).find("canvas");
        var printWin = window.open("", "popupWindow", "width=1200, height=1000, scrollbars=no");
        var dataUrl = $(ele).find("canvas")[0].toDataURL();
        var windowContent = '<!DOCTYPE html>';
        windowContent += '<html>'
        windowContent += '<head><title></title></head>';
        windowContent += '<body>'
        windowContent += '<img src="' + dataUrl + '" style="' + canvas.attr('style') + '">';
        windowContent += '</body>';
        windowContent += '</html>';

        //printWin.document.open();
        printWin.document.write(windowContent);
        //if (canvas) {
        setTimeout(function () {
            printWin.document.close();
            printWin.focus();
            printWin.print();
            printWin.close();
        }, 0);
        //}

        //doc.close();

        //var frameWindow = iframe.contentWindow;
        ////var frameWindow = canvas;
        //frameWindow.close();
        //frameWindow.focus();
        //frameWindow.print();
        return false;
    };
    var removePrintArea = function (id) {
        $("iframe#" + id).remove();
    };
})(jQuery);