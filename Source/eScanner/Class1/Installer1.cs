﻿using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace Class1
{
    [RunInstaller(true)]
    public partial class Installer1 : System.Configuration.Install.Installer
    {
        public Installer1()
        {
            InitializeComponent();
        }

        public override void Commit(System.Collections.IDictionary savedState)
        {
            //base.Commit(savedState);
            //System.Diagnostics.Process.Start(Context.Parameters["TARGETDIR"].ToString() + "application.exe");
            //// Very important! Removes all those nasty temp files.
            //base.Dispose();

            System.Diagnostics.EventLog.WriteEntry("InstallerClass", "Installerclass Invoked By Kavit");
            try
            {
                if (Environment.Is64BitOperatingSystem)//IntPtr.Size == 4)
                {
                    RegistryKey key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Wow6432Node\PFU\ScanSnap Extension\Scan2Edf", true);

                    if (key != null)
                    {
                        key.DeleteSubKey(@"SOFTWARE\Wow6432Node\PFU\ScanSnap Extension\Scan2Edf");

                        key = Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Wow6432Node\PFU\ScanSnap Extension\Scan2Edf");
                        key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Wow6432Node\PFU\ScanSnap Extension\Scan2Edf", true);
                        key.SetValue("", @"C:\Program Files (x86)\EdFiles\Scan2Edf\Scan2Edf.exe");
                        key.SetValue("Config", @"C:\Program Files (x86)\EdFiles\Scan2Edf\Scan2Edf.ini");
                        key.SetValue("Path", @"C:\Program Files (x86)\EdFiles\Scan2Edf");
                        // Key doesn't exist. Do whatever you want to handle
                        // this case
                    }
                    else
                    {
                        key = Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Wow6432Node\PFU\ScanSnap Extension\Scan2Edf");
                        key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Wow6432Node\PFU\ScanSnap Extension\Scan2Edf", true);
                        key.SetValue("", @"C:\Program Files (x86)\EdFiles\Scan2Edf\Scan2Edf.exe");
                        key.SetValue("Config", @"C:\Program Files (x86)\EdFiles\Scan2Edf\Scan2Edf.ini");
                        key.SetValue("Path", @"C:\Program Files (x86)\EdFiles\Scan2Edf");
                    }
                    key.Close();

                    // 64-bit
                    //System.Diagnostics.EventLog.WriteEntry("InstallerClass", "Installerclass Invoked By 64 bit");
                    //RegistryKey key = Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Wow6432Node\PFU\ScanSnap Extension\Scan2Edf");
                    //key.SetValue("", @"C:\Program Files (x86)\EdFiles\Scan2Edf\Scan2Edf.exe");
                    //key.SetValue("Path", @"C:\Program Files (x86)\EdFiles\Scan2Edf");
                    //key.SetValue("Config", @"C:\Program Files (x86)\EdFiles\Scan2Edf\Scan2Edf.ini");
                    //key.Close();

                }
                else
                {
                    // 32-bit 
                    System.Diagnostics.EventLog.WriteEntry("InstallerClass", "Installerclass Invoked By 32 bit" + IntPtr.Size.ToString());
                    RegistryKey key = Registry.CurrentUser.CreateSubKey(@"\SOFTWARE\PFU\ScanSnap Extension\Scan2Edf");
                    key.SetValue("", @"C:\Program Files\EdFiles\Scan2Edf\Scan2Edf.exe");
                    key.SetValue("Path", @"C:\Program Files\EdFiles\Scan2Edf");
                    key.SetValue("Config", @"C:\Program Files\EdFiles\Scan2Edf\Scan2Edf.ini");
                    //key.Close();



                }
            }
            catch (Exception exc)
            {
                System.Diagnostics.EventLog.WriteEntry("InstallerClassError", exc.Message);
            }
        }
    }
}
