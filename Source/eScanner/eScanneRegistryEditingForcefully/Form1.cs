﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;

namespace eScanneRegistryEditingForcefully
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            // New Code
        }

        /// <summary>
        /// Handles the Click event of the button1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button1_Click(object sender, EventArgs e)
        {
            System.Diagnostics.EventLog.WriteEntry("InstallerClass", "Installerclass Invoked By Kavit");
            try
            {
                if (Environment.Is64BitOperatingSystem)//IntPtr.Size == 4)
                {
                    // 64-bit
                    System.Diagnostics.EventLog.WriteEntry("InstallerClass", "Installerclass Invoked By 64 bit");

                    RegistryKey key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Wow6432Node\PFU\ScanSnap Extension\Scan2Edf", true);

                    if (key != null)
                    {
                        label1.Text = "1> Added Scan2Edf.exe Registry Successfully";



                        key.DeleteSubKey(@"SOFTWARE\Wow6432Node\PFU\ScanSnap Extension\Scan2Edf");

                        key = Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Wow6432Node\PFU\ScanSnap Extension\Scan2Edf");
                        key.SetValue("", @"C:\Program Files (x86)\EdFiles\Scan2Edf\Scan2Edf.exe");
                        key.SetValue("Config", @"C:\Program Files (x86)\EdFiles\Scan2Edf\Scan2Edf.ini");
                        key.SetValue("Path", @"C:\Program Files (x86)\EdFiles\Scan2Edf");
                        // Key doesn't exist. Do whatever you want to handle
                        // this case
                    }
                    else
                    {
                        key = Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Wow6432Node\PFU\ScanSnap Extension\Scan2Edf");
                        key.SetValue("", @"C:\Program Files (x86)\EdFiles\Scan2Edf\Scan2Edf.exe");
                        key.SetValue("Config", @"C:\Program Files (x86)\EdFiles\Scan2Edf\Scan2Edf.ini");
                        key.SetValue("Path", @"C:\Program Files (x86)\EdFiles\Scan2Edf");
                    }
                    key.Close();

                    //label1.Text = "1> Added Scan2Edf.exe Registry Successfully";
                    //key.SetValue("", @"C:\Program Files (x86)\EdFiles\Scan2Edf\Scan2Edf.exe");
                    //key.SetValue("Path", @"C:\Program Files (x86)\EdFiles\Scan2Edf");
                    //key.SetValue("Config", @"C:\Program Files (x86)\EdFiles\Scan2Edf\Scan2Edf.ini");


                    //if (Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\PFU\ScanSnap Extension\Scan2Edf", "", null) == null)
                    //{
                    //    label1.Text = "1> Added Scan2Edf.exe Registry Successfully";
                    //}
                    //else 
                    //{
                    //    label1.Text = "1> Registry Scan2Edf.exe exists";
                    //}
                    //if (Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\PFU\ScanSnap Extension\Scan2Edf", "Path", null) == null)
                    //{
                    //    label2.Text = "2> Added Scan2Edf Registry Successfully";
                    //}
                    //else
                    //{
                    //    label2.Text = "2> Registry Scan2Edf exists";
                    //}
                    //if (Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\PFU\ScanSnap Extension\Scan2Edf", "Config", null) == null)
                    //{
                    //    label3.Text = "3> Added Scan2Edf.ini Registry Successfully";
                    //}
                    //else
                    //{
                    //    label3.Text = "3> Registry Scan2Edf.ini exists";
                    //}
                    label4.Text = "4> Added Registry Successfully";
                    //key.Close();
                }
                else
                {
                    // 32-bit 
                    System.Diagnostics.EventLog.WriteEntry("InstallerClass", "Installerclass Invoked By 32 bit" + IntPtr.Size.ToString());
                    RegistryKey key = Registry.CurrentUser.CreateSubKey(@"\SOFTWARE\PFU\ScanSnap Extension\Scan2Edf");
                    if (Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\PFU\ScanSnap Extension\Scan2Edf", "", null) == null)
                    {
                        key.SetValue("", @"C:\Program Files\EdFiles\Scan2Edf\Scan2Edf.exe");
                        label1.Text = "1> Added Scan2Edf.exe Registry Successfully";
                    }
                    else
                    {
                        label1.Text = "1> Registry Scan2Edf.exe exists";
                    }
                    if (Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\PFU\ScanSnap Extension\Scan2Edf", "Path", null) == null)
                    {
                        key.SetValue("Path", @"C:\Program Files\EdFiles\Scan2Edf");
                        label2.Text = "2> Added Scan2Edf Registry Successfully";
                    }
                    else
                    {
                        label2.Text = "2> Registry Scan2Edf exists";
                    }
                    if (Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\PFU\ScanSnap Extension\Scan2Edf", "Config", null) == null)
                    {
                        key.SetValue("Config", @"C:\Program Files\EdFiles\Scan2Edf\Scan2Edf.ini");
                        label3.Text = "3> Added Scan2Edf.ini Registry Successfully";
                    }
                    else
                    {
                        label3.Text = "3> Registry Scan2Edf.ini exists";
                    }
                    label4.Text = "4> Added Registry Successfully";
                    //key.Close();
                }
            }
            catch (Exception exc)
            {
                label1.Text = exc.Message;
                label2.Text = "";
                label3.Text = "";
                label4.Text = "";
                System.Diagnostics.EventLog.WriteEntry("InstallerClassError", exc.Message);
            }
        }
    }
}
