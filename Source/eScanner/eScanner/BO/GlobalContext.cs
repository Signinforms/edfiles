﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eScanner.BO
{
    public static class GlobalContext
    {
        private static string _userId;
        private static string _userName;
        private static string _password;

        public static string UserId
        {
            get 
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }

        public static string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
            }
        }

        public static string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
            }
        }
    }
}
