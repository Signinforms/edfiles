﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace eScanner.CustomControl
{
    public class ImageButton : Button
    {

        #region Fields

        /// <summary>
        /// The image source property
        /// </summary>
        public static DependencyProperty ImageSourceProperty;
        /// <summary>
        /// The image width property
        /// </summary>
        public static DependencyProperty ImageWidthProperty;
        /// <summary>
        /// The image height property
        /// </summary>
        public static DependencyProperty ImageHeightProperty;

        #endregion

        #region Constructor

        /// <summary>
        /// Initializes static members of the <see cref="ImageButton"/> class.
        /// </summary>
        static ImageButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ImageButton), new FrameworkPropertyMetadata(typeof(ImageButton)));

            ImageSourceProperty = DependencyProperty.Register("ImageSource", typeof(ImageSource), typeof(ImageButton), new UIPropertyMetadata(null));
            ImageWidthProperty = DependencyProperty.Register("ImageWidth", typeof(int), typeof(ImageButton), new UIPropertyMetadata(null));
            ImageHeightProperty = DependencyProperty.Register("ImageHeight", typeof(int), typeof(ImageButton), new UIPropertyMetadata(null));

        }
        #endregion

        #region Custom Control Properties

        /// <summary>
        /// Gets or sets the image source.
        /// </summary>
        /// <value>The image source.</value>
        [Description("The image represented by the button"), Category("Common Properties")]
        public ImageSource ImageSource
        {
            get { return (ImageSource)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }

        /// <summary>
        /// Gets or sets the width of the image.
        /// </summary>
        /// <value>The width of the image.</value>
        [Description("The represented by the image width"), Category("Common Properties")]
        public int ImageWidth
        {
            get { return (int)GetValue(ImageWidthProperty); }
            set { SetValue(ImageWidthProperty, value); }
        }

        /// <summary>
        /// Gets or sets the height of the image.
        /// </summary>
        /// <value>The height of the image.</value>
        [Description("The represented by the image height"), Category("Common Properties")]
        public int ImageHeight
        {
            get { return (int)GetValue(ImageHeightProperty); }
            set { SetValue(ImageHeightProperty, value); }
        }

        #endregion

    }
}
