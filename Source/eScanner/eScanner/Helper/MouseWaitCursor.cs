﻿using System;
using System.Windows.Input;


namespace eScanner.Helper
{
    /// <summary>
    /// Class MouseWaitCursor
    /// </summary>
    public class MouseWaitCursor : IDisposable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MouseWaitCursor"/> class.
        /// </summary>
        public MouseWaitCursor()
        {
            Mouse.OverrideCursor = Cursors.Wait;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Mouse.OverrideCursor = null;
        }
    }
}
