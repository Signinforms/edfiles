﻿using eScanner.BO;
using eScanner.eScannerService;
using eScanner.Interfaces;
using eScanner.Model;
using eScanner.UserControls;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PATH = System.IO.Path;
using System.Threading;
using eScanner.Helper;
using System.Text.RegularExpressions;

namespace eScanner
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string[] args = Environment.GetCommandLineArgs();
        PDFViewer pvMain = new PDFViewer("about:blank");
        private OpenFileDialog dlgOpenFile = new OpenFileDialog();


        public const Int32 WM_COPYDATA = 0x4A;          // Value of WM_COPYDATA
        public static string TempDirectory;
        public static string ResultFilePath;
        private static Mutex mutex;

        // DllImport
        [DllImport("USER32.dll", EntryPoint = "FindWindow")]
        public static extern IntPtr FindWindow(
                                            string lpClassName,
                                            string lpWindowName);

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern Int32 SendMessage(
                                            IntPtr hWnd,
                                            UInt32 Msg,
                                            UInt32 wParam,
                                            ref COPYDATASTRUCT lParam);

        //COPYDATASTRUCT
        public struct COPYDATASTRUCT
        {
            public Int32 dwData;
            public Int32 cbData;
            public string lpData;
        }

        public clsCopyFiles objclsCopyFiles = new clsCopyFiles();


        public GeneralViewer currentGV = null;

        /// <summary>
        /// MainWindow
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += MainWindow_Loaded;
            #region Mutex related code
            // MessageBox.Show(string.Join("#",args));
            if (args.Length == 0)
            {
                ResultFilePath = null;
            }
            else
            {
                try
                {

                    ResultFilePath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures) + "\\Scan2Edf.ini";

                    WriteErrorLog("final path = " + ResultFilePath, new Exception());

                    //ResultFilePath = "C:\\Users\\pca65\\Desktop\\Scan2Eff\\Scan2Eff.ini";
                    //ResultFilePath = args[1].ToString();// "C:\\Users\\pca65\\Desktop\\Scan2Eff\\Scan2Eff.ini";
                }
                catch (Exception ex)
                {
                    WriteErrorLog(ex.Message + "1", ex);
                }

            }
            string tempPath = PATH.GetTempPath();
            TempDirectory = tempPath + "ScanToEDF";
            if (Directory.Exists(TempDirectory) == false)
            {
                try
                {
                    // create
                    Directory.CreateDirectory(TempDirectory);
                }
                catch (Exception exc)
                {
                    WriteErrorLog(exc.Message + "2", exc);
                    return;
                }
            }

            mutex = new Mutex(false, "eScanner");
            if (mutex.WaitOne(0, false) == false)
            {
                // Process exist

                if (args.Length == 0)
                {
                    return;
                }

                string windowName = "EFFTEST";                        // todo: Please update Window Title
                IntPtr hwnd = FindWindow(null, windowName);                     // Win32API    
                if (hwnd != IntPtr.Zero)
                {
                    // SendMessage COPYDATA
                    Int32 len = Encoding.Default.GetByteCount(ResultFilePath);
                    COPYDATASTRUCT cds;
                    cds.dwData = 0;
                    cds.lpData = ResultFilePath;
                    cds.cbData = len + 1;

                    SendMessage(hwnd, WM_COPYDATA, 0, ref cds);                 // Win32API
                }

                // Close Mutex
                mutex.Close();

                Environment.Exit(0);
            }

            try
            {
                //Directory.Delete(TempDirectory, true);
            }
            catch (Exception exc)
            {
                WriteErrorLog(exc.Message + "3", exc);
            }
            //mutex.Close();

            #endregion

        }

        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            objclsCopyFiles.Show();
            objclsCopyFiles.Hide();
        }

        /// <summary>
        /// Shows the control.
        /// </summary>
        /// <param name="controlToShow">The control to show.</param>
        /// <param name="needsScrolling">if set to <c>true</c> [needs scrolling].</param>
        private void ShowControl(Control controlToShow, bool needsScrolling)
        {
            if (SystemParameters.PrimaryScreenHeight < 870 || SystemParameters.PrimaryScreenWidth < 1042)
            {
                needsScrolling = true;
            }
            else
            {
                //this.MinHeight = 870;
                //this.MinWidth = 1042;
            }

            // needsScrolling = false;
            if (controlToShow.ToString().ToLower().Contains("generalviewer") || controlToShow.ToString().ToLower().Contains("pdfviewer"))
            {
                dockContainer1.Children.Clear();
                dockContainer1.Children.Add(controlToShow);
            }
            else if (controlToShow.ToString().ToLower().Contains("login"))
            {
                dockContainer.Children.Clear();
                dockContainer.Children.Add(controlToShow);
            }
            else if (controlToShow.ToString().ToLower().Contains("actionfile"))
            {
                dockContainer.Children.Clear();
                dockContainer.Children.Add(controlToShow);
            }
            this.RaiseEvent(new RoutedEventArgs(System.Windows.FrameworkElement.SizeChangedEvent));
        }

        /// <summary>
        /// Views the control.
        /// </summary>
        /// <param name="control">The control.</param>
        /// <param name="ancestor">The ancestor.</param>
        public void ViewControl(Control control, object ancestor)
        {
            try
            {
                LoadControl(control, true, null, ancestor, false);
            }
            catch (Exception exc)
            {
                WriteErrorLog(exc.Message + "4", exc);
            }
        }

        /// <summary>
        /// Loads the control.
        /// </summary>
        /// <param name="controlToLoad">The control to load.</param>
        /// <param name="isInvokedFromMenu">if set to <c>true</c> [is invoked from menu].</param>
        /// <param name="initializationData">The initialization data.</param>
        /// <param name="ancestorControl">The ancestor control.</param>
        /// <param name="needsScrolling">if set to <c>true</c> [needs scrolling].</param>
        private void LoadControl(Control controlToLoad, bool isInvokedFromMenu, object initializationData, object ancestorControl, bool needsScrolling)
        {
            if (controlToLoad is INotify)
            {
                ((INotify)controlToLoad).Notify -= new NotifyEventHandler(OnNotified);
                ((INotify)controlToLoad).Notify += new NotifyEventHandler(OnNotified);
            }

            if (controlToLoad is IInitializable)
            {
                IInitializable initializableControl = (IInitializable)controlToLoad;
                if (ancestorControl != null)
                    initializableControl.AncestorControl = (Control)ancestorControl;
                if (initializationData != null)
                    initializableControl.Initialize(initializationData);
            }
            //Todo: consider change later on
            if (!isInvokedFromMenu)
            {
                if (controlToLoad is IRefreshable)
                    ((IRefreshable)controlToLoad).Refresh();
            }

            ShowControl(controlToLoad, needsScrolling);
        }

        /// <summary>
        /// Called when [notified].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="targetControl">The target control.</param>
        /// <param name="initializationData">The initialization data.</param>
        private void OnNotified(object sender, Type targetControl, object initializationData)
        {
            try
            {
                Control controlToLoad = null;
                if (targetControl != null)
                {
                    controlToLoad = (Control)Activator.CreateInstance(targetControl);
                }
                else if (sender is IInitializable)
                {
                    controlToLoad = ((IInitializable)sender).AncestorControl; //ParentControl;
                    if (controlToLoad is IInitializable)
                    {
                        initializationData = null;
                        sender = null;
                    }
                }

                LoadControl(controlToLoad, false, initializationData, sender, false);
            }
            catch (Exception exc)
            {
                WriteErrorLog(exc.Message + "5", exc);
            }
        }

        /// <summary>
        /// Tabcontrol Selection Change Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TabControl_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            switch (((sender as TabControl).SelectedItem as TabItem).Name)
            {
                case "tabAction":
                    ViewControl(new ActionFile(), null);
                    break;

                case "tabLogin":
                    tabAction.IsEnabled = false;
                    btnWorkArea.IsEnabled = false;
                    btnEfileFolder.IsEnabled = false;
                    btnWorkArea.Opacity = 0.5;
                    btnEfileFolder.Opacity = 0.5;
                    ViewControl(new Login(), null);
                    break;
            }
        }

        /// <summary>
        /// Tabcontrol Selection Change Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TabControl_SelectionChanged_2(object sender, SelectionChangedEventArgs e)
        {
            switch (((sender as TabControl).SelectedItem as TabItem).Name)
            {
                case "tabGeneral":
                    pvMain.pdfWebViewer.Dispose();
                    if (currentGV == null)
                        currentGV = new GeneralViewer();
                    ViewControl(currentGV, null);
                    break;

                case "tabViewer":
                    if (tabViewer.Tag != null)
                    {
                        pvMain.pdfWebViewer.Dispose();
                        pvMain = new PDFViewer(tabViewer.Tag.ToString());
                        ViewControl(pvMain, null);
                        tabViewer.Tag = null;
                    }
                    else
                        ViewControl(new PDFViewer(), null);
                    break;
            }
        }

        /// <summary>
        /// Edit Button Click Event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            GeneralViewer gv = dockContainer1.Children.OfType<GeneralViewer>().FirstOrDefault();
            eScanner.Model.FileItem item = ((gv.DataContext as eScanner.Model.FileList).SelectedItem as eScanner.Model.FileItem);
            try
            {
                if (item.FileName != null)
                {
                    string tempPath = PATH.GetTempPath();
                    //TempDirectory = ConfigurationManager.AppSettings["FileBasePath"];// +"ScanSnapSample";
                    string oldFile = TempDirectory + "\\" + item.FileName;
                    string oldName = item.FileName;
                    EditViewer ev = new EditViewer(item);
                    Application.Current.MainWindow.Opacity = 0.2;
                    if (ev.ShowDialog() == true)
                    {
                        if ((item.FileName.Length) > 1)
                        {
                            item.FileName = RemoveInvalidFilePathCharacters(item.FileName, "");

                            if (!item.FileName.ToLower().Contains(".pdf"))
                            {
                                item.FileName = item.FileName + ".pdf";
                            }
                            string newFile = TempDirectory + "\\" + item.FileName;

                            if (File.Exists(newFile) && newFile != oldFile)
                            {
                                File.Delete(newFile);
                            }
                            File.Move(oldFile, newFile);
                        }
                        else
                        {
                            item.FileName = oldName;
                            MessageBox.Show("Invalid filename");
                        }

                    }

                    Application.Current.MainWindow.Opacity = 1;
                }
            }
            catch (Exception exc)
            {
                WriteErrorLog(exc.Message + "6", exc);
                MessageBox.Show("Invalid filename" + exc.ToString());
            }

        }

        /// <summary>
        /// Remove Invalid Characters From FileName.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="replaceChar"></param>
        /// <returns></returns>
        public static string RemoveInvalidFilePathCharacters(string filename, string replaceChar)
        {
            string regexSearch = new string(System.IO.Path.GetInvalidFileNameChars()) + new string(System.IO.Path.GetInvalidPathChars());
            Regex r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            return r.Replace(filename, replaceChar);
        }

        /// <summary>
        /// btnExit Click Event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.MainWindow.Close();
        }

        /// <summary>
        /// btnViewer Click Event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnViewer_Click(object sender, RoutedEventArgs e)
        {
            GeneralViewer gv = dockContainer1.Children.OfType<GeneralViewer>().FirstOrDefault();
            PDFViewer pv = dockContainer1.Children.OfType<PDFViewer>().FirstOrDefault();
            if (pv == null)
            {
                eScanner.Model.FileItem item = ((gv.DataContext as eScanner.Model.FileList).SelectedItem as eScanner.Model.FileItem);
                if (item.FileName != null)
                {
                    string path = item.FilePath.ToString() + "//" + item.FileName.ToString();
                    (Application.Current.MainWindow as MainWindow).tabViewer.Tag = path;
                    (Application.Current.MainWindow as MainWindow).tabViewer.IsSelected = true;
                }
            }

        }

        /// <summary>
        /// btnViewer Click Event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnWorkArea_Click(object sender, RoutedEventArgs e)
        {
            GeneralViewer gv = dockContainer1.Children.OfType<GeneralViewer>().FirstOrDefault();
            PDFViewer pv = dockContainer1.Children.OfType<PDFViewer>().FirstOrDefault();

            if (gv != null)
            {
                eScanner.Model.FileItem item = ((gv.DataContext as eScanner.Model.FileList).SelectedItem as eScanner.Model.FileItem);
                try
                {
                    if (item.FileName != null)
                    {
                        string tempPath = PATH.GetTempPath();
                        //TempDirectory = ConfigurationManager.AppSettings["FileBasePath"];// +"ScanSnapSample";
                        string pathName = item.FilePath;
                        string fileName = item.FileName;
                        int fileExtention = 1;   // For *.PDF
                        byte[] fileData = File.ReadAllBytes(TempDirectory + "\\" + fileName);

                        int fileNameidx = item.FileName.LastIndexOf(".");
                        string fileNameToDisplay = item.FileName.Substring(0, fileNameidx); // to prevemt name like xyz.pdf.PDF

                        eScannerService.EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();
                        AuthenticationInfo obj2 = new AuthenticationInfo();
                        obj2.username = GlobalContext.UserName;
                        obj2.password = GlobalContext.Password;

                        using (MouseWaitCursor cursor = new MouseWaitCursor())
                        {
                            int id = obj.MoveDocumentsToWorkAreaFolder(obj2, pathName, fileNameToDisplay, fileExtention, fileData, GlobalContext.UserId);

                            if (id == 1)
                            {
                                string localFile = TempDirectory + "\\" + item.FileName;
                                pvMain.pdfWebViewer.Dispose();
                                File.Delete(localFile);
                                gv.DataContext = new FileList();
                                MessageBox.Show("File has been uploaded to WorkArea");
                            }
                            else
                                MessageBox.Show("No Such File exist!");
                        }

                    }
                }
                catch (Exception exc)
                {
                    WriteErrorLog(exc.Message + "7", exc);
                    MessageBox.Show(exc.Message, "ToWorkArea");
                }
            }
            else
                MessageBox.Show("Please select one document to upload !!!");
        }

        /// <summary>
        /// btnEFileFolder Click Event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEfileFolder_Click(object sender, RoutedEventArgs e)
        {
            ActionFile av = dockContainer.Children.OfType<ActionFile>().FirstOrDefault();
            eScanner.Model.FolderDetails folderDetails = ((av.DataContext as eScanner.Model.ActionFileViewModel).SelectedFolder as eScanner.Model.FolderDetails);
            eScanner.Model.DividerDetails dividerDetails = ((av.DataContext as eScanner.Model.ActionFileViewModel).SelectedDivider as eScanner.Model.DividerDetails);
            try
            {
                if (folderDetails != null && dividerDetails != null)
                {
                    if (folderDetails.ID.ToString() != "" && dividerDetails.ID.ToString() != "")
                    {
                        GeneralViewer gv = dockContainer1.Children.OfType<GeneralViewer>().FirstOrDefault();
                        if (gv != null)
                        {
                            eScanner.Model.FileItem item = ((gv.DataContext as eScanner.Model.FileList).SelectedItem as eScanner.Model.FileItem);
                            if (item.FileName != null)
                            {
                                string tempPath = PATH.GetTempPath();
                                //TempDirectory = ConfigurationManager.AppSettings["FileBasePath"];// +"ScanSnapSample";
                                string pathName = item.FilePath;
                                string fileName = item.FileName;

                                int fileNameidx = item.FileName.LastIndexOf(".");
                                string fileNameToDisplay = item.FileName.Substring(0, fileNameidx); // to prevemt name like xyz.pdf.PDF

                                int fileExtention = 1;   // For *.PDF

                                byte[] fileData = File.ReadAllBytes(TempDirectory + "\\" + fileName);

                                eScannerService.EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();
                                AuthenticationInfo obj2 = new AuthenticationInfo();
                                obj2.username = GlobalContext.UserName;
                                obj2.password = GlobalContext.Password;
                                string error = "";

                                using (MouseWaitCursor cursor = new MouseWaitCursor())
                                {
                                    int id = obj.MoveDocumentsToAnotherFolder(obj2, pathName, fileNameToDisplay, fileExtention, folderDetails.ID.ToString(), dividerDetails.ID.ToString(), fileData, GlobalContext.UserId, item.Alert1, item.Alert2);
                                    if (id == 1)
                                    {
                                        string localFile = TempDirectory + "\\" + item.FileName;
                                        pvMain.pdfWebViewer.Dispose();
                                        File.Delete(localFile);
                                        gv.DataContext = new FileList();
                                        av.cmbDivider.SelectedItem = av.cmbDivider.SelectedItem;                 // To refresh rightside grid
                                        MessageBox.Show("File has been uploaded to EdFiles");
                                    }
                                    else
                                        MessageBox.Show("No Such File exist!");
                                }

                            }
                        }
                        else
                            MessageBox.Show("Please select one document to upload !!!");
                    }
                    else
                    {
                        if (folderDetails.ID.ToString() == "")
                            MessageBox.Show("Please select folder");
                        if (dividerDetails.ID.ToString() == "")
                            MessageBox.Show("Please select divider");
                    }

                }
                else
                {
                    MessageBox.Show("Please select folder/divider");
                }
            }
            catch (Exception exc)
            {
                WriteErrorLog(exc.Message + "8", exc);
                MessageBox.Show(exc.ToString());
            }
        }

        private void ScanToEFF_Closed(object sender, EventArgs e)
        {
            try
            {
                Directory.Delete(TempDirectory, true);
            }
            catch (Exception exc)
            {
                WriteErrorLog(exc.Message + "9", exc);
            }
        }

        private void btnOpen_Click(object sender, RoutedEventArgs e)
        {
            GeneralViewer gv = dockContainer1.Children.OfType<GeneralViewer>().FirstOrDefault();
            dlgOpenFile.Multiselect = true;
            dlgOpenFile.Filter = "All Files (*.PDF)|*.PDF";
            string dataFileName;
            string destFilename = "";
            string[] arrFileNames = new string[] { };
            try
            {
                if (dlgOpenFile.ShowDialog() == true)
                {
                    dataFileName = dlgOpenFile.FileName;
                    arrFileNames = dlgOpenFile.FileNames;
                    destFilename = TempDirectory + @"\"; // +dataFileName;
                    if (arrFileNames != null && destFilename != string.Empty)
                    {
                        foreach (var item in arrFileNames)
                        {
                            int idx = item.LastIndexOf(@"\");
                            File.Copy(item, destFilename + item.Substring(idx + 1), true);
                        }
                    }
                    gv.DataContext = new FileList();
                }

            }
            catch (Exception exc)
            {
                WriteErrorLog(exc.Message + "10", exc);
                MessageBox.Show(exc.ToString());
            }
        }

        public static void WriteErrorLog(string message, Exception ex)
        {
            try
            {
                if (!Directory.Exists(PATH.GetTempPath() + "eff"))
                    Directory.CreateDirectory(PATH.GetTempPath() + "eff");

                StreamWriter swData2 = new StreamWriter(PATH.GetTempPath() + "eff" + "\\ErrorLog.txt", true);
                swData2.WriteLine("=====Unhandled Exception occured at  : " + DateTime.Now);
                swData2.WriteLine("Message = " + message);
                swData2.WriteLine("Error = " + ex.Message);
                swData2.WriteLine(ex.StackTrace);
                swData2.WriteLine(ex.Source);
                swData2.WriteLine(ex.InnerException);
                swData2.WriteLine("====");
                swData2.Close();
                swData2.Dispose();
            }
            catch (Exception ex1)
            {
                string errorMessage = ex1.Message;
            }
        }
    }
}
