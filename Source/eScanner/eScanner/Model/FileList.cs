﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using PATH = System.IO.Path;
using eScanner.UserControls;

namespace eScanner.Model
{
    public class FileList : BaseNotifyPropertyChanged
    {
        public static string FileDirectory;

        #region Constructor
        /// <summary>
        /// File List.
        /// </summary>
        public FileList()
        {
            GetDetails();

        }
        #endregion

        #region Methods
        /// <summary>
        /// The GetDetails.
        /// </summary>
        public void GetDetails()
        {
            try
            {
                WriteErrorLog("Entered in GetDetail", new Exception());
                (App.Current.MainWindow as MainWindow).objclsCopyFiles.startCopy();
                //clsCopyFiles obj = new clsCopyFiles(); 

                string tempPath = PATH.GetTempPath();
                //FileDirectory = ConfigurationManager.AppSettings["FileBasePath"];
                //if (Directory.Exists(FileDirectory) == false)
                //{
                //    try
                //    {
                //        // create
                //        Directory.CreateDirectory(FileDirectory);
                //    }
                //    catch (Exception ex)
                //    {
                //        WriteErrorLog(ex.Message, new Exception());
                //        return;
                //    }
                //}
                string filePath = "";
                string fileName = "";
                List<FileItem> lstFile = new List<FileItem>();
                WriteErrorLog(eScanner.MainWindow.TempDirectory, new Exception());
                foreach (string txtName in Directory.GetFiles(eScanner.MainWindow.TempDirectory, "*.pdf"))
                {
                    WriteErrorLog(txtName, new Exception());
                    int idx = txtName.LastIndexOf(@"\");
                    filePath = txtName.Substring(0, idx).Replace(@"\", "//");
                    fileName = txtName.Substring(idx + 1);
                    lstFile.Add(new FileItem(fileName, "", "", "", "", DateTime.Today.ToString("MM.dd.yyyy"), "", filePath));
                }
                WriteErrorLog("getfilesComplete" + lstFile.Count, new Exception());
                this.Items = lstFile;
            }
            catch (Exception ex)
            {
                WriteErrorLog(ex.Message, new Exception());
            }
        }
        #endregion

        public static void WriteErrorLog(string message, Exception ex)
        {
            try
            {
                if (!Directory.Exists(PATH.GetTempPath() + "eff"))
                    Directory.CreateDirectory(PATH.GetTempPath() + "eff");

                StreamWriter swData2 = new StreamWriter(PATH.GetTempPath() + "eff" + "\\ErrorLog.txt", true);
                swData2.WriteLine("=====Unhandled Exception occured at  : " + DateTime.Now);
                swData2.WriteLine("Message = " + message);
                swData2.WriteLine("Error = " + ex.Message);
                swData2.WriteLine(ex.StackTrace);
                swData2.WriteLine(ex.Source);
                swData2.WriteLine(ex.InnerException);
                swData2.WriteLine("====");
                swData2.Close();
                swData2.Dispose();
            }
            catch (Exception ex1)
            {
                string errorMessage = ex1.Message;
            }
        }

        #region Properties
        private FileItem _selectedItem = new FileItem();
        public FileItem SelectedItem
        {
            get
            {
                return _selectedItem;
            }
            set
            {
                _selectedItem = value;
                RaisePropertyChanged(() => SelectedItem);
            }
        }

        private List<FileItem> _items = new List<FileItem>();
        public List<FileItem> Items
        {
            get
            {
                return _items;
            }
            set
            {
                _items = value;
                RaisePropertyChanged(() => Items);
            }
        }
        #endregion
    }

    public class FileItem : BaseNotifyPropertyChanged
    {
        private string _fileName;
        private string _folderName;
        private string _divider;
        private string _alert1;
        private string _alert2;
        private string _date;
        private string _status;
        private string _filePath;

        #region Constructor
        /// <summary>
        /// The FileItem
        /// </summary>
        public FileItem()
        {

        }

        /// <summary>
        /// The File Item.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="folderName"></param>
        /// <param name="divider"></param>
        /// <param name="alert1"></param>
        /// <param name="alert2"></param>
        /// <param name="date"></param>
        /// <param name="status"></param>
        /// <param name="filePath"></param>
        public FileItem(string fileName, string folderName, string divider, string alert1, string alert2, string date, string status, string filePath)
        {
            FileName = fileName;
            FolderName = folderName;
            Divider = divider;
            //Alert1 = alert1;
            //Alert2 = alert2;
            Date = date;
            FilePath = filePath;
        }
        #endregion

        #region Properties
        public string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
                RaisePropertyChanged(() => FileName);
            }
        }

        public string FilePath
        {
            get
            {
                return _filePath;
            }
            set
            {
                _filePath = value;
                RaisePropertyChanged(() => FilePath);
            }
        }

        public string FolderName
        {
            get
            {
                return _folderName;
            }
            set
            {
                _folderName = value;
                RaisePropertyChanged(() => FolderName);
            }
        }

        public string Divider
        {
            get
            {
                return _divider;
            }
            set
            {
                _divider = value;
                RaisePropertyChanged(() => Divider);
            }
        }

        public string Alert1
        {
            get
            {
                return _alert1;
            }
            set
            {
                _alert1 = value;
                RaisePropertyChanged(() => Alert1);
            }
        }

        public string Alert2
        {
            get
            {
                return _alert2;
            }
            set
            {
                _alert2 = value;
                RaisePropertyChanged(() => Alert2);
            }
        }

        public string Date
        {
            get
            {
                return _date;
            }
            set
            {
                _date = value;
                RaisePropertyChanged(() => Date);
            }
        }

        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                RaisePropertyChanged(() => Status);
            }
        }
        #endregion
    }

}
