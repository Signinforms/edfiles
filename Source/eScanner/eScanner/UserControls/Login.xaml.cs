﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using eScanner.eScannerService;
using GlobalContext = eScanner.BO.GlobalContext;

namespace eScanner.UserControls
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : UserControl
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                eScannerService.EFileFolderJSONWSSoapClient obj = new EFileFolderJSONWSSoapClient();
                AuthenticationInfo obj2 = new AuthenticationInfo();
                obj2.username = txtUserName.Text;
                obj2.password = txtPassword.Password;
                string Uid = obj.CheckUser(obj2, txtUserName.Text, txtPassword.Password);
                if (Uid != null)
                {
                    GlobalContext.UserId = Uid;
                    GlobalContext.UserName = obj2.username;
                    GlobalContext.Password = obj2.password;
                    (Application.Current.MainWindow as MainWindow).tabAction.IsEnabled = true;
                    (Application.Current.MainWindow as MainWindow).tabAction.IsSelected = true;
                    (Application.Current.MainWindow as MainWindow).tabLogin.IsEnabled = false;
                    (Application.Current.MainWindow as MainWindow).btnEfileFolder.IsEnabled = true;
                    (Application.Current.MainWindow as MainWindow).btnWorkArea.IsEnabled = true;
                    (Application.Current.MainWindow as MainWindow).btnWorkArea.Opacity = 1;
                    (Application.Current.MainWindow as MainWindow).btnEfileFolder.Opacity = 1;
                }
                else
                {
                    MessageBox.Show("Invalid UserName or Password !","Login");
                }
            }
            catch (Exception exc)
            {
 
            }
        }
    }
}
