﻿using eScanner.BO;
using eScanner.eScannerService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace eScanner.UserControls
{
    /// <summary>
    /// Interaction logic for PDFViewer.xaml
    /// </summary>
    public partial class PDFViewer : UserControl
    {
        public bool isLoading { get; set; }

        #region Constructor
        public PDFViewer()
        {
            InitializeComponent();
            this.Loaded += PDFViewer_Loaded;
        }

        /// <summary>
        /// PDFViewer
        /// </summary>
        /// <param name="path">The path</param>
        public PDFViewer(string path)
        {
            InitializeComponent();
            try
            {
                if (!string.IsNullOrEmpty(path))
                {
                    path = path.Replace("////", "/");
                    path = path.Replace("//", "/");
                    path = "file:///" + path;
                    MyNavigate(path);

                }
            }
            catch (Exception exc)
            {
 
            }
        }
        #endregion

        #region Events
        void PDFViewer_Loaded(object sender, RoutedEventArgs e)
        {
            pdfWebViewer.Navigated += pdfWebViewer_Navigated;
        }

        void pdfWebViewer_Navigated(object sender, NavigationEventArgs e)
        {
            isLoading = false;
        }
        #endregion

        /// <summary>
        /// My Navigate.
        /// </summary>
        /// <param name="path"></param>
        public void MyNavigate(string path)
        {
            pdfWebViewer.Navigate(path);
            isLoading = true;
        }
    }
}
