﻿using System;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;
using eScanner.Model;
using System.Linq;

namespace eScanner.UserControls
{
    public class clsCopyFiles : Form
    {
        private static UIntPtr HKEY_LOCAL_MACHINE = new UIntPtr(0x80000002u);

        [DllImport("User32.dll", EntryPoint = "SendMessage")]
        public static extern Int32 SendMessage(
                                            IntPtr hWnd,
                                            UInt32 Msg,
                                            UInt32 wParam,
                                            ref COPYDATASTRUCT lParam);

        [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileString")]
        public static extern Int32 GetPrivateProfileString(
                                            string lpAppName,
                                            string lpKeyName,
                                            string lpDefault,
                                            StringBuilder lpReturnedString,
                                            UInt32 nSize,
                                            string lpFileName);

        [DllImport("kernel32.dll", EntryPoint = "GetPrivateProfileInt")]
        public static extern UInt32 GetPrivateProfileInt(
                                            string lpAppName,
                                            string lpKeyName,
                                            Int32 nDefault,
                                            string lpFileName);

        [DllImport("advapi32.dll", EntryPoint = "RegOpenKeyEx")]
        public static extern Int32 RegOpenKeyEx(
                                            UIntPtr hKey,
                                            string lpSubKey,
                                            UInt32 ulOptions,
                                            UInt32 samDesired,
                                            out UIntPtr phkResult);

        [DllImport("advapi32.dll", EntryPoint = "RegQueryValueEx")]
        public static extern Int32 RegQueryValueEx(
                                            UIntPtr hKey,
                                            string lpValueName,
                                            IntPtr lpReserved,
                                            out UInt32 lpType,
                                            StringBuilder lpData,
                                            ref UInt32 lpcbData);

        [DllImport("advapi32.dll", EntryPoint = "RegCloseKey")]
        public static extern Int32 RegCloseKey(UIntPtr hKey);


        /// <summary>
        /// COPYDATASTRUCT
        /// </summary>
        public struct COPYDATASTRUCT
        {
            public Int32 dwData;
            public Int32 cbData;
            public IntPtr lpData;
        }

        /// <summary>
        /// SS_NOTIFY
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        private struct SS_NOTIFY
        {
            public Int32 Mode;
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 255)]
            public byte[] AppName;
        }

        /// <summary>
        /// SS_SCAN
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        private struct SS_SCAN
        {
            public Int32 Mode;
            public bool ScanningSide;
        }



        /// <summary>
        /// override
        /// </summary>
        /// <param name="winMessage">Windows message</param>
        protected override void WndProc(ref Message winMessage)
        {
            switch (winMessage.Msg)
            {
                // sendmessage
                case eScanner.MainWindow.WM_COPYDATA:
                    string filePath;
                    int result = 1;         // success
                    try
                    {
                        WriteErrorLog("copydata1 ", new Exception());
                        eScanner.MainWindow.COPYDATASTRUCT cds = new eScanner.MainWindow.COPYDATASTRUCT();
                        Type cdsType = cds.GetType();
                        cds = (eScanner.MainWindow.COPYDATASTRUCT)winMessage.GetLParam(cdsType);
                        filePath = cds.lpData;
                        //MessageBox.Show("Received:" + eScanner.MainWindow.WM_COPYDATA.ToString());
                        //MessageBox.Show("Received:" + cds.ToString());
                        //MessageBox.Show("Received:" + filePath);
                        // copy file or move file
                        CopyFile(filePath);

                        GeneralViewer gv = (App.Current.MainWindow as MainWindow).dockContainer1.Children.OfType<GeneralViewer>().FirstOrDefault();
                        gv.DataContext = new FileList();

                        // delete ini file
                        File.Delete(filePath);
                    }
                    catch(Exception ex)
                    {
                        // Please describe processing when the error occurs.
                        result = 0;         // failed
                        WriteErrorLog("copydata2 " + ex.Message, ex);
                    }
                    winMessage.Result = (IntPtr)result;
                    break;
                default:
                    base.WndProc(ref winMessage);
                    break;
            }
        }

        public static void WriteErrorLog(string message, Exception ex)
        {
            try
            {
                if (!Directory.Exists(Path.GetTempPath() + "eff"))
                    Directory.CreateDirectory(Path.GetTempPath() + "eff");

                StreamWriter swData2 = new StreamWriter(Path.GetTempPath() + "eff" + "\\ErrorLog.txt", true);
                swData2.WriteLine("=====Unhandled Exception occured at  : " + DateTime.Now);
                swData2.WriteLine("Message = " + message);
                swData2.WriteLine("Error = " + ex.Message);
                swData2.WriteLine(ex.StackTrace);
                swData2.WriteLine(ex.Source);
                swData2.WriteLine(ex.InnerException);
                swData2.WriteLine("====");
                swData2.Close();
                swData2.Dispose();
            }
            catch (Exception ex1)
            {
                string errorMessage = ex1.Message;
            }
        }

        /// <summary>
        /// analyze, and copy file or move file
        /// </summary>
        /// <param name="resultFilePath">result file path</param>
        /// <returns>TRUE:SUCCESS,FALSE:ERROR</returns>
        private bool CopyFile(string resultFilePath)
        {
            int maxPath = 260;                  // MAXPATH
            StringBuilder returnedString = new StringBuilder(maxPath);
            bool isDeleteFile = false;

            // Get appName
            GetPrivateProfileString("INFO", "Application", "", returnedString,
                                    (UInt32)(returnedString.Capacity), resultFilePath);    // Win32API
            string appName = returnedString.ToString();
            //if (appName.Contains("ScanSnap Manager") == true)
            //{
            //    isDeleteFile = true;
            //}
            //else if (appName.Contains("ScanSnap Organizer") == true)
            //{
            //    isDeleteFile = false;
            //}

            UInt32 dataFileNum = GetPrivateProfileInt("FILES", "FileCount", 0, resultFilePath);     // Win32API

            WriteErrorLog("dataFileNum : " + dataFileNum, new Exception());

            WriteErrorLog("destFilename : " + eScanner.MainWindow.TempDirectory, new Exception());

            if (dataFileNum == 0)
            {
                WriteErrorLog("return  : " + dataFileNum, new Exception());
                return false;
            }

            for (int i = 1; i <= dataFileNum; i++)
            {
                string keyName = "File" + i;
                string dataFilePath;
                string dataFileName;
                string destFilename;

                returnedString.Remove(0, returnedString.Length);
                GetPrivateProfileString("FILES", keyName, "", returnedString,
                                        (UInt32)(returnedString.Capacity), resultFilePath);         // Win32API
                dataFilePath = returnedString.ToString();
                dataFileName = Path.GetFileName(dataFilePath);
                destFilename = eScanner.MainWindow.TempDirectory + @"\" + dataFileName;


                //copy or move data
                if (isDeleteFile == true)
                {
                    File.Move(dataFilePath, destFilename);
                }
                else
                {
                    File.Copy(dataFilePath, destFilename, true);    // overwrite
                }
            }
            return true;
        }

        public clsCopyFiles()
        {
            InitializeComponent();
        }
        public void startCopy()
        {
            if (String.IsNullOrEmpty(eScanner.MainWindow.ResultFilePath) == false)
            {
                try
                {
                    // copy file or move file
                    CopyFile(eScanner.MainWindow.ResultFilePath);

                    // delete ini file
                    File.Delete(eScanner.MainWindow.ResultFilePath);
                    eScanner.MainWindow.ResultFilePath = null;
                }
                catch
                {
                    // Please describe processing when the error occurs.
                }
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // clsCopyFiles
            // 
            this.ClientSize = new System.Drawing.Size(0, 0);
            this.Name = "clsCopyFiles";
            this.Text = "EFFTEST";
            this.ResumeLayout(false);
            this.Visible = false;
        }


    }
}
