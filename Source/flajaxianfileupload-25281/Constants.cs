﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Web.Security;
using System.Web.UI.WebControls;

namespace com.flajaxian
{
    /// <summary>
    /// The class to be used for non localizable constants
    /// </summary>
    public static class Constants
    {
        public const string CdataBeginning = "<![CDATA[";
        public const string CdataEnd = "]]>";
        public const string DefaultImageExtensions = "jpeg;jpg;png;gif";
        public static readonly Color DefaultProgressBarBorderColor = Color.FromArgb(0x8C, 0x8C, 0x8C);
        public const string IdLimiter = "_";
        public const string IncludeKey_SwfObject = "SwfObject";
        public const string JS_WindowPrefix = "window.";
        public const string JsObjectAndPropertyTemplate = "{0}.{1}";

        public const string ResourcePath_FileUploader2 = "com.flajaxian.js.FileUploader2.js";
        public const string ResourcePath_SwfObject = "com.flajaxian.js.SwfObject.js";
        public const string ResourcePath_SwfObjectDebug = "com.flajaxian.js.SwfObject.Debug.js";
        public const string ResourcePath_CloseButtonImage = "com.flajaxian.images.CloseButton.gif";
        public const string ResourcePath_ClosedArrowImage = "com.flajaxian.images.ClosedArrow.gif";
        public const string ResourcePath_OpenedArrowImage = "com.flajaxian.images.OpenedArrow.gif";
        public const string ResourcePath_Flash = "com.flajaxian.flash.FileUploader2.swf";
        public const string ResourcePath_ExpressInstall = "com.flajaxian.flash.expressInstall.swf";
        public const string ResourcePath_CommonCss = "com.flajaxian.css.Common.css";
        public const string Regex_StripScriptTags = "<[/]?[ ]?script[^>]*>";
        public const string StatePrefix = "S_";

        
        public const string Key_AspCookie_SessionState = "__AspCookie.Session";
        public const string Key_AspCookie_AuthState = "__AspCookie.Auth";
        public const string Key_AspCookie_Session = "ASP.NET_SessionId";
        public readonly static string Key_AspCookie_Auth = FormsAuthentication.FormsCookieName;
        public static readonly Unit DefaultWidth = new Unit(251);
        public static readonly Unit DefaultHeight = new Unit(40);
        public static readonly Color DefaultBackColor = Color.FromArgb(0xFFFFFF);
        public const string LinkTagTemplate = @"<link href=""{0}"" type=""text/css"" rel=""stylesheet"" />";
        
    }
}
