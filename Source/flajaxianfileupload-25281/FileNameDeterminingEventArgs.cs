﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace com.flajaxian
{
    /// <summary>
    /// 
    /// </summary>
    public class FileNameDeterminingEventArgs : EventArgs
    {
        private string _fileName;
        private readonly HttpPostedFile _file;
        private bool _cancelProcessing;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        public FileNameDeterminingEventArgs(HttpPostedFile file)
        {
            this._file = file;
            if (this._file != null)
            {
                this._fileName = file.FileName;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public string FileName
        {
            get { return this._fileName; }
            set { this._fileName = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool CancelProcessing
        {
            get { return this._cancelProcessing; }
            set { this._cancelProcessing = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public HttpPostedFile File
        {
            get { return this._file; }
        }
    }
}
