using System;
using System.Web;
using System.Collections.Generic;
using System.Text;

namespace com.flajaxian
{
    /// <summary>
    /// 
    /// </summary>
    public class FileReceivedEventArgs : EventArgs
    {
        private readonly HttpPostedFile _file;
        private readonly bool _isLast;
        private readonly int _index;

        /// <summary>
        /// Constructor for the FileReceivedEventArgs
        /// </summary>
        /// <param name="file"></param>
        /// <param name="isLast"></param>
        /// <param name="index"></param>
        public FileReceivedEventArgs(HttpPostedFile file, bool isLast, int index)
        {
            this._file = file;
            this._isLast = isLast;
            this._index = index;
        }
        /// <summary>
        /// 
        /// </summary>
        public HttpPostedFile File
        {
            get { return this._file; }
        }
        /// <summary>
        /// Flag indicating if this is the last file in the queue
        /// </summary>
        public bool isLast
        {
            get { return this._isLast; }
        }
        /// <summary>
        /// Zero based file index
        /// </summary>
        public int Index
        {
            get { return this._index; }
        }
    }
}
