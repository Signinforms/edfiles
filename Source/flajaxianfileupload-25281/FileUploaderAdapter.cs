using System;
using System.Web;
using System.Collections;
using System.Text;
using System.Web.UI;

namespace com.flajaxian
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class FileUploaderAdapter
    {
        private FileUploader _parent;
        private bool _uploadRequiresJsConfirmation;

        public event EventHandler<EventArgs> Init;

        /// <summary>
        /// If set to true upload will not start before confirmation from JavaScript 
        /// generated by the adapter in WritePreInitializationJavaScript method
        /// </summary>
        public virtual bool UploadRequiresJsConfirmation
        {
            get { return false; }
        }

        /// <summary>
        /// 
        /// </summary>
        public FileUploader Parent
        {
            get { return this._parent; }
            set
            {
                bool doInitialize = (value != this._parent) ;
                this._parent = value;
                if (doInitialize)
                {
                    this.OnInit(EventArgs.Empty);
                }
            }
        }

        /// <summary>
        /// To be used for server processing of the uploaded  file
        /// </summary>
        /// <param name="file">the file</param>
        public abstract void ProcessFile(HttpPostedFile file);

        /// <summary>
        /// A virtual method that can be overriden for a particular adapter in order to initialize it
        /// </summary>
        protected virtual void OnInit(EventArgs e)
        {
            if (this.Init != null)
            {
                this.Init(this, e);
            }
        }

        /// <summary>
        /// Method that if overriden can be used to add javascript that will be called
        /// just before FileUploader.initialize() JavaScript method is called
        /// </summary>
        /// <param name="writer"></param>
        public virtual void WritePreInitializationJavaScript(HtmlTextWriter writer)
        {
        }

        /// <summary>
        /// Method that if overriden can be used to add javascript that will be called 
        /// when the rest of JavaScript is rendered
        /// </summary>
        /// <param name="writer"></param>
        public virtual void WriteJavaScriptInstantiation(HtmlTextWriter writer)
        {
        }
    }
}
