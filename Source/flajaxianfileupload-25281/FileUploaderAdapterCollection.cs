using System;
using System.Web.UI;
using System.ComponentModel;
using System.Collections;
using System.Text;

namespace com.flajaxian
{
    /// <summary>
    /// 
    /// </summary>
    [ParseChildren(true), PersistChildren(false), TypeConverter(typeof(ExpandableObjectConverter))]
    public class FileUploaderAdapterCollection : CollectionBase
    {
        
        private FileUploader _parent;

        /// <summary>
        /// 
        /// </summary>
        public FileUploader Parent
        {
            get { return this._parent; }
            internal set { this._parent = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="adapter"></param>
        /// <returns></returns>
        public int Add(FileUploaderAdapter adapter)
        {
            adapter.Parent = this._parent;
            return base.List.Add(adapter);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="adapter"></param>
        /// <returns></returns>
        public bool Contains(FileUploaderAdapter adapter)
        {
            return base.List.Contains(adapter);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="adapter"></param>
        /// <returns></returns>
        public int IndexOf(FileUploaderAdapter adapter)
        {
            return base.List.IndexOf(adapter);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="adapter"></param>
        public void Insert(int index, FileUploaderAdapter adapter)
        {
            base.List.Insert(index, adapter);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="adapter"></param>
        public void Remove(FileUploaderAdapter adapter)
        {
            base.List.Remove(adapter);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public FileUploaderAdapter this[int index]
        {
            get
            {
                return (base.List[index] as FileUploaderAdapter);
            }
            set
            {
                base.List[index] = value;
            }
        }
    }



}
