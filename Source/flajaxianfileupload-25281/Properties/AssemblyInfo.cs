﻿using System.Web.UI;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("com.flajaxian.FileUploader 2.0 BETA")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Flajaxian")]
[assembly: AssemblyProduct("com.flajaxian.FileUploader")]
[assembly: AssemblyCopyright("Copyright ©  2008")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("4634906A-A484-4703-978C-3B382F8AC556")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("2.0.3430.0")]
[assembly: AssemblyFileVersion("2.0.3430.0")]

[assembly: WebResource("com.flajaxian.js.FileUploader2.js", "text/javascript")]
[assembly: WebResource("com.flajaxian.js.SwfObject.js", "text/javascript")]
[assembly: WebResource("com.flajaxian.js.SwfObject.Debug.js", "text/javascript")]
[assembly: WebResource("com.flajaxian.flash.expressInstall.swf", "application/x-shockwave-flash")]
[assembly: WebResource("com.flajaxian.flash.FileUploader2.swf", "application/x-shockwave-flash")]
[assembly: WebResource("com.flajaxian.images.CloseButton.gif", "image/gif")]
[assembly: WebResource("com.flajaxian.images.ClosedArrow.gif", "image/gif")]
[assembly: WebResource("com.flajaxian.images.OpenedArrow.gif", "image/gif")]
[assembly: WebResource("com.flajaxian.css.Common.css", "text/css")]
[assembly: AllowPartiallyTrustedCallers]