﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace com.flajaxian {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class SR {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal SR() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("com.flajaxian.SR", typeof(SR).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to - error.
        /// </summary>
        internal static string DefaultErrorSuffix {
            get {
                return ResourceManager.GetString("DefaultErrorSuffix", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The maximum number of files allowed to be selected of &apos;{0}&apos; has been reached..
        /// </summary>
        internal static string MaxFileNumberReached {
            get {
                return ResourceManager.GetString("MaxFileNumberReached", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The maximum allowed file size of &apos;{0}&apos; has been exceeded..
        /// </summary>
        internal static string MaxFileSizeReached {
            get {
                return ResourceManager.GetString("MaxFileSizeReached", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The maximum allowed size of &apos;{0}&apos; for all the files in the queue has been exceeded..
        /// </summary>
        internal static string MaxQueueSizeReached {
            get {
                return ResourceManager.GetString("MaxQueueSizeReached", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Please install Adobe Flash Player.
        /// </summary>
        internal static string PleaseInstallAdobeFlashPlayer {
            get {
                return ResourceManager.GetString("PleaseInstallAdobeFlashPlayer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Allowed file types must be defined in a format &quot;File Group 1 Name:FileType1;FileType2;FileType3:MAC types|File Group 2 Name:FileType1;FileType2;FileType3:MAC types]&quot; for example: &quot;Image Files:*.jpg;*.jpeg;*.gif;*.png:JPEG;jp2_;GIFf;PNGf|Flash Movies:*.swf:SWFL&quot;.
        /// </summary>
        internal static string ProblemAllowedFileTypesWrongFormat {
            get {
                return ResourceManager.GetString("ProblemAllowedFileTypesWrongFormat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There is a problem when trying to parse the file queue size &apos;{0}&apos;.
        /// </summary>
        internal static string ProblemParsingFileQueueSize {
            get {
                return ResourceManager.GetString("ProblemParsingFileQueueSize", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There is a problem when trying to parse the file size &apos;{0}&apos;.
        /// </summary>
        internal static string ProblemParsingFileSize {
            get {
                return ResourceManager.GetString("ProblemParsingFileSize", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ProgressBorderAlpha value must be between 0 and 1.
        /// </summary>
        internal static string ProgressBorderAlphaMustBeFrom0To1 {
            get {
                return ResourceManager.GetString("ProgressBorderAlphaMustBeFrom0To1", resourceCulture);
            }
        }
    }
}
