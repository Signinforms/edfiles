﻿package com.flajaxian {
	
	import flash.events.*;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	
	
	
	public class ButtonGroup extends Sprite
	{
		private var _on:Sprite;
		private var _over:Sprite;
		private var _press:Sprite;
		private var _off:Sprite;
		
		private var _groupName:String;
		private var _parentObj:DisplayObjectContainer;
		
		private var _isDisabled:Boolean;
		
		public function ButtonGroup(parentObj:DisplayObjectContainer, p:Params, grpName:String){
			this._groupName = grpName;
			this._parentObj = parentObj;
			this._parentObj.addChild(this);
			this._on =		this.generateSquare(p, grpName);
			this._over =	this.generateSquare(p, grpName);
			this._press =	this.generateSquare(p, grpName);
			this._off =		this.generateSquare(p, grpName);
			
			this.mouseChildren = false;

			this.addEventListener(MouseEvent.MOUSE_DOWN, this.onMouseDown);
			this.addEventListener(MouseEvent.MOUSE_UP, this.onMouseUp);
			this.addEventListener(MouseEvent.MOUSE_OVER, this.onMouseOver);
			this.addEventListener(MouseEvent.MOUSE_OUT, this.onMouseOut);
			this.setOn();
		}
		
		
		
		private function onMouseOver(evt:Event):void{
			this.setOver();
		}
		private function onMouseOut(evt:Event):void{
			this.setOn();
		}
		private function onMouseDown(evt:Event):void{
			this.setPress();
		}
		private function onMouseUp(evt:Event):void{
			this.setOver();
		}
		
		public function setOn():void{
			if(this.isDisabled){ return; }
			this.on.visible = true;
			this.over.visible = this.press.visible = this.off.visible = false;
		}
		public function setOver():void{
			if(this.isDisabled){ return; }
			this.over.visible = true;
			this.on.visible = this.press.visible = this.off.visible = false;
		}
		public function setPress():void{
			if(this.isDisabled){ return; }
			this.press.visible = true;
			this.over.visible = this.on.visible = this.off.visible = false;
		}
		public function setOff():void{
			this.off.visible = true;
			this.over.visible = this.press.visible = this.on.visible = false;
		}
		public function setDisabled(bln:Boolean):void{
			this._isDisabled = bln;
			if(this.isDisabled){
				this.setOff();
			}else{
				this.setOn();
			}
		}
		
		public function get groupName():String { return this._groupName; }
		
		public function get isDisabled():Boolean { return this._isDisabled; }
		public function get on():Sprite { return this._on; }
		public function get over():Sprite { return this._over; }
		public function get press():Sprite { return this._press; }
		public function get off():Sprite { return this._off; }
		
		private function generateSquare(p:Params, prop:String):Sprite{
			var spr:Sprite = Util.generateSquare(this, p, prop, 0);
			if(this.x == 0) this.x = spr.x;
			if(this.y == 0) this.y = spr.y;
			spr.x = 0;
			spr.y = 0;
			return spr;
		}
		
		public static function isNullOrEmpty(val:String):Boolean{
			return val === null || val === "";
		}
	}
}