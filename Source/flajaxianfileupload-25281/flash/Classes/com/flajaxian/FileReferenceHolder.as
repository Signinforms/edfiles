﻿package com.flajaxian {
	
	import flash.net.FileReference;
	import flash.net.URLRequest;
	
	public class FileReferenceHolder
	{
		private var _file:FileReference = null;
		private var _name:String = null;
		private var _state:int = FileState.Selected; 
		private var _parent:FileReferenceCollection;
		private var _bytes:Number = 0;
		private var _id:int;
		private var _hashKey:String;
		private var _httpStatus:uint = 200;
		private var _completeFired:Boolean = false;
		
		public function FileReferenceHolder(prnt:FileReferenceCollection, fr:FileReference){
			this._parent = prnt;
			this._file = fr;
			this._id = Util.nextInt();
			this._hashKey =	this.file.name+":"+
						this.file.size+":"+
						this.file.type+":"+
						this.file.creationDate+":"+
						this.file.modificationDate+":"+
						this.file.creator+":";
						
			this._bytes = this.file.size;
			this._name = this.file.name;
		}
		
		public function get id():Number{ return this._id; }
		public function get hashKey():String{ return this._hashKey; }
		
		public function get httpStatus():uint{ return _httpStatus; }
		public function get parent():FileReferenceCollection{ return this._parent; }
		public function get file():FileReference{ return this._file; }
		public function get name():String{ return this._name; }
		public function get state():int{ return this._state; }
		public function get bytes():Number{ return this._bytes; }
		
		public function setState(s:int):void{ this._state = s; }
		public function setHttpStatus(hs:uint):void{ this._httpStatus = hs; }
		
		public function cancel():void{
			this.file.cancel();
		}
		
		public function upload(req:URLRequest, uploadDataFieldName:String = "Filedata"):void{
			this.file.upload(req, uploadDataFieldName);
		}
		
		public function toJson():Object{
			var sb:Array = [];
			sb.push("{");
			sb.push("id:"+this.id+",");
			sb.push("name:"+Util.putInQuotes(this.name)+",");
			sb.push("bytes:"+this.bytes.toString()+",");
			sb.push("state:"+this.state.toString());
			sb.push("}");
			return sb.join("");
		}
	}
}