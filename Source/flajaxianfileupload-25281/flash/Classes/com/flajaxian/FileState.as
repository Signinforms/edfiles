﻿package com.flajaxian 
{
	public class FileState
	{
		public static var Selected:int = 1;
		public static var Uploading:int = 2;
		public static var Uploaded:int = 3;
		public static var Cancelled:int = 4;
		public static var Error:int = 5;
	}
}