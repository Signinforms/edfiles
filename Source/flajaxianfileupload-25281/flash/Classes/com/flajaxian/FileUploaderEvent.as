﻿package com.flajaxian {
	
	import flash.events.*;
	
	public class FileUploaderEvent extends Event
	{
		private var _state:Object;
		
		public function FileUploaderEvent(eventName:String, state:Object = null){
			super(eventName, false, false);
			this._state = state;
		}
		
		public function get state():Object{
			return this._state;
		}
	}
}