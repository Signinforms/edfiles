﻿package com.flajaxian {
	
	import flash.events.*;
	import flash.events.EventDispatcher;
	import flash.external.ExternalInterface;
	
	public class FlashJsManager extends EventDispatcher
	{
		public static var FILE_REMOVED:String = "file_removed";
		public static var CANCEL_ALL:String = "cancel_all";
		public static var INITIATE_FILE_UPLOAD:String = "initiate_file_upload";
		public static var SET_VISIBILITY:String = "set_visibility";
		public static var START_UPLOAD:String = "start_upload";
		public static var DISABLED:String = "disabled";
		public static var ENABLED:String = "enabled";
		
		private static var _instance:FlashJsManager;
		
		public function FlashJsManager():void{
			ExternalInterface.addCallback("disable", this.disable);
			ExternalInterface.addCallback("enable", this.enable);
			ExternalInterface.addCallback("fileRemoved", this.fileRemoved);
			ExternalInterface.addCallback("cancelAll", this.cancelAll);
			ExternalInterface.addCallback("setStateVariables", this.setStateVariables);
			ExternalInterface.addCallback("deleteStateVariables", this.deleteStateVariables);
			ExternalInterface.addCallback("setVisibility", this.setVisibility);
			ExternalInterface.addCallback("startUpload", this.startUpload);
		}
		
		private function disable():void{
			if(Params.instance.debug) t.str("disable");
			this.dispatchEvent(new FileUploaderEvent(FlashJsManager.DISABLED));
		}
		
		private function enable():void{
			if(Params.instance.debug) t.str("enable");
			this.dispatchEvent(new FileUploaderEvent(FlashJsManager.ENABLED));
		}
		
		private function fileRemoved(id:Number):void{
			if(Params.instance.debug) t.str("fileRemoved", id);
			this.dispatchEvent(new FileUploaderEvent(FlashJsManager.FILE_REMOVED,id));
		}
		
		private function cancelAll():void{
			if(Params.instance.debug) t.str("cancelAll");
			this.dispatchEvent(new FileUploaderEvent(FlashJsManager.CANCEL_ALL));
		}
		
		private function setStateVariables(list:Array, initiateUpload:Boolean):void{
			if(Params.instance.debug) t.obj(list, "setStateVariables", initiateUpload);
			for(var i:int = 0; i < list.length; i++){
				if(!!list[i].key)
					Params.instance.setStateVariable(list[i].key, list[i].value, list[i].fileID);
			}
			if(initiateUpload){
				this.dispatchEvent(new FileUploaderEvent(FlashJsManager.INITIATE_FILE_UPLOAD));
			}
		}
		
		private function deleteStateVariables():void{
			Params.instance.deleteStateVariables();
		}
		
		private function setVisibility(bln:Boolean):void{
			this.dispatchEvent(new FileUploaderEvent(FlashJsManager.SET_VISIBILITY, bln));
		}
		
		private function startUpload():void{
			this.dispatchEvent(new FileUploaderEvent(FlashJsManager.START_UPLOAD));
		}
		
		public static function get instance():FlashJsManager{
			if(_instance === null){
				_instance = new FlashJsManager();
			}
			return _instance;
		}
		
		public function call(js:String):void{
			if(Params.instance.debug) t.str("CALL to JavaScript:"+js);
			try{
				ExternalInterface.call(js);
			}catch(e:Error){
				t.obj(e, "Error calling:" + js);
			}
		}
		
		public function markInitialized():void{
			this.call(Params.instance.id+"_MarkInitialized()");
		}
		
		public function confirmUpload():void{
			this.call(Params.instance.id+"_ConfirmUpload()");
		}
		
		public function setFileList(json:String):void{
			this.call(Params.instance.id+"_SetFileList("+json+")");
		}
		
		public function setUploadProgress(totalBytes:Number, 
										  loadedBytes:Number,
										  currTotal:Number,
										  currLoaded:Number):void{
			var info:String = 
				"{total:"+totalBytes+",loaded:"+loadedBytes+
					",currTotal:"+currTotal+",currLoaded:"+currLoaded+"}";
			this.call(Params.instance.id+"_SetUploadProgress("+info+")");
		}
		
		public function setChangedFileStates(json:String):void{
			this.call(Params.instance.id+"_SetChangedFileStates("+json+")");
		}
		
		public function maxFileNumberReached():void{
			this.call(Params.instance.id+"_LimitReached('fileNumber')");
		}
		
		public function maxFileSizeReached():void{
			this.call(Params.instance.id+"_LimitReached('fileSize')");
		}
		
		public function maxQueueSizeReached():void{
			this.call(Params.instance.id+"_LimitReached('queueSize')");
		}
		
		
		
		
		
	}
}