﻿package com.flajaxian{

	import flash.utils.describeType;
	import flash.net.URLVariables;

	public class Params {

		private var _id:String;

		private var _targetUrl:String;

		private var _imagesPath:String;

		private var _state:URLVariables;
		private var _statePerFile:Object;

		private var _allowedFileTypes:String;
		private var _maxFileSize:Number = Number.MAX_VALUE;
		private var _maxFileQueueSize:Number = Number.MAX_VALUE;
		private var _maxFileNumberLimit:uint = uint.MAX_VALUE;
		private var _debug:Boolean = false;
		private var _enabled:Boolean = true;
		private var _isIE:Boolean = true;
		private var _clearListAtEnd:Boolean = true;
		private var _uploadRequiresJsConfirmation:Boolean = false;
		private var _initiallyInvisible:Boolean = false;
		private var _uploadDataFieldName:String = "Filedata";
		private var _appendToUrlFileIndex:Boolean = true;

		private var _browseBtnUrl:String;
		private var _browseBtnOverUrl:String;
		private var _browseBtnPressUrl:String;
		private var _browseBtnDisableUrl:String;

		private var _uploadBtnUrl:String;
		private var _uploadBtnOverUrl:String;
		private var _uploadBtnPressUrl:String;
		private var _uploadBtnDisableUrl:String;

		private var _cancelBtnUrl:String;
		private var _cancelBtnOverUrl:String;
		private var _cancelBtnPressUrl:String;
		private var _cancelBtnDisableUrl:String;

		private var _progressBackUrl:String;
		private var _progressForeUrl:String;

		private var _browseBtnX:Number = 1;
		private var _browseBtnY:Number = 15;
		private var _browseBtnWidth:Number = 85;
		private var _browseBtnHeight:Number = 24;

		private var _uploadBtnX:Number = 87;
		private var _uploadBtnY:Number = 15;
		private var _uploadBtnWidth:Number = 85;
		private var _uploadBtnHeight:Number = 24;

		private var _cancelBtnX:Number = 173;
		private var _cancelBtnY:Number = 15;
		private var _cancelBtnWidth:Number = 85;
		private var _cancelBtnHeight:Number = 24;

		private var _progressX:Number = 1;
		private var _progressY:Number = 1;
		private var _progressWidth:Number = 249;
		private var _progressHeight:Number = 11;

		private var _progressBorderColor:Number = 0x8C8C8C;
		private var _progressBorderSize:Number = 1;
		private var _progressBorderAlpha:Number = 1;

		public static  var _instance:Params;


		public function Params(src:Object) {
			this.assign(src);
			_instance = this;
		}
		public static function get instance():Params {
			return _instance;
		}
		public function ensureConsistency():void {
			this.ensureButton("browse");
			this.ensureButton("upload");
			this.ensureButton("cancel");
		}
		public function resetStatePerFile():void {
			this._statePerFile = new Object();
		}
		public function deleteStateVariables():void {
			this._statePerFile = new Object();
			this._state = new URLVariables();
		}
		public function setStateVariable(key:String, value:String, fileID:int):void {
			if (key != null && key != "") {
				if (fileID <= 0) {
					this.state[key] = value;
				} else {
					var fileKey:String = "F"+fileID;
					if (!this.statePerFile.hasOwnProperty(fileKey)) {
						this.statePerFile[fileKey] = new URLVariables();
					}
					this.statePerFile[fileKey][key] = value;
				}
			}
		}
		private function ensureButton(prop:String):void {
			var val:String = this[prop + "BtnUrl"];
			if (!Util.isNullOrEmpty(val)) {
				if (Util.isNullOrEmpty(this[prop + "BtnOverUrl"])) {
					this["_" + prop + "BtnOverUrl"] = val;
				}
				if (Util.isNullOrEmpty(this[prop + "BtnPressUrl"])) {
					this["_" + prop + "BtnPressUrl"] = val;
				}
			}
		}
		private function assign(src:Object):void {
			var xml = describeType(this);
			for each (var accessor:XML in xml.accessor) {
				var propName:String = accessor.@name;
				var propType:String = accessor.@type;

				var type:String = typeof(src[propName]);
				if (type === "string" || type === "number" || type === "boolean") {
					var val:String = src[propName].split("_(!AMP!)_").join("&");

					if (propType == "Number") {
						this["_"+propName] = Number( val );
					} else if (propType == "uint") {
						this["_"+propName] = uint( val );
					} else if (propType == "Boolean") {
						this["_"+propName] = val.toString().toLowerCase() == "true" || val.toString() == "1";
					} else {
						this["_"+propName] = 
						( 
						 propName != "targetUrl" &&
						Util.endsWith(propName,"Url") && 
						!Util.isNullOrEmpty(src["imagesPath"])
						) ? src["imagesPath"] + val : val;
					}
				}
			}
			for(var each:String in src){;
			if (Util.startsWith(each, "S_")) {
				this.state[each.substring(2)] = 
				src[each].split("_(!AMP!)_").join("&");
			}
		}

	}
	public function get id():String {
		return this._id;
	}
	public function get targetUrl():String {
		return this._targetUrl;
	}
	public function get imagesPath():String {
		return this._imagesPath;
	}
	public function get state():URLVariables {
		if (this._state == null) {
			this._state = new URLVariables();
		}
		return this._state;
	}
	public function get statePerFile():Object {
		if (this._statePerFile == null) {
			this._statePerFile = new Object();
		}
		return this._statePerFile;
	}
	public function get allowedFileTypes():String {
		return this._allowedFileTypes;
	}
	public function get maxFileSize():Number {
		return this._maxFileSize;
	}
	public function get maxFileQueueSize():Number {
		return this._maxFileQueueSize;
	}
	public function get maxFileNumberLimit():uint {
		return this._maxFileNumberLimit;
	}
	public function get debug():Boolean {
		return this._debug;
	}
	public function get enabled():Boolean {
		return this._enabled;
	}
	public function get isIE():Boolean {
		return this._isIE;
	}
	public function get appendToUrlFileIndex():Boolean {
		return this._appendToUrlFileIndex;
	}
	public function get clearListAtEnd():Boolean {
		return this._clearListAtEnd;
	}
	public function get uploadRequiresJsConfirmation():Boolean {
		return this._uploadRequiresJsConfirmation;
	}
	public function get initiallyInvisible():Boolean {
		return this._initiallyInvisible;
	}
	
	public function get uploadDataFieldName():String {
		return this._uploadDataFieldName;
	}
	public function get browseBtnUrl():String {
		return this._browseBtnUrl;
	}
	public function get browseBtnOverUrl():String {
		return this._browseBtnOverUrl;
	}
	public function get browseBtnPressUrl():String {
		return this._browseBtnPressUrl;
	}
	public function get browseBtnDisableUrl():String {
		return this._browseBtnDisableUrl;
	}
	public function get uploadBtnUrl():String {
		return this._uploadBtnUrl;
	}
	public function get uploadBtnOverUrl():String {
		return this._uploadBtnOverUrl;
	}
	public function get uploadBtnPressUrl():String {
		return this._uploadBtnPressUrl;
	}
	public function get uploadBtnDisableUrl():String {
		return this._uploadBtnDisableUrl;
	}
	public function get cancelBtnUrl():String {
		return this._cancelBtnUrl;
	}
	public function get cancelBtnOverUrl():String {
		return this._cancelBtnOverUrl;
	}
	public function get cancelBtnPressUrl():String {
		return this._cancelBtnPressUrl;
	}
	public function get cancelBtnDisableUrl():String {
		return this._cancelBtnDisableUrl;
	}
	public function get progressBackUrl():String {
		return this._progressBackUrl;
	}
	public function get progressForeUrl():String {
		return this._progressForeUrl;
	}
	public function get browseBtnX():Number {
		return this._browseBtnX;
	}
	public function get browseBtnY():Number {
		return this._browseBtnY;
	}
	public function get browseBtnWidth():Number {
		return this._browseBtnWidth;
	}
	public function get browseBtnHeight():Number {
		return this._browseBtnHeight;
	}
	public function get uploadBtnX():Number {
		return this._uploadBtnX;
	}
	public function get uploadBtnY():Number {
		return this._uploadBtnY;
	}
	public function get uploadBtnWidth():Number {
		return this._uploadBtnWidth;
	}
	public function get uploadBtnHeight():Number {
		return this._uploadBtnHeight;
	}
	public function get cancelBtnX():Number {
		return this._cancelBtnX;
	}
	public function get cancelBtnY():Number {
		return this._cancelBtnY;
	}
	public function get cancelBtnWidth():Number {
		return this._cancelBtnWidth;
	}
	public function get cancelBtnHeight():Number {
		return this._cancelBtnHeight;
	}
	public function get progressX():Number {
		return this._progressX;
	}
	public function get progressY():Number {
		return this._progressY;
	}
	public function get progressWidth():Number {
		return this._progressWidth;
	}
	public function get progressHeight():Number {
		return this._progressHeight;
	}
	public function get progressBorderColor():Number {
		return this._progressBorderColor;
	}
	public function get progressBorderSize():Number {
		return this._progressBorderSize;
	}
	public function get progressBorderAlpha():Number {
		return this._progressBorderAlpha;
	}
}
}