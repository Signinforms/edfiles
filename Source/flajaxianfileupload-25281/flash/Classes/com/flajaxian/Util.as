﻿package com.flajaxian {
	
	import flash.net.navigateToURL;
    import flash.net.URLRequest;
	import flash.net.FileFilter;
	
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	
	public class Util
	{
		private static var _incrementor:int = 0;
		
		public static function getURL(url:String, window:String = "_self"):void{
			var req:URLRequest = new URLRequest(url);
            trace("getURL", url);
            try
            {
                navigateToURL(req, window);
            }
            catch (e:Error)
            {
                trace("Navigate to URL failed", e.message);
            }
		}
		
		public static function nextInt():int{
			return ++_incrementor;
		}
		
		public static function putInQuotes(str:String):String{
			return "\"" + str.split("\"").join("\\\"") + "\"";
		}
		
		public static function isNullOrEmpty(val:String):Boolean{
			return val === null || val === "";
		}
		
		public static function startsWith(src:String, val:String):Boolean{
			if(src === null) return false;
			if(src === val) return true;
			return (src.indexOf(val) == 0);
		}
		
		public static function endsWith(src:String, val:String):Boolean{
			if(src === null) return false;
			if(src === val) return true;
			return (src.indexOf(val) > 0 && src.indexOf(val) == (src.length - val.length));
		}
		
		public static function generateSquare(parentObj:DisplayObjectContainer, p:Params, prop:String, alphaVal:Number = 0, color:Number = 0xFF0000):Sprite{
			var xVal:Number = p[prop+"X"];
			var yVal:Number = p[prop+"Y"];
			var widthVal:Number = p[prop+"Width"];
			var heightVal:Number = p[prop+"Height"];
			var spr:Sprite = new Sprite();
			spr.graphics.beginFill(color, alphaVal);
			spr.graphics.drawRect(0,0,widthVal,heightVal);
			spr.graphics.endFill();
			parentObj.addChild(spr);
			spr.x = xVal;
			spr.y = yVal;
			return spr;
		}
		
		public static function trim(p_string:String):String {
			if (p_string == null) { return ''; }
			return p_string.replace(/^\s+|\s+$/g, '');
		}
		
		public static function fileTypesToFileFilterArray(str:String):Array{
			var array:Array = splitNotDoubleLimiter(str, "\\|");
			var resultArray:Array = [];
			for(var i:int = 0; i < array.length; i++){
				var subArray:Array = splitNotDoubleLimiter(array[i],":");
				var descr:String = (subArray.length < 1) ? "All Files" : Util.trim(subArray[0]);
				var ext:String = (subArray.length < 2) ? "*.*" : Util.trim(subArray[1]);
				var mac:String = (subArray.length < 3) ? null : Util.trim(subArray[2]);
				var ff:FileFilter = new FileFilter(descr, ext, mac);
				resultArray.push(ff);
			}
			return resultArray;
		}
		
		
		public static function splitNotDoubleLimiter(str:String, limiter:String, limiterModifier:String = ""):Array{
			var subArray:Array = str.split(new RegExp(limiter + limiter, limiterModifier));
			var array:Array = [];
			var previousString:String = "";
			for(var i:int = 0; i < subArray.length; i++){
				var innerArray:Array = subArray[i].split(new RegExp(limiter, limiterModifier));
				
				for(var j:int = 0; j < innerArray.length; j++){
					var curr:String = innerArray[j];
					if(j < (innerArray.length - 1)){
						if(!!previousString) previousString += limiter.split("\\").join("");
						array.push(previousString + curr);
						previousString = "";
					}else{ // j == (innerArray.length - 1) - the last one
						if(!!previousString) previousString += limiter.split("\\").join("");
						previousString = previousString + curr;
					}
				}
				
			}
			if(!!previousString) array.push(previousString);
			return array;
		}
		
		
	}
}